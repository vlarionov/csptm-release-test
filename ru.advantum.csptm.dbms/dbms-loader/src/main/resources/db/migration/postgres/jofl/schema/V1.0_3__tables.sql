--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;
SET search_path TO jofl;



--
-- Name: jofl; Type: SCHEMA; Schema: -; Owner: jofl

CREATE TABLE ref_role (
    id_role text NOT NULL,
    role_name text NOT NULL,
    is_active numeric(1,0) NOT NULL
);


ALTER TABLE ref_role OWNER TO adv;

--
-- Name: COLUMN ref_role.id_role; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_role.id_role IS 'ИД роли';


--
-- Name: COLUMN ref_role.role_name; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_role.role_name IS 'Наименование роли';


--
-- Name: COLUMN ref_role.is_active; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_role.is_active IS 'Активна';
--
-- Name: app_menu; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE app_menu (
    id_menu numeric(18,0) NOT NULL,
    menu_title text NOT NULL,
    id_menu_parent numeric(18,0),
    db_method text,
    icon text,
    n_order numeric(18,0),
    schema_name text
);


ALTER TABLE app_menu OWNER TO adv;

--
-- Name: COLUMN app_menu.id_menu; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN app_menu.id_menu IS 'ИД Меню';


--
-- Name: COLUMN app_menu.menu_title; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN app_menu.menu_title IS 'Наименование меню';


--
-- Name: COLUMN app_menu.id_menu_parent; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN app_menu.id_menu_parent IS 'Меню родитель';


--
-- Name: COLUMN app_menu.db_method; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN app_menu.db_method IS 'Наименование метода';


--
-- Name: COLUMN app_menu.icon; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN app_menu.icon IS 'имя иконки';


--
-- Name: COLUMN app_menu.n_order; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN app_menu.n_order IS 'Порядок сортировки';


--
-- Name: COLUMN app_menu.schema_name; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN app_menu.schema_name IS 'Наименование схемы в которую будут генерироваться объекты';


--
-- Name: jf_project; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE jf_project (
    schema_name text NOT NULL,
    project_title text NOT NULL,
    "row$color" numeric(18,0)
);


ALTER TABLE jf_project OWNER TO adv;

--
-- Name: COLUMN jf_project.schema_name; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN jf_project.schema_name IS 'наименование схемы в которую будут генерироваться объекты';


--
-- Name: COLUMN jf_project.project_title; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN jf_project.project_title IS 'наименование прокта';


--
-- Name: COLUMN jf_project."row$color"; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN jf_project."row$color" IS 'подсветка проекта';

--
-- Name: role2call; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE role2call (
    id_role text NOT NULL,
    db_method text NOT NULL,
    call_action text NOT NULL
);


ALTER TABLE role2call OWNER TO adv;

--
-- Name: COLUMN role2call.id_role; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN role2call.id_role IS 'ИД роли';


--
-- Name: COLUMN role2call.db_method; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN role2call.db_method IS 'Наименование метода';


--
-- Name: COLUMN role2call.call_action; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN role2call.call_action IS 'Наименование метода (DO_ACTION)';

--
-- Name: win_action_detail; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE win_action_detail (
    db_method text NOT NULL,
    action_title text NOT NULL,
    detail_db_method text NOT NULL,
    is_row_depend numeric(1,0) DEFAULT 0 NOT NULL,
    n_sort_order numeric(18,0) DEFAULT 0 NOT NULL
);


ALTER TABLE win_action_detail OWNER TO adv;

--
-- Name: COLUMN win_action_detail.db_method; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_action_detail.db_method IS 'Наименование метода';


--
-- Name: COLUMN win_action_detail.action_title; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_action_detail.action_title IS 'Отображение ACTION';


--
-- Name: COLUMN win_action_detail.detail_db_method; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_action_detail.detail_db_method IS 'Наименование метода';


--
-- Name: COLUMN win_action_detail.is_row_depend; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_action_detail.is_row_depend IS 'Строкозависимая деталь';


--
-- Name: COLUMN win_action_detail.n_sort_order; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_action_detail.n_sort_order IS 'Порядок сортировки';

--
-- Name: win_field; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE win_field (
    db_method text NOT NULL,
    field_name text NOT NULL,
    field_cons_type numeric(1,0) NOT NULL,
    field_type character(1) NOT NULL,
    display_title text NOT NULL,
    display_description text,
    default_value text,
    n_order numeric(18,0) NOT NULL,
    n_visibility numeric(1,0) NOT NULL,
    is_display_name numeric(1,0) DEFAULT 0 NOT NULL,
    is_mandatory numeric(1,0) DEFAULT 0 NOT NULL,
    pattern text,
    max_len numeric(18,0),
    weight numeric(18,0) DEFAULT 1 NOT NULL,
    rowid text DEFAULT (nextval('sq_jofl'::regclass))::text
);


ALTER TABLE win_field OWNER TO adv;

--
-- Name: COLUMN win_field.db_method; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_field.db_method IS 'Наименование метода';


--
-- Name: COLUMN win_field.field_name; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_field.field_name IS 'Имя поля в БД ';


--
-- Name: COLUMN win_field.field_cons_type; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_field.field_cons_type IS 'Тип поля: 0 - PK, 1 - PK_WRITEABLE, 1 - RW, 2 - RO';


--
-- Name: COLUMN win_field.field_type; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_field.field_type IS 'Тип данных: N, V, T, D';


--
-- Name: COLUMN win_field.display_title; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_field.display_title IS 'Наименование объекта';


--
-- Name: COLUMN win_field.display_description; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_field.display_description IS 'Описание объекта';


--
-- Name: COLUMN win_field.default_value; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_field.default_value IS 'Значение по умолчанию';


--
-- Name: COLUMN win_field.n_order; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_field.n_order IS 'Порядок полей';


--
-- Name: COLUMN win_field.n_visibility; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_field.n_visibility IS 'Видимость поля: 0 - NONE, 1 - PROP, 2 - OUTLINE, 3 - BOTH';


--
-- Name: COLUMN win_field.is_display_name; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_field.is_display_name IS 'Является DisplayName';


--
-- Name: COLUMN win_field.is_mandatory; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_field.is_mandatory IS 'Признак обязательности поля';


--
-- Name: COLUMN win_field.pattern; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_field.pattern IS 'Формат поля';


--
-- Name: COLUMN win_field.max_len; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_field.max_len IS 'Длина значения в символах';


--
-- Name: COLUMN win_field.weight; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_field.weight IS 'Вес поля';


--
-- Name: win_action_call; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE win_action_call (
    db_method text NOT NULL,
    call_action text NOT NULL,
    action_title text NOT NULL,
    is_refresh numeric(1,0) DEFAULT 0 NOT NULL,
    is_internal numeric(1,0) DEFAULT 0 NOT NULL,
    is_win_action numeric(1,0) DEFAULT 0 NOT NULL,
    n_sort_order numeric(18,0),
    is_row_depend numeric(1,0) DEFAULT 0 NOT NULL,
    call_default text,
    opt_interface_pkg text
);


ALTER TABLE win_action_call OWNER TO adv;

--
-- Name: COLUMN win_action_call.db_method; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_action_call.db_method IS 'Наименование метода';


--
-- Name: COLUMN win_action_call.call_action; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_action_call.call_action IS 'Наименование метода (DO_ACTION)';


--
-- Name: COLUMN win_action_call.action_title; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_action_call.action_title IS 'Отображение ACTION';


--
-- Name: COLUMN win_action_call.is_refresh; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_action_call.is_refresh IS 'Требуется обновление';


--
-- Name: COLUMN win_action_call.is_internal; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_action_call.is_internal IS 'Внутренний вызов (INSERT, UPDATE, DELETE)';


--
-- Name: COLUMN win_action_call.is_win_action; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_action_call.is_win_action IS 'Метод окна';


--
-- Name: COLUMN win_action_call.n_sort_order; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_action_call.n_sort_order IS 'Порядок сортировки';


--
-- Name: COLUMN win_action_call.is_row_depend; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_action_call.is_row_depend IS 'Строкозависимый';


--
-- Name: COLUMN win_action_call.call_default; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_action_call.call_default IS 'Значения по умолчанию';


--
-- Name: COLUMN win_action_call.opt_interface_pkg; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_action_call.opt_interface_pkg IS 'Пакет (опционально)';

--
-- Name: win_field_ref; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE win_field_ref (
    db_method text NOT NULL,
    field_name text NOT NULL,
    ref_db_method text,
    ref_field text NOT NULL,
    dst_field text NOT NULL,
    is_custom_factory numeric(1,0) DEFAULT 0 NOT NULL,
    ref_factory text,
    rowid text DEFAULT (nextval('sq_jofl'::regclass))::text
);


ALTER TABLE win_field_ref OWNER TO adv;

--
-- Name: COLUMN win_field_ref.db_method; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_field_ref.db_method IS 'Наименование метода';


--
-- Name: COLUMN win_field_ref.field_name; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_field_ref.field_name IS 'Наименование поля';


--
-- Name: COLUMN win_field_ref.ref_db_method; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_field_ref.ref_db_method IS 'Наименование справочника';


--
-- Name: COLUMN win_field_ref.ref_field; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_field_ref.ref_field IS 'Имя поля справочника';


--
-- Name: COLUMN win_field_ref.dst_field; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_field_ref.dst_field IS 'Поле объекта';


--
-- Name: COLUMN win_field_ref.is_custom_factory; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_field_ref.is_custom_factory IS 'Признак того, что будет использована Custom фабрика';


--
-- Name: COLUMN win_field_ref.ref_factory; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_field_ref.ref_factory IS 'Класс (сервис AgileCustomPropertyEditorProvider)';


--
-- Name: window; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE "window" (
    db_method text NOT NULL,
    win_title text,
    custom_topcomponent text,
    custom_jscomponent text,
    action_factory text,
    editor_factory text,
    interface_pkg text,
    n_auth_based numeric(1,0) NOT NULL,
    cache_min numeric(18,0) DEFAULT 0 NOT NULL,
    icon text,
    refresh_interval_sec numeric(18,0) DEFAULT 0 NOT NULL,
    schema_name text
);


ALTER TABLE "window" OWNER TO adv;

--
-- Name: COLUMN "window".db_method; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN "window".db_method IS 'Наименование метода';


--
-- Name: COLUMN "window".custom_jscomponent; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN "window".custom_jscomponent IS 'JS-Класс';


--
-- Name: COLUMN "window".action_factory; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN "window".action_factory IS 'Класс фабрики Action';


--
-- Name: COLUMN "window".editor_factory; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN "window".editor_factory IS 'Фабрика редактора';


--
-- Name: COLUMN "window".interface_pkg; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN "window".interface_pkg IS 'Пакет отвечающий за работу объекта';


--
-- Name: COLUMN "window".n_auth_based; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN "window".n_auth_based IS '0 - без использования параметра ID_ACCOUNT, 1 - с использованием параметра';


--
-- Name: COLUMN "window".cache_min; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN "window".cache_min IS 'Кол-во минут которое будет работать кэш';


--
-- Name: COLUMN "window".icon; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN "window".icon IS 'имя иконки';


--
-- Name: COLUMN "window".refresh_interval_sec; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN "window".refresh_interval_sec IS 'Автоматически обновлять данные (сек)';


--
-- Name: COLUMN "window".schema_name; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN "window".schema_name IS 'Наименование схемы в которую будут генерироваться объекты';


--
-- Name: account; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE account (
    id_jofl_account numeric(18,0) NOT NULL,
    schema_name text NOT NULL,
    id_account_local numeric(18,0) NOT NULL
);


ALTER TABLE account OWNER TO adv;

--
-- Name: COLUMN account.id_jofl_account; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN account.id_jofl_account IS 'ИД Учетной записи JOFL';


--
-- Name: COLUMN account.schema_name; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN account.schema_name IS 'Наименование схемы в которую будут генерироваться объекты';


--
-- Name: COLUMN account.id_account_local; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN account.id_account_local IS 'ИД Учетной записи внешней системы';


--
-- Name: constants_num; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE constants_num (
    schema_name text NOT NULL,
    c_name text NOT NULL,
    c_val numeric,
    ref_type text NOT NULL
);


ALTER TABLE constants_num OWNER TO adv;

--
-- Name: COLUMN constants_num.schema_name; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN constants_num.schema_name IS 'наименование схемы';


--
-- Name: COLUMN constants_num.c_name; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN constants_num.c_name IS 'название константы';


--
-- Name: COLUMN constants_num.c_val; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN constants_num.c_val IS 'значение константы';


--
-- Name: COLUMN constants_num.ref_type; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN constants_num.ref_type IS 'ссылка на поле типа константы';


--
-- Name: constants_text; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE constants_text (
    schema_name text NOT NULL,
    c_name text NOT NULL,
    c_val text,
    ref_type text NOT NULL
);


ALTER TABLE constants_text OWNER TO adv;

--
-- Name: COLUMN constants_text.schema_name; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN constants_text.schema_name IS 'наименование схемы';


--
-- Name: COLUMN constants_text.c_name; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN constants_text.c_name IS 'название константы';


--
-- Name: COLUMN constants_text.c_val; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN constants_text.c_val IS 'значение константы';


--
-- Name: COLUMN constants_text.ref_type; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN constants_text.ref_type IS 'ссылка на поле типа константы';


--
-- Name: create$java$lob$table; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE "create$java$lob$table" (
    name text,
    lob bytea,
    loadtime timestamp without time zone
);


ALTER TABLE "create$java$lob$table" OWNER TO adv;

--
-- Name: java$options; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE "java$options" (
    what text,
    opt text,
    value text
);


ALTER TABLE "java$options" OWNER TO adv;

SET default_tablespace = jofl_data;

--
-- Name: ref_const_type; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE ref_const_type (
    field_cons_type numeric(1,0) NOT NULL,
    field_const_name text NOT NULL
);


ALTER TABLE ref_const_type OWNER TO adv;

--
-- Name: COLUMN ref_const_type.field_cons_type; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_const_type.field_cons_type IS 'Тип поля: 0 - PK, 1 - PK_WRITEABLE, 1 - RW, 2 - RO';


--
-- Name: COLUMN ref_const_type.field_const_name; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_const_type.field_const_name IS 'Наименование Constraint';


--
-- Name: ref_editor; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE ref_editor (
    ref_factory text
);


ALTER TABLE ref_editor OWNER TO adv;

--
-- Name: COLUMN ref_editor.ref_factory; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_editor.ref_factory IS 'Класс (сервис AgileCustomPropertyEditorProvider)';


--
-- Name: ref_field_type; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE ref_field_type (
    field_type character(1) NOT NULL,
    field_type_name text NOT NULL
);


ALTER TABLE ref_field_type OWNER TO adv;

--
-- Name: COLUMN ref_field_type.field_type; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_field_type.field_type IS 'Тип данных: N, V, T, D';


--
-- Name: COLUMN ref_field_type.field_type_name; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_field_type.field_type_name IS 'Наименование типа данных';


--
-- Name: ref_field_visibility; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE ref_field_visibility (
    n_visibility numeric(1,0) NOT NULL,
    visibility_name text NOT NULL
);


ALTER TABLE ref_field_visibility OWNER TO adv;

--
-- Name: COLUMN ref_field_visibility.n_visibility; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_field_visibility.n_visibility IS 'Видимость поля: 0 - NONE, 1 - PROP, 2 - OUTLINE, 3 - BOTH';


--
-- Name: COLUMN ref_field_visibility.visibility_name; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_field_visibility.visibility_name IS 'Наименование видимости';


--
-- Name: ref_filter; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE ref_filter (
    filter_class text NOT NULL,
    init_template text
);


ALTER TABLE ref_filter OWNER TO adv;

--
-- Name: COLUMN ref_filter.filter_class; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_filter.filter_class IS 'Класс фабрики фильтров';


--
-- Name: COLUMN ref_filter.init_template; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_filter.init_template IS 'Шаблон параметров инициализации';


--
-- Name: ref_filter_source; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE ref_filter_source (
    id_platform text NOT NULL,
    filter_class text NOT NULL,
    source_class text NOT NULL
);


ALTER TABLE ref_filter_source OWNER TO adv;

--
-- Name: COLUMN ref_filter_source.id_platform; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_filter_source.id_platform IS 'Платформа';


--
-- Name: COLUMN ref_filter_source.filter_class; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_filter_source.filter_class IS 'Класс фабрики фильтров';


--
-- Name: COLUMN ref_filter_source.source_class; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_filter_source.source_class IS 'Ссылка на java/js класс фильтра';


--
-- Name: ref_locale; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE ref_locale (
    lc_locale character(5) NOT NULL,
    rowid text DEFAULT (nextval('sq_jofl'::regclass))::text
);


ALTER TABLE ref_locale OWNER TO adv;

--
-- Name: TABLE ref_locale; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON TABLE ref_locale IS 'Поддерживаемые языки';


--
-- Name: COLUMN ref_locale.lc_locale; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_locale.lc_locale IS 'Language and Country';


--
-- Name: ref_locale_tag; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE ref_locale_tag (
    lc_tag text NOT NULL,
    rowid text DEFAULT (nextval('sq_jofl'::regclass))::text,
    tag_name text NOT NULL
);


ALTER TABLE ref_locale_tag OWNER TO adv;

--
-- Name: TABLE ref_locale_tag; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON TABLE ref_locale_tag IS 'Типы ключей для локализации';


--
-- Name: COLUMN ref_locale_tag.lc_tag; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_locale_tag.lc_tag IS 'Тип';


--
-- Name: COLUMN ref_locale_tag.tag_name; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_locale_tag.tag_name IS 'Наименование';


--
-- Name: ref_menu_vocabulary; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE ref_menu_vocabulary (
    lc_locale character(5) NOT NULL,
    id_menu numeric(18,0) NOT NULL,
    rowid text DEFAULT (nextval('sq_jofl'::regclass))::text,
    menu_title text NOT NULL
);


ALTER TABLE ref_menu_vocabulary OWNER TO adv;

--
-- Name: COLUMN ref_menu_vocabulary.lc_locale; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_menu_vocabulary.lc_locale IS 'Language and Country';


--
-- Name: COLUMN ref_menu_vocabulary.id_menu; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_menu_vocabulary.id_menu IS 'ИД Меню';


--
-- Name: COLUMN ref_menu_vocabulary.menu_title; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_menu_vocabulary.menu_title IS 'Название';


--
-- Name: ref_ui_platform; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE ref_ui_platform (
    id_platform text NOT NULL,
    plat_name text NOT NULL,
    rowid text DEFAULT (nextval('sq_jofl'::regclass))::text
);


ALTER TABLE ref_ui_platform OWNER TO adv;

--
-- Name: COLUMN ref_ui_platform.id_platform; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_ui_platform.id_platform IS 'Платформа';


--
-- Name: COLUMN ref_ui_platform.plat_name; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_ui_platform.plat_name IS 'Наименование';


--
-- Name: ref_vocabulary; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE ref_vocabulary (
    db_method text NOT NULL,
    lc_locale character(5) NOT NULL,
    lc_tag text NOT NULL,
    lc_key text,
    lc_value text NOT NULL,
    rowid text DEFAULT (nextval('sq_jofl'::regclass))::text
);


ALTER TABLE ref_vocabulary OWNER TO adv;

--
-- Name: TABLE ref_vocabulary; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON TABLE ref_vocabulary IS 'Словарь';


--
-- Name: COLUMN ref_vocabulary.db_method; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_vocabulary.db_method IS 'Наименование метода';


--
-- Name: COLUMN ref_vocabulary.lc_locale; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_vocabulary.lc_locale IS 'Language and Country';


--
-- Name: COLUMN ref_vocabulary.lc_tag; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_vocabulary.lc_tag IS 'Тип';


--
-- Name: COLUMN ref_vocabulary.lc_key; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_vocabulary.lc_key IS 'Ключ';


--
-- Name: COLUMN ref_vocabulary.lc_value; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN ref_vocabulary.lc_value IS 'Значение';


--
-- Name: role2account; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE role2account (
    id_role text NOT NULL,
    id_jofl_account numeric(18,0) NOT NULL
);


ALTER TABLE role2account OWNER TO adv;

--
-- Name: COLUMN role2account.id_role; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN role2account.id_role IS 'ИД роли';


--
-- Name: COLUMN role2account.id_jofl_account; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN role2account.id_jofl_account IS 'ИД Учетной записи JOFL';

--
-- Name: sq_role; Type: SEQUENCE; Schema: jofl; Owner: postgres
--

CREATE SEQUENCE sq_role
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_role OWNER TO adv;

SET default_tablespace = jofl_data;

--
-- Name: win_action_field; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE win_action_field (
    id_afield text NOT NULL,
    db_method text NOT NULL,
    call_action text NOT NULL,
    filter_class text NOT NULL,
    initial_param text,
    n_sort_order numeric(18,0),
    field_title text,
    rowid text DEFAULT (nextval('sq_jofl'::regclass))::text
);


ALTER TABLE win_action_field OWNER TO adv;

--
-- Name: COLUMN win_action_field.db_method; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_action_field.db_method IS 'Наименование метода';


--
-- Name: COLUMN win_action_field.call_action; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_action_field.call_action IS 'Наименование метода (DO_ACTION)';


--
-- Name: COLUMN win_action_field.filter_class; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_action_field.filter_class IS 'Класс фабрики фильтров';


--
-- Name: COLUMN win_action_field.initial_param; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_action_field.initial_param IS 'Параметр инициализации (JSON-MAP)';


--
-- Name: COLUMN win_action_field.n_sort_order; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_action_field.n_sort_order IS 'Порядок сортировки';


--
-- Name: COLUMN win_action_field.field_title; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_action_field.field_title IS 'Наименование фильтра';


--
-- Name: win_filter; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE win_filter (
    db_method text NOT NULL,
    filter_class text NOT NULL,
    initial_param text,
    grid_x numeric(18,0) NOT NULL,
    grid_y numeric(18,0) NOT NULL,
    filter_title text,
    id_filter text NOT NULL,
    rowid text DEFAULT (nextval('sq_jofl'::regclass))::text
);


ALTER TABLE win_filter OWNER TO adv;

--
-- Name: COLUMN win_filter.db_method; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_filter.db_method IS 'Наименование метода';


--
-- Name: COLUMN win_filter.filter_class; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_filter.filter_class IS 'Класс фабрики фильтров';


--
-- Name: COLUMN win_filter.initial_param; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_filter.initial_param IS 'Параметр инициализации (JSON-MAP)';


--
-- Name: COLUMN win_filter.grid_x; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_filter.grid_x IS 'Положение Х';


--
-- Name: COLUMN win_filter.grid_y; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_filter.grid_y IS 'Положение Y';


--
-- Name: COLUMN win_filter.filter_title; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_filter.filter_title IS 'Наименование поля';


--
-- Name: win_filter_opts; Type: TABLE; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

CREATE TABLE win_filter_opts (
    db_method text NOT NULL,
    but_apply_caption text NOT NULL,
    but_reset_caption text NOT NULL,
    is_reset_present numeric(1,0) NOT NULL,
    rowid text DEFAULT (nextval('sq_jofl'::regclass))::text
);


ALTER TABLE win_filter_opts OWNER TO adv;

--
-- Name: COLUMN win_filter_opts.db_method; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_filter_opts.db_method IS 'Наименование метода';


--
-- Name: COLUMN win_filter_opts.but_apply_caption; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_filter_opts.but_apply_caption IS 'Текст "Apply"';


--
-- Name: COLUMN win_filter_opts.but_reset_caption; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_filter_opts.but_reset_caption IS 'Текст "Reset"';


--
-- Name: COLUMN win_filter_opts.is_reset_present; Type: COMMENT; Schema: jofl; Owner: postgres
--

COMMENT ON COLUMN win_filter_opts.is_reset_present IS 'Видно Reset';

SET default_tablespace = jofl_idx;

--
-- Name: pk_account; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY account
    ADD CONSTRAINT pk_account PRIMARY KEY (id_jofl_account);


--
-- Name: pk_app_menu; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY app_menu
    ADD CONSTRAINT pk_app_menu PRIMARY KEY (id_menu);


--
-- Name: pk_constants_num; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY constants_num
    ADD CONSTRAINT pk_constants_num PRIMARY KEY (c_name);


--
-- Name: pk_constants_text; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY constants_text
    ADD CONSTRAINT pk_constants_text PRIMARY KEY (c_name);


--
-- Name: pk_jf_project; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY jf_project
    ADD CONSTRAINT pk_jf_project PRIMARY KEY (schema_name);


--
-- Name: pk_jofl_role; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY ref_role
    ADD CONSTRAINT pk_jofl_role PRIMARY KEY (id_role);


--
-- Name: pk_ref_const_type; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY ref_const_type
    ADD CONSTRAINT pk_ref_const_type PRIMARY KEY (field_cons_type);


--
-- Name: pk_ref_field_type; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY ref_field_type
    ADD CONSTRAINT pk_ref_field_type PRIMARY KEY (field_type);


--
-- Name: pk_ref_field_visibility; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY ref_field_visibility
    ADD CONSTRAINT pk_ref_field_visibility PRIMARY KEY (n_visibility);


--
-- Name: pk_ref_filter; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY ref_filter
    ADD CONSTRAINT pk_ref_filter PRIMARY KEY (filter_class);


--
-- Name: pk_ref_filter_source; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY ref_filter_source
    ADD CONSTRAINT pk_ref_filter_source PRIMARY KEY (id_platform, filter_class);


--
-- Name: pk_ref_locale; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY ref_locale
    ADD CONSTRAINT pk_ref_locale PRIMARY KEY (lc_locale);


--
-- Name: pk_ref_locale_tag; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY ref_locale_tag
    ADD CONSTRAINT pk_ref_locale_tag PRIMARY KEY (lc_tag);


--
-- Name: pk_ref_menu_vocabulary; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY ref_menu_vocabulary
    ADD CONSTRAINT pk_ref_menu_vocabulary PRIMARY KEY (lc_locale, id_menu);


--
-- Name: pk_ref_ui_platform; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY ref_ui_platform
    ADD CONSTRAINT pk_ref_ui_platform PRIMARY KEY (id_platform);


--
-- Name: pk_role2account; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY role2account
    ADD CONSTRAINT pk_role2account PRIMARY KEY (id_role, id_jofl_account);


--
-- Name: pk_role2call; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY role2call
    ADD CONSTRAINT pk_role2call PRIMARY KEY (id_role, db_method, call_action);


--
-- Name: pk_win_action_call; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY win_action_call
    ADD CONSTRAINT pk_win_action_call PRIMARY KEY (db_method, call_action);


--
-- Name: pk_win_action_field; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY win_action_field
    ADD CONSTRAINT pk_win_action_field PRIMARY KEY (id_afield);


--
-- Name: pk_win_detail; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY win_action_detail
    ADD CONSTRAINT pk_win_detail PRIMARY KEY (db_method, detail_db_method);


--
-- Name: pk_win_field; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY win_field
    ADD CONSTRAINT pk_win_field PRIMARY KEY (field_name, db_method);


--
-- Name: pk_win_field_ref; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY win_field_ref
    ADD CONSTRAINT pk_win_field_ref PRIMARY KEY (db_method, field_name, dst_field);


--
-- Name: pk_win_filter; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY win_filter
    ADD CONSTRAINT pk_win_filter PRIMARY KEY (id_filter, db_method);


--
-- Name: pk_win_filter_opts; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY win_filter_opts
    ADD CONSTRAINT pk_win_filter_opts PRIMARY KEY (db_method);


--
-- Name: pk_window; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY "window"
    ADD CONSTRAINT pk_window PRIMARY KEY (db_method);


SET default_tablespace = jofl_data;

--
-- Name: pku_createjavalobtable; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_data
--

ALTER TABLE ONLY "create$java$lob$table"
    ADD CONSTRAINT pku_createjavalobtable UNIQUE (name);


SET default_tablespace = jofl_idx;

--
-- Name: uq_ref_vocabulary_translate; Type: CONSTRAINT; Schema: jofl; Owner: postgres; Tablespace: jofl_idx
--

ALTER TABLE ONLY ref_vocabulary
    ADD CONSTRAINT uq_ref_vocabulary_translate UNIQUE (db_method, lc_locale, lc_tag, lc_key);


--
-- Name: account_schema_name_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY account
    ADD CONSTRAINT account_schema_name_fkey FOREIGN KEY (schema_name) REFERENCES jf_project(schema_name) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_menu_db_method_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY app_menu
    ADD CONSTRAINT app_menu_db_method_fkey FOREIGN KEY (db_method) REFERENCES "window"(db_method) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_menu_id_menu_parent_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY app_menu
    ADD CONSTRAINT app_menu_id_menu_parent_fkey FOREIGN KEY (id_menu_parent) REFERENCES app_menu(id_menu) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_menu_schema_name_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY app_menu
    ADD CONSTRAINT app_menu_schema_name_fkey FOREIGN KEY (schema_name) REFERENCES jf_project(schema_name) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ref_filter_source_filter_class_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY ref_filter_source
    ADD CONSTRAINT ref_filter_source_filter_class_fkey FOREIGN KEY (filter_class) REFERENCES ref_filter(filter_class) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ref_filter_source_id_platform_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY ref_filter_source
    ADD CONSTRAINT ref_filter_source_id_platform_fkey FOREIGN KEY (id_platform) REFERENCES ref_ui_platform(id_platform) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ref_menu_vocabulary_id_menu_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY ref_menu_vocabulary
    ADD CONSTRAINT ref_menu_vocabulary_id_menu_fkey FOREIGN KEY (id_menu) REFERENCES app_menu(id_menu);


--
-- Name: ref_menu_vocabulary_lc_locale_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY ref_menu_vocabulary
    ADD CONSTRAINT ref_menu_vocabulary_lc_locale_fkey FOREIGN KEY (lc_locale) REFERENCES ref_locale(lc_locale);


--
-- Name: ref_vocabulary_db_method_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY ref_vocabulary
    ADD CONSTRAINT ref_vocabulary_db_method_fkey FOREIGN KEY (db_method) REFERENCES "window"(db_method) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ref_vocabulary_lc_locale_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY ref_vocabulary
    ADD CONSTRAINT ref_vocabulary_lc_locale_fkey FOREIGN KEY (lc_locale) REFERENCES ref_locale(lc_locale) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ref_vocabulary_lc_tag_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY ref_vocabulary
    ADD CONSTRAINT ref_vocabulary_lc_tag_fkey FOREIGN KEY (lc_tag) REFERENCES ref_locale_tag(lc_tag) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: role2account_id_jofl_account_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY role2account
    ADD CONSTRAINT role2account_id_jofl_account_fkey FOREIGN KEY (id_jofl_account) REFERENCES account(id_jofl_account) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: role2account_id_role_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY role2account
    ADD CONSTRAINT role2account_id_role_fkey FOREIGN KEY (id_role) REFERENCES ref_role(id_role) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: role2call_db_method_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY role2call
    ADD CONSTRAINT role2call_db_method_fkey FOREIGN KEY (db_method, call_action) REFERENCES win_action_call(db_method, call_action) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: role2call_id_role_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY role2call
    ADD CONSTRAINT role2call_id_role_fkey FOREIGN KEY (id_role) REFERENCES ref_role(id_role) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: win_action_call_db_method_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY win_action_call
    ADD CONSTRAINT win_action_call_db_method_fkey FOREIGN KEY (db_method) REFERENCES "window"(db_method) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: win_action_detail_db_method_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY win_action_detail
    ADD CONSTRAINT win_action_detail_db_method_fkey FOREIGN KEY (db_method) REFERENCES "window"(db_method) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: win_action_detail_detail_db_method_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY win_action_detail
    ADD CONSTRAINT win_action_detail_detail_db_method_fkey FOREIGN KEY (detail_db_method) REFERENCES "window"(db_method) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: win_action_field_db_method_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY win_action_field
    ADD CONSTRAINT win_action_field_db_method_fkey FOREIGN KEY (db_method, call_action) REFERENCES win_action_call(db_method, call_action) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: win_action_field_filter_class_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY win_action_field
    ADD CONSTRAINT win_action_field_filter_class_fkey FOREIGN KEY (filter_class) REFERENCES ref_filter(filter_class) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: win_field_db_method_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY win_field
    ADD CONSTRAINT win_field_db_method_fkey FOREIGN KEY (db_method) REFERENCES "window"(db_method) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: win_field_field_cons_type_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY win_field
    ADD CONSTRAINT win_field_field_cons_type_fkey FOREIGN KEY (field_cons_type) REFERENCES ref_const_type(field_cons_type) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: win_field_field_type_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY win_field
    ADD CONSTRAINT win_field_field_type_fkey FOREIGN KEY (field_type) REFERENCES ref_field_type(field_type) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: win_field_n_visibility_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY win_field
    ADD CONSTRAINT win_field_n_visibility_fkey FOREIGN KEY (n_visibility) REFERENCES ref_field_visibility(n_visibility) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: win_field_ref_field_name_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY win_field_ref
    ADD CONSTRAINT win_field_ref_field_name_fkey FOREIGN KEY (field_name, db_method) REFERENCES win_field(field_name, db_method);


--
-- Name: win_field_ref_ref_db_method_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY win_field_ref
    ADD CONSTRAINT win_field_ref_ref_db_method_fkey FOREIGN KEY (ref_db_method) REFERENCES "window"(db_method) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: win_filter_db_method_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY win_filter
    ADD CONSTRAINT win_filter_db_method_fkey FOREIGN KEY (db_method) REFERENCES "window"(db_method) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: win_filter_filter_class_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY win_filter
    ADD CONSTRAINT win_filter_filter_class_fkey FOREIGN KEY (filter_class) REFERENCES ref_filter(filter_class) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: win_filter_opts_db_method_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY win_filter_opts
    ADD CONSTRAINT win_filter_opts_db_method_fkey FOREIGN KEY (db_method) REFERENCES "window"(db_method) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: window_schema_name_fkey; Type: FK CONSTRAINT; Schema: jofl; Owner: postgres
--

ALTER TABLE ONLY "window"
    ADD CONSTRAINT window_schema_name_fkey FOREIGN KEY (schema_name) REFERENCES jf_project(schema_name) DEFERRABLE INITIALLY DEFERRED;