create or replace function  jofl.jofl_link_util$build(link text[]) returns text as $$
	declare
		lv_out text;
	begin
		lv_out := '[';
		for i in 1 .. array_length(link, 1)
		loop
			lv_out := lv_out || link[i];
			if (i != array_length(link, 1))
			then
				lv_out := lv_out || ', ';
			end if;
		end loop;
	
		return lv_out || ']';
	end ;
$$ LANGUAGE plpgsql;