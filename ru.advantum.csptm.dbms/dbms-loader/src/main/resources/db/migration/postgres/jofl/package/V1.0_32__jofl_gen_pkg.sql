create or replace function  jofl.jofl_gen_pkg$fill_fields(adb_method jofl.window.db_method%type
											 																	 ,aobj_name text) returns void as $$
	DECLARE
		lv_own  text;
		lv_name text;
		lv_type text;

		lv_ct         jofl.win_field.field_cons_type%type;
		lv_field_type jofl.win_field.field_type%type;
		c record;
		const record;
	begin
		begin
			select table_schema
						,table_name
						,'TABLE' as object_type
			into strict lv_own
					,lv_name
					,lv_type
			from information_schema.tables
			where table_schema || '.' || table_name = lower(aobj_name);
		exception
			when no_data_found then
				RAISE EXCEPTION USING ERRCODE = 20001, MESSAGE = '<<Объект не найден: ' || aobj_name || '>>';
		end;

		if (lv_type = 'TABLE')
		then
			for c in (
						with seld as (select relname as tbl,  nspname as sch, description
											from pg_description pd
													join pg_class pc on pd.objoid = pc.oid
														join pg_namespace pn on pc.relnamespace = pn.oid)
											select tc.column_name
														,data_type
														,is_nullable as NULLABLE
														,numeric_precision as DATA_PRECISION
														,numeric_scale as DATA_SCALE
														,ordinal_position as COLUMN_ID
														,description as COMMENTS
														,tc.character_maximum_length as CHAR_LENGTH
											from information_schema.columns  tc
													left join seld seld on (seld.tbl=tc.table_name and seld.sch=tc.table_schema)
											where tc.table_schema =lv_own and tc.table_name = lv_name
											order by ordinal_position)
			loop
				-- проверим тип constraint
				lv_ct := 2;
				for const in (SELECT substr(upper(constraint_type),1,1) as constraint_type
											FROM
													information_schema.table_constraints AS tc
													JOIN information_schema.key_column_usage AS kcu
														ON tc.constraint_name = kcu.constraint_name
													JOIN information_schema.constraint_column_usage AS ccu
														ON ccu.constraint_name = tc.constraint_name
											WHERE tc.table_schema= lv_own AND tc.table_name= lv_name
										  order by ordinal_position nulls last)
				loop
					if (const.constraint_type = 'P')
					then
						lv_ct := 0;
					end if;
					exit;
				end loop;

				if (c.data_type ilike '%NUMERIC%' or c.data_type ilike '%DOUBLE%' and coalesce(c.data_precision, 0) = 1)
				then
					lv_field_type := 'B';
                elsif (c.data_type ilike '%NUMERIC%' or c.data_type ilike '%DOUBLE%' or c.data_type ILIKE 'INTEGER')
                then
                    lv_field_type := 'N';
                elsif (c.data_type ilike '%timestamp%')
                then
                    lv_field_type := 'T';
				else
					lv_field_type := 'V';
				end if;

				insert into jofl.win_field
					(db_method
					,field_name
					,field_cons_type
					,field_type
					,display_title
					,display_description
					,default_value
					,n_order
					,n_visibility
					,is_display_name
					,is_mandatory
					,max_len
					,weight)
				values
					(adb_method
					,upper(c.column_name)
					,lv_ct
					,lv_field_type
					,coalesce(nullif(c.comments,''), c.column_name)
					,c.comments
					,null
					,c.column_id * 10
					,3
					,0
					, decode(c.nullable::text, 'Y'::text, 0::integer, 1::integer)
					,case when lv_field_type = 'V' and coalesce(c.char_length, 0) > 0 then
					 c.char_length else null end
					,1);
			end loop;
		end if;

end;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------

create or replace function  jofl.jofl_pkg$attr_to_rowtype(attr text) returns jofl.window as $$
	declare
		lr jofl.window%rowtype;
	begin
		lr.db_method := trim(jofl.jofl_pkg$extract_varchar(attr,'DB_METHOD', false));
		lr.win_title := jofl.jofl_pkg$extract_varchar(attr,'WIN_TITLE', true);
		lr.cache_min := jofl.jofl_pkg$extract_number(attr,'CACHE_MIN', true);
		lr.refresh_interval_sec := jofl.jofl_pkg$extract_number(attr,'REFRESH_INTERVAL_SEC', true);

		lr.n_auth_based := jofl.jofl_pkg$extract_boolean(attr,'N_AUTH_BASED', true);

		-- NULLABLE параметры
		BEGIN
			lr.custom_topcomponent := jofl.jofl_pkg$extract_varchar(attr,'CUSTOM_TOPCOMPONENT', true);
		exception  WHEN others THEN
			lr.custom_topcomponent := null;
		end;

		lr.custom_jscomponent := jofl.jofl_pkg$extract_varchar(attr, 'CUSTOM_JSCOMPONENT', true);

		begin
			lr.action_factory := jofl.jofl_pkg$extract_varchar(attr, 'ACTION_FACTORY', true);
		exception
			when others then
				lr.action_factory := null;
		end;

		lr.editor_factory := jofl.jofl_pkg$extract_varchar(attr, 'EDITOR_FACTORY', true);

		begin
			lr.interface_pkg := trim(both ' ' from jofl.jofl_pkg$extract_varchar(attr,'INTERFACE_PKG',true));
		exception
			when others then
				lr.interface_pkg := null;
		end;

		begin
			lr.icon := attr('ICON');
		exception
			when others then
				lr.icon := null;
		end;

		begin
			lr.schema_name := jofl.jofl_pkg$extract_varchar(attr,'SCHEMA_NAME',true);
		exception
			when others then
				lr.schema_name := null;
		end;

		return lr;
	end;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------

create or replace function  jofl.jofl_gen_pkg$gen_extract_by_field_type(afield_name jofl.win_field.field_name%type
							  	 ,afield_type jofl.win_field.field_type%type) returns text as $$
	declare
	begin
		if afield_type = any (ARRAY['N','F']) then
			return 'jofl.jofl_pkg$extract_number(attr, ''' ||afield_name||''', true); ';
		elsif afield_type = any (ARRAY['L','R', 'V']) then
			return 'jofl.jofl_pkg$extract_varchar(attr, ''' ||afield_name||''', true); ';
		elsif afield_type = 'B' then
			return 'jofl.jofl_pkg$extract_boolean(attr, ''' ||afield_name||''', true); ';
		elsif afield_type = any (ARRAY['D','T']) then
			return 'jofl.jofl_pkg$extract_date(attr, ''' ||afield_name||''', true); ';
		else
			return '';
		end if;
	end;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------

create or replace function  jofl.jofl_gen_pkg$gen_attr_to_rowtype(adb_method jofl.window.db_method%type
								 ,apkg_name jofl.window.interface_pkg%type
							  	 ,aobj       text) returns text as $$
	declare
		lfields record;
		lfunc text;

	begin
		if not exists (select 1 from jofl.win_field where db_method = adb_method)
		then
			return null;
		end if;

		lfunc := 'create or replace function  ' || lower(apkg_name) || '$attr_to_rowtype(attr text) returns '
			|| lower(aobj) || ' as $' || '$ ' || chr(10) || '	declare ' || chr(10) || ' 		lr '
			|| lower(aobj) || '%rowtype; ' || chr(10) || 'begin ' || chr(10);

		for lfields in (select * from jofl.win_field where db_method = adb_method  )
			loop
				if position ('$' in lfields.field_name) < 1 then
					lfunc := lfunc || '  		lr.' || lower(lfields.field_name) || ' := ' ||
						jofl.jofl_gen_pkg$gen_extract_by_field_type(lfields.field_name, lfields.field_type) || chr(10);
				end if;
			end loop;
		lfunc := lfunc || ' 	return lr;'	|| chr(10)
			       || 'end;' || chr(10) || ' $' || '$ LANGUAGE plpgsql;';
		return lfunc;
	end;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------

create or replace function  jofl.jofl_gen_pkg$gen_of_rows(apkg_name jofl.window.interface_pkg%type
							  ,an_auth   jofl.window.n_auth_based%type) returns text as $$
	declare
		lfields record;
		lfunc text;
		lauth text;
	begin
		if an_auth = 1 then
			lauth := 'aid_account numeric,';
		else
			lauth := '';
		end if;
		lfunc := 'create or replace function  ' || lower(apkg_name) || '$of_rows('|| lauth || ' out arows refcursor, attr text) returns refcursor '
			|| ' as $' || '$ ' || chr(10) || '	declare ' || chr(10) || 'begin ' || chr(10)
			|| 'end;' || chr(10) || ' $' || '$ LANGUAGE plpgsql;';
		return lfunc;
	end;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------

create or replace function  jofl.jofl_gen_pkg$gen_of_action(apkg_name jofl.window.interface_pkg%type
							   								,an_auth   jofl.window.n_auth_based%type
							   								,aaction text) returns text as $$
	declare
		lfields record;
		lfunc text;
		lauth text;
	begin
		if an_auth = 1 then
			lauth := 'aid_account numeric,';
		else
			lauth := '';
		end if;
		lfunc := 'create or replace function  ' || lower(apkg_name) || '$' || aaction || '('|| lauth || ' attr text) returns text '
			|| ' as $' || '$ ' || chr(10) || '	declare ' || chr(10) || 'begin ' || chr(10)
			|| ' 		return null;' || chr(10)
			|| 'end;' || chr(10) || ' $' || '$ LANGUAGE plpgsql;';
		return lfunc;
	end;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------

create or replace function  jofl.jofl_gen_pkg$generate_package(adb_method jofl.window.db_method%type
							     ,apackage_name jofl.window.interface_pkg%type
							     ,aobject       text) returns jofl.window.interface_pkg%type as $$
	declare
		lfunc text;
		lschema jofl.window.schema_name%type;
		ln_auth   jofl.window.n_auth_based%type;
		lpkg_name jofl.window.interface_pkg%type;
		lobject text;
		laction text;
	begin

		select interface_pkg,
			schema_name,
			n_auth_based
			into
			lpkg_name,
			lschema,
			ln_auth
		from jofl.window;

		if lpkg_name is null then
			RAISE EXCEPTION USING ERRCODE = 20001, MESSAGE = '<<Метод не существует!>>';
		end if;

		if not exists (select 1 from pg_tables where (schemaname=lower(lschema) and tablename =lower(aobject)) or schemaname||'.'||tablename=lower(aobject)) then
			RAISE EXCEPTION USING ERRCODE = 20001, MESSAGE = '<<Объект не существует!>>';
		end if;

		if position('.' in apackage_name) < 1 then
			lpkg_name := lschema||'.'||apackage_name;
		else
			lpkg_name := apackage_name;
		end if;

		if position('.' in aobject) < 1 then
			lobject := lschema||'.'||aobject;
		else
			lobject := aobject;
		end if;

		lfunc := jofl.jofl_gen_pkg$gen_attr_to_rowtype(adb_method, lpkg_name, lobject);

		if lfunc is not null then
			execute lfunc;
		end if;

		lfunc := jofl.jofl_gen_pkg$gen_of_rows(lpkg_name, ln_auth);
		execute lfunc;

		for laction in (select unnest from unnest(ARRAY['of_insert', 'of_delete', 'of_update']))
		loop
			lfunc := jofl.jofl_gen_pkg$gen_of_action(lpkg_name, ln_auth, laction);
			execute lfunc;
		end loop;

		return  upper(lpkg_name);

	end;
$$ LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
