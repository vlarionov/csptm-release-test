create or replace function  jofl.jofl_pkg$register_account(aid_account_local jofl.account.id_account_local%type
    				  			   ,aproj jofl.account.schema_name%type) returns  jofl.account.id_jofl_account%type  AS $$
    declare
    	ln_jofl_account jofl.account.id_jofl_account%type;
    begin
    select a.id_jofl_account
    		into strict ln_jofl_account
    		from jofl.account a
    		where a.id_account_local = aid_account_local and a.schema_name = aproj;

    		return ln_jofl_account;
    	exception
    		when no_data_found then
    			insert into jofl.account
    				(id_jofl_account
    				,schema_name
    				,id_account_local)
    			values
    				(nextval('jofl.sq_jofl')
    				,aproj
    				,aid_account_local)
    			returning id_jofl_account into ln_jofl_account;
    			return ln_jofl_account;
    end;
    $$ LANGUAGE plpgsql;

    -----------------------------------------------------------------------------------------------------------------------------------------------
 create or replace function  jofl.jofl_pkg$grant_role(aid_account_local numeric,
        						    aproj text,
        						    arole text,
        						    single_role boolean DEFAULT false)  RETURNS void AS $$

        	declare
        		ln_jofl_account jofl.account.id_jofl_account%type;
        		lv_role         jofl.ref_role.id_role%type;
        		ldupl           numeric;
        	begin
        		ln_jofl_account := jofl.jofl_pkg$register_account(aid_account_local,
        								  aproj);

        		if (single_role)
        		then
        			with
        			nrs as (select arole as id_role,
        				       ln_jofl_account as id_jofl_account
        			),
        			dl as (select count (1) cnt from  jofl.role2account ra , nrs
        					where ra.id_jofl_account=nrs.id_jofl_account

        			),
        			upd as (update jofl.role2account ra
        				set id_role = nrs.id_role
        				FROM nrs, dl
        				where ra.id_jofl_account = nrs.id_jofl_account and dl.cnt< 2
        				returning *  ),
        			inst as (
        				insert into jofl.role2account
        						(id_role
        						,id_jofl_account)
        					select id_role
        						,id_jofl_account from nrs, dl where dl.cnt=0
        				returning *
        			)
        			select dl.cnt into ln_jofl_account from inst
        			full outer join  dl
        			on 1=1
        			full outer join  upd
        			on 1=1;

        			if (ldupl > 1)
        			then
        				RAISE EXCEPTION USING ERRCODE = 20001, MESSAGE = '<<Пользователю уже назначена более чем 1 роль!>>';
        			end if;
        		else
        			select id_role
        			into lv_role
        			from jofl.role2account ra
        			where ra.id_jofl_account = ln_jofl_account and ra.id_role = arole;
        		end if;

        	exception
        		when no_data_found then
        			insert into role2account
        				(id_role
        				,id_jofl_account)
        			values
        				(arole
        				,ln_jofl_account);
        end;
        $$ LANGUAGE plpgsql;

    ------------------------------------------------------------------------------------------------------------------------------------------------
 create or replace function  jofl.jofl_pkg$revoke_role(	aid_account_local numeric,
    				aproj text,
    				arole text)  RETURNS void AS $$

    	declare
    		ln_jofl_account jofl.account.id_jofl_account%type;
    		r record;
    	begin
    		ln_jofl_account := jofl.jofl_pkg$register_account(aid_account_local,
    								  aproj);

		with sel as (select id_jofl_account as jacc from jofl.account where id_account_local=ln_jofl_account
				and schema_name=aproj),
		     del as (delete from jofl.role2account ra
				where ra.id_role = arole and ra.id_jofl_account in (select jacc from sel)
				returning *)
		select * from sel, del into r ;
    	end;
    $$ LANGUAGE plpgsql;

    ------------------------------------------------------------------------------------------------------------------------------------------------
    create or replace function  jofl.jofl_pkg$unregister_account(aid_account_local jofl.account.id_account_local%type,
    							     aproj jofl.account.schema_name%type) RETURNS void AS $$
    	declare
			r record;
    	begin
    		with
    			rs as (select a.id_jofl_account
    				from jofl.account a
    				where a.schema_name = aproj
    					and a.id_account_local = aid_account_local
    			),
    			dl_acc as (delete from jofl.account a
    				where a.id_jofl_account in (select id_jofl_account from rs)
    				returning * ),
    			dl_r2a as (delete from jofl.role2account ra
    				 where ra.id_jofl_account in (select id_jofl_account from rs)
    				returning * )
    			select * into r from dl_acc full outer  join dl_r2a on 1=1;
    	end;
    $$ LANGUAGE plpgsql;



    ------------------------------------------------------------------------------------------------------------------------------------------------
    create or replace function  jofl.jofl_pkg$translate(aid_jofl_account jofl.account.id_jofl_account%type) RETURNS jofl.account.id_account_local%type AS $$
    	select id_account_local from jofl.account where id_jofl_account = aid_jofl_account;
    $$ LANGUAGE SQL STRICT IMMUTABLE;
    ------------------------------------------------------------------------------------------------------------------------------------------------
    create or replace function  jofl.jofl_pkg$translate(aid_account_local jofl.account.id_account_local%type
    						    ,aproj jofl.jf_project.schema_name%type)  RETURNS jofl.account.id_jofl_account%type AS $$
    select a.id_jofl_account
    	from jofl.account a
    	where a.id_account_local = aid_account_local and a.schema_name = aproj
    union all
    	select -1 as id_jofl_account
    	where not exists (select 1
    			  from jofl.account a
    			  where a.id_account_local = aid_account_local
    			  and a.schema_name = aproj);
    $$ LANGUAGE SQL STRICT IMMUTABLE;

    ------------------------------------------------------------------------------------------------------------------------------------------------
    --select * from jofl.jofl_pkg$validate_row('','2;',1)
    create or replace function  jofl.jofl_pkg$validate_row(atable_name text
    						 ,out amsg text
    						 ,out avalid   boolean
    						 ,arowid   text
    						 ,ascn     numeric default null) AS $$
    	begin
    		avalid := true;
    		amsg   := null;
    	end ;
    $$ LANGUAGE plpgsql;

    ------------------------------------------------------------------------------------------------------------------------------------------------
    create or replace function  jofl.jofl_pkg$extract_date(a text,
    				allownull boolean default false) returns timestamp without time zone as $$
    	declare
    		ldt timestamp without time zone;
    	begin
    		ldt := a::timestamp without time zone;
    		if allownull or ldt is not null then
    			return ldt;
    		else
    			RAISE  data_exception;
    		end if;
    	exception when invalid_datetime_format then
    		if allownull then
    			return null;
    		else
    			RAISE  data_exception;
    		end if;
    	end ;
    $$ LANGUAGE plpgsql;

    ------------------------------------------------------------------------------------------------------------------------------------------------

    create or replace function  jofl.jofl_pkg$extract_date(attr      text,
    			      attrname  text,
    			      allownull boolean default false)  returns timestamp without time zone as $$
    	declare
    		latrval text;
    		ldt timestamp without time zone;
    	begin
    		select value::text into latrval from json_each_text(attr::json) where key = attrname;
    		ldt := latrval::timestamp without time zone;
    		if allownull or ldt is not null then
    			return ldt;
    		else
    			RAISE  data_exception;
    		end if;
    	exception when invalid_datetime_format then
    		if allownull then
    			return null;
    		else
    			RAISE  data_exception;
    		end if;
    	end ;
    $$ LANGUAGE plpgsql;
    ------------------------------------------------------------------------------------------------------------------------------------------------
    create or replace function  jofl.jofl_pkg$extract_number(attr      text,
    						      attrname  text,
    						      allownull boolean default false)  returns numeric as $$
    	declare
    		latrval numeric;
    	begin
    		select nullif(value,'')::numeric into latrval from json_each_text(attr::json) where key = attrname;

    		if allownull or latrval is not null then
    			return latrval;
    		else
    			RAISE  data_exception;
    		end if;
    	exception when invalid_datetime_format then
    		if allownull then
    			return null;
    		else
    			RAISE data_exception;
    		end if;
    	end ;
    $$ LANGUAGE plpgsql;
    ------------------------------------------------------------------------------------------------------------------------------------------------
    create or replace function  jofl.jofl_pkg$extract_varchar(attr      text,
    						      attrname  text,
    						      allownull boolean default false)  returns text as $$
    	declare
    		latrval text;
    	begin
    		select value::text into latrval from json_each_text(attr::json) where key = attrname;

    		if allownull or latrval is not null then
    			return latrval;
    		else
    			RAISE data_exception;
    		end if;
    	exception when invalid_datetime_format then
    		if allownull then
    			return null;
    		else
    			RAISE data_exception;
    		end if;
    	end ;
    $$ LANGUAGE plpgsql;

    ------------------------------------------------------------------------------------------------------------------------------------------------
    create or replace function  jofl.jofl_pkg$extract_display_name(adbmethod jofl.window.db_method%type,
    				arows refcursor,
    				out adisplay_name text) returns text as $$
    		declare
    			rec record;
    			mdrec record;
    			jsonrec json;
    		begin
    		/*В постгрес это проще сделать через JSON*/
    			adisplay_name := null;
    			FETCH arows INTO rec;
    			EXIT WHEN NOT FOUND;
    			jsonrec = row_to_json(rec);
    				if exists (select 1 from json_each_text(jsonrec) where key in (
    								select field_name
    									from jofl.win_field f
    										where db_method = adbmethod and is_display_name = 1 order by n_order))
    				then


    					for mdrec in (select f.field_name, xmlmd.value
    							from jofl.win_field f
    							join (select key as name, value from json_each_text(jsonrec) ) xmlmd
    								on xmlmd.name = f.field_name
    							where f.db_method = adbmethod and f.is_display_name = 1
    							order by n_order
    							)
    					loop
    						if (adisplay_name is null)
    							then
    								adisplay_name := mdrec.value;
    							else
    								adisplay_name := adisplay_name || ', ' || mdrec.value;
    							end if;
    					end loop;
    				end if;
    		END;
    $$ LANGUAGE plpgsql;
    ------------------------------------------------------------------------------------------------------------------------------------------------

create or replace function  jofl.jofl_pkg$get_modify_opts(aid_jofl_account jofl.account.id_jofl_account%type
							 ,adb_method  jofl.window.db_method%type) returns numeric as $$
declare
	lres numeric;
begin
	if (jofl.jofl_pkg$isrootrole(aid_jofl_account))
		then
			return 15;
		else
			with t as (select ac.call_action
					from jofl.role2account    r2a
						,jofl.role2call       r2c
						,jofl.win_action_call ac
						where r2a.id_jofl_account = aid_jofl_account and
							r2a.id_role = r2c.id_role and r2c.db_method =  adb_method and
							r2c.db_method = ac.db_method and
							r2c.call_action = ac.call_action and ac.is_internal = 1)
			select ((exists (select 1 from t where call_action='OF_ROWS'))::integer::text||
				   (exists (select 1 from t where call_action='OF_DELETE'))::integer::text||
				   (exists (select 1 from t where call_action='OF_INSERT'))::integer::text||
				   (exists (select 1 from t where call_action='OF_UPDATE'))::integer::text)::bit(4)::integer
				   into lres;
				   return lres;
		end if;
	exception
		when no_data_found then
			return 0;
END;
$$ LANGUAGE plpgsql;


    ------------------------------------------------------------------------------------------------------------------------------------------------
    	-- jofl.api functions

    ------------------------------------------------------------------------------------------------------------------------------------------------

    create or replace function  jofl.jofl_pkg$of_rows(aid_account numeric,
    						out arows refcursor) returns refcursor as $$
    	begin
    		arows := jofl.jofl_pkg$of_rows(aid_account, null::text);
    	end;
    $$ LANGUAGE plpgsql;
    -----------------------------------------------------------------------------------------------------------------------------------------------
    create or replace function  jofl.jofl_pkg$of_rows(aid_account numeric
    						  ,attr text
    						  ,out arows refcursor)  returns refcursor as $$
    	declare
    		lv_proj jofl.window.schema_name%type := jofl.jofl_pkg$extract_varchar(attr,
    										 'F_SCHEMA_NAME',
    										 true);
    	begin
    		open arows for
    			select db_method	as	"DB_METHOD"
                       ,win_title	as	"WIN_TITLE"
                       ,custom_topcomponent	as	"CUSTOM_TOPCOMPONENT"
                       ,action_factory	as	"ACTION_FACTORY"
                       ,interface_pkg	as	"INTERFACE_PKG"
                       ,n_auth_based	as	"N_AUTH_BASED"
                       ,cache_min	as	"CACHE_MIN"
                       ,icon	as	"ICON"
                       ,refresh_interval_sec	as	"REFRESH_INTERVAL_SEC"
                       ,w.schema_name	as	"SCHEMA_NAME"
                       ,p.row$color	as	"ROW$COLOR"
                       ,w.editor_factory	as	"EDITOR_FACTORY"
                       ,w.custom_jscomponent	as	"CUSTOM_JSCOMPONENT"
    			from jofl.window     w
    				left join jofl.jf_project p
    					on w.schema_name = p.schema_name
    			where ((lv_proj is null) or
    			      (lv_proj is not null and w.schema_name = lv_proj))
    			order by w.schema_name
    				,w.db_method
    				,w.win_title;
    	end;
    $$ LANGUAGE plpgsql;


    -----------------------------------------------------------------------------------------------------------------------------------------------

    create or replace function  jofl.jofl_pkg$attr_to_rowtype(attr text) returns jofl.window as $$
    	declare
    		lr jofl.window%rowtype;
    	begin
    		lr.db_method := trim(jofl.jofl_pkg$extract_varchar(attr,'DB_METHOD', false));
    		lr.win_title := jofl.jofl_pkg$extract_varchar(attr,'WIN_TITLE', false);
    		lr.cache_min := jofl.jofl_pkg$extract_number(attr,'CACHE_MIN', false);
    		lr.refresh_interval_sec := jofl.jofl_pkg$extract_number(attr,'REFRESH_INTERVAL_SEC', false);

    		lr.n_auth_based := jofl.jofl_pkg$extract_boolean(attr,'N_AUTH_BASED', false);

    		-- NULLABLE параметры
    		BEGIN
    			lr.custom_topcomponent := jofl.jofl_pkg$extract_varchar(attr,'CUSTOM_TOPCOMPONENT', true);
    		exception  WHEN others THEN
    			lr.custom_topcomponent := null;
    		end;

    		lr.custom_jscomponent := jofl.jofl_pkg$extract_varchar(attr, 'CUSTOM_JSCOMPONENT', true);

    		begin
    			lr.action_factory := jofl.jofl_pkg$extract_varchar(attr, 'ACTION_FACTORY', true);
    		exception
    			when others then
    				lr.action_factory := null;
    		end;

    		lr.editor_factory := jofl.jofl_pkg$extract_varchar(attr, 'EDITOR_FACTORY', true);

    		begin
    			lr.interface_pkg := trim(both ' ' from jofl.jofl_pkg$extract_varchar(attr,'INTERFACE_PKG',true));
    		exception
    			when others then
    				lr.interface_pkg := null;
    		end;

    		begin
    			lr.icon := attr('ICON');
    		exception
    			when others then
    				lr.icon := null;
    		end;

    		begin
    			lr.schema_name := jofl.jofl_pkg$extract_varchar(attr,'SCHEMA_NAME',true);
    		exception
    			when others then
    				lr.schema_name := null;
    		end;

    		return lr;
    	end;
    $$ LANGUAGE plpgsql;

    -----------------------------------------------------------------------------------------------------------------------------------------------
    create or replace function  jofl.jofl_pkg$of_fill_field(aid_account numeric
        							 ,attr text) returns text as $$
        	declare
        		lr jofl.window%rowtype;
        		lv_source_table text := jofl.jofl_pkg$extract_varchar(attr, 'TARGET_TABLE_TEXT');
        	begin
        		lr := jofl.jofl_pkg$attr_to_rowtype(attr);


        		if exists (select 1 from jofl.win_field where db_method = lr.db_method)
        		then
        			perform jofl.jofl_gen_pkg$fill_fields(lr.db_method, lv_source_table);
        			return 'Поля успешно заполнены, нажмите "Обновить", далее можно воспользоваться меню "Сгенерировать пакет".';
        		else
        			RAISE EXCEPTION USING ERRCODE = 20001, MESSAGE = '<<Поля для выбранного окна уже заполнены!>>';
        		end if;

        	end;
   $$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function  jofl.jofl_pkg$gson2hashmap(gson text) returns table(attr text, attrname text) as $$
	begin 
		return query select value as attr , key as attrname from json_each_text(gson::json);
	end ;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------

 CREATE OR REPLACE FUNCTION jofl.jofl_pkg$extract_boolean(attr text,
														 attrname text,
														 allownull boolean DEFAULT false)
   RETURNS integer AS $$
 	declare
 		latrval text;
 	begin
 		select value::text into latrval from json_each_text(attr::json) where key = attrname;

 		if not allownull and latrval  is null then
 			RAISE  data_exception;
 		end if;
 		return latrval::boolean::integer;
 	end ;
 $$
   LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function  jofl.jofl_pkg$get_agile_configuration(aid_jofl_account jofl.account.id_jofl_account%type
								 ,adb_method       jofl.window.db_method%type
								 ,out atop         refcursor
								 ,out afield       refcursor
								 ,out adetail      refcursor
								 ,out aref         refcursor
								 ,out acall        refcursor
								 ,out afilter      refcursor
								 ,out afilter_opts refcursor
								 ,out acall_form   refcursor
								 ,target           text default jofl.constant_text('JOFL', 'jofl_pkg$target_platform_openide')
								 ,alocale          jofl.ref_locale.lc_locale%type default jofl.constant_text('JOFL'::text, 'jofl_pkg$default_locale'::text))
									returns record as $$
declare
		ln_modify_opts numeric;

		ln_auth jofl.window.n_auth_based%type;
	begin
	ln_modify_opts := jofl.jofl_pkg$get_modify_opts(aid_jofl_account, adb_method);

		--- если ничего недоступно (должно быть > 0) - то надо просто выпасть
		if ln_modify_opts = 0 then
			-- Если не требует авторизации, то можно дать конфигурацию
			select n_auth_based
			into ln_auth
			from jofl.window
			where db_method = adb_method;

			if ln_auth = 1	then
				return;
			else
				-- Для целей выдачи данных без авторизации
				-- Дадим права только на select
				--ln_modify_opts := bin_to_num(0, 0, 0, 1);
				ln_modify_opts := 1;
			end if;
		end if;

		open atop for
			select coalesce(nullif(v.lc_value,''), win_title) as	"WIN_TITLE"
                   ,custom_topcomponent	as	"CUSTOM_TOPCOMPONENT"
                   ,custom_jscomponent	as	"CUSTOM_JSCOMPONENT"
                   ,action_factory	as	"ACTION_FACTORY"
                   ,editor_factory	as	"EDITOR_FACTORY"
                   ,w.cache_min	as	"CACHE_MIN"
                   ,icon	as	"ICON"
                   ,ln_modify_opts as	"MODIFY_OPTS"
			from jofl.window w
				left join (select v.lc_value
						,v.db_method
					  from jofl.ref_vocabulary v
					  where v.lc_tag = '#title' and v.lc_locale = alocale) v
				on w.db_method = v.db_method
				where w.db_method = adb_method;

		open afield for
			select
				db_method					as 	"DB_METHOD"
				,field_name					as 	"FIELD_NAME"
				,field_cons_type			as 	"FIELD_CONS_TYPE"
				,field_type					as 	"FIELD_TYPE"
				,coalesce(nullif(v.lc_value,''), display_title) as "DISPLAY_TITLE"
				,coalesce(nullif(vd.lc_value,''), display_description)  as 	"DISPLAY_DESCRIPTION"
				,default_value				as 	"DEFAULT_VALUE"
				,n_order					as 	"N_ORDER"
				,n_visibility				as 	"N_VISIBILITY"
				,is_display_name				as 	"IS_DISPLAY_NAME"
				,f.is_mandatory				as 	"IS_MANDATORY"
				,f.pattern					as 	"PATTERN"
				,f.max_len					as 	"MAX_LEN"
				,f.weight					as 	"WEIGHT"
			from jofl.win_field f
				left join (select v.lc_value
						  ,v.lc_key
						from jofl.ref_vocabulary v
						where v.lc_tag = '#field' and v.lc_locale = alocale and
									v.db_method = adb_method) v
					on f.field_name = v.lc_key
				left join (select v.lc_value
						  ,v.lc_key
						from jofl.ref_vocabulary v
						where v.lc_tag = '#field.description' and v.lc_locale = alocale and
									v.db_method = adb_method) vd
					on f.field_name = vd.lc_key
			where f.db_method = adb_method
			order by f.n_order;

		-- #detail +
		open adetail for
			select coalesce(nullif(v.lc_value,''), d.action_title) as "ACTION_TITLE"
				   ,d.detail_db_method	AS	    	"DETAIL_DB_METHOD"
				   ,w.icon	AS	    	"ICON"
				   ,d.is_row_depend	AS	"IS_ROW_DEPEND"
			from jofl.win_action_detail d
				join jofl.window w
					on  d.detail_db_method = w.db_method
				left join (select v.lc_value
						,v.lc_key
					   from jofl.ref_vocabulary v
						where v.lc_tag = '#detail' and v.lc_locale = alocale and
								v.db_method = adb_method) v
					on d.detail_db_method = v.lc_key
			where d.db_method = adb_method and jofl.jofl_pkg$get_modify_opts(aid_jofl_account, d.detail_db_method) > 0
			order by d.n_sort_order nulls last;

		open aref for
			select field_name	AS	"FIELD_NAME"
                   ,ref_db_method	AS	"REF_DB_METHOD"
                   ,r.is_custom_factory	AS	"IS_CUSTOM_FACTORY"
                   ,r.ref_factory	AS	"REF_FACTORY"
                   ,ref_field	AS	"REF_FIELD"
                   ,dst_field	AS	"DST_FIELD"
			from jofl.win_field_ref r
			where r.db_method = adb_method
			order by field_name;


		-- #action +
		if jofl.jofl_pkg$isrootrole(aid_jofl_account)
		then
			open acall for
				select
					call_action	AS	"CALL_ACTION"
					,coalesce(nullif(v.lc_value,''), action_title) "ACTION_TITLE"
					,is_refresh	AS	"IS_REFRESH"
					,is_win_action	AS	"IS_WIN_ACTION"
					,is_row_depend	AS	"IS_ROW_DEPEND"
					,call_default	AS	"CALL_DEFAULT"
					,case
						 when call_default is null then
							0
						 else
							1
					 end  "IS_CALL_DEFAULT"
					,is_internal	AS	"IS_INTERNAL"
				from jofl.win_action_call c
					left join (select v.lc_value
						   	 ,v.lc_key
						    from jofl.ref_vocabulary v
						    where v.lc_tag = '#action' and v.lc_locale = alocale and
										v.db_method = adb_method) v
						on  c.call_action = v.lc_key
				where db_method = adb_method and
							(target = 'JAVASCRIPT' or is_internal = 0)
				order by c.n_sort_order nulls first;
		else
			open acall for
				select c.call_action	AS	"CALL_ACTION"
                       ,coalesce(nullif(v.lc_value,''), action_title) as "ACTION_TITLE"
                       ,c.is_refresh		as "IS_REFRESH"
                       ,c.is_win_action		as "IS_WIN_ACTION"
                       ,c.is_row_depend		as "IS_ROW_DEPEND"
                       ,case
                       	 when c.call_default is null then
                       		0
                       	 else
                       		1
                        end "IS_CALL_DEFAULT"
                       ,c.is_internal	as "IS_INTERNAL"
				from jofl.win_action_call c
					join (select distinct r2c.call_action
						from jofl.role2call    r2c
							,jofl.role2account r2a
						where r2c.db_method = adb_method and
							r2c.id_role = r2a.id_role and
							r2a.id_jofl_account = aid_jofl_account) g
						on c.call_action = g.call_action
					left join (select v.lc_value
							,v.lc_key
						from jofl.ref_vocabulary v
						where v.lc_tag = '#action' and v.lc_locale = alocale and
									v.db_method = adb_method) v
						on c.call_action = v.lc_key
				where (target = 'JAVASCRIPT' or is_internal = 0)
				 and c.db_method = adb_method
				order by c.n_sort_order nulls first;
		end if;


		-- #filter +
		open afilter for
			select fs.source_class as "FILTER_CLASS"
                   ,initial_param	as	"INITIAL_PARAM"
                   ,grid_x	as	"GRID_X"
                   ,grid_y	as	"GRID_Y"
                   ,coalesce(nullif(v.lc_value,''), filter_title) as	"FILTER_TITLE"
			from jofl.win_filter f
				join jofl.ref_filter r
					on f.filter_class = r.filter_class
				join jofl.ref_filter_source fs
					on fs.filter_class = r.filter_class
				left join (select v.lc_value
					   	 ,v.lc_key
					   from jofl.ref_vocabulary v
					   where v.lc_tag = '#filter' and v.lc_locale = alocale and
							v.db_method = adb_method) v
					on f.id_filter = v.lc_key
			where f.db_method = adb_method
						 and fs.id_platform = target;


		-- #filter.apply +
		-- #filter.reset +
		open afilter_opts for
			select coalesce(nullif(jofl.localization_pkg$translate(adb_method,
								   alocale,
								   '#filter.apply'),''),
								 o.but_apply_caption) as "BUT_APPLY_CAPTION"
			      ,coalesce(nullif(jofl.localization_pkg$translate(adb_method,
							      alocale,
							      '#filter.reset'),''),
			       o.but_reset_caption) as "BUT_RESET_CAPTION"
			      ,o.is_reset_present as "IS_RESET_PRESENT"
			from jofl.win_filter_opts o
			where o.db_method = adb_method;

		-- #action.field +
		open acall_form for
			select f.call_action as "CALL_ACTION"
				,fs.source_class as "FILTER_CLASS"
				,f.initial_param as "INITIAL_PARAM"
				,coalesce(nullif(v.lc_key,''), f.field_title) as "FIELD_TITLE"
			from jofl.win_action_field f
				join jofl.ref_filter_source fs
					on fs.filter_class = f.filter_class 
				join jofl.ref_filter fl
					on f.filter_class = fl.filter_class
					
				left join (select v.lc_value
					   	 ,v.lc_key
					   from jofl.ref_vocabulary v
						where v.lc_tag = '#action.field' and v.lc_locale = alocale and
									v.db_method = adb_method) v
					on f.id_afield = v.lc_key
			where f.db_method = adb_method
						and fs.id_platform = target
			order by f.n_sort_order nulls last
				,f.call_action;
	
	end;
$$ LANGUAGE plpgsql;
		

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function  jofl.jofl_pkg$isrootrole(aid_jofl_account jofl.account.id_jofl_account%type) returns boolean as $$
	with t as (select true res
	from jofl.role2account r2a
	where r2a.id_role = 'ROOT' and r2a.id_jofl_account = aid_jofl_account)
	select res from t
	union all
	select false
	where not exists (select 1 from t);
$$ LANGUAGE SQL STRICT IMMUTABLE;

-----------------------------------------------------------------------------------------------------------------------------------------------

create or replace function  jofl.jofl_pkg$attr_to_rowtype(attr text) returns jofl.window as $$
	declare
		lr jofl.window%rowtype;
	begin		
		lr.db_method := trim(jofl.jofl_pkg$extract_varchar(attr,'DB_METHOD', false));
		lr.win_title := jofl.jofl_pkg$extract_varchar(attr,'WIN_TITLE', true);
		lr.cache_min := jofl.jofl_pkg$extract_number(attr,'CACHE_MIN', true);
		lr.refresh_interval_sec := jofl.jofl_pkg$extract_number(attr,'REFRESH_INTERVAL_SEC', true);
	
		lr.n_auth_based := jofl.jofl_pkg$extract_boolean(attr,'N_AUTH_BASED', true);
	
		-- NULLABLE параметры 
		BEGIN
			lr.custom_topcomponent := jofl.jofl_pkg$extract_varchar(attr,'CUSTOM_TOPCOMPONENT', true);
		exception  WHEN others THEN
			lr.custom_topcomponent := null;
		end;
	
		lr.custom_jscomponent := jofl.jofl_pkg$extract_varchar(attr, 'CUSTOM_JSCOMPONENT', true);

		begin
			lr.action_factory := jofl.jofl_pkg$extract_varchar(attr, 'ACTION_FACTORY', true);
		exception
			when others then
				lr.action_factory := null;
		end;
	
		lr.editor_factory := jofl.jofl_pkg$extract_varchar(attr, 'EDITOR_FACTORY', true);
	
		begin
			lr.interface_pkg := trim(both ' ' from jofl.jofl_pkg$extract_varchar(attr,'INTERFACE_PKG',true));
		exception
			when others then
				lr.interface_pkg := null;
		end;
	
		begin
			lr.icon := attr('ICON');
		exception
			when others then
				lr.icon := null;
		end;
	
		begin
			lr.schema_name := jofl.jofl_pkg$extract_varchar(attr,'SCHEMA_NAME',true);
		exception
			when others then
				lr.schema_name := null;
		end;
	
		return lr;
	end;
$$ LANGUAGE plpgsql;	

-----------------------------------------------------------------------------------------------------------------------------------------------									 

create or replace function  jofl.jofl_pkg$of_insert(aid_account numeric
						   ,attr  text) returns text as $$
	declare
		lr jofl.window%rowtype;
	begin
		
		
		lr := jofl.jofl_pkg$attr_to_rowtype(attr);
		insert into jofl.window(db_method,
					win_title,
					custom_topcomponent,
					custom_jscomponent,
					action_factory,
					editor_factory,
					interface_pkg,
					n_auth_based,
					cache_min,
					icon,
					refresh_interval_sec,
					schema_name) 
				values (lr.db_method,
					lr.win_title,
					lr.custom_topcomponent,
					lr.custom_jscomponent,
					lr.action_factory,
					lr.editor_factory,
					lr.interface_pkg,
					lr.n_auth_based,
					lr.cache_min,
					lr.icon,
					lr.refresh_interval_sec,
					lr.schema_name);
	
		insert into jofl.win_action_call
			(db_method
			,call_action
			,action_title
			,is_refresh
			,is_internal)
		values
			(lr.db_method
			,'OF_INSERT'
			,'<<INSERT>>'
			,0
			,1);
	
		insert into jofl.win_action_call
			(db_method
			,call_action
			,action_title
			,is_refresh
			,is_internal)
		values
			(lr.db_method
			,'OF_UPDATE'
			,'<<UPDATE>>'
			,0
			,1);
	
		insert into jofl.win_action_call
			(db_method
			,call_action
			,action_title
			,is_refresh
			,is_internal)
		values
			(lr.db_method
			,'OF_DELETE'
			,'<<DELETE>>'
			,0
			,1);
	
		insert into jofl.win_action_call
			(db_method
			,call_action
			,action_title
			,is_refresh
			,is_internal)
		values
			(lr.db_method
			,'OF_ROWS'
			,'<<SELECT>>'
			,0
			,1);
	
		return 'Теперь можно воспользоваться меню "Заполнить поля" (Это удобно)';	
	end;
$$ LANGUAGE plpgsql;
-----------------------------------------------------------------------------------------------------------------------------------------------										 
create or replace function  jofl.jofl_pkg$of_update(aid_account numeric
						   ,attr text) returns text as $$
	declare 
		lr jofl.window%rowtype;
	begin
		lr := jofl.jofl_pkg$attr_to_rowtype(attr);
		update jofl.window w set (db_method	,
			win_title	,
			custom_topcomponent	,
			custom_jscomponent	,
			action_factory	,
			editor_factory	,
			interface_pkg	,
			n_auth_based	,
			cache_min	,
			icon	,
			refresh_interval_sec	,
			schema_name	) = (select lr.*) where w.db_method = lr.db_method;
		return null;
	end;
$$ LANGUAGE plpgsql;
-----------------------------------------------------------------------------------------------------------------------------------------------
create or replace function  jofl.jofl_pkg$of_delete(aid_account numeric
			   ,attr text) returns text as $$
	declare
		lr jofl.window%rowtype;
	begin
		lr := jofl.jofl_pkg$attr_to_rowtype(attr);

    delete from jofl.win_field_ref  where db_method = lr.db_method;
    delete from jofl.win_field where db_method = lr.db_method;
    delete from jofl.app_menu  where db_method = lr.db_method;
    delete from jofl.win_action_call  where db_method = lr.db_method;
    delete from jofl.role2call   where db_method = lr.db_method;
    delete from jofl.win_filter  where db_method = lr.db_method;
    delete from jofl.WIN_FILTER_OPTS where db_method = lr.db_method;
    delete from jofl.win_action_detail where db_method = lr.db_method;
    delete from jofl.win_action_field where db_method = lr.db_method;
    delete from jofl.window where db_method = lr.db_method;

		delete from jofl.window w where w.db_method = lr.db_method;
		return null::text;
	end;
$$ LANGUAGE plpgsql;
-----------------------------------------------------------------------------------------------------------------------------------------------										 
create or replace function  jofl.jofl_pkg$of_generate_package(aid_account numeric
				     ,attr text) returns text as $$
	declare 
		lr jofl.window%rowtype;
		lv_pack char(32) := upper(jofl.jofl_pkg$extract_varchar(attr,'TARGET_PACK_TEXT',false));
		lv_obj  char(32) := upper(jofl.jofl_pkg$extract_varchar(attr,'TARGET_OBJ_TEXT',false));
	begin
		lr := jofl.jofl_pkg$attr_to_rowtype(attr);
		lr.interface_pkg := jofl.jofl_gen_pkg$generate_package(lr.db_method,
								       lv_pack,
								       lv_obj);
		update jofl.window
		set interface_pkg = lr.interface_pkg
		where db_method = lr.db_method;
		return 'Создан пакет: ' || lr.interface_pkg;
	end;
$$ LANGUAGE plpgsql;