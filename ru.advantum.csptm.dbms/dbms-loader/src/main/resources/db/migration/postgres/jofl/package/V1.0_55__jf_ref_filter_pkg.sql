create or replace function jofl.jf_ref_filter_pkg$attr_to_rowtype(attr text) returns table(id text
						,scn numeric
						,filter_class text
						,init_template text) as $$
	begin		
		return query 
			select null::text as id, 
				  null::numeric as scn,
				  jofl.jofl_pkg$extract_varchar(attr,'FILTER_CLASS', true) as filter_class,
				  jofl.jofl_pkg$extract_varchar(attr,'INIT_TEMPLATE', true) as init_template;		
	end;
$$ LANGUAGE plpgsql;

	
------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_ref_filter_pkg$of_rows(aid_account numeric							      
							      ,out arows refcursor
							      ,attr text default null) returns refcursor as $$
	begin
		open arows for
				select filter_class as "FILTER_CLASS"
			      ,init_template AS "INIT_TEMPLATE"
			from jofl.ref_filter src;
	end;
$$ LANGUAGE plpgsql;
