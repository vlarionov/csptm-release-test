create or replace function jofl.install_login(ausername text
					,akey      text
					,ahash     text
					,aappcode  numeric
					,aappver   numeric)
returns table (ausermesg text,
	      anum numeric) as $$
begin
	return query 
		select 
			'Please change this function on install!'::text as ausermesg,
			1::numeric as anum; -- root account!
	end ;
$$ LANGUAGE plpgsql;