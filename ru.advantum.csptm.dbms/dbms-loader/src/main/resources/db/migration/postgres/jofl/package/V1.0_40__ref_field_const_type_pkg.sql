create or replace function  jofl.ref_field_const_type_pkg$of_rows(aid_account numeric
																 ,out arows refcursor
																 ,attr text default '{}') returns refcursor as $$
	begin
		open arows for
			select field_cons_type AS "FIELD_CONS_TYPE"
				,field_const_name AS "FIELD_CONST_NAME"
			from jofl.ref_const_type
			order by field_cons_type;
	end ;
$$ LANGUAGE plpgsql;