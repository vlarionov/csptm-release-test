create or replace function jofl.jf_app_menu_pkg$of_rows(aid_jofl_account in jofl.account.id_jofl_account%type,
														out arows refcursor,
														attr text default null )  RETURNS refcursor AS $$
	BEGIN
    OPEN arows FOR
	select src.id_menu	as	"ID_MENU"
		  ,src.menu_title	as	"MENU_TITLE"
		  ,src.id_menu_parent	as	"ID_MENU_PARENT"
		  ,src.db_method	as	"DB_METHOD"
		  ,parent.menu_title as	"PARENT_TITLE"
		  ,src.icon	as	"ICON"
		  ,src.n_order	as	"N_ORDER"
        from jofl.app_menu src
	    left join jofl.app_menu parent
        on src.id_menu_parent = parent.id_menu;
	END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_app_menu_pkg$get_app_menu (aid_jofl_account in jofl.account.id_jofl_account%type,
								out arows refcursor,
								alocale jofl.ref_locale.lc_locale%type default null,
								aproj jofl.app_menu.schema_name%type default null)
								RETURNS refcursor AS $$
	begin
		open arows for
		WITH RECURSIVE r  as
        	(select id_menu, id_menu_parent, menu_title,icon, n_order	,db_method, 1 AS level
                    from wins where id_menu_parent = 0
                union all
                    select wins.id_menu, wins.id_menu_parent, wins.menu_title, wins.icon, wins.n_order	,wins.db_method, r.level+1 AS level
                    from wins
                    join r
                    on r.id_menu=wins.id_menu_parent),
        wins as (select distinct id_menu, coalesce (id_menu_parent,0) id_menu_parent, menu_title,icon, n_order,m.db_method
        					from jofl.role2account r2a
        						join jofl.role2call    r2c
        						on r2c.id_role = r2a.id_role
        						left join jofl.app_menu     m
        						on (m.db_method = r2c.db_method or  m.db_method  is null)
        					where r2a.id_jofl_account = aid_jofl_account
        						and r2c.call_action = 'OF_ROWS'
        						and (select substr(r2a.id_role, 0 ,position('.' in r2a.id_role))) = m.schema_name

        					union
        					select null id_menu, null id_menu_parent,  null menu_title,  null icon, null n_order, db_method
        					from jofl.window
        					where jofl.jofl_pkg$isrootrole(aid_jofl_account)::integer = 1)
        select w.id_menu AS "ID_MENU"
        			 ,w.menu_title AS "MENU_TITLE"
        			 ,w.db_method AS "DB_METHOD"
        			 ,w.id_menu_parent AS "ID_MENU_PARENT"
        			 ,w.icon AS "ICON"
        			 ,w.n_order AS "N_ORDER"
        			 from r w
		order by w.n_order nulls last;
 END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_app_menu_pkg$attr_to_rowtype(attr text)
		returns jofl.app_menu as $$
	declare
		lr jofl.app_menu%rowtype;
	begin
		lr.id_menu := jofl.jofl_pkg$extract_number(attr,'ID_MENU',true);
		lr.menu_title := jofl.jofl_pkg$extract_varchar(attr, 'MENU_TITLE', true);
		lr.id_menu_parent := jofl.jofl_pkg$extract_varchar(attr,
							      'ID_MENU_PARENT',
							      true);

		lr.n_order := jofl.jofl_pkg$extract_number(attr,
						      'N_ORDER',
						      true);
		lr.icon := jofl.jofl_pkg$extract_varchar(attr,
						    'ICON',
						    true);

		lr.db_method := jofl.jofl_pkg$extract_varchar(attr,
							 'DB_METHOD',
							 true);

		lr.schema_name := jofl.jofl_pkg$extract_varchar(attr,
							   'SCHEMA_NAME',
							   true);
		return lr;
	end;
$$ LANGUAGE plpgsql;


------------------------------------------------------------------------------------------------------------------------------------------------

create or replace function jofl.jf_app_menu_pkg$of_insert(aid_account numeric
							  ,attr text) returns text as $$
	declare
		lr    jofl.app_menu%rowtype;
	begin
		lr := attr_to_rowtype(attr);
		insert into jofl.app_menu 
			(id_menu,
			menu_title,
			id_menu_parent,
			db_method,
			icon,
			n_order,
			schema_name)
		values (nextval('jofl.sq_menu'),
			lr.menu_title,
			lr.id_menu_parent,
			lr.db_method,
			lr.icon,
			lr.n_order,
			lr.schema_name);	
		return null;
	end;
$$ LANGUAGE plpgsql;										 
------------------------------------------------------------------------------------------------------------------------------------------------										 
create or replace function jofl.jf_app_menu_pkg$of_update(aid_account numeric
							 ,attr  text) returns text as $$
	declare
		lr    jofl.app_menu%rowtype;
	begin
		lr := attr_to_rowtype(attr);
		update jofl.app_menu f set 
			id_menu	= lr.id_menu,
			menu_title = lr.menu_title,
			id_menu_parent	= lr.id_menu_parent,
			db_method = lr.db_method,
			icon = lr.icon,
			n_order	= lr.n_order,
			schema_name = lr.schema_name 
		where f.id_menu = lr.id_menu;
		return null;
	end;
$$ LANGUAGE plpgsql;			

------------------------------------------------------------------------------------------------------------------------------------------------										 
create or replace function jofl.jf_app_menu_pkg$of_delete(aid_account numeric
							 ,attr text) returns text as $$
	declare
		lr    jofl.app_menu%rowtype;
	begin
		lr := attr_to_rowtype(attr);
		delete from jofl.app_menu f where f.id_menu = lr.id_menu;
		return null;
	end;
$$ LANGUAGE plpgsql;			
