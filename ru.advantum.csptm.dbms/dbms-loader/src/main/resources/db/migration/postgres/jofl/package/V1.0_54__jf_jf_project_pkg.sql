
create or replace function jofl.jf_jf_project_pkg$attr_to_rowtype(attr text)
		returns jofl.jf_project as $$
	declare
		lr jofl.jf_project%rowtype;
	begin
		lr.schema_name   := jofl.jofl_pkg$extract_varchar(attr,'SCHEMA_NAME',false);
		lr.project_title := jofl.jofl_pkg$extract_varchar(attr,'PROJECT_TITLE',false);
		lr.row$color := jofl.jofl_pkg$extract_number(attr,
							    'ROW$COLOR',
							    true);
		return lr;
	end;
$$ LANGUAGE plpgsql;		

------------------------------------------------------------------------------------------------------------------------------------------------
 create or replace function jofl.jf_jf_project_pkg$of_rows(aid_account numeric
 							  ,out arows refcursor
							  ,attr  text) returns refcursor as $$
	begin
		open arows for
			select schema_name	as	"SCHEMA_NAME"
					,project_title	as	"PROJECT_TITLE"
					,row$color	as	"ROW$COLOR"
			from jofl.jf_project src;
	end;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_jf_project_pkg$of_insert(aid_account numeric, 
							    attr text) returns text as $$
	declare
		lr jofl.jf_project%rowtype;
	begin
		lr := attr_to_rowtype(attr);

		insert into jofl.jf_project (schema_name,
						project_title,
						row$color)
		values (lr.schema_name,
			lr.project_title,
			lr.row$color);
		
	return null;
	end;
$$ LANGUAGE plpgsql;
	
------------------------------------------------------------------------------------------------------------------------------------------------										 
create or replace function jofl.jf_jf_project_pkg$of_update(aid_account numeric, 
							    attr text) returns text as $$
	declare
		lr jofl.jf_project%rowtype;
	begin
		lr := attr_to_rowtype(attr);

		update jofl.jf_project
		set schema_name	= lr.schema_name,
		    project_title = lr.project_title,
		    row$color =	lr.row$color
		where schema_name = lr.schema_name;
	return null;
	end;
$$ LANGUAGE plpgsql;
	
------------------------------------------------------------------------------------------------------------------------------------------------										 
create or replace function jofl.jf_jf_project_pkg$of_delete(aid_account numeric, 
							    attr text) returns text as $$
	declare
		lr jofl.jf_project%rowtype;
	begin
		lr := attr_to_rowtype(attr);
		delete from jofl.jf_project where schema_name = lr.schema_name;
	return null;
	end;
$$ LANGUAGE plpgsql;
	
			