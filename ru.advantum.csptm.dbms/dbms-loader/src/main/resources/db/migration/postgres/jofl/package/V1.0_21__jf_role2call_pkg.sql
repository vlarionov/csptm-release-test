create or replace function jofl.jf_role2call_pkg$attr_to_rowtype(attr text)
		returns jofl.role2call as $$
	declare
		lr jofl.role2call%rowtype;
	begin
		lr.id_role     := jofl.jofl_pkg$extract_varchar(attr,'ID_ROLE',false);
		lr.db_method   := jofl.jofl_pkg$extract_varchar(attr,'DB_METHOD', false);
		lr.call_action :=jofl.jofl_pkg$extract_varchar(attr,'CALL_ACTION', false);	
		return lr;
	end;
$$ LANGUAGE plpgsql;	

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_role2call_pkg$of_insert(aid_account numeric, attr text) returns text as $$	
	begin
		insert into jofl.role2call
			select * from jofl.jf_role2call_pkg$attr_to_rowtype(attr);
	return null;
	end;
$$ LANGUAGE plpgsql;	

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_role2call_pkg$of_update(aid_account numeric, attr text) returns text as $$
	declare
		lr jofl.role2call%rowtype;
	begin
		lr := jofl.jf_role2call_pkg$attr_to_rowtype(attr);
		update jofl.role2call
		set id_role = lr.id_role,
		    db_method = lr.db_method,
		    call_action	= lr.call_action
		where id_role = lr.id_role
					and db_method = lr.db_method
					and call_action = lr.call_action;
	return null;
	end;
$$ LANGUAGE plpgsql;	

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_role2call_pkg$of_delete(aid_account numeric, attr text) returns text as $$
	declare
		lr jofl.role2call%rowtype;
	begin
		lr := jofl.jf_role2call_pkg$attr_to_rowtype(attr);
		delete from jofl.role2call
		where f.id_role = lr.id_role
			and db_method = lr.db_method
			and call_action = lr.call_action;
	return null;
	end;
$$ LANGUAGE plpgsql;	

------------------------------------------------------------------------------------------------------------------------------------------------

create or replace function jofl.jf_role2call_pkg$of_add_all(aid_account numeric, attr text) returns text as $$
	declare
		lr jofl.role2call%rowtype;
		rowcount numeric;
	begin
		lr := jofl.jf_role2call_pkg$attr_to_rowtype(attr);
		with rs as (select lr.id_role
				   ,wa.db_method
				   ,wa.call_action
			from jofl.win_action_call wa
			where wa.db_method = lr.db_method
				and wa.call_action not in (lr.call_action)),
		      cnt as (select count(1) as cn
					from jofl.win_action_call wa
					where wa.db_method = lr.db_method
				and wa.call_action not in (lr.call_action)),
		      ins as (insert into jofl.role2call (id_role
						   ,db_method
						   ,call_action)
					select id_role
					       ,db_method
					        ,call_action from rs
				returning *)
		     select cnt.cn into rowcount from inst
			full outer join  cnt
			on 1=1;
		return 'Добавлено ' || rowcount || ' методов!';
	return null;
	end;
$$ LANGUAGE plpgsql;	


------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_role2call_pkg$of_rows(aid_account numeric
						    ,out arows refcursor
							,attr  text) returns refcursor as $$
	declare
		lv_role jofl.ref_role.id_role%type := jofl.jofl_pkg$extract_varchar(attr,'ID_ROLE',false);
		lrows refcursor;
	begin
		open arows for
			select id_role AS "ID_ROLE"
				,db_method AS "DB_METHOD"
				,call_action AS "CALL_ACTION"
			from jofl.role2call src
			where src.id_role = lv_role
			order by db_method
				,call_action;
	end;
$$ LANGUAGE plpgsql;
