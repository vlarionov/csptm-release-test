create or replace function jofl.jf_win_filter_pkg$attr_to_rowtype(attr text) 
		returns table(  scn numeric,
				db_method	jofl.win_filter.	db_method	%type,
				filter_class	jofl.win_filter.	filter_class	%type,
				initial_param	jofl.win_filter.	initial_param	%type,
				grid_x	jofl.win_filter.	grid_x	%type,
				grid_y	jofl.win_filter.	grid_y	%type,
				filter_title	jofl.win_filter.	filter_title	%type,
				id_filter	jofl.win_filter.	id_filter	%type,
				rowid	jofl.win_filter.	rowid	%type) as $$
	begin
		return query 
			select null::numeric as scn,
			       jofl.jofl_pkg$extract_varchar(attr,'DB_METHOD', false)	as	db_method	,
			       jofl.jofl_pkg$extract_varchar(attr, 'FILTER_CLASS', false) as	filter_class	, 
			       jofl.jofl_pkg$extract_varchar(attr,  'INITIAL_PARAM', true) as	initial_param	,
			       jofl.jofl_pkg$extract_number(attr, 'GRID_X', false) as	grid_x	,
			       jofl.jofl_pkg$extract_number(attr, 'GRID_Y', false) as	grid_y	,
			       jofl.jofl_pkg$extract_varchar(attr,'FILTER_TITLE' ,true) as	filter_title	, 
			       jofl.jofl_pkg$extract_varchar(attr,'ID_FILTER', false) as	id_filter	,
			       jofl.jofl_pkg$extract_varchar(attr, 'ROWID', true) as	rowid;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_win_filter_pkg$of_insert(aid_account numeric, 
							    attr  text) returns text as $$
	begin
		insert into jofl.win_filter(    db_method	,
						filter_class	,
						initial_param	,
						grid_x	,
						grid_y	,
						filter_title	,
						id_filter) 
		select db_method	,
			filter_class	,
			initial_param	,
			grid_x	,
			grid_y	,			
			filter_title	,
			case when (lr.id_filter is null or substring (lr.id_filter from 1 for 1) = '-') then
				upper(replace(uuid_generate_v4()::text,'-',''))
				else lr.id_filter end id_filter
		from jofl.jf_win_filter_pkg$attr_to_rowtype(attr) lr;
		return null;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_win_filter_pkg$of_update(aid_account numeric, 
							    attr  text) returns text as $$
	declare
		lr record;
	begin
		lr := jofl.jf_win_filter_pkg$attr_to_rowtype(attr);
		update jofl.win_filter set
			db_method	=	lr.	db_method,
			filter_class	=	lr.	filter_class,
			initial_param	=	lr.	initial_param,
			grid_x	=	lr.	grid_x,
			grid_y	=	lr.	grid_y,
			filter_title	=	lr.	filter_title,
			id_filter	=	lr.	id_filter
		where rowid = lr.rowid;
		return null;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_win_filter_pkg$of_delete(aid_account numeric, 
							    attr  text) returns text as $$
	declare
		lr record;
	begin
		lr := jofl.jf_win_filter_pkg$attr_to_rowtype(attr);
		delete from jofl.win_filter where rowid = lr.rowid;
		return null;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_win_filter_pkg$of_rows(aid_account numeric
														 ,out arows refcursor
														 ,attr text) returns refcursor as $$
	declare
		lv_dbmethod jofl.win_field.db_method%type := jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', false);
	begin
		open arows for
			select rowid	AS	ROWID
                   				,id_filter	AS	"ID_FILTER"
                   				,db_method	AS	"DB_METHOD"
                   				,filter_class	AS	"FILTER_CLASS"
                   				,initial_param	AS	"INITIAL_PARAM"
                   				,grid_x	AS	"GRID_X"
                   				,grid_y	AS	"GRID_Y"
                   				,filter_title	AS	"FILTER_TITLE"
			from jofl.win_filter src
			where src.db_method = lv_dbmethod;
	end;
$$ LANGUAGE plpgsql;