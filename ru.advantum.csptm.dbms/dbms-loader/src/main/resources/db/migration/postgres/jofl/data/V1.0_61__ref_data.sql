--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = jofl;

--
-- Data for Name: constants_num; Type: TABLE DATA; Schema: jofl; Owner: postgres
--

COPY constants_num (schema_name, c_name, c_val, ref_type) FROM stdin;
\.


--
-- Data for Name: constants_text; Type: TABLE DATA; Schema: jofl; Owner: postgres
--

COPY constants_text (schema_name, c_name, c_val, ref_type) FROM stdin;
\.


--
-- Data for Name: create$java$lob$table; Type: TABLE DATA; Schema: jofl; Owner: postgres
--

--
-- Data for Name: ref_const_type; Type: TABLE DATA; Schema: jofl; Owner: postgres
--

COPY ref_const_type (field_cons_type, field_const_name) FROM stdin;
0	(PK) Первичный ключ
1	(PK) Редактируемый Первичный ключ
2	(READWRITE) Чтение/Запись
3	(READONLY) Только для чтения
\.


--
-- Data for Name: ref_editor; Type: TABLE DATA; Schema: jofl; Owner: postgres
--

COPY ref_editor (ref_factory) FROM stdin;
\.


--
-- Data for Name: ref_field_type; Type: TABLE DATA; Schema: jofl; Owner: postgres
--

COPY ref_field_type (field_type, field_type_name) FROM stdin;
L	Линк (hyperlink)
R	Файл
N	Целое
V	Строка
F	Действительное
B	Boolean
D	Дата
T	Время
I	Интервал
\.


--
-- Data for Name: ref_field_visibility; Type: TABLE DATA; Schema: jofl; Owner: postgres
--

COPY ref_field_visibility (n_visibility, visibility_name) FROM stdin;
0	Нет
1	Только в Properties
2	Только в Outline
3	Везде
\.


--
-- Data for Name: ref_filter; Type: TABLE DATA; Schema: jofl; Owner: postgres
--

COPY ref_filter (filter_class, init_template) FROM stdin;
NODE	{"TITLE":"<название>", "PREFIX":"F_", "FACTORY":"<метод>", "FIELD":"<поле>", "WIDTH":"300"}
TEXT	{"TITLE":"<название>", "PREFIX": "STR_FIELD_", "MANDATORY": "true", "WIDTH": "400" }
NUMBER	{"TITLE":"<название>", "PREFIX": "F_", "DEFAULT_VALUE": "1", "MANDATORY":"true", "MIN":"0", "MAX":"100", "STEP":"1", "FRACTION":"1"} 
DATE	{"TITLE":"<название>", "PREFIX": "END_", "MANDATORY": "true",  "DEFAULT_DATE": "END_OF_MONTH"}
TIME	{"TITLE":"<название>", "PREFIX": "END_", "MANDATORY": "true",  "DEFAULT_DATE": "END_OF_MONTH"}
TREE	{"TITLE":"<название>", "PREFIX":"F_", "FACTORY":"<метод>", "FIELD":"<поле>", "ID_FIELD":"<поле>", "ID_PARENT_FIELD":"<поле>", "WIDTH":"300"}
TIME_ONLY	{"TITLE":"<название>", "PREFIX": "END_", "MANDATORY": "true",  "DEFAULT_TIME": "END_OF_DATE"}
INPLACE	{"TITLE":"<название>", "PREFIX": "F_", "MANDATORY": "true",  "INPLACE": [ {"K": "KEY", "V":"VALUE"}  ]}
\.


--
-- Data for Name: ref_ui_platform; Type: TABLE DATA; Schema: jofl; Owner: postgres
--

COPY ref_ui_platform (id_platform, plat_name, rowid) FROM stdin;
OPENIDE	Приложение Netbeans Platform	3088
JAVASCRIPT	Веб приложение	3089
\.


--
-- Data for Name: ref_filter_source; Type: TABLE DATA; Schema: jofl; Owner: postgres
--

COPY ref_filter_source (id_platform, filter_class, source_class) FROM stdin;
OPENIDE	NODE	ru.advantum.agilebeans.service.filter.NodeFilter
OPENIDE	TEXT	ru.advantum.agilebeans.service.filter.TextFilter
OPENIDE	NUMBER	ru.advantum.agilebeans.service.filter.NumberFilter
OPENIDE	DATE	ru.advantum.agilebeans.service.filter.DateFilter
OPENIDE	TIME	ru.advantum.agilebeans.service.filter.TimeFilter
JAVASCRIPT	TREE	jsTree
JAVASCRIPT	TIME	jsTime
JAVASCRIPT	DATE	jsDate
JAVASCRIPT	NUMBER	jsNumber
JAVASCRIPT	TEXT	jsText
JAVASCRIPT	NODE	jsNode
JAVASCRIPT	TIME_ONLY	jsTimeOnly
JAVASCRIPT	INPLACE	jsInplace
OPENIDE	INPLACE	ru.advantum.agilebeans.service.filter.InplaceKVFilter
\.


--
-- Data for Name: ref_locale; Type: TABLE DATA; Schema: jofl; Owner: postgres
--

COPY ref_locale (lc_locale, rowid) FROM stdin;
en_EN	1188
RUR  	3094
\.


--
-- Data for Name: ref_locale_tag; Type: TABLE DATA; Schema: jofl; Owner: postgres
--

COPY ref_locale_tag (lc_tag, rowid, tag_name) FROM stdin;
#bound	1284	Текст
#title	1285	Заголовок окна
#field	1286	Поле
#detail	1287	Деталь
#action	1288	Метод
#action.field	1289	Поле формы метода
#filter.apply	1290	Фильтр: применить
#filter.reset	1291	Фильтр: сбросить
#filter	1292	Фильтр
#field.description	1293	Описание поля
\.