create or replace function jofl.constant_text(text, text)  returns text AS $$
  select c_val from jofl.constants_text where schema_name=$1 and c_name=$2;
$$ LANGUAGE SQL STRICT IMMUTABLE;