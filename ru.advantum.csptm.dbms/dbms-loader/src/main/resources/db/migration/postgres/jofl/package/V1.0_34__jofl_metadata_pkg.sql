create or replace function  jofl.jofl_metadata_pkg$get_data_description(aid_jofl_account jofl.account.id_jofl_account%type,
									adb_method  jofl.window.db_method%type)
		returns table (pkg jofl.window.interface_pkg%type
			      ,n_auth jofl.window.n_auth_based%type) as $$	
	begin
		return query			
			select case when w.n_auth_based = 1  and not jofl.jofl_proxy_pkg$validate_permission(aid_jofl_account, adb_method, 'OF_ROWS') then null
					else  w.interface_pkg end
			       pkg
			      ,w.n_auth_based as n_auth
			from jofl.window w
			where w.db_method = adb_method;
	
	end ;
$$ LANGUAGE plpgsql;