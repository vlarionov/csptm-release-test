create or replace function jofl.jofl$ref_pkg$attr_to_rowtype(attr text) returns jofl.win_field_ref as $$
	declare
		lr jofl.win_field_ref%rowtype;
	begin
		lr.field_name := jofl.jofl_pkg$extract_varchar(attr,'FIELD_NAME', false);
		lr.db_method  := jofl.jofl_pkg$extract_varchar(attr,'DB_METHOD', false);
		lr.ref_field  := jofl.jofl_pkg$extract_varchar(attr,'REF_FIELD', false);	
		lr.rowid := jofl.jofl_pkg$extract_varchar(attr,'ROWID', true);	
		lr.ref_db_method := jofl.jofl_pkg$extract_varchar(attr,'REF_DB_METHOD', true);	
		lr.dst_field := jofl.jofl_pkg$extract_varchar(attr,'DST_FIELD',false);	
		lr.is_custom_factory := jofl.jofl_pkg$extract_boolean(attr, 'IS_CUSTOM_FACTORY');
		lr.ref_factory := jofl.jofl_pkg$extract_varchar(attr, 'REF_FACTORY', true);
		return lr;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jofl$ref_pkg$of_rows(aid_account numeric
													,out arows refcursor
													,attr text) returns refcursor as $$
	declare
		lv_dbmethod jofl.win_field.db_method%type := jofl.jofl_pkg$extract_varchar(attr,'DB_METHOD', false);
		lv_field    jofl.win_field.field_name%type := jofl.jofl_pkg$extract_varchar(attr,'FIELD_NAME', false);
	begin
		open arows for
			select rowid	AS	ROWID
                   				,db_method	AS	"DB_METHOD"
                   				,field_name	AS	"FIELD_NAME"
                   				,ref_db_method	AS	"REF_DB_METHOD"
                   				,ref_field	AS	"REF_FIELD"
                   				,dst_field	AS	"DST_FIELD"
                   				,is_custom_factory	AS	"IS_CUSTOM_FACTORY"
                   				,ref_factory	AS	"REF_FACTORY"
			from jofl.win_field_ref f
			where f.db_method = lv_dbmethod
					and f.field_name = lv_field;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jofl$ref_pkg$of_insert(aid_account numeric, attr text) returns text as $$
	declare 
		lr jofl.win_field_ref%rowtype;		
	begin
		lr := jofl.jofl$ref_pkg$attr_to_rowtype(attr);
		insert into jofl.win_field_ref select lr.*;
		return null;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jofl$ref_pkg$of_update(aid_account numeric, attr text) returns text as $$
	declare 
		lr jofl.win_field_ref%rowtype;		
	begin
		lr := jofl.jofl$ref_pkg$attr_to_rowtype(attr);
		update jofl.win_field_ref  set
			db_method	=	lr.	db_method	,
			field_name	=	lr.	field_name	,
			ref_db_method	=	lr.	ref_db_method	,
			ref_field	=	lr.	ref_field	,
			dst_field	=	lr.	dst_field	,
			is_custom_factory	=	lr.	is_custom_factory	,
			ref_factory	=	lr.	ref_factory
		where rowid = lr.rowid;
		return null;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jofl$ref_pkg$of_delete(aid_account numeric, attr text) returns text as $$
	declare 
		lr jofl.win_field_ref%rowtype;		
	begin
		lr := jofl.jofl$ref_pkg$attr_to_rowtype(attr);
		delete from win_field_ref where rowid = lr.rowid;
		return null;
	end;
$$ LANGUAGE plpgsql;