 CREATE OR REPLACE FUNCTION core.ass_account_pkg$sso_login(  ausername text,
                                                             adomain text) RETURNS numeric AS $$
        declare
               lid_account     core.account.account_id%type;
               lis_locked      core.account.is_locked%type;
           begin
               select a.account_id,
                      a.is_locked
               into  strict lid_account,
                            lis_locked
                            from core.account a
                    join jofl.account ja
                        on a.account_id = ja.id_account_local
               where login ilike ausername||'@'||adomain;

               if lis_locked = true then
                   RAISE EXCEPTION USING ERRCODE = 20423, MESSAGE = '<<Учетная запись заблокирована!>>';
               else
                   return lid_account;
               end if;
            
           exception
               when no_data_found then
                   RAISE EXCEPTION USING ERRCODE = 20401, MESSAGE = '<<Не найдена учетная запись '
                   || ausername
                   || ' в домене '
                   || adomain
                   || '!>>';
           end;
    $$  LANGUAGE plpgsql ;


------------------------------------------------------------------------------------------------------------------------

    CREATE OR REPLACE FUNCTION core.ass_account_pkg$login(  ausername text,
                                                             apass text) RETURNS numeric AS $$
        declare
               lid_account     core.account.account_id%type;
               lis_locked      core.account.is_locked%type;
           begin
               select a.account_id,
                      a.is_locked
               into  strict lid_account,
                            lis_locked
                            from core.account a
                    join jofl.account ja
                        on a.account_id = ja.id_account_local
               where login ilike ausername
               and password = apass;

               if lis_locked = true then
                   RAISE EXCEPTION USING ERRCODE = 20423, MESSAGE = '<<Учетная запись заблокирована!>>';
               else
                   return lid_account;
               end if;

           exception
               when no_data_found then
                   RAISE EXCEPTION USING ERRCODE = 20401, MESSAGE = '<<Не правильный логин или пароль!>>';
           end;
    $$  LANGUAGE plpgsql ;


------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION core.ass_account_pkg$of_get_info( aid_account numeric,
		attr text) RETURNS text AS $$
      declare
          lres  text;
      begin
        with acc as ( select  --row_to_json( t.*)::text
                       a.login as "LOGIN" ,
                       a.first_name as "FIRST_NAME",
                       a.middle_name as "MIDDLE_NAME",
                       a.last_name as "LAST_NAME",
                       a.is_locked as "AS_LOCKED"
            from core.account a
                join jofl.account ja
                    on a.account_id = ja.id_account_local
                    where ja.id_jofl_account = aid_account),
            roles as ( select rr.id_role,
                              rr.role_name
                 from core.account a
                  join jofl.account ja
                      on a.account_id = ja.id_account_local
                  join jofl.role2account r2a
                      on ja.id_jofl_account = r2a.id_jofl_account
                  join jofl.ref_role rr
                      on r2a.id_role = rr.id_role
                       where ja.id_jofl_account =  aid_account),
               rl as (select array_to_json(ARRAY(select id_role from roles)) as "ROLES"),
               rn as  (select array_to_json(ARRAY(select role_name from roles)) as "ROLES_NAME"),
               allr as (select * from acc, rl, rn ),
               dn as (select row_to_json(al.*) as "displayObject" from allr al)
	   select row_to_json(dn.*) into lres from dn;

         return lres;
      end; $$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."ass_account_pkg$of_get_info"(numeric, text)
  OWNER TO adv;
