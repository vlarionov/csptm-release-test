

create or replace function jofl.jofl$call_pkg$attr_to_rowtype(attr text)
		returns jofl.win_action_call as $$
	declare
		lr jofl.win_action_call%rowtype;
	begin
		lr.opt_interface_pkg := jofl.jofl_pkg$extract_varchar(attr, 'OPT_INTERFACE_PKG', true);
		lr.call_action := jofl.jofl_pkg$extract_varchar(attr,'CALL_ACTION', false);
		lr.db_method := jofl.jofl_pkg$extract_varchar(attr,'DB_METHOD', false);
		lr.action_title := jofl.jofl_pkg$extract_varchar(attr,'ACTION_TITLE', false);
		lr.call_default := jofl.jofl_pkg$extract_varchar(attr,'CALL_DEFAULT', true);
		lr.is_refresh := jofl.jofl_pkg$extract_boolean(attr,'IS_REFRESH');
		lr.is_internal := jofl.jofl_pkg$extract_boolean(attr,'IS_INTERNAL');
		lr.is_win_action := jofl.jofl_pkg$extract_boolean(attr, 'IS_WIN_ACTION');
		lr.n_sort_order := jofl.jofl_pkg$extract_number(attr, 'N_SORT_ORDER', true);
		lr.is_row_depend := jofl.jofl_pkg$extract_boolean(attr, 'IS_ROW_DEPEND');
		return lr;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jofl$call_pkg$of_rows(aid_account in numeric, arows refcursor) returns refcursor as $$
	begin
		open arows for
			select db_method	AS	DB_METHOD
                  				,call_action	AS	"CALL_ACTION"
                  				,action_title	AS	"ACTION_TITLE"
                  				,is_refresh	AS	"IS_REFRESH"
                  				,is_internal	AS	"IS_INTERNAL"
			from jofl.win_action_call;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jofl$call_pkg$of_rows(aid_account in numeric,out arows refcursor, attr text) returns refcursor as $$
	declare
		lv_dbmethod jofl.win_field.db_method%type := jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', false);
	begin	
		open arows for
			select db_method	AS	"DB_METHOD"
                   				,call_action	AS	"CALL_ACTION"
                   				,action_title	AS	"ACTION_TITLE"
                   				,is_refresh	AS	"IS_REFRESH"
                   				,is_internal	AS	"IS_INTERNAL"
                      ,coalesce(is_win_action, 0) 	AS	"IS_WIN_ACTION"
                   				,n_sort_order	AS	"N_SORT_ORDER"
                   				,call_default	AS	"CALL_DEFAULT"
                   				,is_row_depend	AS	"IS_ROW_DEPEND"
                   				,opt_interface_pkg		AS	"OPT_INTERFACE_PKG"
			from jofl.win_action_call
			where db_method = lv_dbmethod
			order by n_sort_order nulls first;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jofl$call_pkg$of_insert(aid_account numeric
							 ,attr  text) returns text as $$
	declare
		lr jofl.win_action_call%rowtype;
	begin
		lr := jofl.jofl$call_pkg$attr_to_rowtype(attr);
		insert into jofl.win_action_call select lr.*;
		return null;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jofl$call_pkg$of_update(aid_account numeric
							 ,attr  text) returns text as $$
	declare
		lr jofl.win_action_call%rowtype;
	begin
		lr := jofl.jofl$call_pkg$attr_to_rowtype(attr);
		update jofl.win_action_call
		set     action_title	=	lr.	action_title,
			is_refresh	=	lr.	is_refresh,
			is_internal	=	lr.	is_internal,
			is_win_action	=	lr.	is_win_action,
			n_sort_order	=	lr.	n_sort_order,
			is_row_depend	=	lr.	is_row_depend,
			call_default	=	lr.	call_default,
			opt_interface_pkg	=	lr.	opt_interface_pkg
		where f.db_method = lr.db_method and f.call_action = lr.call_action;
		return null;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jofl$call_pkg$of_delete(aid_account numeric
							 ,attr  text) returns text as $$
	declare
		lr jofl.win_action_call%rowtype;
	begin
		lr := jofl.jofl$call_pkg$attr_to_rowtype(attr);
		delete from jofl.win_action_call
		where db_method = lr.db_method and call_action = lr.call_action;
		return null;
	end;
$$ LANGUAGE plpgsql;