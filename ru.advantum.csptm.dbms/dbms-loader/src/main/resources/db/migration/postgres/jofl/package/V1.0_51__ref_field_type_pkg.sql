create or replace function  jofl.ref_field_type_pkg$of_rows(aid_account numeric
															,out arows refcursor
															,attr text) returns refcursor as $$
	begin
		open arows for
			select field_type AS "FIELD_TYPE"
				,field_type_name AS "FIELD_TYPE_NAME"
			from jofl.ref_field_type;
	end ;
$$ LANGUAGE plpgsql;