CREATE TABLE core.account
(
  account_id bigserial NOT NULL, -- Пользователь
  last_name text NOT NULL, -- Фамилия
  first_name text, -- Имя
  middle_name text, -- Отчество
  login text NOT NULL, -- Логин
  password text NOT NULL, -- Пароль
  department_id bigint, -- Идентификатор подразделения
  CONSTRAINT pk_account_account_id PRIMARY KEY (account_id)
  USING INDEX TABLESPACE core_idx,
  CONSTRAINT fk_account_department FOREIGN KEY (department_id)
      REFERENCES core.department (department_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=TRUE
);
ALTER TABLE core.account
  OWNER TO adv;
COMMENT ON TABLE core.account
  IS 'Пользователь';
COMMENT ON COLUMN core.account.account_id IS 'Пользователь';
COMMENT ON COLUMN core.account.last_name IS 'Фамилия';
COMMENT ON COLUMN core.account.first_name IS 'Имя';
COMMENT ON COLUMN core.account.middle_name IS 'Отчество';
COMMENT ON COLUMN core.account.login IS 'Логин';
COMMENT ON COLUMN core.account.password IS 'Пароль';
COMMENT ON COLUMN core.account.department_id IS 'Идентификатор подразделения';

CREATE INDEX ix_account_department_id
  ON core.account
  USING btree
  (department_id)
TABLESPACE core_idx;


CREATE TABLE core.address
(
  adress_id bigserial NOT NULL, -- Идентификатор адреса
  address_text text NOT NULL, -- Адрес
  CONSTRAINT pk_address PRIMARY KEY (adress_id)
  USING INDEX TABLESPACE core_idx
)
WITH (
  OIDS=TRUE
);
ALTER TABLE core.address
  OWNER TO adv;
COMMENT ON TABLE core.address
  IS 'Адрес';
COMMENT ON COLUMN core.address.adress_id IS 'Идентификатор адреса';
COMMENT ON COLUMN core.address.address_text IS 'Адрес';


CREATE TABLE core.entity
(
  entity_id bigserial NOT NULL, -- Идентификатор сущности
  adress_id bigint, -- Идентификатор адреса
  name_full text, -- Наименование полное
  name_short text NOT NULL, -- Наименование сокращенное
  comment text, -- Контактная информация
  CONSTRAINT pk_entity PRIMARY KEY (entity_id)
  USING INDEX TABLESPACE core_idx,
  CONSTRAINT fk_address_to_entity FOREIGN KEY (adress_id)
      REFERENCES core.address (adress_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=TRUE
);
ALTER TABLE core.entity
  OWNER TO adv;
COMMENT ON TABLE core.entity
  IS 'Идентификатор реквизитов';
COMMENT ON COLUMN core.entity.entity_id IS 'Идентификатор сущности';
COMMENT ON COLUMN core.entity.adress_id IS 'Идентификатор адреса';
COMMENT ON COLUMN core.entity.name_full IS 'Наименование полное';
COMMENT ON COLUMN core.entity.name_short IS 'Наименование сокращенное';
COMMENT ON COLUMN core.entity.comment IS 'Контактная информация';

CREATE INDEX ix_adderss_id
  ON core.entity
  USING btree
  (adress_id)
TABLESPACE core_idx;


CREATE TABLE core.carrier
(
  carrier_id bigint NOT NULL, -- Идентификатор Транспортная организация
  CONSTRAINT pk_carrier PRIMARY KEY (carrier_id)
  USING INDEX TABLESPACE core_idx,
  CONSTRAINT fk_facility_entity FOREIGN KEY (carrier_id)
      REFERENCES core.entity (entity_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=TRUE
);
ALTER TABLE core.carrier
  OWNER TO adv;
COMMENT ON TABLE core.carrier
  IS 'Транспортная организация';
COMMENT ON COLUMN core.carrier.carrier_id IS 'Идентификатор Транспортная организация';


CREATE TABLE core.department
(
  department_id bigint NOT NULL, -- Идентификатор подразделения
  parent_id bigint, -- Идентификатор вышестоящего подразделения
  dt_begin timestamp without time zone NOT NULL, -- Дата ввода в действие
  dt_end timestamp without time zone, -- Дата ликвидации
  code integer NOT NULL, -- Код подразделения
  CONSTRAINT pk_department PRIMARY KEY (department_id)
  USING INDEX TABLESPACE core_idx,
  CONSTRAINT fk_department_parent_to_child FOREIGN KEY (parent_id)
      REFERENCES core.department (department_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_entity_to_department FOREIGN KEY (department_id)
      REFERENCES core.entity (entity_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=TRUE
);
ALTER TABLE core.department
  OWNER TO adv;
COMMENT ON COLUMN core.department.department_id IS 'Идентификатор подразделения';
COMMENT ON COLUMN core.department.parent_id IS 'Идентификатор вышестоящего подразделения';
COMMENT ON COLUMN core.department.dt_begin IS 'Дата ввода в действие';
COMMENT ON COLUMN core.department.dt_end IS 'Дата ликвидации';
COMMENT ON COLUMN core.department.code IS 'Код подразделения';


CREATE INDEX ix_department_parent_id
  ON core.department
  USING btree
  (parent_id)
TABLESPACE core_idx;


CREATE TABLE core.depo
(
  code integer NOT NULL, -- Код предприятия
  depo_id bigint NOT NULL, -- Идентификатор Транспортное предприятие
  carrier_id bigint NOT NULL,
  CONSTRAINT pk_depo PRIMARY KEY (depo_id)
  USING INDEX TABLESPACE core_idx,
  CONSTRAINT fk_carrier_to_depo FOREIGN KEY (carrier_id)
      REFERENCES core.carrier (carrier_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_entity_ident_to_depo FOREIGN KEY (depo_id)
      REFERENCES core.entity (entity_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=TRUE
);
ALTER TABLE core.depo
  OWNER TO adv;
COMMENT ON TABLE core.depo
  IS 'Транспортное предприятие';
COMMENT ON COLUMN core.depo.code IS 'Код предприятия';
COMMENT ON COLUMN core.depo.depo_id IS 'Идентификатор Транспортное предприятие';


CREATE INDEX ix_carrirer_id
  ON core.depo
  USING btree
  (carrier_id)
TABLESPACE core_idx;

CREATE TABLE core.territory
(
  territory_id bigint NOT NULL, -- Идентификатор территории
  carrier_id bigint, -- Транспортное предприятие
  code integer NOT NULL, -- Номер территории
  dt_begin timestamp without time zone NOT NULL, -- Дата ввода в действие
  dt_end timestamp without time zone, -- Дата ликвидации
  CONSTRAINT pk_terrirtory_id PRIMARY KEY (territory_id),
  CONSTRAINT fk_depo_to_terrirory FOREIGN KEY (carrier_id)
      REFERENCES core.depo (depo_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_entity_to_territory FOREIGN KEY (territory_id)
      REFERENCES core.entity (entity_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=TRUE
);
ALTER TABLE core.territory
  OWNER TO adv;
COMMENT ON TABLE core.territory
  IS 'Территория';
COMMENT ON COLUMN core.territory.territory_id IS 'Идентификатор территории';
COMMENT ON COLUMN core.territory.carrier_id IS 'Транспортное предприятие';
COMMENT ON COLUMN core.territory.code IS 'Номер территории';
COMMENT ON COLUMN core.territory.dt_begin IS 'Дата ввода в действие';
COMMENT ON COLUMN core.territory.dt_end IS 'Дата ликвидации';


CREATE INDEX ix_territory_carrier_id
  ON core.territory
  USING btree
  (carrier_id)
TABLESPACE core_idx;






