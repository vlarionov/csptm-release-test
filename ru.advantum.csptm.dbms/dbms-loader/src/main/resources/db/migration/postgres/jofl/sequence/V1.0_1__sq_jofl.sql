SET search_path TO jofl;
CREATE SEQUENCE jofl.sq_jofl
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE jofl.sq_jofl OWNER TO postgres;
