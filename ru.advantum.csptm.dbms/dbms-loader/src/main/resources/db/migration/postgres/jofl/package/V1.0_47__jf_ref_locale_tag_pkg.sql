create or replace function jofl.jf_ref_locale_tag_pkg$attr_to_rowtype(attr text) 
	returns table(id  text
		     ,scn numeric
		     ,lc_tag jofl.ref_locale_tag.lc_tag%type
		     ,tag_name jofl.ref_locale_tag.tag_name%type) as $$
	declare
	begin
		return query
			select null::text as id,
			null::numeric as scn,
			jofl.jofl_pkg$extract_varchar(attr,'LC_TAG', false)::jofl.ref_locale_tag.lc_tag%type as lc_tag,			
			jofl.jofl_pkg$extract_varchar(attr,'TAG_NAME', false)::jofl.ref_locale_tag.tag_name%type as tag_name;
	end;
$$ LANGUAGE plpgsql;

  
  
------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_ref_locale_tag_pkg$of_rows(aid_account numeric
							      ,out arows refcursor
							      ,attr text default null) returns refcursor as $$
	begin
		open arows for
			select lc_tag AS "LC_TAG"
		   	      ,tag_name AS "TAG_NAME"
			from jofl.ref_locale_tag src;
	end;
$$ LANGUAGE plpgsql;