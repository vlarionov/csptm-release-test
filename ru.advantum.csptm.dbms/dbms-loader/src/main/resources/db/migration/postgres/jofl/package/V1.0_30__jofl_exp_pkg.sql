create or replace function  jofl.jofl_exp_pkg$get_metadata(adb_method    in jofl.window.db_method%type
				,out awindow       refcursor
				,out afield        refcursor
				,out afield_ref    refcursor
				,out acall         refcursor
				,out acall_form    refcursor
				,out afilter       refcursor
				,out afilter_opts  refcursor
				,out adetail       refcursor
				,out alocalization refcursor) returns record as $$
	declare
		lv_method jofl.window.db_method%type;
	begin
		select db_method
		into lv_method
		from jofl.window
		where db_method = adb_method;
	
		open awindow for
			select * from jofl.window where db_method = adb_method;
	
		open afield for
			select * from jofl.win_field f where f.db_method = adb_method;
	
		open afield_ref for
			select * from jofl.win_field_ref f where f.db_method = adb_method;
	
		open acall for
			select * from jofl.win_action_call where db_method = adb_method;
	
		open acall_form for
			select * from jofl.win_action_field where db_method = adb_method;
	
		open afilter for
			select * from jofl.win_filter where db_method = adb_method;
	
		open afilter_opts for
			select * from jofl.win_filter_opts where db_method = adb_method;
	
		open adetail for
			select * from jofl.win_action_detail where db_method = adb_method;
	
		open alocalization for
			select * from jofl.ref_vocabulary where db_method = adb_method;
		return;
	
	exception
		when no_data_found then
			RAISE EXCEPTION USING ERRCODE = 20001, MESSAGE = 'Method <<' || adb_method || '>> not found!';
	end ;
$$ LANGUAGE plpgsql;

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function  jofl.jofl_exp_pkg$get_role_metadata(aid_role jofl.ref_role.id_role%type) returns refcursor as $$
	declare
		rolecall refcursor;
	begin
	
		open rolecall for
			select * from jofl.role2call r2c where r2c.id_role = aid_role;	
		return rolecall;
	exception
		when no_data_found then
			RAISE EXCEPTION USING ERRCODE = 20001, MESSAGE = 'Role <<' || AID_ROLE || '>> not found!';
	end ;
$$ LANGUAGE plpgsql;