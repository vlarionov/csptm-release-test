
create or replace function jofl.jf_ref_locale_pkg$attr_to_rowtype(attr text) returns table( id  text
		,scn numeric
		,lc_locale jofl.ref_locale.lc_locale%type) as $$
	begin
		return query
			select jofl.jofl_pkg$extract_varchar(attr,'ROWID', true) as id,
			       null::numeric as scn,
			       jofl.jofl_pkg$extract_varchar(attr,'LC_LOCALE', false)::character(5) as lc_locale;
	end;
$$ LANGUAGE plpgsql;


------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_ref_locale_pkg$of_insert(aid_account numeric
							    ,attr text) returns text as $$
	declare
		lc jofl.ref_locale.lc_locale%type;
	begin
		select lc_locale into lc from  jofl.jf_ref_locale_pkg$attr_to_rowtype(attr);
		insert into jofl.ref_locale (lc_locale) values (lc);
		return null;
	end;
$$ LANGUAGE plpgsql;


------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_ref_locale_pkg$of_update(aid_account numeric
							   ,attr text) returns text as $$
	declare
		lr record;
	begin
		lr := jofl.jf_ref_locale_pkg$attr_to_rowtype(attr);
		update jofl.ref_locale
		set lc_locale = lr.lc_locale
		where rowid = lr.rowid;
		return null;
	end;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_ref_locale_pkg$of_delete(aid_account numeric
							   ,attr text) returns text as $$
	declare
		lr record;
	begin
		lr := jofl.jf_ref_locale_pkg$attr_to_rowtype(attr);
		delete from jofl.ref_locale where rowid = lr.id;
		return null;
	end;
$$ LANGUAGE plpgsql;
	

------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_ref_locale_pkg$of_rows(aid_account numeric
							 ,out arows refcursor
							 ,attr text default null) returns refcursor as $$
	begin
		open arows for
			select lc_locale AS "LC_LOCALE"
			, rowid AS "ROWID"
			from jofl.ref_locale src;
	end;
$$ LANGUAGE plpgsql;