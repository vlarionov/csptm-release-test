SET search_path TO jofl;
CREATE SEQUENCE jofl.sq_menu
    START WITH 6
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE jofl.sq_menu OWNER TO postgres;