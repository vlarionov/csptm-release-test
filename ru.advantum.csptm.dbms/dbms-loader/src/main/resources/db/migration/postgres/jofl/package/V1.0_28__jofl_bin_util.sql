create or replace function jofl.jofl_bin_util$extract_file_id(abinary text, afield_name text) returns text as $$
	begin
		return jofl.jofl_pkg$extract_varchar(abinary, afield_name, false);
	exception
		when data_exception then
			return null;
	end ;
$$ LANGUAGE plpgsql;