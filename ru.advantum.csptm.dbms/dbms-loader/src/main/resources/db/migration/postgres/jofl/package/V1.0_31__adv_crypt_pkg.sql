create or replace function jofl.adv_crypt_pkg$md5(text)  returns text AS $$
select crypt($1, gen_salt('md5'));
$$ LANGUAGE SQL STRICT IMMUTABLE;
-----------------------------------------------------------------------------------------------------------------------------------------------

create or replace function jofl.adv_crypt_pkg$md5_wtih_seed(md5passwd text,
							seed      text) returns text AS $$
select jofl.adv_crypt_pkg$md5(seed||'|'||md5passwd);
$$ LANGUAGE SQL STRICT IMMUTABLE;