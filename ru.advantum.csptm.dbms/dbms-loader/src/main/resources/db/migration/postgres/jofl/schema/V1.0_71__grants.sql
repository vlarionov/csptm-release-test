--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;
SET search_path TO jofl;
--
-- Name: jofl; Type: ACL; Schema: -; Owner: jofl
--

GRANT ALL ON SCHEMA jofl TO adv;


--
-- Name: ref_role; Type: ACL; Schema: jofl; Owner: postgres
--

GRANT ALL ON TABLE ref_role TO postgres;
GRANT ALL ON TABLE ref_role TO adv;


--
-- Name: app_menu; Type: ACL; Schema: jofl; Owner: postgres
--

GRANT ALL ON TABLE app_menu TO postgres;
GRANT ALL ON TABLE app_menu TO adv;


--
-- Name: jf_project; Type: ACL; Schema: jofl; Owner: postgres
--

GRANT ALL ON TABLE jf_project TO postgres;
GRANT ALL ON TABLE jf_project TO adv;


--
-- Name: role2call; Type: ACL; Schema: jofl; Owner: postgres
--

GRANT ALL ON TABLE role2call TO postgres;
GRANT ALL ON TABLE role2call TO adv;


--
-- Name: win_action_detail; Type: ACL; Schema: jofl; Owner: postgres
--

GRANT ALL ON TABLE win_action_detail TO postgres;
GRANT ALL ON TABLE win_action_detail TO adv;


--
-- Name: sq_jofl; Type: ACL; Schema: jofl; Owner: postgres
--

GRANT ALL ON SEQUENCE sq_jofl TO postgres;
GRANT ALL ON SEQUENCE sq_jofl TO adv;


--
-- Name: win_field; Type: ACL; Schema: jofl; Owner: postgres
--

GRANT ALL ON TABLE win_field TO postgres;
GRANT ALL ON TABLE win_field TO adv;


--
-- Name: win_action_call; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE win_action_call FROM PUBLIC;
REVOKE ALL ON TABLE win_action_call FROM postgres;
GRANT ALL ON TABLE win_action_call TO postgres;
GRANT ALL ON TABLE win_action_call TO adv;


--
-- Name: win_field_ref; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE win_field_ref FROM PUBLIC;
REVOKE ALL ON TABLE win_field_ref FROM postgres;
GRANT ALL ON TABLE win_field_ref TO postgres;
GRANT ALL ON TABLE win_field_ref TO adv;


--
-- Name: window; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE "window" FROM PUBLIC;
REVOKE ALL ON TABLE "window" FROM postgres;
GRANT ALL ON TABLE "window" TO postgres;
GRANT ALL ON TABLE "window" TO adv;

--
-- Name: account; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE account FROM PUBLIC;
REVOKE ALL ON TABLE account FROM postgres;
GRANT ALL ON TABLE account TO postgres;
GRANT ALL ON TABLE account TO adv;


--
-- Name: constants_num; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE constants_num FROM PUBLIC;
REVOKE ALL ON TABLE constants_num FROM postgres;
GRANT ALL ON TABLE constants_num TO postgres;
GRANT ALL ON TABLE constants_num TO adv;


--
-- Name: constants_text; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE constants_text FROM PUBLIC;
REVOKE ALL ON TABLE constants_text FROM postgres;
GRANT ALL ON TABLE constants_text TO postgres;
GRANT ALL ON TABLE constants_text TO adv;


--
-- Name: create$java$lob$table; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE "create$java$lob$table" FROM PUBLIC;
REVOKE ALL ON TABLE "create$java$lob$table" FROM postgres;
GRANT ALL ON TABLE "create$java$lob$table" TO postgres;
GRANT ALL ON TABLE "create$java$lob$table" TO adv;


--
-- Name: java$options; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE "java$options" FROM PUBLIC;
REVOKE ALL ON TABLE "java$options" FROM postgres;
GRANT ALL ON TABLE "java$options" TO postgres;
GRANT ALL ON TABLE "java$options" TO adv;

--
-- Name: ref_const_type; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE ref_const_type FROM PUBLIC;
REVOKE ALL ON TABLE ref_const_type FROM postgres;
GRANT ALL ON TABLE ref_const_type TO postgres;
GRANT ALL ON TABLE ref_const_type TO adv;


--
-- Name: ref_editor; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE ref_editor FROM PUBLIC;
REVOKE ALL ON TABLE ref_editor FROM postgres;
GRANT ALL ON TABLE ref_editor TO postgres;
GRANT ALL ON TABLE ref_editor TO adv;


--
-- Name: ref_field_type; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE ref_field_type FROM PUBLIC;
REVOKE ALL ON TABLE ref_field_type FROM postgres;
GRANT ALL ON TABLE ref_field_type TO postgres;
GRANT ALL ON TABLE ref_field_type TO adv;


--
-- Name: ref_field_visibility; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE ref_field_visibility FROM PUBLIC;
REVOKE ALL ON TABLE ref_field_visibility FROM postgres;
GRANT ALL ON TABLE ref_field_visibility TO postgres;
GRANT ALL ON TABLE ref_field_visibility TO adv;


--
-- Name: ref_filter; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE ref_filter FROM PUBLIC;
REVOKE ALL ON TABLE ref_filter FROM postgres;
GRANT ALL ON TABLE ref_filter TO postgres;
GRANT ALL ON TABLE ref_filter TO adv;


--
-- Name: ref_filter_source; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE ref_filter_source FROM PUBLIC;
REVOKE ALL ON TABLE ref_filter_source FROM postgres;
GRANT ALL ON TABLE ref_filter_source TO postgres;
GRANT ALL ON TABLE ref_filter_source TO adv;


--
-- Name: ref_locale; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE ref_locale FROM PUBLIC;
REVOKE ALL ON TABLE ref_locale FROM postgres;
GRANT ALL ON TABLE ref_locale TO postgres;
GRANT ALL ON TABLE ref_locale TO adv;


--
-- Name: ref_locale_tag; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE ref_locale_tag FROM PUBLIC;
REVOKE ALL ON TABLE ref_locale_tag FROM postgres;
GRANT ALL ON TABLE ref_locale_tag TO postgres;
GRANT ALL ON TABLE ref_locale_tag TO adv;


--
-- Name: ref_menu_vocabulary; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE ref_menu_vocabulary FROM PUBLIC;
REVOKE ALL ON TABLE ref_menu_vocabulary FROM postgres;
GRANT ALL ON TABLE ref_menu_vocabulary TO postgres;
GRANT ALL ON TABLE ref_menu_vocabulary TO adv;


--
-- Name: ref_ui_platform; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE ref_ui_platform FROM PUBLIC;
REVOKE ALL ON TABLE ref_ui_platform FROM postgres;
GRANT ALL ON TABLE ref_ui_platform TO postgres;
GRANT ALL ON TABLE ref_ui_platform TO adv;


--
-- Name: ref_vocabulary; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE ref_vocabulary FROM PUBLIC;
REVOKE ALL ON TABLE ref_vocabulary FROM postgres;
GRANT ALL ON TABLE ref_vocabulary TO postgres;
GRANT ALL ON TABLE ref_vocabulary TO adv;


--
-- Name: role2account; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE role2account FROM PUBLIC;
REVOKE ALL ON TABLE role2account FROM postgres;
GRANT ALL ON TABLE role2account TO postgres;
GRANT ALL ON TABLE role2account TO adv;


--
-- Name: sq_menu; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON SEQUENCE sq_menu FROM PUBLIC;
REVOKE ALL ON SEQUENCE sq_menu FROM postgres;
GRANT ALL ON SEQUENCE sq_menu TO postgres;
GRANT ALL ON SEQUENCE sq_menu TO adv;


--
-- Name: sq_role; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON SEQUENCE sq_role FROM PUBLIC;
REVOKE ALL ON SEQUENCE sq_role FROM postgres;
GRANT ALL ON SEQUENCE sq_role TO postgres;
GRANT ALL ON SEQUENCE sq_role TO adv;


--
-- Name: win_action_field; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE win_action_field FROM PUBLIC;
REVOKE ALL ON TABLE win_action_field FROM postgres;
GRANT ALL ON TABLE win_action_field TO postgres;
GRANT ALL ON TABLE win_action_field TO adv;


--
-- Name: win_filter; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE win_filter FROM PUBLIC;
REVOKE ALL ON TABLE win_filter FROM postgres;
GRANT ALL ON TABLE win_filter TO postgres;
GRANT ALL ON TABLE win_filter TO adv;


--
-- Name: win_filter_opts; Type: ACL; Schema: jofl; Owner: postgres
--

REVOKE ALL ON TABLE win_filter_opts FROM PUBLIC;
REVOKE ALL ON TABLE win_filter_opts FROM postgres;
GRANT ALL ON TABLE win_filter_opts TO postgres;
GRANT ALL ON TABLE win_filter_opts TO adv;


--
-- PostgreSQL database dump complete
--

