package ru.advantum.pg.dbms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by zlobina on 11.08.16.
 */
public class DataBaseMigrateService {
    private static final Logger LOG = LoggerFactory.getLogger(DataBaseMigrateService.class);
    public static void main(String[] args){
        try {
            DBMigrateUtils.loadProperties(new String[]{"datasource.url",
                    "datasource.username",
                    "datasource.password",
                    "datasource.driverClassName"});

            DataSource ds = DBMigrateUtils.getDataSource();

            DBMigrateUtils.getFlyway(ds);
        } catch (IOException e) {
            LOG.error("Cannot load properties file", e);
        } catch (SQLException e) {
            LOG.error("Cannot load update scripts", e);
        }
    }


}
