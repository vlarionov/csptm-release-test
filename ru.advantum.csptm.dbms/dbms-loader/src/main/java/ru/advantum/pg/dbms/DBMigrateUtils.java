package ru.advantum.pg.dbms;

import org.flywaydb.core.Flyway;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by zlobina on 11.08.16.
 */
public class DBMigrateUtils {
    public static Flyway getFlyway(DataSource dataSource) throws SQLException {
        Flyway flyway = new Flyway();
/*

        Connection c= dataSource.getConnection();
        c.setAutoCommit(false);
        c.commit();
*/

        flyway.setDataSource(dataSource);

        flyway.setValidateOnMigrate(true);
        flyway.setLocations(new String[]{/*
                "db/migration/postgres/functions",
                "db/migration/postgres/schema",*/
                "db"/*,
                "postgres"*/});

//


     //   flyway.setSchemas("PUBLIC");

        //flyway.setPlaceholders(myPlaceholders);

        //flyway.setPlaceholders();
        System.out.print(flyway.getPlaceholders());
        //flyway.setBaselineOnMigrate(true);
        flyway.setBaselineOnMigrate(true);


     //   flyway.setSchemas("jofl");

        System.out.println("___" +flyway.migrate());

        return flyway;
    }

    public static DataSource getDataSource(){
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setUrl(System.getProperty("datasource.url"));
        ds.setUsername(System.getProperty("datasource.username"));
        ds.setPassword(System.getProperty("datasource.password"));
        ds.setDriverClassName(System.getProperty("datasource.driverClassName"));
      /*  DataSourceTransactionManager txManager = new DataSourceTransactionManager(ds);
        txManager.setNestedTransactionAllowed(false);
 */       Properties p = new Properties();
     /*   p.setProperty("defaultAutoCommit","true");
        p.setProperty("autoCommit","true");*/
        ds.setConnectionProperties(p);

        return ds;
    }

    public static void loadProperties(String... propertyKeys) throws IOException {
        Properties properties = new Properties();
        String dir = System.getProperty("advconf");
        dir = dir == null ? "/" : dir + "/";
        System.out.print("Property file " + dir + "dbms.properties");
        properties.load(Files.newInputStream(Paths.get(dir + "dbms.properties"), StandardOpenOption.READ));
        for (String pk : propertyKeys) {
            String p = properties.getProperty(pk);
            if (p != null && !p.isEmpty()){
                System.setProperty(pk, p);
            }
        }
    }
}