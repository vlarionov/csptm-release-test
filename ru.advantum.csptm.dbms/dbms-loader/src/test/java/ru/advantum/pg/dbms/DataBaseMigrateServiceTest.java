package ru.advantum.pg.dbms;

import org.junit.Test;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by zlobina on 11.08.16.
 */
public class DataBaseMigrateServiceTest {

   @Test
    public void test(){
        try {
            DBMigrateUtils.loadProperties(new String[]{"datasource.url",
                    "datasource.username",
                    "datasource.password",
                    "datasource.driverClassName"});


            DataSource ds = DBMigrateUtils.getDataSource();

            DBMigrateUtils.getFlyway(ds);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


}
