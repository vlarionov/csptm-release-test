/*transport units without unit*/
    with cur_rounds as (select *
                        from (
                               select
                                 ol.tr_id,
                                 ol.order_num,
                                 tte.timetable_entry_num,
                                 r.muid route_muid,
                                 r.number route_number,
                                 row_number()   over (partition by ol.tr_id    order by orv.time_plan_begin) rn,
                                 dr.tab_num
                               from tt.order_list ol
                                 join tt.driver d on ol.driver_id = d.driver_id
                                 join tt.order_round ord on ol.order_list_id = ord.order_list_id
                                 join asd.oper_round_visit orv on ord.order_round_id = orv.order_round_id
                                 join tt.round rnd on ord.round_id = rnd.round_id
                                 join tt.timetable_entry tte on tte.timetable_entry_id = rnd.timetable_entry_id
                                 join gis.route_trajectories rt on rnd.route_trajectory_muid = rt.muid
                                 join gis.route_rounds rr on rt.route_round_muid = rr.muid
                                 join gis.route_variants rv on rr.route_variant_muid = rv.muid
                                 join gis.routes r on rv.route_muid = r.muid
                                 left join tt.driver dr on dr.driver_id = ol.driver_id
                               where ol.order_date = now() :: date :: timestamp
                                     and ord.is_active = 1
                                     and ol.tr_id is not null
                                     ) t
                        where rn = 1)
    select
      tr.tr_id
      ,
      tr.seat_qty,
      tr.seat_qty_total,
      tr.has_low_floor,
      tr.has_conditioner,
      tr.garage_num,
      tr.licence,
      tr.serial_num,
      tr.year_of_issue,
      tr.dt_begin,
      tr.dt_end,
      tr.tr_type_id,
      tr_model.tr_capacity_id,
      tr.tr_status_id,
      tr.tr_model_id,
      tr.territory_id,
      tr_status.name tr_status_name,
      tr_model.name tr_model_name,
      tr_type.name tr_type_name,
      entity.name_short territory_name,
      tr_capacity.qty tr_capacity_qty,
      tr_capacity.short_name as tr_capacity_short_name,
      cr.route_number,
      cr.order_num,
      cr.timetable_entry_num,
      cr.tab_num,
      ent.name_full as depo_name,
      ent.name_short as depo_short_name,
        (select
           coalesce(snsr.check_if_unit_has_sensor_type(u2t.unit_id, core.jf_equipment_type_pkg$cn_type_skah()),case tr.tr_type_id when core.tr_type_pkg$tb() then false else null end)::int
         from core.unit2tr u2t join core.equipment on equipment.equipment_id = u2t.unit_id and equipment_model_id in (143, 144)
         where u2t.tr_id = tr.tr_id limit 1
      )
      as has_sah
    from core.tr
      join core.tr_model on tr_model.tr_model_id = tr.tr_model_id
      join core.tr_status on tr_status.tr_status_id = tr.tr_status_id
      join core.tr_type on tr_type.tr_type_id = tr.tr_type_id
      left join core.territory on territory.territory_id = tr.territory_id
      left join core.entity on entity.entity_id = territory.territory_id
      join core.tr_capacity on tr_capacity.tr_capacity_id = tr_model.tr_capacity_id
      join core.depo depo on depo.depo_id =tr.depo_id
      join core.entity ent on ent.entity_id = depo.depo_id
      left join cur_rounds cr on tr.tr_id = cr.tr_id
where tr.tr_id not in (
  SELECT tr.tr_id
  FROM core.tr tr
    JOIN core.depo d ON tr.depo_id = d.depo_id
    JOIN core.unit2tr ut ON tr.tr_id = ut.tr_id
    JOIN core.unit_bnst ub ON ub.unit_bnst_id = ut.unit_id
    JOIN core.equipment eq ON eq.equipment_id = ub.unit_bnst_id
    JOIN core.equipment_model em ON eq.equipment_model_id = em.equipment_model_id
    JOIN core.entity e ON d.depo_id = e.entity_id
where ut.sys_period @> now()
)
;
