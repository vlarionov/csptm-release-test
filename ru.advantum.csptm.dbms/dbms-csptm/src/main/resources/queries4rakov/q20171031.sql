/*1. Список БНСТ*/
SELECT
  e.equipment_id bnst_id,
  e.serial_num,
  em.name model_name
FROM core.unit_bnst ub
  JOIN core.equipment e ON ub.unit_bnst_id = e.equipment_id
  left JOIN core.equipment_model em ON e.equipment_model_id = em.equipment_model_id
;


/*2. Список ТС*/
SELECT
  tr.tr_id,
  tr.garage_num,
  d.tn_instance park_code
FROM core.tr tr
  JOIN core.depo d ON tr.depo_id = d.depo_id
;

 
/*3. Список привязки ТС к БНСТ*/
SELECT
  d.tn_instance as park_code,
  tr.garage_num,
  eq.serial_num,
  em.name
FROM core.tr tr
  JOIN core.depo d ON tr.depo_id = d.depo_id
  JOIN core.unit2tr ut ON tr.tr_id = ut.tr_id
  JOIN core.unit_bnst ub ON ub.unit_bnst_id = ut.unit_id
  join core.equipment eq on eq.equipment_id=ub.unit_bnst_id
  JOIN core.equipment_model em on eq.equipment_model_id = em.equipment_model_id
  JOIN core.entity e ON d.depo_id = e.entity_id
;


/*4. Навигационные отметки за период от ... до ..., исключая отметки недостоверные*/
SELECT
  unit_id,
  extract (epoch from  event_time) ut_eventtime,
  extract (epoch from  gps_time) ut_gpstime,
  extract (epoch from  receive_time) ut_receivetime,
  round(lon * 10000000)::INTEGER as lon,
  round(lat * 10000000)::INTEGER as lat,
  heading,
  is_hist_data
FROM core.traffic
WHERE location_valid
and event_time between :aDtB and :aDtE;


/*5. Навигационные отметки за период от ... до ..., исключая отметки недостоверные, по номеру БНСТ*/ 
SELECT
  unit_id,
  extract (epoch from  event_time) ut_eventtime,
  extract (epoch from  gps_time) ut_gpstime,
  extract (epoch from  receive_time) ut_receivetime,
  round(lon * 10000000)::INTEGER as lon,
  round(lat * 10000000)::INTEGER as lat,
  heading,
  is_hist_data
FROM core.traffic
WHERE location_valid
  and unit_id = :aUnid
and event_time between :aDtB and :aDtE;

/*6. Последние навигационные отметки, только с меткой реальльные*/
SELECT DISTINCT ON (t.unit_id)  unit_id,
  extract (epoch from  event_time) ut_eventtime,
  extract (epoch from  gps_time) ut_gpstime,
  extract (epoch from  receive_time) ut_receivetime,
  round(lon * 10000000)::INTEGER as lon,
  round(lat * 10000000)::INTEGER as lat,
  heading,
  is_hist_data
FROM core.traffic t
WHERE t.location_valid
      AND t.event_time BETWEEN (now() - INTERVAL '1 DAY') AND now()
ORDER BY t.unit_id , t.event_time desc;

/*7. Тоже что и 6
добавить поле: Последнее сообщение от БНСТ, id сообщения (id посланное устройством)
*/
SELECT DISTINCT ON (t.unit_id)  t.unit_id,
  extract (epoch from  t.event_time) ut_eventtime,
  extract (epoch from  t.gps_time) ut_gpstime,
  extract (epoch from  t.receive_time) ut_receivetime,
  round(t.lon * 10000000)::INTEGER as lon,
  round(t.lat * 10000000)::INTEGER as lat,
  t.heading,
  t.is_hist_data
, (select rs.response_name from cmnd.cmnd_request rq join cmnd.cmnd_response rs on rq.cmnd_request_id = rs.cmnd_request_id where rq.unit_id=t.unit_id order by rs.response_time desc LIMIT 1) RESP_NAME
, (select rs.response_param from cmnd.cmnd_request rq join cmnd.cmnd_response rs on rq.cmnd_request_id = rs.cmnd_request_id where rq.unit_id=t.unit_id order by rs.response_time desc LIMIT 1) RESP_PARAM
FROM core.traffic t
WHERE t.location_valid
      AND t.event_time BETWEEN (now() - INTERVAL '1 DAY') AND (now() + INTERVAL '1 DAY')
ORDER BY t.unit_id , t.event_time desc
;

/*8. Тоже что и 6
добавить поля: Версия Прошивки, Модель устройства */
SELECT DISTINCT ON (t.unit_id)  t.unit_id,
  extract (epoch from  t.event_time) ut_eventtime,
  extract (epoch from  t.gps_time) ut_gpstime,
  extract (epoch from  t.receive_time) ut_receivetime,
  round(t.lon * 10000000)/*::INTEGER as */lon,
  round(t.lat * 10000000)/*::INTEGER as */lat,
  t.heading,
  t.is_hist_data
, fw.version fw_version
, em.name model_name
FROM core.traffic t
  join core.unit_bnst ub on ub.unit_bnst_id=t.unit_id
  join core.equipment eq on eq.equipment_id=t.unit_id
  join core.equipment_model em on em.equipment_model_id=eq.equipment_model_id
  left join core.firmware fw on fw.firmware_id = eq.firmware_id
WHERE t.location_valid
      AND t.event_time BETWEEN (now() - INTERVAL '1 DAY') AND (now() + INTERVAL '1 DAY')
ORDER BY t.unit_id , t.event_time desc
;

/*9. Тоже что и 6
добавить поля: IMSI, последняя дата пакета c IMSI
*/
SELECT DISTINCT ON (t.unit_id)  t.unit_id,
  extract (epoch from  t.event_time) ut_eventtime,
  extract (epoch from  t.gps_time) ut_gpstime,
  extract (epoch from  t.receive_time) ut_receivetime,
  round(t.lon * 10000000)::INTEGER as lon,
  round(t.lat * 10000000)::INTEGER as lat,
  t.heading,
  t.is_hist_data
, (select rs.response_time from cmnd.cmnd_request rq join cmnd.cmnd_response rs on rq.cmnd_request_id = rs.cmnd_request_id where rq.unit_id=t.unit_id and rq.cmnd_ref_id = 14/*IMSI*/ order by rs.response_time desc LIMIT 1) IMSI_TIME
FROM core.traffic t
WHERE t.location_valid
      AND t.event_time BETWEEN (now() - INTERVAL '1 DAY') AND (now() + INTERVAL '1 DAY')
ORDER BY t.unit_id , t.event_time desc
;
