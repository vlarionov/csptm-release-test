
/*Номер БНСТ + Время +Широта+ Долгота+Курс(Азимут) */
SELECT
  unit_id,
  extract (epoch from  event_time) ut_eventtime,
  round(lon * 10000000)::INTEGER as lon,
  round(lat * 10000000)::INTEGER as lat,
  heading
FROM core.traffic
WHERE event_time between '2017-11-06 00:00:00'::timestamp and '2017-11-07 00:00:00'::timestamp;


/*Номер БНСТ+ Кол-во отметок*/ 
select unit_id, count(1) from core.traffic
WHERE location_valid
and event_time between '2017-11-06 00:00:00'::timestamp and '2017-11-07 00:00:00'::timestamp
GROUP BY unit_id
;


/*Время +Широта+ Долгота+Курс(Азимут) - как в протоколе NDTP за все время работы БНСТ*/
SELECT
  unit_id,
  extract (epoch from  event_time) ut_eventtime,
  round(lon * 10000000)::INTEGER as lon,
  round(lat * 10000000)::INTEGER as lat,
  heading
FROM core.traffic
;
