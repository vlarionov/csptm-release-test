select
  (with cur_rounds as (select *
                        from (
                               select
                                 ol.tr_id,
                                 ol.order_num,
                                 tte.timetable_entry_num,
                                 r.muid route_muid,
                                 r.number route_number,
                                 row_number()   over (partition by ol.tr_id    order by orv.time_plan_begin) rn,
                                 dr.tab_num
                               from tt.order_list ol
                                 join tt.driver d on ol.driver_id = d.driver_id
                                 join tt.order_round ord on ol.order_list_id = ord.order_list_id
                                 join asd.oper_round_visit orv on ord.order_round_id = orv.order_round_id
                                 join tt.round rnd on ord.round_id = rnd.round_id
                                 join tt.timetable_entry tte on tte.timetable_entry_id = rnd.timetable_entry_id
                                 join gis.route_trajectories rt on rnd.route_trajectory_muid = rt.muid
                                 join gis.route_rounds rr on rt.route_round_muid = rr.muid
                                 join gis.route_variants rv on rr.route_variant_muid = rv.muid
                                 join gis.routes r on rv.route_muid = r.muid
                                 left join tt.driver dr on dr.driver_id = ol.driver_id
                               where ol.order_date = now() :: date :: timestamp
                                     and ord.is_active = 1
                                     and ol.tr_id is not null
                                     ) t
                        where rn = 1)
    select
count (1)    from core.tr
      join core.tr_model on tr_model.tr_model_id = tr.tr_model_id
      join core.tr_status on tr_status.tr_status_id = tr.tr_status_id
      join core.tr_type on tr_type.tr_type_id = tr.tr_type_id
      left join core.territory on territory.territory_id = tr.territory_id
      left join core.entity on entity.entity_id = territory.territory_id
      join core.tr_capacity on tr_capacity.tr_capacity_id = tr_model.tr_capacity_id
      join core.depo depo on depo.depo_id =tr.depo_id
      join core.entity ent on ent.entity_id = depo.depo_id
      left join cur_rounds cr on tr.tr_id = cr.tr_id) cnt1
,
  (
    SELECT count(DISTINCT tr.tr_id)
    FROM core.tr tr
      JOIN core.depo d ON tr.depo_id = d.depo_id
      JOIN core.unit2tr ut ON tr.tr_id = ut.tr_id
      JOIN core.unit_bnst ub ON ub.unit_bnst_id = ut.unit_id
      JOIN core.equipment eq ON eq.equipment_id = ub.unit_bnst_id
      JOIN core.equipment_model em ON eq.equipment_model_id = em.equipment_model_id
      JOIN core.entity e ON d.depo_id = e.entity_id
    WHERE ut.sys_period @> now()
  ) cnt2
  , (select count(1) from (
                            WITH cur_rounds AS (SELECT *
                                                FROM (
                                                       SELECT
                                                         ol.tr_id,
                                                         ol.order_num,
                                                         tte.timetable_entry_num,
                                                         r.muid                           route_muid,
                                                         r.number                         route_number,
                                                         row_number()
                                                         OVER (
                                                           PARTITION BY ol.tr_id
                                                           ORDER BY orv.time_plan_begin ) rn,
                                                         dr.tab_num
                                                       FROM tt.order_list ol
                                                         JOIN tt.driver d ON ol.driver_id = d.driver_id
                                                         JOIN tt.order_round ord ON ol.order_list_id = ord.order_list_id
                                                         JOIN asd.oper_round_visit orv
                                                           ON ord.order_round_id = orv.order_round_id
                                                         JOIN tt.round rnd ON ord.round_id = rnd.round_id
                                                         JOIN tt.timetable_entry tte
                                                           ON tte.timetable_entry_id = rnd.timetable_entry_id
                                                         JOIN gis.route_trajectories rt
                                                           ON rnd.route_trajectory_muid = rt.muid
                                                         JOIN gis.route_rounds rr ON rt.route_round_muid = rr.muid
                                                         JOIN gis.route_variants rv ON rr.route_variant_muid = rv.muid
                                                         JOIN gis.routes r ON rv.route_muid = r.muid
                                                         LEFT JOIN tt.driver dr ON dr.driver_id = ol.driver_id
                                                       WHERE ol.order_date = now() :: DATE :: TIMESTAMP
                                                             AND ord.is_active = 1
                                                             AND ol.tr_id IS NOT NULL
                                                     ) t
                                                WHERE rn = 1)
                            SELECT
                              tr.tr_id,
                              tr.seat_qty,
                              tr.seat_qty_total,
                              tr.has_low_floor,
                              tr.has_conditioner,
                              tr.garage_num,
                              tr.licence,
                              tr.serial_num,
                              tr.year_of_issue,
                              tr.dt_begin,
                              tr.dt_end,
                              tr.tr_type_id,
                              tr_model.tr_capacity_id,
                              tr.tr_status_id,
                              tr.tr_model_id,
                              tr.territory_id,
                              tr_status.name            tr_status_name,
                              tr_model.name             tr_model_name,
                              tr_type.name              tr_type_name,
                              entity.name_short         territory_name,
                              tr_capacity.qty           tr_capacity_qty,
                              tr_capacity.short_name AS tr_capacity_short_name,
                              cr.route_number,
                              cr.order_num,
                              cr.timetable_entry_num,
                              cr.tab_num,
                              ent.name_full          AS depo_name,
                              ent.name_short         AS depo_short_name,
                              (SELECT coalesce(snsr.check_if_unit_has_sensor_type(u2t.unit_id,
                                                                                  core.jf_equipment_type_pkg$cn_type_skah()),
                                               CASE tr.tr_type_id
                                               WHEN core.tr_type_pkg$tb()
                                                 THEN FALSE
                                               ELSE NULL END) :: INT
                               FROM core.unit2tr u2t
                                 JOIN core.equipment
                                   ON equipment.equipment_id = u2t.unit_id AND equipment_model_id IN (143, 144)
                               WHERE u2t.tr_id = tr.tr_id
                               LIMIT 1
                              )
                                                     AS has_sah
                            FROM core.tr
                              JOIN core.tr_model ON tr_model.tr_model_id = tr.tr_model_id
                              JOIN core.tr_status ON tr_status.tr_status_id = tr.tr_status_id
                              JOIN core.tr_type ON tr_type.tr_type_id = tr.tr_type_id
                              LEFT JOIN core.territory ON territory.territory_id = tr.territory_id
                              LEFT JOIN core.entity ON entity.entity_id = territory.territory_id
                              JOIN core.tr_capacity ON tr_capacity.tr_capacity_id = tr_model.tr_capacity_id
                              JOIN core.depo depo ON depo.depo_id = tr.depo_id
                              JOIN core.entity ent ON ent.entity_id = depo.depo_id
                              LEFT JOIN cur_rounds cr ON tr.tr_id = cr.tr_id
                            WHERE tr.tr_id NOT IN (
                              SELECT tr.tr_id
                              FROM core.tr tr
                                JOIN core.depo d ON tr.depo_id = d.depo_id
                                JOIN core.unit2tr ut ON tr.tr_id = ut.tr_id
                                JOIN core.unit_bnst ub ON ub.unit_bnst_id = ut.unit_id
                                JOIN core.equipment eq ON eq.equipment_id = ub.unit_bnst_id
                                JOIN core.equipment_model em ON eq.equipment_model_id = em.equipment_model_id
                                JOIN core.entity e ON d.depo_id = e.entity_id
                              WHERE ut.sys_period @> now()
                            )
                          ) t) cnt3
;
