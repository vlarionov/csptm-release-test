/*4 search for the same codes and times*/
with aData as (
    SELECT
      unit_id,
      extract(EPOCH FROM event_time)      ut_eventtime,
      extract(EPOCH FROM gps_time)        ut_gpstime,
      extract(EPOCH FROM receive_time)    ut_receivetime,
      round(lon * 10000000) :: INTEGER AS lon,
      round(lat * 10000000) :: INTEGER AS lat,
      heading,
      is_hist_data
    FROM core.traffic
    WHERE location_valid
          AND event_time BETWEEN :aDtB AND :aDtE
)
select unit_id, ut_eventtime, count(1) from aData
group by unit_id, ut_eventtime
having count(1) > 1
;

/*4 sample data*/
select * from core.traffic t where t.unit_id=664273
                                   AND event_time BETWEEN :aDtB AND :aDtE
                                   and extract(EPOCH FROM event_time) = 1509618073
ORDER BY packet_id
;

/*6 search for the same codes and times*/
with aData as (
  SELECT DISTINCT ON (t.unit_id)  unit_id,
  extract (epoch from  event_time) ut_eventtime,
  extract (epoch from  gps_time) ut_gpstime,
  extract (epoch from  receive_time) ut_receivetime,
  round(lon * 10000000)::INTEGER as lon,
  round(lat * 10000000)::INTEGER as lat,
  heading,
  is_hist_data
  , event_time
FROM core.traffic t
WHERE t.location_valid
      AND t.event_time BETWEEN (now() - INTERVAL '1 DAY') AND now()
ORDER BY t.unit_id , t.event_time desc
)
select unit_id, ut_eventtime, event_time, count(1) from aData
group by unit_id, ut_eventtime, event_time
having count(1) > 1;
