/*online*/
with b as (
  SELECT DISTINCT ON (usl.unit_id, u.unit_bnst_id)
    usl.unit_id,u.unit_bnst_id,
    usl.time_start, usl.time_stop, usl.tr_id, usl.session_id, usl.total_packet_qty
  FROM kdbo.unit_session_log usl
    join core.unit_bnst u on u.unit_bnst_id=usl.unit_id
   WHERE 1=1
     and usl.time_stop IS NULL
  ORDER BY usl.unit_id, u.unit_bnst_id, usl.time_start desc
)
, t as (
  select DISTINCT ON (t.unit_id) t.unit_id
  , t.gps_time, t.event_time
  from core.traffic t
    where 1=1
    and t.location_valid
    and t.event_time BETWEEN now() - INTERVAL'1 day' and now()
  order by t.unit_id, t.event_time desc
)
select * from b join t on t.unit_id=b.unit_id
;

/**/