﻿
-- [Разработка КСУПТ (CSPTM) - Предпроектные работы #21898] (Новая) Логирование данных базовых таблиц
-- http://redmine.advantum.ru/issues/21898
-- Параметры:
-- :table - базовая таблица, на основе которой создается историческая
-- :schema - схема базовой таблицы
with q as
  (
    WITH q AS (SELECT
                 chr(13) || chr(10) crlf,
                 :schema::text as sch, :table::text as tab
    )
    SELECT  sch, tab base_tab,
           (select oid from pg_class
            where relkind = 'r' and relname = tab
              and relnamespace = (select oid from pg_namespace where nspname = sch)
           ) base_tab_oid,
           'create table hist.%s_%s (like %s.%s including comments)'::text create_table,
           'CREATE INDEX ON hist.%s_%s USING gist (sys_period) tablespace hist_idx;'::text create_index,
            concat($$CREATE TRIGGER versioning_trigger$$, crlf,
                   $$BEFORE INSERT OR UPDATE OR DELETE ON %s.%s FOR EACH ROW $$, crlf,
                   $$EXECUTE PROCEDURE public.versioning( 'sys_period', 'hist.%s_%s', true);$$
            )  create_trigger,
            concat($$CREATE VIEW hist.v_%s_hist AS$$, crlf,
                   $$SELECT * FROM %s.%s$$, crlf,
                   $$  UNION ALL  $$, crlf,
                   $$SELECT * FROM hist.%s_%s;$$
            ) create_view,
            $$comment on table hist.%s_%s is '%s';$$::text create_tab_comment,
            $$alter table hist.%s_%s add constraint %s_%s %s;$$::text create_constraint

    FROM q
  )
select format(create_table, sch, base_tab, sch, base_tab) ddl from q
    union all
select format(create_index, sch, base_tab) from q
    union all
select format(create_trigger, sch, base_tab, sch, base_tab) from q
    union all
select format(create_view, base_tab, sch, base_tab, sch, base_tab) from q
    union all
select format(create_tab_comment, sch, base_tab, d.description)
from pg_description d, q
where d.objoid = q.base_tab_oid
  and d.objsubid = 0
    union all
select format(create_constraint, sch, base_tab, sch, conname, pg_get_constraintdef(oid))
from pg_constraint c, q
where c.contype in ('c', 'f')
  and c.conrelid = q.base_tab_oid


