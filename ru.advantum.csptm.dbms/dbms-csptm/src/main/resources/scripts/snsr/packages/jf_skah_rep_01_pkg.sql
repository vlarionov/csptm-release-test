CREATE OR REPLACE FUNCTION snsr.jf_skah_rep_01_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare
  l_garage_num int := jofl.jofl_pkg$extract_number(p_attr, 'GARAGE_NUM_TEXT', true)::INT;
  l_dt_begin timestamp := jofl.jofl_pkg$extract_date(p_attr,'BEGIN_DT', true);
  l_dt_end timestamp := jofl.jofl_pkg$extract_date(p_attr,'END_DT', true);
  l_depo_id_array int[] := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_depo_id', true);
  l_territory_id_array int [] := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_territory_id', true);
  l_route_id_array text [] := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_route_muid', true);
  l_view_route_var int := jofl.jofl_pkg$extract_number(p_attr, 'F_VIEW_INPLACE_K', true)::INT;
begin

  open p_rows for
  WITH
      tr as (
        select tr_id
        from core.tr
        where((garage_num = l_garage_num)  or l_garage_num is null)
          and ((depo_id = any(l_depo_id_array))  or (array_length(l_depo_id_array, 1)  is null))
          and ((territory_id = any(l_territory_id_array)) or (array_length(l_territory_id_array, 1)  is null))
    )

    , a  AS
  (
      SELECT
        tr_id
        ,val
        ,sensor_id
        ,event_time
        ,row_number() OVER (ORDER BY tr_id, sensor_id, event_time, packet_id) rn
      FROM snsr.binary_sensor_data
      where equipment_type_id = core.jf_equipment_type_pkg$cn_type_skah()
            and binary_sensor_data.tr_id = ANY (ARRAY(select tr_id  from tr))
            and event_time::date = current_date::date
            and event_time between l_dt_begin and l_dt_end
  )

    ,b AS (
      SELECT
        tr_id
        ,val
        ,sensor_id
        ,event_time
        ,row_number() OVER (ORDER BY tr_id, sensor_id, event_time, rn) - row_number() OVER (PARTITION BY tr_id, sensor_id, val ORDER BY tr_id, sensor_id, event_time,rn) gr_id
      FROM a
  )
    ,c as (
      select
        tr_id,
        val,
        sensor_id,
        min(event_time) event_time_min,
        max(event_time) event_time_max,
        ( select
             tte.route_variant_muid
           from asd.oper_round_visit orv
             join tt.order_round ord on ord.order_round_id = orv.order_round_id
             join tt.order_list ol on ol.order_list_id= ord.order_list_id and ol.sign_deleted = 0
             join tt.timetable_entry tte on tte.timetable_entry_id = ol.timetable_entry_id
           where
             min(event_time) between orv.time_fact_begin and orv.time_fact_end
             and ol.tr_id = b.tr_id
             and orv.order_round_status_id in (1,3,4) --пройден, выполняется, пройден частично
           group by tte.route_variant_muid) as route_variant_id
        -- ,   asd.utils_pkg$get_route_var_id(min(event_time), tr_id) as route_variant_id
      from b
      where val
      GROUP BY tr_id,
        sensor_id,
        gr_id,
        val
      --having max(event_time) > min(event_time)
  )

  ,adata as (
  select
     event_time_min as time_start
    ,event_time_max as time_stop
    ,case event_time_max when event_time_min then 1 else ceil(extract (EPOCH FROM  (event_time_max - event_time_min))::float/60) end as  duration
    ,garage_num as garage_num
    ,tr_model_name tr_model_name
    ,depo_name depo_name
    ,territory_name territory_name
    ,equipment_model_name unit_type_name
    ,unit_serial_num equipment_serial_num
    ,sensor_model_name sensor_module_name
    ,sensor_serial_num sensor_serial_num
    ,c.route_variant_id
    --,r.number  as route_variant_num
    ,coalesce(r.number , '-') as route_variant_num
    ,c.tr_id,
    0 distance
  FROM snsr.v_sensor_attrib sa
    join c on sa.tr_id = c.tr_id and sa.sensor_id = c.sensor_id
    left join gis.route_variants rv on rv.muid = c.route_variant_id
    left join gis.routes r on r.muid= rv.route_muid
  WHERE sa.unit2tr_sys_period @> event_time_min::TIMESTAMPTZ
    and sa.sensor2unit_hist_sys_period @> event_time_min::TIMESTAMPTZ
    and sa.tr_id in (select adm.account_data_realm_pkg$get_trs_by_tr_type_and_depo(p_id_account))
    and  ((r.muid::text = any(l_route_id_array))  or (array_length(l_route_id_array, 1)  is null))
    and (coalesce(l_view_route_var,0)=0 or (l_view_route_var=1 and  route_variant_id is not null) or (l_view_route_var=-1 and  route_variant_id is null ))

  UNION ALL

  select
    lower(whc.vals_period) as time_start,
    upper(whc.vals_period) as time_stop,
    whc.val_int_01 as duration,
    tr.garage_num as garage_num,
    m.name as  tr_model_name,
    depo_entity.name_short AS depo_name,
    terr_entity.name_full AS territory_name,
    uem.name  as unit_type_name,
    u.serial_num AS equipment_serial_num,
    em.name AS sensor_model_name,
    e.serial_num AS sensor_serial_num,
    whc.route_variant_id,
    coalesce(r.number , '-') as route_variant_num,
    --r.number  as route_variant_num,
    whc.tr_id,
    whc.val_int_02 distance
  from  wh.wh_core  whc
    join tr tr_select on tr_select.tr_id = whc.tr_id
    join core.depo d ON  d.depo_id  = whc.depo_id
    join core.tr tr on tr.tr_id= whc.tr_id
    join core.tr_model m on m.tr_model_id = tr.tr_model_id
    join core.territory trr on trr.territory_id  = whc.territory_id
    join core.entity depo_entity on d.depo_id = depo_entity.entity_id
    left join  core.entity terr_entity on trr.territory_id = terr_entity.entity_id
    join core.equipment u on u.equipment_id = whc.unit_id
    join core.equipment_model uem on uem.equipment_model_id = u.equipment_model_id
    join core.sensor s on s.sensor_id = whc.sensor_id
    join core.equipment e on e.equipment_id = s.sensor_id
    join core.equipment_model em on em.equipment_model_id = e.equipment_model_id
    left join gis.route_variants rv on rv.muid = whc.route_variant_id
    left join gis.routes r on r.muid= rv.route_muid
  where
    whc.wh_type_id = wh.wh_core_process_pkg$cn_skah_period()
    and l_dt_begin is not null and  l_dt_end is not null
    and whc.vals_period &&  tsrange(l_dt_begin, l_dt_end)
    and lower(whc.vals_period) >= l_dt_end - interval '7 day'  --добавлено условие чтобы ограничить выборку (сгласовано с Пановым В)
    and whc.tr_id = ANY (ARRAY(select tr_id  from tr))
    and  ((r.muid::text = any(l_route_id_array))  or (array_length(l_route_id_array, 1)  is null))
    and (coalesce(l_view_route_var,0)=0 or (l_view_route_var=1 and  route_variant_id is not null) or (l_view_route_var=-1 and  route_variant_id is null ))
)
  select *   from adata
  ;

end;
$function$
;