CREATE OR REPLACE FUNCTION snsr.check_if_unit_has_sensor_type(p_unit_id bigint, p_equipment_type_id bigint)
  RETURNS BOOLEAN
LANGUAGE plpgsql
AS $function$
declare
  l_return BOOLEAN := false;
begin
  select true
  into l_return
  from snsr.v_sensors
  where unit_id = p_unit_id and equipment_type_id = p_equipment_type_id;

  return l_return;
end;
$function$
;