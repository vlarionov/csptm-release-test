CREATE OR REPLACE FUNCTION snsr.jf_skah_rep_02_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare

  l_garage_num int := jofl.jofl_pkg$extract_number(p_attr, 'GARAGE_NUM_TEXT', true)::INT;
  l_dt_begin timestamp := jofl.jofl_pkg$extract_date(p_attr,'BEGIN_DT', true);
  l_dt_end timestamp := jofl.jofl_pkg$extract_date(p_attr,'END_DT', true);
  l_min int := jofl.jofl_pkg$extract_number(p_attr, 'F_MIN_NUMBER', true)::INT;
  l_max int := jofl.jofl_pkg$extract_number(p_attr, 'F_MAX_NUMBER', true)::INT;
  l_depo_id_array int[] := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_depo_id', true);
  l_territory_id_array int [] := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_territory_id', true);
  l_route_id_array text [] := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_route_muid', true);
  l_view_route_var int := jofl.jofl_pkg$extract_number(p_attr, 'F_VIEW_INPLACE_K', true)::INT;

begin

  open p_rows for
  WITH
  tr as (
        select tr_id
        from core.tr
        where((garage_num = l_garage_num)  or l_garage_num is null)
             and ((depo_id = any(l_depo_id_array))  or (array_length(l_depo_id_array, 1)  is null))
             and ((territory_id = any(l_territory_id_array)) or (array_length(l_territory_id_array, 1)  is null))
    )

  ,a  AS(
    SELECT
      bsd.tr_id
      ,u2t.unit_id
      ,val
      ,bsd.sensor_id
      ,event_time
      ,row_number() OVER (ORDER BY bsd.tr_id, bsd.sensor_id, packet_id) rn
      ,lower(u2t.sys_period * s2u.sys_period) as start_time_stamp
    FROM snsr.binary_sensor_data bsd
        join hist.v_unit2tr_hist u2t on u2t.tr_id =  bsd.tr_id and u2t.sys_period @> bsd.event_time::TIMESTAMPTZ
        join hist.v_sensor2unit_hist s2u on u2t.unit_id = s2u.unit_id and s2u.sensor_id = bsd.sensor_id and s2u.sys_period @> bsd.event_time::TIMESTAMPTZ and u2t.sys_period && u2t.sys_period
    where equipment_type_id = core.jf_equipment_type_pkg$cn_type_skah()
            and event_time between l_dt_begin and l_dt_end
            and event_time::date =  current_date
            and bsd.tr_id = ANY (ARRAY(select tr_id  from tr))
        )
    ,b AS (
      SELECT
        tr_id
        ,unit_id
        ,val
        ,sensor_id
        ,start_time_stamp
        ,event_time
        ,row_number() OVER (ORDER BY tr_id, unit_id, sensor_id, start_time_stamp, event_time, rn) - row_number() OVER (PARTITION BY tr_id, unit_id, sensor_id, start_time_stamp, val ORDER BY tr_id, unit_id, sensor_id, start_time_stamp, event_time,rn) gr_id
      FROM a
  )
    ,c as (
      select
        tr_id,
        unit_id,
        val,
        sensor_id,
        start_time_stamp,
        case max(event_time)  when min(event_time) then 1 else ceil (extract(EPOCH FROM (max(event_time)  - min(event_time)))::decimal/60) end as minutes,
        ( select
            tte.route_variant_muid
          from asd.oper_round_visit orv
            join tt.order_round ord on ord.order_round_id = orv.order_round_id
            join tt.order_list ol on ol.order_list_id= ord.order_list_id and ol.sign_deleted = 0
            join tt.timetable_entry tte on tte.timetable_entry_id = ol.timetable_entry_id
          where
            min(event_time) between orv.time_fact_begin and orv.time_fact_end
            and ol.tr_id = b.tr_id
            and orv.order_round_status_id in (1,3,4) --пройден, выполняется, пройден частично
          group by tte.route_variant_muid) as route_variant_id
      -- ,   asd.utils_pkg$get_route_var_id(min(event_time), tr_id) as route_variant_id
      from b
      where val
      GROUP BY tr_id,
        unit_id,
        sensor_id,
        start_time_stamp,
        gr_id,
        val
    --  having max(event_time) > min(event_time)
  )
    ,d as (
      select
        tr_id,
        unit_id,
        sensor_id,
        start_time_stamp,
        sum(minutes) minutes,
        count(*) cnt,
        array_to_string(ARRAY_AGG(distinct r.number ),',') as route_variant_num
      from c
      left join gis.route_variants rv on rv.muid = c.route_variant_id
      left join gis.routes r on r.muid= rv.route_muid
      where val
      and  ((r.muid::text = any(l_route_id_array))  or (array_length(l_route_id_array, 1)  is null))
      and (coalesce(l_view_route_var,0)=0 or (l_view_route_var=1 and  route_variant_id is not null) or (l_view_route_var=-1 and  route_variant_id is null ))
      GROUP BY tr_id,
        unit_id,
        sensor_id,
        start_time_stamp
  )
    , adata as (
  select
    to_char(l_dt_begin + interval '3 hours','dd.mm.yyyy hh24:mi:ss') || ' - ' || to_char(l_dt_end  + interval '3 hours','dd.mm.yyyy hh24:mi:ss') as period,
    '[' || json_build_object('CAPTION', 'Общие сведения', 'JUMP',
                             json_build_object('METHOD', 'snsr.skah_rep_01','ATTR',
                                               json_build_object('BEGIN_DT', to_char(l_dt_begin,'yyyy-mm-dd hh24:mi:ss'), 'END_DT', to_char(l_dt_end,'yyyy-mm-dd hh24:mi:ss'), 'GARAGE_NUM_TEXT', garage_num::text)::text
                             )) || ']' as jump_skah_rep_01
    ,minutes as  duration
    ,garage_num as garage_num
    ,tr_model_name tr_model_name
    ,depo_name depo_name
    ,territory_name territory_name
    ,equipment_model_name unit_type_name
    ,unit_serial_num equipment_serial_num
    ,sensor_model_name sensor_module_name
    ,sensor_serial_num sensor_serial_num
    ,cnt
    ,route_variant_num
    ,d.tr_id
    ,0 distance
  FROM snsr.v_sensor_attrib sa join d on sa.tr_id = d.tr_id and sa.sensor_id = d.sensor_id and sa.unit_id = d.unit_id
  WHERE sa.unit2tr_sys_period @> start_time_stamp
        and sa.sensor2unit_hist_sys_period @> start_time_stamp
        and d.tr_id in (select adm.account_data_realm_pkg$get_trs_by_tr_type_and_depo(p_id_account))
        and minutes between coalesce(l_min, minutes) and coalesce(l_max, minutes)

  union all

  select
    to_char(l_dt_begin + interval '3 hours','dd.mm.yyyy hh24:mi:ss') || ' - ' || to_char(l_dt_end  + interval '3 hours','dd.mm.yyyy hh24:mi:ss') as period,
    '[' || json_build_object('CAPTION', 'Общие сведения', 'JUMP',
                             json_build_object('METHOD', 'snsr.skah_rep_01','ATTR',
                                               json_build_object('BEGIN_DT', to_char(l_dt_begin,'yyyy-mm-dd hh24:mi:ss'), 'END_DT', to_char(l_dt_end,'yyyy-mm-dd hh24:mi:ss'), 'GARAGE_NUM_TEXT', garage_num::text)::text
                             )) || ']' as jump_skah_rep_01
    ,
    sum(whc.val_int_01) as duration,
    tr.garage_num as garage_num,
    m.name as  tr_model_name,
    depo_entity.name_short AS depo_name,
    terr_entity.name_full AS territory_name,
    uem.name  as unit_type_name,
    u.serial_num AS equipment_serial_num,
    em.name AS sensor_model_name,
    e.serial_num AS sensor_serial_num,
    count(*) cnt,
    array_to_string(ARRAY_AGG(distinct r.number ),',') as route_variant_num,
    whc.tr_id,
    sum(whc.val_int_02) distance
  from  wh.wh_core  whc
    join core.depo d ON  d.depo_id  = whc.depo_id
    join core.tr tr on tr.tr_id= whc.tr_id
    join core.tr_model m on m.tr_model_id = tr.tr_model_id
    join core.territory trr on trr.territory_id  = whc.territory_id
    join core.entity depo_entity on d.depo_id = depo_entity.entity_id
    left join  core.entity terr_entity on trr.territory_id = terr_entity.entity_id
    join core.equipment u on u.equipment_id = whc.unit_id
    join core.equipment_model uem on uem.equipment_model_id = u.equipment_model_id
    join core.sensor s on s.sensor_id = whc.sensor_id
    join core.equipment e on e.equipment_id = s.sensor_id
    join core.equipment_model em on em.equipment_model_id = e.equipment_model_id
    left join gis.route_variants rv on rv.muid = whc.route_variant_id
    left join gis.routes r on r.muid= rv.route_muid
  where whc.wh_type_id = wh.wh_core_process_pkg$cn_skah_period()
        and l_dt_begin is not null and  l_dt_end is not null
        and whc.vals_period &&  tsrange(l_dt_begin, l_dt_end)
        and whc.tr_id = ANY (ARRAY(select tr_id  from tr))
        and  ((r.muid::text = any(l_route_id_array))  or (array_length(l_route_id_array, 1)  is null))
        and (coalesce(l_view_route_var,0)=0 or (l_view_route_var=1 and  route_variant_id is not null) or (l_view_route_var=-1 and  route_variant_id is null ))
  group by
    tr.garage_num ,
    m.name,
    depo_entity.name_short,
    terr_entity.name_full,
    uem.name,
    u.serial_num,
    em.name ,
    e.serial_num,
    whc.tr_id
  having sum(whc.val_int_01) between coalesce(l_min,  sum(whc.val_int_01)) and coalesce(l_max,  sum(whc.val_int_01))
)
  select * from adata


  ;
end;
$function$
;