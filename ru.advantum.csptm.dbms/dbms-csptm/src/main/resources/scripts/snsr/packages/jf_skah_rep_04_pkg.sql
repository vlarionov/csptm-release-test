CREATE OR REPLACE FUNCTION snsr.jf_skah_rep_04_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare

  l_garage_num int := jofl.jofl_pkg$extract_number(p_attr, 'GARAGE_NUM_TEXT', true)::INT;
  l_dt_begin timestamp := jofl.jofl_pkg$extract_date(p_attr,'BEGIN_DT', true);
  l_dt_end timestamp := jofl.jofl_pkg$extract_date(p_attr,'END_DT', true);
begin

  open p_rows for
WITH  a  AS(
    SELECT
      bsd.tr_id
      ,val
      ,event_time
      ,packet_id
      ,lower(u2t.sys_period * s2u.sys_period) as start_time_stamp
    FROM snsr.binary_sensor_data bsd
      join hist.v_unit2tr_hist u2t on u2t.tr_id =  bsd.tr_id and u2t.sys_period @> bsd.event_time::TIMESTAMPTZ
      join hist.v_sensor2unit_hist s2u on u2t.unit_id = s2u.unit_id and s2u.sensor_id = bsd.sensor_id and s2u.sys_period @> bsd.event_time::TIMESTAMPTZ and s2u.sys_period && u2t.sys_period
    where equipment_type_id = core.jf_equipment_type_pkg$cn_type_skah()
          and event_time between l_dt_begin and l_dt_end
          and (
            bsd.tr_id in (select tr_id from core.tr where garage_num = l_garage_num)
            or
            l_garage_num is null
          )
)
  ,b AS (
    SELECT
      tr_id
      ,val
      ,start_time_stamp
      ,event_time
      ,row_number() OVER (ORDER BY tr_id, event_time, packet_id) - row_number() OVER (PARTITION BY tr_id, val ORDER BY tr_id, event_time, packet_id) gr_id
    FROM a
)
  ,c as (
    select
      (event_time + interval '3 hours')::date dt,
      tr_id,
      val,
      case max(event_time)  when min(event_time) then 1 else ceil (extract(EPOCH FROM (max(event_time)  - min(event_time)))::decimal/60) end as minutes
    from b
    where val
    GROUP BY
      (event_time + interval '3 hours')::date,
      tr_id,
      gr_id,
      val
   -- having max(event_time) > min(event_time)
),
  aData as (
    select
      dt,
      depo2territory.depo_id,
      sum(minutes) minutes,
      count(*) cnt,
      entity.name_full
    from c join core.tr on c.tr_id = tr.tr_id
      join core.territory on tr.territory_id = territory.territory_id
      join core.depo2territory on territory.territory_id = depo2territory.territory_id and dt between depo2territory.dt_begin and coalesce(depo2territory.dt_end, now())
      join core.entity on entity.entity_id = depo2territory.depo_id
    where val
    GROUP BY dt, depo2territory.depo_id, entity.name_full
    )
   select  * from adata
  ;
end;
$function$
;