CREATE OR REPLACE FUNCTION snsr.jf_skah_registry_total_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  f_bgn_dt			timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_bgn_DT', true);
  l_trolley_type 	core.tr_type.tr_type_id%type;
  l_rf_depo_id   	core.depo.depo_id%type		:= jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', true);
  l_view_route_var 	int := jofl.jofl_pkg$extract_number(p_attr, 'F_VIEW_INPLACE_K', true)::INT;
begin
  l_trolley_type := core.tr_type_pkg$tb();
 open p_rows for
    with period_data as
     (
         select 
           dt,
           depo_id,
           sum(val_int_01) as duration,
           count(1) as cnt,
           count(distinct tr_id) as cnt_tr
         from wh.wh_core whc
         where whc.wh_type_id = wh.wh_core_process_pkg$cn_skah_period()
               and whc.tr_type_id = l_trolley_type
               and whc.dt = f_bgn_dt
               and (whc.depo_id = l_rf_depo_id or l_rf_depo_id is null)
               and (coalesce(l_view_route_var,0)=0 or (l_view_route_var=1 and  whc.route_variant_id is not null) or (l_view_route_var=-1 and  whc.route_variant_id is null ))
         group by depo_id, dt
     ),
     tr_depo as (
        select depo.depo_id,
               count(tr.tr_id) tr_cnt
          from core.tr tr 
          join core.depo depo on depo.depo_id = tr.depo_id
          where tr.tr_type_id = l_trolley_type 
          group by depo.depo_id 
     )
     select 
          pd.dt,
          ent.name_full as depo_name,
          tr_depo.tr_cnt as trs_cnt,
          count(distinct tr.tr_id) as sah_cnt,
          tr_depo.tr_cnt - count(distinct tr.tr_id) as not_sah_cnt,
          round(extract (epoch from to_timestamp(pd.duration*60))) as duration,
          pd.cnt,
          pd.cnt_tr
          from core.tr tr 
          join core.depo depo on depo.depo_id = tr.depo_id
          join core.entity ent on ent.entity_id = depo.depo_id
          join core.sensor2tr s2tr on s2tr.tr_id = tr.tr_id
          join core.sensor s on s2tr.sensor_id = s.sensor_id
          join core.equipment eq on s.sensor_id = eq.equipment_id
          join core.equipment_model em on eq.equipment_model_id = em.equipment_model_id 
            						  and em.equipment_type_id = core.jf_equipment_type_pkg$cn_type_skah()
          join core.sensor2unit s2u on s.sensor_id = s2u.sensor_id 
          join tr_depo on depo.depo_id = tr_depo.depo_id
          left join period_data pd on tr.depo_id = pd.depo_id
        where tr.tr_type_id = l_trolley_type
          and (depo.depo_id = l_rf_depo_id or l_rf_depo_id is null)
	   group by pd.dt,
                ent.name_full,     
                pd.cnt_tr,
                pd.duration,
                pd.cnt,
                tr_depo.tr_cnt
        order by ent.name_full
        ;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;