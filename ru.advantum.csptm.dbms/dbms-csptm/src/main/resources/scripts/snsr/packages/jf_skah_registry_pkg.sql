CREATE OR REPLACE FUNCTION snsr.jf_skah_registry_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
	l_trolley_type 		core.tr_type.tr_type_id%type := core.tr_type_pkg$tb();
	f_bgn_dt			timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_bgn_DT', true);
	larr_rf_depo_id   	bigint []	:= jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', true);
	l_view_route_var 	int := jofl.jofl_pkg$extract_number(p_attr, 'F_VIEW_INPLACE_K', true)::INT;
	l_route_id_array 	text [] := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_route_muid', true);
begin

 open p_rows for
 with period_data as
     (
         select whc.tr_id,
                whc.unit_id,
                sum(val_int_01) as duration,
                count(whc.tr_id) as cnt
         from wh.wh_core whc
         where whc.wh_type_id = wh.wh_core_process_pkg$cn_skah_period()
               and whc.tr_type_id = l_trolley_type
               and whc.dt = f_bgn_dt
               and (whc.depo_id = any(larr_rf_depo_id) or array_length(larr_rf_depo_id, 1) is null)
         group by tr_id, 
                  whc.unit_id
     )
     ,cnt_data as
     (
         select whc.tr_id,
                whc.route_variant_id,
                whc.unit_id,
                sum(val_int_01) as cnt
         from wh.wh_core whc
         where whc.wh_type_id = wh.wh_core_process_pkg$cn_skah_cnt()
               and whc.tr_type_id = l_trolley_type
               and whc.dt = f_bgn_dt
               and (whc.depo_id = any(larr_rf_depo_id) or array_length(larr_rf_depo_id, 1) is null)
               and (coalesce(l_view_route_var, 0) = 0 
                     or (l_view_route_var = 1 and  whc.route_variant_id is not null)
                     or (l_view_route_var = -1 and  whc.route_variant_id is null)
               		)
         group by tr_id, 
         		  whc.route_variant_id,
                  whc.unit_id
     )
     
     , tr_with_sakh as (
          select tr.tr_id
          		,u.serial_num
            from core.tr tr 
            join core.sensor2tr s2tr on s2tr.tr_id = tr.tr_id
            join core.sensor s on s2tr.sensor_id = s.sensor_id
            join core.equipment eq on s.sensor_id = eq.equipment_id
            join core.equipment_model em on eq.equipment_model_id = em.equipment_model_id 
               						    and em.equipment_type_id = core.jf_equipment_type_pkg$cn_type_skah()
            join core.sensor2unit s2u on s.sensor_id = s2u.sensor_id 
            join core.equipment u on u.equipment_id = s2u.unit_id 
            where (tr.depo_id = any(larr_rf_depo_id) or array_length(larr_rf_depo_id, 1) is null)
     )
    select
      tr.tr_id,
      tr.garage_num,
      tr.licence,
      ent.name_full as depo_name,
      tm.name as model,
      CASE tr_sakh.tr_id is null WHEN true THEN 0 ELSE 1 END has_sah,
      coalesce(r.number , '-') as route_variant_num,
      round(extract (epoch from to_timestamp(pd.duration*60))) as duration,
      --cd.cnt,
      pd.cnt as cnt,
      '[' || json_build_object('CAPTION', 'Общие сведения', 'JUMP',
                               json_build_object('METHOD', 'snsr.skah_rep_01','ATTR',
                                                 json_build_object('BEGIN_DT', to_char(f_bgn_dt::date,'yyyy-mm-dd hh24:mi:ss'), 'END_DT', to_char((f_bgn_dt::date + 1- interval '1 sec'),'yyyy-mm-dd hh24:mi:ss'), 'GARAGE_NUM_TEXT', tr.garage_num::text)::text
                               )) || ']' as jump_skah_rep_01,
      tr_sakh.serial_num serial_num
    from  core.tr tr 
      join core.tr_model tm on tm.tr_model_id = tr.tr_model_id
      --join core.tr_status ts on ts.tr_status_id = tr.tr_status_id
      join core.depo depo on depo.depo_id =tr.depo_id
      join core.entity ent on ent.entity_id = depo.depo_id
      left join tr_with_sakh tr_sakh on tr.tr_id = tr_sakh.tr_id
      left join period_data pd on pd.tr_id= tr.tr_id
      left join cnt_data cd on cd.tr_id= tr.tr_id /*and pd.unit_id = cd.unit_id*/
      left join core.territory on territory.territory_id = tr.territory_id
      left join core.entity on entity.entity_id = territory.territory_id
      left join gis.route_variants rv on rv.muid = cd.route_variant_id
      left join gis.routes r on r.muid= rv.route_muid          
    where tr.tr_type_id = l_trolley_type   
      and (tr.depo_id = any(larr_rf_depo_id) or array_length(larr_rf_depo_id, 1) is null ) 
      and (r.muid::text = any(l_route_id_array)  or array_length(l_route_id_array, 1)  is null)
      and (coalesce(l_view_route_var, 0) = 0 
           or (l_view_route_var =  1 and  rv.muid is not null)
           or (l_view_route_var = -1 and  rv.muid is null)
           )
    order by ent.name_full, tr.garage_num
    ;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;