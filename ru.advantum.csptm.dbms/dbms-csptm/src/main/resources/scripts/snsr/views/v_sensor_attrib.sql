--drop view  snsr.v_sensor_attrib;

create or replace view snsr.v_sensor_attrib
as
select
  tr.garage_num
  ,tm.name tr_model_name
  ,depo_entity.name_short depo_name
  ,terr_entity.name_full territory_name
  ,em.name sensor_model_name
  ,uem.name as equipment_model_name
  ,uet.serial_num unit_serial_num
  ,e.serial_num sensor_serial_num
  ,et.equipment_type_id
  ,d.depo_id
  ,tr.tr_id
  ,s.sensor_id
  ,u2t.unit_id
  ,u2t.sys_period as unit2tr_sys_period
  ,s2u.sys_period as sensor2unit_hist_sys_period
  ,u2t.sys_period * s2u.sys_period link_period
  ,depo_entity.name_full as depo_name_full
  ,tr.tr_type_id
  ,tr.territory_id
from core.equipment_type et
  join core.equipment_model em on et.equipment_type_id = em.equipment_type_id
  join core.equipment e on em.equipment_model_id = e.equipment_model_id
  join core.sensor s on e.equipment_id = s.sensor_id
  join hist.v_sensor2unit_hist s2u on s.sensor_id = s2u.sensor_id
  join hist.v_unit2tr_hist u2t on u2t.unit_id = s2u.unit_id
  join core.tr on u2t.tr_id = tr.tr_id
  join core.depo d on e.depo_id = d.depo_id
  JOIN core.tr_model tm on tr.tr_model_id = tm.tr_model_id
  join core.carrier cr on d.carrier_id = cr.carrier_id
  join core.entity depo_entity on d.depo_id = depo_entity.entity_id
  left join core.entity terr_entity on tr.territory_id = terr_entity.entity_id
  join core.unit u on u2t.unit_id = u.unit_id
  join core.equipment uet on uet.equipment_id = u.unit_id
  JOIN core.equipment_model uem ON uem.equipment_model_id = uet.equipment_model_id
where u2t.sys_period && s2u.sys_period;

comment on view snsr.v_sensor_attrib is 'Полная атрибутика сенсора';
comment on column snsr.v_sensor_attrib.garage_num is 'Гаражный номер';
comment on column snsr.v_sensor_attrib.tr_model_name is 'Модель транспортного средства';
comment on column snsr.v_sensor_attrib.depo_name is 'Наименование траспортного предприятия';
comment on column snsr.v_sensor_attrib.territory_name is 'Наименование территории';
comment on column snsr.v_sensor_attrib.sensor_model_name is 'Наименование модели сенсора';
comment on column snsr.v_sensor_attrib.equipment_model_name is 'Наименование модели блока';
comment on column snsr.v_sensor_attrib.unit_serial_num is 'Серийный номер блока';
comment on column snsr.v_sensor_attrib.sensor_serial_num is 'Серийный номер сенсора';
comment on column snsr.v_sensor_attrib.equipment_type_id is 'Идентификатор типа оборудования';
comment on column snsr.v_sensor_attrib.depo_id is 'Идентификатор траспортного предприятия';
comment on column snsr.v_sensor_attrib.tr_id is 'Идентификатор ТС';
comment on column snsr.v_sensor_attrib.sensor_id is 'Идентификатор сенсора';
comment on column snsr.v_sensor_attrib.unit_id is 'Идентификатор блока';
comment on column snsr.v_sensor_attrib.unit2tr_sys_period is 'Период установки блока на ТС';
comment on column snsr.v_sensor_attrib.sensor2unit_hist_sys_period is 'Период установки сенсора на блоке';
comment on column snsr.v_sensor_attrib.link_period is 'Период устаноски сенсора на блоке на ТС';
comment on column snsr.v_sensor_attrib.depo_name_full is '';
comment on column snsr.v_sensor_attrib.tr_type_id is 'Идентификатор типа ТС';
comment on column snsr.v_sensor_attrib.territory_id is 'Идентификатор территории';