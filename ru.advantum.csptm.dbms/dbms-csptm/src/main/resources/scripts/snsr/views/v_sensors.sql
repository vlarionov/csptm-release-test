drop view snsr.v_sensors;

create or replace view snsr.v_sensors
as
select distinct u2tr.unit_id, s2u.sensor_num, s.sensor_id, em.equipment_type_id, u2tr.tr_id
from core.equipment e
join core.sensor s on e.equipment_id = s.sensor_id
join core.sensor2unit s2u on s.sensor_id = s2u.sensor_id
join core.unit2tr u2tr on s2u.unit_id = u2tr.unit_id
join core.equipment_model em on e.equipment_model_id = em.equipment_model_id ;


comment on view snsr.v_sensors is 'Перечень сенсоров для сервиса обработки датчиков';
comment on column snsr.v_sensors.unit_id is 'Идентификатор блока';
comment on column snsr.v_sensors.sensor_num is 'Уникальный номер блока';
comment on column snsr.v_sensors.sensor_id is 'Идентификатор сенсора';
comment on column snsr.v_sensors.equipment_type_id is 'Идентификатор типа сенсора';
comment on column snsr.v_sensors.tr_id is 'Идентификатор ТС';