create schema snsr;

create tablespace snsr_data location '/u00/postgres/pg_data/snsr/snsr_data';

create tablespace snsr_idx location  '/u00/postgres/pg_data/snsr/snsr_idx';