CREATE OR REPLACE FUNCTION aissc."jf_emergency_type_pkg$attr_to_rowtype"(p_attr text)
 RETURNS aissc.emergency_type
 LANGUAGE plpgsql
AS $function$
declare
   l_r aissc.emergency_type%rowtype;
begin
   l_r.title := jofl.jofl_pkg$extract_varchar(p_attr, 'title', true);
   l_r.emergency_type_id := jofl.jofl_pkg$extract_number(p_attr, 'emergency_type_id', true);

   return l_r;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION aissc."jf_emergency_type_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare
begin
 open p_rows for
      select
        emergency_type_id,
        title
      from aissc.emergency_type et
	  order by et.emergency_type_id;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION  aissc."jf_emergency_type_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
   l_r aissc.emergency_type%rowtype;
begin
   l_r := aissc.jf_emergency_type_pkg$attr_to_rowtype(p_attr);

   update aissc.emergency_type set
          title = l_r.title
   where
          emergency_type_id = l_r.emergency_type_id;

   return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION aissc."jf_emergency_type_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
   l_r aissc.emergency_type%rowtype;
begin
   l_r := aissc.jf_emergency_type_pkg$attr_to_rowtype(p_attr);

   delete from  aissc.emergency_type where  emergency_type_id = l_r.emergency_type_id;

   return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION aissc."jf_emergency_type_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
  l_r aissc.emergency_type%rowtype;
begin
  l_r := aissc.jf_emergency_type_pkg$attr_to_rowtype(p_attr);
  insert into aissc.emergency_type(emergency_type_id, title)
  values (l_r.emergency_type_id, l_r.title);

  return null;
end;
$function$
;