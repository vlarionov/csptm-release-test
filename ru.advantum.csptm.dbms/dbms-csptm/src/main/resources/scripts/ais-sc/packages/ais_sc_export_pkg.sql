-- ========================================================================
-- Пакет выгрузки данных из АИС СЦ сервиса КСУПТ-а
-- ========================================================================

create or replace function aissc.ais_sc_export_pkg$export_table_event()
  returns setof aissc.table_event
  -- ========================================================================
  -- Экспорт данных о событиях
  -- ========================================================================
as $$
begin
  return query select *
               from aissc.table_event;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_export_pkg$export_route_switch_done
  ( v_id in bigint )
  returns setof aissc.route_switch_done
  -- ========================================================================
  -- Экспорт данных о закрытых переключениях маршрутов
  -- ========================================================================
as $$
begin
return query select *
             from aissc.route_switch_done
             where id = v_id;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_export_pkg$export_route()
  returns setof aissc.route
  -- ========================================================================
  -- Экспорт данных о маршрутах
  -- ========================================================================
as $$
begin
  return query select *
               from aissc.route;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_export_pkg$export_exit()
  returns setof aissc.exit
  -- ========================================================================
  -- Экспорт данных о выходах
  -- ========================================================================
as $$
begin
  return query select *
               from aissc.exit;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_export_pkg$export_trip()
  returns setof aissc.trip
  -- ========================================================================
  -- Экспорт данных о рейсах
  -- ========================================================================
as $$
begin
  return query select *
               from aissc.trip;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_export_pkg$export_trip_stop()
  returns setof aissc.trip_stop
  -- ========================================================================
  -- Экспорт данных о рейсах
  -- ========================================================================
as $$
begin
  return query select *
               from aissc.trip_stop;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_export_pkg$export_order()
  returns setof aissc.order
  -- ========================================================================
  -- Экспорт данных о нарядах
  -- ========================================================================
as $$
begin
  return query select *
               from aissc.order;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_export_pkg$export_change()
  returns setof aissc.change
  -- ========================================================================
  -- Экспорт данных об изменениях в БД
  -- ========================================================================
as $$
begin
  return query select *
               from aissc.change;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_export_pkg$export_ts()
  returns setof aissc.ts
  -- ========================================================================
  -- Экспорт справочника транспортных средств
  -- ========================================================================
as $$
begin
  return query select *
               from aissc.ts;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_export_pkg$export_capacity()
  returns setof aissc.capacity
  -- ========================================================================
  -- Экспорт справочника вместимостей
  -- ========================================================================
as $$
begin
  return query select *
               from aissc.capacity;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_export_pkg$export_mark()
  returns setof aissc.mark
  -- ========================================================================
  -- Экспорт справочника марок
  -- ========================================================================
as $$
begin
  return query select *
               from aissc.mark;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_export_pkg$export_model()
  returns setof aissc.model
  -- ========================================================================
  -- Экспорт справочника моделей
  -- ========================================================================
as $$
begin
  return query select *
               from aissc.model;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_export_pkg$export_status()
  returns setof aissc.status
  -- ========================================================================
  -- Экспорт справочника статусов
  -- ========================================================================
as $$
begin
  return query select *
               from aissc.status;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_export_pkg$export_contragent()
  returns setof aissc.contragent
  -- ========================================================================
  -- Экспорт справочника организаций
  -- ========================================================================
as $$
begin
  return query select *
               from aissc.contragent;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_export_pkg$export_contragent_type()
  returns setof aissc.contragent_type
  -- ========================================================================
  -- Экспорт справочника типов организаций
  -- ========================================================================
as $$
begin
  return query select *
               from aissc.contragent_type;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_export_pkg$export_territory_department()
  returns setof aissc.territory_department
  -- ========================================================================
  -- Экспорт справочника территориальных подразделений
  -- ========================================================================
as $$
begin
  return query select *
               from aissc.territory_department;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_export_pkg$export_employee()
  returns setof aissc.employee
  -- ========================================================================
  -- Экспорт справочника работников
  -- ========================================================================
as $$
begin
  return query select *
               from aissc.employee;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_export_pkg$export_ts_block()
  returns setof aissc.ts_block
  -- ========================================================================
  -- Экспорт справочника привязки блока к ТС
  -- ========================================================================
as $$
begin
  return query select *
               from aissc.ts_block;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_export_pkg$export_block()
  returns setof aissc.block
  -- ========================================================================
  -- Экспорт справочника блоков
  -- ========================================================================
as $$
begin
  return query select *
               from aissc.block;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_export_pkg$export_block_type()
  returns setof aissc.block_type
  -- ========================================================================
  -- Экспорт справочника типов блоков
  -- ========================================================================
as $$
begin
  return query select *
               from aissc.block_type;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_export_pkg$export_route_type()
  returns setof aissc.route_type
  -- ========================================================================
  -- Экспорт справочника типов маршрутов
  -- ========================================================================
as $$
begin
  return query select *
               from aissc.route_type;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_export_pkg$export_race_type()
  returns setof aissc.race_type
  -- ========================================================================
  -- Экспорт справочника типов рейсов
  -- ========================================================================
as $$
begin
  return query select *
               from aissc.race_type;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_export_pkg$export_event_type()
  returns setof aissc.event_type
  -- ========================================================================
  -- Экспорт справочника типов событий
  -- ========================================================================
as $$
begin
  return query select *
               from aissc.event_type;
end;
$$ language plpgsql;