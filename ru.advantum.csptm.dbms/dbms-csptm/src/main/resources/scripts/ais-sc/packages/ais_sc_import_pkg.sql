-- ========================================================================
-- Пакет загрузки данных в АИС СЦ сервис КСУПТ-а
-- ========================================================================

create or replace function aissc.ais_sc_import_pkg$import_emergency_situation
  ( v_emergency_time in date,
    v_emergency_type in text,
    v_emergency_lon in double precision,
    v_emergency_lat in double precision,
    v_danger_level in text,
    v_vehicles in text,
    v_description in text )
  returns integer
  -- ========================================================================
  -- Импорт данных о нештатной ситуации от пользователя
  -- ========================================================================
as $$ declare
  v_id integer;
begin
  insert into aissc.emergency_situation (emergency_time, emergency_type, emergency_place, danger_level, vehicles, description)
  values (v_emergency_time, v_emergency_type, point(v_emergency_lon, v_emergency_lat)::geometry, v_danger_level, v_vehicles, v_description)
  returning emergency_situation_id into v_id;

  return v_id;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_import_pkg$import_emergency_situation_external
  ( v_es_external_id in bigint,
    v_emergency_type in bigint )
  returns integer
  -- ========================================================================
  -- Импорт данных о нештатной ситуации из внешней системы
  -- ========================================================================
as $$ declare
  v_id integer;
begin
  insert into aissc.emergency_situation_external (emergency_situation_external_id, emergency_type)
  values (v_es_external_id, v_emergency_type)
  returning emergency_situation_external_id into v_id;

  return v_id;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_import_pkg$import_emergency_type
  ( v_emergency_type_id in bigint,
    v_title in text )
  returns integer
  -- ========================================================================
  -- Импорт классификатора нештатных ситуаций
  -- ========================================================================
as $$ declare
  v_id integer;
begin
  insert into aissc.emergency_type (emergency_type_id, title)
  values (v_emergency_type_id, v_title) on conflict (emergency_type_id) do
            update set title = EXCLUDED.title returning emergency_type_id into v_id;

  return v_id;
end;
$$ language plpgsql;


create or replace function aissc.ais_sc_import_pkg$import_route_switch
  ( v_route_switch_id in bigint,
    v_user_id in bigint,
    v_vehicles in text,
    v_target_place_id in integer,
    v_start_time in date,
    v_end_time in date,
    v_route_id in integer,
    v_direction in text )
  returns integer
  -- ========================================================================
  -- Импорт данных о переключении маршрута
  -- ========================================================================
as $$ declare
  v_id integer;
begin
  insert into aissc.route_switch (route_switch_id, user_id, vehicles, target_place_id, start_time, end_time, route_id, direction)
  values (v_route_switch_id, v_user_id, v_vehicles, v_target_place_id, v_start_time, v_end_time, v_route_id, v_direction)
  returning route_switch_id into v_id;

  return v_id;
end;
$$ language plpgsql;