SELECT
  n.nspname                   AS schemaname,
  c.relname                   AS tablename,
  pg_get_userbyid(c.relowner) AS tableowner,
  t.spcname                   AS tablespace,
  obj_description(c.OID) tbl_comment
FROM pg_class c
  LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
  LEFT JOIN pg_tablespace t ON t.oid = c.reltablespace
WHERE (c.relkind = 'r' :: "char")
      AND obj_description(c.OID) IS NULL
      AND NOT exists(SELECT 1
                     FROM pg_inherits
                     WHERE (inhrelid = c.oid))
      AND n.nspname IN ('adm',
  'aissc',
  'asd',
  'askp',
  'asupbk',
  'cmnd',
  'core',
  'crep',
  'cron',
  'easu',
  'evt',
  'gis',
  'hist',
  'ibrd',
  'jofl',
  'jrep',
  'kdbo',
  'mnt',
  'oud',
  'paa',
  'rnis',
                        'rts',
                        'snsr',
                        'tt',
                        'ttb',
                        'udump',
                        'usw',
                        'voip',
                        'wh',
                        'yandex'
)
ORDER BY 1, 2;
