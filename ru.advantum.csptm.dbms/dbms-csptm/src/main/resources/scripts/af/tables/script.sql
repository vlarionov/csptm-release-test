[postgres@mgt-pg01 ~]$ mkdir /u00/postgres/pg_data/af_idx

[postgres@mgt-pg01 ~]$ mkdir /u00/postgres/pg_data/af_data

CREATE TABLESPACE af_data OWNER adv LOCATION '/u00/postgres/pg_data/af_data';

CREATE TABLESPACE af_idx OWNER adv LOCATION '/u00/postgres/pg_data/af_idx';

CREATE SCHEMA af;

-- Table af.tt_action_af


CREATE TABLE af.tt_action_fact_af(
 tt_action_fact_id Bigint NOT NULL,
 is_manual_fill Boolean DEFAULT false NOT NULL
)
TABLESPACE af_data
;

COMMENT ON TABLE af.tt_action_fact_af IS 'http://redmine.advantum.ru/issues/23979

В форму Отметки выхода необходимо добавить признак "Заполнено вручную".
Заполнять этот признак для тех строк рейса, которые заполнены в результате действия "Заполнить факт прохождения".'
;
COMMENT ON COLUMN af.tt_action_fact_af.is_manual_fill IS 'Заполнено вручную'
;

-- Add keys for table af.tt_action_fact_af

ALTER TABLE af.tt_action_fact_af ADD CONSTRAINT pk_tt_action_af PRIMARY KEY (tt_action_fact_id)
 USING INDEX TABLESPACE af_idx
;

ALTER TABLE af.tt_action_fact_af ADD CONSTRAINT fk_tt_action_fact_to_tt_action_af FOREIGN KEY (tt_action_fact_id) REFERENCES ttb.tt_action_fact (tt_action_fact_id) ON DELETE CASCADE ON UPDATE NO ACTION
;
