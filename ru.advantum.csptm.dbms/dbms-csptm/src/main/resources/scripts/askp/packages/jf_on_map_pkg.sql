CREATE or replace FUNCTION askp.jf_on_map_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
LANGUAGE plpgsql
AS $$

declare
  --f_stop_place_muid BIGINT 	:= jofl.jofl_pkg$extract_number(p_attr, 'sp_muid', true);
  l_trip_pairs_agg_id BIGINT 	:= jofl.jofl_pkg$extract_number(p_attr, 'trip_pairs_agg_id', true);
  -- массив начальных остановок
  l_sp_muid_arr BIGINT[] := array(select stop_place_id from askp.trip_pairs_start_sp where trip_pairs_agg_id = l_trip_pairs_agg_id);
  l_temp_arr BIGINT[] :=  l_sp_muid_arr;
  f_dt_start timestamp 	:= jofl.jofl_pkg$extract_date(p_attr, 'd_from', true);
  f_dt_end	 timestamp	:= jofl.jofl_pkg$extract_date(p_attr, 'd_to', true);
  l_amount INTEGER := jofl.jofl_pkg$extract_number(p_attr, 'trip_number', true);

  l_dow TEXT := jofl.jofl_pkg$extract_number(p_attr, 'dow', true);
  -- массив цифр, типа {2,5}
  l_dow_arr SMALLINT[] := array(select DISTINCT unnest((regexp_matches(l_dow, '\d{1}', 'g'))));

  l_sp_muid_rts TEXT;
begin
  -- остановки связанные с начальными
  l_sp_muid_arr = array(select DISTINCT muid from luchininov.connected_stop_places2 where stop_places_muid = ANY(l_sp_muid_arr));
  l_sp_muid_arr = array_cat(l_sp_muid_arr, l_temp_arr); -- еще сами остановки

  l_sp_muid_rts = '["'|| array_to_string(array(select stop_item_id from rts.stop_item2gis i2g where i2g.stop_place_muid = ANY(l_sp_muid_arr)), '", "')||'"]';
  open p_rows for
  WITH sd AS( SELECT start_sp_muid, end_sp_muid, count(end_sp_muid), (select name from gis.stops where muid = (select stop_muid from gis.stop_places where muid = end_sp_muid) )
              from askp.end_station
              where start_sp_muid = ANY(l_sp_muid_arr)
              --AND end_sp_muid <> l_sp_muid_arr[1]--f_stop_place_muid
              AND check_time >= f_dt_start AND check_time <= f_dt_end    -- фильтр по дате
              AND (EXTRACT(ISODOW FROM check_time) = ANY(l_dow_arr) OR l_dow IS NULL)-- фильтр по дню недели
              GROUP BY end_sp_muid, start_sp_muid order by count(end_sp_muid) desc /*limit 25*/),
      --select * from sd;
      b AS (select distinct on (sp1, sp2) t1.route_trajectory_muid ,t1.stop_place_muid sp1, t1.order_num on1, t2.stop_place_muid sp2, t2.order_num on2
            from gis.stop_place2route_traj t1
              JOIN gis.stop_place2route_traj t2 on t1.route_trajectory_muid = t2.route_trajectory_muid
              JOIN sd ON t1.stop_place_muid = sd.end_sp_muid
                   AND t1.sign_deleted = 0 AND t2.sign_deleted = 0
                   AND t2.stop_place_muid = sd.start_sp_muid--2993277929106909976/*l_sp_muid_arr[0]*/--f_stop_place_muid----2993277851146111717--2993277937696844584
                   AND (sd.count > l_amount OR l_amount IS NULL)
      ),
      --select * from b;
      f AS (
        select b.route_trajectory_muid, sp1 end_sp, sp2 start_sp, t5.stop_place_muid, t5.order_num, sd.count  from b
          JOIN gis.stop_place2route_traj t5
            ON t5.route_trajectory_muid = b.route_trajectory_muid
          JOIN sd ON sd.end_sp_muid = b.sp1 AND sd.start_sp_muid = sp2
          AND (/*t5.order_num BETWEEN b.on1 AND b.on2 OR*/ t5.order_num BETWEEN b.on2 AND b.on1)
    ),
    --select * from f;
  fr AS (select *, lead(stop_place_muid) OVER (PARTITION BY start_sp, end_sp ORDER BY order_num) from f),
  result AS (select distinct on (stop_place_muid, next_stop) end_sp, route_trajectory_muid as trajectory_muid, order_num::text,
  stop_place_muid, lead as next_stop,
  (SELECT SUM(count) from fr t1 where t1.stop_place_muid = t2.stop_place_muid AND t1.lead = t2.lead ) as amount--, '["'||array_to_string(l_sp_muid_arr, '", "')||'"]' as start_points
  from fr t2 where lead IS NOT NULL AND stop_place_muid <> lead),
  tr_type2route AS(
      select distinct on (si2r.round_id) si2r.round_id, tr_type_id, result.trajectory_muid
      from rts.stop_item2round si2r
      JOIN rts.stop_item si ON si.stop_item_id = si2r.stop_item_id
      JOIN rts.round2gis i2g ON i2g.round_id = si2r.round_id --result.trajectory_muid
      JOIN result ON result.trajectory_muid = i2g.route_trajectory_muid
  )
  select amount,
         (select stop_item_id from rts.stop_item2gis i2g where i2g.stop_place_muid = result.end_sp AND tr_type_id = tr_type2route.tr_type_id ) as end_sp,
         (select stop_item_id from rts.stop_item2gis i2g where i2g.stop_place_muid = result.next_stop AND tr_type_id = tr_type2route.tr_type_id) as next_stop,
          order_num,
         (select stop_item_id from rts.stop_item2gis i2g where i2g.stop_place_muid = result.stop_place_muid AND tr_type_id = tr_type2route.tr_type_id) as stop_place_muid,
          l_sp_muid_rts as start_points,
          tr_type2route.round_id as trajectory_muid
  from result
  JOIN tr_type2route ON tr_type2route.trajectory_muid = result.trajectory_muid;
end;
$$;