CREATE OR REPLACE FUNCTION askp."jf_route_stops_pkg$of_rows"(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  f_dt_start 			timestamp 	:= jofl.jofl_pkg$extract_date(p_attr, 'BGN_DT', true);
  f_dt_end	 			timestamp	:= jofl.jofl_pkg$extract_date(p_attr, 'END_DT', true);
BEGIN
  OPEN p_rows FOR
  /*select r.number, stops.name, rtt.name as direction
  from gis.routes r
    JOIN gis.route_variants rv ON rv.route_muid = r.muid
                                  AND rv.start_date < now() AND (rv.end_date > now() OR rv.end_date IS NULL)
                                  AND r.sign_deleted = 0
    JOIN askp.askp_to_gis_route askp2gis ON askp2gis.muid = rv.muid
    JOIN gis.route_rounds rr ON rr.route_variant_muid = rv.muid AND rr.sign_deleted = 0 AND rr.code = '00' -- основной
    JOIN gis.route_trajectories rt ON rr.muid = rt.route_round_muid AND rt.sign_deleted = 0
    JOIN gis.stop_place2route_traj s2t ON rt.muid = s2t.route_trajectory_muid
    JOIN gis.stop_places sp ON s2t.stop_place_muid = sp.muid AND sp.sign_deleted = 0
    JOIN gis.stops ON sp.stop_muid = stops.muid AND stops.sign_deleted = 0
    JOIN gis.ref_route_trajectory_types rtt ON rt.trajectory_type_muid = rtt.muid
  ORDER BY r.number, rtt.name;*/
  with a AS(select r.number, stops.name, rtt.name as direction, sp.muid, rtt.muid dir_id, stops.muid stop_id
            from gis.routes r
              JOIN gis.route_variants rv ON rv.route_muid = r.muid
                                            AND rv.start_date < now() AND (rv.end_date > now() OR rv.end_date IS NULL)
                                            AND r.sign_deleted = 0
              JOIN askp.askp_to_gis_route askp2gis ON askp2gis.muid = rv.muid
              JOIN gis.route_rounds rr ON rr.route_variant_muid = rv.muid AND rr.sign_deleted = 0 AND rr.code = '00' -- основной
              JOIN gis.route_trajectories rt ON rr.muid = rt.route_round_muid AND rt.sign_deleted = 0
              JOIN gis.stop_place2route_traj s2t ON rt.muid = s2t.route_trajectory_muid
              JOIN gis.stop_places sp ON s2t.stop_place_muid = sp.muid AND sp.sign_deleted = 0
              JOIN gis.stops ON sp.stop_muid = stops.muid AND stops.sign_deleted = 0
              JOIN gis.ref_route_trajectory_types rtt ON rt.trajectory_type_muid = rtt.muid
            -- where r.number = '267'
            ORDER BY r.number, rtt.name)
  select distinct number, name, direction, row_to_json(a)::TEXT as ALL_param, rank() OVER (ORDER BY number, name, direction) as id from a
  order by id;
END;
$$;