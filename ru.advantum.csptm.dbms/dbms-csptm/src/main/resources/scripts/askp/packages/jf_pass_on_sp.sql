create or REPLACE function askp."jf_pass_on_sp_pkg$attr_to_rowtype"(p_attr text) returns askp.pass_on_sp
LANGUAGE plpgsql
AS $$
declare
  l_r askp.pass_on_sp%rowtype;
begin
  l_r.conf_pass_on_sp_id := jofl.jofl_pkg$extract_number(p_attr, 'conf_pass_on_sp_id', true);
  l_r.pass_on_sp_id := jofl.jofl_pkg$extract_number(p_attr, 'pass_on_sp_id', true);
  l_r.route_id := jofl.jofl_pkg$extract_number(p_attr, 'route_id', true);
  l_r.direction_id := jofl.jofl_pkg$extract_varchar(p_attr, 'direction_id', true);
  l_r.pass_in := jofl.jofl_pkg$extract_number(p_attr, 'pass_in', true);
  l_r.pass_out := jofl.jofl_pkg$extract_number(p_attr, 'pass_out', true);
  l_r.fulness := jofl.jofl_pkg$extract_number(p_attr, 'fulness', true);
  l_r.load_mean := jofl.jofl_pkg$extract_number(p_attr, 'load_mean', true);
  l_r.load_max := jofl.jofl_pkg$extract_number(p_attr, 'load_max', true);

  return l_r;
end;
$$;


create or replace function askp."jf_pass_on_sp_pkg$of_delete"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.pass_on_sp%rowtype;
begin
  l_r := askp.jf_pass_on_sp_pkg$attr_to_rowtype(p_attr);

  return null;
end;
$$;


create or REPLACE function askp."jf_pass_on_sp_pkg$of_insert"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.pass_on_sp%rowtype;
begin
  l_r := askp.jf_pass_on_sp_pkg$attr_to_rowtype(p_attr);

  insert into askp.pass_on_sp select l_r.*;

  return null;
end;
$$;


create or replace function askp."jf_pass_on_sp_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
  l_conf_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'conf_pass_on_sp_id', true);
  l_sp_name TEXT;
begin
  SELECT name INTO l_sp_name from askp.conf_pass_on_sp
  JOIN gis.stop_places sp ON sp.muid = stop_place_muid
  JOIN gis.stops s ON s.muid = sp.stop_muid
  where conf_pass_on_sp_id = l_conf_id;
  open p_rows for
  select
    conf_pass_on_sp_id,
    pass_on_sp_id,
    r.route_num route_id,
    md.move_direction_name direction_id,
    pass_in,
    pass_out,
    pass_in + pass_out AS sp_work,
    fulness,
    round(load_mean::NUMERIC,0) load_mean,
    round(load_max::NUMERIC,0) load_max,
    l_sp_name sp_name,
    tt.short_name tr_type
  from askp.pass_on_sp
  JOIN rts.route r USING(route_id)
  JOIN core.tr_type tt ON tt.tr_type_id = r.tr_type_id
  JOIN rts.move_direction md ON md.move_direction_id = direction_id
  where conf_pass_on_sp_id = l_conf_id
  UNION ALL
  select
  null conf_pass_on_sp_id,
  null pass_on_sp_id,
  null route_id,
  null direction_id,
  SUM(pass_in) pass_in,
  SUM(pass_out) pass_out,
  SUM(pass_in + pass_out) AS sp_work,
  SUM(fulness) fulness,
  null load_mean,
  null load_max,
  'Итого' sp_name,
  null tr_type
  from askp.pass_on_sp
  where conf_pass_on_sp_id = l_conf_id
  GROUP BY conf_pass_on_sp_id;
end;
$$;

create or replace function askp."jf_pass_on_sp_pkg$of_update"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.pass_on_sp%rowtype;
begin
  l_r := askp.jf_pass_on_sp_pkg$attr_to_rowtype(p_attr);

  return null;
end;
$$;
