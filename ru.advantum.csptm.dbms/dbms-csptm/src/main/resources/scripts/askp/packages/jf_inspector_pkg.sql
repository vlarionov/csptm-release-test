CREATE OR REPLACE FUNCTION askp.jf_inspector_pkg$attr_to_rowtype(p_attr TEXT)
    RETURNS askp.inspector
LANGUAGE plpgsql
AS $function$
DECLARE
    l_r askp.inspector%ROWTYPE;
BEGIN
    l_r.inspector_id:= jofl.jofl_pkg$extract_number(p_attr, 'inspector_id', TRUE);
    l_r.comment:= jofl.jofl_pkg$extract_varchar(p_attr, 'comment', TRUE);
    l_r.first_name:= jofl.jofl_pkg$extract_varchar(p_attr, 'first_name', TRUE);
    l_r.last_name:= jofl.jofl_pkg$extract_varchar(p_attr, 'last_name', TRUE);
    l_r.middle_name:= jofl.jofl_pkg$extract_varchar(p_attr, 'middle_name', TRUE);
    RETURN l_r;
END;
$function$;

--------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION askp.jf_inspector_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
    RETURNS REFCURSOR
LANGUAGE plpgsql
AS $function$
DECLARE
    l_rf_db_method text:=jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
BEGIN
    OPEN p_rows FOR
    SELECT
    i.comment,
    i.middle_name,
    i.last_name,
    i.first_name,
    i.inspector_id,
        CASE WHEN l_rf_db_method='askp.visual_inspection' THEN i.last_name||' '||i.first_name||' '||i.middle_name END as full_name
    FROM ASKP.inspector i;
END;
$function$;

--------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION askp.jf_inspector_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
    RETURNS TEXT
LANGUAGE plpgsql
AS $function$
DECLARE
    l_inspector_id askp.inspector.inspector_id%TYPE :=jofl.jofl_pkg$extract_number(p_attr,
                                                                                               'inspector_id',
                                                                                               TRUE);
BEGIN
    DELETE FROM askp.inspector
    WHERE inspector_id = l_inspector_id;
    RETURN NULL;
END;
$function$;

--------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION askp.jf_inspector_pkg$of_update(p_id_account NUMERIC, p_attr TEXT)
    RETURNS TEXT
LANGUAGE plpgsql
AS $function$
DECLARE
    l_r askp.inspector%ROWTYPE;
BEGIN
    l_r :=askp.jf_inspector_pkg$attr_to_rowtype(p_attr);
    update askp.inspector
        SET
            middle_name=l_r.middle_name,
            first_name=l_r.first_name,
            last_name=l_r.last_name,
            comment=l_r.comment
    WHERE inspector_id = l_r.inspector_id;
    RETURN NULL;
END;
$function$;

--------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION askp.jf_inspector_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
    RETURNS TEXT
LANGUAGE plpgsql
AS $function$
DECLARE
    l_r askp.inspector%ROWTYPE;
BEGIN
    l_r := askp.jf_inspector_pkg$attr_to_rowtype(p_attr);
    INSERT INTO askp.inspector (first_name, last_name, middle_name, comment)
    VALUES
        (l_r.first_name,l_r.last_name,l_r.middle_name,l_r.comment);
    RETURN NULL;
END;
$function$;
