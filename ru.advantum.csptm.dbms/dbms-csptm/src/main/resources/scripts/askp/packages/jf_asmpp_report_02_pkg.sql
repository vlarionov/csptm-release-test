create or replace function askp."jf_asmpp_report_02_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
language plpgsql
as $$
DECLARE
  l_rpt_dt timestamp := jofl.jofl_pkg$extract_date(p_attr, 'REP_DT', true);
  l_end_dt timestamp := jofl.jofl_pkg$extract_date(p_attr, 'END_DT', true);
  l_start_number int := jofl.jofl_pkg$extract_number(p_attr, 'START_NUMBER', true);
  l_end_number int := jofl.jofl_pkg$extract_number(p_attr, 'END_NUMBER', true);
  l_stop_muid bigint := jofl.jofl_pkg$extract_number(p_attr, 'STOP_MUID', true);
  l_week_day_key text := replace(jofl.jofl_pkg$extract_varchar(p_attr, 'F_WEEKDAY_INPLACE_K', true), ' ', '');
  l_report_day_name text := jofl.jofl_pkg$extract_varchar(p_attr, 'F_WEEKDAY_INPLACE_V', true);

  l_start_time time;
  l_end_time time;
  l_round_normal int[] := array[1];
BEGIN
  if core.assigned(l_rpt_dt::text, l_end_dt::text, l_start_number::text, l_end_number::text, l_stop_muid::text) then
    l_start_time := case when l_start_number = 24 then '23:59:59'::time else (l_start_number::text||':00')::time end;
    l_end_time := case when l_end_number = 24 then '23:59:59'::time else (l_end_number::text||':00')::time end;
    OPEN p_rows FOR
      with d as (
                  SELECT array_agg(a.asmpp_agg_id) id_arr, count(*) day_count
                  fROM askp.asmpp_agg a
                  where a.time_start in (
                                           select core.to_utc(dt)::date
                                           from ttb.calendar_item ci join ttb.calendar c on ci.calendar_id = c.calendar_id
                                           where ci.calendar_tag_id = any(string_to_array(l_week_day_key, ',')::int[])
                                             and c.dt between core.from_utc(l_rpt_dt)::date and core.from_utc(l_end_dt)::date + 1
                                        )
                )
      select  p_attr att,
             concat(to_char(core.from_utc(l_rpt_dt)::date, 'dd.mm.yyyy'), ' - ', to_char(core.from_utc(l_end_dt)::date, 'dd.mm.yyyy')) dperiod,
             concat(to_char(l_start_time, 'hh24:mi'), ' - ', to_char(l_end_time, 'hh24:mi')) tperiod,
             l_report_day_name report_day,
             st2.name stop_name,
             r.number,
             tt.short_name tr_type,
             st.name first_stop,
             'день'::text day_type,
             avg_sum_in,
             avg_sum_out,
             avg_sum_in + avg_sum_out stop_work
      from
        (
          select q.route_variants_muid, q.stop_place_id,
                 round(sum(in_corrected)/nullif(day_count, 0))  avg_sum_in,
                 round(sum(out_corrected)/nullif(day_count, 0)) avg_sum_out
          from
            ( select t.*,
                    (select first_value(orf.stop_place_muid) over(order by orf.stop_order_num) from tt.order_fact orf where orf.order_round_id = t.order_round_id limit 1) stop_place_id
              from askp.asmpp_agg_item t
              where exists (select 1 from asd.oper_round_visit v where v.order_round_id = t.order_round_id and v.order_round_status_id = any(l_round_normal))
            ) q join d on q.asmpp_agg_id = any(d.id_arr)
          where q.stops_muid = l_stop_muid
            and q.time_start >= l_rpt_dt and q.time_start < l_end_dt + interval '1 day'
            and extract(hour from core.to_utc(q.time_start)) between l_start_number and l_end_number
          group by q.route_variants_muid, q.stop_place_id, d.day_count
        ) qq join gis.route_variants rv on qq.route_variants_muid = rv.muid
             join gis.stop_places stp on qq.stop_place_id = stp.muid
             join gis.routes r on rv.route_muid = r.muid
             join core.tr_type tt on tt.transport_kind_muid = r.transport_kind_muid
             left join gis.stops st on st.muid = stp.stop_muid
             join gis.stops st2 on st2.muid = l_stop_muid;
  ELSE
    open p_rows for
      select null att,
             null dperiod,
             null tperiod,
             null stop_name,
             null number,
             null trans_type,
             null first_stop,
             null day_type,
             null avg_sum_in,
             null avg_sum_out,
             null stop_work;
  end if;
end;
$$;
