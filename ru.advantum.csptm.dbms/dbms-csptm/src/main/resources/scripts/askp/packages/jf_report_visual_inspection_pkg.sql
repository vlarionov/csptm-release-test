CREATE OR REPLACE FUNCTION askp.jf_report_visual_inspection_pkg(
    p_id_account           TEXT,
    p_visual_inspection_id TEXT,
    P_TIMEZONE             TEXT
)
    RETURNS TABLE(
        lt_date       TEXT,
        lt_time_from  TEXT,
        lt_time_to    TEXT,
        lt_stop_name  TEXT,
        lt_routes_num TEXT,
        lt_direction  TEXT,
        lt_full_name  TEXT,
        lt_number     TEXT
    ) AS
$body$
DECLARE
BEGIN
    RETURN QUERY
    WITH rname AS (
        SELECT
            string_agg(DISTINCT (t.short_name || ' ' || rr.route_num), ', ') AS routes_num,
            si.stop_item_id                                                  AS stop,
            sl.stop_id
        FROM rts.stop_location sl
            JOIN rts.stop_item si on si.stop_location_id = sl.stop_location_id
            JOIN rts.stop_item2round si2r ON si.stop_item_id = si2r.stop_item_id
            JOIN rts.round r ON si2r.round_id = r.round_id
            JOIN rts.route_variant rv ON r.route_variant_id = rv.route_variant_id
            JOIN rts.route rr ON rv.route_variant_id = rr.current_route_variant_id
            JOIN core.tr_type t ON rr.tr_type_id = t.tr_type_id
        WHERE rv.rv_status_id = 1 :: SMALLINT AND (NOT si2r.sign_deleted)
        GROUP BY stop
    )
    SELECT
        CASE WHEN p_timezone IS NULL OR p_timezone = ''
            THEN avi.plan_date :: DATE :: TEXT
        ELSE (avi.plan_date + (p_timezone || 'hour') :: INTERVAL) :: DATE :: TEXT END lt_date,
        div(div(extract(epoch FROM avi.plan_date::time)::numeric, 60), 60) || ' ч : ' || mod(div(extract(epoch FROM avi.plan_date::time)::numeric, 60), 60) ||
        ' мин'                                                                        lt_time_from,
        div(div(extract(epoch FROM avi.plan_date::time)::numeric, 60), 60) || ' ч : ' || mod(div(extract(epoch FROM avi.plan_date::time)::numeric, 60), 60) ||
        ' мин'                                                                        lt_time_to,
        rs.name :: TEXT                                                               lt_stop_name,
        r.routes_num :: TEXT AS                                                       lt_routes_num,
        md.move_direction_name :: TEXT                                                lt_direction,
        i.last_name||' '||i.first_name||' '||i.middle_name                            lt_full_name,
        a.lt_number :: TEXT                                                           lt_number
    FROM askp.visual_inspection avi
        join askp.inspector i on avi.inspector_id = i.inspector_id
        JOIN rname r ON r.stop = avi.stop_item_id
        LEFT JOIN rts.stop rs ON rs.stop_id = r.stop_id
        JOIN rts.move_direction md ON avi.move_direction_id = md.move_direction_id
        JOIN (SELECT generate_series(1, 20) lt_number) a ON 1 = 1
    WHERE avi.visual_inspection_id :: TEXT = p_visual_inspection_id;
END;
$body$
LANGUAGE 'plpgsql';