create or replace function askp."jf_report_avgdaily_check4vehicletype_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare 
    v_attr text;
begin
 /* добавляем параметр: тип отчета (по типу транспортного средства) */
 select p_attr::jsonb || '{"P_REPORT_TYPE": "VEHICLETYPE"}'::jsonb into v_attr;
 select askp.report_avgdaily_check(p_id_account, v_attr) into p_rows; 
end;
$$;
