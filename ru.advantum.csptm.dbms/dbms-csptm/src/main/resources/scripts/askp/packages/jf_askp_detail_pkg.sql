CREATE OR REPLACE FUNCTION askp."jf_askp_detail_pkg$of_rows"(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_tr_id         INT := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', TRUE);
  l_time_start    TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'time_start_', TRUE);
  l_order_fact_id INT := jofl.jofl_pkg$extract_number(p_attr, 'order_fact_id', TRUE);
  l_lead TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'lead', TRUE);
  l_lead_fact    BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'lead_fact', TRUE);
  l_time_error_interval INTERVAL := make_interval(secs => (30 - asd.get_constant('TIME_FACT_ERROR'))::int);
  l_shift_interval INTERVAL := INTERVAL '3 hour' - INTERVAL '30 SECOND' + l_time_error_interval;

  l_order_num BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'order_num', TRUE);
  l_lead_order_num BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'lead_order_num', TRUE);
  l_order_round_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'order_round_id', TRUE);
BEGIN
  OPEN p_rows FOR
  /*select adata.check_time, /*ticket_num,*/ticket_id as ticket_num, ticket_type_name from askp.askp_data adata
    JOIN askp.ticket_type tt ON tt.ticket_type_id = adata.ticket_type_id
    -- JOIN askp.ticket_oper t_o ON t_o.ticket_id = adata.ticket_id
    JOIN askp.asmpp_agg_item aagg ON aagg.asmpp_agg_item_id = adata.asmpp_agg_item_id
  WHERE aagg.tr_id = l_tr_id AND aagg.time_start = l_time_start + INTERVAL '3 hours';*/

  /*WITH a AS (SELECT * from askp.ticket_oper
             where check_time > (select distinct time_fact from askp.validation2stops_binding where order_fact_id = 595276971 /*l_order_fact_id*/)
             and check_time < (select distinct time_fact from askp.validation2stops_binding where order_fact_id = 595276971 /*l_order_fact_id*/) +
             INTERVAL '6 minutes')
  select * from a ;*/
  WITH a AS (
      SELECT *
      FROM askp.validation2stops_binding
      WHERE (order_num = l_order_num OR order_num = l_lead_order_num)
      AND order_round_id = l_order_round_id
  ), b AS (
      SELECT *
      FROM askp.ticket_oper
      --WHERE check_time >= (SELECT MIN(check_time) FROM a) AND check_time <= (SELECT MAX(check_time) FROM a)
      WHERE ((check_time >= l_time_start + l_shift_interval/*'2017-11-15 14:39:17.882'::TIMESTAMP*//* + INTERVAL '3 hour' - INTERVAL '30 SECOND' + l_time_error_interval*/)
            AND (check_time < l_lead + l_shift_interval/*'2017-11-15 14:41:04.14'::TIMESTAMP*/ /*+ INTERVAL '3 hours' - INTERVAL '30 SECOND' + l_time_error_interval*/))
  ),
  c AS (SELECT * FROM b WHERE askp_check_id IN (SELECT a.askp_check_id FROM a))
  select check_time/* - interval '3 hours'*/ as check_time, ticket_num, ticket_type_name from c -- - 3 часа, потому, что клиент сделает +3
  JOIN askp.ticket_type tt ON tt.ticket_type_id = c.ticket_type_id;
END;
$$;

