create or replace function askp."jf_report_hourly_check4route_avg_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
 v_attr text;
 begin
 /* добавляем параметр: тип отчета (почасовой, за период) */
 select p_attr::jsonb || '{"P_REPORT_TYPE": "PERIOD"}'::jsonb into v_attr;
 -- select askp.report_hourly_check4route(p_id_account, v_attr) into p_rows;
 select askp.report_hourly_check4route_plain (p_id_account, v_attr) into p_rows;
end;
$$;
