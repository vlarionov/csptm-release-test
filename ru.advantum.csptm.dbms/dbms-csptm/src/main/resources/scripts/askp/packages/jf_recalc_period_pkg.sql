create OR replace function askp."jf_recalc_period_pkg$attr_to_rowtype"(p_attr text) returns askp.recalc_period
LANGUAGE plpgsql
AS $$
declare
  l_r askp.recalc_period%rowtype;
begin
  l_r.recalc_period_id := jofl.jofl_pkg$extract_number(p_attr, 'recalc_period_id', true);
  l_r.time_from := jofl.jofl_pkg$extract_date(p_attr, 'time_from', true);
  l_r.time_to := jofl.jofl_pkg$extract_date(p_attr, 'time_to', true);
  l_r.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true);

  return l_r;
end;
$$;


create OR replace function askp."jf_recalc_period_pkg$of_delete"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.recalc_period%rowtype;
begin
  l_r := askp.jf_recalc_period_pkg$attr_to_rowtype(p_attr);

  delete from  askp.recalc_period where  recalc_period_id = l_r.recalc_period_id;

  return null;
end;
$$;


create OR replace function askp."jf_recalc_period_pkg$of_insert"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.recalc_period%rowtype;
begin
  l_r := askp.jf_recalc_period_pkg$attr_to_rowtype(p_attr);
  l_r.recalc_period_id := nextval('askp.recalc_period_recalc_period_id_seq'::regclass);

  insert into askp.recalc_period select l_r.*;

  return null;
end;
$$;


create OR replace function askp."jf_recalc_period_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
begin
  open p_rows for
  select
    recalc_period_id,
    time_from,
    time_to,
    comment,
    CASE WHEN time_execution_finish IS NOT NULL THEN 'Выполнено'
         WHEN time_execution_start IS NOT NULL THEN 'В обработке'
         WHEN time_put_in_queue IS NOT NULL THEN 'В очереди'
      END AS state
  from askp.recalc_period rp
    LEFT JOIN askp.event_manager_log logg ON logg.em_log_id = rp.em_log_id;
end;
$$;


create OR replace function askp."jf_recalc_period_pkg$of_update"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.recalc_period%rowtype;
begin
  l_r := askp.jf_recalc_period_pkg$attr_to_rowtype(p_attr);

  update askp.recalc_period set
    time_from = l_r.time_from,
    time_to = l_r.time_to,
    comment = l_r.comment
  where
    recalc_period_id = l_r.recalc_period_id;

  return null;
end;
$$;


create or REPLACE function askp."jf_recalc_period_pkg$of_recalc1"(p_id_account numeric, p_attr text) returns boolean
LANGUAGE plpgsql
AS $$
DECLARE
  ld_start_time TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'time_from', true);
  ld_end_time TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'time_to', true);

  l_recalc_period_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'recalc_period_id', true);

  l_task_id BIGINT := nextval('askp.event_manager_log_em_log_id_seq'::regclass);
  l_json JSON;
BEGIN
  INSERT INTO askp.event_manager_log(em_log_id, time_put_in_queue, id_account)
      VALUES (l_task_id, now(), p_id_account);

  UPDATE askp.recalc_period SET em_log_id = l_task_id where recalc_period_id = l_recalc_period_id;

  -- core.event_type_binding
  l_json :=  json_build_object('em_log_id', l_task_id, 'time_from', ld_start_time, 'time_to', ld_end_time);
  PERFORM paa.paa_api_pkg$basic_publish
  (cmnd.jf_cmnd_ref4unit_pkg$cn_command_rmq_broker(), '', 'event-manager-booked', l_json::TEXT, 'ASKP_RECALC');
  return TRUE;
END;
$$;

create or REPLACE function askp.askp_recalc(p_json in jsonb) returns VOID
LANGUAGE plpgsql
AS $$
DECLARE
  l_id BIGINT := (p_json->>'em_log_id')::bigint;
  -- Период за который нужно произвести перерасчет
  ld_start_time TIMESTAMP := (p_json->>'time_from')::TIMESTAMP + INTERVAL '3 hour';-- время UTC
  ld_end_time TIMESTAMP := (p_json->>'time_to')::TIMESTAMP + INTERVAL '3 hour';-- время UTC

  l_agg_id BIGINT := nextval('askp.asmpp_agg_asmpp_agg_id_seq'::regclass);
BEGIN
  -- запись о начале выполнения
  UPDATE askp.event_manager_log SET time_execution_start = now() WHERE em_log_id = l_id;
  --
  DELETE FROM askp.asmpp_agg_item where time_start BETWEEN ld_start_time AND ld_end_time;
  INSERT INTO askp.asmpp_agg(asmpp_agg_id, time_start, time_finish)
     VALUES(l_agg_id, ld_start_time, ld_end_time) ;
  PERFORM askp.create_agg_items(ld_start_time, ld_end_time, l_agg_id);
  --
  DELETE FROM askp.askp_data where check_time
         BETWEEN (ld_start_time + INTERVAL '3 hour') AND (ld_end_time + INTERVAL '3 hour');
  PERFORM askp.create_askp_data(ld_start_time, ld_end_time, l_id);
  -- балансировка входящих и выходящих пассажиров АСМПП
  PERFORM askp.balancing_asmpp(ld_start_time, ld_end_time, l_id);

  DELETE from askp.validation2stops_binding WHERE time_fact BETWEEN ld_start_time AND ld_end_time;
  PERFORM askp.bind_validations(ld_start_time, ld_end_time);
  PERFORM askp.aggregate_binding4reports(ld_start_time, ld_end_time);
  -- запись о конце выполнения
  UPDATE askp.event_manager_log SET time_execution_finish = now() WHERE em_log_id = l_id;
END;
$$;


--select * from paa.action where action_params::TEXT LIKE '%ASKP_RECALC%' AND create_time > '2017-12-18'
--select * from askp.askp_data order by check_time desc