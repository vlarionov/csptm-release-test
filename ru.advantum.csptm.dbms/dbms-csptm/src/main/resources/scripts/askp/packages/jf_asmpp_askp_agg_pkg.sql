CREATE OR REPLACE FUNCTION askp.jf_asmpp_askp_agg_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
DECLARE
  f_dt_start 			DATE 		:= jofl.jofl_pkg$extract_date(p_attr, 'BGN_DT', false);
  f_dt_end	 			DATE 		:= jofl.jofl_pkg$extract_date(p_attr, 'END_DT', false);
  f_arr_tr_id 			BIGINT [] 	:= jofl.jofl_pkg$extract_narray(p_attr, 'f_list_tr_id', true);
  f_arr_rv_muid 		BIGINT [] 	:= jofl.jofl_pkg$extract_narray(p_attr, 'f_list_rv_muid', true);
  f_arr_depo_id			BIGINT [] 	:= jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', true); 
  f_tr_id				BIGINT 		:= jofl.jofl_pkg$extract_number(p_attr, 'f_tr_id', true);
  f_arr_route_muid		BIGINT [] 	:= jofl.jofl_pkg$extract_narray(p_attr, 'f_list_route_muid', true);
  f_dir_muid			BIGINT 		:= jofl.jofl_pkg$extract_number(p_attr, 'f_dir_MUID', true); 
  f_arr_tte_num			BIGINT [] 	:= jofl.jofl_pkg$extract_narray(p_attr, 'f_list_timetable_entry_num', true);
  f_arr_rtype_muid		BIGINT [] 	:= jofl.jofl_pkg$extract_narray(p_attr, 'f_list_rtype_MUID', true); 
  f_arr_ornd_num		BIGINT [] 	:= jofl.jofl_pkg$extract_narray(p_attr, 'f_list_order_round_num', true); 
BEGIN
OPEN p_rows FOR
       with agg as (       
             SELECT
              ent.name_short as depo_name,
              tr.garage_num as tr_garage_num,
              trcap.full_name as tr_capacity_full_name,
              trt.name as tr_type_name,
              rrt.name as ornd_type,
              rrtt.name as traj_type,
              orf.stop_order_num,
              orf.time_fact as sp_time_fact,
              round(EXTRACT(EPOCH FROM orf.time_fact) - 
              lag( EXTRACT(EPOCH FROM orf.time_fact)) over (partition by aagi.tr_id, 
                                                                         aagi.route_variants_muid, 
                                                                         ornd.order_round_id, 
                                                                         rt.trajectory_type_muid  
                                                                order by orf.stop_order_num)) as sp_period_fact,
              to_char((orv.time_fact_begin), 'dd.mm.yyyy hh24:mi:ss') || 
              ' - ' || 
              to_char((orv.time_fact_end), 'dd.mm.yyyy hh24:mi:ss') as ornd_period,
              aagi.asmpp_agg_item_id,
              aagi.asmpp_agg_id,
              aagi.tr_id,
              aagi.stops_muid,
              aagi.route_variants_muid,
              ornd.order_round_id,
              aagi.in_cnt,
              aagi.out_cnt,
              aagi.time_start,
              aagi.time_finish,
              s.name,
              tr.licence,
              tte.timetable_entry_num,
              ornd.order_round_num, -- 'номер рейса в наряде'
              r.number, -- 'номер',
              (select count(1) from askp.askp_data where askp_data.asmpp_agg_item_id = aagi.asmpp_agg_item_id) valid_tickets_qty,
              aagi.in_corrected,
              aagi.out_corrected,
              0 as total_pass_deliv_to_st,
              sum(aagi.in_corrected) OVER (partition by aagi.tr_id, 
                                                        aagi.route_variants_muid, 
                                                        ornd.order_round_id, 
                                                        rt.trajectory_type_muid  
                                               order by orf.stop_order_num) -
              sum(aagi.out_corrected) OVER (partition by aagi.tr_id, 
                                                        aagi.route_variants_muid, 
                                                        ornd.order_round_id, 
                                                        rt.trajectory_type_muid  
                                               order by orf.stop_order_num) as total_pass_ship_from_st,
              tr.depo_id,
              r.muid as route_muid,
              rt.trajectory_type_muid,
              rr.route_round_type_muid,
              sp.muid as sp_muid                                                 
            from /*askp.asmpp_agg aag */
            /*join*/ askp.asmpp_agg_item aagi /*on aagi.asmpp_agg_id = aag.asmpp_agg_id*/
            join gis.stops s on s.muid = aagi.stops_muid
            join core.tr tr on tr.tr_id = aagi.tr_id
            join tt.order_round ornd on ornd.order_round_id = aagi.order_round_id
            join tt.order_list orlst on orlst.order_list_id = ornd.order_list_id
            join tt.timetable_entry tte on tte.timetable_entry_id = orlst.timetable_entry_id
            join gis.route_variants rv on rv.muid = aagi.route_variants_muid
            join gis.routes r on r.current_route_variant_muid = rv.muid   
            join tt.round rnd on rnd.round_id = ornd.round_id 
                              and tte.timetable_entry_id = rnd.timetable_entry_id                        
            join tt.order_fact orf on orf.order_round_id = ornd.order_round_id
            join gis.stop_places sp on sp.muid = orf.stop_place_muid and sp.stop_muid = s.muid             
            join gis.route_trajectories rt on rt.muid = rnd.route_trajectory_muid 
            join gis.stop_place2route_traj sp2rt on sp2rt.route_trajectory_muid = rt.muid and sp2rt.stop_place_muid = sp.muid
            join gis.ref_route_trajectory_types rrtt on rrtt.muid = rt.trajectory_type_muid
            join gis.route_rounds rr on rr.route_variant_muid = rv.muid and rr.muid = rt.route_round_muid
            join gis.ref_route_round_types rrt on rrt.muid = rr.route_round_type_muid
            join core.entity ent on ent.entity_id = tr.depo_id            
            join asd.oper_round_visit orv on orv.order_round_id = ornd.order_round_id  
            join core.tr_model trm ON tr.tr_model_id = trm.tr_model_id
       		join core.tr_capacity trcap ON trm.tr_capacity_id =	trcap.tr_capacity_id 
            join core.tr_type trt ON tr.tr_type_id = trt.tr_type_id         
            )
        select 
              agg.depo_name,
              agg.tr_garage_num,
              agg.tr_capacity_full_name,
              agg.tr_type_name,
              agg.ornd_type,
              agg.traj_type,
              agg.stop_order_num,
              agg.sp_time_fact,
              agg.sp_period_fact,
              agg.ornd_period,
              agg.asmpp_agg_item_id,
              agg.asmpp_agg_id,
              agg.tr_id,
              agg.stops_muid,
              agg.route_variants_muid,
              agg.order_round_id,
              agg.in_cnt,
              agg.out_cnt,
              agg.name,
              agg.timetable_entry_num,
              agg.order_round_num, 
              agg.number, 
              agg.valid_tickets_qty,
              agg.in_corrected,
              agg.out_corrected,
              lag(agg.total_pass_ship_from_st) over (partition by agg.tr_garage_num, 
                                                                  agg.route_variants_muid, 
                                                                  agg.order_round_id, 
                                                                  agg.traj_type
                                                         order by agg.stop_order_num) as total_pass_deliv_to_st,
              agg.total_pass_ship_from_st 
        from agg
        WHERE agg.time_start >= f_dt_start and agg.time_finish <= f_dt_end
             AND (agg.tr_id = ANY (f_arr_tr_id) OR array_length(f_arr_tr_id, 1) IS NULL)
             AND (agg.route_variants_muid = ANY (f_arr_rv_muid)  OR array_length(f_arr_rv_muid, 1) IS NULL)
             AND (agg.depo_id = ANY (f_arr_depo_id) OR array_length(f_arr_depo_id, 1) IS NULL)
             AND (agg.route_muid = ANY (f_arr_route_muid) OR array_length(f_arr_route_muid, 1) IS NULL)
             AND (agg.trajectory_type_muid = f_dir_muid OR f_dir_muid IS NULL)
             AND (agg.timetable_entry_num = ANY (f_arr_tte_num) OR array_length(f_arr_tte_num, 1) IS NULL)
             AND (agg.route_round_type_muid = ANY (f_arr_rtype_muid) OR array_length(f_arr_rtype_muid, 1) IS NULL)
             AND (agg.order_round_num = ANY (f_arr_ornd_num) OR array_length(f_arr_ornd_num, 1) IS NULL)             
            ;   
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;