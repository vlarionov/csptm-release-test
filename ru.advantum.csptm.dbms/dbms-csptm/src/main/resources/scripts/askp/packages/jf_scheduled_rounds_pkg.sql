CREATE OR REPLACE FUNCTION askp.jf_scheduled_rounds_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                                p_attr       TEXT)
    RETURNS REFCURSOR
LANGUAGE plpgsql
AS $function$
DECLARE
    l_route_inspection_id askp.route_inspection.route_inspection_id%TYPE := jofl.jofl_pkg$extract_number(p_attr,'route_inspection_id',TRUE);
    l_dow TEXT := jofl.jofl_pkg$extract_number(p_attr, 'dow', true);
    l_dow_arr SMALLINT[] := array(select DISTINCT unnest((regexp_matches(l_dow, '\d{1}', 'g'))));
BEGIN
    OPEN p_rows FOR
    SELECT
        r.order_round_num,
        o.round_code,
        te.timetable_entry_num,
        count(*)                                                                                            done,
        askp.jf_scheduled_rounds_pkg_percent(count(*) :: INTEGER, i.route_inspection_id)::NUMERIC :: TEXT || ' %' AS per
    FROM gis.routes r2
        JOIN rts.route2gis r2g ON r2.muid = r2g.route_muid
        JOIN askp.route_inspection i ON i.route_id = r2g.route_id
        JOIN gis.route_variants rv ON r2.current_route_variant_muid = rv.muid
        JOIN gis.route_rounds rr ON rv.muid = rr.route_variant_muid
        JOIN gis.route_trajectories rt ON rr.muid = rt.route_round_muid
        JOIN tt.round o ON rt.muid = o.route_trajectory_muid
        JOIN tt.order_round r ON o.round_id = r.round_id
        JOIN tt.order_list ol ON r.order_list_id = ol.order_list_id
        JOIN tt.timetable_entry te ON o.timetable_entry_id = te.timetable_entry_id
        JOIN asd.oper_round_visit orv ON r.order_round_id = orv.order_round_id
    WHERE (ol.order_date BETWEEN i.inspection_begin_date AND i.inspection_end_date AND
           ((EXTRACT(ISODOW FROM ol.order_date) = ANY(l_dow_arr)) OR l_dow IS NULL))
          AND r.is_active = 1
          AND ol.is_active = 1 AND ol.sign_deleted = 0
          AND orv.order_round_status_id = 1
          AND i.route_inspection_id = l_route_inspection_id
          AND (orv.time_plan_end BETWEEN i.inspection_begin_date AND i.inspection_end_date)
    GROUP BY te.timetable_entry_num, o.round_code, i.route_inspection_id, r.order_round_num;
END;
$function$;

CREATE OR REPLACE FUNCTION askp.jf_scheduled_rounds_pkg_percent(p_count_fact          INTEGER,
                                                                p_route_inspection_id askp.route_inspection.route_inspection_id%TYPE,
    OUT                                                         p_percent             REAL)
    RETURNS REAL
LANGUAGE plpgsql
AS $function$
BEGIN
    WITH cp AS
    (
        SELECT (i.round_count * i.inspection_count) AS count_plan
        FROM askp.route_inspection i
        WHERE i.route_inspection_id = p_route_inspection_id
    )
    SELECT CASE WHEN  p_count_fact / cp.count_plan ::NUMERIC> 1
        THEN 100
           ELSE
               round((100 * p_count_fact::REAL / cp.count_plan)::NUMERIC,2)::REAL END
    INTO p_percent
    FROM cp;
END;
$function$;
