create or replace function askp."jf_askp_traffic_rep_02_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
    l_date date := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_DT', true) + interval '3 hours';
    l_route_id bigint[] := jofl.jofl_pkg$extract_tarray(p_attr, 'F_route_muid', TRUE);
    l_move text[] := jofl.jofl_pkg$extract_tarray(p_attr, 'rep_move_num', TRUE);
begin
  if core.assigned(l_route_id::text, l_move::text) then
    open p_rows for
      select  t.number,
              tt.short_name tr_type_name,
              t.move_num,
              sum(check_all) pass_count,
              sum(check_privilege) check_privilege,
              sum(check_ordinary) check_ordinary
      from askp.validation4reports_agg t join core.tr_type tt on t.tr_type_id = tt.tr_type_id
      where check_time >= l_date::TIMESTAMP and check_time < l_date::TIMESTAMP + INTERVAL '1 day'
          and (t.number, t.tr_type_id) in ( select rt.route_num, rt.tr_type_id
                                            from rts.route rt join rts.route2gis r2g on rt.route_id = r2g.route_id
                                            where r2g.route_muid = any(l_route_id)
                                              and not rt.sign_deleted
                                          )
        and t.move_num::text = any(l_move)
      group by t.number, tt.short_name, t.move_num;
  ELSE
--    open p_rows for select * from (values(null, null, null, p_patt) as t(number, move_num, pass_count, dt);
    open p_rows for select * from (values(null, null, null)) as t(number, move_num, pass_count);
  end if;
end;
$$;

