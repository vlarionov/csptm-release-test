create or REPLACE function askp."jf_many_starts_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
  -- период расчета
  l_trip_pairs_agg_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'trip_pairs_agg_id', true);

  l_sps BIGINT[] := array(select stop_place_id from askp.trip_pairs_start_sp where trip_pairs_agg_id = l_trip_pairs_agg_id);

  l_trip_number BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'trip_number', true);

  f_dt_start timestamp 	:= jofl.jofl_pkg$extract_date(p_attr, 'd_from', true);
  f_dt_end	 timestamp	:= jofl.jofl_pkg$extract_date(p_attr, 'd_to', true);
  l_dow TEXT := jofl.jofl_pkg$extract_number(p_attr, 'dow', true);
  -- массив цифр, типа {2,5}
  l_dow_arr SMALLINT[] := array(select DISTINCT unnest((regexp_matches(l_dow, '\d{1}', 'g'))));

  l_seconds_from BIGINT := (select time_from from askp.trip_pairs_agg where trip_pairs_agg_id = l_trip_pairs_agg_id);
  l_seconds_to BIGINT := (select time_to from askp.trip_pairs_agg where trip_pairs_agg_id = l_trip_pairs_agg_id);
begin
  open p_rows for
  /*WITH z AS(
    (select muid from luchininov.connected_stop_places2 where stop_places_muid = 2993277929106909976)
  ),
      a AS(select * from askp.end_station where start_sp_muid IN (select muid from z)),
      b as (select (select wkt_geom from gis.stop_places where muid = end_sp_muid)::TEXT as end_sp_geom, COUNT(*) as amount from a GROUP BY end_sp_muid)
  Select (select string_agg(wkt_geom, ', ') from gis.stop_places where muid IN (select muid from z)) as start_sps_geom,
    * from b order by amount desc limit 15;*/
  -- может быть несколько начальных остановок
  WITH z AS(
    -- связанные остановки
    select distinct on (muid, stop_places_muid) stop_places_muid, muid from luchininov.connected_stop_places2
     where stop_places_muid = /*ANY(l_sps)*/ANY(array(select stop_place_id from askp.trip_pairs_start_sp where trip_pairs_agg_id = l_trip_pairs_agg_id))
    -- еще сама остановка (котрая указана в параметрах)
    UNION ALL
    select unnest(l_sps), unnest(l_sps)
  ), -- 2993277929106909976    2993277473840376620
   --select * from z;
   dd AS (select stop_places_muid, -- начальная остановка и геометрия связанных остановок
          (select string_agg(wkt_geom, ', ') from z z1
          JOIN gis.stop_places sp ON sp.muid = z1.muid
          where z1.stop_places_muid = z.stop_places_muid) as start_sps_geom
    from z GROUP BY stop_places_muid),
      -- поток от связанных остановок
      /*a AS(select *, (SELECT count(*) as amount from askp.end_station es
                      where z1.muid = es.start_sp_muid
                      GROUP BY end_sp_muid)
           from z z1)
      select * from a;*/
      a As(select start_sp_muid, end_sp_muid, COUNT(*) as amount from askp.end_station es
           JOIN z ON z.muid = es.start_sp_muid
           AND es.check_time >= f_dt_start AND es.check_time <= f_dt_end -- фильтр по дате
           AND (EXTRACT(ISODOW FROM check_time) = ANY(l_dow_arr) OR l_dow IS NULL)-- фильтр по дню недели
           -- фильтр по периоду суток
           AND ( (EXTRACT(HOUR from now()) * 3600 + EXTRACT(minute from now()) * 60 /*секунды*/
                 BETWEEN (l_seconds_from - 10800) AND (l_seconds_to - 10800))
                 OR (l_seconds_from = 0 AND l_seconds_to = 0)
                 OR (l_seconds_from IS NULL AND l_seconds_to IS NULL) )
           GROUP BY end_sp_muid, start_sp_muid),
      bb AS (select stop_places_muid, end_sp_muid, sum(amount) amount,
      (select wkt_geom from gis.stop_places where muid = end_sp_muid)::TEXT as end_sp_geom,
      (select name from gis.stops where muid = (select stop_muid from gis.stop_places where muid = end_sp_muid))::TEXT as end_sp_name,
      (select name from gis.stops where muid = (select stop_muid from gis.stop_places where muid = stop_places_muid))::TEXT as start_sp_name,
      (select start_sps_geom from dd WHERE dd.stop_places_muid = z.stop_places_muid)
      from a
      JOIN z ON z.muid = a.start_sp_muid
      GROUP BY stop_places_muid, end_sp_muid)
      select * from bb where amount > l_trip_number;

      /*select a.*, z.stop_places_muid, (select wkt_geom from gis.stop_places where muid = end_sp_muid)::TEXT as end_sp_geom
      from a;
      --JOIN z ON  = z.muid;
      b as (select (select wkt_geom from gis.stop_places where muid = end_sp_muid)::TEXT as end_sp_geom, COUNT(*) as amount from a GROUP BY end_sp_muid)
  Select (select string_agg(wkt_geom, ', ') from gis.stop_places where muid IN (select muid from z)) as start_sps_geom,
    * from b order by amount desc limit 15;*/
end;
$$;