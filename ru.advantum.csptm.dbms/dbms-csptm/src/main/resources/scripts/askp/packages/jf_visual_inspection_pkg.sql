create or replace function askp.jf_visual_inspection_pkg$attr_to_rowtype(p_attr text)
    returns askp.visual_inspection
language plpgsql
as $function$
declare
    l_r askp.visual_inspection%rowtype;
begin
    l_r.visual_inspection_id:= jofl.jofl_pkg$extract_number(p_attr, 'visual_inspection_id', true);
    l_r.plan_date:= jofl.jofl_pkg$extract_date(p_attr, 'plan_date', true);
    l_r.inspector_id:= mod(jofl.jofl_pkg$extract_number(p_attr, 'inspector_id', true), 24 * 3600);
    l_r.status:= jofl.jofl_pkg$extract_number(p_attr, 'status', true);
    l_r.stop_item_id:= jofl.jofl_pkg$extract_number(p_attr, 'stop_item_id', true);
    l_r.move_direction_id:= jofl.jofl_pkg$extract_number(p_attr, 'move_direction_id', true);
    l_r.route_id:= jofl.jofl_pkg$extract_number(p_attr, 'route_id', true);
    l_r.comment:= jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true);

    return l_r;
end;
$function$;
-------------------------------------------
create or replace function askp.jf_visual_inspection_pkg$of_rows(p_id_account numeric, out p_rows refcursor,
                                                                 p_attr       text)
    returns refcursor
language plpgsql
as $function$
declare
    l_f_review_dt    timestamp := jofl.jofl_pkg$extract_date(p_attr, 'F_REVIEW_DT', true);
    l_f_end_dt       timestamp := jofl.jofl_pkg$extract_date(p_attr, 'F_END_DT', true);
    l_f_status       integer := jofl.jofl_pkg$extract_number(p_attr, 'F_STATUS_INPLACE_K', true);
    l_f_inspector_id integer [] :=jofl.jofl_pkg$extract_narray(p_attr, 'f_inspector_id', true);
    l_f_stop_item_id integer [] :=jofl.jofl_pkg$extract_narray(p_attr, 'f_stop_item_id', true);
begin
    open p_rows for
    select
        avi.visual_inspection_id,
        avi.visual_inspection_id :: text                              p_visual_inspection_id,
        avi.plan_date,
        avi.status,
        avi.stop_item_id,
        sg.stop_group_short_name                                      stop_group_short_name,
        avi.comment,
        i.last_name || ' ' || i.first_name || ' ' || i.middle_name as full_name,
        i.inspector_id,
        rs.name                                                    as stop_name,
        r.route_num,
        r.route_id,
        sl.building_address
    from askp.visual_inspection avi
        left join askp.inspector i on avi.inspector_id = i.inspector_id
        left join rts.stop_item rsi on avi.stop_item_id = rsi.stop_item_id
        join rts.stop_location sl on rsi.stop_location_id = sl.stop_location_id
        join rts.stop rs on rs.stop_id = sl.stop_id
        join rts.stop2stop_group s2sg on s2sg.stop_id = rs.stop_id
        join rts.stop_group sg on s2sg.stop_group_id = sg.stop_group_id
        join rts.route r on r.route_id = avi.route_id
    where (avi.plan_date between l_f_review_dt and l_f_end_dt)
          and (l_f_status is null or avi.status = l_f_status)
          and (avi.inspector_id = any (l_f_inspector_id) or cardinality(l_f_inspector_id) = 0 or l_f_inspector_id isnull)
          and (avi.stop_item_id = any (l_f_stop_item_id) or cardinality(l_f_stop_item_id) = 0 or l_f_stop_item_id isnull);
end;
$function$;
-------------------
create or replace function askp."jf_visual_inspection_pkg$of_delete"(p_id_account numeric, p_attr text)
    returns text
language plpgsql
as $function$
declare
    l_visual_inspection_id askp.visual_inspection.visual_inspection_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'visual_inspection_id', true);
begin

    delete from askp.tr_inspection t
    where t.visit_tr_stop_id in
          (
              select vts.visit_tr_stop_id
              from askp.visit_tr_stop vts
              where vts.visual_inspection_id = l_visual_inspection_id
          );

    delete from askp.visit_tr_stop vs
    where vs.visual_inspection_id = l_visual_inspection_id;

    delete from askp.visual_inspection vi
    where vi.visual_inspection_id = l_visual_inspection_id;
    return null;
end;
$function$;
-------------------
create or replace function askp.jf_visual_inspection_pkg$of_insert(p_id_account numeric, p_attr text)
    returns text
language plpgsql
as $function$
declare
    l_r askp.visual_inspection%rowtype;
begin
    l_r := askp.jf_visual_inspection_pkg$attr_to_rowtype(p_attr);
    insert into askp.visual_inspection (plan_date, status, stop_item_id, move_direction_id, comment, inspector_id, route_id)
    values (l_r.plan_date, l_r.status, l_r.stop_item_id, l_r.move_direction_id, l_r.comment, l_r.inspector_id, l_r.route_id);
    return null;
end;
$function$;

create or replace function askp.jf_visual_inspection_pkg$of_update(p_id_account numeric, p_attr text)
    returns text
language plpgsql
as $function$
declare
    l_r askp.visual_inspection%rowtype;
begin
    l_r := askp.jf_visual_inspection_pkg$attr_to_rowtype(p_attr);
    update askp.visual_inspection
    set
        plan_date         = l_r.plan_date,
        status            = l_r.status,
        stop_item_id      = l_r.stop_item_id,
        move_direction_id = l_r.move_direction_id,
        comment           = l_r.comment,
        inspector_id      = l_r.inspector_id,
        route_id          = l_r.route_id
    where visual_inspection_id = l_r.visual_inspection_id;
    return null;
end;
$function$;