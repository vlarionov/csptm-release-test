﻿-- ========================================================================
-- Пакет функций для формирования отчетов по АСКП (Анализ пассажиропотоков)
-- ========================================================================

create or replace function askp.report_hourly_check4route_plain (p_id_account numeric, p_attr text) returns refcursor
/* почасовые отчеты по проездам на маршруте без использования crosstab */
LANGUAGE plpgsql
AS $$
declare
 res_cursor refcursor;
 p_report_type text :=  jofl.jofl_pkg$extract_varchar(p_attr, 'P_REPORT_TYPE', true);
 a_check_types text[] := array['Льготные','Оплаченные','Все'];

 /* период отчета - параметры и переменные для забора данных */
 f_report_period_start_attr timestamp;
 f_report_period_end_attr timestamp;
 f_report_period_start timestamp;
 f_report_period_end timestamp;

 /* переменные для фильтра по дню недели */
 f_report_day_list text;
 f_report_day_name text;
 f_report_day_set smallint[];

 /* массив с выбранными маршрутами */
 f_arr_rep_route bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_route_muid', true);

begin
 /* читаем только нужные параметры для типа отчета */
 if p_report_type = 'ONE_DAY' then
 -- для отчета на дату
  f_report_period_start_attr := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_DT', true);
  f_report_period_end_attr := f_report_period_start_attr;
 elsif p_report_type = 'PERIOD' then
 -- для отчета за период
  f_report_period_start_attr := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_START_DT', true);
  f_report_period_end_attr := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_END_DT', true);
  f_report_day_list := jofl.jofl_pkg$extract_varchar(p_attr, 'F_WEEKDAY_INPLACE_K', true);
  f_report_day_name := jofl.jofl_pkg$extract_varchar(p_attr, 'F_WEEKDAY_INPLACE_V', true);
 end if;

  /* без установленных фильтров гарантированно открываем "пустой" отчет,
     если фильтры установлены - собираем данные */
 if f_report_period_start_attr is not null or
    f_report_period_end_attr is not null or
    array_length(f_arr_rep_route, 1) is not null or
    f_report_day_list is not null then

    /* вычисляем дату начала и окончания периода отчета */
    f_report_period_start := date_trunc('day' , f_report_period_start_attr + interval '3 hours');
    f_report_period_end := date_trunc('day' , f_report_period_end_attr + interval '3 hours') + interval '23:59:59';

	/* определяем массив необходимых дней недели (по ttb.calendar_tag - Будни / Выходные) */
	case coalesce(f_report_day_list, 'EVERYDAY')
	 when 'WORKINGDAY' then f_report_day_set := array[8];
	 when 'DAYOFF' then f_report_day_set := array[9];
	 when 'EVERYDAY' then f_report_day_set := array[8,9];
	end case;

  /* открываем отчет по почасовому агрегату */
  open res_cursor for
   /* считаем количество проездов (в разрезе типов билетов) по маршрутам и часам за нужный период
      дополняем недостающими строками
	  накладываем фильтры по дням недели и маршрутам */
   with src_data as (select extract ('hour' from v4rag2.check_time) as hr,
                            v4rag2.number as route_number,
                            v4rag2.tr_type_id as tr_type_id,
                            sum(v4rag2.check_privilege) as check_privilege,
                            sum(v4rag2.check_ordinary) as check_ordinary,
                            sum(v4rag2.check_all) as check_all
                       from askp.validation4reports_agg v4rag2
                       join (select distinct ci.calendar_id
                               from ttb.calendar_item ci
                              where ci.calendar_tag_id = any (f_report_day_set) and
                                    ci.calendar_id between trunc(extract (epoch from f_report_period_start)/(24*60*60)) and
                                                           trunc(extract (epoch from f_report_period_end)/(24*60*60))) fcal2 on 1=1
                      where v4rag2.check_time between f_report_period_start and f_report_period_end and
                            /* фильтр по дню недели, с учетом производственного кадендаря */
                            trunc(extract (epoch from v4rag2.check_time)/(24*60*60)) = fcal2.calendar_id and
                            /* фильтр по маршрутам (с учетом вида транспорта маршрута) */
                            (array_length(f_arr_rep_route, 1) IS NULL OR
                             exists (select 1
                                       from rts.route2gis r2g
                                       join rts.route rrt on rrt.route_id = r2g.route_id
                                       where r2g.route_muid = ANY (f_arr_rep_route) and
                                             rrt.route_num = v4rag2.number and
                                             rrt.tr_type_id = v4rag2.tr_type_id))
                      group by v4rag2.number, v4rag2.tr_type_id, v4rag2.check_time::date, grouping sets (extract ('hour' from v4rag2.check_time),()))
   select case p_report_type
           when 'ONE_DAY' then to_char(f_report_period_start, 'DD.MM.YYYY')
           when 'PERIOD'  then to_char(f_report_period_start, 'DD.MM.YYYY') || '-' || to_char(f_report_period_end, 'DD.MM.YYYY')
           else ''
          end as dt,
          f_report_day_name as day_tp,
          sd.route_number ||' '|| tr_tp.short_name as route_number,
          round(avg(case sd.hr when '0' then sd.check_privilege else null end::bigint)) as h0_p,
          round(avg(case sd.hr when '0' then sd.check_ordinary else null end::bigint))  as h0_o,
          round(avg(case sd.hr when '0' then sd.check_all else null end::bigint))       as h0_a,
          round(avg(case sd.hr when '1' then sd.check_privilege else null end::bigint)) as h1_p,
          round(avg(case sd.hr when '1' then sd.check_ordinary else null end::bigint))  as h1_o,
          round(avg(case sd.hr when '1' then sd.check_all else null end::bigint))       as h1_a,
          round(avg(case sd.hr when '2' then sd.check_privilege else null end::bigint)) as h2_p,
          round(avg(case sd.hr when '2' then sd.check_ordinary else null end::bigint))  as h2_o,
          round(avg(case sd.hr when '2' then sd.check_all else null end::bigint))       as h2_a,
          round(avg(case sd.hr when '3' then sd.check_privilege else null end::bigint)) as h3_p,
          round(avg(case sd.hr when '3' then sd.check_ordinary else null end::bigint))  as h3_o,
          round(avg(case sd.hr when '3' then sd.check_all else null end::bigint))       as h3_a,
          round(avg(case sd.hr when '4' then sd.check_privilege else null end::bigint)) as h4_p,
          round(avg(case sd.hr when '4' then sd.check_ordinary else null end::bigint))  as h4_o,
          round(avg(case sd.hr when '4' then sd.check_all else null end::bigint))       as h4_a,
          round(avg(case sd.hr when '5' then sd.check_privilege else null end::bigint)) as h5_p,
          round(avg(case sd.hr when '5' then sd.check_ordinary else null end::bigint))  as h5_o,
          round(avg(case sd.hr when '5' then sd.check_all else null end::bigint))       as h5_a,
          round(avg(case sd.hr when '6' then sd.check_privilege else null end::bigint)) as h6_p,
          round(avg(case sd.hr when '6' then sd.check_ordinary else null end::bigint))  as h6_o,
          round(avg(case sd.hr when '6' then sd.check_all else null end::bigint))       as h6_a,
          round(avg(case sd.hr when '7' then sd.check_privilege else null end::bigint)) as h7_p,
          round(avg(case sd.hr when '7' then sd.check_ordinary else null end::bigint))  as h7_o,
          round(avg(case sd.hr when '7' then sd.check_all else null end::bigint))       as h7_a,
          round(avg(case sd.hr when '8' then sd.check_privilege else null end::bigint)) as h8_p,
          round(avg(case sd.hr when '8' then sd.check_ordinary else null end::bigint))  as h8_o,
          round(avg(case sd.hr when '8' then sd.check_all else null end::bigint))       as h8_a,
          round(avg(case sd.hr when '9' then sd.check_privilege else null end::bigint)) as h9_p,
          round(avg(case sd.hr when '9' then sd.check_ordinary else null end::bigint))  as h9_o,
          round(avg(case sd.hr when '9' then sd.check_all else null end::bigint))       as h9_a,
          round(avg(case sd.hr when '10' then sd.check_privilege else null end::bigint)) as h10_p,
          round(avg(case sd.hr when '10' then sd.check_ordinary else null end::bigint))  as h10_o,
          round(avg(case sd.hr when '10' then sd.check_all else null end::bigint))       as h10_a,
          round(avg(case sd.hr when '11' then sd.check_privilege else null end::bigint)) as h11_p,
          round(avg(case sd.hr when '11' then sd.check_ordinary else null end::bigint))  as h11_o,
          round(avg(case sd.hr when '11' then sd.check_all else null end::bigint))       as h11_a,
          round(avg(case sd.hr when '12' then sd.check_privilege else null end::bigint)) as h12_p,
          round(avg(case sd.hr when '12' then sd.check_ordinary else null end::bigint))  as h12_o,
          round(avg(case sd.hr when '12' then sd.check_all else null end::bigint))       as h12_a,
          round(avg(case sd.hr when '13' then sd.check_privilege else null end::bigint)) as h13_p,
          round(avg(case sd.hr when '13' then sd.check_ordinary else null end::bigint))  as h13_o,
          round(avg(case sd.hr when '13' then sd.check_all else null end::bigint))       as h13_a,
          round(avg(case sd.hr when '14' then sd.check_privilege else null end::bigint)) as h14_p,
          round(avg(case sd.hr when '14' then sd.check_ordinary else null end::bigint))  as h14_o,
          round(avg(case sd.hr when '14' then sd.check_all else null end::bigint))       as h14_a,
          round(avg(case sd.hr when '15' then sd.check_privilege else null end::bigint)) as h15_p,
          round(avg(case sd.hr when '15' then sd.check_ordinary else null end::bigint))  as h15_o,
          round(avg(case sd.hr when '15' then sd.check_all else null end::bigint))       as h15_a,
          round(avg(case sd.hr when '16' then sd.check_privilege else null end::bigint)) as h16_p,
          round(avg(case sd.hr when '16' then sd.check_ordinary else null end::bigint))  as h16_o,
          round(avg(case sd.hr when '16' then sd.check_all else null end::bigint))       as h16_a,
          round(avg(case sd.hr when '17' then sd.check_privilege else null end::bigint)) as h17_p,
          round(avg(case sd.hr when '17' then sd.check_ordinary else null end::bigint))  as h17_o,
          round(avg(case sd.hr when '17' then sd.check_all else null end::bigint))       as h17_a,
          round(avg(case sd.hr when '18' then sd.check_privilege else null end::bigint)) as h18_p,
          round(avg(case sd.hr when '18' then sd.check_ordinary else null end::bigint))  as h18_o,
          round(avg(case sd.hr when '18' then sd.check_all else null end::bigint))       as h18_a,
          round(avg(case sd.hr when '19' then sd.check_privilege else null end::bigint)) as h19_p,
          round(avg(case sd.hr when '19' then sd.check_ordinary else null end::bigint))  as h19_o,
          round(avg(case sd.hr when '19' then sd.check_all else null end::bigint))       as h19_a,
          round(avg(case sd.hr when '20' then sd.check_privilege else null end::bigint)) as h20_p,
          round(avg(case sd.hr when '20' then sd.check_ordinary else null end::bigint))  as h20_o,
          round(avg(case sd.hr when '20' then sd.check_all else null end::bigint))       as h20_a,
          round(avg(case sd.hr when '21' then sd.check_privilege else null end::bigint)) as h21_p,
          round(avg(case sd.hr when '21' then sd.check_ordinary else null end::bigint))  as h21_o,
          round(avg(case sd.hr when '21' then sd.check_all else null end::bigint))       as h21_a,
          round(avg(case sd.hr when '22' then sd.check_privilege else null end::bigint)) as h22_p,
          round(avg(case sd.hr when '22' then sd.check_ordinary else null end::bigint))  as h22_o,
          round(avg(case sd.hr when '22' then sd.check_all else null end::bigint))       as h22_a,
          round(avg(case sd.hr when '23' then sd.check_privilege else null end::bigint)) as h23_p,
          round(avg(case sd.hr when '23' then sd.check_ordinary else null end::bigint))  as h23_o,
          round(avg(case sd.hr when '23' then sd.check_all else null end::bigint))       as h23_a,
          round(avg(case sd.hr is null when true then sd.check_privilege else null end::bigint)) as    summ_p,
          round(avg(case sd.hr is null when true then sd.check_ordinary else null end::bigint))  as    summ_o,
          round(avg(case sd.hr is null when true then sd.check_all else null end::bigint))       as    summ_a
     from src_data sd
     join core.tr_type tr_tp on tr_tp.tr_type_id = sd.tr_type_id
 group by sd.route_number,
          tr_tp.short_name,
          sd.tr_type_id;

else
  open res_cursor for
   select '' as dt,
          '' as day_tp,
          '' as route_number,
          null::bigint as h0_p,
          null::bigint as h0_o,
          null::bigint as h0_a,
          null::bigint as h1_p,
          null::bigint as h1_o,
          null::bigint as h1_a,
          null::bigint as h2_p,
          null::bigint as h2_o,
          null::bigint as h2_a,
          null::bigint as h3_p,
          null::bigint as h3_o,
          null::bigint as h3_a,
          null::bigint as h4_p,
          null::bigint as h4_o,
          null::bigint as h4_a,
          null::bigint as h5_p,
          null::bigint as h5_o,
          null::bigint as h5_a,
          null::bigint as h6_p,
          null::bigint as h6_o,
          null::bigint as h6_a,
          null::bigint as h7_p,
          null::bigint as h7_o,
          null::bigint as h7_a,
          null::bigint as h8_p,
          null::bigint as h8_o,
          null::bigint as h8_a,
          null::bigint as h9_p,
          null::bigint as h9_o,
          null::bigint as h9_a,
          null::bigint as h10_p,
          null::bigint as h10_o,
          null::bigint as h10_a,
          null::bigint as h11_p,
          null::bigint as h11_o,
          null::bigint as h11_a,
          null::bigint as h12_p,
          null::bigint as h12_o,
          null::bigint as h12_a,
          null::bigint as h13_p,
          null::bigint as h13_o,
          null::bigint as h13_a,
          null::bigint as h14_p,
          null::bigint as h14_o,
          null::bigint as h14_a,
          null::bigint as h15_p,
          null::bigint as h15_o,
          null::bigint as h15_a,
          null::bigint as h16_p,
          null::bigint as h16_o,
          null::bigint as h16_a,
          null::bigint as h17_p,
          null::bigint as h17_o,
          null::bigint as h17_a,
          null::bigint as h18_p,
          null::bigint as h18_o,
          null::bigint as h18_a,
          null::bigint as h19_p,
          null::bigint as h19_o,
          null::bigint as h19_a,
          null::bigint as h20_p,
          null::bigint as h20_o,
          null::bigint as h20_a,
          null::bigint as h21_p,
          null::bigint as h21_o,
          null::bigint as h21_a,
          null::bigint as h22_p,
          null::bigint as h22_o,
          null::bigint as h22_a,
          null::bigint as h23_p,
          null::bigint as h23_o,
          null::bigint as h23_a,
          null::bigint as summ_p,
          null::bigint as summ_o,
          null::bigint as summ_a;

 end if;
 return res_cursor;
end;
$$;

create or replace function askp.report_hourly_check4route (p_id_account numeric, p_attr text) returns refcursor
/* почасовые отчеты по проездам на маршруте */
LANGUAGE plpgsql
AS $$
declare
 res_cursor refcursor;
 p_report_type text :=  jofl.jofl_pkg$extract_varchar(p_attr, 'P_REPORT_TYPE', true);
 a_check_types text[] := array['Льготные','Оплаченные','Все'];

 /* период отчета - параметры и переменные для забора данных */
 f_report_period_start_attr timestamp;
 f_report_period_end_attr timestamp;
 f_report_period_start timestamp;
 f_report_period_end timestamp;

 /* переменные для фильтра по дню недели */
 f_report_day_list text;
 f_report_day_name text;
 f_report_day_set smallint[];

 /* массив с выбранными маршрутами */
 f_arr_rep_route bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_route_muid', true);

begin
 /* читаем только нужные параметры для типа отчета */
 if p_report_type = 'ONE_DAY' then
 -- для отчета на дату
  f_report_period_start_attr := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_DT', true);
  f_report_period_end_attr := f_report_period_start_attr;
 elsif p_report_type = 'PERIOD' then
 -- для отчета за период
  f_report_period_start_attr := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_START_DT', true);
  f_report_period_end_attr := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_END_DT', true);
  f_report_day_list := jofl.jofl_pkg$extract_varchar(p_attr, 'F_WEEKDAY_INPLACE_K', true);
  f_report_day_name := jofl.jofl_pkg$extract_varchar(p_attr, 'F_WEEKDAY_INPLACE_V', true);
 end if;

  /* без установленных фильтров гарантированно открываем "пустой" отчет,
     если фильтры установлены - собираем данные */
 if f_report_period_start_attr is not null or 
    f_report_period_end_attr is not null or
    array_length(f_arr_rep_route, 1) is not null or
    f_report_day_list is not null then
	
    /* вычисляем дату начала и окончания периода отчета */
    f_report_period_start := date_trunc('day' , f_report_period_start_attr + interval '3 hours');
    f_report_period_end := date_trunc('day' , f_report_period_end_attr + interval '3 hours') + interval '23:59:59';
	
	/* определяем массив необходимых дней недели (по ttb.calendar_tag - Будни / Выходные) */
	case coalesce(f_report_day_list, 'EVERYDAY')
	 when 'WORKINGDAY' then f_report_day_set := array[8];
	 when 'DAYOFF' then f_report_day_set := array[9];
	 when 'EVERYDAY' then f_report_day_set := array[8,9];
	end case;

  /* открываем отчет по почасовому агрегату */
  open res_cursor for
   /* считаем количество проездов (в разрезе типов билетов) по маршрутам и часам за нужный период
      дополняем недостающими строками
	  накладываем фильтры по дням недели и маршрутам */
   with hourly_route_data as (select f_report_period_start as period_start,
                                     f_report_period_end as period_end,
                                     date_trunc('hour', route_checktime_matrix.check_time) as rep_hour,
                                     route_checktime_matrix.route_number,
                                     route_checktime_matrix.tr_type_id,
                                     sum(src_data.check_privilege) as check_privilege,
                                     sum(src_data.check_ordinary) as check_ordinary,
                                     sum(src_data.check_all) as check_all
                              from (select distinct v4rag1.number as route_number,
                                                    v4rag1.tr_type_id as tr_type_id,
                                                    time_line.point as check_time
                                    from askp.validation4reports_agg v4rag1
                                         join (select generate_series(f_report_period_start, f_report_period_end, '1 hour') as point) time_line on 1=1
                                         join (select distinct ci.calendar_id
                                               from ttb.calendar_item ci
                                               where ci.calendar_tag_id = any (f_report_day_set)
                                                     and ci.calendar_id between trunc(extract (epoch from f_report_period_start)/(24*60*60)) and
											                                                          trunc(extract (epoch from f_report_period_end)/(24*60*60))) fcal1 on 1=1
                                    where v4rag1.check_time between f_report_period_start and f_report_period_end
                                          /* фильтр по дню недели, с учетом производственного кадендаря */
                                          and trunc(extract (epoch from v4rag1.check_time)/(24*60*60)) = fcal1.calendar_id) route_checktime_matrix
                                   left join
                                    (select v4rag2.check_time,
                                            v4rag2.number as route_number,
                                            v4rag2.tr_type_id as tr_type_id,
                                            v4rag2.check_privilege as check_privilege,
                                            v4rag2.check_ordinary as check_ordinary,
                                            v4rag2.check_all as check_all,
                                            v4rag2.tr_id
                                     from askp.validation4reports_agg v4rag2
                                          join (select distinct ci.calendar_id
                                               from ttb.calendar_item ci
                                               where ci.calendar_tag_id = any (f_report_day_set)
                                                     and ci.calendar_id between trunc(extract (epoch from f_report_period_start)/(24*60*60)) and
											                                                          trunc(extract (epoch from f_report_period_end)/(24*60*60))) fcal2 on 1=1
                                     where v4rag2.check_time between f_report_period_start and f_report_period_end
                                          /* фильтр по дню недели, с учетом производственного кадендаря */
                                          and trunc(extract (epoch from v4rag2.check_time)/(24*60*60)) = fcal2.calendar_id) src_data
                                   on      route_checktime_matrix.route_number = src_data.route_number
                                      and  route_checktime_matrix.check_time = src_data.check_time
                                      and  route_checktime_matrix.tr_type_id = src_data.tr_type_id
                              where /* фильтр по маршрутам (с учетом вида транспорта маршрута) */
                                    (array_length(f_arr_rep_route, 1) IS NULL OR
                                     exists (select 1
                                             from rts.route2gis r2g
                                                  join rts.route rrt on rrt.route_id = r2g.route_id
                                             where r2g.route_muid = ANY (f_arr_rep_route)
                                                   and rrt.route_num = route_checktime_matrix.route_number
                                                   and rrt.tr_type_id = route_checktime_matrix.tr_type_id))
                              group by date_trunc('day', route_checktime_matrix.check_time),
                                       date_trunc('hour', route_checktime_matrix.check_time),
                                       route_checktime_matrix.route_number,
                                       route_checktime_matrix.tr_type_id)
   /* считаем итоговые данные
      готовим данные для выдачи в js-crosstab */
   select
    case
     when date_trunc('day', report_data.period_start) = date_trunc('day', report_data.period_end) then to_char(report_data.period_start, 'DD.MM.YYYY')
     else to_char(report_data.period_start, 'DD.MM.YYYY') || '-' || to_char(report_data.period_end, 'DD.MM.YYYY')
    end as dt,
    f_report_day_name as day_tp,
    coalesce(rep_hour_str, 'Всего за ' || 
	         case
              when date_trunc('day', report_data.period_start) = date_trunc('day', report_data.period_end) then 'сутки'
              else 'период'
             end) as hr,
    report_data.route_number  || ' ' || tr_tp.short_name as route_number,
    chtl.check_type,
    CASE chtl.check_type
     when 'Льготные' then round(report_data.check_privilege)
     when 'Оплаченные' then round(report_data.check_ordinary)
     when 'Все' then round(report_data.check_all)
    end as check_value
   from (select hrd_hr.period_start,
                hrd_hr.period_end,
                to_char(hrd_hr.rep_hour, 'HH24') || ' - ' || to_char(hrd_hr.rep_hour + interval '1 hour', 'HH24') as rep_hour_str,
                extract ('hour' from hrd_hr.rep_hour) as rep_hour,
                hrd_hr.route_number,
                hrd_hr.tr_type_id,
                avg(hrd_hr.check_privilege) as check_privilege,
                avg(hrd_hr.check_ordinary) as check_ordinary,
                avg(hrd_hr.check_all) as check_all
         from hourly_route_data hrd_hr
         group by hrd_hr.period_start,
                  hrd_hr.period_end,
                  to_char(hrd_hr.rep_hour, 'HH24') || ' - ' || to_char(hrd_hr.rep_hour + interval '1 hour', 'HH24'),
                  extract ('hour' from hrd_hr.rep_hour),
                  hrd_hr.route_number,
                  hrd_hr.tr_type_id
         union ALL
         select hrd_gr.period_start,
                hrd_gr.period_end,
                null as rep_hour_str,
                null as rep_hour,
                hrd_gr.route_number,
                hrd_gr.tr_type_id,
                sum(hrd_gr.check_privilege) as check_privilege,
                sum(hrd_gr.check_ordinary) as check_ordinary,
                sum(hrd_gr.check_all) as check_all
         from hourly_route_data hrd_gr
         group by hrd_gr.period_start,
                  hrd_gr.period_end,
                  hrd_gr.route_number,
                  hrd_gr.tr_type_id) report_data
        join core.tr_type tr_tp on tr_tp.tr_type_id = report_data.tr_type_id
        join (select unnest(a_check_types) as check_type) chtl on 1=1 ;

 else
 /* если не заданы фильтры - открываем "пустой" отчет
    для js-crosstab возвращаются данные для формирования "шапки" */
     open res_cursor for
      with time_line as (select generate_series(date_trunc('day', now() + interval '3 hour'),
                                                date_trunc('day', now() + interval '3 hour') + interval '23:59:59',
                                               '1 hour') as point),
           check_type_arr as (select unnest(a_check_types) as check_type)
      select to_char(null::timestamp, 'DD.MM.YYYY') as dt,
             '' as day_tp,
             to_char(tl.point, 'HH24') || '-' || to_char(tl.point + interval '1 hour', 'HH24')  as hr,
             '' as route_number,
             chta.check_type as check_type,
             null as check_value
      from time_line tl,
           check_type_arr chta
      union all
      select to_char(null::timestamp, 'DD.MM.YYYY') as dt,
             '' as day_tp,
             'Всего' as hr,
             '' as route_number,
             chtl.check_type as check_type,
             null as check_value
      from (select unnest(a_check_types) as check_type) chtl;

 end if;
 return res_cursor;
end;
$$;

-- --------------------------------------------------------------------------------------------------------------

create or replace function askp.report_daily_check4route(p_id_account numeric, p_attr text) returns refcursor
/* отчеты по проездам на маршруте за сутки (конкретная дата / по всем датам периода с фильтром по дням недели) */
LANGUAGE plpgsql
AS $$
declare
 res_cursor refcursor;
 p_report_type text :=  jofl.jofl_pkg$extract_varchar(p_attr, 'P_REPORT_TYPE', true);

 /* период отчета - параметры и переменные для забора данных */
 f_report_period_start_attr timestamp;
 f_report_period_end_attr timestamp;
 f_report_period_start timestamp;
 f_report_period_end timestamp;

 /* переменные для фильтра по дню недели */
 f_report_day_list text;
 f_report_day_name text;
 f_report_day_set smallint[];

 /* массив с выбранными маршрутами */
 f_arr_rep_route bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_route_muid', true);

begin
 /* читаем только нужные параметры для тпа отчета */
 if p_report_type = 'ONE_DAY' then
 -- для отчета на дату
  f_report_period_start_attr := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_DT', true);
  f_report_period_end_attr := f_report_period_start_attr;
 elsif p_report_type = 'PERIOD' then
 -- для отчета за период
  f_report_period_start_attr := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_START_DT', true);
  f_report_period_end_attr := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_END_DT', true);
  f_report_day_list := jofl.jofl_pkg$extract_varchar(p_attr, 'F_WEEKDAY_INPLACE_K', true);
  f_report_day_name := jofl.jofl_pkg$extract_varchar(p_attr, 'F_WEEKDAY_INPLACE_V', true);
 end if;

 -- {"F_REPORT_DT":"2017-12-19 21:00:00.0","F_route_muid":"$array[2994040611212639700]"}
 
  /* без установленных фильтров гарантированно открываем "пустой" отчет,
     если фильтры установлены - собираем данные */
  if f_report_period_start_attr is not null or
     f_report_period_end_attr is not null or
     array_length(f_arr_rep_route, 1) is not null or
     f_report_day_list is not null then

    /* вычисляем дату начала и окончания периода отчета - для отчета на дату и на период */
    f_report_period_start := date_trunc('day' , f_report_period_start_attr + interval '3 hours');
    f_report_period_end := date_trunc('day' , f_report_period_end_attr + interval '3 hours') + interval '23:59:59';

	/* определяем массив необходимых дней недели (по ttb.calendar_tag - Будни / Выходные) */
	case coalesce(f_report_day_list, 'EVERYDAY')
	 when 'WORKINGDAY' then f_report_day_set := array[8];
	 when 'DAYOFF' then f_report_day_set := array[9];
	 when 'EVERYDAY' then f_report_day_set := array[8,9];
	end case;

    /* открываем отчет по почасовому агрегату */
    open res_cursor for
     select date_trunc('day' ,v4rag.check_time) as dt,
            wd.wd_full_name as day_of_week,
            v4rag.number || ' ' || tr_tp.short_name as route_number,
            sum(v4rag.check_privilege) as privilege_check,
            sum(v4rag.check_ordinary) as ordinary_check,
            sum(v4rag.check_all) as all_check,
            to_char(f_report_period_start, 'DD.MM.YYYY HH24:MI:SS') as report_start,
	        to_char(f_report_period_end, 'DD.MM.YYYY HH24:MI:SS') as report_end,
            to_char(now(), 'DD.MM.YYYY HH24:MI:SS') as report_run_date
     from askp.validation4reports_agg v4rag
          join core.week_day wd on wd.week_day_id = to_char(date_trunc('day' ,v4rag.check_time), 'ID')::smallint
	      join core.tr_type tr_tp on tr_tp.tr_type_id = v4rag.tr_type_id
     where v4rag.check_time between f_report_period_start and f_report_period_end
           /* фильтр по дню недели, с учетом производственного кадендаря */
           and trunc(extract (epoch from v4rag.check_time)/(24*60*60)) in
               (select distinct ci.calendar_id
                from ttb.calendar_item ci
                where ci.calendar_tag_id = any (f_report_day_set) AND
                      ci.calendar_id between trunc(extract (epoch from f_report_period_start)/(24*60*60)) and
				                             trunc(extract (epoch from f_report_period_end)/(24*60*60)))
           /* фильтр по маршрутам (с учетом вида транспорта маршрута) */
          and (array_length(f_arr_rep_route, 1) IS NULL OR
               exists (select 1
                       from rts.route2gis r2g
                            join rts.route rrt on rrt.route_id = r2g.route_id
                       where r2g.route_muid = ANY (f_arr_rep_route)
                             and rrt.route_num = v4rag.number
                             and rrt.tr_type_id = v4rag.tr_type_id))
     group by date_trunc('day' ,v4rag.check_time),
              wd.wd_full_name,
              v4rag.number || ' ' || tr_tp.short_name;

  else
  /* если не заданы фильтры - открываем "пустой" отчет */
   open res_cursor for
     select now() as dt,
            '' as day_of_week,
            '' as route_number,
            0 as privilege_check,
            0 as ordinary_check,
            0 as all_check,
            to_char(now(), 'DD.MM.YYYY HH24:MI:SS') as report_start,
            to_char(now(), 'DD.MM.YYYY HH24:MI:SS') as report_end,
            to_char(now(), 'DD.MM.YYYY HH24:MI:SS') as report_run_date
     where 1 = 2;

	 end if;
 return res_cursor;
end;
$$;

-- --------------------------------------------------------------------------------------------------------------

drop function askp.report_avgdaily_check(numeric, text);
create or replace function askp.report_avgdaily_check(p_id_account numeric, p_attr text) returns refcursor
/* отчеты по среднесуточным показателям */
LANGUAGE plpgsql
AS $$
declare 
 res_cursor refcursor;
 p_report_type text :=  jofl.jofl_pkg$extract_varchar(p_attr, 'P_REPORT_TYPE', true);
 
 f_report_year_start_attr smallint := jofl.jofl_pkg$extract_number(p_attr, 'F_YEAR_FROMNUMBER', true);
 f_report_year_end_attr smallint := jofl.jofl_pkg$extract_number(p_attr, 'F_YEAR_TONUMBER', true);

 f_report_day_list text := jofl.jofl_pkg$extract_varchar(p_attr, 'F_WEEKDAY_INPLACE_K', true);
 f_report_day_name text := jofl.jofl_pkg$extract_varchar(p_attr, 'F_WEEKDAY_INPLACE_V', true);
 f_report_day_set  smallint[];

 /* фильтры заполняютя в зависимости от типа отчета */
 f_arr_rep_route bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_route_muid', true);
 f_arr_rep_depo bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_REPORT_depo_id', true);
 f_arr_rep_tr_type bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_REPORT_tr_type_id', true); 
 f_report_period_start timestamp;
 f_report_period_end timestamp;
 
 begin

 /* без установленных фильтров гарантированно открываем "пустой" отчет,
     если фильтры установлены - собираем данные */
 if f_report_year_start_attr is not null or
    f_report_year_end_attr is not null or
    array_length(f_arr_rep_route, 1) is not null or
	  array_length(f_arr_rep_depo, 1) is not null or
	  array_length(f_arr_rep_tr_type, 1) is not null or
    f_report_day_list is not null then 

   /* вычисляем дату начала и окончания периода отчета */
   f_report_period_start := make_timestamp(f_report_year_start_attr, 1, 1, 0, 0, 0);
   f_report_period_end := make_timestamp(f_report_year_end_attr, 12, 31, 23, 59, 59);

   /* определяем массив необходимых дней недели (по ttb.calendar_tag - Будни / Выходные) */
   case coalesce(f_report_day_list, 'EVERYDAY')
	  when 'WORKINGDAY' then f_report_day_set := array[8];
	  when 'DAYOFF' then f_report_day_set := array[9];
	  when 'EVERYDAY' then f_report_day_set := array[8,9];
   end case;

    /* открываем отчет по почасовому агрегату */
   open res_cursor for
    with src_data as (select daily_dt.*,
                             tr_tp.name as vehicle_type,
                             tr_tp.short_name as short_name,
                             depo.name_full as depo_name
                      from (/* собираем исходные (посуточные) данные в нужных разрезах */
	                          select date_trunc('day', v4rag.check_time) as check_time,
                                   v4rag.tr_type_id,
                                   v4rag.number as route_number,
                                   v4rag.depo_id,
                                   v4rag.tr_id,
                                   sum(v4rag.check_privilege) as check_privilege,
                                   sum(v4rag.check_ordinary) as check_ordinary,
                                   sum(v4rag.check_all) as check_all
                            from askp.validation4reports_agg v4rag
                            where v4rag.check_time between f_report_period_start and f_report_period_end
                                  /* фильтр по дню недели, с учетом производственного кадендаря */
                                  and trunc(extract (epoch from v4rag.check_time)/(24*60*60)) in
                                       (select distinct ci.calendar_id
                                        from ttb.calendar_item ci
                                        where ci.calendar_tag_id = any (f_report_day_set)
                                              and ci.calendar_id between trunc(extract (epoch from f_report_period_start)/(24*60*60))
                                              and trunc(extract (epoch from f_report_period_end)/(24*60*60)))
                                  /* фильтр по маршрутам (с учетом вида транспорта маршрута) */
                                  and (array_length(f_arr_rep_route, 1) IS NULL OR
                                       exists (select 1
                                               from rts.route2gis r2g
                                                    join rts.route rrt on rrt.route_id = r2g.route_id
                                               where r2g.route_muid = ANY (f_arr_rep_route)
                                                     and rrt.route_num = v4rag.number
                                                     and rrt.tr_type_id = v4rag.tr_type_id))
                                  /* фильтр по трапспортным предприятиям */
                                  AND (v4rag.depo_id = ANY (f_arr_rep_depo) OR
                                       array_length(f_arr_rep_depo, 1) IS NULL)
                                  /* фильтр по типам транспортных средств */
                                  AND (v4rag.tr_type_id = ANY (f_arr_rep_tr_type) OR
                                       array_length(f_arr_rep_tr_type, 1) IS NULL)
		                        group by date_trunc('day', v4rag.check_time),
                                     v4rag.tr_type_id,
                                     grouping sets ((v4rag.number, v4rag.depo_id),
                                                    (v4rag.number, v4rag.tr_id),
                                                    v4rag.depo_id,
                                                    v4rag.number,
                                                    ())) daily_dt
                           join core.tr_type tr_tp on tr_tp.tr_type_id = daily_dt.tr_type_id
                           left outer join core.entity depo on depo.entity_id = daily_dt.depo_id
                     where /* в зависимости от типа отчета выбираем строки нужного разреза данных */
                               (p_report_type = 'ROUTENUMBER' and daily_dt.route_number is not null
                                                               and daily_dt.depo_id      is null
                                                               and daily_dt.tr_id        is null)       /* отчет по маршрутам */
 	                          or (p_report_type = 'DEPO'         and daily_dt.depo_id      is not null
                                                               and daily_dt.tr_id        is null
                                                               and daily_dt.route_number is not null)   /* отчет по депо */
                            or (p_report_type = 'VEHICLETYPE' and daily_dt.route_number is null
                                                               and daily_dt.depo_id      is null)       /*  отчет по видам ТС */
                            or (p_report_type = 'VEHICLE'     and daily_dt.tr_id        is not null)),  /* отчет по ТС */
    avg_data as (SELECT daily_data.vehicle_type,
           daily_data.depo_name,
           daily_data.route_number || ' ' || daily_data.short_name as route_number,
           extract (year from daily_data.check_time) as rep_year,
           extract (month from daily_data.check_time) as rep_month,
           f_report_day_name as weekday_type,
           round(avg(daily_data.check_privilege)) as check_privilege,
           round(avg(daily_data.check_ordinary)) as check_ordinary,
           round(avg(daily_data.check_all)) as check_all,
           min(trc.avg_tr_count) as tr_count
    from src_data daily_data
         left join (select distinct t.check_time, t.route_number, t.tr_type_id, round(avg(t.day_tr_count) over (partition by date_trunc('month', t.check_time), t.route_number, t.tr_type_id)) as avg_tr_count
                    from (select sd.check_time, sd.tr_type_id, sd.route_number, count(distinct sd.tr_id) as day_tr_count
                          from src_data sd
                          where p_report_type = 'VEHICLE'
                          group by sd.check_time, sd.tr_type_id, sd.route_number) t ) trc on daily_data.check_time = trc.check_time and
                                                                                                      daily_data.tr_type_id = trc.tr_type_id and
                                                                                                      daily_data.route_number = trc.route_number
    group by daily_data.vehicle_type,
             daily_data.depo_name,
             daily_data.route_number || ' ' || daily_data.short_name,
             extract (year from daily_data.check_time),
             extract (month from daily_data.check_time),
             f_report_day_name)
    select adt1.*,
           1 as sort_order
    from avg_data adt1
    union all
    select adt2.vehicle_type,
           adt2.depo_name,
           'Все маршруты' as route_number,
           adt2.rep_year,
           adt2.rep_month,
           adt2.weekday_type,
           sum(adt2.check_privilege) as check_privilege,
           sum(adt2.check_ordinary) as check_ordinary,
           sum(adt2.check_all) as check_all,
           adt2.tr_count,
		       2 as sort_order
    from avg_data adt2
    where p_report_type = 'DEPO'
    group by adt2.vehicle_type,
             adt2.depo_name,
             adt2.rep_year,
             adt2.rep_month,
             adt2.weekday_type,
             adt2.tr_count
    order by vehicle_type, depo_name, rep_year, rep_month, sort_order, route_number;

  else
   open res_cursor for
      select '' as vehicle_type,
	         '' as depo_name,
             '' as route_number,
             null as rep_year,
             null as rep_month,
             '' as weekday_type,
             0 as check_privilege,
             0 as check_ordinary,
             0 as check_all
      where 1=2;
			
  end if;
 return res_cursor;  
end;
$$;

-- --------------------------------------------------------------------------------------------------------------

create or replace function askp.report_total_check(p_id_account numeric, p_attr text) returns refcursor
/* отчеты с суммарными показателями за период */
LANGUAGE plpgsql
AS $$
declare
 res_cursor refcursor;
 p_report_type text :=  jofl.jofl_pkg$extract_varchar(p_attr, 'P_REPORT_TYPE', true);

 f_report_period_start_attr timestamp := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_START_DT', true);
 f_report_period_end_attr timestamp := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_END_DT', true); 

 f_report_day_list text := jofl.jofl_pkg$extract_varchar(p_attr, 'F_WEEKDAY_INPLACE_K', true);
 f_report_day_name text := jofl.jofl_pkg$extract_varchar(p_attr, 'F_WEEKDAY_INPLACE_V', true);
 f_report_day_set  smallint[];

 f_arr_rep_route bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_route_muid', true);
 f_arr_rep_depo bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_REPORT_depo_id', true);
 f_report_period_start timestamp;
 f_report_period_end timestamp;
 
 begin

 /* без установленных фильтров гарантированно открываем "пустой" отчет,
    если фильтры установлены - собираем данные */
 if f_report_period_start_attr is not null or
    f_report_period_end_attr is not null or
    array_length(f_arr_rep_route, 1) is not null or
    array_length(f_arr_rep_depo, 1) is not null or
    f_report_day_list is not null then 
 
	/* вычисляем дату начала и окончания периода отчета */
     f_report_period_start := date_trunc('day' , f_report_period_start_attr + interval '3 hours');
     f_report_period_end := date_trunc('day' , f_report_period_end_attr + interval '3 hours') + interval '23:59:59';

	/* определяем массив необходимых дней недели (по ttb.calendar_tag - Будни / Выходные) */
	case coalesce(f_report_day_list, 'EVERYDAY')
	 when 'WORKINGDAY' then f_report_day_set := array[8];
	 when 'DAYOFF' then f_report_day_set := array[9];
	 when 'EVERYDAY' then f_report_day_set := array[8,9];
	end case;

    /* открываем отчет по почасовому агрегату */
    open res_cursor for
     /* суммируем проезды за период с итогами по транспортному предприятию, виду транспорта, маршруту */
           select to_char(f_report_period_start, 'DD.MM.YYYY') ||'-'||to_char(f_report_period_end, 'DD.MM.YYYY') as filter_period,
                  coalesce(depo.name_full, 'Все транспортные предприятия') as depo_name,
                  coalesce(v4rag.number || ' ' || tr_tp.short_name, 'Все маршруты') as route_number,
                  case
                   when p_report_type = 'VEHICLETYPE' then coalesce(tr_tp.name, 'Итого')
                   else tr_tp.name
                  end as vehicle_type,
                  f_report_day_name as weekday_type,
                  sum(v4rag.check_privilege) as check_privilege,
                  sum(v4rag.check_ordinary) as check_ordinary,
                  sum(v4rag.check_all) as check_all,
                  to_char(now(), 'DD.MM.YYYY HH24:MI:SS') as report_run_date
           from askp.validation4reports_agg v4rag
                join core.entity depo on v4rag.depo_id = depo.entity_id
                join core.tr_type tr_tp on tr_tp.tr_type_id = v4rag.tr_type_id
           where v4rag.check_time between f_report_period_start and f_report_period_end
                 /* фильтр по дню недели, с учетом производственного кадендаря */
                 and trunc(extract (epoch from v4rag.check_time)/(24*60*60)) in
                     (select distinct ci.calendar_id
                      from ttb.calendar_item ci
                      where ci.calendar_tag_id = any (f_report_day_set)
                            and ci.calendar_id between trunc(extract (epoch from f_report_period_start)/(24*60*60)) and
						                                           trunc(extract (epoch from f_report_period_end)/(24*60*60)))
                 /* фильтр по маршрутам (с учетом вида транспорта маршрута) */
                 and (array_length(f_arr_rep_route, 1) IS NULL OR
                      exists (select 1
                              from rts.route2gis r2g
                                   join rts.route rrt on rrt.route_id = r2g.route_id
                              where r2g.route_muid = ANY (f_arr_rep_route)
                                    and rrt.route_num = v4rag.number
                                    and rrt.tr_type_id = v4rag.tr_type_id))
                 /* фильтр по транспортным предприятиям */
                 AND (v4rag.depo_id = ANY (f_arr_rep_depo) OR
                      array_length(f_arr_rep_depo, 1) IS NULL)
           group by to_char(f_report_period_start, 'DD.MM.YYYY') ||'-'||to_char(f_report_period_end, 'DD.MM.YYYY'),
                    f_report_day_name,
                    grouping sets ((depo.name_full, v4rag.number || ' ' || tr_tp.short_name),
                                   v4rag.number || ' ' || tr_tp.short_name,
                                   depo.name_full,
                                   tr_tp.name)
           /* отбираем строки для каждого отчета */
           having (p_report_type = 'ROUTENUMBER' and depo.name_full is null and tr_tp.name is null) or
                  (p_report_type = 'DEPO' and depo.name_full is not null) or
                  (p_report_type = 'VEHICLETYPE' and (tr_tp.name is not null or
                                                      (tr_tp.name is null and
                                                       depo.name_full is null and
                                                      v4rag.number || ' ' || tr_tp.short_name is null)))
          order by depo.name_full nulls last,
                   v4rag.number || ' ' || tr_tp.short_name nulls last,
                   tr_tp.name nulls last;

 else
  /* если не заданы фильтры - открываем "пустой" отчет */
   open res_cursor for
    select '' as filter_period,
           '' as depo_name,
	         '' as route_number,
           '' as vehicle_type,
           '' as weekday_type,
           0 as check_all,
           to_char(now(), 'DD.MM.YYYY HH24:MI:SS') as report_run_date
    where 1=2;
   
 end if;
 return res_cursor;
end;
$$;
