create or REPLACE function askp."jf_pass_traffic_on_map_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
  l_traffic_matrix_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'traffic_matrix_id', true);
  l_tr_type_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'tr_type_id', true);

begin
  open p_rows for

  select CASE WHEN (passengers > 1) THEN round(passengers::NUMERIC, 0) -- было 2
              ELSE round(passengers::NUMERIC, 0)-- было 5
         END as pass,
         stop_from_muid, stop_to_muid,
         order_num_from||' '||(select name from gis.stops where muid = (select stop_muid from gis.stop_places where muid = stop_from_muid)) as order_num,
         order_num_to||'|'||order_num_to as order_num_to,
    passanger_traffic_matrix_id,
         order_num_from as sort,
         round(sum(passengers) OVER (PARTITION BY order_num_from) ::NUMERIC, 0) as sum,
         '[' || json_build_object(
             'CAPTION', 'На карте',
             'JUMP', json_build_object
             ('METHOD', 'askp.map_traffic','ATTR', json_build_object('matrix_id', passanger_traffic_matrix_id,'tr_type_id',l_tr_type_id)::TEXT ) )|| ']' as url
  from askp.passenger_traffic_matrix_item
  WHERE passanger_traffic_matrix_id = l_traffic_matrix_id;

end;
$$;