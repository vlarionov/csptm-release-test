CREATE OR REPLACE FUNCTION askp."jf_ref_move_num_pkg$of_rows"(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  -- номера маршрута
  l_route BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_route_id', true); -- rts
BEGIN
  OPEN p_rows FOR
  select distinct on (tt_out_num) r.route_id, tt_out_num timetable_entry_num from rts.route r
    JOIN rts.route_variant rv ON rv.route_id = r.route_id
    JOIN ttb.tt_variant v ON v.route_variant_id = rv.route_variant_id
    JOIN ttb.tt_out o ON o.tt_variant_id = v.tt_variant_id
  WHERE (array_length(l_route,1) IS NULL OR r.route_id = ANY(l_route))
  ORDER BY tt_out_num;
END;
$$;