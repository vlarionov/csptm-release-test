CREATE OR REPLACE FUNCTION askp.jf_entrances_survey_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                                p_attr       TEXT)
    RETURNS REFCURSOR
LANGUAGE plpgsql
AS $function$
DECLARE
    l_route_inspection_id askp.route_inspection.route_inspection_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'route_inspection_id', TRUE);
BEGIN
    OPEN p_rows FOR
    SELECT DISTINCT
        a.round_code,
        (sum(a.done)
        OVER (
            PARTITION BY a.timetable_entry_num, a.round_code )) AS done,
        a.timetable_entry_num,
        CASE WHEN a.per > 100
            THEN 100
        ELSE round(a.per::NUMERIC, 2) END || ' %'                        AS per
    FROM askp.jf_entrances_survey_pkg$calc(l_route_inspection_id) a
    ORDER BY a.timetable_entry_num;
END;
$function$;

CREATE OR REPLACE FUNCTION askp.jf_entrances_survey_pkg$calc(p_route_inspection_id askp.route_inspection.route_inspection_id%TYPE)
    RETURNS TABLE(round_code TEXT, timetable_entry_num SMALLINT, done REAL, order_round_num BIGINT, count_all INTEGER, per REAL)
LANGUAGE plpgsql
AS $function$
BEGIN
    RETURN QUERY
    SELECT
        o.round_code,
        te.timetable_entry_num,
        count(orv.order_round_status_id = 1 OR NULL) :: REAL                                                                              done,
        r.order_round_num,
        i.round_count * i.inspection_count                                                                                             AS count_all,
        ((sum(count(orv.order_round_status_id = 1 OR NULL))
                    OVER (
                        PARTITION BY te.timetable_entry_num, o.round_code )) /
             (sum(1)
             OVER (
                 PARTITION BY te.timetable_entry_num, o.round_code )) / (i.round_count * i.inspection_count) * 100)::REAL AS per
    FROM gis.routes r2
        JOIN rts.route2gis r2g ON r2.muid = r2g.route_muid
        JOIN askp.route_inspection i ON i.route_id = r2g.route_id
        JOIN gis.route_variants rv ON r2.current_route_variant_muid = rv.muid
        JOIN gis.route_rounds rr ON rv.muid = rr.route_variant_muid
        JOIN gis.route_trajectories rt ON rr.muid = rt.route_round_muid
        JOIN tt.round o ON rt.muid = o.route_trajectory_muid
        JOIN tt.order_round r ON o.round_id = r.round_id
        JOIN tt.order_list ol ON r.order_list_id = ol.order_list_id
        JOIN tt.timetable_entry te ON o.timetable_entry_id = te.timetable_entry_id
        JOIN asd.oper_round_visit orv ON r.order_round_id = orv.order_round_id
    WHERE
        (ol.order_date BETWEEN i.inspection_begin_date AND i.inspection_end_date)
        AND i.route_inspection_id = p_route_inspection_id
        AND r.is_active = 1
        AND ol.is_active = 1 AND ol.sign_deleted = 0
        AND orv.order_round_status_id = 1
        AND (orv.time_plan_end BETWEEN i.inspection_begin_date AND i.inspection_end_date)
    GROUP BY te.timetable_entry_num, o.round_code, i.route_inspection_id, r.order_round_num, i.round_count,
        i.inspection_count
    ORDER BY te.timetable_entry_num;
END;
$function$;

