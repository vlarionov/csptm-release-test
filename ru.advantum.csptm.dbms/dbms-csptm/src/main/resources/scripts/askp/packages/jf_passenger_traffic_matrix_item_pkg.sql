create OR REPLACE function askp."jf_passenger_traffic_matrix_item_pkg$of_delete"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r askp.passenger_traffic_matrix_item%rowtype;
begin 
   l_r := askp.jf_passenger_traffic_matrix_item_pkg$attr_to_rowtype(p_attr);

   delete from  askp.passenger_traffic_matrix_item where  matrix_item_id = l_r.matrix_item_id;

   return null;
end;

$$;

create or REPLACE function askp."jf_passenger_traffic_matrix_item_pkg$attr_to_rowtype"(p_attr text) returns askp.passenger_traffic_matrix_item
LANGUAGE plpgsql
AS $$
declare 
   l_r askp.passenger_traffic_matrix_item%rowtype; 
begin 
   l_r.matrix_item_id := jofl.jofl_pkg$extract_number(p_attr, 'matrix_item_id', true); 
   l_r.passanger_traffic_matrix_id := jofl.jofl_pkg$extract_number(p_attr, 'passanger_traffic_matrix_id', true); 
   l_r.stop_from_muid := jofl.jofl_pkg$extract_number(p_attr, 'stop_from_muid', true); 
   l_r.stop_to_muid := jofl.jofl_pkg$extract_number(p_attr, 'stop_to_muid', true); 
   l_r.passenger_traffic := jofl.jofl_pkg$extract_number(p_attr, 'passenger_traffic', true);

   l_r.order_num_from := jofl.jofl_pkg$extract_number(p_attr, 'order_num_from', true);
   l_r.order_num_to := jofl.jofl_pkg$extract_number(p_attr, 'order_num_to', true);
  return l_r;
end;

$$;


create OR REPLACE  function askp."jf_passenger_traffic_matrix_item_pkg$of_insert"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r askp.passenger_traffic_matrix_item%rowtype;
begin 
   l_r := askp.jf_passenger_traffic_matrix_item_pkg$attr_to_rowtype(p_attr);

   insert into askp.passenger_traffic_matrix_item select l_r.*;

   return null;
end;

$$;


create or REPLACE function askp."jf_passenger_traffic_matrix_item_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
  l_traffic_matrix_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'traffic_matrix_id', true);
begin 
 open p_rows for 
      select 
        matrix_item_id,
        passanger_traffic_matrix_id, 
        stop_from_muid::TEXT,
        (select name from gis.stops where muid = (select stop_muid from gis.stop_places where muid = stop_from_muid)) stop_from,
        stop_to_muid::TEXT,
        (select name from gis.stops where muid = (select stop_muid from gis.stop_places where muid = stop_to_muid)) stop_to,
        passenger_traffic,
        order_num_from,
        order_num_to
      from askp.passenger_traffic_matrix_item where passanger_traffic_matrix_id = l_traffic_matrix_id;
end;
$$;


create OR REPLACE  function askp."jf_passenger_traffic_matrix_item_pkg$of_update"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r askp.passenger_traffic_matrix_item%rowtype;
begin 
   l_r := askp.jf_passenger_traffic_matrix_item_pkg$attr_to_rowtype(p_attr);

   update askp.passenger_traffic_matrix_item set 
          passanger_traffic_matrix_id = l_r.passanger_traffic_matrix_id, 
          stop_from_muid = l_r.stop_from_muid, 
          stop_to_muid = l_r.stop_to_muid, 
          passenger_traffic = l_r.passenger_traffic
   where 
          matrix_item_id = l_r.matrix_item_id;

   return null;
end;

$$;