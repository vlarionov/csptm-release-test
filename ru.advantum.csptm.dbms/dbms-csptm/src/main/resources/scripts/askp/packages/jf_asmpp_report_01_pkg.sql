create or replace function askp."jf_asmpp_report_01_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
language plpgsql
as $$
DECLARE
  l_rpt_dt timestamp := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_START_DT', true);
  l_end_dt timestamp := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_END_DT', true);
  l_route bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_route_muid', TRUE);
  l_week_day_key text := replace(jofl.jofl_pkg$extract_varchar(p_attr, 'F_WEEKDAY_INPLACE_K', true), ' ', '');
  l_week_day_name text := jofl.jofl_pkg$extract_varchar(p_attr, 'F_WEEKDAY_INPLACE_V', true);
  l_rep_value_key text := replace(jofl.jofl_pkg$extract_varchar(p_attr, 'F_REP_VALUE_INPLACE_K', true), ' ', '');
  l_round_normal int[] := array[1];
BEGIN
  if core.assigned(l_rpt_dt::text, l_end_dt::text, l_route::text) then
    OPEN p_rows FOR
      with   d as (
                  SELECT array_agg(a.asmpp_agg_id) id_arr, count(*) day_count
                  fROM askp.asmpp_agg a
                  where a.time_start in (
                                           select core.to_utc(dt)::date
                                           from ttb.calendar_item ci join ttb.calendar c on ci.calendar_id = c.calendar_id
                                           where ci.calendar_tag_id = any(string_to_array(l_week_day_key, ',')::int[])
                                             and c.dt between core.from_utc(l_rpt_dt)::date and core.from_utc(l_end_dt)::date + 1
                                        )
                    )

      select concat(to_char(core.from_utc(l_rpt_dt)::date, 'dd.mm.yyyy'), ' - ', to_char(core.from_utc(l_end_dt)::date, 'dd.mm.yyyy')) dperiod,
             l_week_day_name report_day,
             r.number,
             tt.short_name tr_type,
             case qq.direction_id when 1 then 'Прямое' when 2 then 'Обратное' end direction,
             qq.route_variants_muid,
             val_type,
             case val_type when 1 then 'Вход' when 2 then 'Выход' when 3 then 'Наполненность' end val_type_name,
             value_0,
             value_1,
             value_2,
             value_3,
             value_4,
             value_5,
             value_6,
             value_7,
             value_8,
             value_9,
             value_10,
             value_11,
             value_12,
             value_13,
             value_14,
             value_15,
             value_16,
             value_17,
             value_18,
             value_19,
             value_20,
             value_21,
             value_22,
             value_23
      from
         (
          select q.route_variants_muid, q.direction_id, val_type,
                 round(avg(rep_value[array_position(hour_num, 0)])) value_0,
                 round(avg(rep_value[array_position(hour_num, 1)])) value_1,
                 round(avg(rep_value[array_position(hour_num, 2)])) value_2,
                 round(avg(rep_value[array_position(hour_num, 3)])) value_3,
                 round(avg(rep_value[array_position(hour_num, 4)])) value_4,
                 round(avg(rep_value[array_position(hour_num, 5)])) value_5,
                 round(avg(rep_value[array_position(hour_num, 6)])) value_6,
                 round(avg(rep_value[array_position(hour_num, 7)])) value_7,
                 round(avg(rep_value[array_position(hour_num, 8)])) value_8,
                 round(avg(rep_value[array_position(hour_num, 9)])) value_9,
                 round(avg(rep_value[array_position(hour_num, 10)])) value_10,
                 round(avg(rep_value[array_position(hour_num, 11)])) value_11,
                 round(avg(rep_value[array_position(hour_num, 12)])) value_12,
                 round(avg(rep_value[array_position(hour_num, 13)])) value_13,
                 round(avg(rep_value[array_position(hour_num, 14)])) value_14,
                 round(avg(rep_value[array_position(hour_num, 15)])) value_15,
                 round(avg(rep_value[array_position(hour_num, 16)])) value_16,
                 round(avg(rep_value[array_position(hour_num, 17)])) value_17,
                 round(avg(rep_value[array_position(hour_num, 18)])) value_18,
                 round(avg(rep_value[array_position(hour_num, 19)])) value_19,
                 round(avg(rep_value[array_position(hour_num, 20)])) value_20,
                 round(avg(rep_value[array_position(hour_num, 21)])) value_21,
                 round(avg(rep_value[array_position(hour_num, 22)])) value_22,
                 round(avg(rep_value[array_position(hour_num, 23)])) value_23
          FROM
            (
              select q.route_variants_muid, q.direction_id, val_type, q.rep_day, --vt.val_type,
                     array_agg(case val_type when 1 then in_corrected when 2 then out_corrected when 3 then total_pass_after end order by q.hour_num) rep_value,
                     array_agg(hour_num order by q.hour_num)::int[] hour_num
              from
                (
                  select t.route_variants_muid, rnd.direction_id,
                         extract(hour from core.from_utc(t.time_start))::int hour_num,
                         date_trunc('day', core.from_utc(t.time_start)) rep_day,
                         sum(in_corrected) in_corrected, sum(out_corrected) out_corrected, avg(total_pass_after) total_pass_after,
                         d.day_count --,  t.*
                  from
                    askp.asmpp_agg_item t join tt.order_round ornd on t.order_round_id = ornd.order_round_id
                                          join tt.round rnd on rnd.round_id = ornd.round_id
                                          join d on t.asmpp_agg_id = any(d.id_arr)
                  where t.time_start >= l_rpt_dt and t.time_start < l_end_dt + interval '1 day'
                    and exists (select 1 from asd.oper_round_visit v where v.order_round_id = t.order_round_id and v.order_round_status_id = any(l_round_normal))
                  group by t.route_variants_muid, rnd.direction_id, hour_num, rep_day, d.day_count--, val_type
                ) q, (select unnest(string_to_array(l_rep_value_key, ',')::int[]) val_type) vt
              group by q.route_variants_muid, q.direction_id, q.rep_day, q.hour_num, vt.val_type
             ) q
           group by q.route_variants_muid, q.direction_id, val_type
         ) qq join  gis.route_variants rv on qq.route_variants_muid = rv.muid
              join  gis.routes r on rv.route_muid = r.muid
              join core.tr_type tt on tt.transport_kind_muid = r.transport_kind_muid
      where true
  --      and rv.route_muid = any('{2994044583790883061,3186194532325936221,2994045596099705555,3452591463131531765,3452591463131531765,2994042288974887619}'::bigint[])
        and rv.route_muid = any(l_route)
      order by number, tr_type, direction_id, val_type;
  else
    open p_rows for
      select null dperiod,
             null report_day,
             null number,
             null tr_type,
             null direction,
             null route_variants_muid,
             null val_type,
             null val_type_name,
             null value_0,
             null value_1,
             null value_2,
             null value_3,
             null value_4,
             null value_5,
             null value_6,
             null value_7,
             null value_8,
             null value_9,
             null value_10,
             null value_11,
             null value_12,
             null value_13,
             null value_14,
             null value_15,
             null value_16,
             null value_17,
             null value_18,
             null value_19,
             null value_20,
             null value_21,
             null value_22,
             null value_23
      where false;
  end if;
end;
$$;
