create or REPLACE function askp."jf_conf_pass_on_sp_detail_pkg$attr_to_rowtype"(p_attr text)
  returns askp.conf_pass_on_sp_detail
LANGUAGE plpgsql
AS $$
declare
  l_r askp.conf_pass_on_sp_detail%rowtype;
begin
  l_r.conf_pass_on_sp_datail_id := jofl.jofl_pkg$extract_number(p_attr, 'conf_pass_on_sp_datail_id', true);
  l_r.conf_pass_on_sp_id := jofl.jofl_pkg$extract_number(p_attr, 'conf_pass_on_sp_id', true);
  l_r.route_id := jofl.jofl_pkg$extract_number(p_attr, 'route_id', true);
  l_r.dt_from := jofl.jofl_pkg$extract_date(p_attr, 'dt_from', true) + ttb.get_utc_offset();
  l_r.dt_to := jofl.jofl_pkg$extract_date(p_attr, 'dt_to', true) + ttb.get_utc_offset();

  return l_r;
end;
$$;


create or REPLACE function askp."jf_conf_pass_on_sp_detail_pkg$of_delete"(p_id_account numeric, p_attr text)
  returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.conf_pass_on_sp_detail%rowtype;
begin
  l_r := askp.jf_conf_pass_on_sp_detail_pkg$attr_to_rowtype(p_attr);

  DELETE FROM askp.conf_pass_on_sp_detail WHERE conf_pass_on_sp_datail_id = l_r.conf_pass_on_sp_datail_id;

  return null;
end;
$$;

create or REPLACE function askp."jf_conf_pass_on_sp_detail_pkg$of_insert"(p_id_account numeric, p_attr text)
  returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.conf_pass_on_sp_detail%rowtype;
begin
  l_r := askp.jf_conf_pass_on_sp_detail_pkg$attr_to_rowtype(p_attr);

  l_r.conf_pass_on_sp_datail_id = nextval('askp.conf_pass_on_sp_detail_conf_pass_on_sp_datail_id_seq'::regclass);

  insert into askp.conf_pass_on_sp_detail select l_r.*;

  return null;
end;
$$;

create OR REPLACE function askp."jf_conf_pass_on_sp_detail_pkg$of_rows"(p_id_account numeric,
  OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
  l_conf_id BIGINT = jofl.jofl_pkg$extract_varchar(p_attr, 'conf_pass_on_sp_id', true);
begin
  open p_rows for
  select
    conf_pass_on_sp_datail_id,
    conf_pass_on_sp_id,
    route_id,
    dt_from,
    dt_to,
    route_num
  from askp.conf_pass_on_sp_detail
  LEFT JOIN rts.route USING(route_id)
  WHERE conf_pass_on_sp_id = l_conf_id;
end;
$$;

create or REPLACE function askp."jf_conf_pass_on_sp_detail_pkg$of_update"(p_id_account numeric, p_attr text)
  returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.conf_pass_on_sp_detail%rowtype;
begin
  l_r := askp.jf_conf_pass_on_sp_detail_pkg$attr_to_rowtype(p_attr);

  UPDATE askp.conf_pass_on_sp_detail SET
    route_id = l_r.route_id,
    dt_from = l_r.dt_from,
    dt_to = l_r.dt_to
    WHERE conf_pass_on_sp_datail_id = l_r.conf_pass_on_sp_datail_id;
  return null;
end;

$$;


