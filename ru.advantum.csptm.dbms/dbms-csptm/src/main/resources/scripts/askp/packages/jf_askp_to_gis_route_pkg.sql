CREATE or replace FUNCTION askp.jf_askp_to_gis_route_pkg$attr_to_rowtype(p_attr TEXT)
    RETURNS askp.ASKP_TO_GIS_ROUTE
LANGUAGE plpgsql
AS $$
DECLARE
    l_r askp.askp_to_gis_route%ROWTYPE;
BEGIN
    l_r.muid := jofl.jofl_pkg$extract_varchar(p_attr, 'muid', TRUE)::bigint;
    l_r.route_muid := jofl.jofl_pkg$extract_varchar(p_attr, 'route_muid', TRUE)::bigint;
    l_r.askp_route_id := jofl.jofl_pkg$extract_number(p_attr, 'askp_route_id', TRUE);
    l_r.askp_to_gis_route_id:=jofl.jofl_pkg$extract_number(p_attr, 'askp_to_gis_route_id', true);
    RETURN l_r;
END;
$$;

CREATE OR REPLACE FUNCTION askp.jf_askp_to_gis_route_pkg$of_insert(p_id_account NUMERIC,p_attr TEXT)
    RETURNS TEXT AS
$$
DECLARE
    l_r askp.askp_to_gis_route%ROWTYPE;
    l_route_id integer;
BEGIN
    l_r := askp.jf_askp_to_gis_route_pkg$attr_to_rowtype(p_attr);
    select
        r.route_id into l_route_id
        from rts.route2gis r
        where r.route_muid=l_r.route_muid
    limit 1;
    IF (SELECT count(a.askp_route_id)=1
              FROM askp.askp_to_gis_route a
              WHERE a.askp_route_id = l_r.askp_route_id or a.route_muid=l_r.route_muid)
    THEN RAISE EXCEPTION '<<Данный маршрут в АСКП и/или КСУПТ уже существует!>>';
    ELSE
        INSERT INTO askp.askp_to_gis_route (muid, askp_route_id, route_muid, rts_route_id) values (l_r.muid,l_r.askp_route_id,l_r.route_muid,l_route_id) ;
    END IF;
    RETURN NULL;
END;
$$
LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION askp."jf_askp_to_gis_route_pkg$of_rows"(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
    RETURNS REFCURSOR
AS $$
DECLARE
BEGIN
    OPEN p_rows FOR
    SELECT
        a.askp_to_gis_route_id,
        a.muid::text,
        a.askp_route_id,
        r.number AS route_number,
        k.short_name,
        a.route_muid::text
    FROM askp.askp_to_gis_route a
        LEFT JOIN gis.routes r ON a.route_muid = r.muid
    join gis.ref_transport_kinds k ON r.transport_kind_muid = k.muid;
END;
$$
LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION askp.jf_askp_to_gis_route_pkg$of_update(p_id_account NUMERIC, p_attr TEXT)
    RETURNS TEXT AS
$$
DECLARE
    l_r        askp.askp_to_gis_route%rowtype;
    l_route_id integer;
BEGIN
    l_r:=askp.jf_askp_to_gis_route_pkg$attr_to_rowtype(p_attr);
    select r.route_id
    into l_route_id
    from rts.route2gis r
    where r.route_muid = l_r.route_muid
    limit 1;

    if (select count(a.askp_route_id) = 1
        from askp.askp_to_gis_route a
        where (a.route_muid = l_r.route_muid or a.askp_route_id = l_r.askp_route_id)
              and a.askp_to_gis_route_id <> l_r.askp_to_gis_route_id)
    then
        raise exception 'Такой маршрут или маршрут АСКП существует';
    else
        update askp.askp_to_gis_route
        set
            muid          = l_r.muid,
            route_muid    = l_r.route_muid,
            rts_route_id  = l_route_id,
            askp_route_id = l_r.askp_route_id
        where
            askp_to_gis_route_id = l_r.askp_to_gis_route_id;
    end if;
    RETURN NULL;
END;
$$
LANGUAGE plpgsql VOLATILE;

/*
*  Вставка в таблицу соответсвия маршрутов АСКП и route_variant
   новых значений из таблицы транзакций по проездным билетам
*/
CREATE OR REPLACE FUNCTION askp.jf_askp_to_gis_route_pkg$of_create_from_askp(
    p_id_account NUMERIC,
    p_attr       TEXT)
    RETURNS BOOLEAN AS
$BODY$
DECLARE
BEGIN
    INSERT INTO askp.askp_to_gis_route (askp_route_id)
        SELECT DISTINCT temp.ID AS IDD
        FROM (SELECT route_id AS ID
              FROM askp.ticket_oper
              WHERE
                  check_time BETWEEN current_timestamp AND (current_timestamp - INTERVAL '30 day')
             ) temp
        EXCEPT SELECT askp_route_id
               FROM askp.askp_to_gis_route;
    RETURN TRUE;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;









