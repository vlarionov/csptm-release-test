CREATE OR REPLACE FUNCTION askp."jf_conf_pass_on_sp_pkg$of_rows"(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
BEGIN
  OPEN p_rows FOR
  select conf_pass_on_sp_id, stop_place_muid::TEXT, (t_from * 3600) t_from,
  (t_to * 3600) t_to, w_days, round_type_muid, code round_type_name, s.name stop_place_name, state,
  (select sg.stop_group_short_name
  from rts.stop2stop_group s2sg
    join rts.stop_group sg on sg.stop_group_id = s2sg.stop_group_id
         and sg.stop_group_id = any(array[8,9])
    JOIN rts.stop_location sl ON sl.stop_id = s2sg.stop_id
    JOIN rts.stop_item si ON si.stop_location_id = sl.stop_location_id
    JOIN rts.stop_item2gis si2gis ON si2gis.stop_item_id = si.stop_item_id
  WHERE si2gis.stop_place_muid = cp.stop_place_muid LIMIT 1) center_dir
  from askp.conf_pass_on_sp cp
  LEFT JOIN GIS.REF_ROUTE_ROUND_TYPES rrt ON rrt.muid = round_type_muid
  LEFT JOIN gis.stop_places sp ON sp.muid = stop_place_muid
  LEFT JOIN gis.stops s ON sp.stop_muid = s.muid;
END;
$$;

create or replace function askp."jf_conf_pass_on_sp_pkg$attr_to_rowtype"(p_attr text) returns askp.conf_pass_on_sp
LANGUAGE plpgsql
AS $$
declare
  l_r askp.conf_pass_on_sp%rowtype;
begin
  l_r.w_days := jofl.jofl_pkg$extract_varchar(p_attr, 'w_days', true);
  l_r.round_type_muid := jofl.jofl_pkg$extract_varchar(p_attr, 'round_type_muid', true);
  l_r.conf_pass_on_sp_id := jofl.jofl_pkg$extract_number(p_attr, 'conf_pass_on_sp_id', true);
  l_r.stop_place_muid := jofl.jofl_pkg$extract_number(p_attr, 'stop_place_muid', true);
  l_r.t_from := floor(jofl.jofl_pkg$extract_number(p_attr, 't_from', true) / 3600);
  l_r.t_to := floor(jofl.jofl_pkg$extract_number(p_attr, 't_to', true) / 3600);
  l_r.state := jofl.jofl_pkg$extract_varchar(p_attr, 'state', true);

  return l_r;
end;

$$;

create or REPLACE function askp."jf_conf_pass_on_sp_pkg$of_delete"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.conf_pass_on_sp%rowtype;
begin
  l_r := askp.jf_conf_pass_on_sp_pkg$attr_to_rowtype(p_attr);

  DELETE from askp.conf_pass_on_sp where conf_pass_on_sp_id = l_r.conf_pass_on_sp_id;

  return null;
end;
$$;

create or REPLACE function askp."jf_conf_pass_on_sp_pkg$of_insert"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.conf_pass_on_sp%rowtype;
begin
  l_r := askp.jf_conf_pass_on_sp_pkg$attr_to_rowtype(p_attr);

  l_r.conf_pass_on_sp_id = nextval('askp.conf_pass_on_sp_conf_pass_on_sp_id_seq'::regclass);

  insert into askp.conf_pass_on_sp select l_r.*;

  return null;
end;
$$;


create or REPLACE function askp."jf_conf_pass_on_sp_pkg$of_update"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.conf_pass_on_sp%rowtype;
begin
  l_r := askp.jf_conf_pass_on_sp_pkg$attr_to_rowtype(p_attr);

  UPDATE askp.conf_pass_on_sp SET
  stop_place_muid = l_r.stop_place_muid,
  t_from = l_r.t_from,
  t_to   = l_r.t_to,
  w_days = l_r.w_days,
  round_type_muid = l_r.round_type_muid
  WHERE conf_pass_on_sp_id = l_r.conf_pass_on_sp_id;
  return null;
end;
$$;


create or REPLACE function askp."jf_conf_pass_on_sp_pkg$of_calc"(p_id_account numeric, p_attr text) returns BOOLEAN
LANGUAGE plpgsql
AS $$
declare
  l_conf_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'conf_pass_on_sp_id', true);
  l_stop_place_muid BIGINT = jofl.jofl_pkg$extract_number(p_attr, 'stop_place_muid', true);
  l_r RECORD;
  l_time_from SMALLINT;
  l_time_to SMALLINT;
  l_round_type_muid SMALLINT;
  l_day_type TEXT;
begin
  --l_r := askp.jf_conf_pass_on_sp_pkg$attr_to_rowtype(p_attr);
  select t_from, t_to, round_type_muid, w_days INTO l_time_from, l_time_to, l_round_type_muid, l_day_type from askp.conf_pass_on_sp
  where conf_pass_on_sp_id = l_conf_id;

  DELETE FROM askp.pass_on_sp where conf_pass_on_sp_id = l_conf_id;

  FOR l_r IN select * from askp.conf_pass_on_sp_detail where conf_pass_on_sp_id = l_conf_id
  LOOP

    with t1 AS(
        select substring(hr_str from 1 for strpos(hr_str, ':') - 1)::SMALLINT h_from,
               substring(hr_str from (strpos(hr_str, '-') + 1) for strpos(hr_str, ':') - 1)::SMALLINT h_to,
          *
        from askp.asmpp_report_pkg$report_data(2, (jsonb_build_object('BGN_DT', l_r.dt_from - ttb.get_utc_offset() /*'2018-04-16 00:00:00'*/,
                                                                      'END_DT', l_r.dt_to - ttb.get_utc_offset() /*'2018-04-17 00:00:00'*/,
                                                                      /*'f_dir_MUID', '1',*/
                                                                      'f_route_id', l_r.route_id /*4790*/,
                                                                      'f_rtype_MUID', l_round_type_muid/*'1'*/,
                                                                      'F_WEEKDAY_INPLACE_K', l_day_type))::text) tt
    ),
        t2 AS(
          select sum(report_in) pass_in, sum(report_out) pass_out, sum(report_fullness) fulness,
                 avg(report_loading) load_mean, max(report_loading) load_max, direction_id
          from t1
          where stop_place_muid = l_stop_place_muid /*2993277465250441985*/
                AND h_from >= l_time_from/*10*/ AND h_from < l_time_to/*15*/-- время московское
          GROUP BY stop_place_muid, direction_id
      )
    INSERT INTO askp.pass_on_sp (pass_on_sp_id, route_id, conf_pass_on_sp_id,pass_in,pass_out,fulness,
                                 load_mean,load_max, direction_id)
      select nextval('askp.pass_on_sp_pass_on_sp_id_seq'::regclass), l_r.route_id, l_conf_id/*4*/ ,t2.pass_in, t2.pass_out, t2.fulness,
        t2.load_mean, t2.load_max, t2.direction_id
      from t2;

  END LOOP;

  UPDATE askp.conf_pass_on_sp set state = 'Готов' WHERE conf_pass_on_sp_id = l_conf_id;

  return TRUE;
end;
$$;
