﻿-- ========================================================================
-- Пакет функций для формирования отчетов по АСМПП (Анализ пассажиропотоков)
-- ========================================================================

create or replace function askp.asmpp_report_pkg$check_route_variant (p_route_id bigint, p_begin timestamp, p_end timestamp)
 returns boolean
 /* проверяет единственность действующего варианта маршрута в период [отчета] */
LANGUAGE plpgsql
AS $$
DECLARE
 l_result boolean := false;
 l_rv_count smallint;
begin
 select count(rv.muid)
 into l_rv_count
 from gis.route_variants rv
      join rts.route2gis r2g on r2g.route_muid = rv.route_muid
 where r2g.route_id = p_route_id and
       (rv.start_date is not null or rv.end_date is not null) and
       tsrange(rv.start_date, rv.end_date) && tsrange(p_begin, p_end);

  if l_rv_count in (0, 1)
   then l_result := true;
  end if;
  return l_result;
END;
$$

-- drop function askp.asmpp_report_pkg$report_data(NUMERIC,TEXT);
CREATE OR REPLACE FUNCTION askp.asmpp_report_pkg$report_data(p_id_account NUMERIC, p_attr TEXT)
    RETURNS TABLE (stop_number smallint,
                   move_direction text,
                   direction_id smallint,
                   stop_name text,
                   stop_place_muid bigint,
                   hr smallint,
                   hr_str text,
                   report_in bigint,
                   report_out bigint,
                   report_fullness bigint,
                   report_loading numeric(2))
LANGUAGE plpgsql
/*
TO DO:
 -- "нормализовать" данные - все ОП и все часы суток
 -- проверить отрицательную нагрузку - предположительно из-за неравномерности движения - рейс "растягивается" на несколько часов
*/
AS $$
DECLARE
 l_attr text;
 l_route_id      bigint     := jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', true);
 l_dt_start 	   timestamp  := jofl.jofl_pkg$extract_date(p_attr, 'BGN_DT', true);
 l_dt_end	 	     timestamp  := jofl.jofl_pkg$extract_date(p_attr, 'END_DT', true);
 l_dt_start_text text       := jofl.jofl_pkg$extract_varchar(p_attr, 'BGN_DT', true);
 l_dt_end_text	 text       := jofl.jofl_pkg$extract_varchar(p_attr, 'END_DT', true);
 -- траектория прямая, обратная
 l_direction SMALLINT := jofl.jofl_pkg$extract_number(p_attr, 'f_dir_MUID', true);
 -- вид рейса укороченный, экспресс и т.д.
 l_route_type BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_rtype_MUID', true);
 l_stop_arr bigint[];
 -- тип дня недели
  l_weekday_type text := jofl.jofl_pkg$extract_varchar(p_attr, 'F_WEEKDAY_INPLACE_K', true);
  l_weekday_list smallint[] := array[0,1,2,3,4,5,6];
  -- переменные для параметров-массивов
  f_list_route_id text;
  f_list_rtype_MUID text;
BEGIN
 -- проверяем действующий вариант маршрута
 if not askp.asmpp_report_pkg$check_route_variant(l_route_id, l_dt_start, l_dt_end)
  then raise exception '<<В заданном периоде 2 или более действующих варианта трассы маршрута>>';
 end if;

 -- работаем с параметрами отчета
 f_list_route_id := '$array['||l_route_id::text||']';
 f_list_rtype_MUID :=  '$array['||l_route_type::text||']';

 -- формируем параметры для запроса данных АСМПП
 l_attr :=(jsonb_build_object('BGN_DT', l_dt_start_text,
                              'END_DT', l_dt_end_text,
                              'f_dir_MUID', l_direction::text,
                              'f_list_route_id', f_list_route_id,
                              'f_list_rtype_MUID', f_list_rtype_MUID,
                              'F_BROKEN_INPLACE_K','K2'))::text;

  -- получаем список ОП для прямой и обратной траекторий "основного" рейса
  l_stop_arr := array(select distinct sp2rt.stop_place_muid
                        from rts.route2gis r2g
                        join gis.route_variants rv on rv.route_muid = r2g.route_muid
                        join gis.route_rounds rr on rr.route_variant_muid = rv.muid and
                                                    rr.route_round_type_muid = 1 /* константа!! */
                        join gis.route_trajectories rt on rt.route_round_muid = rr.muid
                        join gis.stop_place2route_traj sp2rt on sp2rt.route_trajectory_muid = rt.muid
                       where r2g.route_id = l_route_id and
                             tsrange(rv.start_date, rv.end_date) && tsrange(l_dt_start, l_dt_end));

 -- формируем массив дней недели
 case l_weekday_type
  when 'WD' then l_weekday_list := array[1,2,3,4,5];
  when 'WE' then l_weekday_list := array[0,6];
  else l_weekday_list := array[0,1,2,3,4,5,6];
 end case;

 return query
with
source_data as ( /* исходные данные для формирвоаня отчета */
                   select sdt.timetable_id,
                          sdt.tr_id,
                          sdt.seat_qty_total,
                          sdt.direction_id,
                          sdt.direction,
                          sdt.vid_muid,
                          sdt.vid,
                          sdt.stop_place_muid,
                          sdt.stop,
                          sdt.order_num,
                          sdt.time_start,
                          sdt.in_corrected,
                          sdt.out_corrected,
                          to_char(date_trunc('hour', sdt.time_start + interval '3 hours'), 'hh24:MI') || '-' ||
                          to_char(date_trunc('hour', sdt.time_start + interval '4 hour'), 'hh24:MI') as hr_str,
                          extract('hour' from sdt.time_start) as hr
                   from askp.asmpp_full_data_get_rows(2, l_attr) sdt
                   where ((l_route_type is null and sdt.stop_place_muid = any(l_stop_arr)  /* взять ВСЕ ОП из всех видов рейсов, совпадающие с ОП из основного вида */) or
                          (l_route_type is not null and sdt.vid_muid = l_route_type)) and
                          /* учитываем дни недели - рабочие/выходные/все */
                          extract ('dow' from sdt.time_start + interval '3 hours' )+1 = any (l_weekday_list)),
tr_hours as ( /* п.16 - количество ТС за час и суммарная вместимость */
             select dd.vid_muid,
                    dd.direction_id,
                    dd.hr,
                    count(1) as tr_count,
                    sum(dd.seat_qty_total) as sum_seat_qty
               from (select distinct
                            sd1.vid_muid,
                            sd1.direction_id,
                            sd1.hr,
                            sd1.tr_id,
                            sd1.seat_qty_total
                       from source_data sd1) dd
           group by dd.vid_muid,
                    dd.direction_id,
                    dd.hr),
run_round as (/* п.4 - количество "пройденных" рейсов, прошедших через каждую отстановку (по часам) и количество дней */
              select e.timetable_id,
                     r.direction_id,
                     rr.route_round_type_muid as vid_muid,
                     ofc.stop_place_muid,
                     ofc.stop_order_num,
                     extract('hour' from ofc.time_fact) as hr,
                     count(1) as stop_round_cnt,
                     count(distinct date_trunc('day', ol.order_date)) as days_cnt
          from tt.timetable_entry e
          join tt.order_list ol on ol.timetable_entry_id = e.timetable_entry_id
          join tt.order_round ord on ord.order_list_id = ol.order_list_id and
                                     ord.is_active = 1
          join tt.order_fact ofc on ofc.order_round_id = ord.order_round_id
          join asd.oper_round_visit orv on orv.order_round_id = ord.order_round_id and
                                           orv.order_round_status_id = 1::smallint /* "пройден" - заменить на константу! */
          /* направление */
          join tt.round r on r.timetable_entry_id = e.timetable_entry_id and
                             r.round_id = ord.round_id and
                             r.direction_id = coalesce(l_direction, r.direction_id)
          /* определеяем тип рейса */
          join gis.route_variants rv ON rv.muid = e.route_variant_muid
          join gis.route_rounds rr ON rr.route_variant_muid = rv.muid and
                                      rr.code = r.round_code
          where e.timetable_id in (select distinct timetable_id from source_data) and
                ol.order_date between l_dt_start + interval '3 hours' and l_dt_end + interval '3 hours' and
                /* учитываем дни недели - рабочие/выходные/все */
                extract ('dow' from ol.order_date)+1 = any (l_weekday_list)
       group by e.timetable_id,
                r.direction_id,
                rr.route_round_type_muid,
                ofc.stop_place_muid,
                ofc.stop_order_num,
                extract('hour' from ofc.time_fact)),
proc_data_step1 as ( /* рассчитанные значения */
                    select sd2.hr,
                           sd2.hr_str,
                           sd2.stop_place_muid,
                           sd2.stop,
                           sd2.order_num,
                           sd2.direction_id,
                           sd2.direction,
                           sd2.vid_muid,
                           sd2.vid,
                           count(1) as cnt_round,                                        -- п.2        - количетсво рейсов на остановке (за час)
                           sum(sd2.in_corrected) as sum_inc,                             -- п.1        - количество ВОШЕДШИХ на остановке (за час)
                           round(sum(sd2.in_corrected)/count(1), 3) as avg_in_round,     -- п.3        - вошедшие на одном "условном" рейсе на остановке
                           sum(sd2.out_corrected) as sum_outc,                           -- п.7 -> п.1 - количество ВЫШЕДШИХ на остановке (за час)
                           round(sum(sd2.out_corrected)/count(1), 3) as avg_out_round    -- п.7 -> п.3 - вышедшие на одном "условном" рейсе на остановке
                      from source_data sd2
                  group by sd2.hr,
                           sd2.hr_str,
                           sd2.stop_place_muid,
                           sd2.stop,
                           sd2.order_num,
                           sd2.direction_id,
                           sd2.direction,
                           sd2.vid_muid,
                           sd2.vid),
proc_data_step3 as ( /* поправочные коэффициенты */
                    select pd2.*,
                           case sign(pd2.sum_in_final - pd2.sum_out_final)
                            when 0 then 1
                            when 1 then  1-abs(pd2.sum_in_final - pd2.sum_out_final)/pd2.sum_in_final
                            when -1 then 1
                           end as k1,
                           case sign(pd2.sum_in_final - pd2.sum_out_final)
                            when 0 then 1
                            when 1 then 1
                            when -1 then 1-abs(pd2.sum_in_final - pd2.sum_out_final)/pd2.sum_out_final
                           end as k2
                    from (select pd1.*,
                                 rc.days_cnt,
                                 rc.stop_round_cnt,
                                 pd1.avg_in_round * (rc.stop_round_cnt/rc.days_cnt) as hour_stop_in,       -- п.5        - рассчетное количество ВОШЕДШИХ пассажиров на ОП за час за весь период
                                 pd1.avg_out_round * (rc.stop_round_cnt/rc.days_cnt) as hour_stop_out,     -- п.7 -> п.5 - рассчетное количество ВЫШЕДШИХ пассажиров на ОП за час за весь период
                                 sum(pd1.avg_in_round * (rc.stop_round_cnt/rc.days_cnt)) over (partition by pd1.direction_id, pd1.vid_muid, pd1.hr order by pd1.order_num rows between unbounded preceding and unbounded following) as sum_in_final,   -- п.8 - итоговое (рассчетное) количество ВОШЕДШИХ за час за период по ВСЕМ ОП
                                 sum(pd1.avg_out_round * (rc.stop_round_cnt/rc.days_cnt)) over (partition by pd1.direction_id, pd1.vid_muid, pd1.hr order by pd1.order_num rows between unbounded preceding and unbounded following) as sum_out_final,  -- п.8 - итоговое (рассчетное) количество ВЫШЕДШИХ за час за период по ВСЕМ ОП
                                 (trh.sum_seat_qty/trh.tr_count) * (rc.stop_round_cnt/rc.days_cnt) as hour_capacity -- п.17 = п.16 * п.4
                            from proc_data_step1 pd1
                       left join run_round rc on rc.direction_id = pd1.direction_id and
                                                 rc.stop_place_muid = pd1.stop_place_muid and
                                                 rc.vid_muid = pd1.vid_muid and
                                                 rc.stop_order_num = pd1.order_num and
                                                 rc.hr = pd1.hr
                       left join tr_hours trh on trh.direction_id = pd1.direction_id and
                                                  trh.vid_muid = pd1.vid_muid and
                                                  trh.hr = pd1.hr) pd2)
select pd3.order_num::smallint as stop_number,
       pd3.direction as move_direction,
       pd3.direction_id as direction_id,
       pd3.stop as stop_name,
       pd3.stop_place_muid::bigint as stop_place_muid,
       pd3.hr::smallint as hr, -- часы UTC в числовом представлении
       pd3.hr_str::text,       -- часы MSK в строке ('NN:00-MM:00' MM=NN+1)
       round(pd3.hour_stop_in * pd3.k1)::bigint as report_in,
       round(pd3.hour_stop_out * pd3.k2)::bigint as report_out,
       (case
         when pd3.order_num = max(pd3.order_num) over (partition by pd3.vid_muid, pd3.direction_id) then 0
         else sum(round(pd3.hour_stop_in * pd3.k1) - round(pd3.hour_stop_out * pd3.k2)) over (partition by pd3.vid_muid, pd3.direction_id, pd3.hr order by pd3.order_num rows between unbounded preceding and current row)
       end)::bigint as report_fullness,
--        (sum(round(pd3.hour_stop_in * pd3.k1) - round(pd3.hour_stop_out * pd3.k2)) over (partition by pd3.vid_muid, pd3.direction_id, pd3.hr order by pd3.order_num rows between unbounded preceding and current row))::bigint as report_fullness,
       (round((sum(round(pd3.hour_stop_in * pd3.k1) - round(pd3.hour_stop_out * pd3.k2)) over (partition by pd3.vid_muid, pd3.direction_id, pd3.hr order by pd3.order_num rows between unbounded preceding and current row)) / pd3.hour_capacity * 100, 2))::numeric as report_loading
from proc_data_step3 pd3 ;
END;
$$
