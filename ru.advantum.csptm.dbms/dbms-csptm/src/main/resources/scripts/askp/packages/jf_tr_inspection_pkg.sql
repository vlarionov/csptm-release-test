CREATE OR REPLACE FUNCTION askp.jf_tr_inspection_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
    RETURNS REFCURSOR
LANGUAGE plpgsql
AS $function$
DECLARE
    l_visit_tr_stop_id askp.visit_tr_stop.visit_tr_stop_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'visit_tr_stop_id', TRUE) :: INTEGER;
BEGIN
    OPEN p_rows FOR
    SELECT
        vts.visit_tr_stop_id,
        t.tr_inspection_id,
        t.tr_id,
        t.visit_tr_stop_id,
        ct.garage_num,
        ct.licence,
        e.name_short,
        ct.seat_qty_total AS qty
    FROM askp.tr_inspection t
        JOIN askp.visit_tr_stop vts ON t.visit_tr_stop_id = vts.visit_tr_stop_id
        JOIN core.tr ct ON t.tr_id = ct.tr_id
        JOIN core.depo cd ON ct.depo_id = cd.depo_id
        JOIN core.entity e ON cd.depo_id = e.entity_id
    WHERE t.visit_tr_stop_id = l_visit_tr_stop_id;
END;
$function$;

--------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION askp.jf_tr_inspection_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
    RETURNS TEXT
LANGUAGE plpgsql
AS $function$
DECLARE
    l_tr_inspection_id askp.tr_inspection.tr_inspection_id%TYPE :=jofl.jofl_pkg$extract_number(p_attr, 'tr_inspection_id', TRUE);
BEGIN
    DELETE FROM askp.tr_inspection
    WHERE tr_inspection_id = l_tr_inspection_id;
    RETURN NULL;
END;
$function$;

--------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION askp.jf_tr_inspection_pkg$of_action_insert(p_id_account NUMERIC, p_attr TEXT)
    RETURNS text
LANGUAGE plpgsql
AS $function$
DECLARE
    l_visit_tr_stop_id askp.tr_inspection.visit_tr_stop_id%TYPE :=jofl.jofl_pkg$extract_number(p_attr, 'visit_tr_stop_id', TRUE);
    l_f_tr_id          BIGINT [] :=jofl.jofl_pkg$extract_narray(p_attr, 'f_tr_id', TRUE);
BEGIN
    INSERT into askp.tr_inspection (tr_id, visit_tr_stop_id)
    SELECT arr_tr_id,l_visit_tr_stop_id
    FROM unnest(l_f_tr_id) arr_tr_id;
    return null;
END;
$function$;
