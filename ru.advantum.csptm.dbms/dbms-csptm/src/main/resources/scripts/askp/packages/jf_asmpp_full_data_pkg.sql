CREATE OR REPLACE FUNCTION askp."jf_asmpp_full_data_pkg$of_rows"(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
BEGIN
 OPEN p_rows FOR
  select f.name_short,
         f.garage_num,
         f.seat_qty_total,
         f.short_name,
         f.number,
         f.timetable_entry_num,
         f.order_round_num,
         f.period,
         f.vid,
         f.direction,
         f.stop,
         f.order_num,
         f.time_start,
         f.in_corrected,
         f.out_corrected,
         f.total_pass,
         f.broken,
         f.is_broken, --, l_broken_rounds
         f.sum_out,
         f.sum_in,
         f.raw_in,
         f.raw_out
    from askp.asmpp_full_data_get_rows(p_id_account , p_attr) f;
END;
$$;
