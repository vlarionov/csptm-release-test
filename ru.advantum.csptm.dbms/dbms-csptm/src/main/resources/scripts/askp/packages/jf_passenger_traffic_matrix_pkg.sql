create or replace function askp."jf_passenger_traffic_matrix_pkg$attr_to_rowtype"(p_attr text) returns askp.passenger_traffic_matrix
LANGUAGE plpgsql
AS $$
declare
  l_r askp.passenger_traffic_matrix%rowtype;
begin
  l_r.route_variant_muid := jofl.jofl_pkg$extract_varchar(p_attr, 'route_variant_muid', true);
  l_r.route_round_type_muid := jofl.jofl_pkg$extract_number(p_attr, 'route_round_type_muid', true);
  l_r.route_round_muid := jofl.jofl_pkg$extract_number(p_attr, 'route_round_muid', true);
  l_r.traffic_matrix_id := jofl.jofl_pkg$extract_number(p_attr, 'traffic_matrix_id', true);
  l_r.route_trajectory_type_muid := jofl.jofl_pkg$extract_number(p_attr, 'route_trajectory_type_muid', true);
  l_r.time_from := jofl.jofl_pkg$extract_date(p_attr, 'time_from', true);
  l_r.time_to := jofl.jofl_pkg$extract_date(p_attr, 'time_to', true);
  l_r.days_of_week := jofl.jofl_pkg$extract_varchar(p_attr, 'days_of_week', true);
  l_r.period_from := jofl.jofl_pkg$extract_date(p_attr, 'period_from', true) + INTERVAL '3 hour';
  l_r.period_to := jofl.jofl_pkg$extract_date(p_attr, 'period_to', true) + INTERVAL '3 hour';

  l_r.k_time_from := jofl.jofl_pkg$extract_date(p_attr, 'k_time_from', true);
  l_r.k_time_to := jofl.jofl_pkg$extract_date(p_attr, 'k_time_to', true);
  l_r.k_period_from := jofl.jofl_pkg$extract_date(p_attr, 'k_period_from', true) +  INTERVAL '3 hour';
  l_r.k_period_to := jofl.jofl_pkg$extract_date(p_attr, 'k_period_to', true) + INTERVAL '3 hour';
  l_r.k_days_of_week := jofl.jofl_pkg$extract_varchar(p_attr, 'k_days_of_week', true);

  l_r.creation_date := jofl.jofl_pkg$extract_date(p_attr, 'creation_date', true);
  l_r.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true);
  l_r.code := jofl.jofl_pkg$extract_varchar(p_attr, 'code', true);
  return l_r;
end;
$$;


create or replace function askp."jf_passenger_traffic_matrix_pkg$of_delete"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.passenger_traffic_matrix%rowtype;
begin
  l_r := askp.jf_passenger_traffic_matrix_pkg$attr_to_rowtype(p_attr);

  delete from  askp.passenger_traffic_matrix where  traffic_matrix_id = l_r.traffic_matrix_id;

  return null;
end;

$$;


create or replace function askp."jf_passenger_traffic_matrix_pkg$of_insert"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.passenger_traffic_matrix%rowtype;
begin
  l_r := askp.jf_passenger_traffic_matrix_pkg$attr_to_rowtype(p_attr);

  l_r.traffic_matrix_id = nextval('askp.passenger_traffic_matrix_id_seq'::regclass);
  l_r.creation_date = now();

  insert into askp.passenger_traffic_matrix select l_r.*;

  return null;
end;
$$;

create or replace function askp."jf_passenger_traffic_matrix_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
begin
  open p_rows for
  select
    (select number from gis.routes r
      JOIN gis.route_variants rv ON rv.route_muid = r.muid
           AND rv.muid = p.route_variant_muid limit 1) as number,
    /*(select number from gis.routes r
            JOIN gis.route_variants rv ON rv.route_muid = r.muid
                 AND rv.muid = route_variant_muid limit 1) as  */
    p.route_variant_muid::TEXT,
    p.route_round_type_muid::TEXT,
    p.route_round_muid::TEXT,
    rtt.name AS direction,
    rrt.name AS route_type,
    p.traffic_matrix_id,
    p.route_trajectory_type_muid::TEXT,
    p.time_from ,
    p.time_to,
    p.days_of_week,
    p.period_from,
    p.period_to,
    p.creation_date,
    p.comment,
    p.k_period_from,
    p.k_period_to,
    p.k_time_from,
    p.k_time_to,
    p.k_days_of_week,
    p.code,
    tc.muid as tr_type_id
  from askp.passenger_traffic_matrix p
  JOIN gis.ref_route_trajectory_types rtt ON p.route_trajectory_type_muid = rtt.muid
       AND rtt.sign_deleted = 0
  JOIN gis.ref_route_round_types rrt ON p.route_round_type_muid = rrt.muid
  LEFT JOIN gis.route_variants rv ON rv.muid=p.route_variant_muid
  LEFT JOIN gis.routes r ON rv.route_muid = r.muid
  AND r.agency_muid = 1 -- только МГТ
  AND r.sign_deleted = 0
  LEFT JOIN gis.ref_transport_kinds tc ON r.transport_kind_muid = tc.muid;
end;
$$;


create or replace function askp."jf_passenger_traffic_matrix_pkg$of_update"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.passenger_traffic_matrix%rowtype;
begin
  l_r := askp.jf_passenger_traffic_matrix_pkg$attr_to_rowtype(p_attr);

  update askp.passenger_traffic_matrix set
    route_variant_muid = l_r.route_variant_muid,
    route_round_type_muid = l_r.route_round_type_muid,
    route_round_muid = l_r.route_round_muid,
    route_trajectory_type_muid = l_r.route_trajectory_type_muid,
    time_from = l_r.time_from,
    time_to = l_r.time_to,
    days_of_week = l_r.days_of_week,
    period_from = l_r.period_from,
    period_to = l_r.period_to,
    creation_date = l_r.creation_date,
    comment = l_r.comment,

    k_time_from = l_r.k_time_from,
    k_time_to = l_r.k_time_to,
    k_period_from = l_r.k_period_from,
    k_period_to = l_r.k_period_to,
    k_days_of_week = l_r.k_days_of_week,
    code = l_r.code
  where
    traffic_matrix_id = l_r.traffic_matrix_id;
  return null;
end;
$$;


create or REPLACE function askp."jf_passenger_traffic_matrix_pkg$of_calc"
  (p_id_account numeric, p_attr text) returns boolean
LANGUAGE plpgsql
AS $$
DECLARE

  l_traffic_matrix_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'traffic_matrix_id', true);
  l_route_variant_muid BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'route_variant_muid', true);
  l_route_trajectory_type_muid BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'route_trajectory_type_muid', true);
  l_route_round_type_muid BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'route_round_type_muid', true);

  l_sp_num_on_route SMALLINT;
  l_koeff double precision;
  l_prev_koeff double precision;
  l_stop_from_muid BIGINT;
  -- не исторический период из таблицы, а параметр расчета
  l_history_from timestamp := jofl.jofl_pkg$extract_date(p_attr, 'k_period_from', true);--to_timestamp(asd.get_constant('ASMPP_history_period_from'));
  l_history_to timestamp := jofl.jofl_pkg$extract_date(p_attr, 'k_period_to', true);--to_timestamp(asd.get_constant('ASMPP_history_period_to'));

  l_time_from TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'k_time_from', true) + INTERVAL '3 hour' ;
  l_time_to TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'k_time_to', true) + INTERVAL '3 hour';

  l_hour_from SMALLINT := EXTRACT(hour FROM l_time_from);
  l_hour_to   SMALLINT := EXTRACT(hour FROM l_time_to);

  l_min_from SMALLINT := EXTRACT(min FROM l_time_from);
  l_min_to   SMALLINT := EXTRACT(min FROM l_time_to);

  l_code TEXT := jofl.jofl_pkg$extract_varchar(p_attr, 'code', true);
  -- начало и конец периода за который вычисляется пассажиропоток
  l_period_from_p TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'period_from', true)/* + INTERVAL '3 hour'*/;
  l_period_to_p   TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'period_to', true)/* + INTERVAL '3 hour'*/;

  l_period_time_from_p TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'time_from', true) + INTERVAL '3 hour';
  l_period_time_to_p   TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'time_to', true) + INTERVAL '3 hour';

  l_hour_from_p SMALLINT := EXTRACT(hour FROM l_period_time_from_p);
  l_hour_to_p   SMALLINT := EXTRACT(hour FROM l_period_time_to_p);

  l_min_from_p SMALLINT := EXTRACT(min FROM l_period_time_from_p);
  l_min_to_p   SMALLINT := EXTRACT(min FROM l_period_time_to_p);

  -- допустимое отклонение АСКП и АСМПП
  l_delta SMALLINT := asd.get_constant('ASMPP_ASKP_DELTA');

  l_dow TEXT := jofl.jofl_pkg$extract_number(p_attr, 'days_of_week', true);
  l_dow_p TEXT := jofl.jofl_pkg$extract_number(p_attr, 'k_days_of_week', true);
    -- массив цифр, типа {2,5}
  l_dow_arr SMALLINT[] := array(select DISTINCT unnest((regexp_matches(l_dow, '\d{1}', 'g'))));
  l_dow_arr_p SMALLINT[] := array(select DISTINCT unnest((regexp_matches(l_dow_p, '\d{1}', 'g'))));
BEGIN
  DELETE FROM askp.passenger_traffic_matrix_item WHERE passanger_traffic_matrix_id = l_traffic_matrix_id;

  INSERT INTO askp.passenger_traffic_matrix_item(passanger_traffic_matrix_id, order_num_from, stop_from_muid, stop_to_muid, order_num_to, passenger_traffic)
  with a AS (select rr.*, sp2rt.* , sp.*, rt.trajectory_type_muid
    from gis.route_rounds rr
    JOIN gis.route_trajectories rt ON rt.route_round_muid = rr.muid
    JOIN gis.stop_place2route_traj sp2rt ON sp2rt.route_trajectory_muid = rt.muid
    JOIN gis.stop_places sp ON sp.muid = sp2rt.stop_place_muid
  where route_variant_muid = l_route_variant_muid
        AND rr.code = l_code -- у одного варианта маршрута много разных кодов
        AND rr.route_round_type_muid = l_route_round_type_muid
        ORDER BY route_trajectory_muid, order_num),
    --select * from a;
    b as (
      select a.stop_place_muid, a.route_trajectory_muid, a.route_variant_muid, a.order_num, a.trajectory_type_muid,
             sum(COALESCE(in_corrected, 0)) sum_in, sum(COALESCE(out_corrected,0)) sum_out
             from a
      LEFT JOIN askp.asmpp_agg_item agg ON agg.stop_place_muid = a.stop_place_muid
                AND agg.route_variants_muid = a.route_variant_muid
                AND agg.time_start BETWEEN /*'2017-11-01' AND '2017-11-18'*/l_history_from AND l_history_to -- исторический период
                AND (EXTRACT(hour FROM agg.time_start) * 60 + EXTRACT(min FROM agg.time_start)) -- по времени
                     BETWEEN (l_hour_from * 60 + l_min_from) AND (l_hour_to * 60 + l_min_to)
                AND ((EXTRACT(ISODOW FROM agg.time_start) = ANY(l_dow_arr)) OR l_dow IS NULL) -- фильтр по дню недели
                AND (time_finish - time_start) < INTERVAL '30 minutes'
      GROUP BY a.stop_place_muid, a.route_variant_muid, order_num, trajectory_type_muid, route_trajectory_muid
               order by trajectory_type_muid, order_num
    ),
    --select * from b;
    c AS(
        select *, sum(sum_out) OVER (PARTITION BY route_trajectory_muid) as total_out_sum from b where trajectory_type_muid = l_route_trajectory_type_muid
    ),
    --select * from c;
    q AS (select stop_place_muid, order_num, sum_out/total_out_sum koeff from c where total_out_sum > 0)
    --select * from q;
    select l_traffic_matrix_id as passanger_traffic_matrix_id, 1 as order_num_from, (SELECT stop_place_muid from q where order_num = 1) AS stop_from_muid,
           stop_place_muid as stop_to_muid, order_num as order_num_to, koeff passenger_traffic
    from q where order_num > 1;

    -- записали в базу коэфф от первой остановки до последующих
    SELECT MAX(order_num_to) INTO l_sp_num_on_route FROM askp.passenger_traffic_matrix_item t where t.passanger_traffic_matrix_id = l_traffic_matrix_id;
    IF l_sp_num_on_route IS NULL THEN
      RETURN FALSE;
    END IF;
    -- для всех остановок кроме первой и последней
    FOR i IN 2..(l_sp_num_on_route - 1) LOOP
      SELECT passenger_traffic, stop_to_muid INTO l_prev_koeff, l_stop_from_muid FROM askp.passenger_traffic_matrix_item t
             where t.passanger_traffic_matrix_id = l_traffic_matrix_id
             AND t.order_num_from = (i-1) AND t.order_num_to = i;
      l_koeff :=  l_prev_koeff/(l_sp_num_on_route - i);

      INSERT INTO askp.passenger_traffic_matrix_item(passanger_traffic_matrix_id, order_num_from, stop_from_muid, stop_to_muid, order_num_to, passenger_traffic)
      SELECT t.passanger_traffic_matrix_id, i as order_num_from, l_stop_from_muid as stop_from_muid, stop_to_muid, order_num_to, passenger_traffic + l_koeff
               FROM askp.passenger_traffic_matrix_item t where t.passanger_traffic_matrix_id = l_traffic_matrix_id
               AND order_num_from = (i-1) AND order_num_to > i;
    END LOOP;
    -- рассчитали коэффициенты за исторический период
    -- теперь нужно коэффициенту умножить на количество ВОшедших на ОП с заданным номером
    -- суммируем количество вошедших на данной остановке, по каждой остановке маршрута

  with a AS (select rr.*, sp2rt.* , sp.*, rt.trajectory_type_muid
             from gis.route_rounds rr
               JOIN gis.route_trajectories rt ON rt.route_round_muid = rr.muid
                    AND rt.trajectory_type_muid = l_route_trajectory_type_muid--2 -- прямая/ обратная
                    AND rt.sign_deleted = 0
               JOIN gis.stop_place2route_traj sp2rt ON sp2rt.route_trajectory_muid = rt.muid
                    AND sp2rt.sign_deleted = 0
               JOIN gis.stop_places sp ON sp.muid = sp2rt.stop_place_muid
                    AND sp.sign_deleted = 0
             where route_variant_muid = l_route_variant_muid--3674514414839751083
                   AND rr.route_round_type_muid = l_route_round_type_muid--2
                   AND rr.sign_deleted = 0
                   AND rr.code = l_code
    ),
      b as (
        select agg.stop_place_muid, order_num,
          (SELECT COUNT(*) from askp.validation2stops_binding bind
          where bind.time_fact BETWEEN l_time_from AND l_time_to
                AND bind.order_round_id = agg.order_round_id AND bind.stop_order_num = agg.stop_order_num) count, in_corrected,
          SUM(in_corrected)  OVER (PARTITION BY agg.order_round_id) inn_summ,-- сколько за рейс вошло
          SUM(out_corrected) OVER (PARTITION BY agg.order_round_id) out_summ -- и вышло
        from askp.asmpp_agg_item agg
          JOIN a ON a.stop_place_muid = agg.stop_place_muid
        where agg.route_variants_muid = l_route_variant_muid
              AND agg.time_start BETWEEN l_period_from_p AND l_period_to_p
              AND (EXTRACT(hour FROM agg.time_start) * 60 + EXTRACT(min FROM agg.time_start)) -- по времени
              BETWEEN (l_hour_from_p * 60 + l_min_from_p) AND (l_hour_to_p * 60 + l_min_to_p)
              AND ((EXTRACT(ISODOW FROM agg.time_start) = ANY(l_dow_arr_p)) OR l_dow_p IS NULL) -- фильтр по дню недели
              AND (time_finish - time_start) < INTERVAL '30 minutes'
              AND trajectory_type_muid =  l_route_trajectory_type_muid
    ),
    --select * from b;
      qq AS(select order_num, stop_place_muid,
              SUM (CASE WHEN (@(COALESCE(in_corrected,0) - count) > l_delta) THEN count
                   ELSE ceil((count + COALESCE(in_corrected,0))/2) END) AS sum_in/*in_corrected2*/
            from b -- l_in_out_defference анализ на превышение расхождение вошло/вышло
            GROUP BY order_num, stop_place_muid
    ),
      dd AS (select /*order_num as sort, order_num||' '||(select name from gis.stops where muid = (select stop_muid from gis.stop_places where muid = /*b*/qq.stop_place_muid)) AS order_num,*/
                    --t.order_num_to ||'|'||t.order_num_to::TEXT AS order_num_to,
                   /* CASE WHEN (sum_in * t.passenger_traffic > 1) THEN
                      round((sum_in * t.passenger_traffic)::NUMERIC, 2)
                    ELSE round((sum_in * t.passenger_traffic)::NUMERIC, 5)
                    END  as pass,*/
                    sum_in * t.passenger_traffic as pass,
                    /*'[' || json_build_object('CAPTION', 'На карте',
                                             'JUMP', json_build_object('METHOD', 'askp.map_traffic',
                                                                       'ATTR', json_build_object('matrix_id', t.passanger_traffic_matrix_id)::TEXT ) )|| ']' as url,*/
               t.stop_from_muid/*::TEXT*/, t.stop_to_muid/*::TEXT*/, t.passanger_traffic_matrix_id
             from qq/*b*/ -- порядковый номер остановки, количество вошедших
               JOIN askp.passenger_traffic_matrix_item t ON t.order_num_from = /*b*/qq.order_num
                    AND t.passanger_traffic_matrix_id = l_traffic_matrix_id)/*,
    result AS (
      select *, round(sum(pass) OVER (PARTITION BY sort) ::NUMERIC, 2) as sum from dd
    )*/
    --select * from result;
    UPDATE askp.passenger_traffic_matrix_item mi SET passengers =
    (SELECT pass from dd r where r.stop_from_muid = mi.stop_from_muid AND r.stop_to_muid = mi.stop_to_muid
     AND mi.passanger_traffic_matrix_id = l_traffic_matrix_id);

    return true;
END;
$$;


create or REPLACE function askp."jf_passenger_traffic_matrix_pkg$delete_items"
(p_traffic_matrix_id BIGINT) returns void
LANGUAGE plpgsql
AS $$
DECLARE

BEGIN
  DELETE FROM askp.passenger_traffic_matrix_item WHERE passanger_traffic_matrix_id = p_traffic_matrix_id;
END;
$$;
