CREATE OR REPLACE FUNCTION askp."jf_askp_asmpp_pkg$of_rows"(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
-- DECLARE
BEGIN
 OPEN p_rows FOR
  select
   tp,
   garage_num,
   seat_qty_total,
   tr_type,
   number,
   move_num,
   order_round_num,
   period,
   vid,
   direction,
   stop,
   order_num,
   time_fact,
   "interval",
   count,
   in_cnt,
   out_cnt,
   total_pass_after,
   total_pass_before,
   order_round_id,
   in_corrected,     -- CASE WHEN is_end = 1 THEN 0 ELSE in_corrected END
   out_corrected,    -- CASE WHEN is_end = 1 THEN corrected_total2 ELSE out_corrected END
   corrected_total,  -- CASE WHEN is_end = 1 THEN 0 ELSE corrected_total END
   corrected_total2  -- CASE WHEN is_end = 1 THEN 0 ELSE corrected_total2 END
  from askp.askp_asmpp_get_rows(p_id_account, p_attr);
END;
$$;
