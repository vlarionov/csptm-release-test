create or replace function askp."jf_f_move_by_route_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
   l_rep_dt date := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_DT', TRUE)  + interval '3 hours';
   l_start_dt date := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_START_DT', TRUE)  + interval '3 hours';
   l_end_dt date := jofl.jofl_pkg$extract_date(p_attr, 'END_DT', TRUE)  + interval '3 hours';
   l_route_id bigint[] := jofl.jofl_pkg$extract_tarray(p_attr, 'F_route_muid', TRUE);
begin
  l_rep_dt := coalesce(l_rep_dt, l_start_dt);
  open p_rows for
     select distinct te.timetable_entry_num move_num, p_attr dt
     from tt.order_list o join  tt.timetable_entry te on o.timetable_entry_id = te.timetable_entry_id
        join gis.route_variants rv on te.route_variant_muid = rv.muid
     where  order_date between l_rep_dt and coalesce(l_end_dt, l_rep_dt)
       and rv.route_muid = any(l_route_id)
       and o.sign_deleted = 0
     order by te.timetable_entry_num;
end;
$$;
