CREATE or replace FUNCTION askp.jf_map_traffic_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
LANGUAGE plpgsql
AS $$
declare
  l_matrix_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'matrix_id', true);
  l_tr_type_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'tr_type_id', true);
  l_sort BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'sort', true);
begin
  open p_rows for
  WITH a AS (select rt.muid::TEXT,
             (select max(order_num_to) from askp.passenger_traffic_matrix_item where passanger_traffic_matrix_id = l_matrix_id AND order_num_from = 1)
             from askp.passenger_traffic_matrix mx
    JOIN gis.route_trajectories rt ON rt.route_round_muid = mx.route_round_muid
         AND rt.trajectory_type_muid = mx.route_trajectory_type_muid
         AND mx.traffic_matrix_id = l_matrix_id),
  b AS (select order_num_to, si2gf.stop_item_id::TEXT as stop_place_muid, si2gt.stop_item_id::TEXT as next_stop,
         /*passenger_traffic::TEXT*/jofl.jofl_pkg$extract_number(p_attr, 'col'||((select max from a limit 1) - order_num_to), true) as amount,
         '["'||si2gf.stop_item_id::TEXT||'"]' as start_points,
         (select muid from a) as trajectory_muid, l_sort
  from askp.passenger_traffic_matrix_item
      JOIN rts.stop_item2gis si2gf ON si2gf.stop_place_muid=stop_from_muid AND (si2gf.tr_type_id::BIGINT=l_tr_type_id or l_tr_type_id ISNULL )
      JOIN rts.stop_item2gis si2gt ON si2gt.stop_place_muid=stop_to_muid AND (si2gt.tr_type_id::BIGINT=l_tr_type_id or l_tr_type_id ISNULL )
  where passanger_traffic_matrix_id = l_matrix_id
        AND order_num_from = l_sort ORDER BY order_num_to asc)
  select b.order_num_to, b.stop_place_muid, b.next_stop, b.amount, b.start_points, b.l_sort, r2g.round_id trajectory_muid from b
  JOIN rts.round2gis r2g ON r2g.route_trajectory_muid = b.trajectory_muid::BIGINT;
end;
$$;