﻿-- ========================================================================
-- Пакет загрузки данных АСКП
-- ========================================================================

create or replace function askp.askp_import_pkg$get_flag_file_loaded
   (v_filename in text)
returns integer
-- ========================================================================
-- Проверка что файл уже загружен
-- ========================================================================
as $$ declare
    v_result     integer;
begin
    select count(*) into v_result from askp.import_files where filename = v_filename;
    
    if not found then
       return 0;
    end if;
    
    return v_result;
end;
$$ language plpgsql security definer;

-- drop function askp.askp_import_pkg$set_flag_file_loaded(text)

create or replace function askp.askp_import_pkg$set_flag_file_loaded
  (v_filename    in text,
   v_data_date   in timestamptz)
returns integer
-- ========================================================================
-- Установка флага, файл загружен
-- ========================================================================
as $$ declare
    v_result     integer;
begin
    insert into askp.import_files(filename, data_date, import_date)
    values (v_filename, v_data_date, now())
    on conflict (filename, data_date)
    do nothing;
    
    get diagnostics v_result = row_count;
    return v_result;
end;
$$ language plpgsql security definer;


create or replace function askp.askp_import_pkg$import_ticket_type
   (v_field1 in text,
    v_field2 in text,
    v_field3 in text)
returns integer
-- ========================================================================
-- Загрузка строки данных из файла типов билетов
-- ========================================================================
as $$ declare
    v_result integer;
begin
    insert into askp.ticket_type as t (ticket_type_id, ticket_type_group, ticket_type_name)
    values (trim(both '"' from v_field1)::integer, trim(both '"' from v_field2), trim(both '"' from v_field3))
    on conflict (ticket_type_id)
    do update set (ticket_type_group,
                   ticket_type_name) = 
                  (excluded.ticket_type_group,
                   excluded.ticket_type_name)
    where t.ticket_type_group is distinct from excluded.ticket_type_group or
          t.ticket_type_name  is distinct from excluded.ticket_type_name;
    
    get diagnostics v_result = row_count;
    return v_result;
end;
$$ language plpgsql security definer;

/*

drop function askp.askp_import_pkg$import_ticket_oper(text,text,text,text,text)

create or replace function askp.askp_import_pkg$import_ticket_oper
   (v_field1 in text,
    v_field2 in text,
    v_field3 in text,
    v_field4 in text,
    v_field5 in text)
returns integer
-- ========================================================================
-- Не используется
-- Загрузка строки данных из файла проходов по билетам
-- ========================================================================
as $$ declare
    v_result integer;
begin
    insert into askp.ticket_oper as t (check_time, ticket_id, ticket_type_id, route_id, move_num)
    values (to_timestamp(v_field4, 'YYYY.MM.DD HH24:MI:SS'), v_field5, v_field3::integer, v_field1::integer, v_field2::integer)
    on conflict (check_time, ticket_id)
    do update set (ticket_type_id, 
                   route_id, 
                   move_num) =
                  (excluded.ticket_type_id,
                   excluded.route_id,
                   excluded.move_num)
    where t.ticket_type_id is distinct from excluded.ticket_type_id or
          t.route_id       is distinct from excluded.route_id       or
          t.move_num       is distinct from excluded.move_num;
    
    get diagnostics v_result = row_count;
    return v_result;
end;
$$ language plpgsql security definer;
*/
