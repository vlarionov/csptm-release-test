create or replace function askp."jf_route_number_ref_pkg$of_rows"
  (p_id_account numeric, OUT p_rows refcursor, p_attr text)
  returns refcursor
AS
$$
DECLARE
BEGIN
  OPEN p_rows FOR
  SELECT r.muid::text as route_muid,
    r.current_route_variant_muid::text as muid,
    r.number ,
    null start_date,
    null end_date,
    k.short_name
  FROM gis.routes r
  join gis.ref_transport_kinds k ON r.transport_kind_muid = k.muid
where r.current_route_variant_muid is not null and r.sign_deleted = 0;
END;
$$
LANGUAGE plpgsql VOLATILE;