CREATE OR REPLACE FUNCTION askp."jf_askp_full_data_pkg$of_rows"(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  f_dt_start 		timestamp 	:= jofl.jofl_pkg$extract_date(p_attr, 'BGN_DT', true) - INTERVAL '3 hours';
  f_dt_end	 		timestamp	:= jofl.jofl_pkg$extract_date(p_attr, 'END_DT', true) - INTERVAL '3 hours';

  l_route BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_route_muid', true);
  l_routes BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_route_id', true);-- RTS

  l_rv BIGINT[] := ARRAY(select current_route_variant_muid from gis.routes where muid = ANY(l_route));
  l_rvs BIGINT[] := ARRAY(select route_variant_id from rts.route_variant where route_id = ANY(l_routes) AND sign_deleted = false);-- rts

  -- траектория прямая, обратная
  l_direction SMALLINT := jofl.jofl_pkg$extract_number(p_attr, 'f_dir_MUID', true);
  -- вид рейса
  l_route_type BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_rtype_MUID', true);
  -- выход
  l_move_num BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_timetable_entry_num', true);

  -- остановки
  l_gis_stops_muid BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_gisMUID', true);
  l_gis_sp_muid BIGINT[] := array(select distinct muid from gis.stop_places where stop_muid = ANY(l_gis_stops_muid));

  -- tr id
  l_tr_ids BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_tr_id', true);
  -- номер рейса в наряде
  l_round_num BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_round_num_num', true);

  l_time_error_interval INTERVAL := make_interval(secs => (30 - asd.get_constant('TIME_FACT_ERROR'))::int);
  l_shift_interval INTERVAL := INTERVAL '3 hour' - INTERVAL '30 SECOND' + l_time_error_interval;

  -- остановки для данного номера маршрута
  l_route_stop_places BIGINT[] := array(
      select distinct on (stop_place_muid) sp2rt.stop_place_muid from gis.stop_place2route_traj sp2rt
        JOIN gis.route_trajectories rt ON rt.muid = sp2rt.route_trajectory_muid
        JOIN gis.route_rounds rr ON rr.muid = rt.route_round_muid
             AND (l_direction IS NULL OR rt.trajectory_type_muid = l_direction)
        JOIN gis.routes t1 ON t1.current_route_variant_muid = rr.route_variant_muid
        JOIN gis.routes t2 ON t2.muid = ANY(l_route)
             AND t1.number = t2.number
             AND t1.current_route_variant_muid IS NOT NULL
  );
BEGIN
  -- временно
  l_rv = ARRAY(select route_variant_muid from rts.route_variant2gis where route_variant_id = ANY(l_rvs) );

  OPEN p_rows FOR
  WITH b AS (
      SELECT distinct v2sb.order_round_id from askp.validation2stops_binding v2sb
      WHERE v2sb.time_fact BETWEEN f_dt_start AND f_dt_end
            AND (route_variant_muid = ANY(l_rv) OR array_length(l_rv,1) IS NULL)
            AND (stop_place_muid = ANY(l_gis_sp_muid) or array_length(l_gis_sp_muid,1) IS NULL )
            AND (v2sb.move_num = ANY(l_move_num) OR array_length(l_move_num,1) IS NULL )
            AND (v2sb.order_round_num = ANY(l_round_num) OR array_length(l_round_num,1) IS NULL )
            AND v2sb.order_round_id IS NOT NULL
      ),
      c AS (
        SELECT
          e.name_short,
          tr.garage_num,
          tr.seat_qty_total,
          tr_type.name short_name,
          number,
          timetable_entry_num,
          order_round_num,
          askp.format_time_period(o_round.order_round_id) AS Period,
          rrt.name vid,
          rtt.name direction,
          s.name stop,
          of.stop_order_num order_num,
          of.time_fact::DATE as date_,
          of.time_fact as time_start,
          COALESCE(lead(time_fact)
                   OVER (
                     PARTITION BY order_round_num, o_round.order_round_id ORDER BY time_fact ASC ), of.time_fact + INTERVAL '120 second') AS lead,
          /*lead(order_fact_id)
          OVER (
            PARTITION BY order_round_num, o_round.order_round_id ORDER BY time_fact ASC ) lead_fact,*/ order_fact_id,
          tr.tr_id,
          o_round.order_round_id,
          lead(of.stop_order_num)
          OVER (
            PARTITION BY order_round_num, o_round.order_round_id ORDER BY time_fact ASC ) lead_order_num
        FROM b
          JOIN tt.order_round o_round ON o_round.order_round_id = b.order_round_id
               AND o_round.order_round_id IN (-- только хорошие рейсы
            SELECT order_round_id
            FROM asd.oper_round_visit
            WHERE time_fact_begin BETWEEN f_dt_start AND f_dt_end
              AND order_round_status_id = 1)
          JOIN tt.order_list ol ON ol.order_list_id = o_round.order_list_id
          JOIN tt.timetable_entry tte ON tte.timetable_entry_id = ol.timetable_entry_id
          JOIN tt.round round ON round.round_id = o_round.round_id
          JOIN core.tr tr ON tr.tr_id = ol.tr_id
               AND (tr.tr_id = ANY(l_tr_ids) OR array_length(l_tr_ids, 1) IS NULL )
          JOIN core.tr_type ON tr.tr_type_id = tr_type.tr_type_id
          JOIN gis.route_variants rv ON rv.muid = tte.route_variant_muid
          JOIN gis.route_rounds rr ON rr.code = round.round_code AND rr.route_variant_muid = rv.muid
          JOIN gis.routes r ON r.current_route_variant_muid = rv.muid
          JOIN core.entity e ON e.entity_id = tr.depo_id
          JOIN gis.ref_route_trajectory_types rtt ON rtt.muid = round.direction_id
               AND (rtt.muid = l_direction OR l_direction IS NULL)
          JOIN gis.ref_route_round_types rrt ON rrt.muid = rr.route_round_type_muid--rrt.code = round.round_code
               AND (rrt.muid = ANY(l_route_type) OR array_length(l_route_type, 1) IS NULL )
          JOIN tt.order_fact of ON of.order_round_id = b.order_round_id
               AND of.sign_deleted = 0
          JOIN gis.stop_places sp ON sp.muid = of.stop_place_muid
               AND (sp.muid = ANY(l_route_stop_places) OR array_length(l_route_stop_places,1) IS NULL )
               AND (sp.muid = ANY(l_gis_sp_muid) OR array_length(l_gis_sp_muid,1) IS NULL )
          JOIN gis.stops s ON s.muid = sp.stop_muid
        ORDER BY o_round.order_round_id, stop_order_num
    )
  select *, time_start as time_start_ ,time_start + INTERVAL '30 second' as time_start,
    (SELECT count(*) from askp.validation2stops_binding where
    (order_round_id = c.order_round_id AND (stop_order_num = c.order_num OR stop_order_num = c.lead_order_num))
    --(order_fact_id = c.order_fact_id OR order_fact_id = c.lead_fact)
    AND check_time > (time_start +l_shift_interval) AND check_time <= (lead + l_shift_interval) )
    from c;
END;
$$;
