CREATE OR REPLACE FUNCTION askp.jf_asmpp_full_data_report_02_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_hour_begin smallint := jofl.jofl_pkg$extract_number(p_attr, 'f_hour_from_NUMBER', true);
  l_hour_end   smallint := jofl.jofl_pkg$extract_number(p_attr, 'f_hour_to_NUMBER', true);
BEGIN

  -- период времени в часах MSK (как ввел пользователь)
  l_hour_begin := coalesce(l_hour_begin, 0)-3;
  l_hour_end   := coalesce(l_hour_end, 0)-1-3;

  -- часы MSK переводим в часы UTC
  l_hour_begin := 24 * abs(sign(l_hour_begin)) * sign(abs((sign(l_hour_begin)-1))) + (l_hour_begin);
  l_hour_end   := 24 * abs(sign(l_hour_end)) * sign(abs((sign(l_hour_end)-1))) + (l_hour_end);

 OPEN p_rows FOR
 with report1_data as (select f.move_direction,
                              f.direction_id,
                              f.stop_number,
                              f.stop_name,
		                          sum(f.report_in) as report_in,
                              sum(f.report_out) as report_out,
                              sum(f.report_fullness) as report_fullness,
                              avg(f.report_loading) as report_loading
                         from askp.asmpp_report_pkg$report_data(p_id_account, p_attr) f
                        where (l_hour_begin <= l_hour_end and f.hr between l_hour_begin and l_hour_end) or
                              (l_hour_begin > l_hour_end and (f.hr between l_hour_begin and 23 or f.hr between 0 and l_hour_end))
                     group by f.direction_id,
                              f.move_direction,
                              f.stop_number,
                              f.stop_name)
 select sdd.move_direction,
        sdd.direction_id,
        sdd.stop_number,
        sdd.stop_name,
		    sdd.report_in,
        sdd.report_out,
        sdd.report_fullness,
        round(sdd.report_loading) as report_loading,
        round((sdd.report_in + sdd.report_out)/(sum(sdd.report_in + sdd.report_out) over (partition by sdd.move_direction))*100, 1) as pass_turnover
   from report1_data sdd
 union all
 select sds.move_direction,
        sds.direction_id,
        null::smallint as stop_number,
        'Суммарный пассажиропоток' as stop_name,
		    sum(sds.report_in) as report_in,
        null::smallint as report_out,
        null::smallint as report_fullness,
        null::numeric as report_loading,
        null::numeric as pass_turnover
   from report1_data sds
 group by grouping sets ((sds.direction_id, sds.move_direction), ())
order by direction_id nulls last, move_direction nulls last, stop_number nulls last;
END;
$$;
