create or REPLACE function askp."jf_askp_end_stations_pkg$attr_to_rowtype"(p_attr text) returns askp.askp_end_stations
LANGUAGE plpgsql
AS $$
declare
  l_r askp.askp_end_stations%rowtype;
begin
  l_r.askp_data_id := jofl.jofl_pkg$extract_number(p_attr, 'askp_data_id', true);
  l_r.muid := jofl.jofl_pkg$extract_number(p_attr, 'muid', true);

  return l_r;
end;
$$;

create or REPLACE function askp."jf_askp_end_stations_pkg$of_delete"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.askp_end_stations%rowtype;
begin
  l_r := askp.jf_askp_end_stations_pkg$attr_to_rowtype(p_attr);

  delete from  askp.askp_end_stations where  askp_data_id = l_r.askp_data_id;

  return null;
end;
$$;

create or REPLACE function askp."jf_askp_end_stations_pkg$of_insert"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.askp_end_stations%rowtype;
begin
  l_r := askp.jf_askp_end_stations_pkg$attr_to_rowtype(p_attr);

  insert into askp.askp_end_stations select l_r.*;

  return null;
end;
$$;

create or REPLACE function askp."jf_askp_end_stations_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
begin
  -- из документа, выходные данные :
  -- Идентификатор проездного документа;
  -- тип проездного
  -- дни недели
  -- дата и время транзакции 1 (начало поездки)
  -- тип валидации
  -- номер маршрута, направление рейса
  -- наименование ОП начала, наименование оп конца поездки
  open p_rows for
  SELECT stops.name AS trip_start, stops2.name AS trip_end, gis.routes.number , data.check_time ,
         ticket_type_name AS ticket_type, data.ticket_id AS ticket_id, rtt.name AS direction, end_st.*, rrt.code
  from askp.askp_end_stations end_st
    JOIN askp.askp_data data ON data.askp_data_id = end_st.askp_data_id
    JOIN askp.ticket_type t_type ON data.ticket_type_id = t_type.ticket_type_id
    JOIN askp.asmpp_agg_item item ON data.asmpp_agg_item_id = item.asmpp_agg_item_id
    JOIN gis.stops stops ON item.stops_muid = stops.muid
         AND stops.sign_deleted = 0
    JOIN gis.stops stops2 ON end_st.muid = stops2.muid
         AND stops2.sign_deleted = 0
    JOIN gis.route_variants rv ON rv.muid = item.route_variants_muid
         AND rv.sign_deleted = 0
    JOIN gis.routes ON rv.route_muid = gis.routes.muid
         AND gis.routes.sign_deleted = 0
    JOIN tt.order_round o_r ON o_r.order_round_id = item.order_round_id
         AND o_r.is_active = 1
    JOIN tt.round r ON r.round_id = o_r.round_id
         AND r.sign_deleted = 0
    JOIN gis.route_trajectories rt ON rt.muid = r.route_trajectory_muid
         AND rt.sign_deleted = 0
    JOIN gis.ref_route_trajectory_types rtt ON rt.trajectory_type_muid = rtt.muid
         AND rtt.sign_deleted = 0
    JOIN gis.route_rounds rr ON rt.route_round_muid = rr.muid
         AND rr.sign_deleted = 0
    JOIN gis.ref_route_round_types rrt ON rrt.muid = rr.route_round_type_muid
  ;
end;
$$;

create or REPLACE function askp."jf_askp_end_stations_pkg$of_update"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.askp_end_stations%rowtype;
begin
  l_r := askp.jf_askp_end_stations_pkg$attr_to_rowtype(p_attr);

  update askp.askp_end_stations set
    muid = l_r.muid
  where
    askp_data_id = l_r.askp_data_id;

  return null;
end;
$$;

