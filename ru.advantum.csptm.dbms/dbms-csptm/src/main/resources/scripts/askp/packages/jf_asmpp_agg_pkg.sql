create or REPLACE function askp."jf_asmpp_agg_pkg$of_recalc"
  (p_id_account numeric, p_attr text) returns boolean
LANGUAGE plpgsql
AS $$
DECLARE
    ld_start_date TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'time_start', true);
    ld_end_date TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'time_finish', true);
    id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'asmpp_agg_id', true);
BEGIN
    PERFORM askp.create_agg_items(ld_start_date, ld_end_date, id);
    PERFORM askp.create_askp_data(ld_start_date, ld_end_date, id);
    PERFORM askp.balancing_asmpp(ld_start_date, ld_end_date, id);
    -- поиск пар поездок
    --PERFORM askp.find_trip_tirminal_stop(ld_start_date, ld_end_date, id);
    return TRUE;
END;
$$;

create or REPLACE function askp."jf_asmpp_agg_pkg$attr_to_rowtype"(p_attr text) returns askp.asmpp_agg
LANGUAGE plpgsql
AS $$
declare
  l_r askp.asmpp_agg%rowtype;
begin
  l_r.asmpp_agg_id := jofl.jofl_pkg$extract_number(p_attr, 'asmpp_agg_id', true);
  l_r.time_start := jofl.jofl_pkg$extract_date(p_attr, 'time_start', true);
  l_r.time_finish := jofl.jofl_pkg$extract_date(p_attr, 'time_finish', true);

  return l_r;
end;

$$;

create or REPLACE function askp."jf_asmpp_agg_pkg$of_delete"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.asmpp_agg%rowtype;
begin
  l_r := askp.jf_asmpp_agg_pkg$attr_to_rowtype(p_attr);

  delete from  askp.asmpp_agg where  asmpp_agg_id = l_r.asmpp_agg_id;

  return null;
end;

$$;

create or replace function askp."jf_asmpp_agg_pkg$of_insert"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.asmpp_agg%rowtype;
begin
  l_r := askp.jf_asmpp_agg_pkg$attr_to_rowtype(p_attr);
  l_r.asmpp_agg_id = nextval( 'askp.asmpp_agg_asmpp_agg_id_seq' );
  insert into askp.asmpp_agg select l_r.*;

  return null;
end;

$$;


create or replace function askp."jf_asmpp_agg_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
  l_dt_start DATE := current_date - INTERVAL '2 day';
begin
  IF jofl.jofl_pkg$extract_date(p_attr, 'END_DT', true) IS NOT NULL THEN
    l_dt_start := jofl.jofl_pkg$extract_date(p_attr, 'END_DT', true) ;
  END IF;
  open p_rows for
  select
    asmpp_agg_id,
    time_start,
    time_finish,
    (select count(*)
        from askp.askp_data ad
        join askp.asmpp_agg_item aai on ad.asmpp_agg_item_id = aai.asmpp_agg_item_id
        where aai.asmpp_agg_id = asmpp_agg.asmpp_agg_id
    ) valid_ticket_qty
  from askp.asmpp_agg WHERE time_start > l_dt_start;
end;
$$;

create or REPLACE function askp."jf_asmpp_agg_pkg$of_update"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.asmpp_agg%rowtype;
begin
  l_r := askp.jf_asmpp_agg_pkg$attr_to_rowtype(p_attr);

  update askp.asmpp_agg set
    time_start = l_r.time_start,
    time_finish = l_r.time_finish
  where
    asmpp_agg_id = l_r.asmpp_agg_id;

  return null;
end;

$$;

