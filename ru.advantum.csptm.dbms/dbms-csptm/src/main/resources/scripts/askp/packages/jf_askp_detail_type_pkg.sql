CREATE OR REPLACE FUNCTION askp."jf_askp_detail_type_pkg$of_rows"(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_time_start    TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'time_start_', TRUE);
  l_lead TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'lead', TRUE);
  l_time_error_interval INTERVAL := make_interval(secs => (30 - asd.get_constant('TIME_FACT_ERROR'))::int);
  l_shift_interval INTERVAL := INTERVAL '3 hour' - INTERVAL '30 SECOND' + l_time_error_interval;

  l_order_num BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'order_num', TRUE);
  l_lead_order_num BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'lead_order_num', TRUE);
  l_order_round_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'order_round_id', TRUE);
BEGIN
  OPEN p_rows FOR
  WITH a AS (
      SELECT *
      FROM askp.validation2stops_binding
      WHERE (order_num = l_order_num OR order_num = l_lead_order_num)
             AND order_round_id = l_order_round_id
  ), b AS (
      SELECT *
      FROM askp.ticket_oper
      WHERE ((check_time >= l_time_start + l_shift_interval) AND (check_time < l_lead + l_shift_interval))
  ),
  c AS (SELECT * FROM b WHERE askp_check_id IN (SELECT a.askp_check_id FROM a))
  select ticket_type_name ticket_type, COUNT(*) num from c
     JOIN askp.ticket_type tt ON tt.ticket_type_id = c.ticket_type_id
     GROUP BY tt.ticket_type_name;
END;
$$;