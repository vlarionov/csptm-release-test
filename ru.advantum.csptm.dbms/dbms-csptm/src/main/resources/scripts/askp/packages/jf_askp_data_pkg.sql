
CREATE or replace FUNCTION askp.jf_askp_data_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$

declare
    l_asmpp_agg_item_id bigint := jofl.jofl_pkg$extract_number(p_attr, 'asmpp_agg_item_id', true);
    /*l_asmpp_agg_id bigint := jofl.jofl_pkg$extract_number(p_attr, 'asmpp_agg_id', true);*/
begin
         open p_rows for
              select
                askp_data.asmpp_agg_item_id,
                check_time,
                askp_data.ticket_type_id,
                ticket_id,
                ticket_type_name
              from askp.askp_data join askp.ticket_type on ticket_type.ticket_type_id = askp_data.ticket_type_id
              where askp_data.asmpp_agg_item_id = l_asmpp_agg_item_id;
end;

$$;