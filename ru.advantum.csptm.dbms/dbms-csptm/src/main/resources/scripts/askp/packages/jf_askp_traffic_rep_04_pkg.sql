create or replace function askp."jf_askp_traffic_rep_04_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
  l_rpt_dt date := core.from_utc(jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_START_DT', true));
  l_end_dt date := core.from_utc(jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_END_DT', true));
  l_route_id bigint[] := jofl.jofl_pkg$extract_tarray(p_attr, 'F_route_muid', TRUE);
  l_move text[] := jofl.jofl_pkg$extract_tarray(p_attr, 'rep_move_num', TRUE);
  l_report_day_key text := replace(jofl.jofl_pkg$extract_varchar(p_attr, 'F_WEEKDAY_INPLACE_K', true), ' ', '');
begin
  if core.assigned(l_rpt_dt::text, l_end_dt::text, l_route_id::text, l_move::text) then
    open p_rows for
      with d as (
                 select dt, tg.tag_name day_name, ci.calendar_tag_id
                 from ttb.calendar c  join ttb.calendar_item ci  on ci.calendar_id = c.calendar_id
                      join  ttb.calendar_tag tg on  tg.calendar_tag_id = ci.calendar_tag_id
                      join ttb.calendar_item ci2 on ci2.calendar_id = c.calendar_id
                 where c.dt between l_rpt_dt and l_end_dt
                   and tg.calendar_tag_group_id = 1
                   and ci2.calendar_tag_id = any(string_to_array(l_report_day_key, ',')::int[])
                )
        select  t.dt,
                t.number,
                tt.short_name tr_type_name,
                t.move_num,
                sum(t.check_ordinary) check_ordinary,
                sum(t.check_privilege) check_privilege,
                sum(check_all) check_all, --,  p_attr dt
                d.day_name
        from (select t.*, check_time::date dt from askp.validation4reports_agg t) t join d on t.dt = d.dt
             join core.tr_type tt on t.tr_type_id = tt.tr_type_id
        where check_time >= l_rpt_dt and check_time < l_end_dt + INTERVAL '1 day'
          and (t.number, t.tr_type_id) in ( select rt.route_num, rt.tr_type_id
                                            from rts.route rt join rts.route2gis r2g on rt.route_id = r2g.route_id
                                            where r2g.route_muid = any(l_route_id)
                                              and not rt.sign_deleted
                                          )
          and t.move_num::text = any(l_move)
        group by t.dt, t.number, tt.short_name, t.move_num, d.day_name
        order by t.dt, t.number, t.move_num;
  ELSE
    open p_rows for
      select null dt,  null number, null  tr_type_name, null move_num,
             null check_ordinary,
             null check_privilege,
             null check_all,
             null day_name;
  end if;
end;
$$;

