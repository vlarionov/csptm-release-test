CREATE OR REPLACE FUNCTION askp.jf_ref_order_round_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
begin 
 open p_rows for 
      select 
        ornd.order_round_id, 
        ornd.order_list_id, 
        ornd.round_id, 
        ornd.tn_instance, 
        ornd.is_active, 
        ornd.dt_update, 
        ornd.order_round_num
      from tt.order_round ornd
      where ornd.is_active = 1
      limit 100;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;