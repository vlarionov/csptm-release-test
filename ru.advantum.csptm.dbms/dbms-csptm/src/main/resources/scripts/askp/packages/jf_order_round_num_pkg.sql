--номер рейса в наряде
CREATE OR REPLACE FUNCTION askp."jf_ref_order_round_num_pkg$of_rows"(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE

BEGIN
  OPEN p_rows FOR
  SELECT * FROM generate_series(1, 100) num;
END;
$$;
