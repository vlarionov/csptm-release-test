CREATE OR REPLACE FUNCTION askp."jf_asmpp_agg_item_pkg$attr_to_rowtype"(p_attr TEXT)
  RETURNS askp.ASMPP_AGG_ITEM
LANGUAGE plpgsql
AS $$
DECLARE
  l_r askp.asmpp_agg_item%ROWTYPE;
BEGIN
  l_r.asmpp_agg_item_id := jofl.jofl_pkg$extract_number(p_attr, 'asmpp_agg_item_id', TRUE);
  l_r.asmpp_agg_id := jofl.jofl_pkg$extract_number(p_attr, 'asmpp_agg_id', TRUE);
  l_r.tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', TRUE);
  l_r.stops_muid := jofl.jofl_pkg$extract_number(p_attr, 'stops_muid', TRUE);
  l_r.route_variants_muid := jofl.jofl_pkg$extract_number(p_attr, 'route_variants_muid', TRUE);
  l_r.order_round_id := jofl.jofl_pkg$extract_number(p_attr, 'order_round_id', TRUE);
  l_r.in_cnt := jofl.jofl_pkg$extract_number(p_attr, 'in_cnt', TRUE);
  l_r.out_cnt := jofl.jofl_pkg$extract_number(p_attr, 'out_cnt', TRUE);
  l_r.time_start := jofl.jofl_pkg$extract_date(p_attr, 'time_start', TRUE);
  l_r.time_finish := jofl.jofl_pkg$extract_date(p_attr, 'time_finish', TRUE);

  RETURN l_r;
END;

$$;

CREATE OR REPLACE  FUNCTION askp."jf_asmpp_agg_item_pkg$of_delete"(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_r askp.asmpp_agg_item%ROWTYPE;
BEGIN
  l_r := askp.jf_asmpp_agg_item_pkg$attr_to_rowtype(p_attr);

  DELETE FROM askp.asmpp_agg_item
  WHERE asmpp_agg_item_id = l_r.asmpp_agg_item_id;

  RETURN NULL;
END;

$$;

CREATE OR REPLACE  FUNCTION askp."jf_asmpp_agg_item_pkg$of_insert"(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_r askp.asmpp_agg_item%ROWTYPE;
BEGIN
  l_r := askp.jf_asmpp_agg_item_pkg$attr_to_rowtype(p_attr);

  INSERT INTO askp.asmpp_agg_item SELECT l_r.*;

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION askp."jf_asmpp_agg_item_pkg$of_rows"(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  id            BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'asmpp_agg_id', TRUE);
  l_array_tr_id BIGINT [] := jofl.jofl_pkg$extract_narray(p_attr, 'F_TR_ID_tr_id', FALSE);
  l_route_variant_muid BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'F_ROUTE_VARIANT_ID_muid', TRUE);
BEGIN
  OPEN p_rows FOR

  WITH a AS
  (
      SELECT
        asmpp_agg_item_id,
        asmpp_agg_id,
        askp.asmpp_agg_item.tr_id,
        stops_muid,
        route_variants_muid,
        tt.order_round.order_round_id,
        in_cnt,
        out_cnt,
        time_start,
        time_finish,
        gis.stops.name,
        core.tr.licence,
        tt.timetable_entry.timetable_entry_num,
        tt.order_round.order_round_num, -- 'номер рейса в наряде'
        gis.routes.number, -- 'номер',
        (select count(*) from askp.askp_data where askp_data.asmpp_agg_item_id = asmpp_agg_item.asmpp_agg_item_id) valid_tickets_qty,
        in_corrected,
        out_corrected
      FROM askp.asmpp_agg_item, gis.stops, core.tr, tt.order_round,  tt.order_list, tt.timetable_entry,
           gis.routes, gis.route_variants
      WHERE asmpp_agg_id = id
            AND gis.stops.muid = askp.asmpp_agg_item.stops_muid
            AND core.tr.tr_id = askp.asmpp_agg_item.tr_id
            AND (askp.asmpp_agg_item.tr_id = ANY (l_array_tr_id) OR array_length(l_array_tr_id, 1) IS NULL)
            AND (l_route_variant_muid IS NULL OR askp.asmpp_agg_item.route_variants_muid = l_route_variant_muid)
            AND tt.order_round.order_round_id = askp.asmpp_agg_item.order_round_id
            AND tt.order_list.order_list_id = tt.order_round.order_list_id
            AND tt.order_list.timetable_entry_id = tt.timetable_entry.timetable_entry_id
            AND gis.route_variants.muid = askp.asmpp_agg_item.route_variants_muid
            AND gis.route_variants.route_muid = gis.routes.muid
  )
  SELECT *, sum(in_corrected) OVER (PARTITION BY order_round_id ORDER BY time_start) -
            sum(out_corrected) OVER (PARTITION BY order_round_id ORDER BY time_start) AS total_pass
         FROM a;
END;
$$;


CREATE OR REPLACE  FUNCTION askp."jf_asmpp_agg_item_pkg$of_update"(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_r askp.asmpp_agg_item%ROWTYPE;
BEGIN
  l_r := askp.jf_asmpp_agg_item_pkg$attr_to_rowtype(p_attr);

  UPDATE askp.asmpp_agg_item
  SET
    asmpp_agg_id        = l_r.asmpp_agg_id,
    tr_id               = l_r.tr_id,
    stops_muid          = l_r.stops_muid,
    route_variants_muid = l_r.route_variants_muid,
    order_round_id      = l_r.order_round_id,
    in_cnt              = l_r.in_cnt,
    out_cnt             = l_r.out_cnt,
    time_start          = l_r.time_start,
    time_finish         = l_r.time_finish
  WHERE
    asmpp_agg_item_id = l_r.asmpp_agg_item_id;

  RETURN NULL;
END;

$$;
