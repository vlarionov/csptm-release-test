CREATE OR REPLACE FUNCTION askp.jf_asmpp_full_data_report_01_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_indicator TEXT := jofl.jofl_pkg$extract_varchar(p_attr, 'F_INDICATOR_INPLACE_K', true);
BEGIN
 OPEN p_rows FOR
  select f.move_direction,
         f.direction_id,
         f.stop_number,
         f.stop_name,
         f.hr_str,
		     case l_indicator /* выбираем показатель отчета */
           when 'IN' then report_in
           when 'OUT' then report_out
           when 'FULLNESS' then report_fullness
           when 'LOADING' then round(report_loading)
           else null::bigint
         end as indicator_value
    from askp.asmpp_report_pkg$report_data(p_id_account , p_attr) f
order by f.direction_id, f.stop_number;
END;
$$;
