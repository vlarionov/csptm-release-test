create or REPLACE function askp."jf_trip_pairs_start_sp_pkg$attr_to_rowtype"(p_attr text) returns askp.trip_pairs_start_sp
LANGUAGE plpgsql
AS $$
declare
  l_r askp.trip_pairs_start_sp%rowtype;
begin
  l_r.stop_place_id := jofl.jofl_pkg$extract_number(p_attr, 'stop_place_id', true);
  l_r.trip_pairs_agg_id := jofl.jofl_pkg$extract_number(p_attr, 'trip_pairs_agg_id', true);
  l_r.trip_pairs_start_sp_id := jofl.jofl_pkg$extract_number(p_attr, 'trip_pairs_start_sp_id', true);
  return l_r;
end;
$$;


create or replace function askp."jf_trip_pairs_start_sp_pkg$of_delete"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.trip_pairs_start_sp%rowtype;
begin
  l_r := askp.jf_trip_pairs_start_sp_pkg$attr_to_rowtype(p_attr);

  delete from  askp.trip_pairs_start_sp where  trip_pairs_start_sp_id = l_r.trip_pairs_start_sp_id;

  return null;
end;
$$;

create or REPLACE  function askp."jf_trip_pairs_start_sp_pkg$of_insert"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.trip_pairs_start_sp%rowtype;

  l_trip_pairs_agg_id BIGINT 	:= jofl.jofl_pkg$extract_number(p_attr, 'trip_pairs_agg_id', true);
begin

  l_r := askp.jf_trip_pairs_start_sp_pkg$attr_to_rowtype(p_attr);
  select nextval('askp.trip_pairs_start_sp_trip_pairs_start_sp_id_seq'::regclass) INTO l_r.trip_pairs_start_sp_id;
  l_r.trip_pairs_agg_id := l_trip_pairs_agg_id;

  insert into askp.trip_pairs_start_sp select l_r.*;
  return null;
end;
$$;


create or REPLACE function askp."jf_trip_pairs_start_sp_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
  l_trip_pairs_agg_id BIGINT 	:= jofl.jofl_pkg$extract_number(p_attr, 'trip_pairs_agg_id', true);
begin
  open p_rows for
  select
    trip_pairs_agg_id::TEXT,
    stop_place_id::TEXT,
    trip_pairs_start_sp_id,
    (select name from gis.stops where muid = (SELECT stop_muid from gis.stop_places where muid = stop_place_id)) AS name
  from askp.trip_pairs_start_sp where trip_pairs_agg_id = l_trip_pairs_agg_id;
end;
$$;


create or REPLACE function askp."jf_trip_pairs_start_sp_pkg$of_update"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.trip_pairs_start_sp%rowtype;
begin
  l_r := askp.jf_trip_pairs_start_sp_pkg$attr_to_rowtype(p_attr);

  update askp.trip_pairs_start_sp set
    stop_place_id = l_r.stop_place_id,
    trip_pairs_agg_id = l_r.trip_pairs_agg_id
  where
    trip_pairs_start_sp_id = l_r.trip_pairs_start_sp_id;

  return null;
end;

$$;


