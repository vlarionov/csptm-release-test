CREATE OR REPLACE FUNCTION askp."jf_end_stations_pkg$of_rows"(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_trip_pairs_agg_id BIGINT 	:= jofl.jofl_pkg$extract_number(p_attr, 'trip_pairs_agg_id', true);
BEGIN
  OPEN p_rows FOR
  select
    (select name from gis.stops where muid = (select stop_muid from gis.stop_places sp where sp.muid = start_sp_muid)) name_sp_start,
    (select name from gis.stops where muid = (select stop_muid from gis.stop_places sp where sp.muid = end_sp_muid)) "name",
    count from askp.end_stations_agg t1 where t1.trip_pairs_agg_id = l_trip_pairs_agg_id;
END;
$$;

