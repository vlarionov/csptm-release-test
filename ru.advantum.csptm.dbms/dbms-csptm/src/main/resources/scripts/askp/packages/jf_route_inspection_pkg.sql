CREATE OR REPLACE FUNCTION askp.jf_route_inspection_pkg$attr_to_rowtype(p_attr TEXT)
    RETURNS askp.ROUTE_INSPECTION
LANGUAGE plpgsql
AS $function$
DECLARE
    l_r askp.route_inspection%ROWTYPE;
BEGIN
    l_r.route_inspection_id:= jofl.jofl_pkg$extract_number(p_attr, 'route_inspection_id', TRUE);
    l_r.route_id:= jofl.jofl_pkg$extract_number(p_attr, 'route_id', TRUE);
    l_r.status:= jofl.jofl_pkg$extract_number(p_attr, 'status', TRUE);
    l_r.inspection_begin_date:= jofl.jofl_pkg$extract_date(p_attr, 'inspection_begin_date', TRUE);
    l_r.inspection_end_date:= jofl.jofl_pkg$extract_date(p_attr, 'inspection_end_date', TRUE);
    l_r.inspection_count:= jofl.jofl_pkg$extract_number(p_attr, 'inspection_count', TRUE);
    l_r.round_count:= jofl.jofl_pkg$extract_number(p_attr, 'round_count', TRUE);
    l_r.comment:= jofl.jofl_pkg$extract_varchar(p_attr, 'comment', TRUE);
    l_r.dow:= jofl.jofl_pkg$extract_varchar(p_attr, 'dow', TRUE);
    l_r.route_percentage = NULL;
    RETURN l_r;
END;
$function$;


CREATE OR REPLACE FUNCTION askp.jf_route_inspection_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                                p_attr       TEXT)
    RETURNS REFCURSOR
LANGUAGE plpgsql
AS $function$
BEGIN
    OPEN p_rows FOR
    SELECT
        ri.route_inspection_id,
        ri.route_id,
        r.route_num,
        ri.status,
        ri.inspection_begin_date,
        ri.inspection_end_date,
        ri.round_count,
        (SELECT string_agg(wd_short_name, ', ')
         FROM core.week_day
         WHERE week_day_id :: TEXT IN (SELECT unnest((regexp_matches(ri.dow, '\d{1}', 'g'))))) AS dow_str,
        ri.dow,
        ri.inspection_count,
        ri.comment,
        CASE WHEN ri.route_percentage > 100
            THEN 100
        ELSE ri.route_percentage :: NUMERIC END || ' %'                                        AS route_percentage
    FROM askp.route_inspection ri
        JOIN rts.route r ON ri.route_id = r.route_id;
END;
$function$;

CREATE OR REPLACE FUNCTION askp.jf_route_inspection_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
    RETURNS TEXT
LANGUAGE plpgsql
AS $function$
DECLARE
    l_r                   askp.route_inspection%ROWTYPE;
    l_route_inspection_id askp.route_inspection.route_inspection_id%TYPE :=nextval('askp.route_inspection_route_inspection_id_seq');
BEGIN
    l_r:= askp.jf_route_inspection_pkg$attr_to_rowtype(p_attr);
    INSERT INTO askp.route_inspection (inspection_begin_date, inspection_end_date, round_count, inspection_count, status, route_id, comment, route_inspection_id, dow)
    VALUES (l_r.inspection_begin_date, l_r.inspection_end_date, l_r.round_count, l_r.inspection_count, l_r.status, l_r.route_id, l_r.comment,
            l_route_inspection_id, l_r.dow);
    UPDATE askp.route_inspection
    SET
        route_percentage = (SELECT round(avg(a.per) :: NUMERIC, 2)
                            FROM askp.jf_entrances_survey_pkg$calc(route_inspection_id) a)
    WHERE route_inspection_id = l_route_inspection_id;
    RETURN NULL;
END;
$function$;


CREATE OR REPLACE FUNCTION askp.jf_route_inspection_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
    RETURNS TEXT
LANGUAGE plpgsql
AS $function$
DECLARE
    l_route_inspection_id askp.route_inspection.route_inspection_id%TYPE :=jofl.jofl_pkg$extract_number(p_attr,
                                                                                                        'route_inspection_id',
                                                                                                        TRUE);
BEGIN
    DELETE FROM askp.route_inspection r
    WHERE r.route_inspection_id = l_route_inspection_id;
    RETURN NULL;
END;
$function$;


CREATE OR REPLACE FUNCTION askp.jf_route_inspection_pkg$of_update(p_id_account NUMERIC, p_attr TEXT)
    RETURNS TEXT
LANGUAGE plpgsql
AS $function$
DECLARE
    l_r askp.route_inspection%ROWTYPE;
BEGIN
    l_r := askp.jf_route_inspection_pkg$attr_to_rowtype(p_attr);

    UPDATE askp.route_inspection
    SET
        inspection_begin_date = l_r.inspection_begin_date,
        inspection_end_date   = l_r.inspection_end_date,
        route_id              = l_r.route_id,
        comment               = l_r.comment,
        round_count           = l_r.round_count,
        status                = l_r.status,
        inspection_count      = l_r.inspection_count,
        dow                   = l_r.dow,
        route_percentage      = (SELECT round(avg(a.per) :: NUMERIC, 2)
                                 FROM askp.jf_entrances_survey_pkg$calc(route_inspection_id) a)
    WHERE
        route_inspection_id = l_r.route_inspection_id;
    RETURN NULL;
END;
$function$;