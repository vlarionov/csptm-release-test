CREATE OR REPLACE FUNCTION askp."jf_ticket_type_pkg$of_rows"(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE

BEGIN
  OPEN p_rows FOR
  select * from askp.ticket_type;
END;
$$;


create or REPLACE function askp."jf_ticket_type_pkg$of_change_privilege" (p_id_account numeric, p_attr text) returns boolean
LANGUAGE plpgsql
AS $$
DECLARE
  l_ticket_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'ticket_type_id', true);
BEGIN
  UPDATE askp.ticket_type SET is_privilege = NOT is_privilege WHERE ticket_type_id = l_ticket_id;
  return TRUE;
END;
$$;


