create or replace function askp."jf_passenger_matrix_route_ref_pkg$of_rows"
  (p_id_account numeric, OUT p_rows refcursor, p_attr text)
  returns refcursor
AS
$$
DECLARE
BEGIN
  OPEN p_rows FOR
SELECT r.number AS "number" ,
  rv.muid::TEXT AS "route_variant",
  rv.start_date::DATE AS "start_date", rv.end_date::DATE AS "end_date",
  rr.muid::TEXT AS "route",
  rrt.name AS "route_type", rrt.muid::TEXT AS "route_type_id",
  rtt.muid AS "direction_id", rtt.name AS "direction",
  tc.short_name, rr.code,
  tc.muid as tr_type_id
from gis.route_variants rv
JOIN gis.route_rounds rr ON rv.muid = rr.route_variant_muid
     AND rr.sign_deleted = 0
JOIN gis.routes r ON rv.route_muid = r.muid
     AND r.agency_muid = 1 -- только МГТ
     AND r.current_route_variant_muid = rv.muid
     AND r.sign_deleted = 0
JOIN gis.ref_transport_kinds tc ON r.transport_kind_muid = tc.muid
JOIN gis.ref_route_round_types rrt ON rr.route_round_type_muid = rrt.muid
     AND rrt.sign_deleted = 0
JOIN gis.route_trajectories rt ON rr.muid = rt.route_round_muid
     AND rt.sign_deleted = 0
JOIN gis.ref_route_trajectory_types rtt ON rt.trajectory_type_muid = rtt.muid
     AND rtt.sign_deleted = 0
  WHERE rv.sign_deleted = 0
  ORDER BY r.number, rv.muid, rv.start_date, rrt.muid;
END;
$$
LANGUAGE plpgsql VOLATILE;