create or replace function askp."jf_report_comparision_volumes_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare 
 f_report_period_start_attr timestamp := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_START_DT', true);
 f_report_period_end_attr timestamp := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_END_DT', true); 

 f_report_day_list text := jofl.jofl_pkg$extract_varchar(p_attr, 'F_WEEKDAY_INPLACE_K', true);
 f_report_day_name text := jofl.jofl_pkg$extract_varchar(p_attr, 'F_WEEKDAY_INPLACE_V', true);
 f_report_day_set smallint[];

-- общие фильтры и параметры
 f_arr_rep_route bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_route_muid', true);
 f_report_period_start timestamp;
 f_report_period_end timestamp;
begin
     /* без установленных фильтров гарантированно открываем "пустой" отчет,
     если фильтры установлены - собираем данные */
 if f_report_period_start_attr is not null or
    f_report_period_end_attr is not null or
    array_length(f_arr_rep_route, 1) is not null or
    f_report_day_list is not null then

  /* для отчета за период - от 00:00:00 начала периода до 23:59:59 окончания периода */
  f_report_period_start := date_trunc('day' , f_report_period_start_attr + interval '3 hours');
  f_report_period_end := date_trunc('day' , f_report_period_end_attr + interval '3 hours') + interval '23:59:59';

 	/* определяем массив необходимых дней недели (по ttb.calendar_tag - Будни / Выходные) */
	case coalesce(f_report_day_list, 'EVERYDAY')
	 when 'WORKINGDAY' then f_report_day_set := array[8];
	 when 'DAYOFF' then f_report_day_set := array[9];
	 when 'EVERYDAY' then f_report_day_set := array[8,9]; f_report_day_name := 'Все';
	end case;

/* открываем отчет по почасовому агрегату */
  open p_rows for
        /* periods - границы трех периодов: указанного пользователем (текущего), предыдущего и "год назад" */
   with periods as (select period.p_start,
                           period.p_end,
                           period.p_start - period.p_days as lp_start,
                           period.p_start - period.p_days + period.p_len as lp_end,
                           period.p_start - interval '1 year' as ly_start,
                           period.p_start - interval '1 year' + period.p_len as ly_end
                    from (select f_report_period_start AS p_start,
                                 f_report_period_end AS p_end,
                                 f_report_period_end - f_report_period_start as p_len,
                                 f_report_period_end - f_report_period_start + interval '1 second' as p_days) period),
      	/* route_date_checks - "перевозка за день" за период от "год назад" до окончания текущего периода в разрезе даты, маршрута и типа транспортного средства */
        route_date_checks as (SELECT date_trunc('day', v4rag.check_time) as check_time,
                                     v4rag.number as route_number,
                                     v4rag.tr_type_id as tr_type_id,
                                     sum(v4rag.check_all) as check_all
                              from askp.validation4reports_agg v4rag
                              where v4rag.check_time between f_report_period_start - interval '1 year' and f_report_period_end
                                    /* фильтр по дню недели, с учетом производственного кадендаря */
                                    and trunc(extract (epoch from v4rag.check_time)/(24*60*60)) in
                                        (select distinct ci.calendar_id
                                         from ttb.calendar_item ci
                                         where ci.calendar_tag_id = any (f_report_day_set)
                                               and ci.calendar_id between trunc(extract (epoch from f_report_period_start - interval '1 year')/(24*60*60)) and
											                                                    trunc(extract (epoch from f_report_period_end)/(24*60*60)))
                                    /* фильтр по маршрутам (с учетом вида транспорта маршрута) */
                                    and (array_length(f_arr_rep_route, 1) IS NULL OR
                                         exists (select 1
                                                 from rts.route2gis r2g
                                                      join rts.route rrt on rrt.route_id = r2g.route_id
                                                 where r2g.route_muid = ANY (f_arr_rep_route)
                                                       and rrt.route_num = v4rag.number
                                                       and rrt.tr_type_id = v4rag.tr_type_id))
                              group by date_trunc('day', v4rag.check_time),
                                       v4rag.number,
                                       v4rag.tr_type_id)
   select /* формируем строки отчета */
          user_period.route_number || ' ' || tr_tp.short_name as route_number,
          user_period.weekday_type,
          to_char(user_period.period_start, 'DD.MM.YYYY') ||'-'|| to_char(user_period.period_end, 'DD.MM.YYYY') as filter_period,
          user_period.check_all,
          to_char(user_period.last_start, 'DD.MM.YYYY') ||'-'|| to_char(user_period.last_end, 'DD.MM.YYYY') as last_period,
          last_period.last_check_all,
          round(user_period.check_all/last_period.last_check_all*100, 2) as prc2last,
          to_char(user_period.year_start, 'DD.MM.YYYY') ||'-'|| to_char(user_period.year_end, 'DD.MM.YYYY') as year_period,
          year_period.year_check_all,
          round(user_period.check_all/year_period.year_check_all*100, 2) as prc2year
   from  /* считаем среднесуточную перевозку за текущий период */
        (select p1.p_start as period_start,
                p1.p_end as period_end,
                f_report_day_name as weekday_type,
                rdc1.route_number,
                rdc1.tr_type_id,
                round(avg(rdc1.check_all)) as check_all,
                p1.lp_start as last_start,
                p1.lp_end as last_end,
                p1.ly_start as year_start,
                p1.ly_end as year_end
         from route_date_checks rdc1,
              periods p1
         where rdc1.check_time between p1.p_start and p1.p_end
         group by p1.p_start,
                  p1.p_end,
                  rdc1.route_number,
                  rdc1.tr_type_id,
                  p1.lp_start,
                  p1.lp_end,
                  p1.ly_start,
                  p1.ly_end) user_period
    left outer JOIN /* считаем и привязываем (по маршруту и типу ТС) среднесуточную перевозку за предыдущий период */
       (select p2.lp_start as last_start,
               p2.lp_end as last_end,
              rdc2.route_number,
              rdc2.tr_type_id,
              round(avg(rdc2.check_all)) as last_check_all
        from route_date_checks rdc2,
             periods p2
        where rdc2.check_time between p2.lp_start and p2.lp_end
        group by p2.lp_start,
                 p2.lp_end,
                 rdc2.route_number,
                 rdc2.tr_type_id) last_period
     on     user_period.route_number = last_period.route_number
        and user_period.tr_type_id = last_period.tr_type_id
    left outer JOIN /* считаем и привязываем (по маршруту и типу ТС) к текущему периоду среднесуточную перевозку за период "год назад" */
      (select p3.ly_start as year_start,
              p3.ly_end as year_end,
             rdc3.route_number,
             rdc3.tr_type_id,
             round(avg(rdc3.check_all)) as year_check_all
       from route_date_checks rdc3,
            periods p3
       where rdc3.check_time between p3.ly_start and p3.ly_end
       group by p3.ly_start,
                p3.ly_end,
                rdc3.route_number,
                rdc3.tr_type_id) year_period
     on     user_period.route_number = year_period.route_number
        and user_period.tr_type_id = year_period.tr_type_id
    join core.tr_type tr_tp on tr_tp.tr_type_id = user_period.tr_type_id;

 else
  /* если не заданы фильтры - открываем "пустой" отчет */
  open p_rows for
   select '' as route_number,
          '' as weekday_type,
          '' as filter_period,
          0 as check_all,
          '' as last_period,
          0 as last_check_all,
          0 as prc2last,
          '' as year_period,
          0 as year_check_all,
          0 as prc2year
    where 1 = 2;

   end if;

end;
$$;
