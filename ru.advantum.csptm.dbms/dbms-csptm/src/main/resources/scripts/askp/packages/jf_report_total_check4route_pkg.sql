create or replace function askp."jf_report_total_check4route_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
 v_attr text;
begin
 /* добавляем параметр: тип отчета (по маршруту) */
 select p_attr::jsonb || '{"P_REPORT_TYPE": "ROUTENUMBER"}'::jsonb into v_attr;
 select askp.report_total_check(p_id_account, v_attr) into p_rows;
end;
$$;
