create or replace function askp."jf_asmpp_report_03_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
language plpgsql
as $$
DECLARE
  l_rpt_dt timestamp := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_START_DT', true);
  l_end_dt timestamp := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_END_DT', true);
  l_route bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_route_muid', TRUE);
  l_round_normal int[] := array[1];
  l_max_in_out_difference NUMERIC := 1 + asd.get_constant('ASMPP_IN_OUT_DIFFERENCE') / 100.0;

BEGIN
  if core.assigned(l_rpt_dt::text, l_end_dt::text, l_route::text) then
    OPEN p_rows FOR
      select grp,
             dt,
             number,
             tr_type_short_name,
             order_round_num,
             ornd_period,
             timetable_entry_num,
             direction,
             case when grp = 1 then null else stop_order_num end stop_order_num,
             stop_name,
             stop_time_fact,
             sum_in,
             sum_out,
             sum(sum_in - sum_out) over(partition by order_round_id order by stop_order_num) in_saloon,
             order_round_id
      from
        (
           with orf as (select r.number, ornd.order_round_num, tte.timetable_entry_num, ornd.round_id, olst.tr_id,
                        orf.stop_order_num = min(orf.stop_order_num) over(partition by orf.order_round_id) is_first_stop,
                        orf.stop_order_num = max(orf.stop_order_num) over(partition by orf.order_round_id) is_last_stop,
                        orf.*
                        from
                          (select * from tt.order_fact orf ) orf
                        join tt.order_round ornd on orf.order_round_id = ornd.order_round_id
                        join tt.order_list olst on ornd.order_list_id = olst.order_list_id
                                               and ((olst.order_date >= core.from_utc(l_rpt_dt)::date and olst.order_date <= core.from_utc(l_end_dt)::date + 1)
                                                        and (orf.time_fact >= l_rpt_dt and orf.time_fact < l_end_dt + interval '1 day' and orf.sign_deleted = 0)
                                                   )
                        join tt.timetable_entry tte on olst.timetable_entry_id = tte.timetable_entry_id
                        join gis.route_variants rv on rv.muid = tte.route_variant_muid
                        join gis.routes r on rv.route_muid = r.muid
                        where r.muid = any(l_route)
                  ),
                t as (SELECT  t.asmpp_agg_item_id id,
                              t.order_fact_id,
                              t.order_round_id,
                              t.stop_order_num,
                              t.time_start,
                              t.in_corrected sum_in, t.out_corrected sum_out, t.total_pass_after total_after
                      FROM askp.asmpp_agg_item t
                      WHERE t.asmpp_agg_id in (SELECT a.asmpp_agg_id
                                                FROM askp.asmpp_agg a
                                                WHERE a.time_start >= l_rpt_dt::date AND a.time_start <= l_end_dt::date + 1
                                               )
                        and t.time_start >= l_rpt_dt and t.time_start < l_end_dt + interval '1 day'
                     )
          select grouping(qq.order_round_id, order_fact_id) grp,
                 max(dt) dt,
                 max(number) number,
                 max(tr_type_short_name) tr_type_short_name,
                 case when grouping(qq.order_round_id, order_fact_id) = 1 then 'Итого по рейсу '||max(order_round_num)::text else max(order_round_num)::text end order_round_num,
                 max(format('%s - %s (%s мин)',
                             to_char(round_start, 'hh24:mi'),
                             to_char(round_end, 'hh24:mi'),
                             date_part('hour',round_end - round_start)*60
                                  + date_part('minute', round_end - round_start)
                            )
                     )ornd_period,
                 max(timetable_entry_num) timetable_entry_num,
                 max(direction) direction,
                 case when grouping(qq.order_round_id, order_fact_id) = 0 then max(stop_order_num) else 100500 end stop_order_num,
                 case when grouping(qq.order_round_id, order_fact_id) = 0 then max(stop_name) end stop_name,
                 case when grouping(qq.order_round_id, order_fact_id) = 0 then max(stop_time_fact) end stop_time_fact,
                 case when not bool_and(qq.is_last_stop) then sum(sum_in) else 0 end sum_in,
                 sum(least(sum_out, in_saloon_prev + sum_in)) sum_out,
                 sum(in_saloon_prev) in_saloon_prev,
                 order_round_id,
                 bool_and(qq.is_first_stop) is_first_stop,
                 bool_and(qq.is_last_stop) is_last_stop
          from
            (
              select *
              from
                  (
                  select orf.order_fact_id,
                         core.tr_type.short_name tr_type_short_name,
                         orf.number number,
                         orf.order_round_num,
                         core.from_utc(orf.time_fact)::date dt,
                         date_trunc('minute',core.from_utc(min(orf.time_fact) over(partition by orf.order_round_id))) round_start,
                         date_trunc('minute', core.from_utc(max(orf.time_fact) over(partition by orf.order_round_id))) round_end,
                         orf.timetable_entry_num,
                         case when rnd.direction_id = 1 then 'Прямое' else 'Обратное' end direction,
                         orf.stop_order_num,
                         orf.is_first_stop,
                         orf.is_last_stop,
                         st.name stop_name,
                         t.time_start stop_time_fact,
                         case when orf.is_last_stop then 0 else  t.sum_in end sum_in,
                         case when orf.is_first_stop then 0 else t.sum_out end sum_out,
                         coalesce(sum(case when orf.is_last_stop then 0 else  t.sum_in end -  case when orf.is_first_stop then 0 else t.sum_out end)
                                       over(partition by orf.order_round_id order by orf.stop_order_num rows between unbounded preceding and 1 preceding )
                            ,0) in_saloon_prev,
                         sum(t.sum_in) over (partition by orf.order_round_id) sum_in_round,
                         sum(t.sum_out) over (partition by orf.order_round_id) sum_out_round,
                         t.total_after,
                         orf.order_round_id,
                         count(*) over(partition by orf.order_round_id) stop_count,
                         count(*) filter(where t.order_round_id is null and t.stop_order_num is null) over(partition by orf.order_round_id) stop_null_count
                  from  orf join asd.oper_round_visit orv on orf.order_round_id = orv.order_round_id and orv.order_round_status_id = any(l_round_normal)
                        join tt.round rnd on rnd.round_id = orf.round_id
                        join gis.stop_places stp on stp.muid = orf.stop_place_muid
                        join gis.stops st on st.muid = stp.stop_muid
                        join core.tr on core.tr.tr_id = orf.tr_id
                        join core.tr_type on core.tr_type.tr_type_id = core.tr.tr_type_id
                        left join t on t.order_round_id = orf.order_round_id and t.stop_order_num = orf.stop_order_num
                ) q
              where (stop_count <> stop_null_count)
                and not (sum_in_round/nullif(sum_out_round, 0)::numeric > l_max_in_out_difference or sum_out_round/nullif(sum_in_round, 0)::numeric > l_max_in_out_difference)
            ) qq
          group by rollup(order_round_id, order_fact_id)
          having grouping(qq.order_round_id, order_fact_id) in (0,1)
        ) qqq
      order by dt, number, ornd_period, grp, stop_order_num;
/*      select grp,
             dt,
             number,
             tr_type_short_name,
             order_round_num,
             ornd_period,
             timetable_entry_num,
             direction,
             case when grp = 1 then null else stop_order_num end stop_order_num,
             stop_name,
             stop_time_fact,
             case
               when grp = 1
               then sum_in
               else case when is_last_stop then null else sum_in end
             end as sum_in,
             case
               when grp = 1
               then sum_in
               else case when is_last_stop then sum(sum_in - sum_out) over(partition by order_round_id order by stop_order_num) else sum_out end
              end as sum_out,
             case
               when grp = 1
               then 0
               else sum(sum_in - sum_out) over(partition by order_round_id order by stop_order_num)
             end as in_saloon,
             order_round_id
      from
        (
           with orf as (select r.number, ornd.order_round_num, tte.timetable_entry_num, ornd.round_id, olst.tr_id,
                        orf.stop_order_num = min(orf.stop_order_num) over(partition by orf.order_round_id) is_first_stop,
                        orf.stop_order_num = max(orf.stop_order_num) over(partition by orf.order_round_id) is_last_stop,
                        orf.*
                        from
                          (select * from tt.order_fact orf where orf.time_fact >= l_rpt_dt and orf.time_fact < l_end_dt + interval '1 day' and orf.sign_deleted = 0) orf
                        join tt.order_round ornd on orf.order_round_id = ornd.order_round_id
                        join tt.order_list olst on ornd.order_list_id = olst.order_list_id
                                               and olst.order_date >= core.from_utc(l_rpt_dt)::date and olst.order_date <= core.from_utc(l_end_dt)::date + 1
                        join tt.timetable_entry tte on olst.timetable_entry_id = tte.timetable_entry_id
                        join gis.route_variants rv on rv.muid = tte.route_variant_muid
                        join gis.routes r on rv.route_muid = r.muid
                        where r.muid = any(l_route)
                  ),
                t as (SELECT  t.asmpp_agg_item_id id,
                              t.order_fact_id,
                              t.order_round_id,
                              t.stop_order_num,
                              t.time_start,
                              t.in_corrected sum_in, t.out_corrected sum_out, t.total_pass_after total_after
                      FROM askp.asmpp_agg_item t
                      WHERE t.asmpp_agg_id in (SELECT a.asmpp_agg_id
                                                FROM askp.asmpp_agg a
                                                WHERE a.time_start >= l_rpt_dt::date AND a.time_start <= l_end_dt::date + 1
                                               )
                        and t.time_start >= l_rpt_dt and t.time_start < l_end_dt + interval '1 day'
                     )
          select grouping(q.order_round_id, order_fact_id) grp,
                 max(dt) dt,
                 max(number) number,
                 max(tr_type_short_name) tr_type_short_name,
                 case when grouping(q.order_round_id, order_fact_id) = 1 then 'Итого по рейсу '||max(order_round_num)::text else max(order_round_num)::text end order_round_num,
                 max(format('%s - %s (%s мин)',
                             to_char(round_start, 'hh24:mi'),
                             to_char(round_end, 'hh24:mi'),
                             date_part('hour',round_end - round_start)*60
                                  + date_part('minute', round_end - round_start)
                            )
                     )ornd_period,
                 max(timetable_entry_num) timetable_entry_num,
                 max(direction) direction,
                 case when grouping(q.order_round_id, order_fact_id) = 0 then max(stop_order_num) else 100500 end stop_order_num,
                 case when grouping(q.order_round_id, order_fact_id) = 0 then max(stop_name) end stop_name,
                 case when grouping(q.order_round_id, order_fact_id) = 0 then max(stop_time_fact) end stop_time_fact,
                 case when not bool_and(q.is_last_stop) then sum(sum_in) else 0 end sum_in,
                 case when not bool_and(q.is_first_stop) then sum(sum_out) else 0 end sum_out,
                 order_round_id,
                 bool_and(q.is_first_stop) is_first_stop,
                 bool_and(q.is_last_stop) is_last_stop
          from
            (
              select orf.order_fact_id,
                     core.tr_type.short_name tr_type_short_name,
                     orf.number number,
                     orf.order_round_num,
                     core.from_utc(orf.time_fact)::date dt,
                     date_trunc('minute',core.from_utc(min(orf.time_fact) over(partition by orf.order_round_id))) round_start,
                     date_trunc('minute', core.from_utc(max(orf.time_fact) over(partition by orf.order_round_id))) round_end,
                     orf.timetable_entry_num,
                     case when rnd.direction_id = 1 then 'Прямое' else 'Обратное' end direction,
                     orf.stop_order_num,
                     orf.is_first_stop,
                     orf.is_last_stop,
                     st.name stop_name,
                     t.time_start stop_time_fact,
                     t.sum_in,
                     t.sum_out,
                     sum(t.sum_in) over (partition by orf.order_round_id) sum_in_round,
                     sum(t.sum_out) over (partition by orf.order_round_id) sum_out_round,
                     t.total_after,
                     orf.order_round_id,
                     count(*) over(partition by orf.order_round_id) stop_count,
                     count(*) filter(where t.order_round_id is null and t.stop_order_num is null) over(partition by orf.order_round_id) stop_null_count
              from  orf join asd.oper_round_visit orv on orf.order_round_id = orv.order_round_id and orv.order_round_status_id = any(l_round_normal)
                    join tt.round rnd on rnd.round_id = orf.round_id
                    join gis.stop_places stp on stp.muid = orf.stop_place_muid
                    join gis.stops st on st.muid = stp.stop_muid
                    join core.tr on core.tr.tr_id = orf.tr_id
                    join core.tr_type on core.tr_type.tr_type_id = core.tr.tr_type_id
                    --left join t on t.order_fact_id = orf.order_fact_id
                    left join t on t.order_round_id = orf.order_round_id and t.stop_order_num = orf.stop_order_num
            ) q
          where (stop_count <> stop_null_count)
            and not (sum_in_round/nullif(sum_out_round, 0)::numeric > l_max_in_out_difference or sum_out_round/nullif(sum_in_round, 0)::numeric > l_max_in_out_difference)
          group by rollup(order_round_id, order_fact_id)
          having grouping(q.order_round_id, order_fact_id) in (0,1)

        ) qq
      order by dt, number, ornd_period, grp, stop_order_num; */
  ELSE
    open p_rows for
    select null dt, null number, null tr_type_short_name, null order_round_num,
           null ornd_period,
           null timetable_entry_num,
           null direction,
           null stop_order_num,
           null stop_name,
           null stop_time_fact,
           null sum_in,
           null sum_out,
           null in_saloon,
           null attr;
  end if;
end;
$$;