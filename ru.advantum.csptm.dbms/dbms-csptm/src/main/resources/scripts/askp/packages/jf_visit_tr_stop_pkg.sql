CREATE OR REPLACE FUNCTION askp.jf_visit_tr_stop_pkg$attr_to_rowtype(p_attr TEXT)
    RETURNS askp.VISIT_TR_STOP
LANGUAGE plpgsql
AS $function$
DECLARE
    l_r askp.visit_tr_stop%ROWTYPE;
BEGIN
    l_r.visit_tr_stop_id:= jofl.jofl_pkg$extract_number(p_attr, 'visit_tr_stop_id', TRUE);
    l_r.inspection_date:= jofl.jofl_pkg$extract_date(p_attr, 'inspection_date', TRUE);
    l_r.arrived_count:= mod(jofl.jofl_pkg$extract_number(p_attr, 'arrived_count', TRUE), 24 * 3600);
    l_r.min_interval:= jofl.jofl_pkg$extract_number(p_attr, 'min_interval', TRUE);
    l_r.max_interval:= jofl.jofl_pkg$extract_number(p_attr, 'max_interval', TRUE);
    l_r.entered_count:= jofl.jofl_pkg$extract_number(p_attr, 'entered_count', TRUE);
    l_r.released_count:= jofl.jofl_pkg$extract_number(p_attr, 'released_count', TRUE);
    l_r.time_end_inspection:= mod(jofl.jofl_pkg$extract_number(p_attr, 'time_end_inspection', TRUE), 24 * 60 * 60);
    l_r.time_begin_inspection:= mod(jofl.jofl_pkg$extract_number(p_attr, 'time_begin_inspection', TRUE), 24 * 60 * 60);
    l_r.landing_refusal_count:= jofl.jofl_pkg$extract_number(p_attr, 'landing_refusal_count', TRUE);
    l_r.tr_filling_count:= jofl.jofl_pkg$extract_number(p_attr, 'tr_filling_count', TRUE);
    l_r.visual_inspection_id:= jofl.jofl_pkg$extract_number(p_attr, 'visual_inspection_id', TRUE);
    l_r.begin_stop_item_id:= jofl.jofl_pkg$extract_number(p_attr, 'begin_stop_item_id', TRUE);
    l_r.end_stop_item_id:= jofl.jofl_pkg$extract_number(p_attr, 'end_stop_item_id', TRUE);
    RETURN l_r;
END;
$function$;

--------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION askp.jf_visit_tr_stop_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
    RETURNS REFCURSOR
LANGUAGE plpgsql
AS $function$
DECLARE
    l_visual_inspection_id INTEGER := jofl.jofl_pkg$extract_number(p_attr, 'visual_inspection_id', TRUE) :: INTEGER;
BEGIN
    OPEN p_rows FOR
    SELECT
        av.visual_inspection_id,
        av.visit_tr_stop_id,
        av.inspection_date,
        av.min_interval,
        av.max_interval,
        av.time_begin_inspection,
        av.time_end_inspection,
        av.begin_stop_item_id,
        av.end_stop_item_id,
        av.entered_count,
        av.released_count,
        av.arrived_count,
        av.landing_refusal_count,
        av.tr_filling_count,
        rse.name                                                                      end_stop_of_route,
        rsb.name                                                                      begin_stop_of_route,
        sum(ct.seat_qty_total)                                                                  qty,
        count(ct.tr_id)                                                               tr_nums,
        round((GREATEST(av.arrived_count,av.tr_filling_count) :: REAL / sum(ct.seat_qty_total) * 100) :: NUMERIC, 2) || ' %' AS actual_load
    FROM askp.visit_tr_stop av
        JOIN rts.stop_item sib ON av.begin_stop_item_id = sib.stop_item_id
        JOIN rts.stop_item sie ON av.end_stop_item_id = sie.stop_item_id

        JOIN rts.stop_location slb on slb.stop_location_id = sib.stop_location_id
        JOIN rts.stop_location sle on sle.stop_location_id = sie.stop_location_id

        JOIN rts.stop rsb ON slb.stop_id = rsb.stop_id
        JOIN rts.stop rse ON sle.stop_id = rse.stop_id

        left JOIN askp.tr_inspection t ON av.visit_tr_stop_id = t.visit_tr_stop_id
        left JOIN core.tr ct ON t.tr_id = ct.tr_id
    WHERE av.visual_inspection_id = l_visual_inspection_id
    GROUP BY
        av.visual_inspection_id,
        av.visit_tr_stop_id,
        av.inspection_date,
        av.min_interval,
        av.max_interval,
        av.time_begin_inspection,
        av.time_end_inspection,
        av.begin_stop_item_id,
        av.end_stop_item_id,
        av.entered_count,
        av.released_count,
        av.arrived_count,
        av.landing_refusal_count,
        av.tr_filling_count,
        rse.name,
        rsb.name;
END;
$function$;

--------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION askp.jf_visit_tr_stop_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
    RETURNS TEXT
LANGUAGE plpgsql
AS $function$
DECLARE
    l_visit_ts_stop_id askp.visit_tr_stop.visit_tr_stop_id%TYPE :=jofl.jofl_pkg$extract_number(p_attr, 'visit_ts_stop_id', TRUE);
BEGIN
    DELETE FROM askp.visit_tr_stop
    WHERE visit_tr_stop_id = l_visit_ts_stop_id;
    RETURN NULL;
END;
$function$;

--------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION askp.jf_visit_tr_stop_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
    RETURNS TEXT
LANGUAGE plpgsql
AS $function$
DECLARE
    l_r askp.visit_tr_stop%ROWTYPE;
BEGIN
    l_r := askp.jf_visit_tr_stop_pkg$attr_to_rowtype(p_attr);
    INSERT INTO askp.visit_tr_stop (
        inspection_date,
        time_begin_inspection,
        time_end_inspection,
        max_interval,
        min_interval,
        entered_count,
        released_count,
        arrived_count,
        landing_refusal_count,
        tr_filling_count,
        visual_inspection_id,
        begin_stop_item_id,
        end_stop_item_id
    )
    VALUES
        (
            l_r.inspection_date,
            l_r.time_begin_inspection,
            l_r.time_end_inspection,
            l_r.max_interval,
            l_r.min_interval,
            l_r.entered_count,
            l_r.released_count,
            l_r.arrived_count,
            l_r.landing_refusal_count,
            l_r.tr_filling_count,
            l_r.visual_inspection_id,
            l_r.begin_stop_item_id,
            l_r.end_stop_item_id
        );
    RETURN NULL;
END;
$function$;

CREATE OR REPLACE FUNCTION askp.jf_visit_tr_stop_pkg$of_update(p_id_account NUMERIC, p_attr TEXT)
    RETURNS TEXT
LANGUAGE plpgsql
AS $function$
DECLARE
    l_r askp.visit_tr_stop%ROWTYPE;
BEGIN
    l_r := askp.jf_visit_tr_stop_pkg$attr_to_rowtype(p_attr);
    UPDATE askp.visit_tr_stop
    SET inspection_date       = l_r.inspection_date,
        time_begin_inspection = l_r.time_begin_inspection,
        time_end_inspection   = l_r.time_end_inspection,
        max_interval          = l_r.max_interval,
        min_interval          = l_r.min_interval,
        entered_count         = l_r.entered_count,
        released_count        = l_r.released_count,
        arrived_count         = l_r.arrived_count,
        landing_refusal_count = l_r.landing_refusal_count,
        tr_filling_count      = l_r.tr_filling_count,
        visual_inspection_id  = l_r.visual_inspection_id,
        begin_stop_item_id    = l_r.begin_stop_item_id,
        end_stop_item_id      = l_r.end_stop_item_id
    WHERE visit_tr_stop_id = l_r.visit_tr_stop_id;
    RETURN NULL;
END;
$function$;