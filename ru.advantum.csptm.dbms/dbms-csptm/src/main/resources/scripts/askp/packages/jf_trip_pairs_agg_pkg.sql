create OR REPLACE function askp."jf_trip_pairs_agg_pkg$of_insert"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.trip_pairs_agg%rowtype;

  l_sp_muid_arr BIGINT[];
  -- массив цифр, типа {2,5}
  l_dow_arr SMALLINT[];
begin
  l_r := askp.jf_trip_pairs_agg_pkg$attr_to_rowtype(p_attr);

  select nextval('askp.trip_pairs_agg_id_seq') INTO l_r.trip_pairs_agg_id;
  l_r.state := 'завершен';

  insert into askp.trip_pairs_agg select l_r.*;

  return null;
end;
$$;


create OR REPLACE function askp."jf_trip_pairs_agg_pkg$attr_to_rowtype"(p_attr text) returns askp.trip_pairs_agg
LANGUAGE plpgsql
AS $$
declare
  l_r askp.trip_pairs_agg%rowtype;
begin
  l_r.date_from := jofl.jofl_pkg$extract_date(p_attr, 'date_from', true);
  l_r.date_to := jofl.jofl_pkg$extract_date(p_attr, 'date_to', true);
  l_r.time_from := jofl.jofl_pkg$extract_number(p_attr, 'time_from', true);
  l_r.time_to := jofl.jofl_pkg$extract_number(p_attr, 'time_to', true);
  l_r.dow := jofl.jofl_pkg$extract_varchar(p_attr, 'dow', true);
  --l_r.sp_id := jofl.jofl_pkg$extract_number(p_attr, 'sp_id', true);
  l_r.trip_number := jofl.jofl_pkg$extract_number(p_attr, 'trip_number', true);
  l_r.state := jofl.jofl_pkg$extract_varchar(p_attr, 'state', true);
  l_r.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true);
  l_r.url := jofl.jofl_pkg$extract_varchar(p_attr, 'url', true);
  l_r.trip_pairs_agg_id := jofl.jofl_pkg$extract_number(p_attr, 'trip_pairs_agg_id', true);
  return l_r;
end;
$$;

create OR REPLACE function askp."jf_trip_pairs_agg_pkg$of_delete"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.trip_pairs_agg%rowtype;
begin
  l_r := askp.jf_trip_pairs_agg_pkg$attr_to_rowtype(p_attr);
  -- удаление из таблицы с начальными остановками
  DELETE from askp.end_stations_agg where trip_pairs_agg_id = l_r.trip_pairs_agg_id;
  DELETE from askp.trip_pairs_start_sp where trip_pairs_agg_id = l_r.trip_pairs_agg_id;
  delete from  askp.trip_pairs_agg where  trip_pairs_agg_id = l_r.trip_pairs_agg_id;

  return null;
end;

$$;


create OR REPLACE function askp."jf_trip_pairs_agg_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
  f_dt_start 			timestamp := jofl.jofl_pkg$extract_date(p_attr, 'BGN_DT', true);
  f_dt_end	 			timestamp	:= jofl.jofl_pkg$extract_date(p_attr, 'END_DT', true);
begin
  open p_rows for
  select
    date_from,
    date_to,
    time_from,
    time_to,
     dow,
    /*(select s.name from gis.stops s
     JOIN gis.stop_places sp ON sp.stop_muid = s.muid AND sp.muid = a.sp_id) sp,*/
    trip_number,
    state,
    comment,
    --sp_id::TEXT as sp_muid,
    --sp_id::TEXT,
    --sp_id::TEXT as f_list_muid,
    CASE WHEN state = 'новый' THEN NULL
    ELSE '[' || json_build_object('CAPTION', 'На карте',
   'JUMP', json_build_object('METHOD', 'askp.on_map', 'ATTR', '{}' ))|| ']'
    END as url,
    trip_pairs_agg_id,
    ((date_from + INTERVAL '3 hour')::DATE)::TEXT as d_from,
    ((date_to + INTERVAL '3 hour')::DATE)::TEXT as d_to,
    (select string_agg(wd_short_name, ', ') from  core.week_day where week_day_id::TEXT IN (select unnest((regexp_matches(dow, '\d{1}', 'g')))))
     AS dow_str,
    '[' || json_build_object('CAPTION', 'На карте',
                             'JUMP', json_build_object('METHOD', 'askp.many_starts', 'ATTR', '{}' ))|| ']'
     AS url_rays
  from askp.trip_pairs_agg a
  where (date_from + INTERVAL '3 hour' >= f_dt_start AND f_dt_end >= date_to + INTERVAL '3 hour')
  OR (date_from + INTERVAL '3 hour' >= f_dt_start AND f_dt_end IS NULL)
  OR (f_dt_end >= date_to + INTERVAL '3 hour' AND f_dt_start IS NULL)
  OR (f_dt_end IS NULL AND f_dt_start IS NULL);
end;
$$;

create OR REPLACE function askp."jf_trip_pairs_agg_pkg$of_update"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r askp.trip_pairs_agg%rowtype;
begin
  l_r := askp.jf_trip_pairs_agg_pkg$attr_to_rowtype(p_attr);

  update askp.trip_pairs_agg set
    date_from = l_r.date_from,
    date_to = l_r.date_to,
    time_from = l_r.time_from,
    time_to = l_r.time_to,
    dow = l_r.dow,
    --sp_id = l_r.sp_id,
    trip_number = l_r.trip_number,
    state = l_r.state,
    comment = l_r.comment,
    url = l_r.url
  where
    trip_pairs_agg_id = l_r.trip_pairs_agg_id;

  return null;
end;
$$;


create OR REPLACE function askp."jf_trip_pairs_agg_pkg$of_calc"(p_id_account NUMERIC, p_attr TEXT)
RETURNS BOOLEAN
LANGUAGE plpgsql
AS $$
declare
  f_stop_place_muid BIGINT 	:= jofl.jofl_pkg$extract_number(p_attr, 'f_list_muid', true);

  f_dt_start timestamp 	:= jofl.jofl_pkg$extract_date(p_attr, 'd_from', true);
  f_dt_end	 timestamp	:= jofl.jofl_pkg$extract_date(p_attr, 'd_to', true);

  l_amount INTEGER := jofl.jofl_pkg$extract_number(p_attr, 'trip_number', true);

  l_dow TEXT := jofl.jofl_pkg$extract_number(p_attr, 'dow', true);
  -- массив цифр, типа {2,5}
  l_dow_arr SMALLINT[] := array(select DISTINCT unnest((regexp_matches(l_dow, '\d{1}', 'g'))));
  -- сразу несколько остановок
  l_trip_pairs_agg_id BIGINT 	:= jofl.jofl_pkg$extract_number(p_attr, 'trip_pairs_agg_id', true);
  l_sp_muid_arr BIGINT[] := array(select stop_place_id from askp.trip_pairs_start_sp where trip_pairs_agg_id = l_trip_pairs_agg_id);

  l_temp_arr BIGINT[] := l_sp_muid_arr;
begin
  -- удаление, расчета, который уже был
  DELETE from askp.end_stations_agg where trip_pairs_agg_id = l_trip_pairs_agg_id;
  -- остановки связанные с начальными
  l_sp_muid_arr = array(select distinct muid from luchininov.connected_stop_places2 where stop_places_muid = ANY(l_sp_muid_arr));
  l_sp_muid_arr = array_cat(l_sp_muid_arr, l_temp_arr); -- еще сами остановки
  INSERT INTO askp.end_stations_agg(trip_pairs_agg_id, count, start_sp_muid, end_sp_muid)
  WITH a AS(
      SELECT l_trip_pairs_agg_id as trip_pairs_agg_id, count(end_sp_muid), start_sp_muid, end_sp_muid
      from  askp.end_station where start_sp_muid = ANY(l_sp_muid_arr)
            AND ((check_time >= f_dt_start AND check_time <= f_dt_end) OR (f_dt_start IS NULL AND f_dt_end IS NULL)) -- фильтр по дате
            AND ((EXTRACT(ISODOW FROM check_time) = ANY(l_dow_arr)) OR l_dow IS NULL) -- фильтр по дню недели
      GROUP BY end_sp_muid, start_sp_muid)
  select *
  from a where (count > l_amount) OR (l_amount IS NULL)
  order by count desc;
  RETURN true;
end;
$$;


