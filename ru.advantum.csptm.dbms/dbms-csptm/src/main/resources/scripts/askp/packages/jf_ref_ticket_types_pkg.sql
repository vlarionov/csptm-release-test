CREATE OR REPLACE FUNCTION askp.jf_ref_ticket_types_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
begin 
 open p_rows for 
      select 
        tt.ticket_type_group, 
        tt.ticket_type_name, 
        tt.ticket_type_id
      from askp.ticket_type tt; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;