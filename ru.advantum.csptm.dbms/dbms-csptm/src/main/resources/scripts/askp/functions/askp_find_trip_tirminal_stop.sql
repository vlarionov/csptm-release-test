-- определение остановки, где мог выйти пассажир, по данным АСКП. Анализируются пары последовательных
-- валидаций
CREATE OR REPLACE FUNCTION
  askp.find_trip_tirminal_stop(p_timestamp_from TIMESTAMP, p_timestamp_to TIMESTAMP, p_id BIGINT)
  RETURNS BOOLEAN AS $$
DECLARE
    x_askp_data CURSOR FOR
    WITH a AS (
      -- билеты где было хотябы 2 валидации
        SELECT ticket_id
        FROM askp.askp_data WHERE askp.askp_data.check_time BETWEEN p_timestamp_from AND p_timestamp_to
        GROUP BY ticket_id
        HAVING COUNT(ticket_id) > 1
    )
    SELECT
      /*askp.askp_data.* , stops_muid*/ *
    FROM askp.askp_data
      JOIN askp.asmpp_agg_item ON askp.asmpp_agg_item.asmpp_agg_item_id = askp.askp_data.asmpp_agg_item_id
    WHERE askp.askp_data.check_time BETWEEN p_timestamp_from AND p_timestamp_to
          AND ticket_id IN (SELECT * FROM a)
    ORDER BY ticket_id, check_time;

  l_current_row              RECORD;
  l_next_row                 RECORD;

  l_route_variant_stops_muid BIGINT ARRAY;
  l_possible_end_stops_id    BIGINT ARRAY;
  l_last_stop_geometry       GEOMETRY;
  l_end_stop_id              BIGINT;

  l_dist NUMERIC;

  /*start_stop_name TEXT;
  end_stop_names TEXT ARRAY;
  l_bus_number TEXT;*/
BEGIN
  OPEN x_askp_data;
  FETCH x_askp_data INTO l_current_row;
  FETCH x_askp_data INTO l_next_row;

  LOOP
    -- если  валидации относятся к разному id билета, то пропускаем
    IF l_next_row.ticket_id <> l_current_row.ticket_id
    THEN
      l_current_row := l_next_row;
      FETCH x_askp_data INTO l_next_row;
      EXIT WHEN NOT FOUND;
      CONTINUE;
    END IF;
    -- если между двумя последовательными валидациямим прошло менее 30 секунд, считаем, что это несколько человека
    -- прошли по одному проездному билету (такое часто наблюдается в записях)
    IF (l_next_row.check_time - l_current_row.check_time)::INTERVAL < INTERVAL '30 SECOND'
    THEN
      -- RAISE NOTICE 'Validation time % % %', l_next_row.check_time, l_current_row.check_time, l_next_row.check_time - l_current_row.check_time;
      l_current_row := l_next_row;
      FETCH x_askp_data INTO l_next_row;
      EXIT WHEN NOT FOUND;
      CONTINUE;
    END IF;

    -- геометрия последней (2-й) валидации
    SELECT geom
    INTO l_last_stop_geometry
    FROM gis.stop_places
    WHERE gis.stop_places.stop_muid = l_next_row.stops_muid;

    --RAISE NOTICE 'Last geom: %', l_last_stop_geometry;
    -- id остановок, в заданном радиусе от остановки 2-й валидации (связанные остановки, в терминологии документа)
    l_possible_end_stops_id := array(
        SELECT muid -- , ST_Distance(l_last_stop_geometry :: GEOGRAPHY, geom :: GEOGRAPHY, TRUE) AS DIST
        FROM gis.stop_places
        WHERE ST_DWithin(l_last_stop_geometry :: GEOGRAPHY, geom :: GEOGRAPHY,
                         asd.get_constant('ASKP_LAST_STOP_SEARCH_RADIUS')));

    /*select name INTO start_stop_name from gis.stop_places sp JOIN gis.stops st ON sp.stop_muid = st.muid  WHERE sp.stop_muid = l_current_row.stops_muid;
    select gis.routes.number INTO l_bus_number from askp.askp_data data
           JOIN askp.asmpp_agg_item agg_item ON data.asmpp_agg_item_id = agg_item.asmpp_agg_item_id
           JOIN gis.route_variants ON agg_item.route_variants_muid = gis.route_variants.muid
           JOIN gis.routes ON gis.routes.muid = gis.route_variants.route_muid
           WHERE data.askp_data_id = l_current_row.askp_data_id;
    RAISE NOTICE 'Начало поездки, маршрут: % ; %', start_stop_name, l_bus_number;*/

    /*select name INTO start_stop_name from gis.stop_places sp JOIN gis.stops st ON sp.stop_muid = st.muid  WHERE sp.stop_muid = l_next_row.stops_muid;
    select gis.routes.number INTO l_bus_number from askp.askp_data data
      JOIN askp.asmpp_agg_item agg_item ON data.asmpp_agg_item_id = agg_item.asmpp_agg_item_id
      JOIN gis.route_variants ON agg_item.route_variants_muid = gis.route_variants.muid
      JOIN gis.routes ON gis.routes.muid = gis.route_variants.route_muid
    WHERE data.askp_data_id = l_next_row.askp_data_id;
    RAISE NOTICE 'Вторая валидация, маршрут: % ; %', start_stop_name, l_bus_number;*/

    -- id остановок, маршрута первой валидации, которые попали в заданный радиус
    l_route_variant_stops_muid := array(SELECT DISTINCT gis.stop_place2route_traj.stop_place_muid
                                        FROM gis.routes
                                          JOIN gis.route_variants ON gis.route_variants.route_muid = gis.routes.muid
                                          JOIN gis.route_rounds
                                            ON gis.route_variants.muid = gis.route_rounds.route_variant_muid
                                          JOIN gis.route_trajectories
                                            ON gis.route_rounds.muid = gis.route_trajectories.route_round_muid
                                          JOIN gis.stop_place2route_traj ON gis.route_trajectories.muid =
                                                                            gis.stop_place2route_traj.route_trajectory_muid
                                          JOIN gis.stop_places
                                            ON gis.stop_place2route_traj.stop_place_muid = gis.stop_places.muid
                                               AND gis.stop_places.muid = ANY (l_possible_end_stops_id)
                                          JOIN gis.stops ON gis.stop_places.stop_muid = gis.stops.muid
                                        WHERE gis.route_variants.muid = l_current_row.route_variants_muid);

    -- RAISE NOTICE 'Possible end stops, muid: %', l_route_variant_stops_muid;
    /*end_stop_names := array(select name from gis.stop_places sp JOIN gis.stops st ON sp.stop_muid = st.muid  WHERE sp.muid = ANY(l_route_variant_stops_muid));
    RAISE NOTICE 'Возможные остановки конца: %', end_stop_names;*/

    -- среди них остановку с минимальным расстоянием до места второй валидации
    WITH a AS(
        SELECT stops.muid , ST_Distance(l_last_stop_geometry :: GEOGRAPHY, sp.geom :: GEOGRAPHY, TRUE) AS DIST
        FROM gis.stop_places sp
          JOIN gis.stops stops ON stops.muid = sp.stop_muid
        WHERE sp.muid = ANY(/*l_possible_end_stops_id*/ l_route_variant_stops_muid) ORDER BY DIST LIMIT 2
    )SELECT muid, dist INTO l_end_stop_id, l_dist FROM a WHERE DIST > 0;

    IF l_end_stop_id IS NOT NULL THEN
      -- если нашли остановку, которая могла быть конечной, для маршрута первой валидации
      INSERT INTO askp.askp_end_stations(askp_data_id, muid) VALUES(l_current_row.askp_data_id, l_end_stop_id) ;
    END IF;

    /*RAISE NOTICE 'Stops, dist: %  %', l_end_stop_id, l_dist;
    select name INTO start_stop_name from gis.stop_places sp JOIN gis.stops st ON sp.stop_muid = st.muid  WHERE sp.muid = l_end_stop_id;
    RAISE NOTICE 'Конец поездки : %', start_stop_name;*/

    l_current_row := l_next_row;
    FETCH x_askp_data INTO l_next_row;
    EXIT WHEN NOT FOUND;
  END LOOP;

  RETURN TRUE;
END;
$$ LANGUAGE plpgsql;


DO LANGUAGE plpgsql $$
DECLARE
    x_askp_data CURSOR FOR
    WITH a AS (
      -- билеты где было хотябы 2 валидации
        SELECT ticket_id
        FROM askp.askp_data WHERE askp.askp_data.check_time BETWEEN '2017-09-10' :: TIMESTAMP AND '2017-09-11' :: TIMESTAMP
        GROUP BY ticket_id
        HAVING COUNT(ticket_id) > 1
    )
    SELECT
      /*askp.askp_data.* , stops_muid*/ *
    FROM askp.askp_data
      JOIN askp.asmpp_agg_item ON askp.asmpp_agg_item.asmpp_agg_item_id = askp.askp_data.asmpp_agg_item_id
    WHERE askp.askp_data.check_time BETWEEN '2017-09-10' :: TIMESTAMP AND '2017-09-11' :: TIMESTAMP
          AND ticket_id IN (SELECT * FROM a)
    ORDER BY ticket_id, check_time LIMIT 100;

  l_current_row              RECORD;
  l_next_row                 RECORD;

  l_route_variant_stops_muid BIGINT ARRAY;
  l_possible_end_stops_id    BIGINT ARRAY;
  l_last_stop_geometry       GEOMETRY;
  l_end_stop_id              BIGINT;

  l_dist NUMERIC;

  start_stop_name TEXT;
  end_stop_names TEXT ARRAY;
  bus_number TEXT;
BEGIN
  OPEN x_askp_data;
  FETCH x_askp_data INTO l_current_row;
  FETCH x_askp_data INTO l_next_row;

  --WHILE (l_next_row IS NOT NULL) LOOP
  LOOP
    -- если  валидации относятся к разному id билета, то пропускаем
    IF l_next_row.ticket_id <> l_current_row.ticket_id
    THEN
      l_current_row := l_next_row;
      FETCH x_askp_data INTO l_next_row;
      EXIT WHEN NOT FOUND;
      CONTINUE;
    END IF;
    -- если между двумя последовательными валидациямим прошло менее 30 секунд, считаем, что это несколько человека
    -- прошли по одному проездному билету (такое часто наблюдается в записях)
    IF (l_next_row.check_time - l_current_row.check_time)::INTERVAL < INTERVAL '30 SECOND'
    THEN
      -- RAISE NOTICE 'Validation time % % %', l_next_row.check_time, l_current_row.check_time, l_next_row.check_time - l_current_row.check_time;
      l_current_row := l_next_row;
      FETCH x_askp_data INTO l_next_row;
      EXIT WHEN NOT FOUND;
      CONTINUE;
    END IF;

    -- геометрия последней (2-й) валидации
    SELECT geom
    INTO l_last_stop_geometry
    FROM gis.stop_places
    WHERE gis.stop_places.stop_muid = l_next_row.stops_muid;

    --RAISE NOTICE 'Last geom: %', l_last_stop_geometry;
    -- id остановок, в заданном радиусе от остановки 2-й валидации (связанные остановки, в терминологии документа)
    l_possible_end_stops_id := array(
        SELECT muid -- , ST_Distance(l_last_stop_geometry :: GEOGRAPHY, geom :: GEOGRAPHY, TRUE) AS DIST
        FROM gis.stop_places
        WHERE ST_DWithin(l_last_stop_geometry :: GEOGRAPHY, geom :: GEOGRAPHY,
                         asd.get_constant('ASKP_LAST_STOP_SEARCH_RADIUS')));

    /*select name INTO start_stop_name from gis.stop_places sp JOIN gis.stops st ON sp.stop_muid = st.muid  WHERE sp.stop_muid = l_current_row.stops_muid;
    select gis.routes.number INTO bus_number from askp.askp_data data
           JOIN askp.asmpp_agg_item agg_item ON data.asmpp_agg_item_id = agg_item.asmpp_agg_item_id
           JOIN gis.route_variants ON agg_item.route_variants_muid = gis.route_variants.muid
           JOIN gis.routes ON gis.routes.muid = gis.route_variants.route_muid
           WHERE data.askp_data_id = l_current_row.askp_data_id;
    RAISE NOTICE 'Начало поездки, маршрут: % ; %', start_stop_name, bus_number;*/

    /*select name INTO start_stop_name from gis.stop_places sp JOIN gis.stops st ON sp.stop_muid = st.muid  WHERE sp.stop_muid = l_next_row.stops_muid;
    select gis.routes.number INTO bus_number from askp.askp_data data
      JOIN askp.asmpp_agg_item agg_item ON data.asmpp_agg_item_id = agg_item.asmpp_agg_item_id
      JOIN gis.route_variants ON agg_item.route_variants_muid = gis.route_variants.muid
      JOIN gis.routes ON gis.routes.muid = gis.route_variants.route_muid
    WHERE data.askp_data_id = l_next_row.askp_data_id;
    RAISE NOTICE 'Вторая валидация, маршрут: % ; %', start_stop_name, bus_number;*/

    -- id остановок, маршрута первой валидации, которые попали в заданный радиус
    l_route_variant_stops_muid := array(SELECT DISTINCT gis.stop_place2route_traj.stop_place_muid
                                        FROM gis.routes
                                          JOIN gis.route_variants ON gis.route_variants.route_muid = gis.routes.muid
                                          JOIN gis.route_rounds
                                            ON gis.route_variants.muid = gis.route_rounds.route_variant_muid
                                          JOIN gis.route_trajectories
                                            ON gis.route_rounds.muid = gis.route_trajectories.route_round_muid
                                          JOIN gis.stop_place2route_traj ON gis.route_trajectories.muid =
                                                                            gis.stop_place2route_traj.route_trajectory_muid
                                          JOIN gis.stop_places
                                            ON gis.stop_place2route_traj.stop_place_muid = gis.stop_places.muid
                                            AND gis.stop_places.muid = ANY (l_possible_end_stops_id)
                                          JOIN gis.stops ON gis.stop_places.stop_muid = gis.stops.muid
                                        WHERE gis.route_variants.muid = l_current_row.route_variants_muid);

    -- RAISE NOTICE 'Possible end stops, muid: %', l_route_variant_stops_muid;
    /*end_stop_names := array(select name from gis.stop_places sp JOIN gis.stops st ON sp.stop_muid = st.muid  WHERE sp.muid = ANY(l_route_variant_stops_muid));
    RAISE NOTICE 'Возможные остановки конца: %', end_stop_names;*/

    -- среди них остановку с минимальным расстоянием до места второй валидации
    WITH a AS(
        SELECT sp.muid , ST_Distance(l_last_stop_geometry :: GEOGRAPHY, sp.geom :: GEOGRAPHY, TRUE) AS DIST
        FROM gis.stop_places sp
        WHERE sp.muid = ANY(/*l_possible_end_stops_id*/ l_route_variant_stops_muid) ORDER BY DIST LIMIT 2
    )SELECT muid, dist INTO l_end_stop_id, l_dist FROM a WHERE DIST > 0;

    IF l_end_stop_id IS NOT NULL THEN
      -- если нашли остановку, которая могла быть конечной, для маршрута первой валидации
      INSERT INTO askp.askp_end_stations(askp_data_id, muid) VALUES(l_current_row.askp_data_id, l_end_stop_id) ;
    END IF;

    --RAISE NOTICE 'Stops, dist: %  %', l_end_stop_id, l_dist;
    select name INTO start_stop_name from gis.stop_places sp JOIN gis.stops st ON sp.stop_muid = st.muid  WHERE sp.muid = l_end_stop_id;
    RAISE NOTICE 'Конец поездки : %', start_stop_name;

    l_current_row := l_next_row;
    FETCH x_askp_data INTO l_next_row;
    EXIT WHEN NOT FOUND;
  END LOOP;
END $$;
