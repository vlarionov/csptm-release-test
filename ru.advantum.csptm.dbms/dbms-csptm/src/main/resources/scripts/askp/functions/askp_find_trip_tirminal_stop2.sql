-- определение остановки, где мог выйти пассажир, по данным АСКП. Анализируются пары последовательных
-- валидаций

-- используется другая таблица. Таблица валидаций, связанная с order_fact.
CREATE OR REPLACE FUNCTION
  askp.find_trip_tirminal_stop2(p_timestamp_from TIMESTAMP, p_timestamp_to TIMESTAMP)
  RETURNS BOOLEAN AS $$
DECLARE

BEGIN
    INSERT INTO askp.end_station(check_time, ticket_id, end_sp_muid, route_variant_muid, start_sp_muid)
    with rt AS(
      with aa AS(SELECT distinct gis.route_variants.muid , gis.stop_place2route_traj.stop_place_muid, order_num, route_trajectories.trajectory_type_muid
                 FROM gis.routes
                   JOIN gis.route_variants ON gis.routes.current_route_variant_muid = gis.route_variants.muid
                        AND gis.route_variants.sign_deleted = 0 AND gis.routes.sign_deleted = 0
                   JOIN gis.route_rounds ON gis.route_variants.muid = gis.route_rounds.route_variant_muid
                        AND gis.route_rounds.sign_deleted = 0 AND gis.route_rounds.code = '00'
                   JOIN gis.route_trajectories ON gis.route_rounds.muid = gis.route_trajectories.route_round_muid
                        AND gis.route_trajectories.sign_deleted = 0
                   JOIN gis.stop_place2route_traj ON gis.route_trajectories.muid = gis.stop_place2route_traj.route_trajectory_muid
                        AND gis.stop_place2route_traj.sign_deleted = 0
      )
      select * from aa
    ), -- вариант маршрута, остановки (массивом)
        tic as (
          select t.ticket_id
          from askp.ticket_oper t
          where t.check_time between p_timestamp_from and p_timestamp_to
          group by ticket_id having count(1)>1
      ),-- id билетов, по которым больше чем 2 валидации
        adata as (
          select t.ticket_id, t.check_time, t.askp_check_id, lead (t.check_time) over (partition by t.ticket_id order by t.check_time) as next_check_time,
                                                             lead (t.askp_check_id) over (partition by t.ticket_id order by t.check_time)  as next_askp_check_id
          from askp.ticket_oper t where exists (select ticket_id from tic where tic.ticket_id = t.ticket_id)
                                        and t.check_time between p_timestamp_from and p_timestamp_to
      ), -- время валидации и id валидации, т следующая вал и id валидации
      --select * from adata;
        f AS(select b.check_time , ticket_id, b.route_variant_muid, f.stop_place_muid as start_sp_muid, f1.stop_place_muid as end_stop
             from adata
               JOIN askp.validation2stops_binding b ON
                    b.check_time between p_timestamp_from and p_timestamp_to AND b.askp_check_id = adata.askp_check_id
                    -- две подряд валидации в одном ТС не анализируются
                    AND ( (adata.next_check_time - adata.check_time) > INTERVAL '30 second')
               JOIN tt.order_fact f
                    ON f.order_round_id = b.order_round_id
                    AND f.stop_order_num = b.stop_order_num
                    AND f.sign_deleted = 0
               JOIN askp.validation2stops_binding b1 ON
                    b1.check_time between p_timestamp_from and p_timestamp_to AND b1.askp_check_id = adata.next_askp_check_id
               JOIN tt.order_fact f1
                    ON f1.order_round_id = b1.order_round_id
                    AND f1.stop_order_num = b1.stop_order_num
                    AND f1.sign_deleted = 0
      ), -- вариант маршрута, остановка первой валидации и остановка следующей валидации
        kk AS(
          select distinct on (ticket_id, start_sp_muid, end_stop, t.muid)
            f.check_time, ticket_id::BIGINT, t.muid as end_stop_place_muid, route_variant_muid, start_sp_muid  from f
            JOIN luchininov.connected_stop_places2 t ON t.stop_places_muid = end_stop
            JOIN rt rt1 ON rt1.muid = f.route_variant_muid AND rt1.stop_place_muid = t.muid -- возможная конечная
            JOIN rt rt2 ON rt2.muid = f.route_variant_muid AND rt2.stop_place_muid = start_sp_muid -- начальная остановка
          -- ОП на одной траектории, и остановка высадки расположена дальше, остановки посадки
          WHERE rt1.order_num > rt2.order_num AND rt1.trajectory_type_muid = rt2.trajectory_type_muid
    )
    select * from kk;

  RETURN TRUE;
END;
$$ LANGUAGE plpgsql;