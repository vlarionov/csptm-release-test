--
CREATE OR REPLACE FUNCTION askp.create_askp_data(p_timestamp_from TIMESTAMP, p_timestamp_to TIMESTAMP, p_id BIGINT)
  RETURNS BOOLEAN AS $$
DECLARE
BEGIN
  -- при пересчете если такой id уже есть, delete
  DELETE FROM askp.askp_data
  WHERE asmpp_agg_item_id IN (SELECT asmpp_agg_item_id FROM askp.asmpp_agg_item WHERE asmpp_agg_id = p_id);

  INSERT INTO askp.askp_data (asmpp_agg_item_id, ticket_type_id, ticket_id, check_time)
      WITH a AS (
          SELECT
            asmpp_agg_item.*,
            tt.timetable_entry.timetable_entry_num
          FROM askp.asmpp_agg_item
            JOIN tt.order_round ON tt.order_round.order_round_id = askp.asmpp_agg_item.order_round_id
            JOIN tt.order_list ON tt.order_round.order_list_id = tt.order_list.order_list_id
                 AND tt.order_list.tr_id = askp.asmpp_agg_item.tr_id
                 AND tt.order_list.sign_deleted = 0
                 AND tt.order_list.is_active = 1
                 AND tt.order_list.order_date = ((p_timestamp_from :: TIMESTAMP) :: DATE) :: TIMESTAMP
            JOIN tt.timetable_entry ON tt.order_list.timetable_entry_id = tt.timetable_entry.timetable_entry_id
                 AND tt.timetable_entry.route_variant_muid = askp.asmpp_agg_item.route_variants_muid
                 AND tt.timetable_entry.sign_deleted = 0
          WHERE time_start BETWEEN p_timestamp_from + INTERVAL '3 hours' AND p_timestamp_to + INTERVAL '3 hours'
      ),
          b AS (
            SELECT
              askp.ticket_oper.check_time,
              askp.ticket_oper.ticket_type_id,
              askp.ticket_oper.ticket_id,
              askp.ticket_oper.move_num,
              muid
            FROM askp.askp_to_gis_route, askp.ticket_oper
            WHERE askp.ticket_oper.route_id = askp.askp_to_gis_route.askp_route_id
                  AND check_time BETWEEN p_timestamp_from + INTERVAL '3 hours' AND p_timestamp_to + INTERVAL '3 hours'
        )
      SELECT
        a.asmpp_agg_item_id,
        b.ticket_type_id,
        b.ticket_id,
        b.check_time
      FROM a
        JOIN b ON b.muid = a.route_variants_muid
                  AND b.move_num = a.timetable_entry_num
                  AND b.check_time >= a.time_start AND b.check_time <= a.time_finish;
  RETURN TRUE;
END;
$$ LANGUAGE plpgsql;