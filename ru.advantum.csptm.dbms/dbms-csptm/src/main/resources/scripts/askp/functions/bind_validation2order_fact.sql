-- процедура для ежедневной обработки данных АСКП и АСМПП, вызывается crone и обрабатывает данные за предыдущие сутки
CREATE OR REPLACE FUNCTION askp.bind_validations(p_from TIMESTAMP, p_to TIMESTAMP)
  RETURNS BOOLEAN AS $$
DECLARE
  -- время на которое привязываются валидации
  l_time_from TIMESTAMP := ((now()::DATE) - INTERVAL '2 day')::TIMESTAMP;
  l_time_to TIMESTAMP := l_time_from + INTERVAL '1 day';

  l_count_on_tech_stop SMALLINT := asd.get_constant('COUNT_ON_TECH_STOPS');
BEGIN
  IF p_from IS NOT NULL THEN
    l_time_from := p_from;
  END IF;
  IF p_to IS NOT NULL THEN
    l_time_to := p_to;
  END IF;
  INSERT into askp.validation2stops_binding(askp_check_id, time_fact, check_time,
    tr_type_id, tr_id, number, move_num, depo_id, route_variant_muid, vid_id, direction_id,stop_place_muid,order_num,
    garage_num,period,seat_qty_total,order_round_num, order_round_id, stop_order_num)
    with bb AS(
        select of.order_fact_id, of.time_fact as time_fact, of.stop_order_num, of.order_round_id, of.stop_place_muid
        from tt.order_fact of
        WHERE of.sign_deleted = 0 AND time_fact BETWEEN l_time_from AND l_time_to
        ORDER BY of.order_round_id , of.stop_order_num
    ),
        a AS(
          select bb.order_fact_id, time_fact as time_fact, (lag(time_fact, -1) OVER (ORDER BY order_round_id)) AS lag,
            stop_order_num, lag(stop_order_num, -1) OVER (ORDER BY order_round_id) - stop_order_num AS ord,
            order_round_id, stop_place_muid
          from bb
      ),
        b AS (
          SELECT e.name_short, tr_type.short_name, tr.tr_id, tr.depo_id, tr.tr_type_id, a.*, tr.garage_num, tr.seat_qty_total, tte.timetable_entry_num, r.number, a2g.askp_route_id,
            /*( SELECT ( MIN (time_fact + INTERVAL '3 hour'):: TIME ):: TEXT || ' - ' || (max(time_fact + INTERVAL '3 hour'):: TIME ):: TEXT FROM tt.order_fact ooo
            WHERE ooo.order_round_id = o_r.order_round_id AND sign_deleted = 0/* AND is_active = 1*/) AS Period,*/
            askp.format_time_period(o_r.order_round_id) AS Period,
            rrt.name VID, rrt.muid as vid_id, rtt.name direction, rtt.muid as direction_id, stops.name stop, s2t.order_num, tte.route_variant_muid,
            o_r.order_round_num
          FROM a
            JOIN tt.order_round o_r ON o_r.order_round_id = a.order_round_id AND o_r.is_active = 1
            JOIN tt.order_list ol ON ol.order_list_id = o_r.order_list_id AND ol.sign_deleted = 0 AND ol.is_active = 1
            JOIN core.tr tr ON tr.tr_id = ol.tr_id
            JOIN tt.timetable_entry tte ON tte.timetable_entry_id = ol.timetable_entry_id AND tte.sign_deleted = 0
            JOIN gis.route_variants rv ON rv.muid = tte.route_variant_muid AND rv.sign_deleted = 0
            JOIN gis.routes r ON rv.route_muid = r.muid
            JOIN askp.askp_to_gis_route a2g ON a2g.route_muid = rv.route_muid
            JOIN tt.round ON round.round_id = o_r.round_id
            JOIN gis.route_trajectories rt ON rt.muid = round.route_trajectory_muid
            JOIN gis.route_rounds rr ON rr.muid = rt.route_round_muid
            JOIN gis.ref_route_trajectory_types rtt ON rt.trajectory_type_muid = rtt.muid
            JOIN gis.stop_places sp ON sp.muid = a.stop_place_muid AND sp.sign_deleted = 0
                 -- учитывать или нет технологические установки (0 - не считать)
                 AND ( CASE WHEN l_count_on_tech_stop = 0 THEN sp.is_technical = 0 -- не технологические
                       ELSE TRUE -- все остановки
                       END)
            JOIN gis.stops ON stops.muid = sp.stop_muid
            JOIN gis.stop_place2route_traj s2t ON sp.muid = s2t.stop_place_muid AND rt.muid = s2t.route_trajectory_muid
            JOIN gis.ref_route_round_types rrt ON rrt.muid = rr.route_round_type_muid
            JOIN core.tr_type ON tr.tr_type_id = tr_type.tr_type_id
            JOIN core.entity e ON e.entity_id = tr.territory_id
      ),
        oper AS(
          SELECT * FROM askp.ticket_oper where
            check_time - interval '3 hours' BETWEEN l_time_from AND l_time_to
      ),
        c AS(select * FROM b WHERE ord = 1) -- иначе это конечная остановка, не нужно привязывать валидации
    select t.askp_check_id, c.time_fact, t.check_time, c.tr_type_id,
           c.tr_id, c.number, c.timetable_entry_num, c.depo_id, c.route_variant_muid,
      -- новое
      c.vid_id, c.direction_id, c.stop_place_muid, c.order_num, c.garage_num, c.Period, c.seat_qty_total, c.order_round_num, c.order_round_id, c.stop_order_num
      from c
      LEFT JOIN oper t ON t.check_time - interval '3 hours' BETWEEN c.time_fact - interval '30 second' AND c.lag - interval '30 second'
           AND t.move_num = c.timetable_entry_num AND t.route_id = c.askp_route_id
    where check_time IS NOT NULL AND time_fact IS NOT NULL AND order_fact_id IS NOT NULL;

  PERFORM askp.find_trip_tirminal_stop2(l_time_from, l_time_to);
  
  /* вызываем заполнение агрегата для отчетов.
     при заполнении будет учитываться интервал от 00:00:00 l_time_from до 23:59:59 l_time_to */
  PERFORM askp.aggregate_binding4reports(l_time_from, l_time_to);
  
  RETURN TRUE;
END;
$$ LANGUAGE plpgsql;

/*

WITH RECURSIVE  a AS(
  select action_type_name, action_type_id, action_type_id::text idd  from ttb.action_type where parent_type_id IS NULL
  UNION ALL
  select a.action_type_name||' \ '||t.action_type_name as action_type_name,
    t.action_type_id,
         t.action_type_id::text||' \ '||idd as idd
  from ttb.action_type t
    JOIN a ON t.parent_type_id = a.action_type_id
),
    proiz AS (
      SELECT *, array_length(regexp_split_to_array(idd, '\\'),1) FROM a
      WHERE idd LIKE '% 25 \\%' AND array_length(regexp_split_to_array(idd, '\\'),1) = 4
  ),
    arrs AS (
      SELECT
        array(SELECT action_type_id FROM proiz WHERE idd ~ '\D1 \\')  AS main,
        -- основной
        array(SELECT action_type_id FROM proiz WHERE idd !~ '\D1 \\') AS not_main -- не основной
  )
select * from proiz;



WITH t1 AS (select ai.time_begin , af.time_fact_begin, at.action_type_name, si2r.order_num, ar.round_status_id,
              greatest(-(af.time_fact_begin - ai.time_begin), (af.time_fact_begin - ai.time_begin))
            from ttb.tt_action_fact af
              JOIN ttb.tt_action a ON a.tt_action_id = af.tt_action_id
              JOIN ttb.action_type at ON at.action_type_id = a.action_type_id
              JOIN rts.stop_item2round si2r ON si2r.stop_item2round_id = af.stop_item2round_id
              JOIN ttb.tt_action_item ai ON ai.tt_action_id = af.tt_action_id
                                            AND ai.stop_item2round_id = af.stop_item2round_id
              JOIN ttb.tt_action_round ar ON ar.tt_action_id = af.tt_action_id
            ORDER BY af.tt_action_id, af.time_fact_begin
)
select *, extract(HOUR from greatest)*60 + extract(MIN from greatest) from t1;


-- валидации привязанные к варианту маршрута
select t1.*, a2g.rts_route_id, r.route_num, r.current_route_variant_id--, v.*
from askp.ticket_oper_1802 t1
JOIN askp.askp_to_gis_route a2g ON a2g.askp_route_id = t1.route_id
JOIN rts.route r ON r.route_id = a2g.rts_route_id
JOIN ttb.tt_variant v ON v.route_variant_id = r.current_route_variant_id
     AND v.parent_tt_variant_id IS NOT NULL -- оперативное расписание
     AND v.order_date = '2018-02-10'
JOIN ttb.tt_out out ON out.tt_out_num = t1.move_num
     AND out.tt_variant_id = v.tt_variant_id
where t1.check_time >= '2018-02-10 12:00:00' AND t1.check_time < '2018-02-10 12:10:00' -- 36132 -- 493 только привязали


-- фактическое исполнение рейсов, только пройденные рейсы
WITH RECURSIVE  a AS(
  select action_type_name, action_type_id, action_type_id::text idd  from ttb.action_type where parent_type_id IS NULL
  UNION ALL
  select a.action_type_name||' \ '||t.action_type_name as action_type_name,
    t.action_type_id,
         t.action_type_id::text||' \ '||idd as idd
  from ttb.action_type t
    JOIN a ON t.parent_type_id = a.action_type_id
),
    proiz AS (
      SELECT *, array_length(regexp_split_to_array(idd, '\\'),1) FROM a
      WHERE idd LIKE '% 25 \\%' AND array_length(regexp_split_to_array(idd, '\\'),1) = 4
  ),
    arrs AS (
      SELECT
        array(SELECT action_type_id FROM proiz WHERE idd ~ '\D1 \\')  AS main,
        -- основной
        array(SELECT action_type_id FROM proiz WHERE idd !~ '\D1 \\') AS not_main -- не основной
  ),
  rezult AS (
SELECT r.route_id, r.route_num, ooper.tt_out_num, ooper.tr_id, ttv.route_variant_id, ttv.order_date, a.tt_action_id, a.action_type_id, a.driver_id,
ai.time_begin, ai.driver_id, si2r.order_num,
af.time_fact_begin, lag(af.time_fact_begin) OVER ( PARTITION BY a.tt_action_id ORDER BY order_num DESC ),
ar.round_status_id, rs.round_status_name
FROM ttb.v_tt_out_oper ooper
JOIN ttb.tt_variant ttv ON ttv.tt_variant_id = ooper.tt_variant_id
JOIN rts.route r ON r.current_route_variant_id = ttv.route_variant_id
JOIN ttb.tt_action a ON a.tt_out_id = ooper.tt_out_id
JOIN ttb.tt_action_item ai ON ai.tt_action_id = a.tt_action_id
JOIN rts.stop_item2round si2r ON si2r.stop_item2round_id = ai.stop_item2round_id
LEFT JOIN ttb.tt_action_fact af ON af.stop_item2round_id = ai.stop_item2round_id
AND af.tt_action_id = a.tt_action_id
JOIN ttb.tt_action_round ar ON ar.tt_action_id = a.tt_action_id
JOIN ttb.round_status rs ON rs.round_status_id = ar.round_status_id
WHERE ttv.order_date = '2018-02-09'
AND a.action_type_id IN ( SELECT action_type_id FROM proiz)
AND rs.round_status_id = 1 -- пройден
ORDER BY a.tt_action_id, si2r.order_num
)
select *
  -- количество валидаций
  /*(select COUNT(*)
   from askp.ticket_oper_1802 t1
   JOIN askp.askp_to_gis_route a2g ON a2g.askp_route_id = t1.route_id
   where a2g.rts_route_id = route_id AND tt_out_num = t1.move_num
         AND t1.check_time >= (time_fact_begin + INTERVAL '3 hour') AND t1.check_time <= (lag + INTERVAL '3 hour')
  )*/
from rezult



/*UPDATE askp.askp_to_gis_route
SET rts_route_id = (select route_id from rts.route2gis r2g where r2g.route_muid = askp_to_gis_route.route_muid)*/

*/