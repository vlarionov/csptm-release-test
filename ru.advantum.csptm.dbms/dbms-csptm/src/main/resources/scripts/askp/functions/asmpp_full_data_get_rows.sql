drop function askp.asmpp_full_data_get_rows(NUMERIC,TEXT);
CREATE OR REPLACE FUNCTION askp.asmpp_full_data_get_rows(p_id_account NUMERIC, p_attr TEXT)
    RETURNS TABLE (name_short text,
                   tr_id bigint,
                   garage_num bigint,
                   seat_qty_total smallint,
                   short_name text,
                   number text,
                   timetable_entry_num smallint,
                   timetable_entry_id bigint,
                   timetable_id bigint,
                   order_round_num bigint,
                   order_round_id bigint,
                   period text,
                   vid text,
                   vid_muid bigint,
                   direction text,
                   direction_id smallint,
                   stop text,
                   stop_muid bigint,
                   stop_place_muid bigint,
                   order_num smallint,
                   time_start timestamp,
                   in_corrected smallint,
                   out_corrected smallint,
                   total_pass smallint,
                   broken text,
                   is_broken boolean, --, l_broken_rounds
                   sum_out smallint,
                   sum_in smallint,
                   raw_in smallint,
                   raw_out smallint)
LANGUAGE plpgsql
AS $$
DECLARE
  f_dt_start 			timestamp 	:= jofl.jofl_pkg$extract_date(p_attr, 'BGN_DT', true);
  f_dt_end	 			timestamp	:= jofl.jofl_pkg$extract_date(p_attr, 'END_DT', true);
  l_stops	 			TEXT[]	:= jofl.jofl_pkg$extract_tarray(p_attr, 'F_id', true);
  l_json_str TEXT := '['||array_to_string(l_stops,', ')||']';
  l_tr_id	 			BIGINT[]	:= jofl.jofl_pkg$extract_narray(p_attr, 'F_tr_id', true);

  --l_route BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_route_muid', true);
  l_routes BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_route_id', true);-- RTS
  l_route BIGINT[]  := array(select route_muid from rts.route2gis where route_id = ANY(l_routes) );
  l_number TEXT[]   := ARRAY(select upper(gr.number) from gis.routes gr where gr.muid = ANY(l_route));
  l_rv BIGINT[]     := ARRAY(select current_route_variant_muid from gis.routes where muid = ANY(l_route));
  -- выход
  l_move_num BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_timetable_entry_num', true);
  -- остановки
  l_gis_stops_muid BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_gisMUID', true);
  l_gis_sp_muid BIGINT[] := array(select sp.muid from gis.stop_places sp where sp.stop_muid = ANY(l_gis_stops_muid));

  -- траектория прямая, обратная
  l_direction SMALLINT := jofl.jofl_pkg$extract_number(p_attr, 'f_dir_MUID', true);
  -- вид рейса укороченный, экспресс и т.д.
  l_route_type BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_rtype_MUID', true);

  -- хорошие рейсы со статусом пройден
  l_completed_order_round BIGINT[] := array(select orv.order_round_id
                                              from asd.oper_round_visit orv
                                             where orv.time_fact_begin BETWEEN f_dt_start AND f_dt_end AND
                                                   orv.order_round_status_id = 1);

  -- остановки для данного номера маршрута
  l_route_stop_places BIGINT[] := array(select distinct on (sp2rt.stop_place_muid)
                                               sp2rt.stop_place_muid
                                          from gis.stop_place2route_traj sp2rt
                                          JOIN gis.route_trajectories rt ON rt.muid = sp2rt.route_trajectory_muid
                                          JOIN gis.route_rounds rr ON rr.muid = rt.route_round_muid and
                                                                      (l_direction IS NULL OR rt.trajectory_type_muid = l_direction)
                                          JOIN gis.routes t1 ON t1.current_route_variant_muid = rr.route_variant_muid
                                          JOIN gis.routes t2 ON t2.muid = ANY(l_route) and
                                                                t1.number = t2.number and
                                                                t1.current_route_variant_muid IS NOT NULL);

  l_count_on_tech_stop SMALLINT := asd.get_constant('COUNT_ON_TECH_STOPS');
  l_max_in_out_difference NUMERIC := asd.get_constant('ASMPP_IN_OUT_DIFFERENCE') / 100.0;

  -- показывать заброкованные рейсы
  -- "K1", "V":"Да"}, {"K": "K2", "V":"Нет"},{"K": "K3", "V":"Все"}
  l_broken_rounds TEXT := jofl.jofl_pkg$extract_varchar(p_attr, 'F_BROKEN_INPLACE_K', true);
BEGIN
  return query
  WITH
  a AS ( -- рейсы наряда за эти дни
       select distinct on (o_round.order_round_id) o_round.order_round_id,
              o_round.order_round_num, tte.timetable_entry_num, tte.timetable_entry_id, tte.timetable_id,
              round.round_code, round.direction_id,
              tr.garage_num, tr.seat_qty_total, tr.tr_id, tr_type.short_name,
              tr_type.short_name || ' ' || r.number AS NUMBER,
              e.name_short,
              askp.format_time_period(o_round.order_round_id) AS Period,
              rtt.name direction,
              rrt.name vid,
              rrt.muid as vid_muid
        from askp.asmpp_agg_item agg
        JOIN tt.order_round o_round ON o_round.order_round_id = agg.order_round_id and
                                       o_round.order_round_id = ANY(l_completed_order_round)
        JOIN tt.order_list ol ON ol.order_list_id = o_round.order_list_id
        JOIN tt.timetable_entry tte ON tte.timetable_entry_id = ol.timetable_entry_id and
                                       (tte.timetable_entry_num = ANY(l_move_num) OR array_length(l_move_num,1) IS NULL )
        JOIN tt.round round ON round.round_id = o_round.round_id
        JOIN core.tr tr ON tr.tr_id = ol.tr_id and
                           (tr.tr_id = ANY(l_tr_id) OR array_length(l_tr_id,1) IS NULL )
        JOIN core.tr_type ON tr.tr_type_id = tr_type.tr_type_id
        JOIN gis.route_variants rv ON rv.muid = tte.route_variant_muid and
                                      (route_variant_muid = ANY(l_rv) OR array_length(l_rv,1) IS NULL)
        JOIN gis.route_rounds rr ON rr.code = round.round_code AND rr.route_variant_muid = rv.muid
        JOIN gis.routes r ON r.current_route_variant_muid = rv.muid and
                             (r.number = ANY(l_number) OR array_length(l_number,1) IS NULL)
        JOIN core.entity e ON e.entity_id = tr.depo_id
        JOIN gis.ref_route_trajectory_types rtt ON rtt.muid = round.direction_id and
                                                   (l_direction IS NULL OR rtt.muid = l_direction)
        JOIN gis.ref_route_round_types rrt ON rrt.muid = rr.route_round_type_muid and
                                              (rrt.muid = ANY(l_route_type) OR array_length(l_route_type,1) IS NULL )
      where agg.time_start BETWEEN f_dt_start AND f_dt_end
      order by o_round.order_round_id),
  b AS (SELECT agg.in_corrected, agg.out_corrected, agg.total_pass_before, agg.total_pass_after,
               ofc.stop_place_muid as stop_place_muid, ofc.stop_order_num, ofc.time_fact, s.name stop, s.muid as stop_muid,  a.*,
               lead(ofc.stop_order_num) OVER (PARTITION BY ofc.order_round_id, sp.is_technical ORDER BY ofc.stop_order_num ASC) as lead,
               count(*) OVER (PARTITION BY ofc.order_round_id) as stationNum,
               sp.is_technical,
               sum(agg.in_corrected) OVER (PARTITION BY ofc.order_round_id) sum_in,
               sum(agg.out_corrected) OVER (PARTITION BY ofc.order_round_id) sum_out,
               in_cnt, out_cnt
          from tt.order_fact ofc
          JOIN a ON a.order_round_id = ofc.order_round_id
          JOIN gis.stop_places sp ON sp.muid = ofc.stop_place_muid and
                                     (sp.muid = ANY(l_gis_sp_muid) OR array_length(l_gis_sp_muid,1) IS NULL ) and
                                     (sp.muid = ANY(l_route_stop_places) OR array_length(l_route_stop_places,1) IS NULL)
         JOIN gis.stops s ON s.muid = sp.stop_muid
         LEFT JOIN askp.asmpp_agg_item agg ON agg.order_round_id = ofc.order_round_id and
                                              agg.stop_order_num = ofc.stop_order_num and
                                              ofc.sign_deleted = 0
            order by a.order_round_id, ofc.stop_order_num),
  rr AS (select t2.name_short, t2.garage_num, t2.seat_qty_total, t2.tr_id, t2.short_name, t2.number,
                t2.timetable_entry_num, t2.timetable_entry_id, t2.timetable_id, t2.order_round_num, t2.period,
                t2.vid, t2.vid_muid,  t2.direction, t2.direction_id, t2.stop, t2.stop_muid, t2.stop_place_muid, t2.stop_order_num order_num,
                (t2.time_fact + INTERVAL '30 second') time_start,
                COALESCE(t2.in_corrected, 0) in_corrected,
                COALESCE(t2.out_corrected, 0) out_corrected,
                COALESCE(CASE WHEN t2.total_pass_after IS NULL
                          THEN (select t1.total_pass_after
                                from b t1
                                where t1.order_round_id = t2.order_round_id and
                                      t1.stop_order_num < t2.stop_order_num and
                                      t1.total_pass_after IS NOT NULL
                                order by t1.order_round_id,
                                         t1.stop_order_num desc
                                LIMIT 1)
                          ELSE t2.total_pass_after
                         END, 0) total_pass,
             t2.lead, t2.stationNum, t2.order_round_id, t2.is_technical,
             CASE WHEN t2.sum_out = 0
              THEN NULL
              ELSE t2.sum_in::NUMERIC/t2.sum_out::NUMERIC
             END u1,
             CASE WHEN t2.sum_in = 0
              THEN NULL
              ELSE t2.sum_out::NUMERIC/t2.sum_in::NUMERIC
             end u2,
             t2.sum_out, t2.sum_in, t2.in_cnt, t2.out_cnt
  from b t2),
  u AS (-- order_round_id и номер остановки где все выходят и никто не входит
        select distinct
               rtab.order_round_id,
               CASE
                WHEN l_count_on_tech_stop = 1
                  THEN last_value(rtab.order_num) OVER (PARTITION BY rtab.order_round_id)
                ELSE first_value(rtab.order_num) OVER (PARTITION BY rtab.order_round_id)
               END as order_num
        from rr rtab
        where rtab.lead IS NULL AND
              rtab.order_num > rtab.stationNum-3),
  final_table AS (select tr.name_short, tr.garage_num, tr.seat_qty_total, tr.tr_id, tr.short_name, tr.number,
                         tr.timetable_entry_num, tr.timetable_entry_id, tr.timetable_id, tr.order_round_num, tr.order_round_id, tr.period,
                         tr.vid, tr.vid_muid, tr.direction, tr.direction_id,
                         tr.stop, tr.stop_muid, tr.stop_place_muid, tr.order_num, tr.time_start,
                         CASE WHEN ( tr.order_num >= tu.order_num )
                          THEN 0 -- на последней остановке никто не входит
                          ELSE COALESCE(tr.in_corrected, 0)
                         END in_corrected,
                         CASE
                          WHEN ( tr.order_num = tu.order_num )
                           THEN COALESCE( lag(tr.total_pass) OVER (PARTITION BY tr.order_round_id ORDER BY tr.order_num ASC), 0)  -- на последней остановке все выходят "принудительно"
                          WHEN ( tr.order_num > tu.order_num )
                           THEN 0
                          ELSE COALESCE(tr.out_corrected, 0)
                         END as out_corrected,
                         CASE WHEN (tr.order_num >= tu.order_num)
                          THEN 0
                          ELSE tr.total_pass
                         END as total_pass,
                         round(tr.u1,1)||' '||round(tr.u2,1) as broken,
                         CASE WHEN ( (tr.u1 > (1 + l_max_in_out_difference)) OR (tr.u2 > (1 + l_max_in_out_difference)) )
                          THEN TRUE
                          ELSE FALSE
                         END as is_broken, --, l_broken_rounds
                         tr.sum_out, tr.sum_in,
                         coalesce(tr.in_cnt,0) raw_in,
                         coalesce(tr.out_cnt,0) raw_out
                  from rr tr
                  JOIN u tu ON tu.order_round_id = tr.order_round_id)
  /* главный select */
  SELECT ft.name_short,
         ft.tr_id::bigint,
         ft.garage_num::bigint,
         ft.seat_qty_total::smallint,
         ft.short_name,
         ft.number,
         ft.timetable_entry_num,
         ft.timetable_entry_id::bigint,
         ft.timetable_id::bigint,
         ft.order_round_num::bigint,
         ft.order_round_id::bigint,
         ft.period,
         ft.vid,
         ft.vid_muid::bigint,
         ft.direction,
         ft.direction_id::smallint,
         ft.stop,
         ft.stop_muid::bigint,
         ft.stop_place_muid::bigint,
         ft.order_num::smallint,
         ft.time_start,
         ft.in_corrected::smallint,
         ft.out_corrected::smallint,
         ft.total_pass::smallint,
         ft.broken,
         ft.is_broken, --, l_broken_rounds
         ft.sum_out::smallint,
         ft.sum_in::smallint,
         ft.raw_in::smallint,
         ft.raw_out::smallint
    from final_table ft
   WHERE ft.is_broken =  CASE WHEN l_broken_rounds = 'K1' THEN TRUE -- забракованные
                              WHEN l_broken_rounds = 'K2' THEN FALSE -- НЕ забракованные
                              WHEN l_broken_rounds = 'K3' THEN ft.is_broken -- все рейсы
                              WHEN l_broken_rounds = '' THEN ft.is_broken -- все рейсы
                         END;
END;
$$;
