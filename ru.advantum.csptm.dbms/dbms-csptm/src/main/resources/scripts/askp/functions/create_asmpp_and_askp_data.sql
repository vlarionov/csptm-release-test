
CREATE OR REPLACE FUNCTION askp.create_asmpp_and_askp_data(timestamp_from TIMESTAMP, timestamp_to TIMESTAMP, id BIGINT)
  RETURNS BOOLEAN AS $$
DECLARE
BEGIN
  PERFORM askp.create_agg_items(timestamp_from, timestamp_to, id);
  PERFORM askp.create_askp_data(timestamp_from, timestamp_to, id);
  RETURN TRUE;
END;
$$ LANGUAGE plpgsql;