-- drop function askp.askp_asmpp_get_rows(numeric, text);
CREATE OR REPLACE FUNCTION askp.askp_asmpp_get_rows(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TABLE (
   tp text, 
   garage_num integer, 
   seat_qty_total integer, 
   tr_type text,
   number text,
   move_num smallint,
   order_round_num smallint, 
   period text, 
   vid text, 
   direction text, 
   stop text, 
   order_num bigint, 
   time_fact timestamp,
   "interval" text, 
   count bigint,
   in_cnt bigint, 
   out_cnt bigint, 
   total_pass_after bigint, 
   total_pass_before bigint,
   order_round_id bigint,
   in_corrected bigint,     -- CASE WHEN is_end = 1 THEN 0 ELSE in_corrected END 
   out_corrected bigint,    -- CASE WHEN is_end = 1 THEN corrected_total2 ELSE out_corrected END
   corrected_total bigint,  -- CASE WHEN is_end = 1 THEN 0 ELSE corrected_total END
   corrected_total2 bigint  -- CASE WHEN is_end = 1 THEN 0 ELSE corrected_total2 END
  )
LANGUAGE plpgsql
AS $$
DECLARE
  f_dt_start 		TIMESTAMP 	:= jofl.jofl_pkg$extract_date(p_attr, 'BGN_DT', true);
  f_dt_end	 		TIMESTAMP	:= jofl.jofl_pkg$extract_date(p_attr, 'END_DT', true);

  --l_route BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_route_muid', true);
  l_routes BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_route_id', true);-- RTS
  l_route BIGINT[] := array(select r2g.route_muid from rts.route2gis r2g where r2g.route_id = ANY(l_routes) );
  l_number TEXT[] := ARRAY(select upper(r.number) from gis.routes r where r.muid = ANY(l_route));
  -- варианты маршрута выбранных маршрутов
  l_rv BIGINT[] := ARRAY(select r.current_route_variant_muid from gis.routes r where r.muid = ANY(l_route));
  -- id ТС
  l_tr_id BIGINT[]	:= jofl.jofl_pkg$extract_narray(p_attr, 'F_tr_id', true);
  -- вид рейса
  l_route_type BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_rtype_MUID', true);
  -- траектория прямая, обратная
  l_direction SMALLINT := jofl.jofl_pkg$extract_number(p_attr, 'f_dir_MUID', true);
  -- выход
  l_move_num BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_timetable_entry_num', true);
  -- допустимое отклонение АСКП и АСМПП
  l_delta SMALLINT := asd.get_constant('ASMPP_ASKP_DELTA');

  l_gis_stops_muid BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_gisMUID', true);
  l_gis_sp_muid BIGINT[] := array(select sp.muid from gis.stop_places sp where sp.stop_muid = ANY(l_gis_stops_muid));

  -- остановки маршрута
  l_gis_sp_muid2 BIGINT[] := array(select distinct on (sp2rt.stop_place_muid) stop_place_muid
                                   from gis.stop_place2route_traj sp2rt
                                     JOIN gis.route_trajectories rt ON rt.muid = sp2rt.route_trajectory_muid AND rt.sign_deleted = 0
                                     JOIN gis.ref_route_trajectory_types rtt ON rt.trajectory_type_muid = rtt.muid
                                          AND (l_direction IS NULL OR rtt.muid = l_direction)
                                     JOIN gis.stop_places sp ON sp.muid = sp2rt.stop_place_muid AND sp.sign_deleted = 0
                                     JOIN gis.stops stops ON stops.muid = sp.stop_muid AND stops.sign_deleted = 0
                                     JOIN gis.route_rounds rr ON rr.muid = rt.route_round_muid AND rr.code = '00' AND rr.sign_deleted = 0
                                     JOIN gis.routes r ON r.current_route_variant_muid = rr.route_variant_muid AND r.sign_deleted = 0
                                          AND r.number = ANY(l_number));

  l_max_in_out_difference NUMERIC := asd.get_constant('ASMPP_IN_OUT_DIFFERENCE') / 100.0;

  l_time_error_interval INTERVAL := make_interval(secs => (30 - asd.get_constant('TIME_FACT_ERROR'))::int);
  l_shift_interval INTERVAL := INTERVAL '3 hour' - INTERVAL '30 SECOND' + l_time_error_interval;
BEGIN
return query
--
  WITH round_ids AS (SELECT distinct v2sb.order_round_id
                     from askp.validation2stops_binding v2sb
                     WHERE v2sb.check_time BETWEEN f_dt_start AND f_dt_end
                           AND (v2sb.route_variant_muid = ANY(l_rv) OR array_length(l_rv, 1) IS NULL )
                           AND (v2sb.move_num = ANY(l_move_num) OR array_length(l_move_num,1) IS NULL )
                           AND (l_direction IS NULL OR v2sb.direction_id = l_direction)
                           AND (v2sb.tr_id = ANY(l_tr_id) OR array_length(l_tr_id,1) IS NULL )
                           AND v2sb.order_round_id IS NOT NULL),
  all_data AS (SELECT e.name_short,
                      tr.garage_num,
                      tr.seat_qty_total,
                      tr_type.name,
                      r.number,
                      timetable_entry_num,
                      o_round.order_round_num,
                      askp.format_time_period(o_round.order_round_id) AS fperiod,
                      rrt.name vid,
                      rtt.name direction,
                      s.name stop,
                      of.stop_order_num,
          --of.order_round_id,
                      of.time_fact,
                      COALESCE(lead(of.time_fact) OVER (PARTITION BY o_round.order_round_num, o_round.order_round_id ORDER BY of.time_fact ASC ),
                               of.time_fact + INTERVAL '120 second') AS lead_time_fact,
          /*lead(order_fact_id)
          OVER (
            PARTITION BY order_round_num, o_round.order_round_id
            ORDER BY time_fact ASC ) lead_of_id, order_fact_id,*/
                      lead(stop_order_num) OVER (PARTITION BY o_round.order_round_num, o_round.order_round_id ORDER BY of.time_fact ASC) lead_order_num,
                      o_round.order_round_id,
                      sp.is_technical
               FROM round_ids
                    JOIN tt.order_round o_round ON o_round.order_round_id = round_ids.order_round_id
                                                   AND o_round.order_round_id IN (SELECT orv.order_round_id /* только хорошие рейсы */
                                                                                  FROM asd.oper_round_visit orv
                                                                                  WHERE orv.time_fact_begin BETWEEN f_dt_start AND f_dt_end--'2017-12-04 00:00:00' AND '2017-12-05 00:00:00'
                                                                                        AND orv.order_round_status_id = 1)
                    JOIN tt.order_list ol ON ol.order_list_id = o_round.order_list_id
                    JOIN tt.timetable_entry tte ON tte.timetable_entry_id = ol.timetable_entry_id
                                                   AND (tte.timetable_entry_num = ANY(l_move_num) OR array_length(l_move_num,1) IS NULL)
                    JOIN tt.round round ON round.round_id = o_round.round_id
                    JOIN core.tr tr ON tr.tr_id = ol.tr_id
                                       AND (tr.tr_id = ANY(l_tr_id) OR array_length(l_tr_id,1) IS NULL )
                    JOIN core.tr_type ON tr.tr_type_id = tr_type.tr_type_id
                    JOIN gis.route_variants rv ON rv.muid = tte.route_variant_muid
                    JOIN gis.route_rounds rr ON rr.code = round.round_code AND rr.route_variant_muid = rv.muid
                    JOIN gis.routes r ON r.current_route_variant_muid = rv.muid
                    JOIN core.entity e ON e.entity_id = tr.depo_id
                    JOIN gis.ref_route_trajectory_types rtt ON rtt.muid = round.direction_id
                                                               AND (l_direction IS NULL OR rtt.muid = l_direction)
                    JOIN gis.ref_route_round_types rrt ON rrt.muid = rr.route_round_type_muid
                                                          AND (rrt.muid = ANY(l_route_type) OR array_length(l_route_type,1) IS NULL )
                    JOIN tt.order_fact of ON of.order_round_id = round_ids.order_round_id
                                             AND of.sign_deleted = 0
                    JOIN gis.stop_places sp ON sp.muid = of.stop_place_muid
                                               AND (sp.muid = ANY(l_gis_sp_muid2) OR array_length(l_gis_sp_muid2,1) IS NULL)
                                               AND (sp.muid = ANY(l_gis_sp_muid) OR array_length(l_gis_sp_muid,1) IS NULL)
                    JOIN gis.stops s ON s.muid = sp.stop_muid
               ORDER BY o_round.order_round_id, stop_order_num),
  join_asmpp AS(select t2.*,
                       (SELECT count(*)
                        from askp.validation2stops_binding av2sb
                        where /*order_fact_id = t2.order_fact_id OR order_fact_id = t2.lead_of_id*/
                              (av2sb.order_round_id = t2.order_round_id AND (stop_order_num = t2.stop_order_num OR stop_order_num = lead_order_num))
                              AND check_time > (t2.time_fact + INTERVAL '3 hour' - INTERVAL '30 SECOND')
                              AND check_time <= (lead_time_fact + INTERVAL '3 hour' - INTERVAL '30 SECOND'/*l_shift_interval*/)),
                       COALESCE(item.in_cnt, 0) in_cnt,
                       COALESCE(item.out_cnt, 0) out_cnt,
                       item.total_pass_before, item.total_pass_after,
                       item.in_corrected, item.out_corrected
                from all_data t2
                     LEFT JOIN askp.asmpp_agg_item item ON item.order_round_id = t2.order_round_id
                                                           AND item.stop_order_num = t2.stop_order_num),
    /*join_asmpp AS(
        select *,
          sum(join_asmpp2.in_cnt) OVER (PARTITION BY join_asmpp2.order_round_id) sum_in,
          sum(join_asmpp2.out_cnt) OVER (PARTITION BY join_asmpp2.order_round_id) sum_out
        from join_asmpp2
    ),*/
   corrected_data AS (SELECT t3.name_short as tp, t3.garage_num, t3.seat_qty_total, name tr_type, t3.number, timetable_entry_num move_num,
                             t3.order_round_num, t3.fperiod as period, t3.vid, t3.direction, t3.stop, stop_order_num stop_num, t3.time_fact,
                             to_char((lead_time_fact-t3.time_fact), 'HH24:MI:SS') as "interval",
                             t3.count, t3.in_cnt, t3.out_cnt,
                             COALESCE(CASE
                                       WHEN t3.total_pass_after IS NULL THEN (select t11.total_pass_after
                                                                              from join_asmpp t11
                                                                              where t11.order_round_id = t3.order_round_id
                                                                                    AND t11.stop_order_num < t3.stop_order_num
                                                                                    AND t11.total_pass_after IS NOT NULL
                                                                              order by t11.stop_order_num desc
                                                                              LIMIT 1)
                                       ELSE t3.total_pass_after
                                      END, 0) total_pass_after,
                             COALESCE(CASE
                                       WHEN t3.total_pass_before IS NULL THEN -- берем из другого столбца
                                                                           (select t12.total_pass_after
                                                                            from join_asmpp t12
                                                                            where t12.order_round_id = t3.order_round_id
                                                                                  AND t12.stop_order_num < t3.stop_order_num
                                                                                  AND t12.total_pass_before IS NOT NULL
                                                                            order by t12.stop_order_num desc
                                                                            LIMIT 1)
                                       ELSE t3.total_pass_before
                                      END, 0) total_pass_before,
                             -- если превысило допустимую разницу
                             CASE
                              WHEN (@(COALESCE(t3.in_corrected,0) - t3.count) > l_delta) THEN t3.count
                              ELSE ceil((t3.count + COALESCE(t3.in_corrected,0))/2)
                             END AS in_corrected,
                             COALESCE(t3.out_corrected,0) out_corrected,
                             t3.order_round_id,
                             -- 0 если нет АСКП
                            sum( COALESCE(t3.out_corrected, 0) ) over (PARTITION BY t3.order_round_id) as out_corrected2,
                            lead(stop_order_num) OVER (w) as lead_, lead(stop_order_num,2) OVER (w) as lead_2,
                            lead(t3.is_technical) OVER (w) as lead_is_technical
                      from join_asmpp t3
                      WINDOW w AS (PARTITION BY t3.order_round_id ORDER BY stop_order_num ASC)
      -- только хорошие рейсы (не превышен порог вошедших и вышедших)
      /*where ( sum_in = 0 AND sum_out = 0 ) -- нет данных АСМПП
      OR (CASE WHEN sum_in > 0 THEN ( ((sum_out / sum_in) > (1-l_max_in_out_difference)) AND ((sum_out / sum_in) < (1+l_max_in_out_difference)) )
               WHEN sum_out > 0 THEN ( ((sum_in / sum_out) > (1-l_max_in_out_difference)) AND ((sum_in/sum_out) < (1+l_max_in_out_difference)) )
          END
        )*/
    ),
  final_data AS (select cd.tp, cd.garage_num, cd.seat_qty_total, cd.tr_type, cd.number, cd.move_num,
                        cd.order_round_num, cd.period, cd.vid, cd.direction, cd.stop, cd.stop_num as order_num,
                        (cd.time_fact + INTERVAL '30 second') as time_fact,-- округление до мин
                        cd."interval", cd.count, cd.in_cnt, cd.out_cnt, cd.total_pass_after, cd.total_pass_before,
                        cd.in_corrected, cd.out_corrected,
                        CASE
                         WHEN cd.out_corrected2 > 0 THEN sum(cd.in_corrected - cd.out_corrected ) over (PARTITION BY cd.order_round_id ORDER BY cd.stop_num ASC)
                         --ElSE null
                        END as corrected_total,
                        cd.order_round_id, cd.lead_, cd.lead_2, cd.lead_is_technical,
                        CASE
                         WHEN ((cd.lead_2 IS NULL) AND ((cd.lead_ IS NULL) OR (cd.lead_is_technical=1))) THEN 1
                         ELSE 0
                        END as is_end
                 from corrected_data cd),
  total_data AS (select fd.*,
                        lag(fd.corrected_total) OVER (PARTITION BY fd.order_round_id ORDER BY fd.order_num) as corrected_total2
                 from final_data fd),
  end_table AS(select td.tp, td.garage_num, td.seat_qty_total, td.tr_type, td.number, td.move_num,
                      td.order_round_num, td.period, td.vid, td.direction, td.stop, td.order_num, td.time_fact,
                      td."interval", td.count, td.in_cnt, td.out_cnt, td.total_pass_after, td.total_pass_before,
                      td.in_corrected, td.out_corrected, /*corrected_total,*/ td.order_round_id,
                      case
                       WHEN td.corrected_total < 0 THEN 0
                       ELSE td.corrected_total
                      END as corrected_total,
                      case
                       WHEN td.corrected_total2 < 0 THEN 0
                       ELSE td.corrected_total2
                      END as corrected_total2,
                      td.lead_, td.lead_is_technical, td.is_end
               from total_data td)
--
  select
   et.tp::text,
   et.garage_num::integer,
   et.seat_qty_total::integer,
   et.tr_type::text,
   et.number::text,
   et.move_num::smallint,
   et.order_round_num::smallint,
   et.period::text,
   et.vid::text,
   et.direction::text,
   et.stop::text,
   et.order_num::bigint,
   et.time_fact::timestamp,
   et."interval"::text,
   et.count::bigint,
   et.in_cnt::bigint,
   et.out_cnt::bigint,
   et.total_pass_after::bigint,
   et.total_pass_before::bigint,
   et.order_round_id::bigint,
   (CASE WHEN et.is_end = 1 THEN 0 ELSE et.in_corrected END)::bigint in_corrected,
   (CASE WHEN et.is_end = 1 THEN 0 ELSE et.out_corrected END)::bigint out_corrected,
   (CASE WHEN et.is_end = 1 THEN 0 ELSE et.corrected_total END)::bigint corrected_total,
   (CASE WHEN et.is_end = 1 THEN 0 ELSE et.corrected_total2 END)::bigint corrected_total2
  from end_table et;
END;
$$;
