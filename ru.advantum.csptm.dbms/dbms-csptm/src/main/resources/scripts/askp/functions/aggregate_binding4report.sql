-- Процедура подготовки предагрегированных данных для отчетов АСКП
CREATE OR REPLACE FUNCTION askp.aggregate_binding4reports(p_from TIMESTAMP, p_to TIMESTAMP)
  RETURNS BOOLEAN AS $$
DECLARE
  -- начальная и конечная дата для агрегации
  l_time_from TIMESTAMP := date_trunc('day', now()) - interval '2 day';
  l_time_to TIMESTAMP := l_time_from + interval '23:59:59';
BEGIN
  -- включаем в рассмотрение полный интервал от 00:00:00 p_from до 23:59:59 p_to
  IF p_from IS NOT NULL THEN
    l_time_from := date_trunc('day', p_from);   
  END IF;
  IF p_to IS NOT NULL THEN
    l_time_to := date_trunc('day', p_to) + interval '23:59:59';
  END IF;

  /* предварительно удаляем записи */
  delete from askp.validation4reports_agg v4rag
   where v4rag.check_time between l_time_from and l_time_to;
   
  /* строим почасовой агрегат по количеству проездов в разрезе "льгтности" билетов, маршрута и атрибутов транспортного средства */
  insert into askp.validation4reports_agg
  select v2sb.check_hour as check_time,
         v2sb.tr_type_id,
         v2sb.tr_id,
         v2sb.route_number,
         v2sb.move_num,
         v2sb.depo_id,
         sum(case when not askp_tt.is_privilege
                   then 1
                   else 0
             end) as check_ordinary,
         sum(case when askp_tt.is_privilege
                   then 1
                   else 0
             end) as check_privilege,
         count(1) as check_all
  from ( /* устраняем возможные задвоения записей о проездах в askp.validation2stops_binding */
        select distinct src_data.askp_check_id,
		                src_data.check_time,
						date_trunc('hour', src_data.check_time) as check_hour,
                        src_data.tr_type_id,
                        src_data.tr_id,
                        src_data.number as route_number,
                        src_data.move_num,
                        src_data.depo_id
        from askp.validation2stops_binding src_data
		where src_data.check_time between l_time_from and l_time_to) v2sb
       join (select *
             from askp.ticket_oper ltiop
             where ltiop.check_time between l_time_from and l_time_to) tiop on tiop.askp_check_id = v2sb.askp_check_id
       join askp.ticket_type askp_tt on askp_tt.ticket_type_id = tiop.ticket_type_id
  group by v2sb.check_hour,
           v2sb.tr_type_id,
           v2sb.tr_id,
           v2sb.route_number,
           v2sb.move_num,
           v2sb.depo_id;
  
  RETURN TRUE;
END;
$$ LANGUAGE plpgsql;
