-- балансировка показаний АСМПП входящих и выходящих пассажиров
CREATE OR REPLACE FUNCTION
  askp.balancing_asmpp(p_timestamp_from TIMESTAMP, p_timestamp_to TIMESTAMP, p_id BIGINT)
  RETURNS BOOLEAN AS $$
DECLARE
    x_agg_items CURSOR FOR SELECT asmpp_agg_item_id,
                             askp.asmpp_agg_item.order_round_id,
                             time_start,
                             askp.asmpp_agg_item.in_cnt as innn,
                             askp.asmpp_agg_item.out_cnt as outtt,
                             core.tr.seat_qty_total AS total_seats
                           FROM askp.asmpp_agg_item
                             JOIN core.tr ON core.tr.tr_id = askp.asmpp_agg_item.tr_id
                                             AND core.tr.seat_qty_total > 0
                                             AND core.tr.seat_qty_total IS NOT NULL
                                             AND core.tr.dt_end IS NULL
                           WHERE time_start BETWEEN p_timestamp_from AND p_timestamp_to
                                 AND askp.asmpp_agg_item.asmpp_agg_id = p_id
                           Order BY order_round_id, time_start;

  l_row RECORD;
  l_total_corrected integer = 0;
  l_overr integer;

  l_order_round_prev BIGINT = null;
  l_order_round_curr BIGINT = null;
  l_balancing_koeff numeric = 1;

  l_before_sp SMALLINT;
  l_after_sp SMALLINT;
BEGIN
  l_balancing_koeff := asd.get_constant('ASMPP_BALANCING_COEFFICIENT');
  FOR l_row IN x_agg_items LOOP
    SELECT l_row.order_round_id INTO l_order_round_curr;
    IF l_order_round_prev ISNULL OR l_order_round_curr<>l_order_round_prev THEN
      l_order_round_prev := l_order_round_curr;
      UPDATE askp.asmpp_agg_item SET out_corrected = 0,
        in_corrected = CASE WHEN l_row.innn > l_row.total_seats * l_balancing_koeff THEN
                                 l_row.total_seats * l_balancing_koeff
                            ELSE l_row.innn end,
        total_pass_before = 0,
        total_pass_after = CASE WHEN l_row.innn > l_row.total_seats * l_balancing_koeff THEN
                                     l_row.total_seats * l_balancing_koeff
                                ELSE l_row.innn end
        WHERE askp.asmpp_agg_item.asmpp_agg_item_id = l_row.asmpp_agg_item_id;
    ELSE
      SELECT SUM(t.in_corrected) - SUM(t.out_corrected) INTO l_total_corrected FROM askp.asmpp_agg_item t
          WHERE t.time_start < l_row.time_start AND t.order_round_id = l_row.order_round_id;
      l_before_sp := l_total_corrected;
      l_total_corrected := l_total_corrected + l_row.innn - l_row.outtt;
      l_overr := l_row.total_seats * l_balancing_koeff - l_total_corrected;
      IF l_overr < 0  THEN -- зашло слишком много
        UPDATE askp.asmpp_agg_item SET in_corrected = l_row.innn + l_overr, out_corrected = l_row.outtt WHERE
          askp.asmpp_agg_item.asmpp_agg_item_id = l_row.asmpp_agg_item_id;
      ELSE
        IF (l_overr < l_row.total_seats * l_balancing_koeff) THEN -- соответствует вместимости
          UPDATE askp.asmpp_agg_item SET in_corrected = l_row.innn, out_corrected = l_row.outtt WHERE
            askp.asmpp_agg_item.asmpp_agg_item_id = l_row.asmpp_agg_item_id;
        END IF;
        IF (l_overr > l_row.total_seats * l_balancing_koeff) THEN -- вышло много
          l_overr := l_overr - l_row.total_seats * l_balancing_koeff;
          UPDATE askp.asmpp_agg_item SET in_corrected = l_row.innn, out_corrected = l_row.outtt - l_overr WHERE
                 askp.asmpp_agg_item.asmpp_agg_item_id = l_row.asmpp_agg_item_id;
        END IF;
      END IF;
      SELECT SUM(t.in_corrected) - SUM(t.out_corrected) INTO l_after_sp FROM askp.asmpp_agg_item t
      WHERE t.time_start <= l_row.time_start AND t.order_round_id = l_row.order_round_id;

      UPDATE askp.asmpp_agg_item t SET total_pass_before = l_before_sp, total_pass_after = l_after_sp
        WHERE t.asmpp_agg_item_id = l_row.asmpp_agg_item_id;

    END IF; -- IF ISNULL OR
  END LOOP ;
  RETURN TRUE;
END;
$$ LANGUAGE plpgsql;