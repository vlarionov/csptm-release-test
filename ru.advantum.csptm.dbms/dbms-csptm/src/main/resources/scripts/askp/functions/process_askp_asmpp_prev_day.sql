-- процедура для ежедневной обработки данных АСКП и АСМПП, вызывается crone и обрабатывает данные за предыдущие сутки
CREATE OR REPLACE FUNCTION
  askp.process_askp_asmpp_prev_day()
  RETURNS BOOLEAN AS $$
DECLARE
  l_timestamp_from TIMESTAMP;
  l_timestamp_to TIMESTAMP;
  l_id BIGINT;
BEGIN
  SELECT current_date - interval '1 day', current_date::timestamp INTO l_timestamp_from, l_timestamp_to;

  INSERT INTO askp.asmpp_agg(time_start, time_finish) VALUES(l_timestamp_from, l_timestamp_to) ;

  SELECT asmpp_agg_id INTO l_id FROM askp.asmpp_agg WHERE time_start = l_timestamp_from AND time_finish = l_timestamp_to;

  PERFORM askp.create_agg_items(l_timestamp_from, l_timestamp_to, l_id);
  PERFORM askp.create_askp_data(l_timestamp_from, l_timestamp_to, l_id);
  -- балансировка входящих и выходящих пассажиров АСМПП
  PERFORM askp.balancing_asmpp(l_timestamp_from, l_timestamp_to, l_id);
  -- поиск пар поездок
  --PERFORM askp.find_trip_tirminal_stop(l_timestamp_from, l_timestamp_to, l_id);
  RETURN TRUE;
END;
$$ LANGUAGE plpgsql;