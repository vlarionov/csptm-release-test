create or replace function askp.format_time_period(p_order_round_id BIGINT) returns text
LANGUAGE plpgsql
AS $$
declare
  l_result TEXT;
  l_time_from TIME;
  l_time_to TIME;

  l_period NUMERIC;
begin
  SELECT MIN(time_fact) + INTERVAL '3 hour', max(time_fact)+ INTERVAL '3 hour'
  INTO l_time_from, l_time_to
  FROM tt.order_fact ooo
  WHERE ooo.order_round_id = p_order_round_id AND sign_deleted = 0;
  l_period := round( ( EXTRACT(hour FROM ( l_time_to - l_time_from ))*60 + EXTRACT(min FROM ( l_time_to - l_time_from )) +
                       EXTRACT(SECOND FROM ( l_time_to - l_time_from ))/60)::NUMERIC , 0);
  IF l_period < 0 THEN -- переход через сутки был
    l_period := l_period + 1440;-- 24 * 60
  END IF;
  --l_result := date_trunc('minute', l_time_from::TIME + INTERVAL '30 second')|| ' - ' || date_trunc('minute',l_time_to::TIME + INTERVAL '30 second')||' ('||l_period|| ' мин.)';
  l_result := to_char(l_time_from::TIME + INTERVAL '30 second', 'HH24:MI')|| ' - ' || to_char(l_time_to::TIME + INTERVAL '30 second', 'HH24:MI')||' ('||l_period|| ' мин.)';
  return l_result;
end;
$$;

/*
*  Выходной вид данных - 14:22:51 - 15:35:33 (72.7 мин.)
*  На входе может быть строка '14:22:51 - 15:35:33' или '14:22:51 - 15:35:33 (72.7 мин.)'
*  Нужно, потому, что формат "14:22:51 - 15:35:33 (72.7 мин.)" появился позже .
*/

/*
create or replace function askp.asmpp_get_full_data_pkg$format_time_ASKP(p_in_text TEXT) returns text
LANGUAGE plpgsql
AS $$
declare
  l_result TEXT;

  l_period NUMERIC;
  l_time_from TIME;
  l_time_to TIME;

  l_arr TEXT[] := regexp_split_to_array(p_in_text, ' - ');
begin
  IF array_length(l_arr,1) IS NOT NULL THEN
    l_time_from := l_arr[1]::TIME;
    IF position(' ' in l_arr[2]) > 0 THEN
      l_time_to :=  substring(l_arr[2] FROM 1 for position(' ' in l_arr[2]) - 1 )::TIME;
    END IF;
    IF position(' ' in l_arr[2]) = 0 THEN
      l_time_to :=  l_arr[2]::TIME;
    END IF;
  END IF;

  l_period := round( ( EXTRACT(hour FROM ( l_time_to - l_time_from ))*60 + EXTRACT(min FROM ( l_time_to - l_time_from )) +
                       EXTRACT(SECOND FROM ( l_time_to - l_time_from ))/60)::NUMERIC , 1);
  IF l_period < 0 THEN -- переход через сутки был
    l_period := l_period + 1440;-- 24 * 60
  END IF;

  l_result := date_trunc('second',l_time_from)|| ' - ' || date_trunc('second',l_time_to) ||' ('||l_period|| ' мин.)';
  return l_result;
end;
$$;
*/