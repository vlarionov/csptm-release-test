CREATE OR REPLACE FUNCTION askp.create_agg_items(date_from TIMESTAMP, date_to TIMESTAMP, id BIGINT)
  RETURNS BOOLEAN AS $$
DECLARE
  l_count_on_tech_stop SMALLINT := asd.get_constant('COUNT_ON_TECH_STOPS');
BEGIN
  DELETE FROM askp.askp_data
  WHERE asmpp_agg_item_id IN (SELECT asmpp_agg_item_id
                              FROM askp.asmpp_agg_item
                              WHERE asmpp_agg_id = id);
  -- при пересчете если такой id уже есть, delete
  DELETE FROM askp.asmpp_agg_item
  WHERE asmpp_agg_id = id;

  INSERT INTO askp.asmpp_agg_item (asmpp_agg_id, tr_id, stops_muid, stop_place_muid, route_variants_muid, order_round_id,
                                   in_cnt, out_cnt, time_start, time_finish, stop_order_num/*order_fact_id*/)
    /*(
      SELECT
        id,
        tr_id,
        stopId,
        route_variant_muid,
        order_round_id,
        SUM(TOTAL_IN),
        SUM(TOTAL_OUT),
        time_fact,
        next_stop_time,
        table4.order_fact_id
      FROM
        (
          SELECT
            (CASE WHEN in1 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
              THEN 0
             ELSE in1 END *
             (CASE has_door1
              WHEN 'true'
                THEN 1
              WHEN 'false'
                THEN 0 END) /*
             (CASE is_closed_door1
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/ +
             CASE WHEN in2 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
               THEN 0
             ELSE in2 END *
             (CASE has_door2
              WHEN 'true'
                THEN 1
              WHEN 'false'
                THEN 0 END) /*
             (CASE is_closed_door2
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/ +
             /*in3*/ CASE WHEN in3 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
              THEN 0
                     ELSE in3 END *
                     (CASE has_door3
                      WHEN 'true'
                        THEN 1
                      WHEN 'false'
                        THEN 0 END) /*
             (CASE is_closed_door3
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/ +
             CASE WHEN in4 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
               THEN 0
             ELSE in4 END *
             (CASE has_door4
              WHEN 'true'
                THEN 1
              WHEN 'false'
                THEN 0 END) /*
             (CASE is_closed_door4
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/
            )                                              AS TOTAL_IN,

            (CASE WHEN out1 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
              THEN 0
             ELSE out1 END *
             (CASE has_door1
              WHEN 'true'
                THEN 1
              WHEN 'false'
                THEN 0 END) /*
             (CASE is_closed_door1
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/ +
             CASE WHEN out2 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
               THEN 0
             ELSE out2 END *
             (CASE has_door2
              WHEN 'true'
                THEN 1
              WHEN 'false'
                THEN 0 END) /*
             (CASE is_closed_door2
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/ +
             CASE WHEN out3 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
               THEN 0
             ELSE out3 END *
             (CASE has_door3
              WHEN 'true'
                THEN 1
              WHEN 'false'
                THEN 0 END) /*
             (CASE is_closed_door3
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/ +
             CASE WHEN out4 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
               THEN 0
             ELSE out4 END *
             (CASE has_door4
              WHEN 'true'
                THEN 1
              WHEN 'false'
                THEN 0 END) /*
             (CASE is_closed_door4
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/) AS TOTAL_OUT,
            event_time,
            tr_id,
            time_fact,
            next_stop_time,
            stopId,
            order_round_id,
            route_variant_muid,
            temp1.order_fact_id
          FROM snsr.irma_sensor_data t5,
            (
              SELECT
                trid,
                stopId,
                time_fact,
                COALESCE(lag(time_fact, -1) OVER ( PARTITION BY trid, order_round_id order by time_fact), time_fact + INTERVAL '2 minutes')
                  AS next_stop_time, -- если последняя остановка
                order_round_id,
                route_variant_muid,
                t2.order_fact_id
              FROM (
                     SELECT
                       tt.order_list.tr_id AS trid,
                       gis.stops.muid      AS stopId,
                       time_fact,
                       tt.order_round.order_round_id,
                       tt.timetable_entry.route_variant_muid,
                       order_fact.order_fact_id
                     FROM tt.order_fact
                       JOIN tt.order_round ON order_fact.order_round_id = order_round.order_round_id
                       JOIN tt.order_list ON order_round.order_list_id = order_list.order_list_id
                       JOIN tt.timetable_entry
                         ON tt.order_list.timetable_entry_id = tt.timetable_entry.timetable_entry_id
                       JOIN gis.route_variants ON gis.route_variants.muid = tt.timetable_entry.route_variant_muid
                       JOIN gis.stop_places ON tt.order_fact.stop_place_muid = gis.stop_places.muid
                       JOIN gis.stops ON gis.stop_places.stop_muid = gis.stops.muid
                     WHERE time_fact BETWEEN date_from AND date_to AND tt.order_fact.sign_deleted = 0 AND
                           tt.order_list.sign_deleted = 0 AND tt.order_list.is_active = 1 AND
                           tt.order_round.is_active = 1
                     ORDER BY tt.order_list.tr_id, time_fact) t2
            ) AS temp1
          WHERE event_time BETWEEN date_from AND date_to AND
                t5.tr_id = temp1.trid AND t5.event_time > temp1.time_fact AND t5.event_time < temp1.next_stop_time
          ORDER BY tr_id, event_time
        ) table4
      GROUP BY time_fact, next_stop_time, stopId, order_round_id, tr_id, route_variant_muid, order_fact_id
      ORDER BY time_fact
    )*/
    WITH aa AS (
        SELECT
          tt.order_list.tr_id AS trid,
          gis.stops.muid      AS stopId,
          gis.stop_places.muid AS stop_place_muid,
          time_fact,
          tt.order_round.order_round_id,
          tt.timetable_entry.route_variant_muid,
          order_fact.order_fact_id,
          order_fact.stop_order_num
        FROM tt.order_fact
          JOIN tt.order_round ON order_fact.order_round_id = order_round.order_round_id --AND order_fact.order_round_id = 36494967
          JOIN tt.order_list ON order_round.order_list_id = order_list.order_list_id
          JOIN tt.timetable_entry
            ON tt.order_list.timetable_entry_id = tt.timetable_entry.timetable_entry_id
          JOIN gis.route_variants ON gis.route_variants.muid = tt.timetable_entry.route_variant_muid
          JOIN gis.stop_places ON tt.order_fact.stop_place_muid = gis.stop_places.muid
               -- учитывать или нет технологические установки (0 - не считать)
               AND ( CASE WHEN l_count_on_tech_stop = 0 THEN gis.stop_places.is_technical = 0 -- не технологические
                          ELSE TRUE -- все остановки
                     END)
          JOIN gis.stops ON gis.stop_places.stop_muid = gis.stops.muid
        WHERE time_fact BETWEEN date_from AND date_to AND tt.order_fact.sign_deleted = 0 AND
              tt.order_list.sign_deleted = 0 AND tt.order_list.is_active = 1 AND
              tt.order_round.is_active = 1
        ORDER BY tt.order_list.tr_id, time_fact
    ),a AS(
        SELECT
          trid,
          stopId,
          stop_place_muid,
          time_fact, -- даем одну минуту, с учетом ошибки вычисления времени прибытия, чтобы все вышли на последней остановке
          COALESCE(lag(time_fact, -1) OVER ( PARTITION BY trid, order_round_id order by time_fact), time_fact + INTERVAL '1 minutes')
            AS next_stop_time,
          order_round_id,
          route_variant_muid,
          aa.order_fact_id,
          aa.stop_order_num
        FROM aa),

        dd AS(
          SELECT
            (CASE WHEN in1 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
              THEN 0
             ELSE in1 END *
             (CASE has_door1
              WHEN 'true'
                THEN 1
              WHEN 'false'
                THEN 0 END) /*
             (CASE is_closed_door1
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/ +
             CASE WHEN in2 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
               THEN 0
             ELSE in2 END *
             (CASE has_door2
              WHEN 'true'
                THEN 1
              WHEN 'false'
                THEN 0 END) /*
             (CASE is_closed_door2
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/ +
             /*in3*/ CASE WHEN in3 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
              THEN 0
                     ELSE in3 END *
                     (CASE has_door3
                      WHEN 'true'
                        THEN 1
                      WHEN 'false'
                        THEN 0 END) /*
             (CASE is_closed_door3
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/ +
             CASE WHEN in4 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
               THEN 0
             ELSE in4 END *
             (CASE has_door4
              WHEN 'true'
                THEN 1
              WHEN 'false'
                THEN 0 END) /*
             (CASE is_closed_door4
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/
            )                                              AS TOTAL_IN,

            (CASE WHEN out1 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
              THEN 0
             ELSE out1 END *
             (CASE has_door1
              WHEN 'true'
                THEN 1
              WHEN 'false'
                THEN 0 END) /*
             (CASE is_closed_door1
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/ +
             CASE WHEN out2 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
               THEN 0
             ELSE out2 END *
             (CASE has_door2
              WHEN 'true'
                THEN 1
              WHEN 'false'
                THEN 0 END) /*
             (CASE is_closed_door2
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/ +
             CASE WHEN out3 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
               THEN 0
             ELSE out3 END *
             (CASE has_door3
              WHEN 'true'
                THEN 1
              WHEN 'false'
                THEN 0 END) /*
             (CASE is_closed_door3
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/ +
             CASE WHEN out4 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
               THEN 0
             ELSE out4 END *
             (CASE has_door4
              WHEN 'true'
                THEN 1
              WHEN 'false'
                THEN 0 END) /*
             (CASE is_closed_door4
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/) AS TOTAL_OUT,
            event_time,
            tr_id,
            time_fact,
            next_stop_time,
            stopId,
            stop_place_muid,
            order_round_id,
            route_variant_muid,
            a.order_fact_id,
            a.stop_order_num
          FROM snsr.irma_sensor_data t5, a
          WHERE event_time BETWEEN date_from AND date_to AND
                t5.tr_id = a.trid AND t5.event_time > a.time_fact AND t5.event_time < a.next_stop_time
          ORDER BY tr_id, event_time
      )
    SELECT
      id,
      tr_id,
      stopId,
      stop_place_muid,
      route_variant_muid,
      order_round_id,
      SUM(TOTAL_IN),
      SUM(TOTAL_OUT),
      time_fact,
      next_stop_time,
      --dd.order_fact_id
      dd.stop_order_num
    from dd
    GROUP BY time_fact, next_stop_time, stopId, stop_place_muid, order_round_id, tr_id, route_variant_muid, stop_order_num/*order_fact_id*/
    ORDER BY time_fact  ;
  RETURN TRUE;
END;
$$ LANGUAGE plpgsql;


/*
WITH aa AS (
    SELECT
      tt.order_list.tr_id AS trid,
      gis.stops.muid      AS stopId,
      time_fact,
      tt.order_round.order_round_id,
      tt.timetable_entry.route_variant_muid,
      order_fact.order_fact_id
    FROM tt.order_fact
      JOIN tt.order_round ON order_fact.order_round_id = order_round.order_round_id --AND order_fact.order_round_id = 36494967
      JOIN tt.order_list ON order_round.order_list_id = order_list.order_list_id
      JOIN tt.timetable_entry
        ON tt.order_list.timetable_entry_id = tt.timetable_entry.timetable_entry_id
      JOIN gis.route_variants ON gis.route_variants.muid = tt.timetable_entry.route_variant_muid
      JOIN gis.stop_places ON tt.order_fact.stop_place_muid = gis.stop_places.muid
      JOIN gis.stops ON gis.stop_places.stop_muid = gis.stops.muid
    WHERE time_fact BETWEEN '2017-10-28 00:00:00'::TIMESTAMP AND '2017-10-28 23:00:00'::TIMESTAMP --date_from AND date_to AND tt.order_fact.sign_deleted = 0
          AND tt.order_list.sign_deleted = 0 AND tt.order_list.is_active = 1
          AND tt.order_round.is_active = 1
    ORDER BY tt.order_list.tr_id, time_fact
),a AS(
    SELECT
      trid,
      stopId,
      time_fact, -- даем одну минуту, с учетом ошибки вычисления времени прибытия, чтобы все вышли на последней остановке
      COALESCE(lag(time_fact, -1) OVER ( PARTITION BY trid, order_round_id order by time_fact), time_fact + INTERVAL '1 minutes')
        AS next_stop_time,
      order_round_id,
      route_variant_muid,
      aa.order_fact_id
    FROM aa),

    dd AS(
      SELECT
        (CASE WHEN in1 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
          THEN 0
         ELSE in1 END *
         (CASE has_door1
          WHEN 'true'
            THEN 1
          WHEN 'false'
            THEN 0 END) /*
             (CASE is_closed_door1
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/ +
         CASE WHEN in2 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
           THEN 0
         ELSE in2 END *
         (CASE has_door2
          WHEN 'true'
            THEN 1
          WHEN 'false'
            THEN 0 END) /*
             (CASE is_closed_door2
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/ +
         /*in3*/ CASE WHEN in3 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
          THEN 0
                 ELSE in3 END *
                 (CASE has_door3
                  WHEN 'true'
                    THEN 1
                  WHEN 'false'
                    THEN 0 END) /*
             (CASE is_closed_door3
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/ +
         CASE WHEN in4 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
           THEN 0
         ELSE in4 END *
         (CASE has_door4
          WHEN 'true'
            THEN 1
          WHEN 'false'
            THEN 0 END) /*
             (CASE is_closed_door4
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/
        )                                              AS TOTAL_IN,

        (CASE WHEN out1 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
          THEN 0
         ELSE out1 END *
         (CASE has_door1
          WHEN 'true'
            THEN 1
          WHEN 'false'
            THEN 0 END) /*
             (CASE is_closed_door1
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/ +
         CASE WHEN out2 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
           THEN 0
         ELSE out2 END *
         (CASE has_door2
          WHEN 'true'
            THEN 1
          WHEN 'false'
            THEN 0 END) /*
             (CASE is_closed_door2
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/ +
         CASE WHEN out3 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
           THEN 0
         ELSE out3 END *
         (CASE has_door3
          WHEN 'true'
            THEN 1
          WHEN 'false'
            THEN 0 END) /*
             (CASE is_closed_door3
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/ +
         CASE WHEN out4 > asd.get_constant('ASMPP_THRESHOLD_INCORRECT_INOUT_VALUES')
           THEN 0
         ELSE out4 END *
         (CASE has_door4
          WHEN 'true'
            THEN 1
          WHEN 'false'
            THEN 0 END) /*
             (CASE is_closed_door4
              WHEN 'true'
                THEN 0
              WHEN 'false'
                THEN 1 END)*/) AS TOTAL_OUT,
        event_time,
        tr_id,
        time_fact,
        next_stop_time,
        stopId,
        order_round_id,
        route_variant_muid,
        a.order_fact_id
      FROM snsr.irma_sensor_data t5, a
      WHERE event_time BETWEEN '2017-10-28 00:00:00'::TIMESTAMP AND '2017-10-28 23:00:00'::TIMESTAMP --date_from AND date_to
            AND t5.tr_id = a.trid AND t5.event_time > a.time_fact AND t5.event_time < a.next_stop_time
      ORDER BY tr_id, event_time
  )
SELECT
  --id,
  tr_id,
  stopId,
  route_variant_muid,
  order_round_id,
  SUM(TOTAL_IN),
  SUM(TOTAL_OUT),
  time_fact,
  next_stop_time,
  dd.order_fact_id
from dd
GROUP BY time_fact, next_stop_time, stopId, order_round_id, tr_id, route_variant_muid, order_fact_id
ORDER BY order time_fact;
*/
/*

WITH a AS(
select SUM(in_corrected) over (PARTITION BY order_round_id order by time_start) -
       SUM(out_corrected) over (PARTITION BY order_round_id order by time_start) - in_corrected + out_corrected as before_pass,
       SUM(in_corrected) over (PARTITION BY order_round_id order by time_start) -
       SUM(out_corrected) over (PARTITION BY order_round_id order by time_start)  as after_pass,
asmpp_agg_item_id/*, tr_id, stops_muid, order_round_id, in_corrected, out_corrected*/ from askp.asmpp_agg_item
where time_start >= '2017-10-30 00:00:00' AND time_start <= '2017-10-30 23:59:59' --AND order_round_id = 37782089
/*ORDER BY order_round_id , time_start desc*/)
UPDATE askp.asmpp_agg_item b SET (total_pass_before, total_pass_after) =
(select a.before_pass, a.after_pass from a where a.asmpp_agg_item_id = b.asmpp_agg_item_id);

select * from askp.asmpp_agg_item where total_pass_after IS NOT null;

*/