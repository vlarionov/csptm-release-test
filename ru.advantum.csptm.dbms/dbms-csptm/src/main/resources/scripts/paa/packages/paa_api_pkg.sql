create or replace function paa.paa_api_pkg$basic_publish(
    p_broker_id   paa.broker.broker_id%type,
    p_exchange    text,
    p_routing_key text,
    p_payload     text,
    p_type        text
)
    returns void
language plpgsql
as $$
begin
    perform paa.pg_amqp_adapter_pkg$queue_action(p_broker_id,
                                                 'BASIC_PUBLISH',
                                                 jsonb_build_object('exchange', p_exchange,
                                                                    'routing_key', p_routing_key,
                                                                    'payload', p_payload,
                                                                    'type', p_type));
end;
$$;

create or replace function paa.paa_api_pkg$basic_publish(
    p_broker_id   paa.broker.broker_id%type,
    p_exchange    text,
    p_routing_key text,
    p_payload     text,
    p_type        text,
    p_headers     jsonb
)
    returns void
language plpgsql
as $$
begin
    perform paa.pg_amqp_adapter_pkg$queue_action(p_broker_id,
                                                 'BASIC_PUBLISH',
                                                 jsonb_build_object('exchange', p_exchange,
                                                                    'routing_key', p_routing_key,
                                                                    'payload', p_payload,
                                                                    'type', p_type,
                                                                    'headers', p_headers));
end;
$$;
