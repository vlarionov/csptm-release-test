create or replace function paa.pg_amqp_adapter_pkg$process_action(p_action_id paa.action.action_id%type)
    returns void
language plpgsql
as $$
begin
    update paa.action
    set is_processed = true, process_time = now()
    where action_id = p_action_id;
end;
$$;

create or replace function paa.pg_amqp_adapter_pkg$queue_action(
    p_broker_id      paa.broker.broker_id%type,
    p_action_type_id paa.action_type.action_type_id%type,
    p_action_params  jsonb
)
    returns void
language plpgsql
as $$
begin
    insert into paa.action (broker_id, action_type_id, action_params)
    values (p_broker_id, p_action_type_id, p_action_params);
end;
$$;

create or replace function paa.pg_amqp_adapter_pkg$get_not_processed_actions(
    p_broker_id paa.broker.broker_id%type
)
    returns setof paa.action
language plpgsql
as $$
begin
    return query select *
                 from paa.action
                 where broker_id = p_broker_id and not is_processed
                 order by action_id
                 limit 500;
end;
$$;

