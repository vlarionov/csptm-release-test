create or replace function crep.jf_report_filter_pkg$attr_to_rowtype(p_attr text)
    returns jrep.report_filter
language plpgsql
as $$
declare
    l_r jrep.report_filter%rowtype;
begin
    l_r.id_report_filter := jofl.jofl_pkg$extract_number(p_attr, 'id_report_filter', true);
    l_r.id_report := jofl.jofl_pkg$extract_number(p_attr, 'id_report', true);
    l_r.filter_class := jofl.jofl_pkg$extract_varchar(p_attr, 'filter_class', true);
    l_r.initial_param := jofl.jofl_pkg$extract_varchar(p_attr, 'initial_param', true);
    l_r.grid_x := jofl.jofl_pkg$extract_varchar(p_attr, 'grid_x', true);
    l_r.grid_y := jofl.jofl_pkg$extract_varchar(p_attr, 'grid_y', true);

    return l_r;
end;
$$;

create or replace function crep.jf_report_filter_pkg$of_rows(p_id_account numeric, out p_rows refcursor, p_attr text)
    returns refcursor
language plpgsql
as $$
declare
    l_id_report jrep.report.id_report%type := jofl.jofl_pkg$extract_number(p_attr, 'id_report');
begin
    open p_rows for
    select
        id_report_filter,
        id_report,
        filter_class,
        initial_param,
        grid_x,
        grid_y
    from jrep.report_filter
    where id_report = l_id_report;
end;
$$;

create or replace function crep.jf_report_filter_pkg$of_insert(p_id_account numeric, p_attr text)
    returns text
language plpgsql
as $$
declare
    l_r jrep.report_filter%rowtype;
begin
    l_r := crep.jf_report_filter_pkg$attr_to_rowtype(p_attr);

    perform crep.user_report_pkg$create_filter(l_r.id_report,
                                               l_r.filter_class,
                                               l_r.initial_param,
                                               l_r.grid_x,
                                               l_r.grid_y);

    return null;
end;
$$;

create or replace function crep.jf_report_filter_pkg$of_delete(p_id_account numeric, p_attr text)
    returns text
language plpgsql
as $$
declare
    l_r jrep.report_filter%rowtype;
begin
    l_r := crep.jf_report_filter_pkg$attr_to_rowtype(p_attr);

    perform crep.user_report_pkg$delete_filter(l_r.id_report_filter);

    return null;
end;
$$;

create or replace function crep.jf_report_filter_pkg$of_update(p_id_account numeric, p_attr text)
    returns text
language plpgsql
as $$
declare
    l_r jrep.report_filter%rowtype;
begin
    l_r := crep.jf_report_filter_pkg$attr_to_rowtype(p_attr);

    perform crep.user_report_pkg$update_filter(l_r.id_report_filter,
                                               l_r.filter_class,
                                               l_r.initial_param,
                                               l_r.grid_x,
                                               l_r.grid_y);

    return null;
end;
$$;

