create or replace function crep.user_report_pkg$cn_user_report_group()
    returns jrep.report_group.id_report_group%type
language sql immutable as $$ select 10 :: integer $$;

create or replace function crep.user_report_pkg$create_report(
    p_report_name    jrep.report.s_name%type,
    p_report_comment crep.user_report.comment_text%type,
    p_account_id     core.account.account_id%type
)
    returns jrep.report.id_report%type
language plpgsql
as $$
declare
    l_id_report jrep.report.id_report%type;
begin
    insert into jrep.report (id_report_group, s_name, id_location, order_num, name_mask, is_immediate)
    values (crep.user_report_pkg$cn_user_report_group(), p_report_name, 'LIST', 0, p_report_name, 1)
    returning id_report
        into l_id_report;

    insert into crep.user_report (id_report, account_id, comment_text)
    values (l_id_report, p_account_id, p_report_comment);

    return l_id_report;
end;
$$;

create or replace function crep.user_report_pkg$update_report(
    p_id_report      jrep.report.id_report%type,
    p_report_name    jrep.report.s_name%type,
    p_report_comment crep.user_report.comment_text%type
)
    returns void
language plpgsql
as $$
declare
begin
    update jrep.report
    set s_name    = p_report_name,
        name_mask = p_report_name
    where id_report = p_id_report;

    update crep.user_report
    set comment_text = p_report_comment,
        update_time  = now()
    where id_report = p_id_report;
end;
$$;

create or replace function crep.user_report_pkg$delete_report(
    p_id_report jrep.report.id_report%type
)
    returns void
language plpgsql
as $$
declare
begin
    delete from crep.user_report
    where id_report = p_id_report;

    delete from jrep.report
    where id_report = p_id_report;
end;
$$;

create or replace function crep.user_report_pkg$create_filter(
    p_id_report     jrep.report.id_report%type,
    p_filter_class  jofl.ref_filter.filter_class%type,
    p_initial_param jrep.report_filter.initial_param%type,
    p_grid_x        jrep.report_filter.grid_x%type,
    p_grid_y        jrep.report_filter.grid_y%type
)
    returns jrep.report_filter.id_report_filter%type
language plpgsql
as $$
declare
    l_id_report_filter jrep.report_filter.id_report_filter%type;
begin
    insert into jrep.report_filter (id_report, filter_class, initial_param, grid_x, grid_y)
    values (p_id_report, p_filter_class, p_initial_param, p_grid_x, p_grid_y)
    returning id_report_filter
        into l_id_report_filter;

    return l_id_report_filter;
end;
$$;

create or replace function crep.user_report_pkg$update_filter(
    p_id_report_filter jrep.report_filter.id_report_filter%type,
    p_filter_class     jofl.ref_filter.filter_class%type,
    p_initial_param    jrep.report_filter.initial_param%type,
    p_grid_x           jrep.report_filter.grid_x%type,
    p_grid_y           jrep.report_filter.grid_y%type
)
    returns void
language plpgsql
as $$
declare
begin
    update jrep.report_filter
    set filter_class  = p_filter_class,
        initial_param = p_initial_param,
        grid_x        = p_grid_x,
        grid_y        = p_grid_y
    where id_report_filter = p_id_report_filter;
end;
$$;

create or replace function crep.user_report_pkg$delete_filter(
    p_id_report_filter jrep.report_filter.id_report_filter%type
)
    returns void
language plpgsql
as $$
declare
begin
    delete from jrep.report_filter
    where id_report_filter = p_id_report_filter;
end;
$$;

create or replace function crep.user_report_pkg$create_format(
    p_id_report   jrep.report.id_report%type,
    p_format_type jrep.format_type.format_type_id%type,
    p_sourcefile  jrep.report_format.sourcefile%type,
    p_account_id  core.account.account_id%type,
    p_filename    crep.user_report_format.filename%type
)
    returns void
language plpgsql
as $$
declare
begin
    insert into jrep.report_format (id_report, format_type, sourcefile)
    values (p_id_report, p_format_type, p_sourcefile);

    insert into crep.user_report_format (id_report, format_type, account_id, filename)
    values (p_id_report, p_format_type, p_account_id, p_filename);
end;
$$;

create or replace function crep.user_report_pkg$update_format(
    p_id_report   jrep.report.id_report%type,
    p_format_type jrep.format_type.format_type_id%type,
    p_sourcefile  jrep.report_format.sourcefile%type,
    p_account_id  core.account.account_id%type,
    p_filename    crep.user_report_format.filename%type
)
    returns void
language plpgsql
as $$
declare
begin
    update jrep.report_format
    set sourcefile = p_sourcefile
    where id_report = p_id_report
          and format_type = p_format_type;

    update crep.user_report_format
    set filename    = p_filename,
        account_id  = p_account_id,
        upload_time = now()
    where id_report = p_id_report
          and format_type = p_format_type;
end;
$$;

create or replace function crep.user_report_pkg$delete_format(
    p_id_report   jrep.report.id_report%type,
    p_format_type jrep.format_type.format_type_id%type
)
    returns void
language plpgsql
as $$
declare
begin
    delete from crep.user_report_format
    where id_report = p_id_report
          and format_type = p_format_type;

    delete from jrep.report_format
    where id_report = p_id_report
          and format_type = p_format_type;
end;
$$;

