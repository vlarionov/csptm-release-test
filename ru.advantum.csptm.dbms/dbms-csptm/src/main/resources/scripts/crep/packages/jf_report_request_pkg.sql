create or replace function crep.jf_report_request_pkg$of_rows(p_id_account numeric,
    out                                                       p_rows       refcursor,
                                                              p_attr       text)
    returns refcursor
language plpgsql
as $$
declare
    l_id_report  integer := jofl.jofl_pkg$extract_number(p_attr, 'f_id_report', true);
    l_begin_date timestamp := jofl.jofl_pkg$extract_date(p_attr, 'F_BEG_DT', true);
    l_end_date   timestamp := jofl.jofl_pkg$extract_date(p_attr, 'F_END_DT', true);
begin
    open p_rows for
    select
        rr.report_hash,
        rr.id_jofl_account,
        coalesce(a.last_name || ' ', '')
        || coalesce(a.first_name || ' ', '')
        || coalesce(a.middle_name, '') account_full_name,
        a.login account_login,
        r.s_name report_name,
        rr.gmt_create
    from jrep.report_request rr
        join jrep.report r on r.id_report = rr.id_report
        join jofl.account ja on rr.id_jofl_account = ja.id_jofl_account
        join core.account a on ja.id_account_local :: bigint = a.account_id
    where rr.gmt_create between l_begin_date and l_end_date
          and (l_id_report is null or rr.id_report = l_id_report)
    order by rr.gmt_create;
end;
$$;