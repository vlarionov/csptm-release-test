create or replace function crep.zabbix_api_pkg$get_data(
    p_method text,
    p_params json
)
    returns table(r json)
language plpgsql
as $$
declare
begin
    return query select
    json_array_elements((http_post('http://188.93.208.243/zabbix/api_jsonrpc.php',
    json_build_object('jsonrpc', '2.0',
    'method', p_method,
    'params', p_params,
    'id', 2,
    'auth', '505633d432598063f37b01950e1cd476') :: text,
    'application/json')).content :: json -> 'result');
end;
$$;