create or replace function crep.jf_user_report_format_pkg$of_rows(p_id_account numeric, out p_rows refcursor,
                                                                  p_attr       text)
    returns refcursor
language plpgsql
as $$
declare
    l_id_report jrep.report.id_report%type := jofl.jofl_pkg$extract_number(p_attr, 'id_report');
begin
    open p_rows for
    select
        urf.id_report,
        urf.format_type,
        ft.extension,
        urf.upload_time,
        adm.jf_account_pkg$get_full_name(urf.account_id) account_full_name,
        urf.filename,
        jofl.jofl_link_util$build(urf.filename, rf.sourcefile, urf.filename) sourcefile_link,
        'P{RO},D{extension}' "ROW$POLICY"
    from crep.user_report_format urf
        join jrep.report_format rf on urf.id_report = rf.id_report and urf.format_type = rf.format_type
        join jrep.format_type ft on urf.format_type = ft.format_type_id
    where urf.id_report = l_id_report;
end;
$$;

create or replace function crep.jf_user_report_format_pkg$of_insert(p_id_account numeric,
                                                                    p_attr       text,
                                                                    p_binary     text)
    returns text
language plpgsql
as $$
declare
    l_id_report   jrep.report.id_report%type := jofl.jofl_pkg$extract_number(p_attr, 'id_report');
    l_format_type jrep.format_type.format_type_id%type := jofl.jofl_pkg$extract_varchar(p_attr, 'format_type');
    l_sourcefile  jrep.report_format.sourcefile%type := jofl.jofl_bin_util$extract_file_id(p_binary, 'sourcefile');
    l_filename    crep.user_report_format.filename%type := jofl.jofl_pkg$extract_varchar(p_attr, 'sourcefile');
begin
    perform crep.user_report_pkg$create_format(l_id_report,
                                               l_format_type,
                                               l_sourcefile,
                                               p_id_account :: bigint,
                                               l_filename);

    return null;
end;
$$;

create or replace function crep.jf_user_report_format_pkg$of_delete(p_id_account numeric,
                                                                    p_attr       text)
    returns text
language plpgsql
as $$
declare
    l_id_report   jrep.report.id_report%type := jofl.jofl_pkg$extract_number(p_attr, 'id_report');
    l_format_type jrep.format_type.format_type_id%type := jofl.jofl_pkg$extract_varchar(p_attr, 'format_type');
begin
    perform crep.user_report_pkg$delete_format(l_id_report,
                                               l_format_type);

    return null;
end;
$$;

create or replace function crep.jf_user_report_format_pkg$of_update(p_id_account numeric,
                                                                    p_attr       text,
                                                                    p_binary     text)
    returns text
language plpgsql
as $$
declare
    l_id_report   jrep.report.id_report%type := jofl.jofl_pkg$extract_number(p_attr, 'id_report');
    l_format_type jrep.format_type.format_type_id%type := jofl.jofl_pkg$extract_varchar(p_attr, 'format_type');
    l_sourcefile  jrep.report_format.sourcefile%type := jofl.jofl_bin_util$extract_file_id(p_binary, 'sourcefile');
    l_filename    crep.user_report_format.filename%type := jofl.jofl_pkg$extract_varchar(p_attr, 'sourcefile');
begin
    perform crep.user_report_pkg$update_format(l_id_report,
                                               l_format_type,
                                               l_sourcefile,
                                               p_id_account :: bigint,
                                               l_filename);

    return null;
end;
$$;

