--drop type crep.jf_user_report_type cascade;

do $$
begin
  if not exists(select * from pg_type where typname = 'jf_user_report_type') THEN

    create type crep.jf_user_report_type as
        (r_report      jrep.report,
         r_user_report crep.user_report);

    alter type crep.jf_user_report_type
        owner to adv;

  end if;
end$$;

create or replace function crep.jf_user_report_pkg$attr_to_rowtype(p_attr text)
    returns crep.jf_user_report_type
language plpgsql
as $$
declare
    l_return      crep.jf_user_report_type;

    l_report      jrep.report;
    l_user_report crep.user_report;
begin
    l_report.s_name := jofl.jofl_pkg$extract_varchar(p_attr, 'report_name', true);

    l_user_report.id_report := jofl.jofl_pkg$extract_number(p_attr, 'id_report', true);
    l_user_report.account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', true);
    l_user_report.comment_text := jofl.jofl_pkg$extract_varchar(p_attr, 'comment_text', true);
    l_user_report.create_time := jofl.jofl_pkg$extract_date(p_attr, 'create_time', true);
    l_user_report.update_time := jofl.jofl_pkg$extract_date(p_attr, 'update_time', true);

    l_return.r_report := l_report;
    l_return.r_user_report := l_user_report;

    return l_return;
end;
$$;

create or replace function crep.jf_user_report_pkg$of_rows(p_id_account numeric, out p_rows refcursor, p_attr text)
    returns refcursor
language plpgsql
as $$
declare
begin
    open p_rows for
    select
        r.id_report,
        r.s_name report_name,
        ur.create_time,
        ur.comment_text,
        adm.jf_account_pkg$get_full_name(ur.account_id) account_full_name
    from crep.user_report ur
        join jrep.report r on ur.id_report = r.id_report;
end;
$$;

--insert into jrep.report_group(id_report_group, s_name, order_num) values (10, 'Пользовательские отчеты', 0);

create or replace function crep.jf_user_report_pkg$of_insert(p_id_account numeric, p_attr text)
    returns text
language plpgsql
as $$
declare
    l_r crep.jf_user_report_type;
begin
    l_r := crep.jf_user_report_pkg$attr_to_rowtype(p_attr);

    perform crep.user_report_pkg$create_report((l_r.r_report).s_name,
                                               (l_r.r_user_report).comment_text,
                                               p_id_account::bigint);

    return null;
end;
$$;

create or replace function crep.jf_user_report_pkg$of_delete(p_id_account numeric, p_attr text)
    returns text
language plpgsql
as $$
declare
    l_r crep.jf_user_report_type;
begin
    l_r := crep.jf_user_report_pkg$attr_to_rowtype(p_attr);

    perform crep.user_report_pkg$delete_report((l_r.r_user_report).id_report);

    return null;
end;
$$;

create or replace function crep.jf_user_report_pkg$of_update(p_id_account numeric, p_attr text)
    returns text
language plpgsql
as $$
declare
    l_r crep.jf_user_report_type;
begin
    l_r := crep.jf_user_report_pkg$attr_to_rowtype(p_attr);

    perform crep.user_report_pkg$update_report((l_r.r_user_report).id_report,
                                               (l_r.r_report).s_name,
                                               (l_r.r_user_report).comment_text);

    return null;
end;
$$;
