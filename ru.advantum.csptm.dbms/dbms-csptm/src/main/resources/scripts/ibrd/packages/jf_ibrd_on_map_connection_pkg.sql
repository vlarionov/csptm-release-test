CREATE OR REPLACE FUNCTION ibrd.jf_ibrd_on_map_connection_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                                      p_attr       TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_f_info_boards_ids INTEGER [];
BEGIN
  l_f_info_boards_ids := jofl.jofl_pkg$extract_narray(p_attr, 'f_info_boards_ids', TRUE);

  OPEN p_rows FOR
  SELECT
    ib.info_board_id,
    ibrd.event_pkg$is_connected(ib.info_board_id) is_connected
  FROM ibrd.info_board ib
  WHERE (l_f_info_boards_ids IS NULL OR ib.info_board_id = ANY (l_f_info_boards_ids))
        AND NOT ib.is_deleted;
END;
$$;