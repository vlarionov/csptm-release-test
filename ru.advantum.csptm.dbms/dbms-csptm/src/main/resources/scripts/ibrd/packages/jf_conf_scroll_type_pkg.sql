CREATE OR REPLACE FUNCTION ibrd.jf_conf_scroll_type_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ibrd.scroll_type AS
$body$
declare 
   l_r ibrd.scroll_type%rowtype; 
begin 
   l_r.scroll_type_id := jofl.jofl_pkg$extract_number(p_attr, 'scroll_type_id', true); 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_conf_scroll_type_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.scroll_type%rowtype;
begin 
   l_r := ibrd.jf_conf_scroll_type_pkg$attr_to_rowtype(p_attr);

   /*insert into ibrd.scroll_type select l_r.*;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_conf_scroll_type_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
begin 
 open p_rows for 
      select 
        scroll_type_id, 
        name
      from ibrd.scroll_type; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_conf_scroll_type_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.scroll_type%rowtype;
begin 
   l_r := ibrd.jf_conf_scroll_type_pkg$attr_to_rowtype(p_attr);

  /* update ibrd.scroll_type set 
          name = l_r.name
   where 
          scroll_type_id = l_r.scroll_type_id;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_conf_scroll_type_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.scroll_type%rowtype;
begin 
   l_r := ibrd.jf_conf_scroll_type_pkg$attr_to_rowtype(p_attr);

  /* delete from  ibrd.scroll_type where  scroll_type_id = l_r.scroll_type_id;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------