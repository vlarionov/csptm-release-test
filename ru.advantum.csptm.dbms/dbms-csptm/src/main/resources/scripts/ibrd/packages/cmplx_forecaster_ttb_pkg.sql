﻿drop function ibrd.cmplx_forecaster_ttb_pkg$get_training_dataset(p_route_id in rts.route.route_id%type, p_begin_dt in ttb.order_list.order_date%type, p_end_dt in ttb.order_list.order_date%type, p_time_window_width in smallint, p_skip_nulls in boolean );
drop function ibrd.cmplx_forecaster_ttb_pkg$get_tt_action_tt(p_tt_action_id ttb.tt_action.tt_action_id%type );
drop function ibrd.cmplx_forecaster_ttb_pkg$get_next_tt_action(p_tt_action_id ttb.tt_action.tt_action_id%type );
drop function ibrd.cmplx_forecaster_ttb_pkg$get_round_stops(p_round_id rts.round.round_id%type );

create or replace function ibrd.cmplx_forecaster_ttb_pkg$get_training_dataset
    (p_route_id          in rts.route.route_id%type,
     p_begin_dt          in ttb.tt_variant.order_date%type,
     p_end_dt            in ttb.tt_variant.order_date%type,
     p_time_window_width in smallint,
     p_skip_nulls        in boolean default true)
    returns table(order_date     ttb.tt_variant.order_date%type,
                  tr_id          core.tr.tr_id%type,
                  tt_action_id   ttb.tt_action.tt_action_id%type,
                  round_id       rts.round.round_id%type,
                  stop_item_id   rts.stop_item.stop_item_id%type,
                  stop_order_num rts.stop_item2round.order_num%type,
                  time_oper      ttb.tt_action_item.time_begin%type,
                  time_fact      ttb.tt_action_fact.time_fact_begin%type,
                  day_of_week    smallint,
                  time_window    smallint,
                  tt             real,
                  tt_prev        real,
                  tt_next        real)
as $$
declare
begin
    return query
    select *
    from (select
              t.order_date,
              t.tr_id,
              t.tt_action_id,
              t.round_id,
              t.stop_item_id,
              t.stop_order_num,
              t.time_oper,
              t.time_fact,
              extract(isodow from t.order_date) :: smallint day_of_week,
              extract(hour from t.time_fact) :: smallint / p_time_window_width time_window,
              t.tt :: real tt,
              lead(t.tt_prev)
              over (
                  partition by t.tt_action_id
                  order by t.stop_order_num ) :: real tt_prev,
              t.tt_next :: real tt_next
          from (select
                    t.order_date,
                    t.tr_id,
                    t.tt_action_id,
                    t.round_id,
                    t.stop_item_id,
                    t.stop_order_num,
                    t.time_oper,
                    t.time_fact,
                    t.tt,
                    lag(t.tt)
                    over (
                        partition by t.order_date, t.round_id, t.stop_order_num
                        order by t.time_fact ) tt_prev,
                    lead(t.tt)
                    over (
                        partition by t.tt_action_id
                        order by t.stop_order_num ) tt_next
                from (select
                          tv.order_date,
                          tout.tr_id,
                          ta.tt_action_id,
                          tar.round_id,
                          si2r.stop_item_id,
                          si2r.order_num stop_order_num,
                          tai.time_begin time_oper,
                          taf.time_fact_begin time_fact,
                          coalesce(extract(epoch from taf.time_fact_begin - lag(taf.time_fact_begin)
                          over (
                              partition by ta.tt_action_id
                              order by si2r.order_num )), 0) tt
                      from ttb.tt_variant tv
                          join rts.route_variant rv on tv.route_variant_id = rv.route_variant_id
                          join ttb.tt_out tout on tv.tt_variant_id = tout.tt_variant_id
                          join ttb.tt_action ta on tout.tt_out_id = ta.tt_out_id
                          join ttb.tt_action_round tar on ta.tt_action_id = tar.tt_action_id
                          join ttb.tt_action_item tai on ta.tt_action_id = tai.tt_action_id
                          join ttb.tt_action_fact taf
                              on ta.tt_action_id = taf.tt_action_id and tai.stop_item2round_id = taf.stop_item2round_id
                          join rts.stop_item2round si2r on taf.stop_item2round_id = si2r.stop_item2round_id
                      where tv.order_date between p_begin_dt and p_end_dt
                            and rv.route_id = p_route_id
                            and not tv.sign_deleted
                            and not tout.sign_deleted
                            and not ta.sign_deleted
                            and not ta.sign_deleted
                            and not tai.sign_deleted
                            and not si2r.sign_deleted
                     ) t
               ) t
         ) t
    where not p_skip_nulls or t.tt_prev is not null
    order by
        order_date,
        tr_id,
        time_oper;
end;
$$
language plpgsql
security definer;

create or replace function ibrd.cmplx_forecaster_ttb_pkg$get_tt_action_tt
    (p_tt_action_id ttb.tt_action.tt_action_id%type)
    returns table(stop_order_num rts.stop_item2round.order_num%type,
                  stop_item_id   rts.stop_item.stop_item_id%type,
                  time_oper      timestamp,
                  tt             real)
as $$
declare
begin
    return query
    select *
    from (select
              si2r.order_num stop_order_num,
              si2r.stop_item_id,
              tai.time_begin time_oper,
              coalesce(extract(epoch from (tai.time_begin - lag(tai.time_begin)
              over (
                  order by si2r.order_num ))), 0) :: real tt
          from ttb.tt_action_item tai
              join rts.stop_item2round si2r on tai.stop_item2round_id = si2r.stop_item2round_id
          where tai.tt_action_id = p_tt_action_id
                and not si2r.sign_deleted) t
    where t.tt > 0 or t.stop_order_num = 1;
end;
$$
language plpgsql
security definer;

create or replace function ibrd.cmplx_forecaster_ttb_pkg$get_next_tt_action
    (p_tt_action_id ttb.tt_action.tt_action_id%type)
    returns table(tt_action_id    ttb.tt_action.tt_action_id%type,
                  round_id        rts.round.round_id%type,
                  time_plan_begin timestamp)
as $$
declare
begin
    return query
    with cur_tt_action as (
        select
            tv.order_date,
            tout.tr_id,
            ta.dt_action
        from ttb.tt_action ta
            join ttb.tt_out tout on ta.tt_out_id = tout.tt_out_id
            join ttb.tt_variant tv on tout.tt_variant_id = tv.tt_variant_id
        where ta.tt_action_id = p_tt_action_id
    )
    select
        ta.tt_action_id,
        tar.round_id,
        ta.dt_action
    from cur_tt_action cta
        join ttb.tt_variant tv on cta.order_date = tv.order_date
        join ttb.tt_out tout on tout.tt_variant_id = tv.tt_variant_id and cta.tr_id = tout.tr_id
        join ttb.tt_action ta on ta.tt_out_id = tout.tt_out_id
        join ttb.tt_action_round tar on ta.tt_action_id = tar.tt_action_id
    where ta.dt_action > cta.dt_action
          and not tv.sign_deleted
          and not tout.sign_deleted
          and not ta.sign_deleted
    order by ta.dt_action
    limit 1;
end;
$$
language plpgsql
security definer;

create or replace function ibrd.cmplx_forecaster_ttb_pkg$get_round_stops
    (p_round_id rts.round.round_id%type)
    returns table(round_id       rts.round.round_id%type,
                  stop_order_num rts.stop_item2round.order_num%type,
                  stop_item_id   rts.stop_item.stop_item_id%type)
as $$
begin
    return query
    select
        si2r.round_id,
        si2r.order_num stop_order_num,
        si2r.stop_item_id
    from rts.stop_item2round si2r
    where si2r.round_id = p_round_id;
end;
$$
language plpgsql
security definer;

create or replace function ibrd.cmplx_forecaster_ttb_pkg$train_algorithm
    (p_classifier         text,
     p_classifier_options text,
     p_route_id           rts.route.route_id%type,
     p_begin_dt           ttb.tt_variant.order_date%type,
     p_end_dt             ttb.tt_variant.order_date%type,
     p_window_h           int)
    returns void
as $$
declare
    lroute_id rts.route.route_id%type;
begin
    select route_id
    into lroute_id
    from rts.route
    where route_id = p_route_id;

    if lroute_id is null
    then
        raise exception 'No such route';
    end if;

    perform paa.paa_api_pkg$basic_publish(cmnd.jf_cmnd_ref4unit_pkg$cn_command_rmq_broker(),
                                          '',
                                          'model-train-request-ttb',
                                          json_build_object('customConfig',
                                                            json_build_object('classifier', p_classifier,
                                                                              'classifierOptions',
                                                                              p_classifier_options),
                                                            'routeId', p_route_id,
                                                            'beginDate', p_begin_dt,
                                                            'endDate', p_end_dt,
                                                            'windowWidth', p_window_h) :: text,
                                          'TRAIN_REQUEST');
end;
$$
language plpgsql
security definer;

create or replace function ibrd.cmplx_forecaster_ttb_pkg$train_algorithm_1
    (p_route_id rts.route.route_id%type,
     p_begin_dt ttb.tt_variant.order_date%type,
     p_end_dt   ttb.tt_variant.order_date%type,
     p_window_h int)
    returns void
as $$
begin
    perform ibrd.cmplx_forecaster_ttb_pkg$train_algorithm('weka.classifiers.functions.MultilayerPerceptron',
                                                          '-N 10 -H 10',
                                                          p_route_id,
                                                          p_begin_dt,
                                                          p_end_dt,
                                                          p_window_h);
end;
$$
language plpgsql
security definer;

create or replace function ibrd.cmplx_forecaster_ttb_pkg$train_algorithm_2
    (p_route_id rts.route.route_id%type,
     p_begin_dt ttb.tt_variant.order_date%type,
     p_end_dt   ttb.tt_variant.order_date%type,
     p_window_h int)
    returns void
as $$
begin
    perform ibrd.cmplx_forecaster_ttb_pkg$train_algorithm('weka.classifiers.meta.AdditiveRegression',
                                                          '-I 30 -W weka.classifiers.trees.RandomTree -- -depth 3',
                                                          p_route_id,
                                                          p_begin_dt,
                                                          p_end_dt,
                                                          p_window_h);
end;
$$
language plpgsql
security definer;