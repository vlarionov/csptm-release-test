CREATE OR REPLACE FUNCTION ibrd.event_type_pkg$connect()
  RETURNS ibrd.event_type.event_type_id%TYPE
IMMUTABLE
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN 0;
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.event_type_pkg$disconnect()
  RETURNS ibrd.event_type.event_type_id%TYPE
IMMUTABLE
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN 1;
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.event_type_pkg$command_response()
  RETURNS ibrd.event_type.event_type_id%TYPE
IMMUTABLE
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN 2;
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.event_type_pkg$set_config()
  RETURNS ibrd.event_type.event_type_id%TYPE
IMMUTABLE
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN 3;
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.event_type_pkg$set_info()
  RETURNS ibrd.event_type.event_type_id%TYPE
IMMUTABLE
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN 4;
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.event_type_pkg$set_datetime()
  RETURNS ibrd.event_type.event_type_id%TYPE
IMMUTABLE
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN 5;
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.event_type_pkg$set_temperature()
  RETURNS ibrd.event_type.event_type_id%TYPE
IMMUTABLE
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN 6;
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.event_type_pkg$telematics_request()
  RETURNS ibrd.event_type.event_type_id%TYPE
IMMUTABLE
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN 7;
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.event_type_pkg$telematics_response()
  RETURNS ibrd.event_type.event_type_id%TYPE
IMMUTABLE
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN 8;
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.event_type_pkg$get_commands()
  RETURNS INTEGER []
IMMUTABLE
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN ARRAY[
      ibrd.event_type_pkg$set_config(),
      ibrd.event_type_pkg$set_info(),
      ibrd.event_type_pkg$set_datetime(),
      ibrd.event_type_pkg$set_temperature(),
      ibrd.event_type_pkg$telematics_request()
  ];
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.event_type_pkg$get_commands_responses()
  RETURNS INTEGER []
IMMUTABLE
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN ARRAY[
      ibrd.event_type_pkg$command_response(),
      ibrd.event_type_pkg$telematics_response()
  ];
END;
$$;

