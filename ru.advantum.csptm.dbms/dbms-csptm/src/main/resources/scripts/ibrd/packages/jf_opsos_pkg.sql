CREATE OR REPLACE FUNCTION ibrd.jf_opsos_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ibrd.mobile_operator AS
$body$
declare 
   l_r ibrd.mobile_operator%rowtype; 
begin 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 
   l_r.mobile_operator_id := jofl.jofl_pkg$extract_number(p_attr, 'mobile_operator_id', true); 
   l_r.is_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'is_deleted', true); 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_opsos_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.mobile_operator%rowtype;
begin 
   l_r := ibrd.jf_opsos_pkg$attr_to_rowtype(p_attr);

   /*delete from  ibrd.mobile_operator where  mobile_operator_id = l_r.mobile_operator_id;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_opsos_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.mobile_operator%rowtype;
begin 
   l_r := ibrd.jf_opsos_pkg$attr_to_rowtype(p_attr);

   /*insert into ibrd.mobile_operator select l_r.*;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_opsos_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
begin 
 open p_rows for 
      select 
        name, 
        mobile_operator_id, 
        is_deleted
      from ibrd.mobile_operator
     where not is_deleted; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
-------------------------------------------------
-------------------------------------------------