﻿-- ========================================================================
-- abc: Пакет расчета прогноза по сообытию прохода через остановку
-- ========================================================================

create or replace function ibrd.forecaster_pkg$analyze_stat
  (p_oper_date           in timestamp)
returns bigint
-- ========================================================================
-- Собрать статистику да день по остановкам рейсов
-- ========================================================================
as $$ declare
   l_oper_date           timestamp := p_oper_date::date::timestamp;
   l_result              bigint := 0;
   l_tmp                 timestamp;
begin

   select oper_date into l_tmp from ibrd.round_stat_date where oper_date = l_oper_date;

   if not found then

      with stat_data as
      (select round_id, stop_order_num, item_count, sum_date_diff
         from (select q.round_id, q.stop_order_num,
                      count((q.time_fact - q.time_oper) - (q.pre_time_fact - q.pre_time_oper)) as item_count,
                      sum((q.time_fact - q.time_oper) - (q.pre_time_fact - q.pre_time_oper)) as sum_date_diff
                 from (select orr.round_id, oro.stop_order_num, oro.time_oper, orf.time_fact,
                              lag(oro.time_oper, 1, oro.time_oper) over (partition by oro.order_round_id order by oro.time_oper, oro.stop_order_num) as pre_time_oper,
                              lag(orf.time_fact, 1, orf.time_fact) over (partition by oro.order_round_id order by oro.time_oper, oro.stop_order_num) as pre_time_fact
                         from tt.order_list  orl
                         join tt.order_round orr on orr.order_list_id  = orl.order_list_id
                         join tt.order_oper  oro on oro.order_round_id = orr.order_round_id
                         left join tt.order_fact orf on orf.order_round_id  = oro.order_round_id
                                                    and orf.stop_place_muid = oro.stop_place_muid
                                                    and orf.stop_order_num  = oro.stop_order_num
                                                    and orf.sign_deleted = 0
                        where orl.sign_deleted = 0
                          and orl.is_active = 1
                          and orr.is_active = 1
                          and oro.sign_deleted = 0
                          and oro.is_active = 1
                          and orl.order_date = l_oper_date
                      ) q
                group by q.round_id, q.stop_order_num
              ) a
        where a.item_count > 0
        order by round_id, stop_order_num
      )
      insert into ibrd.round_stat as t (round_id, stop_order_num, sum_date_diff, item_count)
      select round_id, stop_order_num, sum_date_diff, item_count
        from stat_data as s
          on conflict (round_id, stop_order_num)
          do update set (sum_date_diff,
                         item_count
                        ) =
                        (t.sum_date_diff + excluded.sum_date_diff,
                         t.item_count + excluded.item_count
                        );

      get diagnostics l_result = row_count;
      if l_result > 0 then
         insert into ibrd.round_stat_date(oper_date) values (l_oper_date);
         l_result := 1;
      end if;

   end if;

   return l_result;
end;
$$ language plpgsql security definer;


create or replace function ibrd.forecaster_pkg$analyze_stat_prev_day()
returns bigint
-- ========================================================================
-- Собрать статистику за предыдущий день
-- ========================================================================
as $$ declare
   l_oper_date           timestamp := now()::date::timestamp - interval '1 day';
begin
   return ibrd.forecaster_pkg$analyze_stat(l_oper_date);
end;
$$ language plpgsql security definer;


create or replace function ibrd.forecaster_pkg$round_forecast
  (p_round_id            in bigint,
   p_stop_order_num      in bigint,
   p_time_oper           in timestamp,
   p_pre_order_num       in bigint,
   p_pre_time_fact       in timestamp,
   p_pre_time_oper       in timestamp)
returns timestamp
-- ========================================================================
-- Простой прогноз по статистике
-- ========================================================================
as $$ declare
   l_result              timestamp;
   l_E                   interval;
begin
   if p_pre_order_num is not null and p_pre_time_fact is not null and p_pre_time_oper is not null and p_pre_order_num < p_stop_order_num then

      select sum(sum_date_diff / item_count)
        into l_E
        from ibrd.round_stat
       where round_id = p_round_id
         and stop_order_num > p_pre_order_num
         and stop_order_num <= p_stop_order_num
         and item_count <> 0;

      l_result := p_time_oper + (p_pre_time_fact - p_pre_time_oper) + coalesce(l_E, interval '0 min');
   else
      select sum_date_diff / item_count
        into l_E
        from ibrd.round_stat
       where round_id = p_round_id
         and stop_order_num = p_stop_order_num
         and item_count <> 0;

      l_result := p_time_oper + coalesce(l_E, interval '0 min');
   end if;

   return l_result;
end;
$$ language plpgsql security definer;


create or replace function ibrd.forecaster_pkg$get_stop_place_forecast
  (p_oper_date           in timestamp,
   p_stop_place_muid     in bigint)
returns table(order_round_id int8, stop_place_muid int8, stop_order_num int8, creation_date timestamp, forecast timestamp)
-- ========================================================================
-- Простой прогноз по статистике для остановки
-- ========================================================================
as $$ declare
begin
   return query
   select q.order_round_id, q.stop_place_muid, q.stop_order_num,
          localtimestamp as creation_date,
          ibrd.forecaster_pkg$round_forecast (q.round_id, q.stop_order_num, q.time_oper, f.stop_order_num, f.time_fact, o.time_oper) as forecast
     from (select rov.route_muid, rnd.round_id, oro.order_round_id, oro.stop_place_muid, oro.stop_order_num, oro.time_oper,
                  (select orf.order_fact_id
                     from tt.order_fact orf
                    where orf.order_round_id = oro.order_round_id
                      and orf.sign_deleted = 0
                      and orf.time_fact =
                          (select max(srf.time_fact)
                             from tt.order_fact srf
                            where srf.sign_deleted = 0
                              and srf.order_round_id = oro.order_round_id
                              and srf.stop_order_num <= oro.stop_order_num
                          )
                  ) as order_fact_id
             from tt.order_oper oro
             join tt.order_round orr on orr.order_round_id = oro.order_round_id
             join tt.round rnd on rnd.round_id = orr.round_id
             join gis.route_trajectories rtj on rtj.muid = rnd.route_trajectory_muid
             join gis.route_rounds ror on ror.muid = rtj.route_round_muid
             join gis.route_variants rov on rov.muid = ror.route_variant_muid
            where oro.sign_deleted = 0
              and oro.is_active = 1
              and orr.is_active = 1
              and oro.time_oper > p_oper_date
              and oro.stop_place_muid = p_stop_place_muid
              and (rov.route_muid, oro.time_oper) in
                  (select route_muid, min(time_oper) as time_oper
                     from tt.order_oper oro
                     join tt.order_round orr on orr.order_round_id = oro.order_round_id
                     join tt.round rnd on rnd.round_id = orr.round_id
                     join gis.route_trajectories rtj on rtj.muid = rnd.route_trajectory_muid
                     join gis.route_rounds ror on ror.muid = rtj.route_round_muid
                     join gis.route_variants rov on rov.muid = ror.route_variant_muid
                    where oro.sign_deleted = 0
                      and oro.is_active = 1
                      and orr.is_active = 1
                      and oro.stop_place_muid = p_stop_place_muid
                      and oro.time_oper > p_oper_date
                    group by route_muid
                  )
          ) q
     left join tt.order_fact f on f.order_fact_id   = q.order_fact_id
     left join tt.order_oper o on o.order_round_id  = f.order_round_id
                              and o.stop_place_muid = f.stop_place_muid
                              and o.stop_order_num  = f.stop_order_num
                              and o.sign_deleted    = 0;
end;
$$ language plpgsql security definer;


create or replace function ibrd.forecaster_pkg$get_forecast_by_stop_visit
  (p_oper_date           in timestamp,
   p_order_round_id      in bigint,
   p_stop_order_num      in bigint)
returns table(order_round_id int8, stop_place_muid int8, stop_order_num int8, creation_date timestamp, forecast timestamp)
-- ========================================================================
-- Прогноз по событию прохождения остновки
-- ========================================================================
as $$ declare
   r record;
begin
   for r in (select oro.stop_place_muid
               from tt.order_oper oro
               join ibrd.stop_place2info_board sp2b on sp2b.stop_place_muid = oro.stop_place_muid
              where oro.order_round_id = p_order_round_id
                and oro.sign_deleted = 0
                and oro.is_active = 1
                and oro.stop_order_num >= p_stop_order_num
              order by oro.stop_order_num)
   loop
      return query select q.order_round_id, q.stop_place_muid, q.stop_order_num, q.creation_date, q.forecast
                     from ibrd.forecaster_pkg$get_stop_place_forecast (p_oper_date, r.stop_place_muid) as q;
   end loop;
end;
$$ language plpgsql security definer;


create or replace function ibrd.forecaster_pkg$get_forecast_by_stop_visit
  (p_oper_date   in timestamp,
   p_json        in jsonb)
returns table(order_round_id int8, stop_place_muid int8, stop_order_num int8, creation_date timestamp, forecast timestamp)
-- ========================================================================
-- Прогноз по событию прохождения остновки
-- ========================================================================
as $$ declare
   l_order_round_id   bigint;
   l_stop_order_num   bigint;
   l_json_item        jsonb;
begin
   for l_json_item in select jsonb_array_elements(p_json) loop

      l_order_round_id := (l_json_item->>'orderRoundId')::bigint;
      l_stop_order_num := (l_json_item->>'stopPlaceOrderNum')::bigint;

      return query select q.order_round_id, q.stop_place_muid, q.stop_order_num, q.creation_date, q.forecast
                     from ibrd.forecaster_pkg$get_forecast_by_stop_visit (p_oper_date, l_order_round_id, l_stop_order_num) as q;
   end loop;
end;
$$ language plpgsql security definer;


create or replace function ibrd.forecaster_pkg$get_forecast_by_stop_visit
  (p_json    in jsonb)
returns text
-- ========================================================================
-- Прогноз по событию прохождения остновки
-- ========================================================================
as $$ declare
   l_oper_date        timestamp := localtimestamp;
   l_result           text;
begin
   select array_to_json(array_agg(row_to_json(q))) into l_result
     from ibrd.forecaster_pkg$get_forecast_by_stop_visit(l_oper_date, p_json) q;

   return l_result;
end;
$$ language plpgsql security definer;

create or replace function ibrd.forecaster_pkg$train_algorithm
    (p_classifier         text,
     p_classifier_options text,
     p_route_id           bigint,
     p_begin_dt           timestamp,
     p_end_dt             timestamp,
     p_window_h           int)
    returns void
as $$
declare
    lroute_muid bigint;
begin
    select muid
    into lroute_muid
    from gis.routes
    where muid = p_route_id;

    if lroute_muid is null
    then
        raise exception 'No such route';
    end if;

    perform paa.paa_api_pkg$basic_publish(cmnd.jf_cmnd_ref4unit_pkg$cn_command_rmq_broker(),
                                          '',
                                          'model-train-request',
                                          json_build_object('customConfig', json_build_object('classifier', p_classifier,
                                                                                              'classifierOptions', p_classifier_options),
                                                            'routeId', p_route_id,
                                                            'beginDate', p_begin_dt,
                                                            'endDate', p_end_dt,
                                                            'windowWidth', p_window_h) :: text,
                                          'TRAIN_REQUEST');
end;
$$ language plpgsql security definer;

create or replace function ibrd.forecaster_pkg$train_algorithm_1
    (p_route_id bigint,
     p_begin_dt timestamp,
     p_end_dt   timestamp,
     p_window_h int)
    returns void
as $$
begin
    perform ibrd.forecaster_pkg$train_algorithm('weka.classifiers.functions.MultilayerPerceptron',
                                                '-N 10 -H 10',
                                                p_route_id,
                                                p_begin_dt,
                                                p_end_dt,
                                                p_window_h);
end;
$$ language plpgsql security definer;

create or replace function ibrd.forecaster_pkg$train_algorithm_2
    (p_route_id bigint,
     p_begin_dt timestamp,
     p_end_dt   timestamp,
     p_window_h int)
    returns void
as $$
begin
    perform ibrd.forecaster_pkg$train_algorithm('weka.classifiers.meta.AdditiveRegression',
                                                '-I 30 -W weka.classifiers.trees.RandomTree -- -depth 3',
                                                p_route_id,
                                                p_begin_dt,
                                                p_end_dt,
                                                p_window_h);
end;
$$ language plpgsql security definer;