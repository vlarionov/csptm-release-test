create or replace function ibrd.forecast_pkg$find_actual_forecast(p_info_board_id integer)
  returns table(
    round_id    rts.round.round_id%type,
    forecast_id ibrd.forecast.forecast_id%type
  )
language plpgsql
as $$
declare
begin
  return query
  with last_forecast as (
      select distinct on (f.tt_action_id, f.stop_item_id, f.stop_order_num) f.*
      from ibrd.stop_location2info_board sl2ib
        join rts.stop_item si on sl2ib.stop_location_id = si.stop_location_id
        join ibrd.forecast f on si.stop_item_id = f.stop_item_id
      where sl2ib.info_board_id = p_info_board_id
      order by f.tt_action_id, f.stop_item_id, f.stop_order_num, f.from_stop_order_num desc, f.creation_time desc
  )
  select
    ttar.round_id,
    lf.forecast_id
  from last_forecast lf
    join ttb.tt_action_round ttar on lf.tt_action_id = ttar.tt_action_id
  where ibrd.forecast_pkg$is_show(p_info_board_id, ttar.round_id)
        and lf.pass_time_forecast >= now()
        and not exists(select 1
                       from ttb.tt_action tta
                         join ttb.tt_action_item ttai on tta.tt_action_id = ttai.tt_action_id
                         join rts.stop_item2round si2r on ttai.stop_item2round_id = si2r.stop_item2round_id
                         join ttb.tt_action_fact ttaf on tta.tt_action_id = ttaf.tt_action_id and si2r.stop_item2round_id = ttaf.stop_item2round_id
                       where tta.tt_action_id = lf.tt_action_id
                             and si2r.stop_item_id = lf.stop_item_id
                             and si2r.order_num = lf.stop_order_num)
  order by lf.forecast_id;
end;
$$;

create or replace function ibrd.forecast_pkg$find_current(
  p_info_board_id ibrd.info_board.info_board_id%type
)
  returns table(
    forecast_id  ibrd.forecast.forecast_id%type,
    route_number text,
    arrival_time timestamp,
    terminal     text
  )
language plpgsql
as $$
begin
  return query
  with round_terminal as (
      select
        af.forecast_id,
        af.round_id,
        si2r.stop_item2round_id,
        si2r.order_num,
        max(si2r.order_num) over ( partition by af.round_id ) max_order_num
      from ibrd.forecast_pkg$find_actual_forecast(p_info_board_id) af
        join rts.stop_item2round si2r on af.round_id = si2r.round_id
  )
  select distinct on (r.route_num, s.name)
    rt.forecast_id,
    ibrd.forecast_pkg$get_route_display_name(r.route_num, r.tr_type_id),
    f.pass_time_forecast,
    s.name
  from round_terminal rt
    join ibrd.forecast f on rt.forecast_id = f.forecast_id
    join rts.round rnd on rt.round_id = rnd.round_id
    join rts.route_variant rv on rnd.route_variant_id = rv.route_variant_id
    join rts.route r on rv.route_id = r.route_id
    join rts.stop_item2round si2r on rt.stop_item2round_id = si2r.stop_item2round_id
    join rts.stop_item si on si2r.stop_item_id = si.stop_item_id
    join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
    join rts.stop s on sl.stop_id = s.stop_id
  where rt.order_num = max_order_num
  order by r.route_num, s.name, f.pass_time_forecast;
end;
$$;

create or replace function ibrd.forecast_pkg$is_show(
  p_info_board_id ibrd.info_board.info_board_id%type,
  p_round_id      rts.round.round_id%type
)
  returns boolean
language plpgsql
as $$
begin
  return not exists(
      select 1
      from ibrd.disabled_round4forecast dr4f
      where dr4f.info_board_id = p_info_board_id and
            dr4f.round_id = p_round_id
  );
end;
$$;

create or replace function ibrd.forecast_pkg$get_route_display_name(
  p_number     rts.route.route_num%type,
  p_tr_type_id core.tr_type.tr_type_id%type
)
  returns text
language plpgsql
as $$
begin
  case p_tr_type_id
    when core.tr_type_pkg$bus()
    then return p_number;
    when core.tr_type_pkg$tb()
    then return 'т' || p_number;
    when core.tr_type_pkg$tm()
    then return 'тм' || p_number;
  else
    return p_number;
  end case;
end;
$$;

create or replace function ibrd.forecast_pkg$get_last_forecast(
  p_info_board_id ibrd.info_board.info_board_id%type,
  p_round_id      rts.round.round_id%type,
  p_stop_item_id  rts.stop_item.stop_item_id%type
)
  returns table(pass_time_forecast timestamp, tr_id bigint, garage_num text)
language plpgsql
as $$
begin
  return query
  select distinct on (tr.tr_id)
    f.pass_time_forecast,
    tr.tr_id,
    tr.garage_num :: text
  from ibrd.forecast_pkg$find_actual_forecast(p_info_board_id) af
    join ibrd.forecast f on f.forecast_id = af.forecast_id
    join ttb.tt_action tta on f.tt_action_id = tta.tt_action_id
    join ttb.tt_action_round ttar on tta.tt_action_id = ttar.tt_action_id
    join ttb.tt_out tto on tta.tt_out_id = tto.tt_out_id
    join core.tr tr on tto.tr_id = tr.tr_id
  where f.stop_item_id = p_stop_item_id
        and ttar.round_id = p_round_id;
end;
$$;

create or replace function ibrd.forecast_pkg$get_terminal(
      p_round_id  rts.round.round_id%type,
  out p_result text
)
language plpgsql
as $$
begin
  select s.name
  from rts.stop_item2round si2r
    join rts.stop_item si on si2r.stop_item_id = si.stop_item_id
    join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
    join rts.stop s on sl.stop_id = s.stop_id
  where si2r.round_id = p_round_id
  order by si2r.order_num desc
  limit 1
  into p_result;
end;
$$;

create or replace function ibrd.forecast_pkg$find_target_stop_item_ids()
  returns setof rts.stop_item.stop_item_id%type
language plpgsql
as $$
declare
begin
  return query
  select distinct si.stop_item_id
  from ibrd.stop_location2info_board sl2ib
    join rts.stop_item si on sl2ib.stop_location_id = si.stop_location_id;
end;
$$;