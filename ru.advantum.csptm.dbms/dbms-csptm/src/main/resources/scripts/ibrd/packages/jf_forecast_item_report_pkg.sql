CREATE OR REPLACE FUNCTION ibrd.jf_forecast_item_report_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                                    p_attr       TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_info_board_id   ibrd.info_board.info_board_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'info_board_id', TRUE);
  l_order_round_id  BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'order_round_id', TRUE);
  l_stop_place_muid gis.stop_places.muid%TYPE :=  jofl.jofl_pkg$extract_varchar(p_attr, 'stop_place_muid', TRUE) :: BIGINT;
  l_stop_order_num  INTEGER := jofl.jofl_pkg$extract_number(p_attr, 'stop_order_num', TRUE);
BEGIN
  OPEN p_rows FOR
  SELECT
    f.creation_time,
    f.pass_time_forecast
  FROM
    ibrd.forecast f
  WHERE f.order_round_id = l_order_round_id
        AND f.stop_place_muid = l_stop_place_muid
        AND f.stop_place_order_num = l_stop_order_num
        AND exists(
            SELECT 1
            FROM ibrd.event e
            WHERE f.forecast_id = ANY (e.forecasts_ids)
                  AND e.info_board_id = l_info_board_id
                  AND e.event_type_id = ibrd.event_type_pkg$set_info()
                  AND exists(
                      SELECT 1
                      FROM ibrd.event re
                      WHERE e.event_id = re.command_event_id AND re.error_type_id =  ibrd.error_type_pkg$null()
                  )
        )
  ORDER BY f.creation_time;
END;
$$;