CREATE OR REPLACE FUNCTION ibrd.jf_mono_rgb_config_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                          p_attr       TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_info_board_id ibrd.info_board.info_board_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'info_board_id', TRUE);
  l_rf_db_method  TEXT := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
BEGIN
  IF l_rf_db_method IS NOT NULL
  THEN l_info_board_id = NULL;
  END IF;

  OPEN p_rows FOR
  SELECT
    c.config_id,
    c.is_default,
    mrc.is_display_datetime,
    c.is_display_temperature,
    mrc.running_row_position_id,
    rrp.name running_row_position_name,
    mrc.scroll_type_id,
    st.name scroll_type_name,
    mrc.scroll_direction_id,
    sc.name scroll_direction_name,
    mrc.rows_number,
    mrc.route_number_style_id,
    rnfs.name route_number_style_name,
    mrc.arrival_time_style_id,
    atfs.name arrival_time_style_name,
    mrc.terminal_style_id,
    tfs.name terminal_style_name,
    extract(EPOCH FROM mrc.forecast_delay) forecast_delay,
    c.running_row_speed,
    extract(EPOCH FROM c.data_waiting_time) /60 data_waiting_time,
    extract(EPOCH FROM c.max_forecast_interval) / 60 max_forecast_interval,
    c.is_limit_forecasts_by_rows,
    CASE WHEN l_info_board_id IS NULL
      THEN 'P{A},D{OF_DELETE}'
    ELSE 'P{A},D{OF_BIND}'
    END                                             "ROW$POLICY"
  FROM ibrd.config c
    JOIN ibrd.mono_rgb_config mrc ON c.config_id = mrc.config_id
    JOIN ibrd.running_row_position rrp ON mrc.running_row_position_id = rrp.runnning_row_position_id
    JOIN ibrd.scroll_type st ON mrc.scroll_type_id = st.scroll_type_id
    JOIN ibrd.scroll_direction sc ON mrc.scroll_direction_id = sc.scroll_direction_id
    JOIN ibrd.font_style rnfs ON mrc.route_number_style_id = rnfs.font_style_id
    JOIN ibrd.font_style atfs ON mrc.arrival_time_style_id = atfs.font_style_id
    JOIN ibrd.font_style tfs ON mrc.terminal_style_id = tfs.font_style_id
  WHERE (l_info_board_id IS NULL OR exists(SELECT 1
                                           FROM ibrd.info_board2config ib2c
                                           WHERE ib2c.info_board_id = l_info_board_id AND ib2c.config_id = c.config_id));
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.jf_mono_rgb_config_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_config_id              ibrd.config.config_id%TYPE;

  l_is_display_datetime           ibrd.mono_rgb_config.is_display_datetime%TYPE := jofl.jofl_pkg$extract_boolean(p_attr, 'is_display_datetime', FALSE);
  l_is_display_temperature           ibrd.config.is_display_temperature%TYPE := jofl.jofl_pkg$extract_boolean(p_attr, 'is_display_temperature', FALSE);
  l_running_row_position_id           ibrd.mono_rgb_config.running_row_position_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'running_row_position_id', FALSE);
  l_scroll_type_id           ibrd.mono_rgb_config.scroll_type_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'scroll_type_id', FALSE);
  l_scroll_direction_id      ibrd.mono_rgb_config.scroll_direction_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'scroll_direction_id', FALSE);
  l_rows_number      ibrd.mono_rgb_config.rows_number%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'rows_number', FALSE);
  l_route_number_style_id      ibrd.mono_rgb_config.route_number_style_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'route_number_style_id', FALSE);
  l_arrival_time_style_id      ibrd.mono_rgb_config.arrival_time_style_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'arrival_time_style_id', FALSE);
  l_terminal_style_id      ibrd.mono_rgb_config.terminal_style_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'terminal_style_id', FALSE);
  l_forecast_delay      ibrd.mono_rgb_config.forecast_delay%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'forecast_delay', FALSE) * '1 SECONDS'::INTERVAL;
  l_running_row_speed      ibrd.config.running_row_speed%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'running_row_speed', FALSE);
  l_data_waiting_time      ibrd.config.data_waiting_time%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'data_waiting_time', FALSE) * '1 MINUTES'::INTERVAL;
  l_max_forecast_interval ibrd.config.max_forecast_interval%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'max_forecast_interval', FALSE) * '1 MINUTES' :: INTERVAL;
  l_is_limit_forecasts_by_rows ibrd.config.is_limit_forecasts_by_rows%TYPE := jofl.jofl_pkg$extract_boolean(p_attr, 'is_limit_forecasts_by_rows', FALSE);

  l_info_board_id          ibrd.info_board.info_board_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'info_board_id', TRUE);
BEGIN
  INSERT INTO ibrd.config (is_default, running_row_speed, data_waiting_time, is_display_temperature, max_forecast_interval, is_limit_forecasts_by_rows) VALUES (
    FALSE,
    l_running_row_speed,
    l_data_waiting_time,
    l_is_display_temperature,
    l_max_forecast_interval,
    l_is_limit_forecasts_by_rows
  )
  RETURNING config_id
    INTO l_config_id;

  INSERT INTO ibrd.mono_rgb_config (config_id, running_row_position_id, scroll_type_id, scroll_direction_id, rows_number, route_number_style_id, arrival_time_style_id, terminal_style_id, forecast_delay, is_display_datetime) VALUES (
    l_config_id,
    l_running_row_position_id,
    l_scroll_type_id,
    l_scroll_direction_id,
    l_rows_number,
    l_route_number_style_id,
    l_arrival_time_style_id,
    l_terminal_style_id,
    l_forecast_delay,
    l_is_display_datetime
  );

  IF l_info_board_id IS NOT NULL AND l_info_board_id <> -1
  THEN
    DELETE FROM ibrd.info_board2config
    WHERE info_board_id = l_info_board_id;
    INSERT INTO ibrd.info_board2config (info_board_id, config_id) VALUES (l_info_board_id, l_config_id);
  END IF;

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.jf_mono_rgb_config_pkg$of_update(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_config_id              ibrd.config.config_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'config_id', FALSE);

  l_is_display_datetime           ibrd.mono_rgb_config.is_display_datetime%TYPE := jofl.jofl_pkg$extract_boolean(p_attr, 'is_display_datetime', FALSE);
  l_is_display_temperature           ibrd.config.is_display_temperature%TYPE := jofl.jofl_pkg$extract_boolean(p_attr, 'is_display_temperature', FALSE);
  l_running_row_position_id           ibrd.mono_rgb_config.running_row_position_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'running_row_position_id', FALSE);
  l_scroll_type_id           ibrd.mono_rgb_config.scroll_type_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'scroll_type_id', FALSE);
  l_scroll_direction_id      ibrd.mono_rgb_config.scroll_direction_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'scroll_direction_id', FALSE);
  l_rows_number      ibrd.mono_rgb_config.rows_number%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'rows_number', FALSE);
  l_route_number_style_id      ibrd.mono_rgb_config.route_number_style_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'route_number_style_id', FALSE);
  l_arrival_time_style_id      ibrd.mono_rgb_config.arrival_time_style_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'arrival_time_style_id', FALSE);
  l_terminal_style_id      ibrd.mono_rgb_config.terminal_style_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'terminal_style_id', FALSE);
  l_forecast_delay      ibrd.mono_rgb_config.forecast_delay%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'forecast_delay', FALSE) * '1 SECONDS'::INTERVAL;
  l_running_row_speed      ibrd.config.running_row_speed%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'running_row_speed', FALSE);
  l_data_waiting_time      ibrd.config.data_waiting_time%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'data_waiting_time', FALSE) * '1 MINUTES'::INTERVAL;
  l_max_forecast_interval ibrd.config.max_forecast_interval%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'max_forecast_interval', FALSE) * '1 MINUTES' :: INTERVAL;
  l_is_limit_forecasts_by_rows ibrd.config.is_limit_forecasts_by_rows%TYPE := jofl.jofl_pkg$extract_boolean(p_attr, 'is_limit_forecasts_by_rows', FALSE);

BEGIN
  UPDATE ibrd.config
  SET
    running_row_speed      = l_running_row_speed,
    data_waiting_time      = l_data_waiting_time,
    is_display_temperature = l_is_display_temperature,
    max_forecast_interval  = l_max_forecast_interval,
    is_limit_forecasts_by_rows = l_is_limit_forecasts_by_rows
  WHERE config_id = l_config_id;

  UPDATE ibrd.mono_rgb_config
  SET
    running_row_position_id = l_running_row_position_id,
    scroll_type_id = l_scroll_type_id,
    scroll_direction_id = l_scroll_direction_id,
    rows_number = l_rows_number,
    route_number_style_id = l_route_number_style_id,
    arrival_time_style_id = l_arrival_time_style_id,
    terminal_style_id = l_terminal_style_id,
    forecast_delay = l_forecast_delay,
    is_display_datetime = l_is_display_datetime
  WHERE config_id = l_config_id;

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.jf_mono_rgb_config_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_config_id ibrd.config.config_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'config_id', FALSE);
BEGIN
  DELETE FROM ibrd.mono_rgb_config
  WHERE config_id = l_config_id;

  DELETE FROM ibrd.config
  WHERE config_id = l_config_id;

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.jf_mono_rgb_config_pkg$of_copy(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN ibrd.jf_mono_rgb_config_pkg$of_insert(p_id_account, p_attr);
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.jf_mono_rgb_config_pkg$of_bind(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_config_id              ibrd.config.config_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'f_config_id', FALSE);
  l_info_board_id ibrd.info_board.info_board_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'info_board_id', FALSE);
BEGIN
    DELETE FROM ibrd.info_board2config
    WHERE info_board_id = l_info_board_id;
    INSERT INTO ibrd.info_board2config (info_board_id, config_id) VALUES (l_info_board_id, l_config_id);

  RETURN NULL;
END;
$$;