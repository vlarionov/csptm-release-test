CREATE OR REPLACE FUNCTION ibrd.jf_row_config_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                          p_attr       TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_info_board_id ibrd.info_board.info_board_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'info_board_id', TRUE);
  l_rf_db_method  TEXT := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
BEGIN
  IF l_rf_db_method IS NOT NULL
  THEN l_info_board_id = NULL;
  END IF;

  OPEN p_rows FOR
  SELECT
    c.config_id,
    c.is_default,
    c.running_row_speed,
    rc.scroll_speed,
    extract(EPOCH FROM rc.block_rate)               block_rate,
    rc.is_display_date,
    rc.is_display_time,
    c.is_display_temperature,
    extract(EPOCH FROM rc.server_waiting_time) / 60 server_waiting_time,
    extract(EPOCH FROM c.data_waiting_time) / 60    data_waiting_time,
    extract(EPOCH FROM rc.reboot_time) / 3600       reboot_time,
    extract(EPOCH FROM c.max_forecast_interval) / 60 max_forecast_interval,
    c.is_limit_forecasts_by_rows,
    CASE WHEN l_info_board_id IS NULL
      THEN 'P{A},D{OF_DELETE}'
    ELSE 'P{A},D{OF_BIND}'
    END                                             "ROW$POLICY"
  FROM ibrd.config c
    JOIN ibrd.row_config rc ON c.config_id = rc.config_id
  WHERE (l_info_board_id IS NULL OR exists(SELECT 1
                                           FROM ibrd.info_board2config ib2c
                                           WHERE ib2c.info_board_id = l_info_board_id AND ib2c.config_id = c.config_id));
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.jf_row_config_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_config_id              ibrd.config.config_id%TYPE;

  l_running_row_speed      ibrd.config.running_row_speed%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'running_row_speed', FALSE);
  l_scroll_speed           ibrd.row_config.scroll_speed%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'scroll_speed', FALSE);
  l_block_rate             ibrd.row_config.block_rate%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'block_rate', FALSE) * '1 SECONDS' :: INTERVAL;
  l_is_display_date        ibrd.row_config.is_display_date%TYPE := jofl.jofl_pkg$extract_boolean(p_attr, 'is_display_date', FALSE);
  l_is_display_time        ibrd.row_config.is_display_time%TYPE := jofl.jofl_pkg$extract_boolean(p_attr, 'is_display_time', FALSE);
  l_is_display_temperature ibrd.config.is_display_temperature%TYPE := jofl.jofl_pkg$extract_boolean(p_attr, 'is_display_temperature', FALSE);
  l_server_waiting_time    ibrd.row_config.server_waiting_time%TYPE :=  jofl.jofl_pkg$extract_number(p_attr, 'server_waiting_time', FALSE) * '1 MINUTES' :: INTERVAL;
  l_data_waiting_time      ibrd.config.data_waiting_time%TYPE :=  jofl.jofl_pkg$extract_number(p_attr, 'data_waiting_time', FALSE) * '1 MINUTES' :: INTERVAL;
  l_reboot_time            ibrd.row_config.reboot_time%TYPE :=  jofl.jofl_pkg$extract_number(p_attr, 'reboot_time', FALSE) * '1 HOURS' :: INTERVAL;
  l_max_forecast_interval ibrd.config.max_forecast_interval%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'max_forecast_interval', FALSE) * '1 MINUTES' :: INTERVAL;
  l_is_limit_forecasts_by_rows ibrd.config.is_limit_forecasts_by_rows%TYPE := jofl.jofl_pkg$extract_boolean(p_attr, 'is_limit_forecasts_by_rows', FALSE);

  l_info_board_id          ibrd.info_board.info_board_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'info_board_id', TRUE);
BEGIN
  INSERT INTO ibrd.config (is_default, running_row_speed, data_waiting_time, is_display_temperature, max_forecast_interval, is_limit_forecasts_by_rows) VALUES (
    FALSE,
    l_running_row_speed,
    l_data_waiting_time,
    l_is_display_temperature,
    l_max_forecast_interval,
    l_is_limit_forecasts_by_rows
  )
  RETURNING config_id
    INTO l_config_id;

  INSERT INTO ibrd.row_config (config_id, scroll_speed, block_rate, is_display_date, is_display_time, server_waiting_time, reboot_time) VALUES (
    l_config_id,
    l_scroll_speed,
    l_block_rate,
    l_is_display_date,
    l_is_display_time,
    l_server_waiting_time,
    l_reboot_time
  );

  IF l_info_board_id IS NOT NULL
  THEN
    DELETE FROM ibrd.info_board2config
    WHERE info_board_id = l_info_board_id;
    INSERT INTO ibrd.info_board2config (info_board_id, config_id) VALUES (l_info_board_id, l_config_id);
  END IF;

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.jf_row_config_pkg$of_update(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_config_id              ibrd.config.config_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'config_id', FALSE);

  l_running_row_speed      ibrd.config.running_row_speed%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'running_row_speed', FALSE);
  l_scroll_speed           ibrd.row_config.scroll_speed%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'scroll_speed', FALSE);
  l_block_rate             ibrd.row_config.block_rate%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'block_rate', FALSE) * '1 SECONDS' :: INTERVAL;
  l_is_display_date        ibrd.row_config.is_display_date%TYPE := jofl.jofl_pkg$extract_boolean(p_attr, 'is_display_date', FALSE);
  l_is_display_time        ibrd.row_config.is_display_time%TYPE := jofl.jofl_pkg$extract_boolean(p_attr, 'is_display_time', FALSE);
  l_is_display_temperature ibrd.config.is_display_temperature%TYPE := jofl.jofl_pkg$extract_boolean(p_attr, 'is_display_temperature', FALSE);
  l_server_waiting_time    ibrd.row_config.server_waiting_time%TYPE :=  jofl.jofl_pkg$extract_number(p_attr, 'server_waiting_time', FALSE) * '1 MINUTES' :: INTERVAL;
  l_data_waiting_time      ibrd.config.data_waiting_time%TYPE :=  jofl.jofl_pkg$extract_number(p_attr, 'data_waiting_time', FALSE) * '1 MINUTES' :: INTERVAL;
  l_reboot_time            ibrd.row_config.reboot_time%TYPE :=  jofl.jofl_pkg$extract_number(p_attr, 'reboot_time', FALSE) * '1 HOURS' :: INTERVAL;
  l_max_forecast_interval ibrd.config.max_forecast_interval%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'max_forecast_interval', FALSE) * '1 MINUTES' :: INTERVAL;
  l_is_limit_forecasts_by_rows ibrd.config.is_limit_forecasts_by_rows%TYPE := jofl.jofl_pkg$extract_boolean(p_attr, 'is_limit_forecasts_by_rows', FALSE);

BEGIN
  UPDATE ibrd.config
  SET
    running_row_speed      = l_running_row_speed,
    data_waiting_time      = l_data_waiting_time,
    is_display_temperature = l_is_display_temperature,
    max_forecast_interval = l_max_forecast_interval,
    is_limit_forecasts_by_rows = l_is_limit_forecasts_by_rows
  WHERE config_id = l_config_id;

  UPDATE ibrd.row_config
  SET
    scroll_speed        = l_scroll_speed,
    block_rate          = l_block_rate,
    is_display_date     = l_is_display_date,
    is_display_time     = l_is_display_time,
    server_waiting_time = l_server_waiting_time,
    reboot_time         = l_reboot_time
  WHERE config_id = l_config_id;

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.jf_row_config_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_config_id ibrd.config.config_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'config_id', FALSE);
BEGIN
  DELETE FROM ibrd.row_config
  WHERE config_id = l_config_id;

  DELETE FROM ibrd.config
  WHERE config_id = l_config_id;

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.jf_row_config_pkg$of_copy(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN ibrd.jf_row_config_pkg$of_insert(p_id_account, p_attr);
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.jf_row_config_pkg$of_bind(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_config_id              ibrd.config.config_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'f_config_id', FALSE);
  l_info_board_id ibrd.info_board.info_board_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'info_board_id', FALSE);
BEGIN
    DELETE FROM ibrd.info_board2config
    WHERE info_board_id = l_info_board_id;
    INSERT INTO ibrd.info_board2config (info_board_id, config_id) VALUES (l_info_board_id, l_config_id);

  RETURN NULL;
END;
$$;