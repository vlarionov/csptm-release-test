CREATE OR REPLACE FUNCTION ibrd.jf_conf_run_row_pos_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.running_row_position%rowtype;
begin 
   l_r := ibrd.jf_conf_run_row_pos_pkg$attr_to_rowtype(p_attr);

   /*update ibrd.running_row_position set 
          name = l_r.name
   where 
          runnning_row_position_id = l_r.runnning_row_position_id;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_conf_run_row_pos_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ibrd.running_row_position AS
$body$
declare 
   l_r ibrd.running_row_position%rowtype; 
begin 
   l_r.runnning_row_position_id := jofl.jofl_pkg$extract_number(p_attr, 'runnning_row_position_id', true); 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_conf_run_row_pos_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.running_row_position%rowtype;
begin 
   l_r := ibrd.jf_conf_run_row_pos_pkg$attr_to_rowtype(p_attr);

   /*delete from  ibrd.running_row_position where  runnning_row_position_id = l_r.runnning_row_position_id;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_conf_run_row_pos_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.running_row_position%rowtype;
begin 
   l_r := ibrd.jf_conf_run_row_pos_pkg$attr_to_rowtype(p_attr);

   /*insert into ibrd.running_row_position select l_r.*;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_conf_run_row_pos_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
begin 
 open p_rows for 
      select 
        runnning_row_position_id, 
        name
      from ibrd.running_row_position; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------