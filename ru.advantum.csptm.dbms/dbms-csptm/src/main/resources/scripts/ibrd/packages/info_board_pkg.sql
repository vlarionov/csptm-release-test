CREATE OR REPLACE FUNCTION ibrd.info_board_pkg$find_protocol_id(p_info_board_id ibrd.info_board.info_board_id%TYPE)
  RETURNS ibrd.ibrd_protocol.protocol_id%TYPE
LANGUAGE plpgsql
AS $$
DECLARE
  l_result ibrd.ibrd_protocol.protocol_id%TYPE;
BEGIN
  SELECT p.protocol_id
  INTO l_result
  FROM ibrd.info_board ib
    JOIN ibrd.model m ON ib.model_id = m.model_id
    JOIN ibrd.ibrd_protocol p ON m.type_id = p.type_id
  WHERE ib.info_board_id = p_info_board_id;

  RETURN l_result;
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.info_board_pkg$find_hub_id(p_info_board_id ibrd.info_board.info_board_id%TYPE)
  RETURNS core.hub.hub_id%TYPE
LANGUAGE plpgsql
AS $$
DECLARE
  l_result core.hub.hub_id%TYPE;
BEGIN
  SELECT h2ib."hubId"
  INTO l_result
  FROM ibrd.info_board_pkg$hub2ibrd() h2ib
  WHERE h2ib."unitId" = p_info_board_id;

  RETURN l_result;
END;
$$;


CREATE OR REPLACE FUNCTION ibrd.info_board_pkg$hub2ibrd()
  RETURNS SETOF core.V_HUB2UNIT
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT
    max(h.hub_id)                   "hubId",
    ib.imei                         "unitNum",
    max(ib.info_board_id :: BIGINT) "unitId"
  FROM ibrd.info_board ib
    JOIN ibrd.model m ON ib.model_id = m.model_id
    JOIN ibrd.type t ON m.type_id = t.type_id
    JOIN ibrd.ibrd_protocol p ON t.type_id = p.type_id
    JOIN core.hub h ON p.protocol_id = h.protocol_id
  WHERE NOT ib.is_deleted
  GROUP BY ib.imei;
END;
$$;

create or replace function ibrd.info_board_pkg$find_stops(
      p_info_board_id ibrd.info_board.info_board_id%type,
  out p_result        text
)
language plpgsql
as $$
begin
  select string_agg(distinct s.name, ', ')
    from ibrd.stop_location2info_board sl2ib
      join rts.stop_item si on sl2ib.stop_location_id = si.stop_location_id
      join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
      join rts.stop s on sl.stop_id = s.stop_id
    where sl2ib.info_board_id = p_info_board_id
    into p_result;
end;
$$;
