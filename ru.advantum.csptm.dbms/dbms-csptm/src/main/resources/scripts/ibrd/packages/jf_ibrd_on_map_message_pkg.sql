CREATE OR REPLACE FUNCTION ibrd.jf_ibrd_on_map_message_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                                    p_attr       TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_info_board_id ibrd.info_board.info_board_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'info_board_id',
                                                                                     FALSE);
  l_last_success_set_info_event_id ibrd.event.event_id%TYPE;
BEGIN
  l_last_success_set_info_event_id := ibrd.event_pkg$get_last_success_set_info_event_id(l_info_board_id);

  OPEN p_rows FOR
  SELECT
    e.message "text"
  FROM ibrd.event e
  WHERE e.event_id = l_last_success_set_info_event_id;
END;
$$;