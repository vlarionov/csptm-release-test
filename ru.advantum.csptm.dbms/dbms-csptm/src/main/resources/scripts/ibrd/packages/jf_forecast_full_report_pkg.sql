CREATE OR REPLACE FUNCTION ibrd.jf_forecast_full_report_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                                    p_attr       TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  f_date_from            TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'f_date_from_DT', TRUE);
  f_date_to              TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'f_date_to_DT', TRUE);
  f_info_board_id        ibrd.info_board.info_board_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'f_info_board_id', TRUE);
  l_f_round_id           rts.round.round_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'f_round_id', true);
  l_f_stop_location_id   rts.stop_location.stop_location_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_stop_location_id', true);
BEGIN
  OPEN p_rows FOR
  WITH e1 AS (
      SELECT
        e.info_board_id,
        e.event_id,
        e.forecasts_ids
      FROM ibrd.event e
      WHERE e.info_board_id = f_info_board_id
            AND e.event_date BETWEEN f_date_from AND f_date_to
            AND e.event_type_id = ibrd.event_type_pkg$set_info()
  )
  SELECT DISTINCT
    ib.info_board_id,
    ib.code,
    t.name                                       "type",
    ib.serial_number,
    si2r.order_num                               stop_order_num,
    s.name                                       stop_place_name,
    r.route_num                                  route_name,
    ibrd.forecast_pkg$get_terminal(rnd.round_id) terminal,
    ttaf.time_fact_begin                         time_fact,
    tr.garage_num                                tr_garage_num,
    f.creation_time,
    f.pass_time_forecast
  FROM e1
    JOIN ibrd.info_board ib ON e1.info_board_id = ib.info_board_id
    JOIN ibrd.event re ON e1.event_id = re.command_event_id
    JOIN ibrd.forecast f ON f.forecast_id = ANY (e1.forecasts_ids)
    join ttb.tt_action tta on f.tt_action_id = tta.tt_action_id
    join ttb.tt_action_round ttar on tta.tt_action_id = ttar.tt_action_id
    join rts.round rnd on ttar.round_id = rnd.round_id
    join rts.stop_item2round si2r on rnd.round_id = si2r.round_id and f.stop_item_id = si2r.stop_item_id and f.stop_order_num = si2r.order_num
    join rts.stop_item si on si2r.stop_item_id = si.stop_item_id
    join rts.stop_location sl on si.stop_location_id = sl.stop_location_id
    join rts.stop s on sl.stop_id = s.stop_id
    join ttb.tt_action_fact ttaf on tta.tt_action_id = ttaf.tt_action_id and si2r.stop_item2round_id = ttaf.stop_item2round_id
    join rts.route_variant rv on rnd.route_variant_id = rv.route_variant_id
    join rts.route r on rv.route_id = r.route_id
    join ttb.tt_out tto on tta.tt_out_id = tto.tt_out_id
    join core.tr tr on tto.tr_id = tr.tr_id
    JOIN ibrd.model m ON ib.model_id = m.model_id
    JOIN ibrd.type t ON m.type_id = t.type_id
  WHERE
    ib.info_board_id = f_info_board_id
    AND re.error_type_id = ibrd.error_type_pkg$null()
    AND ttaf.time_fact_begin BETWEEN f_date_from AND f_date_to
    and (l_f_stop_location_id is null or sl.stop_location_id = l_f_stop_location_id)
    and (l_f_round_id is null or rnd.round_id = l_f_round_id)
  ORDER BY code, route_name, creation_time;
END;
$$;
