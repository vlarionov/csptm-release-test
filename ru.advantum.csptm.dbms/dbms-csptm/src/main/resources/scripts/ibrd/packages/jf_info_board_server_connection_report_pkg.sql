CREATE OR REPLACE FUNCTION ibrd.jf_info_board_server_connection_report_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
DECLARE
  C_SET_INFO           INTEGER := ibrd.event_type_pkg$set_info();
  C_CONNECT            INTEGER := ibrd.event_type_pkg$connect();
  C_DISCONNECT         INTEGER := ibrd.event_type_pkg$disconnect();

  f_dtb_DT             TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'F_DT', TRUE);
  f_list_info_board_id integer [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_info_board_id', TRUE);
  l_f_stop_location_id  integer := jofl.jofl_pkg$extract_number(p_attr, 'f_stop_location_id', true);
  f_ibrd_type_type_id  BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_ibrd_type_type_id', TRUE);
  f_model_id           BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_model_id', TRUE);
  f_group_id           integer := jofl.jofl_pkg$extract_number(p_attr, 'f_group_id', TRUE);
  f_state_id           BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_state_id', TRUE);
  /*
  •	Код ИТОП – можно выбрать один или несколько ИТОП.  Если не выбрано, то по всем.
  •	Остановочный пункт – можно выбрать остановочный пункт, к которому относится посадки-высадки ИТОП;
  •	Тип ИТОП: Краснодиодное / Желтодиодное / Полноцветное – можно выбрать тип ИТОП, чтобы посмотреть список ИТОП выбранного типа (с учетом других фильтров). Если тип не выбран, то выборка осуществляется без учета типа ИТОП, т.е. по всем типам;
  •	Модель ИТОП – можно выбрать модель ИТОП, чтобы посмотреть список ИТОП выбранной модели. Если модель не выбрана, то выбора осуществляется без учета модели ИТОП;
  •	Группы ИТОП – можно выбрать группу, чтобы посмотреть сведения по ИТОП, входящим в группу.
  •	Статус ИТОП – можно выбрать статус ИТОП, чтобы посмотреть список ИТОП, которые на текущий момент времени находятся в выбранном статусе;
  */
  -- {"F_DT":"2017-08-09 13:16:31.0"}
BEGIN
  OPEN p_rows FOR
  WITH t AS (
      SELECT
        ib.info_board_id,
        ib.code,
        s.name                       stop_name,
        ib.imei,
        t.name                       type_name,
        ib.serial_number,
        m.name                       model_name,
        ib.mobile_number,
        max(CASE e.event_type_id
            WHEN C_DISCONNECT
              THEN e.event_date END) DISCONNECT,
        max(CASE e.event_type_id
            WHEN C_CONNECT
              THEN e.event_date END) CONNECT,
        max(CASE e.event_type_id
            WHEN C_SET_INFO
              THEN e.event_date END) SET_INFO,
        max(CASE WHEN e.event_type_id NOT IN (C_CONNECT, C_DISCONNECT, C_SET_INFO)
          THEN e.event_date END)     OTHERS,
        f_dtb_DT                     f_rpt_DT
      FROM ibrd.info_board ib
        JOIN ibrd.model m ON m.model_id = ib.model_id
        JOIN ibrd.type t ON t.type_id = m.type_id
        LEFT JOIN ibrd.stop_location2info_board sl2ib ON sl2ib.info_board_id = ib.info_board_id AND sl2ib.is_base
        left join rts.stop_location sl on sl2ib.stop_location_id = sl.stop_location_id
        left join rts.stop s on sl.stop_id = s.stop_id
        LEFT JOIN ibrd.event e ON e.info_board_id = ib.info_board_id AND e.event_date <= f_dtb_DT
      WHERE 1 = 1
            AND NOT ib.is_deleted
            AND (ib.info_board_id = ANY (f_list_info_board_id) OR array_length(f_list_info_board_id, 1) IS NULL)
            AND (l_f_stop_location_id is null or sl.stop_location_id = l_f_stop_location_id)
            AND (m.type_id = f_ibrd_type_type_id OR f_ibrd_type_type_id IS NULL)
            AND (m.model_id = f_model_id OR f_model_id IS NULL)
            AND (ib.state_id = f_state_id OR f_state_id IS NULL)
            AND (f_group_id is null OR ib.info_board_id IN (SELECT ib2g.info_board_id
                                                           FROM ibrd.info_board2group ib2g where ib2g.group_id = f_group_id))
      GROUP BY ib.info_board_id
        , ib.code
        , s.name
        , ib.imei
        , t.name
        , ib.serial_number
        , m.name
        , ib.mobile_number
        , ib.address
  )
  ,tt as (
  SELECT
    info_board_id,
    code,
    stop_name,
    imei,
    type_name,
    serial_number,
    model_name,
    mobile_number,
    disconnect,
    round(extract(EPOCH FROM (now() - disconnect))) disconnect_duration,
    connect,
    round(extract(EPOCH FROM (now() - connect)))    connect_duration,
    set_info,
    round(extract(EPOCH FROM (now() - set_info)))   set_info_duration,
    CASE WHEN ibrd.event_pkg$is_connected(info_board_id)
      THEN 'Подключен'
    ELSE 'Отключен' END                             is_connected
  FROM t
  --  where info_board_id=553
  )
  select  tt.info_board_id,
          tt.code,
          tt.stop_name,
          tt.imei,
          tt.type_name,
          tt.serial_number,
          tt.model_name,
          tt.mobile_number,
          tt.disconnect,
          tt.disconnect_duration,
          tt.connect,
          tt.connect_duration,
          tt.set_info,
          tt.set_info_duration,
          tt.is_connected,
          case 
          	when ibrd.event_pkg$is_connected(tt.info_board_id)
              then 8965120  /*#88cc00*/
            when tt.disconnect_duration > 900 /*15 мин*/ 
              then 16731469 /*#ff4d4d*/
            else null         
          end "ROW$COLOR"
  from tt
  ;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;