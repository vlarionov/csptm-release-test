create or replace function ibrd.jf_message_pkg$of_rows(
      p_id_account numeric,
  out p_rows       refcursor,
      p_attr       text
)
  returns refcursor
language plpgsql as $$
declare
  l_group_id      ibrd.group.group_id%type := jofl.jofl_pkg$extract_number(p_attr, 'group_id', true );
  l_info_board_id ibrd.info_board.info_board_id%type := jofl.jofl_pkg$extract_number(p_attr, 'info_board_id', true);
begin
  open p_rows for
  select
    m.message_id,
    m.info_board_id,
    m.group_id,
    m.number,
    m.msg_text,
    m.priority,
    m.is_enabled,
    m.start_date,
    m.end_date,
    ibrd.jf_message_pkg$get_dow_list(m.message_id) as dow_list,
    l_group_id
  from ibrd.message m
  where (m.group_id = l_group_id or m.info_board_id = l_info_board_id);
end;
$$;

create or replace function ibrd.jf_message_pkg$attr_to_rowtype(p_attr text)
  returns ibrd.message
language plpgsql as $$
declare
  l_r           ibrd.message%rowtype;
  l_protocol_id ibrd.ibrd_protocol.protocol_id%type;
begin
  l_r.msg_text := jofl.jofl_pkg$extract_varchar(p_attr, 'msg_text', true);
  l_r.priority := jofl.jofl_pkg$extract_number(p_attr, 'priority', true);
  l_r.is_enabled := jofl.jofl_pkg$extract_boolean(p_attr, 'is_enabled', true);
  l_r.start_date := jofl.jofl_pkg$extract_date(p_attr, 'start_date', true);
  l_r.end_date := jofl.jofl_pkg$extract_date(p_attr, 'end_date', true);

  l_protocol_id := ibrd.protocol_pkg$find_protocol_id(l_r.info_board_id);
  if not ibrd.message_pkg$is_valid_size(l_protocol_id, l_r.msg_text)
  then
    raise exception using message = 'Размер сообщения превышает максимальный(' || ibrd.message_pkg$max_size(l_protocol_id) || ' символов)!';
  end if;

  return l_r;
end;
$$;

create or replace function ibrd.jf_message_pkg$of_insert(
  p_id_account numeric,
  p_attr       text
)
  returns text
language plpgsql as $$
declare
  l_r             ibrd.message%rowtype;

  l_info_board_id ibrd.info_board.info_board_id%type := jofl.jofl_pkg$extract_number(p_attr, 'info_board_id', true);
  l_group_id      ibrd.group.group_id%type := jofl.jofl_pkg$extract_number(p_attr, 'group_id', true);

  l_message_id    ibrd.message.message_id%type := nextval(pg_get_serial_sequence('ibrd.message', 'message_id'));
begin
  l_r := ibrd.jf_message_pkg$attr_to_rowtype(p_attr);

  if l_info_board_id = -1
  then l_info_board_id = null;
  end if;
  if l_group_id = -1
  then l_group_id = null;
  end if;

  insert into ibrd.message (message_id, number, msg_text, priority, is_enabled, start_date, end_date, info_board_id, group_id)
  values (
    l_message_id,
    l_message_id || '-' || extract(year from current_date),
    l_r.msg_text,
    l_r.priority,
    l_r.is_enabled,
    l_r.start_date,
    l_r.end_date,
    l_info_board_id,
    l_group_id
  );

  return null;
end;
$$;

create or replace function ibrd.jf_message_pkg$of_update(
  p_id_account numeric,
  p_attr       text
)
  returns text
language plpgsql as $$
declare
  l_r ibrd.message%rowtype;
begin
  l_r := ibrd.jf_message_pkg$attr_to_rowtype(p_attr);

  update ibrd.message
  set
    msg_text   = l_r.msg_text,
    priority   = l_r.priority,
    is_enabled = l_r.is_enabled,
    start_date = l_r.start_date,
    end_date   = l_r.end_date
  where
    message_id = jofl.jofl_pkg$extract_number(p_attr, 'message_id', false);

  return null;
end;
$$;

create or replace function ibrd.jf_message_pkg$of_delete(p_id_account numeric, p_attr text)
  returns text
language plpgsql as $$
declare
  l_message_id ibrd.message.message_id%type := jofl.jofl_pkg$extract_number(p_attr, 'message_id', false);
begin
  delete from ibrd.message_schedule
  where message_id = l_message_id;

  delete from ibrd.message
  where message_id = l_message_id;

  return null;
end;
$$;

create or replace function ibrd.jf_message_pkg$get_dow_list(
      p_message_id ibrd.message.message_id%type,
  out p_result     text
)
language plpgsql as $$
begin
  with message_week_day as (
      select distinct on (ms.week_day) (ibrd.jf_message_schedule_dow_pkg$get_dow_list()) [ms.week_day] dow_name
      from ibrd.message_schedule ms
      where ms.message_id = p_message_id
      order by ms.week_day
  )
  select string_agg(dow_name, ' ')
  from message_week_day
  into p_result;
end;
$$;