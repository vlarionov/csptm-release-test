CREATE OR REPLACE FUNCTION ibrd.jf_info_board_history_report_pkg$of_rows(
  IN  p_id_account NUMERIC,
  OUT p_rows       REFCURSOR,
  IN  p_attr       TEXT)
  RETURNS REFCURSOR AS
$BODY$
DECLARE
  f_dtb_DT             TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'f_dtb_DT', TRUE);
  f_dte_DT             TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'f_dte_DT', TRUE);

  f_list_info_board_id BIGINT [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_info_board_id', TRUE);
  l_f_stop_location_id integer := jofl.jofl_pkg$extract_number(p_attr, 'f_stop_location_id', true);
  f_ibrd_type_type_id  BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_ibrd_type_type_id', TRUE);
  f_model_id           BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_model_id', TRUE);
  f_group_id          integer  := jofl.jofl_pkg$extract_number(p_attr, 'f_group_id', TRUE);
  f_state_id           BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_state_id', TRUE);
BEGIN
  OPEN p_rows FOR
  SELECT
    et.name       et_name,
    ec.event_date dt_e,
    ib.code       code,
    re.event_date dt_r,
    ert.name      err_name,
    ert.code      err_code
  FROM ibrd.event ec
    JOIN ibrd.info_board ib ON ib.info_board_id = ec.info_board_id
    LEFT JOIN ibrd.event_type et ON et.event_type_id = ec.event_type_id AND et.event_type_id <> 2
    JOIN ibrd.event re ON ec.event_id = re.command_event_id
    join ibrd.model m on ib.model_id = m.model_id
    join ibrd.type t on m.type_id = t.type_id
    LEFT JOIN ibrd.error_type ert
      ON ert.error_type_id = re.error_type_id AND ert.error_type_id <> ibrd.error_type_pkg$null()
  WHERE ec.event_date BETWEEN f_dtb_DT AND f_dte_DT
      AND (ib.info_board_id = ANY (f_list_info_board_id) OR array_length(f_list_info_board_id, 1) IS NULL)
        AND (f_model_id ISNULL OR ib.model_id = f_model_id)
        AND (f_state_id IS NULL OR ib.state_id = f_state_id)
        and (l_f_stop_location_id is null or ib.info_board_id in (select sl2ib.info_board_id
                                                                  from ibrd.stop_location2info_board sl2ib
                                                                  where sl2ib.stop_location_id = l_f_stop_location_id))
        AND (f_ibrd_type_type_id ISNULL OR t.type_id = f_ibrd_type_type_id)
        AND (f_group_id is null OR ib.info_board_id IN (SELECT ib2g.info_board_id
                                                           FROM ibrd.info_board2group ib2g
                                                           where ib2g.group_id = f_group_id))
  ORDER BY ec.event_date;
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
