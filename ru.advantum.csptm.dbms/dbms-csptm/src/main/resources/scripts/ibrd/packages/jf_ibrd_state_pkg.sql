CREATE OR REPLACE FUNCTION ibrd.jf_ibrd_state_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ibrd.state AS
$body$
declare 
   l_r ibrd.state%rowtype; 
begin 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 
   l_r.state_id := jofl.jofl_pkg$extract_number(p_attr, 'state_id', true); 
   l_r.is_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'is_deleted', true); 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_ibrd_state_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.state%rowtype;
begin 
   l_r := ibrd.jf_ibrd_state_pkg$attr_to_rowtype(p_attr);

   /*delete from  ibrd.state where  state_id = l_r.state_id;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_ibrd_state_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.state%rowtype;
begin 
   l_r := ibrd.jf_ibrd_state_pkg$attr_to_rowtype(p_attr);

   /*insert into ibrd.state select l_r.*;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_ibrd_state_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
  ln_ref_state_id bigint := jofl.jofl_pkg$extract_number(p_attr, 'state_id', true); 
begin 
 open p_rows for 
      select 
        name, 
        state_id, 
        is_deleted
      from ibrd.state
      where not is_deleted
      and (state_id = ln_ref_state_id or ln_ref_state_id is null); 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
-------------------------------------------------
-------------------------------------------------