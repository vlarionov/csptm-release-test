CREATE OR REPLACE FUNCTION ibrd.jf_ibrd_config_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
  ln_ref_info_board_id bigint := jofl.jofl_pkg$extract_number(p_attr, 'info_board_id', true); 
begin 
 open p_rows for 
      select ib.info_board_id
	  ,c.config_id
      ,c.running_row_speed
      ,extract(epoch from c.data_waiting_time) data_waiting_time      
      ,rc.scroll_speed as scroll_speed_rc
      ,extract(epoch from rc.block_rate) as block_rate_rc
      ,rc.is_display_date as is_display_date_rc
      ,rc.is_display_time as is_display_time_rc
      ,extract(epoch from rc.reboot_time) as reboot_time_rc
      ,extract(epoch from rc.server_waiting_time) as server_waiting_time_rc
      ,mrcst.scroll_type_id as scroll_type_id_mrc
	  ,mrcst.name as scroll_type_name_mrc
      ,mrcrrp.runnning_row_position_id as run_row_pos_id_mrc
      ,mrcrrp.name as run_row_pos_name_mrc
      ,mrcsd.scroll_direction_id as scroll_dir_id_mrc
      ,mrcsd.name as scroll_dir_name_mrc
      ,mrcfsr.font_style_id as font_route_id_mrc
	  ,mrcfsr.name as font_route_name_mrc
      ,mrcfsa.font_style_id as font_arrive_id_mrc
      ,mrcfsa.name as font_arrive_name_mrc
      ,mrcfst.font_style_id as font_terminal_id_mrc
      ,mrcfst.name as font_terminal_name_mrc
      ,mrcbl.hour as bright_hour_mrc
      ,mrcbl.value as bright_value_mrc
      ,case 
      	when rc.config_id is null then 'P{RO},D{scroll_speed_rc, block_rate_rc, is_display_date_rc, is_display_time_rc, reboot_time_rc, server_waiting_time_rc, bright_hour_mrc, data_waiting_time}' 
        when mrc.config_id is null then 'P{RO},D{scroll_type_name_mrc, run_row_pos_name_mrc, scroll_dir_name_mrc, font_route_name_mrc, font_arrive_name_mrc, font_terminal_name_mrc, bright_hour_mrc, bright_value_mrc, data_waiting_time}' 
       end "ROW$POLICY"	
		 from ibrd.info_board ib
         join ibrd.model ibm on ibm.model_id = ib.model_id
         join ibrd.type ibt on ibt.type_id = ibm.type_id
         join ibrd.info_board2config ib2c on ib2c.info_board_id = ib.info_board_id
		 join ibrd.config c on c.config_id = ib2c.config_id
         left join ibrd.row_config rc on rc.config_id = ib2c.config_id
         left join ibrd.mono_rgb_config mrc on mrc.config_id = ib2c.config_id
         left join ibrd.scroll_type mrcst on mrcst.scroll_type_id = mrc.scroll_type_id
         left join ibrd.running_row_position mrcrrp on mrcrrp.runnning_row_position_id = mrc.running_row_position_id
         left join ibrd.scroll_direction mrcsd on mrcsd.scroll_direction_id = mrc.scroll_direction_id
         left join ibrd.font_style mrcfsr on mrcfsr.font_style_id = mrc.route_number_style_id
         left join ibrd.font_style mrcfsa on mrcfsa.font_style_id = mrc.arrival_time_style_id
         left join ibrd.font_style mrcfst on mrcfst.font_style_id = mrc.terminal_style_id
         left join ibrd.brightness_level mrcbl on mrcbl.config_id = mrc.config_id
      where (ib.info_board_id = ln_ref_info_board_id or ln_ref_info_board_id is null);
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-----------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_ibrd_config_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
  ln_ref_info_board_id bigint			:= jofl.jofl_pkg$extract_number(p_attr, 'info_board_id', true); 
  ln_ref_config_id bigint				:= jofl.jofl_pkg$extract_number(p_attr, 'config_id', true); 
  ln_ref_running_row_speed smallint		:= jofl.jofl_pkg$extract_number(p_attr, 'running_row_speed', true); 
  ln_ref_data_waiting_time bigint		:= jofl.jofl_pkg$extract_number(p_attr, 'data_waiting_time', true); 
  ln_ref_scroll_speed_rc smallint		:= jofl.jofl_pkg$extract_number(p_attr, 'scroll_speed_rc', true); 
  ln_ref_block_rate_rc bigint			:= jofl.jofl_pkg$extract_number(p_attr, 'block_rate_rc', true); 
  ln_ref_is_display_date_rc boolean		:= jofl.jofl_pkg$extract_boolean(p_attr, 'is_display_date_rc', true); 
  ln_ref_is_display_time_rc boolean		:= jofl.jofl_pkg$extract_boolean(p_attr, 'is_display_time_rc', true); 
  ln_ref_reboot_time_rc bigint			:= jofl.jofl_pkg$extract_number(p_attr, 'reboot_time_rc', true); 
  ln_ref_server_waiting_time_rc bigint  := jofl.jofl_pkg$extract_number(p_attr, 'server_waiting_time_rc', true); 
  ln_ref_scroll_type_id_mrc bigint		:= jofl.jofl_pkg$extract_number(p_attr, 'scroll_type_id_mrc', true); 
  ln_ref_scroll_type_name_mrc text;
  ln_ref_run_row_pos_id_mrc bigint		:= jofl.jofl_pkg$extract_number(p_attr, 'run_row_pos_id_mrc', true); 
  ln_ref_run_row_pos_name_mrc text;
  ln_ref_scroll_dir_id_mrc bigint		:= jofl.jofl_pkg$extract_number(p_attr, 'scroll_dir_id_mrc', true); 
  ln_ref_scroll_dir_name_mrc text;
  ln_ref_font_route_id_mrc bigint		:= jofl.jofl_pkg$extract_number(p_attr, 'font_route_id_mrc', true); 
  ln_ref_font_route_name_mrc text;
  ln_ref_font_arrive_id_mrc bigint		:= jofl.jofl_pkg$extract_number(p_attr, 'font_arrive_id_mrc', true); 
  ln_ref_font_arrive_name_mrc text;
  ln_ref_font_terminal_id_mrc bigint	:= jofl.jofl_pkg$extract_number(p_attr, 'font_terminal_id_mrc', true); 
  ln_ref_font_terminal_name_mrc text;
  ln_ref_bright_hour_mrc smallint		:= jofl.jofl_pkg$extract_number(p_attr, 'bright_hour_mrc', true); 
  ln_ref_bright_value_mrc smallint		:= jofl.jofl_pkg$extract_number(p_attr, 'bright_value_mrc', true); 
begin 
   update ibrd.row_config 
      set scroll_speed = ln_ref_scroll_speed_rc
         ,block_rate = to_timestamp(ln_ref_block_rate_rc)::time::interval
         ,is_display_date = ln_ref_is_display_date_rc
         ,is_display_time = ln_ref_is_display_time_rc
         ,server_waiting_time = to_timestamp(ln_ref_server_waiting_time_rc)::time::interval
         ,reboot_time = to_timestamp(ln_ref_reboot_time_rc)::time::interval
   where config_id = ln_ref_config_id;
   
   update ibrd.mono_rgb_config
      set running_row_position_id = ln_ref_run_row_pos_id_mrc
         ,scroll_type_id = ln_ref_scroll_type_id_mrc
         ,scroll_direction_id = ln_ref_scroll_dir_id_mrc
         ,route_number_style_id = ln_ref_font_route_id_mrc
         ,arrival_time_style_id = ln_ref_font_arrive_id_mrc
         ,terminal_style_id = ln_ref_font_terminal_id_mrc         
   where config_id = ln_ref_config_id;   
   
   update ibrd.brightness_level
      set value = ln_ref_bright_value_mrc
   where config_id = ln_ref_config_id
     and hour = ln_ref_bright_hour_mrc; 
    
   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;