create or replace function ibrd.jf_config_ibrd_pkg$of_rows(
      p_id_account numeric,
  out p_rows       refcursor,
      p_attr       text
)
  returns refcursor
language plpgsql as $$
declare
  l_config_id ibrd.config.config_id%type := jofl.jofl_pkg$extract_number(p_attr, 'config_id', false);
begin
  open p_rows for
  select distinct
    ib2c.config_id,
    ib.info_board_id,
    ib.code,
    '[' || json_build_object('CAPTION', ib.code, 'JUMP',
                             json_build_object('METHOD', 'ibrd.info_board', 'ATTR', '{}')) || ']' code_link,
    t.name                                                                                        type_name,
    m.name                                                                                        model_name,
    ib.mobile_number,
    ibrd.info_board_pkg$find_stops(ib.info_board_id)                                              stops,
    ibrd.jf_group_ibrd_pkg$get_routes(ib.info_board_id)                                           routes,
    ip.name                                                                                       installation_place_name,
    st_x(sl.wkt_geom) as                                                                          lon,
    st_y(sl.wkt_geom) as                                                                          lat,
    s.name            as                                                                          status_name
  from
    ibrd.info_board2config ib2c
    join ibrd.info_board ib on ib2c.info_board_id = ib.info_board_id
    left join ibrd.state s on s.state_id = ib.state_id
    left join ibrd.model m on m.model_id = ib.model_id
    left join ibrd.type t on t.type_id = m.type_id
    left join ibrd.manufacturer mf on mf.manufacturer_id = m.manufacturer_id
    left join ibrd.balance_organization bo on bo.balance_organization_id = ib.balance_organization_id
    left join ibrd.mobile_operator mo on mo.mobile_operator_id = ib.mobile_operator_id
    left join ibrd.address_program ap on ap.address_program_id = ib.address_program_id
    left join ibrd.installation_place ip on ip.installation_place_id = ib.installation_place_id
    left join ibrd.service_contract sc on sc.service_contract_id = ib.service_contract_id
    left join ibrd.service_organization so on so.service_organization_id = sc.service_organization_id
    left join ibrd.stop_location2info_board sl2ib on ib.info_board_id = sl2ib.info_board_id and sl2ib.is_base
    left join rts.stop_location sl on sl2ib.stop_location_id = sl.stop_location_id
  where ib2c.config_id = l_config_id
        and not ib.is_deleted;
end;
$$;