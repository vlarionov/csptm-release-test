create or replace function ibrd.jf_message_schedule_pkg$of_rows(
      p_id_account numeric,
  out p_rows       refcursor,
      p_attr       text
)
  returns refcursor
language plpgsql as $$
declare
  l_message_id bigint := jofl.jofl_pkg$extract_number(p_attr, 'message_id', false);
begin
  open p_rows for
  select
    ms.message_id,
    ms.message_schedule_id,
    ms.week_day                                                     week_day_number,
    (ibrd.jf_message_schedule_dow_pkg$get_dow_list()) [ms.week_day] week_day_name,
    extract(epoch from ms.start_time) as                            start_time,
    extract(epoch from ms.end_time)   as                            end_time
  from ibrd.message_schedule ms
  where ms.message_id = l_message_id;
end;
$$;

create or replace function ibrd.jf_message_schedule_pkg$attr_to_rowtype(p_attr text)
  returns ibrd.message_schedule
language plpgsql as $$
declare
  l_r ibrd.message_schedule%rowtype;
begin
  l_r.week_day := jofl.jofl_pkg$extract_number(p_attr, 'week_day_number', true);
  l_r.start_time := to_timestamp(jofl.jofl_pkg$extract_number(p_attr, 'start_time', true))::time;
  l_r.end_time := to_timestamp(jofl.jofl_pkg$extract_number(p_attr, 'end_time', true))::time;

  return l_r;
end;
$$;

create or replace function ibrd.jf_message_schedule_pkg$of_insert(
  p_id_account numeric,
  p_attr       text
)
  returns text
language plpgsql as $$
declare
  l_r          ibrd.message_schedule%rowtype;

  l_message_id bigint := jofl.jofl_pkg$extract_number(p_attr, 'message_id', false);
begin
  l_r := ibrd.jf_message_schedule_pkg$attr_to_rowtype(p_attr);

  insert into ibrd.message_schedule (week_day, start_time, end_time, message_id)
  values (
    l_r.week_day,
    l_r.start_time,
    l_r.end_time,
    l_message_id
  );

  return null;
end;
$$;

create or replace function ibrd.jf_message_schedule_pkg$of_update(
  p_id_account numeric,
  p_attr       text
)
  returns text
language plpgsql as $$
declare
  l_r          ibrd.message_schedule%rowtype;
begin
  l_r := ibrd.jf_message_schedule_pkg$attr_to_rowtype(p_attr);

  update ibrd.message_schedule
  set
    week_day   = l_r.week_day,
    start_time = l_r.start_time,
    end_time   = l_r.end_time
  where
    message_schedule_id = jofl.jofl_pkg$extract_number(p_attr, 'message_schedule_id', false);

  return null;
end;
$$;


create or replace function ibrd.jf_message_schedule_pkg$of_delete(
  p_id_account numeric,
  p_attr       text
)
  returns text
language plpgsql as $$
begin
  delete from ibrd.message_schedule
  where message_schedule_id = jofl.jofl_pkg$extract_number(p_attr, 'message_schedule_id', false);

  return null;
end;
$$;