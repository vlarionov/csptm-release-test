CREATE OR REPLACE FUNCTION ibrd.jf_ibrd_on_map_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                           p_attr       TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_f_info_boards_ids INTEGER [];
BEGIN
  l_f_info_boards_ids := jofl.jofl_pkg$extract_narray(p_attr, 'f_info_boards_ids', TRUE);

  OPEN p_rows FOR
  SELECT
    ib.info_board_id,
    ib.code,
    t.name                                                         "type",
    m.name                                                         model,
    s.name                                                         status,
    ib.imei,
    ib.mobile_number,
    mo.name                                                        mobile_operator,
    ibrd.info_board_pkg$find_stops(ib.info_board_id)               stops,
    st_x(sl.wkt_geom)                                                  lon,
    st_y(sl.wkt_geom)                                                  lat,
    CASE WHEN ibrd.event_pkg$is_connected(ib.info_board_id)
      THEN 'Подключено'
    ELSE 'Отключено' END                                           connection_state,
    (SELECT lc.event_date
     FROM ibrd.event_pkg$get_last_connection(ib.info_board_id) lc) last_connection_time
  FROM ibrd.info_board ib
    JOIN ibrd.stop_location2info_board sl2ib ON ib.info_board_id = sl2ib.info_board_id
    JOIN rts.stop_location sl ON sl2ib.stop_location_id = sl.stop_location_id
    LEFT JOIN ibrd.model m ON ib.model_id = m.model_id
    LEFT JOIN ibrd.type t ON m.type_id = t.type_id
    LEFT JOIN ibrd.state s ON ib.state_id = s.state_id
    LEFT JOIN ibrd.mobile_operator mo ON ib.mobile_operator_id = mo.mobile_operator_id
  WHERE NOT ib.is_deleted
        AND (l_f_info_boards_ids IS NULL OR ib.info_board_id = ANY (l_f_info_boards_ids))
        AND sl2ib.is_base;
END;
$$;

