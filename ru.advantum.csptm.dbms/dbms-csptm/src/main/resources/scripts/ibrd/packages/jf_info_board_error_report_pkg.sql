CREATE OR REPLACE FUNCTION ibrd.jf_info_board_error_report_pkg$of_rows(
  IN  p_id_account NUMERIC,
  OUT p_rows       REFCURSOR,
  IN  p_attr       TEXT)
  RETURNS REFCURSOR AS
$BODY$
DECLARE
  C_SET_INFO           INTEGER := ibrd.event_type_pkg$set_info();
  C_CONNECT            INTEGER := ibrd.event_type_pkg$connect();
  C_DISCONNECT         INTEGER := ibrd.event_type_pkg$disconnect();

  f_dtb_DT             TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'f_dtb_DT', TRUE);
  f_dte_DT             TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'f_dte_DT', TRUE);

  f_list_info_board_id BIGINT [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_info_board_id', TRUE);
  l_f_stop_location_id  integer := jofl.jofl_pkg$extract_number(p_attr, 'f_stop_location_id', true);
  f_ibrd_type_type_id  BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_ibrd_type_type_id', TRUE);
  f_model_id           BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_model_id', TRUE);
  f_group_id           integer := jofl.jofl_pkg$extract_number(p_attr, 'f_group_id', TRUE);
  f_state_id           BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_state_id', TRUE);

  /*
  •	Период с … по … - период, в который были получены пакеты данных с ошибками. По умолчанию текущие сутки;
  •	Код ИТОП – можно выбрать один или несколько ИТОП;
  •	Остановочный пункт – можно выбрать остановочный пункт, к которому относится базовое место посадки-высадки ИТОП;
  •	Тип ИТОП: Краснодиодное / Желтодиодное / Полноцветное – можно выбрать тип ИТОП, чтобы посмотреть список ИТОП выбранного типа (с учетом других фильтров). Если тип не выбран, то выборка осуществляется без учета типа ИТОП, т.е. по всем типам;
  •	Модель ИТОП – можно выбрать модель ИТОП, чтобы посмотреть список ИТОП выбранной модели. Если модель не выбрана, то выбора осуществляется без учета модели ИТОП;
  •	Группа ИТОП – можно выбрать группу, чтобы посмотреть сведения по ИТОП, входящим в группу.
  •	Статус ИТОП – можно выбрать статус ИТОП, чтобы посмотреть список ИТОП, которые на текущий момент времени находятся в выбранном статусе;
  */
BEGIN
  OPEN p_rows FOR
  WITH t AS (

      SELECT
        ib.info_board_id,
        ib.code,
        s.name     stop_name,
        ib.imei,
        t.name     type_name,
        ib.serial_number,
        m.name     model_name,
        ib.mobile_number,
        e.event_date,
        et.code    et_code,
        et.name    et_name,
    CASE WHEN connect_date > disconnect_date
      THEN 'Подключен'
    ELSE 'Отключен' END                              is_connected


      FROM ibrd.info_board ib
        JOIN ibrd.model m ON m.model_id = ib.model_id
        JOIN ibrd.type t ON t.type_id = m.type_id
        LEFT JOIN ibrd.stop_location2info_board sl2ib ON sl2ib.info_board_id = ib.info_board_id AND sl2ib.is_base
        left join rts.stop_location sl on sl2ib.stop_location_id = sl.stop_location_id
        left join rts.stop s on sl.stop_id = s.stop_id
        LEFT JOIN ibrd.event e ON e.info_board_id = ib.info_board_id
        JOIN ibrd.event re ON re.command_event_id = e.event_id AND  re.error_type_id >  ibrd.error_type_pkg$null()
        JOIN ibrd.error_type et ON et.error_type_id = re.error_type_id
        left join (
                select max(c_e.event_date) connect_date, c_e.info_board_id from ibrd.event c_e
                where c_e.event_type_id = C_CONNECT
                  and c_e.event_date < f_dte_DT
                group by c_e.info_board_id
                  ) c_e on c_e.info_board_id=ib.info_board_id
        left join (
                select max(d_e.event_date) disconnect_date, d_e.info_board_id from ibrd.event d_e
                where d_e.event_type_id = C_DISCONNECT
                  and d_e.event_date < f_dte_DT
                group by d_e.info_board_id
                  ) d_e on d_e.info_board_id=ib.info_board_id

      WHERE 1 = 1
            AND e.event_date BETWEEN f_dtb_DT AND f_dte_DT
            AND (ib.info_board_id = ANY (f_list_info_board_id) OR array_length(f_list_info_board_id, 1) IS NULL)
            AND (l_f_stop_location_id is null or sl.stop_location_id = l_f_stop_location_id)
            AND (m.type_id = f_ibrd_type_type_id OR f_ibrd_type_type_id IS NULL)
            AND (m.model_id = f_model_id OR f_model_id IS NULL)
            AND (ib.state_id = f_state_id OR f_state_id IS NULL)
            AND (f_group_id is null OR ib.info_board_id IN (SELECT ib.info_board_id
                                                           FROM ibrd.info_board2group ib2g
                                                           where ib2g.group_id = f_group_id))
  )
  SELECT
    t.info_board_id,
    t.code,
    t.stop_name,
    t.imei,
    t.type_name,
    t.serial_number,
    t.model_name,
    t.mobile_number,
    t.event_date,
    t.et_code,
    t.et_name AS et_description,
    t.is_connected

  FROM t;
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

