CREATE OR REPLACE FUNCTION ibrd.jf_message_report_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                              p_attr       TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$DECLARE
  f_date_from            TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'f_date_from_DT', TRUE);
  f_date_to              TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'f_date_to_DT', TRUE);
  f_info_board_id        ibrd.info_board.info_board_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'f_info_board_id', TRUE);
  l_f_stop_location_id   integer := jofl.jofl_pkg$extract_number(p_attr, 'f_stop_location_id', true);
BEGIN
  OPEN p_rows FOR
  WITH e1 AS (
      SELECT
        e.event_id      event_id,
        e.message       msg_text,
        e.event_date    event_date,
        e.info_board_id info_board_id
      FROM ibrd.event AS e
        JOIN ibrd.event re ON e.event_id = re.command_event_id
      WHERE e.event_date BETWEEN f_date_from AND f_date_to
            AND (f_info_board_id is null or e.info_board_id = f_info_board_id)
            AND e.event_type_id = ibrd.event_type_pkg$set_info()
            AND re.error_type_id = ibrd.error_type_pkg$null()
  )
    , e2 AS (
      SELECT
        event_id,
        info_board_id,
        msg_text,
        event_date,
        lead(msg_text)
        OVER (
          PARTITION BY info_board_id
          ORDER BY event_date DESC ) next_text
      FROM e1
      ORDER BY event_date DESC
  )
  SELECT
    code,
    t.name                     "type",
    serial_number,
    s.name                     stop_place_name,
    msg_text,
    e2.event_date              from_date,
    lead(e2.event_date)
    OVER (
      ORDER BY e2.event_date ) to_date
  FROM e2
    JOIN ibrd.info_board ib ON e2.info_board_id = ib.info_board_id
    JOIN ibrd.model m ON ib.model_id = m.model_id
    JOIN ibrd.type t ON m.type_id = t.type_id
    LEFT JOIN ibrd.stop_location2info_board sl2ib ON sl2ib.info_board_id = ib.info_board_id AND sl2ib.is_base
    left join rts.stop_location sl on sl2ib.stop_location_id = sl.stop_location_id
    left join rts.stop s on sl.stop_id = s.stop_id
  WHERE (msg_text NOTNULL AND next_text ISNULL
        OR msg_text ISNULL AND next_text NOTNULL
        OR msg_text <> next_text)
        and msg_text <> '' and msg_text is not null --02.04.2018--Абдуллин Т.Г. - Задача 23493 убирает все строчки с пустым полем текста сообщения.
    and (l_f_stop_location_id is null or sl.stop_location_id = l_f_stop_location_id)
  ORDER BY event_date DESC;

END;
$$;