CREATE OR REPLACE FUNCTION ibrd.jf_address_program_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ibrd.address_program AS
$body$
declare 
   l_r ibrd.address_program%rowtype; 
begin 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 
   l_r.address_program_id := jofl.jofl_pkg$extract_number(p_attr, 'address_program_id', true); 
   l_r.is_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'is_deleted', true); 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_address_program_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.address_program%rowtype;
begin 
   l_r := ibrd.jf_address_program_pkg$attr_to_rowtype(p_attr);

   /*delete from  ibrd.address_program where  address_program_id = l_r.address_program_id;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_address_program_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.address_program%rowtype;
begin 
   l_r := ibrd.jf_address_program_pkg$attr_to_rowtype(p_attr);

   /*insert into ibrd.address_program select l_r.*;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_address_program_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
begin 
 open p_rows for 
      select 
        name, 
        address_program_id, 
        is_deleted
      from ibrd.address_program
     where not is_deleted; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_address_program_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.address_program%rowtype;
begin 
   l_r := ibrd.jf_address_program_pkg$attr_to_rowtype(p_attr);

   /*update ibrd.address_program set 
          name = l_r.name, 
          is_deleted = l_r.is_deleted
   where 
          address_program_id = l_r.address_program_id;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
-------------------------------------------------