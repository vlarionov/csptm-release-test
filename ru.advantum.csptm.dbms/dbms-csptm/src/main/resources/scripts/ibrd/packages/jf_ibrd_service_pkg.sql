CREATE OR REPLACE FUNCTION ibrd.jf_ibrd_service_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 ln_ref_service_contract_id bigint := jofl.jofl_pkg$extract_number(p_attr, 'service_contract_id', true); 
begin 
 open p_rows for 
      select 
        sc.service_contract_id, 
        sc.number, 
        sc.contract_date, 
        sc.service_organization_id, 
        sor.name as ser_org_name,
        sc.start_date, 
        sc.end_date
      from ibrd.service_contract sc 
      join ibrd.service_organization sor on sor.service_organization_id = sc.service_organization_id
      where sc.service_contract_id = ln_ref_service_contract_id; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;