CREATE OR REPLACE FUNCTION ibrd.config_pkg$find_row_config(p_info_board_id ibrd.info_board.info_board_id%TYPE)
  RETURNS TABLE(
    is_display_temperature     BOOLEAN,
    running_row_speed          SMALLINT,
    data_waiting_time          INTERVAL,
    max_forecast_interval      INTERVAL,
    is_limit_forecasts_by_rows BOOLEAN,
    is_display_date            BOOLEAN,
    is_display_time            BOOLEAN,
    scroll_speed               SMALLINT,
    block_rate                 INTERVAL,
    server_waiting_time        INTERVAL,
    reboot_time                INTERVAL
  )
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT
    conf.is_display_temperature,
    conf.running_row_speed,
    conf.data_waiting_time,
    conf.max_forecast_interval,
    conf.is_limit_forecasts_by_rows,
    rconf.is_display_date,
    rconf.is_display_time,
    rconf.scroll_speed,
    rconf.block_rate,
    rconf.server_waiting_time,
    rconf.reboot_time
  FROM ibrd.info_board2config ib2c
    JOIN ibrd.config conf ON ib2c.config_id = conf.config_id
    JOIN ibrd.row_config rconf ON conf.config_id = rconf.config_id
  WHERE ib2c.info_board_id = p_info_board_id;
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.config_pkg$find_mono_rgb_config(p_info_board_id ibrd.info_board.info_board_id%TYPE)
  RETURNS TABLE(
    is_display_temperature  BOOLEAN,
    running_row_speed       SMALLINT,
    data_waiting_time       INTERVAL,
    max_forecast_interval      INTERVAL,
    is_limit_forecasts_by_rows BOOLEAN,
    is_display_datetime     BOOLEAN,
    running_row_position_id INTEGER,
    scroll_type_id          INTEGER,
    scroll_direction_id     INTEGER,
    rows_number             SMALLINT,
    route_number_style_id   INTEGER,
    arrival_time_style_id   INTEGER,
    terminal_style_id       INTEGER,
    forecast_delay          INTERVAL,
    brightness_levels       JSON
  )
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT
    conf.is_display_temperature,
    conf.running_row_speed,
    conf.data_waiting_time,
    conf.max_forecast_interval,
    conf.is_limit_forecasts_by_rows,
    mrconf.is_display_datetime,
    mrconf.running_row_position_id,
    mrconf.scroll_type_id,
    mrconf.scroll_direction_id,
    mrconf.rows_number,
    mrconf.route_number_style_id,
    mrconf.arrival_time_style_id,
    mrconf.terminal_style_id,
    mrconf.forecast_delay,
    coalesce(
        (SELECT array_to_json(array_agg(row_to_json(bl)))
         FROM (SELECT
                 bl.hour,
                 bl.value
               FROM ibrd.brightness_level bl
               WHERE bl.config_id = 2
              ) bl
        ),
        '[]' :: JSON
    ) brightness_levels
  FROM ibrd.info_board2config ib2c
    JOIN ibrd.config conf ON ib2c.config_id = conf.config_id
    JOIN ibrd.mono_rgb_config mrconf ON conf.config_id = mrconf.config_id
  WHERE ib2c.info_board_id = p_info_board_id;
END;
$$;