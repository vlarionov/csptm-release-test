create or replace function ibrd.jf_ibrd_group_pkg$of_rows(
      p_id_account numeric,
  out p_rows       refcursor,
      p_attr       text
)
  returns refcursor
language plpgsql as $$
declare
  l_info_board_id ibrd.info_board.info_board_id%type := jofl.jofl_pkg$extract_number(p_attr, 'info_board_id', true);
begin
  open p_rows for
  select
    g.group_id,
    ib2g.info_board_id,
    g.name                 group_name,
    g.route_id is not null is_auto,
    ib2g.relating_date
  from ibrd.info_board2group ib2g
    join ibrd.group g on ib2g.group_id = g.group_id
  where (l_info_board_id is null or ib2g.info_board_id = l_info_board_id);
end;
$$; 