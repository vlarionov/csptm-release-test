CREATE OR REPLACE FUNCTION ibrd.protocol_pkg$row_protocol_id()
  RETURNS ibrd.ibrd_protocol.protocol_id%TYPE
IMMUTABLE
LANGUAGE plpgsql
AS $$
DECLARE
BEGIN
  RETURN 8;
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.protocol_pkg$mono_protocol_id()
  RETURNS ibrd.ibrd_protocol.protocol_id%TYPE
IMMUTABLE
LANGUAGE plpgsql
AS $$
DECLARE
BEGIN
  RETURN 9;
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.protocol_pkg$rgb_protocol_id()
  RETURNS ibrd.ibrd_protocol.protocol_id%TYPE
IMMUTABLE
LANGUAGE plpgsql
AS $$
DECLARE
BEGIN
  RETURN 10;
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.protocol_pkg$find_protocol_id(p_info_board_id ibrd.info_board.info_board_id%TYPE)
  RETURNS ibrd.ibrd_protocol.protocol_id%TYPE
LANGUAGE plpgsql
AS $$
DECLARE
  l_result ibrd.ibrd_protocol.protocol_id%TYPE;
BEGIN
  SELECT p.protocol_id
  INTO l_result
  FROM ibrd.info_board ib
    JOIN ibrd.model m ON ib.model_id = m.model_id
    JOIN ibrd.type t ON m.type_id = t.type_id
    JOIN ibrd.ibrd_protocol p ON t.type_id = p.type_id
  WHERE ib.info_board_id = p_info_board_id;

  RETURN l_result;
END;
$$;