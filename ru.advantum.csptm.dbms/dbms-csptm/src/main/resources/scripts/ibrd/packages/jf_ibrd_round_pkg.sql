create or replace function ibrd.jf_ibrd_round_pkg$of_rows(
  in  p_id_account numeric,
  out p_rows       refcursor,
  in  p_attr       text
)
  returns refcursor
language plpgsql as $$
declare
  l_info_board_id ibrd.info_board.info_board_id%type := jofl.jofl_pkg$extract_number(p_attr, 'info_board_id', true );

   l_rf_db_method  text := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true);
begin
  if l_rf_db_method = 'ibrd.forecast_full_report'
  then l_info_board_id = jofl.jofl_pkg$extract_number(p_attr, 'f_info_board_id', true);
  end if;

  open p_rows for
  select
    sl2ib.info_board_id,
    rnd.round_id,
    r.route_id,
    si2r.order_num,
    s.name                                                                                                                     stop_name,
    r.route_num,
    trt.name                                                                                                                   tr_type_name,
    md.move_direction_name,
    last_s.name                                                                                                                last_stop_name,
    ibrd.forecast_pkg$is_show(sl2ib.info_board_id, rnd.round_id)                                                               is_show_forecast,
    lf.garage_num                                                                                                              tr_garage_num,
    rnd.code                                                                                                                   round_code,
    to_char(lf.pass_time_forecast + '3 hours' :: interval, 'hh24:mi') || ' (' ||
    (extract(epoch from greatest(lf.pass_time_forecast - current_timestamp, interval '0 secs')) / 60) :: integer || ' мин)' as current_forecast
  from ibrd.stop_location2info_board sl2ib
    join rts.stop_location sl on sl2ib.stop_location_id = sl.stop_location_id
    join rts.stop s on sl.stop_id = s.stop_id
    join rts.stop_item si on sl.stop_location_id = si.stop_location_id
    join core.tr_type trt on si.tr_type_id = trt.tr_type_id
    join rts.stop_item2round si2r on si.stop_item_id = si2r.stop_item_id
    join rts.round rnd on si2r.round_id = rnd.round_id
    join rts.move_direction md on rnd.move_direction_id = md.move_direction_id
    join rts.route_variant rv on rnd.route_variant_id = rv.route_variant_id
    join rts.route r on rv.route_id = r.route_id and rv.route_variant_id = r.current_route_variant_id
    join rts.stop_item last_si on rts.round_pkg$get_last_stop_item_id(rnd.round_id) = last_si.stop_item_id
    join rts.stop_location last_sl on last_si.stop_location_id = last_sl.stop_location_id
    join rts.stop last_s on last_sl.stop_id = last_s.stop_id
    left join ibrd.forecast_pkg$get_last_forecast(l_info_board_id, rnd.round_id, si.stop_item_id) lf on true
  where sl2ib.info_board_id = l_info_board_id
        and not sl.sign_deleted
        and not s.sign_deleted
        and not si2r.sign_deleted
        and not rnd.sign_deleted
        and not rv.sign_deleted
  order by lf.pass_time_forecast, r.route_num, order_num;
end;
$$;

create or replace function ibrd.jf_ibrd_round_pkg$of_update(p_id_account numeric, p_attr text)
  returns void
language plpgsql
as $$
declare
  l_info_board_id           ibrd.info_board.info_board_id%type := jofl.jofl_pkg$extract_number(p_attr, 'info_board_id', false);
  l_round_id                rts.round.round_id%type := jofl.jofl_pkg$extract_number(p_attr, 'round_id', false);
  l_is_show_forecast        boolean := jofl.jofl_pkg$extract_boolean(p_attr, 'is_show_forecast', false);

  l_is_show_forecast_before boolean;
begin
  l_is_show_forecast_before := ibrd.forecast_pkg$is_show(l_info_board_id, l_round_id);

  if l_is_show_forecast and not l_is_show_forecast_before
  then
    delete from ibrd.disabled_round4forecast
    where info_board_id = l_info_board_id and round_id = l_round_id;
  elseif not l_is_show_forecast and l_is_show_forecast_before
    then
      insert into ibrd.disabled_round4forecast (info_board_id, round_id)
      values (l_info_board_id, l_round_id);
  end if;
end;
$$;
