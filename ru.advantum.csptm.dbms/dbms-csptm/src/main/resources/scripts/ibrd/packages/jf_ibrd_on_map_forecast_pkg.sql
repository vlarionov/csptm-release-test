create or replace function ibrd.jf_ibrd_on_map_forecast_pkg$of_rows(p_id_account numeric, out p_rows refcursor,
                                                                    p_attr       text)
  returns refcursor
language plpgsql
as $$
declare
  l_info_board_id                  ibrd.info_board.info_board_id%type := jofl.jofl_pkg$extract_number(p_attr, 'info_board_id',
                                                                                                      false);
  l_last_success_set_info_event_id ibrd.event.event_id%type;
begin
  l_last_success_set_info_event_id := ibrd.event_pkg$get_last_success_set_info_event_id(l_info_board_id);

  open p_rows for
  select
    r.route_num                                                                                                                route,
    ibrd.forecast_pkg$get_terminal(ttar.round_id)                                                                              terminal,
    to_char(f.pass_time_forecast + '3 HOURS' :: interval, 'HH24:MI') || ' (' ||
    (extract(epoch from greatest(f.pass_time_forecast - response_e.event_date, interval '0 secs')) / 60) :: integer || ' мин)' arrival_time,
    tr.garage_num                                                                                                              tr
  from ibrd.event e
    join ibrd.event response_e on response_e.command_event_id = e.event_id
    join ibrd.forecast f on f.forecast_id = any (e.forecasts_ids)
    join ttb.tt_action tta on f.tt_action_id = tta.tt_action_id
    join ttb.tt_action_round ttar on tta.tt_action_id = ttar.tt_action_id
    join rts.round rnd on ttar.round_id = rnd.round_id
    join rts.route_variant rv on rnd.route_variant_id = rv.route_variant_id
    join rts.route r on rv.route_id = r.route_id
    join ttb.tt_out tto on tta.tt_out_id = tto.tt_out_id
    join core.tr tr on tto.tr_id = tr.tr_id
  where e.event_id = l_last_success_set_info_event_id;
end;
$$;