create or replace function ibrd.jf_message_schedule_dow_pkg$of_rows(
      p_id_account numeric,
  out p_rows       refcursor,
      p_attr       text
)
  returns refcursor

language plpgsql as $$
begin
  open p_rows for
  select
    ordinality week_day_number,
    wd         week_day_name
  from unnest(ibrd.jf_message_schedule_dow_pkg$get_dow_list()) with ordinality wd;
end;
$$;

create or replace function ibrd.jf_message_schedule_dow_pkg$get_dow_list()
  returns text []
immutable
language plpgsql as $$
begin
  return array ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВС'];
end;
$$;