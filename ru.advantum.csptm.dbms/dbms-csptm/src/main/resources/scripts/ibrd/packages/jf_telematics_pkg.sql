create or replace function ibrd.jf_telematics_pkg$of_rows(p_id_account numeric, out p_rows refcursor,
                                                          p_attr       text)
  returns refcursor
language plpgsql
as $$
declare
  l_info_board_id ibrd.info_board.info_board_id%type := jofl.jofl_pkg$extract_number(p_attr, 'info_board_id', false);
begin
  open p_rows for
  select
    e.info_board_id,
    e.event_date,
    e.lon,
    e.lat,
    st_x(sl.wkt_geom) gis_lon,
    st_y(sl.wkt_geom) gis_lat
  from ibrd.event e
    left join ibrd.stop_location2info_board sl2ib on e.info_board_id = sl2ib.info_board_id and sl2ib.is_base
    left join rts.stop_location sl on sl.stop_location_id = sl2ib.stop_location_id
  where e.info_board_id = l_info_board_id
        and e.event_type_id = ibrd.event_type_pkg$telematics_response();
end;
$$;