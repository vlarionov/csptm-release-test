create or replace function ibrd.jf_group_pkg$of_rows(
      p_id_account numeric,
  out p_rows       refcursor,
      p_attr       text
)
  returns refcursor
language plpgsql as $$
declare
  l_info_board_ids integer [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_info_board_id', true);
  l_f_is_auto      boolean := jofl.jofl_pkg$extract_boolean(p_attr, 'f_is_auto_INPLACE_K', true);
begin
  open p_rows for
  select
    g.group_id,
    g.name                                                   group_name,
    g.route_id is not null                                   is_auto,
    g.dt_create,
    g.info,
    ibrd.jf_group_pkg$get_row_policy(g.route_id is not null) "ROW$POLICY"
  from ibrd.group g
  where (array_length(l_info_board_ids, 1) is null or g.group_id in (select ib2g.group_id
                                                                     from ibrd.info_board2group ib2g
                                                                     where ib2g.info_board_id = any (l_info_board_ids)))
        and (l_f_is_auto is null or (g.route_id is not null) = l_f_is_auto);
end;
$$;

create or replace function ibrd.jf_group_pkg$get_row_policy(
  p_is_group_auto boolean
)
  returns text
language plpgsql as $$
begin
  if (p_is_group_auto)
  then return 'P{RO},D{group_name}';
  else
    return 'P{A},D{OF_DELETE, OF_ADD_IBRD}';
  end if;
end;
$$;

create or replace function ibrd.jf_group_pkg$of_insert(
  p_id_account numeric,
  p_attr       text
)
  returns text
language plpgsql as
$$
declare
  l_group_name ibrd.group.name%type := jofl.jofl_pkg$extract_varchar(p_attr, 'group_name', false);
  l_info       ibrd.group.info%type := jofl.jofl_pkg$extract_varchar(p_attr, 'info', true);
begin
  insert into ibrd.group (name, info, dt_create)
  values (l_group_name, l_info, current_timestamp);

  return null;
end;
$$;

create or replace function ibrd.jf_group_pkg$of_update(
  p_id_account numeric,
  p_attr       text
)
  returns text
language plpgsql as $$
declare
  l_group_id   ibrd.group.group_id%type := jofl.jofl_pkg$extract_number(p_attr, 'group_id', false);
  l_group_name ibrd.group.name%type := jofl.jofl_pkg$extract_varchar(p_attr, 'group_name', true);
  l_info       ibrd.group.info%type := jofl.jofl_pkg$extract_varchar(p_attr, 'info', true);
begin
  update ibrd.group
  set
    name = l_group_name,
    info = l_info
  where group_id = l_group_id;

  return null;
end;
$$;

create or replace function ibrd.jf_group_pkg$of_delete(
  p_id_account numeric,
  p_attr       text
)
  returns text
language plpgsql as $$
declare
  l_group_id ibrd.group.group_id%type := jofl.jofl_pkg$extract_number(p_attr, 'group_id', false);
begin
  delete from ibrd.group
  where group_id = l_group_id;

  return null;
end;
$$;

create or replace function ibrd.jf_group_pkg$of_add_ibrd(
  p_id_account numeric,
  p_attr       text
)
  returns text
language plpgsql as $$
declare
  l_group_id       ibrd.group.group_id%type := jofl.jofl_pkg$extract_varchar(p_attr, 'group_id', false);
  l_info_board_ids integer [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_info_board_id', false);
begin
  insert into ibrd.info_board2group (group_id, info_board_id, relating_date)
  values (l_group_id, unnest(l_info_board_ids), current_timestamp);

  return null;
end;
$$;
