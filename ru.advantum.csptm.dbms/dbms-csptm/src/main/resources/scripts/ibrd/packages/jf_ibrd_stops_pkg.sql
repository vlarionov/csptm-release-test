create or replace function ibrd.jf_ibrd_stops_pkg$of_rows(
  in  p_id_account numeric,
  out p_rows       refcursor,
  in  p_attr       text
)
  returns refcursor
language plpgsql
as $$
declare
  l_info_board_id ibrd.info_board.info_board_id%type := jofl.jofl_pkg$extract_number(p_attr, 'info_board_id', true);

  l_rf_db_method  text := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true);
begin
  if l_rf_db_method = 'ibrd.forecast_full_report'
  then l_info_board_id = jofl.jofl_pkg$extract_number(p_attr, 'f_info_board_id', true);
  end if;

  open p_rows for
  select
    sl.stop_location_id,
    s.name,
    sl.building_address,
    exists(select 1
           from rts.stop_item si
           where si.tr_type_id = core.tr_type_pkg$bus() and si.stop_location_id = sl.stop_location_id) has_bus,
    exists(select 1
           from rts.stop_item si
           where si.tr_type_id = core.tr_type_pkg$tb() and si.stop_location_id = sl.stop_location_id)  has_trolley,
    exists(select 1
           from rts.stop_item si
           where si.tr_type_id = core.tr_type_pkg$tm() and si.stop_location_id = sl.stop_location_id)  has_tram,
    exists(select 1
           from rts.stop_item si
           where si.tr_type_id = core.tr_type_pkg$stm() and si.stop_location_id = sl.stop_location_id) has_speedtram,
    '[' || json_build_object('CAPTION', 'На карте',
                             'LINK', json_build_object('URL', '/app.html?m=modules/StopPlaceOnMapCtrl&attr={"wkt":"' ||
                                                              ST_AsText(sp.geom) ||
                                                              '","sp_muid":"' || sp.muid || '"}'
                             )) :: text || ']' as                                                      "JUMP_TO_SP"
  from ibrd.stop_location2info_board sl2ib
    join rts.stop_location sl on sl2ib.stop_location_id = sl.stop_location_id
    join rts.stop s on sl.stop_id = s.stop_id
    join rts.stop_location2gis sl2g on sl.stop_location_id = sl2g.stop_location_id
    join gis.stop_places sp on sl2g.stop_location_muid = sp.muid
  where (sl2ib.info_board_id = l_info_board_id or l_info_board_id is null);
end;
$$;