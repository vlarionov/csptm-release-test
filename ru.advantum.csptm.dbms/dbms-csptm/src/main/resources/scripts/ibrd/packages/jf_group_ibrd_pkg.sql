create or replace function ibrd.jf_group_ibrd_pkg$of_rows(
      p_id_account numeric,
  out p_rows       refcursor,
      p_attr       text
)
  returns refcursor
language plpgsql as
$$
declare
  l_group_id ibrd.group.group_id%type := jofl.jofl_pkg$extract_number(p_attr, 'group_id', false);
begin
  open p_rows for
  select
    ib2g.group_id,
    ib2g.info_board_id,
    ib2g.relating_date,
    ib.code,
    ibrd.jf_info_board_pkg$get_code_link(ib.info_board_id) code_link,
    t.name                                                 type_name,
    m.name                                                 model_name,
    ib.mobile_number,
    ibrd.info_board_pkg$find_stops(ib.info_board_id)       stops,
    ibrd.jf_group_ibrd_pkg$get_routes(ib.info_board_id)    routes,
    st_x(sl.wkt_geom)                                      lon,
    st_y(sl.wkt_geom)                                      lat,
    ip.name                                                installation_place_name,
    s.name                                                 state_name
  from ibrd.info_board2group ib2g
    join ibrd.info_board ib on ib2g.info_board_id = ib.info_board_id
    left join ibrd.model m on ib.model_id = m.model_id
    left join ibrd.type t on m.type_id = t.type_id
    left join ibrd.installation_place ip on ib.installation_place_id = ip.installation_place_id
    left join ibrd.stop_location2info_board sl2ib on ib.info_board_id = sl2ib.info_board_id and sl2ib.is_base
    left join rts.stop_location sl on sl2ib.stop_location_id = sl.stop_location_id
    left join ibrd.state s on ib.state_id = s.state_id
  where ib2g.group_id = l_group_id
        and not ib.is_deleted;
end;
$$;

create or replace function ibrd.jf_group_ibrd_pkg$of_delete(
  p_id_account numeric,
  p_attr       text
)
  returns text
language plpgsql as $$
declare
  l_info_board_id bigint := jofl.jofl_pkg$extract_number(p_attr, 'info_board_id', false);
  l_group_id      bigint := jofl.jofl_pkg$extract_number(p_attr, 'group_id', false);
begin
  delete from ibrd.info_board2group
  where info_board_id = l_info_board_id
        and group_id = l_group_id;

  return null;
end;
$$;

create or replace function ibrd.jf_group_ibrd_pkg$get_routes(
      p_info_board_id ibrd.info_board.info_board_id%type,
  out p_result        text
)
language plpgsql as $$
begin
  select string_agg(distinct r.route_num, ';')
  from ibrd.stop_location2info_board sl2ib
    join rts.stop_item si on si.stop_location_id = sl2ib.stop_location_id
    join rts.stop_item2round si2r on si.stop_item_id = si2r.stop_item_id
    join rts.round rnd on si2r.round_id = rnd.round_id
    join rts.route_variant rv on rnd.route_variant_id = rv.route_variant_id
    join rts.route r on rv.route_variant_id = r.current_route_variant_id
  where sl2ib.info_board_id = p_info_board_id
        and not si.sign_deleted
        and not si2r.sign_deleted
        and not rnd.sign_deleted
        and not rv.sign_deleted
        and not r.sign_deleted
  group by r.route_num
  order by r.route_num
  into p_result;
end;
$$;
