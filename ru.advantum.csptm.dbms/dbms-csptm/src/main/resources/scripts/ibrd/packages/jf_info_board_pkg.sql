create or replace function ibrd.jf_info_board_pkg$of_rows(
      p_id_account numeric,
  out p_rows       refcursor,
      p_attr       text
)
  returns refcursor
language plpgsql as
$$
declare
  l_f_info_boards_ids         integer [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_info_boards_ids', true);
  l_f_stop_location_ids       bigint [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_stop_location_id', true);
  l_f_type_id                 bigint := jofl.jofl_pkg$extract_number(p_attr, 'f_type_id', true);
  l_f_model_id                bigint := jofl.jofl_pkg$extract_number(p_attr, 'f_model_id', true);
  l_f_ibrd_code               text := jofl.jofl_pkg$extract_varchar(p_attr, 'f_ibrd_code_TEXT', true);
  l_f_manufacturer_id         bigint := jofl.jofl_pkg$extract_number(p_attr, 'f_manufacturer_id', true);
  l_f_balance_organization_id bigint := jofl.jofl_pkg$extract_number(p_attr, 'f_balance_organization_id', true);
  l_f_mobile_operator_id      bigint := jofl.jofl_pkg$extract_number(p_attr, 'f_mobile_operator_id', true);
  l_f_service_organization_id bigint := jofl.jofl_pkg$extract_number(p_attr, 'f_service_organization_id', true);
  l_f_service_contract_id     bigint := jofl.jofl_pkg$extract_number(p_attr, 'f_service_contract_id', true);
  l_f_state_id                bigint := jofl.jofl_pkg$extract_number(p_attr, 'f_state_id', true);
  l_f_address_program_id      bigint := jofl.jofl_pkg$extract_number(p_attr, 'f_address_program_id', true);

  l_info_board_id ibrd.info_board.info_board_id%type := jofl.jofl_pkg$extract_number(p_attr, 'info_board_id', true);
begin
  open p_rows for
  select distinct
    ib.info_board_id :: text,
    ib.info_board_id :: text as                                        f_info_board_id,
    ib.code :: integer       as                                        code,
    ib.model_id,
    ib.serial_number,
    ib.address_program_id,
    ib.installation_place_id,
    ib.address,
    ib.has_stop_pavilion,
    ib.imei,
    ib.mobile_operator_id,
    ib.mobile_number,
    ib.balance_organization_id,
    ib.inventory_number,
    ib.start_date,
    ib.useful_duration,
    ib.end_date,
    ib.state_id,
    ib.state_end_date,
    ib.service_contract_id,
    st.name                                                            status_name,
    mod.name                                                           model_name,
    typ.name                                                           type_name,
    man.name                                                           manufacturer_name,
    ap.name                                                            address_program_name,
    ip.name                                                            installation_place_name,
    st_x(sl.wkt_geom)                                                  lon,
    st_y(sl.wkt_geom)                                                  lat,
    mo.name                                                            mobile_operator_name,
    bo.name                                                            balance_organization_name,
    s.name                                                             stop_name,
    case when ibrd.event_pkg$is_connected(ib.info_board_id)
      then 'Подключено'
    else 'Отключено' end                                               connection_state,
    (select lc.event_date
     from ibrd.event_pkg$get_last_connection(ib.info_board_id) lc)     last_connection_time,
    ibrd.jf_info_board_pkg$get_map_link(ib.info_board_id, sl.wkt_geom) "JUMP_TO_MAP",
    ibrd.jf_info_board_pkg$get_row_policy(ib.info_board_id)            "ROW$POLICY"
  from ibrd.info_board ib
    left join ibrd.stop_location2info_board sl2ib on ib.info_board_id = sl2ib.info_board_id and sl2ib.is_base
    left join rts.stop_location sl on sl2ib.stop_location_id = sl.stop_location_id
    left join rts.stop s on sl.stop_id = s.stop_id
    left join ibrd.state st on st.state_id = ib.state_id
    left join ibrd.model mod on mod.model_id = ib.model_id
    left join ibrd.type typ on typ.type_id = mod.type_id
    left join ibrd.manufacturer man on man.manufacturer_id = mod.manufacturer_id
    left join ibrd.balance_organization bo on bo.balance_organization_id = ib.balance_organization_id
    left join ibrd.mobile_operator mo on mo.mobile_operator_id = ib.mobile_operator_id
    left join ibrd.address_program ap on ap.address_program_id = ib.address_program_id
    left join ibrd.installation_place ip on ip.installation_place_id = ib.installation_place_id
    left join ibrd.service_contract scon on scon.service_contract_id = ib.service_contract_id
    left join ibrd.service_organization sorg on sorg.service_organization_id = scon.service_organization_id
  where
    not ib.is_deleted
    and (ib.info_board_id = any (l_f_info_boards_ids) or l_f_info_boards_ids is null or
         array_length(l_f_info_boards_ids, 1) = 0)
    and ((array_length(l_f_stop_location_ids, 1) > 0 and sl2ib.stop_location_id = any (l_f_stop_location_ids)) or
         array_length(l_f_stop_location_ids, 1) is null)
    and (typ.type_id = l_f_type_id or l_f_type_id is null)
    and (ib.model_id = l_f_model_id or l_f_model_id is null)
    and (ib.code = l_f_ibrd_code or l_f_ibrd_code = '' or l_f_ibrd_code is null)
    and (man.manufacturer_id = l_f_manufacturer_id or l_f_manufacturer_id is null)
    and (ib.balance_organization_id = l_f_balance_organization_id or l_f_balance_organization_id is null)
    and (ib.mobile_operator_id = l_f_mobile_operator_id or l_f_mobile_operator_id is null)
    and (sorg.service_organization_id = l_f_service_organization_id or l_f_service_organization_id is null)
    and (ib.service_contract_id = l_f_service_contract_id or l_f_service_contract_id is null)
    and (ib.state_id = l_f_state_id or l_f_state_id is null)
    and (ib.address_program_id = l_f_address_program_id or l_f_address_program_id is null)
    and (l_info_board_id is null or ib.info_board_id = l_info_board_id);
end;
$$;

create or replace function ibrd.jf_info_board_pkg$get_code_link(
      p_info_board_id ibrd.info_board.info_board_id%type,
  out p_result        text
)
language plpgsql as $$
begin
  select '[' || json_build_object(
      'CAPTION', code,
      'JUMP', json_build_object('METHOD', 'ibrd.info_board', 'ATTR', '{"f_info_boards_ids" : "$array[' || info_board_id || ']"}')
  ) || ']'
  from ibrd.info_board
  where info_board_id = p_info_board_id
  into p_result;
end;
$$;

create or replace function ibrd.jf_info_board_pkg$get_map_link(
  p_info_board_id ibrd.info_board.info_board_id%type,
  p_geom          geometry
)
  returns text
language plpgsql
as $$
begin
  return '['
         || json_build_object(
             'CAPTION', 'На карте',
             'LINK', json_build_object(
                 'URL',
                 '/app.html?m=modules/IbrdOnMapCtrl&attr={"wkt":"' || ST_AsText(p_geom) || '","info_board_id":"' || p_info_board_id || '"}'
             )
         ) :: text
         || ']';
end;
$$;

create or replace function ibrd.jf_info_board_pkg$get_row_policy(p_info_board_id ibrd.info_board.info_board_id%type)
  returns text
language plpgsql
as $$
begin
  if ibrd.protocol_pkg$find_protocol_id(p_info_board_id) = ibrd.protocol_pkg$row_protocol_id()
  then return 'P{D},D{ibrd.row_config,ibrd.telematics},P{A},D{OF_TELEMATICS_REQUEST}';
  else return 'P{D},D{ibrd.mono_rgb_config}';
  end if;
end;
$$;

create or replace function ibrd.jf_info_board_pkg$of_telematics_request(p_id_account numeric, p_attr text)
  returns text
language plpgsql
as $$
declare
  l_info_board_id bigint := jofl.jofl_pkg$extract_number(p_attr, 'info_board_id', true);
begin
  perform paa.paa_api_pkg$basic_publish(
      cmnd.jf_cmnd_ref4unit_pkg$cn_command_rmq_broker(),
      'ibrd-telematics-requests',
      '',
      json_build_object('ibrdId', l_info_board_id) :: text,
      'TelematicsRequest'
  );

  return null;
end;
$$;