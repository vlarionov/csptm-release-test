CREATE OR REPLACE FUNCTION ibrd.jf_manufacturer_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ibrd.manufacturer AS
$body$
declare 
   l_r ibrd.manufacturer%rowtype; 
begin 
   l_r.manufacturer_id := jofl.jofl_pkg$extract_number(p_attr, 'manufacturer_id', true); 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 
   l_r.is_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'is_deleted', true); 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_manufacturer_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.manufacturer%rowtype;
begin 
   l_r := ibrd.jf_manufacturer_pkg$attr_to_rowtype(p_attr);

   /*delete from  ibrd.manufacturer where  manufacturer_id = l_r.manufacturer_id;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_manufacturer_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.manufacturer%rowtype;
begin 
   l_r := ibrd.jf_manufacturer_pkg$attr_to_rowtype(p_attr);

   /*insert into ibrd.manufacturer select l_r.*;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_manufacturer_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
begin 
 open p_rows for 
      select 
        manufacturer_id, 
        name, 
        is_deleted
      from ibrd.manufacturer
     where not is_deleted; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_manufacturer_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.manufacturer%rowtype;
begin 
   l_r := ibrd.jf_manufacturer_pkg$attr_to_rowtype(p_attr);

   /*update ibrd.manufacturer set 
          name = l_r.name, 
          is_deleted = l_r.is_deleted
   where 
          manufacturer_id = l_r.manufacturer_id;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
-------------------------------------------------