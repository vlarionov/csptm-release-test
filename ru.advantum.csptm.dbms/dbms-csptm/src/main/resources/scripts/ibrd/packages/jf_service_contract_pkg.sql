CREATE OR REPLACE FUNCTION ibrd.jf_service_contract_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ibrd.service_contract AS
$body$
declare 
   l_r ibrd.service_contract%rowtype; 
begin 
   l_r.number := jofl.jofl_pkg$extract_varchar(p_attr, 'number', true); 
   l_r.service_contract_id := jofl.jofl_pkg$extract_number(p_attr, 'service_contract_id', true); 
   l_r.service_organization_id := jofl.jofl_pkg$extract_number(p_attr, 'service_organization_id', true); 
   l_r.start_date := jofl.jofl_pkg$extract_date(p_attr, 'start_date', true); 
   l_r.end_date := jofl.jofl_pkg$extract_date(p_attr, 'end_date', true); 
   l_r.contract_date := jofl.jofl_pkg$extract_date(p_attr, 'contract_date', true); 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_service_contract_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.service_contract%rowtype;
begin 
   l_r := ibrd.jf_service_contract_pkg$attr_to_rowtype(p_attr);

  /* delete from  ibrd.service_contract where  service_contract_id = l_r.service_contract_id;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_service_contract_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.service_contract%rowtype;
begin 
   l_r := ibrd.jf_service_contract_pkg$attr_to_rowtype(p_attr);

   /*insert into ibrd.service_contract select l_r.*;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_service_contract_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 ln_ref_service_org_id bigint := jofl.jofl_pkg$extract_number(p_attr, 'f_service_organization_id', true);
begin 
 open p_rows for 
      select 
        number, 
        service_contract_id, 
        service_organization_id, 
        start_date, 
        end_date, 
        contract_date
      from ibrd.service_contract
     where service_organization_id = ln_ref_service_org_id; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_service_contract_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.service_contract%rowtype;
begin 
   l_r := ibrd.jf_service_contract_pkg$attr_to_rowtype(p_attr);

   /*update ibrd.service_contract set 
          number = l_r.number, 
          service_organization_id = l_r.service_organization_id, 
          start_date = l_r.start_date, 
          end_date = l_r.end_date, 
          contract_date = l_r.contract_date
   where 
          service_contract_id = l_r.service_contract_id;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------