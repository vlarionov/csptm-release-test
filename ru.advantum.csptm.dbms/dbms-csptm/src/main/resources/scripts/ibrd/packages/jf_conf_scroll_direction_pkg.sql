CREATE OR REPLACE FUNCTION ibrd.jf_conf_scroll_direction_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.scroll_direction%rowtype;
begin 
   l_r := ibrd.jf_conf_scroll_direction_pkg$attr_to_rowtype(p_attr);

   /*update ibrd.scroll_direction set 
          name = l_r.name
   where 
          scroll_direction_id = l_r.scroll_direction_id;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_conf_scroll_direction_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
begin 
 open p_rows for 
      select 
        name, 
        scroll_direction_id
      from ibrd.scroll_direction; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_conf_scroll_direction_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.scroll_direction%rowtype;
begin 
   l_r := ibrd.jf_conf_scroll_direction_pkg$attr_to_rowtype(p_attr);

   /*insert into ibrd.scroll_direction select l_r.*;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_conf_scroll_direction_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.scroll_direction%rowtype;
begin 
   l_r := ibrd.jf_conf_scroll_direction_pkg$attr_to_rowtype(p_attr);

   /*delete from  ibrd.scroll_direction where  scroll_direction_id = l_r.scroll_direction_id;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_conf_scroll_direction_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ibrd.scroll_direction AS
$body$
declare 
   l_r ibrd.scroll_direction%rowtype; 
begin 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 
   l_r.scroll_direction_id := jofl.jofl_pkg$extract_number(p_attr, 'scroll_direction_id', true); 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
-------------------------------------------------