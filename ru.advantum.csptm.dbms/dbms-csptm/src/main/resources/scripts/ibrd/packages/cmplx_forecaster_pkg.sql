﻿create or replace function ibrd.cmplx_forecaster_pkg$get_training_dataset
    (p_route_muid        in gis.routes.muid%type,
     p_begin_dt          in tt.order_list.order_date%type,
     p_end_dt            in tt.order_list.order_date%type,
     p_time_window_width in smallint,
     p_skip_nulls        in boolean default true)
    returns table(order_date            tt.order_list.order_date%type,
                  shift                 tt.order_list.shift%type,
                  tr_id                 core.tr.tr_id%type,
                  order_round_num       tt.order_round.order_round_num%type,
                  order_round_id        tt.order_round.order_round_id%type,
                  round_id              tt.round.round_id%type,
                  route_trajectory_muid gis.route_trajectories.muid%type,
                  stop_place_muid       gis.stop_places.muid%type,
                  stop_order_num        gis.stop_place2route_traj.order_num%type,
                  time_oper             tt.order_oper.time_oper%type,
                  time_fact             tt.order_fact.time_fact%type,
                  day_of_week           smallint,
                  time_window           smallint,
                  tt                    real,
                  tt_prev               real,
                  tt_next               real)
as $$
declare
begin
    return query
    select *
    from (select
              t.order_date,
              t.shift,
              t.tr_id,
              t.order_round_num,
              t.order_round_id,
              t.round_id,
              t.route_trajectory_muid,
              t.stop_place_muid,
              t.stop_order_num,
              t.time_oper,
              t.time_fact,
              extract(isodow from t.order_date) :: smallint day_of_week,
              extract(hour from t.time_fact) :: smallint / p_time_window_width time_window,
              t.tt :: real tt,
              lead(t.tt_prev)
              over (
                  partition by t.order_round_id
                  order by t.stop_order_num ) :: real tt_prev,
              t.tt_next :: real tt_next
          from (select
                    t.order_date,
                    t.shift,
                    t.tr_id,
                    t.order_round_num,
                    t.order_round_id,
                    t.round_id,
                    t.route_trajectory_muid,
                    t.stop_place_muid,
                    t.stop_order_num,
                    t.time_oper,
                    t.time_fact,
                    t.tt,
                    lag(t.tt)
                    over (
                        partition by t.order_date, t.route_trajectory_muid, t.stop_order_num
                        order by t.time_fact ) tt_prev,
                    lead(t.tt)
                    over (
                        partition by t.order_round_id
                        order by t.stop_order_num ) tt_next
                from (select
                          ol.order_date,
                          ol.shift,
                          ol.tr_id,
                          ord.order_round_num,
                          ord.order_round_id,
                          rnd.round_id,
                          rt.muid route_trajectory_muid,
                          oo.stop_place_muid,
                          oo.stop_order_num,
                          oo.time_oper,
                          of.time_fact,
                          coalesce(extract(epoch from of.time_fact - lag(of.time_fact)
                          over (
                              partition by ord.order_round_id
                              order by of.stop_order_num )), 0) tt
                      from tt.order_list ol
                          join tt.order_round ord on ol.order_list_id = ord.order_list_id
                          join asd.oper_round_visit orv on ord.order_round_id = orv.order_round_id
                          join tt.round rnd on ord.round_id = rnd.round_id
                          join gis.route_trajectories rt on rnd.route_trajectory_muid = rt.muid
                          join gis.route_rounds rr on rt.route_round_muid = rr.muid
                          join gis.route_variants rv on rr.route_variant_muid = rv.muid
                          join gis.routes r on rv.route_muid = r.muid
                          join tt.order_oper oo on ord.order_round_id = oo.order_round_id
                          join tt.order_fact of
                              on ord.order_round_id = of.order_round_id and oo.stop_order_num = of.stop_order_num
                      where ol.order_date between p_begin_dt and p_end_dt
                            and r.muid = p_route_muid
                            and ol.is_active = 1
                            and ol.sign_deleted = 0
                            and ord.is_active = 1
                            and oo.is_active = 1
                            and oo.sign_deleted = 0
                            and of.sign_deleted = 0
                     ) t
               ) t
         ) t
    where not p_skip_nulls or t.tt_prev is not null
    order by
        order_date,
        tr_id,
        shift,
        order_round_num,
        stop_order_num;
end;
$$ language plpgsql security definer;

create or replace function ibrd.cmplx_forecaster_pkg$get_order_round_tt
    (p_order_round_id tt.order_round.order_round_id%type)
    returns table(stop_order_num  gis.stop_place2route_traj.order_num%type,
                  stop_place_muid gis.stop_places.muid%type,
                  time_oper       tt.order_oper.time_oper%type,
                  tt              real)
as $$
declare
begin
    return query
        select * from (
            select
                oo.stop_order_num,
                oo.stop_place_muid,
                oo.time_oper,
                coalesce(extract(epoch from (oo.time_oper - lag(oo.time_oper) over (order by oo.stop_order_num))), 0)::real tt
            from tt.order_oper oo
            where oo.order_round_id = p_order_round_id
                  and oo.is_active = 1
                  and oo.sign_deleted = 0
                  and oo.stop_order_num is not null
                  and oo.time_oper is not null
        ) t
        where t.tt > 0 or t.stop_order_num = 1
        order by t.stop_order_num;
end;
$$ language plpgsql security definer;

create or replace function ibrd.cmplx_forecaster_pkg$get_next_order_round
    (p_order_round_id tt.order_round.order_round_id%type)
    returns table(order_round_id        tt.order_round.order_round_id%type,
                  route_trajectory_muid gis.route_trajectories.muid%type,
                  time_plan_begin       asd.oper_round_visit.time_plan_begin%type)
as $$
declare
    l_next_order_round_id tt.order_round.order_round_id%type;
begin
    l_next_order_round_id := ibrd.simple_forecaster_pkg$get_next_order_round(p_order_round_id);

    return query
    select
        ord.order_round_id,
        rnd.route_trajectory_muid,
        orv.time_plan_begin
    from tt.order_round ord
        join tt.round rnd on ord.round_id = rnd.round_id
        join asd.oper_round_visit orv on ord.order_round_id = orv.order_round_id
    where ord.order_round_id = l_next_order_round_id;
end;
$$ language plpgsql security definer;

create or replace function ibrd.cmplx_forecaster_pkg$get_route_traj_stops
    (p_route_trajectory_id gis.route_trajectories.muid%type)
    returns table(route_trajectory_muid gis.route_trajectories.muid%type,
                  stop_order_num        gis.stop_place2route_traj.order_num%type,
                  stop_place_muid       gis.stop_places.muid%type)
as $$
begin
    return query
    select
        sp2rt.route_trajectory_muid,
        sp2rt.order_num,
        sp2rt.stop_place_muid
    from gis.stop_place2route_traj sp2rt
    where sp2rt.route_trajectory_muid = p_route_trajectory_id;
end;
$$ language plpgsql security definer;