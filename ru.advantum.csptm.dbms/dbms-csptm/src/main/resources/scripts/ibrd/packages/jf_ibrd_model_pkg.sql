CREATE OR REPLACE FUNCTION ibrd.jf_ibrd_model_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ibrd.model AS
$body$
declare 
   l_r ibrd.model%rowtype; 
begin 
   l_r.model_id := jofl.jofl_pkg$extract_number(p_attr, 'model_id', true); 
   l_r.is_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'is_deleted', true); 
   l_r.manufacturer_id := jofl.jofl_pkg$extract_number(p_attr, 'manufacturer_id', true); 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 
   l_r.type_id := jofl.jofl_pkg$extract_number(p_attr, 'type_id', true); 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_ibrd_model_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.model%rowtype;
begin 
   l_r := ibrd.jf_ibrd_model_pkg$attr_to_rowtype(p_attr);

   /*delete from  ibrd.model where  model_id = l_r.model_id;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_ibrd_model_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.model%rowtype;
begin 
   l_r := ibrd.jf_ibrd_model_pkg$attr_to_rowtype(p_attr);

   /*insert into ibrd.model select l_r.*;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_ibrd_model_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
begin 
 open p_rows for 
      select 
        model_id, 
        is_deleted, 
        manufacturer_id, 
        name, 
        type_id
      from ibrd.model
 	 where not is_deleted; 	  
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_ibrd_model_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.model%rowtype;
begin 
   l_r := ibrd.jf_ibrd_model_pkg$attr_to_rowtype(p_attr);

   /*update ibrd.model set 
          is_deleted = l_r.is_deleted, 
          manufacturer_id = l_r.manufacturer_id, 
          name = l_r.name, 
          type_id = l_r.type_id
   where 
          model_id = l_r.model_id;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
----------------------------------------------------------