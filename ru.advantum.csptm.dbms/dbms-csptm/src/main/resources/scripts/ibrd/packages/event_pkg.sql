CREATE OR REPLACE FUNCTION ibrd.event_pkg$get_last_connection(
  p_info_board_id ibrd.info_board.info_board_id%TYPE
)
  RETURNS SETOF ibrd.EVENT
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT *
  FROM ibrd.event e
  WHERE e.info_board_id = p_info_board_id AND e.event_type_id = ibrd.event_type_pkg$connect()
  ORDER BY e.event_date DESC
  LIMIT 1;
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.event_pkg$get_last_success_set_info_event_id(
  p_info_board_id ibrd.info_board.info_board_id%TYPE
)
  RETURNS ibrd.event.event_id%TYPE
LANGUAGE plpgsql
AS $$
DECLARE
  l_result ibrd.event.event_id%TYPE;
BEGIN
  SELECT e.event_id
  INTO l_result
  FROM ibrd.event e
  WHERE
    e.info_board_id = p_info_board_id AND
    e.event_type_id = ibrd.event_type_pkg$set_info() AND
    EXISTS(SELECT 1
           FROM ibrd.event response_e
           WHERE e.event_id = response_e.command_event_id AND response_e.error_type_id =  ibrd.error_type_pkg$null())
  ORDER BY e.event_date DESC
  LIMIT 1;
  RETURN l_result;
END;
$$;

CREATE OR REPLACE FUNCTION ibrd.event_pkg$is_connected(
  p_info_board_id ibrd.info_board.info_board_id%TYPE
)
  RETURNS BOOLEAN
LANGUAGE plpgsql
AS $$
DECLARE
  l_last_connect_date    TIMESTAMP;
  l_last_disconnect_date TIMESTAMP;
BEGIN
  SELECT e.event_date
  INTO l_last_connect_date
  FROM ibrd.event e
  WHERE e.info_board_id = p_info_board_id
        AND e.event_type_id = ibrd.event_type_pkg$connect()
  ORDER BY e.event_date DESC
  LIMIT 1;

  SELECT e.event_date
  INTO l_last_disconnect_date
  FROM ibrd.event e
  WHERE e.info_board_id = p_info_board_id
        AND e.event_type_id = ibrd.event_type_pkg$disconnect()
  ORDER BY e.event_date DESC
  LIMIT 1;

  RETURN coalesce(
      l_last_connect_date >= l_last_disconnect_date,
      l_last_connect_date IS NOT NULL, l_last_disconnect_date IS NULL
  );
END;
$$;

