CREATE OR REPLACE FUNCTION ibrd.jf_conf_font_style_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.font_style%rowtype;
begin 
   l_r := ibrd.jf_conf_font_style_pkg$attr_to_rowtype(p_attr);

   /*update ibrd.font_style set 
          name = l_r.name
   where 
          font_style_id = l_r.font_style_id;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_conf_font_style_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
begin 
 open p_rows for 
      select 
        font_style_id, 
        name
      from ibrd.font_style; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_conf_font_style_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.font_style%rowtype;
begin 
   l_r := ibrd.jf_conf_font_style_pkg$attr_to_rowtype(p_attr);

   /*insert into ibrd.font_style select l_r.*;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_conf_font_style_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.font_style%rowtype;
begin 
   l_r := ibrd.jf_conf_font_style_pkg$attr_to_rowtype(p_attr);

   /*delete from  ibrd.font_style where  font_style_id = l_r.font_style_id;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_conf_font_style_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ibrd.font_style AS
$body$
declare 
   l_r ibrd.font_style%rowtype; 
begin 
   l_r.font_style_id := jofl.jofl_pkg$extract_number(p_attr, 'font_style_id', true); 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
-------------------------------------------------