create or replace function ibrd.message_pkg$find(p_info_board_id integer)
  returns table(text text, r_color integer, g_color integer, b_color integer)
language plpgsql
as $$
declare
  l_protocol_id ibrd.ibrd_protocol.protocol_id%type;
begin
  l_protocol_id := ibrd.protocol_pkg$find_protocol_id(p_info_board_id);

  if l_protocol_id = ibrd.protocol_pkg$row_protocol_id()
  then
    return query
    select *
    from ibrd.message_pkg$find_row(p_info_board_id, l_protocol_id);
  else
    return query
    select *
    from ibrd.message_pkg$find_mono_rgb(p_info_board_id, l_protocol_id);
  end if;
end;
$$;

create or replace function ibrd.message_pkg$find_mono_rgb(p_info_board_id ibrd.info_board.info_board_id%type,
                                                          p_protocol_id   smallint)
  returns table(text text, r_color integer, g_color integer, b_color integer)
language plpgsql
as $$
declare
  l_ibrd_messages_ids integer [];
  l_message           record;
  l_text              text;
  l_r_color           integer;
  l_g_color           integer;
  l_b_color           integer;
begin
  l_ibrd_messages_ids = ibrd.message_pkg$find_messages_ids(p_info_board_id);
  for l_message in
  select
    m.msg_text,
    m.priority,
    null :: integer r_color,
    null :: integer g_color,
    null :: integer b_color
  from ibrd.message m
  where m.message_id = any (l_ibrd_messages_ids)
  order by m.priority
  loop
    if l_text is null
    then
      l_text := l_message.msg_text;
      l_r_color := l_message.r_color;
      l_g_color := l_message.g_color;
      l_b_color := l_message.b_color;
    else
      if ibrd.message_pkg$is_valid_size(p_protocol_id, l_text || ' ' || l_message.msg_text)
      then
        l_text := l_text || ' ' || l_message.msg_text;
      else
        exit;
      end if;
    end if;
  end loop;

  if l_text is null
  then
    return query select
                   '' :: text,
                   null :: integer,
                   null :: integer,
                   null :: integer;
  else
    return query select
                   l_text,
                   l_r_color,
                   l_g_color,
                   l_b_color;
  end if;

end;
$$;

create or replace function ibrd.message_pkg$find_row(p_info_board_id integer, p_protocol_id smallint)
  returns table(text text, r_color integer, g_color integer, b_color integer)
language plpgsql
as $$
declare
  l_ibrd_messages_ids integer [];
begin
  l_ibrd_messages_ids = ibrd.message_pkg$find_messages_ids(p_info_board_id);
  return query
  select
    m.msg_text,
    null :: integer r_color,
    null :: integer g_color,
    null :: integer b_color
  from ibrd.message m
  where m.message_id = any (l_ibrd_messages_ids)
  order by m.priority;
end;
$$;

create or replace function ibrd.message_pkg$find_messages_ids(p_info_board_id ibrd.info_board.info_board_id%type)
  returns integer []
language plpgsql
as $$
declare
  l_protocol_id smallint;
begin
  l_protocol_id := ibrd.protocol_pkg$find_protocol_id(p_info_board_id);

  return ARRAY(
      select m.message_id
      from ibrd.message m
        join ibrd.message_schedule ms on m.message_id = ms.message_id
      where (m.info_board_id = p_info_board_id or m.group_id in (select m.group_id
                                                                 from ibrd.info_board2group ib2g
                                                                 where ib2g.info_board_id = p_info_board_id))
            and m.start_date <= current_timestamp and (current_timestamp <= m.end_date or m.end_date is null)
            and ms.week_day = extract(dow from current_timestamp)
            and (ms.start_time is null or ms.start_time <= (current_time + '3 hours' :: interval))
            and (ms.end_time is null or (current_time + '3 hours' :: interval) <= ms.end_time)
            and m.is_enabled
            and ibrd.message_pkg$is_valid_size(l_protocol_id, m.msg_text)
      order by m.message_id
  );
end;
$$;

create or replace function ibrd.message_pkg$is_valid_size(p_protocol_id ibrd.ibrd_protocol.protocol_id%type,
                                                          p_text        text)
  returns boolean
language plpgsql
as $$
begin
  return bit_length(convert(p_text :: bytea, 'UTF8', 'windows_1251')) / 8 <= ibrd.message_pkg$max_size(p_protocol_id);
end;
$$;

create or replace function ibrd.message_pkg$max_size(p_protocol_id ibrd.ibrd_protocol.protocol_id%type)
  returns integer
immutable
language plpgsql
as $$
begin
  if p_protocol_id = ibrd.protocol_pkg$row_protocol_id()
  then return 125;
  elseif p_protocol_id = ibrd.protocol_pkg$mono_protocol_id() or p_protocol_id = ibrd.protocol_pkg$rgb_protocol_id()
    then return 254;
  else
    return null;
  end if;
end;
$$;








