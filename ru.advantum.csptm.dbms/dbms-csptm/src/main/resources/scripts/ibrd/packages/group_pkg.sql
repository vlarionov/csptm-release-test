create or replace function ibrd.group_pkg$actualize_by_routes()
  returns void
language plpgsql
as $$
begin
  insert into ibrd.group as g (route_id, name, dt_create)
    select
      r.route_id,
      r.route_num || '-' || trt.short_name,
      current_timestamp
    from rts.route r
      join core.tr_type trt on r.tr_type_id = trt.tr_type_id
    where not r.sign_deleted
          and r.route_id not in (select existed_g.route_id
                                 from ibrd.group existed_g
                                 where r.route_id is not null);

  update ibrd.group as g
  set
    name = r.route_num || '-' || trt.short_name
  from rts.route r
    join core.tr_type trt on r.tr_type_id = trt.tr_type_id
  where not r.sign_deleted
        and r.route_id = g.route_id
        and (r.route_num || '-' || trt.short_name) <> g.name;

  delete from ibrd.info_board2group
  where (info_board_id, group_id) not in (select *
                                          from ibrd.group_pkg$find_ibrd_groups())
        and group_id in (select g.group_id
                         from ibrd.group g
                         where g.route_id is not null );

  insert into ibrd.info_board2group (group_id, info_board_id, relating_date)
    select
      igs.group_id,
      igs.info_board_id,
      now()
    from ibrd.group_pkg$find_ibrd_groups() igs
  on conflict (info_board_id, group_id) do nothing;
end;
$$;

create or replace function ibrd.group_pkg$find_ibrd_groups()
  returns table(
    info_board_id ibrd.info_board.info_board_id%type,
    group_id      ibrd.group.group_id%type
  )
stable
language plpgsql
as $$
begin
  return query
  select
    sl2ib.info_board_id,
    g.group_id
  from ibrd.stop_location2info_board sl2ib
    join rts.stop_item si on sl2ib.stop_location_id = si.stop_location_id
    join rts.stop_item2round si2r on si.stop_item_id = si2r.stop_item_id
    join rts.round rnd on si2r.round_id = rnd.round_id
    join rts.route_variant rv on rnd.route_variant_id = rv.route_variant_id
    join ibrd.group g on rv.route_id = g.route_id
  where not si.sign_deleted
        and not si2r.sign_deleted
        and not rnd.sign_deleted
        and not rv.sign_deleted;
end;
$$;