CREATE OR REPLACE FUNCTION ibrd.jf_service_org_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ibrd.service_organization AS
$body$
declare 
   l_r ibrd.service_organization%rowtype; 
begin 
   l_r.service_organization_id := jofl.jofl_pkg$extract_number(p_attr, 'service_organization_id', true); 
   l_r.is_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'is_deleted', true); 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_service_org_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.service_organization%rowtype;
begin 
   l_r := ibrd.jf_service_org_pkg$attr_to_rowtype(p_attr);

   /*delete from  ibrd.service_organization where  service_organization_id = l_r.service_organization_id;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_service_org_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.service_organization%rowtype;
begin 
   l_r := ibrd.jf_service_org_pkg$attr_to_rowtype(p_attr);

   /*insert into ibrd.service_organization select l_r.*;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_service_org_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
begin 
 open p_rows for 
      select 
        service_organization_id, 
        is_deleted, 
        name
      from ibrd.service_organization
     where not is_deleted; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ibrd.jf_service_org_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ibrd.service_organization%rowtype;
begin 
   l_r := ibrd.jf_service_org_pkg$attr_to_rowtype(p_attr);

   /*update ibrd.service_organization set 
          is_deleted = l_r.is_deleted, 
          name = l_r.name
   where 
          service_organization_id = l_r.service_organization_id;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------