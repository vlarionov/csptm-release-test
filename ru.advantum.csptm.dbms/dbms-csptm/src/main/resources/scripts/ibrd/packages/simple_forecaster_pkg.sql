﻿create or replace function ibrd.simple_forecaster_pkg$get_forecast_by_stop_visit
    (p_stop_visits in jsonb)
    returns table(order_round_id            tt.order_round.order_round_id%type,
                  stop_place_muid           gis.stop_places.muid%type,
                  stop_place_order_num      gis.stop_place2route_traj.order_num%type,
                  creation_date             timestamp,
                  pass_time_forecast        timestamp,
                  from_order_round_id       tt.order_round.order_round_id%type,
                  from_stop_place_order_num gis.stop_place2route_traj.order_num%type)
as $$ declare
begin
    return query
    with stop_visits as (
        select
            (sv ->> 'orderRoundId') :: bigint order_round_id,
            (sv ->> 'routeTrajectoryMuid') :: bigint route_trajectory_muid,
            (sv ->> 'stopPlaceMuid') :: bigint stop_place_muid,
            (sv ->> 'stopPlaceOrderNum') :: bigint stop_place_order_num,
            (sv ->> 'passTime') :: timestamp pass_time
        from (select jsonb_array_elements(p_stop_visits) sv) t
    ), tt_deviation as (
        select
            sv.order_round_id,
            ibrd.simple_forecaster_pkg$get_next_order_round(sv.order_round_id) next_order_round_id,
            sv.stop_place_muid,
            sv.stop_place_order_num,
            sv.pass_time,
            sv.pass_time - oo.time_oper deviation
        from stop_visits sv
            join tt.order_oper oo
                on sv.order_round_id = oo.order_round_id and oo.stop_order_num = sv.stop_place_order_num
        where oo.is_active = 1
              and oo.sign_deleted = 0
    )
    select *
    from (select
              oo.order_round_id,
              oo.stop_place_muid,
              oo.stop_order_num,
              now() :: timestamp creation_date,
              oo.time_oper + td.deviation pass_time_forecast,
              td.order_round_id from_order_round_id,
              td.stop_place_order_num from_stop_place_order_num
          from tt_deviation td
              join tt.order_oper oo
                  on td.order_round_id = oo.order_round_id and oo.stop_order_num > td.stop_place_order_num
          where oo.is_active = 1
                and oo.sign_deleted = 0
                and oo.time_oper + td.deviation < td.pass_time + ibrd.simple_forecaster_pkg$get_max_interval()
--           union all
--           select
--               oo.order_round_id,
--               oo.stop_place_muid,
--               oo.stop_order_num,
--               now() :: timestamp,
--               oo.time_oper + td.deviation,
--               td.order_round_id,
--               td.stop_place_order_num
--           from tt_deviation td
--               join tt.order_oper oo on td.next_order_round_id = oo.order_round_id
--           where oo.is_active = 1
--                 and oo.sign_deleted = 0
--                 and oo.time_oper + td.deviation < td.pass_time + ibrd.simple_forecaster_pkg$get_max_interval()
         ) t
    order by from_order_round_id, from_stop_place_order_num, pass_time_forecast;
end;
$$ language plpgsql security definer;

create or replace function ibrd.simple_forecaster_pkg$get_forecast_by_stop_visit_with_stat
    (p_stop_visits in jsonb)
    returns table(order_round_id            tt.order_round.order_round_id%type,
                  stop_place_muid           gis.stop_places.muid%type,
                  stop_place_order_num      gis.stop_place2route_traj.order_num%type,
                  creation_date             timestamp,
                  pass_time_forecast        timestamp,
                  from_order_round_id       tt.order_round.order_round_id%type,
                  from_stop_place_order_num gis.stop_place2route_traj.order_num%type)
as $$ declare
begin
    return query
    with stop_visits as (
        select
            (sv ->> 'orderRoundId') :: bigint order_round_id,
            (sv ->> 'routeTrajectoryMuid') :: bigint route_trajectory_muid,
            (sv ->> 'stopPlaceMuid') :: bigint stop_place_muid,
            (sv ->> 'stopPlaceOrderNum') :: bigint stop_place_order_num,
            (sv ->> 'passTime') :: timestamp pass_time
        from (select jsonb_array_elements(p_stop_visits) sv) t
    ), tt_deviation as (
        select
            sv.order_round_id,
            ibrd.simple_forecaster_pkg$get_next_order_round(sv.order_round_id) next_order_round_id,
            sv.stop_place_muid,
            sv.stop_place_order_num,
            sv.pass_time,
            coalesce(sv.pass_time - oo.time_oper, interval '0 secs') deviation
        from stop_visits sv
            join tt.order_oper oo
                on sv.order_round_id = oo.order_round_id and oo.stop_order_num = sv.stop_place_order_num
        where oo.is_active = 1
              and oo.sign_deleted = 0
    )
    select
        t.order_round_id,
        t.stop_place_muid,
        t.stop_order_num,
        now() :: timestamp creation_date,
        t.time_oper + t.deviation + sum(t.deviation_diff)
        over (
            partition by t.from_order_round_id, t.from_stop_place_order_num
            order by t.round_order_num, t.stop_order_num ) pass_time_forecast,
        t.from_order_round_id,
        t.from_stop_place_order_num
    from (select
              oo.order_round_id,
              oo.stop_place_muid,
              oo.stop_order_num,
              td.deviation,
              oo.time_oper,
              td.order_round_id from_order_round_id,
              td.stop_place_order_num from_stop_place_order_num,
              0 round_order_num,
              coalesce(rs.sum_date_diff / rs.item_count, interval '0 secs') deviation_diff
          from tt_deviation td
              join tt.order_oper oo
                  on td.order_round_id = oo.order_round_id and oo.stop_order_num > td.stop_place_order_num
              join tt.order_round oro on oo.order_round_id = oro.order_round_id
              left join ibrd.round_stat rs on oro.round_id = rs.round_id and oo.stop_order_num = rs.stop_order_num
          where oo.is_active = 1
                and oo.sign_deleted = 0
                and oo.time_oper + td.deviation < td.pass_time + ibrd.simple_forecaster_pkg$get_max_interval()
--           union all
--           select
--               oo.order_round_id,
--               oo.stop_place_muid,
--               oo.stop_order_num,
--               td.deviation,
--               oo.time_oper,
--               td.order_round_id,
--               td.stop_place_order_num,
--               1 round_order_num,
--               coalesce(rs.sum_date_diff / rs.item_count, interval '0 secs')
--           from tt_deviation td
--               join tt.order_oper oo on td.next_order_round_id = oo.order_round_id
--               join tt.order_round oro on oo.order_round_id = oro.order_round_id
--               left join ibrd.round_stat rs on oro.round_id = rs.round_id and oo.stop_order_num = rs.stop_order_num
--           where oo.is_active = 1
--                 and oo.sign_deleted = 0
--                 and oo.time_oper + td.deviation < td.pass_time + ibrd.simple_forecaster_pkg$get_max_interval()
         ) t
    order by t.from_order_round_id,
        t.from_stop_place_order_num,
        t.round_order_num,
        t.stop_order_num;
end;
$$ language plpgsql security definer;

create or replace function ibrd.simple_forecaster_pkg$algorithm_1
    (p_stop_visits in jsonb)
    returns table(order_round_id            tt.order_round.order_round_id%type,
                  stop_place_muid           gis.stop_places.muid%type,
                  stop_place_order_num      gis.stop_place2route_traj.order_num%type,
                  creation_date             timestamp,
                  pass_time_forecast        timestamp,
                  from_order_round_id       tt.order_round.order_round_id%type,
                  from_stop_place_order_num gis.stop_place2route_traj.order_num%type)
as $$ declare
begin
    return query
    select *
    from ibrd.simple_forecaster_pkg$get_forecast_by_stop_visit(p_stop_visits);
end;
$$ language plpgsql security definer;

create or replace function ibrd.simple_forecaster_pkg$algorithm_2
    (p_stop_visits in jsonb)
    returns table(order_round_id            tt.order_round.order_round_id%type,
                  stop_place_muid           gis.stop_places.muid%type,
                  stop_place_order_num      gis.stop_place2route_traj.order_num%type,
                  creation_date             timestamp,
                  pass_time_forecast        timestamp,
                  from_order_round_id       tt.order_round.order_round_id%type,
                  from_stop_place_order_num gis.stop_place2route_traj.order_num%type)
as $$ declare
begin
    return query
    select *
    from ibrd.simple_forecaster_pkg$get_forecast_by_stop_visit_with_stat(p_stop_visits);
end;
$$ language plpgsql security definer;

create or replace function ibrd.simple_forecaster_pkg$get_next_order_round
    (p_order_round_id in tt.order_round.order_round_id%type)
    returns tt.order_round.order_round_id%type
as $$
declare
    l_order_date          tt.order_list.order_date%type;
    l_tr_id               core.tr.tr_id%type;
    l_shift               tt.order_list.shift%type;
    l_order_round_num     tt.order_round.order_round_num%type;

    l_next_order_round_id tt.order_round.order_round_id%type;
begin
    select
        ol.order_date,
        ol.tr_id,
        ol.shift,
        oro.order_round_num
    into l_order_date,
        l_tr_id,
        l_shift,
        l_order_round_num
    from tt.order_round oro
        join tt.order_list ol on oro.order_list_id = ol.order_list_id
    where oro.order_round_id = p_order_round_id;

    select oro.order_round_id
    into l_next_order_round_id
    from tt.order_list ol
        join tt.order_round oro on ol.order_list_id = oro.order_list_id
        join asd.oper_round_visit orv on oro.order_round_id = orv.order_round_id
    where ol.order_date = l_order_date
          and ol.tr_id = l_tr_id
          and ol.is_active = 1
          and oro.is_active = 1
          and (ol.shift, oro.order_round_num) > (l_shift, l_order_round_num)
    order by ol.shift, oro.order_round_num
    limit 1;

    return l_next_order_round_id;
end;
$$ language plpgsql security definer;


create or replace function ibrd.simple_forecaster_pkg$get_max_interval()
    returns interval
as $$
begin
  return interval '999 mins';
end;
$$ language plpgsql security definer;
