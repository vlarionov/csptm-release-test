﻿CREATE OR REPLACE FUNCTION ibrd.jf_info_board_statistic_report_pkg$of_rows(
  IN  p_id_account NUMERIC,
  OUT p_rows       REFCURSOR,
  IN  p_attr       TEXT)
  RETURNS REFCURSOR AS
$BODY$
DECLARE
  --   C_SET_INFO           INTEGER := ibrd.event_type_pkg$set_info();
  --   C_CONNECT            INTEGER := ibrd.event_type_pkg$connect();
  --   C_DISCONNECT         INTEGER := ibrd.event_type_pkg$disconnect();
  --   C_RESPONSE           INTEGER := ibrd.event_type_pkg$command_response();
  --   C_SET_CONFIG         INTEGER := ibrd.event_type_pkg$set_config();


  f_dtb_DT             TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'f_dtb_DT', TRUE);
  f_dte_DT             TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'f_dte_DT', TRUE);
  l_full_duration      BIGINT := extract(EPOCH FROM (f_dte_DT - f_dtb_DT));

  f_list_info_board_id BIGINT [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_info_board_id', TRUE);
  l_f_stop_location_id  integer := jofl.jofl_pkg$extract_number(p_attr, 'f_stop_location_id', true);
  f_ibrd_type_type_id  BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_ibrd_type_type_id', TRUE);
  f_model_id           BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_model_id', TRUE);
  f_group_id           integer := jofl.jofl_pkg$extract_number(p_attr, 'f_group_id', TRUE);
  f_state_id           BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_state_id', TRUE);

  /*
  •	Период с … по … - период, в который были получены пакеты данных с ошибками. По умолчанию текущие сутки;
  •	Код ИТОП – можно выбрать один или несколько ИТОП;
  •	Остановочный пункт – можно выбрать остановочный пункт, к которому относится базовое место посадки-высадки ИТОП;
  •	Тип ИТОП: Краснодиодное / Желтодиодное / Полноцветное – можно выбрать тип ИТОП, чтобы посмотреть список ИТОП выбранного типа (с учетом других фильтров). Если тип не выбран, то выборка осуществляется без учета типа ИТОП, т.е. по всем типам;
  •	Модель ИТОП – можно выбрать модель ИТОП, чтобы посмотреть список ИТОП выбранной модели. Если модель не выбрана, то выбора осуществляется без учета модели ИТОП;
  •	Группа ИТОП – можно выбрать группу, чтобы посмотреть сведения по ИТОП, входящим в группу.
  •	Статус ИТОП – можно выбрать статус ИТОП, чтобы посмотреть список ИТОП, которые на текущий момент времени находятся в выбранном статусе;
  */
BEGIN
  OPEN p_rows FOR
  WITH lastEvt0 AS (
      SELECT
        first_value(emax.event_date)
        OVER w AS last_dt,
        first_value(emax.event_type_id)
        OVER w AS last_event_type,
        emax.info_board_id
      FROM
        ibrd.event emax
      WINDOW w AS (
        PARTITION BY emax.info_board_id
        ORDER BY emax.event_date DESC, emax.event_id DESC
        ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
      )
  ), lastEvt AS (
      SELECT
        max(lastEvt0.last_dt)         lastDt,
        max(lastEvt0.last_event_type) lastEventTypeId,
        lastEvt0.info_board_id
      FROM lastEvt0
      GROUP BY lastEvt0.info_board_id
  ), e AS (
      SELECT
        e.event_id,
        e.info_board_id,
        e.event_type_id,
        e.event_date,
        e.channel_id,
        row_number()
        OVER (
          PARTITION BY e.info_board_id
          ORDER BY e.event_date, e.event_id ) - row_number()
        OVER (
          PARTITION BY e.info_board_id, e.event_type_id
          ORDER BY e.event_date, e.event_id ) rn
      FROM ibrd.event e
      WHERE 1 = 1
            AND e.event_date BETWEEN f_dtb_DT AND f_dte_DT
  ), channel AS (
      SELECT DISTINCT
        e.info_board_id,
        e.channel_id
      FROM e
  ), connection_duration AS (
      SELECT
        channel.info_board_id,
        SUM(ibrd.jf_info_board_statistic_report_pkg$channel_duration(channel.channel_id, f_dtb_DT, f_dte_DT)) connection_duration
      FROM channel
      GROUP BY channel.info_board_id
  ), connection_stats AS (
      SELECT
        connection_duration.info_board_id,
        connection_duration.connection_duration,
        round((100.0 * (connection_duration.connection_duration / l_full_duration)) :: NUMERIC, 2) connection_percent_duration
      FROM connection_duration
  ), evt AS (
      SELECT
        e.info_board_id,
        max(CASE e.event_type_id
            WHEN ibrd.event_type_pkg$connect()
              THEN e.event_date END) dt_connect,
        max(CASE e.event_type_id
            WHEN ibrd.event_type_pkg$disconnect()
              THEN e.event_date END) dt_disconnect,
        count(CASE e.event_type_id
              WHEN ibrd.event_type_pkg$connect()
                THEN 1 END)          cnt_connect,
        count(CASE e.event_type_id
              WHEN ibrd.event_type_pkg$disconnect()
                THEN 1 END)          cnt_disconnect,
        count(1)                     all_event,
        count(CASE
              WHEN e.event_type_id = ANY (ibrd.event_type_pkg$get_commands_responses())
                THEN 1 END)          all_response,
        count(CASE WHEN e.event_type_id = ANY (ibrd.event_type_pkg$get_commands())
          THEN 1 END)                all_command
      FROM e
      WHERE 1 = 1
      GROUP BY e.info_board_id
  )
    , re AS (
      SELECT
        e.info_board_id,
        count(CASE re.error_type_id
              WHEN 0
                THEN 1 END) success_command,
        count(CASE WHEN re.error_type_id > ibrd.error_type_pkg$null()
          THEN 1 END)       error_command
      FROM ibrd.event re
        JOIN e ON e.event_id = re.command_event_id
      WHERE 1 = 1
      GROUP BY e.info_board_id
  )
    , ns AS (
      SELECT
        e.info_board_id,
        count(1) not_sended
      FROM e
        JOIN ibrd.event_type et ON et.event_type_id = e.event_type_id
      WHERE 1 = 1
            AND e.event_type_id = ANY (ibrd.event_type_pkg$get_commands())
            AND NOT EXISTS(SELECT 1
                           FROM ibrd.event re
                           WHERE re.command_event_id = e.event_id)
      GROUP BY e.info_board_id
  ), t AS (

      SELECT
        ib.info_board_id,
        ib.code,
        S.name stop_name,
        ib.imei,
        t.name type_name,
        ib.serial_number,
        M.name model_name,
        ib.mobile_number,
        lastEvt.lastDt,
        lastEvt.lastEventTypeId,
        evt.cnt_connect,
        evt.cnt_disconnect,
        evt.all_event,
        evt.all_command,
        evt.all_response,
        re.success_command,
        re.error_command,
        ns.not_sended,
        connection_stats.connection_duration,
        connection_stats.connection_percent_duration
      FROM ibrd.info_board ib
        JOIN ibrd.model M ON M.model_id = ib.model_id
        JOIN ibrd.type t ON t.type_id = M.type_id
        LEFT JOIN ibrd.stop_location2info_board sl2ib ON sl2ib.info_board_id = ib.info_board_id AND sl2ib.is_base
        left join rts.stop_location sl on sl2ib.stop_location_id = sl.stop_location_id
        left join rts.stop s on sl.stop_id = s.stop_id
        LEFT JOIN lastEvt ON lastEvt.info_board_id = ib.info_board_id
        LEFT JOIN evt ON evt.info_board_id = ib.info_board_id
        LEFT JOIN re ON re.info_board_id = ib.info_board_id
        LEFT JOIN ns ON ns.info_board_id = ib.info_board_id
        LEFT JOIN connection_stats ON connection_stats.info_board_id = ib.info_board_id
      WHERE 1 = 1
            AND (ib.info_board_id = ANY (f_list_info_board_id) OR array_length(f_list_info_board_id, 1) IS NULL)
            AND (l_f_stop_location_id is null or sl.stop_location_id = l_f_stop_location_id)
            AND (M.type_id = f_ibrd_type_type_id OR f_ibrd_type_type_id IS NULL)
            AND (M.model_id = f_model_id OR f_model_id IS NULL)
            AND (ib.state_id = f_state_id OR f_state_id IS NULL)
            AND (f_group_id is null OR ib.info_board_id IN (SELECT ib2g.info_board_id
                                                           FROM ibrd.info_board2group ib2g where ib2g.group_id = f_group_id))
  )
  SELECT
    t.info_board_id,
    t.code,
    t.stop_name,
    t.imei,
    t.type_name,
    t.serial_number,
    t.model_name,
    t.mobile_number,
    t.lastDt,
    t.lastEventTypeId,
    t.cnt_connect,
    t.all_command,
    t.success_command,
    round(100.0 * t.success_command / t.all_command, 3) percent_success,
    t.error_command,
    round(100.0 * t.error_command / t.all_command, 3)   percent_error,
    t.not_sended,
    round(100.0 * t.not_sended / t.all_command, 3)      percent_not_sended,
    t.connection_duration,
    t.connection_percent_duration,
    -------------------------
    t.cnt_disconnect,
    t.all_event,
    t.all_response
  FROM t
  --   WHERE info_board_id IN (1, 2)
  ;

END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

CREATE OR REPLACE FUNCTION ibrd."jf_info_board_statistic_report_pkg$channel_duration"(
    p_channel_id text,
    p_start_date timestamp without time zone,
    p_end_date timestamp without time zone)
  RETURNS bigint AS
$BODY$
DECLARE
  l_result BIGINT;
BEGIN

WITH first_resp AS (
      select  first_resp.command_event_id
      FROM ibrd.event   first_resp    WHERE first_resp.channel_id = p_channel_id
	and first_resp.event_type_id = ANY (ibrd.event_type_pkg$get_commands_responses()) 
	order by first_resp.event_date limit 1
), start_date AS (
      SELECT greatest(connection_event.event_date, p_start_date) dt
      FROM ibrd.event connection_event
      join first_resp on connection_event.event_id=first_resp.command_event_id
      WHERE connection_event.channel_id = p_channel_id
), end_date AS (
      SELECT  least(max((last_response.event_date)), p_end_date) dt
      FROM ibrd.event last_response
      WHERE last_response.channel_id = p_channel_id
            AND (last_response.event_type_id = ibrd.event_type_pkg$disconnect()
                 OR last_response.event_type_id = ANY (ibrd.event_type_pkg$get_commands_responses()))
), range AS (
      SELECT 
       extract(EPOCH FROM (upper(tsrange(start_date.dt, end_date.dt)) - lower(tsrange(start_date.dt, end_date.dt)))) as channel_duration
        FROM start_date, end_date
        WHERE end_date.dt >= start_date.dt
)
select case when (select count(1) from range) = 0 then 0 else (select channel_duration from range) end  as channel_duration 
INTO l_result;

  RETURN l_result;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
