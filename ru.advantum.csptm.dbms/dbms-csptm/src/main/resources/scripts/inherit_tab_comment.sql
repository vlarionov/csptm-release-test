select concat('comment on table "', schname, '"."', relname, '" is ''', pcomment, ''';') tab_comm
FROM
  (
    SELECT
      h.*,
      t.relname,
      ns.nspname                   schname,
      p.relname                    parent_name,
      pns.nspname                  pschname,
      obj_description(h.inhrelid)  tcomment,
      replace(obj_description(h.inhparent), '''', '''''') pcomment
    FROM pg_inherits h, pg_class t, pg_class p, pg_namespace ns, pg_namespace pns
    WHERE h.inhrelid = t.OID
          AND h.inhparent = p.OID
          AND ns.oid = t.relnamespace
          AND pns.OID = p.relnamespace
          and obj_description(ns.oid) LIKE 'PCUIP%'

  ) q
where tcomment is null and pcomment is not null
order by relname
