/*
Закомментировал от греха подальше
TRUNCATE TABLE core.unit_bnsr CASCADE;
TRUNCATE TABLE core.sim_card2unit CASCADE;
TRUNCATE TABLE core.sim_card CASCADE;
TRUNCATE TABLE core.sim_card_group CASCADE;
TRUNCATE TABLE core.mobile_tariff CASCADE;
TRUNCATE TABLE core.sensor2unit CASCADE;
TRUNCATE TABLE core.sensor CASCADE;
TRUNCATE TABLE core.unit2tr CASCADE;
TRUNCATE TABLE core.tr CASCADE;
TRUNCATE TABLE core.tr_model CASCADE;
TRUNCATE TABLE core.tr_mark CASCADE;
TRUNCATE TABLE core.tr_schema_install CASCADE;
TRUNCATE TABLE core.unit CASCADE;
TRUNCATE TABLE core.equipment CASCADE;
TRUNCATE TABLE core.firmware CASCADE;
TRUNCATE TABLE core.equipment_model CASCADE;
TRUNCATE TABLE core.equipment_type CASCADE;
TRUNCATE TABLE core.equipment_class CASCADE;
TRUNCATE TABLE core.tr_capacity CASCADE;
TRUNCATE TABLE core.tr_status CASCADE;
TRUNCATE TABLE core.depo CASCADE;
TRUNCATE TABLE core.territory CASCADE;
TRUNCATE TABLE core.facility2type CASCADE;
TRUNCATE TABLE core.facility CASCADE;
TRUNCATE TABLE core.facility_type CASCADE;
TRUNCATE TABLE core.unit_service_type CASCADE;
TRUNCATE TABLE core.tr_type CASCADE;
TRUNCATE TABLE core.carrier CASCADE;
TRUNCATE TABLE core.entity CASCADE;
TRUNCATE TABLE core.equipment_status CASCADE;
TRUNCATE TABLE core.depo2territory CASCADE;
*/


update ifmp.i06_depo set tn_instance = '0001' where park_muid='2907228053288628463';
update ifmp.i06_depo set tn_instance = '0106' where park_muid='2907228056240702371';
update ifmp.i06_depo set tn_instance = '0109' where park_muid='2907228056240702388';
update ifmp.i06_depo set tn_instance = '0019' where park_muid='2907228056240702390';
update ifmp.i06_depo set tn_instance = '0003' where park_muid='2907228057135964625';
update ifmp.i06_depo set tn_instance = '0004' where park_muid='2907228057135964626';
update ifmp.i06_depo set tn_instance = '0011' where park_muid='2907228057135964632';
update ifmp.i06_depo set tn_instance = '0203' where park_muid='2907228056240702376';
update ifmp.i06_depo set tn_instance = '0103' where park_muid='2907228057135964640';
update ifmp.i06_depo set tn_instance = '0017' where park_muid='2907228057135964637';
update ifmp.i06_depo set tn_instance = '0015' where park_muid='2907228057135964635';
update ifmp.i06_depo set tn_instance = '0008' where park_muid='2907228056240702368';
update ifmp.i06_depo set tn_instance = '1016' where park_muid='2907228057135964636';
update ifmp.i06_depo set tn_instance = '0014' where park_muid='2907228057135964634';
update ifmp.i06_depo set tn_instance = '0108' where park_muid='2907228056240702373';


INSERT INTO core.facility_type (facility_type_id, name) VALUES (2, 'Сотовый оператор');
INSERT INTO core.facility_type (facility_type_id, name) VALUES (1, 'Производитель БО');
INSERT INTO core.facility_type (facility_type_id, name) VALUES (3, 'Подрядчик');
INSERT INTO core.facility_type (facility_type_id, name) VALUES (5, 'Производитель прошивки');
INSERT INTO core.facility_type (facility_type_id, name) VALUES (7, 'Обслуживающая компания');
INSERT INTO core.facility_type (facility_type_id, name) VALUES (4, 'Производитель ТС');
INSERT INTO core.facility_type (facility_type_id, name) VALUES (8, 'Производитель ИТОП');
INSERT INTO core.facility_type (facility_type_id, name) VALUES (9, 'Балансодержатель ИТОП');

INSERT INTO core.equipment_status (equipment_status_id, name, description) VALUES (1, 'Неопределен', 'Устанавливается для БО, статус которого по тем или иным причинам не представляется возможным определить. БО с данным статусом нуждается в проверке его состояния. ');
INSERT INTO core.equipment_status (equipment_status_id, name, description) VALUES (2, 'Исправно', 'БО находится в исправном состоянии. Установлено на ТС или находится на складе');
INSERT INTO core.equipment_status (equipment_status_id, name, description) VALUES (3, 'Неисправно', 'БО нуждается в ремонте, но не передано Подрядчику на ремонт');
INSERT INTO core.equipment_status (equipment_status_id, name, description) VALUES (4, 'Ремонт у Подрядчика', 'БО находится на ремонте у Подрядчика или в другом подразделении ГУП МГТ');
INSERT INTO core.equipment_status (equipment_status_id, name, description) VALUES (5, 'Выбыло', 'БО списано по причине морального устаревания, физического износа или выбыло по иным причинам ');
INSERT INTO core.equipment_status (equipment_status_id, name, description) VALUES (6, 'Временно демонтировано', 'БО снято с ТС на время проведения капитального ремонта ТС');
INSERT INTO core.equipment_status (equipment_status_id, name, description) VALUES (7, 'Ремонт в парке', 'Ремонт БО собственными силами в парке/депо');

INSERT INTO core.tr_type (tr_type_id, name, transport_kind_muid, short_name) VALUES (1, 'Автобус', 1, 'А');
INSERT INTO core.tr_type (tr_type_id, name, transport_kind_muid, short_name) VALUES (3, 'Трамвай', 3, 'Тм');
INSERT INTO core.tr_type (tr_type_id, name, transport_kind_muid, short_name) VALUES (4, 'Скоростной трамвай', 4, 'СТм');
INSERT INTO core.tr_type (tr_type_id, name, transport_kind_muid, short_name) VALUES (2, 'Троллейбус', 2, 'Тб');

INSERT INTO core.equipment_class (equipment_class_id, name) VALUES (0, 'Бортовой блок');
INSERT INTO core.equipment_class (equipment_class_id, name) VALUES (1, 'Датчик');
INSERT INTO core.equipment_class (equipment_class_id, name) VALUES (2, 'Другое');

INSERT INTO core.tr_status (tr_status_id, name) VALUES (1, 'Новый');
INSERT INTO core.tr_status (tr_status_id, name) VALUES (0, 'Не определен');
INSERT INTO core.tr_status (tr_status_id, name) VALUES (2, 'Старый');
INSERT INTO core.tr_status (tr_status_id, name) VALUES (4, 'Работают на линии');
INSERT INTO core.tr_status (tr_status_id, name) VALUES (5, 'Аварийное состояние');
INSERT INTO core.tr_status (tr_status_id, name) VALUES (6, 'Текущий ремонт');
INSERT INTO core.tr_status (tr_status_id, name) VALUES (7, 'Рекламация');
INSERT INTO core.tr_status (tr_status_id, name) VALUES (8, 'Долгосрочный ремонт');
INSERT INTO core.tr_status (tr_status_id, name) VALUES (9, 'Консервация');
INSERT INTO core.tr_status (tr_status_id, name) VALUES (10, 'Подготовка к списанию');
INSERT INTO core.tr_status (tr_status_id, name) VALUES (11, 'Капитальный ремонт');
INSERT INTO core.tr_status (tr_status_id, name) VALUES (12, 'Подготавливается для передачи в другую Транспортную организацию');


INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (2, 'Бортовой навигационно-связной терминал', 'БНСТ', 0, true);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (3, 'Датчик уровня топлива', 'ДУТ', 1, true);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (4, 'Автоматизированная система мониторинга пассажиропотоков', 'АСМПП', 1, true);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (9, 'Бортовая навигационной-связная радиостанция', 'БНСР', 0, true);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (10, 'Комплект бортового телематического оборудования безопасности', 'КБТОБ', 0, true);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (18, 'Камера наблюдения', 'Камера наблюдения', 1, true);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (55, 'Температурный датчик', 'ДТ', 1, true);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (6, 'Тревожная кнопка', 'ТК', 1, false);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (7, 'Датчик включения заднего хода', 'ДВ ЗХ', 1, false);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (5, 'Датчик задымления в кабине водителя', 'ИПК', 1, false);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (16, 'Модем', 'Модем', 2, false);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (19, 'Жесткий диск', 'ЖД', 2, true);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (13, 'Манипулятор БНСР', 'Манипулятор БНСР', 2, false);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (12, 'Манипулятор БНСТ', 'Манипулятор БНСТ', 2, false);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (14, 'Микрофон', 'Микрофон', 2, false);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (17, 'Монитор водителя', 'Монитор водителя', 2, true);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (54, 'Система автономного хода', 'САХ', 1, false);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (56, 'Aналоговый датчик', 'АналогДатчик', 1, false);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (57, 'Импульсный датчик', 'ИмпульсДатчик', 1, false);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (59, 'Цифровой выход', 'ЦифровойВых', 1, true);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (58, 'Цифровой вход', 'ЦифровойВход', 1, true);

/*
insert into core.facility_type(name)
select facility_type_name from ifmp.i01_facility_type;
*/
--select * from core.facility_type;

insert into core.sim_card_group(name)
select * from ifmp.i01_sim_card_group;

--select * from core.sim_card_group;
/*
insert into core.equipment_status(name, description)
  select name, description from ifmp.i02_equipment_status;
*/
/*
insert into core.tr_type(name, transport_kind_muid)
select short_name, transport_kind_muid from ifmp.i03_tr_type;
*/

INSERT INTO core.entity (name_short, name_full, comment)
select facility_name, name_full, comment
from ifmp.i05_facility;

--select * from core.entity;

INSERT INTO core.facility(facility_id)
select entity_id
from core.entity
join ifmp.i05_facility on entity.name_short = i05_facility.facility_name;


insert into core.tr_mark(name, facility_id)
select i04_tr_mark.name, entity.entity_id
from ifmp.i04_tr_mark
left join core.entity on i04_tr_mark.entity_name_short = entity.name_short;


--select * from core.tr_mark;
--  06
insert into core.entity(name_short, name_full) VALUES ('МГТ','МосГорТранс');
insert into core.carrier(carrier_id, sign_deleted, update_date) VALUES
 ((SELECT entity_id from core.entity where name_short = 'МГТ'), 0, CURRENT_DATE );

-- DEPO
insert into core.entity(name_short) select depo_name from ifmp.i06_depo;

insert into core.depo(
    depo_id,
    carrier_id,
    park_muid,
    tr_type_id,
    wkt_geom,
    tn_instance)
SELECT e.entity_id,
        (SELECT entity_id FROM core.entity WHERE name_short = 'МГТ'),
       depo.park_muid::BIGINT,
       core.tr_type.tr_type_id,
       wkt_geom,
       depo.tn_instance
FROM ifmp.i06_depo depo
JOIN core.entity e ON lower(e.name_short) = lower(depo.depo_name)
left JOIN core.tr_type ON core.tr_type.short_name = depo.tr_type_short_name;

-- 07 territory
insert into core.entity(name_short) select name_short from ifmp.i07_territory;


insert into core.territory (territory_id, code, dt_begin, wkt_geom)
SELECT e.entity_id, code, current_timestamp , wkt_geom
FROM ifmp.i07_territory i07
JOIN core.entity e ON lower(e.name_short) = lower(i07.name_short);


insert into core.depo2territory(territory_id, depo_id)
select entity_territory.entity_id, entity_depo.entity_id
from ifmp.i07_territory
    join core.entity entity_depo on lower(i07_territory.depo_name) = lower(entity_depo.name_short)
    join core.entity entity_territory on lower(i07_territory.name_short) = lower(entity_territory.name_short)
    left join core.territory on territory.territory_id = entity_territory.entity_id;
/*
-- 08 tr_status
INSERT INTO core.tr_status(name) SELECT name FROM ifmp.i08_tr_status;
*/
--select * from core.tr_status;

-- 09 tr_capacity
INSERT INTO core.tr_capacity(qty, full_name, short_name) SELECT qty, full_name, short_name FROM ifmp.i09_tr_capacity;

--select * from core.tr_capacity

-- 10 service_type
INSERT INTO core.unit_service_type(name) SELECT name FROM ifmp.i10_service_type;

-- 11 tr_model

--select * from ifmp.i11_tr_model;

--select * from core.tr_model;

INSERT INTO core.tr_model(name, tr_mark_id, tr_capacity_id, has_low_floor, has_conditioner, has_wheelchair_space,
                          has_bicicle_space, seat_qty, seat_qty_disabled_people, seat_qty_total)
SELECT i11.name, tr_mark_id, tr_capacity_id, has_low_floor, has_conditioner, has_wheelchair_space,
       has_bicicle_space, seat_qty, seat_qty_disabled_people, seat_qty_total
FROM ifmp.i11_tr_model i11
JOIN core.tr_mark ON lower(tr_mark.name) = lower(i11.tr_mark_name)
JOIN core.tr_capacity ON core.tr_capacity.short_name = i11.tr_capacity_short_name;

/*
-- 12 equipment_class
INSERT INTO core.equipment_class(name)
SELECT name FROM ifmp.i12_equipment_class;
*/
-- 13 equpment_type
/*
INSERT INTO core.equipment_type(name_full, name_short, equipment_class_id, has_diagnostic)
SELECT name_full, equipment_type_name, equipment_class_id, has_diagnostic FROM ifmp.i13_equipment_type
JOIN core.equipment_class ON lower(core.equipment_class.name) = lower(ifmp.i13_equipment_type.equipment_class_name);
*/

-- 14 equpment_model
INSERT INTO core.equipment_model(name, equipment_type_id, has_sim, has_diagnostic, input_digital, input_analog, facility_id)
SELECT i14.name, equipment_type_id, has_sim, i14.has_diagnostic, input_digital, input_analog, e.entity_id
FROM ifmp.i14_equipment_model i14
JOIN core.equipment_type ON lower(core.equipment_type.name_short) = lower(i14.equipment_type_name_short)
join core.entity e ON lower(e.name_short) = lower(i14.facility_name_short);


-- 15 firmware
INSERT INTO core.firmware(facility_id, equipment_model_id, version, dt, port, ip_address, comment, url_ftp,
                          path_ftp, ftp_user, ftp_user_password, name, file_name)
  SELECT e.entity_id, em.equipment_model_id, version, dt, port, ip_address, e.comment, url_ftp,
         path_ftp, ftp_user, ftp_user_password, i15.name, file_name
  FROM ifmp.i15_firmware i15
JOIN core.equipment_model em ON lower(i15.equipment_model_name) = lower(em.name)
JOIN core.entity e ON lower(e.name_short) = lower(i15.facility_name_short);

--  16 mobile tariff
INSERT INTO core.mobile_tariff(name, is_active, comment, facility_id)
SELECT i16.name, TRUE, i16.comment, e.entity_id FROM ifmp.i16_mobile_tariff i16
  JOIN core.entity e ON lower(e.name_short) = lower(i16.facility_name);

-- 17 sim card
INSERT INTO core.sim_card(dt_begin, iccid, phone_num, imsi, mobile_tariff_id,
                          sim_card_group_id, equipment_status_id, depo_id, territory_id)
  SELECT localtimestamp, iccid, phone_num, imsi, tariff.mobile_tariff_id,
         s_group.sim_card_group_id, equipment_status_id , d2t.depo_id, e.entity_id
      FROM ifmp.i17_sim_card i17
       JOIN core.mobile_tariff tariff ON lower(tariff.name)=lower(i17.mobile_tariff_name)
       JOIN core.sim_card_group s_group ON lower(s_group.name) = lower(i17.sim_card_group_name)
       JOIN core.equipment_status e_status ON lower(e_status.name) = lower(i17.equipment_status_name)
       JOIN core.entity e ON lower(e.name_short) = lower(i17.territory_name_short)
       JOIN core.depo2territory d2t ON e.entity_id = d2t.territory_id;

-- 18 equipment
insert into core.equipment
(
    firmware_id,
    unit_service_type_id,
    facility_id,
    equipment_status_id,
    dt_begin,
    serial_num,
    equipment_model_id,
    depo_id,
    territory_id,
    unit_service_facility_id,
    comment,
    external_id
 )
select
    firmware.firmware_id,
    unit_service_type.unit_service_type_id,
    equipment_model.facility_id,
    equipment_status.equipment_status_id,
    now(),
    serial_num,
    equipment_model.equipment_model_id,
    depo2territory.depo_id,
    entity.entity_id,
    facility.entity_id,
    i18_equipment.comment,
    i18_equipment.equipment_id
from  ifmp.i18_equipment
    join core.equipment_model on equipment_model.name = i18_equipment.equipment_model_name
    join core.unit_service_type on unit_service_type.name = i18_equipment.service_type_name
    join core.equipment_status on equipment_status.name =  i18_equipment.equipment_status_name
    join core.entity on i18_equipment.territory_name_short = entity.name_short
    join core.depo2territory on depo2territory.territory_id = entity.entity_id
    left join core.entity facility ON facility.name_short = i18_equipment.unit_service_facility_name_short
    LEFT JOIN core.firmware on firmware.version = i18_equipment.firmware_version;


--select count(*) from core.equipment;

-- 35 tr_schema_install
INSERT INTO core.tr_schema_install(tr_type_id, tr_capacity_id, install_name, install_desc, dt_begin, is_active)
  SELECT tr_type_id, tr_capacity_id, install_name, install_desc, now(), true
  FROM ifmp.i35_tr_schema_install i35
  JOIN core.tr_type ON lower(core.tr_type.short_name) = lower(i35.tr_type_name)
  JOIN core.tr_capacity cap ON lower(cap.short_name) = lower(i35.capacity_name);

-- 18 tr

INSERT INTO core.tr(seat_qty, has_low_floor, has_conditioner, garage_num, licence, serial_num, dt_begin,
                    tr_type_id, tr_status_id, tr_model_id, territory_id, has_wheelchair_space,
                    has_bicicle_space, seat_qty_disabled_people, seat_qty_total, year_of_issue, garage_num_add,
                    equipment_afixed, tr_schema_install_id, depo_id)
SELECT seat_qty, has_low_floor, has_conditioner, garage_num, licence, serial_num, i18.dt_begin,
       type.tr_type_id, tr_status_id, tr_model_id, e.entity_id, has_wheelchair_space,
       has_bicicle_space, seat_qty_disabled_people, seat_qty_total, year_of_issue, garage_num_add,
       equipment_afixed, tr_schema_install_id, depo.entity_id
from ifmp.i18_tr i18
JOIN core.tr_model model ON lower(model.name) = lower(i18.tr_model_name)
JOIN core.tr_type type ON lower(type.short_name) = lower(i18.tr_type_short_name)
JOIN core.tr_status status ON lower(status.name) = lower(i18.tr_status_name)
JOIN core.entity e ON lower(e.name_short) = lower(i18.territory_name_short)
join core.tr_schema_install on tr_schema_install.install_name = i18.install_name
join core.entity depo on depo.name_short = i18.depo_name;


--select count(*) from core.tr;
--select * from ifmp.i18_tr i18;

-- 19 unit
INSERT INTO core.unit(unit_id, unit_num, hub_id)
  SELECT equipment.equipment_id, i19_unit.unit_num, i19_unit.hub_id
  FROM ifmp.i19_unit join core.equipment on i19_unit.equipment_id::text = equipment.external_id;

--select count(*) from core.unit;


-- 20 unit2tr
INSERT INTO core.unit2tr(tr_id, unit_id)
SELECT tr.tr_id, equipment.equipment_id
FROM ifmp.i20_unit2tr i20
    join core.entity on entity.name_short = i20.depo_name
    JOIN core.tr tr ON tr.garage_num = i20.garage_num and entity.entity_id = tr.depo_id
    join core.equipment on i20.equipment_id::text = equipment.external_id
    join core.unit on equipment.equipment_id = unit.unit_id;


--select count(*) from core.unit2tr;


-- 21 sensor2unit_model
INSERT INTO core.sensor2unit_model(num, equipment_type_sensor_id, equipment_type_unit_id)
  SELECT num, et1.equipment_type_id, et2.equipment_type_id  FROM ifmp.i21_sensor2unit_model i21
      JOIN core.equipment_type et1 ON lower(et1.name_short) = lower(i21.equipment_type_name_sensor)
      JOIN core.equipment_type et2 ON lower(et2.name_short) = lower(i21.equipment_type_name_unit);

-- 22 sensor2unit
INSERT INTO core.sensor(sensor_id, sensor_num)
  SELECT equipment_sensor.equipment_id, sensor_num
  FROM ifmp.i22_sensor2unit
       join core.equipment equipment_sensor on i22_sensor2unit.equipment_id_sensor::text = equipment_sensor.external_id;

-- INSERT INTO core.sensor(sensor_id, sensor_num)
SELECT equipment_sensor.equipment_id, sensor_num
FROM ifmp.i22_sensor2unit i22 join core.equipment equipment_sensor on
     i22.equipment_id_sensor::text = equipment_sensor.external_id
     WHERE NOT EXISTS (SELECT 1 from core.sensor snr
           WHERE snr.sensor_id = equipment_sensor.equipment_id
           AND snr.sensor_num = i22.sensor_num);

INSERT INTO core.sensor2unit(sensor_id, unit_id, sensor_num)
  SELECT equipment_sensor.equipment_id, equipment_unit.equipment_id, sensor_num
  FROM ifmp.i22_sensor2unit
       join core.equipment equipment_unit on i22_sensor2unit.equipment_id_unit::text = equipment_unit.external_id
       join core.equipment equipment_sensor on i22_sensor2unit.equipment_id_sensor::text = equipment_sensor.external_id;

-- INSERT INTO core.sensor2unit(sensor_id, unit_id, sensor_num)
SELECT equipment_sensor.equipment_id, equipment_unit.equipment_id, sensor_num
FROM ifmp.i22_sensor2unit i22
  join core.equipment equipment_unit on i22.equipment_id_unit::text = equipment_unit.external_id
  join core.equipment equipment_sensor on i22.equipment_id_sensor::text = equipment_sensor.external_id
  WHERE NOT EXISTS (SELECT 1 FROM core.sensor2unit s2u
        WHERE s2u.sensor_id = equipment_sensor.equipment_id
        AND s2u.unit_id = equipment_unit.equipment_id
        AND s2u.sensor_num = i22.sensor_num);

-- 24 sensor2tr
INSERT INTO core.sensor2tr(sensor_id, tr_id)
  SELECT equipment.equipment_id, tr_id
  FROM ifmp.i24_sensor2tr i24
      join core.entity on entity.name_short = i24.depo_name
      JOIN core.tr tr ON tr.garage_num = i24.garage_num and entity.entity_id = tr.depo_id
      join core.equipment on i24.equipment_id::text = equipment.external_id;

-- INSERT INTO core.sensor2tr(sensor_id, tr_id)
SELECT equipment.equipment_id, tr_id
FROM ifmp.i24_sensor2tr i24
  join core.entity on entity.name_short = i24.depo_name
  JOIN core.tr tr ON tr.garage_num = i24.garage_num and entity.entity_id = tr.depo_id
  join core.equipment on i24.equipment_id::text = equipment.external_id
  WHERE NOT EXISTS(SELECT 1
                   FROM core.sensor2tr s2tr
                   WHERE s2tr.sensor_id = equipment.equipment_id
                   AND s2tr.tr_id = tr.tr_id);

-- 25 sim_card2unit
INSERT INTO core.sim_card2unit(sim_card_id, equipment_id)
  SELECT card.sim_card_id, equipment.equipment_id
  FROM ifmp.i25_sim_card2unit i25
  JOIN core.sim_card card ON  card.iccid = i25.iccid
  join core.equipment on i25.equipment_id::text = equipment.external_id;


--select * from ifmp.i25_sim_card2unit;

-- 26 facility2type
INSERT INTO core.facility2type(facility_id, facility_type_id)
  SELECT entity_id, facility_type_id FROM ifmp.i26_facility2type i26
  JOIN core.entity e ON lower(e.name_short) = lower(i26.facility_name)
  JOIN core.facility_type t ON lower(t.name) = lower(i26.facility_type_name);


--select * from core.facility2type;
-- 27 modem
INSERT INTO core.modem(modem_id, imei, speed, sim_card_id)
SELECT equipment.equipment_id, imei, speed, card.sim_card_id FROM ifmp.i27_modem i27
    JOIN core.sim_card card ON  card.iccid = i27.iccid
    join core.equipment on i27.equipment_id::text = equipment.external_id;


-- 28 modem2unit
INSERT INTO core.modem2unit(unit_id, modem_id)
  SELECT equipment_unit.equipment_id, equipment_modem.equipment_id
  FROM ifmp.i28_modem2unit
  join core.equipment equipment_unit on i28_modem2unit.equipment_id_unit::text = equipment_unit.external_id
  join core.equipment equipment_modem on i28_modem2unit.equipment_id_modem::text = equipment_modem.external_id;


--select * from ifmp.i28_modem2unit;
-- 29 hdd
INSERT INTO core.hdd(type, capacity, size, hdd_id)
  SELECT type, capacity, size, equipment.equipment_id
  FROM ifmp.i29_hdd
  join core.equipment on i29_hdd.equipment_id::text = equipment.external_id;

-- 29 monitor2unit
INSERT INTO core.monitor2unit(unit_id, monitor_id)
  SELECT equipment_monitor.equipment_id, equipment_monitor.equipment_id
  FROM ifmp.i29_monitor2unit
  join core.equipment equipment_unit on i29_monitor2unit.equipment_id_unit::text = equipment_unit.external_id
  join core.equipment equipment_monitor on i29_monitor2unit.equipment_id_monitor::text = equipment_monitor.external_id;
  ;

-- 30 hdd2unit
INSERT INTO core.hdd2unit(unit_id, hdd_id)
  SELECT equipment_unit.equipment_id, equipment_hdd.equipment_id
  FROM ifmp.i30_hdd2unit
  join core.equipment equipment_unit on i30_hdd2unit.equipment_id_unit::text = equipment_unit.external_id
  join core.equipment equipment_hdd on i30_hdd2unit.equipment_id_hdd::text = equipment_hdd.external_id;



-- 36 tr_schema_install_unit
INSERT INTO core.tr_schema_install_unit(equipment_type_id, tr_schema_install_id, is_main)
  SELECT e_type.equipment_type_id ,s_install.tr_schema_install_id ,is_main FROM ifmp.i36_tr_schema_install_unit i36
  JOIN core.tr_schema_install s_install ON lower(s_install.install_name) = lower(i36.install_name)
  JOIN core.equipment_type e_type ON lower(e_type.name_short) = lower(i36.equipment_type_name);

--37
insert into core.tr_schema_install_detail
(sensor2unit_model_id, tr_schema_install_unit_id, is_required, install_desc)
select d.sensor2unit_model_id, tr_schema_install_unit_id, a.is_required, a.install_desc
from ifmp.i37_tr_schema_install_detail a
join core.equipment_type on a.equipment_type_name_unit = equipment_type.name_short
join core.tr_schema_install_unit b on b.equipment_type_id = equipment_type.equipment_type_id
join core.tr_schema_install c on c.install_name = a.install_name
join core.sensor2unit_model d on d.equipment_type_unit_id = equipment_type.equipment_type_id and d.num = a.num

--i38_unit_bnsr
INSERT INTO core.unit_bnsr(
 unit_bnsr_id,
 channel_num,
 has_manipulator
)
 SELECT equipment.equipment_id,
        channel_num,
        has_manipulator
 FROM  ifmp.i38_unit_bnsr
 join core.equipment on i38_unit_bnsr.equipment_id::text = equipment.external_id;


insert into core.hdd(hdd_id)
  select equipment_id from core.equipment
    join core.equipment_model on equipment.equipment_model_id = equipment_model.equipment_model_id
  where equipment_model.equipment_type_id = 12;


select * from core.tr where tr_id = 85491;