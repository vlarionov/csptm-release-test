with a as (
    SELECT
      c.table_name,
      obj_description(pgd.objoid) as table_desc,
      c.column_name,
      pgd.description as column_desc,
      c.data_type,
      c.is_nullable,
      c.table_schema
    FROM pg_catalog.pg_statio_all_tables AS st
      LEFT JOIN pg_catalog.pg_description pgd ON (pgd.objoid = st.relid)
      INNER JOIN information_schema.columns c ON (pgd.objsubid = c .ordinal_position
                                                  AND c.table_schema = st.schemaname AND c.table_name = st.relname)
    WHERE c.table_schema = 'ifmp'
),
pk as
  (
      SELECT
        c.column_name,
        c.table_name
      FROM

        information_schema.table_constraints tc
        JOIN information_schema.constraint_column_usage AS ccu USING (constraint_schema, constraint_name)
        JOIN information_schema.columns AS c
          ON c.table_schema = tc.constraint_schema AND tc.table_name = c.table_name AND ccu.column_name = c.column_name
      WHERE constraint_type = 'PRIMARY KEY' and tc.constraint_schema = 'ifmp'
  )
select         a.table_name,
                a.table_desc,
                a.column_name,
                a.column_desc,
                a.data_type,
                a.is_nullable,
  coalesce((select true from pk where pk.table_name = a.table_name and pk.column_name = a.column_name limit 1 ), false) is_unic
from a
ORDER BY 1, 7 desc, 2;



with a as (
    SELECT
      c.table_name,
      obj_description(pgd.objoid) as table_desc,
      c.column_name,
      pgd.description as column_desc,
      c.data_type,
      c.is_nullable,
      c.table_schema,
      c.ordinal_position
    FROM pg_catalog.pg_statio_all_tables AS st
      LEFT JOIN pg_catalog.pg_description pgd ON (pgd.objoid = st.relid)
      INNER JOIN information_schema.columns c ON (pgd.objsubid = c .ordinal_position
                                                  AND c.table_schema = st.schemaname AND c.table_name = st.relname)
    WHERE c.table_schema = 'ifmp'
),
    pk as
  (
      SELECT
        c.column_name,
        c.table_name
      FROM

        information_schema.table_constraints tc
        JOIN information_schema.constraint_column_usage AS ccu USING (constraint_schema, constraint_name)
        JOIN information_schema.columns AS c
          ON c.table_schema = tc.constraint_schema AND tc.table_name = c.table_name AND ccu.column_name = c.column_name
      WHERE constraint_type = 'PRIMARY KEY' and tc.constraint_schema = 'ifmp'
  ),

x as (SELECT
        a.table_name,
        a.table_desc,
        a.column_name,
        a.column_desc,
        a.data_type,
        a.is_nullable,
        coalesce((SELECT TRUE
                  FROM pk
                  WHERE pk.table_name = a.table_name AND pk.column_name = a.column_name
                  LIMIT 1), FALSE) is_unic
      FROM a
      ORDER BY 1, 7 DESC, a.ordinal_position asc, 2
  )
select
  'csptm_dev_ifmp_'||x.table_name,
  string_agg(x.column_name, ',')
from x
GROUP BY x.table_name
order by x.table_name;




--Создание констрейна
SELECT
  'ALTER TABLE '||tc.table_schema||'.'||tc.table_name||' ADD CONSTRAINT '||tc.constraint_name||' FOREIGN KEY ('||kcu.column_name||') REFERENCES '||ccu.table_schema||'.'||ccu.table_name||' ('||ccu.column_name||');'
FROM
  information_schema.table_constraints AS tc
  JOIN information_schema.key_column_usage AS kcu
    ON tc.constraint_name = kcu.constraint_name
  JOIN information_schema.constraint_column_usage AS ccu
    ON ccu.constraint_name = tc.constraint_name
WHERE constraint_type = 'FOREIGN KEY' AND ccu.table_name='account' AND ccu.table_schema='core';;

--Удаление констрейна
SELECT
  'ALTER TABLE '||tc.table_schema||'.'||tc.table_name||' DROP CONSTRAINT '||tc.constraint_name||';'
FROM
  information_schema.table_constraints AS tc
  JOIN information_schema.key_column_usage AS kcu
    ON tc.constraint_name = kcu.constraint_name
  JOIN information_schema.constraint_column_usage AS ccu
    ON ccu.constraint_name = tc.constraint_name
WHERE constraint_type = 'FOREIGN KEY' AND ccu.table_name='account' AND ccu.table_schema='core';