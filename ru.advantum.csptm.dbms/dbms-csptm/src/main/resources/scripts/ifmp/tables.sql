
DROP SCHEMA IF EXISTS ifmp cascade;
create schema ifmp;

/*
Created: 7/12/2016
Modified: 9/21/2017
Project: csptm
Model: Integration
Database: PostgreSQL 9.4
*/

-- Create tables section -------------------------------------------------

-- Table ifmp.i02_equipment_status

CREATE TABLE ifmp.i02_equipment_status(
 name Text NOT NULL,
 description Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i02_equipment_status IS 'Статус БО'
;
COMMENT ON COLUMN ifmp.i02_equipment_status.name IS 'Статус'
;
COMMENT ON COLUMN ifmp.i02_equipment_status.description IS 'Комментарий'
;

-- Add keys for table ifmp.i02_equipment_status

ALTER TABLE ifmp.i02_equipment_status ADD CONSTRAINT pk_equipment_status PRIMARY KEY (name)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_equipment_status ON ifmp.i02_equipment_status IS NULL
;

-- Table ifmp.i13_equipment_type

CREATE TABLE ifmp.i13_equipment_type(
 equipment_type_name Text NOT NULL,
 equipment_class_name Text,
 name_full Text NOT NULL,
 has_diagnostic Boolean DEFAULT true NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i13_equipment_type IS 'Тип БО'
;
COMMENT ON COLUMN ifmp.i13_equipment_type.equipment_type_name IS 'Наименование сокращенное'
;
COMMENT ON COLUMN ifmp.i13_equipment_type.equipment_class_name IS 'Класс типа БО'
;
COMMENT ON COLUMN ifmp.i13_equipment_type.name_full IS 'Наименование типа полное'
;
COMMENT ON COLUMN ifmp.i13_equipment_type.has_diagnostic IS 'Диагностика включена'
;

-- Create indexes for table ifmp.i13_equipment_type

CREATE INDEX IX_Relationship191 ON ifmp.i13_equipment_type (equipment_class_name)
;
COMMENT ON INDEX ifmp.IX_Relationship191 IS NULL
;

-- Add keys for table ifmp.i13_equipment_type

ALTER TABLE ifmp.i13_equipment_type ADD CONSTRAINT pk_equipment_type_equipment_type_id PRIMARY KEY (equipment_type_name)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_equipment_type_equipment_type_id ON ifmp.i13_equipment_type IS NULL
;

-- Table ifmp.i16_mobile_tariff

CREATE TABLE ifmp.i16_mobile_tariff(
 name Text NOT NULL,
 comment Text,
 facility_name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i16_mobile_tariff IS 'Тариф мобильной связи'
;
COMMENT ON COLUMN ifmp.i16_mobile_tariff.name IS 'Тарифный план'
;
COMMENT ON COLUMN ifmp.i16_mobile_tariff.comment IS 'Комментарий'
;
COMMENT ON COLUMN ifmp.i16_mobile_tariff.facility_name IS 'Сотовый оператор (Организация)'
;

-- Create indexes for table ifmp.i16_mobile_tariff

CREATE INDEX IX_Relationship201 ON ifmp.i16_mobile_tariff (facility_name)
;
COMMENT ON INDEX ifmp.IX_Relationship201 IS NULL
;

-- Add keys for table ifmp.i16_mobile_tariff

ALTER TABLE ifmp.i16_mobile_tariff ADD CONSTRAINT pk_mobile_tariff_mobile_tariff_id PRIMARY KEY (name)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_mobile_tariff_mobile_tariff_id ON ifmp.i16_mobile_tariff IS NULL
;

-- Table ifmp.i01_sim_card_group

CREATE TABLE ifmp.i01_sim_card_group(
 name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i01_sim_card_group IS 'Группа sim-карт'
;
COMMENT ON COLUMN ifmp.i01_sim_card_group.name IS 'Наименование группы sim-карт'
;

-- Add keys for table ifmp.i01_sim_card_group

ALTER TABLE ifmp.i01_sim_card_group ADD CONSTRAINT pk_sim_card_group_sim_card_group_id PRIMARY KEY (name)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_sim_card_group_sim_card_group_id ON ifmp.i01_sim_card_group IS NULL
;

-- Create tables section -------------------------------------------------

-- Table ifmp.i15_firmware

CREATE TABLE ifmp.i15_firmware(
 version Text NOT NULL,
 equipment_model_name Text,
 dt Timestamp NOT NULL,
 facility_name_short Text NOT NULL,
 port Text,
 ip_address Text,
 comment Text,
 url_ftp Text,
 path_ftp Text,
 ftp_user Text,
 ftp_user_password Text,
 name Text NOT NULL,
 file_name Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i15_firmware IS 'Прошивка БО'
;
COMMENT ON COLUMN ifmp.i15_firmware.version IS 'Версия прошивки'
;
COMMENT ON COLUMN ifmp.i15_firmware.equipment_model_name IS 'Модель оборудования'
;
COMMENT ON COLUMN ifmp.i15_firmware.dt IS 'Дата прошивки'
;
COMMENT ON COLUMN ifmp.i15_firmware.facility_name_short IS 'Производитель прошивки'
;
COMMENT ON COLUMN ifmp.i15_firmware.port IS 'Порт FTP'
;
COMMENT ON COLUMN ifmp.i15_firmware.ip_address IS 'IP-адрес  FTP'
;
COMMENT ON COLUMN ifmp.i15_firmware.comment IS 'Комментарий'
;
COMMENT ON COLUMN ifmp.i15_firmware.url_ftp IS 'URL адрес FTP'
;
COMMENT ON COLUMN ifmp.i15_firmware.path_ftp IS 'Путь к папке прошивки '
;
COMMENT ON COLUMN ifmp.i15_firmware.ftp_user IS 'Имя пользователя FTP'
;
COMMENT ON COLUMN ifmp.i15_firmware.ftp_user_password IS 'Пароль пользователя FTP'
;
COMMENT ON COLUMN ifmp.i15_firmware.name IS 'Наименование прошивки'
;
COMMENT ON COLUMN ifmp.i15_firmware.file_name IS 'Ссылка на файл'
;

-- Create indexes for table ifmp.i15_firmware

CREATE INDEX IX_Relationship195 ON ifmp.i15_firmware (equipment_model_name)
;

CREATE INDEX IX_Relationship196 ON ifmp.i15_firmware (facility_name_short)
;
COMMENT ON INDEX ifmp.IX_Relationship195 IS NULL
;
COMMENT ON INDEX ifmp.IX_Relationship196 IS NULL
;

-- Add keys for table ifmp.i15_firmware

ALTER TABLE ifmp.i15_firmware ADD CONSTRAINT pk_firmware PRIMARY KEY (version)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_firmware ON ifmp.i15_firmware IS NULL
;

-- Create tables section -------------------------------------------------

-- Table ifmp.i38_unit_bnsr

CREATE TABLE IF NOT EXISTS ifmp.i38_unit_bnsr(
 equipment_id Bigint NOT NULL,
 channel_num Integer NOT NULL,
 has_manipulator Integer DEFAULT 0,
 usw_num Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i38_unit_bnsr IS 'Бортовой блок:БНСР'
;
COMMENT ON COLUMN ifmp.i38_unit_bnsr.equipment_id IS 'Идентификатор блока'
;
COMMENT ON COLUMN ifmp.i38_unit_bnsr.channel_num IS 'Номер канала'
;
COMMENT ON COLUMN ifmp.i38_unit_bnsr.has_manipulator IS 'Манипулятор к комплекте'
;
COMMENT ON COLUMN ifmp.i38_unit_bnsr.usw_num IS 'Номер радиостанции'
;

-- Add keys for table ifmp.i38_unit_bnsr

ALTER TABLE ifmp.i38_unit_bnsr ADD CONSTRAINT pk_unit_bnsr_id PRIMARY KEY (equipment_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_unit_bnsr_id ON ifmp.i38_unit_bnsr IS NULL
;

-- Create tables section -------------------------------------------------

-- Table ifmp.i17_sim_card

CREATE TABLE ifmp.i17_sim_card(
 iccid Text NOT NULL,
 phone_num Text NOT NULL,
 imsi Text,
 sim_card_group_name Text NOT NULL,
 territory_name_short Text NOT NULL,
 mobile_tariff_name Text NOT NULL,
 equipment_status_name Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i17_sim_card IS 'СИМ карты'
;
COMMENT ON COLUMN ifmp.i17_sim_card.iccid IS 'Номер sim-карты (ICCID)'
;
COMMENT ON COLUMN ifmp.i17_sim_card.phone_num IS 'Номер телефона'
;
COMMENT ON COLUMN ifmp.i17_sim_card.imsi IS 'IMSI'
;
COMMENT ON COLUMN ifmp.i17_sim_card.sim_card_group_name IS 'Группа sim-карт'
;
COMMENT ON COLUMN ifmp.i17_sim_card.territory_name_short IS 'Территория ТП'
;
COMMENT ON COLUMN ifmp.i17_sim_card.mobile_tariff_name IS 'Наименование тарифа'
;
COMMENT ON COLUMN ifmp.i17_sim_card.equipment_status_name IS 'Статус оборудования'
;

-- Create indexes for table ifmp.i17_sim_card

CREATE INDEX IX_Relationship202 ON ifmp.i17_sim_card (sim_card_group_name)
;

CREATE INDEX IX_Relationship204 ON ifmp.i17_sim_card (territory_name_short)
;

CREATE INDEX IX_Relationship205 ON ifmp.i17_sim_card (mobile_tariff_name)
;

CREATE INDEX IX_Relationship206 ON ifmp.i17_sim_card (equipment_status_name)
;
COMMENT ON INDEX ifmp.IX_Relationship202 IS NULL
;
COMMENT ON INDEX ifmp.IX_Relationship204 IS NULL
;
COMMENT ON INDEX ifmp.IX_Relationship205 IS NULL
;
COMMENT ON INDEX ifmp.IX_Relationship206 IS NULL
;

-- Add keys for table ifmp.i17_sim_card

ALTER TABLE ifmp.i17_sim_card ADD CONSTRAINT pk_sim_card_sim_card_id PRIMARY KEY (iccid)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_sim_card_sim_card_id ON ifmp.i17_sim_card IS NULL
;

-- Create tables section -------------------------------------------------

-- Table ifmp.i25_sim_card2unit

CREATE TABLE ifmp.i25_sim_card2unit(
 equipment_id Bigint,
 iccid Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i25_sim_card2unit IS 'Sim карта в блоке'
;
COMMENT ON COLUMN ifmp.i25_sim_card2unit.equipment_id IS NULL
;
COMMENT ON COLUMN ifmp.i25_sim_card2unit.iccid IS NULL
;

-- Create indexes for table ifmp.i25_sim_card2unit

CREATE INDEX IX_Relationship228 ON ifmp.i25_sim_card2unit (iccid)
;
COMMENT ON INDEX ifmp.IX_Relationship228 IS NULL
;

-- Create tables section -------------------------------------------------

-- Table ifmp.i19_unit

CREATE TABLE ifmp.i19_unit(
 equipment_id Bigint NOT NULL,
 unit_num Text NOT NULL,
 hub_id Integer
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i19_unit IS 'Бортовой блок'
;
COMMENT ON COLUMN ifmp.i19_unit.equipment_id IS 'Идентификатор оборудования'
;
COMMENT ON COLUMN ifmp.i19_unit.unit_num IS 'Уникальный номер'
;
COMMENT ON COLUMN ifmp.i19_unit.hub_id IS 'Сервис приема-передачи данных'
;

-- Create indexes for table ifmp.i19_unit

CREATE UNIQUE INDEX uq_exp_unit_unit_num_hub_id ON ifmp.i19_unit (unit_num,hub_id)
TABLESPACE core_idx
;

CREATE INDEX IX_Relationship200 ON ifmp.i19_unit (equipment_id)
;
COMMENT ON INDEX ifmp.uq_exp_unit_unit_num_hub_id IS NULL
;
COMMENT ON INDEX ifmp.IX_Relationship200 IS NULL
;

-- Add keys for table ifmp.i19_unit

ALTER TABLE ifmp.i19_unit ADD CONSTRAINT pk_unit PRIMARY KEY (equipment_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_unit ON ifmp.i19_unit IS NULL
;

-- Table ifmp.i10_service_type

CREATE TABLE ifmp.i10_service_type(
 name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i10_service_type IS 'Вид тех. обслуживания_const'
;
COMMENT ON COLUMN ifmp.i10_service_type.name IS 'Вид тех. обслуживания'
;

-- Add keys for table ifmp.i10_service_type

ALTER TABLE ifmp.i10_service_type ADD CONSTRAINT pk_unit_service_type PRIMARY KEY (name)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_unit_service_type ON ifmp.i10_service_type IS NULL
;

-- Table ifmp.i07_territory

CREATE TABLE ifmp.i07_territory(
 name_short Text NOT NULL,
 code Integer NOT NULL,
 wkt_geom Text,
 depo_name Text
)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i07_territory IS 'Территория'
;
COMMENT ON COLUMN ifmp.i07_territory.name_short IS 'Наименование территории'
;
COMMENT ON COLUMN ifmp.i07_territory.code IS 'Номер территории'
;
COMMENT ON COLUMN ifmp.i07_territory.wkt_geom IS 'Координаты'
;
COMMENT ON COLUMN ifmp.i07_territory.depo_name IS 'Транспортное предприятие'
;

-- Create indexes for table ifmp.i07_territory

CREATE INDEX IX_Relationship208 ON ifmp.i07_territory (depo_name)
;
COMMENT ON INDEX ifmp.IX_Relationship208 IS NULL
;

-- Add keys for table ifmp.i07_territory

ALTER TABLE ifmp.i07_territory ADD CONSTRAINT pk_terrirtory_id PRIMARY KEY (name_short)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_terrirtory_id ON ifmp.i07_territory IS NULL
;

-- Table ifmp.i18_tr

CREATE TABLE ifmp.i18_tr(
 garage_num Integer NOT NULL,
 depo_name Text NOT NULL,
 tr_status_name Text,
 territory_name_short Text,
 tr_model_name Text,
 tr_type_short_name Text,
 licence Text,
 serial_num Text,
 dt_begin Timestamp NOT NULL,
 year_of_issue Integer NOT NULL,
 garage_num_add Integer,
 equipment_afixed Text,
 install_name Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i18_tr IS 'Транспортное средство'
;
COMMENT ON COLUMN ifmp.i18_tr.garage_num IS 'Гаражный номер'
;
COMMENT ON COLUMN ifmp.i18_tr.depo_name IS 'Наименование тр. предпиятия'
;
COMMENT ON COLUMN ifmp.i18_tr.tr_status_name IS 'Статус ТС'
;
COMMENT ON COLUMN ifmp.i18_tr.territory_name_short IS 'Территория'
;
COMMENT ON COLUMN ifmp.i18_tr.tr_model_name IS 'Модель'
;
COMMENT ON COLUMN ifmp.i18_tr.tr_type_short_name IS 'Вид ТС'
;
COMMENT ON COLUMN ifmp.i18_tr.licence IS 'Государственный номер'
;
COMMENT ON COLUMN ifmp.i18_tr.serial_num IS 'Заводской (серийный) номер'
;
COMMENT ON COLUMN ifmp.i18_tr.dt_begin IS 'Дата ввода в эксплуатацию'
;
COMMENT ON COLUMN ifmp.i18_tr.year_of_issue IS 'Год выпуска'
;
COMMENT ON COLUMN ifmp.i18_tr.garage_num_add IS 'Дополнительный гаражный номер'
;
COMMENT ON COLUMN ifmp.i18_tr.equipment_afixed IS 'Закрепленное за ТС оборудование'
;
COMMENT ON COLUMN ifmp.i18_tr.install_name IS 'Схема подключения БО'
;

-- Create indexes for table ifmp.i18_tr

CREATE INDEX IX_Relationship280 ON ifmp.i18_tr (install_name)
;
COMMENT ON INDEX ifmp.IX_Relationship280 IS NULL
;

-- Add keys for table ifmp.i18_tr

ALTER TABLE ifmp.i18_tr ADD CONSTRAINT pk_tr_id PRIMARY KEY (garage_num,depo_name)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_tr_id ON ifmp.i18_tr IS NULL
;

-- Table ifmp.i09_tr_capacity

CREATE TABLE ifmp.i09_tr_capacity(
 short_name Text NOT NULL,
 qty Integer NOT NULL,
 full_name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i09_tr_capacity IS 'Вместимость ТС'
;
COMMENT ON COLUMN ifmp.i09_tr_capacity.short_name IS 'Краткое наименование'
;
COMMENT ON COLUMN ifmp.i09_tr_capacity.qty IS 'Число мест'
;
COMMENT ON COLUMN ifmp.i09_tr_capacity.full_name IS 'Полное наименование'
;

-- Add keys for table ifmp.i09_tr_capacity

ALTER TABLE ifmp.i09_tr_capacity ADD CONSTRAINT pk_tr_capacity PRIMARY KEY (short_name)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_tr_capacity ON ifmp.i09_tr_capacity IS NULL
;

-- Table ifmp.i04_tr_mark

CREATE TABLE ifmp.i04_tr_mark(
 name Text NOT NULL,
 entity_name_short Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i04_tr_mark IS 'Марка ТС'
;
COMMENT ON COLUMN ifmp.i04_tr_mark.name IS 'Марка ТС'
;
COMMENT ON COLUMN ifmp.i04_tr_mark.entity_name_short IS 'Производитель'
;

-- Add keys for table ifmp.i04_tr_mark

ALTER TABLE ifmp.i04_tr_mark ADD CONSTRAINT fk_tr_mark_tr_mark_id PRIMARY KEY (name)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT fk_tr_mark_tr_mark_id ON ifmp.i04_tr_mark IS NULL
;

-- Table ifmp.i06_depo

CREATE TABLE ifmp.i06_depo(
 depo_name Text NOT NULL,
 tr_type_short_name Text NOT NULL,
 wkt_geom Text,
 park_muid Text,
 tn_instance Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i06_depo IS 'Транспортное предприятие'
;
COMMENT ON COLUMN ifmp.i06_depo.depo_name IS 'Наименование тр. предприятия'
;
COMMENT ON COLUMN ifmp.i06_depo.tr_type_short_name IS 'Вид ТС'
;
COMMENT ON COLUMN ifmp.i06_depo.wkt_geom IS 'Координаты'
;
COMMENT ON COLUMN ifmp.i06_depo.park_muid IS 'id парка из ГИС'
;
COMMENT ON COLUMN ifmp.i06_depo.tn_instance IS 'Номер источника данных'
;

-- Add keys for table ifmp.i06_depo

ALTER TABLE ifmp.i06_depo ADD CONSTRAINT pk_depo PRIMARY KEY (depo_name)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_depo ON ifmp.i06_depo IS NULL
;

-- Table ifmp.i11_tr_model

CREATE TABLE ifmp.i11_tr_model(
 name Text NOT NULL,
 tr_mark_name Text,
 tr_capacity_short_name Text,
 has_low_floor Boolean,
 has_conditioner Boolean,
 has_wheelchair_space Boolean,
 has_bicicle_space Boolean,
 seat_qty Integer,
 seat_qty_disabled_people Integer,
 seat_qty_total Integer NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i11_tr_model IS 'Модель ТС'
;
COMMENT ON COLUMN ifmp.i11_tr_model.name IS 'Наименование модели ТС'
;
COMMENT ON COLUMN ifmp.i11_tr_model.tr_mark_name IS 'Марка ТС'
;
COMMENT ON COLUMN ifmp.i11_tr_model.tr_capacity_short_name IS 'Вместимость'
;
COMMENT ON COLUMN ifmp.i11_tr_model.has_low_floor IS 'признак низкопольности'
;
COMMENT ON COLUMN ifmp.i11_tr_model.has_conditioner IS 'признак наличия кондиционера'
;
COMMENT ON COLUMN ifmp.i11_tr_model.has_wheelchair_space IS 'признак наличия площадки (и/или специальных креплений) для инвалидной коляски'
;
COMMENT ON COLUMN ifmp.i11_tr_model.has_bicicle_space IS 'признак наличия площадки для велосипеда;'
;
COMMENT ON COLUMN ifmp.i11_tr_model.seat_qty IS 'количество сидячих мест'
;
COMMENT ON COLUMN ifmp.i11_tr_model.seat_qty_disabled_people IS 'количество сидячих мест для инвалидов'
;
COMMENT ON COLUMN ifmp.i11_tr_model.seat_qty_total IS 'общее число место мест в ТС'
;

-- Add keys for table ifmp.i11_tr_model

ALTER TABLE ifmp.i11_tr_model ADD CONSTRAINT tr_model_tr_model_id PRIMARY KEY (name)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT tr_model_tr_model_id ON ifmp.i11_tr_model IS NULL
;

-- Table ifmp.i08_tr_status

CREATE TABLE ifmp.i08_tr_status(
 name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i08_tr_status IS 'Статус ТС'
;
COMMENT ON COLUMN ifmp.i08_tr_status.name IS 'Статус ТС'
;

-- Add keys for table ifmp.i08_tr_status

ALTER TABLE ifmp.i08_tr_status ADD CONSTRAINT pk_tr_status_tr_status_id PRIMARY KEY (name)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_tr_status_tr_status_id ON ifmp.i08_tr_status IS NULL
;

-- Table ifmp.i14_equipment_model

CREATE TABLE ifmp.i14_equipment_model(
 name Text NOT NULL,
 has_sim Boolean DEFAULT false,
 has_diagnostic Boolean DEFAULT true NOT NULL,
 input_digital Smallint,
 input_analog Smallint,
 equipment_type_name_short Text,
 facility_name_short Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i14_equipment_model IS 'МодельБО'
;
COMMENT ON COLUMN ifmp.i14_equipment_model.name IS 'Наименование модели'
;
COMMENT ON COLUMN ifmp.i14_equipment_model.has_sim IS 'Наличие sim-карты'
;
COMMENT ON COLUMN ifmp.i14_equipment_model.has_diagnostic IS 'Диагностика включена'
;
COMMENT ON COLUMN ifmp.i14_equipment_model.input_digital IS 'Количество цифровых входов'
;
COMMENT ON COLUMN ifmp.i14_equipment_model.input_analog IS 'Количество аналоговых входов'
;
COMMENT ON COLUMN ifmp.i14_equipment_model.equipment_type_name_short IS 'Тип БО'
;
COMMENT ON COLUMN ifmp.i14_equipment_model.facility_name_short IS 'Производитель'
;

-- Create indexes for table ifmp.i14_equipment_model

CREATE INDEX IX_Relationship192 ON ifmp.i14_equipment_model (equipment_type_name_short)
;

CREATE INDEX IX_Relationship193 ON ifmp.i14_equipment_model (facility_name_short)
;
COMMENT ON INDEX ifmp.IX_Relationship192 IS NULL
;
COMMENT ON INDEX ifmp.IX_Relationship193 IS NULL
;

-- Add keys for table ifmp.i14_equipment_model

ALTER TABLE ifmp.i14_equipment_model ADD CONSTRAINT pk_equipment_model PRIMARY KEY (name)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_equipment_model ON ifmp.i14_equipment_model IS NULL
;

-- Table ifmp.i01_facility_type

CREATE TABLE ifmp.i01_facility_type(
 facility_type_name Text NOT NULL
)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i01_facility_type IS 'Типы организаций'
;
COMMENT ON COLUMN ifmp.i01_facility_type.facility_type_name IS 'Тип организации'
;

-- Add keys for table ifmp.i01_facility_type

ALTER TABLE ifmp.i01_facility_type ADD CONSTRAINT pk_facility_type PRIMARY KEY (facility_type_name)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_facility_type ON ifmp.i01_facility_type IS NULL
;

-- Table ifmp.i18_equipment

CREATE TABLE ifmp.i18_equipment(
 equipment_id BigSerial NOT NULL,
 serial_num Text,
 comment Text,
 equipment_status_name Text NOT NULL,
 territory_name_short Text,
 equipment_model_name Text,
 firmware_version Text,
 unit_service_facility_name_short Text,
 service_type_name Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i18_equipment IS 'equipment'
;
COMMENT ON COLUMN ifmp.i18_equipment.equipment_id IS 'Идентификатор оборудования'
;
COMMENT ON COLUMN ifmp.i18_equipment.serial_num IS 'Серийный номер'
;
COMMENT ON COLUMN ifmp.i18_equipment.comment IS 'Комментарий'
;
COMMENT ON COLUMN ifmp.i18_equipment.equipment_status_name IS 'Статус'
;
COMMENT ON COLUMN ifmp.i18_equipment.territory_name_short IS 'Территория транспортного предприятия'
;
COMMENT ON COLUMN ifmp.i18_equipment.equipment_model_name IS 'Наименование модели'
;
COMMENT ON COLUMN ifmp.i18_equipment.firmware_version IS 'Прошивка БО'
;
COMMENT ON COLUMN ifmp.i18_equipment.unit_service_facility_name_short IS 'Обслуживающая организация'
;
COMMENT ON COLUMN ifmp.i18_equipment.service_type_name IS 'Вид технического обслуживания'
;

-- Create indexes for table ifmp.i18_equipment

CREATE INDEX IX_Relationship187 ON ifmp.i18_equipment (equipment_status_name)
;

CREATE INDEX IX_Relationship190 ON ifmp.i18_equipment (territory_name_short)
;

CREATE INDEX IX_Relationship194 ON ifmp.i18_equipment (equipment_model_name)
;

CREATE INDEX IX_Relationship197 ON ifmp.i18_equipment (firmware_version)
;

CREATE INDEX IX_Relationship198 ON ifmp.i18_equipment (unit_service_facility_name_short)
;

CREATE INDEX IX_Relationship199 ON ifmp.i18_equipment (service_type_name)
;
COMMENT ON INDEX ifmp.IX_Relationship187 IS NULL
;
COMMENT ON INDEX ifmp.IX_Relationship190 IS NULL
;
COMMENT ON INDEX ifmp.IX_Relationship194 IS NULL
;
COMMENT ON INDEX ifmp.IX_Relationship197 IS NULL
;
COMMENT ON INDEX ifmp.IX_Relationship198 IS NULL
;
COMMENT ON INDEX ifmp.IX_Relationship199 IS NULL
;

-- Add keys for table ifmp.i18_equipment

ALTER TABLE ifmp.i18_equipment ADD CONSTRAINT pk_equipment PRIMARY KEY (equipment_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_equipment ON ifmp.i18_equipment IS NULL
;

-- Table ifmp.I22_sensor2unit

CREATE TABLE ifmp.I22_sensor2unit(
 sensor_num Bigint NOT NULL,
 equipment_id_unit Bigint,
 equipment_id_sensor Bigint NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.I22_sensor2unit IS 'Идентификатор сенсора'
;
COMMENT ON COLUMN ifmp.I22_sensor2unit.sensor_num IS 'Номер датчика'
;
COMMENT ON COLUMN ifmp.I22_sensor2unit.equipment_id_unit IS 'Ид блока'
;
COMMENT ON COLUMN ifmp.I22_sensor2unit.equipment_id_sensor IS 'Ид сенсора'
;

-- Create indexes for table ifmp.I22_sensor2unit

CREATE INDEX IX_Relationship218 ON ifmp.I22_sensor2unit (equipment_id_unit)
;

CREATE INDEX IX_Relationship219 ON ifmp.I22_sensor2unit (equipment_id_sensor)
;
CREATE UNIQUE INDEX ux_sensor2unit ON ifmp.I22_sensor2unit (sensor_num,equipment_id_unit)
TABLESPACE core_idx
;
COMMENT ON INDEX ifmp.IX_Relationship218 IS NULL
;
COMMENT ON INDEX ifmp.IX_Relationship219 IS NULL
;

-- Add keys for table ifmp.I22_sensor2unit

ALTER TABLE ifmp.I22_sensor2unit ADD CONSTRAINT pk_sensor2unit_sensor_id PRIMARY KEY (equipment_id_sensor)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_sensor2unit_sensor_id ON ifmp.I22_sensor2unit IS NULL
;

-- Table ifmp.i03_tr_type

CREATE TABLE ifmp.i03_tr_type(
 short_name Text NOT NULL,
 transport_kind_muid Bigint
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i03_tr_type IS 'Вид ТС'
;
COMMENT ON COLUMN ifmp.i03_tr_type.short_name IS 'Краткое наименование'
;
COMMENT ON COLUMN ifmp.i03_tr_type.transport_kind_muid IS 'Вид ТС из ГИС (transport_kind_muid)'
;

-- Add keys for table ifmp.i03_tr_type

ALTER TABLE ifmp.i03_tr_type ADD CONSTRAINT pk_tr_type PRIMARY KEY (short_name)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_tr_type ON ifmp.i03_tr_type IS NULL
;

-- Table ifmp.i12_equipment_class

CREATE TABLE ifmp.i12_equipment_class(
 name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i12_equipment_class IS 'Класс типа БО'
;
COMMENT ON COLUMN ifmp.i12_equipment_class.name IS 'Наименование Класс типа БО'
;

-- Add keys for table ifmp.i12_equipment_class

ALTER TABLE ifmp.i12_equipment_class ADD CONSTRAINT pk_equipment_class_id PRIMARY KEY (name)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_equipment_class_id ON ifmp.i12_equipment_class IS NULL
;

-- Table ifmp.i20_unit2tr

CREATE TABLE ifmp.i20_unit2tr(
 equipment_id Bigint NOT NULL,
 garage_num Integer,
 depo_name Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i20_unit2tr IS 'Блоки в транспортном средстве'
;
COMMENT ON COLUMN ifmp.i20_unit2tr.equipment_id IS 'Идентификатор блока'
;
COMMENT ON COLUMN ifmp.i20_unit2tr.garage_num IS 'Гаражный номер'
;
COMMENT ON COLUMN ifmp.i20_unit2tr.depo_name IS NULL
;

-- Create indexes for table ifmp.i20_unit2tr

CREATE INDEX IX_Relationship211 ON ifmp.i20_unit2tr (garage_num,depo_name)
;
COMMENT ON INDEX ifmp.IX_Relationship211 IS NULL
;

-- Add keys for table ifmp.i20_unit2tr

ALTER TABLE ifmp.i20_unit2tr ADD CONSTRAINT unit2tr_unit_id_pk PRIMARY KEY (equipment_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT unit2tr_unit_id_pk ON ifmp.i20_unit2tr IS NULL
;

-- Table ifmp.i05_facility

CREATE TABLE ifmp.i05_facility(
 facility_name Text NOT NULL,
 name_full Text,
 comment Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i05_facility IS 'Контрагенты'
;
COMMENT ON COLUMN ifmp.i05_facility.facility_name IS 'Наименование сокращенное'
;
COMMENT ON COLUMN ifmp.i05_facility.name_full IS 'Наименование полное'
;
COMMENT ON COLUMN ifmp.i05_facility.comment IS 'Дополнительно'
;

-- Add keys for table ifmp.i05_facility

ALTER TABLE ifmp.i05_facility ADD CONSTRAINT pk_entity PRIMARY KEY (facility_name)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_entity ON ifmp.i05_facility IS NULL
;

-- Table ifmp.i26_facility2type

CREATE TABLE ifmp.i26_facility2type(
 facility_type_name Text,
 facility_name Text
)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i26_facility2type IS 'Связь контрагентов с типами организаций'
;
COMMENT ON COLUMN ifmp.i26_facility2type.facility_type_name IS 'Наименование типа организации'
;
COMMENT ON COLUMN ifmp.i26_facility2type.facility_name IS 'Наименование организации'
;

-- Create indexes for table ifmp.i26_facility2type

CREATE INDEX IX_Relationship224 ON ifmp.i26_facility2type (facility_type_name)
;

CREATE INDEX IX_Relationship225 ON ifmp.i26_facility2type (facility_name)
;
COMMENT ON INDEX ifmp.IX_Relationship224 IS NULL
;
COMMENT ON INDEX ifmp.IX_Relationship225 IS NULL
;

-- Table ifmp.i27_modem

CREATE TABLE ifmp.i27_modem(
 equipment_id Bigint NOT NULL,
 imei Text NOT NULL,
 iccid Text,
 speed Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE ifmp.i27_modem ALTER COLUMN imei SET STORAGE EXTENDED
;
ALTER TABLE ifmp.i27_modem ALTER COLUMN speed SET STORAGE EXTENDED
;

COMMENT ON TABLE ifmp.i27_modem IS 'Модемы'
;
COMMENT ON COLUMN ifmp.i27_modem.equipment_id IS 'Оборудование'
;
COMMENT ON COLUMN ifmp.i27_modem.imei IS 'IMEI'
;
COMMENT ON COLUMN ifmp.i27_modem.iccid IS 'Сим-карта'
;
COMMENT ON COLUMN ifmp.i27_modem.speed IS 'Скорость'
;

-- Create indexes for table ifmp.i27_modem

CREATE INDEX IX_Relationship232 ON ifmp.i27_modem (equipment_id)
;

CREATE INDEX IX_Relationship233 ON ifmp.i27_modem (iccid)
;
COMMENT ON INDEX ifmp.IX_Relationship232 IS NULL
;
COMMENT ON INDEX ifmp.IX_Relationship233 IS NULL
;

-- Add keys for table ifmp.i27_modem

ALTER TABLE ifmp.i27_modem ADD CONSTRAINT pk_modem_modem_id PRIMARY KEY (equipment_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_modem_modem_id ON ifmp.i27_modem IS NULL
;

-- Table ifmp.i28_modem2unit

CREATE TABLE ifmp.i28_modem2unit(
 equipment_id_modem Bigint NOT NULL,
 equipment_id_unit Bigint NOT NULL
)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i28_modem2unit IS 'Модемы в транспортном средстве'
;
COMMENT ON COLUMN ifmp.i28_modem2unit.equipment_id_modem IS 'Модем'
;
COMMENT ON COLUMN ifmp.i28_modem2unit.equipment_id_unit IS 'Блок'
;

-- Create indexes for table ifmp.i28_modem2unit

CREATE INDEX IX_Relationship235 ON ifmp.i28_modem2unit (equipment_id_modem)
;

CREATE INDEX IX_Relationship238 ON ifmp.i28_modem2unit (equipment_id_unit)
;
COMMENT ON INDEX ifmp.IX_Relationship235 IS NULL
;
COMMENT ON INDEX ifmp.IX_Relationship238 IS NULL
;

-- Add keys for table ifmp.i28_modem2unit

ALTER TABLE ifmp.i28_modem2unit ADD CONSTRAINT pk_modem2unit_unit_id_modem_id PRIMARY KEY (equipment_id_modem)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_modem2unit_unit_id_modem_id ON ifmp.i28_modem2unit IS NULL
;

-- Table ifmp.i29_monitor2unit

CREATE TABLE ifmp.i29_monitor2unit(
 equipment_id_unit Bigint NOT NULL,
 equipment_id_monitor Bigint NOT NULL
)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i29_monitor2unit IS 'Монитор на блоке'
;
COMMENT ON COLUMN ifmp.i29_monitor2unit.equipment_id_unit IS NULL
;
COMMENT ON COLUMN ifmp.i29_monitor2unit.equipment_id_monitor IS NULL
;

-- Create indexes for table ifmp.i29_monitor2unit

CREATE INDEX IX_Relationship239 ON ifmp.i29_monitor2unit (equipment_id_unit)
;

CREATE INDEX IX_Relationship241 ON ifmp.i29_monitor2unit (equipment_id_monitor)
;
COMMENT ON INDEX ifmp.IX_Relationship239 IS NULL
;
COMMENT ON INDEX ifmp.IX_Relationship241 IS NULL
;

-- Table ifmp.i29_hdd

CREATE TABLE ifmp.i29_hdd(
 equipment_id Bigint NOT NULL,
 type Integer,
 capacity Double precision,
 size Double precision
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE ifmp.i29_hdd ALTER COLUMN type SET STORAGE PLAIN
;
ALTER TABLE ifmp.i29_hdd ALTER COLUMN capacity SET STORAGE PLAIN
;
ALTER TABLE ifmp.i29_hdd ALTER COLUMN size SET STORAGE PLAIN
;

COMMENT ON TABLE ifmp.i29_hdd IS 'Жесткий диск'
;
COMMENT ON COLUMN ifmp.i29_hdd.equipment_id IS 'Оборудование'
;
COMMENT ON COLUMN ifmp.i29_hdd.type IS 'Тип диска: 1 - НМЖД или 2 -твердотельный;'
;
COMMENT ON COLUMN ifmp.i29_hdd.capacity IS 'Объем диска, Гб'
;
COMMENT ON COLUMN ifmp.i29_hdd.size IS 'Типоразмер диска, дюйм'
;

-- Create indexes for table ifmp.i29_hdd

CREATE INDEX IX_Relationship242 ON ifmp.i29_hdd (equipment_id)
;
COMMENT ON INDEX ifmp.IX_Relationship242 IS NULL
;

-- Add keys for table ifmp.i29_hdd

ALTER TABLE ifmp.i29_hdd ADD CONSTRAINT pk_hdd_hdd_id PRIMARY KEY (equipment_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_hdd_hdd_id ON ifmp.i29_hdd IS NULL
;

-- Table ifmp.i30_hdd2unit

CREATE TABLE ifmp.i30_hdd2unit(
 equipment_id_unit Bigint,
 equipment_id_hdd Bigint
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i30_hdd2unit IS 'Жесткий диск на блоке'
;
COMMENT ON COLUMN ifmp.i30_hdd2unit.equipment_id_unit IS 'Блок'
;
COMMENT ON COLUMN ifmp.i30_hdd2unit.equipment_id_hdd IS 'Жеский диск'
;

-- Create indexes for table ifmp.i30_hdd2unit

CREATE INDEX IX_Relationship243 ON ifmp.i30_hdd2unit (equipment_id_unit)
;
COMMENT ON INDEX ifmp.IX_Relationship243 IS NULL
;

-- Table ifmp.i24_sensor2tr

CREATE TABLE ifmp.i24_sensor2tr(
 equipment_id Bigint NOT NULL,
 garage_num Integer NOT NULL,
 depo_name Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i24_sensor2tr IS 'Места устаноки датчиков в ТС'
;
COMMENT ON COLUMN ifmp.i24_sensor2tr.equipment_id IS 'Ид датчика'
;
COMMENT ON COLUMN ifmp.i24_sensor2tr.garage_num IS 'Наименование тр. предпиятия'
;
COMMENT ON COLUMN ifmp.i24_sensor2tr.depo_name IS NULL
;

-- Create indexes for table ifmp.i24_sensor2tr

CREATE INDEX IX_Relationship222 ON ifmp.i24_sensor2tr (equipment_id)
;

CREATE INDEX IX_Relationship223 ON ifmp.i24_sensor2tr (garage_num,depo_name)
;
COMMENT ON INDEX ifmp.IX_Relationship222 IS NULL
;
COMMENT ON INDEX ifmp.IX_Relationship223 IS NULL
;

-- Add keys for table ifmp.i24_sensor2tr

ALTER TABLE ifmp.i24_sensor2tr ADD CONSTRAINT pk_sensor2tr_sensor_id PRIMARY KEY (equipment_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_sensor2tr_sensor_id ON ifmp.i24_sensor2tr IS NULL
;

-- Table ifmp.i35_tr_schema_install

CREATE TABLE ifmp.i35_tr_schema_install(
 install_name Text NOT NULL,
 tr_type_name Text NOT NULL,
 capacity_name Text NOT NULL,
 install_desc Text
)
TABLESPACE core_data
;
ALTER TABLE ifmp.i35_tr_schema_install ALTER COLUMN install_name SET STORAGE EXTENDED
;
ALTER TABLE ifmp.i35_tr_schema_install ALTER COLUMN install_desc SET STORAGE EXTENDED
;

COMMENT ON TABLE ifmp.i35_tr_schema_install IS 'Схема подключения БО к видам ТС'
;
COMMENT ON COLUMN ifmp.i35_tr_schema_install.install_name IS 'Наименование схемы подключения
'
;
COMMENT ON COLUMN ifmp.i35_tr_schema_install.tr_type_name IS 'Вид транспорта'
;
COMMENT ON COLUMN ifmp.i35_tr_schema_install.capacity_name IS 'Вместимость'
;
COMMENT ON COLUMN ifmp.i35_tr_schema_install.install_desc IS 'Комментарий'
;

-- Create indexes for table ifmp.i35_tr_schema_install

CREATE INDEX IX_Relationship273 ON ifmp.i35_tr_schema_install (tr_type_name)
;

CREATE INDEX IX_Relationship274 ON ifmp.i35_tr_schema_install (capacity_name)
;
COMMENT ON INDEX ifmp.IX_Relationship273 IS NULL
;
COMMENT ON INDEX ifmp.IX_Relationship274 IS NULL
;

-- Add keys for table ifmp.i35_tr_schema_install

ALTER TABLE ifmp.i35_tr_schema_install ADD CONSTRAINT pk_tr_schema_install PRIMARY KEY (install_name)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_tr_schema_install ON ifmp.i35_tr_schema_install IS NULL
;

-- Table ifmp.i37_tr_schema_install_detail

CREATE TABLE ifmp.i37_tr_schema_install_detail(
 install_name Text NOT NULL,
 nn Smallint NOT NULL,
 num Smallint NOT NULL,
 equipment_type_name_unit Text,
 is_required Boolean DEFAULT true NOT NULL,
 install_desc Text
)
TABLESPACE core_data
;
ALTER TABLE ifmp.i37_tr_schema_install_detail ALTER COLUMN is_required SET STORAGE PLAIN
;
ALTER TABLE ifmp.i37_tr_schema_install_detail ALTER COLUMN install_desc SET STORAGE EXTENDED
;

COMMENT ON TABLE ifmp.i37_tr_schema_install_detail IS 'Детали подключения оборудования в схеме подключения к видам ТС'
;
COMMENT ON COLUMN ifmp.i37_tr_schema_install_detail.install_name IS 'Схема подключения БО к видам ТС'
;
COMMENT ON COLUMN ifmp.i37_tr_schema_install_detail.nn IS 'Порядковый номер блока'
;
COMMENT ON COLUMN ifmp.i37_tr_schema_install_detail.num IS 'Номер Входа'
;
COMMENT ON COLUMN ifmp.i37_tr_schema_install_detail.equipment_type_name_unit IS 'Тип блока'
;
COMMENT ON COLUMN ifmp.i37_tr_schema_install_detail.is_required IS 'обязательно для подключения'
;
COMMENT ON COLUMN ifmp.i37_tr_schema_install_detail.install_desc IS 'Комментарий'
;

-- Create indexes for table ifmp.i37_tr_schema_install_detail

CREATE INDEX IX_Relationship276 ON ifmp.i37_tr_schema_install_detail (num,equipment_type_name_unit)
;

CREATE INDEX IX_Relationship278 ON ifmp.i37_tr_schema_install_detail (install_name,nn)
;
COMMENT ON INDEX ifmp.IX_Relationship276 IS NULL
;
COMMENT ON INDEX ifmp.IX_Relationship278 IS NULL
;

-- Add keys for table ifmp.i37_tr_schema_install_detail

ALTER TABLE ifmp.i37_tr_schema_install_detail ADD CONSTRAINT pk_tr_schema_install_detail PRIMARY KEY (install_name,nn,num)
;
COMMENT ON CONSTRAINT pk_tr_schema_install_detail ON ifmp.i37_tr_schema_install_detail IS NULL
;

-- Table ifmp.i36_tr_schema_install_unit

CREATE TABLE ifmp.i36_tr_schema_install_unit(
 install_name Text NOT NULL,
 nn Smallint NOT NULL,
 equipment_type_name Text NOT NULL,
 is_main Boolean NOT NULL
)
TABLESPACE core_idx
;

COMMENT ON TABLE ifmp.i36_tr_schema_install_unit IS 'ББ в схеме подключения'
;
COMMENT ON COLUMN ifmp.i36_tr_schema_install_unit.install_name IS 'Схема подключения БО к видам ТС'
;
COMMENT ON COLUMN ifmp.i36_tr_schema_install_unit.nn IS 'Порядковый номер блока'
;
COMMENT ON COLUMN ifmp.i36_tr_schema_install_unit.equipment_type_name IS 'Тип бортового оборудования'
;
COMMENT ON COLUMN ifmp.i36_tr_schema_install_unit.is_main IS 'Признак основного блока'
;

-- Create indexes for table ifmp.i36_tr_schema_install_unit

CREATE INDEX IX_Relationship270 ON ifmp.i36_tr_schema_install_unit (install_name)
;

CREATE INDEX IX_Relationship275 ON ifmp.i36_tr_schema_install_unit (equipment_type_name)
;
COMMENT ON INDEX ifmp.IX_Relationship270 IS NULL
;
COMMENT ON INDEX ifmp.IX_Relationship275 IS NULL
;

-- Add keys for table ifmp.i36_tr_schema_install_unit

ALTER TABLE ifmp.i36_tr_schema_install_unit ADD CONSTRAINT pk_tr_schema_install_unit PRIMARY KEY (install_name,nn)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_tr_schema_install_unit ON ifmp.i36_tr_schema_install_unit IS NULL
;

-- Table ifmp.i21_sensor2unit_model

CREATE TABLE ifmp.i21_sensor2unit_model(
 num Smallint NOT NULL,
 equipment_type_name_unit Text NOT NULL,
 equipment_type_name_sensor Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE ifmp.i21_sensor2unit_model ALTER COLUMN num SET STORAGE PLAIN
;

COMMENT ON TABLE ifmp.i21_sensor2unit_model IS 'Схема подключения датчиков'
;
COMMENT ON COLUMN ifmp.i21_sensor2unit_model.num IS 'Номер '
;
COMMENT ON COLUMN ifmp.i21_sensor2unit_model.equipment_type_name_unit IS 'Тип блока'
;
COMMENT ON COLUMN ifmp.i21_sensor2unit_model.equipment_type_name_sensor IS 'Тип сенсора'
;

-- Create indexes for table ifmp.i21_sensor2unit_model

CREATE INDEX IX_Relationship216 ON ifmp.i21_sensor2unit_model (equipment_type_name_unit)
;

CREATE INDEX IX_Relationship217 ON ifmp.i21_sensor2unit_model (equipment_type_name_sensor)
;
COMMENT ON INDEX ifmp.IX_Relationship216 IS NULL
;
COMMENT ON INDEX ifmp.IX_Relationship217 IS NULL
;

-- Add keys for table ifmp.i21_sensor2unit_model

ALTER TABLE ifmp.i21_sensor2unit_model ADD CONSTRAINT pk_sensor2unit_model_sensor2unit_model_id PRIMARY KEY (num,equipment_type_name_unit)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_sensor2unit_model_sensor2unit_model_id ON ifmp.i21_sensor2unit_model IS NULL
;

-- Create relationships section -------------------------------------------------

ALTER TABLE ifmp.i06_depo ADD CONSTRAINT Relationship172 FOREIGN KEY (tr_type_short_name) REFERENCES ifmp.i03_tr_type (short_name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship172 ON ifmp.i06_depo IS NULL
;

ALTER TABLE ifmp.i04_tr_mark ADD CONSTRAINT Relationship177 FOREIGN KEY (entity_name_short) REFERENCES ifmp.i05_facility (facility_name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship177 ON ifmp.i04_tr_mark IS NULL
;

ALTER TABLE ifmp.i11_tr_model ADD CONSTRAINT Relationship179 FOREIGN KEY (tr_capacity_short_name) REFERENCES ifmp.i09_tr_capacity (short_name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship179 ON ifmp.i11_tr_model IS NULL
;

ALTER TABLE ifmp.i11_tr_model ADD CONSTRAINT Relationship180 FOREIGN KEY (tr_mark_name) REFERENCES ifmp.i04_tr_mark (name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship180 ON ifmp.i11_tr_model IS NULL
;

ALTER TABLE ifmp.i18_tr ADD CONSTRAINT Relationship182 FOREIGN KEY (tr_status_name) REFERENCES ifmp.i08_tr_status (name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship182 ON ifmp.i18_tr IS NULL
;

ALTER TABLE ifmp.i18_tr ADD CONSTRAINT Relationship183 FOREIGN KEY (territory_name_short) REFERENCES ifmp.i07_territory (name_short) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship183 ON ifmp.i18_tr IS NULL
;

ALTER TABLE ifmp.i18_tr ADD CONSTRAINT Relationship184 FOREIGN KEY (tr_model_name) REFERENCES ifmp.i11_tr_model (name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship184 ON ifmp.i18_tr IS NULL
;

ALTER TABLE ifmp.i18_tr ADD CONSTRAINT Relationship185 FOREIGN KEY (tr_type_short_name) REFERENCES ifmp.i03_tr_type (short_name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship185 ON ifmp.i18_tr IS NULL
;

ALTER TABLE ifmp.i18_equipment ADD CONSTRAINT Relationship187 FOREIGN KEY (equipment_status_name) REFERENCES ifmp.i02_equipment_status (name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship187 ON ifmp.i18_equipment IS NULL
;

ALTER TABLE ifmp.i18_equipment ADD CONSTRAINT Relationship190 FOREIGN KEY (territory_name_short) REFERENCES ifmp.i07_territory (name_short) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship190 ON ifmp.i18_equipment IS NULL
;

ALTER TABLE ifmp.i13_equipment_type ADD CONSTRAINT Relationship191 FOREIGN KEY (equipment_class_name) REFERENCES ifmp.i12_equipment_class (name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship191 ON ifmp.i13_equipment_type IS NULL
;

ALTER TABLE ifmp.i14_equipment_model ADD CONSTRAINT Relationship192 FOREIGN KEY (equipment_type_name_short) REFERENCES ifmp.i13_equipment_type (equipment_type_name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship192 ON ifmp.i14_equipment_model IS NULL
;

ALTER TABLE ifmp.i14_equipment_model ADD CONSTRAINT Relationship193 FOREIGN KEY (facility_name_short) REFERENCES ifmp.i05_facility (facility_name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship193 ON ifmp.i14_equipment_model IS NULL
;

ALTER TABLE ifmp.i18_equipment ADD CONSTRAINT Relationship194 FOREIGN KEY (equipment_model_name) REFERENCES ifmp.i14_equipment_model (name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship194 ON ifmp.i18_equipment IS NULL
;

ALTER TABLE ifmp.i15_firmware ADD CONSTRAINT Relationship195 FOREIGN KEY (equipment_model_name) REFERENCES ifmp.i14_equipment_model (name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship195 ON ifmp.i15_firmware IS NULL
;

ALTER TABLE ifmp.i15_firmware ADD CONSTRAINT Relationship196 FOREIGN KEY (facility_name_short) REFERENCES ifmp.i05_facility (facility_name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship196 ON ifmp.i15_firmware IS NULL
;

ALTER TABLE ifmp.i18_equipment ADD CONSTRAINT Relationship197 FOREIGN KEY (firmware_version) REFERENCES ifmp.i15_firmware (version) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship197 ON ifmp.i18_equipment IS NULL
;

ALTER TABLE ifmp.i18_equipment ADD CONSTRAINT Relationship198 FOREIGN KEY (unit_service_facility_name_short) REFERENCES ifmp.i05_facility (facility_name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship198 ON ifmp.i18_equipment IS NULL
;

ALTER TABLE ifmp.i18_equipment ADD CONSTRAINT Relationship199 FOREIGN KEY (service_type_name) REFERENCES ifmp.i10_service_type (name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship199 ON ifmp.i18_equipment IS NULL
;

ALTER TABLE ifmp.i19_unit ADD CONSTRAINT Relationship200 FOREIGN KEY (equipment_id) REFERENCES ifmp.i18_equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship200 ON ifmp.i19_unit IS NULL
;

ALTER TABLE ifmp.i16_mobile_tariff ADD CONSTRAINT Relationship201 FOREIGN KEY (facility_name) REFERENCES ifmp.i05_facility (facility_name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship201 ON ifmp.i16_mobile_tariff IS NULL
;

ALTER TABLE ifmp.i17_sim_card ADD CONSTRAINT Relationship202 FOREIGN KEY (sim_card_group_name) REFERENCES ifmp.i01_sim_card_group (name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship202 ON ifmp.i17_sim_card IS NULL
;

ALTER TABLE ifmp.i17_sim_card ADD CONSTRAINT Relationship204 FOREIGN KEY (territory_name_short) REFERENCES ifmp.i07_territory (name_short) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship204 ON ifmp.i17_sim_card IS NULL
;

ALTER TABLE ifmp.i17_sim_card ADD CONSTRAINT Relationship205 FOREIGN KEY (mobile_tariff_name) REFERENCES ifmp.i16_mobile_tariff (name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship205 ON ifmp.i17_sim_card IS NULL
;

ALTER TABLE ifmp.i17_sim_card ADD CONSTRAINT Relationship206 FOREIGN KEY (equipment_status_name) REFERENCES ifmp.i02_equipment_status (name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship206 ON ifmp.i17_sim_card IS NULL
;

ALTER TABLE ifmp.i07_territory ADD CONSTRAINT Relationship208 FOREIGN KEY (depo_name) REFERENCES ifmp.i06_depo (depo_name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship208 ON ifmp.i07_territory IS NULL
;

ALTER TABLE ifmp.i20_unit2tr ADD CONSTRAINT Relationship211 FOREIGN KEY (garage_num, depo_name) REFERENCES ifmp.i18_tr (garage_num, depo_name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship211 ON ifmp.i20_unit2tr IS NULL
;

ALTER TABLE ifmp.i20_unit2tr ADD CONSTRAINT Relationship212 FOREIGN KEY (equipment_id) REFERENCES ifmp.i19_unit (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship212 ON ifmp.i20_unit2tr IS NULL
;

ALTER TABLE ifmp.i21_sensor2unit_model ADD CONSTRAINT Relationship216 FOREIGN KEY (equipment_type_name_unit) REFERENCES ifmp.i13_equipment_type (equipment_type_name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship216 ON ifmp.i21_sensor2unit_model IS NULL
;

ALTER TABLE ifmp.i21_sensor2unit_model ADD CONSTRAINT Relationship217 FOREIGN KEY (equipment_type_name_sensor) REFERENCES ifmp.i13_equipment_type (equipment_type_name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship217 ON ifmp.i21_sensor2unit_model IS NULL
;

ALTER TABLE ifmp.I22_sensor2unit ADD CONSTRAINT Relationship218 FOREIGN KEY (equipment_id_unit) REFERENCES ifmp.i19_unit (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship218 ON ifmp.I22_sensor2unit IS NULL
;

ALTER TABLE ifmp.I22_sensor2unit ADD CONSTRAINT Relationship219 FOREIGN KEY (equipment_id_sensor) REFERENCES ifmp.i18_equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship219 ON ifmp.I22_sensor2unit IS NULL
;

ALTER TABLE ifmp.i24_sensor2tr ADD CONSTRAINT Relationship222 FOREIGN KEY (equipment_id) REFERENCES ifmp.i18_equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship222 ON ifmp.i24_sensor2tr IS NULL
;

ALTER TABLE ifmp.i24_sensor2tr ADD CONSTRAINT Relationship223 FOREIGN KEY (garage_num, depo_name) REFERENCES ifmp.i18_tr (garage_num, depo_name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship223 ON ifmp.i24_sensor2tr IS NULL
;

ALTER TABLE ifmp.i26_facility2type ADD CONSTRAINT Relationship224 FOREIGN KEY (facility_type_name) REFERENCES ifmp.i01_facility_type (facility_type_name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship224 ON ifmp.i26_facility2type IS NULL
;

ALTER TABLE ifmp.i26_facility2type ADD CONSTRAINT Relationship225 FOREIGN KEY (facility_name) REFERENCES ifmp.i05_facility (facility_name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship225 ON ifmp.i26_facility2type IS NULL
;

ALTER TABLE ifmp.i25_sim_card2unit ADD CONSTRAINT Relationship226 FOREIGN KEY (equipment_id) REFERENCES ifmp.i18_equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship226 ON ifmp.i25_sim_card2unit IS NULL
;

ALTER TABLE ifmp.i25_sim_card2unit ADD CONSTRAINT Relationship227 FOREIGN KEY (equipment_id) REFERENCES ifmp.i19_unit (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship227 ON ifmp.i25_sim_card2unit IS NULL
;

ALTER TABLE ifmp.i25_sim_card2unit ADD CONSTRAINT Relationship228 FOREIGN KEY (iccid) REFERENCES ifmp.i17_sim_card (iccid) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship228 ON ifmp.i25_sim_card2unit IS NULL
;

ALTER TABLE ifmp.i27_modem ADD CONSTRAINT Relationship232 FOREIGN KEY (equipment_id) REFERENCES ifmp.i18_equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship232 ON ifmp.i27_modem IS NULL
;

ALTER TABLE ifmp.i27_modem ADD CONSTRAINT Relationship233 FOREIGN KEY (iccid) REFERENCES ifmp.i17_sim_card (iccid) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship233 ON ifmp.i27_modem IS NULL
;

ALTER TABLE ifmp.i28_modem2unit ADD CONSTRAINT Relationship235 FOREIGN KEY (equipment_id_modem) REFERENCES ifmp.i27_modem (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship235 ON ifmp.i28_modem2unit IS NULL
;

ALTER TABLE ifmp.i28_modem2unit ADD CONSTRAINT Relationship238 FOREIGN KEY (equipment_id_unit) REFERENCES ifmp.i19_unit (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship238 ON ifmp.i28_modem2unit IS NULL
;

ALTER TABLE ifmp.i29_monitor2unit ADD CONSTRAINT Relationship239 FOREIGN KEY (equipment_id_unit) REFERENCES ifmp.i19_unit (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship239 ON ifmp.i29_monitor2unit IS NULL
;

ALTER TABLE ifmp.i29_monitor2unit ADD CONSTRAINT Relationship241 FOREIGN KEY (equipment_id_monitor) REFERENCES ifmp.i18_equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship241 ON ifmp.i29_monitor2unit IS NULL
;

ALTER TABLE ifmp.i29_hdd ADD CONSTRAINT Relationship242 FOREIGN KEY (equipment_id) REFERENCES ifmp.i18_equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship242 ON ifmp.i29_hdd IS NULL
;

ALTER TABLE ifmp.i30_hdd2unit ADD CONSTRAINT Relationship243 FOREIGN KEY (equipment_id_unit) REFERENCES ifmp.i19_unit (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship243 ON ifmp.i30_hdd2unit IS NULL
;

ALTER TABLE ifmp.i30_hdd2unit ADD CONSTRAINT Relationship244 FOREIGN KEY (equipment_id_hdd) REFERENCES ifmp.i29_hdd (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship244 ON ifmp.i30_hdd2unit IS NULL
;

ALTER TABLE ifmp.i36_tr_schema_install_unit ADD CONSTRAINT Relationship270 FOREIGN KEY (install_name) REFERENCES ifmp.i35_tr_schema_install (install_name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship270 ON ifmp.i36_tr_schema_install_unit IS NULL
;

ALTER TABLE ifmp.i35_tr_schema_install ADD CONSTRAINT Relationship273 FOREIGN KEY (tr_type_name) REFERENCES ifmp.i03_tr_type (short_name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship273 ON ifmp.i35_tr_schema_install IS NULL
;

ALTER TABLE ifmp.i35_tr_schema_install ADD CONSTRAINT Relationship274 FOREIGN KEY (capacity_name) REFERENCES ifmp.i09_tr_capacity (short_name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship274 ON ifmp.i35_tr_schema_install IS NULL
;

ALTER TABLE ifmp.i36_tr_schema_install_unit ADD CONSTRAINT Relationship275 FOREIGN KEY (equipment_type_name) REFERENCES ifmp.i13_equipment_type (equipment_type_name) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship275 ON ifmp.i36_tr_schema_install_unit IS NULL
;

ALTER TABLE ifmp.i37_tr_schema_install_detail ADD CONSTRAINT Relationship276 FOREIGN KEY (num, equipment_type_name_unit) REFERENCES ifmp.i21_sensor2unit_model (num, equipment_type_name_unit) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship276 ON ifmp.i37_tr_schema_install_detail IS NULL
;

ALTER TABLE ifmp.i37_tr_schema_install_detail ADD CONSTRAINT Relationship278 FOREIGN KEY (install_name, nn) REFERENCES ifmp.i36_tr_schema_install_unit (install_name, nn) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship278 ON ifmp.i37_tr_schema_install_detail IS NULL
;

ALTER TABLE ifmp.i38_unit_bnsr ADD CONSTRAINT Relationship279 FOREIGN KEY (equipment_id) REFERENCES ifmp.i19_unit (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship279 ON ifmp.i38_unit_bnsr IS NULL
;










/*
Created: 7/12/2016
Modified: 9/8/2017
Project: csptm
Model: Integration
Database: PostgreSQL 9.4
*/

-- Create tables section -------------------------------------------------

-- Table ifmp.i50_account

CREATE TABLE ifmp.i50_account(
 login Text NOT NULL,
 last_name Text NOT NULL,
 first_name Text,
 middle_name Text,
 password Text,
 position Text,
 email Text,
 cell_phone Text,
 work_phone Text,
 operator_phone Text,
 comment Text,
 is_email_subscriber Boolean DEFAULT false NOT NULL,
 is_phone_subscriber Boolean DEFAULT false,
 depo_name Text,
 facility_name Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE ifmp.i50_account IS 'Пользователи'
;
COMMENT ON COLUMN ifmp.i50_account.login IS 'Логин'
;
COMMENT ON COLUMN ifmp.i50_account.last_name IS 'Фамилия'
;
COMMENT ON COLUMN ifmp.i50_account.first_name IS 'Имя'
;
COMMENT ON COLUMN ifmp.i50_account.middle_name IS 'Отчество'
;
COMMENT ON COLUMN ifmp.i50_account.password IS 'Пароль'
;
COMMENT ON COLUMN ifmp.i50_account.position IS 'Должность'
;
COMMENT ON COLUMN ifmp.i50_account.email IS 'Email'
;
COMMENT ON COLUMN ifmp.i50_account.cell_phone IS 'Мобильный телефон'
;
COMMENT ON COLUMN ifmp.i50_account.work_phone IS 'Рабочий телефон'
;
COMMENT ON COLUMN ifmp.i50_account.operator_phone IS 'Телефон оператора'
;
COMMENT ON COLUMN ifmp.i50_account.comment IS 'Комментарий'
;
COMMENT ON COLUMN ifmp.i50_account.is_email_subscriber IS 'Нужно ли оповещать о системных событиях по email'
;
COMMENT ON COLUMN ifmp.i50_account.is_phone_subscriber IS 'Нужно ли оповещать о системных событиях по телефону'
;
COMMENT ON COLUMN ifmp.i50_account.depo_name IS 'Транспортное предприятие'
;
COMMENT ON COLUMN ifmp.i50_account.facility_name IS 'Сторонняя организация-контрагент'
;

-- Create indexes for table ifmp.i50_account

CREATE INDEX IX_Relationship247 ON ifmp.i50_account (depo_name)
;

CREATE INDEX IX_Relationship248 ON ifmp.i50_account (facility_name)
;
COMMENT ON INDEX ifmp.IX_Relationship247 IS NULL
;
COMMENT ON INDEX ifmp.IX_Relationship248 IS NULL
;

-- Add keys for table ifmp.i50_account

ALTER TABLE ifmp.i50_account ADD CONSTRAINT pk_account_account_id PRIMARY KEY (login)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_account_account_id ON ifmp.i50_account IS NULL
;

-- Table ifmp.i53_ad_account

CREATE TABLE ifmp.i53_ad_account(
 login Text NOT NULL,
 ad_domain_id Integer NOT NULL,
 ad_name Text NOT NULL,
 guid Text NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE ifmp.i53_ad_account IS 'Привязка аккаунта к Active Directory'
;
COMMENT ON COLUMN ifmp.i53_ad_account.login IS 'Учетная запись пользователя'
;
COMMENT ON COLUMN ifmp.i53_ad_account.ad_domain_id IS 'Домен AD'
;
COMMENT ON COLUMN ifmp.i53_ad_account.ad_name IS 'Имя в AD'
;
COMMENT ON COLUMN ifmp.i53_ad_account.guid IS 'ObjectGUID в AD'
;

-- Add keys for table ifmp.i53_ad_account

ALTER TABLE ifmp.i53_ad_account ADD CONSTRAINT pk_ad_account PRIMARY KEY (login)
 USING INDEX TABLESPACE adm_idx
;

ALTER TABLE ifmp.i53_ad_account ADD CONSTRAINT uq_ad_account_guid UNIQUE (guid)
 USING INDEX TABLESPACE adm_idx
;
COMMENT ON CONSTRAINT pk_ad_account ON ifmp.i53_ad_account IS NULL
;
COMMENT ON CONSTRAINT uq_ad_account_guid ON ifmp.i53_ad_account IS NULL
;

-- Table ifmp.i51_group

CREATE TABLE ifmp.i51_group(
 group_name Text NOT NULL,
 comment Text,
 is_personal Boolean NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE ifmp.i51_group IS 'Группа пользователей'
;
COMMENT ON COLUMN ifmp.i51_group.group_name IS 'Наименование группы'
;
COMMENT ON COLUMN ifmp.i51_group.comment IS 'Комментарий'
;
COMMENT ON COLUMN ifmp.i51_group.is_personal IS 'Флаг определяющий скрытая это группа или нет, для выделения индивидуальных прав пользователя'
;

-- Add keys for table ifmp.i51_group

ALTER TABLE ifmp.i51_group ADD CONSTRAINT pk_group PRIMARY KEY (group_name)
 USING INDEX TABLESPACE adm_idx
;
COMMENT ON CONSTRAINT pk_group ON ifmp.i51_group IS NULL
;

-- Table ifmp.i54_account2group

CREATE TABLE ifmp.i54_account2group(
 login Text NOT NULL,
 group_name Text NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE ifmp.i54_account2group IS 'Связь аккаунтов с группами'
;
COMMENT ON COLUMN ifmp.i54_account2group.login IS 'Учетная запись пользователя'
;
COMMENT ON COLUMN ifmp.i54_account2group.group_name IS 'Группа пользователй'
;

-- Add keys for table ifmp.i54_account2group

ALTER TABLE ifmp.i54_account2group ADD CONSTRAINT pk_account2group PRIMARY KEY (login,group_name)
 USING INDEX TABLESPACE adm_idx
;
COMMENT ON CONSTRAINT pk_account2group ON ifmp.i54_account2group IS NULL
;

-- Table ifmp.i51_ad_group

CREATE TABLE ifmp.i51_ad_group(
 ad_group_name Text NOT NULL,
 guid Text,
 group_name Text NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE ifmp.i51_ad_group IS 'Привязка группы к Active Directory'
;
COMMENT ON COLUMN ifmp.i51_ad_group.ad_group_name IS 'Имя в AD'
;
COMMENT ON COLUMN ifmp.i51_ad_group.guid IS 'ObjectGUID в AD'
;
COMMENT ON COLUMN ifmp.i51_ad_group.group_name IS 'Группа'
;

-- Add keys for table ifmp.i51_ad_group

ALTER TABLE ifmp.i51_ad_group ADD CONSTRAINT pk_ad_group PRIMARY KEY (ad_group_name,group_name)
 USING INDEX TABLESPACE adm_idx
;
COMMENT ON CONSTRAINT pk_ad_group ON ifmp.i51_ad_group IS NULL
;

-- Table ifmp.i56_group_admin

CREATE TABLE ifmp.i56_group_admin(
 login Text NOT NULL,
 group_name Text NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE ifmp.i56_group_admin IS 'Администраторы группы'
;
COMMENT ON COLUMN ifmp.i56_group_admin.login IS 'Учетная запись пользователя'
;
COMMENT ON COLUMN ifmp.i56_group_admin.group_name IS 'Группа'
;

-- Add keys for table ifmp.i56_group_admin

ALTER TABLE ifmp.i56_group_admin ADD CONSTRAINT pk_group_admin PRIMARY KEY (login,group_name)
 USING INDEX TABLESPACE adm_idx
;
COMMENT ON CONSTRAINT pk_group_admin ON ifmp.i56_group_admin IS NULL
;

-- Table ifmp.i57_event_type

CREATE TABLE ifmp.i57_event_type(
 event_type_name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE evt_data
;

COMMENT ON TABLE ifmp.i57_event_type IS 'Тип события'
;
COMMENT ON COLUMN ifmp.i57_event_type.event_type_name IS 'наименование'
;

-- Add keys for table ifmp.i57_event_type

ALTER TABLE ifmp.i57_event_type ADD CONSTRAINT event_type_pk PRIMARY KEY (event_type_name)
 USING INDEX TABLESPACE evt_idx
;
COMMENT ON CONSTRAINT event_type_pk ON ifmp.i57_event_type IS NULL
;

-- Table ifmp.i58_sys_audit

CREATE TABLE ifmp.i58_sys_audit(
 sys_audit_name Text NOT NULL,
 operation Integer NOT NULL,
 comment Text,
 table_name Text NOT NULL,
 query Text,
 message_template Text NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE ifmp.i58_sys_audit IS 'Аудит системных событий'
;
COMMENT ON COLUMN ifmp.i58_sys_audit.sys_audit_name IS 'Название аудита'
;
COMMENT ON COLUMN ifmp.i58_sys_audit.operation IS 'Операция над таблицей, после которой производится аудит
0 - INSERT
1 - UPDATE
2 - DELETE'
;
COMMENT ON COLUMN ifmp.i58_sys_audit.comment IS 'Комментарий'
;
COMMENT ON COLUMN ifmp.i58_sys_audit.table_name IS 'Таблица, при изменении которой будет производиться аудит'
;
COMMENT ON COLUMN ifmp.i58_sys_audit.query IS 'SQL запрос параметров события для формирования сообщения'
;
COMMENT ON COLUMN ifmp.i58_sys_audit.message_template IS 'Шаблон сообщения о событии'
;

-- Add keys for table ifmp.i58_sys_audit

ALTER TABLE ifmp.i58_sys_audit ADD CONSTRAINT pk_sys_audit PRIMARY KEY (sys_audit_name)
 USING INDEX TABLESPACE adm_idx
;
COMMENT ON CONSTRAINT pk_sys_audit ON ifmp.i58_sys_audit IS NULL
;

-- Table ifmp.i59_sys_audit2group

CREATE TABLE ifmp.i59_sys_audit2group(
 group_name Text NOT NULL,
 sys_audit_name Text NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE ifmp.i59_sys_audit2group IS 'Группы Аудита системных событий'
;
COMMENT ON COLUMN ifmp.i59_sys_audit2group.group_name IS 'Группа'
;
COMMENT ON COLUMN ifmp.i59_sys_audit2group.sys_audit_name IS 'Ссистемное событие'
;

-- Add keys for table ifmp.i59_sys_audit2group

ALTER TABLE ifmp.i59_sys_audit2group ADD CONSTRAINT pk_sys_audit2group PRIMARY KEY (group_name,sys_audit_name)
 USING INDEX TABLESPACE adm_idx
;
COMMENT ON CONSTRAINT pk_sys_audit2group ON ifmp.i59_sys_audit2group IS NULL
;

-- Table ifmp.i57_type2group

CREATE TABLE ifmp.i57_type2group(
 event_type_name Text NOT NULL,
 group_name Text NOT NULL,
 can_confirm Boolean NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE ifmp.i57_type2group IS 'Доступные для группы тревожные сигналы и возможность их подверждения'
;
COMMENT ON COLUMN ifmp.i57_type2group.event_type_name IS 'Тип события'
;
COMMENT ON COLUMN ifmp.i57_type2group.group_name IS 'Группа'
;
COMMENT ON COLUMN ifmp.i57_type2group.can_confirm IS 'Могут ли пользователи группы подтвержать события'
;

-- Add keys for table ifmp.i57_type2group

ALTER TABLE ifmp.i57_type2group ADD CONSTRAINT pk_event_type2group PRIMARY KEY (event_type_name,group_name)
;
COMMENT ON CONSTRAINT pk_event_type2group ON ifmp.i57_type2group IS NULL
;

-- Table ifmp.i55_group_owner

CREATE TABLE ifmp.i55_group_owner(
 login Text NOT NULL,
 group_name Text NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE ifmp.i55_group_owner IS 'Владельцы группы'
;
COMMENT ON COLUMN ifmp.i55_group_owner.login IS 'Учетная запись пользователя'
;
COMMENT ON COLUMN ifmp.i55_group_owner.group_name IS 'Группа пользователей'
;

-- Add keys for table ifmp.i55_group_owner

ALTER TABLE ifmp.i55_group_owner ADD CONSTRAINT pk_group_owner PRIMARY KEY (login,group_name)
 USING INDEX TABLESPACE adm_idx
;
COMMENT ON CONSTRAINT pk_group_owner ON ifmp.i55_group_owner IS NULL
;


-- Table ifmp.i60_report

CREATE TABLE ifmp.i60_report(
 report_name Text NOT NULL
)
TABLESPACE jrep_data
;

COMMENT ON TABLE ifmp.i60_report IS 'Отчеты'
;
COMMENT ON COLUMN ifmp.i60_report.report_name IS 'Наименование отчета'
;

-- Add keys for table ifmp.i60_report

ALTER TABLE ifmp.i60_report ADD CONSTRAINT pk_report PRIMARY KEY (report_name)
 USING INDEX TABLESPACE jrep_idx
;
COMMENT ON CONSTRAINT pk_report ON ifmp.i60_report IS NULL
;


-- Table ifmp.i61_report2group

CREATE TABLE ifmp.i61_report2group(
 group_name Text NOT NULL,
 report_name Text NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE ifmp.i61_report2group IS 'Отчеты групп пользователей'
;
COMMENT ON COLUMN ifmp.i61_report2group.group_name IS 'Учетная запись пользователя'
;
COMMENT ON COLUMN ifmp.i61_report2group.report_name IS 'Отчет'
;

-- Create indexes for table ifmp.i61_report2group

CREATE INDEX IX_Relationship266 ON ifmp.i61_report2group (report_name)
;
COMMENT ON INDEX ifmp.IX_Relationship266 IS NULL
;

-- Add keys for table ifmp.i61_report2group

ALTER TABLE ifmp.i61_report2group ADD CONSTRAINT pk_report2group PRIMARY KEY (group_name,report_name)
 USING INDEX TABLESPACE adm_idx
;
COMMENT ON CONSTRAINT pk_report2group ON ifmp.i61_report2group IS NULL
;


-- таблица для количества камер по ТС
create table ifmp.camera2tr2unit
(
 depo_name text constraint depo_ref references ifmp.i06_depo(depo_name) not null,
 garage_num INTEGER /*constraint garage_num_ref references ifmp.i18_tr (garage_num)*/ not null,
 unit_id text /*constraint unit_id_ref references ifmp.i19_unit (unit_num)*/ not null,
 qty_cameras SMALLINT,
 PRIMARY KEY (depo_name, garage_num)
);

create index ix_depo_name_ref on ifmp.camera2tr2unit (depo_name);
create index ix_garage_num_ref on ifmp.camera2tr2unit (garage_num);
create index ix_unit_id_ref on ifmp.camera2tr2unit (unit_id);