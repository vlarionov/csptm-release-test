﻿/*
Created: 30.08.2016
Modified: 06.09.2016
Model: kdbo-physical
Database: PostgreSQL 9.4
*/

DROP SCHEMA core CASCADE;

/*
Created: 30.08.2016
Modified: 07.09.2016
Model: kdbo-physical
Database: PostgreSQL 9.4
*/


-- Create schemas section -------------------------------------------------

CREATE SCHEMA core;

/*
Created: 7/12/2016
Modified: 9/27/2017
Project: csptm
Model: Integration
Database: PostgreSQL 9.4
*/

-- Create sequences section -------------------------------------------------

CREATE SEQUENCE core.equipnemt_addition_equipnemt_addition_id_seq
INCREMENT BY 1
NO MAXVALUE
NO MINVALUE
CACHE 1
;

CREATE SEQUENCE core.sensor2unit_model_sensor2unit_model_id_seq
INCREMENT BY 1
NO MAXVALUE
NO MINVALUE
CACHE 1
;

CREATE SEQUENCE core.sim_card2unit_h_sim_card2unit_h_id_seq
INCREMENT BY 1
NO MAXVALUE
NO MINVALUE
CACHE 1
;

CREATE SEQUENCE core.unit_bnst_has_manipulator_seq
INCREMENT BY 1
NO MAXVALUE
NO MINVALUE
CACHE 1
;

CREATE SEQUENCE core.equipment2tr_h_equipment2tr_h_id_seq
INCREMENT BY 1
NO MAXVALUE
NO MINVALUE
CACHE 1
;

CREATE SEQUENCE core.unit_remove_reason_unit_remove_reason_id_seq
INCREMENT BY 1
NO MAXVALUE
NO MINVALUE
CACHE 1
;

CREATE SEQUENCE core.breakdown_breakdown_id_seq
INCREMENT BY 1
NO MAXVALUE
NO MINVALUE
CACHE 1
;

CREATE SEQUENCE core.breakdown_num_new_seq
INCREMENT BY 1
NO MAXVALUE
NO MINVALUE
CACHE 1
;

CREATE SEQUENCE core.breakdown_status_breakdown_status_id_seq
INCREMENT BY 1
NO MAXVALUE
NO MINVALUE
CACHE 1
;

CREATE SEQUENCE core.breakdown_type_breakdown_type_seq
INCREMENT BY 1
NO MAXVALUE
NO MINVALUE
CACHE 1
;

CREATE SEQUENCE core.close_result_close_result_id_seq
INCREMENT BY 1
NO MAXVALUE
NO MINVALUE
CACHE 1
;

CREATE SEQUENCE core.repair_request_comment_repair_request_comment_id_seq
INCREMENT BY 1
NO MAXVALUE
NO MINVALUE
CACHE 1
;

CREATE SEQUENCE core.repair_request_num_seq
INCREMENT BY 1
NO MAXVALUE
NO MINVALUE
CACHE 1
;

CREATE SEQUENCE core.repair_request_repair_request_id_seq
INCREMENT BY 1
NO MAXVALUE
NO MINVALUE
CACHE 1
;

CREATE SEQUENCE core.repair_request_status_repair_request_status_id_seq
INCREMENT BY 1
NO MAXVALUE
NO MINVALUE
CACHE 1
;

CREATE SEQUENCE core.tr_schema_install_detail_tr_schema_install_detail_id_seq
INCREMENT BY 1
NO MAXVALUE
NO MINVALUE
CACHE 1
;

CREATE SEQUENCE core.tr_schema_install_limit_tr_schema_install_limit_id_seq
INCREMENT BY 1
NO MAXVALUE
NO MINVALUE
CACHE 1
;

CREATE SEQUENCE core.tr_schema_install_tr_schema_install_id_seq
INCREMENT BY 1
NO MAXVALUE
NO MINVALUE
CACHE 1
;

-- Create tables section -------------------------------------------------

-- Table core.protocol

CREATE TABLE core.protocol(
  protocol_id Smallserial NOT NULL,
  name_short Text NOT NULL,
  name_full Text,
  comment Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.protocol IS 'Протокол передачи данных'
;
COMMENT ON COLUMN core.protocol.protocol_id IS 'Протокол передачи данных'
;
COMMENT ON COLUMN core.protocol.name_short IS 'Наименование сокращенное'
;
COMMENT ON COLUMN core.protocol.name_full IS 'Наименование полное'
;
COMMENT ON COLUMN core.protocol.comment IS 'Комментарий'
;

-- Add keys for table core.protocol

ALTER TABLE core.protocol ADD CONSTRAINT pk_protocol_protokol_id PRIMARY KEY (protocol_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.equipment_status

CREATE TABLE core.equipment_status(
  equipment_status_id Smallserial NOT NULL,
  name Text NOT NULL,
  description Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.equipment_status IS 'Статус БО'
;
COMMENT ON COLUMN core.equipment_status.name IS 'Статус'
;
COMMENT ON COLUMN core.equipment_status.description IS 'Комментарий'
;

-- Add keys for table core.equipment_status

ALTER TABLE core.equipment_status ADD CONSTRAINT pk_equipment_status PRIMARY KEY (equipment_status_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.equipment_type

CREATE TABLE core.equipment_type(
  equipment_type_id Smallserial NOT NULL,
  name_full Text NOT NULL,
  name_short Text NOT NULL,
  equipment_class_id Smallint NOT NULL,
  has_diagnostic Boolean DEFAULT true NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.equipment_type IS 'Тип БО'
;
COMMENT ON COLUMN core.equipment_type.equipment_type_id IS 'Тип БО'
;
COMMENT ON COLUMN core.equipment_type.name_full IS 'Наименование типа полное'
;
COMMENT ON COLUMN core.equipment_type.name_short IS 'Наименование сокращенное'
;
COMMENT ON COLUMN core.equipment_type.equipment_class_id IS 'Класс
0- Бортовой блок
1 - Датчик'
;
COMMENT ON COLUMN core.equipment_type.has_diagnostic IS 'Диагностика включена'
;

-- Create indexes for table core.equipment_type

CREATE INDEX ix_equipment_type_class_id ON core.equipment_type (equipment_class_id)
TABLESPACE core_idx
;

-- Add keys for table core.equipment_type

ALTER TABLE core.equipment_type ADD CONSTRAINT pk_equipment_type_equipment_type_id PRIMARY KEY (equipment_type_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.mobile_tariff

CREATE TABLE core.mobile_tariff(
  mobile_tariff_id BigSerial NOT NULL,
  name Text NOT NULL,
  is_active Boolean DEFAULT true NOT NULL,
  comment Text,
  facility_id Bigint
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.mobile_tariff IS 'Тариф'
;
COMMENT ON COLUMN core.mobile_tariff.mobile_tariff_id IS 'Идентификатор'
;
COMMENT ON COLUMN core.mobile_tariff.name IS 'Тарифный план'
;
COMMENT ON COLUMN core.mobile_tariff.is_active IS 'Используется'
;
COMMENT ON COLUMN core.mobile_tariff.comment IS 'Комментарий'
;
COMMENT ON COLUMN core.mobile_tariff.facility_id IS 'Сотовый оператор (Организация)'
;

-- Create indexes for table core.mobile_tariff

CREATE INDEX ix_mobile_tariff_facility_id ON core.mobile_tariff (facility_id)
TABLESPACE core_idx
;

-- Add keys for table core.mobile_tariff

ALTER TABLE core.mobile_tariff ADD CONSTRAINT pk_mobile_tariff_mobile_tariff_id PRIMARY KEY (mobile_tariff_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.sim_card_group

CREATE TABLE core.sim_card_group(
  sim_card_group_id BigSerial NOT NULL,
  name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.sim_card_group IS 'Группа sim-карт'
;
COMMENT ON COLUMN core.sim_card_group.sim_card_group_id IS 'Идентификатор'
;
COMMENT ON COLUMN core.sim_card_group.name IS 'Наименование'
;

-- Add keys for table core.sim_card_group

ALTER TABLE core.sim_card_group ADD CONSTRAINT pk_sim_card_group_sim_card_group_id PRIMARY KEY (sim_card_group_id)
USING INDEX TABLESPACE core_idx
;

-- Create tables section -------------------------------------------------

-- Table core.firmware

CREATE TABLE core.firmware(
  firmware_id BigSerial NOT NULL,
  facility_id Bigint NOT NULL,
  equipment_model_id Integer NOT NULL,
  version Text NOT NULL,
  dt Timestamp NOT NULL,
  port Text,
  ip_address Text,
  comment Text,
  url_ftp Text,
  path_ftp Text,
  ftp_user Text,
  ftp_user_password Text,
  name Character varying(29) NOT NULL,
  file_name Character varying(29)
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
COMMENT ON COLUMN core.firmware.facility_id IS 'Производитель прошивки'
;
COMMENT ON COLUMN core.firmware.equipment_model_id IS 'Модель оборудования'
;
COMMENT ON COLUMN core.firmware.version IS 'Версия прошивки'
;
COMMENT ON COLUMN core.firmware.dt IS 'Дата прошивки'
;
COMMENT ON COLUMN core.firmware.port IS 'Порт FTP'
;
COMMENT ON COLUMN core.firmware.ip_address IS 'IP-адрес  FTP'
;
COMMENT ON COLUMN core.firmware.comment IS 'Комментарий'
;
COMMENT ON COLUMN core.firmware.url_ftp IS 'URL адрес FTP'
;
COMMENT ON COLUMN core.firmware.path_ftp IS 'Путь к папке прошивки '
;
COMMENT ON COLUMN core.firmware.ftp_user IS 'Имя пользователя FTP'
;
COMMENT ON COLUMN core.firmware.ftp_user_password IS 'Пароль пользователя FTP'
;
COMMENT ON COLUMN core.firmware.name IS 'Наименование прошивки'
;
COMMENT ON COLUMN core.firmware.file_name IS 'Ссылка на файл'
;

-- Create indexes for table core.firmware

CREATE INDEX ix_firmware_facility_id ON core.firmware (facility_id)
TABLESPACE core_idx
;

CREATE INDEX ix_firmware_equipment_model_id ON core.firmware (equipment_model_id)
TABLESPACE core_idx
;

-- Add keys for table core.firmware

ALTER TABLE core.firmware ADD CONSTRAINT pk_firmware PRIMARY KEY (firmware_id)
USING INDEX TABLESPACE core_idx
;

-- Create tables section -------------------------------------------------

-- Table core.facility

CREATE TABLE core.facility(
  facility_id Bigint NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.facility IS 'Организации'
;
COMMENT ON COLUMN core.facility.facility_id IS 'Идентификатор организации'
;

-- Add keys for table core.facility

ALTER TABLE core.facility ADD CONSTRAINT pk_facility PRIMARY KEY (facility_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.decommission_reason

CREATE TABLE core.decommission_reason(
  decommission_reason_id Smallserial NOT NULL,
  name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.decommission_reason IS 'Причина списания БО'
;
COMMENT ON COLUMN core.decommission_reason.decommission_reason_id IS 'ID'
;
COMMENT ON COLUMN core.decommission_reason.name IS 'Причина списания БО'
;

-- Add keys for table core.decommission_reason

ALTER TABLE core.decommission_reason ADD CONSTRAINT pk_defeat_reason PRIMARY KEY (decommission_reason_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.unit_bnsr

CREATE TABLE IF NOT EXISTS core.unit_bnsr(
  unit_bnsr_id Bigint NOT NULL,
  channel_num Integer NOT NULL,
  has_manipulator Integer DEFAULT 0,
  usw_num Text,
  last_call Timestamp
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.unit_bnsr IS 'Бортовой блок:БНСР'
;
COMMENT ON COLUMN core.unit_bnsr.unit_bnsr_id IS 'ID бортового блока'
;
COMMENT ON COLUMN core.unit_bnsr.channel_num IS 'Номер канала'
;
COMMENT ON COLUMN core.unit_bnsr.has_manipulator IS 'Манипулятор к комплекте'
;
COMMENT ON COLUMN core.unit_bnsr.usw_num IS 'Номер радиостанции'
;
COMMENT ON COLUMN core.unit_bnsr.last_call IS 'Время последней команды'
;

-- Add keys for table core.unit_bnsr

ALTER TABLE core.unit_bnsr ADD CONSTRAINT pk_unit_bnsr_id PRIMARY KEY (unit_bnsr_id)
USING INDEX TABLESPACE core_idx
;

-- Create tables section -------------------------------------------------

-- Table core.sim_card

CREATE TABLE core.sim_card(
  sim_card_id BigSerial NOT NULL,
  dt_begin Timestamp NOT NULL,
  iccid Text NOT NULL,
  phone_num Text NOT NULL,
  imsi Text,
  mobile_tariff_id Bigint NOT NULL,
  sim_card_group_id Bigint NOT NULL,
  equipment_status_id Smallint NOT NULL,
  depo_id Integer NOT NULL,
  territory_id Bigint
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
COMMENT ON COLUMN core.sim_card.sim_card_id IS 'Sim-карта'
;
COMMENT ON COLUMN core.sim_card.dt_begin IS 'Дата создания'
;
COMMENT ON COLUMN core.sim_card.iccid IS 'Номер sim-карты (ICCID)'
;
COMMENT ON COLUMN core.sim_card.phone_num IS 'Номер телефона'
;
COMMENT ON COLUMN core.sim_card.imsi IS 'IMSI'
;
COMMENT ON COLUMN core.sim_card.mobile_tariff_id IS 'Тариф'
;
COMMENT ON COLUMN core.sim_card.sim_card_group_id IS 'Группа sim-карт'
;
COMMENT ON COLUMN core.sim_card.equipment_status_id IS 'Статус'
;
COMMENT ON COLUMN core.sim_card.depo_id IS 'ТП'
;
COMMENT ON COLUMN core.sim_card.territory_id IS 'Территория ТП'
;

-- Create indexes for table core.sim_card

CREATE INDEX ix_sim_card_sim_card_group_id ON core.sim_card (sim_card_group_id)
TABLESPACE core_idx
;

CREATE INDEX ix_sim_card_mobile_tariff_id ON core.sim_card (mobile_tariff_id)
TABLESPACE core_idx
;

CREATE INDEX ix_sim_card_equipment_status_id ON core.sim_card (equipment_status_id)
TABLESPACE core_idx
;

CREATE INDEX ix_sim_card_equipment_depo_id ON core.sim_card (depo_id)
TABLESPACE core_idx
;

-- Add keys for table core.sim_card

ALTER TABLE core.sim_card ADD CONSTRAINT pk_sim_card_sim_card_id PRIMARY KEY (sim_card_id)
USING INDEX TABLESPACE core_idx
;

-- Create tables section -------------------------------------------------

-- Table core.sim_card2unit

CREATE TABLE core.sim_card2unit(
  sim_card_id Bigint NOT NULL,
  equipment_id Bigint NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.sim_card2unit IS 'Sim карта в блоке'
;
COMMENT ON COLUMN core.sim_card2unit.sim_card_id IS 'Sim-карта'
;
COMMENT ON COLUMN core.sim_card2unit.equipment_id IS 'ID бортового блока'
;

-- Add keys for table core.sim_card2unit

ALTER TABLE core.sim_card2unit ADD CONSTRAINT pk_sim_card2unit PRIMARY KEY (sim_card_id,equipment_id)
USING INDEX TABLESPACE core_idx
;

-- Create tables section -------------------------------------------------

-- Table core.unit

CREATE TABLE core.unit(
  unit_id Bigint NOT NULL,
  unit_num Text NOT NULL,
  hub_id Integer
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.unit IS 'Бортовой блок'
;
COMMENT ON COLUMN core.unit.unit_num IS 'Уникальный номер'
;
COMMENT ON COLUMN core.unit.hub_id IS 'Сервис приема-передачи данных'
;

-- Create indexes for table core.unit

CREATE INDEX ix_unit_hub_id ON core.unit (hub_id)
TABLESPACE core_idx
;

CREATE UNIQUE INDEX uq_unit_unit_num_hub_id ON core.unit (unit_num,hub_id)
TABLESPACE core_idx
;

-- Add keys for table core.unit

ALTER TABLE core.unit ADD CONSTRAINT pk_unit PRIMARY KEY (unit_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.department

CREATE TABLE core.department(
  department_id Bigint NOT NULL,
  parent_id Bigint,
  dt_begin Timestamp NOT NULL,
  dt_end Timestamp,
  code Integer NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.department IS 'Подразделения'
;
COMMENT ON COLUMN core.department.department_id IS 'Идентификатор подразделения'
;
COMMENT ON COLUMN core.department.parent_id IS 'Идентификатор вышестоящего подразделения'
;
COMMENT ON COLUMN core.department.dt_begin IS 'Дата ввода в действие'
;
COMMENT ON COLUMN core.department.dt_end IS 'Дата ликвидации'
;
COMMENT ON COLUMN core.department.code IS 'Код подразделения'
;

-- Create indexes for table core.department

CREATE INDEX ix_department_parent_id ON core.department (parent_id)
TABLESPACE core_idx
;

-- Add keys for table core.department

ALTER TABLE core.department ADD CONSTRAINT pk_department PRIMARY KEY (department_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.unit_service_type

CREATE TABLE core.unit_service_type(
  unit_service_type_id Smallserial NOT NULL,
  name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.unit_service_type IS 'Вид тех. обслуживания_const'
;
COMMENT ON COLUMN core.unit_service_type.unit_service_type_id IS 'Вид тех. обслуживания'
;
COMMENT ON COLUMN core.unit_service_type.name IS 'Вид тех. обслуживания'
;

-- Add keys for table core.unit_service_type

ALTER TABLE core.unit_service_type ADD CONSTRAINT pk_unit_service_type PRIMARY KEY (unit_service_type_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.territory

CREATE TABLE core.territory(
  territory_id Bigint NOT NULL,
  code Integer NOT NULL,
  dt_begin Timestamp NOT NULL,
  dt_end Timestamp,
  wkt_geom Text,
  geom geometry,
  sign_deleted Smallint DEFAULT 0,
  park_zone_muid Bigint
)
TABLESPACE core_data
;

COMMENT ON TABLE core.territory IS 'Территория'
;
COMMENT ON COLUMN core.territory.territory_id IS 'Идентификатор территории'
;
COMMENT ON COLUMN core.territory.code IS 'Номер территории'
;
COMMENT ON COLUMN core.territory.dt_begin IS 'Дата ввода в действие'
;
COMMENT ON COLUMN core.territory.dt_end IS 'Дата ликвидации'
;
COMMENT ON COLUMN core.territory.wkt_geom IS 'Координаты'
;
COMMENT ON COLUMN core.territory.geom IS 'Геометрия'
;
COMMENT ON COLUMN core.territory.sign_deleted IS 'удален'
;
COMMENT ON COLUMN core.territory.park_zone_muid IS 'территория парка из ГИС'
;

-- Add keys for table core.territory

ALTER TABLE core.territory ADD CONSTRAINT pk_terrirtory_id PRIMARY KEY (territory_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.sensor

CREATE TABLE core.sensor(
  sensor_id Bigint NOT NULL,
  sensor_num Bigint
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.sensor IS 'Датчик'
;

-- Add keys for table core.sensor

ALTER TABLE core.sensor ADD CONSTRAINT pk_sensor_sensor_id PRIMARY KEY (sensor_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.tr

CREATE TABLE core.tr(
  tr_id BigSerial NOT NULL,
  seat_qty Integer DEFAULT 0 NOT NULL,
  has_low_floor Boolean DEFAULT false NOT NULL,
  has_conditioner Boolean DEFAULT false NOT NULL,
  garage_num Integer NOT NULL,
  licence Text,
  serial_num Text,
  dt_begin Timestamp NOT NULL,
  dt_end Timestamp,
  tr_type_id Smallint NOT NULL,
  tr_status_id Bigint DEFAULT 0 NOT NULL,
  tr_model_id Bigint NOT NULL,
  territory_id Bigint,
  park_instance_id Text DEFAULT '-'::text NOT NULL,
  has_wheelchair_space Boolean,
  has_bicicle_space Boolean,
  seat_qty_disabled_people Integer,
  seat_qty_total Integer NOT NULL,
  depo_id Bigint,
  year_of_issue Integer NOT NULL,
  garage_num_add Integer,
  equipment_afixed Text,
  tr_schema_install_id Integer,
  sys_period tstzrange DEFAULT tstzrange(current_timestamp, null) NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.tr IS 'Транспортное средство'
;
COMMENT ON COLUMN core.tr.seat_qty IS 'Кол-во сидячих мест'
;
COMMENT ON COLUMN core.tr.has_low_floor IS 'Признак низкопольности'
;
COMMENT ON COLUMN core.tr.has_conditioner IS 'Наличие кондиционера'
;
COMMENT ON COLUMN core.tr.garage_num IS 'Гаражный номер'
;
COMMENT ON COLUMN core.tr.licence IS 'Государственный номер'
;
COMMENT ON COLUMN core.tr.serial_num IS 'Заводской (серийный) номер'
;
COMMENT ON COLUMN core.tr.dt_begin IS 'Дата ввода в эксплуатацию'
;
COMMENT ON COLUMN core.tr.dt_end IS 'Дата списания'
;
COMMENT ON COLUMN core.tr.tr_type_id IS 'Вид ТС'
;
COMMENT ON COLUMN core.tr.tr_status_id IS 'Статус ТС'
;
COMMENT ON COLUMN core.tr.tr_model_id IS 'Модель ТС'
;
COMMENT ON COLUMN core.tr.territory_id IS 'Территории'
;
COMMENT ON COLUMN core.tr.park_instance_id IS 'ID парка в АСДУ'
;
COMMENT ON COLUMN core.tr.has_wheelchair_space IS 'признак наличия площадки (и/или специальных креплений) для инвалидной коляски'
;
COMMENT ON COLUMN core.tr.has_bicicle_space IS 'признак наличия площадки для велосипеда'
;
COMMENT ON COLUMN core.tr.seat_qty_disabled_people IS 'количество сидячих мест для инвалидов'
;
COMMENT ON COLUMN core.tr.seat_qty_total IS 'общее число место мест в ТС'
;
COMMENT ON COLUMN core.tr.depo_id IS 'Транспортное предприятие'
;
COMMENT ON COLUMN core.tr.year_of_issue IS 'Год выпуска'
;
COMMENT ON COLUMN core.tr.garage_num_add IS 'Дополнительный гаражный номер'
;
COMMENT ON COLUMN core.tr.equipment_afixed IS 'Закрепленное за ТС оборудование'
;
COMMENT ON COLUMN core.tr.tr_schema_install_id IS 'Схема подключения БО'
;
COMMENT ON COLUMN core.tr.sys_period IS 'Фиксация изменений'
;

-- Create indexes for table core.tr

CREATE INDEX ix_tr_territory_id ON core.tr (territory_id)
TABLESPACE core_idx
;

CREATE INDEX ix_tr_tr_type ON core.tr (tr_type_id)
TABLESPACE core_idx
;

CREATE INDEX ix_tr_status_id ON core.tr (tr_status_id)
TABLESPACE core_idx
;

CREATE INDEX ix_tr_model_id ON core.tr (tr_model_id)
TABLESPACE core_idx
;

CREATE INDEX ix_tr_depo_id ON core.tr (depo_id)
TABLESPACE core_idx
;

CREATE INDEX ix_tr_schema_install ON core.tr (tr_schema_install_id)
TABLESPACE core_idx
;

CREATE UNIQUE INDEX ixunq_tr_garage_num_depo ON core.tr (garage_num,depo_id)
TABLESPACE core_idx
  WHERE (garage_num <> '-1'::integer)
;

-- Add keys for table core.tr

ALTER TABLE core.tr ADD CONSTRAINT pk_tr_id PRIMARY KEY (tr_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.tr_capacity

CREATE TABLE core.tr_capacity(
  tr_capacity_id Smallserial NOT NULL,
  qty Integer NOT NULL,
  full_name Text NOT NULL,
  short_name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.tr_capacity IS 'Вместимость ТС'
;
COMMENT ON COLUMN core.tr_capacity.tr_capacity_id IS 'Вместимость ТС'
;
COMMENT ON COLUMN core.tr_capacity.qty IS 'Число мест'
;
COMMENT ON COLUMN core.tr_capacity.full_name IS 'Полное наименование'
;
COMMENT ON COLUMN core.tr_capacity.short_name IS 'Краткое наименование'
;

-- Add keys for table core.tr_capacity

ALTER TABLE core.tr_capacity ADD CONSTRAINT pk_tr_capacity PRIMARY KEY (tr_capacity_id)
USING INDEX TABLESPACE core_idx
;
ALTER TABLE core.tr_capacity ADD CONSTRAINT short_name UNIQUE (short_name)
;

-- Table core.tr_mark

CREATE TABLE core.tr_mark(
  tr_mark_id BigSerial NOT NULL,
  name Text NOT NULL,
  facility_id Bigint
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.tr_mark IS 'Марка ТС'
;
COMMENT ON COLUMN core.tr_mark.tr_mark_id IS 'Марка ТС'
;
COMMENT ON COLUMN core.tr_mark.facility_id IS 'Производитель'
;

-- Create indexes for table core.tr_mark

CREATE INDEX ix_tr_mark_facility ON core.tr_mark (facility_id)
TABLESPACE core_idx
;

-- Add keys for table core.tr_mark

ALTER TABLE core.tr_mark ADD CONSTRAINT fk_tr_mark_tr_mark_id PRIMARY KEY (tr_mark_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.depo

CREATE TABLE core.depo(
  depo_id Bigint NOT NULL,
  tn_instance Text NOT NULL,
  carrier_id Bigint NOT NULL,
  park_muid Bigint,
  tr_type_id Smallint NOT NULL,
  wkt_geom Text,
  geom geometry,
  sign_deleted Smallint DEFAULT 0 NOT NULL,
  sys_period tstzrange DEFAULT tstzrange(now(), NULL::timestamp with time zone) NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.depo IS 'Транспортное предприятие'
;
COMMENT ON COLUMN core.depo.depo_id IS 'Идентификатор Транспортное предприятие'
;
COMMENT ON COLUMN core.depo.tn_instance IS 'Код предприятия'
;
COMMENT ON COLUMN core.depo.carrier_id IS 'Идентификатор Транспортная организация'
;
COMMENT ON COLUMN core.depo.park_muid IS 'id парка из ГИС'
;
COMMENT ON COLUMN core.depo.tr_type_id IS 'Вид транспорта'
;
COMMENT ON COLUMN core.depo.wkt_geom IS 'Координаты'
;
COMMENT ON COLUMN core.depo.geom IS 'геометрия'
;
COMMENT ON COLUMN core.depo.sign_deleted IS 'удален'
;

-- Create indexes for table core.depo

CREATE INDEX ix_depo_carrier_id ON core.depo (carrier_id)
TABLESPACE core_idx
;

CREATE UNIQUE INDEX uq_depo_tn_instance ON core.depo (tn_instance)
TABLESPACE core_idx
;

CREATE INDEX ix_depo_park_muid ON core.depo (park_muid)
TABLESPACE core_idx
;

-- Add keys for table core.depo

ALTER TABLE core.depo ADD CONSTRAINT pk_depo PRIMARY KEY (depo_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.tr_model

CREATE TABLE core.tr_model(
  tr_model_id BigSerial NOT NULL,
  name Text NOT NULL,
  tr_mark_id Bigint NOT NULL,
  tr_capacity_id Smallint NOT NULL,
  has_low_floor Boolean,
  has_conditioner Boolean,
  has_wheelchair_space Boolean,
  has_bicicle_space Boolean,
  seat_qty Integer,
  seat_qty_disabled_people Integer,
  seat_qty_total Integer NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.tr_model IS 'Модель ТС'
;
COMMENT ON COLUMN core.tr_model.name IS 'Наименование модели ТС'
;
COMMENT ON COLUMN core.tr_model.tr_mark_id IS 'Марка ТС'
;
COMMENT ON COLUMN core.tr_model.tr_capacity_id IS 'вместимость ТС'
;
COMMENT ON COLUMN core.tr_model.has_low_floor IS 'признак низкопольности'
;
COMMENT ON COLUMN core.tr_model.has_conditioner IS 'признак наличия кондиционера'
;
COMMENT ON COLUMN core.tr_model.has_wheelchair_space IS 'признак наличия площадки (и/или специальных креплений) для инвалидной коляски'
;
COMMENT ON COLUMN core.tr_model.has_bicicle_space IS 'признак наличия площадки для велосипеда;'
;
COMMENT ON COLUMN core.tr_model.seat_qty IS 'количество сидячих мест'
;
COMMENT ON COLUMN core.tr_model.seat_qty_disabled_people IS 'количество сидячих мест для инвалидов'
;
COMMENT ON COLUMN core.tr_model.seat_qty_total IS 'общее число место мест в ТС'
;

-- Create indexes for table core.tr_model

CREATE INDEX ix_tr_model_tr_mark_id ON core.tr_model (tr_mark_id)
TABLESPACE core_idx
;

CREATE INDEX ix_tr_model_tr_capacity_id ON core.tr_model (tr_capacity_id)
TABLESPACE core_idx
;

-- Add keys for table core.tr_model

ALTER TABLE core.tr_model ADD CONSTRAINT tr_model_tr_model_id PRIMARY KEY (tr_model_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.tr_status

CREATE TABLE core.tr_status(
  tr_status_id BigSerial NOT NULL,
  name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.tr_status IS 'Статус ТС'
;
COMMENT ON COLUMN core.tr_status.tr_status_id IS 'Идентификатор Статус ТС'
;
COMMENT ON COLUMN core.tr_status.name IS 'Статус ТС'
;

-- Add keys for table core.tr_status

ALTER TABLE core.tr_status ADD CONSTRAINT pk_tr_status_tr_status_id PRIMARY KEY (tr_status_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.equipment_model

CREATE TABLE core.equipment_model(
  equipment_model_id Serial NOT NULL,
  name Text NOT NULL,
  equipment_type_id Smallint NOT NULL,
  facility_id Bigint,
  has_sim Boolean DEFAULT false,
  has_diagnostic Boolean DEFAULT true NOT NULL,
  input_digital Smallint,
  input_analog Smallint
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.equipment_model IS 'МодельБО'
;
COMMENT ON COLUMN core.equipment_model.equipment_model_id IS 'Модель БО'
;
COMMENT ON COLUMN core.equipment_model.name IS 'Наименование модели'
;
COMMENT ON COLUMN core.equipment_model.equipment_type_id IS 'Тип БО'
;
COMMENT ON COLUMN core.equipment_model.facility_id IS 'Производитель'
;
COMMENT ON COLUMN core.equipment_model.has_sim IS 'Наличие sim-карты'
;
COMMENT ON COLUMN core.equipment_model.has_diagnostic IS 'Диагностика включена'
;
COMMENT ON COLUMN core.equipment_model.input_digital IS 'Количество цифровых входов'
;
COMMENT ON COLUMN core.equipment_model.input_analog IS 'Количество аналоговых входов'
;

-- Create indexes for table core.equipment_model

CREATE INDEX ix_equipment_type_id ON core.equipment_model (equipment_type_id)
TABLESPACE core_idx
;

CREATE INDEX ix_facility_id ON core.equipment_model (facility_id)
TABLESPACE core_idx
;

-- Add keys for table core.equipment_model

ALTER TABLE core.equipment_model ADD CONSTRAINT pk_equipment_model PRIMARY KEY (equipment_model_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.facility_type

CREATE TABLE core.facility_type(
  facility_type_id BigSerial NOT NULL,
  name Text
)
TABLESPACE core_data
;

COMMENT ON TABLE core.facility_type IS 'Типы организаций'
;
COMMENT ON COLUMN core.facility_type.facility_type_id IS 'Идентификатор'
;

-- Add keys for table core.facility_type

ALTER TABLE core.facility_type ADD CONSTRAINT pk_facility_type PRIMARY KEY (facility_type_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.equipment

CREATE TABLE core.equipment(
  equipment_id BigSerial NOT NULL,
  firmware_id Bigint,
  unit_service_type_id Smallint NOT NULL,
  facility_id Bigint NOT NULL,
  equipment_status_id Smallint NOT NULL,
  dt_begin Timestamp NOT NULL,
  serial_num Text,
  equipment_model_id Integer,
  depo_id Bigint NOT NULL,
  territory_id Bigint,
  unit_service_facility_id Bigint,
  has_diagnostic Boolean DEFAULT true NOT NULL,
  comment Text,
  external_id Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.equipment IS 'Оборудование'
;
COMMENT ON COLUMN core.equipment.equipment_id IS 'Идентификатор Оборудования'
;
COMMENT ON COLUMN core.equipment.firmware_id IS 'Прошивка БО'
;
COMMENT ON COLUMN core.equipment.unit_service_type_id IS 'Вид тех. обслуживания'
;
COMMENT ON COLUMN core.equipment.facility_id IS 'Производитель'
;
COMMENT ON COLUMN core.equipment.equipment_status_id IS 'Статус'
;
COMMENT ON COLUMN core.equipment.dt_begin IS 'Дата создания'
;
COMMENT ON COLUMN core.equipment.serial_num IS 'Серийный номер'
;
COMMENT ON COLUMN core.equipment.equipment_model_id IS 'Модель БО'
;
COMMENT ON COLUMN core.equipment.depo_id IS 'Транспортное предприятие'
;
COMMENT ON COLUMN core.equipment.territory_id IS 'Территория транспортного предприятия'
;
COMMENT ON COLUMN core.equipment.unit_service_facility_id IS 'Обслуживающая организация
'
;
COMMENT ON COLUMN core.equipment.has_diagnostic IS 'Диагностика включена'
;
COMMENT ON COLUMN core.equipment.comment IS 'Комментарий'
;
COMMENT ON COLUMN core.equipment.external_id IS 'Внешний идентификатор (загрузка/выгрузка)'
;

-- Create indexes for table core.equipment

CREATE INDEX ix_equipment_firmware_id ON core.equipment (firmware_id)
TABLESPACE core_idx
;

CREATE INDEX ix_equipment_unit_service_type_id ON core.equipment (unit_service_type_id)
TABLESPACE core_idx
;

CREATE INDEX ix_equipment_facility_id ON core.equipment (facility_id)
TABLESPACE core_idx
;

CREATE INDEX ix_equipment_equipment_status_id ON core.equipment (equipment_status_id)
TABLESPACE core_idx
;

CREATE INDEX ix_equipment_equipment_model_id ON core.equipment (equipment_model_id)
TABLESPACE core_idx
;

CREATE INDEX ix_equipment_depo_id ON core.equipment (depo_id)
TABLESPACE core_idx
;

CREATE INDEX ix_equipment_territory_id ON core.equipment (territory_id)
TABLESPACE core_idx
;

CREATE INDEX ix_equipment_unit_service_facility_id ON core.equipment (unit_service_facility_id)
TABLESPACE core_idx
;

-- Add keys for table core.equipment

ALTER TABLE core.equipment ADD CONSTRAINT pk_equipment PRIMARY KEY (equipment_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.sensor2unit

CREATE TABLE core.sensor2unit(
  sensor_id Bigint NOT NULL,
  unit_id Bigint NOT NULL,
  sensor_num Bigint NOT NULL,
  sys_period tstzrange DEFAULT tstzrange(now(), NULL::timestamp with time zone) NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
COMMENT ON COLUMN core.sensor2unit.unit_id IS 'Бортовой блок'
;
COMMENT ON COLUMN core.sensor2unit.sensor_num IS 'Номер датчика'
;

-- Create indexes for table core.sensor2unit

CREATE UNIQUE INDEX ux_unit_id_sensor_num ON core.sensor2unit (unit_id,sensor_num)
TABLESPACE core_idx
;

CREATE INDEX ix_sensor2unit_sys_period ON core.sensor2unit USING gist (sys_period)
TABLESPACE core_idx
;

-- Add keys for table core.sensor2unit

ALTER TABLE core.sensor2unit ADD CONSTRAINT pk_sensor2unit_sensor_id PRIMARY KEY (sensor_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.tr_type

CREATE TABLE core.tr_type(
  tr_type_id Smallserial NOT NULL,
  name Text NOT NULL,
  transport_kind_muid Bigint,
  short_name Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.tr_type IS 'Вид ТС'
;
COMMENT ON COLUMN core.tr_type.tr_type_id IS 'Вид ТС'
;
COMMENT ON COLUMN core.tr_type.name IS 'Наименование Вид ТС'
;
COMMENT ON COLUMN core.tr_type.transport_kind_muid IS 'Вид ТС  из ГИС'
;
COMMENT ON COLUMN core.tr_type.short_name IS 'Краткое наименование'
;

-- Create indexes for table core.tr_type

CREATE UNIQUE INDEX ix_transport_kind_muid ON core.tr_type (transport_kind_muid)
TABLESPACE core_idx
;

-- Add keys for table core.tr_type

ALTER TABLE core.tr_type ADD CONSTRAINT pk_tr_type PRIMARY KEY (tr_type_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.installation_site

CREATE TABLE core.installation_site(
  installation_site_id BigSerial NOT NULL,
  name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.installation_site IS 'Место установки'
;
COMMENT ON COLUMN core.installation_site.installation_site_id IS 'ID'
;
COMMENT ON COLUMN core.installation_site.name IS 'Наименование'
;

-- Add keys for table core.installation_site

ALTER TABLE core.installation_site ADD CONSTRAINT pk_installation_site_installation_site_id PRIMARY KEY (installation_site_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.equipment_class

CREATE TABLE core.equipment_class(
  equipment_class_id Smallserial NOT NULL,
  name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.equipment_class IS 'Класс типа БО'
;
COMMENT ON COLUMN core.equipment_class.equipment_class_id IS 'Класс типа БО'
;
COMMENT ON COLUMN core.equipment_class.name IS 'Наименование'
;

-- Add keys for table core.equipment_class

ALTER TABLE core.equipment_class ADD CONSTRAINT pk_equipment_class_id PRIMARY KEY (equipment_class_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.unit2tr

CREATE TABLE core.unit2tr(
  tr_id Bigint NOT NULL,
  unit_id Bigint NOT NULL,
  sys_period tstzrange DEFAULT tstzrange(now(), NULL::timestamp with time zone) NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.unit2tr IS 'Блоки в транспортном средстве'
;
COMMENT ON COLUMN core.unit2tr.tr_id IS 'ТС'
;

-- Create indexes for table core.unit2tr

CREATE INDEX ix_unit2tr_tr_id ON core.unit2tr (tr_id)
TABLESPACE core_idx
;

CREATE INDEX ix_unit2tr_sys_period ON core.unit2tr USING gist (sys_period)
TABLESPACE core_idx
;

-- Add keys for table core.unit2tr

ALTER TABLE core.unit2tr ADD CONSTRAINT unit2tr_unit_id_pk PRIMARY KEY (unit_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.account

CREATE TABLE core.account(
  account_id BigSerial NOT NULL,
  last_name Text NOT NULL,
  first_name Text,
  middle_name Text,
  login Text NOT NULL,
  password Text NOT NULL,
  depo_id Bigint,
  facility_id Bigint,
  position Text,
  dt_insert Timestamp DEFAULT now(),
  dt_update Timestamp,
  dt_lock Timestamp,
  is_locked Boolean DEFAULT true,
  is_deleted Boolean DEFAULT false NOT NULL,
  email Text,
  cell_phone Text,
  work_phone Text,
  operator_phone Text,
  comment Text,
  is_email_subscriber Boolean DEFAULT false NOT NULL,
  is_phone_subscriber Boolean DEFAULT false
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.account IS 'Пользователь'
;
COMMENT ON COLUMN core.account.account_id IS 'Пользователь'
;
COMMENT ON COLUMN core.account.last_name IS 'Фамилия'
;
COMMENT ON COLUMN core.account.first_name IS 'Имя'
;
COMMENT ON COLUMN core.account.middle_name IS 'Отчество'
;
COMMENT ON COLUMN core.account.login IS 'Логин'
;
COMMENT ON COLUMN core.account.password IS 'Пароль'
;
COMMENT ON COLUMN core.account.depo_id IS 'Транспортное предприятие'
;
COMMENT ON COLUMN core.account.facility_id IS 'Сторонняя организация-контрагент'
;
COMMENT ON COLUMN core.account.position IS 'Должность'
;
COMMENT ON COLUMN core.account.dt_insert IS 'Дата создания'
;
COMMENT ON COLUMN core.account.dt_update IS 'Дата изменения'
;
COMMENT ON COLUMN core.account.dt_lock IS 'Дата блокировки'
;
COMMENT ON COLUMN core.account.is_locked IS 'Аккаунт блокирован'
;
COMMENT ON COLUMN core.account.is_deleted IS 'Удален'
;
COMMENT ON COLUMN core.account.email IS 'Email'
;
COMMENT ON COLUMN core.account.cell_phone IS 'Мобильный телефон'
;
COMMENT ON COLUMN core.account.work_phone IS 'Рабочий телефон'
;
COMMENT ON COLUMN core.account.operator_phone IS 'Телефон оператора'
;
COMMENT ON COLUMN core.account.comment IS 'Комментарий'
;
COMMENT ON COLUMN core.account.is_email_subscriber IS 'Нужно ли оповещать о системных событиях по email'
;
COMMENT ON COLUMN core.account.is_phone_subscriber IS 'Нужно ли оповещать о системных событиях по телефону'
;

-- Create indexes for table core.account

CREATE INDEX ix_account_facility_id ON core.account (facility_id)
TABLESPACE core_idx
;

CREATE INDEX ix_account_depo_id ON core.account (depo_id)
TABLESPACE core_idx
;

-- Add keys for table core.account

ALTER TABLE core.account ADD CONSTRAINT pk_account_account_id PRIMARY KEY (account_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.address

CREATE TABLE core.address(
  address_id BigSerial NOT NULL,
  address_text Text NOT NULL,
  entity_id Bigint NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.address IS 'Адрес'
;
COMMENT ON COLUMN core.address.address_id IS 'Идентификатор адреса'
;
COMMENT ON COLUMN core.address.address_text IS 'Адрес'
;
COMMENT ON COLUMN core.address.entity_id IS 'Идентификатор сущности'
;

-- Create indexes for table core.address

CREATE INDEX ix_address_entity_id ON core.address (entity_id)
TABLESPACE core_idx
;

-- Add keys for table core.address

ALTER TABLE core.address ADD CONSTRAINT pk_address PRIMARY KEY (address_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.entity

CREATE TABLE core.entity(
  entity_id BigSerial NOT NULL,
  name_full Text,
  name_short Text NOT NULL,
  comment Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.entity IS 'Идентификатор реквизитов'
;
COMMENT ON COLUMN core.entity.entity_id IS 'Идентификатор сущности'
;
COMMENT ON COLUMN core.entity.name_full IS 'Наименование полное'
;
COMMENT ON COLUMN core.entity.name_short IS 'Наименование сокращенное'
;
COMMENT ON COLUMN core.entity.comment IS 'Дополнительно'
;

-- Add keys for table core.entity

ALTER TABLE core.entity ADD CONSTRAINT pk_entity PRIMARY KEY (entity_id)
USING INDEX TABLESPACE core_idx
;

CREATE UNIQUE INDEX ux_entity_name_short ON core.entity (name_short)
TABLESPACE core_idx
;
-- Table core.carrier

CREATE TABLE core.carrier(
  carrier_id Bigint NOT NULL,
  sign_deleted Bigint NOT NULL,
  update_date Timestamp NOT NULL,
  agencies_muid Bigint,
  org_legal_form_id Smallint
)
TABLESPACE core_data
;

COMMENT ON TABLE core.carrier IS 'Транспортная организация'
;
COMMENT ON COLUMN core.carrier.agencies_muid IS 'связь с ГИС'
;
COMMENT ON COLUMN core.carrier.org_legal_form_id IS 'Организационно-правовая форма'
;

-- Create indexes for table core.carrier

CREATE INDEX ix_carrier_org_legal_form_id ON core.carrier (org_legal_form_id)
TABLESPACE core_idx
;

CREATE INDEX ix_carrier_agencies_muid ON core.carrier (agencies_muid)
TABLESPACE core_idx
;

-- Add keys for table core.carrier

ALTER TABLE core.carrier ADD CONSTRAINT pk_carrier PRIMARY KEY (carrier_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.traffic

CREATE TABLE core.traffic(
  packet_id Bigint NOT NULL,
  tr_id Bigint NOT NULL,
  unit_id Bigint NOT NULL,
  event_time Timestamp NOT NULL,
  device_event_id Integer NOT NULL,
  location_valid Boolean NOT NULL,
  gps_time Timestamp,
  lon Real,
  lat Real,
  alt Integer,
  speed Integer,
  heading Integer,
  receive_time Timestamp,
  is_hist_data Boolean DEFAULT false NOT NULL
)
TABLESPACE core_data
;

COMMENT ON TABLE core.traffic IS 'Координаты'
;
COMMENT ON COLUMN core.traffic.packet_id IS 'ID пакета'
;
COMMENT ON COLUMN core.traffic.tr_id IS 'ID ТС'
;
COMMENT ON COLUMN core.traffic.unit_id IS 'ID блока'
;
COMMENT ON COLUMN core.traffic.event_time IS 'Время события'
;
COMMENT ON COLUMN core.traffic.device_event_id IS 'Событие блока'
;
COMMENT ON COLUMN core.traffic.location_valid IS 'Координаты валидны'
;
COMMENT ON COLUMN core.traffic.gps_time IS 'Время координат'
;
COMMENT ON COLUMN core.traffic.lon IS 'Долгота'
;
COMMENT ON COLUMN core.traffic.lat IS 'Широта'
;
COMMENT ON COLUMN core.traffic.alt IS 'Высота'
;
COMMENT ON COLUMN core.traffic.speed IS 'Скорость'
;
COMMENT ON COLUMN core.traffic.heading IS 'Направление'
;
COMMENT ON COLUMN core.traffic.receive_time IS 'Время получения'
;
COMMENT ON COLUMN core.traffic.is_hist_data IS 'Признак историчности данных
false - онлайн данные
true - данные из памяти '
;

-- Create indexes for table core.traffic

CREATE INDEX ix_traffic_unit_id ON core.traffic (unit_id,event_time)
TABLESPACE core_idx
;

CREATE INDEX ix_traffic_tr_id ON core.traffic (tr_id,event_time)
TABLESPACE core_idx
;

-- Table core.tr_last_state

CREATE TABLE core.tr_last_state(
  tr_id Bigint NOT NULL,
  packet_id Bigint NOT NULL,
  unit_id Integer NOT NULL,
  event_time Timestamp NOT NULL,
  device_event_id Integer NOT NULL,
  location_valid Boolean NOT NULL,
  gps_time Timestamp,
  lon Real,
  lat Real,
  alt Integer,
  speed Integer,
  heading Integer
)
TABLESPACE core_data
;

COMMENT ON TABLE core.tr_last_state IS 'Текущее состояние ТС'
;
COMMENT ON COLUMN core.tr_last_state.tr_id IS 'ID ТС'
;
COMMENT ON COLUMN core.tr_last_state.packet_id IS 'ID пакета'
;
COMMENT ON COLUMN core.tr_last_state.unit_id IS 'ID блока'
;
COMMENT ON COLUMN core.tr_last_state.event_time IS 'Время события'
;
COMMENT ON COLUMN core.tr_last_state.device_event_id IS 'Событие блока'
;
COMMENT ON COLUMN core.tr_last_state.location_valid IS 'Координаты валидны'
;
COMMENT ON COLUMN core.tr_last_state.gps_time IS 'Время координат'
;
COMMENT ON COLUMN core.tr_last_state.lon IS 'Долгота'
;
COMMENT ON COLUMN core.tr_last_state.lat IS 'Широта'
;
COMMENT ON COLUMN core.tr_last_state.alt IS 'Высота'
;
COMMENT ON COLUMN core.tr_last_state.speed IS 'Скорость'
;
COMMENT ON COLUMN core.tr_last_state.heading IS 'Направление'
;

-- Add keys for table core.tr_last_state

ALTER TABLE core.tr_last_state ADD CONSTRAINT pk_tr_last_state PRIMARY KEY (tr_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.driver

CREATE TABLE core.driver(
  driver_id Bigint NOT NULL,
  depo_id Bigint,
  driver_name Text NOT NULL,
  driver_last_name Text,
  driver_middle_name Text NOT NULL,
  tab_num Text,
  dt_begin Timestamp,
  dt_end Timestamp,
  sign_deleted Smallint DEFAULT 0 NOT NULL,
  sys_period tstzrange DEFAULT tstzrange(now(), NULL::timestamp with time zone) NOT NULL
)
TABLESPACE core_data
;

COMMENT ON TABLE core.driver IS 'Справочник водителей'
;
COMMENT ON COLUMN core.driver.driver_id IS 'Водитель'
;
COMMENT ON COLUMN core.driver.depo_id IS 'ТП'
;
COMMENT ON COLUMN core.driver.driver_name IS 'Имя'
;
COMMENT ON COLUMN core.driver.driver_last_name IS 'Фамилия'
;
COMMENT ON COLUMN core.driver.driver_middle_name IS 'Отчество'
;
COMMENT ON COLUMN core.driver.tab_num IS 'Табельный номер'
;
COMMENT ON COLUMN core.driver.dt_begin IS 'c'
;
COMMENT ON COLUMN core.driver.dt_end IS 'по'
;
COMMENT ON COLUMN core.driver.sign_deleted IS 'удален'
;

-- Create indexes for table core.driver

CREATE INDEX ix_driver_depo ON core.driver (depo_id)
TABLESPACE core_idx
;

-- Add keys for table core.driver

ALTER TABLE core.driver ADD CONSTRAINT pk_driver PRIMARY KEY (driver_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.week_day

CREATE TABLE core.week_day(
  week_day_id Smallint NOT NULL,
  wd_full_name Text NOT NULL,
  wd_short_name Text NOT NULL
)
TABLESPACE core_data
;

COMMENT ON TABLE core.week_day IS 'Дни недели'
;
COMMENT ON COLUMN core.week_day.week_day_id IS 'ID'
;
COMMENT ON COLUMN core.week_day.wd_full_name IS 'Полное наименование'
;
COMMENT ON COLUMN core.week_day.wd_short_name IS 'Краткое наименование'
;

-- Add keys for table core.week_day

ALTER TABLE core.week_day ADD CONSTRAINT pk_week_day PRIMARY KEY (week_day_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.tr_capacity2gis

CREATE TABLE core.tr_capacity2gis(
  tr_capacity_id Smallint NOT NULL,
  vehicle_type_muid Bigint NOT NULL
)
TABLESPACE core_data
;

COMMENT ON TABLE core.tr_capacity2gis IS 'Связь Вместимости КСУПТ и ГИС'
;
COMMENT ON COLUMN core.tr_capacity2gis.tr_capacity_id IS 'id КСУПТ'
;
COMMENT ON COLUMN core.tr_capacity2gis.vehicle_type_muid IS 'id ГИС'
;

-- Add keys for table core.tr_capacity2gis

ALTER TABLE core.tr_capacity2gis ADD CONSTRAINT pk_tr_capacity2gis PRIMARY KEY (tr_capacity_id,vehicle_type_muid)
USING INDEX TABLESPACE core_idx
;

-- Table core.depo2territory

CREATE TABLE core.depo2territory(
  territory_id Bigint NOT NULL,
  depo_id Bigint NOT NULL,
  dt_begin Timestamp,
  dt_end Timestamp
)
TABLESPACE core_data
;

COMMENT ON TABLE core.depo2territory IS 'Связь м/у парками и территориями'
;
COMMENT ON COLUMN core.depo2territory.territory_id IS 'Территория'
;
COMMENT ON COLUMN core.depo2territory.depo_id IS 'Парк'
;
COMMENT ON COLUMN core.depo2territory.dt_begin IS 'Дата начала'
;
COMMENT ON COLUMN core.depo2territory.dt_end IS 'Дата окончания'
;

-- Add keys for table core.depo2territory

ALTER TABLE core.depo2territory ADD CONSTRAINT pk_depo2territory PRIMARY KEY (territory_id,depo_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.contact

CREATE TABLE core.contact(
  contact_id BigSerial NOT NULL,
  contact_type_id Bigint,
  contact_desc Text,
  phone_num Character(20),
  entity_id Bigint
)
TABLESPACE core_data
;

COMMENT ON TABLE core.contact IS 'Контактная информация'
;
COMMENT ON COLUMN core.contact.contact_type_id IS 'Тип контактной информации'
;
COMMENT ON COLUMN core.contact.contact_desc IS 'Дополнительная информация'
;
COMMENT ON COLUMN core.contact.phone_num IS 'Телефон'
;

-- Create indexes for table core.contact

CREATE INDEX ix_contact_contact_type_id ON core.contact (contact_type_id)
TABLESPACE core_idx
;

CREATE INDEX ix_contact_entity_id ON core.contact (entity_id)
TABLESPACE core_idx
;

-- Add keys for table core.contact

ALTER TABLE core.contact ADD CONSTRAINT pk_contact PRIMARY KEY (contact_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.contact_type

CREATE TABLE core.contact_type(
  contact_type_id BigSerial NOT NULL,
  name_short Text,
  name_full Text NOT NULL
)
TABLESPACE core_data
;

COMMENT ON TABLE core.contact_type IS 'Тип контактной информации'
;
COMMENT ON COLUMN core.contact_type.contact_type_id IS 'Тип контактной информации'
;
COMMENT ON COLUMN core.contact_type.name_short IS 'Краткое наименование'
;
COMMENT ON COLUMN core.contact_type.name_full IS 'Полное наименование'
;

-- Add keys for table core.contact_type

ALTER TABLE core.contact_type ADD CONSTRAINT pk_contact_type PRIMARY KEY (contact_type_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.org_legal_form

CREATE TABLE core.org_legal_form(
  org_legal_form_id Smallint NOT NULL,
  name_full Text NOT NULL,
  name_short Text NOT NULL
)
TABLESPACE core_data
;

COMMENT ON TABLE core.org_legal_form IS 'Организационно-правовые формы предприятий'
;
COMMENT ON COLUMN core.org_legal_form.org_legal_form_id IS 'Организационно-правовые формы предприятий'
;
COMMENT ON COLUMN core.org_legal_form.name_full IS 'Наименование полное'
;
COMMENT ON COLUMN core.org_legal_form.name_short IS 'Наименование краткое'
;

-- Add keys for table core.org_legal_form

ALTER TABLE core.org_legal_form ADD CONSTRAINT pk_org_legal_form PRIMARY KEY (org_legal_form_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.facility2type

CREATE TABLE core.facility2type(
  facility_id Bigint NOT NULL,
  facility_type_id Bigint NOT NULL
)
TABLESPACE core_data
;

COMMENT ON TABLE core.facility2type IS 'Связь контрагентов с типами организаций'
;
COMMENT ON COLUMN core.facility2type.facility_id IS 'Контрагент
'
;
COMMENT ON COLUMN core.facility2type.facility_type_id IS 'Тип организации
'
;

-- Add keys for table core.facility2type

ALTER TABLE core.facility2type ADD CONSTRAINT pk_facility2type PRIMARY KEY (facility_id,facility_type_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.modem

CREATE TABLE core.modem(
  modem_id Bigint NOT NULL,
  imei Text NOT NULL,
  speed Text NOT NULL,
  sim_card_id Bigint
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE core.modem ALTER COLUMN modem_id SET STORAGE PLAIN
;
ALTER TABLE core.modem ALTER COLUMN imei SET STORAGE EXTENDED
;
ALTER TABLE core.modem ALTER COLUMN speed SET STORAGE EXTENDED
;
ALTER TABLE core.modem ALTER COLUMN sim_card_id SET STORAGE PLAIN
;

-- Create indexes for table core.modem

CREATE INDEX ix_modem_sim_card_id ON core.modem (sim_card_id)
TABLESPACE core_idx
;

-- Add keys for table core.modem

ALTER TABLE core.modem ADD CONSTRAINT pk_modem_modem_id PRIMARY KEY (modem_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.modem2unit

CREATE TABLE core.modem2unit(
  unit_id Bigint NOT NULL,
  modem_id Bigint NOT NULL,
  sys_period tstzrange DEFAULT tstzrange(now(), NULL::timestamp with time zone) NOT NULL
)
TABLESPACE core_data
;
ALTER TABLE core.modem2unit ALTER COLUMN unit_id SET STORAGE PLAIN
;
ALTER TABLE core.modem2unit ALTER COLUMN modem_id SET STORAGE PLAIN
;

-- Create indexes for table core.modem2unit

CREATE INDEX ix_modem2unit_sys_period ON core.modem2unit USING gist (sys_period)
TABLESPACE core_idx
;

-- Add keys for table core.modem2unit

ALTER TABLE core.modem2unit ADD CONSTRAINT pk_modem2unit_unit_id_modem_id PRIMARY KEY (unit_id,modem_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.monitor

CREATE TABLE core.monitor(
  monitor_id Bigint NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE core.monitor ALTER COLUMN monitor_id SET STORAGE PLAIN
;

-- Add keys for table core.monitor

ALTER TABLE core.monitor ADD CONSTRAINT pk_monitor_monitor_id PRIMARY KEY (monitor_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.monitor2unit

CREATE TABLE core.monitor2unit(
  unit_id Bigint NOT NULL,
  monitor_id Bigint NOT NULL,
  sys_period tstzrange DEFAULT tstzrange(now(), NULL::timestamp with time zone) NOT NULL
)
TABLESPACE core_data
;
ALTER TABLE core.monitor2unit ALTER COLUMN unit_id SET STORAGE PLAIN
;
ALTER TABLE core.monitor2unit ALTER COLUMN monitor_id SET STORAGE PLAIN
;

-- Create indexes for table core.monitor2unit

CREATE INDEX ix_monitor2unit_sys_period ON core.monitor2unit USING gist (sys_period)
TABLESPACE core_idx
;

-- Add keys for table core.monitor2unit

ALTER TABLE core.monitor2unit ADD CONSTRAINT pk_monitor2unit_unit_id_monitor_id PRIMARY KEY (unit_id,monitor_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.sim_card2unit_h

CREATE TABLE core.sim_card2unit_h(
  sim_card2unit_h_id Bigint DEFAULT nextval('core.sim_card2unit_h_sim_card2unit_h_id_seq'::regclass) NOT NULL,
  dt_begin Timestamp NOT NULL,
  dt_end Timestamp,
  unit_remove_reason Smallint,
  sim_card_id Bigint NOT NULL,
  equipment_id Bigint
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE core.sim_card2unit_h ALTER COLUMN sim_card2unit_h_id SET STORAGE PLAIN
;
ALTER TABLE core.sim_card2unit_h ALTER COLUMN dt_begin SET STORAGE PLAIN
;
ALTER TABLE core.sim_card2unit_h ALTER COLUMN dt_end SET STORAGE PLAIN
;
ALTER TABLE core.sim_card2unit_h ALTER COLUMN unit_remove_reason SET STORAGE PLAIN
;
ALTER TABLE core.sim_card2unit_h ALTER COLUMN sim_card_id SET STORAGE PLAIN
;
ALTER TABLE core.sim_card2unit_h ALTER COLUMN equipment_id SET STORAGE PLAIN
;

COMMENT ON TABLE core.sim_card2unit_h IS 'История подключения Sim карты к блоку'
;
COMMENT ON COLUMN core.sim_card2unit_h.sim_card2unit_h_id IS 'ID'
;
COMMENT ON COLUMN core.sim_card2unit_h.dt_begin IS 'Дата установки'
;
COMMENT ON COLUMN core.sim_card2unit_h.dt_end IS 'Дата снятия'
;
COMMENT ON COLUMN core.sim_card2unit_h.unit_remove_reason IS 'Причина списания'
;
COMMENT ON COLUMN core.sim_card2unit_h.sim_card_id IS 'Sim-карта'
;
COMMENT ON COLUMN core.sim_card2unit_h.equipment_id IS 'Бортовой блок'
;

-- Create indexes for table core.sim_card2unit_h

CREATE INDEX ix_sim_card2unit_h_unit_remove_reason_id ON core.sim_card2unit_h (unit_remove_reason)
TABLESPACE core_idx
;

CREATE INDEX ix_sim_card2unit_h_unit_id ON core.sim_card2unit_h (equipment_id)
TABLESPACE core_idx
;

CREATE INDEX ix__sim_card_h_sim_card_id ON core.sim_card2unit_h (sim_card_id)
TABLESPACE core_idx
;

-- Add keys for table core.sim_card2unit_h

ALTER TABLE core.sim_card2unit_h ADD CONSTRAINT pk_sim_card2unit_h_sim_card2unit_h_id PRIMARY KEY (sim_card2unit_h_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.unit_bnst

CREATE TABLE core.unit_bnst(
  unit_bnst_id Bigint NOT NULL,
  has_manipulator Bigint DEFAULT 0 NOT NULL,
  has_flash_card Integer DEFAULT 0 NOT NULL,
  data_ver Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE core.unit_bnst ALTER COLUMN unit_bnst_id SET STORAGE PLAIN
;
ALTER TABLE core.unit_bnst ALTER COLUMN has_manipulator SET STORAGE PLAIN
;
ALTER TABLE core.unit_bnst ALTER COLUMN has_flash_card SET STORAGE PLAIN
;

COMMENT ON TABLE core.unit_bnst IS 'Блоки БСНТ'
;
COMMENT ON COLUMN core.unit_bnst.unit_bnst_id IS 'БСНТ'
;
COMMENT ON COLUMN core.unit_bnst.has_manipulator IS 'Манипулятор в комплекте'
;
COMMENT ON COLUMN core.unit_bnst.has_flash_card IS 'Флэш-карта в комплекте'
;
COMMENT ON COLUMN core.unit_bnst.data_ver IS 'Прошивка (данные от блока)'
;

-- Add keys for table core.unit_bnst

ALTER TABLE core.unit_bnst ADD CONSTRAINT pk_unit_bnst_id PRIMARY KEY (unit_bnst_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.unit_kbtob

CREATE TABLE core.unit_kbtob(
  unit_kbtob_id Bigint NOT NULL,
  port_qty Integer,
  channel_qty Integer
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE core.unit_kbtob ALTER COLUMN unit_kbtob_id SET STORAGE PLAIN
;
ALTER TABLE core.unit_kbtob ALTER COLUMN port_qty SET STORAGE PLAIN
;
ALTER TABLE core.unit_kbtob ALTER COLUMN channel_qty SET STORAGE PLAIN
;

COMMENT ON TABLE core.unit_kbtob IS 'Блоки КБТОБ'
;
COMMENT ON COLUMN core.unit_kbtob.unit_kbtob_id IS 'Бортовой блок: КБТОБ'
;
COMMENT ON COLUMN core.unit_kbtob.port_qty IS 'Кол-во портов подключения'
;
COMMENT ON COLUMN core.unit_kbtob.channel_qty IS 'Кол-во задействованных каналов'
;

-- Add keys for table core.unit_kbtob

ALTER TABLE core.unit_kbtob ADD CONSTRAINT pk_unit_kbtob_id PRIMARY KEY (unit_kbtob_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.hdd

CREATE TABLE core.hdd(
  type Integer,
  capacity Double precision,
  size Double precision,
  hdd_id Bigint NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE core.hdd ALTER COLUMN type SET STORAGE PLAIN
;
ALTER TABLE core.hdd ALTER COLUMN capacity SET STORAGE PLAIN
;
ALTER TABLE core.hdd ALTER COLUMN size SET STORAGE PLAIN
;
ALTER TABLE core.hdd ALTER COLUMN hdd_id SET STORAGE PLAIN
;
COMMENT ON COLUMN core.hdd.type IS 'Тип диска: 1 - НМЖД или 2 -твердотельный;'
;
COMMENT ON COLUMN core.hdd.capacity IS 'Объем диска, Гб'
;
COMMENT ON COLUMN core.hdd.size IS 'Типоразмер диска, дюйм'
;

-- Add keys for table core.hdd

ALTER TABLE core.hdd ADD CONSTRAINT pk_hdd_hdd_id PRIMARY KEY (hdd_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.hdd2unit

CREATE TABLE core.hdd2unit(
  unit_id Bigint NOT NULL,
  hdd_id Bigint NOT NULL,
  sys_period tstzrange DEFAULT tstzrange(now(), NULL::timestamp with time zone) NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE core.hdd2unit ALTER COLUMN unit_id SET STORAGE PLAIN
;
ALTER TABLE core.hdd2unit ALTER COLUMN hdd_id SET STORAGE PLAIN
;

-- Create indexes for table core.hdd2unit

CREATE INDEX ix_hdd2unit_sys_period ON core.hdd2unit USING gist (sys_period)
TABLESPACE core_idx
;

-- Add keys for table core.hdd2unit

ALTER TABLE core.hdd2unit ADD CONSTRAINT pk_unit_id_hd_id PRIMARY KEY (unit_id,hdd_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.hub

CREATE TABLE core.hub(
  hub_id Serial NOT NULL,
  name Text NOT NULL,
  port Text NOT NULL,
  ip_address Text NOT NULL,
  comment Text,
  protocol_id Smallint
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE core.hub ALTER COLUMN hub_id SET STORAGE PLAIN
;
ALTER TABLE core.hub ALTER COLUMN name SET STORAGE EXTENDED
;
ALTER TABLE core.hub ALTER COLUMN port SET STORAGE EXTENDED
;
ALTER TABLE core.hub ALTER COLUMN ip_address SET STORAGE EXTENDED
;
ALTER TABLE core.hub ALTER COLUMN comment SET STORAGE EXTENDED
;
ALTER TABLE core.hub ALTER COLUMN protocol_id SET STORAGE PLAIN
;

COMMENT ON TABLE core.hub IS 'Сервис приема-передачи данных'
;
COMMENT ON COLUMN core.hub.hub_id IS 'ID'
;
COMMENT ON COLUMN core.hub.name IS 'Наименование'
;
COMMENT ON COLUMN core.hub.port IS 'Порт'
;
COMMENT ON COLUMN core.hub.ip_address IS 'IP-адрес'
;
COMMENT ON COLUMN core.hub.comment IS 'Комментарий'
;
COMMENT ON COLUMN core.hub.protocol_id IS 'Протокол передачи данных'
;

-- Create indexes for table core.hub

CREATE INDEX ix_hub_protokol_id ON core.hub (protocol_id)
TABLESPACE core_idx
;

-- Add keys for table core.hub

ALTER TABLE core.hub ADD CONSTRAINT pk_hub_hub_id PRIMARY KEY (hub_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.equipment2tr

CREATE TABLE core.equipment2tr(
  equipment_id Bigint NOT NULL,
  tr_id Bigint NOT NULL
)
TABLESPACE core_data
;
ALTER TABLE core.equipment2tr ALTER COLUMN equipment_id SET STORAGE PLAIN
;
ALTER TABLE core.equipment2tr ALTER COLUMN tr_id SET STORAGE PLAIN
;

COMMENT ON TABLE core.equipment2tr IS 'Связь БО с ТС'
;
COMMENT ON COLUMN core.equipment2tr.equipment_id IS 'ID'
;
COMMENT ON COLUMN core.equipment2tr.tr_id IS 'ТС'
;

-- Create indexes for table core.equipment2tr

CREATE INDEX ix_equipment2tr_equipment_id ON core.equipment2tr (tr_id)
TABLESPACE core_idx
;

-- Add keys for table core.equipment2tr

ALTER TABLE core.equipment2tr ADD CONSTRAINT pk_equipment2tr PRIMARY KEY (equipment_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.equipment2tr_h

CREATE TABLE core.equipment2tr_h(
  equipment2tr_h_id BigSerial NOT NULL,
  sys_period tsrange DEFAULT tsrange(('now'::text)::timestamp without time zone, NULL::timestamp without time zone) NOT NULL,
  equipment_remove_reason_id Smallint,
  tr_id Bigint NOT NULL,
  equipment_id Bigint NOT NULL,
  installation_site_id Bigint
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE core.equipment2tr_h ALTER COLUMN equipment2tr_h_id SET STORAGE PLAIN
;
ALTER TABLE core.equipment2tr_h ALTER COLUMN sys_period SET STORAGE PLAIN
;
ALTER TABLE core.equipment2tr_h ALTER COLUMN equipment_remove_reason_id SET STORAGE PLAIN
;
ALTER TABLE core.equipment2tr_h ALTER COLUMN tr_id SET STORAGE PLAIN
;
ALTER TABLE core.equipment2tr_h ALTER COLUMN equipment_id SET STORAGE PLAIN
;
ALTER TABLE core.equipment2tr_h ALTER COLUMN installation_site_id SET STORAGE PLAIN
;

COMMENT ON TABLE core.equipment2tr_h IS 'История привязки бортового блока к ТС'
;
COMMENT ON COLUMN core.equipment2tr_h.equipment2tr_h_id IS 'ID'
;
COMMENT ON COLUMN core.equipment2tr_h.sys_period IS 'Период действия'
;
COMMENT ON COLUMN core.equipment2tr_h.equipment_remove_reason_id IS 'Причина снятия'
;
COMMENT ON COLUMN core.equipment2tr_h.tr_id IS 'Транспортное средство'
;
COMMENT ON COLUMN core.equipment2tr_h.equipment_id IS 'БО'
;
COMMENT ON COLUMN core.equipment2tr_h.installation_site_id IS 'Место установки'
;

-- Create indexes for table core.equipment2tr_h

CREATE INDEX ix_equipment2tr_h_remove_reason_id ON core.equipment2tr_h (equipment_remove_reason_id)
TABLESPACE core_idx
;

CREATE INDEX ix_equipment2tr_h_tr_id ON core.equipment2tr_h (tr_id)
TABLESPACE core_idx
;

CREATE INDEX ix_equipment2tr_h_equipment_id ON core.equipment2tr_h (equipment_id)
;

CREATE INDEX ix_equipment2tr_h_instalation_site_id ON core.equipment2tr_h (installation_site_id)
TABLESPACE core_idx
;

CREATE INDEX ix_equipment2tr_h_sys_period ON core.equipment2tr_h USING gist (sys_period)
TABLESPACE core_idx
;

-- Add keys for table core.equipment2tr_h

ALTER TABLE core.equipment2tr_h ADD CONSTRAINT pk_equipment2tr_h_equipment2tr_h_id PRIMARY KEY (equipment2tr_h_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.equipment_remove_reason

CREATE TABLE core.equipment_remove_reason(
  equipment_remove_reason_id Smallserial NOT NULL,
  name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE core.equipment_remove_reason ALTER COLUMN equipment_remove_reason_id SET STORAGE PLAIN
;
ALTER TABLE core.equipment_remove_reason ALTER COLUMN name SET STORAGE EXTENDED
;

COMMENT ON TABLE core.equipment_remove_reason IS 'Причина снятия БО с ТС'
;
COMMENT ON COLUMN core.equipment_remove_reason.equipment_remove_reason_id IS 'ID'
;
COMMENT ON COLUMN core.equipment_remove_reason.name IS 'Причина снятия БО с ТС'
;

-- Add keys for table core.equipment_remove_reason

ALTER TABLE core.equipment_remove_reason ADD CONSTRAINT pk_unit_remove_reason_id PRIMARY KEY (equipment_remove_reason_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.sensor2tr

CREATE TABLE core.sensor2tr(
  sensor_id Bigint NOT NULL,
  tr_id Bigint NOT NULL,
  installation_site_id Bigint
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE core.sensor2tr ALTER COLUMN sensor_id SET STORAGE PLAIN
;
ALTER TABLE core.sensor2tr ALTER COLUMN tr_id SET STORAGE PLAIN
;
ALTER TABLE core.sensor2tr ALTER COLUMN installation_site_id SET STORAGE PLAIN
;

-- Create indexes for table core.sensor2tr

CREATE INDEX ix_sensor2tr_installation_site_id ON core.sensor2tr (installation_site_id)
TABLESPACE core_idx
;

CREATE INDEX ix_sensor2tr_installation_tr_id ON core.sensor2tr (tr_id)
TABLESPACE core_idx
;

-- Add keys for table core.sensor2tr

ALTER TABLE core.sensor2tr ADD CONSTRAINT pk_sensor2tr_sensor_id PRIMARY KEY (sensor_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.breakdown

CREATE TABLE core.breakdown(
  breakdown_id Bigint DEFAULT nextval('core.breakdown_breakdown_id_seq'::regclass) NOT NULL,
  dt_begin Timestamp DEFAULT now() NOT NULL,
  description Text,
  is_confirmed Boolean DEFAULT false NOT NULL,
  dt_end Timestamp,
  tr_id Bigint NOT NULL,
  breakdown_type_id Bigint NOT NULL,
  breakdown_status_id Bigint NOT NULL,
  close_result_id Bigint,
  territory_id Bigint,
  equipment_id Bigint,
  account_id Bigint NOT NULL,
  num Integer DEFAULT nextval('core.breakdown_num_new_seq'::regclass) NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE core.breakdown ALTER COLUMN breakdown_id SET STORAGE PLAIN
;
ALTER TABLE core.breakdown ALTER COLUMN dt_begin SET STORAGE PLAIN
;
ALTER TABLE core.breakdown ALTER COLUMN description SET STORAGE EXTENDED
;
ALTER TABLE core.breakdown ALTER COLUMN is_confirmed SET STORAGE PLAIN
;
ALTER TABLE core.breakdown ALTER COLUMN dt_end SET STORAGE PLAIN
;
ALTER TABLE core.breakdown ALTER COLUMN tr_id SET STORAGE PLAIN
;
ALTER TABLE core.breakdown ALTER COLUMN breakdown_type_id SET STORAGE PLAIN
;
ALTER TABLE core.breakdown ALTER COLUMN breakdown_status_id SET STORAGE PLAIN
;
ALTER TABLE core.breakdown ALTER COLUMN territory_id SET STORAGE PLAIN
;
ALTER TABLE core.breakdown ALTER COLUMN equipment_id SET STORAGE PLAIN
;
ALTER TABLE core.breakdown ALTER COLUMN account_id SET STORAGE PLAIN
;
ALTER TABLE core.breakdown ALTER COLUMN num SET STORAGE PLAIN
;

COMMENT ON TABLE core.breakdown IS 'Запись о поломке БО'
;
COMMENT ON COLUMN core.breakdown.breakdown_id IS 'Запись о поломке БО'
;
COMMENT ON COLUMN core.breakdown.dt_begin IS 'Дата создания'
;
COMMENT ON COLUMN core.breakdown.description IS 'Описание поломки БО'
;
COMMENT ON COLUMN core.breakdown.is_confirmed IS 'Поломка подтверждена'
;
COMMENT ON COLUMN core.breakdown.dt_end IS 'Дата закрытия'
;
COMMENT ON COLUMN core.breakdown.tr_id IS 'Транспортное средство'
;
COMMENT ON COLUMN core.breakdown.breakdown_type_id IS 'Вид поломки БО'
;
COMMENT ON COLUMN core.breakdown.breakdown_status_id IS 'Статус записи о поломке БО'
;
COMMENT ON COLUMN core.breakdown.close_result_id IS 'Результат закрытия'
;
COMMENT ON COLUMN core.breakdown.territory_id IS 'Территория ТП'
;
COMMENT ON COLUMN core.breakdown.equipment_id IS 'Поломанное оборудование'
;
COMMENT ON COLUMN core.breakdown.account_id IS 'Пользователь'
;
COMMENT ON COLUMN core.breakdown.num IS 'номер записи'
;

-- Create indexes for table core.breakdown

CREATE INDEX ix_breakdown_territory_id ON core.breakdown (territory_id)
TABLESPACE core_idx
;

CREATE INDEX ix_breakdown_tr_id ON core.breakdown (tr_id)
TABLESPACE core_idx
;

CREATE INDEX ix_breakdown_breakdown_type_id ON core.breakdown (breakdown_type_id)
TABLESPACE core_idx
;

CREATE INDEX ix_breakdown_breakdown_status_id ON core.breakdown (breakdown_status_id)
TABLESPACE core_idx
;

CREATE INDEX ix_breakdown_equipment_id ON core.breakdown (equipment_id)
TABLESPACE core_idx
;

CREATE INDEX ix_breakdown_account_id ON core.breakdown (account_id)
TABLESPACE core_idx
;

CREATE INDEX ix_breakdown_close_result_id ON core.breakdown (close_result_id)
;

-- Add keys for table core.breakdown

ALTER TABLE core.breakdown ADD CONSTRAINT pk_breakdown_breakdown_id PRIMARY KEY (breakdown_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.breakdown_status

CREATE TABLE core.breakdown_status(
  breakdown_status_id Bigint DEFAULT nextval('core.breakdown_status_breakdown_status_id_seq'::regclass) NOT NULL,
  name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE core.breakdown_status ALTER COLUMN breakdown_status_id SET STORAGE PLAIN
;
ALTER TABLE core.breakdown_status ALTER COLUMN name SET STORAGE EXTENDED
;

COMMENT ON TABLE core.breakdown_status IS 'Статус поломки БО'
;
COMMENT ON COLUMN core.breakdown_status.breakdown_status_id IS 'Идентификатор Статус записи о поломке БО'
;
COMMENT ON COLUMN core.breakdown_status.name IS 'Наименование'
;

-- Add keys for table core.breakdown_status

ALTER TABLE core.breakdown_status ADD CONSTRAINT fk_breakdown_status_breakdown_status_id PRIMARY KEY (breakdown_status_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.breakdown_type

CREATE TABLE core.breakdown_type(
  breakdown_type_id Bigint DEFAULT nextval('core.breakdown_type_breakdown_type_seq'::regclass) NOT NULL,
  name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE core.breakdown_type ALTER COLUMN breakdown_type_id SET STORAGE PLAIN
;
ALTER TABLE core.breakdown_type ALTER COLUMN name SET STORAGE EXTENDED
;

COMMENT ON TABLE core.breakdown_type IS 'Поломки БО'
;
COMMENT ON COLUMN core.breakdown_type.breakdown_type_id IS 'Поломка'
;
COMMENT ON COLUMN core.breakdown_type.name IS 'Поломка БО'
;

-- Add keys for table core.breakdown_type

ALTER TABLE core.breakdown_type ADD CONSTRAINT pk_breaking_type_breaking_type_id PRIMARY KEY (breakdown_type_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.repair_request

CREATE TABLE core.repair_request(
  repair_request_id Bigint DEFAULT nextval('core.repair_request_repair_request_id_seq'::regclass) NOT NULL,
  dt Timestamp NOT NULL,
  description Text,
  dt_closed Timestamp,
  repair_request_status_id Bigint NOT NULL,
  breakdown_type_id Bigint NOT NULL,
  close_result_id Bigint,
  territory_id Bigint,
  facility_id Bigint,
  breakdown_id Bigint,
  num Integer DEFAULT nextval('core.repair_request_num_seq'::regclass) NOT NULL,
  equipment_id Bigint,
  replacement_equipment_id Bigint
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE core.repair_request ALTER COLUMN repair_request_id SET STORAGE PLAIN
;
ALTER TABLE core.repair_request ALTER COLUMN dt SET STORAGE PLAIN
;
ALTER TABLE core.repair_request ALTER COLUMN description SET STORAGE EXTENDED
;
ALTER TABLE core.repair_request ALTER COLUMN dt_closed SET STORAGE PLAIN
;
ALTER TABLE core.repair_request ALTER COLUMN repair_request_status_id SET STORAGE PLAIN
;
ALTER TABLE core.repair_request ALTER COLUMN breakdown_type_id SET STORAGE PLAIN
;
ALTER TABLE core.repair_request ALTER COLUMN territory_id SET STORAGE PLAIN
;
ALTER TABLE core.repair_request ALTER COLUMN facility_id SET STORAGE PLAIN
;
ALTER TABLE core.repair_request ALTER COLUMN breakdown_id SET STORAGE PLAIN
;
ALTER TABLE core.repair_request ALTER COLUMN num SET STORAGE PLAIN
;
ALTER TABLE core.repair_request ALTER COLUMN equipment_id SET STORAGE PLAIN
;
ALTER TABLE core.repair_request ALTER COLUMN replacement_equipment_id SET STORAGE PLAIN
;

COMMENT ON TABLE core.repair_request IS 'Заявка на ремонт БО'
;
COMMENT ON COLUMN core.repair_request.repair_request_id IS 'Заявка на ремонт БО'
;
COMMENT ON COLUMN core.repair_request.dt IS 'Дата создания'
;
COMMENT ON COLUMN core.repair_request.description IS 'Описание поломки'
;
COMMENT ON COLUMN core.repair_request.dt_closed IS 'Дата закрытия'
;
COMMENT ON COLUMN core.repair_request.repair_request_status_id IS 'Статус заявки'
;
COMMENT ON COLUMN core.repair_request.breakdown_type_id IS 'Тип поломки'
;
COMMENT ON COLUMN core.repair_request.close_result_id IS 'Результат закрытия'
;
COMMENT ON COLUMN core.repair_request.territory_id IS 'Территория'
;
COMMENT ON COLUMN core.repair_request.facility_id IS 'Подрядчик'
;
COMMENT ON COLUMN core.repair_request.breakdown_id IS 'Запись о поломке'
;
COMMENT ON COLUMN core.repair_request.num IS 'Номер завки на ремонт'
;
COMMENT ON COLUMN core.repair_request.equipment_id IS 'Оборудование'
;
COMMENT ON COLUMN core.repair_request.replacement_equipment_id IS 'Оборудование на замену'
;

-- Create indexes for table core.repair_request

CREATE INDEX ix_repair_request_breakdown_id ON core.repair_request (breakdown_id)
TABLESPACE core_idx
;

CREATE INDEX ix_repair_request_teritory_id ON core.repair_request (territory_id)
TABLESPACE core_idx
;

CREATE INDEX ix_repair_request_facility_id ON core.repair_request (facility_id)
TABLESPACE core_idx
;

CREATE INDEX ix_repair_request_repare_request_status ON core.repair_request (repair_request_status_id)
TABLESPACE core_idx
;

CREATE INDEX ix_repair_request_breakdown_type ON core.repair_request (breakdown_type_id)
TABLESPACE core_idx
;

CREATE INDEX ix_repair_request_equipment_id ON core.repair_request (equipment_id)
TABLESPACE core_idx
;

CREATE INDEX ix_repair_request_replacemrnt_equipment_id ON core.repair_request (replacement_equipment_id)
TABLESPACE core_idx
;

CREATE INDEX ix_repair_request_close_result_id ON core.repair_request (close_result_id)
TABLESPACE core_idx
;

-- Add keys for table core.repair_request

ALTER TABLE core.repair_request ADD CONSTRAINT pk_repair_request_repair_request_id PRIMARY KEY (repair_request_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.repair_request_comment

CREATE TABLE core.repair_request_comment(
  repair_request_comment_id Bigint DEFAULT nextval('core.repair_request_comment_repair_request_comment_id_seq'::regclass) NOT NULL,
  dt Timestamp NOT NULL,
  comment Text NOT NULL,
  repair_request_id Bigint DEFAULT nextval('core.repair_request_repair_request_id_seq'::regclass) NOT NULL,
  account_id Bigint NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE core.repair_request_comment ALTER COLUMN repair_request_comment_id SET STORAGE PLAIN
;
ALTER TABLE core.repair_request_comment ALTER COLUMN dt SET STORAGE PLAIN
;
ALTER TABLE core.repair_request_comment ALTER COLUMN comment SET STORAGE EXTENDED
;
ALTER TABLE core.repair_request_comment ALTER COLUMN repair_request_id SET STORAGE PLAIN
;
ALTER TABLE core.repair_request_comment ALTER COLUMN account_id SET STORAGE PLAIN
;

COMMENT ON TABLE core.repair_request_comment IS 'Комментарий к заявке'
;
COMMENT ON COLUMN core.repair_request_comment.repair_request_comment_id IS 'Идентификатор'
;
COMMENT ON COLUMN core.repair_request_comment.dt IS 'Дата комментария'
;
COMMENT ON COLUMN core.repair_request_comment.comment IS 'Текст комментария'
;
COMMENT ON COLUMN core.repair_request_comment.repair_request_id IS 'Заявка на ремонт БО'
;
COMMENT ON COLUMN core.repair_request_comment.account_id IS 'Пользователь'
;

-- Create indexes for table core.repair_request_comment

CREATE INDEX ix_repair_request_comment_repair_request_id ON core.repair_request_comment (repair_request_id)
TABLESPACE core_idx
;

CREATE INDEX ix_repair_request_comment_account_id ON core.repair_request_comment (account_id)
TABLESPACE core_idx
;

-- Add keys for table core.repair_request_comment

ALTER TABLE core.repair_request_comment ADD CONSTRAINT pk_repair_request_comment_repair_request_comment_id PRIMARY KEY (repair_request_comment_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.repair_request_status

CREATE TABLE core.repair_request_status(
  repair_request_status_id Bigint DEFAULT nextval('core.repair_request_status_repair_request_status_id_seq'::regclass) NOT NULL,
  name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE core.repair_request_status ALTER COLUMN repair_request_status_id SET STORAGE PLAIN
;
ALTER TABLE core.repair_request_status ALTER COLUMN name SET STORAGE EXTENDED
;

COMMENT ON TABLE core.repair_request_status IS 'Статус заявки'
;
COMMENT ON COLUMN core.repair_request_status.repair_request_status_id IS 'Статус заявки'
;
COMMENT ON COLUMN core.repair_request_status.name IS 'Наименование'
;

-- Add keys for table core.repair_request_status

ALTER TABLE core.repair_request_status ADD CONSTRAINT pk_repair_request_status_repair_request_status_id PRIMARY KEY (repair_request_status_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.replaced_equipment

CREATE TABLE core.replaced_equipment(
  repair_request_id Bigint DEFAULT nextval('core.repair_request_repair_request_id_seq'::regclass) NOT NULL,
  equipment_id Bigint NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE core.replaced_equipment ALTER COLUMN repair_request_id SET STORAGE PLAIN
;
ALTER TABLE core.replaced_equipment ALTER COLUMN equipment_id SET STORAGE PLAIN
;

COMMENT ON TABLE core.replaced_equipment IS 'Замена БО в Заявке'
;
COMMENT ON COLUMN core.replaced_equipment.repair_request_id IS 'заявка'
;
COMMENT ON COLUMN core.replaced_equipment.equipment_id IS 'БО'
;

-- Add keys for table core.replaced_equipment

ALTER TABLE core.replaced_equipment ADD CONSTRAINT pk_replaced_equipment_equipment_id PRIMARY KEY (repair_request_id,equipment_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.equipment_decommission

CREATE TABLE core.equipment_decommission(
  equipment_decommission_id Serial NOT NULL,
  equipment_id Bigint NOT NULL,
  decommission_reason_id Smallint NOT NULL,
  decommission_descr Text,
  dt_end Timestamp NOT NULL,
  dt_create Timestamp DEFAULT now() NOT NULL,
  account_create_id Bigint NOT NULL,
  is_deleted Boolean DEFAULT false NOT NULL,
  dt_deleted Timestamp,
  account_delete_id Bigint
)
TABLESPACE core_data
;

COMMENT ON TABLE core.equipment_decommission IS 'Списанное оборудование'
;
COMMENT ON COLUMN core.equipment_decommission.equipment_decommission_id IS 'Списанное оборудование'
;
COMMENT ON COLUMN core.equipment_decommission.equipment_id IS 'Оборудование'
;
COMMENT ON COLUMN core.equipment_decommission.decommission_reason_id IS 'Причина списания'
;
COMMENT ON COLUMN core.equipment_decommission.decommission_descr IS 'Комментарий'
;
COMMENT ON COLUMN core.equipment_decommission.dt_end IS 'Дата списания'
;
COMMENT ON COLUMN core.equipment_decommission.dt_create IS 'Фактическая дата внесения записи о списании'
;
COMMENT ON COLUMN core.equipment_decommission.account_create_id IS 'Пользователь создавший запись'
;
COMMENT ON COLUMN core.equipment_decommission.is_deleted IS 'Отмена записи о списании'
;
COMMENT ON COLUMN core.equipment_decommission.dt_deleted IS 'Фактическая даты отмена списания'
;
COMMENT ON COLUMN core.equipment_decommission.account_delete_id IS 'Пользователь, отменивший списание'
;

-- Create indexes for table core.equipment_decommission

CREATE INDEX ix_equipment_decommission_equipment ON core.equipment_decommission (equipment_id)
  WHERE (NOT is_deleted)
;

-- Add keys for table core.equipment_decommission

ALTER TABLE core.equipment_decommission ADD CONSTRAINT pk_equipment_decommission PRIMARY KEY (equipment_decommission_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.sim_decommission

CREATE TABLE core.sim_decommission(
  sim_decommission_id Serial NOT NULL,
  sim_card_id Bigint NOT NULL,
  decommission_reason_id Smallint,
  decommission_descr Text,
  dt_end Timestamp NOT NULL,
  dt_create Timestamp DEFAULT now() NOT NULL,
  account_create_id Bigint NOT NULL,
  is_deleted Boolean DEFAULT false NOT NULL,
  account_delete_id Bigint,
  dt_deleted Timestamp
)
TABLESPACE core_data
;

COMMENT ON TABLE core.sim_decommission IS 'Списанное оборудование'
;
COMMENT ON COLUMN core.sim_decommission.sim_decommission_id IS 'Списанное оборудование'
;
COMMENT ON COLUMN core.sim_decommission.sim_card_id IS 'Sim-карта'
;
COMMENT ON COLUMN core.sim_decommission.decommission_reason_id IS 'Причина списания'
;
COMMENT ON COLUMN core.sim_decommission.decommission_descr IS 'Комментарий'
;
COMMENT ON COLUMN core.sim_decommission.dt_end IS 'Дата списания'
;
COMMENT ON COLUMN core.sim_decommission.dt_create IS 'Фактическая дата внесения записи о списании'
;
COMMENT ON COLUMN core.sim_decommission.account_create_id IS 'Пользователь создавший запись'
;
COMMENT ON COLUMN core.sim_decommission.is_deleted IS 'Отмена записи о списании'
;
COMMENT ON COLUMN core.sim_decommission.account_delete_id IS 'Пользователь, отменивший списание'
;
COMMENT ON COLUMN core.sim_decommission.dt_deleted IS 'Фактическая отмена записи о списании'
;

-- Create indexes for table core.sim_decommission

CREATE INDEX ix_sim_decommission_sim_card ON core.sim_decommission (sim_card_id)
TABLESPACE core_idx
;

-- Add keys for table core.sim_decommission

ALTER TABLE core.sim_decommission ADD CONSTRAINT pk_sim_decommission PRIMARY KEY (sim_decommission_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.tr_schema_install

CREATE TABLE core.tr_schema_install(
  tr_schema_install_id Serial NOT NULL,
  tr_type_id Smallint NOT NULL,
  tr_capacity_id Smallint NOT NULL,
  install_name Text NOT NULL,
  install_desc Text,
  dt_begin Timestamp DEFAULT now() NOT NULL,
  is_active Boolean DEFAULT true NOT NULL
)
TABLESPACE core_data
;
ALTER TABLE core.tr_schema_install ALTER COLUMN tr_schema_install_id SET STORAGE PLAIN
;
ALTER TABLE core.tr_schema_install ALTER COLUMN tr_type_id SET STORAGE PLAIN
;
ALTER TABLE core.tr_schema_install ALTER COLUMN tr_capacity_id SET STORAGE PLAIN
;
ALTER TABLE core.tr_schema_install ALTER COLUMN install_name SET STORAGE EXTENDED
;
ALTER TABLE core.tr_schema_install ALTER COLUMN install_desc SET STORAGE EXTENDED
;
ALTER TABLE core.tr_schema_install ALTER COLUMN dt_begin SET STORAGE PLAIN
;
ALTER TABLE core.tr_schema_install ALTER COLUMN is_active SET STORAGE PLAIN
;

COMMENT ON TABLE core.tr_schema_install IS 'Схема подключения БО к видам ТС'
;
COMMENT ON COLUMN core.tr_schema_install.tr_type_id IS 'Вид транспорта
'
;
COMMENT ON COLUMN core.tr_schema_install.tr_capacity_id IS 'Вместимость
'
;
COMMENT ON COLUMN core.tr_schema_install.install_name IS 'Наименование схемы подключения
'
;
COMMENT ON COLUMN core.tr_schema_install.install_desc IS 'Комментарий'
;
COMMENT ON COLUMN core.tr_schema_install.dt_begin IS 'Дата ввода в действие'
;
COMMENT ON COLUMN core.tr_schema_install.is_active IS 'Активная'
;

-- Add keys for table core.tr_schema_install

ALTER TABLE core.tr_schema_install ADD CONSTRAINT pk_tr_schema_install PRIMARY KEY (tr_schema_install_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.tr_schema_install_detail

CREATE TABLE core.tr_schema_install_detail(
  tr_schema_install_detail_id Serial NOT NULL,
  sensor2unit_model_id Integer DEFAULT nextval('core.sensor2unit_model_sensor2unit_model_id_seq'::regclass) NOT NULL,
  is_required Boolean DEFAULT true NOT NULL,
  install_desc Text,
  tr_schema_install_unit_id Integer
)
TABLESPACE core_data
;
ALTER TABLE core.tr_schema_install_detail ALTER COLUMN tr_schema_install_detail_id SET STORAGE PLAIN
;
ALTER TABLE core.tr_schema_install_detail ALTER COLUMN sensor2unit_model_id SET STORAGE PLAIN
;
ALTER TABLE core.tr_schema_install_detail ALTER COLUMN is_required SET STORAGE PLAIN
;
ALTER TABLE core.tr_schema_install_detail ALTER COLUMN install_desc SET STORAGE EXTENDED
;

COMMENT ON TABLE core.tr_schema_install_detail IS 'Детали подключения оборудования в схеме подключения к видам ТС'
;
COMMENT ON COLUMN core.tr_schema_install_detail.tr_schema_install_detail_id IS 'ID'
;
COMMENT ON COLUMN core.tr_schema_install_detail.sensor2unit_model_id IS 'Схема подключения БО к блокам'
;
COMMENT ON COLUMN core.tr_schema_install_detail.is_required IS 'обязательно для подключения'
;
COMMENT ON COLUMN core.tr_schema_install_detail.install_desc IS 'Комментарий'
;
COMMENT ON COLUMN core.tr_schema_install_detail.tr_schema_install_unit_id IS 'ББ в схеме подключения'
;

-- Create indexes for table core.tr_schema_install_detail

CREATE INDEX ix_tr_schema_install_detail_install_unit ON core.tr_schema_install_detail (tr_schema_install_unit_id)
TABLESPACE core_idx
;

-- Add keys for table core.tr_schema_install_detail

ALTER TABLE core.tr_schema_install_detail ADD CONSTRAINT pk_tr_schema_install_detail PRIMARY KEY (tr_schema_install_detail_id)
;

-- Table core.tr_schema_install_unit

CREATE TABLE core.tr_schema_install_unit(
  tr_schema_install_unit_id Serial NOT NULL,
  equipment_type_id Smallint NOT NULL,
  tr_schema_install_id Integer NOT NULL,
  is_main Boolean NOT NULL
)
TABLESPACE core_idx
;

COMMENT ON TABLE core.tr_schema_install_unit IS 'ББ в схеме подключения'
;
COMMENT ON COLUMN core.tr_schema_install_unit.tr_schema_install_unit_id IS 'ID'
;
COMMENT ON COLUMN core.tr_schema_install_unit.equipment_type_id IS 'ББ'
;
COMMENT ON COLUMN core.tr_schema_install_unit.tr_schema_install_id IS 'Схема подключения'
;
COMMENT ON COLUMN core.tr_schema_install_unit.is_main IS 'Признак основного блока'
;

-- Create indexes for table core.tr_schema_install_unit

CREATE INDEX ix_tr_schema_install_unit_eqtype ON core.tr_schema_install_unit (equipment_type_id)
TABLESPACE core_idx
;

CREATE INDEX ix_tr_schema_install_unit_shema_install ON core.tr_schema_install_unit (tr_schema_install_id)
TABLESPACE core_idx
;

-- Add keys for table core.tr_schema_install_unit

ALTER TABLE core.tr_schema_install_unit ADD CONSTRAINT pk_tr_schema_install_unit PRIMARY KEY (tr_schema_install_unit_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.sensor2unit_model

CREATE TABLE core.sensor2unit_model(
  sensor2unit_model_id Integer DEFAULT nextval('core.sensor2unit_model_sensor2unit_model_id_seq'::regclass) NOT NULL,
  num Smallint NOT NULL,
  equipment_type_sensor_id Smallint NOT NULL,
  equipment_type_unit_id Smallint NOT NULL,
  is_required Boolean DEFAULT true NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE core.sensor2unit_model ALTER COLUMN sensor2unit_model_id SET STORAGE PLAIN
;
ALTER TABLE core.sensor2unit_model ALTER COLUMN num SET STORAGE PLAIN
;
ALTER TABLE core.sensor2unit_model ALTER COLUMN equipment_type_sensor_id SET STORAGE PLAIN
;
ALTER TABLE core.sensor2unit_model ALTER COLUMN equipment_type_unit_id SET STORAGE PLAIN
;
ALTER TABLE core.sensor2unit_model ALTER COLUMN is_required SET STORAGE PLAIN
;

COMMENT ON TABLE core.sensor2unit_model IS 'Схема подключения датчиков'
;
COMMENT ON COLUMN core.sensor2unit_model.sensor2unit_model_id IS 'ID'
;
COMMENT ON COLUMN core.sensor2unit_model.num IS 'Номер входа'
;
COMMENT ON COLUMN core.sensor2unit_model.equipment_type_sensor_id IS 'Тип датчика'
;
COMMENT ON COLUMN core.sensor2unit_model.equipment_type_unit_id IS 'Тип блока'
;
COMMENT ON COLUMN core.sensor2unit_model.is_required IS 'Обязательно для подключения'
;

-- Create indexes for table core.sensor2unit_model

CREATE INDEX ix_sensor2equipment_type_id ON core.sensor2unit_model (equipment_type_sensor_id)
TABLESPACE core_idx
;

CREATE UNIQUE INDEX ux_equipment_type_unit_id_num ON core.sensor2unit_model (equipment_type_unit_id,num)
TABLESPACE core_idx
;

-- Add keys for table core.sensor2unit_model

ALTER TABLE core.sensor2unit_model ADD CONSTRAINT pk_sensor2unit_model_sensor2unit_model_id PRIMARY KEY (sensor2unit_model_id)
USING INDEX TABLESPACE core_idx
;

ALTER TABLE core.sensor2unit_model ADD CONSTRAINT uk_sensor2unit_model UNIQUE (equipment_type_unit_id,equipment_type_sensor_id,num)
USING INDEX TABLESPACE core_idx
;

-- Table core.asmpp

CREATE TABLE core.asmpp(
  sensor_id Bigint NOT NULL,
  doors_count Smallint NOT NULL
)
TABLESPACE core_data
;

COMMENT ON TABLE core.asmpp IS 'Датчик АСМПП дополнительные атрибуты'
;
COMMENT ON COLUMN core.asmpp.sensor_id IS 'Датчик'
;
COMMENT ON COLUMN core.asmpp.doors_count IS 'Количество обслуживаемых дверей'
;

-- Add keys for table core.asmpp

ALTER TABLE core.asmpp ADD CONSTRAINT pk_asmpp PRIMARY KEY (sensor_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.breakdown_diagnostic_protocol

CREATE TABLE core.breakdown_diagnostic_protocol(
  breakdown_diagnostic_protocol_id BigSerial NOT NULL,
  breakdown_id Bigint NOT NULL,
  diagnostic_protocol_id Bigint NOT NULL
)
TABLESPACE core_data
;

COMMENT ON TABLE core.breakdown_diagnostic_protocol IS 'Поломки, зафиксированные на основании протокола диагностики'
;
COMMENT ON COLUMN core.breakdown_diagnostic_protocol.breakdown_diagnostic_protocol_id IS 'Код записи Поломки, зафиксированной на основании протокола диагностики'
;
COMMENT ON COLUMN core.breakdown_diagnostic_protocol.breakdown_id IS 'Код поломки'
;
COMMENT ON COLUMN core.breakdown_diagnostic_protocol.diagnostic_protocol_id IS 'Код записи диагностики'
;

-- Create indexes for table core.breakdown_diagnostic_protocol

CREATE UNIQUE INDEX ix_bdp_breakdown ON core.breakdown_diagnostic_protocol (breakdown_id)
TABLESPACE core_idx
;

CREATE UNIQUE INDEX ix_bdp_diagnostic_protocol ON core.breakdown_diagnostic_protocol (diagnostic_protocol_id)
TABLESPACE core_idx
;

-- Add keys for table core.breakdown_diagnostic_protocol

ALTER TABLE core.breakdown_diagnostic_protocol ADD CONSTRAINT pk_breakdown_diagnostic_protocol PRIMARY KEY (breakdown_diagnostic_protocol_id)
;

-- Table core.auxiliary

CREATE TABLE core.auxiliary(
  auxiliary_id Smallserial NOT NULL,
  auxiliary_name Text NOT NULL
)
TABLESPACE core_data
;

COMMENT ON TABLE core.auxiliary IS 'Дополнительные устройства'
;
COMMENT ON COLUMN core.auxiliary.auxiliary_id IS 'Дополнительное устройство'
;
COMMENT ON COLUMN core.auxiliary.auxiliary_name IS 'Наименование'
;

-- Add keys for table core.auxiliary

ALTER TABLE core.auxiliary ADD CONSTRAINT pk_auxiliary PRIMARY KEY (auxiliary_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.equipment_aux

CREATE TABLE core.equipment_aux(
  equipment_id Bigint NOT NULL,
  auxiliary_id Smallint NOT NULL
)
TABLESPACE core_data
;

COMMENT ON TABLE core.equipment_aux IS 'Дополнительные устройство, установленные на оборудовании '
;
COMMENT ON COLUMN core.equipment_aux.equipment_id IS 'Оборудование'
;
COMMENT ON COLUMN core.equipment_aux.auxiliary_id IS 'Дополнительное устройство'
;

-- Add keys for table core.equipment_aux

ALTER TABLE core.equipment_aux ADD CONSTRAINT pk_equipment_aux PRIMARY KEY (equipment_id,auxiliary_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.bd_close_resultdelete

CREATE TABLE core.bd_close_resultdelete(
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE core.bd_close_resultdelete IS 'Результат закрытия строки журнала регистрации неисправностей БО'
;

-- Table core.bd_close_result

CREATE TABLE core.bd_close_result(
  bd_close_result_id BigSerial NOT NULL,
  name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE core.bd_close_result ALTER COLUMN bd_close_result_id SET STORAGE PLAIN
;

COMMENT ON TABLE core.bd_close_result IS 'Результат закрытия строки журнала регистрации неисправностей БО'
;
COMMENT ON COLUMN core.bd_close_result.bd_close_result_id IS 'ID'
;
COMMENT ON COLUMN core.bd_close_result.name IS 'Наименование'
;

-- Add keys for table core.bd_close_result

ALTER TABLE core.bd_close_result ADD CONSTRAINT pk_bd_close_result_bd_close_result_id PRIMARY KEY (bd_close_result_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.rr_close_result

CREATE TABLE core.rr_close_result(
  rr_close_result_id BigSerial NOT NULL,
  name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE core.rr_close_result ALTER COLUMN rr_close_result_id SET STORAGE PLAIN
;
ALTER TABLE core.rr_close_result ALTER COLUMN name SET STORAGE EXTENDED
;

COMMENT ON TABLE core.rr_close_result IS 'Результат закрытия заявки на ремонт'
;
COMMENT ON COLUMN core.rr_close_result.rr_close_result_id IS 'ID'
;
COMMENT ON COLUMN core.rr_close_result.name IS 'Наименование'
;

-- Add keys for table core.rr_close_result

ALTER TABLE core.rr_close_result ADD CONSTRAINT pk_rr_close_result PRIMARY KEY (rr_close_result_id)
USING INDEX TABLESPACE core_idx
;

-- Table core.event_type_binding

CREATE TABLE core.event_type_binding(
  event_type_binding_id Serial NOT NULL,
  event_type Text NOT NULL,
  procedure_name Text NOT NULL
)
TABLESPACE core_data
;

COMMENT ON TABLE core.event_type_binding IS 'Таблица для определения вызываемой процедуры по типу события'
;
COMMENT ON COLUMN core.event_type_binding.event_type_binding_id IS 'ID'
;
COMMENT ON COLUMN core.event_type_binding.event_type IS 'Тип события'
;
COMMENT ON COLUMN core.event_type_binding.procedure_name IS 'Вызываемая процедура'
;

-- Create indexes for table core.event_type_binding

CREATE UNIQUE INDEX uq_event_type_binding_event_type ON core.event_type_binding (event_type)
TABLESPACE core_idx
;

-- Add keys for table core.event_type_binding

ALTER TABLE core.event_type_binding ADD CONSTRAINT pk_event_type_binding PRIMARY KEY (event_type_binding_id)
USING INDEX TABLESPACE core_idx
;

-- Create relationships section -------------------------------------------------

ALTER TABLE core.tr ADD CONSTRAINT fk_tr_tr_status FOREIGN KEY (tr_status_id) REFERENCES core.tr_status (tr_status_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.tr ADD CONSTRAINT fk_tr_tr_model FOREIGN KEY (tr_model_id) REFERENCES core.tr_model (tr_model_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.tr ADD CONSTRAINT fk_tr_territory FOREIGN KEY (territory_id) REFERENCES core.territory (territory_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.tr ADD CONSTRAINT fk_tr_tr_type FOREIGN KEY (tr_type_id) REFERENCES core.tr_type (tr_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.tr_mark ADD CONSTRAINT fk_tr_mark_facility FOREIGN KEY (facility_id) REFERENCES core.facility (facility_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.tr_model ADD CONSTRAINT fk_tr_model_tr_mark_id FOREIGN KEY (tr_mark_id) REFERENCES core.tr_mark (tr_mark_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.department ADD CONSTRAINT fk_department_parent_to_child FOREIGN KEY (parent_id) REFERENCES core.department (department_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.department ADD CONSTRAINT fk_entity_to_department FOREIGN KEY (department_id) REFERENCES core.entity (entity_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.unit2tr ADD CONSTRAINT fk_unit2tr_unit FOREIGN KEY (unit_id) REFERENCES core.unit (unit_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.unit2tr ADD CONSTRAINT fk_unit2tr_tr FOREIGN KEY (tr_id) REFERENCES core.tr (tr_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.tr_model ADD CONSTRAINT fk_tr_model_capacity FOREIGN KEY (tr_capacity_id) REFERENCES core.tr_capacity (tr_capacity_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.tr_capacity2gis ADD CONSTRAINT fk_tr_capacity2gis_tr_capacity FOREIGN KEY (tr_capacity_id) REFERENCES core.tr_capacity (tr_capacity_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.depo ADD CONSTRAINT fk_depo_carrier FOREIGN KEY (carrier_id) REFERENCES core.carrier (carrier_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.carrier ADD CONSTRAINT fk_carrier_entity FOREIGN KEY (carrier_id) REFERENCES core.entity (entity_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.depo ADD CONSTRAINT fk_depo_entity FOREIGN KEY (depo_id) REFERENCES core.entity (entity_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.territory ADD CONSTRAINT fk_territory_entity FOREIGN KEY (territory_id) REFERENCES core.entity (entity_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.tr ADD CONSTRAINT fk_tr_depo FOREIGN KEY (depo_id) REFERENCES core.depo (depo_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.depo2territory ADD CONSTRAINT fk_depo2teritory_territory FOREIGN KEY (territory_id) REFERENCES core.territory (territory_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.tr_last_state ADD CONSTRAINT fk_tr_last_state_tr FOREIGN KEY (tr_id) REFERENCES core.tr (tr_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.contact ADD CONSTRAINT fk_contact_contact_type FOREIGN KEY (contact_type_id) REFERENCES core.contact_type (contact_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.address ADD CONSTRAINT fk_address_entity FOREIGN KEY (entity_id) REFERENCES core.entity (entity_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.contact ADD CONSTRAINT fk_contact_entity FOREIGN KEY (entity_id) REFERENCES core.entity (entity_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.carrier ADD CONSTRAINT fk_carrier_org_legal_form FOREIGN KEY (org_legal_form_id) REFERENCES core.org_legal_form (org_legal_form_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment_model ADD CONSTRAINT fk_equipment_model_equipment_type FOREIGN KEY (equipment_type_id) REFERENCES core.equipment_type (equipment_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment_model ADD CONSTRAINT fk_equipment_model_facility FOREIGN KEY (facility_id) REFERENCES core.facility (facility_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment_type ADD CONSTRAINT fk_equipment_type2equipment_class_id_id FOREIGN KEY (equipment_class_id) REFERENCES core.equipment_class (equipment_class_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.firmware ADD CONSTRAINT fk_firmware_facility FOREIGN KEY (facility_id) REFERENCES core.facility (facility_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.firmware ADD CONSTRAINT fk_equipment_model_to_firmware FOREIGN KEY (equipment_model_id) REFERENCES core.equipment_model (equipment_model_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment ADD CONSTRAINT fk_equipment_unit_service_type FOREIGN KEY (unit_service_type_id) REFERENCES core.unit_service_type (unit_service_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment ADD CONSTRAINT fk_equipment_equipment_status FOREIGN KEY (equipment_status_id) REFERENCES core.equipment_status (equipment_status_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment ADD CONSTRAINT fk_equipment_firmware FOREIGN KEY (firmware_id) REFERENCES core.firmware (firmware_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment ADD CONSTRAINT fk_equipment_facility FOREIGN KEY (facility_id) REFERENCES core.facility (facility_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment ADD CONSTRAINT fk_equipment_equipment_model_id FOREIGN KEY (equipment_model_id) REFERENCES core.equipment_model (equipment_model_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment ADD CONSTRAINT fk_equipment_depo FOREIGN KEY (depo_id) REFERENCES core.depo (depo_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.sensor ADD CONSTRAINT fk_sensor_equipment FOREIGN KEY (sensor_id) REFERENCES core.equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.sensor2unit ADD CONSTRAINT fk_sensor2unit_unit FOREIGN KEY (unit_id) REFERENCES core.unit (unit_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.sensor2unit ADD CONSTRAINT fk_sensor2unit_sensor FOREIGN KEY (sensor_id) REFERENCES core.sensor (sensor_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.account ADD CONSTRAINT fk_account_facility FOREIGN KEY (facility_id) REFERENCES core.facility (facility_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.account ADD CONSTRAINT fk_account_depo FOREIGN KEY (depo_id) REFERENCES core.depo (depo_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.facility ADD CONSTRAINT fk_facility_entity FOREIGN KEY (facility_id) REFERENCES core.entity (entity_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.unit ADD CONSTRAINT fk_unit_equipment FOREIGN KEY (unit_id) REFERENCES core.equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.sim_card2unit ADD CONSTRAINT fk_sim_card2unit_sim_card FOREIGN KEY (sim_card_id) REFERENCES core.sim_card (sim_card_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.sim_card2unit ADD CONSTRAINT fk_sim_card2unit_unit FOREIGN KEY (equipment_id) REFERENCES core.unit (unit_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.unit_bnsr ADD CONSTRAINT fk_unit_bnsr_unit FOREIGN KEY (unit_bnsr_id) REFERENCES core.unit (unit_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.mobile_tariff ADD CONSTRAINT fk_mobile_tariff_facility FOREIGN KEY (facility_id) REFERENCES core.facility (facility_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.sim_card ADD CONSTRAINT fk_sim_card_sim_card_group FOREIGN KEY (sim_card_group_id) REFERENCES core.sim_card_group (sim_card_group_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.sim_card ADD CONSTRAINT fk_sim_card_mobile_tariff FOREIGN KEY (mobile_tariff_id) REFERENCES core.mobile_tariff (mobile_tariff_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.sim_card ADD CONSTRAINT fk_sim_card_equipment_status FOREIGN KEY (equipment_status_id) REFERENCES core.equipment_status (equipment_status_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.depo ADD CONSTRAINT fk_depo_tr_type FOREIGN KEY (tr_type_id) REFERENCES core.tr_type (tr_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.facility2type ADD CONSTRAINT fk_facility2type_facility_id FOREIGN KEY (facility_id) REFERENCES core.facility (facility_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.facility2type ADD CONSTRAINT fk_facility2type_facility_type_id FOREIGN KEY (facility_type_id) REFERENCES core.facility_type (facility_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.modem ADD CONSTRAINT fk_sim_card_modem FOREIGN KEY (sim_card_id) REFERENCES core.sim_card (sim_card_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.modem ADD CONSTRAINT fk_modem_equipment FOREIGN KEY (modem_id) REFERENCES core.equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.unit_bnst ADD CONSTRAINT fk_unit_bnst_unit FOREIGN KEY (unit_bnst_id) REFERENCES core.unit (unit_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.unit_kbtob ADD CONSTRAINT fk_kbtob_unit_unit FOREIGN KEY (unit_kbtob_id) REFERENCES core.unit (unit_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.sim_card2unit_h ADD CONSTRAINT fk_sim_card_h2unit_unit FOREIGN KEY (equipment_id) REFERENCES core.unit (unit_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.sim_card2unit_h ADD CONSTRAINT fk_sim_card_h_sim_card FOREIGN KEY (sim_card_id) REFERENCES core.sim_card (sim_card_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.modem2unit ADD CONSTRAINT fk_modem2unit_unit FOREIGN KEY (unit_id) REFERENCES core.unit (unit_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.modem2unit ADD CONSTRAINT fk_modem2unit_modem FOREIGN KEY (modem_id) REFERENCES core.modem (modem_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.monitor ADD CONSTRAINT fk_monitor_equipment FOREIGN KEY (monitor_id) REFERENCES core.equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.monitor2unit ADD CONSTRAINT fk_monitor2unit_unit FOREIGN KEY (unit_id) REFERENCES core.unit (unit_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.monitor2unit ADD CONSTRAINT fk_monitor2unit_monitor FOREIGN KEY (monitor_id) REFERENCES core.monitor (monitor_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.hub ADD CONSTRAINT pk_hub_protocol FOREIGN KEY (protocol_id) REFERENCES core.protocol (protocol_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.unit ADD CONSTRAINT fk_unit_hub FOREIGN KEY (hub_id) REFERENCES core.hub (hub_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.hdd ADD CONSTRAINT fk_equipment_hdd FOREIGN KEY (hdd_id) REFERENCES core.equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.hdd2unit ADD CONSTRAINT fk_hdd2unit_unit FOREIGN KEY (unit_id) REFERENCES core.unit (unit_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.hdd2unit ADD CONSTRAINT fk_hdd2unit_hdd FOREIGN KEY (hdd_id) REFERENCES core.hdd (hdd_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.sim_card2unit_h ADD CONSTRAINT fk_sim_card2unit_unit_remove_reason FOREIGN KEY (unit_remove_reason) REFERENCES core.equipment_remove_reason (equipment_remove_reason_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment2tr_h ADD CONSTRAINT fk_equipment2tr_h_unit_remove_reason FOREIGN KEY (equipment_remove_reason_id) REFERENCES core.equipment_remove_reason (equipment_remove_reason_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment2tr_h ADD CONSTRAINT fk_equipment2tr_h_tr FOREIGN KEY (tr_id) REFERENCES core.tr (tr_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment2tr_h ADD CONSTRAINT fk_equipment2tr_h_equipment_id FOREIGN KEY (equipment_id) REFERENCES core.equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment2tr_h ADD CONSTRAINT fk_equipment2tr_h_instalation FOREIGN KEY (installation_site_id) REFERENCES core.installation_site (installation_site_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment2tr ADD CONSTRAINT fk_equipment2tr_tr FOREIGN KEY (tr_id) REFERENCES core.tr (tr_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment2tr ADD CONSTRAINT fk_equipment2equipment FOREIGN KEY (equipment_id) REFERENCES core.equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment ADD CONSTRAINT fk_equipment_territory FOREIGN KEY (territory_id) REFERENCES core.territory (territory_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment ADD CONSTRAINT fk_equipment_unit_service_facility FOREIGN KEY (unit_service_facility_id) REFERENCES core.facility (facility_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.sensor2tr ADD CONSTRAINT fk_installation_site_sensor2tr FOREIGN KEY (installation_site_id) REFERENCES core.installation_site (installation_site_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.sensor2tr ADD CONSTRAINT fk_sensor2tr_equipment FOREIGN KEY (sensor_id) REFERENCES core.equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.sensor2tr ADD CONSTRAINT fk_sensor2tr_tr FOREIGN KEY (tr_id) REFERENCES core.tr (tr_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.breakdown ADD CONSTRAINT fk_breakdown_territory FOREIGN KEY (territory_id) REFERENCES core.territory (territory_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.breakdown ADD CONSTRAINT fk_breakdown_tr FOREIGN KEY (tr_id) REFERENCES core.tr (tr_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.breakdown ADD CONSTRAINT fk_account_breakdown FOREIGN KEY (account_id) REFERENCES core.account (account_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.breakdown ADD CONSTRAINT fk_breakdown_equipment FOREIGN KEY (equipment_id) REFERENCES core.equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.breakdown ADD CONSTRAINT fk_breakdown_breakdown_status FOREIGN KEY (breakdown_status_id) REFERENCES core.breakdown_status (breakdown_status_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.breakdown ADD CONSTRAINT fk_breakdown_breakdown_type FOREIGN KEY (breakdown_type_id) REFERENCES core.breakdown_type (breakdown_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.repair_request ADD CONSTRAINT fk_repair_request_facility FOREIGN KEY (facility_id) REFERENCES core.facility (facility_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.repair_request ADD CONSTRAINT fk_repair_request_breakdown_type FOREIGN KEY (breakdown_type_id) REFERENCES core.breakdown_type (breakdown_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.repair_request ADD CONSTRAINT fk_repair_request_territory FOREIGN KEY (territory_id) REFERENCES core.territory (territory_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.repair_request ADD CONSTRAINT fk_repair_request_breakdown FOREIGN KEY (breakdown_id) REFERENCES core.breakdown (breakdown_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.repair_request ADD CONSTRAINT fk_repare_request_repair_request_status FOREIGN KEY (repair_request_status_id) REFERENCES core.repair_request_status (repair_request_status_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.repair_request ADD CONSTRAINT fk_repair_request_equipment FOREIGN KEY (equipment_id) REFERENCES core.equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.repair_request ADD CONSTRAINT repair_request_replacement_equipment_id_fkey FOREIGN KEY (replacement_equipment_id) REFERENCES core.equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.repair_request_comment ADD CONSTRAINT fk_repair_request_comment_account FOREIGN KEY (account_id) REFERENCES core.account (account_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.repair_request_comment ADD CONSTRAINT fk_repair_request_comment_repair_request FOREIGN KEY (repair_request_id) REFERENCES core.repair_request (repair_request_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.replaced_equipment ADD CONSTRAINT fk_repair_request_replaced_equipment FOREIGN KEY (repair_request_id) REFERENCES core.repair_request (repair_request_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.replaced_equipment ADD CONSTRAINT fk_equipment_replaced_equipment_ FOREIGN KEY (equipment_id) REFERENCES core.equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment_decommission ADD CONSTRAINT fk_equipment_decommission_equipment FOREIGN KEY (equipment_id) REFERENCES core.equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment_decommission ADD CONSTRAINT fk_equipment_decommission_ed_reason FOREIGN KEY (decommission_reason_id) REFERENCES core.decommission_reason (decommission_reason_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment_decommission ADD CONSTRAINT fk_equipment_decommission_account_create FOREIGN KEY (account_create_id) REFERENCES core.account (account_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment_decommission ADD CONSTRAINT fk_equipment_decommission_account_delete FOREIGN KEY (account_delete_id) REFERENCES core.account (account_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.sim_card ADD CONSTRAINT fk_sim_card_depo FOREIGN KEY (depo_id) REFERENCES core.depo (depo_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.sim_card ADD CONSTRAINT fk_sim_card_territory FOREIGN KEY (territory_id) REFERENCES core.territory (territory_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.sim_decommission ADD CONSTRAINT fk_sim_decommission_sim_card FOREIGN KEY (sim_card_id) REFERENCES core.sim_card (sim_card_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.sim_decommission ADD CONSTRAINT fk_sim_decommission_decom_reason FOREIGN KEY (decommission_reason_id) REFERENCES core.decommission_reason (decommission_reason_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.sim_decommission ADD CONSTRAINT fk_sim_decommission_account_create FOREIGN KEY (account_create_id) REFERENCES core.account (account_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.sim_decommission ADD CONSTRAINT fk_sim_decommission_account_delete FOREIGN KEY (account_delete_id) REFERENCES core.account (account_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.tr_schema_install ADD CONSTRAINT fk_tr_schema_install_tr_capacity FOREIGN KEY (tr_capacity_id) REFERENCES core.tr_capacity (tr_capacity_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.tr_schema_install ADD CONSTRAINT tr_schema_install_tr_type FOREIGN KEY (tr_type_id) REFERENCES core.tr_type (tr_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.tr_schema_install_unit ADD CONSTRAINT fk_tr_schema_install_unit_eqtype FOREIGN KEY (equipment_type_id) REFERENCES core.equipment_type (equipment_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.tr_schema_install_detail ADD CONSTRAINT fk_tr_schema_install_detail_install_unit FOREIGN KEY (tr_schema_install_unit_id) REFERENCES core.tr_schema_install_unit (tr_schema_install_unit_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.tr_schema_install_unit ADD CONSTRAINT fk_tr_schema_install_unit_schema_install FOREIGN KEY (tr_schema_install_id) REFERENCES core.tr_schema_install (tr_schema_install_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.tr ADD CONSTRAINT fk_tr_tr_schema_install FOREIGN KEY (tr_schema_install_id) REFERENCES core.tr_schema_install (tr_schema_install_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.sensor2unit_model ADD CONSTRAINT fk_sensor2unit_model_equipment_type_sensor FOREIGN KEY (equipment_type_unit_id) REFERENCES core.equipment_type (equipment_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.sensor2unit_model ADD CONSTRAINT fk_sensor2unit_model_equipment_type_unit FOREIGN KEY (equipment_type_sensor_id) REFERENCES core.equipment_type (equipment_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.tr_schema_install_detail ADD CONSTRAINT fk_tr_schema_install_detail_sensor2unit_model FOREIGN KEY (sensor2unit_model_id) REFERENCES core.sensor2unit_model (sensor2unit_model_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.asmpp ADD CONSTRAINT fk_asmpp_sensor FOREIGN KEY (sensor_id) REFERENCES core.sensor (sensor_id) ON DELETE CASCADE ON UPDATE NO ACTION
;

ALTER TABLE core.breakdown_diagnostic_protocol ADD CONSTRAINT fk_bdp_breakdown FOREIGN KEY (breakdown_id) REFERENCES core.breakdown (breakdown_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment_aux ADD CONSTRAINT fk_equipment_aux_equipment FOREIGN KEY (equipment_id) REFERENCES core.equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment_aux ADD CONSTRAINT fk_equipment_aux_aux FOREIGN KEY (auxiliary_id) REFERENCES core.auxiliary (auxiliary_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.breakdown ADD CONSTRAINT fk_breakdown_close_result FOREIGN KEY (close_result_id) REFERENCES core.bd_close_result (bd_close_result_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.repair_request ADD CONSTRAINT fk_repair_request_rr_close_result FOREIGN KEY (close_result_id) REFERENCES core.rr_close_result (rr_close_result_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.depo ADD CONSTRAINT Relationship107 FOREIGN KEY (depo_id) REFERENCES core.entity (entity_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.equipment ADD CONSTRAINT Relationship111 FOREIGN KEY (depo_id) REFERENCES core.depo (depo_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.tr ADD CONSTRAINT Relationship112 FOREIGN KEY (depo_id) REFERENCES core.depo (depo_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.depo2territory ADD CONSTRAINT fk_depo2teritory_depo FOREIGN KEY (depo_id) REFERENCES core.depo (depo_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE core.driver ADD CONSTRAINT Relationship180 FOREIGN KEY (depo_id) REFERENCES core.depo (depo_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;


ALTER SEQUENCE core.sensor2unit_model_sensor2unit_model_id_seq OWNED BY core.sensor2unit_model.sensor2unit_model_id
;
ALTER SEQUENCE core.sim_card2unit_h_sim_card2unit_h_id_seq OWNED BY core.sim_card2unit_h.sim_card2unit_h_id
;
ALTER SEQUENCE core.unit_bnst_has_manipulator_seq OWNED BY core.unit_bnst.has_manipulator
;
ALTER SEQUENCE core.equipment2tr_h_equipment2tr_h_id_seq OWNED BY core.equipment2tr_h.equipment2tr_h_id
;
ALTER SEQUENCE core.unit_remove_reason_unit_remove_reason_id_seq OWNED BY core.equipment_remove_reason.equipment_remove_reason_id
;
ALTER SEQUENCE core.breakdown_breakdown_id_seq OWNED BY core.breakdown.breakdown_id
;
ALTER SEQUENCE core.breakdown_num_new_seq OWNED BY core.breakdown.num
;
ALTER SEQUENCE core.breakdown_status_breakdown_status_id_seq OWNED BY core.breakdown_status.breakdown_status_id
;
ALTER SEQUENCE core.breakdown_type_breakdown_type_seq OWNED BY core.breakdown_type.breakdown_type_id
;
ALTER SEQUENCE core.repair_request_comment_repair_request_comment_id_seq OWNED BY core.repair_request_comment.repair_request_comment_id
;
ALTER SEQUENCE core.repair_request_num_seq OWNED BY core.repair_request.num
;
ALTER SEQUENCE core.repair_request_repair_request_id_seq OWNED BY core.repair_request.repair_request_id
;
ALTER SEQUENCE core.repair_request_status_repair_request_status_id_seq OWNED BY core.repair_request_status.repair_request_status_id
;
ALTER SEQUENCE core.tr_schema_install_detail_tr_schema_install_detail_id_seq OWNED BY core.tr_schema_install_detail.tr_schema_install_detail_id
;
ALTER SEQUENCE core.tr_schema_install_tr_schema_install_id_seq OWNED BY core.tr_schema_install.tr_schema_install_id
;

-- Grant permissions section -------------------------------------------------


