CREATE OR REPLACE FUNCTION core.jf_unit2tr_pkg$attr_to_rowtype(p_attr text)
  RETURNS core.unit2tr
AS
  $BODY$
  declare
   l_r core.unit2tr%rowtype;
begin
   l_r.tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
   l_r.unit_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
   return l_r;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_unit2tr_pkg$of_rows(in p_id_account numeric, out p_rows refcursor, in p_attr text)
  RETURNS refcursor
AS
  $BODY$
declare
    l_rf_db_method text;
    l_tr_id core.tr.tr_id%type;
begin
    l_tr_id := coalesce (jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true), jofl.jofl_pkg$extract_number(p_attr, 'f_tr_id', true));
    l_rf_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);


    open p_rows for
    select
      et.equipment_id,
      u.unit_num,
      et.serial_num,
      u.hub_id,
      hub.name       as hub_name,
      et.firmware_id,
      et.firmware_name,
      et.equipment_status_id,
      et.equipment_status_name,
      et.dt_begin,
      et.equipment_model_id,
      et.equipment_model_name,
      unit2tr.tr_id tr_id,
      et.equipment_type_id unit_equipment_type_id,
      vut.name as unit_type_name,
      vut.row$policy as "ROW$POLICY"
    from core.unit u
      join core.unit2tr on u.unit_id = unit2tr.unit_id
      join core.v_equipment et on et.equipment_id = u.unit_id
      left join core.v_unit_type vut  on vut.unit_id = u.unit_id
      left join core.hub hub on hub.hub_id = u.hub_id
    where (l_tr_id is null or unit2tr.tr_id = l_tr_id)
        ;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_unit2tr_pkg$of_delete(p_id_account numeric, p_attr text)
  RETURNS text
AS
  $BODY$
  declare
    l_r core.unit2tr%rowtype;
begin
   l_r := core.jf_unit2tr_pkg$attr_to_rowtype(p_attr);
   DELETE from core.unit2tr where tr_id = l_r.tr_id and unit_id = l_r.unit_id;
   return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;



CREATE OR REPLACE FUNCTION core.jf_unit2tr_pkg$of_insert(p_id_account numeric, p_attr text)
  RETURNS text
AS
  $BODY$
  declare
    l_r core.unit2tr%rowtype;
    l_r_h core.equipment2tr_h%rowtype;
    l_res text;
begin
   l_r := core.jf_unit2tr_pkg$attr_to_rowtype(p_attr);
   l_r.sys_period := tstzrange(now(), NULL::timestamp with time zone);

   INSERT INTO core.unit2tr SELECT l_r.*;

    l_r_h.sys_period := tsrange(localtimestamp, null);
    l_r_h.equipment_id := l_r.unit_id;
    l_r_h.tr_id:= l_r.tr_id;
    l_res:= core.jf_equipment2tr_h_pkg$of_insert(p_id_account, row_to_json(l_r_h)::text);
   return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;
--********************************************************************************************************

CREATE OR REPLACE FUNCTION core."jf_unit2tr_pkg$of_remove_from_tr"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r core.equipment2tr%rowtype;
  l_r_h core.equipment2tr_h%rowtype;
  l_r_equipment core.equipment%rowtype;
  l_dt_remove timestamp without time zone := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'f_remove_DT', true),localtimestamp);
  l_equipment_status_id core.equipment_model.equipment_model_id%type		:= jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_status_id', true);
  l_equipment_remove_reason_id core.equipment_model.equipment_model_id%type		:= jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_remove_reason_id', true);
  l_res text;
begin
  --сохранить в истории
  l_r := core.jf_equipment2tr_pkg$attr_to_rowtype(p_attr);


  select into l_r_h.equipment2tr_h_id  h.equipment2tr_h_id
  FROM core.equipment2tr_h h
  where h.equipment_id = l_r.equipment_id
        and h.tr_id = l_r.tr_id
        and upper(h.sys_period) is null;
  --RAISE EXCEPTION '%',row_to_json(l_r_h)::text;

  if l_r_h.equipment2tr_h_id is not null then
    l_r_h.sys_period := tsrange(lower(l_r_h.sys_period),l_dt_remove);
    l_r_h.equipment_remove_reason_id:=  l_equipment_remove_reason_id;

    l_res:= core.jf_equipment2tr_h_pkg$of_update(p_id_account, row_to_json(l_r_h)::text);
  end if;

  --обновляем статус оборудования
  update core.equipment set
    equipment_status_id = l_equipment_status_id
  where
    equipment_id = l_r.equipment_id;


  --удаляем связь с ТС
  l_res:= core.jf_unit2tr_pkg$of_delete(p_id_account, p_attr);

  --удаляем связи с БО
  delete from core.sensor2unit where unit_id= l_r.equipment_id;
  delete from core.modem2unit where unit_id= l_r.equipment_id;
  delete from core.monitor2unit where unit_id= l_r.equipment_id;
  delete from core.hdd2unit where unit_id= l_r.equipment_id;
  return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

-----------------------------------------------
CREATE OR REPLACE FUNCTION core.jf_unit2tr_pkg$of_breakdown(
  p_id_account NUMERIC,
  p_attr       TEXT)
  RETURNS TEXT AS
$BODY$
DECLARE
  l_b           core.breakdown%ROWTYPE;
  l_res         TEXT;
BEGIN
  l_b.equipment_id := ((p_attr)::json->> 'equipment_id')::BIGINT;
  l_b.tr_id := ((p_attr)::json->> 'tr_id')::BIGINT;
  l_b.breakdown_id := -1;
  l_b.dt_begin := now();
  l_b.description := (p_attr)::json->> 'DESCR_TEXT';
  l_b.breakdown_type_id := ((p_attr)::json->> 'F_breakdown_type_id')::BIGINT;
  l_b.territory_id := ((p_attr)::json->> 'territory_id')::BIGINT;

  l_res := core.jf_breakdown_pkg$of_insert(p_id_account, (row_to_json(l_b))::VARCHAR);

  RETURN null;
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

ALTER FUNCTION core."jf_unit2tr_pkg$of_remove_from_tr"(numeric, text)
OWNER TO adv;

-----------------------------------------------

create or replace function core.jf_unit2tr_pkg$of_change_unit(
  p_id_account numeric,
  p_attr       text)
  returns text as
$body$
declare
  l_old_equipment_id core.equipment.equipment_id%type		:= jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
  l_new_equipment_id core.equipment.equipment_id%type		:= jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_id', true);
  l_tr_id core.tr.tr_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
  l_res text;
begin

  l_res:= core.jf_unit2tr_pkg$of_insert(p_id_account,  '{"equipment_id":'||l_new_equipment_id::text||',"tr_id":'||l_tr_id::text||'}');


  insert into  core.sensor2unit(unit_id, sensor_id, sensor_num)
  select l_new_equipment_id, sensor_id, sensor_num  
  from core.sensor2unit
  where unit_id =   l_old_equipment_id;


  insert into  core.monitor2unit(unit_id, monitor_id)
    select l_new_equipment_id, monitor_id
    from core.monitor2unit
    where unit_id =   l_old_equipment_id;

  insert into  core.modem2unit(unit_id, modem_id)
    select l_new_equipment_id, modem_id
    from core.modem2unit
    where unit_id =   l_old_equipment_id;

  insert into  core.hdd2unit(unit_id, hdd_id)
    select l_new_equipment_id, hdd_id
    from core.hdd2unit
    where unit_id =   l_old_equipment_id;

 --снимаем блок с машины
  return core.jf_unit2tr_pkg$of_remove_from_tr(p_id_account, p_attr);
end;
$body$
language plpgsql volatile
cost 100;

alter function core."jf_unit2tr_pkg$of_change_unit"(numeric, text)
owner to adv;


--------------------------------------------
create or replace function core.jf_unit2tr_pkg$of_unit_insert(p_id_account numeric, p_attr text)
  returns text
as
$body$
declare
  l_equipment_id core.equipment.equipment_id%type		:= jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_id', true);
begin
  return core.jf_unit2tr_pkg$of_insert(p_id_account,  (('{"equipment_id":"'||l_equipment_id::text||'"}')::jsonb||p_attr::jsonb)::json::text);
end;
$body$
language plpgsql volatile;
