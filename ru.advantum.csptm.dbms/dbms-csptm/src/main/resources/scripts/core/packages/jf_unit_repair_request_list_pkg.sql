create or replace function core."jf_unit_repair_request_list_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
language plpgsql
as $$
DECLARE
  l_equipment_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', TRUE);
begin
  open p_rows for
    select q.repair_start,
           q.repair_end,
           e1.name_short depo_from,
           e2.name_short depo_to,
           bdt.name breakdown_type_name
    from
      (
        select s.repair_request_id,
               max(dt) filter(where s.repair_request_status_id = core.jf_repair_request_pkg$cn_status_to_contractor()) repair_start,
               max(dt) filter(where s.repair_request_status_id = core.jf_repair_request_pkg$cn_status_close()) repair_end,
               max(s.territory_id) filter(where s.repair_request_status_id = core.jf_repair_request_pkg$cn_status_to_contractor()) territory_from,
               max(s.territory_id) filter(where s.repair_request_status_id = core.jf_repair_request_pkg$cn_status_close()) territory_to,
               max(s.breakdown_type_id) breakdown_type_id
        from hist.v_repair_request_hist s
        where s.equipment_id = l_equipment_id
          and s.repair_request_status_id in (core.jf_repair_request_pkg$cn_status_close(), core.jf_repair_request_pkg$cn_status_to_contractor())
        group by s.repair_request_id
      ) q left join core.depo2territory d2t1 on q.territory_from = d2t1.territory_id
          left join core.entity e1 on d2t1.depo_id = e1.entity_id
          left join core.depo2territory d2t2 on q.territory_to = d2t2.territory_id
          left join core.entity e2 on d2t2.depo_id = e2.entity_id
          left join core.breakdown_type bdt on q.breakdown_type_id = bdt.breakdown_type_id
    order by q.repair_start;
end;
$$