﻿--**************************************************************************************************
-- Function: core."jf_equipment_type_block_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_equipment_type_block_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_equipment_type_block_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$
declare

    l_rf_db_method text := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
    l_tr_id core.tr.tr_id%type;
    l_list_unit_type_id bigint[];
begin
 if l_rf_db_method = 'core.tr' then
    l_tr_id:=  jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
    l_list_unit_type_id:=kdbo.jf_tr_check_equipment_pkg$get_available_type_unit(l_tr_id);
   if array_length(l_list_unit_type_id , 1)  is null then
       RAISE EXCEPTION '<<Нет доступных типов блоков для привязки по схеме  подключения ТС>>';
   end if;


 end if;

 open p_rows for
      select
          etp.equipment_type_id,
          etp.name_full,
          etp.name_short,
          etp.equipment_class_id,
          ecl.name as equipment_class_name
      from core.equipment_type etp
      join core.equipment_class ecl on ecl.equipment_class_id = etp.equipment_class_id
     WHERE etp.equipment_class_id = core.jf_equipment_class_pkg$cn_block()
      and (etp.equipment_type_id = any(l_list_unit_type_id ) or  array_length(l_list_unit_type_id , 1)  is null);
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_type_block_pkg$of_rows"(numeric, text)
  OWNER TO adv;


