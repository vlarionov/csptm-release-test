﻿CREATE OR REPLACE FUNCTION core."jf_equipment2tr_h_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.equipment2tr_h AS
$BODY$
declare
  l_r core.equipment2tr_h%rowtype;
begin
  l_r.equipment2tr_h_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment2tr_h_id', true);
  l_r.equipment_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
  l_r.tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
  l_r.installation_site_id:= jofl.jofl_pkg$extract_number(p_attr, 'installation_site_id', true);
  l_r.equipment_remove_reason_id:= jofl.jofl_pkg$extract_number(p_attr, 'equipment_remove_reason_id', true);

  return l_r;
end;
 $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_equipment2tr_h_pkg$attr_to_rowtype"(text)
OWNER TO adv;
--*******************************************************************************************


-- Function: core."jf_equipment2tr_h_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_equipment2tr_h_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_equipment2tr_h_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_rf_db_method TEXT;
  l_equipment_id bigint;
  l_tr_id bigint;
begin
  l_rf_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
  l_equipment_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
  l_tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);

  open p_rows for
      select
        lower(th.sys_period) as dt_begin,
        upper(th.sys_period) as dt_end,
        th.equipment_remove_reason_id,
        urr.name as unit_remove_reason_name,
        th.tr_id,
        tr.garage_num as tr_name,
        th.equipment_id,
        th.equipment2tr_h_id,
        et.equipment_type_name,
        et.equipment_model_name,
        et.serial_num,
        (select unit_num from core.unit where unit.unit_id = et.equipment_id) as unit_num
      from core.equipment2tr_h th
        join core.v_equipment et on et.equipment_id = th.equipment_id
        join core.tr tr on tr.tr_id = th.tr_id
        left join core.equipment_remove_reason urr on urr.equipment_remove_reason_id = th.equipment_remove_reason_id
      where th.equipment_id = l_equipment_id
         or th.tr_id= l_tr_id;

end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment2tr_h_pkg$of_rows"(numeric, text)
  OWNER TO adv;

--********************************************************************************************
CREATE OR REPLACE FUNCTION core."jf_equipment2tr_h_pkg$of_insert"(p_id_account numeric, p_attr text)
  RETURNS text as
$BODY$
declare
  l_r core.equipment2tr_h%rowtype;
begin
  l_r := core.jf_equipment2tr_h_pkg$attr_to_rowtype(p_attr);

  l_r.equipment2tr_h_id := nextval( 'core.equipment2tr_h_equipment2tr_h_id_seq' );
  l_r.sys_period := tsrange(localtimestamp, null);

  insert into core.equipment2tr_h select l_r.*;

  return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_equipment2tr_h_pkg$of_insert"(numeric, text)
OWNER TO adv;

--*********************************************************************************************************
CREATE OR REPLACE FUNCTION core."jf_equipment2tr_h_pkg$of_update"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r core.equipment2tr_h%rowtype;
begin
  l_r := core.jf_equipment2tr_h_pkg$attr_to_rowtype(p_attr);


  update core.equipment2tr_h set
    sys_period = tsrange(lower(sys_period),coalesce(upper(l_r.sys_period),localtimestamp)),
    installation_site_id = l_r.installation_site_id,
    equipment_remove_reason_id = l_r.equipment_remove_reason_id
  where
    equipment2tr_h_id = l_r.equipment2tr_h_id;

  return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_equipment2tr_h_pkg$of_update"(numeric, text)
OWNER TO adv;
