CREATE OR REPLACE function core.jf_breakdown_for_select_pkg$attr_to_rowtype(p_attr text) returns core.breakdown
LANGUAGE plpgsql
AS $BODY$
declare
   l_r core.breakdown%rowtype;
begin
   l_r.breakdown_id := jofl.jofl_pkg$extract_number(p_attr, 'breakdown_id', true);
   l_r.dt_begin := jofl.jofl_pkg$extract_date(p_attr, 'dt_begin', true);
   l_r.description := jofl.jofl_pkg$extract_varchar(p_attr, 'description', true);
   l_r.is_confirmed := jofl.jofl_pkg$extract_boolean(p_attr, 'is_confirmed', true);
   l_r.dt_end := jofl.jofl_pkg$extract_date(p_attr, 'dt_end', true);
   l_r.tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
   l_r.breakdown_type_id := jofl.jofl_pkg$extract_number(p_attr, 'breakdown_type_id', true);
   l_r.breakdown_status_id := jofl.jofl_pkg$extract_number(p_attr, 'breakdown_status_id', true);
   l_r.close_result_id := jofl.jofl_pkg$extract_number(p_attr, 'close_result_id', true);
   l_r.territory_id := jofl.jofl_pkg$extract_number(p_attr, 'territory_id', true);
   l_r.equipment_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
   l_r.account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', true);
   l_r.num := jofl.jofl_pkg$extract_number(p_attr, 'num', true);

   return l_r;
end;

$BODY$;

CREATE OR REPLACE function core.jf_breakdown_for_select_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $BODY$
declare
begin
 open p_rows for
      select
        breakdown_id,
        dt_begin,
        description,
        is_confirmed,
        dt_end,
        tr_id,
        breakdown_type_id,
        breakdown_status_id,
        close_result_id,
        territory_id,
        equipment_id,
        account_id,
        num
      from core.breakdown;
end;

$BODY$;



-- create function core."jf_breakdown_for_select_pkg$of_delete"(p_id_account numeric, p_attr text) returns text
-- LANGUAGE plpgsql
-- AS $$
-- declare
--    l_r core.breakdown%rowtype;
-- begin
--    l_r := core.jf_breakdown_for_select_pkg$attr_to_rowtype(p_attr);
--
--    delete from  core.breakdown where  breakdown_id = l_r.breakdown_id;
--
--    return null;
-- end;
--
-- $$;
--
-- create function core."jf_breakdown_for_select_pkg$of_insert"(p_id_account numeric, p_attr text) returns text
-- LANGUAGE plpgsql
-- AS $$
-- declare
--    l_r core.breakdown%rowtype;
-- begin
--    l_r := core.jf_breakdown_for_select_pkg$attr_to_rowtype(p_attr);
--
--    insert into core.breakdown select l_r.*;
--
--    return null;
-- end;
--
-- $$;
--


