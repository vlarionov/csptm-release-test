CREATE OR REPLACE FUNCTION core.jf_repair_request_pkg$attr_to_rowtype(p_attr TEXT)
  RETURNS core.REPAIR_REQUEST
AS
$BODY$
DECLARE
  l_r core.repair_request%ROWTYPE;
BEGIN

  l_r.repair_request_id := jofl.jofl_pkg$extract_number(p_attr, 'repair_request_id', TRUE);
  l_r.dt := jofl.jofl_pkg$extract_date(p_attr, 'dt', TRUE);
  l_r.num := jofl.jofl_pkg$extract_varchar(p_attr, 'num', TRUE);
  l_r.description := jofl.jofl_pkg$extract_varchar(p_attr, 'description', TRUE);
  l_r.dt_closed := jofl.jofl_pkg$extract_date(p_attr, 'dt_closed', TRUE);
  l_r.repair_request_status_id := jofl.jofl_pkg$extract_number(p_attr, 'repair_request_status_id', TRUE);
  l_r.breakdown_type_id := jofl.jofl_pkg$extract_number(p_attr, 'breakdown_type_id', TRUE);
  l_r.close_result_id := jofl.jofl_pkg$extract_number(p_attr, 'close_result_id', TRUE);
  l_r.territory_id := jofl.jofl_pkg$extract_number(p_attr, 'territory_id', TRUE);
  l_r.facility_id := jofl.jofl_pkg$extract_number(p_attr, 'facility_id', TRUE);
  l_r.breakdown_id := jofl.jofl_pkg$extract_number(p_attr, 'breakdown_id', TRUE);
  l_r.equipment_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', TRUE);
  l_r.replacement_equipment_id := jofl.jofl_pkg$extract_number(p_attr, 'replacement_equipment_id', TRUE);
  l_r.sys_period := jofl.jofl_pkg$extract_number(p_attr, 'sys_period', TRUE);
  RETURN l_r;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_repair_request_pkg$of_rows(IN p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                              IN p_attr       TEXT)
  RETURNS REFCURSOR
AS
$BODY$
DECLARE
  f_dtb_DT                   TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'f_dtb_DT', TRUE);
  f_dte_DT                   TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'f_dte_DT', TRUE);
  f_list_depo_id             BIGINT [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', TRUE);
  f_breakdown_type_id        BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_breakdown_type_id', TRUE);
  f_equipment_type_id        BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_type_id', TRUE);
  f_equipment_model_id       BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_model_id', TRUE);
  f_repair_request_status_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_repair_request_status_id', TRUE);
  f_rr_close_result_id       BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_rr_close_result_id', TRUE);
  f_facility_id              BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_facility_id', TRUE);
BEGIN
  --     if p_attr is not null then
  --     RAISE EXCEPTION USING MESSAGE = p_attr;
  --     end if;
  OPEN p_rows FOR
  SELECT
    rr.repair_request_id,
    rr.dt,
    rr.num,
    rr.description,
    rr.dt_closed,
    rr.repair_request_status_id,
    rrs.name                                repair_request_status_name,
    rr.breakdown_type_id,
    bdp.name                                breakdown_type_name,
    rr.close_result_id,
    rcr.name                                close_result_name,
    rr.territory_id,
    te.name_short                           territory_name,
    rr.facility_id,
    /*fe.name_short*/ supp.name_short                           facility_name,
    rr.breakdown_id,
    '№ ' || bd.num || ' от ' || (bd.dt_begin + interval '3 hours') breakdown_name,
    rr.equipment_id,
    em.name                                 equipment_name,
    eq.serial_num                           equipment_serial_num,
    rr.replacement_equipment_id,
    replacement_equipment.serial_num AS     replacement_equipment_name,
    -- 'Тип оборудования'
    eqt.name_short                          eqt_name,
    --   'ТС',
    --   'Транспортное предприятие',
    de.name_short                           depo_name,
    tr.garage_num                           tr_g_num,
    tr.licence                              tr_s_num,
    fw.version  fw_version,
    CASE rr.repair_request_status_id
    WHEN core.jf_repair_request_pkg$cn_status_new() THEN 'P{A},D{OF_TO_CONTRACTOR, OF_DELETE, OF_CLOSE}'
    WHEN core.jf_repair_request_pkg$cn_status_reject() THEN 'P{A},D{OF_TO_CONTRACTOR, OF_CLOSE}'
    WHEN core.jf_repair_request_pkg$cn_status_to_contractor() THEN 'P{A},D{OF_TO_REPAIR, OF_REJECT}'
    WHEN core.jf_repair_request_pkg$cn_status_to_repair() THEN 'P{A},D{OF_REJECT, OF_RETURN, OF_SUBSTITUTE}'
    WHEN core.jf_repair_request_pkg$cn_status_return() THEN 'P{A},D{OF_TO_CONTRACTOR, OF_CLOSE}'
    WHEN core.jf_repair_request_pkg$cn_status_substitute() THEN 'P{A},D{OF_TO_CONTRACTOR, OF_CLOSE, OF_CREATE_NEW_EQUIPMENT}'
    WHEN core.jf_repair_request_pkg$cn_status_close() THEN 'P{A},D{}'
   ELSE
     NULL
    END                              AS     "ROW$POLICY"
  FROM core.repair_request rr
    JOIN core.repair_request_status rrs ON rrs.repair_request_status_id = rr.repair_request_status_id
    JOIN core.breakdown_type bdp ON bdp.breakdown_type_id = rr.breakdown_type_id
    LEFT JOIN core.rr_close_result rcr ON rcr.rr_close_result_id = rr.close_result_id
    LEFT JOIN core.territory t ON t.territory_id = rr.territory_id
    LEFT JOIN core.entity te ON te.entity_id = t.territory_id
    LEFT JOIN core.facility f ON f.facility_id = rr.facility_id
    LEFT JOIN core.entity fe ON fe.entity_id = f.facility_id
    LEFT JOIN core.breakdown bd ON bd.breakdown_id = rr.breakdown_id
    JOIN core.equipment eq ON eq.equipment_id = rr.equipment_id
    JOIN core.entity de ON de.entity_id = eq.depo_id
    JOIN core.equipment_model em ON em.equipment_model_id = eq.equipment_model_id
    JOIN core.equipment_type eqt ON eqt.equipment_type_id = em.equipment_type_id
    LEFT JOIN core.equipment replacement_equipment ON replacement_equipment.equipment_id = rr.replacement_equipment_id
    LEFT JOIN core.v_equipment2tr4all etr ON etr.equipment_id = eq.equipment_id
    LEFT JOIN core.tr tr ON tr.tr_id = etr.tr_id
    left join core.firmware fw on fw.firmware_id=eq.firmware_id
    /*Павел, прошу исправить по ремонтам и поломкам:
Подрядчик (обслуживающая организация) – в поле Подрядчик ошибочно отображается производитель оборудования.
*/
  left join core.entity supp on supp.entity_id = eq.unit_service_facility_id
  WHERE rr.dt BETWEEN f_dtb_DT AND f_dte_DT
        AND (rr.breakdown_type_id = f_breakdown_type_id OR f_breakdown_type_id IS NULL)
        AND (eq.equipment_model_id = f_equipment_model_id OR f_equipment_model_id IS NULL)
        AND (em.equipment_type_id = f_equipment_type_id OR f_equipment_type_id IS NULL)
        AND (rr.repair_request_status_id = f_repair_request_status_id OR f_repair_request_status_id IS NULL)
        AND (rr.close_result_id = f_rr_close_result_id OR f_rr_close_result_id IS NULL)
        AND (eq.depo_id = ANY (f_list_depo_id) OR array_length(f_list_depo_id, 1) IS NULL)
        AND (rr.facility_id = f_facility_id OR f_facility_id IS NULL);
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_repair_request_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
$BODY$
DECLARE
  l_r core.repair_request%ROWTYPE;
  l_re core.equipment%ROWTYPE;
BEGIN
  l_r := core.jf_repair_request_pkg$attr_to_rowtype(p_attr);
  --         RAISE EXCEPTION USING MESSAGE = p_attr;
/*5.3.6	Требования к функции «Ведение реестра операций технического обслуживания БО»
Заявку на ремонт можно оформить только для того оборудования, в Карточке которого уставлен
вид обслуживания «Техническое сопровождение» и указана Обслуживающая организация*/
  select * into l_re from core.equipment eqp where eqp.equipment_id=l_r.equipment_id;
  IF l_re.unit_service_facility_id is null THEN
    RAISE EXCEPTION '<<Не указана обслуживающая организация, создать заявку на ремонт нельзя!>>';
  END IF;
--   IF l_re.unit_service_type_id <> 4 /*, 'Внешнее техническое обслуживание'*/ THEN
--
--   END IF;

  l_r.dt := current_timestamp;
  l_r.num := nextval('core.repair_request_num_seq');
  l_r.repair_request_id := nextval('core.repair_request_repair_request_id_seq');
  l_r.repair_request_status_id := core.jf_repair_request_pkg$cn_status_new();
    IF l_r.breakdown_type_id IS NULL OR l_r.breakdown_type_id <= 0 THEN
      l_r.breakdown_type_id:= 9;      /*неопределено*/
  END IF;

  IF l_r.breakdown_id IS NOT NULL
  THEN
    SELECT
      b.equipment_id,
      b.breakdown_type_id
    INTO l_r.equipment_id, l_r.breakdown_type_id
    FROM core.breakdown b
    WHERE b.breakdown_id = l_r.breakdown_id;
  END IF;

  SELECT
    e.territory_id,
    e.facility_id
  INTO l_r.territory_id, l_r.facility_id
  FROM core.equipment e
  WHERE e.equipment_id = l_r.equipment_id;

  INSERT INTO core.repair_request SELECT l_r.*;


    select * into l_r from core.repair_request rr where rr.repair_request_id = l_r.repair_request_id;

  RETURN row_to_json(l_r) :: JSONB;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_repair_request_pkg$of_update(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
$BODY$
DECLARE
  l_r core.repair_request%ROWTYPE;
BEGIN
  l_r := core.jf_repair_request_pkg$attr_to_rowtype(p_attr);

   update core.repair_request set
    dt                       = l_r.dt,
    num                      = l_r.num,
    description              = l_r.description,
    dt_closed                = l_r.dt_closed,
    repair_request_status_id = l_r.repair_request_status_id,
    breakdown_type_id        = l_r.breakdown_type_id,
    close_result_id          = l_r.close_result_id,
    territory_id             = l_r.territory_id,
    facility_id              = l_r.facility_id,
    breakdown_id             = l_r.breakdown_id,
    replacement_equipment_id = l_r.replacement_equipment_id
  WHERE
    repair_request_id = l_r.repair_request_id;

  RETURN NULL;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_repair_request_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
$BODY$
DECLARE
  l_r                        core.repair_request%ROWTYPE;
  l_repair_request_status_id NUMERIC;
BEGIN
  l_r := core.jf_repair_request_pkg$attr_to_rowtype(p_attr);

  SELECT repair_request_status_id
  INTO STRICT l_repair_request_status_id
  FROM core.repair_request
    where repair_request_id = l_r.repair_request_id FOR UPDATE NOWAIT;

  IF (l_repair_request_status_id NOT IN (core.jf_repair_request_pkg$cn_status_new()
      , core.jf_repair_request_pkg$cn_status_reject())) then
    RAISE EXCEPTION '<<Удаление допустимо для заявке в статусе Новая>>';
  END IF;

   delete from  core.repair_request where  repair_request_id = l_r.repair_request_id;

  RETURN NULL;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_repair_request_pkg$of_to_contractor(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
$BODY$
DECLARE
  l_r                        core.repair_request%ROWTYPE;
  l_repair_request_status_id NUMERIC;

BEGIN
  --5.3.7.4	Передача заявки на ремонт БО подрядчику
  l_r := core.jf_repair_request_pkg$attr_to_rowtype(p_attr);


  SELECT repair_request_status_id
  INTO STRICT l_repair_request_status_id
  FROM core.repair_request
   where repair_request_id = l_r.repair_request_id FOR UPDATE NOWAIT;

  IF (l_repair_request_status_id NOT IN (core.jf_repair_request_pkg$cn_status_new()
                                        , core.jf_repair_request_pkg$cn_status_reject()
                                        , core.jf_repair_request_pkg$cn_status_return()
                                        , core.jf_repair_request_pkg$cn_status_substitute()
  )) then
    RAISE EXCEPTION '<<Для передачи подрядчику заявка д.б. в статусе Новая, Отказ, Отремонтировано или Замена>>';
  END IF;

   if l_r.equipment_id is null THEN
    RAISE EXCEPTION '<<Для передачи подрядчику заявка д.б. указано оборудование!>>';
  END IF;

  UPDATE core.repair_request
  SET repair_request_status_id = core.jf_repair_request_pkg$cn_status_to_contractor()
  WHERE repair_request_id = l_r.repair_request_id;

  /*
Обратите внимание, что после изменения статуса Заявки на ремонт на «Передано подрядчику»,
Система автоматически изменяет статус бортового оборудования (указанного в заявке) на «Ремонт».
*/
  UPDATE core.equipment
      SET equipment_status_id=4     /* 'Ремонт у Подрядчика', 'БО находится на ремонте у Подрядчика или в другом подразделении ГУП МГТ' equipment_status_id = 4*/
  WHERE equipment_id = l_r.equipment_id;

  RETURN NULL;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_repair_request_pkg$of_reject(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
$BODY$
DECLARE
  l_r                        core.repair_request%ROWTYPE;
  l_repair_request_status_id NUMERIC;
  l_reject_reason            TEXT;

BEGIN
  --5.3.7.5	Отказ подрядчика от ремонта БО
  l_r := core.jf_repair_request_pkg$attr_to_rowtype(p_attr);

  l_reject_reason := jofl.jofl_pkg$extract_varchar(attr := p_attr, attrname := 'f_reject_reason_TEXT', allownull := FALSE);


  SELECT repair_request_status_id
  INTO STRICT l_repair_request_status_id
  FROM core.repair_request
  WHERE repair_request_id = l_r.repair_request_id
  FOR UPDATE NOWAIT;

  IF (l_repair_request_status_id NOT IN (core.jf_repair_request_pkg$cn_status_to_contractor()))
  THEN
    RAISE EXCEPTION '<<Для отказа от ремонта заявка д.б. в статусе Передано подрядчику>>';
  END IF;
  /*
  Отказ от ремонта должен оформляться следующим образом:
  */
  /*•	Подрядчик создает комментарий к Заявке, в котором указывает причину отказа;*/
  INSERT INTO core.repair_request_comment (dt, comment, repair_request_id, account_id)
  VALUES (now(), l_reject_reason, l_r.repair_request_id, p_id_account);
  /*•	Подрядчик переводит Заявку в статус «Отказ». Для этого нужно выделить Заявку в списке, нажать на кнопку «Действие» и выбрать пункт «Отказаться от ремонта». */
  UPDATE core.repair_request
  SET repair_request_status_id = core.jf_repair_request_pkg$cn_status_reject()
  WHERE repair_request_id = l_r.repair_request_id;


  RETURN NULL;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_repair_request_pkg$of_to_repair(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
$BODY$
DECLARE
  l_r                        core.repair_request%ROWTYPE;
  l_repair_request_status_id NUMERIC;
BEGIN
  l_r := core.jf_repair_request_pkg$attr_to_rowtype(p_attr);


  SELECT repair_request_status_id
  INTO STRICT l_repair_request_status_id
  FROM core.repair_request
   where repair_request_id = l_r.repair_request_id FOR UPDATE NOWAIT;

   if (l_repair_request_status_id != core.jf_repair_request_pkg$cn_status_to_contractor() ) then
    RAISE EXCEPTION '<<Для передачи в ремонт заявка д.б. в статусе Передан подрядчику>>';
  END IF;

  UPDATE core.repair_request
  SET repair_request_status_id = core.jf_repair_request_pkg$cn_status_to_repair()
  WHERE repair_request_id = l_r.repair_request_id;

  RETURN NULL;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_repair_request_pkg$of_close(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
$BODY$
DECLARE
  l_r                        core.repair_request%ROWTYPE;
  l_repair_request_status_id NUMERIC;
BEGIN
  l_r := core.jf_repair_request_pkg$attr_to_rowtype(p_attr);


  SELECT repair_request_status_id
  INTO STRICT l_repair_request_status_id
  FROM core.repair_request
   where repair_request_id = l_r.repair_request_id FOR UPDATE NOWAIT;

   if (l_repair_request_status_id not in (core.jf_repair_request_pkg$cn_status_new(),  core.jf_repair_request_pkg$cn_status_return(), core.jf_repair_request_pkg$cn_status_reject(), core.jf_repair_request_pkg$cn_status_substitute() )) then
    RAISE EXCEPTION '<<Для закрытия заявка д.б. в статусе Новая, Отремонтировано, Отказ или Замена>>';
  END IF;

  UPDATE core.repair_request
  SET repair_request_status_id = core.jf_repair_request_pkg$cn_status_close()
  WHERE repair_request_id = l_r.repair_request_id;

  /*http://redmine.advantum.ru/issues/18725#note-16
  6) (добавлено) При выполнении операции Закрыть
  (когда эта команда подается после приемки отремонтированного оборудования из ремонта):
если статус Заявки на ремонт был = Отремонтировано, то изменить статус БО на Исправно
 */
  IF l_repair_request_status_id = core.jf_repair_request_pkg$cn_status_return() THEN
    update core.equipment
      set equipment_status_id = 2/*, Исправно, БО находится в исправном состоянии. Установлено на ТС или находится на складе*/
    where equipment_id = l_r.equipment_id;
  END IF;

  RETURN NULL;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION core.jf_repair_request_pkg$of_return(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
$BODY$
DECLARE
  l_r                        core.repair_request%ROWTYPE;
  l_depo_id           bigint;
  l_territory_id           bigint;
  l_repair_request_status_id NUMERIC;
BEGIN
  l_r := core.jf_repair_request_pkg$attr_to_rowtype(p_attr);
  l_depo_id := jofl.jofl_pkg$extract_number(p_attr, 'f_list_depo_id', TRUE);
  l_territory_id := jofl.jofl_pkg$extract_number(p_attr, 'f_list_territory_id', TRUE);
  perform core.jf_equipment_pkg$switch_depo(l_r.equipment_id, l_depo_id, l_territory_id);

  SELECT repair_request_status_id
  INTO STRICT l_repair_request_status_id
  FROM core.repair_request
   where repair_request_id = l_r.repair_request_id FOR UPDATE NOWAIT;

   if (l_repair_request_status_id != core.jf_repair_request_pkg$cn_status_to_repair() ) then
    RAISE EXCEPTION '<<Для передачи заказчику заявка д.б. в статусе Ремонт>>';
  END IF;

  UPDATE core.repair_request
  SET repair_request_status_id = core.jf_repair_request_pkg$cn_status_return()
  WHERE repair_request_id = l_r.repair_request_id;

  RETURN NULL;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;

create or replace function core.jf_repair_request_pkg$of_substitute(p_id_account numeric, p_attr text)
  returns text
as
$BODY$
declare
  l_r                        core.repair_request%rowtype;
  l_depo_id           bigint;
  l_territory_id           bigint;
  l_repair_request_status_id numeric;
  l_substitution             text;

begin
  l_r := core.jf_repair_request_pkg$attr_to_rowtype(p_attr);
  l_depo_id := jofl.jofl_pkg$extract_number(p_attr, 'f_list_depo_id', TRUE);
  l_territory_id := jofl.jofl_pkg$extract_number(p_attr, 'f_list_territory_id', TRUE);
  perform core.jf_equipment_pkg$switch_depo(l_r.equipment_id, l_depo_id, l_territory_id);

  select repair_request_status_id
  into strict l_repair_request_status_id
  from core.repair_request
  where repair_request_id = l_r.repair_request_id
  for update nowait;

  if (l_repair_request_status_id != core.jf_repair_request_pkg$cn_status_to_repair())
  then
    raise exception '<<Для замены заявка д.б. в статусе Ремонт>>';
  end if;

  l_substitution := jofl.jofl_pkg$extract_varchar(attr := p_attr, attrname := 'f_substitute_TEXT', allownull := false);
  insert into core.repair_request_comment (dt, comment, repair_request_id, account_id)
  values (now(), l_substitution, l_r.repair_request_id, p_id_account);


  update core.repair_request
  set repair_request_status_id = core.jf_repair_request_pkg$cn_status_substitute()

  where repair_request_id = l_r.repair_request_id;


  return null;
end;
$BODY$
language plpgsql volatile;


-- OF_CREATE_NEW_EQUIPMENT
CREATE OR REPLACE FUNCTION core.jf_repair_request_pkg$of_create_new_equipment(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
$BODY$
DECLARE
  l_r                        core.repair_request%ROWTYPE;
  l_repair_request_status_id NUMERIC;
  f_serial_num_TEXT          TEXT;
  f_unique_num_TEXT          TEXT;
  f_equipment_model_id       NUMERIC;
  l_new_id                   BIGINT;
BEGIN

  l_r := core.jf_repair_request_pkg$attr_to_rowtype(p_attr);
  SELECT repair_request_status_id
  INTO STRICT l_repair_request_status_id
  FROM core.repair_request
   where repair_request_id = l_r.repair_request_id FOR UPDATE NOWAIT;

   if (l_repair_request_status_id != core.jf_repair_request_pkg$cn_status_substitute() ) then
    RAISE EXCEPTION '<<Для замены заявка д.б. в статусе Замена>>';
  END IF;

  f_equipment_model_id := jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_model_id');
  f_serial_num_TEXT := jofl.jofl_pkg$extract_varchar(p_attr, 'f_serial_num_TEXT');
  f_unique_num_TEXT := jofl.jofl_pkg$extract_varchar(p_attr, 'f_unique_num_TEXT');

  INSERT INTO core.equipment (territory_id, facility_id, depo_id, unit_service_type_id,
                              /*equipment_id,*/ dt_begin, equipment_status_id,
                              serial_num, equipment_model_id
  )
    select territory_id, facility_id, depo_id,unit_service_type_id ,
      /*l_new_id,*/ now(), 2 /*1 ,Не определен OR 2,Исправно*/,
      f_serial_num_TEXT, f_equipment_model_id
    from core.equipment e where e.equipment_id = l_r.equipment_id
    RETURNING equipment_id INTO STRICT l_new_id
  --nextval('core.equipment_equipment_id_seq'::regclass)
  ;
  --12345098765
  --- UNIT
  INSERT INTO core.unit (unit_id, unit_num, hub_id)
      select l_new_id,f_unique_num_TEXT, hub_id from core.unit where unit_id = l_r.equipment_id;



  INSERT INTO core.unit_bnsr (unit_bnsr_id, channel_num)
      select l_new_id, channel_num from core.unit_bnsr  WHERE unit_bnsr_id = l_r.equipment_id;

  INSERT INTO core.unit_bnst (unit_bnst_id)
      select l_new_id from core.unit_bnst WHERE unit_bnst_id=l_r.equipment_id;

  INSERT INTO core.unit_kbtob (unit_kbtob_id)
      select l_new_id from core.unit_kbtob WHERE unit_kbtob_id =l_r.equipment_id;

  --- SENSOR
  INSERT INTO core.sensor (sensor_id, sensor_num)
      select l_new_id, sensor_num from core .sensor where sensor_id=l_r.equipment_id;

  --- MONITOR
  INSERT INTO core.monitor (monitor_id)
      select l_new_id from core .monitor where monitor_id=l_r.equipment_id;
  --- MODEM
  INSERT INTO core.modem (modem_id, imei, speed)
      select l_new_id, f_unique_num_TEXT, speed FROM core.modem where modem_id=l_r.equipment_id;

  -- HDD
  INSERT INTO core.hdd (hdd_id/*, type, capacity, size*/)
      select l_new_id from core.hdd where hdd_id=l_r.equipment_id;

  --- ASMPP
  INSERT INTO core.asmpp (sensor_id, doors_count)
      select l_new_id, doors_count from core.asmpp where sensor_id=l_r.equipment_id;


  UPDATE core.repair_request
  SET replacement_equipment_id = l_new_id /*замена*/
    , close_result_id          = 3 /*,Подлежит списанию*/
    , dt_closed                = now()
    , repair_request_status_id = 100 /*,Закрыто*/
  WHERE repair_request_id = l_r.repair_request_id;

  /*http://redmine.advantum.ru/issues/18725#note-16
  5) После выполнения операции "Зарегистрировать замену оборудования":
  у нового (заменяющего) оборудования: статус д.б. "Исправно"
  у старого (замененного) оборудования: статус д.б. "Выбыло"
  , причина списания = "Механическая неисправность" (позже скорректируем формулировку)
  , дата списания = текущая дата
  */
  DECLARE
    lr_eqp core.equipment%ROWTYPE;
    lr_nil TEXT;
    lr_dt_end TIMESTAMP;
    lr_decommission_reason_id INTEGER;
  BEGIN
    SELECT *
    INTO lr_eqp
    FROM core.equipment
    WHERE equipment_id = l_r.equipment_id;

  --lr_eqp.dt_end := now();
    lr_dt_end := now();
  --lr_eqp.decommission_reason_id := 5; --, 'Механическая неисправность'
    lr_decommission_reason_id := 5; --, 'Механическая неисправность'

    lr_nil := (
      row_to_json(lr_eqp) :: JSONB
      || ('{"F_decommission_reason_id":'||lr_decommission_reason_id||'}') :: JSONB
      || ('{"END_DT":"'||lr_dt_end ||'"}') :: JSONB
    --|| ('{"F_decommission_reason_id":'||lr_eqp.decommission_reason_id||'}') :: JSONB
    --|| ('{"END_DT":"'||lr_eqp.dt_end ||'"}') :: JSONB
    ) :: JSON :: TEXT;

    lr_nil := core.jf_equipment_pkg$of_decommission(p_id_account, lr_nil);
  END;
  RETURN NULL;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_repair_request_pkg$cn_status_new()
  RETURNS core.repair_request.repair_request_status_id%TYPE AS
'SELECT 1 :: BIGINT;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION core.jf_repair_request_pkg$cn_status_to_contractor()
  RETURNS core.repair_request.repair_request_status_id%TYPE AS
'SELECT 2 :: BIGINT;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION core.jf_repair_request_pkg$cn_status_to_repair()
  RETURNS core.repair_request.repair_request_status_id%TYPE AS
'SELECT 3 :: BIGINT;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION core.jf_repair_request_pkg$cn_status_reject()
  RETURNS core.repair_request.repair_request_status_id%TYPE AS
'SELECT 4 :: BIGINT;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION core.jf_repair_request_pkg$cn_status_close()
  RETURNS core.repair_request.repair_request_status_id%TYPE AS
'SELECT 100 :: BIGINT;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION core.jf_repair_request_pkg$cn_status_return()
  RETURNS core.repair_request.repair_request_status_id%TYPE AS
'SELECT 5 :: BIGINT;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION core.jf_repair_request_pkg$cn_status_substitute()
  RETURNS core.repair_request.repair_request_status_id%TYPE AS
'SELECT 13 :: BIGINT;'
LANGUAGE SQL IMMUTABLE;