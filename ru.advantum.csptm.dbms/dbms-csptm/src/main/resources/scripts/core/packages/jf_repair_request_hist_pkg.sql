create or replace function core.jf_repair_request_hist_pkg$of_rows(in p_id_account numeric, out p_rows refcursor,
                                                                   in p_attr       text)
  returns refcursor
as
$BODY$
declare
  l_repair_request_id numeric;
begin
  l_repair_request_id := jofl.jofl_pkg$extract_number(p_attr, 'repair_request_id', true);
  open p_rows for
  with s1 as (select
                crst.repair_request_status_id as current_status_id,
                lag(vrrh.repair_request_status_id)
                over (
                  order by lower(vrrh.sys_period) )
                                              as previous_status_id,
                lower(vrrh.sys_period)        as sys_period
              from hist.v_repair_request_hist vrrh
                join core.repair_request_status crst on vrrh.repair_request_status_id = crst.repair_request_status_id
              where vrrh.repair_request_id = l_repair_request_id
              order by vrrh.sys_period desc)
  select
    (select name
     from core.repair_request_status
     where repair_request_status_id = s1.current_status_id)  current_status,
    (select name
     from core.repair_request_status
     where repair_request_status_id = s1.previous_status_id) previous_status,
    s1.sys_period
  from s1
  where
    s1.current_status_id <> s1.previous_status_id;

  --   fetch first 1 rows only;
end;
$BODY$
language plpgsql volatile;
