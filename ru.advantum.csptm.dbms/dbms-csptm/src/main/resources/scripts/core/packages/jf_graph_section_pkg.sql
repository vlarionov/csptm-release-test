﻿
create or replace function core.jf_graph_section_pkg$of_rows
  (p_account_id   in numeric,
   p_attr         in text default '{}',
   p_rows         out refcursor)
returns refcursor
as $$ declare
   l_tr_list  text := jofl.jofl_pkg$extract_varchar(p_attr, 'TRJ_LIST',  true);
begin
   open p_rows for
   select t.route_trajectory_muid::text,
          t.graph_section_muid::text,
          t.order_num,
          s.wkt_geom
     from gis.graph_sec2route_traj t
     join gis.graph_sections s on s.muid = t.graph_section_muid
    where t.route_trajectory_muid in (select regexp_split_to_table(l_tr_list, ',')::bigint)
    order by route_trajectory_muid, order_num;
end;
$$ language plpgsql;
