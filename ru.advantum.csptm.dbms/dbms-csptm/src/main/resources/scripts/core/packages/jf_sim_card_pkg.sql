﻿-- Function: core."jf_sim_card_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION core."jf_sim_card_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_sim_card_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.sim_card AS
$BODY$ 
declare 
   l_r core.sim_card%rowtype; 
begin 
   l_r.mobile_tariff_id := jofl.jofl_pkg$extract_number(p_attr, 'mobile_tariff_id', true); 
   l_r.sim_card_group_id := jofl.jofl_pkg$extract_number(p_attr, 'sim_card_group_id', true); 
   l_r.equipment_status_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_status_id', true); 
   l_r.sim_card_id := jofl.jofl_pkg$extract_number(p_attr, 'sim_card_id', true);
   l_r.dt_begin := jofl.jofl_pkg$extract_date(p_attr, 'dt_begin', true); 
   l_r.iccid := jofl.jofl_pkg$extract_varchar(p_attr, 'iccid', true);
   l_r.phone_num := jofl.jofl_pkg$extract_varchar(p_attr, 'phone_num', true); 
   l_r.imsi := jofl.jofl_pkg$extract_varchar(p_attr, 'imsi', true);
   l_r.depo_id := jofl.jofl_pkg$extract_number(p_attr, 'depo_id', true);
   l_r.territory_id := jofl.jofl_pkg$extract_number(p_attr, 'territory_id', true);

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sim_card_pkg$attr_to_rowtype"(text)
  OWNER TO adv;
--*****************************************************************************************************
-- Function: core."jf_sim_card_pkg$of_rows"(numeric, text)

 --DROP FUNCTION core."jf_sim_card_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sim_card_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_rf_db_method text;
  l_arr_rf_db_method text array;
  l_decommission smallint		:=coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_decommission_INPLACE_K', true), 0);
  l_depo_id	 core.depo.depo_id%type 		:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', true), jofl.jofl_pkg$extract_number(p_attr, 'depo_id', true));
  l_territory_id core.sim_card.territory_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_territory_id', true);
  l_mobile_tariff_id core.sim_card.mobile_tariff_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_mobile_tariff_id', true);
  l_mobile_operator_id core.facility.facility_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_facility_id', true);
  l_sim_card_group_id core.sim_card.sim_card_group_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_sim_card_group_id', true);

  l_decommission_reason_id core.decommission_reason.decommission_reason_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_decommission_reason_id', true);
  l_equipment_status_list    smallint[]  := jofl.jofl_pkg$extract_tarray(p_attr, 'f_equipment_status_id', true);
  l_depo_list    bigint[]  := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_depo_id', true);

  l_b_ins_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_insert_from_DT', true);
  l_e_ins_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_insert_to_DT', true);
  l_b_decom_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_decom_from_DT', true);
  l_e_decom_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_decom_to_DT', true);

  l_link_bb			smallint					:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_link_bb_INPLACE_K', true), -1);
  l_link_tr			smallint					:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_link_tr_INPLACE_K', true), -1);

begin
  l_rf_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
  l_arr_rf_db_method := '{"core.sim_card2unit","core.modem"}';


 open p_rows for
 WITH decom AS (
               SELECT d.decommission_reason_id,
                 d.sim_card_id,
                 d.dt_end,
                 dr.name AS decommission_reason_name
               FROM core.sim_decommission d
                 JOIN core.decommission_reason dr ON dr.decommission_reason_id = d.decommission_reason_id
               WHERE NOT d.is_deleted
                 )
    , bo_with_sim as (
               select sc2u.sim_card_id, sc2u.equipment_id, tr.garage_num
               from core.sim_card2unit  sc2u
                 left join core.unit2tr u2tr on u2tr.unit_id = sc2u.equipment_id
                 left join core.tr on tr.tr_id = u2tr.tr_id
               where sc2u.sim_card_id is not null
               union all
               select modem.sim_card_id, modem.modem_id , tr.garage_num
               from core.modem  modem
                 left join core.equipment2tr e2tr on e2tr.equipment_id = modem.modem_id
                 left join core.tr on tr.tr_id = e2tr.tr_id
               where sim_card_id is not null
             )
   , bo as (
               select concat_ws (' ', et.equipment_type_name, et.equipment_model_name, et.serial_num) as bo_info, bo_with_sim.sim_card_id,  bo_with_sim.garage_num
               from bo_with_sim
                 join core.v_equipment et on et.equipment_id = bo_with_sim.equipment_id

            )
   , adata as (
               select
                 case sc.equipment_status_id
                 when core."jf_equipment_status_pkg$cn_status_decommissioned"() then 'P{A},D{OF_DECOMMISSION_CANCEL}'::text
                 else 'P{A},D{OF_UPDATE,OF_DECOMMISSION, OF_CHANGE_STATUS}'::text  end as "ROW$POLICY",
                 sc.mobile_tariff_id,
                 mt.name as mobile_tariff_name,
                 sc.sim_card_group_id,
                 scg.name as sim_card_group_name,
                 sc.equipment_status_id,
                 es.name as equipment_status_name,
                 decom.dt_end,
                 decom.decommission_reason_id,
                 decom.decommission_reason_name,
                 sc.sim_card_id,
                 sc.dt_begin,
                 --sc.dt_end,
                 decom.dt_end,
                 sc.iccid,
                 sc.phone_num,
                 sc.imsi,
                 sc.depo_id,
                 sc.territory_id,
                 entity4depo.name_full AS depo_name_full,
                 entity4depo.name_short AS depo_name_short,
                 entity4territory.name_full AS territory_name_full,
                 entity4territory.name_short AS territory_name_short,
                 entity4oper.name_short mobile_operator_name,
                 bo.bo_info,
                 bo.garage_num
               from core.sim_card sc
                 join core.mobile_tariff mt on mt.mobile_tariff_id = sc.mobile_tariff_id
                 join core.sim_card_group scg on scg.sim_card_group_id = sc.sim_card_group_id
                 join core.equipment_status es on es.equipment_status_id = sc.equipment_status_id
                 join core.entity entity4depo ON entity4depo.entity_id = sc.depo_id
                 left join core.entity entity4territory ON entity4territory.entity_id = sc.territory_id
                 left join core.entity entity4oper ON entity4oper.entity_id = mt.facility_id
                 left join decom ON decom.sim_card_id =  sc.sim_card_id
                 left join bo on bo.sim_card_id = sc.sim_card_id
                where
                    (sc.depo_id =l_depo_id or sc.depo_id = any(l_depo_list) )
                    and
                     ((l_decommission=0 and sc.equipment_status_id != core.jf_equipment_status_pkg$cn_status_decommissioned())
                   or (l_decommission =1 and sc.equipment_status_id = core.jf_equipment_status_pkg$cn_status_decommissioned()) )
                   and (l_b_ins_dt is null or sc.dt_begin >= l_b_ins_dt)
                   and (l_e_ins_dt is null or sc.dt_begin<= l_e_ins_dt)
                   and (l_decommission_reason_id is null or decom.decommission_reason_id = l_decommission_reason_id )
                   and (sc.equipment_status_id = any(l_equipment_status_list) or (array_length(l_equipment_status_list, 1)  is null))
                   and (l_territory_id is null or  sc.territory_id =l_territory_id)
                   and (l_mobile_tariff_id is null or  sc.mobile_tariff_id =l_mobile_tariff_id)
                   and (l_sim_card_group_id is null or  sc.sim_card_group_id =l_sim_card_group_id)
                   and (l_mobile_operator_id is null or  mt.facility_id =l_mobile_operator_id)

                     and (l_b_decom_dt is null or decom.dt_end >= l_b_decom_dt )
                   and (l_e_decom_dt is null or decom.dt_end<= l_e_decom_dt )
                   and
                       (
                   l_rf_db_method is null or
                   l_rf_db_method  != ALL(l_arr_rf_db_method)  or
                  ( not exists (select 1 from core.modem m where m.sim_card_id = sc.sim_card_id)
                  and not exists (select 1 from core.sim_card2unit sc2u where sc2u.sim_card_id = sc.sim_card_id))
                   )
           )
  select *
  from adata
  where (l_link_bb=-1 or coalesce(sign(length(bo_info)),0) = l_link_bb )
        and (l_link_tr =-1 or coalesce(sign(garage_num),0) = l_link_tr )

  ;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sim_card_pkg$of_rows"(numeric, text)
  OWNER TO adv;

--*****************************************************************************************************
-- Function: core."jf_sim_card_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_sim_card_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sim_card_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.sim_card%rowtype;
begin 
   l_r := core.jf_sim_card_pkg$attr_to_rowtype(p_attr);

   delete from  core.sim_card where  sim_card_id = l_r.sim_card_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sim_card_pkg$of_delete"(numeric, text)
  OWNER TO adv;

--*****************************************************************************************************
-- Function: core."jf_sim_card_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_sim_card_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sim_card_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.sim_card%rowtype;
begin 
   l_r := core.jf_sim_card_pkg$attr_to_rowtype(p_attr);
   l_r.sim_card_id := nextval( 'core.sim_card_sim_card_id_seq' );
   insert into core.sim_card select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sim_card_pkg$of_insert"(numeric, text)
  OWNER TO adv;

--*****************************************************************************************************
-- Function: core."jf_sim_card_pkg$of_update"(numeric, text)

-- DROP FUNCTION core."jf_sim_card_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sim_card_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.sim_card%rowtype;
begin 
   l_r := core.jf_sim_card_pkg$attr_to_rowtype(p_attr);

   update core.sim_card set 
          mobile_tariff_id = l_r.mobile_tariff_id, 
          sim_card_group_id = l_r.sim_card_group_id, 
          equipment_status_id = l_r.equipment_status_id, 
          iccid = l_r.iccid,
          phone_num = l_r.phone_num, 
          imsi = l_r.imsi,
          depo_id = l_r.depo_id,
          territory_id = l_r.territory_id
   where 
          sim_card_id = l_r.sim_card_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sim_card_pkg$of_update"(numeric, text)
  OWNER TO adv;

--*****************************************************************************************************

CREATE OR REPLACE FUNCTION core.jf_sim_card_pkg$of_message(
  p_sim_card_id numeric)
  RETURNS text AS
$BODY$
declare
  l_res text;
begin

  select into l_res sc.phone_num || '('|| sc.iccid ||')'
  from core.sim_card sc
  where sim_card_id = p_sim_card_id;


  return l_res;

end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core.jf_sim_card_pkg$of_message(numeric)
OWNER TO adv;

--*****************************************************************************
--списание
CREATE OR REPLACE FUNCTION core."jf_sim_card_pkg$of_decommission"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r core.sim_card%rowtype;
  l_equipment_type_name core.equipment_type.name_full%type;
  l_r_decom core.sim_decommission%rowtype;
  l_decommission_reason_id numeric;
  l_dt_end timestamp;
  l_res text;
begin
  l_r := core.jf_sim_card_pkg$attr_to_rowtype(p_attr);
  l_equipment_type_name:=  jofl.jofl_pkg$extract_varchar(p_attr, 'equipment_type_name', TRUE);
  l_decommission_reason_id:= jofl.jofl_pkg$extract_number(p_attr, 'f_decommission_reason_id', TRUE);

  l_dt_end:= jofl.jofl_pkg$extract_date(p_attr, 'END_DT', TRUE);

  l_r.equipment_status_id = core.jf_equipment_status_pkg$cn_status_decommissioned();

  if l_decommission_reason_id is null then
    RAISE EXCEPTION '<<Введите причину списания>>';
  end if;

  if l_dt_end is null then
    RAISE EXCEPTION '<<Введите дату списания>>';
  end if;

  if exists(select 1 from core.sim_card2unit where sim_card_id = l_r.sim_card_id
            union all
            select 1 from core.modem  where sim_card_id = l_r.sim_card_id
            ) THEN
    RAISE  EXCEPTION '<<Списание невозможно. Sim-карта %   установлена на оборудование (блок или модем)>>', l_r.iccid ;
  end if ;

  l_r_decom.sim_card_id = l_r.sim_card_id;
  l_r_decom.decommission_reason_id:=l_decommission_reason_id;

  l_res:= core.jf_sim_decommission_pkg$of_insert(p_id_account, '{"sim_card_id":'||l_r.sim_card_id||
                                                               ',"decommission_reason_id":'||l_decommission_reason_id||
                                                               ',"dt_end":"'||l_dt_end::text||'"}');


  return core.jf_sim_card_pkg$of_update(p_id_account, row_to_json(l_r)::text);


end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_sim_card_pkg$of_decommission"(numeric, text)
OWNER TO adv;

--************************************************************************
--отмена выбытия
CREATE OR REPLACE FUNCTION core."jf_sim_card_pkg$of_decommission_cancel"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r core.sim_card%rowtype;
  l_new_status core.equipment_status.equipment_status_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_status_id', true);
begin
  l_r := core.jf_sim_card_pkg$attr_to_rowtype(p_attr);
  if l_r.equipment_status_id != core.jf_equipment_status_pkg$cn_status_decommissioned() then --списан
    RAISE  EXCEPTION '<<Статус не может быть изменен, т.к. sim - карта % не выбыла (не списана). >>',l_r.iccid ;
  end if;

  return core.jf_sim_decommission_pkg$of_cancel(p_id_account, p_attr);
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_sim_card_pkg$of_decommission_cancel"(numeric, text)
OWNER TO adv;

--**********************************************************************************
--изменение статуса
CREATE OR REPLACE FUNCTION core."jf_sim_card_pkg$of_change_status"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r core.sim_card%rowtype;
  l_new_status core.equipment_status.equipment_status_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_new_equipment_status_id', true);
begin
  l_r := core.jf_sim_card_pkg$attr_to_rowtype(p_attr);
  if l_r.equipment_status_id = core.jf_equipment_status_pkg$cn_status_decommissioned() then --списан
    RAISE  EXCEPTION '<<Статус не может быть изменен, т.к. sim-карта % выбыла (списана). Для работы с sim-картами необходимо отменить выбытие (списание)>>', l_r.iccid ;
  end if;
  l_r.equipment_status_id:=l_new_status;
  return core.jf_sim_card_pkg$of_update(p_id_account,  row_to_json(l_r)::text);
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_sim_card_pkg$of_change_status"(numeric, text)
OWNER TO adv;

--**********************************************************************************
