﻿-- Function: core."jf_equipment_type_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION core."jf_equipment_type_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_equipment_type_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.equipment_type AS
$BODY$ 
declare 
   l_r core.equipment_type%rowtype; 
begin 
   l_r.equipment_type_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_type_id', true); 
   l_r.name_full := jofl.jofl_pkg$extract_varchar(p_attr, 'name_full', true); 
   l_r.name_short := jofl.jofl_pkg$extract_varchar(p_attr, 'name_short', true); 
   l_r.equipment_class_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_class_id', true);
   l_r.has_diagnostic := jofl.jofl_pkg$extract_boolean(p_attr, 'has_diagnostic', true);
   l_r.note := jofl.jofl_pkg$extract_varchar(p_attr, 'note', true);

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_type_pkg$attr_to_rowtype"(text)
  OWNER TO adv;
--**************************************************************************************************
-- Function: core."jf_equipment_type_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_equipment_type_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_equipment_type_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$
declare
  l_rf_db_method text:= jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
  l_arr_rf_db_method_filtr text array;
begin
  l_arr_rf_db_method_filtr  := '{"core.equipment_model","core.sensor","core.breakdown"' ||
                               ',"core.repair_request","kdbo.diagnostic_list","kdbo.equipment_status_report"' ||
                               ', "kdbo.availability_of_units_report", "kdbo.unit_working_period"}';

  open p_rows for
      select
          etp.equipment_type_id,
          etp.name_full,
          etp.name_short,
          etp.equipment_class_id,
          ecl.name as equipment_class_name,
          etp.has_diagnostic,
          etp.note
      from core.equipment_type etp
      join core.equipment_class ecl on ecl.equipment_class_id = etp.equipment_class_id
      where (l_rf_db_method is null) or
            (l_rf_db_method = any (l_arr_rf_db_method_filtr)) or
            (l_rf_db_method = 'core.sensor_other' and etp.equipment_type_id not in (core."jf_equipment_type_pkg$cn_type_modem"(),
                                                                                   core."jf_equipment_type_pkg$cn_type_monitor"(),
                                                                                   core."jf_equipment_type_pkg$cn_type_hdd"())
                                                  and etp.equipment_class_id = core."jf_equipment_class_pkg$cn_other"() )
  ;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_type_pkg$of_rows"(numeric, text)
  OWNER TO adv;

--**************************************************************************************************
-- Function: core."jf_equipment_type_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_equipment_type_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_equipment_type_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.equipment_type%rowtype;
begin 
   l_r := core.jf_equipment_type_pkg$attr_to_rowtype(p_attr);

   delete from  core.equipment_type where  equipment_type_id = l_r.equipment_type_id;

   return null;
exception
  when 	foreign_key_violation then
     RAISE  EXCEPTION '<<Удаление невозможно! запись используется в других документах системы>>';
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_type_pkg$of_delete"(numeric, text)
  OWNER TO adv;

--**************************************************************************************************
-- Function: core."jf_equipment_type_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_equipment_type_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_equipment_type_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.equipment_type%rowtype;
begin 
   l_r := core.jf_equipment_type_pkg$attr_to_rowtype(p_attr);
   l_r.equipment_type_id := nextval('core.equipment_type_equipment_type_id_seq');

   l_r.has_diagnostic:=true;
   insert into core.equipment_type select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_type_pkg$of_insert"(numeric, text)
  OWNER TO adv;

--**************************************************************************************************
-- Function: core."jf_equipment_type_pkg$of_update"(numeric, text)

-- DROP FUNCTION core."jf_equipment_type_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_equipment_type_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.equipment_type%rowtype;
begin 
   l_r := core.jf_equipment_type_pkg$attr_to_rowtype(p_attr);

   update core.equipment_type set 
          name_full = l_r.name_full, 
          name_short = l_r.name_short, 
          --equipment_class_id = l_r.equipment_class_id, /*редактировать класс нельзя потому что на него завязаны разные выборки*/
          has_diagnostic = l_r.has_diagnostic,
          note = l_r.note
   where 
          equipment_type_id = l_r.equipment_type_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_type_pkg$of_update"(numeric, text)
  OWNER TO adv;

--**************************************************************************************************
CREATE OR REPLACE FUNCTION core."jf_equipment_type_pkg$of_has_diagnostic_switch"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r core.equipment_type%rowtype;
begin
  l_r := core.jf_equipment_type_pkg$attr_to_rowtype(p_attr);
  --Для всего оборудования данного класса переключаем признак

  update core.equipment set
    has_diagnostic = not has_diagnostic
  where
    exists (select 1 from core.equipment_model em where em.equipment_model_id = equipment.equipment_model_id
                                                        and em.equipment_type_id=  l_r.equipment_type_id);

  update core.equipment_model set
    has_diagnostic = not has_diagnostic
  where
    equipment_type_id = l_r.equipment_type_id;

  update core.equipment_type set
    has_diagnostic = not has_diagnostic
  where
    equipment_type_id = l_r.equipment_type_id;

  return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_equipment_type_pkg$of_has_diagnostic_switch"(numeric, text)
OWNER TO adv;


--**************************************************************************************************
-- 2,Бортовой навигационно-связной терминал,БНСТ
CREATE OR REPLACE FUNCTION core."jf_equipment_type_pkg$cn_type_bnst"()
  RETURNS integer AS
'select 2;'
LANGUAGE sql IMMUTABLE;

-- 3,Датчик уровня топлива,ДУТ
CREATE OR REPLACE FUNCTION core.jf_equipment_type_pkg$cn_type_dut()
  RETURNS INTEGER AS
'select 3;'
LANGUAGE SQL IMMUTABLE;

-- 4,Автоматизированная система мониторинга пассажиропотоков,АСМПП
CREATE OR REPLACE FUNCTION core.jf_equipment_type_pkg$cn_type_asmpp()
  RETURNS INTEGER AS
'select 4;'
LANGUAGE SQL IMMUTABLE;

-- 5,Датчик задымления в кабине водителя,ИПК
CREATE OR REPLACE FUNCTION core.jf_equipment_type_pkg$cn_type_fire()
  RETURNS INTEGER AS
  'select 5;'
LANGUAGE SQL IMMUTABLE;

-- 6,Тревожная кнопка,ТК
CREATE OR REPLACE FUNCTION core.jf_equipment_type_pkg$cn_type_alarm_btn()
  RETURNS INTEGER AS
'select 6;'
LANGUAGE SQL IMMUTABLE;

-- 9,Бортовая навигационной-связная радиостанция,БНСР
CREATE OR REPLACE FUNCTION core."jf_equipment_type_pkg$cn_type_bnsr"()
  RETURNS integer AS
'select 9;'
LANGUAGE sql IMMUTABLE;

-- 10,Комплект бортового телематического оборудования безопасности,КБТОБ
CREATE OR REPLACE FUNCTION core."jf_equipment_type_pkg$cn_type_kbtob"()
  RETURNS integer AS
'select 10;'
LANGUAGE sql IMMUTABLE;

-- 16,Модем,Модем
CREATE OR REPLACE FUNCTION core.jf_equipment_type_pkg$cn_type_modem()
  RETURNS INTEGER AS
'select 16;'
LANGUAGE SQL IMMUTABLE;

-- 17,Монитор водителя,Монитор водителя
CREATE OR REPLACE FUNCTION core.jf_equipment_type_pkg$cn_type_monitor()
  RETURNS INTEGER AS
'select 17;'
LANGUAGE SQL IMMUTABLE;

-- 18,Камера наблюдения,Камера наблюдения
CREATE OR REPLACE FUNCTION core.jf_equipment_type_pkg$cn_type_camcorder()
  RETURNS INTEGER AS
  'select 18;'
LANGUAGE SQL IMMUTABLE;

-- 19,Жесткий диск,ЖД
CREATE OR REPLACE FUNCTION core.jf_equipment_type_pkg$cn_type_hdd()
  RETURNS INTEGER AS
'select 19;'
LANGUAGE SQL IMMUTABLE;

-- 54,Система автономного хода,САХ
CREATE OR REPLACE FUNCTION core.jf_equipment_type_pkg$cn_type_skah()
  RETURNS INTEGER AS
  'select 54;'
LANGUAGE SQL IMMUTABLE;

-- 55,Температурный датчик,ДТ
CREATE OR REPLACE FUNCTION core.jf_equipment_type_pkg$cn_type_temperature()
  RETURNS INTEGER AS
  'select 55;'
LANGUAGE SQL IMMUTABLE;


CREATE OR REPLACE FUNCTION core.jf_equipment_type_pkg$cn_type_dvzh()
  RETURNS INTEGER AS
  'select 7;'
LANGUAGE SQL IMMUTABLE;
-- 7,Датчик включения заднего хода,ДВ ЗХ
-- 12,Манипулятор БНСТ,Манипулятор БНСТ
-- 13,Манипулятор БНСР,Манипулятор БНСР
-- 14,Микрофон,Микрофон

