﻿CREATE OR REPLACE FUNCTION core."jf_equipment2tr4all_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_r core.equipment2tr%rowtype;
begin
  l_r.tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
 open p_rows for 
      select
        equipment_id,
        tr_id,
        serial_num,
        firmware_id,
        firmware_name,
        unit_service_type_id,
        unit_service_type_name,
        facility_id,
        facility_name,
        equipment_status_id,
        equipment_status_name,
        decommission_reason_id,
        decommission_reason_name,
        dt_begin,
        dt_end,
        equipment_model_id,
        equipment_model_name,
        equipment_type_name,
        equipment_type_id,
        depo_name_full,
        depo_name_short,
        usw_num,
        phone_num
      from core.v_equipment2tr4all
      where decommission_reason_id is null
       and tr_id= l_r.tr_id ;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment2tr4all_pkg$of_rows"(numeric, text)
  OWNER TO adv;


create or replace function core.jf_equipment2tr4all_pkg$of_dial
  (p_account_id               in numeric,
   p_attr                     in text)
returns text
as $$ declare
   l_tr_id     bigint  := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
   l_unit_id   integer := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
   l_result    text;
begin
   select voip.voice_call_pkg$call_to_unit(p_account_id::bigint, l_tr_id, l_unit_id, null::bigint) into l_result;
   return null;
end;
$$ language plpgsql volatile security invoker;


CREATE OR REPLACE FUNCTION core.jf_equipment2tr4all_pkg$of_breakdown(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS $BODY$
DECLARE
  l_b   core.breakdown%ROWTYPE;
  l_res TEXT;
BEGIN
  l_b.equipment_id := ((p_attr) :: JSON ->> 'equipment_id') :: BIGINT;
  l_b.tr_id := ((p_attr) :: JSON ->> 'tr_id') :: BIGINT;
  l_b.breakdown_id := -1;
  l_b.dt_begin := now();
  l_b.description := (p_attr) :: JSON ->> 'DESCR_TEXT';
  l_b.breakdown_type_id := ((p_attr) :: JSON ->> 'F_breakdown_type_id') :: BIGINT;
  l_b.territory_id := ((p_attr) :: JSON ->> 'territory_id') :: BIGINT;

  l_res := core.jf_breakdown_pkg$of_insert(p_id_account, (row_to_json(l_b)) :: VARCHAR);
  RETURN NULL;
END;

$BODY$ LANGUAGE plpgsql VOLATILE;