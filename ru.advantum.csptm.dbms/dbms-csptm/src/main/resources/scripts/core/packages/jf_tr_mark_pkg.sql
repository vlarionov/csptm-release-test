CREATE OR REPLACE FUNCTION core.jf_tr_mark_pkg$attr_to_rowtype(p_attr text)
  RETURNS core.tr_mark
AS
  $BODY$
  declare
   l_r core.tr_mark%rowtype;
begin
   l_r.tr_mark_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_mark_id', true);
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true);
   l_r.facility_id := jofl.jofl_pkg$extract_number(p_attr, 'facility_id', true);

   return l_r;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION core.jf_tr_mark_pkg$of_rows(in p_id_account numeric, out p_rows refcursor, in p_attr text)
  RETURNS refcursor
AS
  $BODY$
  declare
begin
 open p_rows for
      select
        tr_mark_id,
        name,
        facility.facility_id,
        entity.name_short
      from core.tr_mark
            left join core.facility on facility.facility_id = tr_mark.facility_id
            left join core.entity on entity.entity_id = facility.facility_id;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_tr_mark_pkg$of_insert(p_id_account numeric, p_attr text)
  RETURNS text
AS
  $BODY$
  declare
   l_r core.tr_mark%rowtype;
begin
   l_r := core.jf_tr_mark_pkg$attr_to_rowtype(p_attr);
   l_r.tr_mark_id := nextval('core.tr_mark_tr_mark_id_seq');

   insert into core.tr_mark select l_r.*;

   return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION core.jf_tr_mark_pkg$of_update(p_id_account numeric, p_attr text)
  RETURNS text
AS
  $BODY$
  declare
   l_r core.tr_mark%rowtype;
begin
   l_r := core.jf_tr_mark_pkg$attr_to_rowtype(p_attr);

   update core.tr_mark set
          name = l_r.name,
          facility_id = l_r.facility_id
   where
          tr_mark_id = l_r.tr_mark_id;

   return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_tr_mark_pkg$of_delete(p_id_account numeric, p_attr text)
  RETURNS text
AS
  $BODY$
  declare
   l_r core.tr_mark%rowtype;
begin
   l_r := core.jf_tr_mark_pkg$attr_to_rowtype(p_attr);

   delete from  core.tr_mark where  tr_mark_id = l_r.tr_mark_id;

   return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;