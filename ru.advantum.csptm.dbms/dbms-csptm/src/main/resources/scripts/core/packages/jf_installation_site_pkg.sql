﻿-- Function: core."jf_installation_site_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION core."jf_installation_site_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_installation_site_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.installation_site AS
$BODY$ 
declare 
   l_r core.installation_site%rowtype; 
begin 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 
   l_r.installation_site_id := jofl.jofl_pkg$extract_number(p_attr, 'installation_site_id', true); 

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_installation_site_pkg$attr_to_rowtype"(text)
  OWNER TO adv;
--****************************************************************************************************

-- Function: core."jf_installation_site_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_installation_site_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_installation_site_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
begin 
 open p_rows for 
      select 
        name, 
        installation_site_id
      from core.installation_site; 
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_installation_site_pkg$of_rows"(numeric, text)
  OWNER TO adv;
--****************************************************************************************************
-- Function: core."jf_installation_site_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_installation_site_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_installation_site_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.installation_site%rowtype;
begin 
   l_r := core.jf_installation_site_pkg$attr_to_rowtype(p_attr);

   delete from  core.installation_site where  installation_site_id = l_r.installation_site_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_installation_site_pkg$of_delete"(numeric, text)
  OWNER TO adv;


--****************************************************************************************************
-- Function: core."jf_installation_site_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_installation_site_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_installation_site_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.installation_site%rowtype;
begin 
   l_r := core.jf_installation_site_pkg$attr_to_rowtype(p_attr);
   l_r.installation_site_id := nextval('core.installation_site_installation_site_id_seq');

   insert into core.installation_site select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_installation_site_pkg$of_insert"(numeric, text)
  OWNER TO adv;

--****************************************************************************************************
-- Function: core."jf_installation_site_pkg$of_update"(numeric, text)

-- DROP FUNCTION core."jf_installation_site_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_installation_site_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.installation_site%rowtype;
begin 
   l_r := core.jf_installation_site_pkg$attr_to_rowtype(p_attr);

   update core.installation_site set 
          name = l_r.name
   where 
          installation_site_id = l_r.installation_site_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_installation_site_pkg$of_update"(numeric, text)
  OWNER TO adv;

--****************************************************************************************************