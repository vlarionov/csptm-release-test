CREATE OR REPLACE FUNCTION core.jf_decomissed_equipment_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_rf_db_method 	TEXT;
  l_unit_num 		text := COALESCE(jofl.jofl_pkg$extract_varchar(p_attr, 'STR_FIELD_TEXT', true),
  								    jofl.jofl_pkg$extract_varchar(p_attr, 'f_unit_num_TEXT', true));
  l_equipment_type_id core.equipment_type.equipment_type_id%type		:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_type_id', true),
                                                                                jofl.jofl_pkg$extract_number(p_attr, 'unit_equipment_type_id', true));
  l_arr_depo_id 	bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', true);
  l_serial_num 	text := jofl.jofl_pkg$extract_varchar(p_attr, 'serial_num', TRUE);
  l_equipment_model_id 		core.equipment_model.equipment_model_id%type		:= jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_model_id', true);
  l_facility_id core.facility.facility_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_facility_id', true);
  l_decommission_reason_id 	core.decommission_reason.decommission_reason_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_decommission_reason_id', true);
  l_equipment_status_list   smallint[]  := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_equipment_status_id', true);
  l_b_ins_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_insert_from_DT', true);
  l_e_ins_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_insert_to_DT', true);
  l_b_decom_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_decom_from_DT', true);
  l_e_decom_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_decom_to_DT', true);
begin

  l_rf_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true);

  open p_rows for

    select
      et."ROW$POLICY",
      et.equipment_id,
      et.dt_begin,
      et.depo_id,
      et.depo_name_short,
      et.territory_id,
      et.territory_name_short,
      et.equipment_type_name,
      et.equipment_model_id,
      et.equipment_model_name,
      et.facility_id,
      et.facility_name,
      et.serial_num,
      et.unit_service_type_id,
      et.unit_service_type_name,
      et.unit_service_facility_id,
      et.unit_service_facility_name_short,
      et.equipment_status_id,
      et.equipment_status_name,
      et.firmware_id,
      et.firmware_name,
      et.decommission_reason_id,
      et.decommission_reason_name,
      et.dt_end,
      et.has_diagnostic
    from core.v_equipment et left join core.unit u on et.equipment_id = u.unit_id
   where et.equipment_status_id = core.jf_equipment_status_pkg$cn_status_decommissioned()
     and (et.equipment_type_id = l_equipment_type_id or l_equipment_type_id is null)
          and (et.depo_id = any(l_arr_depo_id))
          and (l_unit_num is null or l_unit_num = '' or u.unit_num like '%'||l_unit_num||'%')
          and (l_equipment_model_id is null or et.equipment_model_id = l_equipment_model_id)
          and (l_facility_id is null or et.facility_id = l_facility_id)
          and (l_decommission_reason_id is null or et.decommission_reason_id = l_decommission_reason_id )
          and (et.equipment_status_id = any(l_equipment_status_list) or (array_length(l_equipment_status_list, 1)  is null))
          and (l_serial_num is null or et.serial_num =l_serial_num )
          and (l_b_ins_dt is null or et.dt_begin >= l_b_ins_dt)
          and (l_e_ins_dt is null or et.dt_begin<= l_e_ins_dt)
          and (l_b_decom_dt is null or et.dt_end >= l_b_decom_dt )
          and (l_e_decom_dt is null or et.dt_end<= l_e_decom_dt )
    ;
end;
$body$
LANGUAGE 'plpgsql'
