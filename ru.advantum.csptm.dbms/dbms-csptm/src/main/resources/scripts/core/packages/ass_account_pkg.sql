CREATE OR REPLACE FUNCTION core.ass_account_pkg$login(p_username TEXT, p_pass TEXT)
  RETURNS NUMERIC
LANGUAGE plpgsql
AS $$
DECLARE
  l_id_jofl_account core.account.account_id%TYPE;
  l_pass            core.account.password%TYPE;
  l_locked          core.account.is_locked%TYPE;
BEGIN
  SELECT
    ja.id_jofl_account,
    a.password,
    a.is_locked
  INTO STRICT l_id_jofl_account, l_pass, l_locked
  FROM core.account a
    JOIN jofl.account ja ON a.account_id = ja.id_account_local
  WHERE login = p_username AND NOT a.is_deleted;

  IF l_pass != p_pass
  THEN
    RAISE EXCEPTION USING ERRCODE = 20402, MESSAGE = '<<Неправильный пароль!>>';
  END IF;

  IF l_locked = TRUE
  THEN
    RAISE EXCEPTION USING ERRCODE = 20423, MESSAGE = '<<Учетная запись заблокирована!>>';
  END IF;

  RETURN l_id_jofl_account;

  EXCEPTION WHEN no_data_found
  THEN
    RAISE EXCEPTION USING ERRCODE = 20401, MESSAGE = '<<Пользователь не найден!>>';
END;
$$;

------------------------------------------------------------------------------------------------------------------------

--drop function core.ass_account_pkg$of_get_info(p_id_jofl_account NUMERIC, attr TEXT)

CREATE OR REPLACE FUNCTION core.ass_account_pkg$of_get_info(p_id_jofl_account NUMERIC, attr TEXT)
  RETURNS TEXT AS $$
DECLARE
  lres            TEXT;
  l_account_roles TEXT [];
BEGIN
  l_account_roles = jofl.jofl_pkg$get_account_roles(p_id_jofl_account);

  WITH acc AS (
      SELECT
        a.login,
        a.first_name,
        a.middle_name,
        a.last_name,
        a.is_locked
      FROM core.account a
        JOIN jofl.account ja
          ON a.account_id = ja.id_account_local
      WHERE ja.id_jofl_account = p_id_jofl_account
  ), roles AS (
      SELECT
        array_agg(rr.id_role)   id_roles,
        array_agg(rr.role_name) role_names
      FROM jofl.ref_role rr
      WHERE rr.id_role = ANY (l_account_roles)
  )
  SELECT json_build_object('displayObject', json_build_object('LOGIN', acc.login,
                                                              'FIRST_NAME', acc.first_name,
                                                              'MIDDLE_NAME', acc.middle_name,
                                                              'LAST_NAME', acc.last_name,
                                                              'AS_LOCKED', acc.is_locked,
                                                              'ROLES', array_to_json(roles.id_roles),
                                                              'ROLES_NAME', array_to_json(roles.role_names)))
  INTO lres
  FROM acc
    JOIN roles ON TRUE;

  RETURN lres;
END; $$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."ass_account_pkg$of_get_info"( NUMERIC, TEXT )
OWNER TO adv;
