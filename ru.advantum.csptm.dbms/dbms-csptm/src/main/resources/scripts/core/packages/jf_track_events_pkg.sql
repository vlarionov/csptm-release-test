﻿create or replace function core.jf_track_events_pkg$of_rows(aid_account in numeric, arows out refcursor, attr in text default '{}') returns refcursor as $$
declare
    f_tr_id    core.tr.tr_id%type := jofl.jofl_pkg$extract_number(attr, 'F_ID_TR',true);
    f_begin_dt timestamp without time zone := jofl.jofl_pkg$extract_date(attr, 'F_BEGIN_DT',true);
    f_end_dt   timestamp without time zone := jofl.jofl_pkg$extract_date(attr, 'F_END_DT',true);
begin
    open arows for
    WITH  a  AS
    (
        SELECT
          tr_id
          ,val
          ,sensor_id
          ,event_time
          ,row_number() OVER (ORDER BY tr_id, sensor_id, event_time, packet_id) rn
        FROM snsr.binary_sensor_data
        where equipment_type_id = core.jf_equipment_type_pkg$cn_type_skah()
              and event_time between f_begin_dt and f_end_dt
              and tr_id = f_tr_id
    )
      ,b AS (
        SELECT
          tr_id
          ,val
          ,sensor_id
          ,event_time
          ,row_number() OVER (ORDER BY tr_id, sensor_id, event_time, rn) - row_number() OVER (PARTITION BY tr_id, sensor_id, val ORDER BY tr_id, sensor_id, event_time,rn) gr_id
        FROM a
    )
            select min(event_time) "GMTBEGIN",
                   max(event_time) "GMTEND",
                   ceil (extract(EPOCH FROM (max(event_time)  - min(event_time)))::decimal/60) "DURATION",
                   null "LAT",
                   null "LON",
                   4 "IDEVENT",
                   'СКАХ' "EVENT_NAME"
        from b
        where val
        GROUP BY tr_id,
          sensor_id,
          gr_id,
          val
        having max(event_time) > min(event_time)          ;

end;
$$ language plpgsql;
