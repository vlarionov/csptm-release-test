CREATE OR REPLACE FUNCTION core.jf_address_pkg$attr_to_rowtype(p_attr text)
  RETURNS core.address
AS
  $BODY$
  declare
 		l_r core.address%rowtype;
begin
  		l_r.address_id := jofl.jofl_pkg$extract_number(p_attr, 'address_id', true);
      l_r.entity_id := jofl.jofl_pkg$extract_number(p_attr, 'entity_id', true);
  		l_r.address_text := jofl.jofl_pkg$extract_varchar(p_attr, 'address_text', true);
 	return l_r;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;

--***************************************************************************************************
CREATE OR REPLACE FUNCTION core.jf_address_pkg$of_delete(p_id_account numeric, p_attr text)
  RETURNS text
AS
  $BODY$
  declare
 l_r CORE.ADDRESS%rowtype;
begin
l_r := CORE.JF_ADDRESS_PKG$attr_to_rowtype(p_attr);

      delete from  CORE.ADDRESS where
 address_id = l_r.address_id;
 		return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;

--***************************************************************************************************
CREATE OR REPLACE FUNCTION core.jf_address_pkg$of_insert(p_id_account numeric, p_attr text)
  RETURNS text
AS
  $BODY$
  declare
 l_r CORE.ADDRESS%rowtype;
begin
l_r := CORE.JF_ADDRESS_PKG$attr_to_rowtype(p_attr);

      insert into CORE.ADDRESS select l_r.*;
 		return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;
--***************************************************************************************************
CREATE OR REPLACE FUNCTION core.jf_address_pkg$of_rows(in p_id_account numeric, out p_rows refcursor, in p_attr text)
  RETURNS refcursor
AS
  $BODY$
  declare
  l_entity_id bigint;
begin
 l_entity_id := jofl.jofl_pkg$extract_number(p_attr, 'entity_id', TRUE);
 OPEN p_rows FOR
 select
   address_id,
   address_text,
   entity_id
 from CORE.ADDRESS
 where entity_id = l_entity_id   ;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;
--***************************************************************************************************
CREATE OR REPLACE FUNCTION core.jf_address_pkg$of_update(p_id_account numeric, p_attr text)
  RETURNS text
AS
  $BODY$
  declare
 l_r CORE.ADDRESS%rowtype;
begin
l_r := CORE.JF_ADDRESS_PKG$attr_to_rowtype(p_attr);

      update CORE.ADDRESS set
 address_text = l_r.address_text
 where
 address_id = l_r.address_id;
 		return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;
--***************************************************************************************************