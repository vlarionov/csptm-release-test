﻿-- Function: core."jf_hub_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION core."jf_hub_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_hub_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.hub AS
$BODY$ 
declare 
   l_r core.hub%rowtype; 
begin 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 
   l_r.port := jofl.jofl_pkg$extract_varchar(p_attr, 'port', true); 
   l_r.ip_address := jofl.jofl_pkg$extract_varchar(p_attr, 'ip_address', true); 
   l_r.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true); 
   l_r.protocol_id := jofl.jofl_pkg$extract_number(p_attr, 'protocol_id', true);
   l_r.hub_id := jofl.jofl_pkg$extract_number(p_attr, 'hub_id', true); 

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_hub_pkg$attr_to_rowtype"(text)
  OWNER TO adv;
--***********************************************************************************
-- Function: core."jf_hub_pkg$of_rows"(numeric, text)

 --DROP FUNCTION core."jf_hub_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_hub_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
begin 
 open p_rows for 
      select 
        h.name, 
        h.port, 
        h.ip_address, 
        h.comment, 
        h.protocol_id,
        h.hub_id,
        p.name_short as protocol_name
      from core.hub h
      left join core.protocol p on p.protocol_id = h.protocol_id;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_hub_pkg$of_rows"(numeric, text)
  OWNER TO adv;

--***********************************************************************************
-- Function: core."jf_hub_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_hub_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_hub_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.hub%rowtype;
begin 
   l_r := core.jf_hub_pkg$attr_to_rowtype(p_attr);

   delete from  core.hub where  hub_id = l_r.hub_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_hub_pkg$of_delete"(numeric, text)
  OWNER TO adv;

--***********************************************************************************
-- Function: core."jf_hub_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_hub_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_hub_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.hub%rowtype;
begin 
   l_r := core.jf_hub_pkg$attr_to_rowtype(p_attr);
   l_r.hub_id := nextval('core.hub_hub_id_seq');
   insert into core.hub select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_hub_pkg$of_insert"(numeric, text)
  OWNER TO adv;

--***********************************************************************************
-- Function: core."jf_hub_pkg$of_update"(numeric, text)

-- DROP FUNCTION core."jf_hub_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_hub_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.hub%rowtype;
begin 
   l_r := core.jf_hub_pkg$attr_to_rowtype(p_attr);

   update core.hub set 
          name = l_r.name, 
          port = l_r.port, 
          ip_address = l_r.ip_address, 
          comment = l_r.comment,
          protocol_id = l_r.protocol_id
   where 
          hub_id = l_r.hub_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_hub_pkg$of_update"(numeric, text)
  OWNER TO adv;

--***********************************************************************************