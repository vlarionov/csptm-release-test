--DROP TYPE core.JF_DEPO_TYPE CASCADE;

do $$
begin
  if not exists(select * from pg_type where typname = 'jf_depo_type') THEN

    CREATE TYPE core.JF_DEPO_TYPE AS
      (r_depo   core.DEPO,
       r_entity core.ENTITY);

    ALTER TYPE core.JF_DEPO_TYPE
      OWNER TO adv;

  end if;
end$$;

--DROP FUNCTION core.jf_depo_pkg$attr_to_rowtype( TEXT );

CREATE OR REPLACE FUNCTION core.jf_depo_pkg$attr_to_rowtype(p_attr TEXT)
  RETURNS core.JF_DEPO_TYPE
AS
$BODY$
DECLARE
  l_r_return core.JF_DEPO_TYPE;
  l_r_depo   core.DEPO;
  l_r_entity core.ENTITY;

BEGIN
  l_r_depo.tn_instance := jofl.jofl_pkg$extract_varchar(p_attr, 'tn_instance', TRUE);
  l_r_depo.depo_id := jofl.jofl_pkg$extract_number(p_attr, 'depo_id', TRUE);
  l_r_depo.carrier_id := jofl.jofl_pkg$extract_number(p_attr, 'carrier_id', TRUE);
  l_r_depo.tr_type_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_type_id', TRUE);

  l_r_entity.name_full := jofl.jofl_pkg$extract_varchar(p_attr, 'name_full', TRUE);
  l_r_entity.name_short := jofl.jofl_pkg$extract_varchar(p_attr, 'name_short', TRUE);
  l_r_entity.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', TRUE);
  l_r_entity.entity_id := jofl.jofl_pkg$extract_varchar(p_attr, 'depo_id', TRUE);

  l_r_return.r_entity := l_r_entity;
  l_r_return.r_depo := l_r_depo;
  RETURN l_r_return;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION core.jf_depo_pkg$of_rows(
      p_id_account NUMERIC,
  OUT p_rows       REFCURSOR,
      p_attr       TEXT
)
  RETURNS REFCURSOR AS
$body$
DECLARE
  l_rf_db_method         TEXT;
  l_tr_type_id           BIGINT;
  l_tr_type_list         BIGINT[];
  l_protected_db_methods TEXT [] := ARRAY [
  'MNT.ROUTES_INFO',
  'core.unit_bnst',
  'core.unit_bnsr',
  'core.unit_kbtob',
  'core.unit',
  'MNT.TRS_WORK',
  'core.trs_on_map',
  'snsr.skah_rep_01',
  'snsr.skah_rep_02',
  'snsr.skah_rep_03',
  'core.tr',
  'snsr.skah_rep_04',
  'kdbo.fuel_waste_report',
  'core.sensor',
  'core.sensor_asmpp',
  'core.sensor_dut',
  'core.modem',
  'core.monitor',
  'core.hdd',
  'core.sensor_other',
  'kdbo.equipment_status_report',
  'core.sim_card',
  'kdbo.availability_of_units_report',
  'kdbo.eqp_on_tr_report',
  'kdbo.reverse_drive_report',
  'kdbo.availability_of_vehicles_report',
  'core.repair_request',
  'core.breakdown',
  'kdbo.diagnostic_protocol_unit',
  'kdbo.diagnostic_protocol_sensor',
  'kdbo.diagnostic_list',
  'kdbo.tr_check_equipment',
  'kdbo.unit_working_period',
  'ttb.route_production_rounds_report',
  'voip.voice_call_request',
  'kdbo.unit_working_period',
  'core.sensor_other',
  'mnt.unit_session_log',
  'voip.voice_call_list',
  'mnt.bnsr_request_list',
  'ASD.RPT_SPEED_ON_ROUTE'
  ];
BEGIN
  l_rf_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
  l_tr_type_id   := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'F_tr_type_id', TRUE),jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id', TRUE));
  l_tr_type_list := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_tr_type_id', TRUE);

  IF l_rf_db_method IN ('snsr.skah_registry'
    , 'snsr.skah_registry_total'
    , 'snsr.skah_rep_01'
    , 'snsr.skah_rep_02'
    , 'snsr.skah_rep_03'
    , 'snsr.skah_rep_04')
  THEN l_tr_type_id = 2;
  END IF;

  OPEN p_rows FOR
  SELECT
    depo.tn_instance,
    depo.depo_id,
    depo.depo_id AS           entity_id,
    depo.carrier_id,
    depo.tr_type_id,
    tt.name      AS           tr_type_name,
    entity4depo.name_full,
    entity4depo.name_short,
    entity4depo.comment,
    entity4carrier.name_short carrier_name_short
  FROM core.depo
    JOIN core.entity entity4depo ON entity4depo.entity_id = depo.depo_id
    JOIN core.tr_type tt ON tt.tr_type_id = depo.tr_type_id
    LEFT JOIN core.carrier ON depo.carrier_id = carrier.carrier_id
    LEFT JOIN core.entity entity4carrier ON entity4carrier.entity_id = carrier.carrier_id
  WHERE (l_tr_type_id IS NULL OR depo.tr_type_id = l_tr_type_id)
        and (array_length(l_tr_type_list, 1) is null or depo.tr_type_id = any(l_tr_type_list))		
        AND (
          (l_rf_db_method IS NULL OR NOT (l_rf_db_method = ANY (l_protected_db_methods))) OR
          (l_rf_db_method = ANY (l_protected_db_methods) AND depo.depo_id in (select adm.account_data_realm_pkg$get_depos_by_all(p_id_account)))
        );
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION core.jf_depo_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
$BODY$
DECLARE
  l_r_return core.JF_DEPO_TYPE;
  l_r_depo   core.DEPO;
  l_r_entity core.ENTITY;
BEGIN
  l_r_return := core.jf_depo_pkg$attr_to_rowtype(p_attr);
  l_r_depo := l_r_return.r_depo;
  l_r_entity := l_r_return.r_entity;
  l_r_entity.entity_id := nextval('core.entity_entity_id_seq');
  l_r_depo.sys_period := tstzrange(now(), NULL :: TIMESTAMP WITH TIME ZONE);

  INSERT INTO core.entity SELECT l_r_entity.*
  RETURNING entity_id
    INTO l_r_depo.depo_id;
  INSERT INTO core.depo SELECT l_r_depo.*;

  RETURN NULL;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION core.jf_depo_pkg$of_update(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
$BODY$
DECLARE
  l_r_return core.JF_DEPO_TYPE;
  l_r_depo   core.DEPO;
  l_r_entity core.ENTITY;
BEGIN
  l_r_return := core.jf_depo_pkg$attr_to_rowtype(p_attr);
  l_r_depo := l_r_return.r_depo;
  l_r_entity := l_r_return.r_entity;

  UPDATE core.depo
  SET
    tn_instance = l_r_depo.tn_instance,
    carrier_id  = l_r_depo.carrier_id,
    tr_type_id  = l_r_depo.tr_type_id
  WHERE
    depo_id = l_r_depo.depo_id;

  UPDATE core.entity
  SET
    name_full  = l_r_entity.name_full,
    name_short = l_r_entity.name_short,
    comment    = l_r_entity.comment
  WHERE
    entity_id = l_r_entity.entity_id;

  RETURN NULL;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION core.jf_depo_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
$BODY$
DECLARE
  l_r_return core.JF_DEPO_TYPE;
  l_r_depo   core.DEPO;
  l_r_entity core.ENTITY;
BEGIN
  l_r_return := core.jf_depo_pkg$attr_to_rowtype(p_attr);
  l_r_depo := l_r_return.r_depo;
  l_r_entity := l_r_return.r_entity;

  DELETE FROM core.depo
  WHERE depo_id = l_r_depo.depo_id;

  DELETE FROM core.entity
  WHERE entity_id = l_r_entity.entity_id;

  RETURN NULL;

END;
$BODY$
LANGUAGE plpgsql VOLATILE;
------------------------------------------------------------------------------------------