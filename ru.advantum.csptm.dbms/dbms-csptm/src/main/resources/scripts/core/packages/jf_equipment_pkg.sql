﻿CREATE OR REPLACE FUNCTION core."jf_equipment_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.equipment AS
$BODY$ 
declare 
   l_r core.equipment%rowtype; 
begin 
   l_r.firmware_id := jofl.jofl_pkg$extract_number(p_attr, 'firmware_id', true); 
   l_r.unit_service_type_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_service_type_id', true); 
   l_r.facility_id := jofl.jofl_pkg$extract_number(p_attr, 'facility_id', true); 
   l_r.equipment_status_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_status_id', true); 
   l_r.dt_begin := jofl.jofl_pkg$extract_date(p_attr, 'dt_begin', true);
   l_r.equipment_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
   l_r.serial_num := jofl.jofl_pkg$extract_varchar(p_attr, 'serial_num', true);
   l_r.equipment_model_id := jofl.jofl_pkg$extract_varchar(p_attr, 'equipment_model_id', true);
   l_r.depo_id := jofl.jofl_pkg$extract_number(p_attr, 'depo_id', true);
   l_r.territory_id := jofl.jofl_pkg$extract_number(p_attr, 'territory_id', true);
   l_r.unit_service_facility_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_service_facility_id', true);
   l_r.has_diagnostic := jofl.jofl_pkg$extract_boolean(p_attr, 'has_diagnostic', true);
   l_r.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true);
   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_pkg$attr_to_rowtype"(text)
  OWNER TO adv;

  
--*********************************************************************************************************
CREATE OR REPLACE FUNCTION core."jf_equipment_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.equipment%rowtype;
begin 
   l_r := core.jf_equipment_pkg$attr_to_rowtype(p_attr);

   delete from  core.equipment where  equipment_id = l_r.equipment_id;

   return null;

  exception
  when 	foreign_key_violation then
    RAISE  EXCEPTION '<<Удаление невозможно! Запись используется в других документах системы>>';
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_pkg$of_delete"(numeric, text)
  OWNER TO adv;


--*********************************************************************************************************
CREATE OR REPLACE FUNCTION core."jf_equipment_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.equipment%rowtype;
begin 
   l_r := core.jf_equipment_pkg$attr_to_rowtype(p_attr);
   --проверка на уникальность . Не сделана на уровне базы по настойчивой просьбе аналитиков 22.08.17
   PERFORM  core.jf_equipment_pkg$of_check_unique(p_id_account,  p_attr);
   if coalesce(l_r.equipment_id,-1)<0 then
      l_r.equipment_id := nextval( 'core.equipment_equipment_id_seq' );
   end if;

   select em.has_diagnostic, equipment_type_id
   into l_r.has_diagnostic, l_r.equipment_type_id
   from core.equipment_model em
   where em.equipment_model_id = l_r.equipment_model_id;

   insert into core.equipment select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_pkg$of_insert"(numeric, text)
  OWNER TO adv;
--*********************************************************************************************************

CREATE OR REPLACE FUNCTION core."jf_equipment_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
 l_rf_db_method TEXT;
 l_unit_id bigint;
begin
 l_rf_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
  l_unit_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);

  open p_rows for
      select
        et.equipment_id,
        l_unit_id as unit_id,
        et.firmware_id, 
        fw.name as firmware_name,
        et.unit_service_type_id,
        et.serial_num,
        ust.name as unit_service_type_name,
        et.facility_id, 
        enm.name_short as facility_name,
        et.equipment_status_id, 
        ets.name as equipment_status_name, 
        dr.decommission_reason_id,
        dr.name as decommission_reason_name,
        et.dt_begin, 
        ed.dt_end,
        et.depo_id,
        et.territory_id,
        et.unit_service_facility_id,
        em.name as equipment_model_name,
        ett.name_short as equipment_type_name,
        entity4depo.name_full depo_name_full,
        entity4depo.name_short depo_name_short,
        et.has_diagnostic,
        et.comment
      from core.equipment et
      join core.equipment_status ets on ets.equipment_status_id = et.equipment_status_id
      join core.unit_service_type ust on ust.unit_service_type_id = et.unit_service_type_id
--       join core.entity ent on ent.entity_id= et.facility_id
      join core.equipment_model em on em.equipment_model_id = et.equipment_model_id
      join core.entity enm on enm.entity_id = em.facility_id /*#22108*/
      join core.equipment_type ett on ett.equipment_type_id = em.equipment_type_id
      join core.entity entity4depo on entity4depo.entity_id = et.depo_id
      left join core.firmware fw on fw.firmware_id = et.firmware_id
      left join core.equipment_decommission ed on et.equipment_id = ed.equipment_id
      left join core.decommission_reason dr on dr.decommission_reason_id = ed.decommission_reason_id
      where  l_rf_db_method != 'core.equipment2tr' or
             (l_rf_db_method is null and
              exists (select 1 from core.v_equipment2unit e2u where e2u.equipment_id=et.equipment_id and  unit_id = l_unit_id)
                ) or
             (not exists (select * from core.equipment2tr where equipment2tr.equipment_id = et.equipment_id)
              and ett.equipment_class_id = core.jf_equipment_class_pkg$cn_other()
              and et.equipment_status_id != core.jf_equipment_status_pkg$cn_status_decommissioned() --списан
             );
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_pkg$of_rows"(numeric, text)
  OWNER TO adv;

--*********************************************************************************************************
CREATE OR REPLACE FUNCTION core."jf_equipment_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.equipment%rowtype;
begin 
   l_r := core.jf_equipment_pkg$attr_to_rowtype(p_attr);
perform  core.jf_equipment_pkg$switch_depo( l_r.equipment_id, l_r.depo_id , l_r.territory_id);
   update core.equipment set
          firmware_id = l_r.firmware_id, 
          unit_service_type_id = l_r.unit_service_type_id, 
          facility_id = l_r.facility_id,
          serial_num = l_r.serial_num,
          equipment_status_id = l_r.equipment_status_id, 
          dt_begin = l_r.dt_begin,
          equipment_model_id =l_r.equipment_model_id,
          depo_id = l_r.depo_id,
          territory_id = l_r.territory_id,
          unit_service_facility_id = l_r.unit_service_facility_id,
          has_diagnostic = coalesce(l_r.has_diagnostic, has_diagnostic),
          comment = l_r.comment
   where 
          equipment_id = l_r.equipment_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_pkg$of_update"(numeric, text)
  OWNER TO adv;

CREATE OR REPLACE FUNCTION core."jf_equipment_pkg$switch_depo"(
  p_equipment_id bigint,
  p_depo_id bigint ,
  p_territory_id bigint)
returns text as
$BODY$
declare
l_equipment_id bigint;
l_depo_id bigint;
l_territory_id bigint;
begin
  l_equipment_id = p_equipment_id;
  l_depo_id = p_depo_id;
  l_territory_id = p_territory_id;
update core.equipment set
  depo_id = l_depo_id,
  territory_id = l_territory_id
where
  equipment_id  = l_equipment_id;
return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;


--******************************************************************************
CREATE OR REPLACE FUNCTION core.jf_equipment_pkg$get_tr_name(
  p_id_equipment numeric)
  RETURNS text AS
$BODY$
declare
  l_res text;
begin
  select into l_res tm.name || ' ' ||tr.licence
  from core.equipment2tr et2tr
    join core.tr tr on tr.tr_id = et2tr.tr_id
    join core.tr_model tm on tm.tr_model_id = tr.tr_model_id
  where et2tr.equipment_id = p_id_equipment;

  return l_res;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core.jf_equipment_pkg$get_tr_name(numeric)
OWNER TO adv;

--**********************************************************************************
CREATE OR REPLACE FUNCTION core.jf_equipment_pkg$get_equipment_name(
  p_id_equipment numeric)
  RETURNS text AS
$BODY$
declare
  l_res text;
begin

select into l_res  ett.name_short || ' '||  et.serial_num
  -- em.name as equipment_model_name,
from core.equipment et
  join core.equipment_model em on em.equipment_model_id = et.equipment_model_id
  join core.equipment_type ett on ett.equipment_type_id = em.equipment_type_id
where et.equipment_id = p_id_equipment;

return l_res;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core.jf_equipment_pkg$get_equipment_name(numeric)
OWNER TO adv;

--**********************************************************************************
--связать с ТС
CREATE OR REPLACE FUNCTION core."jf_equipment_pkg$of_link2tr"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_equipment_id BIGINT;
  l_equipment_type_name core.equipment_type.name_full%type;
  l_serial_num core.equipment.serial_num%type;

  l_tr_id BIGINT;
  l_res text;
begin
  l_equipment_id:=jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', TRUE);
  l_tr_id:= jofl.jofl_pkg$extract_number(p_attr, 'F_tr_id', TRUE);
  l_equipment_type_name:=  jofl.jofl_pkg$extract_varchar(p_attr, 'equipment_type_name', TRUE);
  l_serial_num:=  jofl.jofl_pkg$extract_varchar(p_attr, 'serial_num', TRUE);

  if exists(select 1 from core.equipment2tr where equipment_id = l_equipment_id) THEN
    RAISE  EXCEPTION '<<Оборудование % cерийный номер % уже установлено на ТС>>',l_equipment_type_name, l_serial_num ;
  end IF ;

  l_res:= '{"equipment_id":'||l_equipment_id::text||
          ',"tr_id":'||l_tr_id::text||'}';

  l_res:= core.jf_equipment2tr_pkg$of_insert(p_id_account, l_res);
  return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_equipment_pkg$of_link2tr"(numeric, text)
OWNER TO adv;

--*****************************************************************************
--списание
CREATE OR REPLACE FUNCTION core."jf_equipment_pkg$of_decommission"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
  $BODY$
  declare
    l_r core.equipment%rowtype;
    l_equipment_type_name core.equipment_type.name_full%type;
    l_r_decom core.equipment_decommission%rowtype;
    l_decommission_reason_id numeric;
    l_dt_end timestamp;
    l_res text;
  begin
    l_r := core.jf_equipment_pkg$attr_to_rowtype(p_attr);
    l_equipment_type_name:=  jofl.jofl_pkg$extract_varchar(p_attr, 'equipment_type_name', TRUE);
    l_decommission_reason_id:= jofl.jofl_pkg$extract_number(p_attr, 'F_decommission_reason_id', TRUE);

    l_dt_end:= jofl.jofl_pkg$extract_date(p_attr, 'END_DT', TRUE);

    l_r.equipment_status_id = core.jf_equipment_status_pkg$cn_status_decommissioned();

    if l_decommission_reason_id is null then
      RAISE EXCEPTION '<<Введите причину списания>>';
    end if;

    if l_dt_end is null then
      RAISE EXCEPTION '<<Введите дату списания>>';
    end if;

    if exists(select 1 from core.equipment2tr where equipment_id = l_r.equipment_id
           union all
              select 1 from core.unit2tr where unit_id = l_r.equipment_id
           union all
              select 1 from core.sensor2tr where sensor_id = l_r.equipment_id) THEN
      RAISE  EXCEPTION '<<Списание невозможно. Оборудование % cерийный номер %  установлено на ТС>>',l_equipment_type_name, l_r.serial_num ;
    end if ;

    l_r_decom.equipment_id:= l_r.equipment_id;
    l_r_decom.decommission_reason_id:=l_decommission_reason_id;

    l_res:= core.jf_equipment_decommission_pkg$of_insert(p_id_account, '{"equipment_id":'||l_r.equipment_id||
    ',"decommission_reason_id":'||l_decommission_reason_id||
    ',"dt_end":"'||l_dt_end::text||'"}');



      return core.jf_equipment_pkg$of_update(p_id_account, row_to_json(l_r)::text);


  end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_equipment_pkg$of_decommission"(numeric, text)
OWNER TO adv;

--***************************************************************************************************************

CREATE OR REPLACE FUNCTION core."jf_equipment_pkg$of_remove_from_unit"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_unit_id text;
  l_equipment_id text;
  l_res text;
  l_res1 text;
begin
  l_unit_id := jofl.jofl_pkg$extract_varchar(p_attr, 'unit_id', true);
  l_equipment_id:= jofl.jofl_pkg$extract_varchar(p_attr, 'equipment_id', true);

  l_res:= '{"equipment_id":'||l_unit_id||
          ',"sensor_id":'||l_equipment_id||'}';
  return core.jf_sensor2unit_pkg$of_delete(p_id_account, l_res);

  l_res:= '{"equipment_id":'||l_unit_id||
          ',"monitor_id":'||l_equipment_id||'}';
  return core.jf_monitor2unit_pkg$of_delete(p_id_account, l_res);

  l_res:= '{"equipment_id":'||l_unit_id||
          ',"modem_id":'||l_equipment_id||'}';
  return core.jf_modem2unit_pkg$of_delete(p_id_account, l_res);

  l_res:= '{"equipment_id":'||l_unit_id||
          ',"hdd_id":'||l_equipment_id||'}';
  return core.jf_hdd2unit_pkg$of_delete(p_id_account, l_res);

end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_equipment_pkg$of_remove_from_unit"(numeric, text)
OWNER TO adv;

--**********************************************************************************
--изменение статуса
CREATE OR REPLACE FUNCTION core."jf_equipment_pkg$of_change_status"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r core.equipment%rowtype;
  l_new_status core.equipment_status.equipment_status_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_new_equipment_status_id', true);
begin
  l_r := core.jf_equipment_pkg$attr_to_rowtype(p_attr);
  if l_r.equipment_status_id = core.jf_equipment_status_pkg$cn_status_decommissioned() then --списан
    RAISE  EXCEPTION '<<Статус не может быть изменен, т.к. датчик % выбыл (списан). Для работы с датчикам необходимо отменить выбытие (списание)>>',l_r.serial_num ;
  end if;
  l_r.equipment_status_id:=l_new_status;
  return core.jf_equipment_pkg$of_update(p_id_account,  row_to_json(l_r)::text);
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_equipment_pkg$of_change_status"(numeric, text)
OWNER TO adv;

--**********************************************************************************
--отмена выбытия
CREATE OR REPLACE FUNCTION core."jf_equipment_pkg$of_decommission_cancel"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r core.equipment%rowtype;
  l_new_status core.equipment_status.equipment_status_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_status_id', true);
begin
  l_r := core.jf_equipment_pkg$attr_to_rowtype(p_attr);
  if l_r.equipment_status_id != core.jf_equipment_status_pkg$cn_status_decommissioned() then --списан
    RAISE  EXCEPTION '<<Статус не может быть изменен, т.к. датчик % не выбыл (не списан). >>',l_r.serial_num ;
  end if;

  return core.jf_equipment_decommission_pkg$of_cancel(p_id_account, p_attr);
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_equipment_pkg$of_decommission_cancel"(numeric, text)
OWNER TO adv;

--**********************************************************************************
--проверка
CREATE OR REPLACE FUNCTION core."jf_equipment_pkg$of_check_unique"(
  p_id_account numeric,
  p_attr text)
  returns void as
  $BODY$
  declare
    l_r core.equipment%rowtype;
    l_cnt smallint;
  begin
    l_r := core.jf_equipment_pkg$attr_to_rowtype(p_attr);

    select into l_cnt count(1)
    from core.equipment t
    where t.equipment_model_id = l_r.equipment_model_id
      and t.serial_num = l_r.serial_num;

    if l_cnt>0 then
      RAISE  EXCEPTION '<<ОБорудование такой модели с серийным номером % уже существует. >>',l_r.serial_num ;
    end if;


  end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_equipment_pkg$of_check_unique"(numeric, text)
OWNER TO adv;

--**********************************************************************************
--включить диагностику
CREATE OR REPLACE FUNCTION core."jf_equipment_pkg$of_enable_diagnostic"(
  p_id_account numeric,
  p_attr text)
  returns text as
  $BODY$
  declare
    l_r core.equipment%rowtype;
  begin
    l_r := core.jf_equipment_pkg$attr_to_rowtype(p_attr);

    update core.equipment e
      set has_diagnostic = true
    where e.equipment_id = l_r.equipment_id;

    return null ;
  end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_equipment_pkg$of_enable_diagnostic"(numeric, text)
OWNER TO adv;
--**********************************************************************************
--отключить диагностику
CREATE OR REPLACE FUNCTION core."jf_equipment_pkg$of_disable_diagnostic"(
  p_id_account numeric,
  p_attr text)
  returns text as
  $BODY$
  declare
    l_r core.equipment%rowtype;
  begin
    l_r := core.jf_equipment_pkg$attr_to_rowtype(p_attr);

    update core.equipment e
      set has_diagnostic = false
    where e.equipment_id = l_r.equipment_id;

    return null ;
  end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_equipment_pkg$of_disable_diagnostic"(numeric, text)
OWNER TO adv;