﻿

-- Function: core."jf_contact_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION core."jf_contact_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_contact_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.contact AS
$BODY$
declare
  l_r core.contact%rowtype;
begin
  l_r.contact_desc := jofl.jofl_pkg$extract_varchar(p_attr, 'contact_desc', true);
  l_r.phone_num := jofl.jofl_pkg$extract_varchar(p_attr, 'phone_num', true);
  l_r.entity_id := jofl.jofl_pkg$extract_number(p_attr, 'entity_id', true);
  l_r.contact_id := jofl.jofl_pkg$extract_number(p_attr, 'contact_id', true);
  l_r.contact_type_id := jofl.jofl_pkg$extract_number(p_attr, 'contact_type_id', true);

  return l_r;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_contact_pkg$attr_to_rowtype"(text)
OWNER TO adv;
-------------
-- Function: core."jf_contact_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_contact_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_contact_pkg$of_delete"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r core.contact%rowtype;
begin
  l_r := core.jf_contact_pkg$attr_to_rowtype(p_attr);

  delete from  core.contact where  contact_id = l_r.contact_id;

  return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_contact_pkg$of_delete"(numeric, text)
OWNER TO adv;
-----
-- Function: core."jf_contact_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_contact_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_contact_pkg$of_insert"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r core.contact%rowtype;
begin
  l_r := core.jf_contact_pkg$attr_to_rowtype(p_attr);
  l_r.contact_id := nextval( 'core.contact_contact_id_seq' );

  insert into core.contact select l_r.*;

  return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_contact_pkg$of_insert"(numeric, text)
OWNER TO adv;
----
-- Function: core."jf_contact_pkg$of_rows"(numeric, text)

 --DROP FUNCTION core."jf_contact_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_contact_pkg$of_rows"(
  IN p_id_account numeric,
  OUT p_rows refcursor,
  IN p_attr text)
  RETURNS refcursor AS
$BODY$
declare
  l_entity_id bigint;
begin
  l_entity_id := jofl.jofl_pkg$extract_number(p_attr, 'entity_id', TRUE);
  open p_rows for
  select
    t.contact_desc,
    t.phone_num,
    t.entity_id,
    t.contact_id,
    t.contact_type_id,
    ct.name_full as  contact_type_name
  from core.contact t
  join core.contact_type ct on t.contact_type_id = ct.contact_type_id
  where entity_id = l_entity_id   ;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_contact_pkg$of_rows"(numeric, text)
OWNER TO adv;
----
-- Function: core."jf_contact_pkg$of_update"(numeric, text)

-- DROP FUNCTION core."jf_contact_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_contact_pkg$of_update"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r core.contact%rowtype;
begin
  l_r := core.jf_contact_pkg$attr_to_rowtype(p_attr);

  update core.contact set
    contact_desc = l_r.contact_desc,
    phone_num = l_r.phone_num,
    entity_id = l_r.entity_id,
    contact_type_id = l_r.contact_type_id
  where
    contact_id = l_r.contact_id;

  return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_contact_pkg$of_update"(numeric, text)
OWNER TO adv;
