﻿-- Function: core."jf_tr_schema_install_detail_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION core."jf_tr_schema_install_detail_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_tr_schema_install_detail_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.tr_schema_install_detail AS
$BODY$ 
declare 
   l_r core.tr_schema_install_detail%rowtype; 
begin 
   l_r.tr_schema_install_detail_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_schema_install_detail_id', true); 
   l_r.tr_schema_install_unit_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_schema_install_unit_id', true);
   l_r.sensor2unit_model_id := jofl.jofl_pkg$extract_number(p_attr, 'sensor2unit_model_id', true); 
   l_r.is_required := jofl.jofl_pkg$extract_boolean(p_attr, 'is_required', true);
   l_r.install_desc := jofl.jofl_pkg$extract_varchar(p_attr, 'install_desc', true);
   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_tr_schema_install_detail_pkg$attr_to_rowtype"(text)
  OWNER TO adv;
----------------------------------------------------
-- Function: core."jf_tr_schema_install_detail_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_tr_schema_install_detail_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_tr_schema_install_detail_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.tr_schema_install_detail%rowtype;
begin 
   l_r := core.jf_tr_schema_install_detail_pkg$attr_to_rowtype(p_attr);

   delete from  core.tr_schema_install_detail where  tr_schema_install_detail_id = l_r.tr_schema_install_detail_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_tr_schema_install_detail_pkg$of_delete"(numeric, text)
  OWNER TO adv;

----------------------------------------------------
-- Function: core."jf_tr_schema_install_detail_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_tr_schema_install_detail_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_tr_schema_install_detail_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.tr_schema_install_detail%rowtype;
begin 
   l_r := core.jf_tr_schema_install_detail_pkg$attr_to_rowtype(p_attr);
   l_r.tr_schema_install_detail_id := nextval('core.tr_schema_install_detail_tr_schema_install_detail_id_seq');

   insert into core.tr_schema_install_detail select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_tr_schema_install_detail_pkg$of_insert"(numeric, text)
  OWNER TO adv;


----------------------------------------------------
-- Function: core."jf_tr_schema_install_detail_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_tr_schema_install_detail_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_tr_schema_install_detail_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_r core.tr_schema_install_detail%rowtype;
begin
  l_r.tr_schema_install_unit_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_schema_install_unit_id', true);
 open p_rows for
     select
       tsid.tr_schema_install_detail_id,
       tsid.tr_schema_install_unit_id,
       tsid.sensor2unit_model_id,
       tsid.is_required,
       tsid.install_desc,
       s2um.num,
       et_unit.name_short as unit_type_name,
       et_sensor.name_short as sensor_type_name
     from core.tr_schema_install_detail tsid
       join core.sensor2unit_model s2um on s2um.sensor2unit_model_id  = tsid.sensor2unit_model_id
       join core.equipment_type et_unit on et_unit.equipment_type_id = s2um.equipment_type_unit_id
       join core.equipment_type et_sensor on et_sensor.equipment_type_id = s2um.equipment_type_sensor_id
     where tsid.tr_schema_install_unit_id = l_r.tr_schema_install_unit_id;

end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_tr_schema_install_detail_pkg$of_rows"(numeric, text)
  OWNER TO adv;

----------------------------------------------------
-- Function: core."jf_tr_schema_install_detail_pkg$of_update"(numeric, text)

-- DROP FUNCTION core."jf_tr_schema_install_detail_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_tr_schema_install_detail_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.tr_schema_install_detail%rowtype;
begin 
   l_r := core.jf_tr_schema_install_detail_pkg$attr_to_rowtype(p_attr);

   update core.tr_schema_install_detail set 
          is_required = l_r.is_required
   where 
          tr_schema_install_detail_id = l_r.tr_schema_install_detail_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_tr_schema_install_detail_pkg$of_update"(numeric, text)
  OWNER TO adv;

----------------------------------------------------