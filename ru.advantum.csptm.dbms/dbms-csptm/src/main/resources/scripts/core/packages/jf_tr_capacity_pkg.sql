-- Function: core."jf_tr_capacity_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION core."jf_tr_capacity_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_tr_capacity_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.tr_capacity AS
$BODY$ 
declare 
   l_r core.tr_capacity%rowtype; 
begin 
   l_r.full_name := jofl.jofl_pkg$extract_varchar(p_attr, 'full_name', true);
   l_r.short_name := jofl.jofl_pkg$extract_varchar(p_attr, 'short_name', true);
   l_r.tr_capacity_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_capacity_id', true);
   l_r.qty := jofl.jofl_pkg$extract_number(p_attr, 'qty', true);


   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_tr_capacity_pkg$attr_to_rowtype"(text)
  OWNER TO adv;
----------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION core."jf_tr_capacity_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.tr_capacity%rowtype;
begin 
   l_r := core.jf_tr_capacity_pkg$attr_to_rowtype(p_attr);

   delete from  core.tr_capacity where  TR_CAPACITY_ID = l_r.TR_CAPACITY_ID;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_tr_capacity_pkg$of_delete"(numeric, text)
  OWNER TO adv;
----------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION core."jf_tr_capacity_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.tr_capacity%rowtype;
begin
   l_r := core.jf_tr_capacity_pkg$attr_to_rowtype(p_attr);

   insert into core.tr_capacity select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_tr_capacity_pkg$of_insert"(numeric, text)
  OWNER TO adv;
----------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION core.jf_tr_capacity_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 lt_rf_db_method 		text 	:= coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true), 'xxx');
 ln_tt_variant_id   	bigint 	:= jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
 ln_mode_id				ttb.mode.mode_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tr_mode_id', true);
 ln_calculation_task_id integer := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true);
begin 
 open p_rows for
     select
        cp.tr_capacity_id
       ,cp.full_name
       ,cp.short_name
       ,cp.qty
       ,m2tc.mode2tr_capacity_id 
     from core.tr_capacity cp
     left join (select * -- данные из ttb.mode2tr_capacity нужны  только для соответствующих методов
                 from ttb.mode2tr_capacity
                where lt_rf_db_method     in ('ttb.tt_tr_group'
                                             ,'ttb.tt_tr_period')
                  and mode_id = ln_mode_id
                ) m2tc on m2tc.tr_capacity_id = cp.tr_capacity_id
     where (lt_rf_db_method = 'ttb.tt_change_time' 
     		and not exists (select null from ttb.tt_change_time tct 
            				where tct.tt_variant_id = ln_tt_variant_id 
                              and tct.tr_capacity_id = cp.tr_capacity_id))        
        or (lt_rf_db_method in ('ttb.tt_tr_group', 'ttb.tt_tr_period') and ln_mode_id is null)
        or (lt_rf_db_method in ('ttb.tt_tr_group', 'ttb.tt_tr_period') and exists (
														select null
          												  from ttb.mode2tr_capacity m2c
                                                          where m2c.mode_id = ln_mode_id) 
     											 and m2tc.mode_id = ln_mode_id)
        or (lt_rf_db_method in ('ttb.tt_tr_group', 'ttb.tt_tr_period') and not exists (
														select null
          												  from ttb.mode2tr_capacity m2c
                                                          where m2c.mode_id = ln_mode_id) 
     											 /*and m2tc.mode2tr_capacity_id is null*/)
        or (lt_rf_db_method = 'asd.ct_normative_result' and exists (
        					select null
                          from asd.ct_round_stop_result res
                          join asd.ct_normative norm on norm.calculation_task_id = res.calculation_task_id
                          join asd.calculation_task ct on ct.calculation_task_id = res.calculation_task_id
                          join core.tr_type tt on tt.tr_type_id= res.tr_type_id
                          join rts.round rnd on rnd.round_id =res.round_id and not rnd.sign_deleted
                          join ttb.action_type act on act.action_type_id = rnd.action_type_id
                          join rts.route_variant rv on rv.route_variant_id = rnd.route_variant_id and not rv.sign_deleted
                          join rts.route r on r.route_id = rv.route_id and not r.sign_deleted    
                          join core.tr_capacity cap on cap.tr_capacity_id = res.tr_capacity_id
                          						    and cap.tr_capacity_id = cp.tr_capacity_id
                          join rts.move_direction md on md.move_direction_id = rnd.move_direction_id 
                        where res.calculation_task_id = ln_calculation_task_id
                        and (select sum(vr.norm_duration) 
                        	 from asd.ct_stop_visit_result vr 
                             where vr.ct_round_stop_result_id = res.ct_round_stop_result_id) > 0
                             )
           )
        or (lt_rf_db_method in ('xxx'
        					   ,'ttb.mode2tr_capacity'
                               ,'ttb.rv_change_time'
                               ,'kdbo.availability_of_vehicles_report'
                               ,'core.tr'
                               ,'asd.tr_capacity2ct'
                               ,'ttb.spr_change_time'
                               ,'core.tr_schema_install'
                               )
           )
        ;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION core."jf_tr_capacity_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.tr_capacity%rowtype;
begin 
   l_r := core.jf_tr_capacity_pkg$attr_to_rowtype(p_attr);

   update core.tr_capacity set
          full_name = l_r.full_name,
          short_name  = l_r.short_name,
          QTY = l_r.QTY
   where 
          TR_CAPACITY_ID = l_r.TR_CAPACITY_ID;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_tr_capacity_pkg$of_update"(numeric, text)
  OWNER TO adv;
