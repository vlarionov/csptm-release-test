﻿-- Function: core."jf_mobile_tariff_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION core."jf_mobile_tariff_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_mobile_tariff_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.mobile_tariff AS
$BODY$ 
declare 
   l_r core.mobile_tariff%rowtype; 
begin 
   l_r.mobile_tariff_id := jofl.jofl_pkg$extract_number(p_attr, 'mobile_tariff_id', true); 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 
   l_r.is_active := jofl.jofl_pkg$extract_boolean(p_attr, 'is_active', true); 
   l_r.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true); 
   l_r.facility_id := jofl.jofl_pkg$extract_number(p_attr, 'facility_id', true); 

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_mobile_tariff_pkg$attr_to_rowtype"(text)
  OWNER TO adv;
--*************************************************************************************************
-- Function: core."jf_sensor2unit_model_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_sensor2unit_model_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sensor2unit_model_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.sensor2unit_model%rowtype;
begin 
   l_r := core.jf_sensor2unit_model_pkg$attr_to_rowtype(p_attr);

   delete from  core.sensor2unit_model where  sensor2unit_model_id = l_r.sensor2unit_model_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sensor2unit_model_pkg$of_delete"(numeric, text)
  OWNER TO adv;


--*************************************************************************************************
-- Function: core."jf_mobile_tariff_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_mobile_tariff_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_mobile_tariff_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.mobile_tariff%rowtype;
begin 
   l_r := core.jf_mobile_tariff_pkg$attr_to_rowtype(p_attr);
   l_r.mobile_tariff_id := nextval( 'core.mobile_tariff_mobile_tariff_id_seq' );
   insert into core.mobile_tariff select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_mobile_tariff_pkg$of_insert"(numeric, text)
  OWNER TO adv;

--*************************************************************************************************
-- Function: core."jf_mobile_tariff_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_mobile_tariff_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_mobile_tariff_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
begin 
 open p_rows for 
      select 
        mt.mobile_tariff_id, 
        mt.name, 
        mt.is_active, 
        mt.comment, 
        mt.facility_id,
        ent.name_short as facility_name
      from core.mobile_tariff mt
      join core.entity ent on ent.entity_id = mt.facility_id; 
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_mobile_tariff_pkg$of_rows"(numeric, text)
  OWNER TO adv;

--*************************************************************************************************
-- Function: core."jf_mobile_tariff_pkg$of_update"(numeric, text)

-- DROP FUNCTION core."jf_mobile_tariff_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_mobile_tariff_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.mobile_tariff%rowtype;
begin 
   l_r := core.jf_mobile_tariff_pkg$attr_to_rowtype(p_attr);

   update core.mobile_tariff set 
          name = l_r.name, 
          is_active = l_r.is_active, 
          comment = l_r.comment, 
          facility_id = l_r.facility_id
   where 
          mobile_tariff_id = l_r.mobile_tariff_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_mobile_tariff_pkg$of_update"(numeric, text)
  OWNER TO adv;

--*************************************************************************************************