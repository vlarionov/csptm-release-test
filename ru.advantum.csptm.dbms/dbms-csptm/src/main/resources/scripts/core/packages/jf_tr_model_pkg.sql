CREATE OR REPLACE FUNCTION core.jf_tr_model_pkg$attr_to_rowtype(p_attr TEXT)
  RETURNS core.tr_model
AS
  $BODY$
  DECLARE
    l_r core.tr_model%ROWTYPE;
  BEGIN
    l_r.tr_model_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_model_id', TRUE);
    l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', TRUE);
    l_r.tr_mark_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_mark_id', TRUE);
    l_r.tr_capacity_id  := jofl.jofl_pkg$extract_number(p_attr, 'tr_capacity_id', TRUE);
    l_r.seat_qty := jofl.jofl_pkg$extract_number(p_attr, 'seat_qty', TRUE);
    l_r.seat_qty_total := jofl.jofl_pkg$extract_number(p_attr, 'seat_qty_total', TRUE);
    l_r.seat_qty_disabled_people := jofl.jofl_pkg$extract_number(p_attr, 'seat_qty_disabled_people', TRUE);
    l_r.has_low_floor := jofl.jofl_pkg$extract_boolean(p_attr, 'has_low_floor', TRUE);
    l_r.has_conditioner := jofl.jofl_pkg$extract_boolean(p_attr, 'has_conditioner', TRUE);
    l_r.has_wheelchair_space := jofl.jofl_pkg$extract_boolean(p_attr, 'has_wheelchair_space', TRUE);
    l_r.has_bicicle_space := jofl.jofl_pkg$extract_boolean(p_attr, 'has_bicicle_space', TRUE);
    l_r.door_count := jofl.jofl_pkg$extract_number(p_attr, 'door_count', TRUE);
    RETURN l_r;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_tr_model_pkg$of_rows(IN p_id_account NUMERIC, OUT p_rows REFCURSOR, IN p_attr TEXT)
  RETURNS REFCURSOR
AS
  $BODY$
  DECLARE
  BEGIN
    OPEN p_rows FOR
    SELECT
      md.tr_model_id,
      md.name,
      md.tr_mark_id,
      md.tr_capacity_id,
      cp.short_name as tr_capacity_name,
      md.has_low_floor,
      md.has_conditioner,
      md.has_wheelchair_space,
      md.has_bicicle_space,
      md.seat_qty,
      md.seat_qty_disabled_people,
      md.seat_qty_total,
      mk.name tr_mark_name,
      md.door_count
    FROM core.tr_model md
    JOIN core.tr_mark mk ON mk.tr_mark_id = md.tr_mark_id
    JOIN core.tr_capacity cp on cp.tr_capacity_id = md.tr_capacity_id;

  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION core.jf_tr_model_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
  $BODY$
  DECLARE
    l_r core.tr_model%ROWTYPE;
  BEGIN
    l_r := core.jf_tr_model_pkg$attr_to_rowtype(p_attr);

    DELETE FROM core.tr_model
    WHERE tr_model_id = l_r.tr_model_id;

    RETURN NULL;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_tr_model_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
  $BODY$
  DECLARE
    l_r core.tr_model%ROWTYPE;
  BEGIN
    l_r := core.jf_tr_model_pkg$attr_to_rowtype(p_attr);
    l_r.tr_model_id := nextval('core.tr_model_tr_model_id_seq');
    INSERT INTO core.tr_model SELECT l_r.*;

    RETURN NULL;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_tr_model_pkg$of_update(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
  $BODY$
  DECLARE
    l_r core.tr_model%ROWTYPE;
  BEGIN
    l_r := core.jf_tr_model_pkg$attr_to_rowtype(p_attr);

    UPDATE core.tr_model
    SET
      name       = l_r.name,
      tr_mark_id = l_r.tr_mark_id,
      tr_capacity_id  =l_r.tr_capacity_id,
      seat_qty        = l_r.seat_qty,
      seat_qty_total    = l_r.seat_qty_total,
      seat_qty_disabled_people = l_r.seat_qty_disabled_people,
      has_low_floor   = l_r.has_low_floor,
      has_conditioner = l_r.has_conditioner,
      has_wheelchair_space  = l_r.has_wheelchair_space,
      has_bicicle_space  = l_r.has_bicicle_space,
      door_count = l_r.door_count
    WHERE
      tr_model_id = l_r.tr_model_id;

    RETURN NULL;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;
