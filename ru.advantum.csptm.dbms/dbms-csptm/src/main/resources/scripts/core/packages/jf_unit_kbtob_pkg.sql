﻿
--DROP TYPE core.jf_unit_kbtob_type CASCADE;

do $$
begin
  if not exists(select * from pg_type where typname = 'jf_unit_kbtob_type') THEN

    CREATE TYPE core.jf_unit_kbtob_type AS
      (r_unit core.unit,
       r_equipment core.equipment,
       r_kbtob core.unit_kbtob);

    ALTER TYPE core.jf_unit_kbtob_type
      OWNER TO adv;

  end if;
end$$;

--*****************************************************************************

-- Function: core."jf_unit_kbtob_pkg$attr_to_rowtype"(text)

--DROP FUNCTION core."jf_unit_kbtob_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_unit_kbtob_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.jf_unit_kbtob_type AS
$BODY$ 
declare
   l_r_return core.jf_unit_kbtob_type;
   l_r_unit core.unit%rowtype;
   l_r_equipment core.equipment%rowtype;
   l_r_kbtob core.unit_kbtob%rowtype;
begin
  l_r_unit.unit_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
  l_r_unit.unit_num := jofl.jofl_pkg$extract_varchar(p_attr, 'unit_num', true);
  l_r_unit.hub_id := jofl.jofl_pkg$extract_number(p_attr, 'hub_id', true);
  l_r_unit.guarantee_end := jofl.jofl_pkg$extract_date(p_attr, 'guarantee_end', true);

  l_r_kbtob.port_qty := jofl.jofl_pkg$extract_number(p_attr, 'port_qty', true);
  l_r_kbtob.channel_qty := jofl.jofl_pkg$extract_number(p_attr, 'channel_qty', true);
  l_r_kbtob.unit_kbtob_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);

  l_r_equipment.equipment_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
  l_r_equipment.serial_num := jofl.jofl_pkg$extract_varchar(p_attr, 'serial_num', true);
  l_r_equipment.firmware_id := jofl.jofl_pkg$extract_number(p_attr, 'firmware_id', true);
  l_r_equipment.unit_service_type_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_service_type_id', true);
  l_r_equipment.facility_id := jofl.jofl_pkg$extract_number(p_attr, 'facility_id', true);
  l_r_equipment.equipment_status_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_status_id', true);
  l_r_equipment.dt_begin := jofl.jofl_pkg$extract_date(p_attr, 'dt_begin', true);
  l_r_equipment.equipment_model_id := jofl.jofl_pkg$extract_varchar(p_attr, 'equipment_model_id', true);
  l_r_equipment.depo_id := jofl.jofl_pkg$extract_number(p_attr, 'depo_id', true);
  l_r_equipment.territory_id := jofl.jofl_pkg$extract_number(p_attr, 'territory_id', true);
  l_r_equipment.unit_service_facility_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_service_facility_id', true);
  l_r_equipment.has_diagnostic := jofl.jofl_pkg$extract_boolean(p_attr, 'has_diagnostic', true);
  l_r_equipment.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true);

  l_r_return.r_unit := l_r_unit;
  l_r_return.r_equipment := l_r_equipment;
  l_r_return.r_kbtob := l_r_kbtob;
  return l_r_return;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_unit_kbtob_pkg$attr_to_rowtype"(text)
  OWNER TO adv;
--**************************************************************************************************************
-- Function: core."jf_unit_kbtob_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_unit_kbtob_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core.jf_unit_kbtob_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_limit 			int 	:= coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'f_rows_NUMBER', TRUE), '3000');
  l_unit_num 		text 	:= jofl.jofl_pkg$extract_varchar(p_attr, 'f_unit_num_TEXT', TRUE);  
  l_hub_id 			text 	:= jofl.jofl_pkg$extract_varchar(p_attr, 'f_hub_id', TRUE);
  
  l_serial_num 		text 	:= jofl.jofl_pkg$extract_varchar(p_attr, 'serial_num', TRUE);
  l_depo_id	 		core.depo.depo_id%type 		:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', true),jofl.jofl_pkg$extract_number(p_attr, 'depo_id', true));
  l_equipment_model_id 		core.equipment_model.equipment_model_id%type		:= jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_model_id', true);
  l_unit_service_type_id 	core.unit_service_type.unit_service_type_id%type 	:= jofl.jofl_pkg$extract_number(p_attr, 'f_unit_service_type_id', true);
  l_arr_org_facility_id 	bigint[] 	:= jofl.jofl_pkg$extract_narray(p_attr, 'f_list_org_facility_id', true);
  l_facility_id 	core.facility.facility_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_facility_id', true);
  l_firmware_id 	core.firmware.firmware_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_firmware_id', true);
  l_equipment_status_list   smallint[]  := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_equipment_status_id', true);
  l_depo_list 		bigint[] := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_depo_id', true);

  l_b_ins_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_insert_from_DT', true);
  l_e_ins_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_insert_to_DT', true);

  l_link_tr			smallint	:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_link_tr_INPLACE_K', true), -1);
begin
  open p_rows for
      with adata as (
      select
        et."ROW$POLICY",
        et.equipment_id,
        et.firmware_id,
        et.serial_num,
        et.firmware_name,
        et.unit_service_type_id,
        et.unit_service_type_name,
        et.facility_id,
        et.facility_name,
        et.equipment_status_id,
        et.equipment_status_name,
        et.dt_begin,
        u.guarantee_end,
        et.equipment_model_id,
        et.equipment_model_name,
        et.equipment_type_id,
        et.equipment_type_name,
        et.depo_name_full,
        et.depo_name_short,
        et.depo_id,
        et.territory_id,
        et.territory_name_full,
        et.territory_name_short,
        et.unit_service_facility_id,
        et.unit_service_facility_name_short,
        et.has_diagnostic,
        et.comment,
        u.unit_num,
        u.hub_id,
        hub.name       as hub_name,
        ukbtob.port_qty,
        ukbtob.channel_qty,
        (  select  tr.garage_num  from core.unit2tr u2tr join core.tr tr on tr.tr_id = u2tr.tr_id
        where u2tr.unit_id = u.unit_id) as tr_name
      from core.unit_kbtob ukbtob
        join core.unit u on u.unit_id = ukbtob.unit_kbtob_id
        join core.v_equipment et on et.equipment_id = u.unit_id
        left join core.hub hub on hub.hub_id = u.hub_id
      where et.equipment_status_id != core.jf_equipment_status_pkg$cn_status_decommissioned()
        --подключаем фильтры
        and (et.depo_id =l_depo_id or et.depo_id = any(l_depo_list))
        and (l_unit_num is null or u.unit_num like '%'||l_unit_num||'%')
        and (l_hub_id is null or l_hub_id = '' or u.hub_id = l_hub_id::int)
        and (l_equipment_model_id is null or et.equipment_model_id = l_equipment_model_id)
        and (l_unit_service_type_id is null or et.unit_service_type_id = l_unit_service_type_id)
        and (l_facility_id is null or et.facility_id = l_facility_id)
        and (et.unit_service_facility_id = any(l_arr_org_facility_id) or array_length(l_arr_org_facility_id, 1) is null )
        and (l_firmware_id is null or et.firmware_id =l_firmware_id )
        and (et.equipment_status_id = any(l_equipment_status_list) or (array_length(l_equipment_status_list, 1)  is null))
        and (l_serial_num is null or et.serial_num =l_serial_num )
          
        and (l_b_ins_dt is null or et.dt_begin >= l_b_ins_dt)
        and (l_e_ins_dt is null or et.dt_begin<= l_e_ins_dt)
      )

      select *
      from adata
      where  (l_link_tr =-1 or coalesce(sign(tr_name),0) = l_link_tr )
      ;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

--**************************************************************************************************************
-- Function: core."jf_unit_kbtob_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_unit_kbtob_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_unit_kbtob_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare
  l_r_return core.jf_unit_kbtob_type;
  l_r_unit core.unit%rowtype;
  l_r_equipment core.equipment%rowtype;
  l_r_kbtob core.unit_kbtob%rowtype;
  l_res text;
begin
  l_r_return := core.jf_unit_kbtob_pkg$attr_to_rowtype(p_attr);

  l_r_kbtob:= l_r_return.r_kbtob;
  l_r_unit:= l_r_return.r_unit;
  l_r_equipment := l_r_return.r_equipment;

  delete from  core.unit_kbtob where  unit_kbtob.unit_kbtob_id = l_r_kbtob.unit_kbtob_id;
  l_res :=  core.jf_unit_pkg$of_delete(p_id_account,  (row_to_json(l_r_unit)::jsonb||row_to_json(l_r_equipment)::jsonb)::text);


  return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_unit_kbtob_pkg$of_delete"(numeric, text)
  OWNER TO adv;


--**************************************************************************************************************
-- Function: core."jf_unit_kbtob_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_unit_kbtob_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core.jf_unit_kbtob_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
  l_r_return core.jf_unit_kbtob_type;
  l_r_unit core.unit%rowtype;
  l_r_equipment core.equipment%rowtype;
  l_r_kbtob core.unit_kbtob%rowtype;
  l_res text;
begin
  l_r_return := core.jf_unit_kbtob_pkg$attr_to_rowtype(p_attr);

  l_r_kbtob:= l_r_return.r_kbtob;
  l_r_unit:= l_r_return.r_unit;
  l_r_equipment := l_r_return.r_equipment;
  
  if l_r_equipment.unit_service_type_id = core.jf_unit_service_type_pkg$cn_no_service() then
  	 l_r_equipment.unit_service_facility_id = null;
  end if;

  l_r_kbtob.unit_kbtob_id:= nextval( 'core.equipment_equipment_id_seq');
  l_r_unit.unit_id:= l_r_kbtob.unit_kbtob_id;
  l_r_equipment.equipment_id:= l_r_kbtob.unit_kbtob_id;

  l_res := core.jf_unit_pkg$of_insert(p_id_account,  (row_to_json(l_r_unit)::jsonb||row_to_json(l_r_equipment)::jsonb)::text);
  insert into core.unit_kbtob select l_r_kbtob.*;


   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;



--**************************************************************************************************************
-- Function: core."jf_unit_kbtob_pkg$of_update"(numeric, text)

-- DROP FUNCTION core."jf_unit_kbtob_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core.jf_unit_kbtob_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
  l_r_return core.jf_unit_kbtob_type;
  l_r_unit core.unit%rowtype;
  l_r_equipment core.equipment%rowtype;
  l_r_kbtob core.unit_kbtob%rowtype;
  l_res text;
begin
  l_r_return := core.jf_unit_kbtob_pkg$attr_to_rowtype(p_attr);

  l_r_kbtob:= l_r_return.r_kbtob;
  l_r_unit:= l_r_return.r_unit;
  l_r_equipment := l_r_return.r_equipment;
  
  if l_r_equipment.unit_service_type_id = core.jf_unit_service_type_pkg$cn_no_service() then
  	 l_r_equipment.unit_service_facility_id = null;
  end if;

  l_res:= core.jf_unit_pkg$of_update(p_id_account, (row_to_json(l_r_unit)::jsonb||row_to_json(l_r_equipment)::jsonb)::text);

  update core.unit_kbtob
  set channel_qty = l_r_kbtob.channel_qty,
      port_qty = l_r_kbtob.port_qty
  where unit_kbtob_id = l_r_kbtob.unit_kbtob_id;

  return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--****************************************************************************************************************

CREATE OR REPLACE FUNCTION core."jf_unit_kbtob_pkg$of_link2tr"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
begin
  return core.jf_unit_pkg$of_link2tr(p_id_account, p_attr);
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_unit_kbtob_pkg$of_link2tr"(numeric, text)
OWNER TO adv;
--*******************************************************************************************************
CREATE OR REPLACE FUNCTION core."jf_unit_kbtob_pkg$of_decommission"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
begin
  return core.jf_equipment_pkg$of_decommission(p_id_account, p_attr);
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_unit_kbtob_pkg$of_decommission"(numeric, text)
OWNER TO adv;

--**************************************************************************************************************
--изменение статуса
CREATE OR REPLACE FUNCTION core."jf_unit_kbtob_pkg$of_change_status"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
begin
  return core.jf_equipment_pkg$of_change_status(p_id_account, p_attr);
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_unit_kbtob_pkg$of_change_status"(numeric, text)
OWNER TO adv;

--****************************************************************************************
--отмена списания
CREATE OR REPLACE FUNCTION core."jf_unit_kbtob_pkg$of_decommission_cancel"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
begin
  return core.jf_equipment_pkg$of_decommission_cancel(p_id_account, p_attr);
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_unit_kbtob_pkg$of_decommission_cancel"(numeric, text)
OWNER TO adv;
