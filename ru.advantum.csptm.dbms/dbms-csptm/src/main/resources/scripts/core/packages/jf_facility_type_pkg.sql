﻿
CREATE OR REPLACE FUNCTION core."jf_facility_type_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.facility_type AS
$BODY$ 
declare 
   l_r core.facility_type%rowtype; 
begin 
   l_r.facility_type_id := jofl.jofl_pkg$extract_number(p_attr, 'facility_type_id', true); 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_facility_type_pkg$attr_to_rowtype"(text)
  OWNER TO adv;

--*************************************************************************************************
CREATE OR REPLACE FUNCTION core."jf_facility_type_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.facility_type%rowtype;
begin 
   l_r := core.jf_facility_type_pkg$attr_to_rowtype(p_attr);

   delete from  core.facility_type where  facility_type_id = l_r.facility_type_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_facility_type_pkg$of_delete"(numeric, text)
  OWNER TO adv;

--*************************************************************************************************

CREATE OR REPLACE FUNCTION core."jf_facility_type_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.facility_type%rowtype;
begin 
   l_r := core.jf_facility_type_pkg$attr_to_rowtype(p_attr);
   l_r.facility_type_id := nextval( 'core.facility_type_facility_type_id_seq' );

   insert into core.facility_type select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_facility_type_pkg$of_insert"(numeric, text)
  OWNER TO adv;


--*************************************************************************************************
CREATE OR REPLACE FUNCTION core.jf_facility_type_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_rf_db_method      TEXT := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
  l_r core.facility2type%rowtype;
begin
  l_r.facility_id := jofl.jofl_pkg$extract_number(p_attr, 'facility_id', true);

  open p_rows for
      select
        ft.facility_type_id,
        ft.name
      from core.facility_type ft
      where (l_rf_db_method IS NULL)
         or ((l_rf_db_method = 'core.facility2type') 
              and not exists (select 1 from core.facility2type f2t 
              				  where f2t.facility_type_id = ft.facility_type_id
                              and f2t.facility_id =  l_r.facility_id))
         or (l_rf_db_method = 'core.facility');
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;


--*************************************************************************************************
CREATE OR REPLACE FUNCTION core."jf_facility_type_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.facility_type%rowtype;
begin 
   l_r := core.jf_facility_type_pkg$attr_to_rowtype(p_attr);

   update core.facility_type set 
          name = l_r.name
   where 
          facility_type_id = l_r.facility_type_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_facility_type_pkg$of_update"(numeric, text)
  OWNER TO adv;

--*************************************************************************************************

CREATE OR REPLACE FUNCTION core.jf_facility_type_pkg$cn_producer_bo()
  RETURNS INTEGER AS
'select 1;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION core.jf_facility_type_pkg$cn_mobile_operator()
  RETURNS INTEGER AS
'select 2;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION core.jf_facility_type_pkg$cn_contractor()
  RETURNS INTEGER AS
'select 3;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION core.jf_facility_type_pkg$cn_producer_tr()
  RETURNS INTEGER AS
'select 4;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION core.jf_facility_type_pkg$cn_producer_firmware()
  RETURNS INTEGER AS
'select 5;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION core.jf_facility_type_pkg$cn_service_company()
  RETURNS INTEGER AS
'select 7;'
LANGUAGE SQL IMMUTABLE;





