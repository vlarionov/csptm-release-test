CREATE OR REPLACE FUNCTION core.jf_unit_filter_pkg$of_rows(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$
declare
begin
  open p_rows for
    select
      u.unit_id as equipment_id,
      u.unit_num,
      entity4depo.name_full depo_name_full
    from core.unit u
      join core.equipment et on et.equipment_id= u.unit_id
      join core.entity ent on ent.entity_id= et.facility_id
      join core.entity entity4depo on entity4depo.entity_id = et.depo_id;


end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_unit_pkg$of_rows"(numeric, text)
  OWNER TO adv;