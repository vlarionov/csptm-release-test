CREATE OR REPLACE FUNCTION core.jf_breakdown_status_pkg$attr_to_rowtype(p_attr text)
  RETURNS core.breakdown_status
AS
  $BODY$
  declare
   l_r core.breakdown_status%rowtype;
begin
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true);
   l_r.breakdown_status_id := jofl.jofl_pkg$extract_number(p_attr, 'breakdown_status_id', true);

   return l_r;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_breakdown_status_pkg$of_rows(in p_id_account numeric, out p_rows refcursor, in p_attr text)
  RETURNS refcursor
AS
  $BODY$
  declare
begin
 open p_rows for
      select
        name,
        breakdown_status_id
      from core.breakdown_status;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_breakdown_status_pkg$of_insert(p_id_account numeric, p_attr text)
  RETURNS text
AS
  $BODY$
  declare
   l_r core.breakdown_status%rowtype;
begin
   l_r := core.jf_breakdown_status_pkg$attr_to_rowtype(p_attr);
   l_r.breakdown_status_id := nextval('core.breakdown_status_breakdown_status_id_seq'::regclass);

   insert into core.breakdown_status select l_r.*;

   return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION core.jf_breakdown_status_pkg$of_update(p_id_account numeric, p_attr text)
  RETURNS text
AS
  $BODY$
  declare
   l_r core.breakdown_status%rowtype;
begin
   l_r := core.jf_breakdown_status_pkg$attr_to_rowtype(p_attr);

   update core.breakdown_status set
          name = l_r.name
   where
          breakdown_status_id = l_r.breakdown_status_id;

   return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_breakdown_status_pkg$of_delete(p_id_account numeric, p_attr text)
  RETURNS text
AS
  $BODY$
  declare
   l_r core.breakdown_status%rowtype;
begin
   l_r := core.jf_breakdown_status_pkg$attr_to_rowtype(p_attr);

   delete from  core.breakdown_status where  breakdown_status_id = l_r.breakdown_status_id;

   return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;
