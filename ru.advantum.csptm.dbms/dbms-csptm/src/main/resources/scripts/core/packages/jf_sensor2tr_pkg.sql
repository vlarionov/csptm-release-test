﻿-- Function: core."jf_sensor2tr_pkg$attr_to_rowtype"(text)

 DROP FUNCTION core."jf_sensor2tr_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_sensor2tr_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.sensor2tr AS
$BODY$ 
declare 
   l_r core.sensor2tr%rowtype;
begin 
   l_r.sensor_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
   l_r.installation_site_id := jofl.jofl_pkg$extract_number(p_attr, 'installation_site_id', true);
   l_r.tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sensor2tr_pkg$attr_to_rowtype"(text)
  OWNER TO adv;

--********************************************************************************************************
-- Function: core."jf_sensor2tr_pkg$of_rows"(numeric, text)

 DROP FUNCTION core."jf_sensor2tr_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sensor2tr_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_rf_db_method text:= jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
  l_tr_id core.tr.tr_id%type := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true), jofl.jofl_pkg$extract_number(p_attr, 'f_tr_id', true));
begin

 open p_rows for

 select
   s2tr.sensor_id as equipment_id,
   s2tr.tr_id,
   s2tr.installation_site_id,
   et.serial_num,
   et.firmware_id,
   et.firmware_name,
   et.unit_service_type_id,
   et.unit_service_type_name,
   et.facility_id,
   et.facility_name,
   et.equipment_status_id,
   et.equipment_status_name,
   et.decommission_reason_id,
   et.decommission_reason_name,
   et.dt_begin,
   et.dt_end,
   et.equipment_model_id,
   et.equipment_model_name,
   et.equipment_type_name,
   et.equipment_type_id,
   ist.name as installation_site_name,
   et.depo_name_full,
   et.depo_name_short,
   (select ve.equipment_type_name||' '||ve.serial_num
    from core.v_equipment ve where ve.equipment_id = s2u.unit_id) as sensor2unit
 from core.sensor2tr s2tr
   join core.sensor s on s.sensor_id = s2tr.sensor_id
   join core.v_equipment et on et.equipment_id = s2tr.sensor_id
   left join core.installation_site ist on  ist.installation_site_id = s2tr.installation_site_id
   join core.entity entity4depo on entity4depo.entity_id = et.depo_id
   left join core.sensor2unit s2u on s2u.sensor_id = s.sensor_id
 where s2tr.tr_id = l_tr_id;

end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sensor2tr_pkg$of_rows"(numeric, text)
  OWNER TO adv;

--********************************************************************************************************
-- Function: core."jf_sensor2tr_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_sensor2tr_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sensor2tr_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.sensor2tr%rowtype;
begin 
   l_r := core.jf_sensor2tr_pkg$attr_to_rowtype(p_attr);

   delete from  core.sensor2tr where  sensor_id = l_r.sensor_id and tr_id = l_r.tr_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sensor2tr_pkg$of_delete"(numeric, text)
  OWNER TO adv;

--********************************************************************************************************
-- Function: core."jf_sensor2tr_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_sensor2tr_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sensor2tr_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.sensor2tr%rowtype;
   l_r_h core.equipment2tr_h%rowtype;
   l_res text;
begin 
   l_r := core.jf_sensor2tr_pkg$attr_to_rowtype(p_attr);
   insert into core.sensor2tr select l_r.*;

  l_r_h.sys_period := tsrange(localtimestamp, null);
  l_r_h.equipment_id := l_r.sensor_id;
  l_r_h.installation_site_id := l_r.installation_site_id;
  l_r_h.tr_id:= l_r.tr_id;
  l_res:= core.jf_equipment2tr_h_pkg$of_insert(p_id_account, row_to_json(l_r_h)::text);

  return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sensor2tr_pkg$of_insert"(numeric, text)
  OWNER TO adv;

--********************************************************************************************************
CREATE OR REPLACE FUNCTION core."jf_sensor2tr_pkg$of_update"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r core.sensor2tr%rowtype;
begin

  l_r := core.jf_sensor2tr_pkg$attr_to_rowtype(p_attr);

  update core.sensor2tr set
    installation_site_id = l_r.installation_site_id
  where
    sensor_id =  l_r.sensor_id
    and tr_id =  l_r.tr_id ;

  return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_sensor_pkg$of_update"(numeric, text)
OWNER TO adv;
--********************************************************************************************************

CREATE OR REPLACE FUNCTION core."jf_sensor2tr_pkg$of_remove_from_tr"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r core.equipment2tr%rowtype;
  l_r_h core.equipment2tr_h%rowtype;
  l_r_equipment core.equipment%rowtype;
  l_res text;
begin
  --сохранить в истории
  l_r := core.jf_sensor2tr_pkg$attr_to_rowtype(p_attr);

  select into l_r_h.equipment2tr_h_id  h.equipment2tr_h_id
  FROM core.equipment2tr_h h
  where h.equipment_id = l_r.equipment_id
    and h.tr_id = l_r.tr_id
        and upper(h.sys_period) is null;

  if l_r_h.equipment2tr_h_id is not null then
     l_r_h.sys_period := tsrange(lower(l_r_h.sys_period),localtimestamp);
     l_r_h.equipment_remove_reason_id:= jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_remove_reason_id', true);

     l_res:= core.jf_equipment2tr_h_pkg$of_update(p_id_account, row_to_json(l_r_h)::text);
  end if;

  --обновляем статус оборудования

  /*l_r_equipment:=core.jf_equipment_pkg$attr_to_rowtype(p_attr);
  l_r_equipment.equipment_status_id :=  jofl.jofl_pkg$extract_number(p_attr, 'F_equipment_remove_reason_id', true);
  l_res := core.jf_equipment_pkg$of_update(p_attr, row_to_json(l_r_equipment)::text);*/
  update core.equipment set
    equipment_status_id = jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_status_id', true)
  where
    equipment_id = l_r.equipment_id;

  --отвязываем от блока
  delete from core.sensor2unit where sensor_id=  l_r.equipment_id;

    --удаляем связь с ТС
  return core.jf_sensor2tr_pkg$of_delete(p_id_account, p_attr);


end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_sensor2tr_pkg$of_remove_from_tr"(numeric, text)
OWNER TO adv;

--****************************************************************************************

CREATE OR REPLACE FUNCTION core."jf_sensor2tr_pkg$get_default_status"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_res text;
begin
  l_res:= '{"equipment_status_id":'||core.jf_equipment_status_pkg$cn_status_defective()::text||',"name:: "Неисправное"''}';
  return l_res;

end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_sensor2tr_pkg$get_default_status"(numeric, text)
OWNER TO adv;
