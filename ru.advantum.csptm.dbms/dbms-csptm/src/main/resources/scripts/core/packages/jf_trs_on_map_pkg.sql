create or replace function core."jf_trs_on_map_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
language plpgsql
as $$
declare
  l_depo_id bigint;
  l_tr_type_id bigint;
  l_route_miud bigint;
  l_grg_num    text;
  l_licence    text;
  l_tr_capacity_id bigint;
  l_tab_num    text;
  l_tte_num bigint;
begin
  l_depo_id := jofl.jofl_pkg$extract_number(p_attr, 'F_depo_id', TRUE);
  l_tr_type_id := jofl.jofl_pkg$extract_number(p_attr, 'F_tr_type_id', TRUE);
  l_route_miud := jofl.jofl_pkg$extract_number(p_attr, 'F_MUID', TRUE);
  l_grg_num    := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_GRG_NUM_TEXT', true), '');
  l_licence    := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_LICENCE_TEXT', true), '');
  l_tr_capacity_id:= jofl.jofl_pkg$extract_number(p_attr, 'F_tr_capacity_id', TRUE);
  l_tab_num    :=  coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_TAB_NUM_TEXT', true), '');
  l_tte_num    :=  jofl.jofl_pkg$extract_number(p_attr, 'F_TIMETABLE_ENTRY_NUMBER', true);
   /* open p_rows for
    with cur_rounds as (select *
                        from (
                               select
                                 ol.tr_id,
                                 ol.order_num,
                                 tte.timetable_entry_num,
                                 r.muid route_muid,
                                 r.number route_number,
                                 row_number()   over (partition by ol.tr_id    order by orv.time_plan_begin) rn,
                                 dr.tab_num
                               from tt.order_list ol
                                 join tt.driver d on ol.driver_id = d.driver_id
                                 join tt.order_round ord on ol.order_list_id = ord.order_list_id
                                 join asd.oper_round_visit orv on ord.order_round_id = orv.order_round_id
                                 join tt.round rnd on ord.round_id = rnd.round_id
                                 join tt.timetable_entry tte on tte.timetable_entry_id = rnd.timetable_entry_id
                                 join gis.route_trajectories rt on rnd.route_trajectory_muid = rt.muid
                                 join gis.route_rounds rr on rt.route_round_muid = rr.muid
                                 join gis.route_variants rv on rr.route_variant_muid = rv.muid
                                 join gis.routes r on rv.route_muid = r.muid
                                 left join tt.driver dr on dr.driver_id = ol.driver_id
                               where ol.order_date = now() :: date :: timestamp
                                     --and now() between orv.time_plan_begin and orv.time_plan_end
                                     and ord.is_active = 1
                                     and ol.tr_id is not null
                                     ) t
                        where rn = 1)
    select
      tr.tr_id,
      tr.seat_qty,
      tr.seat_qty_total,
      tr.has_low_floor,
      tr.has_conditioner,
      tr.garage_num,
      tr.licence,
      tr.serial_num,
      tr.year_of_issue,
      tr.dt_begin,
      tr.dt_end,
      tr.tr_type_id,
      tr_model.tr_capacity_id,
      tr.tr_status_id,
      tr.tr_model_id,
      tr.territory_id,
      tr_status.name tr_status_name,
      tr_model.name tr_model_name,
      tr_type.name tr_type_name,
      entity.name_short territory_name,
      tr_capacity.qty tr_capacity_qty,
      tr_capacity.short_name as tr_capacity_short_name,
      cr.route_number,
      cr.order_num,
      cr.timetable_entry_num,
      cr.tab_num,
      ent.name_full as depo_name,
      ent.name_short as depo_short_name,
        (select
           coalesce(snsr.check_if_unit_has_sensor_type(u2t.unit_id, core.jf_equipment_type_pkg$cn_type_skah()),case tr.tr_type_id when core.tr_type_pkg$tb() then false else null end)::int
         from core.unit2tr u2t join core.equipment on equipment.equipment_id = u2t.unit_id and equipment_model_id in (143, 144)
         where u2t.tr_id = tr.tr_id limit 1
      )
      as has_sah
    from core.tr
      join core.tr_model on tr_model.tr_model_id = tr.tr_model_id
      join core.tr_status on tr_status.tr_status_id = tr.tr_status_id
      join core.tr_type on tr_type.tr_type_id = tr.tr_type_id
      left join core.territory on territory.territory_id = tr.territory_id
      left join core.entity on entity.entity_id = territory.territory_id
      join core.tr_capacity on tr_capacity.tr_capacity_id = tr_model.tr_capacity_id
      join core.depo depo on depo.depo_id =tr.depo_id
      join core.entity ent on ent.entity_id = depo.depo_id
      left join cur_rounds cr on tr.tr_id = cr.tr_id
    where
      (l_depo_id is null or tr.depo_id= l_depo_id)
      and (l_tr_type_id is null or tr.tr_type_id = l_tr_type_id)
      and (l_grg_num ='' or tr.garage_num::text =l_grg_num)
      and (l_licence ='' or tr.licence like '%'||l_licence||'%')
      and (l_tr_capacity_id is null or tr_model.tr_capacity_id = l_tr_capacity_id)
      and (l_tab_num ='' or cr.tab_num = l_tab_num)
      and (l_tte_num  is null or cr.timetable_entry_num = l_tte_num)
      and (l_route_miud is null or cr.route_muid =  l_route_miud)
      and (tr.tr_id in (select adm.account_data_realm_pkg$get_trs_by_tr_type_and_depo(p_id_account)) or cr.route_muid in (
        select route_muid
          from rts.route2gis
        where route_id in (select  adm.account_data_realm_pkg$get_routes(p_id_account))
      ));*/
  open p_rows for
   with a as (select p_attr as  p_attr, p_id_account as p_id_account),
        p as (select jofl.jofl_pkg$extract_number(a.p_attr, 'F_depo_id', TRUE) l_depo_id,
                     jofl.jofl_pkg$extract_number(a.p_attr, 'F_tr_type_id', TRUE) l_tr_type_id,
                     jofl.jofl_pkg$extract_number(a.p_attr, 'F_route_id', TRUE) l_route_id,
                     coalesce(jofl.jofl_pkg$extract_varchar(a.p_attr, 'F_GRG_NUM_TEXT', true), '') l_grg_num,
                     coalesce(jofl.jofl_pkg$extract_varchar(a.p_attr, 'F_LICENCE_TEXT', true), '') l_licence,
                     jofl.jofl_pkg$extract_number(a.p_attr, 'F_tr_capacity_id', TRUE) l_tr_capacity_id,
                     coalesce(jofl.jofl_pkg$extract_varchar(a.p_attr, 'F_TAB_NUM_TEXT', true), '') l_tab_num,
                     jofl.jofl_pkg$extract_number(a.p_attr, 'F_TIMETABLE_ENTRY_NUMBER', true) l_tte_num
              from a
             ),
        get_trs_by_tr_type_and_depo as (select adm.account_data_realm_pkg$get_trs_by_tr_type_and_depo(a.p_id_account) as id from a),
        get_routes as (select  adm.account_data_realm_pkg$get_routes(a.p_id_account) as id from a),
        cur_rounds as (
                        select t.tr_id, 'N/A' order_num, t.tt_out_num timetable_entry_num,
                               r.route_id, r.route_num route_number,
                               d.tab_num,
                               (     select ai.dr_shift_id
                                     from ttb.tt_action_item ai
                                     where ai.tt_action_id = ac.tt_action_id and not ai.sign_deleted
                                     limit 1
                               ) as dr_shift_id
                        from ttb.tt_variant v join ttb.tt_out t on v.tt_variant_id = t.tt_variant_id and not t.sign_deleted
                             join rts.route_variant rv on v.route_variant_id = rv.route_variant_id
                             join rts.route r on rv.route_id = r.route_id
                             join ttb.tt_action ac on t.tt_out_id = ac.tt_out_id and not ac.sign_deleted
                             left join core.driver d on ac.driver_id = d.driver_id,
                             a
                        where v.parent_tt_variant_id is not null
                          and now()::timestamp <@ ac.action_range
                          and (t.tr_id in (select id from get_trs_by_tr_type_and_depo) or r.route_id in (select id from get_routes))

                     )
    select
      tr.tr_id,
      tr.seat_qty,
      tr.seat_qty_total,
      tr.has_low_floor,
      tr.has_conditioner,
      tr.garage_num,
      tr.licence,
      tr.serial_num,
      tr.year_of_issue,
      tr.dt_begin,
      tr.dt_end,
      tr.tr_type_id,
      tr_model.tr_capacity_id,
      tr.tr_status_id,
      tr.tr_model_id,
      tr.territory_id,
      tr_status.name tr_status_name,
      tr_model.name tr_model_name,
      tr_type.name tr_type_name,
      entity.name_short territory_name,
      tr_capacity.qty tr_capacity_qty,
      tr_capacity.short_name as tr_capacity_short_name,
      cr.route_number,
      cr.order_num,
      cr.timetable_entry_num||(case when sh.dr_shift_num is null then '' else '/'||sh.dr_shift_num end) timetable_entry_num,
     -- cr.timetable_entry_num
      cr.tab_num,
      ent.name_full as depo_name,
      ent.name_short as depo_short_name,
        (select
           coalesce(snsr.check_if_unit_has_sensor_type(u2t.unit_id, core.jf_equipment_type_pkg$cn_type_skah()),case tr.tr_type_id when core.tr_type_pkg$tb() then false else null end)::int
         from core.unit2tr u2t join core.equipment on equipment.equipment_id = u2t.unit_id and equipment_model_id in (143, 144)
         where u2t.tr_id = tr.tr_id limit 1
      )
      as has_sah
    from a, p,
           core.tr
      join core.tr_model on tr_model.tr_model_id = tr.tr_model_id
      join core.tr_status on tr_status.tr_status_id = tr.tr_status_id
      join core.tr_type on tr_type.tr_type_id = tr.tr_type_id
      left join core.territory on territory.territory_id = tr.territory_id
      left join core.entity on entity.entity_id = territory.territory_id
      join core.tr_capacity on tr_capacity.tr_capacity_id = tr_model.tr_capacity_id
      join core.depo depo on depo.depo_id =tr.depo_id
      join core.entity ent on ent.entity_id = depo.depo_id
      left join cur_rounds cr on tr.tr_id = cr.tr_id
      left join ttb.dr_shift sh on cr.dr_shift_id = sh.dr_shift_id
    where
      (p.l_depo_id is null or tr.depo_id= p.l_depo_id)
      and (p.l_tr_type_id is null or tr.tr_type_id = p.l_tr_type_id)
      and (p.l_grg_num ='' or tr.garage_num::text = p.l_grg_num)
      and (p.l_licence ='' or tr.licence like '%'||p.l_licence||'%')
      and (p.l_tr_capacity_id is null or tr_model.tr_capacity_id = p.l_tr_capacity_id)
      and (p.l_tab_num ='' or cr.tab_num = p.l_tab_num)
      and (p.l_tte_num  is null or cr.timetable_entry_num = p.l_tte_num)
      and (p.l_route_id is null or cr.route_id =  p.l_route_id)
      and (tr.tr_id in (select id from get_trs_by_tr_type_and_depo) or cr.route_id in (select id from get_routes));
end;
$$;

CREATE OR REPLACE FUNCTION core.jf_trs_on_map_pkg$OF_EXTEND_INFO(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_s_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'MUID', true);
  pass NUMERIC;
BEGIN
  --OPEN p_rows FOR
  WITH days_number AS(
    -- количество таких же дней недели, как сейчас, за прошедший месяц
      select count(*) from generate_series( (now()::DATE - INTERVAL '1 month'), now()::DATE, '1 day') g(mydate)
      WHERE EXTRACT(ISODOW FROM mydate) = EXTRACT(ISODOW FROM now())
  )
  -- среднее за час количество пассажиров на этой остановке, в этот день недели
  select COUNT(*)/(select count from days_number) /*as "PASS_POTOK"*/ INTO pass from askp.validation2stops_binding
  where check_time >= (now() - Interval '1 month')
        AND stop_place_muid = l_s_id
        AND EXTRACT(ISODOW FROM check_time) = EXTRACT(ISODOW FROM now())
        AND ( EXTRACT(hour FROM check_time) BETWEEN EXTRACT(hour FROM now()) AND EXTRACT(hour FROM now())+1 );
  return jsonb_build_object('PASS_POTOK', pass)::TEXT;
END;
$$;