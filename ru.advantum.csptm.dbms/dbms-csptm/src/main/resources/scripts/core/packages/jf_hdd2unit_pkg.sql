﻿-- Function: core."jf_hdd2unit_pkg$attr_to_rowtype"(text)

 --DROP FUNCTION core."jf_hdd2unit_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_hdd2unit_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.hdd2unit AS
$BODY$ 
declare 
   l_r core.hdd2unit%rowtype;
begin 
   l_r.hdd_id := jofl.jofl_pkg$extract_number(p_attr, 'hdd_id', true);
   l_r.unit_id := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true), jofl.jofl_pkg$extract_number(p_attr, 'unit_id', true));

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_hdd2unit_pkg$attr_to_rowtype"(text)
  OWNER TO adv;

--********************************************************************************************************
-- Function: core."jf_hdd2unit_pkg$of_rows"(numeric, text)

 --DROP FUNCTION core."jf_hdd2unit_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_hdd2unit_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_r core.hdd2unit%rowtype;
begin
  l_r.unit_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
 open p_rows for
     select
       h2u.hdd_id,
       h2u.unit_id,
       et.equipment_id,
       hdd.type,
       hdd.capacity,
       hdd.size,
       et.firmware_id,
       et.serial_num,
       et.firmware_name,
       et.unit_service_type_id,
       et.unit_service_type_name,
       et.facility_id,
       et.facility_name,
       et.equipment_status_id,
       et.equipment_status_name,
       et.decommission_reason_id,
       et.decommission_reason_name,
       et.dt_begin,
       et.dt_end,
       et.equipment_model_id,
       et.equipment_model_name,
       et.equipment_type_name,
       et.depo_name_full,
       et.depo_name_short
     from core.hdd2unit h2u
       join core.hdd hdd on hdd.hdd_id = h2u.hdd_id
       join core.v_equipment et on et.equipment_id = h2u.hdd_id
     where h2u.unit_id = l_r.unit_id;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_hdd2unit_pkg$of_rows"(numeric, text)
  OWNER TO adv;

--********************************************************************************************************
-- Function: core."jf_hdd2unit_pkg$of_delete"(numeric, text)

 --DROP FUNCTION core."jf_hdd2unit_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_hdd2unit_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.hdd2unit%rowtype;
begin 
   l_r := core.jf_hdd2unit_pkg$attr_to_rowtype(p_attr);

   delete from  core.hdd2unit where  hdd_id = l_r.hdd_id and unit_id = l_r.unit_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_hdd2unit_pkg$of_delete"(numeric, text)
  OWNER TO adv;

--********************************************************************************************************
-- Function: core."jf_hdd2unit_pkg$of_insert"(numeric, text)

 DROP FUNCTION core."jf_hdd2unit_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_hdd2unit_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.hdd2unit%rowtype;
begin 
   l_r := core.jf_hdd2unit_pkg$attr_to_rowtype(p_attr);
   l_r.sys_period := tstzrange(now(), NULL::timestamp with time zone);

     insert into core.hdd2unit select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_hdd2unit_pkg$of_insert"(numeric, text)
  OWNER TO adv;

--********************************************************************************************************

--********************************************************************************************************
