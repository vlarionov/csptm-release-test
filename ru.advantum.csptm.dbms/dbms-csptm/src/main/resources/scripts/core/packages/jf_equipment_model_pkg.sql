﻿CREATE OR REPLACE FUNCTION core."jf_equipment_model_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.equipment_model AS
$BODY$ 
declare 
   l_r core.equipment_model%rowtype; 
begin 
   l_r.equipment_model_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_model_id', true); 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 
   l_r.equipment_type_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_type_id', true); 
   l_r.facility_id := jofl.jofl_pkg$extract_number(p_attr, 'facility_id', true); 
   l_r.has_sim := jofl.jofl_pkg$extract_boolean(p_attr, 'has_sim', true);
   l_r.input_digital := jofl.jofl_pkg$extract_number(p_attr, 'input_digital', true);
   l_r.input_analog := jofl.jofl_pkg$extract_number(p_attr, 'input_analog', true);

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_model_pkg$attr_to_rowtype"(text)
  OWNER TO adv;

--**********************************************************************************************
--drop TYPE core.jf_rf_db_method_model_block_type;

do $$
begin
  if not exists(select * from pg_type where typname = 'jf_rf_db_method_model_block_type') THEN

    CREATE TYPE core.jf_rf_db_method_model_block_type AS ENUM ('core.sensor2unit_model', 'core.unit_kbtob');

  end if;
end$$;

--**********************************************************************************************
 --DROP FUNCTION core."jf_equipment_model_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_equipment_model_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$
declare
  l_rf_db_method text;
  l_arr_rf_db_method_model_block text array;
  l_arr_rf_db_method_model_sensor text array;
  l_arr_rf_db_method_sensor_piece text array;
  l_arr_id_type_sensor_piece numeric array;
  l_equipment_type_id SMALLINT;
begin
  l_rf_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
  --окна моделей  ББ
   l_arr_rf_db_method_model_block:= '{"core.unit_kbtob", ' ||
                                    '"core.unit_bnsr", ' ||
                                    '"core.unit_bnst", ' ||
                                    '"core.sensor2unit_model"}' ;
  --окна моделей датчиков
  l_arr_rf_db_method_model_sensor:= '{"core.sensor", ' ||
                                    '"core.sensor2unit"}' ;
  --окна поштучных датчиков
  l_arr_rf_db_method_sensor_piece:= '{"core.monitor", ' ||
                                    '"core.modem", ' ||
                                    '"core.sensor_asmpp", ' ||
                                    '"core.sensor_dut", ' ||
                                    '"core.hdd", ' ||
                                    '"core.sensor_other"}';

 l_arr_id_type_sensor_piece:= ARRAY[core.jf_equipment_type_pkg$cn_type_asmpp(),
                                    core.jf_equipment_type_pkg$cn_type_dut(),
                                    core.jf_equipment_type_pkg$cn_type_hdd(),
                                    core.jf_equipment_type_pkg$cn_type_monitor(),
                                    core.jf_equipment_type_pkg$cn_type_modem()];

  case l_rf_db_method
    when 'core.unit_bnst' then /*Для БНСТ выбираем только с типом БНСТ*/
    select core.jf_equipment_type_pkg$cn_type_bnst()::smallint into l_equipment_type_id;
    when 'core.unit_bnsr' then /*Для БНСР выбираем только с типом БНСР*/
    select core.jf_equipment_type_pkg$cn_type_bnsr()::smallint into l_equipment_type_id;
    when 'core.unit_kbtob' then /*Для КБТОБ выбираем только с типом КБТОБ*/
    select core.jf_equipment_type_pkg$cn_type_kbtob()::smallint into l_equipment_type_id;
  else
    l_equipment_type_id := null;
  end case;

  open p_rows for
   select 
        em.equipment_model_id, 
        em.name,
        em.equipment_type_id,
        et.name_full as equipment_type_name,
        em.facility_id,
        ent.name_short as facility_name,
        em.has_sim,
        em.input_digital,
        em.input_analog,
        em.has_diagnostic
      from core.equipment_model em
      join core.equipment_type et on et.equipment_type_id =em.equipment_type_id
      left join core.entity ent on ent.entity_id = em.facility_id
      where
        l_rf_db_method is null or
              (l_rf_db_method  != all (l_arr_rf_db_method_model_block||l_arr_rf_db_method_model_sensor||l_arr_rf_db_method_sensor_piece))   or
              (l_rf_db_method = any(l_arr_rf_db_method_model_block)  and et.equipment_class_id = core.jf_equipment_class_pkg$cn_block() and (et.equipment_type_id = l_equipment_type_id or l_equipment_type_id is null)) or
              (l_rf_db_method = any(l_arr_rf_db_method_model_sensor)  and et.equipment_class_id = core.jf_equipment_class_pkg$cn_sensor() and em.equipment_type_id != all (l_arr_id_type_sensor_piece)) or
--              (l_rf_db_method = any(l_arr_rf_db_method_model_other)  and et.equipment_class_id = core.jf_equipment_class_pkg$cn_other()) or
              (l_rf_db_method = 'core.sensor_asmpp'  and em.equipment_type_id = core.jf_equipment_type_pkg$cn_type_asmpp()) or
              (l_rf_db_method = 'core.sensor_dut'  and em.equipment_type_id = core.jf_equipment_type_pkg$cn_type_dut()) or
              (l_rf_db_method = 'core.hdd'  and em.equipment_type_id = core.jf_equipment_type_pkg$cn_type_hdd()) or
              (l_rf_db_method = 'core.monitor'  and em.equipment_type_id = core.jf_equipment_type_pkg$cn_type_monitor()) or
              (l_rf_db_method = 'core.modem'  and em.equipment_type_id = core.jf_equipment_type_pkg$cn_type_modem()) or
              (l_rf_db_method = 'core.sensor_other'  and et.equipment_class_id = core."jf_equipment_class_pkg$cn_other"()
                                  and et.equipment_type_id not in (core."jf_equipment_type_pkg$cn_type_modem"(),
                                                                    core."jf_equipment_type_pkg$cn_type_monitor"(),
                                                                    core."jf_equipment_type_pkg$cn_type_hdd"()))
               ;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_model_pkg$of_rows"(numeric, text)
  OWNER TO adv;

--**********************************************************************************************
-- DROP FUNCTION core."jf_equipment_model_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_equipment_model_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.equipment_model%rowtype;
begin 
   l_r := core.jf_equipment_model_pkg$attr_to_rowtype(p_attr);

   delete from  core.equipment_model where  equipment_model_id = l_r.equipment_model_id;

   return null;
  EXCEPTION
  when 	foreign_key_violation THEN
    RAISE  EXCEPTION '<<Удаление невозможно! запись используется в других документах системы>>';
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_model_pkg$of_delete"(numeric, text)
  OWNER TO adv;
--**********************************************************************************************
-- DROP FUNCTION core."jf_equipment_model_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_equipment_model_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.equipment_model%rowtype;
begin 
   l_r := core.jf_equipment_model_pkg$attr_to_rowtype(p_attr);
   l_r.equipment_model_id := nextval('core.equipment_model_equipment_model_id_seq');

   select into l_r.has_diagnostic et.has_diagnostic
   from core.equipment_type et
   where et.equipment_type_id = l_r.equipment_type_id;

   insert into core.equipment_model select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_model_pkg$of_insert"(numeric, text)
  OWNER TO adv;

--**********************************************************************************************
-- DROP FUNCTION core."jf_equipment_model_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_equipment_model_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.equipment_model%rowtype;
begin 
   l_r := core.jf_equipment_model_pkg$attr_to_rowtype(p_attr);

   update core.equipment_model set 
          name = l_r.name, 
          equipment_type_id = l_r.equipment_type_id, 
          facility_id = l_r.facility_id,
          has_sim = l_r.has_sim,
          input_digital = l_r.input_digital,
          input_analog = l_r.input_analog
   where
          equipment_model_id = l_r.equipment_model_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_model_pkg$of_update"(numeric, text)
  OWNER TO adv;

--**********************************************************************************************

CREATE OR REPLACE FUNCTION core."jf_equipment_model_pkg$of_has_diagnostic_switch"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r core.equipment_model%rowtype;
begin
  l_r := core.jf_equipment_model_pkg$attr_to_rowtype(p_attr);
  --Для всего оборудования данной модели переключаем признак

  update core.equipment set
    has_diagnostic = not has_diagnostic
  where
    equipment_model_id = l_r.equipment_model_id;

  update core.equipment_model set
    has_diagnostic = not has_diagnostic
  where
    equipment_model_id = l_r.equipment_model_id;

  return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_equipment_model_pkg$of_has_diagnostic_switch"(numeric, text)
OWNER TO adv;


