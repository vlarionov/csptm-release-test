CREATE OR REPLACE FUNCTION core.jf_f_driver_fullname_pkg$of_rows(
  IN p_id_account numeric,
  OUT p_rows refcursor,
  IN p_attr text)
  RETURNS refcursor
AS
$BODY$
DECLARE
begin
  open p_rows for
  select 
    (coalesce(dr.driver_last_name, '') || ' '
     || coalesce(dr.driver_name, '') || ' '
     || coalesce(driver_middle_name, '')) as dr_full_name
  from core.driver dr
  order by 1;
end;
$BODY$
LANGUAGE plpgsql;