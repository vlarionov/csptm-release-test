﻿--********************************************************************************************************
-- Function: core."jf_sensor_all2unit_pkg$of_rows"(numeric, text)

 DROP FUNCTION core."jf_sensor_all2unit_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sensor_all2unit_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_r core.sensor2unit%rowtype;
  l_rf_db_method text;
begin
  l_r.unit_id := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true),jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_id', true));
  l_rf_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);


 open p_rows for
 select
   eq2u.equipment_id as sensor_id,
   eq2u.unit_id as equipment_id,
   eq2u.sensor_num,
   et.serial_num,
   et.firmware_id,
   et.firmware_name,
   et.unit_service_type_id,
   et.unit_service_type_name,
   et.facility_id,
   et.facility_name,
   et.equipment_status_id,
   et.equipment_status_name,
   et.decommission_reason_id,
   et.decommission_reason_name,
   et.dt_begin,
   et.dt_end,
   et.equipment_model_id,
   et.equipment_model_name,
   et.equipment_type_name,
   et.equipment_type_id,
   et.depo_name_full,
   et.depo_name_short,
   et_unit.equipment_model_id as  unit_equipment_model_id,
   unit_em.equipment_type_id as unit_equipment_type_id,
   lower(eq2u.sys_period) as dt_link
 from core.v_equipment2unit eq2u
   join core.v_equipment et on et.equipment_id = eq2u.equipment_id
   join core.equipment et_unit  on et_unit.equipment_id = eq2u.unit_id
   join core.equipment_model unit_em on unit_em.equipment_model_id = et_unit.equipment_model_id
  where eq2u.unit_id = l_r.unit_id;

end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sensor_all2unit_pkg$of_rows"(numeric, text)
  OWNER TO adv;

--********************************************************************************************************
-- Function: core."jf_sensor_all2unit_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_sensor_all2unit_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sensor_all2unit_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare
    l_equipment_type_id core.equipment_type.equipment_type_id%type :=  jofl.jofl_pkg$extract_number(p_attr, 'equipment_type_id', true);
    l_unit_equipment_type_id core.equipment_type.equipment_type_id%type :=  jofl.jofl_pkg$extract_number(p_attr, 'unit_equipment_type_id', true);
    l_equipment_id core.equipment.equipment_id%type :=  jofl.jofl_pkg$extract_number(p_attr, 'sensor_id', true);
    l_unit_id core.unit.unit_id%type :=  jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
    l_sensor_num core.sensor2unit.sensor_num%type;
    l_res text;
begin

    if l_equipment_type_id =  core.jf_equipment_type_pkg$cn_type_hdd() then
    l_res:= '{"equipment_id":'||l_unit_id||',"hdd_id":'||l_equipment_id||'}';
    return core.jf_hdd2unit_pkg$of_delete(p_id_account, l_res);

    elsif l_equipment_type_id = core.jf_equipment_type_pkg$cn_type_modem() then
    l_res:= '{"equipment_id":'||l_unit_id||',"modem_id":'||l_equipment_id||'}';
    return core.jf_modem2unit_pkg$of_delete(p_id_account, l_res);

    elsif l_equipment_type_id = core.jf_equipment_type_pkg$cn_type_monitor() then
    l_res:= '{"equipment_id":'||l_unit_id||',"monitor_id":'||l_equipment_id||'}';
    return core.jf_monitor2unit_pkg$of_delete(p_id_account, l_res);

    else
    l_res:= '{"equipment_id":'||l_unit_id||',"sensor_id":'||l_equipment_id||'}';
    return core.jf_sensor2unit_pkg$of_delete(p_id_account, l_res);
    end if;

end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sensor_all2unit_pkg$of_delete"(numeric, text)
  OWNER TO adv;

--********************************************************************************************************
-- Function: core."jf_sensor_all2unit_pkg$of_insert"(numeric, text)

 DROP FUNCTION core."jf_sensor_all2unit_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sensor_all2unit_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
  l_equipment_type_id core.equipment_type.equipment_type_id%type :=  jofl.jofl_pkg$extract_number(p_attr, 'equipment_type_id', true);
  l_unit_equipment_type_id core.equipment_type.equipment_type_id%type :=  jofl.jofl_pkg$extract_number(p_attr, 'unit_equipment_type_id', true);
  l_equipment_id core.equipment.equipment_id%type :=  jofl.jofl_pkg$extract_number(p_attr, 'sensor_id', true);
  l_unit_id core.unit.unit_id%type :=  jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
  l_sensor_num core.sensor2unit.sensor_num%type :=  jofl.jofl_pkg$extract_number(p_attr, 'sensor_num', true);
  l_res text;
begin
  if l_sensor_num is null then
      select into l_sensor_num min(s2um.num)
      from core.sensor2unit_model s2um
      where s2um.equipment_type_sensor_id = l_equipment_type_id
            and s2um.equipment_type_unit_id = l_unit_equipment_type_id
            and  not exists (select 1 from core.sensor2unit s2u where s2u.unit_id = l_unit_id and s2u.sensor_num = s2um.num);

      if l_sensor_num is null then
        RAISE EXCEPTION '<<Не найден свободный вход ББ для подключения >>';
      end if;
  end if;

  if l_equipment_type_id =  core.jf_equipment_type_pkg$cn_type_hdd() then
     l_res:= '{"equipment_id":'||l_unit_id||',"hdd_id":'||l_equipment_id||'}';
     return core.jf_hdd2unit_pkg$of_insert(p_id_account, l_res);
  elsif l_equipment_type_id = core.jf_equipment_type_pkg$cn_type_modem() then
     l_res:= '{"equipment_id":'||l_unit_id||',"modem_id":'||l_equipment_id||'}';
     return core.jf_modem2unit_pkg$of_insert(p_id_account, l_res);
  elsif l_equipment_type_id = core.jf_equipment_type_pkg$cn_type_monitor() then
     l_res:= '{"equipment_id":'||l_unit_id||',"monitor_id":'||l_equipment_id||'}';
     return core.jf_monitor2unit_pkg$of_insert(p_id_account, l_res);
  else
    l_res:= '{"equipment_id":'||l_unit_id||',"sensor_id":'||l_equipment_id||',"sensor_num":'||l_sensor_num||'}';
    return core.jf_sensor2unit_pkg$of_insert(p_id_account, l_res);
  end if;

end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sensor_all2unit_pkg$of_insert"(numeric, text)
  OWNER TO adv;

--********************************************************************************************************

--********************************************************************************************************
