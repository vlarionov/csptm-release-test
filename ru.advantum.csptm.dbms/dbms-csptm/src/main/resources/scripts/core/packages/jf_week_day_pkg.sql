﻿-- Function: core."jf_week_day_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION core."jf_week_day_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_week_day_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.week_day AS
$BODY$ 
declare 
   l_r core.week_day%rowtype; 
begin 
   l_r.wd_full_name := jofl.jofl_pkg$extract_varchar(p_attr, 'wd_full_name', true); 
   l_r.wd_short_name := jofl.jofl_pkg$extract_varchar(p_attr, 'wd_short_name', true); 
   l_r.week_day_id := jofl.jofl_pkg$extract_varchar(p_attr, 'week_day_id', true); 

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_week_day_pkg$attr_to_rowtype"(text)
  OWNER TO adv;

--****************************************************************************************************************

-- Function: core."jf_week_day_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_week_day_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_week_day_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.week_day%rowtype;
begin 
   l_r := core.jf_week_day_pkg$attr_to_rowtype(p_attr);

  /* delete from  core.week_day where  week_day_id = l_r.week_day_id;*/

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_week_day_pkg$of_delete"(numeric, text)
  OWNER TO adv;
--****************************************************************************************************************

-- Function: core."jf_week_day_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_week_day_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_week_day_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.week_day%rowtype;
begin 
   l_r := core.jf_week_day_pkg$attr_to_rowtype(p_attr);

   /*insert into core.week_day select l_r.*;*/

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_week_day_pkg$of_insert"(numeric, text)
  OWNER TO adv;

--****************************************************************************************************************

-- Function: core."jf_week_day_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_week_day_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_week_day_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_rf_db_method TEXT;
  l_calculation_task_id bigint;
begin

  l_rf_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
  l_calculation_task_id=jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true);

 open p_rows for 
      select 
        wd_full_name, 
        wd_short_name, 
        week_day_id
      from core.week_day wd
      where   l_rf_db_method is null or
              (l_rf_db_method = 'asd.week_day2ct' and
               not exists (select 1 from asd.week_day2ct wd2ct
               where wd2ct.week_day_id = wd.week_day_id
                     and wd2ct.calculation_task_id = l_calculation_task_id));
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_week_day_pkg$of_rows"(numeric, text)
  OWNER TO adv;

--****************************************************************************************************************

-- Function: core."jf_week_day_pkg$of_update"(numeric, text)

-- DROP FUNCTION core."jf_week_day_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_week_day_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.week_day%rowtype;
begin 
   l_r := core.jf_week_day_pkg$attr_to_rowtype(p_attr);

   /*update core.week_day set 
          wd_full_name = l_r.wd_full_name, 
          wd_short_name = l_r.wd_short_name
   where 
          week_day_id = l_r.week_day_id;*/

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_week_day_pkg$of_update"(numeric, text)
  OWNER TO adv;

