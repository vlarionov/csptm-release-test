﻿
create or replace function core.statesaver_pkg$save_tr_last_state
   (p_tr_id       in integer, 
    p_packet_id   in bigint, 
    p_unit_id     in integer, 
    p_event_time  in timestamp, 
    p_device_event_id in integer, 
    p_location_valid  in bool, 
    p_gps_time    in timestamp, 
    p_lon         in float4, 
    p_lat         in float4, 
    p_alt         in integer, 
    p_speed       in integer, 
    p_heading     in integer)
returns integer
-- ========================================================================
-- Импорт текущего состояния ТС
-- ========================================================================
as $$ declare
    v_result      integer := 0;
begin
   if p_tr_id is null or p_packet_id is null or p_unit_id is null or p_event_time is null or 
      p_device_event_id is null or p_location_valid is null then
      return v_result;
   end if;
   
   insert into core.tr_last_state as t (tr_id, packet_id, unit_id, event_time, device_event_id, location_valid, gps_time, lon, lat, alt, speed, heading)
   values (p_tr_id, p_packet_id, p_unit_id, p_event_time, p_device_event_id, p_location_valid, p_gps_time, p_lon, p_lat, p_alt, p_speed, p_heading)
   on conflict (tr_id)
   do update set (packet_id, unit_id, event_time, device_event_id, location_valid, gps_time, lon, lat, alt, speed, heading) =
                 (excluded.packet_id, excluded.unit_id, excluded.event_time, excluded.device_event_id, excluded.location_valid,
                  excluded.gps_time, excluded.lon, excluded.lat, excluded.alt, excluded.speed, excluded.heading)
   where t.packet_id   is distinct from excluded.packet_id or
         t.unit_id     is distinct from excluded.unit_id or
         t.event_time  is distinct from excluded.event_time or
         t.device_event_id is distinct from excluded.device_event_id or
         t.location_valid  is distinct from excluded.location_valid or
         t.gps_time    is distinct from excluded.gps_time or
         t.lon         is distinct from excluded.lon or
         t.lat         is distinct from excluded.lat or
         t.alt         is distinct from excluded.alt or
         t.speed       is distinct from excluded.speed or
         t.heading     is distinct from excluded.heading;
    
    get diagnostics v_result = row_count;
    return v_result;
end;
$$ language plpgsql security definer;
