CREATE OR REPLACE FUNCTION core."jf_sim_card2unit_pkg$attr_to_rowtype" (in p_attr text) RETURNS core.sim_card2unit AS
  $BODY$
  declare
     l_r core.sim_card2unit%rowtype;
  begin
     l_r.sim_card_id := jofl.jofl_pkg$extract_number(p_attr, 'sim_card_id', true);
     l_r.equipment_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);

     return l_r;
  end;
$BODY$
LANGUAGE 'plpgsql'
COST 100;
ALTER FUNCTION core."jf_sim_card2unit_pkg$attr_to_rowtype"(text)
OWNER TO adv;

---****************************************************************************************************************************
CREATE OR REPLACE FUNCTION core."jf_sim_card2unit_pkg$of_rows" (in p_id_account numeric, out p_rows refcursor, in p_attr text) RETURNS refcursor AS
  $BODY$
    declare
      l_r core.sim_card2unit%rowtype;
    begin
      l_r := core.jf_sim_card2unit_pkg$attr_to_rowtype(p_attr);

     open p_rows for
     with decom as (
         select d.decommission_reason_id,
           d.sim_card_id,
           d.dt_end,
           dr.name as decommission_reason_name
         from core.sim_decommission d
           join core.decommission_reason dr on dr.decommission_reason_id = d.decommission_reason_id
         where not d.is_deleted
     )
     select
       s2u.sim_card_id,
       s2u.equipment_id,
       mt.name as mobile_tariff_name,
       scg.name as sim_card_group_name,
       es.name as equipment_status_name,
       decom.dt_end,
       decom.decommission_reason_id,
       decom.decommission_reason_name,
       sc.dt_begin,
       sc.iccid,
       sc.phone_num,
       sc.imsi
     from core.sim_card2unit s2u
       join core.sim_card sc on sc.sim_card_id = s2u.sim_card_id
       join core.sim_card_group scg on scg.sim_card_group_id = sc.sim_card_group_id
       join core.mobile_tariff mt on mt.mobile_tariff_id = sc.mobile_tariff_id
       join core.equipment_status es on es.equipment_status_id = sc.equipment_status_id
       left join decom ON decom.sim_card_id =  sc.sim_card_id
     where s2u.equipment_id =  l_r.equipment_id;
    end;
$BODY$
LANGUAGE 'plpgsql'
COST 100;
ALTER FUNCTION core."jf_sim_card2unit_pkg$of_rows"(numeric, text)
OWNER TO adv;
---****************************************************************************************************************************

CREATE OR REPLACE FUNCTION core."jf_sim_card2unit_pkg$of_insert" (in p_id_account numeric, in p_attr text) RETURNS text AS
$BODY$
declare
   l_r core.sim_card2unit%rowtype;
   l_r_h core.sim_card2unit_h%rowtype;
   l_res text;
begin
   l_r := core.jf_sim_card2unit_pkg$attr_to_rowtype(p_attr);

   if exists (select 1 from core.sim_card2unit t where t.equipment_id = l_r.equipment_id) then
     RAISE  EXCEPTION '<<К блоку уже привязана sim-карта>>';
   end if;
   insert into core.sim_card2unit select l_r.*;

  l_r_h.dt_begin := current_timestamp;
  l_r_h.equipment_id := l_r.equipment_id;
  l_r_h.sim_card_id:= l_r.sim_card_id;
  l_res:= core.jf_sim_card2unit_h_pkg$of_insert(p_id_account, row_to_json(l_r_h)::text);
   return null;
end;
$BODY$
LANGUAGE 'plpgsql'
COST 100;
ALTER FUNCTION core."jf_sim_card2unit_pkg$of_insert"(numeric, text)
OWNER TO adv;

---****************************************************************************************************************************
CREATE OR REPLACE FUNCTION core."jf_sim_card2unit_pkg$of_delete" (in p_id_account numeric, in p_attr text) RETURNS text AS
$BODY$
 declare
    l_r core.sim_card2unit%rowtype;
 begin
    l_r := core.jf_sim_card2unit_pkg$attr_to_rowtype(p_attr);

    delete from  core.sim_card2unit where  sim_card_id = l_r.sim_card_id and    equipment_id = l_r.equipment_id;

    return null;
 end;
$BODY$
LANGUAGE 'plpgsql'
COST 100;
ALTER FUNCTION core."jf_sim_card2unit_pkg$of_delete"(numeric, text)
OWNER TO adv;
--*****************************************************************************************************************************

CREATE OR REPLACE FUNCTION core."jf_sim_card2unit_pkg$of_remove_from_unit"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r core.sim_card2unit%rowtype;
  l_r_h core.sim_card2unit_h%rowtype;
  l_res text;

begin
  l_r := core.jf_sim_card2unit_pkg$attr_to_rowtype(p_attr);
  --сохранить в истории


  select into l_r_h.sim_card2unit_h_id  h.sim_card2unit_h_id
  FROM core.sim_card2unit_h h
  where h.equipment_id = l_r.equipment_id
        and h.sim_card_id = l_r.sim_card_id
        and not exists ( select 1
                         from core.sim_decommission d
                         where not d.is_deleted and d.sim_card_id = h.sim_card_id);

  if l_r_h.sim_card2unit_h_id is not null then
    l_r_h.unit_remove_reason:= jofl.jofl_pkg$extract_number(p_attr, 'F_equipment_remove_reason_id', true);
    l_r_h.sim_card_id:= l_r.sim_card_id;

    l_res:= core.jf_sim_card2unit_h_pkg$of_update(p_id_account, row_to_json(l_r_h)::text);
  end if;

  --обновляем статус оборудования

  /*l_r_equipment:=core.jf_equipment_pkg$attr_to_rowtype(p_attr);
  l_r_equipment.equipment_status_id :=  jofl.jofl_pkg$extract_number(p_attr, 'F_equipment_status_id', true);
  l_res := core.jf_equipment_pkg$of_update(p_attr, row_to_json(l_r_equipment)::text);*/
  update core.sim_card set
    equipment_status_id = jofl.jofl_pkg$extract_number(p_attr, 'F_equipment_status_id', true)
  where
    sim_card_id = l_r.sim_card_id;


  --удаляем связь
  l_res:= core.jf_sim_card2unit_pkg$of_delete(p_id_account, p_attr);
  return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_sim_card2unit_pkg$of_remove_from_unit"(numeric, text)
OWNER TO adv;
