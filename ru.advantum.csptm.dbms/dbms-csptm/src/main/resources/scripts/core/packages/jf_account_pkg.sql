CREATE OR REPLACE FUNCTION core.jf_account_pkg$attr_to_rowtype(attr TEXT)
  RETURNS core.account
AS
  $BODY$
  DECLARE
    lr core.account%ROWTYPE;
  BEGIN
    lr.last_name := jofl.jofl_pkg$extract_varchar(attr, 'last_name', true);
    lr.account_id := jofl.jofl_pkg$extract_varchar(attr, 'account_id', true);
    lr.first_name := jofl.jofl_pkg$extract_varchar(attr, 'first_name', true);
    lr.middle_name := jofl.jofl_pkg$extract_varchar(attr, 'middle_name', true);
    lr.login := jofl.jofl_pkg$extract_varchar(attr, 'login', true);
    lr.password := jofl.jofl_pkg$extract_varchar(attr, 'password', true);
    lr.dt_insert := jofl.jofl_pkg$extract_date(attr, 'dt_insert', true);
    lr.dt_update := jofl.jofl_pkg$extract_date(attr, 'dt_update', true);
    lr.dt_lock := jofl.jofl_pkg$extract_date(attr, 'dt_lock', true);
    lr.is_locked := jofl.jofl_pkg$extract_varchar(attr, 'is_locked', true);
    lr.is_deleted := jofl.jofl_pkg$extract_varchar(attr, 'is_deleted', true);
    RETURN lr;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;

--*********************************************************************************************************
CREATE OR REPLACE FUNCTION core.jf_account_pkg$of_delete(aid_account NUMERIC, attr TEXT)
  RETURNS TEXT
AS
  $BODY$
  DECLARE
    lr CORE.ACCOUNT%ROWTYPE;
  BEGIN
    lr := CORE.JF_ACCOUNT_PKG$attr_to_rowtype(attr);

    UPDATE CORE.ACCOUNT
    SET is_deleted = 1
    WHERE
      ACCOUNT_ID = lr.ACCOUNT_ID;
    RETURN NULL;

  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;
--*********************************************************************************************************
CREATE OR REPLACE FUNCTION core.jf_account_pkg$of_insert(aid_account NUMERIC, attr TEXT)
  RETURNS TEXT
AS
  $BODY$
  DECLARE
    lr CORE.ACCOUNT%ROWTYPE;
  BEGIN
    lr := CORE.JF_ACCOUNT_PKG$attr_to_rowtype(attr);
    lr.account_id := nextval('core.account_account_id_seq');
    lr.is_deleted := 0;
    INSERT INTO CORE.ACCOUNT SELECT lr.*;

    perform jofl.jofl_pkg$register_account(lr.account_id, core.const_pkg$core_project_name());

    RETURN NULL;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;
--*********************************************************************************************************
CREATE OR REPLACE FUNCTION core.jf_account_pkg$of_rows(IN aid_account NUMERIC, OUT arows REFCURSOR, IN attr TEXT)
  RETURNS REFCURSOR
AS
  $BODY$
  DECLARE
  BEGIN
    open arows for
    select
      last_name,
      account_id,
      first_name,
      middle_name,
      login,
      password,
      dt_insert,
      dt_update,
      dt_lock,
      is_locked,
      is_deleted
    FROM CORE.ACCOUNT;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;

--*********************************************************************************************************
CREATE OR REPLACE FUNCTION core.jf_account_pkg$of_update(aid_account NUMERIC, attr TEXT)
  RETURNS TEXT
AS
  $BODY$
  DECLARE
    lr CORE.ACCOUNT%ROWTYPE;
  BEGIN
    lr := CORE.JF_ACCOUNT_PKG$attr_to_rowtype(attr);

    UPDATE CORE.ACCOUNT
    SET
      LAST_NAME     = lr.LAST_NAME,
      FIRST_NAME    = lr.FIRST_NAME,
      MIDDLE_NAME   = lr.MIDDLE_NAME,
      LOGIN         = lr.LOGIN,
      PASSWORD      = lr.PASSWORD,
      --DT_INSERT     = lr.DT_INSERT,
      DT_UPDATE     = current_timestamp,
      --DT_LOCK       = lr.DT_LOCK,
      IS_LOCKED     = lr.IS_LOCKED
      --IS_DELETED    = lr.IS_DELETED
    WHERE
      ACCOUNT_ID = lr.ACCOUNT_ID;
    RETURN NULL;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;

