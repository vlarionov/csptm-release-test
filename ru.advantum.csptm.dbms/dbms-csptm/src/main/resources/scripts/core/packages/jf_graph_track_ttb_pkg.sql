﻿
create or replace function core.jf_graph_track_ttb_pkg$of_rows
  (p_account_id  in numeric, 
   p_attr        in text default '{}',
   p_rows        out refcursor)
returns refcursor 
as $$ declare
   l_tr_id    core.tr.tr_id%type := jofl.jofl_pkg$extract_number(p_attr,'F_ID_TR',  true);
   l_begin_dt timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'F_BEGIN_DT',  true);
   l_end_dt   timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'F_END_DT',  true);
begin
   open p_rows for
   select (g.event_time)::text || '.0'    as "GMTEVENTTIME",
          ttr.round_id                    as "ROUND_ID",
          g.graph_section_id              as "GRAPH_SECTION_ID",
          g.order_num                     as "ORDER_NUM",
          g.graph_section_offset          as "GRAPH_SECTION_OFFSET",
          g.round_offset                  as "ROUND_OFFSET",
          tf.speed                        as "SPEED"
     from asd.tf_graph_link_ttb g
     join ttb.tt_action_round ttr on ttr.tt_action_id = g.tt_action_id
     left join core.traffic tf on tf.packet_id = g.packet_id 
                              and tf.tr_id = g.tr_id 
                              and tf.event_time = g.event_time
    where g.tr_id = l_tr_id
      and g.event_time between l_begin_dt and l_end_dt
    order by g.event_time;
end;
$$ language plpgsql;
