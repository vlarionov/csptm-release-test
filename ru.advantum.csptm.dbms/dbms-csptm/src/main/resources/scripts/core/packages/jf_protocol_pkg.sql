﻿-- Function: core."jf_protocol_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION core."jf_protocol_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_protocol_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.protocol AS
$BODY$ 
declare 
   l_r core.protocol%rowtype; 
begin 
   l_r.name_short := jofl.jofl_pkg$extract_varchar(p_attr, 'name_short', true); 
   l_r.name_full := jofl.jofl_pkg$extract_varchar(p_attr, 'name_full', true); 
   l_r.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true); 
   l_r.protocol_id := jofl.jofl_pkg$extract_number(p_attr, 'protocol_id', true); 

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_protocol_pkg$attr_to_rowtype"(text)
  OWNER TO adv;
--*********************************************************************************
-- Function: core."jf_protocol_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_protocol_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_protocol_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
begin 
 open p_rows for 
      select 
        name_short, 
        name_full, 
        comment, 
        protocol_id
      from core.protocol; 
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_protocol_pkg$of_rows"(numeric, text)
  OWNER TO adv;

--*********************************************************************************
-- Function: core."jf_protocol_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_protocol_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_protocol_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.protocol%rowtype;
begin 
   l_r := core.jf_protocol_pkg$attr_to_rowtype(p_attr);

   delete from  core.protocol where  protocol_id = l_r.protocol_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_protocol_pkg$of_delete"(numeric, text)
  OWNER TO adv;

--*********************************************************************************
-- Function: core."jf_protocol_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_protocol_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_protocol_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.protocol%rowtype;
begin 
   l_r := core.jf_protocol_pkg$attr_to_rowtype(p_attr);
   l_r.protocol_id := nextval('core.protocol_protocol_id_seq');
   insert into core.protocol select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_protocol_pkg$of_insert"(numeric, text)
  OWNER TO adv;

--*********************************************************************************
-- Function: core."jf_protocol_pkg$of_update"(numeric, text)

-- DROP FUNCTION core."jf_protocol_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_protocol_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.protocol%rowtype;
begin 
   l_r := core.jf_protocol_pkg$attr_to_rowtype(p_attr);

   update core.protocol set 
          name_short = l_r.name_short, 
          name_full = l_r.name_full, 
          comment = l_r.comment
   where 
          protocol_id = l_r.protocol_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_protocol_pkg$of_update"(numeric, text)
  OWNER TO adv;

--*********************************************************************************