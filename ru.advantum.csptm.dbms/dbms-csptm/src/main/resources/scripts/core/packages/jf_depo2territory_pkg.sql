﻿-- Function: core."jf_depo2territory_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION core."jf_depo2territory_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_depo2territory_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.depo2territory AS
$BODY$ 
declare 
   l_r core.depo2territory%rowtype; 
begin
   l_r.depo2territory_id:= jofl.jofl_pkg$extract_number(p_attr, 'depo2territory_id', true);
   l_r.territory_id := jofl.jofl_pkg$extract_number(p_attr, 'territory_id', true); 
   l_r.depo_id := jofl.jofl_pkg$extract_number(p_attr, 'depo_id', true); 
   l_r.dt_begin := jofl.jofl_pkg$extract_date(p_attr, 'dt_begin', true); 
   l_r.dt_end := jofl.jofl_pkg$extract_date(p_attr, 'dt_end', true); 

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_depo2territory_pkg$attr_to_rowtype"(text)
  OWNER TO adv;
---

-- Function: core."jf_depo2territory_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_depo2territory_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_depo2territory_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.depo2territory%rowtype;
begin 
   l_r := core.jf_depo2territory_pkg$attr_to_rowtype(p_attr);

   delete from  core.depo2territory where  territory_id = l_r.territory_id and 
 depo_id = l_r.depo_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_depo2territory_pkg$of_delete"(numeric, text)
  OWNER TO adv;
--------------------
-- Function: core."jf_depo2territory_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_depo2territory_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_depo2territory_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.depo2territory%rowtype;
begin 
   l_r := core.jf_depo2territory_pkg$attr_to_rowtype(p_attr);
   l_r.depo2territory_id := nextval( 'core.depo2territory_depo2territory_id_seq' );
   insert into core.depo2territory select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_depo2territory_pkg$of_insert"(numeric, text)
  OWNER TO adv;
------------------
-- Function: core."jf_depo2territory_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_depo2territory_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_depo2territory_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
 l_depo_id BIGINT;
 l_territory_id BIGINT;
 l_route_id rts.route.route_id%type;
begin
 l_depo_id := jofl.jofl_pkg$extract_number(p_attr, 'depo_id', TRUE);
 l_territory_id := jofl.jofl_pkg$extract_number(p_attr, 'territory_id', TRUE);
 l_route_id := jofl.jofl_pkg$extract_number(p_attr, 'route_id', TRUE);
 open p_rows for
   select
     d2t.depo2territory_id,
     d2t.territory_id,
     d2t.depo_id,
     d2t.dt_begin,
     d2t.dt_end,
     d.tn_instance::int as depo_num,
     d.sign_deleted as depo_deleted,
     tt.name as tr_type_name,
     ed.name_full as depo_name_full,
     ed.name_short as depo_name_short,
     t.sign_deleted as terr_deleted,
     et.name_full as terr_name_full,
     et.name_short as terr_name_short
   from core.depo2territory d2t
     join core.depo d on d.depo_id = d2t.depo_id
     join core.tr_type tt on tt.tr_type_id = d.tr_type_id
     join core.entity ed on ed.entity_id = d.depo_id
     join core.territory t on t.territory_id = d2t.territory_id
     join core.entity et on et.entity_id = t.territory_id
   where ((l_depo_id is null or d2t.depo_id = l_depo_id)
     and  (l_territory_id is null or d2t.territory_id = l_territory_id))
     and  (l_route_id is null or exists (select 1 from rts.depo2route d2r where d2r.route_id =l_route_id and d2r.depo_id = d2t.depo_id))
  ;

end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_depo2territory_pkg$of_rows"(numeric, text)
  OWNER TO adv;
---------------------


-- DROP FUNCTION core."jf_depo2territory_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_depo2territory_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.depo2territory%rowtype;
begin 
   l_r := core.jf_depo2territory_pkg$attr_to_rowtype(p_attr);

   update core.depo2territory set 
          dt_begin = l_r.dt_begin, 
          dt_end = l_r.dt_end
   where 
          territory_id = l_r.territory_id and 
          depo_id = l_r.depo_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_depo2territory_pkg$of_update"(numeric, text)
  OWNER TO adv;
