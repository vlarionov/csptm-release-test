﻿-- Function: core."jf_sim_card_group$attr_to_rowtype"(text)

-- DROP FUNCTION core."jf_sim_card_group$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_sim_card_group$attr_to_rowtype"(p_attr text)
  RETURNS core.sim_card_group AS
$BODY$ 
declare 
   l_r core.sim_card_group%rowtype; 
begin 
   l_r.sim_card_group_id := jofl.jofl_pkg$extract_number(p_attr, 'sim_card_group_id', true); 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sim_card_group$attr_to_rowtype"(text)
  OWNER TO adv;
--**************************************************************************************************
-- Function: core."jf_sim_card_group$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_sim_card_group$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sim_card_group$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
begin 
 open p_rows for 
      select 
        sim_card_group_id, 
        name
      from core.sim_card_group; 
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sim_card_group$of_rows"(numeric, text)
  OWNER TO adv;

--**************************************************************************************************
-- Function: core."jf_sim_card_group$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_sim_card_group$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sim_card_group$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.sim_card_group%rowtype;
begin 
   l_r := core.jf_sim_card_group$attr_to_rowtype(p_attr);

   delete from  core.sim_card_group where  sim_card_group_id = l_r.sim_card_group_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sim_card_group$of_delete"(numeric, text)
  OWNER TO adv;

--**************************************************************************************************
-- Function: core."jf_sim_card_group$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_sim_card_group$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sim_card_group$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.sim_card_group%rowtype;
begin 
   l_r := core.jf_sim_card_group$attr_to_rowtype(p_attr);
   l_r.sim_card_group_id := nextval( 'core.sim_card_group_sim_card_group_id_seq' );
   
   insert into core.sim_card_group select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sim_card_group$of_insert"(numeric, text)
  OWNER TO adv;

--**************************************************************************************************
-- Function: core."jf_sim_card_group$of_update"(numeric, text)

-- DROP FUNCTION core."jf_sim_card_group$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sim_card_group$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.sim_card_group%rowtype;
begin 
   l_r := core.jf_sim_card_group$attr_to_rowtype(p_attr);

   update core.sim_card_group set 
          name = l_r.name
   where 
          sim_card_group_id = l_r.sim_card_group_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sim_card_group$of_update"(numeric, text)
  OWNER TO adv;

--**************************************************************************************************