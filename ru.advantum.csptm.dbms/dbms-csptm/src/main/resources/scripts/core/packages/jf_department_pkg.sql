--DROP TYPE core.jf_department_type;

do $$
begin
  if not exists(select * from pg_type where typname = 'jf_department_type') THEN

     CREATE TYPE core.jf_department_type AS (
        r_department core.department,
        r_entity core.entity
     );


  end if;
end$$;

/*
   returns table ( r mon.sim,
                  id_sim_target mon.sim_target.id_sim_target%type,
                  id_tr mon.tr.id_tr%type) as $$
    begin
      return query
          with sim as (select jofl.jofl_pkg$extract_number(attr ,'ID_SIM', FALSE) as id_sim,
                              jofl.jofl_pkg$extract_number(attr ,'NUM', FALSE ) as num,
                              null::numeric as id_apn)
          select (sim)::mon.sim as r,
                 jofl.jofl_pkg$extract_number(attr ,'ID_SIM_TARGET', FALSE) as id_sim_target,
                 jofl.jofl_pkg$extract_number(attr ,'ID_TR', FALSE) as id_tr;
 */

--DROP FUNCTION core."jf_department_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core.jf_department_pkg$attr_to_rowtype(attr TEXT)
  RETURNS core.jf_department_type
AS
  $BODY$

  DECLARE
    r_return core.jf_department_type;
    r_department core.department;
    r_entity core.entity;
  BEGIN
    r_department.department_id := jofl.jofl_pkg$extract_varchar(attr, 'department_id', TRUE);
    r_department.parent_id := jofl.jofl_pkg$extract_varchar(attr, 'parent_id', TRUE);
    r_department.dt_begin := jofl.jofl_pkg$extract_date(attr, 'dt_begin', TRUE);
    r_department.dt_end := jofl.jofl_pkg$extract_date(attr, 'dt_end', TRUE);
    r_department.code := jofl.jofl_pkg$extract_number(attr, 'code', TRUE);
    r_entity.name_full := jofl.jofl_pkg$extract_varchar(attr, 'name_full', TRUE);
    r_entity.name_short := jofl.jofl_pkg$extract_varchar(attr, 'name_short', TRUE);
    r_entity.comment := jofl.jofl_pkg$extract_varchar(attr, 'comment', TRUE);
    r_entity.entity_id := jofl.jofl_pkg$extract_varchar(attr, 'department_id', TRUE);

    r_return.r_department := r_department;
    r_return.r_entity := r_entity;
    return r_return;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;
--*************************************************************************************************
CREATE OR REPLACE FUNCTION core.jf_department_pkg$of_rows(IN aid_account NUMERIC, OUT arows REFCURSOR, IN attr TEXT)
  RETURNS REFCURSOR
AS
  $BODY$
  DECLARE
  BEGIN
    OPEN arows FOR
    SELECT
      department_id,
      parent_id,
      dt_begin,
      dt_end,
      code,
      name_full,
      name_short,
      comment
    FROM CORE.DEPARTMENT JOIN core.entity on entity.entity_id = department.department_id;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;
--*************************************************************************************************
CREATE OR REPLACE FUNCTION core.jf_department_pkg$of_insert(aid_account NUMERIC, attr TEXT)
  RETURNS TEXT
AS
  $BODY$
  DECLARE
    r_return core.jf_department_type;
    r_department core.department;
    r_entity core.entity;
  BEGIN
    r_return := CORE.JF_DEPARTMENT_PKG$attr_to_rowtype(attr);
    r_department := r_return.r_department;
    r_entity := r_return.r_entity;

    r_entity.entity_id := nextval( 'core.entity_entity_id_seq' );

    INSERT INTO CORE.entity SELECT r_entity.* RETURNING entity_id INTO r_department.department_id;
    INSERT INTO CORE.DEPARTMENT SELECT r_department.*;
    RETURN NULL;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;
--*************************************************************************************************
CREATE OR REPLACE FUNCTION core.jf_department_pkg$of_update(aid_account NUMERIC, attr TEXT)
  RETURNS TEXT
AS
  $BODY$
  DECLARE
    r_return core.jf_department_type;
    r_department core.department;
    r_entity core.entity;
  BEGIN

    r_return := CORE.JF_DEPARTMENT_PKG$attr_to_rowtype(attr);
    r_department := r_return.r_department;
    r_entity := r_return.r_entity;

    UPDATE CORE.entity
    SET
      name_full = r_entity.name_full,
      name_short = r_entity.name_short,
      comment = r_entity.comment
    WHERE
      entity_id = r_entity.entity_id;

    UPDATE CORE.DEPARTMENT
    SET
      parent_id = r_department.parent_id,
      dt_begin  = r_department.dt_begin,
      dt_end    = r_department.dt_end,
      code      = r_department.code
    WHERE
      department_id = r_department.department_id;
    RETURN NULL;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;
--*************************************************************************************************
CREATE OR REPLACE FUNCTION core.jf_department_pkg$of_delete(aid_account NUMERIC, attr TEXT)
  RETURNS TEXT
AS
  $BODY$
  DECLARE
    r_return core.jf_department_type;
    r_department core.department;
  BEGIN

    r_return := CORE.JF_DEPARTMENT_PKG$attr_to_rowtype(attr);
    r_department := r_return.r_department;

    DELETE FROM CORE.DEPARTMENT
    WHERE
      department_id = r_department.department_id;

    DELETE FROM CORE.entity
    WHERE
      entity_id = r_department.department_id;

    RETURN NULL;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;

