create or replace function core.jf_driver_pkg$attr_to_rowtype(p_attr text)
    returns core.driver
language plpgsql
as $BODY$
declare
    l_r core.driver%rowtype;
begin
    l_r.driver_id := jofl.jofl_pkg$extract_number(p_attr, 'driver_id', true);
    l_r.depo_id := jofl.jofl_pkg$extract_number(p_attr, 'depo_id', true);
    l_r.driver_name := jofl.jofl_pkg$extract_varchar(p_attr, 'driver_name', true);
    l_r.driver_last_name := jofl.jofl_pkg$extract_varchar(p_attr, 'driver_last_name', true);
    l_r.driver_middle_name := jofl.jofl_pkg$extract_varchar(p_attr, 'driver_middle_name', true);
    l_r.tab_num := jofl.jofl_pkg$extract_varchar(p_attr, 'tab_num', true);
    l_r.dt_begin := jofl.jofl_pkg$extract_date(p_attr, 'dt_begin', true);
    l_r.dt_end := jofl.jofl_pkg$extract_date(p_attr, 'dt_end', true);
    l_r.sign_deleted := jofl.jofl_pkg$extract_varchar(p_attr, 'sign_deleted', true);
    l_r.contact_details:=jofl.jofl_pkg$extract_varchar(p_attr, 'contact_details', true);
    l_r.comment:=jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true);
    l_r.birth_year:=jofl.jofl_pkg$extract_number(p_attr, 'birth_year_int', true);
    l_r.experience:=jofl.jofl_pkg$extract_number(p_attr, 'experience', true);
    return l_r;
end;
$BODY$;

create or replace function core.jf_driver_pkg$of_rows(p_id_account numeric, out p_rows refcursor, p_attr text)
    returns refcursor
language plpgsql
as $BODY$
declare
    l_f_depo_id bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_depo_id', true);
begin
    open p_rows for
    select
        cd.driver_id,
        cd.depo_id,
        depo.name_full,
        cd.driver_name,
        cd.driver_last_name,
        cd.driver_middle_name,
        cd.tab_num,
        cd.dt_begin,
        cd.dt_end,
        cd.sign_deleted,
        lower(cd.sys_period)||'-'||upper(cd.sys_period) sys_period,
        cd.contact_details,
        cd.comment,
        cd.birth_year                                                                                    birth_year_int,
        to_date(cd.birth_year :: text, 'y')                                                           as birth_year,
        extract(year from (current_date :: date - cd.birth_year * '1 year' :: interval) :: timestamp) as age,
        cd.experience
    from core.driver cd
        left join core.entity depo on depo.entity_id = cd.depo_id
    where (cd.depo_id = any(l_f_depo_id) or array_length(l_f_depo_id,1) IS NULL)
    ;
end;
$BODY$;

create or replace function core.jf_driver_pkg$of_insert(p_id_account numeric, p_attr text)
    returns text
language plpgsql
as $BODY$
declare
    l_r core.driver%rowtype;
begin
    l_r := core.jf_driver_pkg$attr_to_rowtype(p_attr);

    insert into core.driver select l_r.*;

    return null;
end;
$BODY$;

create or replace function core.jf_driver_pkg$of_delete(p_id_account numeric, p_attr text)
    returns text
language plpgsql
as $BODY$
declare
    l_r core.driver%rowtype;
begin
    l_r := core.jf_driver_pkg$attr_to_rowtype(p_attr);

    delete from core.driver
    where driver_id = l_r.driver_id;

    return null;
end;
$BODY$;

create or replace function core.jf_driver_pkg$of_update(p_id_account numeric, p_attr text)
    returns text
language plpgsql
as $BODY$
declare
    l_r core.driver%rowtype;
begin
    l_r := core.jf_driver_pkg$attr_to_rowtype(p_attr);

    update core.driver
    set
        dt_begin           = l_r.dt_begin,
        dt_end             = l_r.dt_end,
        contact_details    = l_r.contact_details,
        comment            = l_r.comment,
        birth_year         = l_r.birth_year,
        experience         = l_r.experience
    where
        driver_id = l_r.driver_id;

    return null;
end;
$BODY$;

create or replace function core.jf_driver_pkg$get_text(p_driver_id int)
    returns text
language sql
as $$
select concat_ws(' ', driver_last_name, driver_name, driver_middle_name, tab_num)
from core.driver
where driver_id = p_driver_id;
$$