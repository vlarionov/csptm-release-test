﻿-- Function: core."jf_sensor2unit_model_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION core."jf_sensor2unit_model_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_sensor2unit_model_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.sensor2unit_model AS
$BODY$ 
declare 
   l_r core.sensor2unit_model%rowtype; 
begin
   l_r.sensor2unit_model_id := jofl.jofl_pkg$extract_number(p_attr, 'sensor2unit_model_id', true);
   l_r.equipment_type_unit_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_type_unit_id', true);
   l_r.equipment_type_sensor_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_type_sensor_id', true);
   l_r.num := jofl.jofl_pkg$extract_number(p_attr, 'num', true);
   l_r.is_required := jofl.jofl_pkg$extract_boolean(p_attr, 'is_required', true);

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sensor2unit_model_pkg$attr_to_rowtype"(text)
  OWNER TO adv;

--**********************************************************************************************************
-- Function: core."jf_sensor2unit_model_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_sensor2unit_model_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sensor2unit_model_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$
declare
  l_rf_db_method text;
  l_equipment_type_id SMALLINT;
  l_tr_schema_install_unit_id integer;

begin
  l_rf_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
  l_equipment_type_id  := jofl.jofl_pkg$extract_number(p_attr, 'equipment_type_id', true);
  l_tr_schema_install_unit_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_schema_install_unit_id', true);

 open p_rows for
      select
        s2um.sensor2unit_model_id,
        s2um.equipment_type_unit_id,
        s2um.equipment_type_sensor_id,
        s2um.num,
        s2um.is_required,
        et.name_short as sensor_type_name,
        et_unit.name_short as unit_type_name
      from core.sensor2unit_model s2um
      join core.equipment_type et on et.equipment_type_id = s2um.equipment_type_sensor_id
      join core.equipment_type et_unit on et_unit.equipment_type_id = s2um.equipment_type_unit_id
      WHERE l_rf_db_method is null
        OR (l_rf_db_method = 'core.tr_schema_install_detail'
            and s2um.equipment_type_unit_id = l_equipment_type_id
            and not  exists (select 1 from core.tr_schema_install_detail d where d.sensor2unit_model_id=s2um.sensor2unit_model_id and
                                                                                 d.tr_schema_install_unit_id    = l_tr_schema_install_unit_id))
  ;



end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sensor2unit_model_pkg$of_rows"(numeric, text)
  OWNER TO adv;

--**********************************************************************************************************
-- Function: core."jf_sensor2unit_model_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_sensor2unit_model_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sensor2unit_model_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.sensor2unit_model%rowtype;
begin 
   l_r := core.jf_sensor2unit_model_pkg$attr_to_rowtype(p_attr);

   delete from  core.sensor2unit_model where  sensor2unit_model_id = l_r.sensor2unit_model_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sensor2unit_model_pkg$of_delete"(numeric, text)
  OWNER TO adv;

--**********************************************************************************************************
-- Function: core."jf_sensor2unit_model_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_sensor2unit_model_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sensor2unit_model_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.sensor2unit_model%rowtype;
begin 
   l_r := core.jf_sensor2unit_model_pkg$attr_to_rowtype(p_attr);
   l_r.sensor2unit_model_id := nextval( 'core.sensor2unit_model_sensor2unit_model_id_seq' );
   
   insert into core.sensor2unit_model select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sensor2unit_model_pkg$of_insert"(numeric, text)
  OWNER TO adv;

--**********************************************************************************************************

-- Function: core."jf_sensor2unit_model_pkg$of_update"(numeric, text)

-- DROP FUNCTION core."jf_sensor2unit_model_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sensor2unit_model_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.sensor2unit_model%rowtype;
begin 
   l_r := core.jf_sensor2unit_model_pkg$attr_to_rowtype(p_attr);

   update core.sensor2unit_model set 
          equipment_type_unit_id = l_r.equipment_type_unit_id,
          equipment_type_sensor_id = l_r.equipment_type_sensor_id,
          num = l_r.num,
          is_required = l_r.is_required
   where 
          sensor2unit_model_id = l_r.sensor2unit_model_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sensor2unit_model_pkg$of_update"(numeric, text)
  OWNER TO adv;

--**********************************************************************************************************