CREATE OR REPLACE FUNCTION core.jf_tr_type_pkg$of_rows
   (p_id_account in numeric,
    p_rows out  refcursor, 
    p_attr in  text)
  RETURNS refcursor
AS
  $BODY$
declare
  l_rf_db_method TEXT;
  l_calculation_task_id bigint;
  l_tr_type_list bigint[];
begin
  l_rf_db_method := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE), '');
  l_calculation_task_id=coalesce(jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true), -1);
  
  l_tr_type_list := jofl.jofl_pkg$extract_narray(p_attr, 'F_TR_TYPE_LIST', TRUE);
  
  open p_rows for
     select
       tp.tr_type_id,
       tp.name,
       tp.short_name,
       tp.transport_kind_muid,
       tk.name as transport_kind_name
     from core.tr_type tp
       left join gis.ref_transport_kinds tk on tk.muid = tp.transport_kind_muid
     where (l_rf_db_method in ('ASD.RPT_SPEED_ON_ROUTE', 'asd.transport_kind2ct', 'asd.ct_speed_between_stops')
            and   ( l_rf_db_method = 'ASD.RPT_SPEED_ON_ROUTE' and l_calculation_task_id = -1) or
            ( (l_rf_db_method in ('asd.transport_kind2ct', 'asd.ct_speed_between_stops') and
               not exists (select 1 from asd.tr_type2ct tk2ct
               where tk2ct.tr_type_id = tp.tr_type_id
                     and tk2ct.calculation_task_id = l_calculation_task_id)
            )
            )
           )
           or (l_rf_db_method not in ('ASD.RPT_SPEED_ON_ROUTE', 'asd.transport_kind2ct', 'asd.ct_speed_between_stops')
               and tp.tr_type_id in (select adm.account_data_realm_pkg$get_tr_types_by_all(p_id_account))
               and (array_length(l_tr_type_list, 1) is null or tp.tr_type_id = any(l_tr_type_list)));

end;
$BODY$
LANGUAGE plpgsql VOLATILE;
