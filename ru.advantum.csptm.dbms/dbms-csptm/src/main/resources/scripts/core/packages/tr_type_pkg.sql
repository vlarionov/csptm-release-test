create or replace function core.tr_type_pkg$bus()
  returns core.tr_type.tr_type_id%type
immutable
language plpgsql
as $$
begin
  return 1;
end;
$$;

create or replace function core.tr_type_pkg$tb()
  returns core.tr_type.tr_type_id%type
immutable
language plpgsql
as $$
begin
  return 2;
end;
$$;

create or replace function core.tr_type_pkg$tm()
  returns core.tr_type.tr_type_id%type
immutable
language plpgsql
as $$
begin
  return 3;
end;
$$;

create or replace function core.tr_type_pkg$stm()
  returns core.tr_type.tr_type_id%type
immutable
language plpgsql
as $$
begin
  return 4;
end;
$$;