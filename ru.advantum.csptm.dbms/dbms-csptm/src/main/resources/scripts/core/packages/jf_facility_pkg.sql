﻿
-- DROP TYPE core.jf_facility_type;

do $$
begin
  if not exists(select * from pg_type where typname = 'jf_facility_type') THEN

     CREATE TYPE core.jf_facility_type AS
        (r_facility core.facility,
        r_entity core.entity);

     ALTER TYPE core.jf_facility_type
        OWNER TO adv;

  end if;
end$$;

--*************************************************************************************************
--DROP FUNCTION core."jf_facility_type_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_facility_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.jf_facility_type AS
$BODY$ 
declare 
   l_r_return core.jf_facility_type;
   l_r_facility core.facility%rowtype; 
   l_r_entity core.entity;
begin 
   l_r_facility.facility_id := jofl.jofl_pkg$extract_number(p_attr, 'facility_id', true);

   l_r_entity.name_full := jofl.jofl_pkg$extract_varchar(p_attr, 'name_full', TRUE);
   l_r_entity.name_short := jofl.jofl_pkg$extract_varchar(p_attr, 'name_short', TRUE);
   l_r_entity.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', TRUE);
   l_r_entity.entity_id := jofl.jofl_pkg$extract_varchar(p_attr, 'facility_id', TRUE);

    l_r_return.r_facility := l_r_facility;
    l_r_return.r_entity := l_r_entity;
   return l_r_return;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_facility_pkg$attr_to_rowtype"(text)
  OWNER TO adv;

--*************************************************************************************************
-- DROP FUNCTION core."jf_facility_pkg$of_delete"(numeric, text);
CREATE OR REPLACE FUNCTION core."jf_facility_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r_return core.jf_facility_type;
   l_r_facility core.facility%rowtype; 
begin 
   l_r_return := core.jf_facility_pkg$attr_to_rowtype(p_attr);
   l_r_facility := l_r_return.r_facility;

   delete from  core.facility where  facility_id = l_r_facility.facility_id;
   delete from  core.entity where  entity_id = l_r_facility.facility_id;
   return null;

  exception
  when 	foreign_key_violation then
    RAISE  EXCEPTION '<<Удаление невозможно! запись используется в других документах системы>>';

end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_facility_pkg$of_delete"(numeric, text)
  OWNER TO adv;

--*************************************************************************************************
CREATE OR REPLACE FUNCTION core.jf_facility_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
    l_r_return core.jf_facility_type;
    l_r_facility core.facility%rowtype; 
    l_r_entity core.entity;
    l_r_facility_type core.facility_type%rowtype;
   
begin 
    l_r_return := core.jf_facility_pkg$attr_to_rowtype(p_attr);
    l_r_facility := l_r_return.r_facility;
    l_r_entity := l_r_return.r_entity;

    l_r_entity.entity_id := nextval( 'core.entity_entity_id_seq' );
    l_r_facility.facility_id:= l_r_entity.entity_id;
    l_r_facility_type := core.jf_facility_type_pkg$attr_to_rowtype (p_attr);

    insert into core.entity select l_r_entity.* returning entity_id into l_r_facility.facility_id;
    insert into core.facility select l_r_facility.*;
    insert into core.facility2type select l_r_facility.facility_id, l_r_facility_type.facility_type_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--*************************************************************************************************

CREATE OR REPLACE FUNCTION core.jf_facility_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
Declare
  l_rf_db_method 			text			:= jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
  l_arr_model_block_type_id numeric ARRAY 	:= null;
  l_b_show_nothing			boolean 		:= false;
  l_f_unit_service_type_id	bigint          := jofl.jofl_pkg$extract_number(p_attr, 'f_unit_service_type_id', TRUE); 
  l_n_unit_service_type_id	bigint          := jofl.jofl_pkg$extract_number(p_attr, 'unit_service_type_id', TRUE); 
  l_n_producer_tr			BIGINT			:= core.jf_facility_type_pkg$cn_producer_tr();
begin
  l_arr_model_block_type_id := null;

  case l_rf_db_method
    when 'core.sim_card' then /**/
    l_arr_model_block_type_id := ARRAY[core.jf_facility_type_pkg$cn_mobile_operator()];
    when 'core.equipment_model' then /*Модель БО*/
    l_arr_model_block_type_id := ARRAY[core.jf_facility_type_pkg$cn_producer_bo()];
    when 'core.firmware' then /*Прошивка БО*/
    l_arr_model_block_type_id := ARRAY[core.jf_facility_type_pkg$cn_producer_firmware()];
    when 'core.sensor_dut' then /*ДУТ*/
    l_arr_model_block_type_id := ARRAY[core.jf_facility_type_pkg$cn_contractor()];
    when 'core.sensor_asmpp' then /*АСМПП*/
    l_arr_model_block_type_id := ARRAY[core.jf_facility_type_pkg$cn_contractor()];
    when 'core.hdd' then /*Жесткий диск*/
    l_arr_model_block_type_id := ARRAY[core.jf_facility_type_pkg$cn_contractor()];
    when 'core.monitor' then /*Монитор*/
    l_arr_model_block_type_id := ARRAY[core.jf_facility_type_pkg$cn_contractor()];
    when 'core.modem' then /*Модем*/
    l_arr_model_block_type_id := ARRAY[core.jf_facility_type_pkg$cn_contractor()];
    when 'core.mobile_tariff' then /*Оператор мобильной связи*/
    l_arr_model_block_type_id := ARRAY[core.jf_facility_type_pkg$cn_mobile_operator()];
    when 'core.sensor' then /*Датчики*/
    l_arr_model_block_type_id := ARRAY[core.jf_facility_type_pkg$cn_contractor()];
    when 'core.sensor_other' then /*Дополнительное оборудование*/
    l_arr_model_block_type_id := ARRAY[core.jf_facility_type_pkg$cn_contractor()];
    when 'core.unit_kbtob' then /*ББ КБТОБ*/
    l_arr_model_block_type_id := ARRAY[core.jf_facility_type_pkg$cn_contractor(),
                                       core.jf_facility_type_pkg$cn_service_company()];
    when 'core.unit_bnsr' then /*ББ БНСР*/
    l_arr_model_block_type_id := ARRAY[core.jf_facility_type_pkg$cn_contractor(),
                                       core.jf_facility_type_pkg$cn_service_company()];
    when 'core.unit_bnst' then /*ББ БНСТ*/
    l_arr_model_block_type_id := ARRAY[core.jf_facility_type_pkg$cn_contractor(),
                                       core.jf_facility_type_pkg$cn_service_company()];
	when 'core.unit' then /*ББ Другие*/
    l_arr_model_block_type_id := ARRAY[core.jf_facility_type_pkg$cn_contractor(),
                                       core.jf_facility_type_pkg$cn_service_company()];                                       
  else
    l_arr_model_block_type_id := null;
  end case;
  
  if l_f_unit_service_type_id is not null and l_f_unit_service_type_id = core.jf_unit_service_type_pkg$cn_no_service() then
  	 l_b_show_nothing := true;
  elsif l_n_unit_service_type_id is not null and l_n_unit_service_type_id = core.jf_unit_service_type_pkg$cn_no_service() then
  	 l_b_show_nothing := true;
  end if;

 open p_rows for
      select 
        f.facility_id,
        ent.name_full,
        ent.name_short,
        ent.comment
      from core.facility f
      join  core.entity ent on ent.entity_id = f.facility_id
      where
      (exists (select 1 from core.facility2type f2t
              where f2t.facility_id = f.facility_id 
              and (f2t.facility_type_id = any(l_arr_model_block_type_id) or (array_length(l_arr_model_block_type_id, 1) is null))
              and (l_rf_db_method <> 'core.tr_mark' or l_rf_db_method is null)
             ) 
      or 
      (exists (select null from core.facility2type f2t
               where f2t.facility_id = f.facility_id and f2t.facility_type_id = l_n_producer_tr and l_rf_db_method = 'core.tr_mark')  
               ) 
      /*or
      ((array_length(l_arr_model_block_type_id, 1) is null) and (l_rf_db_method <> 'core.tr_mark' or l_rf_db_method is null))*/
      )
      and not l_b_show_nothing
    ;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

--*************************************************************************************************

CREATE OR REPLACE FUNCTION core."jf_facility_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
  l_r_return core.jf_facility_type;
  l_r_entity core.entity;
  l_r_facility core.facility%rowtype;
begin 
    l_r_return := core.jf_facility_pkg$attr_to_rowtype(p_attr);
    l_r_facility := l_r_return.r_facility;
    l_r_entity := l_r_return.r_entity;

    UPDATE CORE.entity
    SET
      name_full = l_r_entity.name_full,
      name_short = l_r_entity.name_short,
      comment = l_r_entity.comment
    WHERE
      entity_id = l_r_entity.entity_id;


   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_facility_pkg$of_update"(numeric, text)
  OWNER TO adv;
--*************************************************************************************************