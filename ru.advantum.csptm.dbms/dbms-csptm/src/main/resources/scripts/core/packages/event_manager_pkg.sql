create or replace function core.event_manager_pkg$get_event_type_binding()
  returns setof core.event_type_binding
language plpgsql
as $$
begin
  return query
  select *
  from core.event_type_binding;
end;
$$;
