﻿-- Function: core."jf_sim_decommission_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION core."jf_sim_decommission_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_sim_decommission_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.sim_decommission AS
$BODY$ 
declare 
   l_r core.sim_decommission%rowtype; 
begin 
   l_r.sim_card_id := jofl.jofl_pkg$extract_number(p_attr, 'sim_card_id', true); 
   l_r.decommission_descr := jofl.jofl_pkg$extract_varchar(p_attr, 'decommission_descr', true); 
   l_r.is_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'is_deleted', true); 
   l_r.sim_decommission_id := jofl.jofl_pkg$extract_number(p_attr, 'sim_decommission_id', true); 
   l_r.decommission_reason_id := jofl.jofl_pkg$extract_varchar(p_attr, 'decommission_reason_id', true); 
   l_r.dt_end := jofl.jofl_pkg$extract_date(p_attr, 'dt_end', true); 
   l_r.account_create_id := jofl.jofl_pkg$extract_number(p_attr, 'account_create_id', true); 
   l_r.dt_deleted := jofl.jofl_pkg$extract_date(p_attr, 'dt_deleted', true); 
   l_r.account_delete_id := jofl.jofl_pkg$extract_number(p_attr, 'account_delete_id', true); 
   l_r.dt_create := jofl.jofl_pkg$extract_date(p_attr, 'dt_create', true); 

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sim_decommission_pkg$attr_to_rowtype"(text)
  OWNER TO adv;

--------------------------------------------------------------------
-- Function: core."jf_sim_decommission_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_sim_decommission_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sim_decommission_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.sim_decommission%rowtype;
begin 
   l_r := core.jf_sim_decommission_pkg$attr_to_rowtype(p_attr);

   l_r.sim_decommission_id := nextval('core.sim_decommission_sim_decommission_id_seq');
   l_r.account_create_id := p_id_account;
   l_r.dt_create := now();
   l_r.is_deleted :=false;
   l_r.dt_deleted :=null;
   l_r.account_delete_id :=null;

   insert into core.sim_decommission select l_r.*;
   return null;

end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sim_decommission_pkg$of_insert"(numeric, text)
  OWNER TO adv;

--------------------------------------------------------------------
-- Function: core."jf_sim_decommission_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_sim_decommission_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sim_decommission_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_r core.sim_decommission%rowtype;
begin
  l_r.sim_card_id := jofl.jofl_pkg$extract_number(p_attr, 'sim_card_id', true);

 open p_rows for
       select

         sd.sim_card_id,
         sd.decommission_descr,
         sd.is_deleted,
         sd.sim_decommission_id,
         sd.decommission_reason_id,
         sd.dt_end,
         sd.account_create_id,
         sd.dt_deleted,
         sd.account_delete_id,
         dt_create,
         dr.name as decommission_reason_name,
         concat_ws(' ',acr.last_name , acr.first_name) account_create_name,
         concat_ws(' ',adl.last_name , adl.first_name) account_deleted_name
       from core.sim_decommission sd
         join core.decommission_reason dr on dr.decommission_reason_id = sd.decommission_reason_id
         join core.account acr on acr.account_id = sd.account_create_id
         left join core.account adl on adl.account_id = sd.account_delete_id
       where sd.sim_card_id = l_r.sim_card_id;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sim_decommission_pkg$of_rows"(numeric, text)
  OWNER TO adv;

--------------------------------------------------------------------
-- Function: core."jf_sim_decommission_pkg$of_update"(numeric, text)

-- DROP FUNCTION core."jf_sim_decommission_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sim_decommission_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.sim_decommission%rowtype;
begin 
   l_r := core.jf_sim_decommission_pkg$attr_to_rowtype(p_attr);

   update core.sim_decommission set 
          decommission_descr = l_r.decommission_descr,
          decommission_reason_id = l_r.decommission_reason_id,
          dt_end = l_r.dt_end
   where 
          sim_decommission_id = l_r.sim_decommission_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sim_decommission_pkg$of_update"(numeric, text)
  OWNER TO adv;

--------------------------------------------------------------------
CREATE OR REPLACE FUNCTION core."jf_sim_decommission_pkg$of_cancel"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r core.sim_decommission%rowtype;
  l_equipment_status_id core.equipment_status.equipment_status_id%type;
begin
  l_r := core.jf_sim_decommission_pkg$attr_to_rowtype(p_attr);
  l_r.dt_deleted :=now();
  l_r.account_delete_id :=p_id_account;
  l_equipment_status_id:= jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_status_id', TRUE);

  update core.sim_decommission set
    is_deleted = not is_deleted,
    dt_deleted = l_r.dt_deleted,
    account_delete_id = l_r.account_delete_id
  where
    sim_card_id = l_r.sim_card_id
    and not is_deleted;

  update core.sim_card
  set equipment_status_id = l_equipment_status_id
  where sim_card_id = l_r.sim_card_id;

  return 'true';


end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_sim_decommission_pkg$of_cancel"(numeric, text)
OWNER TO adv;