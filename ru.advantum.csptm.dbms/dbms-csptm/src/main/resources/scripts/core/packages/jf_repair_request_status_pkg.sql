CREATE OR REPLACE FUNCTION core.jf_repair_request_status_pkg$of_rows(in p_id_account numeric, out p_rows refcursor, in p_attr text)
  RETURNS refcursor
AS
  $BODY$
  declare 
begin 
 open p_rows for 
      select 
        repair_request_status_id, 
        name
      from core.repair_request_status; 
end;
$BODY$
LANGUAGE plpgsql VOLATILE;
