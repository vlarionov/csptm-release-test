﻿CREATE OR REPLACE FUNCTION core."jf_firmware_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.firmware AS
$BODY$ 
declare 
   l_r core.firmware%rowtype; 
begin 
   l_r.facility_id := jofl.jofl_pkg$extract_number(p_attr, 'facility_id', true); 
   l_r.equipment_model_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_model_id', true); 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 
   l_r.version := jofl.jofl_pkg$extract_varchar(p_attr, 'version', true); 
   l_r.dt := jofl.jofl_pkg$extract_date(p_attr, 'dt', true); 
   l_r.file_name := jofl.jofl_pkg$extract_varchar(p_attr, 'file_name', true); 
   l_r.port := jofl.jofl_pkg$extract_varchar(p_attr, 'port', true); 
   l_r.ip_address := jofl.jofl_pkg$extract_varchar(p_attr, 'ip_address', true); 
   l_r.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true); 
   l_r.firmware_id := jofl.jofl_pkg$extract_number(p_attr, 'firmware_id', true); 
   l_r.url_ftp := jofl.jofl_pkg$extract_varchar(p_attr, 'url_ftp', true);
   l_r.path_ftp := jofl.jofl_pkg$extract_varchar(p_attr, 'path_ftp', true);
   l_r.ftp_user := jofl.jofl_pkg$extract_varchar(p_attr, 'ftp_user', true);
   l_r.ftp_user_password := jofl.jofl_pkg$extract_varchar(p_attr, 'ftp_user_password', true);
   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_firmware_pkg$attr_to_rowtype"(text)
  OWNER TO adv;

  /* *****************************************************************************************************************/
  CREATE OR REPLACE FUNCTION core."jf_firmware_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.firmware%rowtype;
begin 
   l_r := core.jf_firmware_pkg$attr_to_rowtype(p_attr);

   delete from  core.firmware where  firmware_id = l_r.firmware_id;

   return null;

  exception
  when 	foreign_key_violation then
    RAISE  EXCEPTION '<<Удаление невозможно! Запись используется в других документах системы>>';
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_firmware_pkg$of_delete"(numeric, text)
  OWNER TO adv;

/* *****************************************************************************************************************/

CREATE OR REPLACE FUNCTION core."jf_firmware_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.firmware%rowtype;
begin 
   l_r := core.jf_firmware_pkg$attr_to_rowtype(p_attr);
   l_r.firmware_id := nextval('core.firmware_firmware_id_seq');

   insert into core.firmware select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_firmware_pkg$of_insert"(numeric, text)
  OWNER TO adv;
  
/* *****************************************************************************************************************/

CREATE OR REPLACE FUNCTION core."jf_firmware_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_rf_db_method text;
  l_equipment_type_id core.equipment_type.equipment_type_id%type;
  l_arr_rf_db_method_filtr text array;
  l_ref_equipment_type_id SMALLINT;
  begin
  l_rf_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
  l_equipment_type_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_type_id', true);
  l_arr_rf_db_method_filtr  := '{"core.sensor", "core.sensor_asmpp","core.sensor_dut","core.hdd","core.monitor","core.modem"}';

  case l_rf_db_method
    when 'core.unit_bnst' then /*Для БНСТ выбираем только с типом БНСТ*/
    select core.jf_equipment_type_pkg$cn_type_bnst()::smallint into l_ref_equipment_type_id;
    when 'core.unit_bnsr' then /*Для БНСР выбираем только с типом БНСР*/
    select core.jf_equipment_type_pkg$cn_type_bnsr()::smallint into l_ref_equipment_type_id;
    when 'core.unit_kbtob' then /*Для КБТОБ выбираем только с типом КБТОБ*/
    select core.jf_equipment_type_pkg$cn_type_kbtob()::smallint into l_ref_equipment_type_id;
  else
    l_ref_equipment_type_id := null;
  end case;

 open p_rows for
 select
   fw.facility_id,
   ent.name_short as facility_name,
   fw.equipment_model_id,
   em.name as equipment_model_name,
   fw.name,
   fw.version,
   fw.dt,
   fw.file_name,
   fw.port,
   fw.ip_address,
   fw.comment,
   fw.firmware_id,
   fw.url_ftp,
   fw.path_ftp,
   fw.ftp_user,
   fw.ftp_user_password,
   et.name_short equipment_type_name_short
 from core.firmware fw
   join core.entity ent on ent.entity_id = fw.facility_id
   join core.equipment_model em on em.equipment_model_id = fw.equipment_model_id
   join core.equipment_type et on et.equipment_type_id =em.equipment_type_id
 where
    (l_rf_db_method is null or
    (l_rf_db_method != all (l_arr_rf_db_method_filtr)) or
    (l_rf_db_method =any (l_arr_rf_db_method_filtr) and  em.equipment_type_id = l_equipment_type_id )
   ) and
    (em.equipment_type_id = l_ref_equipment_type_id or l_ref_equipment_type_id is null)
  ;


end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_firmware_pkg$of_rows"(numeric, text)
  OWNER TO adv;

/* *****************************************************************************************************************/
CREATE OR REPLACE FUNCTION core."jf_firmware_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.firmware%rowtype;
begin 
   l_r := core.jf_firmware_pkg$attr_to_rowtype(p_attr);



   update core.firmware set 
          facility_id = l_r.facility_id, 
          equipment_model_id = l_r.equipment_model_id, 
          name = l_r.name, 
          version = l_r.version, 
          dt = l_r.dt, 
          file_name = l_r.file_name, 
          port = l_r.port, 
          ip_address = l_r.ip_address, 
          comment = l_r.comment,
          url_ftp = l_r.url_ftp,
          path_ftp = l_r.path_ftp,
          ftp_user = l_r.ftp_user,
          ftp_user_password = l_r.ftp_user_password
   where 
          firmware_id = l_r.firmware_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_firmware_pkg$of_update"(numeric, text)
  OWNER TO adv;

/* *****************************************************************************************************************/
