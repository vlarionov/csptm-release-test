﻿
create or replace function core.jf_graph_track_pkg$of_rows
  (p_account_id  in  numeric, 
   p_attr        in  text default '{}',
   p_rows        out refcursor)
returns refcursor 
as $$ declare
   l_tr_id    core.tr.tr_id%type := jofl.jofl_pkg$extract_number(p_attr,'F_ID_TR',  true);
   l_begin_dt timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'F_BEGIN_DT',  true);
   l_end_dt   timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'F_END_DT',  true);
begin
   open p_rows for
   select (g.event_time) :: text || '.0' as "GMTEVENTTIME",
          rdg.round_id :: text           as "ROUTE_TRAJECTORY_MUID",
          gsg.graph_section_id :: text   as "GRAPH_SECTION_MUID",
          g.order_num                    as "ORDER_NUM",
          g.graph_section_offset         as "GRAPH_SECTION_OFFSET",
          g.round_offset                 as "ROUND_OFFSET",
          tf.speed                       as "SPEED",
          rte.route_num                  as "ROUTE_NUM",
          rnd.code || '/' || act.action_type_name as "ROUND_TYPE",
          (case when rnd.move_direction_id = 1 then 'прямая' else 'обратная' end)::text as "DIRECTION",
          tto.tt_out_num                 as "OUT_NUM"
     from asd.tf_graph_link_ttb g
     join ttb.tt_action_round ttr on ttr.tt_action_id = g.tt_action_id
     join rts.round2gis rdg on rdg.round_id = ttr.round_id
     join rts.graph_section2gis gsg on gsg.graph_section_id = g.graph_section_id
     left join core.traffic tf on tf.packet_id = g.packet_id
                              and tf.tr_id = g.tr_id
                              and tf.event_time = g.event_time
     join rts.round rnd on rnd.round_id = ttr.round_id
     join ttb.action_type act on act.action_type_id = rnd.action_type_id
     join rts.route_variant rtv on rtv.route_variant_id = rnd.route_variant_id
     join rts.route rte on rte.route_id = rtv.route_id
     join ttb.tt_action tta on tta.tt_action_id = g.tt_action_id
     join ttb.tt_out tto on tto.tt_out_id = tta.tt_out_id
    where g.tr_id = l_tr_id
      and g.event_time between l_begin_dt and l_end_dt
    order by g.event_time;
end;
$$ language plpgsql;
