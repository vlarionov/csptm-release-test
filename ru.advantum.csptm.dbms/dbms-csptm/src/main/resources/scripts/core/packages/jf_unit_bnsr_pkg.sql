﻿-- DROP TYPE core.jf_unit_bnsr_type CASCADE;

do $$
begin
  if not exists(select * from pg_type where typname = 'jf_unit_bnsr_type') THEN

    CREATE TYPE core.jf_unit_bnsr_type AS
      (r_unit core.unit,
       r_equipment core.equipment,
       r_bnsr core.unit_bnsr);

    ALTER TYPE core.jf_unit_bnsr_type
      OWNER TO adv;

  end if;
end$$;

--*****************************************************************************
-- Function: core."jf_unit_bnsr_pkg$attr_to_rowtype"(text)

--DROP FUNCTION core."jf_unit_bnsr_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_unit_bnsr_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.jf_unit_bnsr_type AS
$BODY$ 
declare
  l_r_return core.jf_unit_bnsr_type;
  l_r_unit core.unit%rowtype;
  l_r_equipment core.equipment%rowtype;
  l_r_bnsr core.unit_bnsr%rowtype;


begin
  l_r_unit.unit_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
  l_r_unit.unit_num := jofl.jofl_pkg$extract_varchar(p_attr, 'unit_num', true);
  l_r_unit.hub_id := jofl.jofl_pkg$extract_number(p_attr, 'hub_id', true);

  l_r_bnsr.has_manipulator := jofl.jofl_pkg$extract_number(p_attr, 'has_manipulator', true);
  l_r_bnsr.channel_num := jofl.jofl_pkg$extract_number(p_attr, 'channel_num', true);
  l_r_bnsr.unit_bnsr_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
  l_r_unit.guarantee_end := jofl.jofl_pkg$extract_date(p_attr, 'guarantee_end', true);

  l_r_equipment.equipment_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
  l_r_equipment.serial_num := jofl.jofl_pkg$extract_varchar(p_attr, 'serial_num', true);
  l_r_equipment.firmware_id := jofl.jofl_pkg$extract_number(p_attr, 'firmware_id', true);
  l_r_equipment.unit_service_type_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_service_type_id', true);
  l_r_equipment.facility_id := jofl.jofl_pkg$extract_number(p_attr, 'facility_id', true);
  l_r_equipment.equipment_status_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_status_id', true);
  l_r_equipment.dt_begin := jofl.jofl_pkg$extract_date(p_attr, 'dt_begin', true);
  l_r_equipment.equipment_model_id := jofl.jofl_pkg$extract_varchar(p_attr, 'equipment_model_id', true);
  l_r_equipment.depo_id := jofl.jofl_pkg$extract_number(p_attr, 'depo_id', true);
  l_r_equipment.territory_id := jofl.jofl_pkg$extract_number(p_attr, 'territory_id', true);
  l_r_equipment.unit_service_facility_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_service_facility_id', true);
  l_r_equipment.has_diagnostic := jofl.jofl_pkg$extract_boolean(p_attr, 'has_diagnostic', true);
  l_r_equipment.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true);

  l_r_return.r_unit := l_r_unit;
  l_r_return.r_equipment := l_r_equipment;
  l_r_return.r_bnsr := l_r_bnsr;
  return l_r_return;


   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_unit_bnsr_pkg$attr_to_rowtype"(text)
  OWNER TO adv;
--*************************************************************************************************
-- Function: core."jf_unit_bnsr_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_unit_bnsr_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core.jf_unit_bnsr_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_limit 		int := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'f_rows_NUMBER', TRUE), '3000');
  l_unit_num 	text := jofl.jofl_pkg$extract_varchar(p_attr, 'f_unit_num_TEXT', TRUE);
  l_hub_id 		text := jofl.jofl_pkg$extract_varchar(p_attr, 'f_hub_id', TRUE);
  
  l_serial_num 	text := jofl.jofl_pkg$extract_varchar(p_attr, 'serial_num', TRUE);
  l_depo_id	 	core.depo.depo_id%type 		:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', true),jofl.jofl_pkg$extract_number(p_attr, 'depo_id', true));
  l_equipment_model_id 		core.equipment_model.equipment_model_id%type		:= jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_model_id', true);
  l_unit_service_type_id 	core.unit_service_type.unit_service_type_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_unit_service_type_id', true);
  l_arr_org_facility_id 	bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_org_facility_id', true);
  l_facility_id core.facility.facility_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_facility_id', true);
  l_firmware_id core.firmware.firmware_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_firmware_id', true);
--  l_decommission_reason_id 	core.decommission_reason.decommission_reason_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_decommission_reason_id', true);
  l_equipment_status_list   smallint[]  := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_equipment_status_id', true);
  l_depo_list 	bigint[] := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_depo_id', true);

  l_b_ins_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_insert_from_DT', true);
  l_e_ins_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_insert_to_DT', true);
--  l_b_decom_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_decom_from_DT', true);
--  l_e_decom_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_decom_to_DT', true);

  l_link_tr			smallint					:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_link_tr_INPLACE_K', true), -1);
--  l_decommission 	smallint					:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_decommission_INPLACE_K', true), 0);

begin
  open p_rows for
  with adata as (
      select
        et."ROW$POLICY",
        et.equipment_id,
        et.firmware_id,
        et.serial_num,
        et.firmware_name,
        et.unit_service_type_id,
        et.unit_service_type_name,
        et.facility_id,
        et.facility_name,
        et.equipment_status_id,
        et.equipment_status_name,
        --et.decommission_reason_id,
        --et.decommission_reason_name,
        et.dt_begin,
        --et.dt_end,
        u.guarantee_end,
        et.equipment_model_id,
        et.equipment_model_name,
        et.equipment_type_id,
        et.equipment_type_name,
        et.depo_name_full,
        et.depo_name_short,
        et.depo_id,
        et.territory_id,
        et.territory_name_full,
        et.territory_name_short,
        et.unit_service_facility_id,
        et.unit_service_facility_name_short,
        et.has_diagnostic,
        et.comment,
        u.unit_num,
        u.hub_id,
        hub.name       as hub_name,
        bnsr.channel_num,
        bnsr.has_manipulator,
        (  select  tr.garage_num  from core.unit2tr u2tr join core.tr tr on tr.tr_id = u2tr.tr_id
        where u2tr.unit_id = u.unit_id) as tr_name,
        last_call
      from core.unit_bnsr bnsr
        join core.unit u on u.unit_id = bnsr.unit_bnsr_id
        join core.v_equipment et on et.equipment_id = u.unit_id
        left join core.hub hub on hub.hub_id = u.hub_id
      where et.equipment_status_id != core.jf_equipment_status_pkg$cn_status_decommissioned()
        --подключаем фильтры
        and
        (
            (l_depo_id is null and array_length(l_depo_list, 1)  is null)
            or et.depo_id = any(l_depo_list)
            or et.depo_id = l_depo_id
        )
       /* and ((l_decommission=0 and et.equipment_status_id != core.jf_equipment_status_pkg$cn_status_decommissioned())
             or (l_decommission =1 and et.equipment_status_id = core.jf_equipment_status_pkg$cn_status_decommissioned()) ) */
        and (l_unit_num is null or u.unit_num like '%'||l_unit_num||'%')
        and (l_hub_id is null or l_hub_id = '' or u.hub_id = l_hub_id::int)
        and (l_equipment_model_id is null or et.equipment_model_id = l_equipment_model_id)
        and (l_unit_service_type_id is null or et.unit_service_type_id = l_unit_service_type_id)
        and (l_facility_id is null or et.facility_id = l_facility_id)
        and (et.unit_service_facility_id = any(l_arr_org_facility_id) or array_length(l_arr_org_facility_id, 1) is null )
        and (l_firmware_id is null or et.firmware_id =l_firmware_id )
      --  and (l_decommission_reason_id is null or et.decommission_reason_id = l_decommission_reason_id )
        and (et.equipment_status_id = any(l_equipment_status_list) or (array_length(l_equipment_status_list, 1)  is null))
        and (l_serial_num is null or et.serial_num =l_serial_num )
          
        and (l_b_ins_dt is null or et.dt_begin >= l_b_ins_dt)
        and (l_e_ins_dt is null or et.dt_begin<= l_e_ins_dt)

     --   and (l_b_decom_dt is null or et.dt_end >= l_b_decom_dt )
     --   and (l_e_decom_dt is null or et.dt_end<= l_e_decom_dt )
  )

  select *
  from adata
  where  (l_link_tr =-1 or coalesce(sign(tr_name),0) = l_link_tr )
  ;


end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;


--*************************************************************************************************
-- Function: core."jf_unit_bnsr_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_unit_bnsr_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_unit_bnsr_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r_return core.jf_unit_bnsr_type;
  l_r_unit core.unit%rowtype;
  l_r_equipment core.equipment%rowtype;
  l_r_bnsr core.unit_bnsr%rowtype;
  l_res text;
begin
  l_r_return := core.jf_unit_bnsr_pkg$attr_to_rowtype(p_attr);

  l_r_bnsr:= l_r_return.r_bnsr;
  l_r_unit:= l_r_return.r_unit;
  l_r_equipment := l_r_return.r_equipment;

  delete from  core.unit_bnsr where  unit_bnsr.unit_bnsr_id = l_r_bnsr.unit_bnsr_id;
  l_res :=  core.jf_unit_pkg$of_delete(p_id_account,  (row_to_json(l_r_unit)::jsonb||row_to_json(l_r_equipment)::jsonb)::text);

   return null;

end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_unit_bnsr_pkg$of_delete"(numeric, text)
  OWNER TO adv;

--*************************************************************************************************
-- Function: core."jf_unit_bnsr_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_unit_bnsr_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core.jf_unit_bnsr_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
  l_r_return core.jf_unit_bnsr_type;
  l_r_unit core.unit%rowtype;
  l_r_equipment core.equipment%rowtype;
  l_r_bnsr core.unit_bnsr%rowtype;
  l_res text;
begin
  l_r_return := core.jf_unit_bnsr_pkg$attr_to_rowtype(p_attr);

  l_r_bnsr:= l_r_return.r_bnsr;
  l_r_unit:= l_r_return.r_unit;
  
  l_r_equipment := l_r_return.r_equipment;
  
  if l_r_equipment.unit_service_type_id = core.jf_unit_service_type_pkg$cn_no_service() then
  	 l_r_equipment.unit_service_facility_id = null;
  end if;

  l_r_bnsr.unit_bnsr_id:= nextval( 'core.equipment_equipment_id_seq');
  l_r_unit.unit_id:= l_r_bnsr.unit_bnsr_id;
  l_r_equipment.equipment_id:= l_r_bnsr.unit_bnsr_id;

  l_res := core.jf_unit_pkg$of_insert(p_id_account,  (row_to_json(l_r_unit)::jsonb||row_to_json(l_r_equipment)::jsonb)::text);
  
  insert into core.unit_bnsr select l_r_bnsr.*;

  return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

--*************************************************************************************************
-- Function: core."jf_unit_bnsr_pkg$of_update"(numeric, text)

-- DROP FUNCTION core."jf_unit_bnsr_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_unit_bnsr_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r_return core.jf_unit_bnsr_type;
  l_r_unit core.unit%rowtype;
  l_r_equipment core.equipment%rowtype;
  l_r_bnsr core.unit_bnsr%rowtype;
  l_res text;
begin
  l_r_return := core.jf_unit_bnsr_pkg$attr_to_rowtype(p_attr);

  l_r_bnsr:= l_r_return.r_bnsr;
  l_r_unit:= l_r_return.r_unit;
  l_r_equipment := l_r_return.r_equipment;
  
  if l_r_equipment.unit_service_type_id = core.jf_unit_service_type_pkg$cn_no_service() then
  	 l_r_equipment.unit_service_facility_id = null;
  end if;
  
  l_res:= core.jf_unit_pkg$of_update(p_id_account, (row_to_json(l_r_unit)::jsonb||row_to_json(l_r_equipment)::jsonb)::text);

  update core.unit_bnsr
  set channel_num = l_r_bnsr.channel_num,
    has_manipulator = l_r_bnsr.has_manipulator
  where unit_bnsr_id = l_r_bnsr.unit_bnsr_id;


  return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_unit_bnsr_pkg$of_update"(numeric, text)
  OWNER TO adv;
--****************************************************************************************************************

CREATE OR REPLACE FUNCTION core."jf_unit_bnsr_pkg$of_link2tr"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
begin
  return core.jf_unit_pkg$of_link2tr(p_id_account, p_attr);
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_unit_bnsr_pkg$of_link2tr"(numeric, text)
OWNER TO adv;
--***************************************************************************************************************
CREATE OR REPLACE FUNCTION core."jf_unit_bnsr_pkg$of_decommission"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
begin
  return core.jf_equipment_pkg$of_decommission(p_id_account, p_attr);
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_unit_bnsr_pkg$of_decommission"(numeric, text)
OWNER TO adv;
--*************************************************************************************************
--изменение статуса
CREATE OR REPLACE FUNCTION core."jf_unit_bnsr_pkg$of_change_status"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
begin
  return core.jf_equipment_pkg$of_change_status(p_id_account, p_attr);
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_unit_bnsr_pkg$of_change_status"(numeric, text)
OWNER TO adv;

--****************************************************************************************
--отмена списания
CREATE OR REPLACE FUNCTION core."jf_unit_bnsr_pkg$of_decommission_cancel"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
begin
  return core.jf_equipment_pkg$of_decommission_cancel(p_id_account, p_attr);
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_unit_bnsr_pkg$of_decommission_cancel"(numeric, text)
OWNER TO adv;


CREATE OR REPLACE FUNCTION core.jf_unit_bnsr_pkg$of_diag(
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare

   ln_cnt smallint;
   lt_res text;
   l_request_param text;
   l_channel_num int;
   l_unit_id numeric;
   cnSTART int := 20;
begin
   l_unit_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id');

   select count(1)
   into ln_cnt
   from cmnd.bnsr_request_list
   where unit_bnsr_id = l_unit_id
   and time_finish is null;

   if ln_cnt > 0 then
     lt_res := 'Мониторинг данного блока уже активирован!';
     return jofl.jofl_util$cover_result('error', lt_res);
   end if;


   l_request_param = mnt.jf_bnsr_request_list_pkg$get_param(l_unit_id);

   select cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd_to_rabbit(p_id_account, l_unit_id, cnSTART, l_request_param) into lt_res;

   update core.unit_bnsr
   set last_call = now()
   where unit_bnsr_id = l_unit_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;


CREATE OR REPLACE FUNCTION core.jf_unit_bnsr_pkg$of_stop_diag(
)
RETURNS text AS
$body$
declare
   ln_cnt smallint;
   lt_res text;
   l_request_param text;
   l_channel_num int;
   l_unit_id numeric;
   rec record;
   cnSTART int := 20;
   cnSTOP int := 21;
begin

   FOR rec IN
        select unit_id
        from cmnd.cmnd_request join cmnd.cmnd_response on (cmnd_request.cmnd_request_id = cmnd_response.cmnd_request_id)
        where cmnd_ref_id = cnSTART and cmnd_request.request_time < now() - INTERVAL '1 minutes'
        and not exists (select 1  from cmnd.cmnd_request a where a.unit_id = cmnd_request.unit_id
        and a.cmnd_ref_id = cnSTOP and a.request_time > cmnd_request.request_time)
        GROUP BY unit_id
   LOOP

         select count(1)
           into ln_cnt
           from cmnd.bnsr_request_list
           where unit_bnsr_id = rec.unit_id
           and time_finish is null;

           if ln_cnt = 0 then
                l_request_param = mnt.jf_bnsr_request_list_pkg$get_param(rec.unit_id);

                select cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd_to_rabbit(2, rec.unit_id, cnSTOP, l_request_param) into lt_res;
           end if;
    END LOOP;


   return null;
end;
$body$
LANGUAGE 'plpgsql';

