-- классы вместимости ТС, по таблице core.tr_capacity

create or replace function core."tr_capacity_pkg$small"() RETURNS smallint
LANGUAGE sql
IMMUTABLE
as $$
  select 52::smallint;
$$;

create or replace function core."tr_capacity_pkg$middle"() RETURNS smallint
LANGUAGE sql
IMMUTABLE
as $$
  select 55::smallint;
$$;

create or replace function core."tr_capacity_pkg$big"() RETURNS smallint
LANGUAGE sql
IMMUTABLE
as $$
  select 54::smallint;
$$;

create or replace function core."tr_capacity_pkg$huge"() RETURNS smallint
LANGUAGE sql
IMMUTABLE
as $$
  select 53::smallint;
$$;
