﻿-- DROP TYPE core.jf_sensor_type;

do $$
begin
  if not exists(select * from pg_type where typname = 'jf_sensor_type') THEN

    CREATE TYPE core.jf_sensor_type AS
      (r_sensor core.sensor,
      r_equipment core.equipment);

    ALTER TYPE core.jf_sensor_type
      OWNER TO adv;

  end if;
end$$;

--************************************************************************************************************
-- Function: core."jf_sensor_pkg$attr_to_rowtype"(text)

DROP FUNCTION core."jf_sensor_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_sensor_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.jf_sensor_type AS
$BODY$ 
declare
  l_r_return core.jf_sensor_type;
  l_r_sensor core.sensor%rowtype;
  l_r_equipment core.equipment%rowtype;
begin
  l_r_sensor.sensor_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);

  l_r_equipment.firmware_id := jofl.jofl_pkg$extract_number(p_attr, 'firmware_id', true);
  l_r_equipment.unit_service_type_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_service_type_id', true);
  l_r_equipment.facility_id := jofl.jofl_pkg$extract_number(p_attr, 'facility_id', true);
  l_r_equipment.equipment_status_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_status_id', true);
  l_r_equipment.dt_begin := jofl.jofl_pkg$extract_date(p_attr, 'dt_begin', true);
  l_r_equipment.equipment_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
  l_r_equipment.serial_num := jofl.jofl_pkg$extract_varchar(p_attr, 'serial_num', true);
  l_r_equipment.equipment_model_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_model_id', true);
  l_r_equipment.depo_id := jofl.jofl_pkg$extract_number(p_attr, 'depo_id', true);
  l_r_equipment.territory_id := jofl.jofl_pkg$extract_number(p_attr, 'territory_id', true);
  l_r_equipment.unit_service_facility_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_service_facility_id', true);
  l_r_equipment.has_diagnostic := jofl.jofl_pkg$extract_boolean(p_attr, 'has_diagnostic', true);
  l_r_equipment.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true);

  l_r_return.r_sensor := l_r_sensor;
  l_r_return.r_equipment := l_r_equipment;
  return l_r_return;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sensor_pkg$attr_to_rowtype"(text)
  OWNER TO adv;
--**************************************************************************************************************
-- Function: core."jf_sensor_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_sensor_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core.jf_sensor_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_rf_db_method 				text;

  l_arr_rf_db_method_sensor2tr 	text array;

  l_depo_id	 					core.depo.depo_id%type 		:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', true), jofl.jofl_pkg$extract_number(p_attr, 'depo_id', true));
  l_equipment_type_id 			core.equipment_type.equipment_type_id%type		:= jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_type_id', true);
  l_equipment_model_id 			core.equipment_model.equipment_model_id%type		:= jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_model_id', true);
  l_unit_service_type_id 		core.unit_service_type.unit_service_type_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_unit_service_type_id', true);
  l_arr_org_facility_id 		bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_org_facility_id', true);
  l_facility_id 				core.facility.facility_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_facility_id', true);
  l_firmware_id 				core.firmware.firmware_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_firmware_id', true);
 -- l_decommission_reason_id 		core.decommission_reason.decommission_reason_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_decommission_reason_id', true);
  l_equipment_status_list    	smallint[]  := jofl.jofl_pkg$extract_tarray(p_attr, 'f_equipment_status_id', true);
  l_serial_num 					text := jofl.jofl_pkg$extract_varchar(p_attr, 'serial_num', true);
  l_depo_list    				bigint[]  := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_depo_id', true);

  l_b_ins_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_insert_from_DT', true);
  l_e_ins_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_insert_to_DT', true);
--  l_b_decom_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_decom_from_DT', true);
--  l_e_decom_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_decom_to_DT', true);

  l_link_bb			smallint					:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_link_bb_INPLACE_K', true), -1);
  l_link_tr			smallint					:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_link_tr_INPLACE_K', true), -1);
 -- l_decommission 	smallint					:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_decommission_INPLACE_K', true), 0);

begin
  l_rf_db_method 				:= jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
  l_arr_rf_db_method_sensor2tr 	:= '{"core.sensor2tr"}';

 open p_rows for
  with adata as (
           select
             et."ROW$POLICY",
             et.equipment_id,
             et.firmware_id,
             et.serial_num,
             et.firmware_name,
             et.unit_service_type_id,
             et.unit_service_type_name,
             et.facility_id,
             et.facility_name,
             et.equipment_status_id,
             et.equipment_status_name,
          --   et.decommission_reason_id,
          --   et.decommission_reason_name,
             et.dt_begin,
          --   et.dt_end,
             et.equipment_model_id,
             et.equipment_model_name,
             et.equipment_type_id,
             et.equipment_type_name,

             et.depo_name_full,
             et.depo_name_short,
             et.depo_id,
             et.territory_id,
             et.territory_name_full,
             et.territory_name_short,
             et.unit_service_facility_id,
             et.unit_service_facility_name_short,
             et.has_diagnostic,
             et.comment,
             s.sensor_num,
             ( select array_to_string(array_agg(ett.name_short || ' '||  eqt.serial_num), ','::text) as array_to_string
               from core.sensor2unit
                 join core.equipment eqt on sensor2unit.unit_id = eqt.equipment_id
                 join core.equipment_model em on em.equipment_model_id = eqt.equipment_model_id
                 join core.equipment_type ett on ett.equipment_type_id = em.equipment_type_id
               where sensor2unit.sensor_id = et.equipment_id
               group by sensor2unit.sensor_id) as unit_name,
             (  select tr.garage_num
                from core.sensor2tr s2tr
                  join core.tr tr on tr.tr_id = s2tr.tr_id
                where s2tr.sensor_id = et.equipment_id) as tr_name

           from
             core.sensor s
             join core.v_equipment et on et.equipment_id = s.sensor_id
     where et.equipment_status_id != core.jf_equipment_status_pkg$cn_status_decommissioned()
       and
          ((
            (
            l_rf_db_method is null or
            l_rf_db_method  != all(l_arr_rf_db_method_sensor2tr) )
           and (et.equipment_type_id not in (core.jf_equipment_type_pkg$cn_type_asmpp(), core.jf_equipment_type_pkg$cn_type_dut()))
           and (et.depo_id =l_depo_id or et.depo_id = any(l_depo_list) )

           )

            or
           (l_rf_db_method = any(l_arr_rf_db_method_sensor2tr) and
           --датчик не привязан к тс
           not exists  (select 1 from core.sensor2tr s2tr  where s2tr.sensor_id = et.equipment_id )

           )
            )


        --подключаем фильтры
         /* and ((l_decommission=0 and et.equipment_status_id != core.jf_equipment_status_pkg$cn_status_decommissioned())
            or (l_decommission =1 and et.equipment_status_id = core.jf_equipment_status_pkg$cn_status_decommissioned()) ) */
          and (l_equipment_type_id is null or et.equipment_type_id  = l_equipment_type_id)
          and (l_equipment_model_id is null or et.equipment_model_id = l_equipment_model_id)
          and (l_unit_service_type_id is null or et.unit_service_type_id = l_unit_service_type_id)
          and (l_facility_id is null or et.facility_id = l_facility_id)
          and (et.unit_service_facility_id = any(l_arr_org_facility_id) or array_length(l_arr_org_facility_id, 1) is null )
          and (l_firmware_id is null or et.firmware_id =l_firmware_id )
       --   and (l_decommission_reason_id is null or et.decommission_reason_id = l_decommission_reason_id )
          and (et.equipment_status_id = any(l_equipment_status_list) or (array_length(l_equipment_status_list, 1)  is null))
          and (l_serial_num is null or et.serial_num =l_serial_num )

          and (l_b_ins_dt is null or et.dt_begin >= l_b_ins_dt)
          and (l_e_ins_dt is null or et.dt_begin<= l_e_ins_dt)

      --    and (l_b_decom_dt is null or et.dt_end >= l_b_decom_dt )
      --    and (l_e_decom_dt is null or et.dt_end<= l_e_decom_dt )
     )
 select *
 from adata
 where (l_link_bb=-1 or coalesce(sign(length(unit_name)),0) = l_link_bb )
   and (l_link_tr =-1 or coalesce(sign(tr_name),0) = l_link_tr )
;


end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

--**************************************************************************************************************
-- Function: core."jf_sensor_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_sensor_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sensor_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare
  l_r_return core.jf_sensor_type;
  l_r_sensor core.sensor%rowtype;
  l_r_equipment core.equipment%rowtype;
  l_res text;
begin
  l_r_return := core.jf_sensor_pkg$attr_to_rowtype(p_attr);

  l_r_sensor:= l_r_return.r_sensor;
  l_r_equipment := l_r_return.r_equipment;

  delete from  core.sensor where  sensor_id = l_r_sensor.sensor_id;
  l_res :=  core.jf_equipment_pkg$of_delete(p_id_account,  row_to_json(l_r_equipment)::text);

  return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sensor_pkg$of_delete"(numeric, text)
  OWNER TO adv;

--**************************************************************************************************************
-- Function: core."jf_sensor_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_sensor_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core.jf_sensor_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
  l_r_return core.jf_sensor_type;
  l_r_sensor core.sensor%rowtype;
  l_r_equipment core.equipment%rowtype;
  l_res text;
  l_sensor_count smallint;
begin
  l_r_return := core.jf_sensor_pkg$attr_to_rowtype(p_attr);

  l_r_sensor:= l_r_return.r_sensor;
  l_r_equipment := l_r_return.r_equipment;
  
  if l_r_equipment.unit_service_type_id = core.jf_unit_service_type_pkg$cn_no_service() then
  	 l_r_equipment.unit_service_facility_id = null;
  end if;

  l_sensor_count:= jofl.jofl_pkg$extract_number(p_attr, 'sensor_count', true);
  if l_sensor_count = 0 then l_sensor_count:=1; end if;


  for i in 1..l_sensor_count loop
      l_r_equipment.equipment_id:= nextval( 'core.equipment_equipment_id_seq' );

      l_r_sensor.sensor_id:= l_r_equipment.equipment_id;

      l_res := core.jf_equipment_pkg$of_insert(p_id_account, row_to_json(l_r_equipment)::text);

      insert into core.sensor select l_r_sensor.*;
  end loop;

  return null;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;


--**************************************************************************************************************
-- Function: core."jf_sensor_pkg$of_update"(numeric, text)

-- DROP FUNCTION core."jf_sensor_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core.jf_sensor_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
  l_r_return core.jf_sensor_type;
  l_r_sensor core.sensor%rowtype;
  l_r_equipment core.equipment%rowtype;
  l_res text;
begin 

  l_r_return := core.jf_sensor_pkg$attr_to_rowtype(p_attr);

  l_r_sensor:= l_r_return.r_sensor;
  l_r_equipment := l_r_return.r_equipment;
  
  if l_r_equipment.unit_service_type_id = core.jf_unit_service_type_pkg$cn_no_service() then
  	 l_r_equipment.unit_service_facility_id = null;
  end if;

  l_res:= core.jf_equipment_pkg$of_update(p_id_account, row_to_json(l_r_equipment)::text);

  /*update core.sensor SET
where sensor_id= l_r_sensor.sensor_id;*/

  return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

--***************************************************************************************************************

CREATE OR REPLACE FUNCTION core."jf_sensor_pkg$of_link2tr"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r core.sensor2tr%ROWTYPE;
  l_equipment_id BIGINT;
  l_equipment_type_name core.equipment_type.name_full%type;
  l_serial_num core.equipment.serial_num%type;

  l_tr_id BIGINT;
  l_res text;
begin
  l_equipment_id:=jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', TRUE);
  l_tr_id:= jofl.jofl_pkg$extract_number(p_attr, 'F_tr_id', TRUE);

  l_equipment_type_name:=  jofl.jofl_pkg$extract_varchar(p_attr, 'equipment_type_name', TRUE);
  l_serial_num:=  jofl.jofl_pkg$extract_varchar(p_attr, 'serial_num', TRUE);

  if exists(select 1 from core.sensor2tr where sensor_id = l_equipment_id) THEN


    RAISE  EXCEPTION '<<Датчик % cерийный номер % уже установлен на ТС>>',l_equipment_type_name, l_serial_num ;

 end IF ;

  l_res:= '{"equipment_id":'||l_equipment_id::text||
          ',"tr_id":'||l_tr_id::text||'}';

  l_res:= core.jf_sensor2tr_pkg$of_insert(p_id_account, l_res);
  return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_sensor_pkg$of_link2tr"(numeric, text)
OWNER TO adv;
--***************************************************************************************************************
--списание
CREATE OR REPLACE FUNCTION core."jf_sensor_pkg$of_decommission"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
begin
  return core.jf_equipment_pkg$of_decommission(p_id_account, p_attr);
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_sensor_pkg$of_decommission"(numeric, text)
OWNER TO adv;

--***************************************************************************************************************
CREATE OR REPLACE FUNCTION core."jf_sensor_pkg$of_link2unit"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r core.sensor2unit%rowtype;
  l_equipment_model_name core.equipment_model.name%type;
  l_equipment_type_id bigint;
  l_equipment_status_id bigint;
begin
  l_r.sensor_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
  l_r.unit_id:= jofl.jofl_pkg$extract_number(p_attr, 'F_equipment_id', true);

  l_equipment_model_name:=  jofl.jofl_pkg$extract_varchar(p_attr, 'equipment_model_name', TRUE);
  l_equipment_type_id:=  jofl.jofl_pkg$extract_number(p_attr, 'equipment_type_id', TRUE);
  l_equipment_status_id:= jofl.jofl_pkg$extract_number(p_attr, 'equipment_status_id', TRUE);


  --ТС ББ и Датчика не совпадают
  --Все входы ББ, определенные  для датчика, заняты

  if l_equipment_status_id = core.jf_equipment_status_pkg$cn_status_decommissioned() then
    RAISE EXCEPTION '<<Устанавливаемый датчик не может быть списан>>';
  end if;

  if l_r.unit_id is null then
    RAISE EXCEPTION '<<Выберите ББ из списка доступных>>';
  end if;

  if exists( select 1 from core.sensor2unit where sensor_id = l_r.sensor_id and unit_id= l_r.unit_id) THEN
    RAISE EXCEPTION '<<Датчик уже привязан к этому блоку>>';
  END IF;

  select into l_r.sensor_num min(s2um.num)
  from core.sensor2unit_model s2um
  where equipment_type_sensor_id=l_equipment_type_id
        and equipment_type_unit_id   in  (select emunit.equipment_type_id
                                          from core.equipment equnit
                                          join core.equipment_model emunit on emunit.equipment_model_id = equnit.equipment_model_id
                                          where equnit.equipment_id = l_r.unit_id) --тип блока
        and not exists (select 1 from core.sensor2unit t where  t.unit_id = l_r.unit_id and t.sensor_num = s2um.num );

  if not found or l_r.sensor_num is null then
    RAISE EXCEPTION '<<Не удалось определить номер входа в схеме подключения либо все входы на блоке заняты.  Датчик:%>>',l_equipment_model_name ;
  end if;


  return core.jf_sensor2unit_pkg$of_insert(p_id_account, row_to_json(l_r)::text);
  --insert into core.sensor2unit select l_r.*;
  --return  null;

end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_sensor_pkg$of_link2unit"(numeric, text)
OWNER TO adv;

--****************************************************************************************
--изменение статуса
CREATE OR REPLACE FUNCTION core."jf_sensor_pkg$of_change_status"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
begin
  return core.jf_equipment_pkg$of_change_status(p_id_account, p_attr);
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_sensor_pkg$of_change_status"(numeric, text)
OWNER TO adv;

--****************************************************************************************
--отмена списания
CREATE OR REPLACE FUNCTION core."jf_sensor_pkg$of_decommission_cancel"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
begin
  return core.jf_equipment_pkg$of_decommission_cancel(p_id_account, p_attr);
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_sensor_pkg$of_decommission_cancel"(numeric, text)
OWNER TO adv;


