﻿-- Function: core."jf_sensor2unit_pkg$attr_to_rowtype"(text)

 DROP FUNCTION core."jf_sensor2unit_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_sensor2unit_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.sensor2unit AS
$BODY$ 
declare 
   l_r core.sensor2unit%rowtype;
begin 
   l_r.sensor_id := jofl.jofl_pkg$extract_number(p_attr, 'sensor_id', true);
   l_r.sensor_num := jofl.jofl_pkg$extract_number(p_attr, 'sensor_num', true);
   l_r.unit_id := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true), jofl.jofl_pkg$extract_number(p_attr, 'unit_id', true));

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sensor2unit_pkg$attr_to_rowtype"(text)
  OWNER TO adv;

--********************************************************************************************************
-- Function: core."jf_sensor2unit_pkg$of_rows"(numeric, text)

 DROP FUNCTION core."jf_sensor2unit_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sensor2unit_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_r core.sensor2unit%rowtype;
  l_rf_db_method text;
  l_equipment_type_id_array  smallint[];
  FUEL_SENSOR CONSTANT text := 'mnt.fuel_sensor_data';
  BINARY_SENSOR CONSTANT text := 'mnt.binary_sensor_data';
  IRMA_SENSOR CONSTANT text := 'mnt.irma_sensor_data';
  TERMO_SENSOR CONSTANT text := 'mnt.termo_data';
begin
  l_r.unit_id := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true),jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_id', true));
  l_rf_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);

  if (l_rf_db_method = FUEL_SENSOR) then
    l_equipment_type_id_array := ARRAY[core.jf_equipment_type_pkg$cn_type_dut()::smallint];
  elsif (l_rf_db_method = BINARY_SENSOR) then
    l_equipment_type_id_array := ARRAY[core.jf_equipment_type_pkg$cn_type_alarm_btn()::smallint,
                                        core.jf_equipment_type_pkg$cn_type_skah()::smallint,
                                        core.jf_equipment_type_pkg$cn_type_fire()::smallint,
                                        core.jf_equipment_type_pkg$cn_type_dvzh()];
  elsif (l_rf_db_method = IRMA_SENSOR) then
    l_equipment_type_id_array := ARRAY[core.jf_equipment_type_pkg$cn_type_asmpp()::smallint];
  elsif (l_rf_db_method = TERMO_SENSOR) then
    l_equipment_type_id_array := ARRAY[core.jf_equipment_type_pkg$cn_type_temperature()::smallint];
  else
    l_equipment_type_id_array := null;
  end if;

--attr:{"f_bgn_DT":"2017-08-29 21:00:00.0","f_tr_id":33931,"f_end_DT":"2017-08-30 20:59:59.0","f_equipment_id":24063,"f_sensor_id":"","RF_DB_METHOD":"mnt.fuel_sensor_data"}


 open p_rows for
 select
   s2u.sensor_id,
   s2u.sensor_num,
   s2u.unit_id as equipment_id,
   et.serial_num,
   et.firmware_id,
   et.firmware_name,
   et.unit_service_type_id,
   et.unit_service_type_name,
   et.facility_id,
   et.facility_name,
   et.equipment_status_id,
   et.equipment_status_name,
   et.decommission_reason_id,
   et.decommission_reason_name,
   et.dt_begin,
   et.dt_end,
   et.equipment_model_id,
   et.equipment_model_name,
   et.equipment_type_name,
   et.equipment_type_id,
   et.depo_name_full,
   et.depo_name_short,
   et_unit.equipment_model_id as  unit_equipment_model_id,
   unit_em.equipment_type_id as unit_equipment_type_id,
   lower(s2u.sys_period) as dt_link
 from core.sensor2unit s2u
   join core.sensor s on s.sensor_id = s2u.sensor_id
   join core.v_equipment et on et.equipment_id = s2u.sensor_id
   join core.equipment et_unit  on et_unit.equipment_id = s2u.unit_id
   join core.equipment_model unit_em on unit_em.equipment_model_id = et_unit.equipment_model_id
  where s2u.unit_id = l_r.unit_id
        and (
            (et.equipment_type_id = any(l_equipment_type_id_array))
                or
            (array_length(l_equipment_type_id_array, 1)  is null)
            );

end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sensor2unit_pkg$of_rows"(numeric, text)
  OWNER TO adv;

--********************************************************************************************************
-- Function: core."jf_sensor2unit_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_sensor2unit_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sensor2unit_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.sensor2unit%rowtype;
begin 
   l_r := core.jf_sensor2unit_pkg$attr_to_rowtype(p_attr);

   delete from  core.sensor2unit where  sensor_id = l_r.sensor_id and unit_id = l_r.unit_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sensor2unit_pkg$of_delete"(numeric, text)
  OWNER TO adv;

--********************************************************************************************************
-- Function: core."jf_sensor2unit_pkg$of_insert"(numeric, text)

 DROP FUNCTION core."jf_sensor2unit_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sensor2unit_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.sensor2unit%rowtype;
begin
   l_r := core.jf_sensor2unit_pkg$attr_to_rowtype(p_attr);
   l_r.sys_period := tstzrange(now(), NULL::timestamp with time zone);

  if l_r.sensor_num is null then
      select into l_r.sensor_num min(s2um.num)
      /*s2um.sensor2unit_model_id,
      s2um.equipment_type_unit_id,
      s2um.equipment_type_sensor_id,
      s2um.num,
      s2um.is_required,
      et.name_short as sensor_type_name,
      et_unit.name_short as unit_type_name*/
      from core.sensor2unit_model s2um
        join core.equipment_type et on et.equipment_type_id = s2um.equipment_type_sensor_id
        join core.equipment_type et_unit on et_unit.equipment_type_id = s2um.equipment_type_unit_id
        join core.equipment_model em on em.equipment_type_id =et.equipment_type_id
        join core.equipment eq on eq.equipment_model_id =em.equipment_model_id
        join core.equipment_model em_unit on em_unit.equipment_type_id =et_unit.equipment_type_id
        join core.equipment eq_unit on eq_unit.equipment_model_id =em_unit.equipment_model_id
      where eq.equipment_id =l_r.sensor_id and eq_unit.equipment_id =l_r.unit_id
            and  not exists (select 1 from core.sensor2unit s2u where s2u.unit_id = l_r.unit_id and s2u.sensor_num = s2um.num);
  end if;

  if l_r.sensor_num is null then
     RAISE EXCEPTION '<<Не найден свободный вход ББ для подключения датчика>>';
  end if;

  insert into core.sensor2unit select l_r.*;

   return null;

end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sensor2unit_pkg$of_insert"(numeric, text)
  OWNER TO adv;

--********************************************************************************************************

--********************************************************************************************************
