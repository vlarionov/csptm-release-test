CREATE OR REPLACE FUNCTION core.jf_repair_request_comment_pkg$attr_to_rowtype(p_attr text)
  RETURNS core.repair_request_comment
AS
  $BODY$
  declare
   l_r core.repair_request_comment%rowtype;
begin
   l_r.repair_request_comment_id := jofl.jofl_pkg$extract_number(p_attr, 'repair_request_comment_id', true);
   l_r.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true);
   l_r.account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', true);
   l_r.dt := jofl.jofl_pkg$extract_date(p_attr, 'dt', true);
   l_r.repair_request_id := jofl.jofl_pkg$extract_number(p_attr, 'repair_request_id', true);

   return l_r;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_repair_request_comment_pkg$of_rows(in p_id_account numeric, out p_rows refcursor, in p_attr text)
  RETURNS refcursor
AS
  $BODY$
  declare
  l_repair_request_id NUMERIC;
begin
 l_repair_request_id := jofl.jofl_pkg$extract_number(p_attr, 'repair_request_id', true);
 open p_rows for
      select
        repair_request_comment.repair_request_comment_id,
        repair_request_comment.comment,
        repair_request_comment.account_id,
        repair_request_comment.dt,
        repair_request_comment.repair_request_id,
        concat_ws(' ', account.first_name , account.middle_name ,account.last_name) account_name
      from core.repair_request_comment
          JOIN core.account on account.account_id = repair_request_comment.account_id
      where repair_request_id = l_repair_request_id;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_repair_request_comment_pkg$of_insert(p_id_account numeric, p_attr text)
  RETURNS text
AS
  $BODY$
  declare
   l_r core.repair_request_comment%rowtype;
begin
   l_r := core.jf_repair_request_comment_pkg$attr_to_rowtype(p_attr);
   l_r.repair_request_comment_id := nextval('core.repair_request_comment_repair_request_comment_id_seq');
   l_r.dt := current_timestamp;
   l_r.account_id := p_id_account;
   insert into core.repair_request_comment select l_r.*;

   return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_repair_request_comment_pkg$of_update(p_id_account numeric, p_attr text)
  RETURNS text
AS
  $BODY$
  declare
   l_r core.repair_request_comment%rowtype;
begin
   l_r := core.jf_repair_request_comment_pkg$attr_to_rowtype(p_attr);

   update core.repair_request_comment set
          comment = l_r.comment,
          account_id = l_r.account_id,
          dt = l_r.dt
   where
          repair_request_comment_id = l_r.repair_request_comment_id and
          repair_request_id = l_r.repair_request_id;

   return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_repair_request_comment_pkg$of_delete(p_id_account numeric, p_attr text)
  RETURNS text
AS
  $BODY$
  declare
   l_r core.repair_request_comment%rowtype;
begin
   l_r := core.jf_repair_request_comment_pkg$attr_to_rowtype(p_attr);

   delete from  core.repair_request_comment where  repair_request_comment_id = l_r.repair_request_comment_id and
 repair_request_id = l_r.repair_request_id;

   return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;
