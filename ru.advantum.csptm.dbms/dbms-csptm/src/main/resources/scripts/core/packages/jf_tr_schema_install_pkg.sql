﻿-- Function: core."jf_tr_schema_install_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION core."jf_tr_schema_install_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_tr_schema_install_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.tr_schema_install AS
$BODY$ 
declare 
   l_r core.tr_schema_install%rowtype; 
begin 
   l_r.tr_schema_install_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_schema_install_id', true); 
   l_r.tr_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tr_type_id', true); 
   l_r.tr_capacity_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tr_capacity_id', true); 
   l_r.install_name := jofl.jofl_pkg$extract_varchar(p_attr, 'install_name', true); 
   l_r.install_desc := jofl.jofl_pkg$extract_varchar(p_attr, 'install_desc', true);
   l_r.dt_begin := jofl.jofl_pkg$extract_date(p_attr, 'dt_begin', true);
   l_r.is_active := jofl.jofl_pkg$extract_boolean(p_attr, 'is_active', true);

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_tr_schema_install_pkg$attr_to_rowtype"(text)
  OWNER TO adv;

-------------------------------------------------------
-- Function: core."jf_tr_schema_install_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_tr_schema_install_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_tr_schema_install_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.tr_schema_install%rowtype;
begin 
   l_r := core.jf_tr_schema_install_pkg$attr_to_rowtype(p_attr);

   delete from  core.tr_schema_install where  tr_schema_install_id = l_r.tr_schema_install_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_tr_schema_install_pkg$of_delete"(numeric, text)
  OWNER TO adv;

-------------------------------------------------------
-- Function: core."jf_tr_schema_install_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_tr_schema_install_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_tr_schema_install_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.tr_schema_install%rowtype;
begin 
   l_r := core.jf_tr_schema_install_pkg$attr_to_rowtype(p_attr);
   l_r.tr_schema_install_id := nextval('core.tr_schema_install_tr_schema_install_id_seq');
   l_r.dt_begin :=now();
   l_r.is_active :=true;

   insert into core.tr_schema_install select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_tr_schema_install_pkg$of_insert"(numeric, text)
  OWNER TO adv;

-------------------------------------------------------
-- Function: core."jf_tr_schema_install_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_tr_schema_install_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_tr_schema_install_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
begin 
 open p_rows for 
      select
        tsi.tr_schema_install_id,
        tsi.tr_type_id,
        tsi.tr_capacity_id,
        tsi.install_name,
        tsi.install_desc,
        tsi.dt_begin,
        tsi.is_active,
        tcap.short_name as tr_capacity_short_name,
        ttp.short_name as tr_type_short_name
      from core.tr_schema_install tsi
      join core.tr_capacity tcap on tcap.tr_capacity_id = tsi.tr_capacity_id
      join core.tr_type ttp on ttp.tr_type_id = tsi.tr_type_id;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_tr_schema_install_pkg$of_rows"(numeric, text)
  OWNER TO adv;

-------------------------------------------------------
-- Function: core."jf_tr_schema_install_pkg$of_update"(numeric, text)

-- DROP FUNCTION core."jf_tr_schema_install_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_tr_schema_install_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.tr_schema_install%rowtype;
begin 
   l_r := core.jf_tr_schema_install_pkg$attr_to_rowtype(p_attr);

   update core.tr_schema_install set 
          tr_type_id = l_r.tr_type_id, 
          tr_capacity_id = l_r.tr_capacity_id, 
          install_name = l_r.install_name, 
          install_desc = l_r.install_desc
   where 
          tr_schema_install_id = l_r.tr_schema_install_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_tr_schema_install_pkg$of_update"(numeric, text)
  OWNER TO adv;

-------------------------------------------------------