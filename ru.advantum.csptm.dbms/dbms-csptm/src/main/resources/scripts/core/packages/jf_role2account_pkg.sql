DROP FUNCTION core.jf_role2account_pkg$attr_to_rowtype(TEXT);

CREATE OR REPLACE FUNCTION core.jf_role2account_pkg$attr_to_rowtype(p_attr TEXT)
  RETURNS core.v_role2account
AS
  $BODY$
  DECLARE
    l_r core.v_role2account%ROWTYPE;
  BEGIN

    l_r.id_role := jofl.jofl_pkg$extract_varchar(p_attr, 'id_role', TRUE);
    l_r.role_name := jofl.jofl_pkg$extract_varchar(p_attr, 'role_name', TRUE);
    l_r.id_jofl_account := jofl.jofl_pkg$extract_number(p_attr, 'id_jofl_account', TRUE);
    l_r.id_account_local := jofl.jofl_pkg$extract_number(p_attr, 'account_id', TRUE);
    l_r.is_include := jofl.jofl_pkg$extract_boolean(p_attr, 'is_include', TRUE);

    RETURN l_r;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION core.jf_role2account_pkg$of_rows(IN p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                            IN p_attr       TEXT)
  RETURNS REFCURSOR
AS
  $BODY$
  DECLARE
    l_account_id numeric;
  BEGIN
    l_account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', TRUE);
    OPEN p_rows FOR
    SELECT
      id_role,
      role_name,
      id_jofl_account,
      id_account_local account_id,
      is_include
    FROM core.v_role2account
    WHERE id_jofl_account = l_account_id;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_role2account_pkg$of_update(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
  $BODY$
  DECLARE
    l_r core.v_role2account%ROWTYPE;
  BEGIN
    l_r := core.jf_role2account_pkg$attr_to_rowtype(p_attr);
    if l_r.is_include then
      insert INTO jofl.role2account(id_role, id_jofl_account)
      VALUES (l_r.id_role, l_r.id_jofl_account);
    ELSE
      delete from jofl.role2account
      where id_role = l_r.id_role and id_jofl_account = l_r.id_jofl_account;
    end if;

    RETURN NULL;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;
