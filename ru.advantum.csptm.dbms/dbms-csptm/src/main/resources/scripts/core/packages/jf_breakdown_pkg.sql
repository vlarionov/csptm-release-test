CREATE OR REPLACE FUNCTION core.jf_breakdown_pkg$attr_to_rowtype(p_attr TEXT)
  RETURNS core.breakdown
AS
  $BODY$
  DECLARE
    l_r core.breakdown%ROWTYPE;
  BEGIN
    l_r.breakdown_id := jofl.jofl_pkg$extract_number(p_attr, 'breakdown_id', TRUE);
    l_r.dt_begin := jofl.jofl_pkg$extract_date(p_attr, 'dt_begin', TRUE);
    l_r.num := jofl.jofl_pkg$extract_varchar(p_attr, 'num', TRUE);
    l_r.description := jofl.jofl_pkg$extract_varchar(p_attr, 'description', TRUE);
    l_r.is_confirmed := jofl.jofl_pkg$extract_boolean(p_attr, 'is_confirmed', TRUE);
    l_r.dt_end := jofl.jofl_pkg$extract_date(p_attr, 'dt_end', TRUE);
    l_r.tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', TRUE);
    l_r.breakdown_type_id := jofl.jofl_pkg$extract_number(p_attr, 'breakdown_type_id', TRUE);
    l_r.breakdown_status_id := jofl.jofl_pkg$extract_number(p_attr, 'breakdown_status_id', TRUE);
    l_r.close_result_id := jofl.jofl_pkg$extract_number(p_attr, 'close_result_id', TRUE);
    l_r.territory_id := jofl.jofl_pkg$extract_number(p_attr, 'territory_id', TRUE);
    l_r.equipment_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', TRUE);
    l_r.account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', TRUE);

    RETURN l_r;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_breakdown_pkg$of_rows(IN p_id_account NUMERIC, OUT p_rows REFCURSOR, IN p_attr TEXT)
  RETURNS REFCURSOR
AS
$BODY$
DECLARE
  l_rf_db_method                 TEXT;
  f_dtb_DT                       TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'f_dtb_DT', TRUE);
  f_dte_DT                       TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'f_dte_DT', TRUE);
  f_list_depo_id                      BIGINT [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', TRUE);
  f_breakdown_type_id            BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_breakdown_type_id', TRUE);
  f_equipment_type_id            BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_type_id', TRUE);
  f_equipment_model_id           BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_model_id', TRUE);
  f_breakdown_status_id          BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_breakdown_status_id', TRUE);
  f_close_result_id              BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_close_result_id', TRUE);
  f_is_confirmed_INPLACE_K BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_is_confirmed_INPLACE_K', TRUE);
BEGIN

  --"RF_DB_METHOD":"core.repair_request"
  l_rf_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);


  OPEN p_rows FOR SELECT
                    breakdown.breakdown_id,
                    breakdown.dt_begin,
                    breakdown.num,
                    breakdown.description,
                    breakdown.is_confirmed,
                    breakdown.dt_end,
                    breakdown.breakdown_type_id,
                    breakdown.breakdown_status_id,
                    breakdown.close_result_id,
                    breakdown.territory_id,
                    breakdown.equipment_id,
                    breakdown.account_id,
                    concat_ws(' ', account.first_name , account.middle_name, account.last_name) AS account_name,
                    breakdown.tr_id,
    (tr.garage_num /*|| ' ( ' :: TEXT) || tr.licence) || ' ) ' :: TEXT*/) AS tr_name,
                    /*territory.code*/terrname.name_short                               AS territory_name,
                    bd_close_result.name                                                AS close_result_name,
                    breakdown_status.name                                               AS breakdown_status_name,
                    breakdown_type.name                                                 AS breakdown_type_name,
                    equipment.serial_num                                                AS equipment_serial_num,
                    equipment_model.name                                                AS equipment_name
,
    '[' ||
        json_build_object('CAPTION',(tr.garage_num /*|| ' ( ' :: TEXT) || tr.licence) || ' ) ' :: TEXT*/)
      , 'JUMP',json_build_object('METHOD', 'core.tr'
                                , 'ATTR'
                      , '{}'
  --,json_build_object('tr_id', tr.tr_id)
                 )
        )
    || ']' tr_link

      , de.name_short depo_name
      , fw.version    fw_version


                   ,CASE
                    WHEN breakdown.breakdown_status_id = core.jf_breakdown_pkg$cn_status_open()
                      THEN
                        'P{A},D{OF_UPDATE, OF_DELETE, OF_CLOSE, OF_REQUEST_REPAIR}'
                    ELSE
                        'P{RO},D{}'
                    END AS "ROW$POLICY"
--                     equipment_model.equipment_model_id,
--                     equipment_model.equipment_type_id,
--                     equipment.depo_id
                  FROM core.breakdown
                    JOIN core.account ON account.account_id = breakdown.account_id
                    JOIN core.breakdown_status ON breakdown_status.breakdown_status_id = breakdown.breakdown_status_id
                    JOIN core.breakdown_type ON breakdown_type.breakdown_type_id = breakdown.breakdown_type_id
                    JOIN core.tr ON tr.tr_id = breakdown.tr_id
                    LEFT JOIN core.equipment ON equipment.equipment_id = breakdown.equipment_id
--                     LEFT JOIN core.depo d on d.depo_id=coalesce(equipment.depo_id, core.tr.depo_id)
                    LEFT JOIN core.entity de on de.entity_id = coalesce(equipment.depo_id, core.tr.depo_id) --d.depo_id
                    LEFT join core.firmware fw on fw.firmware_id=equipment.firmware_id

                    LEFT JOIN core.equipment_model ON equipment_model.equipment_model_id = equipment.equipment_model_id
                    LEFT JOIN core.territory ON territory.territory_id = breakdown.territory_id
                    left join core.entity terrname on terrname.entity_id = territory.territory_id
                    LEFT JOIN core.bd_close_result ON bd_close_result.bd_close_result_id = breakdown.close_result_id

                  WHERE breakdown.dt_begin BETWEEN f_dtb_DT AND f_dte_DT
                        AND (breakdown.breakdown_type_id = f_breakdown_type_id OR f_breakdown_type_id IS NULL)
                        AND (equipment.equipment_model_id = f_equipment_model_id OR f_equipment_model_id IS NULL)
                        AND (equipment_model.equipment_type_id = f_equipment_type_id OR f_equipment_type_id IS NULL)
                        AND (breakdown.breakdown_status_id = f_breakdown_status_id OR f_breakdown_status_id IS NULL)
                        AND (breakdown.close_result_id = f_close_result_id OR f_close_result_id IS NULL)
                        AND (breakdown.is_confirmed = CASE f_is_confirmed_INPLACE_K
                                                        WHEN 0 THEN FALSE
                                                        WHEN 1 THEN TRUE
                                                      END OR f_is_confirmed_INPLACE_K = -1)
               and (equipment.depo_id = any(f_list_depo_id) or array_length(f_list_depo_id, 1) is null)
  ;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION core.jf_breakdown_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
  $BODY$
  DECLARE
    l_r core.breakdown%ROWTYPE;
  BEGIN
    l_r := core.jf_breakdown_pkg$attr_to_rowtype(p_attr);
    if l_r.breakdown_id <= 0 or l_r.breakdown_id is NULL then
      l_r.breakdown_id := nextval('core.breakdown_breakdown_id_seq');
    end if;
    l_r.num := nextval('core.breakdown_num_new_seq');
    l_r.is_confirmed := FALSE;
    l_r.account_id := p_id_account;
    l_r.dt_begin := current_timestamp;
    l_r.breakdown_status_id := core.jf_breakdown_pkg$cn_status_open();

    IF l_r.breakdown_type_id is null or l_r.breakdown_type_id <= 0 THEN
      l_r.breakdown_type_id := 9; /*Неопределен*/
    END IF;

    INSERT INTO core.breakdown SELECT l_r.*;

    select * into l_r from core.breakdown b where b.breakdown_id=l_r.breakdown_id;

    RETURN row_to_json(l_r)::jsonb;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_breakdown_pkg$of_update(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
  $BODY$
  DECLARE
    l_r core.breakdown%ROWTYPE;
  BEGIN
    l_r := core.jf_breakdown_pkg$attr_to_rowtype(p_attr);
    l_r.account_id := p_id_account;

    UPDATE core.breakdown
    SET
      --dt_begin            = l_r.dt_begin,
      --num                 = l_r.num,
      description         = l_r.description,
      is_confirmed        = l_r.is_confirmed,
--       dt_end              = l_r.dt_end,
--       tr_id               = l_r.tr_id,
--       breakdown_type_id   = l_r.breakdown_type_id,
--       breakdown_status_id = l_r.breakdown_status_id,
--       close_result_id     = l_r.close_result_id,
      territory_id        = l_r.territory_id,
--       equipment_id        = l_r.equipment_id
    account_id          = l_r.account_id
    WHERE
      breakdown.breakdown_id = l_r.breakdown_id;

    RETURN NULL;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_breakdown_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
  $BODY$
  DECLARE
    l_r core.breakdown%ROWTYPE;
  BEGIN
    l_r := core.jf_breakdown_pkg$attr_to_rowtype(p_attr);

    DELETE FROM core.breakdown
    WHERE breakdown_id = l_r.breakdown_id;

    RETURN NULL;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION core.jf_breakdown_pkg$of_close(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
  $BODY$
  DECLARE
    l_r core.breakdown%ROWTYPE;
    f_close_result_id BIGINT := jofl.jofl_pkg$extract_number(attr=>p_attr, attrname => 'f_bd_close_result_id', allownull => true);
    f_equipment_status_id BIGINT := jofl.jofl_pkg$extract_number(attr => p_attr, attrname => 'f_equipment_status_id', allownull => true );
    f_dte_DT TIMESTAMP := jofl.jofl_pkg$extract_date(attr => p_attr, attrname => 'f_close_dte_DT', allownull => true );
  BEGIN

    --RAISE EXCEPTION USING MESSAGE = p_attr;
    l_r := core.jf_breakdown_pkg$attr_to_rowtype(p_attr);
    IF l_r.dt_begin >= f_dte_DT THEN
      RAISE EXCEPTION '<<Дата закрытия должна быть позже даты регистрации!>>';
    END IF;
    UPDATE core.breakdown
    SET breakdown_status_id = core.jf_breakdown_pkg$cn_status_close()
      , close_result_id = f_close_result_id
      , dt_end = f_dte_DT
      WHERE breakdown_id = l_r.breakdown_id;

    UPDATE core.equipment
    SET equipment_status_id = f_equipment_status_id
    WHERE equipment_id = l_r.equipment_id;

    RETURN NULL;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_breakdown_pkg$of_request_repair(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
$BODY$
DECLARE
  l_r   core.breakdown%ROWTYPE; --:= core.jf_breakdown_pkg$attr_to_rowtype(p_attr);
  lr_rr core.repair_request%ROWTYPE;
  lExists INT;
  Rc    TEXT;
BEGIN
  l_r := core.jf_breakdown_pkg$attr_to_rowtype(p_attr);

  lr_rr.breakdown_id := l_r.breakdown_id;
  lr_rr.breakdown_type_id := l_r.breakdown_type_id;
  lr_rr.equipment_id := l_r.equipment_id;
  lr_rr.description := l_r.description;
  lr_rr.territory_id := l_r.territory_id;

  select e.facility_id into lr_rr.facility_id from core.equipment e where e.equipment_id = l_r.equipment_id;

  select count(1) into lExists from core.repair_request rr where rr.breakdown_id = l_r.breakdown_id;
  IF lExists = 0  THEN
    rc:= core.jf_repair_request_pkg$of_insert(p_id_account => p_id_account, p_attr => row_to_json(lr_rr) :: TEXT);
  ELSE
    RAISE EXCEPTION USING MESSAGE = '<<По этой записи о поломке уже создана заявка на ремонт!>>';
  END IF;
  RETURN NULL;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION core.jf_breakdown_pkg$cn_status_open()
  RETURNS core.breakdown.breakdown_status_id%type AS
  'select 1::bigint;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION core.jf_breakdown_pkg$cn_status_close()
  RETURNS core.breakdown.breakdown_status_id%type AS
  'select 2::bigint;'
LANGUAGE SQL IMMUTABLE;