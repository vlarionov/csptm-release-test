﻿create or replace function core.report_access_pkg$available_reports(
    p_account_id core.account.account_id%type,
    p_domain     text) returns integer []
as $$
declare
begin
    return (select array_agg(id_report) from jrep.report);
end;
$$ language plpgsql;

create or replace function core.report_access_pkg$check_report_jasper(
    p_account_id core.account.account_id%type,
    p_domain     text,
    aid_report   jrep.report.id_report%type,
    aexec_type   jrep.report_request.report_exec_type%type,
    aformat_type jrep.report_format.format_type%type) returns numeric
as $$
declare
begin
    return 1;
end;
$$ language plpgsql;
