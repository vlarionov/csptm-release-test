--DROP TYPE core.jf_carrier_type CASCADE;

do $$
begin
  if not exists(select * from pg_type where typname = 'jf_carrier_type') THEN

    CREATE TYPE core.jf_carrier_type AS
      (r_carrier core.carrier,
       r_entity core.entity);

    ALTER TYPE core.jf_carrier_type
      OWNER TO adv;

  end if;
end$$;

--DROP FUNCTION core.jf_carrier_pkg$attr_to_rowtype(text);

CREATE OR REPLACE FUNCTION core.jf_carrier_pkg$attr_to_rowtype(p_attr text)
  RETURNS core.jf_carrier_type
AS
  $BODY$
  declare
    l_r_return core.jf_carrier_type;
    l_r_carrier core.carrier;
    l_r_entity core.entity;
    begin
      l_r_carrier.org_legal_form_id := jofl.jofl_pkg$extract_number(p_attr, 'org_legal_form_id', true);

      l_r_entity.name_full := jofl.jofl_pkg$extract_varchar(p_attr, 'name_full', TRUE);
      l_r_entity.name_short := jofl.jofl_pkg$extract_varchar(p_attr, 'name_short', TRUE);
      l_r_entity.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', TRUE);
      l_r_entity.entity_id := jofl.jofl_pkg$extract_varchar(p_attr, 'entity_id', TRUE);

      l_r_return.r_carrier := l_r_carrier;
      l_r_return.r_entity := l_r_entity;
      return l_r_return;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION core.jf_carrier_pkg$of_rows(in p_id_account numeric, out p_rows refcursor, in p_attr text)
  RETURNS refcursor
AS
  $BODY$
  declare
begin
 open p_rows for
      select
        entity.entity_id,
        entity.name_full,
        entity.name_short,
        entity.comment,
        crr.org_legal_form_id,
        olf_ent.name_full as org_legal_form_name,
        olf_ent.name_short as org_legal_form_name_short
    from core.carrier crr
      join core.entity on entity.entity_id = crr.carrier_id
      left join core.entity olf_ent on olf_ent.entity_id = crr.org_legal_form_id
    ;

  end;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_carrier_pkg$of_insert(p_id_account numeric, p_attr text)
  RETURNS text
AS
  $BODY$
  declare
    l_r_return core.jf_carrier_type;
    l_r_entity core.entity;
    l_r_carrier core.carrier;
begin
    l_r_return := core.jf_carrier_pkg$attr_to_rowtype(p_attr);

    l_r_entity := l_r_return.r_entity;
    l_r_carrier := l_r_return.r_carrier;

    l_r_entity.entity_id := nextval( 'core.entity_entity_id_seq' );
    l_r_carrier.carrier_id:= l_r_entity.entity_id;
    l_r_carrier.sign_deleted:=0;
    l_r_carrier.update_date= now();

    insert into core.entity select l_r_entity.* returning entity_id into l_r_carrier.carrier_id;
    insert into core.carrier select l_r_carrier.*;

    return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_carrier_pkg$of_delete(p_id_account numeric, p_attr text)
  RETURNS text
AS
  $BODY$
  declare
    l_r_return core.jf_carrier_type;
    l_r_entity core.entity;
begin
   l_r_return := core.jf_carrier_pkg$attr_to_rowtype(p_attr);
   l_r_entity := l_r_return.r_entity;

    delete from  core.carrier where  carrier_id = l_r_entity.entity_id;
    delete from  core.entity where  entity_id = l_r_entity.entity_id;

   return null;

  exception
    when 	foreign_key_violation then
      RAISE  EXCEPTION '<<Удаление невозможно! Запись используется в других документах системы>>';
end;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_carrier_pkg$of_update(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r_return core.jf_carrier_type;
  l_r_entity core.entity;
  l_r_carrier core.carrier;
begin
  l_r_return := core.jf_carrier_pkg$attr_to_rowtype(p_attr);
  l_r_entity := l_r_return.r_entity;
  l_r_carrier := l_r_return.r_carrier;

  UPDATE CORE.entity
  SET
    name_full = l_r_entity.name_full,
    name_short = l_r_entity.name_short,
    comment = l_r_entity.comment
  WHERE
    entity_id = l_r_entity.entity_id;

    update core.carrier
    set org_legal_form_id =   l_r_carrier.org_legal_form_id
    where carrier_id = l_r_entity.entity_id;
  return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
