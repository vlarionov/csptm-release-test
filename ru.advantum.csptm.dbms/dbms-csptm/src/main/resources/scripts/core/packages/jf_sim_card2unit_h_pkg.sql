﻿-- Function: core."jf_sim_card2unit_h_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION core."jf_sim_card2unit_h_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_sim_card2unit_h_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.sim_card2unit_h AS
$BODY$ 
declare 
   l_r core.sim_card2unit_h%rowtype; 
begin 
   l_r.dt_begin := jofl.jofl_pkg$extract_date(p_attr, 'dt_begin', true); 
   l_r.dt_end := jofl.jofl_pkg$extract_date(p_attr, 'dt_end', true); 
   l_r.unit_remove_reason := jofl.jofl_pkg$extract_number(p_attr, 'unit_remove_reason', true); 
   l_r.sim_card_id := jofl.jofl_pkg$extract_number(p_attr, 'sim_card_id', true); 
   l_r.equipment_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true); 
   l_r.sim_card2unit_h_id := jofl.jofl_pkg$extract_number(p_attr, 'sim_card2unit_h_id', true); 

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sim_card2unit_h_pkg$attr_to_rowtype"(text)
  OWNER TO adv;

--*************************************************************************************************
-- Function: core."jf_sim_card2unit_h_pkg$of_rows"(numeric, text)

 --DROP FUNCTION core."jf_sim_card2unit_h_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sim_card2unit_h_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_r core.sim_card2unit_h%rowtype;
begin
  l_r.sim_card_id := jofl.jofl_pkg$extract_number(p_attr, 'sim_card_id', true);
  l_r.equipment_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
  open p_rows for
      select 
        h.dt_begin,
        h.dt_end,
        h.unit_remove_reason,
        h.sim_card_id,
        h.equipment_id,
        h.sim_card2unit_h_id,
        et.serial_num,
        ets.name as equipment_status_name,
        em.name as equipment_model_name,
        ett.name_short as equipment_type_name,
        urr.name as card_remove_reason_name
      from core.sim_card2unit_h h
        join core.equipment et on et.equipment_id = h.equipment_id
        join core.equipment_status ets on ets.equipment_status_id = et.equipment_status_id
        join core.equipment_model em on em.equipment_model_id = et.equipment_model_id
        join core.equipment_type ett on ett.equipment_type_id = em.equipment_type_id
        left join core.equipment_remove_reason urr on urr.equipment_remove_reason_id = h.unit_remove_reason
     where h.sim_card_id = l_r.sim_card_id
        or h.equipment_id=l_r.equipment_id;

end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sim_card2unit_h_pkg$of_rows"(numeric, text)
  OWNER TO adv;

--*************************************************************************************************
-- Function: core."jf_sim_card2unit_h_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_sim_card2unit_h_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sim_card2unit_h_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.sim_card2unit_h%rowtype;
begin 
   l_r := core.jf_sim_card2unit_h_pkg$attr_to_rowtype(p_attr);

   delete from  core.sim_card2unit_h where  sim_card2unit_h_id = l_r.sim_card2unit_h_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sim_card2unit_h_pkg$of_delete"(numeric, text)
  OWNER TO adv;


--*************************************************************************************************
-- Function: core."jf_sim_card2unit_h_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_sim_card2unit_h_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sim_card2unit_h_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.sim_card2unit_h%rowtype;
begin 
   l_r := core.jf_sim_card2unit_h_pkg$attr_to_rowtype(p_attr);
   l_r.sim_card2unit_h_id := nextval('core.sim_card2unit_h_sim_card2unit_h_id_seq');
   
   insert into core.sim_card2unit_h select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sim_card2unit_h_pkg$of_insert"(numeric, text)
  OWNER TO adv;


--*************************************************************************************************
-- Function: core."jf_sim_card2unit_h_pkg$of_update"(numeric, text)

-- DROP FUNCTION core."jf_sim_card2unit_h_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sim_card2unit_h_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.sim_card2unit_h%rowtype;
begin 
   l_r := core.jf_sim_card2unit_h_pkg$attr_to_rowtype(p_attr);

   update core.sim_card2unit_h set 
          dt_end = l_r.dt_end,
          unit_remove_reason = l_r.unit_remove_reason
   where 
          sim_card2unit_h_id = l_r.sim_card2unit_h_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sim_card2unit_h_pkg$of_update"(numeric, text)
  OWNER TO adv;

--*************************************************************************************************