CREATE OR REPLACE FUNCTION core.jf_f_garage_number_pkg$of_rows(
  IN p_id_account numeric,
  OUT p_rows refcursor,
  IN p_attr text)
  RETURNS refcursor
AS
$BODY$
DECLARE
begin
  open p_rows for
  select tr.garage_num AS garage_num
  from core.tr tr
  order by 1;
end;
$BODY$
LANGUAGE plpgsql;