﻿create or replace function core.jf_track_pkg$of_rows(aid_account in numeric, arows out refcursor, attr in text default '{}') returns refcursor as $$
declare
    f_tr_id    core.tr.tr_id%type := jofl.jofl_pkg$extract_number(attr,'F_ID_TR',  true);
    f_begin_dt timestamp without time zone := jofl.jofl_pkg$extract_date(attr, 'F_BEGIN_DT',  true);
    f_end_dt   timestamp without time zone := jofl.jofl_pkg$extract_date(attr, 'F_END_DT',  true);
begin
    open arows for
        select packet_id "PACKET_ID"
              ,(event_time)::text||'.0' "GMTEVENTTIME"
              ,lat "LAT"
              ,lon "LON"
              ,alt "ALT"
              ,speed "SPEED"
              ,heading "HEADING"
              ,18 "ID_EVENT"
          from core.traffic t
         where tr_id = f_tr_id
           and event_time between f_begin_dt and f_end_dt
           and lat != 0
           and lon != 0
           and location_valid
         order by event_time, packet_id;
end;
$$
  language plpgsql;
