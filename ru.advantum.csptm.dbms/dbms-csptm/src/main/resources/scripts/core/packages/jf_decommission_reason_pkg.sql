﻿CREATE OR REPLACE FUNCTION core."jf_decommission_reason_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.decommission_reason AS
$BODY$ 
declare 
   l_r core.decommission_reason%rowtype; 
begin 
   l_r.decommission_reason_id := jofl.jofl_pkg$extract_number(p_attr, 'decommission_reason_id', true); 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_decommission_reason_pkg$attr_to_rowtype"(text)
  OWNER TO adv;

/* ****************************************************************************************************/ 
CREATE OR REPLACE FUNCTION core."jf_decommission_reason_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.decommission_reason%rowtype;
begin 
   l_r := core.jf_decommission_reason_pkg$attr_to_rowtype(p_attr);

   delete from  core.decommission_reason where  decommission_reason_id = l_r.decommission_reason_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_decommission_reason_pkg$of_delete"(numeric, text)
  OWNER TO adv;


/* ****************************************************************************************************/ 
CREATE OR REPLACE FUNCTION core."jf_decommission_reason_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.decommission_reason%rowtype;
begin 
   l_r := core.jf_decommission_reason_pkg$attr_to_rowtype(p_attr);
   l_r.decommission_reason_id := nextval( 'core.decommission_reason_decommission_reason_id_seq' );

   insert into core.decommission_reason select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_decommission_reason_pkg$of_insert"(numeric, text)
  OWNER TO adv;
/* ****************************************************************************************************/ 
CREATE OR REPLACE FUNCTION core."jf_decommission_reason_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
begin 
 open p_rows for 
      select 
        decommission_reason_id, 
        name
      from core.decommission_reason; 
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_decommission_reason_pkg$of_rows"(numeric, text)
  OWNER TO adv;

/* ****************************************************************************************************/ 

CREATE OR REPLACE FUNCTION core."jf_decommission_reason_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.decommission_reason%rowtype;
begin 
   l_r := core.jf_decommission_reason_pkg$attr_to_rowtype(p_attr);

   update core.decommission_reason set 
          name = l_r.name
   where 
          decommission_reason_id = l_r.decommission_reason_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_decommission_reason_pkg$of_update"(numeric, text)
  OWNER TO adv;
/* ****************************************************************************************************/ 