create or replace function core.jf_bd_close_result_pkg$attr_to_rowtype(p_attr text) returns core.bd_close_result
  language plpgsql
as $BODY$
declare
   l_r core.bd_close_result%rowtype;
begin
   l_r.bd_close_result_id := jofl.jofl_pkg$extract_number(p_attr, 'bd_close_result_id', true);
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true);

   return l_r;
end;
$BODY$
;

create or replace function core.jf_bd_close_result_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
  language plpgsql
as $BODY$
declare
begin
 open p_rows for
      select
        bd_close_result_id,
        name
      from core.bd_close_result
  order by bd_close_result_id
  ;
end;
$BODY$
;

create or replace function core.jf_bd_close_result_pkg$of_insert(p_id_account numeric, p_attr text) returns text
  language plpgsql
as $BODY$
declare
   l_r core.bd_close_result%rowtype;
begin
   l_r := core.jf_bd_close_result_pkg$attr_to_rowtype(p_attr);
   l_r.bd_close_result_id := nextval('core.bd_close_result_bd_close_result_id_seq');

   insert into core.bd_close_result select l_r.*;

   return null;
end;
$BODY$
;

create or replace function core.jf_bd_close_result_pkg$of_delete(p_id_account numeric, p_attr text) returns text
  language plpgsql
as $BODY$
declare
   l_r core.bd_close_result%rowtype;
begin
   l_r := core.jf_bd_close_result_pkg$attr_to_rowtype(p_attr);

   delete from  core.bd_close_result where  bd_close_result_id = l_r.bd_close_result_id;

   return null;
end;
$BODY$
;

create or replace function core.jf_bd_close_result_pkg$of_update(p_id_account numeric, p_attr text) returns text
  language plpgsql
as $BODY$
declare
   l_r core.bd_close_result%rowtype;
begin
   l_r := core.jf_bd_close_result_pkg$attr_to_rowtype(p_attr);

   update core.bd_close_result set
          name = l_r.name
   where
          bd_close_result_id = l_r.bd_close_result_id;

   return null;
end;
$BODY$
;

