﻿-- Function: core."jf_auxiliary_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION core."jf_auxiliary_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_auxiliary_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.auxiliary AS
$BODY$ 
declare 
   l_r core.auxiliary%rowtype; 
begin 
   l_r.auxiliary_id := jofl.jofl_pkg$extract_varchar(p_attr, 'auxiliary_id', true); 
   l_r.auxiliary_name := jofl.jofl_pkg$extract_varchar(p_attr, 'auxiliary_name', true); 

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_auxiliary_pkg$attr_to_rowtype"(text)
  OWNER TO adv;
---****************************************************************************************
-- Function: core."jf_auxiliary_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_auxiliary_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_auxiliary_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.auxiliary%rowtype;
begin 
   l_r := core.jf_auxiliary_pkg$attr_to_rowtype(p_attr);

   delete from  core.auxiliary where  auxiliary_id = l_r.auxiliary_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_auxiliary_pkg$of_delete"(numeric, text)
  OWNER TO adv;

---****************************************************************************************
-- Function: core."jf_auxiliary_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_auxiliary_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_auxiliary_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.auxiliary%rowtype;
begin 
   l_r := core.jf_auxiliary_pkg$attr_to_rowtype(p_attr);
   l_r.auxiliary_id := nextval( 'core.auxiliary_auxiliary_id_seq' );
   insert into core.auxiliary select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_auxiliary_pkg$of_insert"(numeric, text)
  OWNER TO adv;

---****************************************************************************************
-- Function: core."jf_auxiliary_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_auxiliary_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_auxiliary_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
begin 
 open p_rows for 
      select 
        auxiliary_id, 
        auxiliary_name
      from core.auxiliary; 
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_auxiliary_pkg$of_rows"(numeric, text)
  OWNER TO adv;

---****************************************************************************************
-- Function: core."jf_auxiliary_pkg$of_update"(numeric, text)

-- DROP FUNCTION core."jf_auxiliary_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_auxiliary_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.auxiliary%rowtype;
begin 
   l_r := core.jf_auxiliary_pkg$attr_to_rowtype(p_attr);

   update core.auxiliary set 
          auxiliary_name = l_r.auxiliary_name
   where 
          auxiliary_id = l_r.auxiliary_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_auxiliary_pkg$of_update"(numeric, text)
  OWNER TO adv;
