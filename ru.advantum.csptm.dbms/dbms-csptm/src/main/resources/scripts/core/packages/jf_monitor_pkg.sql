﻿-- DROP TYPE core.jf_monitor_type;

do $$
begin
  if not exists(select * from pg_type where typname = 'jf_monitor_type') THEN

    CREATE TYPE core.jf_monitor_type AS
      (r_monitor core.monitor,
       r_equipment core.equipment);

    ALTER TYPE core.jf_monitor_type
      OWNER TO adv;

  end if;
end$$;

--************************************************************************************************************
-- Function: core."jf_monitor_pkg$attr_to_rowtype"(text)

--DROP FUNCTION core."jf_monitor_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_monitor_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.jf_monitor_type AS
$BODY$ 
declare
  l_r_return core.jf_monitor_type;
  l_r_monitor core.monitor%rowtype;
  l_r_equipment core.equipment%rowtype;
begin
  l_r_monitor.monitor_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
  
  l_r_equipment.firmware_id := jofl.jofl_pkg$extract_number(p_attr, 'firmware_id', true);
  l_r_equipment.unit_service_type_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_service_type_id', true);
  l_r_equipment.facility_id := jofl.jofl_pkg$extract_number(p_attr, 'facility_id', true);
  l_r_equipment.equipment_status_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_status_id', true);
  l_r_equipment.dt_begin := jofl.jofl_pkg$extract_date(p_attr, 'dt_begin', true);
  l_r_equipment.equipment_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
  l_r_equipment.serial_num := jofl.jofl_pkg$extract_varchar(p_attr, 'serial_num', true);
  l_r_equipment.equipment_model_id := jofl.jofl_pkg$extract_varchar(p_attr, 'equipment_model_id', true);
  l_r_equipment.depo_id := jofl.jofl_pkg$extract_number(p_attr, 'depo_id', true);
  l_r_equipment.territory_id := jofl.jofl_pkg$extract_number(p_attr, 'territory_id', true);
  l_r_equipment.unit_service_facility_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_service_facility_id', true);
  l_r_equipment.has_diagnostic := jofl.jofl_pkg$extract_boolean(p_attr, 'has_diagnostic', true);
  l_r_equipment.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true);

  l_r_return.r_monitor := l_r_monitor;
  l_r_return.r_equipment := l_r_equipment;
  return l_r_return;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_monitor_pkg$attr_to_rowtype"(text)
  OWNER TO adv;
--**************************************************************************************************************
-- Function: core."jf_monitor_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_monitor_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core.jf_monitor_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_rf_db_method 			text;
  l_depo_id	 				core.depo.depo_id%type 		:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', true), jofl.jofl_pkg$extract_number(p_attr, 'depo_id', true));
  l_equipment_model_id 		core.equipment_model.equipment_model_id%type		:= jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_model_id', true);
  l_unit_service_type_id 	core.unit_service_type.unit_service_type_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_unit_service_type_id', true);
  l_arr_org_facility_id 	bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_org_facility_id', true);
  l_facility_id 			core.facility.facility_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_facility_id', true);
  l_firmware_id 			core.firmware.firmware_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_firmware_id', true);
--  l_decommission_reason_id 	core.decommission_reason.decommission_reason_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_decommission_reason_id', true);
  l_equipment_status_list   smallint[]  := jofl.jofl_pkg$extract_tarray(p_attr, 'f_equipment_status_id', true);
  l_serial_num 				text := jofl.jofl_pkg$extract_varchar(p_attr, 'serial_num', true);
  l_depo_list    			bigint[]  := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_depo_id', true);

  l_b_ins_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_insert_from_DT', true);
  l_e_ins_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_insert_to_DT', true);
--  l_b_decom_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_decom_from_DT', true);
--  l_e_decom_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_decom_to_DT', true);

  l_link_bb			smallint					:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_link_bb_INPLACE_K', true), -1);
  l_link_tr			smallint					:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_link_tr_INPLACE_K', true), -1);
 -- l_decommission 	smallint					:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_decommission_INPLACE_K', true), 0);
begin
  l_rf_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);

  open p_rows for
  with adata as (
           select
           et."ROW$POLICY",
           et.equipment_id,
           et.firmware_id,
           et.serial_num,
           et.firmware_name,
           et.unit_service_type_id,
           et.unit_service_type_name,
           et.facility_id,
           et.facility_name,
           et.equipment_status_id,
           et.equipment_status_name,
      --     et.decommission_reason_id,
      --     et.decommission_reason_name,
           et.dt_begin,
      --     et.dt_end,
           et.equipment_model_id,
           et.equipment_model_name,
           et.equipment_type_id,
           et.equipment_type_name,

           et.depo_name_full,
           et.depo_name_short,
           et.depo_id,
           et.territory_id,
           et.territory_name_full,
           et.territory_name_short,
           et.unit_service_facility_id,
           et.unit_service_facility_name_short,
           et.has_diagnostic,
           et.comment,
           (select array_to_string(array_agg(core.jf_equipment_pkg$get_equipment_name(unit_id)),',')
            from core.monitor2unit
            where monitor_id =et.equipment_id
            group by monitor_id
           ) as unit_name,
          (  select tr.garage_num
                from core.equipment2tr e2tr
                  join core.tr tr on tr.tr_id = e2tr.tr_id
                where e2tr.equipment_id = et.equipment_id) as tr_name
         from
           core.monitor m
           join core.v_equipment et on et.equipment_id = m.monitor_id
         where et.equipment_status_id != core.jf_equipment_status_pkg$cn_status_decommissioned()
           --подключаем фильтры
           and (et.depo_id =l_depo_id or et.depo_id = any(l_depo_list) )
          /* and ((l_decommission=0 and et.equipment_status_id != core.jf_equipment_status_pkg$cn_status_decommissioned())
             or (l_decommission =1 and et.equipment_status_id = core.jf_equipment_status_pkg$cn_status_decommissioned()) ) */
           and (l_equipment_model_id is null or et.equipment_model_id = l_equipment_model_id)
           and (l_unit_service_type_id is null or et.unit_service_type_id = l_unit_service_type_id)
           and (l_facility_id is null or et.facility_id = l_facility_id)
           and (et.unit_service_facility_id = any(l_arr_org_facility_id) or array_length(l_arr_org_facility_id, 1) is null )
           and (l_firmware_id is null or et.firmware_id =l_firmware_id )
        --   and (l_decommission_reason_id is null or et.decommission_reason_id = l_decommission_reason_id )
           and (et.equipment_status_id = any(l_equipment_status_list) or (array_length(l_equipment_status_list, 1)  is null))
           and (l_serial_num is null or et.serial_num =l_serial_num )

           and (l_b_ins_dt is null or et.dt_begin >= l_b_ins_dt)
           and (l_e_ins_dt is null or et.dt_begin<= l_e_ins_dt)

        --   and (l_b_decom_dt is null or et.dt_end >= l_b_decom_dt )
        --   and (l_e_decom_dt is null or et.dt_end<= l_e_decom_dt )
  )

  select *
  from adata
  where (l_link_bb=-1 or coalesce(sign(length(unit_name)),0) = l_link_bb )
        and (l_link_tr =-1 or coalesce(sign(tr_name),0) = l_link_tr )
  ;

  

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

--**************************************************************************************************************
-- Function: core."jf_monitor_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_monitor_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_monitor_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare
  l_r_return core.jf_monitor_type;
  l_r_monitor core.monitor%rowtype;
  l_r_equipment core.equipment%rowtype;
  l_res text;
begin
  l_r_return := core.jf_monitor_pkg$attr_to_rowtype(p_attr);

  l_r_monitor:= l_r_return.r_monitor;
  l_r_equipment := l_r_return.r_equipment;

  delete from  core.monitor where  monitor_id = l_r_monitor.monitor_id;
  l_res :=  core.jf_equipment_pkg$of_delete(p_id_account,  row_to_json(l_r_equipment)::text);

  return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_monitor_pkg$of_delete"(numeric, text)
  OWNER TO adv;

--**************************************************************************************************************
-- Function: core."jf_monitor_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_monitor_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core.jf_monitor_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
  l_r_return core.jf_monitor_type;
  l_r_monitor core.monitor%rowtype;
  l_r_equipment core.equipment%rowtype;
  l_res text;
  l_sensor_count smallint;
begin
  l_r_return := core.jf_monitor_pkg$attr_to_rowtype(p_attr);

  l_r_monitor:= l_r_return.r_monitor;
  l_r_equipment := l_r_return.r_equipment;
  
  if l_r_equipment.unit_service_type_id = core.jf_unit_service_type_pkg$cn_no_service() then
  	 l_r_equipment.unit_service_facility_id = null;
  end if;

  l_sensor_count:= jofl.jofl_pkg$extract_number(p_attr, 'sensor_count', true);
  if l_sensor_count = 0 then l_sensor_count:=1; end if;


  for i in 1..l_sensor_count loop
    l_r_equipment.equipment_id:= nextval( 'core.equipment_equipment_id_seq' );

    l_r_monitor.monitor_id:= l_r_equipment.equipment_id;

    l_res := core.jf_equipment_pkg$of_insert(p_id_account, row_to_json(l_r_equipment)::text);

    insert into core.monitor select l_r_monitor.*;
   end loop;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;


--**************************************************************************************************************
-- Function: core."jf_monitor_pkg$of_update"(numeric, text)

-- DROP FUNCTION core."jf_monitor_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core.jf_monitor_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
  l_r_return core.jf_monitor_type;
  l_r_monitor core.monitor%rowtype;
  l_r_equipment core.equipment%rowtype;
  l_res text;
begin 

  l_r_return := core.jf_monitor_pkg$attr_to_rowtype(p_attr);

  l_r_monitor:= l_r_return.r_monitor;
  l_r_equipment := l_r_return.r_equipment;
  
  if l_r_equipment.unit_service_type_id = core.jf_unit_service_type_pkg$cn_no_service() then
  	 l_r_equipment.unit_service_facility_id = null;
  end if;

  l_res:= core.jf_equipment_pkg$of_update(p_id_account, row_to_json(l_r_equipment)::text);

  /*update core.monitor SET
where monitor_id= l_r_monitor.monitor_id;*/

  return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

--***************************************************************************************************************

CREATE OR REPLACE FUNCTION core."jf_monitor_pkg$of_link2tr"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
begin
  return core.jf_equipment_pkg$of_link2tr(p_id_account, p_attr);
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_monitor_pkg$of_link2tr"(numeric, text)
OWNER TO adv;
--***************************************************************************************************************
CREATE OR REPLACE FUNCTION core."jf_monitor_pkg$of_decommission"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
begin
  return core.jf_equipment_pkg$of_decommission(p_id_account, p_attr);
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_monitor_pkg$of_decommission"(numeric, text)
OWNER TO adv;

--***************************************************************************************************************
CREATE OR REPLACE FUNCTION core."jf_monitor_pkg$of_link2unit"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r core.monitor2unit%rowtype;
  l_equipment_model_name core.equipment_model.name%type;
  l_equipment_type_id bigint;
  l_equipment_status_id bigint;
  l_num core.sensor2unit_model.num%type;
begin
  l_r.monitor_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
  l_r.unit_id:= jofl.jofl_pkg$extract_number(p_attr, 'F_equipment_id', true);
  l_equipment_model_name:=  jofl.jofl_pkg$extract_varchar(p_attr, 'equipment_model_name', TRUE);
  l_equipment_type_id:=  jofl.jofl_pkg$extract_number(p_attr, 'equipment_type_id', TRUE);
  l_equipment_status_id:= jofl.jofl_pkg$extract_number(p_attr, 'equipment_status_id', TRUE);

  if l_equipment_status_id = core.jf_equipment_status_pkg$cn_status_decommissioned() then
    RAISE EXCEPTION '<<Устанавливаемый модем не может быть списан>>';
  end if;

  if l_r.unit_id is null then
    RAISE EXCEPTION '<<Выберите ББ из списка доступных>>';
  end if;

  if exists( select 1 from core.monitor2unit where monitor_id = l_r.monitor_id and unit_id= l_r.unit_id) THEN
    RAISE EXCEPTION '<<Монитор уже привязан к этому блоку>>';
  END IF;

  if not exists(
      select 1
      from core.sensor2unit_model s2um
        join core.equipment unit_e on s2um.equipment_type_unit_id = unit_e.equipment_type_id
      where s2um.equipment_type_sensor_id = l_equipment_type_id
            and unit_e.equipment_id = l_r.unit_id
  )
  then raise exception '<<Не удалось определить оборудование % в схеме подключения.>>', l_equipment_model_name;
  end if;


  insert into core.monitor2unit select l_r.*;
  return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_monitor_pkg$of_link2unit"(numeric, text)
OWNER TO adv;

--************************************************************************
--изменение статуса
CREATE OR REPLACE FUNCTION core."jf_monitor_pkg$of_change_status"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
begin
  return core.jf_equipment_pkg$of_change_status(p_id_account, p_attr);
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_monitor_pkg$of_change_status"(numeric, text)
OWNER TO adv;

--****************************************************************************************
--отмена списания
CREATE OR REPLACE FUNCTION core."jf_monitor_pkg$of_decommission_cancel"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
begin
  return core.jf_equipment_pkg$of_decommission_cancel(p_id_account, p_attr);
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_monitor_pkg$of_decommission_cancel"(numeric, text)
OWNER TO adv;


