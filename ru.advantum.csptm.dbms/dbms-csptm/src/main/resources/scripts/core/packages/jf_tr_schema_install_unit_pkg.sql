﻿-- Function: core."jf_tr_schema_install_unit_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION core."jf_tr_schema_install_unit_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_tr_schema_install_unit_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.tr_schema_install_unit AS
$BODY$ 
declare 
   l_r core.tr_schema_install_unit%rowtype; 
begin 
   l_r.tr_schema_install_unit_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_schema_install_unit_id', true); 
   l_r.equipment_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'equipment_type_id', true); 
   l_r.tr_schema_install_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_schema_install_id', true); 
   l_r.is_main := jofl.jofl_pkg$extract_boolean(p_attr, 'is_main', true); 

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_tr_schema_install_unit_pkg$attr_to_rowtype"(text)
  OWNER TO adv;
----------------------------------------------------------------------------------
-- Function: core."jf_tr_schema_install_unit_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_tr_schema_install_unit_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_tr_schema_install_unit_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.tr_schema_install_unit%rowtype;
begin 
   l_r := core.jf_tr_schema_install_unit_pkg$attr_to_rowtype(p_attr);

   delete from  core.tr_schema_install_unit where  tr_schema_install_unit_id = l_r.tr_schema_install_unit_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_tr_schema_install_unit_pkg$of_delete"(numeric, text)
  OWNER TO adv;

----------------------------------------------------------------------------------
-- Function: core."jf_tr_schema_install_unit_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_tr_schema_install_unit_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_tr_schema_install_unit_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.tr_schema_install_unit%rowtype;
begin 
   l_r := core.jf_tr_schema_install_unit_pkg$attr_to_rowtype(p_attr);
   l_r.tr_schema_install_unit_id := nextval('core.tr_schema_install_unit_tr_schema_install_unit_id_seq');

   insert into core.tr_schema_install_unit select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_tr_schema_install_unit_pkg$of_insert"(numeric, text)
  OWNER TO adv;

----------------------------------------------------------------------------------
-- Function: core."jf_tr_schema_install_unit_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_tr_schema_install_unit_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_tr_schema_install_unit_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_r core.tr_schema_install_unit%rowtype;
begin
  l_r.tr_schema_install_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_schema_install_id', true);

   open p_rows for
   select
     tsiu.tr_schema_install_unit_id,
     tsiu.equipment_type_id,
     tsiu.tr_schema_install_id,
     tsiu.is_main,
     et.name_short as equipment_type_name_short
   from core.tr_schema_install_unit tsiu
     join core.equipment_type et on et.equipment_type_id = tsiu.equipment_type_id
   where tsiu.tr_schema_install_id =  l_r.tr_schema_install_id;

end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_tr_schema_install_unit_pkg$of_rows"(numeric, text)
  OWNER TO adv;

----------------------------------------------------------------------------------
-- Function: core."jf_tr_schema_install_unit_pkg$of_update"(numeric, text)

-- DROP FUNCTION core."jf_tr_schema_install_unit_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_tr_schema_install_unit_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.tr_schema_install_unit%rowtype;
begin 
   l_r := core.jf_tr_schema_install_unit_pkg$attr_to_rowtype(p_attr);

   update core.tr_schema_install_unit set 
          equipment_type_id = l_r.equipment_type_id, 
          is_main = l_r.is_main
   where 
          tr_schema_install_unit_id = l_r.tr_schema_install_unit_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_tr_schema_install_unit_pkg$of_update"(numeric, text)
  OWNER TO adv;

----------------------------------------------------------------------------------