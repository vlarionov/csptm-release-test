﻿
create or replace function core.jf_graph_section_rts_pkg$of_rows
  (p_account_id   in numeric,
   p_attr         in text default '{}',
   p_rows         out refcursor)
returns refcursor
as $$ declare
   l_round_list  text := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'RND_LIST', true),
                                  jofl.jofl_pkg$extract_varchar(p_attr, 'TRJ_LIST', true));
begin
   open p_rows for
   select t.round_id, 
          t.graph_section_id,
          t.order_num,
          s.wkt_geom
     from rts.graph_sec2round t
     join rts.graph_section s on s.graph_section_id = t.graph_section_id
    where t.round_id in (select regexp_split_to_table(l_round_list, ',')::int)
    order by t.round_id, t.order_num;
end;
$$ language plpgsql;
