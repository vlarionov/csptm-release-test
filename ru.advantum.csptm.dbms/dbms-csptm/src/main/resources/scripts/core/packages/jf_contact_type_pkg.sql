﻿
-- DROP FUNCTION core."jf_contact_type_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_contact_type_pkg$of_rows"(
  IN p_id_account numeric,
  OUT p_rows refcursor,
  IN p_attr text)
  RETURNS refcursor AS
$BODY$
declare
begin
  open p_rows for
  select
    name_short,
    name_full,
    contact_type_id
  from core.contact_type;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_contact_type_pkg$of_rows"(numeric, text)
OWNER TO adv;
