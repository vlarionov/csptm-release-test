﻿CREATE OR REPLACE FUNCTION core."jf_unit_service_type_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.unit_service_type AS
$BODY$ 
declare 
   l_r core.unit_service_type%rowtype; 
begin 
   l_r.unit_service_type_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_service_type_id', true); 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_unit_service_type_pkg$attr_to_rowtype"(text)
  OWNER TO adv;
/* *******************************************************************************************************/

CREATE OR REPLACE FUNCTION core."jf_unit_service_type_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.unit_service_type%rowtype;
begin 
   l_r := core.jf_unit_service_type_pkg$attr_to_rowtype(p_attr);

   delete from  core.unit_service_type where  unit_service_type_id = l_r.unit_service_type_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_unit_service_type_pkg$of_delete"(numeric, text)
  OWNER TO adv;
/* *******************************************************************************************************/

CREATE OR REPLACE FUNCTION core."jf_unit_service_type_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.unit_service_type%rowtype;
begin 
   l_r := core.jf_unit_service_type_pkg$attr_to_rowtype(p_attr);
   l_r.unit_service_type_id := nextval( 'core.unit_service_type_unit_service_type_id_seq' );
  
   insert into core.unit_service_type select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_unit_service_type_pkg$of_insert"(numeric, text)
  OWNER TO adv;

/* *******************************************************************************************************/

CREATE OR REPLACE FUNCTION core."jf_unit_service_type_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
begin 
 open p_rows for 
      select 
        unit_service_type_id, 
        name
      from core.unit_service_type; 
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_unit_service_type_pkg$of_rows"(numeric, text)
  OWNER TO adv;

/* *******************************************************************************************************/

CREATE OR REPLACE FUNCTION core."jf_unit_service_type_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.unit_service_type%rowtype;
begin 
   l_r := core.jf_unit_service_type_pkg$attr_to_rowtype(p_attr);

   update core.unit_service_type set 
          name = l_r.name
   where 
          unit_service_type_id = l_r.unit_service_type_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_unit_service_type_pkg$of_update"(numeric, text)
  OWNER TO adv;

/* *******************************************************************************************************/
CREATE OR REPLACE FUNCTION core.jf_unit_service_type_pkg$cn_no_service (
)
RETURNS smallint AS
$body$
begin 
   return 15;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
/* *******************************************************************************************************/