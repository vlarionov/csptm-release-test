create or replace function core.jf_account_info_pkg$of_rows(in aid_account numeric, out arows refcursor, in attr text)
    returns refcursor
as
$BODY$
declare
begin
    select
        a.account_id,
        ja.id_jofl_account,
        (select string_agg(rr.role_name, ', ')
         from jofl.ref_role rr
         where rr.id_role = any (jofl.jofl_pkg$get_account_roles(ja.id_jofl_account))) role_names,
        (select string_agg(g.name, ', ')
         from adm.account2group a2g
             join adm.group g on a2g.group_id = g.group_id
         where a2g.account_id = a.account_id
               and not g.is_personal) group_names
    from core.account a
        join jofl.account ja on a.account_id = ja.id_account_local
    where a.account_id = aid_account;
end;
$BODY$
language plpgsql volatile;