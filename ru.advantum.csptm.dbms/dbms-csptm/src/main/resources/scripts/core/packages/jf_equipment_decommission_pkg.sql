﻿
-- DROP FUNCTION core."jf_equipment_decommission_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_equipment_decommission_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.equipment_decommission AS
$BODY$ 
declare 
   l_r core.equipment_decommission%rowtype; 
begin 
   l_r.equipment_decommission_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_decommission_id', true); 
   l_r.equipment_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true); 
   l_r.decommission_reason_id := jofl.jofl_pkg$extract_varchar(p_attr, 'decommission_reason_id', true); 
   l_r.decommission_descr := jofl.jofl_pkg$extract_varchar(p_attr, 'decommission_descr', true); 
   l_r.dt_end := jofl.jofl_pkg$extract_date(p_attr, 'dt_end', true); 
   l_r.dt_create := jofl.jofl_pkg$extract_date(p_attr, 'dt_create', true); 
   l_r.account_create_id := jofl.jofl_pkg$extract_number(p_attr, 'account_create_id', true); 
   l_r.is_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'is_deleted', true); 
   l_r.dt_deleted := jofl.jofl_pkg$extract_date(p_attr, 'dt_deleted', true); 
   l_r.account_delete_id := jofl.jofl_pkg$extract_number(p_attr, 'account_delete_id', true); 

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_decommission_pkg$attr_to_rowtype"(text)
  OWNER TO adv;

--------------------------------------------------------------------
-- Function: core."jf_equipment_decommission_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_equipment_decommission_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_equipment_decommission_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.equipment_decommission%rowtype;
begin 
   l_r := core.jf_equipment_decommission_pkg$attr_to_rowtype(p_attr);
   l_r.equipment_decommission_id := nextval('core.equipment_decommission_equipment_decommission_id_seq');
   l_r.account_create_id := p_id_account;
   l_r.dt_create := now();
   l_r.is_deleted :=false;
   l_r.dt_deleted :=null;
   l_r.account_delete_id :=null;

   insert into core.equipment_decommission select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_decommission_pkg$of_insert"(numeric, text)
  OWNER TO adv;


--------------------------------------------------------------------
-- Function: core."jf_equipment_decommission_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_equipment_decommission_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_equipment_decommission_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_r core.equipment_decommission%rowtype;
begin
  l_r.equipment_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);

 open p_rows for
 select
   ed.equipment_decommission_id,
   ed.equipment_id,
   ed.decommission_descr,
   ed.is_deleted,
   ed.decommission_reason_id,
   ed.dt_end,
   ed.account_create_id,
   ed.dt_deleted,
   ed.account_delete_id,
   dt_create,
   dr.name as decommission_reason_name,
   concat_ws(' ',acr.last_name , acr.first_name) account_create_name,
   concat_ws(' ',adl.last_name , adl.first_name) account_deleted_name
 from core.equipment_decommission ed
   join core.decommission_reason dr on dr.decommission_reason_id = ed.decommission_reason_id
   join core.account acr on acr.account_id = ed.account_create_id
   left join core.account adl on adl.account_id = ed.account_delete_id
 where ed.equipment_id = l_r.equipment_id;

end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_decommission_pkg$of_rows"(numeric, text)
  OWNER TO adv;

--------------------------------------------------------------------
-- Function: core."jf_equipment_decommission_pkg$of_update"(numeric, text)

-- DROP FUNCTION core."jf_equipment_decommission_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_equipment_decommission_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.equipment_decommission%rowtype;
begin 
   l_r := core.jf_equipment_decommission_pkg$attr_to_rowtype(p_attr);

   update core.equipment_decommission set 
          decommission_reason_id = l_r.decommission_reason_id,
          decommission_descr = l_r.decommission_descr, 
          dt_end = l_r.dt_end
   where
          equipment_decommission_id = l_r.equipment_decommission_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_decommission_pkg$of_update"(numeric, text)
  OWNER TO adv;

--------------------------------------------------------------------
CREATE OR REPLACE FUNCTION core."jf_equipment_decommission_pkg$of_cancel"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
  $BODY$
  declare
     l_r core.equipment_decommission%rowtype;
     l_equipment_status_id core.equipment_status.equipment_status_id%type;
  begin
    l_r := core.jf_equipment_decommission_pkg$attr_to_rowtype(p_attr);
    l_r.dt_deleted :=now();
    l_r.account_delete_id :=p_id_account;
    l_equipment_status_id:= jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_status_id', TRUE);

    update core.equipment_decommission set
      is_deleted = not is_deleted,
      dt_deleted = l_r.dt_deleted,
      account_delete_id = l_r.account_delete_id
    where
      equipment_id = l_r.equipment_id
      and not is_deleted;

    update core.equipment
    set equipment_status_id = l_equipment_status_id
    where equipment_id = l_r.equipment_id;

   return null;
  end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_equipment_decommission_pkg$of_cancel"(numeric, text)
OWNER TO adv;

