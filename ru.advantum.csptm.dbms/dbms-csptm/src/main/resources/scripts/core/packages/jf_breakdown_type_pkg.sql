CREATE OR REPLACE FUNCTION core.jf_breakdown_type_pkg$attr_to_rowtype(p_attr TEXT)
  RETURNS core.breakdown_type
AS
  $BODY$
  DECLARE
    l_r core.breakdown_type%ROWTYPE;
  BEGIN
    l_r.breakdown_type_id := jofl.jofl_pkg$extract_number(p_attr, 'breakdown_type_id', TRUE);
    l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', TRUE);

    RETURN l_r;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_breakdown_type_pkg$of_rows(IN p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                              IN p_attr       TEXT)
  RETURNS REFCURSOR
AS
  $BODY$
  DECLARE
  BEGIN
    OPEN p_rows FOR
    SELECT
      breakdown_type_id,
      name
    FROM core.breakdown_type;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_breakdown_type_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
  $BODY$
  DECLARE
    l_r core.breakdown_type%ROWTYPE;
  BEGIN
    l_r := core.jf_breakdown_type_pkg$attr_to_rowtype(p_attr);
    l_r.breakdown_type_id := nextval('core.breakdown_type_breakdown_type_seq');
    INSERT INTO core.breakdown_type SELECT l_r.*;

    RETURN NULL;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_breakdown_type_pkg$of_update(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
  $BODY$
  DECLARE
    l_r core.breakdown_type%ROWTYPE;
  BEGIN
    l_r := core.jf_breakdown_type_pkg$attr_to_rowtype(p_attr);

    UPDATE core.breakdown_type
    SET
      name = l_r.name
    WHERE
      breakdown_type_id = l_r.breakdown_type_id;

    RETURN NULL;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_breakdown_type_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
  $BODY$
  DECLARE
    l_r core.breakdown_type%ROWTYPE;
  BEGIN
    l_r := core.jf_breakdown_type_pkg$attr_to_rowtype(p_attr);

    DELETE FROM core.breakdown_type
    WHERE breakdown_type_id = l_r.breakdown_type_id;

    RETURN NULL;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;
