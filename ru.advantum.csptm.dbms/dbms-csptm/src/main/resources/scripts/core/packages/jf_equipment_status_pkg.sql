﻿CREATE OR REPLACE FUNCTION core."jf_equipment_status_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.equipment_status AS
$BODY$ 
declare 
   l_r core.equipment_status%rowtype; 
begin 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 
   l_r.equipment_status_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_status_id', true);
   l_r.description := jofl.jofl_pkg$extract_varchar(p_attr, 'description', true);
   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_status_pkg$attr_to_rowtype"(text)
  OWNER TO adv;

/* *****************************************************************************************************************/
CREATE OR REPLACE FUNCTION core."jf_equipment_status_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$
declare
   l_r core.equipment_status%rowtype;
begin
   l_r := core.jf_equipment_status_pkg$attr_to_rowtype(p_attr);

   delete from  core.equipment_status where  equipment_status_id = l_r.equipment_status_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_status_pkg$of_delete"(numeric, text)
  OWNER TO adv;

/* *****************************************************************************************************************/
CREATE OR REPLACE FUNCTION core."jf_equipment_status_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$
declare
   l_r core.equipment_status%rowtype;
begin
   l_r := core.jf_equipment_status_pkg$attr_to_rowtype(p_attr);
   l_r.equipment_status_id := nextval( 'core.equipment_status_equipment_status_id_seq' );

   insert into core.equipment_status select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_status_pkg$of_insert"(numeric, text)
  OWNER TO adv;

/* *****************************************************************************************************************/
CREATE OR REPLACE FUNCTION core."jf_equipment_status_pkg$of_update"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r core.equipment_status%rowtype;
begin
  l_r := core.jf_equipment_status_pkg$attr_to_rowtype(p_attr);

  update core.equipment_status  set
    name = l_r.name,
    description = l_r.description
  where
    equipment_status_id = l_r.equipment_status_id;

  return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_equipment_status_pkg$of_update"(numeric, text)
OWNER TO adv;


/* *****************************************************************************************************************/
CREATE OR REPLACE FUNCTION core."jf_equipment_status_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_rf_db_method text;
begin
 l_rf_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
 open p_rows for 
      select 
        name, 
        equipment_status_id,
        description
      from core.equipment_status
      where l_rf_db_method is null or
            equipment_status_id !=core.jf_equipment_status_pkg$cn_status_decommissioned(); 
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_status_pkg$of_rows"(numeric, text)
  OWNER TO adv;

/* *****************************************************************************************************************/

--DROP FUNCTION core."jf_equipment_status_pkg$cn_status_nothing"()

CREATE OR REPLACE FUNCTION core.jf_equipment_status_pkg$cn_status_nothing()
  RETURNS core.equipment_status.equipment_status_id%type AS
'select 1::smallint;'
LANGUAGE SQL IMMUTABLE;

/* *****************************************************************************************************************/

--DROP FUNCTION core."jf_equipment_status_pkg$cn_status_ok"()

CREATE OR REPLACE FUNCTION core.jf_equipment_status_pkg$cn_status_ok()
  RETURNS core.equipment_status.equipment_status_id%type AS
'select 2::smallint;'
LANGUAGE SQL IMMUTABLE;

/* *****************************************************************************************************************/

--DROP FUNCTION core."jf_equipment_status_pkg$cn_status_defective"()

CREATE OR REPLACE FUNCTION core.jf_equipment_status_pkg$cn_status_defective()
  RETURNS core.equipment_status.equipment_status_id%type AS
'select 3::smallint;'
LANGUAGE SQL IMMUTABLE;

/* *****************************************************************************************************************/

--DROP FUNCTION core."jf_equipment_status_pkg$cn_status_repair"()

CREATE OR REPLACE FUNCTION core.jf_equipment_status_pkg$cn_status_repair()
  RETURNS core.equipment_status.equipment_status_id%type AS
'select 4::smallint;'
LANGUAGE SQL IMMUTABLE;

/* *****************************************************************************************************************/

--DROP FUNCTION core."jf_equipment_status_pkg$cn_status_decommissioned"() cascade

CREATE OR REPLACE FUNCTION core.jf_equipment_status_pkg$cn_status_decommissioned()
  RETURNS core.equipment_status.equipment_status_id%type AS
'select 5::smallint;'
LANGUAGE SQL IMMUTABLE;
