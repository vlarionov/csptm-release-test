﻿
create or replace function core.jf_graph_trajectory_rts_pkg$of_rows
  (p_account_id in  numeric,
   p_attr       in  text default '{}',
   p_rows       out refcursor)
returns refcursor
as $$ declare
   l_round_list text := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'RND_LIST', true),
                                 jofl.jofl_pkg$extract_varchar(p_attr, 'TRJ_LIST', true));
begin
   open p_rows for
   with round_list as (
      select a::numeric as round_id from regexp_split_to_table(l_round_list, ',') a
   )
   select r.round_id, r.wkt_geom
     from rts.round r
     join round_list d on d.round_id = r.round_id;
end;
$$ language plpgsql;
