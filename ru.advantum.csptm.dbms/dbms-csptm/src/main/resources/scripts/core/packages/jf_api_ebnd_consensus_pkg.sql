CREATE OR REPLACE FUNCTION core.jf_api_ebnd_consensus_pkg$of_rows(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$
declare
begin
 open p_rows for
/*      select
        api_unit_num,
        garage_num,
        tr_id,
        unit_id,
        unit_ebnd,
        tn_id
      from core.v_unit2tr_api_ebnd_consensus;
  */
select
    a.unit_num api_unit_num,
    a.garagenum garage_num,
    tr.garage_num tr_id,
    unit.unit_num unit_id,
    (tr.garage_num = a.garagenum::int)::int as unit_ebnd,
    a.tn_instance tn_id,
    equipment_model.name equipment_model_name
from core.v_unit2tr_api_asdu a
  left join core.unit on a.unit_num = unit.unit_num
  left JOIN core.equipment on unit.unit_id = equipment.equipment_id
  left JOIN core.equipment_model on equipment.equipment_model_id = equipment_model.equipment_model_id
  left join core.depo depo_unit on equipment.depo_id = depo_unit.depo_id and a.tn_instance = depo_unit.tn_instance
  left join core.unit2tr on unit.unit_id = unit2tr.unit_id
  left join core.tr on unit2tr.tr_id = tr.tr_id
where a.is_deleted = 0;
end;
$BODY$
LANGUAGE plpgsql;

/*
insert into core.unit2tr(tr_id, unit_id)
select tr_id, unit_id
from core.v_unit2tr_api_asdu a
  join core.tr on a.garagenum::int = tr.garage_num
  join core.depo on a.tn_instance = depo.tn_instance and tr.depo_id = depo.depo_id
  join core.unit on a.unit_num = unit.unit_num
where a.is_deleted = 0 AND unit_id is not null
and not exists (select 1 from core.unit2tr where unit2tr.unit_id = unit.unit_id) ;
*/