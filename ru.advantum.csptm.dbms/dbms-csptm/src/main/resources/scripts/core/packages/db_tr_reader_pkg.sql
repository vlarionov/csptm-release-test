create or replace function core.db_tr_reader_pkg$get_tr_list(out arows           refcursor,
                                                             in  an_id_navserver numeric,
                                                             in  as_service_type text)
    returns refcursor as
$BODY$
begin
    if an_id_navserver = 7
    then -- 14-park crutch4retranslator2fm
        open arows for
        select
            u.unit_id as "ID_TR",
            u.unit_num as "OPTS",
            t.tr_id as "ID4SEND"
        from core.unit u
            join core.unit2tr u2t on u.unit_id = u2t.unit_id
            join core.tr t on u2t.tr_id = t.tr_id
            join core.depo d on t.depo_id = d.depo_id
        where d.tn_instance = '0014';
    else
        open arows for
        select
            u.unit_id as "ID_TR",
            u.unit_num as "OPTS",
            u.unit_num as "ID4SEND"
        from core.unit u
            join core.unit2tr u2t on u.unit_id = u2t.unit_id
        where u.hub_id != 60;
    end if;
end;
$BODY$
language plpgsql volatile
cost 100;
