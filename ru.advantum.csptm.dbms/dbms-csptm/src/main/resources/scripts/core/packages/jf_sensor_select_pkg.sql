﻿
CREATE OR REPLACE FUNCTION core."jf_sensor_select_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_rf_db_method text;
  l_equipment_type_unit_id core.equipment_type.equipment_type_id%type;
  l_unit_id core.unit.unit_id%type;
  l_arr_rf_db_method_sensor2unit text array;
  l_arr_rf_db_method_sensor2tr text array;
begin
  l_rf_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
  l_equipment_type_unit_id := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'equipment_type_id', true), jofl.jofl_pkg$extract_number(p_attr, 'unit_equipment_type_id', true));
  l_unit_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);

  l_arr_rf_db_method_sensor2unit := '{"core.sensor2unit", "core.sensor_all2unit"}';
  l_arr_rf_db_method_sensor2tr := '{"core.sensor2tr"}';
 open p_rows for
           with s_tr as (
               select tr.garage_num as tr_name, s2tr.tr_id, s2tr.sensor_id
               from core.sensor2tr s2tr
                 join core.tr tr on tr.tr_id = s2tr.tr_id
           )

           select
             et."ROW$POLICY",
             et.equipment_id,
             et.firmware_id,
             et.serial_num,
             et.firmware_name,
             et.unit_service_type_id,
             et.unit_service_type_name,
             et.facility_id,
             et.facility_name,
             et.equipment_status_id,
             et.equipment_status_name,
             et.decommission_reason_id,
             et.decommission_reason_name,
             et.dt_begin,
             et.dt_end,
             et.equipment_model_id,
             et.equipment_model_name,
             et.equipment_type_id,
             et.equipment_type_name,

             et.depo_name_full,
             et.depo_name_short,
             et.depo_id,
             et.territory_id,
             et.territory_name_full,
             et.territory_name_short,
             et.unit_service_facility_id,
             et.unit_service_facility_name_short,
             et.has_diagnostic,
             et.comment,
             s.sensor_num,
             ( select array_to_string(array_agg(ett.name_short || ' '||  eqt.serial_num), ','::text) as array_to_string
               from core.sensor2unit
                 join core.equipment eqt on sensor2unit.unit_id = eqt.equipment_id
                 join core.equipment_model em on em.equipment_model_id = eqt.equipment_model_id
                 join core.equipment_type ett on ett.equipment_type_id = em.equipment_type_id
               where sensor2unit.sensor_id = et.equipment_id
               group by sensor2unit.sensor_id) as unit_name,
              s_tr.tr_name
           from
             core.sensor s
             join core.v_equipment et on et.equipment_id = s.sensor_id
             left join s_tr on s_tr.sensor_id = s.sensor_id
     where et.equipment_status_id != core.jf_equipment_status_pkg$cn_status_decommissioned()
       and
       ( l_rf_db_method is null
         or
          (
           --связь блока и датчика
           l_rf_db_method = any(l_arr_rf_db_method_sensor2unit) and
           --есть схема подключения датчик-блок
           exists (select 1 from core.sensor2unit_model s2um
           where s2um.equipment_type_sensor_id  =  et.equipment_type_id
                 and s2um.equipment_type_unit_id = l_equipment_type_unit_id) and
           --есть привязка к машине
           s_tr.tr_id is not null and
           --ББ привязан к той же машине
           exists (select 1 from core.unit2tr u2tr where u2tr.tr_id = s_tr.tr_id and u2tr.unit_id = l_unit_id) and
           --датчик еще не привяан к этому ББ
           not exists (select 1 from core.sensor2unit s2u where s2u.sensor_id = et.equipment_id and s2u.unit_id = l_unit_id )
         )
          or
           (l_rf_db_method = any(l_arr_rf_db_method_sensor2tr) and
           --датчик не привязан к тс
           not exists  (select 1 from core.sensor2tr s2tr  where s2tr.sensor_id = et.equipment_id )

           )
        )
;

end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sensor_select_pkg$of_rows"(numeric, text)
  OWNER TO adv;