-- Function: core."jf_gn2tr_h_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_gn2tr_h_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_gn2tr_h_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_tr_id bigint;
begin
  l_tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);

  open p_rows for

SELECT 
hd.row_id as tr_id,
hd.change_date as dt_begin,
hdi.new_value as garage_num,
e1.name_short as depo_name,
e2.name_short as territory_name,
'' as reason,
'' as link
  FROM adm.history_data hd 
  join adm.history_data_item hdi on hd.history_data_id=hdi.history_data_id
  join core.tr ct on cast(ct.tr_id as TEXT)= hd.row_id 
  join core.entity e1 ON e1.entity_id = ct.depo_id
  join core.entity e2 ON e2.entity_id = ct.territory_id
where 1=1
and table_schema = 'core'
and table_name = 'tr'
and hdi.column_name = 'garage_num'
and hd.row_id = l_tr_id ::text
   order by hd.change_date desc
/*  
SELECT tr.tr_id as tr_id,
lower(tr.sys_period) as dt_begin,
tr.garage_num as garage_num,
e1.name_short as depo_name,
e2.name_short as territory_name,
'' as reason,
'' as link
  from hist.core_tr tr
join  core.entity e1 ON e1.entity_id = tr.depo_id
join  core.entity e2 ON e2.entity_id = tr.territory_id
 where tr.tr_id= l_tr_id
 order by lower(tr.sys_period)
*/

 ;

end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_gn2tr_h_pkg$of_rows"(numeric, text)
  OWNER TO adv;
