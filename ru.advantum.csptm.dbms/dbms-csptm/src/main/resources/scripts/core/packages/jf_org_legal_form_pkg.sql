﻿
CREATE OR REPLACE FUNCTION core."jf_org_legal_form_pkg$of_rows"(
  IN p_id_account numeric,
  OUT p_rows refcursor,
  IN p_attr text)
  RETURNS refcursor AS
$BODY$
declare
begin
  open p_rows for
  select
    org_legal_form_id,
    name_full,
    name_short
  from core.org_legal_form;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION core."jf_org_legal_form_pkg$of_rows"(numeric, text)
OWNER TO adv;
