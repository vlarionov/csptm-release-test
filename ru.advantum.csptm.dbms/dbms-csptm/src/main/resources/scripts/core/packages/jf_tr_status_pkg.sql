CREATE OR REPLACE FUNCTION core.jf_tr_status_pkg$of_rows(in p_id_account numeric, out p_rows refcursor, in p_attr text)
  RETURNS refcursor
AS
  $BODY$
  declare 
begin 
 open p_rows for 
      select 
        tr_status_id, 
        name
      from core.tr_status; 
end;
$BODY$
LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION core.jf_tr_status_pkg$of_update (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r core.tr_status%rowtype;
begin
   l_r := core.jf_tr_status_pkg$attr_to_rowtype(p_attr);

   update core.tr_status set
          name = l_r.name
   where
          tr_status_id = l_r.tr_status_id;

   return null;
end;

$$;

CREATE OR REPLACE FUNCTION core.jf_tr_status_pkg$of_insert (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r core.tr_status%rowtype;
begin
   l_r := core.jf_tr_status_pkg$attr_to_rowtype(p_attr);

   insert into core.tr_status select l_r.*;

   return null;
end;

$$;

CREATE OR REPLACE FUNCTION core.jf_tr_status_pkg$of_delete (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r core.tr_status%rowtype;
begin
   l_r := core.jf_tr_status_pkg$attr_to_rowtype(p_attr);

   delete from  core.tr_status where  tr_status_id = l_r.tr_status_id;

   return null;
end;

$$;
