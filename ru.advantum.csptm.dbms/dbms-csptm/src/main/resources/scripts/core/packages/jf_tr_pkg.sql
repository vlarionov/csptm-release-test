create or replace function core.jf_tr_pkg$attr_to_rowtype(p_attr text)
  returns core.tr
as
$BODY$
declare
  l_r core.tr%rowtype;
begin
  l_r.tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
  l_r.seat_qty := jofl.jofl_pkg$extract_number(p_attr, 'seat_qty', true);
  l_r.seat_qty_total := jofl.jofl_pkg$extract_number(p_attr, 'seat_qty_total', true);
  l_r.seat_qty_disabled_people := jofl.jofl_pkg$extract_number(p_attr, 'seat_qty_disabled_people', true);
  l_r.has_low_floor := jofl.jofl_pkg$extract_boolean(p_attr, 'has_low_floor', true);
  l_r.has_conditioner := jofl.jofl_pkg$extract_boolean(p_attr, 'has_conditioner', true);
  l_r.has_wheelchair_space := jofl.jofl_pkg$extract_boolean(p_attr, 'has_wheelchair_space', true);
  l_r.has_bicicle_space := jofl.jofl_pkg$extract_boolean(p_attr, 'has_bicicle_space', true);
  l_r.garage_num := jofl.jofl_pkg$extract_number(p_attr, 'garage_num', true);
  l_r.licence := jofl.jofl_pkg$extract_varchar(p_attr, 'licence', true);
  l_r.serial_num := jofl.jofl_pkg$extract_varchar(p_attr, 'serial_num', true);
  l_r.year_of_issue := jofl.jofl_pkg$extract_number(p_attr, 'year_of_issue', true);
  l_r.dt_begin := jofl.jofl_pkg$extract_date(p_attr, 'dt_begin', true);
  l_r.guarantee_end := jofl.jofl_pkg$extract_date(p_attr, 'guarantee_end', true);
  l_r.dt_end := jofl.jofl_pkg$extract_date(p_attr, 'dt_end', true);
  l_r.tr_type_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_type_id', true);
  l_r.tr_status_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_status_id', true);
  l_r.tr_model_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_model_id', true);
  l_r.territory_id := jofl.jofl_pkg$extract_number(p_attr, 'territory_id', true);
  l_r.depo_id := jofl.jofl_pkg$extract_number(p_attr, 'depo_id', true);
  l_r.garage_num_add := jofl.jofl_pkg$extract_number(p_attr, 'garage_num_add', true);
  l_r.equipment_afixed := jofl.jofl_pkg$extract_varchar(p_attr, 'equipment_afixed', true);
  l_r.tr_schema_install_id:= jofl.jofl_pkg$extract_number(p_attr, 'tr_schema_install_id', true);
  return l_r;
end;
$BODY$
language plpgsql volatile;

-----------------------------------------

-- DROP FUNCTION core.jf_tr_pkg$of_rows(NUMERIC, text);


-- TODO  постараться избежать этой функции . нужна для получения в of_rows записей без territory_id
create or replace function core.jf_tr_pkg$find_territory(p_tr_id bigint, p_depo_id bigint)
  returns bigint as
$$
declare l_territory_id bigint;
begin
  l_territory_id = (select tr.territory_id
                    from core.tr tr
                    where tr.tr_id = p_tr_id);
  if l_territory_id is not null
  then return l_territory_id;
  else
    l_territory_id = (select c2d.territory_id
                      from core.depo2territory c2d
                      where c2d.depo_id = p_depo_id
                      limit 1);
    return l_territory_id;
  end if;
end;
$$
language plpgsql;


create or replace function core.jf_tr_pkg$of_rows(
      p_id_account numeric,
  out p_rows       refcursor,
      p_attr       text
)
  returns refcursor as
$body$
declare

  l_available_trs_ids    bigint [] := array(select adm.account_data_realm_pkg$get_trs_by_all(p_id_account));

  l_rf_db_method         text := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true);
  l_rf_depo_id           bigint := jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', true);
  l_list_depo_id         bigint [] := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_depo_id', true);
  l_list_territory_id    bigint [] := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_territory_id', true);
  l_tr_model_id          core.tr_model.tr_model_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_model_id', true);
  l_tr_capacity_id       core.tr_model.tr_model_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_capacity_id',
                                                                                        true);
  l_tr_status_id         core.tr_model.tr_model_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_status_id', true);
  l_tr_type_id           core.tr_model.tr_model_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id', true);
  l_garage_num           text := jofl.jofl_pkg$extract_varchar(p_attr, 'f_garage_num_TEXT', true);

  l_tr_schema_install_id numeric;

  l_singe_row            text;
  l_id_tr                bigint;
  l_tt_variant_id int;

  l_excluded_trs_ids    bigint [];
begin

  if l_rf_db_method in ('oud.tt_action_fact') then
              l_tt_variant_id = coalesce(jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true), -1);
              select array_agg(depo_id)
              into l_list_depo_id
                from rts.route
                join rts.depo2route on route.route_id = depo2route.route_id
                join rts.route_variant on route.current_route_variant_id = route_variant.route_variant_id
                join ttb.tt_variant on tt_variant.route_variant_id = route_variant.route_variant_id
                where tt_variant_id = l_tt_variant_id;

                select tr_type_id
                into l_tr_type_id
                from rts.route
                join rts.route_variant on route.current_route_variant_id = route_variant.route_variant_id
                join ttb.tt_variant on tt_variant.route_variant_id = route_variant.route_variant_id
                where tt_variant_id = l_tt_variant_id;
                /*
                select array_agg(tr_id)
                into l_excluded_trs_ids
                from ttb.tt_out
                where tt_variant_id = l_tt_variant_id
                and not tt_out.sign_deleted
                and tr_id is not null;
                                */
  end if;

  l_singe_row := json_array_elements((p_attr :: json ->> 'tr_link') :: json) -> 'JUMP' -> 'METHOD' :: text;
  --insert into core.test$info(p_attr)VALUES (p_attr);
  if l_singe_row = '"core.tr"'
  then
    l_singe_row := p_attr :: json ->> 'tr_id';
    l_id_tr := l_singe_row :: bigint;
    open p_rows for
    select
      tr.tr_id,
      tr.seat_qty,
      tr.has_low_floor,
      tr.has_conditioner,
      tr.garage_num,
      tr.licence,
      tr.serial_num,
      tr.year_of_issue,
      tr.dt_begin,
      tr.dt_end,
      tr.guarantee_end,
      tr.tr_type_id,
      tr_model.tr_capacity_id,
      tr.tr_status_id,
      tr.tr_model_id,
      tr.territory_id,
      tr.has_wheelchair_space,
      tr.has_bicicle_space,
      tr.seat_qty_disabled_people,
      tr.seat_qty_total,
      tr_status.name                                              tr_status_name,
      tr_model.name                                               tr_model_name,
      tr_type.name                                                tr_type_name,
      entity.name_short                                           territory_name,
      tr_capacity.qty                                             tr_capacity_qty,
      tr_capacity.short_name                                   as tr_capacity_short_name,
      tr.depo_id,
      depo.name_short                                          as depo_name,
      tr.garage_num_add,
      tr.equipment_afixed,
      tr.tr_schema_install_id,
      (select t.install_name
       from core.tr_schema_install t
       where t.tr_schema_install_id = tr.tr_schema_install_id) as tr_schema_install_name
    from core.tr
      join core.tr_model on tr_model.tr_model_id = tr.tr_model_id
      join core.tr_status on tr_status.tr_status_id = tr.tr_status_id
      join core.tr_type on tr_type.tr_type_id = tr.tr_type_id
      join core.territory on territory.territory_id = core.jf_tr_pkg$find_territory(tr.tr_id, tr.depo_id)
      join core.entity on entity.entity_id = territory.territory_id
      join core.tr_capacity on tr_capacity.tr_capacity_id = tr_model.tr_capacity_id
      join core.entity depo on depo.entity_id = tr.depo_id
    where
      tr.tr_id = any (l_available_trs_ids)
      and tr.tr_id = l_id_tr;
    return;
  end if;


  l_tr_schema_install_id:= jofl.jofl_pkg$extract_number(p_attr, 'tr_schema_install_id', true);

    open p_rows for
    select
      tr.tr_id,
      tr.seat_qty,
      tr.has_low_floor,
      tr.has_conditioner,
      tr.garage_num,
      tr.licence,
      tr.serial_num,
      tr.year_of_issue,
      tr.dt_begin,
      tr.guarantee_end,
      tr.dt_end,
      tr.tr_type_id,
      tr_model.tr_capacity_id,
      tr.tr_status_id,
      tr.tr_model_id,
      tr.territory_id,
      tr.has_wheelchair_space,
      tr.has_bicicle_space,
      tr.seat_qty_disabled_people,
      tr.seat_qty_total,
      tr_status.name                                              tr_status_name,
      tr_model.name                                               tr_model_name,
      tr_type.name                                                tr_type_name,
      entity.name_short                                           territory_name,
      tr_capacity.qty                                             tr_capacity_qty,
      tr_capacity.short_name                                   as tr_capacity_short_name,
      tr.depo_id,
      depo.name_short                                          as depo_name,
      tr.garage_num_add,
      tr.equipment_afixed,
      tr.tr_schema_install_id,
      (select t.install_name
       from core.tr_schema_install t
       where t.tr_schema_install_id = tr.tr_schema_install_id) as tr_schema_install_name
    from core.tr
      join core.tr_model on tr_model.tr_model_id = tr.tr_model_id
      join core.tr_status on tr_status.tr_status_id = tr.tr_status_id
      join core.tr_type on tr_type.tr_type_id = tr.tr_type_id
      join core.territory on territory.territory_id = core.jf_tr_pkg$find_territory(tr.tr_id, tr.depo_id)
      join core.entity on entity.entity_id = territory.territory_id
      join core.tr_capacity on tr_capacity.tr_capacity_id = tr_model.tr_capacity_id
      join core.entity depo on depo.entity_id = tr.depo_id
    where
      tr.tr_id = any (l_available_trs_ids)
      and (
        (l_rf_db_method is null
         and (tr.depo_id = any (l_list_depo_id))
         and (tr.territory_id = any (l_list_territory_id) or array_length(l_list_territory_id, 1) is null)
         and (l_tr_model_id is null or tr.tr_model_id = l_tr_model_id)
         and (l_tr_capacity_id is null or tr_model.tr_capacity_id = l_tr_capacity_id)
         and (l_tr_type_id is null or tr.tr_type_id = l_tr_type_id)
         and (l_tr_status_id is null or tr.tr_status_id = l_tr_status_id)
         and (l_garage_num = '' or tr.garage_num :: text like '%' || l_garage_num || '%')
         and (l_tr_schema_install_id is null or tr.tr_schema_install_id = l_tr_schema_install_id)
        )
        or
        (l_rf_db_method is not null
         and l_rf_db_method not in
             ('mnt.bnsr_request_list', 'mnt.unit_session_log', 'kdbo.fuel_waste_report', 'kdbo.reverse_drive_report', 'kdbo.eqp_on_tr_report', 'oud.tt_action_fact')
         and (l_tr_schema_install_id is null or tr.tr_schema_install_id = l_tr_schema_install_id))
        or
        (l_rf_db_method in ('mnt.bnsr_request_list', 'mnt.unit_session_log')
         and tr.depo_id = l_rf_depo_id)
        or (
          l_rf_db_method in ('kdbo.eqp_on_tr_report', 'kdbo.fuel_waste_report', 'kdbo.reverse_drive_report')
          and (tr.depo_id = any (l_list_depo_id))
        )
        or (
          l_rf_db_method = 'mnt.route_without_order' and (l_tr_type_id is null or tr_type.tr_type_id = l_tr_type_id)
        )
        or (
           l_rf_db_method = 'oud.tt_action_fact'
           and tr.depo_id = any (l_list_depo_id)
           and tr.tr_type_id = l_tr_type_id
           and (
                not tr.tr_id = any (l_excluded_trs_ids)
                or
                array_length(l_excluded_trs_ids, 1) is null
                )
        )
      );
  --end if;

end;
$body$
language 'plpgsql'
volatile
called on null input
security invoker
cost 100;

--------------------------------------------
create or replace function core.jf_tr_pkg$of_insert(p_id_account numeric, p_attr text)
  returns text
as
$body$
declare
  l_r core.tr%rowtype;
begin
  l_r := core.jf_tr_pkg$attr_to_rowtype(p_attr);
  if coalesce(l_r.tr_id, -1) < 0
  then
    l_r.tr_id := nextval('core.tr_tr_id_seq');
  end if;

  l_r.tr_status_id := 1;
  l_r.park_instance_id = '-';
  insert into core.tr select l_r.*;

  return null;
end;
$body$
language plpgsql volatile;
--------------------------------------------
create or replace function core.jf_tr_pkg$of_update(p_id_account numeric, p_attr text)
  returns text
as
$body$
declare
  l_r          core.tr%rowtype;
  l_equip_list bigint [];
  i            bigint;
begin
  l_r := core.jf_tr_pkg$attr_to_rowtype(p_attr);

  if not (select exists
  (select 1
   from core.depo2territory d2t
   where d2t.territory_id = l_r.territory_id and (d2t.depo_id = l_r.depo_id ))
  )
  then
    raise exception using message = '<<Неправильно указана территория!>>';
  end if;

  perform core.jf_equipment_pkg$of_update(p_id_account, p_attr);
  l_equip_list := (select array(select e2t.equipment_id
                                from core.v_equipment2tr4all e2t
                                where e2t.tr_id = l_r.tr_id) );
--   union  select * from core.v_equipment2tr4all ve where ve. );
  foreach i in array l_equip_list
  loop
    perform core.jf_equipment_pkg$switch_depo(i, l_r.depo_id, l_r.territory_id);
  end loop;


  update core.tr
  set
    seat_qty                 = l_r.seat_qty,
    seat_qty_total           = l_r.seat_qty_total,
    seat_qty_disabled_people = l_r.seat_qty_disabled_people,
    has_low_floor            = l_r.has_low_floor,
    has_conditioner          = l_r.has_conditioner,
    has_wheelchair_space     = l_r.has_wheelchair_space,
    has_bicicle_space        = l_r.has_bicicle_space,
    garage_num               = l_r.garage_num,
    licence                  = l_r.licence,
    serial_num               = l_r.serial_num,
    year_of_issue            = l_r.year_of_issue,
    dt_begin                 = l_r.dt_begin,
    guarantee_end            = l_r.guarantee_end,
    dt_end                   = l_r.dt_end,
    tr_type_id               = l_r.tr_type_id,
    tr_status_id             = l_r.tr_status_id,
    tr_model_id              = l_r.tr_model_id,
    territory_id             = l_r.territory_id,
    depo_id                  = l_r.depo_id,
    garage_num_add           = l_r.garage_num_add,
    equipment_afixed         = l_r.equipment_afixed,
    tr_schema_install_id     = l_r.tr_schema_install_id
  where
    tr_id = l_r.tr_id;

  return null;
end;
$body$
language plpgsql volatile;

--------------------------------------------
create or replace function core.jf_tr_pkg$of_delete(p_id_account numeric, p_attr text)
  returns text
as
$body$
declare
  l_r core.tr%rowtype;
begin
  l_r := core.jf_tr_pkg$attr_to_rowtype(p_attr);

  delete from core.tr
  where tr_id = l_r.tr_id;

  return null;
end;
$body$
language plpgsql volatile;
--------------------------------------------
create or replace function core.jf_tr_pkg$of_breakdown(p_id_account numeric, p_attr text)
  returns text
as
$body$
declare
  l_r    core.breakdown%rowtype;
  l_attr text;
begin
  l_r.breakdown_id := nextval('core.breakdown_breakdown_id_seq');
  l_r.num := nextval('core.breakdown_num_new_seq');
  l_r.is_confirmed := false;
  l_r.account_id := p_id_account;
  l_r.dt_begin := current_timestamp;
  l_r.breakdown_status_id := core.jf_breakdown_pkg$cn_status_open();
  l_r.description := jofl.jofl_pkg$extract_varchar(p_attr, 'STR_FIELD_DESCRIPTIONTEXT', true);
  l_r.tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
  select tr.territory_id
  into l_r.territory_id
  from core.tr
  where tr.tr_id = l_r.tr_id;


  l_attr:=row_to_json(l_r);

  l_attr := core.jf_breakdown_pkg$of_insert(p_id_account, l_attr);

  return null;

end;
$body$
language plpgsql volatile;

--------------------------------------------
create or replace function core.jf_tr_pkg$of_link_unit(p_id_account numeric, p_attr text)
  returns text
as
$body$
declare
  l_equipment_id core.equipment.equipment_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_id', true);
begin
  return core.jf_unit2tr_pkg$of_insert(p_id_account,
                                       (('{"equipment_id":"' || l_equipment_id :: text || '"}') :: jsonb ||
                                        p_attr :: jsonb) :: json :: text);
end;
$body$
language plpgsql volatile;

--------------------------------------------
create or replace function core.jf_tr_pkg$of_link_sensor(p_id_account numeric, p_attr text)
  returns text
as
$body$
declare
  l_equipment_id core.equipment.equipment_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_id', true);
  l_is_sensor    boolean;
begin
  select into l_is_sensor sign(count(1))
  from core.sensor
  where sensor_id = l_equipment_id;
  if l_is_sensor
  then
    return core.jf_sensor2tr_pkg$of_insert(p_id_account,
                                           (('{"equipment_id":"' || l_equipment_id :: text || '"}') :: jsonb ||
                                            p_attr :: jsonb) :: json :: text);
  else
    return core.jf_equipment2tr_pkg$of_insert(p_id_account,
                                              (('{"equipment_id":"' || l_equipment_id :: text || '"}') :: jsonb ||
                                               p_attr :: jsonb) :: json :: text);
  end if;
end;
$body$
language plpgsql volatile;

--------------------------------------------
--Проверка комплектации

create or replace function core.jf_tr_pkg$of_check_equipment(
  p_id_account numeric,
  p_attr       text)
  returns text as
$BODY$
declare
  l_tr_id      core.tr.tr_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
  l_garage_num core.tr.garage_num%type := jofl.jofl_pkg$extract_number(p_attr, 'garage_num', true);
begin
  --http://redmine.advantum.ru/issues/19427#change-103727
  if l_garage_num = -1
  then
    return null;
  end if;
  return kdbo.jf_tr_check_equipment_pkg$run_check_equipment(p_id_account, l_tr_id, false);

end;
$BODY$
language plpgsql volatile
cost 100;
alter function core.jf_tr_pkg$of_check_equipment( numeric, text )
owner to adv;