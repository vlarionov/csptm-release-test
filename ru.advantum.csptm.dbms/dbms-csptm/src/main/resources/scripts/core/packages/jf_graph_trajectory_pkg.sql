﻿create or replace function core.jf_graph_trajectory_pkg$of_rows
    (p_account_id in  numeric,
     p_attr       in  text default '{}',
     p_rows       out refcursor)
    returns refcursor
as $$ declare
    l_tr_list text := jofl.jofl_pkg$extract_varchar(p_attr, 'TRJ_LIST', true);
begin
    open p_rows for
    with table_id as
    (
        select a::numeric
        from regexp_split_to_table(l_tr_list, ',') a
    )
    select
        case when (select t.muid in (ti.a))
            then t.muid:: text
        when (select r.round_id in (ti.a))
            then r.round_id :: text
        else 0:: text end as route_trajectory_muid,
        t.muid,
        t.wkt_geom
    from gis.route_trajectories t
        left join rts.round2gis r on t.muid = r.route_trajectory_muid
        left join table_id ti on 1=1
    where (t.muid in (ti.a)
           or r.round_id in (ti.a)
    )
    order by t.muid;
end;
$$ language plpgsql;
