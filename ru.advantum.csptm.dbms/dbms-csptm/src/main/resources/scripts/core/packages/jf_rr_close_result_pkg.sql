create or replace function core.jf_rr_close_result_pkg$attr_to_rowtype(p_attr text) returns core.rr_close_result
  language plpgsql
as $BODY$
declare
   l_r core.rr_close_result%rowtype;
begin
   l_r.rr_close_result_id := jofl.jofl_pkg$extract_number(p_attr, 'rr_close_result_id', true);
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true);

   return l_r;
end;
$BODY$
;

create or replace function core.jf_rr_close_result_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
  language plpgsql
as $BODY$
declare
begin
 open p_rows for
      select
        rr_close_result_id,
        name
      from core.rr_close_result
  ORDER BY rr_close_result_id;
end;
$BODY$
;

create or replace function core.jf_rr_close_result_pkg$of_insert(p_id_account numeric, p_attr text) returns text
  language plpgsql
as $BODY$
declare
   l_r core.rr_close_result%rowtype;
begin
   l_r := core.jf_rr_close_result_pkg$attr_to_rowtype(p_attr);
   l_r.rr_close_result_id := nextval('core.rr_close_result_rr_close_result_id_seq'::regclass);

   insert into core.rr_close_result select l_r.*;

   return null;
end;
$BODY$
;

create or replace function core.jf_rr_close_result_pkg$of_delete(p_id_account numeric, p_attr text) returns text
  language plpgsql
as $BODY$
declare
   l_r core.rr_close_result%rowtype;
begin
   l_r := core.jf_rr_close_result_pkg$attr_to_rowtype(p_attr);

   delete from core.rr_close_result
     where rr_close_result_id = l_r.rr_close_result_id;

   return null;
end;
$BODY$
;

create or replace function core.jf_rr_close_result_pkg$of_update(p_id_account numeric, p_attr text) returns text
  language plpgsql
as $BODY$
declare
   l_r core.rr_close_result%rowtype;
begin
   l_r := core.jf_rr_close_result_pkg$attr_to_rowtype(p_attr);

   return null;
end;
$BODY$
;

