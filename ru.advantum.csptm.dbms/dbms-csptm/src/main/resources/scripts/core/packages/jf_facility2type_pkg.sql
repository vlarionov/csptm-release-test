﻿

--*************************************************************************************************
DROP FUNCTION core."jf_facility2type_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_facility2type_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.facility2type AS
$BODY$ 
declare 
   l_r core.facility2type%rowtype;
begin
   l_r.facility_type_id := jofl.jofl_pkg$extract_number(p_attr, 'facility_type_id', true);
   l_r.facility_id := jofl.jofl_pkg$extract_number(p_attr, 'facility_id', true);
   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_facility2type_pkg$attr_to_rowtype"(text)
  OWNER TO adv;

--*************************************************************************************************
 DROP FUNCTION core."jf_facility2type_pkg$of_delete"(numeric, text);
CREATE OR REPLACE FUNCTION core."jf_facility2type_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.facility2type%rowtype;
begin 
   l_r := core.jf_facility2type_pkg$attr_to_rowtype(p_attr);

   delete from  core.facility2type where  facility_id = l_r.facility_id and facility_type_id = l_r.facility_type_id;

   return null;

  exception
  when 	foreign_key_violation then
    RAISE  EXCEPTION '<<Удаление невозможно! запись используется в других документах системы>>';

end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_facility2type_pkg$of_delete"(numeric, text)
  OWNER TO adv;

--*************************************************************************************************
 DROP FUNCTION core."jf_facility2type_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_facility2type_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
    l_r core.facility2type%rowtype;
begin
    l_r := core.jf_facility2type_pkg$attr_to_rowtype(p_attr);

    insert into core.facility2type select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_facility2type_pkg$of_insert"(numeric, text)
  OWNER TO adv;
--*************************************************************************************************

CREATE OR REPLACE FUNCTION core."jf_facility2type_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$
declare
  l_r core.facility2type%rowtype;
begin
  l_r.facility_id := jofl.jofl_pkg$extract_number(p_attr, 'facility_id', true);

 open p_rows for 
      select 
        f.facility_type_id, 
        ft.name as facility_type_name,
        f.facility_id
      from core.facility2type f
      join core.facility_type ft on ft.facility_type_id = f.facility_type_id
      where f.facility_id = l_r.facility_id;


end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_facility2type_pkg$of_rows"(numeric, text)
  OWNER TO adv;

--*************************************************************************************************

