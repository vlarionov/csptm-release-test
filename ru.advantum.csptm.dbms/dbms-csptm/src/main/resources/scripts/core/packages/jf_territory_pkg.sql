--DROP TYPE core.jf_territory_type CASCADE;

do $$
begin
  if not exists(select * from pg_type where typname = 'jf_territory_type') THEN

    CREATE TYPE core.jf_territory_type AS
      (r_territory core.territory,
       r_entity    core.entity);

    ALTER TYPE core.jf_territory_type
      OWNER TO adv;

  end if;
end$$;

--DROP FUNCTION core.jf_territory_pkg$attr_to_rowtype( TEXT);

CREATE OR REPLACE FUNCTION core.jf_territory_pkg$attr_to_rowtype(p_attr TEXT)
  RETURNS core.jf_territory_type
AS
  $BODY$
  DECLARE
    l_r_return    core.jf_territory_type;
    l_r_territory core.territory;
    l_r_entity    core.entity;

  BEGIN

    l_r_territory.territory_id := jofl.jofl_pkg$extract_number(p_attr, 'territory_id', TRUE);
    l_r_territory.code := jofl.jofl_pkg$extract_number(p_attr, 'code', TRUE);
    l_r_territory.dt_begin := jofl.jofl_pkg$extract_date(p_attr, 'dt_begin', TRUE);
    l_r_territory.dt_end := jofl.jofl_pkg$extract_date(p_attr, 'dt_end', TRUE);

    l_r_entity.name_full := jofl.jofl_pkg$extract_varchar(p_attr, 'name_full', TRUE);
    l_r_entity.name_short := jofl.jofl_pkg$extract_varchar(p_attr, 'name_short', TRUE);
    l_r_entity.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', TRUE);
    l_r_entity.entity_id := jofl.jofl_pkg$extract_varchar(p_attr, 'territory_id', TRUE);

    l_r_return.r_entity := l_r_entity;
    l_r_return.r_territory := l_r_territory;

    RETURN l_r_return;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_territory_pkg$of_rows(IN p_id_account NUMERIC, OUT p_rows REFCURSOR, IN p_attr TEXT)
  RETURNS REFCURSOR
AS
  $BODY$
  DECLARE
   l_rf_db_method text := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
   l_rf_depo_id	bigint := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', TRUE), jofl.jofl_pkg$extract_number(p_attr, 'depo_id', TRUE));
   l_list_depo_id         bigint[]  := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_depo_id', true);
  BEGIN
    OPEN p_rows FOR
    SELECT
      territory.territory_id,
      territory.territory_id as entity_id,
      territory.code,
      territory.dt_begin,
      territory.dt_end,
      entity4territory.name_full,
      entity4territory.name_short,
      entity4territory.comment
    FROM core.territory
      JOIN core.entity entity4territory ON entity4territory.entity_id = territory.territory_id
      JOIN core.depo2territory d2t on d2t.territory_id = territory.territory_id
      where
        l_rf_db_method is null or
        l_rf_db_method in ('core.breakdown') or
            (d2t.depo_id = any(l_list_depo_id)   or d2t.depo_id = l_rf_depo_id)
         ;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_territory_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
  $BODY$
  DECLARE
    l_r_return    core.jf_territory_type;
    l_r_territory core.territory;
    l_r_entity    core.entity;
  BEGIN
    l_r_return := core.jf_territory_pkg$attr_to_rowtype(p_attr);
    l_r_entity := l_r_return.r_entity;
    l_r_territory := l_r_return.r_territory;

    l_r_entity.entity_id := nextval('core.entity_entity_id_seq');

    INSERT INTO core.entity SELECT l_r_entity.*
    RETURNING entity_id
      INTO l_r_territory.territory_id;

    INSERT INTO core.territory SELECT l_r_territory.*;

    RETURN NULL;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION core.jf_territory_pkg$of_update(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
  $BODY$
  DECLARE
    l_r_return    core.jf_territory_type;
    l_r_territory core.territory;
    l_r_entity    core.entity;
  BEGIN
    l_r_return := core.jf_territory_pkg$attr_to_rowtype(p_attr);
    l_r_entity := l_r_return.r_entity;
    l_r_territory := l_r_return.r_territory;

    UPDATE core.territory
    SET
      code       = l_r_territory.code,
      dt_begin   = l_r_territory.dt_begin,
      dt_end     = l_r_territory.dt_end
    WHERE
      territory_id = l_r_territory.territory_id;

    UPDATE core.entity
    SET
      name_full  = l_r_entity.name_full,
      name_short = l_r_entity.name_short,
      comment    = l_r_entity.comment
    WHERE
      entity_id = l_r_entity.entity_id;

    RETURN NULL;

  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION core.jf_territory_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS
  $BODY$
  DECLARE
    l_r_return    core.jf_territory_type;
    l_r_territory core.territory;
    l_r_entity    core.entity;
  BEGIN
    l_r_return := core.jf_territory_pkg$attr_to_rowtype(p_attr);
    l_r_entity := l_r_return.r_entity;
    l_r_territory := l_r_return.r_territory;

    DELETE FROM core.territory
    WHERE territory_id = l_r_territory.territory_id;

    DELETE FROM core.entity
    WHERE entity_id = l_r_entity.entity_id;

    RETURN NULL;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE;
