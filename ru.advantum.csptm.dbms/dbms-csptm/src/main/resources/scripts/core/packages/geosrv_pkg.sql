﻿--
-- Слой "Производственный рейс"
-- csptm:route_round
--

--drop function core.geosrv_pkg$route_round(route_number text, tr_type text, rr_code text, route_direction int, deleted text, rv_muid bigint );

create or replace function core.geosrv_pkg$route_round(p_route_number    text, p_tr_type text, p_rr_code text,
                                                       p_route_direction int, deleted text, p_route_variant_id integer)
    returns table(route_number            text,
                  park_name               text,
                  stop_name_a             text,
                  stop_name_b             text,
                  route_round_type_name   text,
                  trajectory_type_name    text,
                  tk_short_name           text,
                  number                  text,
                  rr_code                 text,
                  length_fixed            real,
                  trajectory_type_muid    bigint,
                  route_trajectories_muid bigint,
                  route_variant_muid      bigint,
                  geom                    PUBLIC.geometry,
                  geom_type               int
    )
as $$
begin

    return query
    with pat as
    (
        select tat.action_type_id
        from ttb.action_type tat
        where ttb.action_type_pkg$is_production_round(tat.action_type_id)
    )
        , rmm as (
        select
            r.round_id as       round_ids,
            MAX(si2r.order_num) max_num,
            min(si2r.order_num) min_num
        from rts.stop_item2round si2r
            join rts.round r on r.round_id = si2r.round_id
            join pat p on p.action_type_id = r.action_type_id
        where r.route_variant_id = p_route_variant_id or p_route_variant_id = 0 or p_route_variant_id isnull
        group by round_ids
    )
        , min_max_stop_num as (
        select distinct
            round_id,
            first_value(stop_item_id)
            over (
                partition by round_id
                order by order_num
                rows between unbounded preceding and unbounded following ) as stop_item_a_id,
            last_value(stop_item_id)
            over (
                partition by round_id
                order by order_num
                rows between unbounded preceding and unbounded following ) as stop_item_b_id
        from rts.stop_item2round si2r
            join rmm r on r.round_ids=si2r.round_id
        where (not sign_deleted)
    )
    select
        tt.short_name || ' ' || rr.route_num as route_number,
        case when entity4depo.name_full is null
            then ''
        else entity4depo.name_full end      as park_name,
        rs_begin.name                       as stop_name_a,
        rs_end.name                         as stop_name_b,
        a.action_type_name                  as route_round_type_name,
        rr.route_num                        as trajectory_type_name,
        tt.short_name                        as tk_short_name,
        rr.route_num                        as number,
        p_rr_code                           as rr_code,
        r.length :: real                    as length_fixed,
        r.move_direction_id :: bigint      as trajectory_type_muid,
        r.round_id :: bigint                as route_trajectorie_muid,
        rv.route_variant_id :: bigint       as route_variant_muid,
        r.geom,
        1                                   as geom_type
    from rts.route rr
        join rts.route_variant rv on rr.route_id = rv.route_id
        join rts.round r on rv.route_variant_id = r.route_variant_id
        join rmm a2 on a2.round_ids=r.round_id
        join core.tr_type tt on rr.tr_type_id = tt.tr_type_id

        join ttb.action_type a on r.action_type_id = a.action_type_id
        left join min_max_stop_num mmsn on mmsn.round_id = a2.round_ids
        left join rts.stop_item ia on ia.stop_item_id = mmsn.stop_item_a_id
        left join rts.stop_location sla on sla.stop_location_id = ia.stop_location_id
        left join rts.stop rs_begin on sla.stop_id = rs_begin.stop_id
        left join rts.stop_item ib on ib.stop_item_id = mmsn.stop_item_b_id
        left join rts.stop_location slb on slb.stop_location_id = ib.stop_location_id
        left join rts.stop rs_end on slb.stop_id = rs_end.stop_id
        --
        left join rts.depo2route d2r on rr.route_id = d2r.route_id
        left join core.depo cd on d2r.depo_id = cd.depo_id
        left join core.entity entity4depo on entity4depo.entity_id = cd.depo_id
    where   ((rv.route_variant_id=rr.current_route_variant_id and p_route_variant_id=0) or ( p_route_variant_id<>0)) and
        (p_route_direction = 0 or r.move_direction_id = p_route_direction)
        and (p_route_number = '' or lower(rr.route_num) = lower(p_route_number))
        and (p_tr_type = '' or lower(tt.short_name) = lower(p_tr_type))
        and (p_rr_code = '' or lower(r.code) = lower(p_rr_code));
end;
$$ language plpgsql;

------------------------------------------------------------------------------------------------------------------------

--
-- Слой "Технологический рейс"
-- csptm:route_null_round
--

--drop function core.geosrv_pkg$route_null_round(route_number text, tr_type text, rr_code text, deleted text, rv_muid bigint );

create or replace function core.geosrv_pkg$route_null_round(p_route_number     text, p_tr_type text,
                                                            p_rnr_code         text, deleted text,
                                                            p_route_variant_id bigint)
    returns table(route_number           text,
                  rnr_type_name          text,
                  stop_name_a            text,
                  stop_name_b            text,
                  length_fixed           real,
                  rnr_code               text,
                  tk_short_name          text,
                  number                 text,
                  route_variant_muid     bigint,
                  geom                   PUBLIC.geometry,
                  route_trajectorie_muid bigint,
                  geom_type              int
    )
as $$
begin
    return query
    with pat as
    (
        select tat.action_type_id
        from ttb.action_type tat
        where ttb.action_type_pkg$is_technical_round(tat.action_type_id)
    )
        , rmm as (
        select
            r.round_id as   round_ids,
            MAX(si2r.order_num) max_num,
            min(si2r.order_num) min_num
        from rts.stop_item2round si2r
            join rts.round r on r.round_id = si2r.round_id
            join pat p on p.action_type_id = r.action_type_id
        where r.route_variant_id = p_route_variant_id or p_route_variant_id = 0 or p_route_variant_id isnull
        group by round_ids
    )
    select
        t.short_name || ' ' || rr.route_num as route_number,
        a.action_type_name                     rnr_type_name,
        rs_begin.name                          stop_name_a,
        rs_end.name                            stop_name_b,
        r.length :: real                    as length_fixed,
        p_rnr_code                          as rnr_code,
        t.short_name                        as tk_short_name,
        rr.route_num                        as number,
        rv.route_variant_id :: bigint       as route_variant_muid,
        r.geom,
        r.round_id :: bigint                as route_trajectorie_muid,
        1                                   as geom_type
    from rts.round r
        join rts.route_variant rv on r.route_variant_id = rv.route_variant_id
        join ttb.action_type a on r.action_type_id = a.action_type_id
        join rts.route rr on rv.route_id = rr.route_id
        join core.tr_type t on rr.tr_type_id = t.tr_type_id
        join rmm a2 on 1 = 1
        join rts.stop_item2round rsir_begin on rsir_begin.round_id = a2.round_ids
        join rts.stop_item rsi_begin on rsi_begin.stop_item_id = rsir_begin.stop_item_id
        join rts.stop_location sla_begin on sla_begin.stop_location_id = rsi_begin.stop_location_id
        join rts.stop rs_begin on rs_begin.stop_id = sla_begin.stop_id

        join rts.stop_item2round rsir_end on rsir_end.round_id = a2.round_ids
        join rts.stop_item rsi_end on rsi_end.stop_item_id = rsir_end.stop_item_id
        join rts.stop_location sla_end on sla_end.stop_location_id = rsi_end.stop_location_id
        join rts.stop rs_end on rs_end.stop_id = sla_end.stop_id
    where
        a2.min_num = rsir_begin.order_num
        and a2.max_num = rsir_end.order_num
        and r.round_id = a2.round_ids
        and (p_route_number = '' or lower(rr.route_num) = lower(p_route_number))
        and (p_tr_type = '' or lower(t.short_name) = lower(p_tr_type))
        and (p_rnr_code = '' or lower(r.code) = lower(p_rnr_code))
        and ((deleted = '' and r.sign_deleted = false) or deleted <> '');
end;
$$ language plpgsql;
------------------------------------------------------------------------------------------------------------------------

--
-- Слой "Остановки производственного рейса"
-- csptm:route_round_stop_places
--

--drop function core.geosrv_pkg$route_round_stop_places(route_number text, tr_type text, rr_code text, route_direction int );

create or replace function core.geosrv_pkg$route_round_stop_places(p_route_number    text,
                                                                   p_tr_type         text,
                                                                   p_rr_code         text,
                                                                   p_route_direction int)
    returns table(sp2r_muid        bigint,
                  sname            text,
                  building_address text,
                  routes_num       text,
                  geom             PUBLIC.geometry,
                  geom_type        int
    )
as $$
begin
    return query
    with r as (
        select r.round_id
        from rts.route rr
            join rts.route_variant rv on rr.current_route_variant_id = rv.route_variant_id
            join core.tr_type t on rr.tr_type_id = t.tr_type_id
            join rts.round r on rv.route_variant_id = r.route_variant_id
            join rts.move_direction md on r.move_direction_id = md.move_direction_id
        where (p_route_number = '' or lower(rr.route_num) = lower(p_route_number))
              and (p_tr_type = '' or lower(t.short_name) = lower(p_tr_type))
              and (p_rr_code = '' or lower(r.code) = lower(p_rr_code))
              and (p_route_direction = 0 or md.move_direction_id = p_route_direction)
    ),
            rname as (
            select
                string_agg(t.short_name || ' ' || rr.route_num, ', ') as routes_num,
                rst.stop_id                                           as stop
            from rts.stop rst
                join rts.stop_location sl on rst.stop_id = sl.stop_id
                join rts.stop_item si on si.stop_location_id = sl.stop_location_id
                join rts.stop_item2round si2r on si.stop_item_id = si2r.stop_item_id
                join rts.round r on si2r.round_id = r.round_id
                join rts.route_variant rv on r.route_variant_id = rv.route_variant_id
                join rts.route rr on rv.route_variant_id = rr.current_route_variant_id
                join core.tr_type t on rr.tr_type_id = t.tr_type_id
            where rv.rv_status_id = 1 :: smallint and (not si2r.sign_deleted)
            group by stop, t.short_name || ' ' || rr.route_num
        )
    select
        i.stop_item_id :: bigint as sp2r_muid,
        rs.name                  as sname,
        l.building_address       as building_address,
        rn.routes_num            as routes_num,
        l.geom                   as geom,
        4                        as geom_type
    from r
        join rts.stop_item2round si2r on si2r.round_id = r.round_id and (not si2r.sign_deleted)
        join rts.stop_item i on si2r.stop_item_id = i.stop_item_id
        join rts.stop_location l on l.stop_location_id = i.stop_location_id
        join rts.stop rs on l.stop_id = rs.stop_id
        join rname rn on rn.stop = rs.stop_id;
end;
$$ language plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
--
-- csptm:ct_gs_speed_ngpt_geom
--
------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function core.geosrv_pkg$ct_gs_speed_ngpt_geom(
    p_calculation_task_id integer,
    p_route_variant_id    integer,
    p_tr_capacity_id      integer,
    p_tr_type             integer,
    p_hour                integer,
    p_week_day_id         integer
)
    returns table(
        geom        PUBLIC.geometry,
        geom_color  text,
        gs_speed_id bigint
    )
as $$
begin
    return query
    select
        gs.geom,
        core.geosrv_pkg$get_color_by_speed(res.avg_speed) as geom_color,
        res.ct_gs_speed_result_id                         as gs_speed_id
    from asd.ct_gs_speed_result res
        join rts.graph_section gs on res.graph_section_id = gs.graph_section_id
        left join rts.route_variant r on r.route_variant_id = res.route_variant_id
        left join rts.route rr on r.route_id = rr.route_id
    where res.calculation_task_id = p_calculation_task_id and
          (p_tr_type = 0 or res.tr_type_id = p_tr_type :: smallint) and
          (p_route_variant_id = 0 or r.route_variant_id = p_route_variant_id) and
          (p_tr_capacity_id = 0 or res.tr_capacity_id = p_tr_capacity_id :: smallint) and
          (p_hour = 0 or EXTRACT(epoch from (res.hour)) = p_hour) and
          (p_week_day_id = 0 or res.week_day_id = p_week_day_id :: smallint);
end;
$$ language plpgsql;

-----------------------------------------------------------------------------------------------------------------------------------------------
--drop function core."geosrv_pkg$ct_gs_speed_ngpt_geom"( integer, integer, integer, integer, integer, integer );
------------------------------

--
-- Слой "Cредняя скорость на графе НГПТ"
-- csptm:avr_speed_graph_ngpt
--

--drop function core.geosrv_pkg$ct_gs_speed_ngpt(p_calculation_task_id integer, p_transport_kind_muid bigint, p_route_muid bigint, p_vehicle_type_muid bigint, p_hour int, p_week_day_id int );

create or replace function core.geosrv_pkg$ct_gs_speed_ngpt(p_calculation_task_id   integer,
                                                            p_tr_type_id            integer,
                                                            p_route_id              integer,
                                                            p_ct_gs_speed_result_id integer,
                                                            p_hour                  integer,
                                                            p_week_day_id           integer)
    returns table(
        calculation_task_id   int,
        week_day_id           smallint,
        hour                  time,
        transport_kind_muid   smallint,
        route_muid            integer,
        vehicle_type_muid     smallint,
        graph_section_muid    integer,
        avg_speed             double precision,
        tk_name               text,
        route_name            text,
        week_day_name         text,
        vht_name              text,
        gs_length             double precision,
        geom                  geometry,
        geom_color            text,
        ct_gs_speed_result_id bigint
    )
as $$
begin
    return query
    select
        res.calculation_task_id,
        res.week_day_id,
        res.hour,
        res.tr_type_id,
        res.route_variant_id                              route_muid,
        res.tr_capacity_id,
        res.graph_section_id                              graph_section_muid,
        round(res.avg_speed)                              avg_speed,

        case gss.detail_transp_kind
        when true
            then tk.name
        else
            (select string_agg(tt.name, ' ,')
             from core.tr_type tt
             where exists(select 1
                          from asd.tr_type2ct t
                          where t.calculation_task_id = res.calculation_task_id
                                and t.tr_type_id = tt.tr_type_id))
        end                                               tk_name,

        case gss.detail_route
        when true
            then r.route_num
        else
            case (select count(1)
                  from asd.route2ct r2ct
                  where r2ct.calculation_task_id = res.calculation_task_id)
            when 1
                then (select rts.route_num
                      from asd.route2ct r2ct
                          join rts.route rts on rts.route_id = r2ct.route_id
                      where r2ct.calculation_task_id = res.calculation_task_id)
            else '<Список...>' end
        end                                               route_name,

        case gss.detail_route
        when true
            then wd.wd_full_name
        else
            (select string_agg(week_day.wd_short_name, ' ,')
             from core.week_day
             where exists(select 1
                          from asd.week_day2ct t
                          where t.calculation_task_id = res.calculation_task_id
                                and t.week_day_id = week_day.week_day_id)
            )
        end                                               week_day_name,

        case ct.detail_capacity
        when true
            then cp.full_name
        else
            (select string_agg(cp.short_name, ' ,')
             from core.tr_capacity cp
             where exists(select 1
                          from asd.tr_capacity2ct t
                          where t.calculation_task_id = res.calculation_task_id
                                and t.tr_capacity_id = cp.tr_capacity_id))
        end                                               vht_name,

        gs.length                                         gs_length,
        gs.geom,
        core.geosrv_pkg$get_color_by_speed(res.avg_speed) geom_color,
        res.ct_gs_speed_result_id

    from asd.ct_gs_speed_result res
        join asd.calculation_task ct on ct.calculation_task_id = res.calculation_task_id
        join asd.ct_graph_section_speed gss on gss.calculation_task_id = res.calculation_task_id
        join asd.hours2ct h2ct on h2ct.calculation_task_id = res.calculation_task_id
        left join core.tr_type tk on tk.tr_type_id = res.tr_type_id
        left join rts.route_variant rv on rv.route_variant_id = res.route_variant_id
        left join rts.route r on rv.route_id = r.route_id
        left join core.tr_capacity cp on cp.tr_capacity_id = res.tr_capacity_id
        left join rts.graph_section gs on res.graph_section_id = gs.graph_section_id
        left join core.week_day wd on res.week_day_id = wd.week_day_id
    where
        (p_calculation_task_id is null or res.calculation_task_id = p_calculation_task_id or p_calculation_task_id = 0)
        and (p_tr_type_id is null or p_tr_type_id = 0 or res.tr_type_id = p_tr_type_id :: smallint)
        and (p_route_id is null or p_route_id = 0 or r.route_id = p_route_id)
        and (p_ct_gs_speed_result_id is null or p_ct_gs_speed_result_id = 0 or
             res.ct_gs_speed_result_id = p_ct_gs_speed_result_id :: bigint)
        and (p_hour is null or p_hour = 0 or EXTRACT(epoch from (res.hour)) = p_hour)
        and (p_week_day_id is null or p_week_day_id = 0 or res.week_day_id = p_week_day_id :: smallint);
end;
$$
language plpgsql;
------------------------------------------------------------------------------------------------------------------------------------------------
--
-- ct_stop_visit
--

create or replace function core.geosrv_pkg$ct_stop_visit(
    p_calculation_task_id integer,
    p_tr_type_id          integer,
    p_tr_capacity_id      integer,
    p_hour                integer,
    p_route_id            integer
)
    returns table(
        SP1_NAME        text,
        SP2_NAME        text,
        AVG_SPEED       smallint,
        NORM_DURATION   integer,
        ROUND_KIND_NAME text,
        GEOM            geometry,
        GEOM_COLOR      text,
        GEOM_LENGTH     double precision,
        MUID            bigint
    )
as $function$
begin

    return query
    select
        s1.name                                                                                as SP1_NAME,
        s2.name                                                                                as SP2_NAME,
        res.avg_speed                                                                          as AVG_SPEED,
        case when round(div(res.norm_duration, 60) :: numeric) > 0
            then round(div(res.norm_duration, 60) :: numeric) :: integer
        else 1 end                                                                             as NORM_DURATION,
        rr.code                                                                                as ROUND_KIND_NAME,
        core.geosrv_pkg$split_by_2_points(rr.geom, sl1.geom, sl2.geom)                         as GEOM,
        core.geosrv_pkg$get_color_by_speed(res.avg_speed)                                      as GEOM_COLOR,
        round(ST_Length(core.geosrv_pkg$split_by_2_points(rr.geom, sl1.geom, sl2.geom), true)) as GEOM_LENGTH,
        res.ct_stop_visit_result_id                                                            as MUID
    from asd.ct_stop_visit_result res
        join rts.round rr on res.round_id = rr.round_id
        join rts.stop_item2round si2r1 on res.stop_item2round_1_id = si2r1.stop_item2round_id
        join rts.stop_item si1 on si2r1.stop_item_id = si1.stop_item_id
        join rts.stop_location sl1 on si1.stop_location_id = sl1.stop_location_id
        join rts.stop s1 on sl1.stop_id = s1.stop_id

        join rts.stop_item2round si2r2 on res.stop_item2round_2_id = si2r2.stop_item2round_id
        join rts.stop_item si2 on si2r2.stop_item_id = si2.stop_item_id
        join rts.stop_location sl2 on si2.stop_location_id = sl2.stop_location_id
        join rts.stop s2 on sl2.stop_id = s2.stop_id
        left join rts.route_variant rv on rr.route_variant_id = rv.route_variant_id
        left join rts.route r on rv.route_variant_id = r.current_route_variant_id
    where
        (p_calculation_task_id = 0 or res.calculation_task_id = p_calculation_task_id) and
        (p_tr_type_id = 0 or res.tr_type_id = p_tr_type_id :: smallint) and
        (p_tr_capacity_id = 0 or res.tr_capacity_id = p_tr_capacity_id :: smallint) and
        (p_hour = 0 or EXTRACT(epoch from res.hour_from :: time) = p_hour) and
        (p_route_id = 0 or p_route_id is null or (rv.route_id = p_route_id));
end;
$function$
language plpgsql;

------------------------------------------------------------------------------------------------------------------------

--
-- Слой "Транспортные предприятия"
-- csptm:parks
--
create or replace function core.geosrv_pkg$parks()
    returns table(number     text,
                  name       text,
                  short_name text,
                  address    text,
                  phone      text,
                  geom       PUBLIC.geometry,
                  muid       bigint
    )
as $function$
begin
    return query
    select
        cd.tn_instance as number,
        e.name_full,
        e.name_short   as short_name,
        ' ' :: text,
        ' ' :: text,
        l.geom,
        cd.depo_id
    from core.entity e
        join core.depo cd on e.entity_id = cd.depo_id
        join rts.depo2zone d2z on cd.depo_id = d2z.depo_id
        join rts.zone_location l on d2z.zone_location_id = l.zone_location_id
    where cd.sign_deleted = 0;
end;
$function$
language plpgsql;
------------------------------------------------------------------------------------------------------------------------

--
-- Слой "Отстойно-разворотные площадки"
-- csptm:parkings
--

create or replace function core.geosrv_pkg$parkings(
    p_zone_location_id bigint
)
    returns table(
        geom             PUBLIC.geometry,
        zone_location_id bigint
    )
as $function$
begin
    return query
    select
        l.geom as geom,
        l.zone_location_id
    from rts.zone_location l
    where p_zone_location_id = 0 or p_zone_location_id is null or l.zone_location_id = p_zone_location_id;
end;
$function$
language plpgsql;
------------------------------------------------------------------------------------------------------------------------

--
-- ct_stop_visit_geom
--

create or replace function core.geosrv_pkg$ct_stop_visit_geom(
    p_ct_round_stop_result_id bigint
)
    returns table(GEOM       geometry,
                  GEOM_COLOR text,
                  MUID       bigint
    )
as $function$
begin
    return query
    select
        core.geosrv_pkg$split_by_2_points(rr.geom, sl1.geom, sl2.geom) as GEOM,
        core.geosrv_pkg$get_color_by_speed(res.avg_speed)              as GEOM_COLOR,
        res.ct_stop_visit_result_id                                    as MUID
    from asd.ct_round_stop_result crsr
        join asd.ct_stop_visit_result res on res.calculation_task_id = crsr.calculation_task_id
        join rts.round rr on res.round_id = rr.round_id
        join rts.stop_item2round si2r1 on res.stop_item2round_1_id = si2r1.stop_item2round_id
        join rts.stop_item si1 on si2r1.stop_item_id = si1.stop_item_id
        join rts.stop_location sl1 on si1.stop_location_id = sl1.stop_location_id

        join rts.stop_item2round si2r2 on res.stop_item2round_2_id = si2r2.stop_item2round_id
        join rts.stop_item si2 on si2r2.stop_item_id = si2.stop_item_id
        join rts.stop_location sl2 on si2.stop_location_id = sl2.stop_location_id
        left join rts.route_variant rv on rr.route_variant_id = rv.route_variant_id
        left join rts.route r on rv.route_variant_id = r.current_route_variant_id
    where
        crsr.ct_round_stop_result_id = p_ct_round_stop_result_id and
        (res.tr_type_id :: smallint = crsr.tr_type_id :: smallint) and
        (res.tr_capacity_id = crsr.tr_capacity_id :: smallint) and
        (EXTRACT(epoch from res.hour_from :: time) = EXTRACT(epoch from crsr.hour_from :: time));
end;
$function$
language plpgsql;

------------------------------------------------------------------------------------------------------------------------

--
-- stop_places
-- todo Переделать на rts когда будут заменены все все функции вызываемые StopPlaceOnMapCtrl и добавлены поля: region district street
create or replace function core.geosrv_pkg$stop_places(
    p_muid bigint
)
    returns table(
        STOP_NAME        text,
        DIRECTION_NAME   text,
        TR_TYPE          text,
        BUILDING_ADDRESS text,
        REGION           text,
        DISTRICT         text,
        STREET           text,
        MUID             bigint,
        GEOM             geometry
    )
as $function$
begin
    return query
    select
        ss.name                                                                as STOP_NAME,
        rmd.name                                                               as DIRECTION_NAME,
        SUBSTRING(case when sp.has_bus = 1
            then ', A'
                  else '' end || case when sp.has_trolley = 1
            then ', Тб'
                                 else '' end || case when sp.has_tram = 1
            then ', Тр'
                                                else '' end || case when sp.has_speedtram = 1
            then ', СТр'
                                                               else '' end, 3) as TR_TYPE,
        sp.building_address                                                    as BUILDING_ADDRESS,
        ss.region                                                              as REGION,
        ss.district                                                            as DISTRICT,
        ss.street                                                              as STREET,
        sp.muid                                                                as MUID,
        sp.geom                                                                as GEOM
    from gis.stop_places sp
        join gis.stops ss on sp.stop_muid = ss.muid
        join gis.ref_movement_directions rmd on ss.direction_muid = rmd.muid
    where p_muid = 0 or p_muid is null or sp.muid = p_muid;
end;
$function$
language plpgsql;

------------------------------------------------------------------------------------------------------------------------

--
-- real_round
--

create or replace function core.geosrv_pkg$fact_movement(
    p_tr_id    core.tr.tr_id%type,
    p_interval integer
)
    returns table(
        geom  geometry,
        tr_id bigint
    )
as $function$
begin
    return query
    select
        st_makeline(ST_MakePoint(t.lon, t.lat)) geom,
        p_tr_id                                 tr_id
    from core.traffic t
    where
        (p_tr_id is null or p_tr_id = 0 or t.tr_id = p_tr_id)
        and t.event_time between now() - p_interval * '1 sec' :: interval and now()
        and t.location_valid = true
    group by t.tr_id;
end;
$function$
language plpgsql;
------------------------------------------------------------------------------------------------------------------------

--
-- stop_place_route_trj
--

create or replace function core.geosrv_pkg$stop_place_route_trj(
    p_round_id integer []
)
    returns table(
        round_id         integer,
        stop_location_id integer,
        sname            text,
        building_address text,
        geom             geometry
    )
as $function$
begin
    return query
    select
        si2r.round_id,
        si.stop_item_id stop_places_muid,
        rs.name         sname,
        sl.building_address,
        sl.geom
    from rts.stop_item2round si2r
        join rts.stop_item si on si2r.stop_item_id = si.stop_item_id
        join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
        join rts.stop rs on sl.stop_id = rs.stop_id
    where si2r.round_id = any (p_round_id) or p_round_id isnull or cardinality(p_round_id)=0 or p_round_id = array [0];
end;
$function$
language plpgsql;

------------------------------------------------------------------------------------------------------------------------


--
-- rnis_speed
--
create or replace function core.geosrv_pkg$rnis_speed()
    returns table(geometries       geometry,
                  line_number      bigint,
                  from_node_number bigint,
                  to_node_number   bigint,
                  name             text,
                  bus_lanes        bigint,
                  type             bigint,
                  level            bigint,
                  lanes            bigint,
                  speed_limit      bigint,
                  situation_time   timestamp,
                  node_number      bigint,
                  situation_code   bigint
    )
as $function$
begin
    return query
    select
        g.geometries,
        g.line_number,
        g.from_node_number,
        g.to_node_number,
        g.name,
        g.bus_lanes,
        g.type,
        g.level,
        g.lanes,
        g.speed_limit,
        rjc.situation_time,
        rjc.node_number,
        rjc.situation_code
    from rnis.graph g
        join rnis.radio_jam_color rjc on g.line_number = rjc.line_number and g.from_node_number = rjc.node_number;
end;
$function$
language plpgsql;

------------------------------------------------------------------------------------------------------------------------
--
--speed color

--drop function core.geosrv_pkg$get_color_by_speed(p_speed real );


create or replace function core.geosrv_pkg$get_color_by_speed(p_speed real)
    returns text
as $function$ declare
    l_result text;
begin

    if p_speed < 16
    then
        l_result := '#FF3300';
    elsif p_speed >= 16 and p_speed < 36
        then
            l_result := '#E6E600';
    else
        l_result := '#33CC33';
    end if;

    return l_result;
end;
$function$
language plpgsql;

--
-- split_by_2_points
drop function core.geosrv_pkg$split_by_2_points( public .geometry, public .geometry, public .geometry );
create or replace function core.geosrv_pkg$split_by_2_points(p_geom        PUBLIC.geometry,
                                                             p_point_start PUBLIC.geometry,
                                                             p_point_end   PUBLIC.geometry)
    returns PUBLIC.geometry
as $function$ declare
begin
    return case when ST_LineLocatePoint(p_geom, p_point_start) < ST_LineLocatePoint(p_geom, p_point_end)
        then
            ST_Line_Substring(
                p_geom,
                ST_LineLocatePoint(p_geom, p_point_start),
                ST_LineLocatePoint(p_geom, p_point_end)
            )
           else
               ST_Line_Substring(
                   p_geom,
                   ST_LineLocatePoint(p_geom, p_point_end),
                   ST_LineLocatePoint(p_geom, p_point_start)) end;
end;
$function$
language plpgsql;

------------------------------------------------------------------------------------------------------------------------

create or replace function core.geosrv_pkg$ibrd_view(p_info_board_id integer)
    returns table(
        info_board_id        text,
        code                 integer,
        stop_name            text,
        connection_state     boolean,
        connection_state_str text,
        geom                 geometry
    )
as $function$ declare
begin
    return query
    select distinct
        ib.info_board_id :: text,
        ib.code :: integer as                         code,
        s.name                                     stop_name,
        ibrd.event_pkg$is_connected(ib.info_board_id) connection_state,
        case when ibrd.event_pkg$is_connected(ib.info_board_id)
            then 'Подключено'
        else 'Отключено' end                          connection_state_str,
        sl.geom
    from ibrd.info_board ib
        left join ibrd.stop_location2info_board sl2ib on ib.info_board_id = sl2ib.info_board_id and sl2ib.is_base
        left join rts.stop_location sl on sl2ib.stop_location_id = sl.stop_location_id
        left join rts.stop s on sl.stop_id = s.stop_id
    where (p_info_board_id = 0 or ib.info_board_id = p_info_board_id);
end;
$function$
language plpgsql;

------------------------------------------------------------------------------------------------------------------------
