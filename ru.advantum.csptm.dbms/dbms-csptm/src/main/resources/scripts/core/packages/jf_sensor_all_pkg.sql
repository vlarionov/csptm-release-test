﻿-- Function: core."jf_sensor_all_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_sensor_all_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_sensor_all_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_rf_db_method text:=jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
  l_depo_id	 core.depo.depo_id%type 		:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', true), jofl.jofl_pkg$extract_number(p_attr, 'depo_id', true));
  l_equipment_type_id core.equipment_type.equipment_type_id%type		:= jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_type_id', true);
begin

  open p_rows for
  with sens as(
      select s.sensor_id,   'sensor' as sensor_type from core.sensor s union all
      select hdd.hdd_id,    'hdd' from core.hdd hdd union all
      select mn.monitor_id, 'monitor' from core.monitor mn union all
      select md.modem_id,   'modem' from core.modem md
    )
    ,sens2tr as (
    select t.sensor_id from core.sensor2tr t union all
    select t.equipment_id from core.equipment2tr t
    )

  select
    et.equipment_id,
    et.firmware_id,
    et.firmware_name,
    et.unit_service_type_id,
    et.unit_service_type_name,
    et.facility_id,
    et.facility_name,
    et.equipment_status_id,
    et.equipment_status_name,
    et.decommission_reason_id,
    et.decommission_reason_name,
    et.dt_begin,
    et.dt_end,
    et.equipment_model_id,
    et.equipment_model_name,
    et.equipment_type_name,
    et.depo_id,
    et.depo_name_full,
    et.depo_name_short,
    et.territory_id,
    et.unit_service_facility_id,
    et.has_diagnostic,
    et.comment,
    et.serial_num,
    sens.sensor_type
  from sens
    join core.v_equipment et on et.equipment_id= sens.sensor_id
  where
    l_rf_db_method = 'core.tr' and
    et.equipment_type_id= l_equipment_type_id and
    et.depo_id = l_depo_id and
    et.equipment_status_id != core.jf_equipment_status_pkg$cn_status_decommissioned() and
    not exists (select * from sens2tr where sens2tr.sensor_id = et.equipment_id)
  ;


end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_sensor_all_pkg$of_rows"(numeric, text)
  OWNER TO adv;

