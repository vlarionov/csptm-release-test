﻿-- Function: core."jf_equipment_aux_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION core."jf_equipment_aux_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_equipment_aux_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.equipment_aux AS
$BODY$ 
declare 
   l_r core.equipment_aux%rowtype; 
begin 
   l_r.auxiliary_id := jofl.jofl_pkg$extract_varchar(p_attr, 'auxiliary_id', true); 
   l_r.equipment_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true); 

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_aux_pkg$attr_to_rowtype"(text)
  OWNER TO adv;


-- Function: core."jf_equipment_aux_pkg$of_delete"(numeric, text)

-- DROP FUNCTION core."jf_equipment_aux_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_equipment_aux_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.equipment_aux%rowtype;
begin 
   l_r := core.jf_equipment_aux_pkg$attr_to_rowtype(p_attr);

   delete from  core.equipment_aux where  auxiliary_id = l_r.auxiliary_id and equipment_id = l_r.equipment_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_aux_pkg$of_delete"(numeric, text)
  OWNER TO adv;

  -- Function: core."jf_equipment_aux_pkg$of_insert"(numeric, text)

-- DROP FUNCTION core."jf_equipment_aux_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_equipment_aux_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.equipment_aux%rowtype;
begin 
   l_r := core.jf_equipment_aux_pkg$attr_to_rowtype(p_attr);

   insert into core.equipment_aux select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_aux_pkg$of_insert"(numeric, text)
  OWNER TO adv;


-- Function: core."jf_equipment_aux_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_equipment_aux_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_equipment_aux_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_r core.equipment_aux%rowtype;
begin
  l_r := core.jf_equipment_aux_pkg$attr_to_rowtype(p_attr);

 open p_rows for
     select
       eq_aux.auxiliary_id,
       eq_aux.equipment_id,
       aux.auxiliary_name
     from core.equipment_aux eq_aux
       join core.auxiliary aux on aux.auxiliary_id =eq_aux.auxiliary_id
     where eq_aux.equipment_id = l_r.equipment_id;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_aux_pkg$of_rows"(numeric, text)
  OWNER TO adv;
