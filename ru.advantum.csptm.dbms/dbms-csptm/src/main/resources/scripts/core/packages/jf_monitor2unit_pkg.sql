﻿-- Function: core."jf_monitor2unit_pkg$attr_to_rowtype"(text)

--DROP FUNCTION core."jf_monitor2unit_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION core."jf_monitor2unit_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.monitor2unit AS
$BODY$ 
declare 
   l_r core.monitor2unit%rowtype;
begin 
   l_r.monitor_id := jofl.jofl_pkg$extract_number(p_attr, 'monitor_id', true);
   l_r.unit_id := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true),jofl.jofl_pkg$extract_number(p_attr, 'unit_id', true));

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_monitor2unit_pkg$attr_to_rowtype"(text)
  OWNER TO adv;

--********************************************************************************************************
-- Function: core."jf_monitor2unit_pkg$of_rows"(numeric, text)

 --DROP FUNCTION core."jf_monitor2unit_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_monitor2unit_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_r core.monitor2unit%rowtype;
begin
  l_r.unit_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);

 open p_rows for
       select
         m2u.monitor_id,
         m2u.unit_id,
         et.equipment_id,
         et.serial_num,
         et.firmware_id,
         fw.name        as firmware_name,
         et.unit_service_type_id,
         ust.name       as unit_service_type_name,
         et.facility_id,
         ent.name_short as facility_name,
         et.equipment_status_id,
         ets.name       as equipment_status_name,
         etd.decommission_reason_id,
         dr.name        as decommission_reason_name,
         et.dt_begin,
         etd.dt_end,
         et.equipment_model_id,
         em.name as equipment_model_name,
         ett.name_full as equipment_type_name,
         entity4depo.name_full depo_name_full,
         entity4depo.name_short depo_name_short
       from core.monitor2unit m2u
         join core.monitor m on m.monitor_id = m2u.monitor_id
         join core.equipment et on et.equipment_id = m2u.monitor_id
         join core.equipment_status ets on ets.equipment_status_id = et.equipment_status_id
         join core.unit_service_type ust on ust.unit_service_type_id = et.unit_service_type_id
         left join core.firmware fw on fw.firmware_id = et.firmware_id
         join core.entity ent on ent.entity_id = et.facility_id
         join core.equipment_model em on em.equipment_model_id = et.equipment_model_id
         join core.equipment_type ett on ett.equipment_type_id = em.equipment_type_id
         join core.entity entity4depo on entity4depo.entity_id = et.depo_id
         left join core.equipment_decommission etd on et.equipment_id = etd.equipment_id
         left join core.decommission_reason dr on dr.decommission_reason_id = etd.decommission_reason_id
        where m2u.unit_id = l_r.unit_id;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_monitor2unit_pkg$of_rows"(numeric, text)
  OWNER TO adv;

--********************************************************************************************************
-- Function: core."jf_monitor2unit_pkg$of_delete"(numeric, text)

 --DROP FUNCTION core."jf_monitor2unit_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_monitor2unit_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.monitor2unit%rowtype;
begin 
   l_r := core.jf_monitor2unit_pkg$attr_to_rowtype(p_attr);

   delete from  core.monitor2unit where  monitor_id = l_r.monitor_id and unit_id = l_r.unit_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_monitor2unit_pkg$of_delete"(numeric, text)
  OWNER TO adv;

--********************************************************************************************************
-- Function: core."jf_monitor2unit_pkg$of_insert"(numeric, text)

 DROP FUNCTION core."jf_monitor2unit_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_monitor2unit_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r core.monitor2unit%rowtype;
begin 
   l_r := core.jf_monitor2unit_pkg$attr_to_rowtype(p_attr);
   l_r.sys_period := tstzrange(now(), NULL::timestamp with time zone);

   insert into core.monitor2unit select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_monitor2unit_pkg$of_insert"(numeric, text)
  OWNER TO adv;

--********************************************************************************************************

--********************************************************************************************************
