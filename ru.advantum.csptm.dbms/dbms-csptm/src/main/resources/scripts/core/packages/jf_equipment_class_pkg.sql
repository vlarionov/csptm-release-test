﻿-- Function: core."jf_equipment_class_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION core."jf_equipment_class_pkg$attr_to_rowtype"(text);

/*CREATE OR REPLACE FUNCTION core."jf_equipment_class_pkg$attr_to_rowtype"(p_attr text)
  RETURNS core.equipment_class AS
$BODY$ 
declare 
   l_r core.equipment_class%rowtype; 
begin 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 
   l_r.equipment_class_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_class_id', true); 

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_class_pkg$attr_to_rowtype"(text)
  OWNER TO adv;*/
--********************************************************************************************

-- Function: core."jf_equipment_class_pkg$of_rows"(numeric, text)

-- DROP FUNCTION core."jf_equipment_class_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION core."jf_equipment_class_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
begin 
 open p_rows for 
      select 
        name, 
        equipment_class_id
      from core.equipment_class; 
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION core."jf_equipment_class_pkg$of_rows"(numeric, text)
  OWNER TO adv;
--***************************************************************************

--DROP FUNCTION core."jf_equipment_class_pkg$cn_sensor"();

CREATE OR REPLACE FUNCTION core.jf_equipment_class_pkg$cn_sensor()
  RETURNS core.equipment_class.equipment_class_id%type AS
'select 1::smallint;'
LANGUAGE SQL IMMUTABLE;

--DROP FUNCTION core."jf_equipment_class_pkg$cn_block"()

CREATE OR REPLACE FUNCTION core.jf_equipment_class_pkg$cn_block()
  RETURNS core.equipment_class.equipment_class_id%type AS
'select 0::smallint;'
LANGUAGE SQL IMMUTABLE;

--DROP FUNCTION core."jf_equipment_class_pkg$cn_other"()

CREATE OR REPLACE FUNCTION core.jf_equipment_class_pkg$cn_other()
  RETURNS core.equipment_class.equipment_class_id%type AS
'select 2::smallint;'
LANGUAGE SQL IMMUTABLE;