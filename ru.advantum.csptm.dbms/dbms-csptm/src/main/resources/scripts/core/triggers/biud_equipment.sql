do $$
begin
  if not exists(select * from pg_trigger where tgname = 'biud_equipment') THEN

    CREATE TRIGGER biud_equipment
    after insert or update or delete on core.equipment
    for each row
    execute PROCEDURE core.trg_equipment();

  end if;
end$$;
