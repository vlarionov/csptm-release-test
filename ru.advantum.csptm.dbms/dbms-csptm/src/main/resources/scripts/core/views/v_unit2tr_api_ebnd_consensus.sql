--drop view core.v_unit2tr_api_ebnd_consensus;

create or replace view core.v_unit2tr_api_ebnd_consensus
as
select a.unit_num api_unit_num, a.garagenum garage_num, tr.tr_id, unit.unit_id, unit2tr.unit_id as unit_ebnd, a.tn_instance tn_id
from core.v_unit2tr_api_asdu a
  left join core.tr on a.garagenum::int = tr.garage_num
  left join core.depo on a.tn_instance = depo.tn_instance
  left join core.unit on a.unit_num = unit.unit_num
  left JOIN core.equipment on unit.unit_id = equipment.equipment_id
  left join core.depo depo_unit on equipment.depo_id = depo_unit.depo_id and a.tn_instance = depo_unit.tn_instance
  left join core.unit2tr on unit.unit_id = unit2tr.unit_id

where a.is_deleted = 0;


comment on view core.v_unit2tr_api_ebnd_consensus is 'Соответствие данных АПИ АСДУ и ЕБНД в части установки блоков на ТС';
comment on column core.v_unit2tr_api_ebnd_consensus.api_unit_num is 'Уникальный номер блока';
comment on column core.v_unit2tr_api_ebnd_consensus.garage_num is 'Гаражный номер';
comment on column core.v_unit2tr_api_ebnd_consensus.tr_id is 'Идентификатор ТС';
comment on column core.v_unit2tr_api_ebnd_consensus.unit_id is 'Идентификатор блока';
comment on column core.v_unit2tr_api_ebnd_consensus.unit_ebnd is 'Идентификатор блока';
comment on column core.v_unit2tr_api_ebnd_consensus.tn_id is 'Номер сервера';