﻿--DROP VIEW core.v_equipment2tr4all;

create or replace view core.v_equipment2tr4all as
  with adata as (
    select unit2tr.tr_id, 1 as id_type, et.*
    from core.unit u
      join core.unit2tr on u.unit_id = unit2tr.unit_id
      join core.v_equipment et on et.equipment_id = u.unit_id
    union all
    select s2tr.tr_id, 2 as id_type, et.*
    from  core.sensor2tr s2tr
      join core.sensor s on s.sensor_id = s2tr.sensor_id
      join core.v_equipment et on et.equipment_id = s2tr.sensor_id
    union all
    select et2tr.tr_id, 3 as id_type, et.*
    from
      core.equipment2tr et2tr
      join core.v_equipment et on et.equipment_id = et2tr.equipment_id
  )
  select
    et.tr_id,
    et.equipment_id,
    et.serial_num,
    et.firmware_id,
    et.firmware_name,
    et.unit_service_type_id,
    et.unit_service_type_name,
    et.facility_id,
    et.facility_name,
    et.equipment_status_id,
    et.equipment_status_name,
    et.dt_begin,
    et.dt_end,
    et.equipment_model_id,
    et.equipment_model_name,
    et.equipment_type_name,
    et.equipment_type_id,
    et.depo_name_full,
    et.depo_name_short,
    et.decommission_reason_id,
    et.decommission_reason_name,
    sr.usw_num,
    (select smc.phone_num
     from core.sim_card smc
       join core.sim_card2unit smu on smu.sim_card_id = smc.sim_card_id
     where smu.equipment_id = et.equipment_id
     limit 1) as phone_num
  from adata et
    left join core.unit_bnsr sr on sr.unit_bnsr_id = et.equipment_id;

COMMENT ON VIEW core.v_equipment2tr4all IS 'Представление связь оборудования с ТС';
COMMENT ON COLUMN core.v_equipment2tr4all.firmware_name IS 'Прошивка БО';
COMMENT ON COLUMN core.v_equipment2tr4all.unit_service_type_name IS 'Вид тех. обслуживания';
COMMENT ON COLUMN core.v_equipment2tr4all.facility_name IS 'Производитель';
COMMENT ON COLUMN core.v_equipment2tr4all.equipment_status_name IS 'Статус';
COMMENT ON COLUMN core.v_equipment2tr4all.decommission_reason_name IS 'Причина списания';
COMMENT ON COLUMN core.v_equipment2tr4all.dt_begin IS 'Дата создания';
COMMENT ON COLUMN core.v_equipment2tr4all.dt_end IS 'Дата списания';
COMMENT ON COLUMN core.v_equipment2tr4all.serial_num IS 'Серийный номер';
COMMENT ON COLUMN core.v_equipment2tr4all.equipment_model_name IS 'Модель';
COMMENT ON COLUMN core.v_equipment2tr4all.equipment_type_name IS 'Тип';
COMMENT ON COLUMN core.v_equipment2tr4all.usw_num IS 'Номер радиостанции';
COMMENT ON COLUMN core.v_equipment2tr4all.phone_num IS 'Номер телефона';


COMMENT ON COLUMN core.v_equipment2tr4all.equipment_id IS 'Идентификатор Оборудования';
COMMENT ON COLUMN core.v_equipment2tr4all.firmware_id IS 'Прошивка БО';
COMMENT ON COLUMN core.v_equipment2tr4all.unit_service_type_id IS 'Вид тех. обслуживания';
COMMENT ON COLUMN core.v_equipment2tr4all.facility_id IS 'Производитель';
COMMENT ON COLUMN core.v_equipment2tr4all.equipment_status_id IS 'Статус';
COMMENT ON COLUMN core.v_equipment2tr4all.equipment_model_id IS 'Модель БО';
COMMENT ON COLUMN core.v_equipment2tr4all.tr_id IS 'Транспортное средство';
COMMENT ON COLUMN core.v_equipment2tr4all.equipment_type_id IS 'Тип БО (для организации уникального индекса)';
COMMENT ON COLUMN core.v_equipment2tr4all.decommission_reason_id IS 'Списание';
COMMENT ON COLUMN core.v_equipment2tr4all.depo_name_full IS 'Транспортное предприятие(Наименование полное)';
COMMENT ON COLUMN core.v_equipment2tr4all.depo_name_short IS 'Транспортное предприятие(Наименование кратко)';
