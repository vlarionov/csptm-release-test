create or replace view core.v_role2account
as
  with a as  (select ref_role.id_role, role_name, id_jofl_account, id_account_local from jofl.account, jofl.ref_role)
  select a.id_role, a.role_name, a.id_jofl_account, a.id_account_local, COALESCE(role2account.id_jofl_account, 0) > 0 is_include
  from a LEFT JOIN jofl.role2account on ( a.id_role = role2account.id_role and a.id_jofl_account = role2account.id_jofl_account)
    LEFT OUTER JOIN  core.account on account.account_id = a.id_account_local ;

comment on view core.v_role2account is 'Атрибуты рейса';
comment on column core.v_role2account.id_role is 'Идентификатор роли';
comment on column core.v_role2account.role_name is 'Наименование роли';
comment on column core.v_role2account.id_jofl_account is 'Идентификатор учетной записи JOFL';
comment on column core.v_role2account.id_account_local is 'Идентификатор учетной записи ЕБНД';
comment on column core.v_role2account.is_include is 'Признак того, что учетная запись имеет роль';