--DROP VIEW core.v_tr_ref;

CREATE OR REPLACE VIEW core.v_tr_ref AS
SELECT
  tr.tr_id as tr_id
  ,tr.licence as licence
  ,tr.garage_num
  ,tr_type.name as tr_type_name
  ,tr_mark.name as tr_mark_name
  ,tr_model.name as tr_model_name
  ,tr_capacity.full_name as tr_capacity_full_name
  ,tr_capacity.short_name as tr_capacity_short_name
from core.tr
  left join core.tr_type on tr.tr_type_id = tr_type.tr_type_id
  LEFT JOIN core.tr_mark on tr.tr_type_id = tr_mark.tr_mark_id
  left join core.tr_model on tr.tr_model_id = tr_model.tr_model_id
  LEFT JOIN core.tr_capacity on tr_model.tr_capacity_id = tr_capacity.tr_capacity_id
  ;

comment on view core.v_tr_ref is 'Атрибуты ТС';
comment on column core.v_tr_ref.tr_id is 'Идентификатор ТС';
comment on column core.v_tr_ref.licence is 'Гос. номер';
comment on column core.v_tr_ref.garage_num is 'Гаражный номер';
comment on column core.v_tr_ref.tr_type_name is 'Тип ТС';
comment on column core.v_tr_ref.tr_mark_name is 'Марка ТС';
comment on column core.v_tr_ref.tr_model_name is 'Модель ТС';
comment on column core.v_tr_ref.tr_capacity_full_name is 'Вместимость ТС';
comment on column core.v_tr_ref.tr_capacity_short_name is 'Вместимость ТС короткое';
