﻿--drop view core.v_equipment;

create or replace view core.v_equipment  as
  with decom as (
      select d.decommission_reason_id,
        d.equipment_id,
        d.dt_end,
        dr.name  as decommission_reason_name

      from core.equipment_decommission d
        join core.decommission_reason dr on dr.decommission_reason_id = d.decommission_reason_id
      where not d.is_deleted
  )
  select
    case  et.equipment_status_id when core.jf_equipment_status_pkg$cn_status_decommissioned()  then 'P{A},D{OF_DECOMMISSION_CANCEL,OF_ENABLE_DIAGNOSTIC,OF_DISABLE_DIAGNOSTIC}'  else 'P{A},D{OF_UPDATE,OF_DECOMMISSION,OF_LINK2UNIT,OF_LINK2TR,OF_CHANGE_STATUS,OF_ENABLE_DIAGNOSTIC,OF_DISABLE_DIAGNOSTIC}' end  "ROW$POLICY",
    et.equipment_id,
    et.firmware_id,
    et.serial_num,
    fw.name        as firmware_name,
    et.unit_service_type_id,
    ust.name       as unit_service_type_name,
    et.facility_id,
    me.name_short /*ent.name_short*/ as facility_name,
    et.equipment_status_id,
    ets.name       as equipment_status_name,
    decom.decommission_reason_id,
    decom.decommission_reason_name,
    et.dt_begin,
    decom.dt_end,
    et.equipment_model_id,
    em.name as equipment_model_name,
    ett.equipment_type_id,
    ett.name_short as equipment_type_name,
    entity4depo.name_full depo_name_full,
    entity4depo.name_short depo_name_short,
    et.depo_id,
    et.territory_id,
    entity4territory.name_full territory_name_full,
    entity4territory.name_short territory_name_short,
    et.unit_service_facility_id,
    ent_usfacility.name_short as unit_service_facility_name_short,
    et.has_diagnostic,
    et.comment
  from
    core.equipment et
    join core.equipment_status ets on ets.equipment_status_id = et.equipment_status_id
    join core.unit_service_type ust on ust.unit_service_type_id = et.unit_service_type_id
    left join core.firmware fw on fw.firmware_id = et.firmware_id
--     join core.entity ent on ent.entity_id = et.facility_id
    join core.equipment_model em on em.equipment_model_id = et.equipment_model_id
      left join core.entity me on me.entity_id=em.facility_id
    join core.equipment_type ett on ett.equipment_type_id = em.equipment_type_id
    join core.entity entity4depo on entity4depo.entity_id = et.depo_id
    left join core.entity entity4territory on entity4territory.entity_id = et.territory_id
    left join core.entity ent_usfacility ON ent_usfacility.entity_id = et.unit_service_facility_id
    left join decom on decom.equipment_id = et.equipment_id
;


COMMENT ON VIEW core.v_equipment IS 'Представление оборудование';
COMMENT ON COLUMN core.v_equipment.equipment_id IS 'Идентификатор Оборудования';
COMMENT ON COLUMN core.v_equipment.firmware_id IS 'Прошивка БО';
COMMENT ON COLUMN core.v_equipment.unit_service_type_id IS 'Вид тех. обслуживания';
COMMENT ON COLUMN core.v_equipment.facility_id IS 'Производитель';
COMMENT ON COLUMN core.v_equipment.equipment_status_id IS 'Статус';
COMMENT ON COLUMN core.v_equipment.dt_begin IS 'Дата создания';
COMMENT ON COLUMN core.v_equipment.serial_num IS 'Серийный номер';
COMMENT ON COLUMN core.v_equipment.equipment_model_id IS 'Модель БО';
COMMENT ON COLUMN core.v_equipment.depo_id IS 'Транспортное предприятие';
COMMENT ON COLUMN core.v_equipment.territory_id IS 'Территория транспортного предприятия';
COMMENT ON COLUMN core.v_equipment.unit_service_facility_id IS 'Обслуживающая организация';
COMMENT ON COLUMN core.v_equipment.has_diagnostic IS 'Диагностика включена';
COMMENT ON COLUMN core.v_equipment.comment IS 'Комментарий';
COMMENT ON COLUMN core.v_equipment.equipment_type_id IS 'Тип БО (для организации уникального индекса)';


COMMENT ON COLUMN core.v_equipment."ROW$POLICY" IS 'Ограничения на строки';
COMMENT ON COLUMN core.v_equipment.firmware_name IS 'Прошивка БО (Наименование)';
COMMENT ON COLUMN core.v_equipment.unit_service_type_name IS 'Вид тех. обслуживания (Наименование)';
COMMENT ON COLUMN core.v_equipment.facility_name  IS 'Производитель (Наименование)';
COMMENT ON COLUMN core.v_equipment.equipment_status_name IS 'Статус (Наименование)';
COMMENT ON COLUMN core.v_equipment.decommission_reason_id IS 'Списание';
COMMENT ON COLUMN core.v_equipment.decommission_reason_name IS 'Причина списания';
COMMENT ON COLUMN core.v_equipment.dt_end IS 'Дата списания';
COMMENT ON COLUMN core.v_equipment.equipment_model_name IS 'Модель БО (Наименование)';
COMMENT ON COLUMN core.v_equipment.equipment_type_name IS 'Тип БО (Наименование)';
COMMENT ON COLUMN core.v_equipment.depo_name_full IS 'Транспортное предприятие(Наименование полное)';
COMMENT ON COLUMN core.v_equipment.depo_name_short IS 'Транспортное предприятие(Наименование кратко)';
COMMENT ON COLUMN core.v_equipment.territory_name_full  IS 'Территория транспортного предприятия(Наименование полное)';
COMMENT ON COLUMN core.v_equipment.territory_name_short  IS 'Территория транспортного предприятия(Наименование кратко)';
COMMENT ON COLUMN core.v_equipment.unit_service_facility_name_short IS 'Обслуживающая организация(Наименование кратко)';