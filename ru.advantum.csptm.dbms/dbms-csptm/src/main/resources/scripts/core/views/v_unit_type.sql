﻿create or replace view core.v_unit_type as
select unit_kbtob_id  as unit_id, 'КБТОБ' AS name, 'P{D},D{core.sensor2unit}, P{D},D{core.hdd2unit}, P{D},D{core.hdd2unit}, P{D},D{core.monitor2unit}, P{D},D{core.modem2unit}' as  ROW$POLICY from core.unit_kbtob
union all
select unit_bnsr_id as unit_id, 'БНСР','P{D},D{core.sensor2unit}' from core.unit_bnsr
union all
select unit_bnst_id as unit_id, 'БНСТ', 'P{D},D{core.sensor2unit}' from core.unit_bnst
union all
select eqp.equipment_id as unit_id, eqt.name_short as name, 'P{D},D{core.sensor2unit}'
  from core.equipment eqp
  join core.equipment_type eqt on eqt.equipment_type_id = eqp.equipment_type_id
 where eqp.equipment_type_id in (select distinct equipment_type_id
                                   from core.tr_schema_install_unit
                                  where equipment_type_id not in (core.jf_equipment_type_pkg$cn_type_bnst(),
                                                                  core.jf_equipment_type_pkg$cn_type_bnsr(),
                                                                  core.jf_equipment_type_pkg$cn_type_kbtob()));

comment on view core.v_unit_type is 'Блоки';
comment on column core.v_unit_type.unit_id is 'ID блока';
comment on column core.v_unit_type.name is 'Тип блока';
comment on column core.v_unit_type.ROW$POLICY is 'Права';
