--drop VIEW core.v_breakdown;

CREATE OR REPLACE VIEW core.v_breakdown
AS
  SELECT
    breakdown.breakdown_id,
    breakdown.dt_begin,
    breakdown.num,
    breakdown.description,
    breakdown.is_confirmed,
    breakdown.dt_end,
    breakdown.breakdown_type_id,
    breakdown.breakdown_status_id,
    breakdown.close_result_id,
    breakdown.territory_id,
    breakdown.equipment_id,
    breakdown.account_id,
    account.first_name || ' ' || account.middle_name || ' ' || account.last_name account_name,
    breakdown.tr_id,
    tr.garage_num || ' ( ' || tr.licence || ' ) '                                tr_name,
    territory.code territory_name,
    bd_close_result.name close_result_name,
    breakdown_status.name breakdown_status_name,
    breakdown_type.name breakdown_type_name,
    equipment.serial_num equipment_serial_num,
    equipment_model.name equipment_name
  FROM core.breakdown
    JOIN core.account ON account.account_id = breakdown.account_id
    JOIN core.tr ON tr.tr_id = breakdown.tr_id
    LEFT JOIN core.equipment on equipment.equipment_id = breakdown.equipment_id
    LEFT JOIN core.equipment_model on equipment_model.equipment_model_id = equipment.equipment_model_id
    LEFT JOIN core.territory on territory.territory_id = breakdown.territory_id
    LEFT JOIN core.bd_close_result on bd_close_result.bd_close_result_id = breakdown.close_result_id
    JOIN core.breakdown_status ON breakdown_status.breakdown_status_id = breakdown.breakdown_status_id
    LEFT JOIN core.breakdown_type on breakdown_type.breakdown_type_id = breakdown.breakdown_type_id
;


comment ON VIEW core.v_breakdown is 'Запись о поломке БО';
comment on column core.v_breakdown.breakdown_id is 'Запись о поломке БО';
comment on column core.v_breakdown.dt_begin is 'Дата создания';
comment on column core.v_breakdown.description is 'Описание поломки БО';
comment on column core.v_breakdown.is_confirmed is 'Поломка подтверждена';
comment on column core.v_breakdown.dt_end is 'Дата закрытия';
comment on column core.v_breakdown.tr_id is 'Транспортное средство';
comment on column core.v_breakdown.breakdown_type_id is 'Вид поломки БО';
comment on column core.v_breakdown.breakdown_status_id is 'Статус записи о поломке БО';
comment on column core.v_breakdown.close_result_id is 'Результат закрытия';
comment on column core.v_breakdown.territory_id is 'Территория ТП';
comment on column core.v_breakdown.equipment_id is 'Поломанное оборудование';
comment on column core.v_breakdown.account_id is 'Пользователь';
comment on column core.v_breakdown.num is 'номер записи';

comment on column core.v_breakdown.account_name is 'Создал';
comment on column core.v_breakdown.tr_name is 'ТС';
comment on column core.v_breakdown.territory_name is 'Территория';
comment on column core.v_breakdown.close_result_name is 'Результат закрытия';
comment on column core.v_breakdown.breakdown_status_name is 'Статус';
comment on column core.v_breakdown.breakdown_type_name is 'Тип поломки';
comment on column core.v_breakdown.equipment_serial_num is 'Серийный номер';
comment on column core.v_breakdown.equipment_name is 'Наименование оборудования';
