--DROP FOREIGN TABLE core.v_unit2tr_api_asdu cascade;

CREATE FOREIGN TABLE IF NOT EXISTS core.v_unit2tr_api_asdu(
  unit_num text,
  is_deleted int,
  tn_instance text,
  garagenum text
)
    SERVER api_asdu_v2
    OPTIONS (schema_name 'tf', table_name 'v_unit2tr_ebnd');

ALTER FOREIGN TABLE core.v_unit2tr_api_asdu
    OWNER TO adv;

comment on foreign table core.v_unit2tr_api_asdu is 'Блоки, установленные на ТС по данным АПИ АСДУ';
comment on column core.v_unit2tr_api_asdu.unit_num is 'Уникальный номер блока';
comment on column core.v_unit2tr_api_asdu.is_deleted is 'Признак удаления';
comment on column core.v_unit2tr_api_asdu.tn_instance is 'Номер сервера';
comment on column core.v_unit2tr_api_asdu.garagenum is 'Гаражный номер';