﻿--drop view core.v_hub2unit;

create or replace view core.v_hub2unit as
select h.hub_id          "hubId",
       u.unit_num        "unitNum",
       u.unit_id         "unitId"
  from core.hub h
  join core.unit u on h.hub_id = u.hub_id
  join core.unit2tr u2t on u.unit_id = u2t.unit_id
  join core.tr t on u2t.tr_id = t.tr_id
union all
select (select hub_id from core.hub where name = 'egts-v1.6-jep/rnis_msc') "hubId",
       q.tmatic_id::text "unitNum",
       q.object_id       "unitId"
  from (select distinct tmatic_id, object_id
          from rnis.com_vehicles
         where tmatic_id is not null
           and object_id is not null
           and sign_deleted = 0
           and depot_code = 'TScomp'
       ) q
  UNION ALL
      SELECT *
    FROM ibrd.info_board_pkg$hub2ibrd()
 order by "hubId";

-- and object_id not in (3484954, 3484955)

COMMENT ON VIEW core.v_hub2unit IS 'Представление связь хаба с блоком';
COMMENT ON COLUMN core.v_hub2unit."hubId" IS 'Хаб';
COMMENT ON COLUMN core.v_hub2unit."unitNum" IS 'Номер блока';
COMMENT ON COLUMN core.v_hub2unit."unitId" IS 'ID блока';