﻿-- view: core.v_depo

-- drop view core.v_tr2depo;
-- drop view core.v_depo;

create or replace view core.v_depo as
select c.depo_id,
       c.carrier_id,
       c.tn_instance,
       e.name_full as entity_name_full,
       e.name_short as entity_name_short,
       e.comment as entity_comment
  from core.depo c
  join core.entity e on e.entity_id = c.depo_id;

alter table core.v_depo owner to adv;

comment on view core.v_depo is 'Транспортное предприятие';
comment on column core.v_depo.depo_id is 'Идентификатор Транспортное предприятие';
comment on column core.v_depo.carrier_id is 'Идентификатор Транспортная организация';
comment on column core.v_depo.tn_instance is 'Код предприятия';
comment on column core.v_depo.entity_name_full is 'Полное наименование Транспортного предприятия';
comment on column core.v_depo.entity_name_short is 'Краткое наименование Транспортного предприятия';
comment on column core.v_depo.entity_comment is 'примнчание к Транспортному предприятию';

-- select * from core.v_depo
