--drop view core.v_equipment2unit;

create or replace view core.v_equipment2unit  as
  select sensor_id as equipment_id,  unit_id, sys_period, sensor_num  from core.sensor2unit union all
  select monitor_id as equipment_id, unit_id, sys_period, null from core.monitor2unit union all
  select hdd_id as equipment_id,     unit_id, sys_period, null from core.hdd2unit union all
  select modem_id as equipment_id,   unit_id, sys_period, null from core.modem2unit;

COMMENT ON VIEW core.v_equipment2unit IS 'Представление связь оборудования с блоком';
COMMENT ON COLUMN core.v_equipment2unit.equipment_id IS 'Оборудование';
COMMENT ON COLUMN core.v_equipment2unit.unit_id IS 'Блок';
COMMENT ON COLUMN core.v_equipment2unit.sys_period IS 'Период действия';
COMMENT ON COLUMN core.v_equipment2unit.sensor_num IS 'Номер датчика';
