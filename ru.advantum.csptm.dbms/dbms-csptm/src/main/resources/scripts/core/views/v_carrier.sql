﻿-- view: core.v_carrier

--drop view core.v_carrier;

create or replace view core.v_carrier as
select c.carrier_id,  c.sign_deleted, c.update_date,
       e.name_full as carrier_name, e.name_short as carrier_short_name, e.comment as entity_comment
  from core.carrier c
  join core.entity e on e.entity_id = c.carrier_id;

alter table core.v_carrier owner to adv;


comment on view core.v_carrier is 'транспортная организация';
comment on column core.v_carrier.carrier_id is 'id';
comment on column core.v_carrier.sign_deleted is 'удален';
comment on column core.v_carrier.update_date is 'время обновления';
comment on column core.v_carrier.carrier_name IS 'Наименование полное';
comment on column core.v_carrier.carrier_short_name IS 'Наименование сокращенное';
comment on column core.v_carrier.entity_comment IS 'Дополнительно';
