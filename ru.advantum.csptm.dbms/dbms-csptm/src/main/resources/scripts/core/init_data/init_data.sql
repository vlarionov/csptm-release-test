﻿﻿﻿
--core.account
INSERT INTO core.account (account_id, last_name, first_name, middle_name, login, password, dt_insert, dt_update, dt_lock, is_locked) VALUES (1, 'jadmin', 'jadmin', 'jadmin', 'jadmin', '5A2F7FCAC6817747662F8CC3606C4464', null, '2016-09-06 11:16:06.920693', null, '2030-01-01 00:00:00.000000', false);
INSERT INTO core.account (account_id, last_name, first_name, middle_name, login, password, dt_insert, dt_update, dt_lock, is_locked) VALUES (2, 'cadmin', 'cadmin', 'cadmin', 'cadmin', '441A5FB76039CB8CC1E0E58825EDFCBF', null, '2016-09-06 11:16:29.162507', null, '2030-01-01 00:00:00.000000', false);

/*equipment_status
Статусы бортового оборудования. Справочник предзаполненный: 
- Не определен  
- Исправное 
- Неисправное 
- Ремонт 
- Списан*/

insert into core.equipment_status (name ) values('Не определен');
insert into core.equipment_status ( name ) values('Исправно');
insert into core.equipment_status (  name ) values('Неисправное');
insert into core.equipment_status (  name ) values('Ремонт');
insert into core.equipment_status (  name ) values('Списан');



/*core.unit_service_type
Справочник из 3-х значений:
- Отсутствует
- Гарантийное обслуживание
- Внешнее техническое сопровождение
*/
insert into core.unit_service_type(name) values('Отсутствует');
insert into core.unit_service_type(name) values('Гарантийное обслуживание');
insert into core.unit_service_type(name) values('Внешнее техническое сопровождение');

insert INTO core.tr_status(tr_status_id, name) VALUES (1, 'Новый');
insert INTO core.tr_status(tr_status_id, name) VALUES (2, 'Старый');

insert INTO core.tr_type(tr_type_id, name) VALUES (1, 'Автобус');
insert INTO core.tr_type(tr_type_id, name) VALUES (2, 'Троллебус');

insert INTO core.breakdown_status (breakdown_status_id, name) VALUES (1, 'Открыта');
insert INTO core.breakdown_status (breakdown_status_id, name) VALUES (3, 'Закрыта');

insert INTO core.repair_request_status (repair_request_status_id, name) VALUES (1, 'Новая');
insert INTO core.repair_request_status (repair_request_status_id, name) VALUES (2, 'Передано Подрядчику');
insert INTO core.repair_request_status (repair_request_status_id, name) VALUES (3, 'Ремонт');
insert INTO core.repair_request_status (repair_request_status_id, name) VALUES (4, 'Отказ');
insert INTO core.repair_request_status (repair_request_status_id, name) VALUES (5, 'Отремонтировано');
insert INTO core.repair_request_status (repair_request_status_id, name) VALUES (100, 'Закрыто');

insert into core.equipment_class(equipment_class_id, name)  values(0, 'Бортовой блок');
insert into core.equipment_class(equipment_class_id, name)  values(1, 'Датчик');
insert into core.equipment_class(equipment_class_id, name)  values(2, 'Другое');


INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id) VALUES (1, 'Бортовой блок', 'ББ', 0);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id) VALUES (2, 'Бортовой навигационно-связной терминал', 'БНСТ', 0);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id) VALUES (3, 'Датчик уровня топлива', 'ДУТ', 1);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id) VALUES (4, 'Автоматизированная система мониторинга пассажиропотоков', 'АСМПП', 1);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id) VALUES (5, 'Датчик задымления в кабине водителя', 'ИПК', 1);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id) VALUES (6, 'Тревожная кнопка', 'ТК', 1);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id) VALUES (7, 'Датчик включения заднего хода', 'ДВ ЗХ', 1);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id) VALUES (9, 'Бортовая навигационной-связная радиостанция', 'БНСР', 0);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id) VALUES (10, 'Комплект бортового телематического оборудования безопасности', 'КБТОБ', 0);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id) VALUES (11, 'Датчик включения автономного хода', 'ДВ АХ', 1);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id) VALUES (12, 'Манипулятор БНСТ', 'Манипулятор БНСТ', 1);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id) VALUES (13, 'Манипулятор БНСР', 'Манипулятор БНСР', 1);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id) VALUES (14, 'Микрофон', 'Микрофон', 1);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id) VALUES (16, 'Модем', 'Модем', 1);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id) VALUES (17, 'Монитор водителя', 'Монитор водителя', 1);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id) VALUES (18, 'Камера наблюдения', 'Камера наблюдения', 1);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id) VALUES (19, 'Жесткий диск', 'ЖД', 1);



INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (73, 210, 56, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (74, 211, 56, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (75, 212, 56, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (76, 213, 56, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (17, 214, 54, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (54, 215, 6, 9, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (14, 215, 6, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (90, 216, 58, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (91, 217, 58, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (93, 218, 58, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (94, 219, 58, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (95, 220, 58, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (96, 221, 58, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (83, 222, 59, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (97, 223, 59, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (98, 224, 59, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (99, 225, 59, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (100, 226, 59, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (101, 227, 59, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (102, 228, 59, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (103, 229, 59, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (77, 230, 57, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (78, 231, 57, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (79, 232, 57, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (80, 233, 57, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (4, 400, 4, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (26, 800, 3, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (23, 801, 3, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (27, 1600, 55, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (51, 1601, 55, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (52, 1602, 55, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (53, 1603, 55, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (55, 1604, 55, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (56, 1605, 55, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (33, 3001, 5, 10, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (39, 3002, 6, 10, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (37, 3004, 7, 10, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (50, 11000, 19, 10, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (48, 11001, 18, 10, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (49, 11002, 18, 10, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (57, 11003, 18, 10, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (58, 11004, 18, 10, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (59, 11005, 18, 10, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (60, 11006, 18, 10, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (61, 11007, 18, 10, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (62, 11008, 18, 10, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (63, 11009, 18, 10, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (64, 11010, 18, 10, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (65, 11011, 18, 10, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (66, 11012, 18, 10, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (67, 11050, 17, 10, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (72, 11055, 17, 10, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (68, 11060, 16, 10, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (69, 11070, 14, 10, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (70, 12000, 12, 2, true);
INSERT INTO core.sensor2unit_model (sensor2unit_model_id, num, equipment_type_sensor_id, equipment_type_unit_id, is_required) VALUES (71, 12000, 13, 9, true);



INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (2, 'Бортовой навигационно-связной терминал', 'БНСТ', 0, true);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (3, 'Датчик уровня топлива', 'ДУТ', 1, true);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (4, 'Автоматизированная система мониторинга пассажиропотоков', 'АСМПП', 1, true);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (5, 'Датчик задымления в кабине водителя', 'ИПК', 1, false);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (6, 'Тревожная кнопка', 'ТК', 1, false);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (7, 'Датчик включения заднего хода', 'ДВ ЗХ', 1, false);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (9, 'Бортовая навигационной-связная радиостанция', 'БНСР', 0, true);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (10, 'Комплект бортового телематического оборудования безопасности', 'КБТОБ', 0, true);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (12, 'Манипулятор БНСТ', 'Манипулятор БНСТ', 2, false);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (13, 'Манипулятор БНСР', 'Манипулятор БНСР', 2, false);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (14, 'Микрофон', 'Микрофон', 2, false);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (16, 'Модем', 'Модем', 2, false);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (17, 'Монитор водителя', 'Монитор водителя', 2, true);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (18, 'Камера наблюдения', 'Камера наблюдения', 1, true);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (19, 'Жесткий диск', 'ЖД', 2, true);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (54, 'Система автономного хода', 'САХ', 1, false);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (55, 'Температурный датчик', 'ДТ', 1, true);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (56, 'Aналоговый датчик', 'АД', 1, false);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (57, 'Импульсный датчик', 'ИД', 1, false);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (58, 'Цифровой вход', 'ЦВх', 1, true);
INSERT INTO core.equipment_type (equipment_type_id, name_full, name_short, equipment_class_id, has_diagnostic) VALUES (59, 'Цифровой выход', 'ЦВых', 1, true);


INSERT INTO core.protocol (protocol_id, name_short, name_full, comment) VALUES (3, 'NDTP v6.2', 'Navigation Data Transfer Protocol', null);
INSERT INTO core.protocol (protocol_id, name_short, name_full, comment) VALUES (4, 'EverFocus 400/800/1200 ', 'EverFocus Alarm and GPS Output Specification v1.5', null);
INSERT INTO core.protocol (protocol_id, name_short, name_full, comment) VALUES (5, 'Эфирный протокол УКВ', 'Эфирный протокол УКВ', null);
INSERT INTO core.protocol (protocol_id, name_short, name_full, comment) VALUES (7, 'EGTS', 'ЕГТС версия 1.7 (приказ №285 МТРФ)', null);
INSERT INTO core.protocol (protocol_id, name_short, name_full, comment) VALUES (8, 'IBRD ROW', null, null);
INSERT INTO core.protocol (protocol_id, name_short, name_full, comment) VALUES (9, 'IBRD MONO', null, null);
INSERT INTO core.protocol (protocol_id, name_short, name_full, comment) VALUES (10, 'IBRD RGB', null, null);

INSERT INTO core.hub (hub_id, name, port, ip_address, comment, protocol_id) VALUES (2, 'usw-navi/default', '0', '172.16.21.16', null, 5);
INSERT INTO core.hub (hub_id, name, port, ip_address, comment, protocol_id) VALUES (58, 'egts-v1.6-jep/pkcuip', '9306', '172.16.21.61', null, 7);
INSERT INTO core.hub (hub_id, name, port, ip_address, comment, protocol_id) VALUES (3, 'egts-v1.6-jep/rnis_msc', '9202', '172.16.21.61', null, 7);
INSERT INTO core.hub (hub_id, name, port, ip_address, comment, protocol_id) VALUES (60, 'granit-v6.2-jep/highload', '9307', '172.16.21.61', null, 3);
INSERT INTO core.hub (hub_id, name, port, ip_address, comment, protocol_id) VALUES (5, 'ever-focus/v1.5', '9301', '172.16.21.61', null, 4);
INSERT INTO core.hub (hub_id, name, port, ip_address, comment, protocol_id) VALUES (1, 'granit-v6.2-jep/default (9201)', '9201', '172.16.21.61', null, 3);
INSERT INTO core.hub (hub_id, name, port, ip_address, comment, protocol_id) VALUES (4, 'granit-v6.2-jep-test/default(9203)', '9203', '172.16.21.61', null, 3);
INSERT INTO core.hub (hub_id, name, port, ip_address, comment, protocol_id) VALUES (61, 'ibrd-row/default', '36001', '172.16.21.61', null, 8);
INSERT INTO core.hub (hub_id, name, port, ip_address, comment, protocol_id) VALUES (62, 'ibrd-mono/default', '36002', '172.16.21.61', null, 9);
INSERT INTO core.hub (hub_id, name, port, ip_address, comment, protocol_id) VALUES (66, 'granit-v6.2-jep/mgt', '9201', '10.68.1.78', null, 3);

INSERT INTO core.event_type_binding (id, event_type, procedure_name) VALUES (0, 'testType', 'core.event_manager$event_type_binding_list');
INSERT INTO core.event_type_binding (id, event_type, procedure_name) VALUES (1, 'OPER_ROUND_COMPLETE', 'asd.update_oper_round_visit');
INSERT INTO core.event_type_binding (id, event_type, procedure_name) VALUES (2, 'STOP_VISIT', 'tt.update_order_oper_time_fact');
INSERT INTO core.event_type_binding (id, event_type, procedure_name) VALUES (4, 'EVENT_TEST', 'askp.test_rabbit');
INSERT INTO core.event_type_binding (id, event_type, procedure_name) VALUES (3, 'Incident', 'kdbo.protocol_pkg$save');
INSERT INTO core.event_type_binding (id, event_type, procedure_name) VALUES (5, 'ASKP_RECALC', 'askp.askp_recalc');
INSERT INTO core.event_type_binding (id, event_type, procedure_name) VALUES (6, 'ORDER_LISTS_AFTER_LOAD', 'ttb.tt_oper_pkg$create_tt_oper_now');
INSERT INTO core.event_type_binding (id, event_type, procedure_name) VALUES (7, 'STOP_ITEM_PASS', 'ttb.fact_processing_pkg$process_stop_item_pass_json');
INSERT INTO core.event_type_binding (id, event_type, procedure_name) VALUES (8, 'TT_ACTION_COMPLETE', 'ttb.fact_processing_pkg$process_tt_action_complete_json');