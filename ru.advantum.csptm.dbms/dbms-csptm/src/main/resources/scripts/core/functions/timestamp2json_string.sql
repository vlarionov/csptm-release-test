create or replace function core.timestamp2json_string(t timestamp)
  returns text  as
  $$

      select to_char(t, 'YYYY-MM-DD')
                     || 'T'
                     || to_char(t, 'HH24:MI:SS')
                     || 'Z' ;

$$ language sql immutable;