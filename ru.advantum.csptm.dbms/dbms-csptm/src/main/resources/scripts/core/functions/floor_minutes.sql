create or replace function core.floor_minutes
    (timestamp without time zone, integer)
returns timestamp without time zone as
$$
  select
     date_trunc('hour', $1)
     +  cast(($2::varchar||' min') as interval)
     * floor(
     (date_part('minute',$1)::float + date_part('second',$1)/ 60.)::float
     / $2::float
      )
$$ language sql immutable;