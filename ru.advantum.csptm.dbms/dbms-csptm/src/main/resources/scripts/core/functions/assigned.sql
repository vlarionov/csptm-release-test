-- http://redmine.advantum.ru/issues/21998 п.2
create or replace function core.assigned(variadic a text[]) returns boolean
LANGUAGE plpgsql
IMMUTABLE
as $$
declare
  v text;
begin
  if a is null or cardinality(a) = 0 then return false; end if;
  foreach v in array a loop
    if v is null or cardinality(string_to_array(replace(replace(v, '{', ''), '}', ''), ',')) = 0 then
      return false;
    end if;
  end loop;
  return true;
end;
$$
