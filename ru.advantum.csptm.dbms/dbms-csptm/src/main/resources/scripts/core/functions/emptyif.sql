create or replace function core.emptyif(r1 tsrange, r2 tsrange = tsrange(null, null)) returns tsrange
language sql
AS $$
  select case when r1 = r2 then 'empty'::tsrange else r1 end
 $$
