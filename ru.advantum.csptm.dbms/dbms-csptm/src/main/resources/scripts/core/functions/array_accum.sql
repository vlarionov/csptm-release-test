do $$
begin
    if not exists(select * from pg_proc where proname = 'array_accum') then

        create aggregate core.array_accum(anyarray)
        (
            sfunc = array_cat,
            stype = anyarray,
            initcond = '{}'
        );

    end if;
end$$;
