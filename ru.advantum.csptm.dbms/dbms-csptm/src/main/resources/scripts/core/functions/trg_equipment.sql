CREATE OR REPLACE FUNCTION core.trg_equipment()
  RETURNS TRIGGER AS
$BODY$
DECLARE
    l_equipment_type_id core.equipment_model.equipment_type_id%type;
BEGIN

  IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
    BEGIN
      SELECT equipment_type_id
      INTO l_equipment_type_id
      FROM core.equipment_model
      WHERE equipment_model_id = NEW.equipment_model_id;
    END;

    NEW.equipment_type_id := l_equipment_type_id;

    RETURN NEW;
  END IF;
  IF (TG_OP = 'DELETE') THEN
    RETURN OLD;
  END IF;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;