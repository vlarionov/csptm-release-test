create or replace function core.tsrange2hhmi(rng tsrange ) returns text
immutable
language sql
as $$
  select case when is_inf then 'Бесконечно' else concat(to_char((imin - mincnt)/60, '00'), ':', to_char(mincnt, '00')) end
  from (select upper_inf(rng) or lower_inf(rng) is_inf, core.interval2minutes(upper(rng) - lower(rng)) imin, mod(core.interval2minutes(upper(rng) - lower(rng)), 60) mincnt) q
$$