create or replace function week_day(p_dt date, p_tag integer DEFAULT 1) returns text
language sql
as $$
select tg.tag_name
  from ttb.calendar c join  ttb.calendar_item ci on ci.calendar_id = c.calendar_id
       join ttb.calendar_tag tg on tg.calendar_tag_id = ci.calendar_tag_id
  where tg.calendar_tag_group_id = p_tag
     and c.dt = p_dt;
$$;

create or replace function week_day(p_dt text, p_tag integer DEFAULT 1) returns text
language sql
as $$
select tg.tag_name
  from ttb.calendar c join  ttb.calendar_item ci on ci.calendar_id = c.calendar_id
       join ttb.calendar_tag tg on tg.calendar_tag_id = ci.calendar_tag_id
  where tg.calendar_tag_group_id = p_tag
     and c.dt = p_dt::date;
$$;

create or replace function week_day(p_dt timestamp with time zone, p_tag integer DEFAULT 1) returns text
language sql
as $$
select tg.tag_name
  from ttb.calendar c join  ttb.calendar_item ci on ci.calendar_id = c.calendar_id
       join ttb.calendar_tag tg on tg.calendar_tag_id = ci.calendar_tag_id
  where tg.calendar_tag_group_id = p_tag
     and c.dt = p_dt::date;
$$;

create or replace function week_day(p_dt timestamp, p_tag integer DEFAULT 1) returns text
language sql
as $$
select tg.tag_name
  from ttb.calendar c join  ttb.calendar_item ci on ci.calendar_id = c.calendar_id
       join ttb.calendar_tag tg on tg.calendar_tag_id = ci.calendar_tag_id
  where tg.calendar_tag_group_id = p_tag
     and c.dt = p_dt::date;
$$;
