﻿create or replace function core.round_minutes (timestamp)
returns timestamp
as $$
   select date_trunc('minute', $1) + interval '1 min' * round(date_part('second', $1) / 60);
$$ language sql immutable;
