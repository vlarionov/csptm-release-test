﻿create or replace function core.interval2minutes (interval)
returns integer
as $$
   select round(extract(epoch from $1) / 60)::integer
$$ language sql immutable;
