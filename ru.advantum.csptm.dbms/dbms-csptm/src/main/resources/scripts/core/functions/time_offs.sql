create or replace function core.hour_offs() returns int -- смещение от UTC в часах
language sql
IMMUTABLE
STRICT
as $$
  select 3::int;
$$;

create or replace function core.time_offs() returns interval -- смещение от UTC интервал
language sql
IMMUTABLE
STRICT
as $$
  select make_interval(hours => core.hour_offs());
$$;

-- переводы из UTC
create or replace function core.from_utc(dd date) returns timestamp
language sql
IMMUTABLE
STRICT
as $$
  select dd + core.time_offs();
$$;

create or replace function core.from_utc(t timestamp) returns timestamp
language sql
IMMUTABLE
STRICT
as $$
  select t + core.time_offs();
$$;

create or replace function core.from_utc(t text) returns timestamp
language sql
IMMUTABLE
STRICT
as $$
  select t::timestamp + core.time_offs();
$$;

create or replace function core.from_utc(h int) returns int
IMMUTABLE
STRICT
language sql
as $$
  select h + core.hour_offs();
$$;

-- переводы в UTC
create or replace function core.to_utc(dd date) returns timestamp
language sql
IMMUTABLE
STRICT
as $$
  select dd - core.time_offs();
$$;

create or replace function core.to_utc(t timestamp) returns timestamp
language sql
IMMUTABLE
STRICT
as $$
  select t - core.time_offs();
$$;

create or replace function core.to_utc(t text) returns timestamp
language sql
IMMUTABLE
STRICT
as $$
  select t::timestamp - core.time_offs();
$$;

create or replace function core.to_utc(h int) returns int
language sql
IMMUTABLE
STRICT
as $$
  select h - core.hour_offs();
$$;

/*
select now()::text,
       core.from_utc(now()::timestamp)::text, core.from_utc('2017/11/27'::timestamp), core.from_utc('2017/11/27'::date), core.from_utc('2017/11/27'),
       core.to_utc(now()::timestamp)::text, core.to_utc('2017/11/27'::timestamp), core.to_utc('2017/11/27'::date), core.to_utc('2017/11/27')

select core.time_offs(), core.hour_offs()*/
