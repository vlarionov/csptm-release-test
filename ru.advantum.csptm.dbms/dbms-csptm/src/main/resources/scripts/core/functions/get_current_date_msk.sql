create or replace function core.get_date_current_msk()
  returns timestamp  as
  $$
  select date_trunc('day', now() + interval '3 hours')::timestamp;
$$ language sql immutable;

