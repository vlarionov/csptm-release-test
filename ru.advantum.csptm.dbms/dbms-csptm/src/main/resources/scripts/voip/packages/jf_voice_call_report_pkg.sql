CREATE OR REPLACE FUNCTION voip.jf_voice_call_report_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_f_begin_date          TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'f_begin_date_DT', FALSE);
  l_f_end_date            TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'f_end_date_DT', FALSE);
  l_f_route_muid          BIGINT [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_route_MUID', TRUE);
  l_f_timetable_entry_num SMALLINT := jofl.jofl_pkg$extract_number(p_attr, 'f_timetable_entry_num_NUMBER', TRUE);
  l_f_driver_tab_num      TEXT := jofl.jofl_pkg$extract_varchar(p_attr, 'f_driver_tab_num_TEXT', TRUE);
  l_f_garage_num          core.tr.garage_num%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'f_garage_num_TEXT', TRUE);
  l_f_account_id          core.account.account_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'f_account_id', TRUE);
BEGIN
  OPEN p_rows FOR
  WITH voice_call2order_round AS (
      SELECT
        vc.voice_call_id,
        vc2u.tr_id,
        voip.jf_voice_call_report_pkg$find_order_round(vc.voice_call_date, vc2u.tr_id) order_round_id
      FROM voip.voice_call vc
        JOIN voip.voice_call2unit vc2u ON vc.voice_call_id = vc2u.voice_call_id
      WHERE vc.voice_call_date BETWEEN l_f_begin_date AND l_f_end_date
            AND (vc.account_id = l_f_account_id OR l_f_account_id IS NULL)
  )
  SELECT
    vc2ord.voice_call_id,
    de.name_short                                                            depo,
    r.number                                                                 route_num,
    te.timetable_entry_num,
    vc.voice_call_begin                                                      begin_date,
    d.tab_num                                                                driver,
    tr.garage_num,
    adm.jf_account_pkg$get_full_name(vc.account_id)                          account,
    EXTRACT(EPOCH FROM (vc.voice_call_end - vc.voice_call_begin)) :: INTEGER duration,
    voip.jf_voice_call_report_pkg$find_initiator(vc.voice_call_id)           initiator,
    null                                                                     subject,
    CASE WHEN vc.voice_call_file IS NOT NULL
      THEN jofl.jofl_link_util$build(vc.voice_call_file, 'cdr/' || vc.voice_call_file, vc.voice_call_file)
    ELSE '' END AS                                                           audio_file_link
  FROM voice_call2order_round vc2ord
    JOIN voip.voice_call vc ON vc2ord.voice_call_id = vc.voice_call_id
    LEFT JOIN core.tr tr ON vc2ord.tr_id = tr.tr_id
    LEFT JOIN core.entity de ON tr.depo_id = de.entity_id
    LEFT JOIN tt.order_round ord ON vc2ord.order_round_id = ord.order_round_id
    LEFT JOIN tt.order_list ol ON ord.order_list_id = ol.order_list_id
    LEFT JOIN tt.round rnd ON ord.round_id = rnd.round_id
    LEFT JOIN tt.timetable_entry te ON rnd.timetable_entry_id = te.timetable_entry_id
    LEFT JOIN tt.timetable t ON te.timetable_id = t.timetable_id
    LEFT JOIN gis.routes r ON t.route_muid = r.muid
    LEFT JOIN tt.driver d ON ol.driver_id = d.driver_id
  WHERE (r.muid = ANY (l_f_route_muid)
         OR array_length(l_f_route_muid, 1) IS NULL
         OR array_length(l_f_route_muid, 1) < 1)
        AND (te.timetable_entry_num = l_f_timetable_entry_num OR l_f_timetable_entry_num IS NULL)
        AND (d.tab_num = l_f_driver_tab_num OR (l_f_driver_tab_num = '') IS NOT FALSE)
        AND (tr.garage_num = l_f_garage_num OR l_f_garage_num IS NULL);
END;
$$;

CREATE OR REPLACE FUNCTION voip.jf_voice_call_report_pkg$find_order_round(p_call_date TIMESTAMP, p_tr_id core.tr.tr_id%TYPE)
  RETURNS tt.order_round.order_round_id%TYPE
LANGUAGE plpgsql
AS $$
DECLARE
  l_result tt.order_round.order_round_id%TYPE;
BEGIN
  SELECT ord.order_round_id
  FROM tt.order_list ol
    JOIN tt.order_round ord ON ol.order_list_id = ord.order_list_id
    JOIN asd.oper_round_visit orv ON ord.order_round_id = orv.order_round_id
  WHERE ol.order_date IN (p_call_date :: DATE, (p_call_date + '1 days' :: INTERVAL) :: DATE, (p_call_date - '1 days' :: INTERVAL) :: DATE)
        AND ord.is_active = 1
        AND ol.is_active = 1 AND ol.sign_deleted = 0
        AND ol.tr_id = p_tr_id
        AND p_call_date BETWEEN orv.time_plan_begin AND orv.time_plan_end
  INTO l_result;
  RETURN l_result;
END;
$$;

CREATE OR REPLACE FUNCTION voip.jf_voice_call_report_pkg$find_initiator(
  p_voice_call_id        voip.voice_call.voice_call_id%TYPE
)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
BEGIN
  IF exists(SELECT 1
            FROM voip.voice_call_request vcr
            WHERE vcr.voice_call_id = p_voice_call_id)
  THEN RETURN 'Водитель';
  ELSE RETURN 'Диспечер';
  END IF;
END;
$$;


