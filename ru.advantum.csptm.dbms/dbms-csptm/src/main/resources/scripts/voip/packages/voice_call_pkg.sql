﻿-- ========================================================================
-- Пакет работы с голосовыми сообщениями
-- ========================================================================

-- Новый вызов
create or replace function voip.voice_call_pkg$voice_call_status_new()
   returns text as 'select ''NEW'' :: text' language sql immutable;

create or replace function voip.voice_call_pkg$register_call_request
  (p_tr_id                  in bigint,
   p_unit_id                in bigint,
   p_request_date           in timestamp)
returns bigint
-- ========================================================================
-- Зарегистрировать Запрос на соединение
-- ========================================================================
as $$ declare
   l_call_request_id        bigint;
   l_req_window       text   := asd.get_constant('VOIP_REQUEST_WINDOW')::text;
begin
   
   select call_request_id into l_call_request_id
     from voip.voice_call_request
    where tr_id = p_tr_id
      and unit_id = p_unit_id
      and call_request_date =
          (select max(q.call_request_date)
             from voip.voice_call_request q
            where q.tr_id = p_tr_id
              and q.unit_id = p_unit_id
              and q.call_request_date <= p_request_date
              and q.call_request_date > p_request_date - (l_req_window || ' min')::interval
              and q.voice_call_id is null
          )
    limit 1;
   
   if not found then
      insert into voip.voice_call_request(tr_id, unit_id, call_request_date, call_request_status)
      values (p_tr_id, p_unit_id, p_request_date, 0)
      returning call_request_id into l_call_request_id;
      
      declare
         l_send_result json;
      begin
         l_send_result := ttb.jf_incident_pkg$send_incident(
                             p_incidents_array :=
                                ARRAY[ttb.jf_incident_pkg$new_incident(
                                         p_timeStart := now()::timestamp,
                                         p_incident_initiator_id := ttb.jf_incident_pkg$cn_initiator_system(),
                                         p_incident_status_id := ttb.jf_incident_pkg$cn_status_new(),
                                         p_incident_type_id := ttb.jf_incident_pkg$cn_voice_call(),
                                         p_parent_incident_id := null,
                                         p_account_id := null,
                                         p_incident_payload :=
                                            ttb.jf_incident_pkg$new_payload (p_trList := array[p_tr_id::int])
                                     )]
                          );
      end;
   end if;
   
   return l_call_request_id;
end;
$$ language plpgsql security definer;


create or replace function voip.voice_call_pkg$call_to_units
  (p_account_id             in bigint,
   p_call_request_id        in bigint,
   p_unit_list              in integer[],
   p_driver_list            in integer[] default null,
   p_redial                 in boolean default false)
returns text
-- ========================================================================
-- Зарегистрировать и отправить Групповой Вызов голосовой связи
-- ========================================================================
as $$ declare
   l_voice_call_id    bigint;
   l_voice_call_date  timestamp := now();
   l_voice_call_redial_id integer := null;
   
   l_rmq_usw_queue    text   := 'ui2usw2';
   l_rmq_redial_queue text   := 'voip-call';
   l_rmq_queue        text   := l_rmq_usw_queue;
   l_rmq_type         text   := 'UniversalCallRecord';
   
   l_owner_number     text;
   l_equ_type_bnst    bigint := asd.get_constant('VOIP_BNST_TYPE_ID')::bigint;
   l_equ_type_bnsr    bigint := asd.get_constant('VOIP_BNSR_TYPE_ID')::bigint;
   l_equ_type_ext     bigint := -1;     -- abc: пока не понятно откуда брать
   l_req_window       text   := asd.get_constant('VOIP_REQUEST_WINDOW')::text;
   l_unit_id          integer;
   
   l_rec              record;
   l_unit_rec         record;
   l_gsm_numbers      bigint[];
   l_usw_numbers      bigint[];
   l_ext_numbers      bigint[];
   l_tr_arr           bigint[];
   l_unit_arr         bigint[];
   l_command          jsonb;
begin

-- raise notice 'l_equ_type_bnst %', l_equ_type_bnst;

-- raise notice 'p_unit_list = %', p_unit_list;

   select operator_phone into l_owner_number from core.account where account_id = p_account_id;
   
   if l_owner_number is null then
      raise exception using
            errcode = jofl.jofl_proxy_pkg$get_error_code(),
            message = '<<Не задан Телефон Оператора>>';
   end if;
   
   l_command := jsonb_build_object('owner', l_owner_number::bigint);
-- raise notice '%', l_command;

   for l_rec in (select mod.equipment_type_id, u2t.tr_id, u2t.unit_id, uit.unit_num,
                        case 
                           when mod.equipment_type_id = l_equ_type_bnst and left(crd.phone_num, 1) = '7' then
                              '8' || substring(crd.phone_num from 2)
                           when mod.equipment_type_id = l_equ_type_bnsr then
                              uit.unit_num
                           else
                              crd.phone_num
                        end as phone_num
                   from core.equipment equ
                   join core.equipment_model mod on mod.equipment_model_id = equ.equipment_model_id
                   left join core.sim_card2unit unt on unt.equipment_id = equ.equipment_id
                   left join core.sim_card crd on crd.sim_card_id = unt.sim_card_id
                   left join core.unit uit on uit.unit_id = equ.equipment_id
                   left join core.unit2tr u2t on u2t.unit_id = equ.equipment_id
                  where equ.equipment_id = any (p_unit_list))
   loop
      if l_equ_type_bnst = l_rec.equipment_type_id then
         
         l_gsm_numbers := array_append(l_gsm_numbers, l_rec.phone_num::bigint);
         
      elsif l_equ_type_bnsr = l_rec.equipment_type_id then
         
         l_usw_numbers := array_append(l_usw_numbers, l_rec.phone_num::bigint);
         
      elsif l_equ_type_ext = l_rec.equipment_type_id then
         
         l_ext_numbers := array_append(l_ext_numbers, l_rec.phone_num::bigint);
         
      else
         raise exception using
               errcode = jofl.jofl_proxy_pkg$get_error_code(),
               message = '<<Неверный тип блока>>';
      end if;
      
      if l_rec.tr_id is not null then
         l_tr_arr   := array_append(l_tr_arr, l_rec.tr_id);
         l_unit_arr := array_append(l_unit_arr, l_rec.unit_id);
      else
         raise exception using
               errcode = jofl.jofl_proxy_pkg$get_error_code(),
               message = '<<Блок ' || l_rec.unit_num || ' не установлн на ТС>>';
      end if;
      
   end loop;

-- raise notice 'units: %', l_unit_arr::text;
-- raise notice 'tr: %', l_tr_arr::text;
-- raise notice 'driver: %', p_driver_list::text;
   
   if array_length(l_gsm_numbers, 1) > 0 then
      l_command := l_command || jsonb_build_object('gsmNumbers', array_to_json(l_gsm_numbers));
--    raise notice '%', l_command;
   end if;
   
   if array_length(l_usw_numbers, 1) > 0 then
      l_command := l_command || jsonb_build_object('uswNumbers', array_to_json(l_usw_numbers));
--    raise notice '%', l_command;
   end if;   
   
   if array_length(l_ext_numbers, 1) > 0 then
      l_command := l_command || jsonb_build_object('stationNumbers', array_to_json(l_ext_numbers));
--    raise notice '%', l_command;
   end if;
   
   if array_length(l_gsm_numbers, 1) = 0 and
      array_length(l_usw_numbers, 1) = 0 and
      array_length(l_ext_numbers, 1) = 0 then
      return 'NO CALL NUMBER';
   end if;
   
   if p_redial then
      -- Вызов с автодозвоном
      l_rmq_queue := l_rmq_redial_queue;
      
      insert into voip.voice_call_redial(voice_call_redial_time, voice_call_redial_status)
      values (localtimestamp, 'TRY_0')
      returning voice_call_redial_id into l_voice_call_redial_id;
      
      l_command := l_command || jsonb_build_object('idRedialCall', l_voice_call_redial_id);
-- raise notice '%', l_command;
   
   end if;
   
   insert into voip.voice_call(account_id, voice_call_date, voice_call_status, voice_call_redial_id)
   values (p_account_id, l_voice_call_date, voip.voice_call_pkg$voice_call_status_new(), l_voice_call_redial_id)
   returning voice_call_id into l_voice_call_id;
   
   l_command := l_command || jsonb_build_object('idCall', l_voice_call_id);
-- raise notice '%', l_command;
   
   for i in array_lower(l_unit_arr, 1)..array_upper(l_unit_arr, 1) loop
      insert into voip.voice_call2unit(voice_call_id, unit_id, tr_id, driver_id)
      values (l_voice_call_id, l_unit_arr[i], l_tr_arr[i], case when p_driver_list is null then null::int else p_driver_list[i] end)
      on conflict (voice_call_id, unit_id)
      do nothing;
   end loop;
   
   if p_call_request_id is not null then
      update voip.voice_call_request
         set voice_call_id = l_voice_call_id
       where call_request_id = p_call_request_id;
   end if;
   
   perform paa.paa_api_pkg$basic_publish(cmnd.jf_cmnd_ref4unit_pkg$cn_command_rmq_broker(), '', l_rmq_queue, l_command::text, l_rmq_type);
   
   return 'OK';
end;
$$ language plpgsql volatile security invoker;


create or replace function voip.voice_call_pkg$call_to_unit
  (p_account_id             in bigint,
   p_tr_id                  in bigint,
   p_unit_id                in integer,
   p_call_request_id        in bigint,
   p_driver_id              in integer)
returns text
-- ========================================================================
-- Зарегистрировать и отправить Вызов голосовой связи
-- ========================================================================
as $$ declare
begin
   if not exists(select 1 from core.sim_card2unit unt
                   left join core.sim_card crd on crd.sim_card_id = unt.sim_card_id
                  where unt.equipment_id = p_unit_id)
   then
      raise exception '<<"Не найден номер телефона. К блоку не привязана sim-карта. Позвонить на ТС невозможно">>';
   end if;
   
   return voip.voice_call_pkg$call_to_units(p_account_id, p_call_request_id, array[p_unit_id], array[p_driver_id], false);
end;
$$ language plpgsql volatile security invoker;


create or replace function voip.voice_call_pkg$call_to_unit
  (p_account_id             in bigint,
   p_tr_id                  in bigint,
   p_unit_id                in integer,
   p_call_request_id        in bigint,
   p_driver_id              in integer,
   p_redial                 in boolean)
returns text
-- ========================================================================
-- Зарегистрировать и отправить Вызов голосовой связи
-- ========================================================================
as $$ declare
begin
   if not exists(select 1 from core.sim_card2unit unt
                   left join core.sim_card crd on crd.sim_card_id = unt.sim_card_id
                  where unt.equipment_id = p_unit_id)
   then
      raise exception '<<"Не найден номер телефона. К блоку не привязана sim-карта. Позвонить на ТС невозможно">>';
   end if;
   
   return voip.voice_call_pkg$call_to_units(p_account_id, p_call_request_id, array[p_unit_id], array[p_driver_id], p_redial);
end;
$$ language plpgsql volatile security invoker;


create or replace function voip.voice_call_pkg$call_to_unit
  (p_account_id             in bigint,
   p_tr_id                  in bigint,
   p_unit_id                in integer,
   p_call_request_id        in bigint)
returns text
-- ========================================================================
-- Зарегистрировать и отправить Вызов голосовой связи
-- ========================================================================
as $$ declare
begin
   if not exists(select 1 from core.sim_card2unit unt
                   left join core.sim_card crd on crd.sim_card_id = unt.sim_card_id
                  where unt.equipment_id = p_unit_id)
   then
      raise exception '<<"Не найден номер телефона. К блоку не привязана sim-карта. Позвонить на ТС невозможно">>';
   end if;
   
   return voip.voice_call_pkg$call_to_units(p_account_id, p_call_request_id, array[p_unit_id], null, false);
end;
$$ language plpgsql volatile security invoker;


create or replace function voip.voice_call_pkg$update_call_request_status
  (p_call_id                in bigint,
   p_call_begin             in timestamp,
   p_call_end               in timestamp,
   p_audiofile              in text,
   p_status                 in text)
returns bigint
-- ========================================================================
-- Редактировать статус голосового вызова
-- ========================================================================
as $$ declare
   l_result                 bigint := 0;
begin
   update voip.voice_call
      set voice_call_status = coalesce(p_status, voice_call_status),
          voice_call_begin  = coalesce(p_call_begin, voice_call_begin),
          voice_call_end    = coalesce(p_call_end, voice_call_end),
          voice_call_file   = coalesce(p_audiofile, voice_call_file)
    where voice_call_id = p_call_id;
   
   get diagnostics l_result = row_count;
   
   return l_result;
end;
$$ language plpgsql security definer;


create or replace function voip.voice_call_pkg$cancel_call_to_units
  (p_voice_call_redial_id   in integer)
returns text
-- ========================================================================
-- Отмена голосового вызова с автодозвоном
-- ========================================================================
as $$ declare
   l_rmq_queue        text  := 'voip-call';
   l_rmq_type         text  := 'CancelCallRecord';
   l_command          jsonb := jsonb_build_object('idRedialCall', p_voice_call_redial_id);
begin
 --raise notice '%', l_command;
   perform paa.paa_api_pkg$basic_publish(cmnd.jf_cmnd_ref4unit_pkg$cn_command_rmq_broker(), '', l_rmq_queue, l_command::text, l_rmq_type::text);
   return 'OK';
end;
$$ language plpgsql security definer;


create or replace function voip.voice_call_pkg$set_redial_status
  (p_voice_call_redial_id   in integer,
   p_voice_call_redial_status in text)
returns text
-- ========================================================================
-- Установить статус автодозвона
-- ========================================================================
as $$
begin
   update voip.voice_call_redial
      set voice_call_redial_time = localtimestamp,
          voice_call_redial_status = p_voice_call_redial_status
    where voice_call_redial_id = p_voice_call_redial_id;
   
   return 'OK';
end;
$$ language plpgsql security definer;


create or replace function voip.voice_call_pkg$prepare_new_call_try
  (p_call_id                in bigint,
   p_new_call_id            in bigint)
returns text
-- ========================================================================
-- Регистрация попытки автодозвона
-- ========================================================================
as $$ declare
begin
   
   insert into voip.voice_call
         (voice_call_id, account_id, voice_call_date, voice_call_status, voice_call_redial_id)
   select p_new_call_id, account_id, localtimestamp, voip.voice_call_pkg$voice_call_status_new(), voice_call_redial_id
     from voip.voice_call
    where voice_call_id = p_call_id;
   
   insert into voip.voice_call2unit
         (voice_call_id, unit_id, tr_id, driver_id)
   select p_new_call_id, unit_id, tr_id, driver_id
     from voip.voice_call2unit
    where voice_call_id = p_call_id;
   
   return 'OK';   
end;
$$ language plpgsql security definer;


create or replace function voip.voice_call_pkg$get_call_status
  (p_call_id                in bigint)
returns text
-- ========================================================================
-- Статус автодозвона
-- ========================================================================
as $$ declare
   v_answer                     integer;
   v_channel_destroy            integer;
   v_voice_call_redial_status   text;
begin
   
   select vor.voice_call_redial_status
     into v_voice_call_redial_status
     from voip.voice_call voc
     join voip.voice_call_redial vor on vor.voice_call_redial_id = voc.voice_call_redial_id
    where voice_call_id = p_call_id;
   
   if v_voice_call_redial_status is not null and v_voice_call_redial_status = 'CANCEL' then
      return 'CANCEL';
   end if;
   
   select coalesce(sum(case when voice_call_status = 'CHANNEL_ANSWER' then 1 else 0 end), 0) answer,
          coalesce(sum(case when voice_call_status = 'CHANNEL_DESTROY' or voice_call_status = 'CHANNEL_HANGUP_COMPLETE' then 1 else 0 end), 0) channel_destroy
     into v_answer, v_channel_destroy
     from hist.v_voice_call_hist 
    where voice_call_id = p_call_id
      and voice_call_status in ('CHANNEL_ANSWER',
                                'CHANNEL_HANGUP_COMPLETE',
                                'CHANNEL_DESTROY');
   
   if v_answer > 1 then
      return 'OK';
   elsif v_channel_destroy > 0 then
      return 'ERROR';
   end if;
   
   return 'TRY';
end;
$$ language plpgsql security definer;
