﻿create or replace function voip.jf_voice_call_request_pkg$of_rows
  (p_account_id             in numeric,
   p_attr                   in text,
   p_rows                   out refcursor)
returns refcursor
as $$ declare
   l_begin_date   timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'F_BEGIN_DT', true), now() - interval '1 day');
   l_end_date     timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'F_END_DT', true), now());
   l_depo_id      bigint    := jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', true);
   l_tr_id        bigint    := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_id', true);
   l_driver       text      := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_DRIVER_TEXT', true), '');
begin
   open p_rows for
   select vcr.call_request_id,
          vcr.tr_id,
          vcr.unit_id,
          vcr.call_request_date,
          tr.garage_num,
          tr.licence,
          eqt.name_short as unit_type_short_name,
          equ.serial_num as unit_num
     from voip.voice_call_request vcr
     join core.tr on tr.tr_id = vcr.tr_id
     join core.equipment       equ on equ.equipment_id = vcr.unit_id
     join core.equipment_model eqm on eqm.equipment_model_id = equ.equipment_model_id
     join core.equipment_type  eqt on eqt.equipment_type_id = eqm.equipment_type_id
    where vcr.call_request_date >= l_begin_date
      and vcr.call_request_date <  l_end_date
      and vcr.voice_call_id is null
      and (l_depo_id is null or tr.depo_id = l_depo_id)
      and (l_tr_id is null or vcr.tr_id = l_tr_id)
    order by vcr.call_request_date;
end;
$$ language plpgsql volatile security invoker;


create or replace function voip.jf_voice_call_request_pkg$of_dial
  (p_account_id        in numeric,
   p_attr              in text)
returns text
as $$ declare
   l_tr_id             bigint  := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
   l_unit_id           integer := jofl.jofl_pkg$extract_number(p_attr, 'unit_id', true);
   l_call_request_id   bigint := jofl.jofl_pkg$extract_number(p_attr, 'call_request_id', true);
   l_result            text;
begin
   select voip.voice_call_pkg$call_to_unit(p_account_id::bigint, l_tr_id, l_unit_id, l_call_request_id) into l_result;
   return null;
end;
$$ language plpgsql volatile security invoker;
