﻿create or replace function voip.jf_voice_call_pkg$of_rows
  (p_account_id               in numeric,
   p_attr                     in text,
   p_rows                     out refcursor)
returns refcursor
as $$ declare 
   l_begin_date   timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'F_BEGIN_DT', true), now() - interval '1 day');
   l_end_date     timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'F_END_DT', true), now());
   l_depo_id      bigint    := jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', true);
   l_tr_id        bigint    := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_id', true);
   l_driver       text      := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_DRIVER_TEXT', true), '');
   l_dispatcher   text      := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_DISP_TEXT', true), '');
   l_call_type    bigint    := jofl.jofl_pkg$extract_number(p_attr, 'F_CALL_TYPE_INPLACE_K', true);
   l_result_type  bigint    := jofl.jofl_pkg$extract_number(p_attr, 'F_RESULT_INPLACE_K', true);
begin
   open p_rows for
   with voice_call as (
   select vcr.tr_id,
          vcr.unit_id,
          voc.account_id,
          vcr.call_request_date as call_date,
          voc.voice_call_begin  as call_begin,
          'Входящий'            as call_type,
          to_char(voc.voice_call_end - voc.voice_call_begin, 'MI:SS')::text    as call_duration,
          to_char(voc.voice_call_begin - vcr.call_request_date, 'MI:SS')::text as call_delay,
          voice_call_file       as audio_file
     from voip.voice_call_request vcr
     left join voip.voice_call         voc on voc.voice_call_id = vcr.voice_call_id
    where vcr.call_request_date >= l_begin_date
      and vcr.call_request_date <  l_end_date
      and (l_tr_id is null or vcr.tr_id = l_tr_id)
      and (l_call_type is null or l_call_type = 0 or l_call_type = 2)
   union all
   select vcu.tr_id,
          vcu.unit_id,
          voc.account_id,
          voc.voice_call_date   as call_date,
          voc.voice_call_begin  as call_begin,
          'Исходящий'           as call_type,
          to_char(voc.voice_call_end - voc.voice_call_begin, 'MI:SS')::text  as call_duration,
          to_char(voc.voice_call_begin - voc.voice_call_date, 'MI:SS')::text as call_delay,
          voc.voice_call_file   as audio_file
     from voip.voice_call voc
     join voip.voice_call2unit vcu on vcu.voice_call_id = voc.voice_call_id
    where voc.voice_call_date >= l_begin_date
      and voc.voice_call_date <  l_end_date
      and not exists (select 1 from voip.voice_call_request vc2 where vc2.voice_call_id = voc.voice_call_id)
      and (l_tr_id is null or vcu.tr_id = l_tr_id)
      and (l_call_type is null or l_call_type = 0 or l_call_type = 1)
   )
   select v.tr_id,
          tr.garage_num,
          v.unit_id,
          eqt.name_short as unit_type_short_name,
          equ.serial_num as unit_num,
          dep.entity_name_short as depo_short_name,
          v.account_id,
          acc.last_name || ' ' || acc.first_name || ' ' || acc.middle_name as account_name,
          case when v.account_id is not null then 'Принят' else 'Не принят' end as call_result,
          v.call_date,
          v.call_begin,
          v.call_type,
          v.call_duration,
          v.call_delay,
          v.audio_file,
          case when v.audio_file is not null then jofl.jofl_link_util$build(v.audio_file, 'cdr/' || v.audio_file, v.audio_file) else '' end as audio_file_link
     from voice_call v
     join core.tr on tr.tr_id = v.tr_id
     join core.v_depo dep on dep.depo_id = tr.depo_id
     join core.equipment equ on equ.equipment_id = v.unit_id
     join core.equipment_model eqm on eqm.equipment_model_id = equ.equipment_model_id
     join core.equipment_type eqt on eqt.equipment_type_id = eqm.equipment_type_id
     join core.unit unt on unt.unit_id = equ.equipment_id
     left join core.account acc on acc.account_id = v.account_id
    where (l_depo_id is null or tr.depo_id = l_depo_id)
      and (l_dispatcher = '' or acc.last_name || ' ' || acc.first_name || ' ' || acc.middle_name like '%' || l_dispatcher || '%')
      and (l_result_type is null or 
           (v.account_id is not null and (l_result_type = 0 or l_result_type = 1)) or
           (v.account_id is null and (l_result_type = 0 or l_result_type = 2))
          )
    order by v.call_date;
end;
$$ language plpgsql volatile security invoker;
