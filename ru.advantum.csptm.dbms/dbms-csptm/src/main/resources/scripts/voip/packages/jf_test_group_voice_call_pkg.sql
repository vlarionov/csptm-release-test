﻿create or replace function voip.jf_test_group_voice_call_pkg$of_rows
  (p_account_id             in numeric,
   p_attr                   in text,
   p_rows                   out refcursor)
returns refcursor
as $$ declare
begin
   open p_rows for
   select crd.phone_num, typ.name_short, uit.unit_id, uit.unit_num, tr.tr_id, tr.garage_num, tr.licence
     from core.equipment equ
     join core.equipment_model mod on mod.equipment_model_id = equ.equipment_model_id
     join core.equipment_type typ on typ.equipment_type_id = mod.equipment_type_id
     left join core.sim_card2unit unt on unt.equipment_id = equ.equipment_id
     left join core.sim_card crd on crd.sim_card_id = unt.sim_card_id
     left join core.unit uit on uit.unit_id = equ.equipment_id
     left join core.unit2tr u2t on u2t.unit_id = uit.unit_id
     left join core.tr on tr.tr_id = u2t.tr_id
    where equ.equipment_id in (170269, 24074)
       or (crd.phone_num is not null
      and typ.equipment_type_id in (2, 9))
    order by uit.unit_num;
end;
$$ language plpgsql volatile security invoker;


create or replace function voip.jf_test_group_voice_call_pkg$of_dial
  (p_account_id        in numeric,
   p_attr              in text)
returns text
as $$ declare
   l_unit_list integer[] := jofl.jofl_pkg$extract_tarray(p_attr, 'f_unit_id', true);
   l_result    text;
begin
   select voip.voice_call_pkg$call_to_units(p_account_id::bigint, null::bigint, l_unit_list) into l_result;
   return null;
end;
$$ language plpgsql volatile security invoker;
