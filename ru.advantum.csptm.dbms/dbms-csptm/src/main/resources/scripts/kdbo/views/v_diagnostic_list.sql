
create or replace VIEW
  kdbo.v_diagnostic_list
  as
SELECT
  dp.diagnostic_period_id,
  ed.name_short,
  ed.name_full,
  dp.start_date dt_b,
  dp.end_date   dt_e,
  et.name_short etype_name,
  em.name       emodel_name,
  e.serial_num,
  tr.garage_num
FROM kdbo.diagnostic_period dp
  JOIN core.equipment e ON e.equipment_id = dp.equipment_id
  JOIN core.depo d ON d.depo_id = e.depo_id
  JOIN core.entity ed ON ed.entity_id = d.depo_id
  JOIN core.equipment_model em ON em.equipment_model_id = e.equipment_model_id
  JOIN core.equipment_type et ON et.equipment_type_id = em.equipment_type_id
  JOIN core.v_equipment2tr4all etr ON etr.equipment_id = e.equipment_id
  JOIN core.tr tr ON tr.tr_id = etr.tr_id
  ;

comment on column kdbo.v_diagnostic_list.name_short  is  'Транспортное предприятие';
comment on column kdbo.v_diagnostic_list.dt_b  is  'Дата и время начала диагностики БО';
comment on column kdbo.v_diagnostic_list.dt_e  is  'Дата и время окончания диагностики БО (если оно по данным Системы может быть рассчитано или диагностика уже закончена)';
comment on column kdbo.v_diagnostic_list.etype_name  is  'Тип блока';
comment on column kdbo.v_diagnostic_list.emodel_name  is  'Модель блока';
comment on column kdbo.v_diagnostic_list.serial_num  is  'Серийный номер блока';
comment on column kdbo.v_diagnostic_list.garage_num  is  'Гаражный номер ТС, на котором установлен блок';