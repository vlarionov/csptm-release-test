CREATE OR REPLACE VIEW kdbo.v_unit4diagnostic
AS
  WITH tr AS (
    SELECT DISTINCT ol.tr_id
    FROM
      tt.order_list ol
      JOIN tt.order_round ord ON ol.order_list_id = ord.order_list_id
      JOIN asd.oper_round_visit orv ON ord.order_round_id = orv.order_round_id
    WHERE
      order_date = now() :: DATE AND
      ord.is_active = 1
      AND ol.sign_deleted = 0
      AND ol.is_active = 1
      AND now() BETWEEN orv.time_plan_begin AND orv.time_plan_end
    UNION
    SELECT rd.tr_id
    FROM kdbo.required_diagnostic rd
  )
  SELECT u2t.unit_id
  FROM tr
    JOIN core.unit2tr u2t ON tr.tr_id = u2t.tr_id
    JOIN core.equipment e ON u2t.unit_id = e.equipment_id
    JOIN core.equipment_model em ON em.equipment_model_id = e.equipment_model_id
                                    AND e.has_diagnostic
                                    AND em.has_diagnostic
    JOIN core.equipment_type et ON em.equipment_type_id = et.equipment_type_id
  WHERE et.equipment_type_id = 2
        OR (et.equipment_type_id = 9 AND exists(SELECT 1
                                                FROM cmnd.bnsr_request_list brl
                                                WHERE brl.unit_bnsr_id = u2t.unit_id))
        OR et.equipment_type_id = 10;


COMMENT ON VIEW kdbo.v_unit4diagnostic IS 'Представление диагностируемых в данный момент блоков';
COMMENT ON COLUMN kdbo.v_unit4diagnostic.unit_id IS 'ID блока';