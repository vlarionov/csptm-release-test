create schema kdbo;

create tablespace kdbo_data location '/u00/postgres/pg_data/kdbo/kdbo_data';

create tablespace kdbo_idx location  '/u00/postgres/pg_data/kdbo/kdbo_idx';