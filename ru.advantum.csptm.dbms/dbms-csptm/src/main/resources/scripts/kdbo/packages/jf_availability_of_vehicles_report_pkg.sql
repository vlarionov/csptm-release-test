-- drop FUNCTION kdbo.availability_of_vehicles_report_pkg$of_rows(NUMERIC, TEXT);

CREATE OR REPLACE FUNCTION kdbo.jf_availability_of_vehicles_report_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                                            p_attr       TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $BODY$
DECLARE

  f_depo_id        BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', TRUE);
  f_tr_status_id   BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_status_id', TRUE);
  f_tr_capacity_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_capacity_id', TRUE);
  f_tr_type_id     BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id', TRUE);
  f_dt_DT          TIMESTAMP WITH TIME ZONE :=  jofl.jofl_pkg$extract_date(p_attr, 'f_dt_DT',TRUE) :: TIMESTAMP WITH TIME ZONE;
  /*
  •	Транспортное предприятие – выбор значений из Справочника подразделений (доступ определяется настройкой прав пользователя);
  •	Вид ТС – выбор значений из справочника Видов ТС;
  •	Вместимость ТС – выбор значений из справочника Вместимость ТС;
  •	Статус ТС – выбор значений из Справочника статусов ТС
  */
BEGIN

  OPEN p_rows FOR
SELECT
  CASE GROUPING(tr.tr_status_id, trs.name, tr.depo_id, e.name_short)
  WHEN 12
    THEN chr(8239) ||'Итого:'
  ELSE
    trs.name
  END trs_name,
  e.name_short    depo_short_name,
  count(tr.tr_id) CNT
  FROM hist.v_tr_hist tr
    JOIN core.tr_status trs ON trs.tr_status_id = tr.tr_status_id
    JOIN core.tr_model trm ON trm.tr_model_id = tr.tr_model_id
    JOIN (SELECT
          e.name_short || '(' || tt.short_name || ')' || '|' || row_number()
          OVER (
            ORDER BY name_short, tt.short_name ) :: TEXT AS name_short,
          entity_id,
          tt.short_name
        FROM core.entity e
          JOIN core.depo d ON depo_id = e.entity_id
          JOIN core.tr_type tt ON tt.tr_type_id = d.tr_type_id) e ON e.entity_id = tr.depo_id
  WHERE 1=1
        AND sys_period @> f_dt_DT
        AND(trm.tr_capacity_id = f_tr_capacity_id OR f_tr_capacity_id IS NULL)
        AND (tr.tr_status_id = f_tr_status_id OR f_tr_status_id IS NULL)
        AND (tr.depo_id = f_depo_id OR f_depo_id IS NULL)
        AND (tr.tr_type_id = f_tr_type_id OR f_tr_type_id IS NULL)
  GROUP BY GROUPING SETS ((), (tr.tr_status_id, trs.name, tr.depo_id, e.name_short), (tr.depo_id, e.name_short))
  HAVING GROUPING(tr.tr_status_id, trs.name, tr.depo_id, e.name_short) IN (0, 12);
END;
$BODY$