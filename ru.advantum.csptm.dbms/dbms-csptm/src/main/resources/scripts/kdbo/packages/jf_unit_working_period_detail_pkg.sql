CREATE OR REPLACE FUNCTION kdbo.jf_unit_working_period_detail_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_tr_id		bigint 		:= jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
  l_begin_dt 	TIMESTAMP 	:= jofl.jofl_pkg$extract_date(p_attr, 'eq_min_start', TRUE);
  l_end_dt 		TIMESTAMP 	:= jofl.jofl_pkg$extract_date(p_attr, 'eq_max_start', TRUE);
  l_state		int 		:= jofl.jofl_pkg$extract_number(p_attr, 'is_worked', true);
begin
 open p_rows for
    with a as  (
        select incident_id event_id, unit_id, tr_id,
        time_start event_time_start,
        lead(time_start) OVER (PARTITION BY unit_id, tr_id ORDER BY incident_id) as event_time_finish,
        case 
          when incident_type_id = kdbo.jf_unit_working_period_pkg$get_diag_ok()
            then 1
        	else 0
        end is_worked
      FROM ttb.incident
      WHERE incident_type_id = any (kdbo.jf_unit_working_period_pkg$get_diag_event_type_array())
        and time_start between l_begin_dt and l_end_dt
        and tr_id = l_tr_id
    ),
    b as (SELECT row_number()
                 OVER (
                   ORDER BY unit_id, tr_id, event_time_start, event_id) - row_number()
                 OVER (PARTITION BY unit_id, tr_id, is_worked
                   ORDER BY unit_id, tr_id, event_time_start, event_id) gr_id,
                event_time_start,
                event_time_finish,
                is_worked,
                unit_id,
                tr_id
          FROM a
      ),
    c as (SELECT min(event_time_start) as event_time_start,
           max(event_time_finish) as event_time_finish,
           is_worked as is_worked,
           unit_id as unit_id,
           tr_id as tr_id,
           case is_worked
                when 0 then 'Нерабочее'
                else 'Рабочее'
           end as state
    from b
    where 1 = 1
    group by gr_id, unit_id, tr_id, is_worked
    )
    select
        c.event_time_start,
        c.event_time_finish,
        c.unit_id,
        c.tr_id,
        c.state,
        e.serial_num,
        tr.licence,
        em.name model_name,
        tr.garage_num,
        et.name_short eq_name_short,
        ed.name_full depo_name,
        ter.code ter_code,
        ent.name_short ter_name
    from c
    JOIN core.equipment e ON e.equipment_id = c.unit_id
    JOIN core.depo d ON d.depo_id = e.depo_id
    JOIN core.entity ed ON ed.entity_id = d.depo_id
    JOIN core.equipment_model em ON em.equipment_model_id = e.equipment_model_id
    JOIN core.equipment_type et ON et.equipment_type_id = em.equipment_type_id
    JOIN core.tr tr ON tr.tr_id = c.tr_id and tr.tr_id = l_tr_id
    join core.depo2territory d2t on d.depo_id = d2t.depo_id
    join core.territory ter on ter.territory_id = d2t.territory_id 
    						and tr.territory_id = ter.territory_id
    join core.entity ent on ent.entity_id = ter.territory_id
    where c.is_worked = l_state or l_state is null
    order by c.event_time_start desc
    ;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;