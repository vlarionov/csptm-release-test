CREATE OR REPLACE FUNCTION kdbo.tel_conf_pkg$invalid_pkg_number_conf()
  RETURNS TABLE(
    unit_id                    BIGINT,
    invalid_packets_to_disable INTEGER,
    valid_packets_to_enable    INTEGER
  )
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT *
  FROM (
         SELECT
           e.equipment_id unit_id,
           coalesce(
               kdbo.jf_diagnostic_ref_pkg$get_bsnt_invalid_packet(e.equipment_model_id),
               kdbo.jf_diagnostic_ref_pkg$get_bsnr_invalid_packet(e.equipment_model_id)
           ) :: INTEGER   invalid_packets_to_disable,
           coalesce(
               kdbo.jf_diagnostic_ref_pkg$get_bsnt_valid_packet(e.equipment_model_id),
               kdbo.jf_diagnostic_ref_pkg$get_bsnr_valid_packet(e.equipment_model_id)
           ) :: INTEGER   valid_packets_to_enable
         FROM core.equipment e
       ) e
  WHERE e.invalid_packets_to_disable NOTNULL AND e.valid_packets_to_enable NOTNULL;
END;
$$;
CREATE OR REPLACE FUNCTION kdbo.tel_conf_pkg$default_invalid_pkg_number_conf()
  RETURNS TABLE(
    invalid_packets_to_disable INTEGER,
    valid_packets_to_enable    INTEGER
  )
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT 2, 5;
END;
$$;


CREATE OR REPLACE FUNCTION kdbo.tel_conf_pkg$invalid_pkg_rate_conf()
  RETURNS TABLE(
    unit_id                BIGINT,
    invalid_packets_number INTEGER,
    sampling_size          INTEGER
  )
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT *
  FROM (
         SELECT
           e.equipment_id unit_id,
           coalesce(
               kdbo.jf_diagnostic_ref_pkg$get_bsnt_invalid_packet_in_seq(e.equipment_model_id),
               kdbo.jf_diagnostic_ref_pkg$get_bsnr_invalid_packet_in_seq(e.equipment_model_id)
           ) :: INTEGER   invalid_packets_number,
           coalesce(
               kdbo.jf_diagnostic_ref_pkg$get_bsnt_packet_seq_size(e.equipment_model_id),
               kdbo.jf_diagnostic_ref_pkg$get_bsnr_packet_seq_size(e.equipment_model_id)
           ) :: INTEGER   sampling_size
         FROM core.equipment e
       ) e
  WHERE e.invalid_packets_number NOTNULL AND e.sampling_size NOTNULL;
END;
$$;
CREATE OR REPLACE FUNCTION kdbo.tel_conf_pkg$default_invalid_pkg_rate_conf()
  RETURNS TABLE(
    invalid_packets_number INTEGER,
    sampling_size          INTEGER
  )
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT 4, 10;
END;
$$;

CREATE OR REPLACE FUNCTION kdbo.tel_conf_pkg$pkg_time_rate_conf()
  RETURNS TABLE(
    unit_id BIGINT,
    package_waiting_duration INTEGER
  )
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT *
  FROM (
         SELECT
           e.equipment_id unit_id,
           coalesce(
               kdbo.jf_diagnostic_ref_pkg$get_bsnt_time_wait_packet(e.equipment_model_id),
               kdbo.jf_diagnostic_ref_pkg$get_bsnr_time_wait_packet(e.equipment_model_id)
           ) :: INTEGER   package_waiting_duration
         FROM core.equipment e
       ) e
  WHERE e.package_waiting_duration NOTNULL;
END;
$$;
CREATE OR REPLACE FUNCTION kdbo.tel_conf_pkg$default_pkg_time_rate_conf()
  RETURNS TABLE(
    package_waiting_duration INTEGER
  )
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT 10;
END;
$$;