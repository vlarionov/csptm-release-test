﻿-- Function: kdbo."jf_tr_check_equipment_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION kdbo."jf_tr_check_equipment_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION kdbo."jf_tr_check_equipment_pkg$attr_to_rowtype"(p_attr text)
  RETURNS kdbo.tr_check_equipment AS
$BODY$ 
declare 
   l_r kdbo.tr_check_equipment%rowtype; 
begin 
   l_r.tr_check_equipment_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_check_equipment_id', true); 
   l_r.check_result_id := jofl.jofl_pkg$extract_varchar(p_attr, 'check_result_id', true); 
   l_r.tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true); 
   l_r.dt_create := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'dt_create', true), now());
   l_r.check_description := jofl.jofl_pkg$extract_varchar(p_attr, 'check_description', true);
   l_r.is_auto := jofl.jofl_pkg$extract_boolean(p_attr, 'is_auto', true);

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION kdbo."jf_tr_check_equipment_pkg$attr_to_rowtype"(text)
  OWNER TO adv;

--***********************************************************************************
-- Function: kdbo."jf_tr_check_equipment_pkg$of_delete"(numeric, text)

-- DROP FUNCTION kdbo."jf_tr_check_equipment_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION kdbo."jf_tr_check_equipment_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r kdbo.tr_check_equipment%rowtype;
begin 
   l_r := kdbo.jf_tr_check_equipment_pkg$attr_to_rowtype(p_attr);

   delete from  kdbo.tr_check_equipment where  tr_check_equipment_id = l_r.tr_check_equipment_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION kdbo."jf_tr_check_equipment_pkg$of_delete"(numeric, text)
  OWNER TO adv;

--***********************************************************************************
  -- Function: kdbo."jf_tr_check_equipment_pkg$of_insert"(numeric, text)

-- DROP FUNCTION kdbo."jf_tr_check_equipment_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION kdbo."jf_tr_check_equipment_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r kdbo.tr_check_equipment%rowtype;
begin 
   l_r := kdbo.jf_tr_check_equipment_pkg$attr_to_rowtype(p_attr);
   l_r.tr_check_equipment_id:=nextval('kdbo.tr_check_equipment_tr_check_equipment_id_seq'::regclass);
   insert into kdbo.tr_check_equipment select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION kdbo."jf_tr_check_equipment_pkg$of_insert"(numeric, text)
  OWNER TO adv;

--***********************************************************************************
CREATE OR REPLACE FUNCTION kdbo.jf_tr_check_equipment_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_depo_id	 core.depo.depo_id%type 		:= jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', true);
  l_check_result_id	 core.depo.depo_id %type 		:= jofl.jofl_pkg$extract_number(p_attr, 'f_check_result_id', true);
  l_b_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_begin_DT', true);
  l_e_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_end_DT', true);
  l_is_auto		smallint					:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_is_auto_INPLACE_K', true), -1);
  l_depo_list    bigint[]  := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_depo_id', true);
begin 
 open p_rows for
  with t_check as (
     select
       cheq.tr_check_equipment_id,
       array_agg(cheq.tr_check_equipment_id) over (partition by cheq.tr_id)::text as arr_tr_check_equipment_id,
       cheq.check_result_id,
       cheq.tr_id,
       cheq.dt_create,
       cheq.check_description,
       cheq.is_auto,
       not res.is_critical as is_passed,
       res.check_result_name,
       res.is_critical,
       tr.garage_num,
       depo.name_short as depo_name_short,
       cheq.dt_create = max(cheq.dt_create) over (partition by cheq.tr_id) as is_max_dt
      -- count(1) over (partition by cheq.tr_id) as cnt
       , '[' ||
          json_build_object('CAPTION', (count(1) over (partition by cheq.tr_id)::text)
       , 'JUMP',json_build_object('METHOD', 'kdbo.tr_check_equipment_detail'
       , 'ATTR' 
       , '{}' )) || ']' as cnt
     from kdbo.tr_check_equipment cheq
       join kdbo.check_result res on res.check_result_id = cheq.check_result_id
       join core.tr tr on tr.tr_id = cheq.tr_id
       join core.entity depo on depo.entity_id = tr.depo_id
     where
       cheq.dt_create::date between l_b_dt and l_e_dt + interval '1 day'      
       and (tr.depo_id = any(l_depo_list) or array_length(l_depo_list,1) is null)
       and (l_check_result_id is null or cheq.check_result_id = l_check_result_id)
       and (l_is_auto=-1 or cheq.is_auto::int= l_is_auto)
       )
  select t.tr_check_equipment_id,
         replace(replace(t.arr_tr_check_equipment_id, '}', ''), '{', '') as arr_tr_check_equipment_id,
         t.check_result_id,
         t.tr_id,
         t.dt_create,
         t.check_description,
         t.is_auto,
         t.is_passed,
         t.check_result_name,
         t.is_critical,
         t.garage_num,
         t.depo_name_short,
         t.cnt
  from t_check t
  where t.is_max_dt
  order by t.dt_create desc
      ;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

--***********************************************************************************
  -- Function: kdbo."jf_tr_check_equipment_pkg$of_update"(numeric, text)

-- DROP FUNCTION kdbo."jf_tr_check_equipment_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION kdbo."jf_tr_check_equipment_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r kdbo.tr_check_equipment%rowtype;
begin 
   /*l_r := kdbo.jf_tr_check_equipment_pkg$attr_to_rowtype(p_attr);

   update kdbo.tr_check_equipment set
          check_result_id = l_r.check_result_id, 
          tr_id = l_r.tr_id, 
          dt_create = l_r.dt_create, 
          check_description = l_r.check_description
   where 
          tr_check_equipment_id = l_r.tr_check_equipment_id;
*/
   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION kdbo."jf_tr_check_equipment_pkg$of_update"(numeric, text)
  OWNER TO adv;

--***********************************************************************************
--список доступных для ТС ББ по схеме подключения (исключая установленные)
CREATE OR REPLACE FUNCTION kdbo."jf_tr_check_equipment_pkg$get_available_type_unit"(
  p_id_tr numeric)
  RETURNS bigint[] AS
$BODY$
declare
begin

  return array(
      with ut as (
        select inst_unit.equipment_type_id, row_number() over(partition by inst_unit.equipment_type_id) rn
        from core.tr
          join core.tr_schema_install inst on inst.tr_schema_install_id = tr.tr_schema_install_id
          join core.tr_schema_install_unit inst_unit on inst_unit.tr_schema_install_id =inst.tr_schema_install_id
        where tr.tr_id = p_id_tr
              and inst.is_active
        EXCEPT
        select em.equipment_type_id, row_number() over(partition by em.equipment_type_id)  rn
        from core.unit2tr u2tr
          join core.equipment et on et.equipment_id = u2tr.unit_id
          join core.equipment_model em on em.equipment_model_id = et.equipment_model_id
        where u2tr.tr_id = p_id_tr
      )
      select equipment_type_id from ut group by equipment_type_id
    );
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

ALTER FUNCTION kdbo."jf_tr_check_equipment_pkg$get_available_type_unit"(numeric)
OWNER TO adv;

--***********************************************************************************
--список доступных для ТС датчиков и др. оборудования по схеме подключения (исключая установленные)
CREATE OR REPLACE FUNCTION kdbo."jf_tr_check_equipment_pkg$get_available_type_sensor"(
  p_id_tr numeric,
  p_is_required smallint default -1)
  RETURNS bigint[] AS
$BODY$
declare
begin

  return array(
      with sens as
      (
        select t.tr_id, t.sensor_id as equipment_id from core.sensor2tr t
        union all select t.tr_id, t.equipment_id from core.equipment2tr t
      )
        , available as
      (
        select tt.equipment_type_sensor_id, row_number() over(partition by tt.equipment_type_sensor_id) rn
        from
          core.tr
          join core.tr_schema_install inst on inst.tr_schema_install_id = tr.tr_schema_install_id
          join core.tr_schema_install_unit inst_unit on inst_unit.tr_schema_install_id =inst.tr_schema_install_id
          join core.tr_schema_install_detail inst_det on inst_det.tr_schema_install_unit_id= inst_unit.tr_schema_install_unit_id
          join core.sensor2unit_model tt on tt.sensor2unit_model_id = inst_det.sensor2unit_model_id
        where tr.tr_id = p_id_tr
              and inst.is_active
              and (p_is_required = -1 or inst_det.is_required::int = p_is_required)
        EXCEPT
        select em.equipment_type_id, row_number() over(partition by em.equipment_type_id)  rn
        from sens
          join core.equipment et on et.equipment_id = sens.equipment_id
          join core.equipment_model em on em.equipment_model_id = et.equipment_model_id
        where sens.tr_id = p_id_tr)

      select equipment_type_sensor_id from  available group by equipment_type_sensor_id
  );
  
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

ALTER FUNCTION kdbo."jf_tr_check_equipment_pkg$get_available_type_sensor"(numeric, smallint)
OWNER TO adv;

--тут добавить процедуру провести проверку (параметр ТС и автоматическая/ручная)
--+команда которая вызывает процедуру с ручной
--***********************************************************************************
--Проверка комплектации
CREATE OR REPLACE FUNCTION kdbo."jf_tr_check_equipment_pkg$run_check_equipment"(
  p_id_account numeric,
  p_id_tr numeric,
  p_is_auto boolean default true)
  RETURNS text AS
$BODY$
declare
  l_cnt smallint;
  l_r kdbo.tr_check_equipment%rowtype;
  l_sim_card_id core.sim_card.sim_card_id%type;
begin
  l_r.tr_id := p_id_tr;
  l_r.is_auto := p_is_auto;

  select  into l_cnt count(1) from core.tr where tr.tr_id = p_id_tr and tr.tr_schema_install_id is not null;
  if l_cnt=0 then
    l_r.check_result_id:= kdbo.jf_check_result_pkg$cn_result_tr_without_schema_install();
    return kdbo.jf_tr_check_equipment_pkg$of_insert(p_id_account, row_to_json(l_r)::text);
  end if;

  ----критическая ошибка при неполной комплектации обязательным оборудованием
  select into  l_r.check_description  string_agg(t.name_short, ', ' )  from core.equipment_type t
    join unnest(kdbo.jf_tr_check_equipment_pkg$get_available_type_unit(l_r.tr_id)||kdbo.jf_tr_check_equipment_pkg$get_available_type_sensor(l_r.tr_id, 1::smallint)) u on t.equipment_type_id = u;

  if l_r.check_description is not null then
    l_r.check_result_id:= kdbo.jf_check_result_pkg$cn_result_tr_incomplect_critical();
    return kdbo.jf_tr_check_equipment_pkg$of_insert(p_id_account, row_to_json(l_r)::text);
  end if;


  ----критическая ошибка при отсутсвии СППД у блока
  select into  l_r.check_description  string_agg(et.equipment_type_name, ', ' )
  from core.unit2tr u2tr
    join core.unit on unit.unit_id =u2tr.unit_id
    join core.v_equipment et on et.equipment_id= unit.unit_id
  where u2tr.tr_id= p_id_tr and hub_id is null
  group by et.equipment_type_name;

  if l_r.check_description  is not null then
    l_r.check_result_id:= kdbo.jf_check_result_pkg$cn_result_unit_without_link_hub();
    return kdbo.jf_tr_check_equipment_pkg$of_insert(p_id_account, row_to_json(l_r)::text);
  end if;

  --БНСТ не оборудован sim-картой
  begin
    select into l_sim_card_id (select sim_card_id from core.sim_card2unit c2u where c2u.equipment_id = bnst.unit_bnst_id)
    from core.unit2tr u2tr
      join core.unit_bnst bnst on bnst.unit_bnst_id = u2tr.unit_id
    where u2tr.tr_id = p_id_tr;

    if l_sim_card_id is null then
      l_r.check_result_id:= kdbo.jf_check_result_pkg$cn_result_bnst_without_simcard();
      return kdbo.jf_tr_check_equipment_pkg$of_insert(p_id_account, row_to_json(l_r)::text);
    end if;
    exception
    when NO_DATA_FOUND  then
      null;
  end;

  --модем не оборудован sim-картой
  begin
      select into l_sim_card_id modem.sim_card_id
      from core.equipment2tr eq2tr
        join core.modem on modem.modem_id =eq2tr.equipment_id
      where eq2tr.tr_id = p_id_tr;
    if l_sim_card_id is null then
       l_r.check_result_id:= kdbo.jf_check_result_pkg$cn_result_modem_without_simcard();
      return kdbo.jf_tr_check_equipment_pkg$of_insert(p_id_account, row_to_json(l_r)::text);
    end if;
  exception
    when NO_DATA_FOUND  then
        null;
  end;

  ----обычная ошибка при неполной комплектации оборудованием
  select into  l_r.check_description  string_agg(t.name_short, ',' )  from core.equipment_type t
    join unnest(kdbo.jf_tr_check_equipment_pkg$get_available_type_sensor(l_r.tr_id)) u on t.equipment_type_id = u;

  if l_r.check_description  is not null then
    l_r.check_result_id:= kdbo.jf_check_result_pkg$cn_result_tr_incomplect();
    return kdbo.jf_tr_check_equipment_pkg$of_insert(p_id_account, row_to_json(l_r)::text);
  end if;


  l_r.check_result_id:= kdbo.jf_check_result_pkg$cn_result_successful();

  return kdbo.jf_tr_check_equipment_pkg$of_insert(p_id_account, row_to_json(l_r)::text);

end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION kdbo."jf_tr_check_equipment_pkg$run_check_equipment"(numeric, numeric, boolean)
OWNER TO adv;
