CREATE OR REPLACE FUNCTION kdbo.jf_fuel_waste_report_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
    larr_f_depo_id  bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', true);
    larr_f_tr_id    bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_tr_id', true);
    l_f_begin_dt 	timestamp := jofl.jofl_pkg$extract_date(p_attr, 'f_begin_DT', true);
    l_f_end_dt   	timestamp := jofl.jofl_pkg$extract_date(p_attr, 'f_end_DT', true);
begin
    open p_rows for
    with trs as (
        select
            t.depo_id,
            t.tr_id,
            t.garage_num,
            t.licence,
            e.name_full depo_name
        from core.tr t
            join core.depo d on t.depo_id = d.depo_id
            join core.entity e on d.depo_id = e.entity_id
        where (t.tr_id = any (larr_f_tr_id) or array_length(larr_f_tr_id, 1) is null)
          and (t.depo_id = any (larr_f_depo_id) or array_length(larr_f_depo_id, 1) is null)
    )
    ,ttv_oper as (
        select out.tr_id
        	  ,to_char(l_f_begin_dt + interval '3 hours', 'dd.mm.yyyy') as order_list_name
          from ttb.tt_variant ttv
          join ttb.tt_out out on out.tt_variant_id = ttv.tt_variant_id
                             and ttv.action_period && tsrange(l_f_begin_dt, l_f_end_dt) 
                             and ttv.parent_tt_variant_id is not null
                             and out.tr_id is not null
    ) 
    ,fuel as (
        select *
        from (
                 select
                     t.tr_id,
                     row_number()
                     over (
                         partition by t.tr_id
                         order by event_time ) rn,
                     first_value(fsd.event_time)
                     over (
                         partition by t.tr_id
                         order by event_time ) first_event_time,
                     first_value(fsd.event_time)
                     over (
                         partition by t.tr_id
                         order by event_time desc ) last_event_time,
                     first_value(fsd.level_l)
                     over (
                         partition by t.tr_id
                         order by event_time ) first_level_l,
                     first_value(fsd.level_l)
                     over (
                         partition by t.tr_id
                         order by event_time desc ) last_level_l,
                     min(fsd.level_l)
                     over (
                         partition by t.tr_id ) min_level_l,
                     max(fsd.level_l)
                     over (
                         partition by t.tr_id ) max_level_l
                 from trs t
                     join snsr.fuel_sensor_data fsd on t.tr_id = fsd.tr_id
                                                       and fsd.event_time between l_f_begin_dt and l_f_end_dt
                                                       and fsd.switched_on
             ) t
        where t.rn = 1
    )
    select
        t.depo_id,
        t.tr_id,
        f.first_event_time,
        f.last_event_time,
        t.depo_name,
        t.licence,
        t.garage_num,
        f.first_level_l,
        0 refuel_level_l,
        0 wasted_level_l,
        f.last_level_l,
        o.order_list_name
    from trs t
        join fuel f on t.tr_id = f.tr_id
        left join ttv_oper o on o.tr_id = f.tr_id;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;