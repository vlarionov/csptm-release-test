CREATE OR REPLACE FUNCTION kdbo.jf_diagnostic_list_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
DECLARE
  F_BEGIN_DT                TIMESTAMP :=  jofl.jofl_pkg$extract_date(p_attr, 'F_BEGIN_DT', TRUE);
  F_END_DT                  TIMESTAMP :=  jofl.jofl_pkg$extract_date(p_attr, 'F_END_DT', TRUE);
  f_list_depo_id            BIGINT [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', TRUE);
  f_equipment_type_id       BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_type_id', TRUE);
  f_equipment_model_id      BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_model_id', TRUE);
  f_tr_id                   BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_id', TRUE);
  f_is_diagnostic_INPLACE_K BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_is_diagnostic_INPLACE_K', TRUE);
  f_is_required_diagnostic  INT := jofl.jofl_pkg$extract_number(p_attr, 'f_is_required_diagnostic_INPLACE_K', TRUE);
BEGIN
  OPEN p_rows FOR
  with t_dg as (
        SELECT
          dp.diagnostic_period_id,
          array_agg(dp.diagnostic_period_id) over (partition by e.equipment_id)::text as arr_diagnostic_period_id,
          ed.name_short,
          ed.name_full,
          dp.start_date dt_b,
          dp.end_date   dt_e,
          et.name_short etype_name,
          em.name       emodel_name,
          e.serial_num,
          tr.garage_num,
          e.equipment_id,
          rd.tr_id is not null as is_required_diagnostic
          --count (1) over (partition by e.equipment_id) as cnt,
          , '[' ||
             json_build_object('CAPTION', (count (1) over (partition by e.equipment_id)::text)
          , 'JUMP',json_build_object('METHOD', 'kdbo.diagnostic_list_detail'
          , 'ATTR' 
          , '{}' )) || ']' as cnt,   
          dp.start_date = max(dp.start_date) over (partition by e.equipment_id) as is_max_dt,
          case
            when rd.tr_id is not null
              then 'P{A},D{OF_STOP_DIAGNOSTIC}'
            else
              null
          end "ROW$POLICY"
        FROM kdbo.diagnostic_period dp
          JOIN core.equipment e ON e.equipment_id = dp.equipment_id
          JOIN core.depo d ON d.depo_id = e.depo_id
          JOIN core.entity ed ON ed.entity_id = d.depo_id
          JOIN core.equipment_model em ON em.equipment_model_id = e.equipment_model_id
          JOIN core.equipment_type et ON et.equipment_type_id = em.equipment_type_id
          JOIN core.v_equipment2tr4all etr ON etr.equipment_id = e.equipment_id
          JOIN core.tr tr ON tr.tr_id = etr.tr_id
          left join kdbo.required_diagnostic rd on rd.tr_id = tr.tr_id
        WHERE dp.start_date BETWEEN F_BEGIN_DT AND F_END_DT
              AND (e.depo_id = ANY (f_list_depo_id) OR array_length(f_list_depo_id, 1) IS NULL)
              AND (em.equipment_type_id = f_equipment_type_id OR f_equipment_type_id IS NULL)
              AND (em.equipment_model_id = f_equipment_model_id OR f_equipment_model_id IS NULL)
              AND (tr.tr_id = f_tr_id OR f_tr_id IS NULL)
              AND (f_is_diagnostic_INPLACE_K = -1 OR (f_is_diagnostic_INPLACE_K = 1 AND dp.end_date IS NULL) OR
                   (f_is_diagnostic_INPLACE_K = 0 AND dp.end_date IS NOT NULL))
              AND (f_is_required_diagnostic=-1 OR  rd.tr_id IS NOT NULL = f_is_required_diagnostic::BOOLEAN  )
        )
   select t_dg.diagnostic_period_id,
          replace(replace(t_dg.arr_diagnostic_period_id, '}', ''), '{', '') as arr_diagnostic_period_id,
          t_dg.name_short,
          t_dg.name_full,
          t_dg.dt_b,
          t_dg.dt_e,
          t_dg.etype_name,
          t_dg.emodel_name,
          t_dg.serial_num,
          t_dg.garage_num,
          t_dg.equipment_id,
          t_dg.is_required_diagnostic,
          t_dg.cnt,  
          t_dg."ROW$POLICY" as "ROW$POLICY"
  from t_dg
  where t_dg.is_max_dt
  order by t_dg.dt_b desc
  		,  t_dg.dt_e desc
        ;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

----------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION kdbo.jf_diagnostic_list_pkg$of_insert(
  p_id_account NUMERIC,
  p_attr       TEXT)
  RETURNS TEXT AS
$BODY$
DECLARE
  l_r kdbo.diagnostic_period%ROWTYPE;
BEGIN
  l_r.equipment_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', FALSE);
  l_r.start_date := now();


  INSERT INTO kdbo.diagnostic_period (start_date, end_date, equipment_id)
  VALUES (l_r.start_date, NULL, l_r.equipment_id);

  RETURN NULL;
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION kdbo.jf_diagnostic_list_pkg$of_insert(numeric, text)
  OWNER TO adv;


CREATE OR REPLACE FUNCTION kdbo.jf_diagnostic_list_pkg$of_delete(
  p_id_account NUMERIC,
  p_attr       TEXT)
  RETURNS TEXT AS
$BODY$
DECLARE
  l_r kdbo.diagnostic_period%ROWTYPE;
BEGIN
  l_r.diagnostic_period_id := jofl.jofl_pkg$extract_number(p_attr, 'diagnostic_period_id', FALSE);
  l_r.equipment_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', FALSE);
  l_r.end_date := now();


  UPDATE kdbo.diagnostic_period
  set end_date = l_r.end_date
  where diagnostic_period_id = l_r.diagnostic_period_id;

  RETURN NULL;
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION kdbo.jf_diagnostic_list_pkg$of_delete(numeric, text)
  OWNER TO adv;


CREATE OR REPLACE FUNCTION kdbo.jf_diagnostic_list_pkg$of_start_diagnostic(
  p_id_account NUMERIC,
  p_attr       TEXT)
  RETURNS TEXT AS
$BODY$
DECLARE
  l_r kdbo.diagnostic_period%ROWTYPE;
  F_equipment_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'F_equipment_id', true);
BEGIN

  INSERT INTO kdbo.required_diagnostic (tr_id)
    SELECT tr_id
    FROM core.unit2tr utr
    WHERE utr.unit_id = F_equipment_id
  ;

  RETURN NULL ;
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION kdbo.jf_diagnostic_list_pkg$of_start_diagnostic(numeric, text)
  OWNER TO adv;


CREATE OR REPLACE FUNCTION kdbo.jf_diagnostic_list_pkg$of_stop_diagnostic(
  p_id_account NUMERIC,
  p_attr       TEXT)
  RETURNS TEXT AS
$BODY$
DECLARE
  l_r kdbo.diagnostic_period%ROWTYPE;
  l_equipment_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
  l_diagnostic_period_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'diagnostic_period_id', true);

BEGIN

  delete from kdbo.required_diagnostic rd where rd.tr_id in (SELECT tr_id
    FROM core.unit2tr utr
    WHERE utr.unit_id = l_equipment_id);

  RETURN NULL ;
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION kdbo.jf_diagnostic_list_pkg$of_stop_diagnostic(numeric, text)
  OWNER TO adv;
