CREATE OR REPLACE FUNCTION kdbo.diagnostic_period_pkg$find_active()
  RETURNS TABLE(equipment_id BIGINT, start_date TIMESTAMP)
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT
    dp.equipment_id,
    max(dp.start_date)
  FROM kdbo.diagnostic_period dp
  WHERE dp.end_date IS NULL
  GROUP BY dp.equipment_id;
END;
$$;

CREATE OR REPLACE FUNCTION kdbo.diagnostic_period_pkg$start(p_equipment_id BIGINT, p_start_date TIMESTAMP)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE
  l_tr_id             core.tr.tr_id%TYPE;
  l_tr_equipments_ids BIGINT [];
BEGIN
  SELECT u2t.tr_id
  INTO l_tr_id
  FROM core.unit2tr u2t
  WHERE u2t.unit_id = p_equipment_id AND u2t.sys_period @> now();

  SELECT array_agg(u2t.unit_id)
  INTO l_tr_equipments_ids
  FROM core.unit2tr u2t
  WHERE u2t.tr_id = l_tr_id AND u2t.sys_period @> now();

  IF NOT exists(
      SELECT 1
      FROM kdbo.diagnostic_period dp
      WHERE dp.equipment_id = ANY (l_tr_equipments_ids)
            AND (dp.end_date IS NULL OR p_start_date <= dp.end_date)
            AND (p_start_date >= dp.start_date)
  )
  THEN
    PERFORM kdbo.jf_tr_check_equipment_pkg$run_check_equipment(NULL, l_tr_id, TRUE);
  END IF;

  INSERT INTO kdbo.diagnostic_period (start_date, end_date, equipment_id)
  VALUES (p_start_date, NULL, p_equipment_id);
END;
$$;

CREATE OR REPLACE FUNCTION kdbo.diagnostic_period_pkg$finish(p_equipment_id BIGINT, p_end_date TIMESTAMP)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  UPDATE kdbo.diagnostic_period
  SET end_date = p_end_date
  WHERE diagnostic_period_id IN (
    SELECT dp.diagnostic_period_id
    FROM kdbo.diagnostic_period dp
    WHERE dp.equipment_id = p_equipment_id AND dp.end_date IS NULL
    ORDER BY dp.start_date DESC
    LIMIT 1
  );
END;
$$;