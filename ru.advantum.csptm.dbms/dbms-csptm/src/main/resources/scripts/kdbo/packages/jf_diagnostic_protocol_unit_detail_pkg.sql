CREATE OR REPLACE FUNCTION kdbo.jf_diagnostic_protocol_unit_detail_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_diagnostic_protocol_id_list    bigint[]  := jofl.jofl_pkg$extract_tarray(p_attr, 'arr_diagnostic_protocol_id', true);

begin

 open p_rows for
 select
	 pr.diagnostic_protocol_id,
	 pr.dt_create,
	 pr.diagnostic_result_id,
	 pr.equipment_id,
	 et.name_short as equipment_type_name_short,
	 em.name as equipment_model_name,
	 unit.unit_num,
	 eq.serial_num,
	 fw.version as firmware_version,
     hub.ip_address||':'||hub.port as ip_address,
	 dr.dgnt_result_name,
	 dr.dgnt_result_desc,
	 entity4depo.name_short as depo_name_short,
	 entity4territory.name_short as territory_name_short,
   dr.is_critical,
   eq.depo_id,
   tr.tr_id,
   tr.garage_num,
	 --
   '[' ||
   json_build_object('CAPTION',tr.garage_num :: TEXT
   , 'JUMP',json_build_object('METHOD', 'core.tr'
   , 'ATTR'
   , '{}' )) || ']' as tr_link,
   '[' ||
   json_build_object('CAPTION', eq.serial_num
   , 'JUMP',json_build_object('METHOD',
                              case et.equipment_type_id
                              	when core.jf_equipment_type_pkg$cn_type_bnsr() then 'core.unit_bnsr'
                              	when core.jf_equipment_type_pkg$cn_type_bnst() then 'core.unit_bnst'
                              	when core.jf_equipment_type_pkg$cn_type_kbtob() then 'core.unit_kbtob'
                              end
   , 'ATTR' , '{}' )) || ']'
     as unit_link
 from kdbo.diagnostic_protocol pr
	 join kdbo.diagnostic_result dr on dr.diagnostic_result_id = pr.diagnostic_result_id
	 join core.equipment eq on eq.equipment_id = pr.equipment_id
	 join core.unit unit on unit.unit_id = eq.equipment_id
	 join core.equipment_model em on em.equipment_model_id = eq.equipment_model_id
	 join core.equipment_type et on et.equipment_type_id = em.equipment_type_id
	 join core.entity entity4depo on entity4depo.entity_id = eq.depo_id
     join core.equipment2tr_h h on h.equipment_id = pr.equipment_id and pr.dt_create <@ h.sys_period
	 join core.tr tr on tr.tr_id = h.tr_id
	 left join core.entity entity4territory on entity4territory.entity_id = eq.territory_id
	 left join core.firmware fw on fw.firmware_id = eq.firmware_id
     left join core.hub hub on hub.hub_id = unit.hub_id
 where pr.diagnostic_protocol_id = any (l_diagnostic_protocol_id_list)
 order by pr.dt_create desc
  ;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;