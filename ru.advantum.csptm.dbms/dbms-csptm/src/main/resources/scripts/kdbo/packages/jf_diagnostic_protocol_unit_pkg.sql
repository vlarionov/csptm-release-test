﻿CREATE OR REPLACE FUNCTION kdbo.jf_diagnostic_protocol_unit_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_depo_id bigint;
  l_trs_ids bigint[];
  l_dt_begin   timestamp;
  l_dt_end   timestamp;
  l_equipment_type_id  smallint;
  l_equipment_model_id  integer;
  l_is_critical smallint;
  l_depo_list    bigint[]  := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_depo_id', true);

begin
  l_dt_begin := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'F_BEGIN_DT', true), now()::date);
  l_dt_end   := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'F_END_DT', true), now());
  l_depo_id  := jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', true);
  l_equipment_type_id  := jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_type_id', true);
  l_equipment_model_id := jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_model_id', true);
  l_trs_ids            := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_tr_id', true);
  l_is_critical	       := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_is_critical_INPLACE_K', true), -1);

 open p_rows for
   select
       q.diagnostic_protocol_id,
       q.dt_create,
       q.diagnostic_result_id,
       q.equipment_id,
       q.equipment_type_name_short,
       q.equipment_model_name,
       q.unit_num,
       q.serial_num,
       q.firmware_version,
       q.ip_address,
       q.dgnt_result_name,
       q.dgnt_result_desc,
       q.depo_name_short,
       q.territory_name_short,
       q.is_critical,
       q.depo_id,
       q.tr_id,
       q.garage_num,
       --
       q.tr_link,
       q.unit_link,
       q.protocol_count,
       replace(replace(q.arr_diagnostic_protocol_id, '}', ''), '{', '') as arr_diagnostic_protocol_id
   from
     (
       select
         pr.diagnostic_protocol_id,
         array_agg(pr.diagnostic_protocol_id) over(partition by pr.equipment_id)::text as arr_diagnostic_protocol_id,
         pr.dt_create,
         pr.diagnostic_result_id,
         pr.equipment_id,
         et.name_short as equipment_type_name_short,
         em.name as equipment_model_name,
         unit.unit_num,
         eq.serial_num,
         fw.version as firmware_version,
         hub.ip_address||':'||hub.port as ip_address,
         dr.dgnt_result_name,
         dr.dgnt_result_desc,
         entity4depo.name_short as depo_name_short,
         entity4territory.name_short as territory_name_short,
         dr.is_critical,
         eq.depo_id,
         tr.tr_id,
         tr.garage_num,
         --
         '[' ||
         json_build_object('CAPTION',tr.garage_num :: TEXT
         , 'JUMP',json_build_object('METHOD', 'core.tr'
         , 'ATTR'
         , '{}' )) || ']' as tr_link,
         '[' ||
         json_build_object('CAPTION', eq.serial_num
         , 'JUMP',json_build_object('METHOD',
                                    case et.equipment_type_id
                                    when core.jf_equipment_type_pkg$cn_type_bnsr() then 'core.unit_bnsr'
                                    when core.jf_equipment_type_pkg$cn_type_bnst() then 'core.unit_bnst'
                                    when core.jf_equipment_type_pkg$cn_type_kbtob() then 'core.unit_kbtob'
                                    end
         , 'ATTR' , '{}' )) || ']'
           as unit_link
        , pr.dt_create = max(pr.dt_create) over(partition by pr.equipment_id) is_max_dt
        --, count(1) over(partition by pr.equipment_id) protocol_count
        , '[' ||
         json_build_object('CAPTION', (count(1) over(partition by pr.equipment_id)::TEXT)
         , 'JUMP',json_build_object('METHOD', 'kdbo.diagnostic_protocol_unit_detail'
         , 'ATTR' 
         , '{}' )) || ']' as protocol_count
       from kdbo.diagnostic_protocol pr
         join kdbo.diagnostic_result dr on dr.diagnostic_result_id = pr.diagnostic_result_id
         join core.equipment eq on eq.equipment_id = pr.equipment_id
         join core.unit unit on unit.unit_id = eq.equipment_id
         join core.equipment_model em on em.equipment_model_id = eq.equipment_model_id
         join core.equipment_type et on et.equipment_type_id = em.equipment_type_id
         join core.entity entity4depo on entity4depo.entity_id = eq.depo_id
         join core.equipment2tr_h h on h.equipment_id = pr.equipment_id and pr.dt_create <@ h.sys_period
         join core.tr tr on tr.tr_id = h.tr_id
         left join core.entity entity4territory on entity4territory.entity_id = eq.territory_id
         left join core.firmware fw on fw.firmware_id = eq.firmware_id
         left join core.hub hub on hub.hub_id = unit.hub_id
       where et.equipment_class_id = core.jf_equipment_class_pkg$cn_block()
             and pr.dt_create between l_dt_begin and l_dt_end
             and (/*eq.depo_id = l_depo_id or*/ eq.depo_id = any(l_depo_list) or array_length(l_depo_list, 1) is null )
             and (l_equipment_type_id is null or et.equipment_type_id = l_equipment_type_id)
             and (l_equipment_model_id is null or eq.equipment_model_id = l_equipment_model_id)
             and (tr.tr_id = any(l_trs_ids) or (array_length(l_trs_ids, 1)  is null))
             and (l_is_critical =-1 or dr.is_critical::int = l_is_critical )
    ) q
  where q.is_max_dt
    and q.tr_id = any (select adm.account_data_realm_pkg$get_trs_by_tr_type_and_depo(p_id_account))
  ;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_protocol_unit_pkg$of_request"(
  IN p_id_account numeric,
  IN p_attr text)
  RETURNS boolean AS
  $BODY$
  declare
  begin
    --Запись о ремонте
    RETURN kdbo.jf_diagnostic_protocol_pkg$of_request(p_id_account => p_id_account,  p_attr => p_attr )  ;
  end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_protocol_unit_pkg$of_request"(numeric, text)
OWNER TO adv;


CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_protocol_unit_pkg$of_breakdown"(
  IN p_id_account numeric,
  IN p_attr text)
  RETURNS boolean AS
$BODY$
declare
begin
  --Запись о поломке
  RETURN kdbo.jf_diagnostic_protocol_pkg$of_breakdown(p_id_account=>p_id_account, p_attr=>p_attr);
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_protocol_unit_pkg$of_request"(numeric, text)
OWNER TO adv;
