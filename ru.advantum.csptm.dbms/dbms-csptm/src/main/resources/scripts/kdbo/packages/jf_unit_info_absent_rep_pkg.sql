create or replace function kdbo."jf_unit_info_absent_rep_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
language plpgsql
as $$
DECLARE
  l_start_dt timestamp := jofl.jofl_pkg$extract_date(p_attr, 'START_DT', true);
  l_end_dt timestamp  := jofl.jofl_pkg$extract_date(p_attr, 'END_DT', true);
  l_rep_tr_type_id bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'rep_tr_type_id', TRUE)::bigint[];
  -- l_rep_depo_id bigint := jofl.jofl_pkg$extract_number(p_attr, 'rep_depo_id', TRUE);
  l_rep_depo_id bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'rep_depo_id', TRUE)::bigint[];
  l_route bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'rep_route_muid', TRUE);
BEGIN
  if core.assigned(l_start_dt::text, l_end_dt::text) then
    open p_rows for
      select p.name depo_name, tt.short_name tr_type_name,
             r.number,
             te.timetable_entry_num,
             tr.garage_num,
             format('%s(%s)', drv.driver_last_name, drv.tab_num) driver_last_name,
             case rnd.direction_id when 1 then 'Прямое' when 2 then 'Обратное' end direction,
             rtp.name round_code,
             s.name stop_name,
             stop_fact,
             last_receive_time,
             to_char(off_duration, 'hh24:mi:ss') off_duration
      from
        (
          select session_id, session_stop, tr_id, unit_id, stop_place_muid, order_list_id, order_round_id, order_date, driver_id, round_id, max(time_fact) stop_fact,
                 time_start, time_stop, off_duration,
                 (   select max(event_time)
                     from core.traffic t
                     where t.unit_id = q.unit_id and t.tr_id = q.tr_id
                       and t.event_time >= q.order_date_utc
                       and t.event_time < q.time_stop
                 ) last_receive_time
          from
           (
          with u    as ( select u.*, time_next_sess - time_stop off_duration
                         from
                           (
                              select u.*, core.from_utc(u.time_stop)::date session_stop,
                              lead(time_start, 1, l_end_dt) over(partition by tr_id, unit_id order by time_start) time_next_sess
                              from kdbo.unit_session_log u
                              where time_stop is not null and time_stop >= l_start_dt and time_stop < l_end_dt
                           ) u
                         where time_next_sess - time_stop > INTERVAL '1 minutes' --8231
                       ),
                   olst as ( select * from tt.order_list
                             where order_date >= core.from_utc(l_start_dt)::date and order_date <= core.from_utc(l_end_dt)::date + 1 and sign_deleted = 0
                           )
              select max(orf.time_fact) over(partition by u.session_id) max_fact_time,
                     max(olst.order_date) filter(where order_date <= session_stop) over(partition by u.session_id) max_order_date,
                     max(ornd.round_id)  over(partition by u.session_id) max_round,
                     u.session_id,
                     u.time_start,
                     u.time_stop,
                     u.session_stop,
                     off_duration,
                     orf.time_fact,
                     orf.order_fact_id,
                     u.tr_id,
                     u.unit_id,
                     orf.stop_place_muid,
                     ornd.order_list_id,
                     olst.driver_id,
                     olst.order_date,
                     core.to_utc(olst.order_date) order_date_utc,
                     ornd.order_round_id,
                     ornd.round_id
              from u join olst on u.tr_id = olst.tr_id
                     join tt.order_round ornd on ornd.order_list_id = olst.order_list_id
                     join tt.order_fact orf on orf.order_round_id = ornd.order_round_id
                       and (orf.time_fact <= u.time_stop and core.from_utc(orf.time_fact) >= olst.order_date::date and orf.sign_deleted = 0)
              where olst.order_date <= u.session_stop
                and ((orf.time_fact <= u.time_stop and core.from_utc(orf.time_fact) >= olst.order_date::date and orf.sign_deleted = 0) )--or orf.order_round_id is null)
            ) q
          where (q.time_fact = coalesce(q.max_fact_time, q.time_fact))
            or  (q.max_fact_time is null and ((order_date = max_order_date) or max_order_date is null) and (round_id = max_round or max_round is null))
          group by session_id, session_stop, time_start, time_stop, off_duration, tr_id, unit_id,stop_place_muid, order_round_id, order_list_id,
                   order_date, order_date_utc, round_id, driver_id
        ) qq -- 1414481761
          join core.tr on qq.tr_id = tr.tr_id
          join core.tr_type tt on tr.tr_type_id = tt.tr_type_id
          join core.depo d on d.depo_id = tr.depo_id
          join gis.parks p on p.muid = d.park_muid
          left join  tt.round rnd on rnd.round_id = qq.round_id
          left join tt.timetable_entry te on rnd.timetable_entry_id = te.timetable_entry_id and te.sign_deleted = 0
          left join gis.route_variants rv on te.route_variant_muid = rv.muid
          left join gis.routes r on rv.route_muid = r.muid
          left join tt.driver drv on drv.driver_id = qq.driver_id
          left join gis.stop_places stp on stp.muid = qq.stop_place_muid
          left join gis.stops s on s.muid = stp.stop_muid
          join (select orv.*, greatest(orv.time_plan_end, time_fact_end) round_end from  asd.oper_round_visit orv) orv on qq.order_round_id = orv.order_round_id
          join gis.route_trajectories rt on rt.muid = rnd.route_trajectory_muid
          join gis.route_rounds rr on rr.muid = rt.route_round_muid
          join gis.ref_route_round_types rtp on rtp.muid = rr.route_round_type_muid
      where ((qq.time_stop between orv.time_plan_begin and orv.round_end)
               or (qq.time_start between orv.time_plan_begin and orv.round_end and qq.time_stop <= orv.round_end))
        -- and tr.depo_id = l_rep_depo_id
        and ((cardinality(l_rep_tr_type_id) = 0) or (tr.tr_type_id = any(l_rep_tr_type_id)))		
        and ((cardinality(l_rep_depo_id) = 0) or (tr.depo_id = any(l_rep_depo_id)))		
        and ((cardinality(l_route) = 0) or (rv.route_muid = any(l_route)))
      order by stop_fact;
  ELSE
    open p_rows for
      select null depo_name,
             null tr_type_name,
             null number,
             null timetable_entry_num,
             null garage_num,
             null driver_last_name,
             null direction,
             null stop_name,
             null stop_fact,
             null last_receive_time,
             null off_duration,
             null att
      where false;
  end if;
end;
$$
