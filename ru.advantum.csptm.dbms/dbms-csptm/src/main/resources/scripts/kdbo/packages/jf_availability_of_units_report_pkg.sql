-- DROP FUNCTION kdbo.jf_availability_of_units_report_pkg$of_rows(p_id_account NUMERIC, p_attr       TEXT);

CREATE OR REPLACE FUNCTION kdbo.jf_availability_of_units_report_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                                            p_attr       TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $BODY$
DECLARE

  f_list_depo_id             BIGINT [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', TRUE);
  f_dt_DT                    TIMESTAMP WITH TIME ZONE :=  jofl.jofl_pkg$extract_date(p_attr, 'f_dt_DT',
                                                                                     TRUE) :: TIMESTAMP WITH TIME ZONE;

  f_list_equipment_status_id BIGINT [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_equipment_status_id', TRUE);
  f_list_equipment_type_id   BIGINT [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_equipment_type_id', TRUE);

  /*
1) Нужно добавить фильтры:
Дата - выводить отчет на заданную дату. По умолчанию текущая;
ТП - список транспортных предприятий. Множественный выбор;
Типу БО - список типов БО (множественный выбор);
Статус БО - список статусов БО (множественный выбор).
2) Нужна итоговая строка по каждому столбцу

3) Сейчас в отчет не выводится информация по датчикам. В отчет должна выводиться информация по блокам, датчикам и другому оборудованию.  */
BEGIN
  -- RAISE EXCEPTION USING MESSAGE = p_attr;

  OPEN p_rows FOR
  WITH eq AS (
      SELECT
        e.equipment_status_id,
        et.equipment_type_id,
        et.name_short et_name,
        es.name       es_name,
        de.name_short depo_name,
        CASE coalesce(utr.sys_period @> f_dt_DT, str.tr_id IS NOT NULL OR e2t.tr_id IS NOT NULL, FALSE)
        WHEN TRUE
          THEN 1
        END           on_tr
      FROM core.equipment e
        JOIN core.entity de ON de.entity_id = e.depo_id
        JOIN core.equipment_model em ON em.equipment_model_id = e.equipment_model_id
        JOIN core.equipment_type et ON et.equipment_type_id = em.equipment_type_id
        JOIN core.equipment_status es ON es.equipment_status_id = e.equipment_status_id
        /*это блоки*/
        --         left JOIN core.unit u ON u.unit_id = e.equipment_id
        LEFT JOIN core.unit2tr utr ON utr.unit_id = /*u.unit_id */e.equipment_id AND utr.sys_period @> f_dt_DT
        /*это датчики*/
        LEFT JOIN core.sensor2tr str ON str.sensor_id = e.equipment_id
        /*остальное*/
        LEFT JOIN core.equipment2tr e2t ON e2t.equipment_id = e.equipment_id
      WHERE 1 = 1
            AND (e.depo_id = ANY (f_list_depo_id))
            AND
            (em.equipment_type_id = ANY (f_list_equipment_type_id) OR array_length(f_list_equipment_type_id, 1) IS NULL)
            AND (e.equipment_status_id = ANY (f_list_equipment_status_id) OR
                 array_length(f_list_equipment_status_id, 1) IS NULL)
  )
    , aData AS (
    SELECT
      eq.et_name et_name,
      eq.es_name es_name,
      eq.depo_name,
      'Всего'    what,
      count(1)   eq_all
    FROM eq
    GROUP BY eq.et_name, eq.equipment_type_id,
      eq.es_name, eq.equipment_status_id,
      eq.depo_name
    UNION ALL
    SELECT
      eq.et_name      et_name,
      eq.es_name      es_name,
      eq.depo_name,
      'На ТС'         what,
      count(eq.on_tr) eq_all
    FROM eq
    GROUP BY eq.et_name, eq.equipment_type_id,
      eq.es_name, eq.equipment_status_id,
      eq.depo_name
  )
  SELECT
    CASE GROUPING(depo_name, what, et_name, es_name)
    WHEN 3
      THEN chr(8239) /*https://unicode-table.com/ru/#202F код, позволяющий убрать итого вниз*/
    ELSE et_name END AS et_name,
    CASE GROUPING(depo_name, what, et_name, es_name)
    WHEN 3
      THEN chr(8239) || ' Итого' --||depo_name
    ELSE es_name
    END              AS es_name,
    CASE GROUPING(depo_name, what, et_name, es_name)
    WHEN 8
      THEN chr(8239) || ' Итого' --||depo_name
    ELSE depo_name
    END              AS depo_name,
    what,
    sum(eq_all)         eq_all
  FROM aData
  GROUP BY GROUPING SETS ((), (depo_name, what, et_name, es_name),(depo_name, what),(what,et_name,es_name))
  HAVING GROUPING(depo_name, what, et_name, es_name) IN (0, 3, 8);
END;
$BODY$