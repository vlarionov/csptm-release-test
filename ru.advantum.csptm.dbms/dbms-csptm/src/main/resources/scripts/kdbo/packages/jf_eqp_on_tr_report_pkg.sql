﻿-- Function: kdbo."jf_eqp_on_tr_report_pkg$of_rows"(numeric, text)

-- DROP FUNCTION kdbo."jf_eqp_on_tr_report_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION kdbo."jf_eqp_on_tr_report_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$
declare
    larr_f_depo_id  bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', true);
    larr_f_tr_id    bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_tr_id', true);
    f_dt_DT      TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'f_dt_DT', TRUE);


begin

  open p_rows for

WITH
   trid as (select * from unnest (larr_f_tr_id) as id) --добавил для последнего джойна, а не фильтрации по условию where
  ,dp as (select *  from unnest (larr_f_depo_id ) as id)

  ,adata AS (
    SELECT
      unit2tr.tr_id,
      1 AS id_type,
      et_1.equipment_id,
      et_1.firmware_id,
      et_1.unit_service_type_id,
      et_1.facility_id,
      et_1.equipment_status_id,
      et_1.decommission_reason_id,
      et_1.dt_begin,
      et_1.dt_end,
      et_1.serial_num,
      et_1.equipment_model_id,
      et_1.depo_id,
      et_1.territory_id,
      et_1.unit_service_facility_id,
      et_1.has_diagnostic
    FROM ((core.unit u
      JOIN core.unit2tr unit2tr ON ((u.unit_id = unit2tr.unit_id)))
      JOIN core.v_equipment et_1 ON ((et_1.equipment_id = u.unit_id)))
      where f_dt_DT BETWEEN et_1.dt_begin and coalesce(et_1.dt_end,f_dt_DT)
    UNION ALL
    SELECT
      s2tr.tr_id,
      2 AS id_type,
      et_1.equipment_id,
      et_1.firmware_id,
      et_1.unit_service_type_id,
      et_1.facility_id,
      et_1.equipment_status_id,
      et_1.decommission_reason_id,
      et_1.dt_begin,
      et_1.dt_end,
      et_1.serial_num,
      et_1.equipment_model_id,
      et_1.depo_id,
      et_1.territory_id,
      et_1.unit_service_facility_id,
      et_1.has_diagnostic
    FROM ((core.sensor2tr s2tr
      JOIN core.sensor s ON ((s.sensor_id = s2tr.sensor_id)))
      JOIN core.v_equipment et_1 ON ((et_1.equipment_id = s2tr.sensor_id)))
      where f_dt_DT BETWEEN et_1.dt_begin and coalesce(et_1.dt_end,f_dt_DT)
    UNION ALL
    SELECT
      et2tr.tr_id,
      3 AS id_type,
      et_1.equipment_id,
      et_1.firmware_id,
      et_1.unit_service_type_id,
      et_1.facility_id,
      et_1.equipment_status_id,
      et_1.decommission_reason_id,
      et_1.dt_begin,
      et_1.dt_end,
      et_1.serial_num,
      et_1.equipment_model_id,
      et_1.depo_id,
      et_1.territory_id,
      et_1.unit_service_facility_id,
      et_1.has_diagnostic
    FROM (core.equipment2tr et2tr
      JOIN core.v_equipment et_1 ON ((et_1.equipment_id = et2tr.equipment_id)))
      where f_dt_DT BETWEEN et_1.dt_begin and coalesce(et_1.dt_end,f_dt_DT)
  ) 
  , adata_temp2 as (
SELECT
  et.tr_id,
  et.equipment_id,
  et.serial_num,
  et.firmware_id,
  fw.name                AS firmware_name,
  et.unit_service_type_id,
  ust.name               AS unit_service_type_name,
  et.facility_id,
  ent.name_short         AS facility_name,
  et.equipment_status_id,
  ets.name               AS equipment_status_name,
  et.dt_begin,
  et.dt_end,
  et.equipment_model_id,
  em.name                AS equipment_model_name,
  ett.name_short         AS equipment_type_name,
  em.equipment_type_id,
  entity4depo.name_full  AS depo_name_full,
  entity4depo.name_short AS depo_name_short,
  et.decommission_reason_id,
  dr.name                AS decommission_reason_name,
  sr.usw_num,
  sr.has_manipulator        sr_has_manipulator,
  sr.channel_num,
  /*Закомментировал из-за медленной работы скрипта*/
 --(SELECT smc.phone_num
 --  FROM core.sim_card smc
 --    JOIN core.sim_card2unit smu ON smu.sim_card_id = smc.sim_card_id
 -- WHERE smu.equipment_id = et.equipment_id
 --  LIMIT 1)              AS phone_num,
 -- (SELECT smc.iccid
 --  FROM core.sim_card smc
 --   JOIN core.sim_card2unit smu ON smu.sim_card_id = smc.sim_card_id
 --  WHERE smu.equipment_id = et.equipment_id
 --  LIMIT 1)              AS iccid,
  fw.version                fw_version,
  fw.dt data_ver,
  st.has_manipulator        st_has_manipulator,
  st.has_flash_card,
  ob.channel_qty ob_channel_qty ,
  ob.port_qty ob_port_qty
FROM adata et
  JOIN core.equipment_status ets ON ets.equipment_status_id = et.equipment_status_id
  JOIN core.unit_service_type ust ON ust.unit_service_type_id = et.unit_service_type_id
  JOIN core.entity ent ON ent.entity_id = et.facility_id
  JOIN core.equipment_model em ON em.equipment_model_id = et.equipment_model_id
  JOIN core.equipment_type ett ON ett.equipment_type_id = em.equipment_type_id
  JOIN core.entity entity4depo ON entity4depo.entity_id = et.depo_id
  LEFT JOIN core.decommission_reason dr ON dr.decommission_reason_id = et.decommission_reason_id
  LEFT JOIN core.firmware fw ON fw.firmware_id = et.firmware_id
  LEFT JOIN core.unit_bnsr sr ON sr.unit_bnsr_id = et.equipment_id
  
  LEFT JOIN core.unit_bnst st ON st.unit_bnst_id = et.equipment_id
  LEFT JOIN core.unit_kbtob ob on ob.unit_kbtob_id = et.equipment_id
where 1=1
  and ett.equipment_type_id in (2,3,4,5,6,9,10,16,17,18,19,54,55)
-- and et.tr_id = any(larr_f_tr_id) --Абдуллин Т.Г. - 10.04.2018 - закоментировал, так как нужен индекс для фильтрации, фильтрация происходит в последнем запросе
/*
--Абдуллин Т.Г.-26.03.2018 - Заявка 22907 - оптимизировал запрос: убрал два вложенных селекта из блока выбора полей в запросе выше и вынес их в отдельную CTE в запросе ниже.
*/
  
  )
  ,  eqp   as (
   select et.*, smc.phone_num as phone_num, smc.iccid as iccid from adata_temp2 et 
  left JOIN core.sim_card2unit smu on smu.equipment_id = et.equipment_id
  left join core.sim_card smc ON smu.sim_card_id = smc.sim_card_id   /* отдельная CTE*/
)
  , sfl as (
    SELECT
      eqp.equipment_status_name,
      eqp.serial_num,
      eqp.equipment_model_name,
      eqp.unit_service_type_name
    , row_number()over(PARTITION BY eqp.tr_id) rn
    , eqp.tr_id
    FROM eqp
    WHERE eqp.equipment_type_id = core.jf_equipment_type_pkg$cn_type_dut()/*(3, 'Датчик уровня топлива', 'ДУТ')*/
)
  , alarm_btn as (
    SELECT
      eqp.equipment_status_name,
      eqp.serial_num,
      eqp.equipment_model_name,
      eqp.unit_service_type_name
    , row_number()over(PARTITION BY eqp.tr_id order by eqp.equipment_status_id) rn
    , eqp.tr_id
    FROM eqp
    WHERE eqp.equipment_type_id = core.jf_equipment_type_pkg$cn_type_alarm_btn() /*Тревожная кнопка; 6*/
  and eqp.equipment_status_id in (1,2,3) /*(1, 'Не определен');(2, 'Исправно');(3, 'Неисправно');*/
)
  , itog as (
select de.name_short depo_name
,  tr.garage_num
, tr.licence tr_state_num
, t.code, te.name_short terr_name
, trt.name trt_name
, trc.short_name trc_name, trc.qty
, trs.name status_name
, trm.name model_name
, tr.serial_num tr_serial_num
, tr.year_of_issue
--   --------------
 , b1.equipment_model_name b1m_name
 , b1.serial_num b1_serial_num
 , b1.iccid b1_iccid
 , b1.phone_num b1_phone_num
 , ub1.has_manipulator b1_has_manip
 , b1.fw_version b1fw_version
 , b1.unit_service_type_name b1_unit_service_type
   --------------
 , b2.equipment_model_name b2m_name
 , b2.serial_num b2_serial_num
 , b2.usw_num b2_usw_num
 , ub2.channel_num b2_channel_num
 , b2.iccid b2_iccid
 , b2.phone_num b2_phone_num
 , ub2.has_manipulator b2_has_manip
 , b2.fw_version b2fw_version
 , b2.unit_service_type_name b2_unit_service_type
   --------------
 , b3.equipment_model_name b3m_name
 , b3.serial_num b3_serial_num
 , b3.fw_version b3fw_version
 , b3.unit_service_type_name b3_unit_service_type
--   --------------
-----
--   •	Жесткий диск:
-- o	Модель (Наименование);
-- o	Серийный номер;
-- o	Объем;
-- o	Версия прошивки;

 , h1.equipment_model_name h1m_name
 , h1.serial_num h1_serial_num
 , hd1.capacity h1_capacity
 , h1.fw_version h1fw_version
--   --------------
-- •	Модем:
-- o	Модель (Наименование);
-- o	Серийный номер;
-- o	Номер sim-карты;
-- o	Номер телефона;
-- o	Версия прошивки;

 , m1.equipment_model_name m1m_name
 , m1.serial_num m1_serial_num
 , m1.iccid m1_iccid
 , m1.phone_num m1_phone_num
 , m1.fw_version m1fw_version
  ------------
--  , case eqp.equipment_type_id when 17 then eqp.equipment_model_name end m2m_name
--  , case eqp.equipment_type_id when 17 then eqp.serial_num end m2_serial_num
 , m2.equipment_model_name m2m_name
 , m2.serial_num m2_serial_num
  --------------
-- o	Количество – камер, установленных на ТС;
, c1.cnt /*count(c1.equipment_type_id)over(partition by c1.tr_id, c1.equipment_type_id)*/ c1_cnt
  --------------
-- o	Модель (Наименование);
-- o	Серийный номер;
-- o	Статус БО – Текущий статус тревожной кнопки, установленной на ТС;
 , a1.equipment_model_name a1m_name
 , a1.serial_num a1_serial_num
 , a1.equipment_status_name a1st_name
  --------------
 , f1.equipment_model_name f1m_name
 , f1.serial_num f1_serial_num
 , f1.equipment_status_name f1st_name
  --------------
 , a2.equipment_model_name a2m_name
 , a2.serial_num a2_serial_num
  --------------
 , d1.equipment_model_name d1m_name
 , d1.serial_num d1_serial_num
 , d1.equipment_status_name d1st_name
  --------------
 , d2.equipment_model_name d2m_name
 , d2.serial_num d2_serial_num
 , d2.equipment_status_name d2st_name
  --------------
, /*count(t1.equipment_type_id)over(partition by t1.tr_id, t1.equipment_type_id)*/ t1.cnt t1_cnt
  --------------
, tr.tr_id
from core.tr tr
join core.territory t on t.territory_id=tr.territory_id
join core.entity te on te.entity_id=t.territory_id
join core.tr_type trt on trt.tr_type_id=tr.tr_type_id
join core.tr_model trm on trm.tr_model_id=tr.tr_model_id
join core.tr_capacity trc on trc.tr_capacity_id=trm.tr_capacity_id
join core.tr_status trs on trs.tr_status_id=tr.tr_status_id
join core.entity de on de.entity_id=tr.depo_id
  --------------
left join eqp b1 on b1.tr_id=tr.tr_id and b1.equipment_type_id =core.jf_equipment_type_pkg$cn_type_bnst() /*2*/
left join core.unit_bnst ub1 on ub1.unit_bnst_id=b1.equipment_id
  --------------
left join eqp b2 on b2.tr_id=tr.tr_id and b2.equipment_type_id = core.jf_equipment_type_pkg$cn_type_bnsr() /*9*/
left join core.unit_bnsr ub2 on ub2.unit_bnsr_id=b2.equipment_id
  --------------
left join eqp b3 on b3.tr_id=tr.tr_id and b3.equipment_type_id = core.jf_equipment_type_pkg$cn_type_kbtob() /*10*/
left join core.unit_kbtob ub3 on ub3.unit_kbtob_id =b3.equipment_id
  --------------
left join  eqp h1 on h1.tr_id=tr.tr_id and h1.equipment_type_id = core.jf_equipment_type_pkg$cn_type_hdd() /*19*/
left join  core.hdd hd1 on hd1.hdd_id=h1.equipment_id
  --------------
left join  eqp m1 on m1.tr_id=tr.tr_id and m1.equipment_type_id = core.jf_equipment_type_pkg$cn_type_modem() /*16*/
left join core.modem mm1 on mm1.modem_id=m1.equipment_id
--------
left join  eqp m2 on m2.tr_id=tr.tr_id and m2.equipment_type_id = core.jf_equipment_type_pkg$cn_type_monitor() /*17 Монитор водителя:*/
left join (select c1.tr_id, count(1) cnt from eqp c1 where c1.equipment_type_id = core.jf_equipment_type_pkg$cn_type_camcorder() group by c1.tr_id) c1 on c1.tr_id=tr.tr_id /*Камеры: 18*/
left join  alarm_btn a1 on a1.tr_id=tr.tr_id and a1.rn = 1 /*Тревожная кнопка; 6*/
left join  eqp f1 on f1.tr_id=tr.tr_id and f1.equipment_type_id = core.jf_equipment_type_pkg$cn_type_fire() /*Пожарный извещатель: 5*/
left join  eqp a2 on a2.tr_id=tr.tr_id and a2.equipment_type_id = core.jf_equipment_type_pkg$cn_type_asmpp() /*АСМПП: 4*/
left join  sfl d1 on d1.tr_id=tr.tr_id and d1.rn = 1 /*Датчик уровня топлива№1: 3*/
left join  sfl d2 on d2.tr_id=tr.tr_id and d2.rn = 2 /*Датчик уровня топлива№2: 3*/
--left join  eqp t1 on t1.tr_id=tr.tr_id and t1.equipment_type_id = core.jf_equipment_type_pkg$cn_type_temperature() /*Датчик температуры: 55*/
left join  (select eqp.tr_id, count(1) cnt from eqp where  eqp.equipment_type_id = core.jf_equipment_type_pkg$cn_type_temperature() /*Датчик температуры: 55*/ group by eqp.tr_id ) t1 on t1.tr_id=tr.tr_id
        where 
        exists (select 1 from trid where tr.tr_id =  trid.id) --добавил 12.04 эту строчку!
        and  exists (select  1 from dp where tr.depo_id = dp.id)
)
  select * from itog

--  where tr.tr_id in (
--    78334,
--        14037)
;





end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION kdbo."jf_eqp_on_tr_report_pkg$of_rows"(numeric, text)
  OWNER TO adv;
