﻿-- Function: kdbo."jf_diagnostic_ref_param_pkg$of_rows"(numeric, text)

-- DROP FUNCTION kdbo."jf_diagnostic_ref_param_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_param_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_r kdbo.diagnostic_ref_param%rowtype;
begin
  l_r.equipment_type_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_type_id', true);
 open p_rows for 
      select 
        dgnt_ref_param_name, 
        dgnt_ref_param_code, 
        diagnostic_ref_param_id,
        equipment_type_id
      from kdbo.diagnostic_ref_param p
      where p.equipment_type_id = l_r.equipment_type_id;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_param_pkg$of_rows"(numeric, text)
  OWNER TO adv;
