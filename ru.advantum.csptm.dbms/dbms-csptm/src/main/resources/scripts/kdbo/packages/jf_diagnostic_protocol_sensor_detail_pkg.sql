CREATE OR REPLACE FUNCTION kdbo.jf_diagnostic_protocol_sensor_detail_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_diagnostic_protocol_id_list bigint [] := jofl.jofl_pkg$extract_tarray(p_attr, 'arr_diagnostic_protocol_id', true);
  ln_tr_id						bigint := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
begin


  open p_rows for
  with pr as (
      select
        pr.diagnostic_protocol_id,
        pr.dt_create,
        pr.diagnostic_result_id,
        pr.equipment_id
      from kdbo.diagnostic_protocol pr
      where pr.diagnostic_protocol_id = any (l_diagnostic_protocol_id_list)
  ),
      s0 as (
        select
                        pr.dt_create as creation_date,
          dr.dgnt_result_name,
          dr.dgnt_result_desc,
          /*inst.name*/ ' '          as installation_site_name,
          dr.is_critical,
          pr.dt_create,
          pr.equipment_id,
          pr.diagnostic_protocol_id
        from pr
          join kdbo.diagnostic_result dr on dr.diagnostic_result_id = pr.diagnostic_result_id


        where 1 = 1
    ),
      s1 as (select
               s0.*,
               et.name_short               as equipment_type_name_short,
               em.name                     as equipment_model_name,
               eq.serial_num,
               eq.depo_id,
               eq.firmware_id,
               eq.territory_id,
               et.equipment_type_id,
               entity4depo.name_short      as depo_name_short,
               entity4territory.name_short as territory_name_short
             from s0
               join core.equipment eq on s0.equipment_id = eq.equipment_id
               join core.equipment_model em on eq.equipment_model_id = em.equipment_model_id
               join core.equipment_type et on em.equipment_type_id = et.equipment_type_id
               join core.entity entity4depo on eq.depo_id = entity4depo.entity_id
               left join core.entity entity4territory on eq.territory_id = entity4territory.entity_id
    )
    , sensor as (
      select
        s1.*,
        tr.tr_id,
        tr.garage_num,
        hub.ip_address,
        fw.version as firmware_version,
        '[' ||
        json_build_object('CAPTION', tr.garage_num :: text
        , 'JUMP', json_build_object('METHOD', 'core.tr'
        , 'ATTR'
        , '{}'
                          )
        ) || ']'   as tr_link,
        '[' ||
        json_build_object('CAPTION', s1.serial_num
        , 'JUMP', json_build_object('METHOD',
                                    case s1.equipment_type_id
                                    when core.jf_equipment_type_pkg$cn_type_modem()
                                      then 'core.sensor_modem'
                                    when core.jf_equipment_type_pkg$cn_type_monitor()
                                      then 'core.sensor_monitor'
                                    when core.jf_equipment_type_pkg$cn_type_hdd()
                                      then 'core.sensor_hdd'
                                    when core.jf_equipment_type_pkg$cn_type_asmpp()
                                      then 'core.sensor_asmpp'
                                    when core.jf_equipment_type_pkg$cn_type_dut()
                                      then 'core.sensor_dut'
                                    else 'core.sensor'
                                    end
        , 'ATTR', '{}')
        ) || ']'
                   as sensor_link
      from s1
        join hist.v_sensor2unit_hist v_sensor2unit_hist on s1.equipment_id = v_sensor2unit_hist.sensor_id
                                                           and s1.dt_create :: timestamptz <@
                                                               v_sensor2unit_hist.sys_period
        join hist.v_unit2tr_hist v_unit2tr_hist on v_sensor2unit_hist.unit_id = v_unit2tr_hist.unit_id and
                                                   s1.dt_create :: timestamptz <@ v_sensor2unit_hist.sys_period
        join core.tr tr on v_unit2tr_hist.tr_id = tr.tr_id
        left join core.firmware fw on s1.firmware_id = fw.firmware_id
        left join core.entity entity4territory on s1.territory_id = entity4territory.entity_id
        left join core.v_equipment2unit et2u on s1.equipment_id = et2u.equipment_id
        left join core.unit unit on et2u.unit_id = unit.unit_id
        left join core.hub hub on unit.hub_id = hub.hub_id
      where 1 = 1
  )

  select
    sensor.creation_date as dt_create,
    sensor.equipment_type_name_short,
    sensor.equipment_model_name,
    sensor.serial_num,
    sensor.firmware_version,
    sensor.ip_address,
    sensor.dgnt_result_name,
    sensor.dgnt_result_desc,
    sensor.depo_name_short,
    sensor.territory_name_short,
    sensor.garage_num,
    sensor.installation_site_name,
    sensor.is_critical,
    sensor.depo_id,
    sensor.tr_id,
    sensor.tr_link,
    sensor.sensor_link,
    sensor.diagnostic_protocol_id
  from sensor
  where sensor.tr_id = ln_tr_id
  order by sensor.creation_date desc;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;