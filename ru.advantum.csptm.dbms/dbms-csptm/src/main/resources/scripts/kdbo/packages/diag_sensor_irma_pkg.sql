CREATE OR REPLACE FUNCTION kdbo.diag_sensor_irma_pkg$process(p_time_start timestamp, p_time_finish timestamp)
  RETURNS int AS
$BODY$
declare
    ASMPP_INVALID_OP int := 18;--Несоответствие количества пройденных ОП к показаниям АСМПП', 'Несоответствие количества пройденных ОП к показаниям АСМПП
    ASMPP_EXCEED_INVALID_DATA int := 19; --Превышено количество невалидных данных', 4, 'ASMPP_EXCEED_INVALID_DATA');
    ASMPP_NOEQUAL_IN_OUT int := 20; --Несоответствие количества вошедших и вышедших', 'Несоответствие количества вошедших и вышедших;
begin




    insert into kdbo.diagnostic_protocol(dt_create, equipment_id, diagnostic_result_id)
    with irma_all as (
        SELECT
          sensor_id,
          tr_id,
          count(*) cnt
        FROM snsr.irma_sensor_data
        --WHERE event_time BETWEEN p_time_start AND p_time_finish
        WHERE event_time BETWEEN '2017-07-27' :: TIMESTAMP AND '2017-07-28' :: TIMESTAMP
        GROUP BY sensor_id, tr_id
    ),
    tr_with_irma AS
      (SELECT tr_id, sensor_id
       FROM snsr.v_sensors
       WHERE equipment_type_id = 4
      ),
    fact_stops as
    (SELECT
       orl.tr_id,
       count(*) cnt,
       tr_with_irma.sensor_id
     FROM tt.order_list orl
       JOIN tt.order_round orr ON orr.order_list_id = orl.order_list_id
       JOIN tt.order_oper oro ON oro.order_round_id = orr.order_round_id
       LEFT JOIN tt.order_fact orf ON orf.order_round_id = orr.order_round_id
                                      AND orf.stop_place_muid = oro.stop_place_muid
                                      AND orf.stop_order_num = oro.stop_order_num
                                      AND orf.sign_deleted = 0
       JOIN tt.round rnd ON rnd.round_id = orr.round_id
       JOIN gis.route_trajectories gtj ON gtj.muid = rnd.route_trajectory_muid
       JOIN gis.route_rounds grr ON grr.muid = gtj.route_round_muid
       JOIN gis.route_variants grv ON grv.muid = grr.route_variant_muid
       join tr_with_irma on orl.tr_id = tr_with_irma.tr_id
     WHERE orl.order_date = '2017-07-27' :: TIMESTAMP
           AND orl.tr_id IS NOT NULL
           AND orl.sign_deleted = 0
           AND orl.is_active = 1
           AND orr.is_active = 1
           AND oro.is_active = 1
           AND oro.sign_deleted = 0
     GROUP BY orl.tr_id, tr_with_irma.sensor_id
    )
    select now(), irma_all.sensor_id, ASMPP_INVALID_OP
    from fact_stops
      left join irma_all on fact_stops.tr_id = irma_all.tr_id
      JOIN core.equipment ON irma_all.sensor_id = equipment.equipment_id
    where fact_stops.cnt !=0
    and irma_all.cnt/fact_stops.cnt*100 between
         kdbo.jf_diagnostic_ref_pkg$get_asmpp_op_packet_beg(equipment.equipment_model_id)
         and
         kdbo.jf_diagnostic_ref_pkg$get_asmpp_op_packet_end(equipment.equipment_model_id)
    ;


    insert into kdbo.diagnostic_protocol(dt_create, equipment_id, diagnostic_result_id)
    with irma_all as (
        SELECT
          sensor_id,
          count(*) cnt
        FROM snsr.irma_sensor_data
        WHERE event_time BETWEEN p_time_start AND p_time_finish
        GROUP BY sensor_id
    ),
    irma_not_valid as (
      SELECT
        sensor_id,
        count(*) cnt
      FROM snsr.irma_sensor_data
        JOIN core.equipment ON irma_sensor_data.sensor_id = equipment.equipment_id
      WHERE event_time BETWEEN p_time_start AND p_time_finish AND
            (
              (NOT in1 BETWEEN
              kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_beg(equipment.equipment_model_id)
              AND
              kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_end(equipment.equipment_model_id)
              )
              OR
              (NOT in2 BETWEEN
              kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_beg(equipment.equipment_model_id)
              AND
              kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_end(equipment.equipment_model_id)
              )
              OR
              (NOT in3 BETWEEN
              kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_beg(equipment.equipment_model_id)
              AND
              kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_end(equipment.equipment_model_id)
              )

              OR
              (NOT in4 BETWEEN
              kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_beg(equipment.equipment_model_id)
              AND
              kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_end(equipment.equipment_model_id)
              )
              OR
              (NOT out1 BETWEEN
              kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_beg(equipment.equipment_model_id)
              AND
              kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_end(equipment.equipment_model_id)
              )
              OR
              (NOT out2 BETWEEN
              kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_beg(equipment.equipment_model_id)
              AND
              kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_end(equipment.equipment_model_id)
              )
              OR
              (NOT out3 BETWEEN
              kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_beg(equipment.equipment_model_id)
              AND
              kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_end(equipment.equipment_model_id)
              )
              OR
              (NOT out4 BETWEEN
              kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_beg(equipment.equipment_model_id)
              AND
              kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_end(equipment.equipment_model_id)
              )
            )
        GROUP BY sensor_id
  )
    select now(), irma_all.sensor_id, ASMPP_EXCEED_INVALID_DATA
    from irma_all
      left join irma_not_valid on irma_all.sensor_id = irma_not_valid.sensor_id
      join core.equipment on equipment.equipment_id = irma_all.sensor_id
    where (irma_not_valid.cnt/irma_all.cnt*100) > kdbo.jf_diagnostic_ref_pkg$get_asmpp_persent_not_valid(equipment.equipment_model_id) ;


    insert into kdbo.diagnostic_protocol(dt_create, equipment_id, diagnostic_result_id)
    with a as (SELECT
        sensor_id
      FROM snsr.irma_sensor_data
        JOIN core.equipment ON irma_sensor_data.sensor_id = equipment.equipment_id
      WHERE event_time BETWEEN p_time_start AND p_time_finish ANd
            out1 != 0 and out2 != 0 and out3 != 0 and out4 != 0 and
            (
              (NOT in1/out1*100 BETWEEN
                kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_beg(equipment.equipment_model_id)
                AND
                kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_end(equipment.equipment_model_id)
              )
              OR
              (NOT in2/out2*100 BETWEEN
                kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_beg(equipment.equipment_model_id)
                AND
                kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_end(equipment.equipment_model_id)
              )
              OR
              (NOT in3/out3*100 BETWEEN
                kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_beg(equipment.equipment_model_id)
                AND
                kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_end(equipment.equipment_model_id)
              )
              OR
              (NOT in4/out4*100 BETWEEN
                kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_beg(equipment.equipment_model_id)
                AND
                kdbo.jf_diagnostic_ref_pkg$get_asmpp_in_out_end(equipment.equipment_model_id)
              )
            )
        GROUP BY sensor_id
     )
     select now(), sensor_id, ASMPP_NOEQUAL_IN_OUT from a;

    return 1;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;


CREATE OR REPLACE FUNCTION kdbo.diag_sensor_irma_pkg$of_cron_job()
  RETURNS int AS
$BODY$
declare
    l_ret int;
begin
    select kdbo.diag_sensor_irma_pkg$process(current_date - INTERVAL '2 day', current_date - INTERVAL '1 day') into l_ret;
    return 1;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;