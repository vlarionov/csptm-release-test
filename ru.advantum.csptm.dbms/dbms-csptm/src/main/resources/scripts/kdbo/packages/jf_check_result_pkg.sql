﻿-- Function: kdbo."jf_check_result_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION kdbo."jf_check_result_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION kdbo."jf_check_result_pkg$attr_to_rowtype"(p_attr text)
  RETURNS kdbo.check_result AS
$BODY$ 
declare 
   l_r kdbo.check_result%rowtype; 
begin 
   l_r.check_result_id := jofl.jofl_pkg$extract_varchar(p_attr, 'check_result_id', true); 
   l_r.check_result_name := jofl.jofl_pkg$extract_varchar(p_attr, 'check_result_name', true); 
   l_r.is_critical := jofl.jofl_pkg$extract_boolean(p_attr, 'is_critical', true);

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION kdbo."jf_check_result_pkg$attr_to_rowtype"(text)
  OWNER TO adv;

-- Function: kdbo."jf_check_result_pkg$of_delete"(numeric, text)

-- DROP FUNCTION kdbo."jf_check_result_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION kdbo."jf_check_result_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r kdbo.check_result%rowtype;
begin 
   l_r := kdbo.jf_check_result_pkg$attr_to_rowtype(p_attr);

   delete from  kdbo.check_result where  check_result_id = l_r.check_result_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION kdbo."jf_check_result_pkg$of_delete"(numeric, text)
  OWNER TO adv;


-- Function: kdbo."jf_check_result_pkg$of_insert"(numeric, text)

-- DROP FUNCTION kdbo."jf_check_result_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION kdbo."jf_check_result_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r kdbo.check_result%rowtype;
begin 
   /*l_r := kdbo.jf_check_result_pkg$attr_to_rowtype(p_attr);

   insert into kdbo.check_result select l_r.*;*/

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION kdbo."jf_check_result_pkg$of_insert"(numeric, text)
  OWNER TO adv;


-- Function: kdbo."jf_check_result_pkg$of_rows"(numeric, text)

-- DROP FUNCTION kdbo."jf_check_result_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION kdbo."jf_check_result_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
begin 
 open p_rows for 
      select 
        check_result_id, 
        check_result_name, 
        is_critical
      from kdbo.check_result; 
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION kdbo."jf_check_result_pkg$of_rows"(numeric, text)
  OWNER TO adv;

-- Function: kdbo."jf_check_result_pkg$of_update"(numeric, text)

-- DROP FUNCTION kdbo."jf_check_result_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION kdbo."jf_check_result_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r kdbo.check_result%rowtype;
begin 
   l_r := kdbo.jf_check_result_pkg$attr_to_rowtype(p_attr);

   update kdbo.check_result set
          is_critical = l_r.is_critical
   where
          check_result_id = l_r.check_result_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION kdbo."jf_check_result_pkg$of_update"(numeric, text)
  OWNER TO adv;




create or replace function  kdbo.jf_check_result_pkg$cn_result_successful()
  returns kdbo.check_result.check_result_id%type as
   'select 1::smallint;'
language sql immutable;

create or replace function  kdbo.jf_check_result_pkg$cn_result_tr_without_schema_install()
  returns kdbo.check_result.check_result_id%type as
'select 10::smallint;'
language sql immutable;

create or replace function  kdbo.jf_check_result_pkg$cn_result_tr_incomplect_critical()
  returns kdbo.check_result.check_result_id%type as
'select 20::smallint;'
language sql immutable;

create or replace function  kdbo.jf_check_result_pkg$cn_result_tr_incomplect()
  returns kdbo.check_result.check_result_id%type as
'select 25::smallint;'
language sql immutable;

create or replace function  kdbo.jf_check_result_pkg$cn_result_unit_without_link_hub()
  returns kdbo.check_result.check_result_id%type as
'select 30::smallint;'
language sql immutable;

create or replace function  kdbo.jf_check_result_pkg$cn_result_modem_without_simcard()
  returns kdbo.check_result.check_result_id%type as
'select 40::smallint;'
language sql immutable;

create or replace function  kdbo.jf_check_result_pkg$cn_result_bnst_without_simcard()
  returns kdbo.check_result.check_result_id%type as
'select 50::smallint;'
language sql immutable;
