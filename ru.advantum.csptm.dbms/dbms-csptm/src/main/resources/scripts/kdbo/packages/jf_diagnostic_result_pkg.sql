﻿-- Function: kdbo."jf_diagnostic_result_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION kdbo."jf_diagnostic_result_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_result_pkg$attr_to_rowtype"(p_attr text)
  RETURNS kdbo.diagnostic_result AS
$BODY$ 
declare 
   l_r kdbo.diagnostic_result%rowtype; 
begin 
   l_r.diagnostic_result_id := jofl.jofl_pkg$extract_number(p_attr, 'diagnostic_result_id', true); 
   l_r.dgnt_result_name := jofl.jofl_pkg$extract_varchar(p_attr, 'dgnt_result_name', true); 
   l_r.dgnt_result_desc := jofl.jofl_pkg$extract_varchar(p_attr, 'dgnt_result_desc', true); 
   l_r.equipment_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'equipment_type_id', true); 
   l_r.dgnt_code := jofl.jofl_pkg$extract_varchar(p_attr, 'dgnt_code', true); 
   l_r.is_critical := jofl.jofl_pkg$extract_boolean(p_attr, 'is_critical', true); 

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_result_pkg$attr_to_rowtype"(text)
  OWNER TO adv;


-----------------------------------

-- Function: kdbo."jf_diagnostic_result_pkg$of_delete"(numeric, text)

-- DROP FUNCTION kdbo."jf_diagnostic_result_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_result_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r kdbo.diagnostic_result%rowtype;
begin 
  /* l_r := kdbo.jf_diagnostic_result_pkg$attr_to_rowtype(p_attr);

   delete from  kdbo.diagnostic_result where  diagnostic_result_id = l_r.diagnostic_result_id;*/

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_result_pkg$of_delete"(numeric, text)
  OWNER TO adv;

-----------------------------------
-- Function: kdbo."jf_diagnostic_result_pkg$of_insert"(numeric, text)

-- DROP FUNCTION kdbo."jf_diagnostic_result_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_result_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r kdbo.diagnostic_result%rowtype;
begin 
  /*l_r := kdbo.jf_diagnostic_result_pkg$attr_to_rowtype(p_attr);

   insert into kdbo.diagnostic_result select l_r.*;*/

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_result_pkg$of_insert"(numeric, text)
  OWNER TO adv;

-----------------------------------
-- Function: kdbo."jf_diagnostic_result_pkg$of_rows"(numeric, text)

-- DROP FUNCTION kdbo."jf_diagnostic_result_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_result_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
begin 
 open p_rows for
     select
       dr.diagnostic_result_id,
       dr.dgnt_result_name,
       dr.dgnt_result_desc,
       dr.equipment_type_id,
       dr.dgnt_code,
       dr.is_critical,
       et.name_short as equipment_type_name
     from kdbo.diagnostic_result dr
       join core.equipment_type et on et.equipment_type_id =dr.equipment_type_id;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_result_pkg$of_rows"(numeric, text)
  OWNER TO adv;

-----------------------------------
-- Function: kdbo."jf_diagnostic_result_pkg$of_update"(numeric, text)

-- DROP FUNCTION kdbo."jf_diagnostic_result_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_result_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r kdbo.diagnostic_result%rowtype;
begin 
   l_r := kdbo.jf_diagnostic_result_pkg$attr_to_rowtype(p_attr);

   update kdbo.diagnostic_result set 
          is_critical = l_r.is_critical
   where 
          diagnostic_result_id = l_r.diagnostic_result_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_result_pkg$of_update"(numeric, text)
  OWNER TO adv;

-----------------------------------