CREATE OR REPLACE FUNCTION kdbo.jf_reverse_drive_report_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
    larr_f_depo_id  bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', true);
    larr_f_tr_id    bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_tr_id', true);
    l_f_begin_dt timestamp := jofl.jofl_pkg$extract_date(p_attr, 'f_begin_DT', true);
    l_f_end_dt   timestamp := jofl.jofl_pkg$extract_date(p_attr, 'f_end_DT', true);
begin
    open p_rows for
    with trs as (
        select
            t.depo_id,
            t.tr_id,
            t.garage_num,
            t.licence,
            e.name_full depo_name
        from core.tr t
            join core.depo d on t.depo_id = d.depo_id
            join core.entity e on d.depo_id = e.entity_id
        where (t.tr_id = any (larr_f_tr_id) or array_length(larr_f_tr_id, 1) is null)
          and (t.depo_id = any (larr_f_depo_id) or array_length(larr_f_depo_id, 1) is null)
    ), snsr_vals as (
        select
            bsd.tr_id,
            bsd.sensor_id,
            bsd.event_time reverse_begin,
            null reverse_end,
            null reverse_duration,
            null reverse_dist
        from trs t
            join snsr.binary_sensor_data bsd on t.tr_id = bsd.tr_id
        where bsd.event_time between l_f_begin_dt and l_f_end_dt
              and bsd.equipment_type_id = 7
    )
    select
        t.depo_id,
        t.tr_id,
        t.garage_num,
        t.licence,
        t.depo_name,
        sv.reverse_begin,
        sv.reverse_end,
        sv.reverse_duration,
        sv.reverse_dist,
        e.serial_num,
        em.name model_name
    from trs t
        join snsr_vals sv on t.tr_id = sv.tr_id
        join core.sensor s on sv.sensor_id = s.sensor_id
        join core.equipment e on s.sensor_id = e.equipment_id
        join core.equipment_model em on e.equipment_model_id = em.equipment_model_id
    order by sv.reverse_begin;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;