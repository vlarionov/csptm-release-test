create or replace function kdbo.rep_kdbo_pkg$repair_request_print(
    p_repair_request_id core.repair_request.repair_request_id%type,
    p_timezone          float
)
    returns table(breakdown_name             text,
                  dt                         text,
                  equipment_name             text,
                  eqt_name                   text,
                  equipment_serial_num       text,
                  breakdown_type_name        text,
                  num                        int,
                  description                text,
                  dt_closed                  text,
                  repair_request_status_name text,
                  close_result_name          text,
                  tr_g_num                   int,
                  tr_s_num                   text,
                  depo_name                  text,
                  territory_name             text,
                  facility_name              text,
                  replacement_equipment_name text,
                  repair_request_id          bigint)
language plpgsql
as $$
begin
    return query
    select
        '№ ' || bd.num || ' от ' || to_char(bd.dt_begin + (interval '1 hour') * p_timezone, 'DD.MM.YYYY HH24:MI') breakdown_name,
        to_char(rr.dt + (interval '1 hour') * p_timezone, 'DD.MM.YYYY HH24:MI') dt,
        em.name equipment_name,
        eqt.name_short eqt_name,
        eq.serial_num equipment_serial_num,
        bdp.name breakdown_type_name,
        rr.num,
        rr.description,
        to_char(rr.dt_closed + (interval '1 hour') * p_timezone, 'DD.MM.YYYY HH24:MI') dt_closed,
        rrs.name repair_request_status_name,
        rcr.name close_result_name,
        tr.garage_num tr_g_num,
        tr.licence tr_s_num,
        de.name_short depo_name,
        te.name_short territory_name,
        fe.name_short facility_name,
        replacement_equipment.serial_num as replacement_equipment_name,
        rr.repair_request_id
    from core.repair_request rr
        join core.repair_request_status rrs on rrs.repair_request_status_id = rr.repair_request_status_id
        join core.breakdown_type bdp on bdp.breakdown_type_id = rr.breakdown_type_id
        left join core.rr_close_result rcr on rcr.rr_close_result_id = rr.close_result_id
        left join core.territory t on t.territory_id = rr.territory_id
        left join core.entity te on te.entity_id = t.territory_id
        left join core.facility f on f.facility_id = rr.facility_id
        left join core.entity fe on fe.entity_id = f.facility_id
        left join core.breakdown bd on bd.breakdown_id = rr.breakdown_id
        left join core.equipment eq on eq.equipment_id = rr.equipment_id
        left join core.entity de on de.entity_id = eq.depo_id
        left join core.equipment_model em on em.equipment_model_id = eq.equipment_model_id
        left join core.equipment_type eqt on eqt.equipment_type_id = em.equipment_type_id
        left join core.equipment replacement_equipment
            on replacement_equipment.equipment_id = rr.replacement_equipment_id
        left join core.v_equipment2tr4all etr on etr.equipment_id = eq.equipment_id
        left join core.tr tr on tr.tr_id = etr.tr_id
    where rr.repair_request_id = p_repair_request_id;
end;
$$