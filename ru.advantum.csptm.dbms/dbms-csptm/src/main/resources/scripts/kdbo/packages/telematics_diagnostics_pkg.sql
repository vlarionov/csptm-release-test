CREATE OR REPLACE FUNCTION kdbo.telematics_diagnostics_pkg$update_bnsr_request_list(
  p_unit_id         core.unit.unit_id%TYPE,
  p_is_unit_enabled BOOLEAN
)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE
  p_tr_id        core.tr.tr_id%TYPE;
  p_unit_bnsr_id core.unit_bnsr.unit_bnsr_id%TYPE;
BEGIN
  IF NOT exists(
      SELECT 1
      FROM core.unit_bnst
      WHERE unit_bnst_id = p_unit_id
  )
  THEN
    RETURN;
  END IF;

  SELECT u2t.tr_id
  INTO STRICT p_tr_id
  FROM core.unit2tr u2t
  WHERE u2t.unit_id = p_unit_id AND u2t.sys_period @> now();

  SELECT ubsr.unit_bnsr_id
  INTO p_unit_bnsr_id
  FROM core.unit2tr u2t
    JOIN core.unit_bnsr ubsr ON u2t.unit_id = ubsr.unit_bnsr_id
  WHERE u2t.tr_id = p_tr_id;

  IF p_unit_bnsr_id IS NOT NULL
  THEN
    IF p_is_unit_enabled
    THEN
      PERFORM mnt.bnsr_request_list_pkg$auto_remove_by_unit_id(p_unit_bnsr_id);
    ELSE
      PERFORM mnt.bnsr_request_list_pkg$add(p_unit_bnsr_id, now()::timestamp, FALSE);
    END IF;
  END IF;
END;
$$;

CREATE OR REPLACE FUNCTION kdbo.telematics_diagnostics_pkg$get_unit2eq_type()
  RETURNS TABLE(unit_id BIGINT, equipment_type_id SMALLINT)
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
    SELECT e.equipment_id, em.equipment_type_id::SMALLINT
  FROM core.equipment e
  JOIN core.equipment_model em ON e.equipment_model_id = em.equipment_model_id;
END;
$$;