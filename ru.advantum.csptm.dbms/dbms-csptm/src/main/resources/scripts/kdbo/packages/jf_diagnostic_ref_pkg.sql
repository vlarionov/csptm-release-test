﻿-- Function: kdbo."jf_diagnostic_ref_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION kdbo."jf_diagnostic_ref_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$attr_to_rowtype"(p_attr text)
  RETURNS kdbo.diagnostic_ref AS
$BODY$ 
declare 
   l_r kdbo.diagnostic_ref%rowtype; 
begin 
   l_r.diagnostic_ref_id := jofl.jofl_pkg$extract_number(p_attr, 'diagnostic_ref_id', true); 
   l_r.diagnostic_ref_param_id := jofl.jofl_pkg$extract_number(p_attr, 'diagnostic_ref_param_id', true); 
   l_r.equipment_model_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_model_id', true); 
   l_r.val := jofl.jofl_pkg$extract_number(p_attr, 'val', true); 
   l_r.dt_create := jofl.jofl_pkg$extract_varchar(p_attr, 'dt_create', true); 
   l_r.is_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'is_deleted', true); 

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$attr_to_rowtype"(text)
  OWNER TO adv;
---------------------------------------------------------------------------

-- Function: kdbo."jf_diagnostic_ref_pkg$of_delete"(numeric, text)
-- DROP FUNCTION kdbo."jf_diagnostic_ref_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r kdbo.diagnostic_ref%rowtype;
begin 
   l_r := kdbo.jf_diagnostic_ref_pkg$attr_to_rowtype(p_attr);

   delete from  kdbo.diagnostic_ref where  diagnostic_ref_id = l_r.diagnostic_ref_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$of_delete"(numeric, text)
  OWNER TO adv;

---------------------------------------------------------------------------
-- Function: kdbo."jf_diagnostic_ref_pkg$of_insert"(numeric, text)

-- DROP FUNCTION kdbo."jf_diagnostic_ref_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r kdbo.diagnostic_ref%rowtype;
begin 
   l_r := kdbo.jf_diagnostic_ref_pkg$attr_to_rowtype(p_attr);
   l_r.diagnostic_ref_id = nextval( 'kdbo.diagnostic_ref_diagnostic_ref_id_seq' );
   l_r.dt_create :=now();
   l_r.is_deleted := 0;

   insert into kdbo.diagnostic_ref select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$of_insert"(numeric, text)
  OWNER TO adv;


---------------------------------------------------------------------------
-- Function: kdbo."jf_diagnostic_ref_pkg$of_rows"(numeric, text)

-- DROP FUNCTION kdbo."jf_diagnostic_ref_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_r kdbo.diagnostic_ref%rowtype;
begin
  l_r.equipment_model_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_model_id', true);
 open p_rows for 
      select 
        dr.diagnostic_ref_id,
        dr.diagnostic_ref_param_id,
        dr.equipment_model_id,
        dr.val,
        dr.dt_create,
        dr.is_deleted,
        p.dgnt_ref_param_name
      from kdbo.diagnostic_ref dr
      join kdbo.diagnostic_ref_param p on p.diagnostic_ref_param_id = dr.diagnostic_ref_param_id
      where dr.equipment_model_id = l_r.equipment_model_id;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$of_rows"(numeric, text)
  OWNER TO adv;


---------------------------------------------------------------------------
-- Function: kdbo."jf_diagnostic_ref_pkg$of_update"(numeric, text)

-- DROP FUNCTION kdbo."jf_diagnostic_ref_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$of_update"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r kdbo.diagnostic_ref%rowtype;
begin 
   l_r := kdbo.jf_diagnostic_ref_pkg$attr_to_rowtype(p_attr);

   update kdbo.diagnostic_ref set 
          diagnostic_ref_param_id = l_r.diagnostic_ref_param_id, 
          equipment_model_id = l_r.equipment_model_id, 
          val = l_r.val, 
          dt_create = l_r.dt_create, 
          is_deleted = l_r.is_deleted
   where 
          diagnostic_ref_id = l_r.diagnostic_ref_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$of_update"(numeric, text)
  OWNER TO adv;

---------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_value"(
  p_equipment_model_id numeric,
  p_param_name text)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin
  select into l_res dr.val
  from kdbo.diagnostic_ref dr
  join kdbo.diagnostic_ref_param p on p.diagnostic_ref_param_id = dr.diagnostic_ref_param_id
  where dr.equipment_model_id= p_equipment_model_id
    and p.dgnt_ref_param_code = p_param_name;

  return l_res;

  exception
    when NO_DATA_FOUND then
         return null;
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_value"(numeric, text)
OWNER TO adv;

---------------------------------------------------------------------------
--Начать диагностику за … минут до планируемого выхода ТС на линию     БСНТ
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnt_before_output"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_BSNT_BEFORE_OUTPUT');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnt_before_output"(numeric)
OWNER TO adv;

---------------------------------------------------------------------------
--Начать диагностику за … минут до планируемого выхода ТС на линию    БСНР
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnr_before_output"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_BSNR_BEFORE_OUTPUT');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnr_before_output"(numeric)
OWNER TO adv;

---------------------------------------------------------------------------
--Начать диагностику за … минут до планируемого выхода ТС на линию    КБТОБ
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_kbtob_before_output"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_KBTOB_BEFORE_OUTPUT');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_kbtob_before_output"(numeric)
OWNER TO adv;

---------------------------------------------------------------------------
--Регламентированное время ожидания пакета данных от блока    БСНТ
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnt_time_wait_packet"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_BSNT_TIME_WAIT_PACKET');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnt_time_wait_packet"(numeric)
OWNER TO adv;

---------------------------------------------------------------------------
--Регламентированное время ожидания пакета данных от блока    БСНР
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnr_time_wait_packet"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_BSNR_TIME_WAIT_PACKET');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnr_time_wait_packet"(numeric)
OWNER TO adv;
---------------------------------------------------------------------------
--Регламентированное время ожидания пакета данных от блока    БСНТ
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_kbtob_time_wait_packet"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_KBTOB_TIME_WAIT_PACKET');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_kbtob_time_wait_packet"(numeric)
OWNER TO adv;

---------------------------------------------------------------------------
--Допустимое количество подряд получаемых невалидных пакетов данных    БСНТ
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnt_invalid_packet"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_BSNT_INVALID_PACKET');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnt_invalid_packet"(numeric)
OWNER TO adv;

---------------------------------------------------------------------------
--Допустимое количество подряд получаемых невалидных пакетов данных    БСНР
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnr_invalid_packet"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_BSNR_INVALID_PACKET');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnr_invalid_packet"(numeric)
OWNER TO adv;

---------------------------------------------------------------------------
--Допустимое количество невалидных пакетов данных в выборке БСНТ
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnt_invalid_packet_in_seq"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_BSNT_INVALID_PACKET_IN_SEQ');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnt_invalid_packet_in_seq"(numeric)
OWNER TO adv;

---------------------------------------------------------------------------
--Количество пакетов в выборке БСНТ
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnt_packet_seq_size"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_BNST_PACKET_SEQ_SIZE');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnt_packet_seq_size"(numeric)
OWNER TO adv;

---------------------------------------------------------------------------
--Допустимое количество невалидных пакетов данных в выборке БСНP
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnr_invalid_packet_in_seq"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_BSNR_INVALID_PACKET_IN_SEQ');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnr_invalid_packet_in_seq"(numeric)
OWNER TO adv;

---------------------------------------------------------------------------
--Количество пакетов в выборке БСНР
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnr_packet_seq_size"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_BNSR_PACKET_SEQ_SIZE');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnr_packet_seq_size"(numeric)
OWNER TO adv;

---------------------------------------------------------------------------
--Количество подряд получаемых валидных пакетов данных, после получения которых можно считать, что блок находится в Рабочем состоянии      БСНТ
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnt_valid_packet"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_BSNT_VALID_PACKET');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnt_valid_packet"(numeric)
OWNER TO adv;

---------------------------------------------------------------------------
--Количество подряд получаемых валидных пакетов данных, после получения которых можно считать, что блок находится в Рабочем состоянии      БСНР
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnr_valid_packet"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_BSNR_VALID_PACKET');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_bsnr_valid_packet"(numeric)
OWNER TO adv;


---------------------------------------------------------------------------
--Соотношение невалидных показаний АСМПП к всего показаниям АСМПП
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_asmpp_persent_not_valid"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_ASMPP_PERSENT_NOT_VALID');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_asmpp_persent_not_valid"(numeric)
OWNER TO adv;

---------------------------------------------------------------------------
--Допустимый диапазон соотношений всего пройденных остановок к пакетам данных с показаниями АСМПП с…
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_asmpp_op_packet_beg"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_ASMPP_OP_PACKET_BEG');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_asmpp_op_packet_beg"(numeric)
OWNER TO adv;
---------------------------------------------------------------------------
--Допустимый диапазон соотношений всего пройденных остановок к пакетам данных с показаниями АСМПП по…
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_asmpp_op_packet_end"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_ASMPP_OP_PACKET_END');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_asmpp_op_packet_end"(numeric)
OWNER TO adv;

---------------------------------------------------------------------------
--Допустимое количество входящих/выходящих с…
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_asmpp_in_out_beg"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_ASMPP_IN_OUT_BEG');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_asmpp_in_out_beg"(numeric)
OWNER TO adv;

---------------------------------------------------------------------------
--Допустимое количество входящих/выходящих по…
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_asmpp_in_out_end"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_ASMPP_IN_OUT_END');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_asmpp_in_out_end"(numeric)
OWNER TO adv;
---------------------------------------------------------------------------
--Допустимое соотношение пакетов с показаниями ДУТ к всего полученным от блока пакетам данных
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_dut_bb_koeff"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_DUT_BB_KOEFF');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_dut_bb_koeff"(numeric)
OWNER TO adv;

---------------------------------------------------------------------------
--Допустимое соотношение невалидных данных ДУТ к всего полученным показаниям ДУТ.
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_dut_invalid_koeff"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_DUT_INVALID_KOEFF');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_dut_invalid_koeff"(numeric)
OWNER TO adv;

---------------------------------------------------------------------------
-- Допустимые значения уровня топлива в литрах с…
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_dut_liter_beg"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_DUT_LITER_BEG');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_dut_liter_beg"(numeric)
OWNER TO adv;


---------------------------------------------------------------------------
-- Допустимые значения уровня топлива в литрах по…
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_dut_liter_end"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_DUT_LITER_END');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_dut_liter_end"(numeric)
OWNER TO adv;

---------------------------------------------------------------------------
-- Допустимые значения уровня топлива в мм с…
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_dut_mm_beg"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_DUT_MM_BEG');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_dut_mm_beg"(numeric)
OWNER TO adv;


---------------------------------------------------------------------------
-- Допустимые значения уровня топлива в мм по…
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_dut_mm_end"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_DUT_MM_END');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_dut_mm_end"(numeric)
OWNER TO adv;

---------------------------------------------------------------------------
-- Допустимая температура бака в Цельсиях с…
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_dut_temp_tank_beg"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_DUT_TEMP_TANK_BEG');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_dut_temp_tank_beg"(numeric)
OWNER TO adv;


---------------------------------------------------------------------------
-- Допустимая температура бака в Цельсиях по…
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_dut_temp_tank_end"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_DUT_TEMP_TANK_END');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_dut_temp_tank_end"(numeric)
OWNER TO adv;

---------------------------------------------------------------------------
-- Допустимая температура  с…    ДТ
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_dt_temp_beg"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_DT_TEMP_BEG');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_dt_temp_beg"(numeric)
OWNER TO adv;


---------------------------------------------------------------------------
-- Допустимая температура  по…   ДТ
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_dt_temp_end"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_DT_TEMP_END');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_dt_temp_end"(numeric)
OWNER TO adv;

---------------------------------------------------------------------------
-- опустимое соотношение невалидных показаний ДТ к всего полученным показаниям ДТ.
CREATE OR REPLACE FUNCTION kdbo."jf_diagnostic_ref_pkg$get_dt_invalid_koeff"(
  p_equipment_model_id numeric)
  RETURNS numeric AS
$BODY$
declare
  l_res numeric;
begin

  return kdbo.jf_diagnostic_ref_pkg$get_value(p_equipment_model_id, 'P_DT_INVALID_KOEFF');
end;
$BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100;
ALTER FUNCTION kdbo."jf_diagnostic_ref_pkg$get_dt_invalid_koeff"(numeric)
OWNER TO adv;











