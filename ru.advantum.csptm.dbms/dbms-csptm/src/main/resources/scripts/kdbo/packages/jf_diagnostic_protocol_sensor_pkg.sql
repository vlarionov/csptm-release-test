﻿CREATE OR REPLACE FUNCTION kdbo.jf_diagnostic_protocol_sensor_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_depo_id            bigint;
  l_trs_ids            bigint [];
  l_dt_begin           timestamp;
  l_dt_end             timestamp;
  l_equipment_type_id  smallint;
  l_equipment_model_id integer;
  l_is_critical        smallint;
  l_depo_list          bigint [] := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_depo_id', true);
begin
  l_dt_begin := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'F_BEGIN_DT', true), now() - interval '1 hours');
  l_dt_end   := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'F_END_DT', true), now());
  l_depo_id  := jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', true);
  l_equipment_type_id  := jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_type_id', true);
  l_equipment_model_id := jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_model_id', true);
  l_trs_ids            := jofl.jofl_pkg$extract_tarray(p_attr, 'f_list_tr_id', true);
  l_is_critical         := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_is_critical_INPLACE_K', true), -1);

  open p_rows for
  with pr as (
      select
        pr.diagnostic_protocol_id,
        array_agg(pr.diagnostic_protocol_id) over(partition by pr.equipment_id)::text as arr_diagnostic_protocol_id,
        pr.dt_create,
        pr.diagnostic_result_id,
        pr.equipment_id
      from kdbo.diagnostic_protocol pr
      where dt_create between l_dt_begin and l_dt_end
  ),
      s0 as (
        select
          pr.dt_create as creation_date,
          dr.dgnt_result_name,
          dr.dgnt_result_desc,
          /*inst.name*/ ' '          as installation_site_name,
          dr.is_critical,
          pr.dt_create,
          pr.equipment_id,
          pr.arr_diagnostic_protocol_id
        from pr
          join kdbo.diagnostic_result dr on dr.diagnostic_result_id = pr.diagnostic_result_id
        where 1 = 1
    ),
      s1 as (select
               s0.*,
               et.name_short               as equipment_type_name_short,
               em.name                     as equipment_model_name,
               eq.serial_num,
               eq.depo_id,
               eq.firmware_id,
               eq.territory_id,
               et.equipment_type_id,
               entity4depo.name_short      as depo_name_short,
               entity4territory.name_short as territory_name_short
             from s0
               join core.equipment eq on s0.equipment_id = eq.equipment_id
               join core.equipment_model em on eq.equipment_model_id = em.equipment_model_id
               join core.equipment_type et on em.equipment_type_id = et.equipment_type_id
               join core.entity entity4depo on eq.depo_id = entity4depo.entity_id
               left join core.entity entity4territory on eq.territory_id = entity4territory.entity_id
             where 1 = 1
                   and et.equipment_class_id != core.jf_equipment_class_pkg$cn_block()
                   and (l_equipment_type_id is null or eq.equipment_type_id = l_equipment_type_id)
                   and (l_equipment_model_id is null or eq.equipment_model_id = l_equipment_model_id)
    )
    , sensor as (
      select
        s1.*,
        tr.tr_id,
        tr.garage_num,
        hub.ip_address,
        fw.version as firmware_version,
        '[' ||
        json_build_object('CAPTION', tr.garage_num :: text
        , 'JUMP', json_build_object('METHOD', 'core.tr'
        , 'ATTR'
        , '{}'
                          )
        ) || ']'   as tr_link,
        '[' ||
        json_build_object('CAPTION', s1.serial_num
        , 'JUMP', json_build_object('METHOD',
                                    case s1.equipment_type_id
                                    when core.jf_equipment_type_pkg$cn_type_modem()
                                      then 'core.sensor_modem'
                                    when core.jf_equipment_type_pkg$cn_type_monitor()
                                      then 'core.sensor_monitor'
                                    when core.jf_equipment_type_pkg$cn_type_hdd()
                                      then 'core.sensor_hdd'
                                    when core.jf_equipment_type_pkg$cn_type_asmpp()
                                      then 'core.sensor_asmpp'
                                    when core.jf_equipment_type_pkg$cn_type_dut()
                                      then 'core.sensor_dut'
                                    else 'core.sensor'
                                    end
        , 'ATTR', '{}')
        ) || ']'
                   as sensor_link
        --, count(1) over (partition by s1.equipment_id) cnt
         , '[' ||
         json_build_object('CAPTION', (count(1) over (partition by s1.equipment_id, tr.tr_id)::TEXT)
         , 'JUMP',json_build_object('METHOD', 'kdbo.diagnostic_protocol_sensor_detail'
         , 'ATTR' 
         , '{}' )) || ']' as cnt        
         , row_number() over (partition by s1.equipment_id order by s1.dt_create desc) rn
      from s1
        join hist.v_sensor2unit_hist v_sensor2unit_hist on s1.equipment_id = v_sensor2unit_hist.sensor_id
        												and s1.dt_create :: timestamptz <@ v_sensor2unit_hist.sys_period
        join hist.v_unit2tr_hist v_unit2tr_hist on v_sensor2unit_hist.unit_id = v_unit2tr_hist.unit_id 
                                                and s1.dt_create :: timestamptz <@ v_sensor2unit_hist.sys_period
        join core.tr tr on v_unit2tr_hist.tr_id = tr.tr_id
        left join core.firmware fw on s1.firmware_id = fw.firmware_id
        left join core.entity entity4territory on s1.territory_id = entity4territory.entity_id
        left join core.v_equipment2unit et2u on s1.equipment_id = et2u.equipment_id
        left join core.unit unit on et2u.unit_id = unit.unit_id
        left join core.hub hub on unit.hub_id = hub.hub_id
      where 1 = 1
  )

  select
    sensor.creation_date as dt_create,
    sensor.equipment_type_name_short,
    sensor.equipment_model_name,
    sensor.serial_num,
    sensor.firmware_version,
    sensor.ip_address,
    sensor.dgnt_result_name,
    sensor.dgnt_result_desc,
    sensor.depo_name_short,
    sensor.territory_name_short,
    sensor.garage_num,
    sensor.installation_site_name,
    sensor.is_critical,
    sensor.depo_id,
    sensor.tr_id,
    sensor.tr_link,
    sensor.sensor_link,
    sensor.cnt,
    replace(replace(sensor.arr_diagnostic_protocol_id, '}', ''), '{', '') as arr_diagnostic_protocol_id
  from sensor
  where sensor.rn = 1
    and (sensor.depo_id = l_depo_id or sensor.depo_id = any (l_depo_list) or array_length(l_depo_list, 1) is null )
    and (sensor.tr_id in (select adm.account_data_realm_pkg$get_trs_by_tr_type_and_depo(p_id_account)))
    and (sensor.tr_id = any(l_trs_ids) or array_length(l_trs_ids, 1) is null )
    and (l_is_critical = -1 or sensor.is_critical::int = l_is_critical);
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

--------------------------------------------------------------------------------------------------------------------
create or replace function kdbo.jf_diagnostic_protocol_sensor_pkg$of_request(
  in p_id_account numeric,
  in p_attr       text)
  returns boolean as
$BODY$
declare
begin
  return kdbo.jf_diagnostic_protocol_pkg$of_request(p_id_account => p_id_account, p_attr => p_attr);
end;
$BODY$
language plpgsql volatile
cost 100;
alter function kdbo.jf_diagnostic_protocol_sensor_pkg$of_request( numeric, text )
owner to adv;


create or replace function kdbo.jf_diagnostic_protocol_sensor_pkg$of_breakdown(
  in p_id_account numeric,
  in p_attr       text)
  returns boolean as
$BODY$
declare
begin
  --Запись о поломке
  return kdbo.jf_diagnostic_protocol_pkg$of_breakdown(p_id_account => p_id_account, p_attr => p_attr);
end;

$BODY$
language plpgsql volatile
cost 100;

alter function kdbo.jf_diagnostic_protocol_sensor_pkg$of_breakdown( numeric, text )
owner to adv;


create or replace function kdbo.jf_diagnostic_protocol_sensor_pkg$of_process(p_id_account numeric, p_attr text)
  returns text
as
$BODY$
declare
  l_dt_begin timestamp;
  l_dt_end   timestamp;
  l_type     text;
  l_res      int;
begin
  l_dt_begin := jofl.jofl_pkg$extract_date(p_attr, 'BEGIN_DT', false);
  l_dt_end   := jofl.jofl_pkg$extract_date(p_attr, 'END_DT', false);
  l_type  := jofl.jofl_pkg$extract_varchar(p_attr, 'F_INPLACE_K', false);

  if l_type = 'fuel'
  then
    select kdbo.diag_sensor_fuel_pkg$process(l_dt_begin, l_dt_end)
    into l_res;
  elsif l_type = 'termo'
    then
      select kdbo.diag_sensor_termo_pkg$process(l_dt_begin, l_dt_end)
      into l_res;
  elsif l_type = 'irma'
    then
      select kdbo.diag_sensor_irma_pkg$process(l_dt_begin, l_dt_end)
      into l_res;
  end if;

  return '';
end;
$BODY$
language plpgsql volatile
cost 100;

alter function kdbo.jf_diagnostic_protocol_sensor_pkg$of_process( numeric, text )
owner to adv;