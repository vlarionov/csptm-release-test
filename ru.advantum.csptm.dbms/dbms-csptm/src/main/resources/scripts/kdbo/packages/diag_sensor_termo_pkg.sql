--drop FUNCTION kdbo.diag_sensor_termo_pkg$of_cron_job();

CREATE OR REPLACE FUNCTION kdbo.diag_sensor_termo_pkg$process(p_time_start timestamp, p_time_finish timestamp)
  RETURNS int AS
$BODY$
declare
    DT_EXCEED_INVALID_DATA int := 21;
begin
    insert into kdbo.diagnostic_protocol(dt_create, equipment_id, diagnostic_result_id)
    WITH raw_data AS
    (
        SELECT
          sensor_id,
          val,
          equipment_model_id
        FROM snsr.termo_data
              JOIN core.equipment ON termo_data.sensor_id = equipment.equipment_id
        WHERE event_time BETWEEN p_time_start AND p_time_finish
    ),
    _valid as
      (
          SELECT
            sensor_id,
            count(*) cnt,
            equipment_model_id
          FROM raw_data
          GROUP BY sensor_id, equipment_model_id
      ),
    not_valid as
      (
          SELECT
            sensor_id,
            count(*) cnt,
            equipment_model_id
          FROM raw_data
          WHERE NOT val BETWEEN
          kdbo.jf_diagnostic_ref_pkg$get_dt_temp_beg(raw_data.equipment_model_id)
          AND
          kdbo.jf_diagnostic_ref_pkg$get_dt_temp_end(raw_data.equipment_model_id)
          GROUP BY sensor_id, equipment_model_id
      )
    select now(), _valid.sensor_id, DT_EXCEED_INVALID_DATA
    from _valid join not_valid on (_valid.sensor_id = not_valid.sensor_id)
    where (not_valid.cnt/_valid.cnt)*100 > kdbo.jf_diagnostic_ref_pkg$get_dt_invalid_koeff(_valid.equipment_model_id);
    return 1;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;


CREATE OR REPLACE FUNCTION kdbo.diag_sensor_termo_pkg$of_cron_job()
  RETURNS int AS
$BODY$
declare
    l_ret int;
begin
    select kdbo.diag_sensor_termo_pkg$process(current_date - INTERVAL '2 day', current_date - INTERVAL '1 day') into l_ret;
    return 1;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;