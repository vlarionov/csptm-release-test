CREATE OR REPLACE FUNCTION kdbo.jf_diagnostic_list_detail_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
DECLARE
  l_diagnostic_period_id_list  BIGINT [] := jofl.jofl_pkg$extract_tarray(p_attr, 'arr_diagnostic_period_id', TRUE);
BEGIN
  OPEN p_rows FOR
  SELECT
    dp.diagnostic_period_id,
    ed.name_short,
    ed.name_full,
    dp.start_date dt_b,
    dp.end_date   dt_e,
    et.name_short etype_name,
    em.name       emodel_name,
    e.serial_num,
    tr.garage_num,
    e.equipment_id,
    rd.tr_id IS NOT NULL as is_required_diagnostic
  FROM kdbo.diagnostic_period dp
    JOIN core.equipment e ON e.equipment_id = dp.equipment_id
    JOIN core.depo d ON d.depo_id = e.depo_id
    JOIN core.entity ed ON ed.entity_id = d.depo_id
    JOIN core.equipment_model em ON em.equipment_model_id = e.equipment_model_id
    JOIN core.equipment_type et ON et.equipment_type_id = em.equipment_type_id
    JOIN core.v_equipment2tr4all etr ON etr.equipment_id = e.equipment_id
    JOIN core.tr tr ON tr.tr_id = etr.tr_id
    left join kdbo.required_diagnostic rd on rd.tr_id = tr.tr_id
  where dp.diagnostic_period_id = any(l_diagnostic_period_id_list)
  order by dp.start_date desc
  		  ,dp.end_date desc;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;