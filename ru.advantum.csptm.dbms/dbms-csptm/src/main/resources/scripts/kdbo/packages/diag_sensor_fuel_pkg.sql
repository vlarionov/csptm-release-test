--drop FUNCTION kdbo.diag_sensor_fuel_pkg$of_cron_job();

CREATE OR REPLACE FUNCTION kdbo.diag_sensor_fuel_pkg$process(p_time_start timestamp, p_time_finish timestamp)
  RETURNS int AS
$BODY$
declare
    DUT_EXCEED_INVALID_FUEL int :=16; --Превышено количество невалидных показаний ДУТ по уровню топлива
    DUT_EXCEED_INVALID_TEMP_TANK int := 17; --Превышено количество невалидных показаний ДУТ по температуре бака
    DUT_NO_DATA int := 22; --Отсутствуют данные от ДУТ
    DUT_SWITCHED_OFF int := 23; --ДУТ не подключен
    EQUIPMENT_TYPE_DUT int := 3;
begin

  --ДУТ не подключен
  insert into kdbo.diagnostic_protocol(dt_create, equipment_id, diagnostic_result_id)
  with a as (
      SELECT
               sensor_id
               ,min(event_time) event_time
             FROM snsr.fuel_sensor_data
             WHERE event_time BETWEEN p_time_start AND p_time_finish
               and fuel_sensor_data.switched_on = false
             GROUP BY sensor_id
  )
  select coalesce(a.event_time, now()), v_sensors.sensor_id, DUT_SWITCHED_OFF
  FROM snsr.v_sensors
    left join a on a.sensor_id = v_sensors.sensor_id
  WHERE equipment_type_id = EQUIPMENT_TYPE_DUT;



    --Превышено количество пакетов без показаний ДУТ
    insert into kdbo.diagnostic_protocol(dt_create, equipment_id, diagnostic_result_id)
    with a as (SELECT
                 sensor_id,
                 count(*) cnt,
                 max(event_time) event_time
               FROM snsr.fuel_sensor_data
               WHERE event_time BETWEEN p_time_start AND p_time_finish
               GROUP BY sensor_id
    )

    select coalesce(event_time, now()), v_sensors.sensor_id, DUT_NO_DATA
    FROM snsr.v_sensors
      left join a on a.sensor_id = v_sensors.sensor_id
    WHERE equipment_type_id = EQUIPMENT_TYPE_DUT and cnt is null;


    ----Превышено количество невалидных показаний ДУТ по уровню топлива
    insert into kdbo.diagnostic_protocol(dt_create, equipment_id, diagnostic_result_id)
    WITH raw_data AS
    (
        SELECT
          sensor_id,
          level_mm,
          level_l,
          temperature,
          equipment_model_id,
          event_time
        FROM snsr.fuel_sensor_data
              JOIN core.equipment ON fuel_sensor_data.sensor_id = equipment.equipment_id
        WHERE event_time BETWEEN p_time_start AND p_time_finish
    ),
    _valid as
      (
          SELECT
            sensor_id,
            count(*) cnt,
            equipment_model_id,
            min(event_time) event_time
          FROM raw_data
          GROUP BY sensor_id, equipment_model_id
      ),
        koef as (
           select kdbo.jf_diagnostic_ref_pkg$get_dut_mm_beg(equipment_model_id) as dut_mm_beg
                  ,kdbo.jf_diagnostic_ref_pkg$get_dut_mm_end(equipment_model_id) as dut_mm_end
                  ,kdbo.jf_diagnostic_ref_pkg$get_dut_liter_beg(equipment_model_id) as dut_liter_beg
                  ,kdbo.jf_diagnostic_ref_pkg$get_dut_liter_end(equipment_model_id) as dut_liter_end
                  ,equipment_model_id
           from core.equipment_model
        )
        ,
        not_valid as
        (
              SELECT
                sensor_id,
                count(*) cnt,
                koef.equipment_model_id
              FROM raw_data join koef on koef.equipment_model_id = raw_data.equipment_model_id
              WHERE
                (
                  NOT level_mm BETWEEN
                  koef.dut_mm_beg
                  AND
                  koef.dut_mm_end
                )
                OR
                (
                  NOT level_l BETWEEN
                  koef.dut_liter_beg
                  AND
                  koef.dut_liter_end
                )
              GROUP BY sensor_id, koef.equipment_model_id
          )
    select _valid.event_time, _valid.sensor_id, DUT_EXCEED_INVALID_FUEL
    from _valid join not_valid on (_valid.sensor_id = not_valid.sensor_id)
    where (not_valid.cnt/_valid.cnt)*100 > kdbo.jf_diagnostic_ref_pkg$get_dut_invalid_koeff(_valid.equipment_model_id);


    ----Превышено количество невалидных показаний ДУТ по уровню топлива
    insert into kdbo.diagnostic_protocol(dt_create, equipment_id, diagnostic_result_id)
    WITH raw_data AS
    (
        SELECT
          sensor_id,
          level_mm,
          level_l,
          temperature,
          equipment_model_id,
          event_time
        FROM snsr.fuel_sensor_data
              JOIN core.equipment ON fuel_sensor_data.sensor_id = equipment.equipment_id
        WHERE event_time BETWEEN p_time_start AND p_time_finish
    ),
    koef as (
       select kdbo.jf_diagnostic_ref_pkg$get_dut_temp_tank_beg(equipment_model_id) as dut_temp_tank_beg
              ,kdbo.jf_diagnostic_ref_pkg$get_dut_temp_tank_end(equipment_model_id) as dut_temp_tank_end
              ,equipment_model_id
       from core.equipment_model
    ),
    _valid as
      (
          SELECT
            sensor_id,
            count(*) cnt,
            max(event_time) event_time,
            raw_data.equipment_model_id
            FROM raw_data join koef on koef.equipment_model_id = raw_data.equipment_model_id
            GROUP BY sensor_id,  raw_data.equipment_model_id
      ),
    not_valid as
      (
          SELECT
            sensor_id,
            count(*) cnt,
            koef.equipment_model_id
          FROM raw_data join koef on koef.equipment_model_id = raw_data.equipment_model_id
          WHERE NOT temperature BETWEEN koef.dut_temp_tank_beg AND koef.dut_temp_tank_end
          GROUP BY sensor_id, koef.equipment_model_id
      )
    select _valid.event_time, _valid.sensor_id, DUT_EXCEED_INVALID_TEMP_TANK
    from _valid join not_valid on (_valid.sensor_id = not_valid.sensor_id)
    where (not_valid.cnt/_valid.cnt)*100 > kdbo.jf_diagnostic_ref_pkg$get_dut_invalid_koeff(_valid.equipment_model_id);

    return 1;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;


CREATE OR REPLACE FUNCTION kdbo.diag_sensor_fuel_pkg$of_cron_job()
  RETURNS int AS
$BODY$
declare
    l_ret int;
begin
    select kdbo.diag_sensor_fuel_pkg$process(current_date - INTERVAL '2 day', current_date - INTERVAL '1 day') into l_ret;
    return 1;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;