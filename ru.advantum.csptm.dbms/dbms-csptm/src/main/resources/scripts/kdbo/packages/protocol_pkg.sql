CREATE OR REPLACE FUNCTION kdbo.protocol_pkg$save_incident(p_json JSON)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE
  l_unit_id           BIGINT;
  l_event_type_id     SMALLINT;
  l_diagnostic_result INTEGER;
BEGIN
  l_unit_id := (p_json ->> 'unitId') :: BIGINT;
  l_event_type_id := (p_json ->> 'incidentTypeId') :: SMALLINT;
  l_diagnostic_result := kdbo.protocol_pkg$event_type2diagnostic_result(l_unit_id, l_event_type_id);

  IF l_diagnostic_result IS NULL
  THEN RETURN;
  END IF;

  INSERT INTO kdbo.diagnostic_protocol (dt_create, equipment_id, diagnostic_result_id) VALUES (
    (p_json ->> 'timeStart') :: TIMESTAMP,
    l_unit_id,
    l_diagnostic_result
  );
END;
$$;


CREATE OR REPLACE FUNCTION kdbo.protocol_pkg$save(p_json JSONB)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
  begin
    perform
     kdbo.protocol_pkg$save_incident(
        unnest(
             ttb.jf_incident_pkg$get_incident_array
             (
                 p_json::json -> 'incidentBundle'
             )
        )::json
     );
END;
$$;


CREATE OR REPLACE FUNCTION kdbo.protocol_pkg$event_type2diagnostic_result(p_unit_id BIGINT, p_event_type_id SMALLINT)
  RETURNS INTEGER
LANGUAGE plpgsql
AS $$
DECLARE
  l_equipment_type_id SMALLINT;
BEGIN
  SELECT em.equipment_type_id
  INTO l_equipment_type_id
  FROM core.equipment e
    JOIN core.equipment_model em ON e.equipment_model_id = em.equipment_model_id
  WHERE e.equipment_id = p_unit_id;

  IF l_equipment_type_id IS NULL
  THEN
    RETURN NULL;
  END IF;

  IF (p_event_type_id = mnt.jf_monitoring_event_pkg$cn_type_video_loss())
  THEN
    RETURN 14;
  ELSEIF (p_event_type_id = mnt.jf_monitoring_event_pkg$cn_type_pkgs_diag_ok())
    THEN
      RETURN 24;
  ELSEIF (p_event_type_id = mnt.jf_monitoring_event_pkg$cn_type_hdd_full())
    THEN
      RETURN 9;
  ELSEIF (p_event_type_id = mnt.jf_monitoring_event_pkg$cn_type_no_disk())
    THEN
      RETURN 10;
  ELSEIF (p_event_type_id = mnt.jf_monitoring_event_pkg$cn_type_fan_fail())
    THEN
      RETURN 11;
  ELSEIF (p_event_type_id = mnt.jf_monitoring_event_pkg$cn_type_all_video_loss())
    THEN
      RETURN 8;
  ELSEIF (p_event_type_id = mnt.jf_monitoring_event_pkg$cn_type_over_hdd_temp())
    THEN
      RETURN 12;
  ELSEIF (p_event_type_id = mnt.jf_monitoring_event_pkg$cn_type_hdd_fail())
    THEN
      RETURN 13;
  ELSEIF (p_event_type_id = mnt.jf_monitoring_event_pkg$cn_type_inv_pkgs_numbers())
    THEN
      IF (l_equipment_type_id = 2)
      THEN
        RETURN 3;
      ELSEIF (l_equipment_type_id = 9)
        THEN
          RETURN 4;
      END IF;
      RETURN 1;
  ELSEIF (p_event_type_id = mnt.jf_monitoring_event_pkg$cn_type_inv_pkgs_rate())
    THEN
      IF (l_equipment_type_id = 2)
      THEN
        RETURN 5;
      ELSEIF (l_equipment_type_id = 9)
        THEN
          RETURN 6;
      END IF;
  ELSEIF (p_event_type_id = mnt.jf_monitoring_event_pkg$cn_type_pkgs_time_rate())
    THEN
      IF (l_equipment_type_id = 2)
      THEN
        RETURN 1;
      ELSEIF (l_equipment_type_id = 9)
        THEN
          RETURN 2;
          ELSEIF (l_equipment_type_id = 10)
      THEN
      RETURN 7;
      END IF;
  ELSE
    RETURN NULL;
  END IF;
END;
$$;

-- функция, чтобы кое-как найти нужное ТС для оповещения 23
create or replace function kdbo.protocol_pkg$get_tr_by_eq(
  p_equipment_id core.equipment.equipment_id%type,
  p_dt           timestamp
)
  returns core.tr.tr_id%type
language plpgsql
as $$
declare
  l_tr_id core.tr.tr_id%type;
begin
  select tr_id
  into l_tr_id
  from hist.v_unit2tr_hist
  where unit_id = p_equipment_id
        and p_dt::timestamp with time zone <@ sys_period;

  if l_tr_id is not null
  then
    return l_tr_id;
  end if;

  select uh.tr_id
  into l_tr_id
  from hist.v_sensor2unit_hist sh
    join hist.v_unit2tr_hist uh on sh.unit_id = uh.unit_id
  where sh.sensor_id = p_equipment_id
        and p_dt::timestamp with time zone <@ sh.sys_period
        and p_dt::timestamp with time zone <@ uh.sys_period
  limit 1;

  return l_tr_id;
end;
$$;



