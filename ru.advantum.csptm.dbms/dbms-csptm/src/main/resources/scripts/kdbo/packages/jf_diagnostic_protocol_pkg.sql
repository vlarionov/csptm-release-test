CREATE OR REPLACE FUNCTION kdbo.jf_diagnostic_protocol_pkg$attr_to_rowtype(p_attr TEXT)
  RETURNS kdbo.DIAGNOSTIC_PROTOCOL
AS $$
DECLARE l_r kdbo.diagnostic_protocol%ROWTYPE;
BEGIN
  l_r.diagnostic_protocol_id := jofl.jofl_pkg$extract_number(p_attr, 'diagnostic_protocol_id', TRUE);
  l_r.equipment_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', TRUE);
  l_r.diagnostic_result_id := jofl.jofl_pkg$extract_number(p_attr, 'diagnostic_result_id', TRUE);
  l_r.dt_create := jofl.jofl_pkg$extract_date(p_attr, 'dt_create', TRUE);
  RETURN l_r;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION kdbo.jf_diagnostic_protocol_pkg$of_request(
  IN p_id_account NUMERIC,
  IN p_attr       TEXT)
  RETURNS BOOLEAN AS
$BODY$
DECLARE
  lbRc   BOOLEAN;
  lr_doc kdbo.diagnostic_protocol%ROWTYPE;
  lr_rr  core.repair_request%ROWTYPE;
  l_ret  TEXT;
BEGIN
  --Запись о ремонте
  lr_doc := kdbo.jf_diagnostic_protocol_pkg$attr_to_rowtype(p_attr);

  --  RAISE EXCEPTION USING MESSAGE = p_attr;
  /*определим, что у нас там с поломками*/
  SELECT bdp.breakdown_id
  INTO lr_rr.breakdown_id
  FROM core.breakdown_diagnostic_protocol bdp
  WHERE bdp.diagnostic_protocol_id = lr_doc.diagnostic_protocol_id;

  /*теперь с результатами диагностики*/
  SELECT dr.dgnt_result_desc
  INTO lr_rr.description
  FROM kdbo.diagnostic_result dr
  WHERE dr.diagnostic_result_id = lr_doc.diagnostic_result_id;

  SELECT
    e.equipment_id,
    e.facility_id,
    e.territory_id
  INTO
    lr_rr.equipment_id, lr_rr.facility_id, lr_rr.territory_id
  FROM core.equipment e
  WHERE e.equipment_id = lr_doc.equipment_id;

  l_ret := core.jf_repair_request_pkg$of_insert(p_id_account => p_id_account, p_attr => row_to_json(lr_rr) :: TEXT);

  RETURN lbRc;
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION kdbo.jf_diagnostic_protocol_pkg$of_request( NUMERIC, TEXT )
OWNER TO adv;

CREATE OR REPLACE FUNCTION kdbo.jf_diagnostic_protocol_pkg$of_breakdown(
  IN p_id_account NUMERIC,
  IN p_attr       TEXT)
  RETURNS BOOLEAN AS
$BODY$
DECLARE
  lbRc              BOOLEAN;
  brdn_attr         TEXT;
  brdn_row          core.breakdown%ROWTYPE;
  lr_doc            kdbo.diagnostic_protocol%ROWTYPE;
  lnExistsBreakDown INTEGER;
BEGIN
  --Запись о поломке

  lr_doc := kdbo.jf_diagnostic_protocol_pkg$attr_to_rowtype(p_attr);
  SELECT count(1)
  INTO lnExistsBreakDown
  FROM core.breakdown_diagnostic_protocol bdp
  WHERE bdp.diagnostic_protocol_id = lr_doc.diagnostic_protocol_id;
  IF lnExistsBreakDown = 0  THEN

    brdn_row.account_id := p_id_account;
    brdn_row.breakdown_type_id := NULL;
    brdn_row.equipment_id := lr_doc.equipment_id;
    brdn_row.dt_begin := lr_doc.dt_create;
    brdn_row.description := 'Создан по записи протокола диагностики ' || lr_doc.diagnostic_protocol_id :: VARCHAR;

    SELECT
      h.tr_id,
      e.territory_id
    INTO brdn_row.tr_id, brdn_row.territory_id
    FROM kdbo.diagnostic_protocol pr
      JOIN core.equipment2tr_h h ON h.equipment_id = pr.equipment_id AND pr.dt_create <@ h.sys_period
      JOIN core.equipment e ON e.equipment_id = pr.equipment_id
    WHERE pr.diagnostic_protocol_id = lr_doc.diagnostic_protocol_id;

    brdn_attr := row_to_json(brdn_row, TRUE) :: JSONB;

    brdn_attr := core."jf_breakdown_pkg$of_insert"(p_id_account, brdn_attr);

    INSERT INTO core.breakdown_diagnostic_protocol (breakdown_id, diagnostic_protocol_id)
    VALUES ((brdn_attr :: JSON ->> 'breakdown_id') :: BIGINT, lr_doc.diagnostic_protocol_id);
  ELSE
    RAISE EXCEPTION USING MESSAGE = '<<По протоколу уже создана запись о поломке!>>';
  END IF;
  RETURN lbRc;
END;

$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

ALTER FUNCTION kdbo.jf_diagnostic_protocol_pkg$of_request( NUMERIC, TEXT )
OWNER TO adv;
