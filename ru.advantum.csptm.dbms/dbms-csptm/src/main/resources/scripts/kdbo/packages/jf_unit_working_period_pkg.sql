CREATE OR REPLACE FUNCTION kdbo.jf_unit_working_period_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_begin_dt 		TIMESTAMP 	:=  coalesce(jofl.jofl_pkg$extract_date(p_attr, 'BEGIN_DT', TRUE), now()::date::timestamp );
  l_end_dt 			TIMESTAMP 	:=  coalesce(jofl.jofl_pkg$extract_date(p_attr, 'END_DT', TRUE), l_begin_dt + interval '1 day');
  larr_f_depo_id  	bigint[] 	:= jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', true);
  larr_f_tr_id    	bigint[] 	:= jofl.jofl_pkg$extract_narray(p_attr, 'f_list_tr_id', true);
  larr_f_tpbo_id    bigint[] 	:= jofl.jofl_pkg$extract_narray(p_attr, 'f_list_equipment_type_id', true);
  l_state 			int 		:= jofl.jofl_pkg$extract_number(p_attr, 'f_state_INPLACE_K', true);
begin
 open p_rows for
    with a as  (
        select incident_id as event_id
        	  ,unit_id
              ,tr_id
        	  ,time_start event_time_start
        	  ,lead(time_start) OVER (PARTITION BY unit_id, tr_id ORDER BY incident_id) as event_time_finish
        	  ,case 
            	when incident_type_id = kdbo.jf_unit_working_period_pkg$get_diag_ok()
            	  then 1
                  else 0
               end is_worked
      FROM ttb.incident
      WHERE incident_type_id = ANY (kdbo.jf_unit_working_period_pkg$get_diag_event_type_array())
      AND (tr_id = ANY (larr_f_tr_id) OR array_length(larr_f_tr_id, 1) IS NULL)
      AND incident.time_start BETWEEN l_begin_dt AND l_end_dt
    ),
    b as (SELECT row_number()
                 OVER (
                   ORDER BY unit_id, tr_id, event_time_start, event_id) - row_number()
                 OVER (PARTITION BY unit_id, tr_id, is_worked
                   ORDER BY unit_id, tr_id, event_time_start, event_id) gr_id,
                event_time_start,
                event_time_finish,
                event_id,
                is_worked,
                unit_id,
                tr_id
          FROM a
      ),
    c as (SELECT min(event_time_start) as event_time_start,
           max(event_time_finish) as event_time_finish,
           is_worked as is_worked,
           unit_id as unit_id,
           tr_id as tr_id,
           case is_worked
                when 0 then 'Нерабочее'
                else 'Рабочее'
           end as state,
           gr_id
    from b
    group by gr_id, unit_id, tr_id, is_worked
    ),
    d as (
    select
        c.event_time_start,
        c.event_time_finish,
        c.unit_id,
        c.tr_id,
        c.state,
        e.serial_num,
        min(c.event_time_start) over (partition by e.equipment_id) as eq_min_start,
        max(c.event_time_start) over (partition by e.equipment_id) as eq_max_start,
        tr.licence,
        em.name model_name,
        tr.garage_num,
        et.name_short eq_name_short,
        ed.name_full depo_name,
        ter.code ter_code,
        ent.name_short ter_name,
        --count(1) over (partition by c.gr_id, c.unit_id, c.tr_id, c.is_worked) as cnt,
          '[' ||
          json_build_object('CAPTION', (count(1) over (partition by e.equipment_id)::text)
       , 'JUMP',json_build_object('METHOD', 'kdbo.unit_working_period_detail'
       , 'ATTR' 
       , '{}' )) || ']' as cnt,
        c.event_time_start = max(c.event_time_start) over (partition by e.equipment_id) as is_max_dt,
        c.is_worked
    from c
    JOIN core.equipment e ON e.equipment_id = c.unit_id
    JOIN core.depo d ON d.depo_id = e.depo_id
    JOIN core.entity ed ON ed.entity_id = d.depo_id
    JOIN core.equipment_model em ON em.equipment_model_id = e.equipment_model_id
    JOIN core.equipment_type et ON et.equipment_type_id = em.equipment_type_id
    JOIN core.tr tr ON tr.tr_id = c.tr_id
    join core.depo2territory d2t on d.depo_id = d2t.depo_id
    join core.territory ter on ter.territory_id = d2t.territory_id 
    						and tr.territory_id = ter.territory_id
    join core.entity ent on ent.entity_id = ter.territory_id
    where (c.is_worked = l_state or l_state is null)
      and (d.depo_id = any (larr_f_depo_id) or array_length(larr_f_depo_id, 1) is null)
      and (et.equipment_type_id = any (larr_f_tpbo_id) or array_length(larr_f_tpbo_id, 1) is null)
    )
    select d.event_time_start,
          d.event_time_finish,
          d.eq_min_start,
          d.eq_max_start,
          d.unit_id,
          d.tr_id,
          d.state,
          d.serial_num,
          d.licence,
          d.model_name,
          d.garage_num,
          d.eq_name_short,
          d.depo_name,
          d.ter_code,
          d.ter_name,
          d.cnt,
          l_state as is_worked
    from d
    where d.is_max_dt
    order by d.event_time_start desc
    ;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION kdbo.jf_unit_working_period_pkg$get_diag_event_type_array()
  RETURNS INT[] AS
  'select ARRAY[14, 15, 16, 17];'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION kdbo.jf_unit_working_period_pkg$get_diag_ok()
  RETURNS INT AS
  'select 17::int;'
LANGUAGE SQL IMMUTABLE;