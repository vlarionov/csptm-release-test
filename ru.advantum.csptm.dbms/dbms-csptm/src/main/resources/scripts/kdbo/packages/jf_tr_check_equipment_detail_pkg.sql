CREATE OR REPLACE FUNCTION kdbo.jf_tr_check_equipment_detail_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_tr_check_equipment_id_list    bigint[]  := jofl.jofl_pkg$extract_tarray(p_attr, 'arr_tr_check_equipment_id', true);
begin 
 open p_rows for
     select
       cheq.tr_check_equipment_id,
       cheq.check_result_id,
       cheq.tr_id,
       cheq.dt_create,
       cheq.check_description,
       cheq.is_auto,
       not res.is_critical as is_passed,
       res.check_result_name,
       res.is_critical,
       tr.garage_num,
       depo.name_short as depo_name_short
     from kdbo.tr_check_equipment cheq
       join kdbo.check_result res on res.check_result_id = cheq.check_result_id
       join core.tr tr on tr.tr_id = cheq.tr_id
       join core.entity depo on depo.entity_id = tr.depo_id
     where cheq.tr_check_equipment_id = any(l_tr_check_equipment_id_list)
     order by cheq.dt_create desc
      ;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;