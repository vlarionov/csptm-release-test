CREATE OR REPLACE FUNCTION kdbo.jf_equipment_status_report_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                                       p_attr       TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $BODY$
DECLARE
  f_list_depo_id       BIGINT [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', TRUE);
  f_equipment_type_id  BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_type_id', TRUE);
  f_equipment_model_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_model_id', TRUE);

  --  {"f_equipment_type_id":"","f_equipment_model_id":"","f_list_depo_id":"$array[]"}
BEGIN
  OPEN p_rows FOR
  WITH etr AS (
    SELECT
      unit2tr.tr_id,
      1 AS id_type,
      et_1.equipment_id,
      et_1.firmware_id,
      et_1.unit_service_type_id,
      et_1.facility_id,
      et_1.equipment_status_id,
     -- et_1.decommission_reason_id,
      et_1.dt_begin,
     -- et_1.dt_end,
      et_1.serial_num,
      et_1.equipment_model_id,
      et_1.depo_id,
      et_1.territory_id,
      et_1.unit_service_facility_id,
      et_1.has_diagnostic
    FROM ((core.unit u
      JOIN core.unit2tr ON ((u.unit_id = unit2tr.unit_id)))
      JOIN core.equipment et_1 ON ((et_1.equipment_id = u.unit_id)))
    UNION ALL
    SELECT
      s2tr.tr_id,
      2 AS id_type,
      et_1.equipment_id,
      et_1.firmware_id,
      et_1.unit_service_type_id,
      et_1.facility_id,
      et_1.equipment_status_id,
     -- et_1.decommission_reason_id,
      et_1.dt_begin,
     -- et_1.dt_end,
      et_1.serial_num,
      et_1.equipment_model_id,
      et_1.depo_id,
      et_1.territory_id,
      et_1.unit_service_facility_id,
      et_1.has_diagnostic
    FROM ((core.sensor2tr s2tr
      JOIN core.sensor s ON ((s.sensor_id = s2tr.sensor_id)))
      JOIN core.equipment et_1 ON ((et_1.equipment_id = s2tr.sensor_id)))
    UNION ALL
    SELECT
      et2tr.tr_id,
      3 AS id_type,
      et_1.equipment_id,
      et_1.firmware_id,
      et_1.unit_service_type_id,
      et_1.facility_id,
      et_1.equipment_status_id,
     -- et_1.decommission_reason_id,
      et_1.dt_begin,
     -- et_1.dt_end,
      et_1.serial_num,
      et_1.equipment_model_id,
      et_1.depo_id,
      et_1.territory_id,
      et_1.unit_service_facility_id,
      et_1.has_diagnostic
    FROM (core.equipment2tr et2tr
      JOIN core.equipment et_1 ON ((et_1.equipment_id = et2tr.equipment_id)))
  )
  SELECT
    ent.name_short                                  depo_name,
    et.name_short                                   et_name_short,
    em.name                                         em_name,
    es.name                                         es_name,
    count(e.equipment_id)                           cnt_all,
    count(etr.equipment_id)                         cnt_tr,
    count(e.equipment_id) - count(etr.equipment_id) cnt_no_tr
  FROM core.equipment e
    JOIN core.equipment_model em ON em.equipment_model_id = e.equipment_model_id
    JOIN core.equipment_type et ON et.equipment_type_id = em.equipment_type_id
    JOIN core.entity ent ON ent.entity_id = e.depo_id
    JOIN core.equipment_status es ON es.equipment_status_id = e.equipment_status_id
    LEFT JOIN etr ON etr.equipment_id = e.equipment_id
  WHERE 1 = 1
        AND (e.depo_id = ANY (f_list_depo_id))
        AND (em.equipment_type_id = f_equipment_type_id OR f_equipment_type_id IS NULL)
        AND (em.equipment_model_id = f_equipment_model_id OR f_equipment_model_id IS NULL)
  GROUP BY ent.name_short
    , et.name_short
    , em.name
    , es.name;
END;
$BODY$;
