DROP FUNCTION kdbo.substitution_fund_units_report_pkg$of_rows( NUMERIC, TEXT );

CREATE OR REPLACE FUNCTION kdbo.substitution_fund_units_report_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                                           p_attr       TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $BODY$
DECLARE

  f_depo_id            BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', TRUE);
  f_tr_status_id       BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_status_id', TRUE);
  f_tr_capacity_id     BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_capacity_id', TRUE);
  f_tr_type_id         BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id', TRUE);
  /*
  •	Транспортное предприятие – выбор значений из Справочника подразделений (доступ определяется настройкой прав пользователя);
  •	Вид ТС – выбор значений из справочника Видов ТС;
  •	Вместимость ТС – выбор значений из справочника Вместимость ТС;
  •	Статус ТС – выбор значений из Справочника статусов ТС
  */

  l_available_depo_ids BIGINT [] := array(select adm.account_data_realm_pkg$get_depos_by_all(p_id_account));
BEGIN
  OPEN p_rows FOR
  WITH aData AS (SELECT
                   de.name_short depo_name,
                   et.name_short et_name,
                   'Всего'       what,
                   count(1)      cnt
                 FROM core.equipment e
                   JOIN core.equipment_model em ON em.equipment_model_id = e.equipment_model_id
                   JOIN core.equipment_type et ON et.equipment_type_id = em.equipment_type_id
                   JOIN core.entity de ON de.entity_id = e.depo_id
                 WHERE e.equipment_status_id <> 5 /*Выбыло*/
                       AND e.depo_id = ANY (l_available_depo_ids)
                 GROUP BY de.name_short, et.name_short
                 UNION ALL
                 SELECT
                   de.name_short      depo_name,
                   et.name_short      et_name,
                   'На сопровождении' what,
                   count(1)           cnt
                 FROM core.equipment e
                   JOIN core.equipment_model em ON em.equipment_model_id = e.equipment_model_id
                   JOIN core.equipment_type et ON et.equipment_type_id = em.equipment_type_id
                   JOIN core.entity de ON de.entity_id = e.depo_id
                   JOIN core.unit_service_type ust ON ust.unit_service_type_id = e.unit_service_type_id
                 WHERE e.equipment_status_id <> 5 /*Выбыло*/
                       AND e.unit_service_type_id IN (16, 17) /*Гарантийное обслуживание, Внешнее техническое обслуживание*/
                       AND e.depo_id = ANY (l_available_depo_ids)
                 GROUP BY de.name_short, et.name_short
                 UNION ALL
                 SELECT
                   de.name_short         depo_name,
                   et.name_short         et_name,
                   'Ремонт у Подрядчика' what,
                   count(1)              cnt
                 FROM core.equipment e
                   JOIN core.equipment_model em ON em.equipment_model_id = e.equipment_model_id
                   JOIN core.equipment_type et ON et.equipment_type_id = em.equipment_type_id
                   JOIN core.entity de ON de.entity_id = e.depo_id
                   JOIN core.unit_service_type ust ON ust.unit_service_type_id = e.unit_service_type_id
                 WHERE e.unit_service_type_id IN (16, 17) /*Гарантийное обслуживание, Внешнее техническое обслуживание*/
                       AND e.equipment_status_id = 4 /*Ремонт у Подрядчика*/
                       AND e.depo_id = ANY (l_available_depo_ids)
                 GROUP BY de.name_short, et.name_short
                 UNION ALL
                 SELECT
                   de.name_short depo_name,
                   et.name_short et_name,
                   'Неисправно'  what,
                   count(1)      cnt
                 FROM core.equipment e
                   JOIN core.equipment_model em ON em.equipment_model_id = e.equipment_model_id
                   JOIN core.equipment_type et ON et.equipment_type_id = em.equipment_type_id
                   JOIN core.entity de ON de.entity_id = e.depo_id
                   JOIN core.unit_service_type ust ON ust.unit_service_type_id = e.unit_service_type_id
                 WHERE e.unit_service_type_id IN (16, 17) /*Гарантийное обслуживание, Внешнее техническое обслуживание*/
                       AND e.equipment_status_id = 3 /*Неисправно*/
                       AND e.depo_id = ANY (l_available_depo_ids)
                 GROUP BY de.name_short, et.name_short
                 UNION ALL
                 SELECT
                   de.name_short depo_name,
                   et.name_short et_name,
                   'Исправное'   what,
                   count(1)      cnt
                 FROM core.equipment e
                   JOIN core.equipment_model em ON em.equipment_model_id = e.equipment_model_id
                   JOIN core.equipment_type et ON et.equipment_type_id = em.equipment_type_id
                   JOIN core.entity de ON de.entity_id = e.depo_id
                   JOIN core.unit_service_type ust ON ust.unit_service_type_id = e.unit_service_type_id
                 WHERE e.unit_service_type_id IN (16, 17) /*Гарантийное обслуживание, Внешнее техническое обслуживание*/
                       AND e.equipment_status_id IN (6, 2) /*Временно демонтировано Исправно*/
                       AND e.depo_id = ANY (l_available_depo_ids)
                 GROUP BY de.name_short, et.name_short
                 UNION ALL
                 SELECT
                   de.name_short  depo_name,
                   et.name_short  et_name,
                   'Не определен' what,
                   count(1)       cnt
                 FROM core.equipment e
                   JOIN core.equipment_model em ON em.equipment_model_id = e.equipment_model_id
                   JOIN core.equipment_type et ON et.equipment_type_id = em.equipment_type_id
                   JOIN core.entity de ON de.entity_id = e.depo_id
                   JOIN core.unit_service_type ust ON ust.unit_service_type_id = e.unit_service_type_id
                 WHERE e.unit_service_type_id IN (16, 17) /*Гарантийное обслуживание, Внешнее техническое обслуживание*/
                       AND e.equipment_status_id = 1 /*Не определен*/
                       AND e.depo_id = ANY (l_available_depo_ids)
                 GROUP BY de.name_short, et.name_short
    UNION ALL
                 SELECT
                   de.name_short              depo_name,
                   et.name_short              et_name,
                   'Процент на сопровождении' what,
                   round(100.0 * (
                     count((e.unit_service_type_id IN (16, 17)/*Гарантийное обслуживание, Внешнее техническое обслуживание*/
                            AND e.equipment_status_id IN (6, 2) /*Временно демонтировано Исправно*/) OR NULL) :: NUMERIC
                     /
                     count(1) :: NUMERIC), 1) cnt
                 FROM core.equipment e
                   JOIN core.equipment_model em ON em.equipment_model_id = e.equipment_model_id
                   JOIN core.equipment_type et ON et.equipment_type_id = em.equipment_type_id
                   JOIN core.entity de ON de.entity_id = e.depo_id
                   JOIN core.unit_service_type ust ON ust.unit_service_type_id = e.unit_service_type_id
                 WHERE 1 = 1
                      AND e.equipment_status_id <> 5 /*Выбыло*/
                      AND e.depo_id = ANY (l_available_depo_ids)
                 GROUP BY de.name_short, et.name_short
    UNION ALL
                 SELECT
                   de.name_short  depo_name,
                   et.name_short  et_name,
                   'Исправно не на ТС' what,
                   count((e.equipment_status_id = 2	/*Исправно*/
                     /*не на ТС*/
                     and not exists (select 1 from hist.v_unit2tr_hist ut where ut.unit_id = e.equipment_id and ut.sys_period@>now() ))
                         or NULL )    cnt1
                 FROM core.equipment e
                   JOIN core.equipment_model em ON em.equipment_model_id = e.equipment_model_id
                   JOIN core.equipment_type et ON et.equipment_type_id = em.equipment_type_id
                   JOIN core.entity de ON de.entity_id = e.depo_id
                   JOIN core.unit_service_type ust ON ust.unit_service_type_id = e.unit_service_type_id
                 WHERE 1=1
--                    AND e.unit_service_type_id IN (16, 17) /*Гарантийное обслуживание, Внешнее техническое обслуживание*/
                      AND e.depo_id = ANY (l_available_depo_ids)
                 GROUP BY de.name_short, et.name_short
  )
  SELECT
    aData.et_name,
    aData.depo_name,
    aData.what,
    aData.cnt
  FROM aData;

END;
$BODY$
