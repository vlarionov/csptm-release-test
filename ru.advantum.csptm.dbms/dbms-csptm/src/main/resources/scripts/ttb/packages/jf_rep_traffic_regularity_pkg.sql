CREATE OR REPLACE FUNCTION ttb.jf_rep_traffic_regularity_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare

begin

 open p_rows for 
     select
	null as depo_name, /* Наименование ТП */
 null as tr_type_name, /* Вид ТС */
 null as route_num, /* Номер маршрута */
 null as garage_num, /* Гар. номер ТС */
 null as dr_tab_num, /* Водитель */
 null as round_type, /* Тип рейса */
 null as irreg_round_prc, /* % рейсов, выполненных нерегулярно */
 null as prod_round_cnt_fact, /* Количество произв. рейсов факт. */
 null as reg_round_cnt, /* Количество рейсов, выполненных регулярно */
 null as reg_round_prc /* % рейсов, выполненных регулярно */	 
;
end;
$function$
;
