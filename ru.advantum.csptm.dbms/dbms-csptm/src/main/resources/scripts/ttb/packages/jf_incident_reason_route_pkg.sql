CREATE OR REPLACE FUNCTION ttb.jf_incident_reason_route_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$
declare
begin
 open p_rows for
    WITH RECURSIVE  tree as
    (
      select ir.incident_reason_id
        ,ir.incident_reason_parent_id
        ,ir.incident_reason_item_id
        ,ir.ir_code
      from ttb.incident_reason ir
      where ir.incident_reason_parent_id is null
            and ir.incident_reason_item_id = 111
      UNION ALL
      SELECT
        ir.incident_reason_id
        ,ir.incident_reason_parent_id
        ,ir.incident_reason_item_id
        ,ir.ir_code
      FROM
        tree
        JOIN ttb.incident_reason ir ON ir.incident_reason_parent_id = tree.incident_reason_id
        join ttb.incident_reason_item iri on ir.incident_reason_item_id = iri.incident_reason_item_id

    )
    select
      incident_reason.incident_reason_id
      ,incident_reason.incident_reason_parent_id
      ,coalesce(ir_code,'')||' '||incident_reason_item.name as name
      ,coalesce(ir_code,'')||' '||ttb.jf_incident_reason_pkg$get_name(incident_reason.incident_reason_id) full_name
    from tree incident_reason
      join ttb.incident_reason_item on incident_reason.incident_reason_item_id = incident_reason_item.incident_reason_item_id
    ORDER by ir_code ASC ;
end;
$$;


