CREATE OR REPLACE FUNCTION ttb."jf_norm_between_stop_filter_tr_type_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare
  l_norm_id ttb.norm.norm_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true);
begin
  open p_rows for
  select distinct on (nbs.tr_type_id) tt.name as tr_type_name
  from ttb.norm_between_stop nbs
  JOIN  core.tr_type tt on tt.tr_type_id = nbs.tr_type_id 
  WHERE nbs.norm_id = l_norm_id;
end;
$function$;