﻿CREATE OR REPLACE FUNCTION ttb.rv_tune_pkg$get_break_round_time (
  p_route_variant_id numeric
)
RETURNS integer AS
$body$
    declare
      l_res integer;
    begin

      select rvt.break_round_time
      into l_res
      from ttb.rv_tune rvt
      where rvt.route_variant_id = p_route_variant_id;
      
      if l_res is null then
         l_res := asd.get_constant('MAX_INTER_RACE_STOP');
      end if;
      
      return l_res;

    end;
$body$
LANGUAGE 'plpgsql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
