create or replace function ttb.agreement_notice_pkg$notify_group(
  p_tt_variant_id ttb.tt_variant.tt_variant_id%type
)
  returns void
language plpgsql
as $$
declare
  l_msg_template         text := 'Вам направлено на согласование расписание (№%s, действующее с %s, календарь %s) маршрута №%s вид ТС %s. Расписание должно быть рассмотрено до %s';
  l_current_agreement_id ttb.tt_variant_agreement.tt_variant_agreement_id%type;
begin
  l_current_agreement_id := ttb.agreement_pkg$get_current_agreement(p_tt_variant_id);
  perform ttb.agreement_notice_pkg$send_mail(
      a2g.account_id,
      format(
          l_msg_template,
          tv.ttv_name,
          ttb.agreement_notice_pkg$format_date(lower(tv.action_period)),
          ttb.agreement_pkg$get_calendar(tv.tt_variant_id),
          r.route_num,
          tt.name,
          ttb.agreement_notice_pkg$format_date(ttb.agreement_pkg$get_agreement_end_date(tva.tt_variant_agreement_id)))
  )
  from ttb.tt_variant_agreement tva
    join ttb.agreement_group ag on tva.agreement_group_id = ag.agreement_group_id
    join adm.account2group a2g on ag.group_id = a2g.group_id
    join ttb.tt_variant tv on tva.tt_variant_id = tv.tt_variant_id
    join rts.route_variant rv on tv.route_variant_id = rv.route_variant_id
    join rts.route r on rv.route_id = r.route_id
    join core.tr_type tt on r.tr_type_id = tt.tr_type_id
  where tva.tt_variant_agreement_id = l_current_agreement_id;
end;
$$;

create or replace function ttb.agreement_notice_pkg$notify_initiator(
  p_tt_variant_agreement_id ttb.tt_variant_agreement.tt_variant_agreement_id%type
)
  returns void
language plpgsql
as $$
declare
  l_msg_template_of_agree    text := 'Расписание (№%s, действующее с %s, календарь %s) маршрута №%s вид ТС %s согласовано.';
  l_msg_template_of_disagree text := 'Вам направлено на доработку расписание (№%s, действующее с %s, календарь %s) маршрута №%s вид ТС %s. Причина отказа: %s';
begin
  perform ttb.agreement_notice_pkg$send_mail(
      ttb.agreement_pkg$get_initiator(p_tt_variant_agreement_id),
      case when tva.resolution_id = ttb.resolution_pkg$agreed()
        then format(
            l_msg_template_of_agree,
            tv.ttv_name,
            ttb.agreement_notice_pkg$format_date(lower(tv.action_period)),
            ttb.agreement_pkg$get_calendar(tv.tt_variant_id),
            r.route_num,
            tt.name
        )
      else format(
          l_msg_template_of_disagree,
          tv.ttv_name,
          ttb.agreement_notice_pkg$format_date(lower(tv.action_period)),
          ttb.agreement_pkg$get_calendar(tv.tt_variant_id),
          r.route_num,
          tt.name,
          tva.comment
      )
      end
  )
  from ttb.tt_variant_agreement tva
    join ttb.tt_variant tv on tva.tt_variant_id = tv.tt_variant_id
    join rts.route_variant rv on tv.route_variant_id = rv.route_variant_id
    join rts.route r on rv.route_id = r.route_id
    join core.tr_type tt on r.tr_type_id = tt.tr_type_id
  where tva.tt_variant_agreement_id = p_tt_variant_agreement_id;
end;
$$;

create or replace function ttb.agreement_notice_pkg$notify_expiration(p_tt_variant_agreement_id ttb.tt_variant_agreement.tt_variant_agreement_id%type)
  returns void
language plpgsql
as $$
declare
  l_msg_template_for_expired_group text := 'ОБРАЩАЕМ ВАШЕ ВНИМАНИЕ: Расписание (№%s, действующее с %s, календарь %s) маршрута №%s вид ТС %s, ' ||
                                           'направленное Вам на согласование %s, не было рассмотрено в утвержденное время! Просрочка составляет %s дней.';
  l_msg_template_for_next_group    text := 'ОБРАЩАЕМ ВАШЕ ВНИМАНИЕ: Расписание (№%s, действующее с %s, календарь %s) маршрута №%s вид ТС %s, ' ||
                                           'направленное на согласование %s группе пользователей %s, не было рассмотрено в утвержденное время! Просрочка составляет %s дней.';
  l_msg_row                        record;
begin
  for l_msg_row in
  select
    tv.ttv_name,
    ttb.agreement_notice_pkg$format_date(lower(tv.action_period))                                                               ttv_begin_date,
    ttb.agreement_pkg$get_calendar(tv.tt_variant_id)                                                                            ttv_calendar,
    r.route_num                                                                                                                 ttv_route_num,
    tt.name                                                                                                                     ttv_tr_type,
    ttb.agreement_notice_pkg$format_date(previous_tva.agreement_date)                                                           agreement_begin_date,
    ceil((extract(epoch from (now() - ttb.agreement_pkg$get_agreement_end_date(current_tva.tt_variant_agreement_id))) / 86400)) agreement_delay,
    current_ag.group_id                                                                                                         current_group_id,
    next_ag.group_id                                                                                                            next_group_id
  from ttb.tt_variant_agreement current_tva
    join ttb.agreement_group current_ag on current_tva.agreement_group_id = current_ag.agreement_group_id
    join ttb.tt_variant_agreement previous_tva on ttb.agreement_pkg$get_previous_agreement(current_tva.tt_variant_agreement_id) = previous_tva.tt_variant_agreement_id
    join ttb.tt_variant_agreement next_tva on ttb.agreement_pkg$get_next_agreement(current_tva.tt_variant_agreement_id) = next_tva.tt_variant_agreement_id
    join ttb.agreement_group next_ag on next_tva.agreement_group_id = next_ag.agreement_group_id
    join ttb.tt_variant tv on current_tva.tt_variant_id = tv.tt_variant_id
    join rts.route_variant rv on tv.route_variant_id = rv.route_variant_id
    join rts.route r on rv.route_id = r.route_id
    join core.tr_type tt on r.tr_type_id = tt.tr_type_id
  where current_tva.tt_variant_agreement_id = p_tt_variant_agreement_id
  loop
    perform ttb.agreement_notice_pkg$send_mail(
        a2g.account_id,
        format(
            l_msg_template_for_expired_group,
            l_msg_row.ttv_name,
            l_msg_row.ttv_begin_date,
            l_msg_row.ttv_calendar,
            l_msg_row.ttv_route_num,
            l_msg_row.ttv_tr_type,
            l_msg_row.agreement_begin_date,
            l_msg_row.agreement_delay
        )
    )
    from adm.account2group a2g
    where l_msg_row.current_group_id = a2g.group_id;

    perform ttb.agreement_notice_pkg$send_mail(
        a2g.account_id,
        format(
            l_msg_template_for_next_group,
            l_msg_row.ttv_name,
            l_msg_row.ttv_begin_date,
            l_msg_row.ttv_calendar,
            l_msg_row.ttv_route_num,
            l_msg_row.ttv_tr_type,
            l_msg_row.agreement_begin_date,
            (
              select g.name
              from adm.group g
              where l_msg_row.current_group_id = g.group_id
            ),
            l_msg_row.agreement_delay
        )
    )
    from adm.account2group a2g
    where l_msg_row.next_group_id = a2g.group_id;
  end loop;
end;
$$;

create or replace function ttb.agreement_notice_pkg$format_date(
  p_date timestamp
)
  returns text
language plpgsql
as $$
begin
  return to_char(p_date, 'dd.MM.yyyy');
end;
$$;

create or replace function ttb.agreement_notice_pkg$send_mail(
  p_account_id core.account.account_id%type,
  p_text       text
)
  returns void
language plpgsql
as $$
declare
  l_subject text := 'Изменение статуса варианта расписания';
begin
  perform paa.paa_api_pkg$basic_publish(
      1 :: smallint,
      '',
      'mail-service',
      json_build_object(
          'to', a.email,
          'subject', l_subject,
          'body', p_text
      ) :: text,
      'RMQMail'
  )
  from core.account a
  where a.account_id = p_account_id and a.email is not null;
end;
$$;