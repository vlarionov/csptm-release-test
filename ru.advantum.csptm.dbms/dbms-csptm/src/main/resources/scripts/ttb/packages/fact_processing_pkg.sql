drop function ttb.fact_processing_pkg$process_stop_item_pass(p_tr_id core.tr.tr_id%type, p_tt_action_id ttb.tt_action.tt_action_id%type, p_round_id rts.round.round_id%type, p_stop_item_id rts.stop_item.stop_item_id%type, p_stop_order_num rts.stop_item2round.order_num%type, p_pass_time ttb.tt_action_fact.time_fact_begin%type );
drop function ttb.fact_processing_pkg$process_stop_item_pass_json(p_json in json );
drop function ttb.fact_processing_pkg$process_tt_action_complete(p_tt_action_id ttb.tt_action.tt_action_id%type, p_round_id rts.round.round_id%type, p_status text, p_time_packet_begin core.traffic.event_time%type, p_packet_id_begin core.traffic.packet_id%type, p_time_packet_end core.traffic.event_time%type, p_packet_id_end core.traffic.packet_id%type, p_first_graph_section asd.graph_section_speed_ttb.begin_time%type, p_last_graph_section asd.graph_section_speed_ttb.begin_time%type, p_time_fact_begin ttb.tt_action_fact.time_fact_begin%type, p_time_fact_end ttb.tt_action_fact.time_fact_begin%type );
drop function ttb.fact_processing_pkg$process_tt_action_complete_json(p_json in json );

create or replace function ttb.fact_processing_pkg$process_stop_item_pass(
    p_tr_id          core.tr.tr_id%type,
    p_tt_action_id   ttb.tt_action.tt_action_id%type,
    p_round_id       rts.round.round_id%type,
    p_stop_item_id   rts.stop_item.stop_item_id%type,
    p_stop_order_num rts.stop_item2round.order_num%type,
    p_pass_time      ttb.tt_action_fact.time_fact_begin%type
)
    returns void as
$$
declare
    l_stop_item2round_id rts.stop_item2round.stop_item2round_id%type;
begin
    select stop_item2round_id
    into strict l_stop_item2round_id
    from rts.stop_item2round si2r
    where si2r.round_id = p_round_id
          and si2r.order_num = p_stop_order_num;

    insert into ttb.tt_action_fact (tt_action_id, stop_item2round_id, time_fact_begin, time_fact_end)
    values (p_tt_action_id, l_stop_item2round_id, p_pass_time, p_pass_time)
    on conflict (tt_action_id, stop_item2round_id)
        do update set
            time_fact_begin = excluded.time_fact_begin,
            time_fact_end   = excluded.time_fact_end;

    if p_stop_order_num = 1 and p_pass_time is not null
    then
        update ttb.tt_action_round
        set round_status_id = ttb.round_status_pkg$in_progress(),
            time_fact_begin = p_pass_time
        where tt_action_id = p_tt_action_id
              -- "начать" можно только запланированный рейс.
              -- это обезопасит от случаев, когда из-за баги в graph-linker один и тот же рейс два раза прогоняли
              and round_status_id = ttb.round_status_pkg$planned();
    end if;
end;
$$
language 'plpgsql'
volatile
called on null input
security invoker;

create or replace function ttb.fact_processing_pkg$process_stop_item_pass_json(p_json in json)
    returns void as
$$
declare
    l_json_item json;
begin
    for l_json_item in select json_array_elements(p_json) loop
        perform ttb.fact_processing_pkg$process_stop_item_pass((l_json_item ->> 'trId') :: bigint,
                                                               (l_json_item ->> 'ttActionId') :: int,
                                                               (l_json_item ->> 'roundId') :: int,
                                                               (l_json_item ->> 'stopItemId') :: int,
                                                               (l_json_item ->> 'stopOrderNum') :: smallint,
                                                               (l_json_item ->> 'passTime') :: timestamp);
    end loop;
end;
$$
language 'plpgsql'
volatile
called on null input
security invoker;

create or replace function ttb.fact_processing_pkg$process_tt_action_complete(
    p_tt_action_id        ttb.tt_action.tt_action_id%type,
    p_round_id            rts.round.round_id%type,
    p_status              text,
    p_time_packet_begin   core.traffic.event_time%type,
    p_packet_id_begin     core.traffic.packet_id%type,
    p_time_packet_end     core.traffic.event_time%type,
    p_packet_id_end       core.traffic.packet_id%type,
    p_first_graph_section asd.graph_section_speed_ttb.begin_time%type,
    p_last_graph_section  asd.graph_section_speed_ttb.begin_time%type,
    p_time_fact_begin     ttb.tt_action_fact.time_fact_begin%type,
    p_time_fact_end       ttb.tt_action_fact.time_fact_begin%type
)
    returns void as
$$
declare
    l_round_status_id ttb.round_status.round_status_id%type;
begin
    if p_status is null or p_status = 'ROUND_END_ERROR'
    then
        l_round_status_id := ttb.round_status_pkg$error();
    else
        l_round_status_id := ttb.round_status_pkg$passed();
    end if;

    update ttb.tt_action_round
    set
        round_status_id     = l_round_status_id,
        time_packet_begin   = p_time_packet_begin,
        packet_id_begin     = p_packet_id_begin,
        time_packet_end     = p_time_packet_end,
        packet_id_end       = p_packet_id_end,
        first_graph_section = p_first_graph_section,
        last_graph_section  = p_last_graph_section,
        time_fact_begin     = p_time_fact_begin,
        time_fact_end       = p_time_fact_end
    where
        tt_action_id = p_tt_action_id
        -- "закончить" можно только еще не законченный рейс.
        -- это обезопасит от случаев, когда из-за баги в graph-linker один и тот же рейс два раза прогоняли
        and round_status_id != ttb.round_status_pkg$passed();
end;
$$
language 'plpgsql'
volatile
called on null input
security invoker;

create or replace function ttb.fact_processing_pkg$process_tt_action_complete_json(p_json in json)
    returns void as
$$
declare
    l_json_item json;
begin
    for l_json_item in select json_array_elements(p_json) loop
        perform ttb.fact_processing_pkg$process_tt_action_complete(
            (l_json_item ->> 'ttActionId') :: int,
            (l_json_item ->> 'roundId') :: int,
            (l_json_item -> 'eventEnd' ->> 'type'),
            (l_json_item -> 'eventBegin' -> 'simpleUnitPacket' ->> 'eventTime') :: timestamp,
            (l_json_item -> 'eventBegin' -> 'simpleUnitPacket' ->> 'packetId') :: bigint,
            (l_json_item -> 'eventEnd' -> 'simpleUnitPacket' ->> 'eventTime') :: timestamp,
            (l_json_item -> 'eventEnd' -> 'simpleUnitPacket' ->> 'packetId') :: bigint,
            (l_json_item -> 'graphSectionBegin' ->> 'startTime') :: timestamp,
            (l_json_item -> 'graphSectionEnd' ->> 'startTime') :: timestamp,
            (l_json_item -> 'stopItemBegin' ->> 'passTime') :: timestamp,
            (l_json_item -> 'stopItemEnd' ->> 'passTime') :: timestamp
        );
    end loop;
end;
$$
language 'plpgsql'
volatile
called on null input
security invoker;