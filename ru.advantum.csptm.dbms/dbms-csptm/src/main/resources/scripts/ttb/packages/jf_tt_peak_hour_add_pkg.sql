CREATE OR REPLACE FUNCTION ttb.jf_tt_peak_hour_add_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 ln_tt_variant_id 	bigint := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
 ln_cnt				smallint;
begin  
 
  select count(1)
 	into ln_cnt
        from ttb.rv_peak_hour t
        join ttb.calendar_tag ctag on ctag.calendar_tag_id = t.calendar_tag_id
        join ttb.tt_calendar cl on ctag.calendar_tag_id = cl.calendar_tag_id    
        where cl.tt_variant_id = ln_tt_variant_id 
        limit 1;
  if ln_cnt > 0 then /*Используем часы пик из вариантов маршрута*/
  
     open p_rows for 
          select 
            t.rv_peak_hour_id   AS peak_hour_id,
            ctag.tag_name       AS ph_name,
            t.route_variant_id,
            t.time_begin,
            ctag.calendar_tag_id,
            t.time_end,
            t.peak_hour_name
          from ttb.rv_peak_hour t
          join ttb.calendar_tag ctag on ctag.calendar_tag_id = t.calendar_tag_id
          join ttb.tt_calendar cl on ctag.calendar_tag_id = cl.calendar_tag_id    
          where cl.tt_variant_id = ln_tt_variant_id;        
  else	/*Используем часы пик общесистемные*/
      
   open p_rows for 
        select 
          ttph.tune_peak_hour_id   AS peak_hour_id,
          ctag.tag_name       	   AS ph_name,
          null 					   AS route_variant_id,
          ttph.time_begin,
          ctag.calendar_tag_id,
          ttph.time_end,
          ttph.peak_hour_name
        from ttb.tune_peak_hour ttph
        join ttb.calendar_tag ctag on ctag.calendar_tag_id = ttph.calendar_tag_id
        join ttb.tt_calendar cl on ctag.calendar_tag_id = cl.calendar_tag_id    
        where cl.tt_variant_id = ln_tt_variant_id; 
  end if;
  
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;