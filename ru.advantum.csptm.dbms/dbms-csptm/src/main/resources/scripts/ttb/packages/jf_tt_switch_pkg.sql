CREATE OR REPLACE FUNCTION ttb.jf_tt_switch_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.tt_switch AS
$body$
declare 
   l_r 				ttb.tt_switch%rowtype; 
   ld_dt_action 	date;
   ln_sec_b  		int:= jofl.jofl_pkg$extract_number(p_attr, 'sec_b', true);
   ln_tt_variant_id	ttb.tt_variant.tt_variant_id%type;
begin 
   l_r.tt_switch_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_switch_id', true); 
   l_r.tt_schedule_item_id_from := jofl.jofl_pkg$extract_number(p_attr, 'tt_schedule_item_id_from', true); 
   l_r.tt_schedule_id_to := jofl.jofl_pkg$extract_number(p_attr, 'tt_schedule_id_to', true); 
   l_r.tt_tr_period_id_from := jofl.jofl_pkg$extract_number(p_attr, 'tt_tr_period_id_from', true); 
   l_r.tt_tr_period_id_to := jofl.jofl_pkg$extract_number(p_attr, 'tt_tr_period_id_to', true); 
   /*l_r.switch_time := jofl.jofl_pkg$extract_date(p_attr, 'switch_time', true); */
   
   select case 
   			when ttv.parent_tt_variant_id is null 
              then '1900-01-01'::date 
            else lower(ttv.action_period)::date 
   		  end dt_action
         ,ttv.tt_variant_id 
   into ld_dt_action
   	   ,ln_tt_variant_id
   from ttb.tt_schedule sch
   join ttb.tt_variant ttv on ttv.tt_variant_id = sch.tt_variant_id
   where sch.tt_schedule_id = l_r.tt_schedule_id_to
   limit 1;
   
   l_r.switch_time := ld_dt_action + ln_sec_b * interval '1 sec';
   
   if not ttb.jf_tt_variant_pkg$of_get_policy(ln_tt_variant_id)
    then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if; 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_switch_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r 						ttb.tt_switch%rowtype;
   ln_tt_variant_id_from 	ttb.tt_switch_group.tt_variant_id_from%type;
   ln_tt_variant_id_to 		ttb.tt_switch_group.tt_variant_id_to%type;
   ln_cnt					smallint;
begin 
   l_r := ttb.jf_tt_switch_pkg$attr_to_rowtype(p_attr);
   
   select  ttvf.tt_variant_id 
          ,ttvt.tt_variant_id 
   into ln_tt_variant_id_from,
   		ln_tt_variant_id_to
   from ttb.tt_switch sw
   join ttb.tt_schedule_item sif on sif.tt_schedule_item_id = sw.tt_schedule_item_id_from
   join ttb.tt_schedule sf on sf.tt_schedule_id = sif.tt_schedule_id
   join ttb.tt_variant ttvf on ttvf.tt_variant_id = sf.tt_variant_id
   join ttb.tt_schedule st on st.tt_schedule_id = sw.tt_schedule_id_to
   join ttb.tt_variant ttvt on ttvt.tt_variant_id = st.tt_variant_id
   where sw.tt_switch_id = l_r.tt_switch_id;
   
   with denied_del as (
   		  select ttvf.tt_variant_id as tt_variant_id_from
            	,ttvt.tt_variant_id as tt_variant_id_to
          	    ,count(1) as cnt
            from ttb.tt_switch sw
            join ttb.tt_schedule_item sif on sif.tt_schedule_item_id = sw.tt_schedule_item_id_from
            join ttb.tt_schedule sf on sf.tt_schedule_id = sif.tt_schedule_id
            join ttb.tt_variant ttvf on ttvf.tt_variant_id = sf.tt_variant_id
            join ttb.tt_schedule st on st.tt_schedule_id = sw.tt_schedule_id_to
            join ttb.tt_variant ttvt on ttvt.tt_variant_id = st.tt_variant_id
            group by ttvf.tt_variant_id
                    ,ttvt.tt_variant_id 
            having count(1) > 1
            )
   delete from ttb.tt_switch_group dsg
   where dsg.tt_variant_id_from = ln_tt_variant_id_from
     and dsg.tt_variant_id_to = ln_tt_variant_id_to
     and not exists (select null from denied_del dd
      				 where dd.tt_variant_id_from = dsg.tt_variant_id_from
                       and dd.tt_variant_id_to = dsg.tt_variant_id_to);
   
   delete from ttb.tt_switch where  tt_switch_id = l_r.tt_switch_id;
   
   delete from ttb.tt_tr_period where tt_tr_period_id = l_r.tt_tr_period_id_to and not is_own;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_switch_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r 				ttb.tt_switch%rowtype;
   ln_tr_id 		ttb.tt_tr_period.tt_tr_id%type;
   ln_tt_period_id 	ttb.tt_tr_period.tt_period_id%type;
   ln_ord_num		smallint := 1;
   lb_is_own		boolean;
   ln_tt_variant_id_to	ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id_to', true); 
begin 
   l_r := ttb.jf_tt_switch_pkg$attr_to_rowtype(p_attr);
   
   select p.tt_period_id
     into ln_tt_period_id
     from ttb.tt_period p
    where p.tt_schedule_id = l_r.tt_schedule_id_to
      and l_r.switch_time <@ tsrange(p.time_begin, p.time_end);
    
   if ln_tt_period_id is null
   	then RAISE EXCEPTION '<<Выбранное время переключения отсутствует в выбранной последовательности расписания.>>';
   end if;
   
   select ttr.tt_tr_id
   		 ,ttr.is_own
     into ln_tr_id
     	 ,lb_is_own
     from ttb.tt_tr_period ttr
    where ttr.tt_tr_period_id = l_r.tt_tr_period_id_from
    ;   
    
   select sw.tt_tr_period_id_from
     into l_r.tt_tr_period_id_to
     from ttb.tt_switch sw
     join ttb.tt_schedule_item sif on sif.tt_schedule_item_id = sw.tt_schedule_item_id_from
     join ttb.tt_schedule sf on sf.tt_schedule_id = sif.tt_schedule_id
     join ttb.tt_variant ttvf on ttvf.tt_variant_id = sf.tt_variant_id
     join ttb.tt_schedule st on st.tt_schedule_id = sw.tt_schedule_id_to
     join ttb.tt_variant ttvt on ttvt.tt_variant_id = st.tt_variant_id
     join ttb.tt_switch_group tsg on tsg.tt_variant_id_to = ttvt.tt_variant_id
                                 and tsg.tt_variant_id_from = ttvf.tt_variant_id
                                 and sw.tt_tr_period_id_to = l_r.tt_tr_period_id_from
                                 and tsg.tt_variant_id_from = ln_tt_variant_id_to;
  
  if lb_is_own or (not lb_is_own and l_r.tt_tr_period_id_to is null) then  /*Свой или чужой-невозврат*/       
        select count(1) + 1 
          into ln_ord_num
          from ttb.tt_tr_period ttp 
          join ttb.tt_period tp on ttp.tt_period_id = tp.tt_period_id
          join ttb.tt_schedule sch on sch.tt_schedule_id = tp.tt_schedule_id
          where sch.tt_schedule_id in (      
              select sch.tt_schedule_id
                    from ttb.tt_period tp 
                    join ttb.tt_schedule sch on sch.tt_schedule_id = tp.tt_schedule_id
                    where tp.tt_period_id = ln_tt_period_id  
                    );  
          
       insert into ttb.tt_tr_period (tt_period_id
                                     ,tt_tr_id
                                     ,tt_tr_order_num
                                     ,is_own)
                   select  ln_tt_period_id
                            ,ln_tr_id
                            ,ln_ord_num
                            ,false
                            where not exists (select 1 from ttb.tt_tr_period ttp where (ttp.tt_tr_id, ttp.tt_tr_order_num, ttp.is_own)=
                                                                                      (ln_tr_id ,ln_ord_num ,false))
          returning tt_tr_period_id
         into l_r.tt_tr_period_id_to;

    if l_r.tt_tr_period_id_to is null then
      select ttp.tt_tr_period_id
      into l_r.tt_tr_period_id_to
      from ttb.tt_tr_period ttp
      where (ttp.tt_tr_id, ttp.tt_tr_order_num, ttp.is_own)=
            (ln_tr_id ,ln_ord_num ,false);
    end if;
  end if;

   with ins_tt_switch as (
       insert into ttb.tt_switch ( tt_schedule_item_id_from
                                  ,tt_schedule_id_to
                                  ,tt_tr_period_id_from
                                  ,tt_tr_period_id_to
                                  ,switch_time
                                  )
                          values ( l_r.tt_schedule_item_id_from
                                  ,l_r.tt_schedule_id_to
                                  ,l_r.tt_tr_period_id_from
                                  ,l_r.tt_tr_period_id_to
                                  ,l_r.switch_time
                                  )
      returning *
      )
      ,prep_ins_switch_gr as (
      			  select ttvf.tt_variant_id as tt_variant_id_from
              			,ttvt.tt_variant_id as tt_variant_id_to
                from ins_tt_switch isw
                join ttb.tt_schedule_item sif on sif.tt_schedule_item_id = isw.tt_schedule_item_id_from
                join ttb.tt_schedule sf on sf.tt_schedule_id = sif.tt_schedule_id
                join ttb.tt_variant ttvf on ttvf.tt_variant_id = sf.tt_variant_id
                join ttb.tt_schedule st on st.tt_schedule_id = isw.tt_schedule_id_to
                join ttb.tt_variant ttvt on ttvt.tt_variant_id = st.tt_variant_id
      )                             
       insert into ttb.tt_switch_group ( tt_variant_id_from
										,tt_variant_id_to )
       select tt_variant_id_from
       		 ,tt_variant_id_to
       from prep_ins_switch_gr pisg
       where not exists (select null from ttb.tt_switch_group tsg
       								where tsg.tt_variant_id_from = pisg.tt_variant_id_from
                                      and tsg.tt_variant_id_to = pisg.tt_variant_id_to) 
        ;                           

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_switch_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.tt_switch%rowtype;
   ln_tt_period_id 	ttb.tt_tr_period.tt_period_id%type;
begin 
   l_r := ttb.jf_tt_switch_pkg$attr_to_rowtype(p_attr);
   /*
   select p.tt_period_id
     into ln_tt_period_id
     from ttb.tt_period p
    where p.tt_schedule_id = l_r.tt_schedule_id_to
      and l_r.switch_time <@ tsrange(p.time_begin, p.time_end);
    
   if ln_tt_period_id is null
   	then RAISE EXCEPTION '<<Выбранное время переключения отсутствует в выбранной последовательности расписания.>>';
   end if;

   update ttb.tt_switch set 
          tt_schedule_item_id_from = l_r.tt_schedule_item_id_from, 
          tt_schedule_id_to = l_r.tt_schedule_id_to, 
          tt_tr_period_id_from = l_r.tt_tr_period_id_from, 
          tt_tr_period_id_to = l_r.tt_tr_period_id_to, 
          switch_time = l_r.switch_time
   where 
          tt_switch_id = l_r.tt_switch_id;
	*/
   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_switch_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 ln_tt_schedule_id ttb.tt_schedule.tt_schedule_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_schedule_id', true); 
begin 
 open p_rows for 
  select 
        sw.tt_switch_id, 
        sw.tt_schedule_item_id_from, 
        sw.tt_schedule_id_to, 
        sw.tt_tr_period_id_from, 
        sw.tt_tr_period_id_to, 
        sw.switch_time,
        extract (epoch from sw.switch_time-ttb.jf_incident_pkg$cn_plan_tt_date()) as sec_b,
        tagf.tt_action_group_name,
        substring(mf.mode_name from 1 for 1) || lpad(trperf.tt_tr_order_num::text, 2, 0::text) as out_num_from,
        substring(mt.mode_name from 1 for 1) || lpad(trpert.tt_tr_order_num::text, 2, 0::text) as out_num_to,
        ttv.tt_variant_id as tt_variant_id_to, 
        ttv.ttv_name,
        scht.tt_schedule_name as tt_schedule_name_to
      from ttb.tt_switch sw
      join ttb.tt_schedule_item shif on shif.tt_schedule_item_id = sw.tt_schedule_item_id_from
      join ttb.tt_action_group tagf on tagf.tt_action_group_id = shif.tt_action_group_id
   	  join ttb.tt_tr_period  trperf on trperf.tt_tr_period_id = sw.tt_tr_period_id_from
   	  join ttb.tt_period tpf on tpf.tt_period_id = trperf.tt_period_id
   	  join ttb.tt_tr tt_trf on tt_trf.tt_tr_id = trperf.tt_tr_id
   	  join ttb.mode mf on mf.mode_id = tt_trf.tr_mode_id
   	  join ttb.tt_tr_period  trpert on trpert.tt_tr_period_id = sw.tt_tr_period_id_to
   	  join ttb.tt_period tpt on tpt.tt_period_id = trpert.tt_period_id
   	  join ttb.tt_tr tt_trt on tt_trt.tt_tr_id = trpert.tt_tr_id
   	  join ttb.mode mt on mt.mode_id = tt_trt.tr_mode_id  
      join ttb.tt_schedule scht on scht.tt_schedule_id = sw.tt_schedule_id_to   
      join ttb.tt_variant ttv on ttv.tt_variant_id = scht.tt_variant_id 
      where sw.tt_schedule_item_id_from in (select tt_schedule_item_id
      									   from ttb.tt_schedule_item
                                           where tt_schedule_id = ln_tt_schedule_id ) ; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------------------------