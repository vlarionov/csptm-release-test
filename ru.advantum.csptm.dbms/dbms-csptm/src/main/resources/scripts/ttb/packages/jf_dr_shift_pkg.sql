
do $$
begin
  if not exists(select * from pg_type where typname = 'jf_dr_mode_type') THEN

    CREATE TYPE ttb.jf_dr_mode_type AS
      (r_shift ttb.dr_shift,
       r_dr_mode ttb.dr_mode);
    
    ALTER TYPE ttb.jf_dr_mode_type  OWNER TO adv;
    
  end if;
end$$;

---------------------------------------------------------------

--DROP FUNCTION ttb."jf_dr_shift_pkg$attr_to_rowtype"(text) ;
CREATE OR REPLACE FUNCTION ttb.jf_dr_shift_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.dr_shift AS
$body$
declare
  l_r ttb.dr_shift%rowtype;
   
begin 
  l_r.dr_shift_id 			:= jofl.jofl_pkg$extract_varchar(p_attr, 'dr_shift_id', true);
  l_r.mode_id 				:= jofl.jofl_pkg$extract_number(p_attr, 'mode_id', true);
  l_r.dr_shift_num 			:= jofl.jofl_pkg$extract_number(p_attr, 'dr_shift_num', true);
  l_r.dr_shift_name 		:= jofl.jofl_pkg$extract_varchar(p_attr, 'dr_shift_name', true);
  l_r.break_count_min 		:= jofl.jofl_pkg$extract_varchar(p_attr, 'break_count_min', true);
  l_r.break_count_max 		:= jofl.jofl_pkg$extract_varchar(p_attr, 'break_count_max', true);
  l_r.before_time_max 		:= jofl.jofl_pkg$extract_number(p_attr, 'before_time_max', true);
  l_r.before_time_min 		:= jofl.jofl_pkg$extract_number(p_attr, 'before_time_min', true);
  l_r.after_time_max 		:= jofl.jofl_pkg$extract_number(p_attr, 'after_time_max', true);
  /*l_r.break_duration_min 	:= jofl.jofl_pkg$extract_number(p_attr, 'break_duration_min', true);*/
  l_r.break_duration_max 	:= jofl.jofl_pkg$extract_number(p_attr, 'break_duration_max', true);
  l_r.stop_time 			:= jofl.jofl_pkg$extract_number(p_attr, 'stop_time', true);
  l_r.work_duration_min 	:= jofl.jofl_pkg$extract_number(p_attr, 'work_duration_min', true);
  l_r.work_duration_max 	:= jofl.jofl_pkg$extract_number(p_attr, 'work_duration_max', true);  
  l_r.work_break_between_min := jofl.jofl_pkg$extract_number(p_attr, 'work_break_between_min', true);  
  l_r.work_break_between_max := jofl.jofl_pkg$extract_number(p_attr, 'work_break_between_max', true);  
  l_r.before_time_short_break_max := jofl.jofl_pkg$extract_number(p_attr, 'before_time_short_break_max', true);

  if l_r.before_time_min < 1 or
     l_r.before_time_max < 1 or
     l_r.after_time_max < 1 or
     l_r.break_duration_max < 1 or
     l_r.stop_time < 1 or
     l_r.work_break_between_min < 1 or
     l_r.before_time_short_break_max < 1
  then
    raise exception '<<Проверьте введенные данные. В обязательных полях нельзя указать время = 0. >>';
  end if;
  
  if l_r.work_duration_max < l_r.work_duration_min or
     l_r.break_count_max < l_r.break_count_min or
     l_r.work_break_between_max < l_r.work_break_between_min or
     /*l_r.break_duration_max <l_r.break_duration_min  or*/
     l_r.before_time_max <l_r.before_time_min
  then
     raise exception '<<Проверьте введенные данные. Максимальная продолжительность должна быть больше или равна минимальной. >>';
  end if;

  return l_r;


end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_dr_shift_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_mode_id 		ttb.mode.mode_id%type 		:= jofl.jofl_pkg$extract_number(p_attr, 'mode_id', true);
  ln_tr_type_id		ttb.mode.tr_type_id%type 	:= jofl.jofl_pkg$extract_number(p_attr, 'tr_type_id', true);
  lt_rf_db_method 	text 						:= coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true), 'xxx');
begin 
 open p_rows for
     select
       drs.mode_id,
       drs.dr_shift_num,
       drs.dr_shift_name,
       drs.dr_shift_id,
       drs.break_count_max,
       drs.break_duration_max,
       drs.stop_time,
       drs.work_duration_min,
       drs.work_duration_max,
       drs.work_break_between_min,
       drs.work_break_between_max,
       drs.before_time_max,
       drs.break_count_min,
       drs.before_time_min,
       drs.after_time_max,
       drs.before_time_short_break_max,
       m.mode_name
     from ttb.dr_shift drs 
     join ttb.mode m on m.mode_id = drs.mode_id
     where (drs.mode_id = l_mode_id)
        or (lt_rf_db_method = 'ttb.tt_driver' and m.tr_type_id = ln_tr_type_id)
        or (lt_rf_db_method = 'ttb.jf_detail_fact_time_stop_place_rpt')
        or (lt_rf_db_method = 'ttb.jf_plan_fact_time_stop_place_rpt' )
	    	or (lt_rf_db_method = 'ttb.rep_violations4release' )
	    	or (lt_rf_db_method = 'ttb.rep_appointed_tasks' )
  ;
  
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_dr_shift_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
  l_r 						ttb.dr_shift%rowtype;
  ln_dr_mode_time_max_sum 	ttb.dr_mode.time_max%type;

begin
   l_r := ttb.jf_dr_shift_pkg$attr_to_rowtype(p_attr);
  
   select sum(dm.time_max)
   into ln_dr_mode_time_max_sum
   from ttb.dr_mode dm
   where dm.dr_shift_id = l_r.dr_shift_id
   ; 

   if coalesce(ln_dr_mode_time_max_sum, 0) > l_r.break_duration_max then
   	raise exception '<<Проверьте введенные данные. Значения "Max общая продолжительность перерывов" должно быть не меньше значения "Max продолжительность перерыва на обед" и не меньше значения "Max продолжительность перерыва на отстой". >>';
   end if;    

  update ttb.dr_shift set
          		 after_time_max = l_r.after_time_max
		  		,before_time_max = l_r.before_time_max
				,before_time_min = l_r.before_time_min
				,before_time_short_break_max = l_r.before_time_short_break_max
				,break_count_max = l_r.break_count_max
				,break_count_min = l_r.break_count_min
				,break_duration_max = l_r.break_duration_max
				,dr_shift_name = l_r.dr_shift_name
				,dr_shift_num = l_r.dr_shift_num
                /*,mode_id = l_r.mode_id*/
                ,stop_time = l_r.stop_time
                ,work_break_between_max = l_r.work_break_between_max
                ,work_break_between_min = l_r.work_break_between_min
                ,work_duration_max = l_r.work_duration_max
                ,work_duration_min = l_r.work_duration_min
   where
          dr_shift_id = l_r.dr_shift_id;
          
   return null;
 -- return ttb.jf_dr_mode_pkg$of_update(p_id_account, row_to_json(l_r_mode)::text);
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_dr_shift_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
  l_r ttb.dr_shift%rowtype;

begin
  l_r := ttb.jf_dr_shift_pkg$attr_to_rowtype(p_attr);
  
  delete from  ttb.dr_mode where  dr_shift_id = l_r.dr_shift_id;
  delete from  ttb.dr_shift   where  dr_shift_id = l_r.dr_shift_id;

  return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_dr_shift_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
  l_r ttb.dr_shift%rowtype;

begin

  l_r := ttb.jf_dr_shift_pkg$attr_to_rowtype (p_attr);

  l_r.dr_shift_id := nextval('ttb.dr_shift_dr_shift_id_seq');

  insert into ttb.dr_shift select l_r.*;
  
  /*Обед добавляем по дефолту к смене*/
  insert into ttb.dr_mode (
        dr_shift_id,
        action_type_id,
        time_min,
        time_max
        )
        values (
        l_r.dr_shift_id,
        ttb.action_type_pkg$dinner(),
        1800,
        7200
        );


  return null;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;