CREATE OR REPLACE FUNCTION ttb.jf_tt_out_oper_driver_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  ln_tt_out_id		ttb.tt_action.tt_action_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_out_id', true);
begin

 open p_rows for
select
  min(time_begin) time_begin
, max(time_end) time_end
, dr_shift.dr_shift_name
, dr_shift.dr_shift_id
,driver_last_name||' '||driver_name||' '||driver_middle_name fio
,tab_num
from ttb.tt_action_item
join ttb.tt_action on tt_action_item.tt_action_id = tt_action.tt_action_id
join ttb.dr_shift on tt_action_item.dr_shift_id = dr_shift.dr_shift_id
left JOIN core.driver on tt_action_item.driver_id = driver.driver_id
where  tt_action.tt_out_id = ln_tt_out_id
and not tt_action.sign_deleted
and not tt_action_item.sign_deleted
GROUP BY dr_shift.dr_shift_name, driver_last_name, driver_name, driver_middle_name, dr_shift.dr_shift_id, tab_num
order by 1;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;