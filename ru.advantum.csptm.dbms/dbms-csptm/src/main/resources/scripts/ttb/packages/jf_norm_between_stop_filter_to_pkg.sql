CREATE OR REPLACE FUNCTION ttb."jf_norm_between_stop_filter_to_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare
  l_norm_id ttb.norm.norm_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true);
  l_time_from TEXT :=jofl.jofl_pkg$extract_varchar(p_attr, 'F_time_from', true)::TIME;
begin
  open p_rows for
  WITH time_table AS (select distinct on (nbs.hour_to)
  CASE WHEN nbs.hour_to = '00:00:00' THEN '24:00:00'::TIME ELSE nbs.hour_to END time_to
  from ttb.norm_between_stop nbs
  WHERE norm_id = l_norm_id
  AND nbs.hour_to > l_time_from::TIME)
  select to_char(time_to, 'HH24:MI') time_to from time_table ORDER BY time_to;
end;
$function$;