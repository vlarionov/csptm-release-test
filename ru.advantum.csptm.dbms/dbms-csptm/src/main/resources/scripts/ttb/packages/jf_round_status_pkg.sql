create or replace function ttb.jf_round_status_pkg$of_rows(
    p_account_id in  numeric,
    p_attr       in  text,
    p_rows       out refcursor
)
    returns refcursor
language plpgsql
as $$
begin
    open p_rows for
    select
        r.round_status_id,
        r.round_status_name,
        r.round_status_code
    from ttb.round_status r;
end;
$$;