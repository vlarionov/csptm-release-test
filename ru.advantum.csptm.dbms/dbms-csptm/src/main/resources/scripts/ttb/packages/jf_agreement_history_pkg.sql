CREATE OR REPLACE FUNCTION ttb.jf_agreement_history_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_tt_variant_id ttb.tt_variant.tt_variant_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', FALSE);
BEGIN
  OPEN p_rows FOR
  SELECT
    tva.tt_variant_id,
    tva.tt_variant_agreement_id,
    tva.iteration_num,
    ag.order_num,
    tva.agreement_date,
    ttb.agreement_pkg$get_agreement_end_date(tva.tt_variant_agreement_id) agreement_end_date,
    r.resolution_name                                                     resolution,
    tva.comment,
    g.name                                                                group_name,
    a.first_name                                                          account_first_name,
    a.middle_name                                                         account_middle_name,
    a.last_name                                                           account_last_name,
    (SELECT e.name_short
     FROM core.entity e
     WHERE e.entity_id = a.depo_id)                                       account_depo_name,
    a.position                                                            account_position
  FROM ttb.tt_variant_agreement tva
    JOIN ttb.agreement_group ag ON tva.agreement_group_id = ag.agreement_group_id
    JOIN adm.group g ON ag.group_id = g.group_id
    LEFT JOIN ttb.resolution r ON tva.resolution_id = r.resolution_id
    LEFT JOIN core.account a ON tva.account_id = a.account_id
  WHERE tva.tt_variant_id = l_tt_variant_id
  ORDER BY iteration_num DESC, order_num;
END;
$$;