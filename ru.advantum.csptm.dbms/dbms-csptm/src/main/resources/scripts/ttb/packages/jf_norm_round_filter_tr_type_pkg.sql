CREATE OR REPLACE FUNCTION ttb."jf_norm_round_filter_tr_type_pkg$of_rows"(p_id_account numeric,
                                                                          OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare
  l_norm_id ttb.norm.norm_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true);
begin
  open p_rows for
  select distinct on (nr.tr_type_id) tt.name as tr_type_name, tt.tr_type_id
  from ttb.norm_round nr
    JOIN  core.tr_type tt on tt.tr_type_id = nr.tr_type_id
  WHERE nr.norm_id = l_norm_id;
end;
$function$;