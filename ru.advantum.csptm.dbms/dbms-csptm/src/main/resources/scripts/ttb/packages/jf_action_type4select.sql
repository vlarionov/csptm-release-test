--ttb.action_type4select

create or replace function ttb.jf_action_type4select_pkg$attr_to_rowtype(p_attr text) returns ttb.v_action_type
	language plpgsql
as $select$
declare
   l_r ttb.v_action_type%rowtype;
begin
   l_r.action_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'action_type_id', true);
   l_r.parent_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'parent_type_id', true);
   l_r.action_type_name := jofl.jofl_pkg$extract_varchar(p_attr, 'action_type_name', true);
   l_r.action_type_desc := jofl.jofl_pkg$extract_varchar(p_attr, 'action_type_desc', true);
   l_r.action_type_code := jofl.jofl_pkg$extract_varchar(p_attr, 'action_type_code', true);
   l_r.level := jofl.jofl_pkg$extract_number(p_attr, 'level', true);
   l_r.path := jofl.jofl_pkg$extract_varchar(p_attr, 'path', true);
   l_r.parent_name := jofl.jofl_pkg$extract_varchar(p_attr, 'parent_name', true);

   return l_r;
end;
$select$
;
---------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_action_type4select_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
--{"dt_b_DT":"2017-12-17 21:00:00.0","dt_e_DT":"2017-12-18 20:59:59.0","f_arr_tr_type_id":"$array[]","f_list_depo_id":"$array[]","f_route_id":"$array[]","F_<поле>":["","","",""],"f_list_dr_shift_id":"$array[]","f_tr_id":"$array[]","f_driver_id":"","f_list_action_type_id":"$array[]","RF_DB_METHOD":"ttb.jf_plan_fact_time_stop_place_rpt"}
  l_rf_db_method text := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true);
--"ttb.jf_plan_fact_time_stop_place_rpt"
  ln_calculation_task_id integer := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true);
begin
 open p_rows for
      select
        t.action_type_id,
        t.parent_type_id,
        t.action_type_name,
        t.action_type_desc,
        t.action_type_code,
        t.level,
        t.path,
        t.parent_name
      from ttb.v_action_type t
  where 1=1
  and (l_rf_db_method is null)
      or
      (l_rf_db_method in ('ttb.jf_plan_fact_time_stop_place_rpt',
                          'ttb.jf_detail_fact_time_stop_place_rpt',
	                      'ttb.rep_traffic_regularity',
					      'ttb.rep_transwork_violation'
                         ) and 
	   t.parent_type_id = 25)
	  or 
      (l_rf_db_method = 'asd.ct_normative_result'
       and exists (
        			select act.action_type_id
                       from asd.ct_round_stop_result res
                       join asd.ct_normative norm on norm.calculation_task_id = res.calculation_task_id
                       join asd.calculation_task ct on ct.calculation_task_id = res.calculation_task_id
                       join core.tr_type tt on tt.tr_type_id= res.tr_type_id
                       join rts.round rnd on rnd.round_id =res.round_id and not rnd.sign_deleted
                       join ttb.action_type act on act.action_type_id = rnd.action_type_id
                       join rts.route_variant rv on rv.route_variant_id = rnd.route_variant_id and not rv.sign_deleted
                       join rts.route r on r.route_id = rv.route_id and not r.sign_deleted    
                       join core.tr_capacity cap on cap.tr_capacity_id = res.tr_capacity_id
                       join rts.move_direction md on md.move_direction_id = rnd.move_direction_id 
                     where res.calculation_task_id = ln_calculation_task_id
                     and (select sum(vr.norm_duration) 
                     	 from asd.ct_stop_visit_result vr 
                          where vr.ct_round_stop_result_id = res.ct_round_stop_result_id) > 0
                     and (select t.action_type_id from ttb.action_type t where  t.action_type_id = act.parent_type_id) = t.action_type_id
                          )
                     
      )  
  order by t.action_type_id
  ;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

