CREATE OR REPLACE FUNCTION ttb.jf_incident_ribbon_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
  begin
    open p_rows for
      with a as (

          SELECT val
          FROM core.redis_server
          WHERE key = 'incident-ribbon'
      )
      select incident_id
        ,incident_initiator_id
        ,incident_status_id
        ,incident_type_id
        ,time_start
        ,parent_incident_id
        ,account_id
        ,tr_id
        ,unit_id
        ,incident_type_name
        ,tr_licence
        ,tr_garage_name
        ,concat_ws(
                ' '
                ,incident_type_name
                ,tr_garage_name
                ,nullif('( '||tr_licence||' )', '(  )')
                ,'Mаршрут '
                ,tt_variant_name
                ,tr_type_name
                ,concat_ws('/', out_num, shift_num)
                ,driver_name
                ,incident_reason_name
                ,incomming_message
                ,stop_item_name
                ) as incident_message
        ,driver_id
        ,tt_variant_id
        ,driver_name
        ,tt_variant_name
        ,is_alarm
        ,incident_reason_name
        ,incident_reason_id
      from a, json_to_recordset(a.val::json) as x(incident_id bigint
              , incident_initiator_id int
              , incident_status_id int
              , incident_type_id int
              , time_start timestamp
              , parent_incident_id BIGINT
              , account_id int
              , tr_id bigint
              , unit_id bigint
              , incident_type_name text
              , tr_licence text
              , tr_garage_name text
              , driver_id int
              , tt_variant_id  int
              , driver_name text
              , tt_variant_name text
              , is_alarm boolean
              , incident_reason_name text
              , incident_reason_id int
              , route_id int
              , incomming_message text
              , incomming_message_id text
              , out_num text
              , shift_num text
              , tr_type_name text
              , stop_item_name text
              , stop_item_id int)
      where tr_id in (select adm.account_data_realm_pkg$get_trs_by_all(p_id_account))
            or
            route_id in (select adm.account_data_realm_pkg$get_routes(p_id_account))
      ;
end;
$function$;