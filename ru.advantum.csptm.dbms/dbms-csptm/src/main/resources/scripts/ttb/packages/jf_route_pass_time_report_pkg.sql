create or replace function ttb.jf_route_pass_time_report_pkg$of_rows(p_id_account numeric, out p_rows refcursor, p_attr text)
    returns refcursor
language plpgsql
as $function$
declare
    l_tt_variant_id integer := jofl.jofl_pkg$extract_number(p_attr, 'f_tt_variant_id', true);
begin
    open p_rows for
    select distinct
        p_id_account,
        v.tt_variant_id p_tt_variant,
        v.route_variant_id p_route_variant,
        lower(v.action_period) :: date                        p_date,
        to_char(lower(v.action_period) :: date, 'dd.mm.yyyy') p_dfrom,
        r.route_num                                           p_route_num_str,
        r.route_id
          from ttb.tt_variant v
              join rts.route_variant rv on v.route_variant_id = rv.route_variant_id
              join rts.route r on rv.route_id = r.route_id
    where v.tt_variant_id = l_tt_variant_id;
end;
$function$;


create or replace function ttb.jf_route_pass_time_report_pkg$report_header(
    p_id_account text,
    p_timezone   text,
    p_tt_variant text
)
    returns table(
        id_account     text,
        timezone       text,
        route_num      text,
        date           text,
        tr_capacity_id text,
        round_num      text,
        round_code     text,
        tt_variant_id  text,
        day_type       text,
        norm_id        text
    )
language plpgsql
as $function$
declare
begin
    return query
    with day_type as (
        select string_agg(upper(ctag.tag_name), ', ') as all_day
        from ttb.tt_calendar calendar
            join ttb.calendar_tag ctag on ctag.calendar_tag_id = calendar.calendar_tag_id
        where calendar.tt_variant_id = p_tt_variant :: integer
            and ctag.calendar_tag_group_id=ttb.calendar_pkg$get_weekday_type_tag_group_id()
    )
    select
        p_id_account :: text,
        p_timezone :: text,
        r.route_num :: text,
        (case when tv.order_date is null
            then to_char(lower(tv.action_period), 'DD.MM.YYYY')
         else to_char(tv.order_date, 'DD.MM.YYYY') end) :: text,
        tc.tr_capacity_id :: text,
        rr.round_num :: text,
        rr.code :: text as round_code,
        p_tt_variant,
        dt.all_day,
        tv.norm_id::text
    from ttb.tt_variant tv
        join rts.route_variant rv on tv.route_variant_id = rv.route_variant_id
        join rts.route r on rv.route_id = r.route_id
        join rts.round rr on rv.route_variant_id = rr.route_variant_id
        join ttb.action_type a on rr.action_type_id = a.action_type_id
        join ttb.tt_out t on tv.tt_variant_id = t.tt_variant_id
        join core.tr_capacity tc on t.tr_capacity_id = tc.tr_capacity_id
        join day_type dt on 1 = 1
    where
        tv.tt_variant_id = p_tt_variant :: integer and
        ttb.action_type_pkg$is_production_round(rr.action_type_id)
    group by rr.code,r.route_num, tc.short_name, tc.tr_capacity_id, r.route_id, tv.order_date, rr.round_num,  dt.all_day,tv.norm_id,tv.action_period;
end;
$function$;

create or replace function ttb.jf_route_pass_time_report_pkg$report_detail(
    p_id_account  text,
    p_date        text,
    p_timezone    text,
    p_capacity_id text,
    p_round_code  text,
    p_tt_variant  text,
    p_round_num   text,
    p_norm_id     text
)
    returns table(
        capacity   text,
        round_code text,
        time0     text,
        time1      text,
        time2      text,
        time3      text,
        time4      text,
        time5      text,
        time6      text,
        time7      text,
        time8      text,
        time9      text,
        time10     text,
        time11     text,
        time12     text,
        time13     text,
        time14     text,
        time15     text,
        time16     text,
        time17     text,
        time18     text,
        time19     text,
        time20     text,
        time21     text,
        time22     text,
        time23     text,
        stop_num   text,
        stop_name  text,
        avg_time   text,
        speed      text,
        ab         text,
        ba         text,
        abba       text
    )
language plpgsql
as $function$
begin
    return query
    with
            stop2round_dur2 as (
            select
                tn.norm_id,
                extract(hour from nr.hour_from)                                     num_hour,
                (nr.min_inter_round_stop_dur + nr.reg_inter_round_stop_dur) / 60 as dur_time,
                rr.move_direction_id
            from ttb.tt_variant tv
                join ttb.norm tn on tv.norm_id = tn.norm_id
                join ttb.norm_round nr on tn.norm_id = nr.norm_id
                join rts.round rr on nr.round_id = rr.round_id
            where
                tv.tt_variant_id = p_tt_variant :: integer and rr.round_num = p_round_num :: integer and nr.tr_capacity_id = p_capacity_id :: smallint
            order by rr.move_direction_id, nr.round_id, num_hour
        )
        , stop_item_a2b_round_num as (
        select
            si2rb.order_num    stop_num_1,
            si2re.order_num    stop_num_2,
            si2rb.round_id,
            si2rb.stop_item_id stop_a,
            si2re.stop_item_id stop_b,
            r.move_direction_id
        from rts.stop_item2round si2rb
            join rts.stop_item2round si2re on si2re.order_num = 1 + si2rb.order_num
            join rts.round r on si2rb.round_id = r.round_id and si2re.round_id = r.round_id
        where r.round_num = p_round_num :: integer
        order by r.move_direction_id, si2rb.order_num
    )
        , duration_beetwen_stop as (
        select
            extract(hour from nbs.hour_from) num_hour,
            nbs.stop_item_1_id,
            n.stop_num_1,
            nbs.stop_item_2_id,
            n.stop_num_2,
            nbs.between_stop_dur,
            n.round_id
        from ttb.norm_between_stop nbs
            join stop_item_a2b_round_num n on nbs.stop_item_1_id = n.stop_a and nbs.stop_item_2_id = n.stop_b
            join core.tr_capacity t on nbs.tr_capacity_id = t.tr_capacity_id
        where nbs.norm_id = p_norm_id::integer and t.tr_capacity_id=p_capacity_id::smallint
        order by n.move_direction_id, hour_from, stop_num_1
    )
        , diration_on_stop as (
        select
            distinct
            nr.round_id,
            nr.reg_inter_round_stop_dur,
            nr.min_inter_round_stop_dur                                                as dur_stop,
            nr.stop_item_id,
            extract(hour from nr.hour_from)                                               num_hour,
            (coalesce(nr.reg_inter_round_stop_dur, 0) +
             coalesce(nr.min_inter_round_stop_dur, 0)) / 60 as duration
        from ttb.norm_round nr
            join rts.round r on nr.round_id = r.round_id
        where nr.norm_id = p_norm_id::integer and r.round_num = p_round_num :: integer
    )
        , stops_nums as (
        select
            stop_item2round_id,
            stop_item_id,
            order_num,
            r.round_id
        from rts.stop_item2round si2r
            join rts.round r on si2r.round_id = r.round_id
        where r.round_num = p_round_num :: integer
        order by r.move_direction_id, order_num

    )
        , report as (
        select
            sn.order_num,
            sn.stop_item_id,
            sn.round_id,
            sn.stop_item2round_id,
            sh1.between_stop_dur / 60  as time0,
            sh2.between_stop_dur / 60  as time1,
            sh3.between_stop_dur / 60  as time2,
            sh4.between_stop_dur / 60  as time3,
            sh5.between_stop_dur / 60  as time4,
            sh6.between_stop_dur / 60  as time5,
            sh7.between_stop_dur / 60  as time6,
            sh8.between_stop_dur / 60  as time7,
            sh9.between_stop_dur / 60  as time8,
            sh10.between_stop_dur / 60 as time9,
            sh11.between_stop_dur / 60 as time10,
            sh12.between_stop_dur / 60 as time11,
            sh13.between_stop_dur / 60 as time12,
            sh14.between_stop_dur / 60 as time13,
            sh15.between_stop_dur / 60 as time14,
            sh16.between_stop_dur / 60 as time15,
            sh17.between_stop_dur / 60 as time16,
            sh18.between_stop_dur / 60 as time17,
            sh19.between_stop_dur / 60 as time18,
            sh20.between_stop_dur / 60 as time19,
            sh21.between_stop_dur / 60 as time20,
            sh22.between_stop_dur / 60 as time21,
            sh23.between_stop_dur / 60 as time22,
            sh24.between_stop_dur / 60 as time23
        from stops_nums sn
            left join duration_beetwen_stop sh1 on sh1.num_hour = 0 and sh1.stop_item_2_id = sn.stop_item_id and sn.round_id = sh1.round_id
            left join duration_beetwen_stop sh2 on sh2.num_hour = 1 and sh2.stop_item_2_id = sn.stop_item_id and sn.round_id = sh2.round_id
            left join duration_beetwen_stop sh3 on sh3.num_hour = 2 and sh3.stop_item_2_id = sn.stop_item_id and sn.round_id = sh3.round_id
            left join duration_beetwen_stop sh4 on sh4.num_hour = 3 and sh4.stop_item_2_id = sn.stop_item_id and sn.round_id = sh4.round_id
            left join duration_beetwen_stop sh5 on sh5.num_hour = 4 and sh5.stop_item_2_id = sn.stop_item_id and sn.round_id = sh5.round_id
            left join duration_beetwen_stop sh6 on sh6.num_hour = 5 and sh6.stop_item_2_id = sn.stop_item_id and sn.round_id = sh6.round_id
            left join duration_beetwen_stop sh7 on sh7.num_hour = 6 and sh7.stop_item_2_id = sn.stop_item_id and sn.round_id = sh7.round_id
            left join duration_beetwen_stop sh8 on sh8.num_hour = 7 and sh8.stop_item_2_id = sn.stop_item_id and sn.round_id = sh8.round_id
            left join duration_beetwen_stop sh9 on sh9.num_hour = 8 and sh9.stop_item_2_id = sn.stop_item_id and sn.round_id = sh9.round_id
            left join duration_beetwen_stop sh10 on sh10.num_hour = 9 and sh10.stop_item_2_id = sn.stop_item_id and sn.round_id = sh10.round_id
            left join duration_beetwen_stop sh11 on sh11.num_hour = 10 and sh11.stop_item_2_id = sn.stop_item_id and sn.round_id = sh11.round_id
            left join duration_beetwen_stop sh12 on sh12.num_hour = 11 and sh12.stop_item_2_id = sn.stop_item_id and sn.round_id = sh12.round_id
            left join duration_beetwen_stop sh13 on sh13.num_hour = 12 and sh13.stop_item_2_id = sn.stop_item_id and sn.round_id = sh13.round_id
            left join duration_beetwen_stop sh14 on sh14.num_hour = 13 and sh14.stop_item_2_id = sn.stop_item_id and sn.round_id = sh14.round_id
            left join duration_beetwen_stop sh15 on sh15.num_hour = 14 and sh15.stop_item_2_id = sn.stop_item_id and sn.round_id = sh15.round_id
            left join duration_beetwen_stop sh16 on sh16.num_hour = 15 and sh16.stop_item_2_id = sn.stop_item_id and sn.round_id = sh16.round_id
            left join duration_beetwen_stop sh17 on sh17.num_hour = 16 and sh17.stop_item_2_id = sn.stop_item_id and sn.round_id = sh17.round_id
            left join duration_beetwen_stop sh18 on sh18.num_hour = 17 and sh18.stop_item_2_id = sn.stop_item_id and sn.round_id = sh18.round_id
            left join duration_beetwen_stop sh19 on sh19.num_hour = 18 and sh19.stop_item_2_id = sn.stop_item_id and sn.round_id = sh19.round_id
            left join duration_beetwen_stop sh20 on sh20.num_hour = 19 and sh20.stop_item_2_id = sn.stop_item_id and sn.round_id = sh20.round_id
            left join duration_beetwen_stop sh21 on sh21.num_hour = 20 and sh21.stop_item_2_id = sn.stop_item_id and sn.round_id = sh21.round_id
            left join duration_beetwen_stop sh22 on sh22.num_hour = 21 and sh22.stop_item_2_id = sn.stop_item_id and sn.round_id = sh22.round_id
            left join duration_beetwen_stop sh23 on sh23.num_hour = 22 and sh23.stop_item_2_id = sn.stop_item_id and sn.round_id = sh23.round_id
            left join duration_beetwen_stop sh24 on sh24.num_hour = 23 and sh24.stop_item_2_id = sn.stop_item_id and sn.round_id = sh24.round_id
        union
        select
            0,
            0,
            sd0.round_id,
            0,
            sd0.duration,
            sd1.duration,
            sd2.duration,
            sd3.duration,
            sd4.duration,
            sd5.duration,
            sd6.duration,
            sd7.duration,
            sd8.duration,
            sd9.duration,
            sd10.duration,
            sd11.duration,
            sd12.duration,
            sd13.duration,
            sd14.duration,
            sd15.duration,
            sd16.duration,
            sd17.duration,
            sd18.duration,
            sd19.duration,
            sd20.duration,
            sd21.duration,
            sd22.duration,
            sd23.duration
        from diration_on_stop sd0
            left join diration_on_stop sd1 on sd1.num_hour = 1 and sd0.round_id = sd1.round_id
            left join diration_on_stop sd2 on sd2.num_hour = 2 and sd0.round_id = sd2.round_id
            left join diration_on_stop sd3 on sd3.num_hour = 3 and sd0.round_id = sd3.round_id
            left join diration_on_stop sd4 on sd4.num_hour = 4 and sd0.round_id = sd4.round_id
            left join diration_on_stop sd5 on sd5.num_hour = 5 and sd0.round_id = sd5.round_id
            left join diration_on_stop sd6 on sd6.num_hour = 6 and sd0.round_id = sd6.round_id
            left join diration_on_stop sd7 on sd7.num_hour = 7 and sd0.round_id = sd7.round_id
            left join diration_on_stop sd8 on sd8.num_hour = 8 and sd0.round_id = sd8.round_id
            left join diration_on_stop sd9 on sd9.num_hour = 9 and sd0.round_id = sd9.round_id
            left join diration_on_stop sd10 on sd10.num_hour = 10 and sd0.round_id = sd10.round_id
            left join diration_on_stop sd11 on sd11.num_hour = 11 and sd0.round_id = sd11.round_id
            left join diration_on_stop sd12 on sd12.num_hour = 12 and sd0.round_id = sd12.round_id
            left join diration_on_stop sd13 on sd13.num_hour = 13 and sd0.round_id = sd13.round_id
            left join diration_on_stop sd14 on sd14.num_hour = 14 and sd0.round_id = sd14.round_id
            left join diration_on_stop sd15 on sd15.num_hour = 15 and sd0.round_id = sd15.round_id
            left join diration_on_stop sd16 on sd16.num_hour = 16 and sd0.round_id = sd16.round_id
            left join diration_on_stop sd17 on sd17.num_hour = 17 and sd0.round_id = sd17.round_id
            left join diration_on_stop sd18 on sd18.num_hour = 18 and sd0.round_id = sd18.round_id
            left join diration_on_stop sd19 on sd19.num_hour = 19 and sd0.round_id = sd19.round_id
            left join diration_on_stop sd20 on sd20.num_hour = 20 and sd0.round_id = sd20.round_id
            left join diration_on_stop sd21 on sd21.num_hour = 21 and sd0.round_id = sd21.round_id
            left join diration_on_stop sd22 on sd22.num_hour = 22 and sd0.round_id = sd22.round_id
            left join diration_on_stop sd23 on sd23.num_hour = 23 and sd0.round_id = sd23.round_id
        where sd0.num_hour = 0
        order by round_id
    )
        , total_report as (
        select
            sum(r.time0) as time0,
            sum(r.time1)  as time1,
            sum(r.time2)  as time2,
            sum(r.time3)  as time3,
            sum(r.time4)  as time4,
            sum(r.time5)  as time5,
            sum(r.time6)  as time6,
            sum(r.time7)  as time7,
            sum(r.time8)  as time8,
            sum(r.time9)  as time9,
            sum(r.time10) as time10,
            sum(r.time11) as time11,
            sum(r.time12) as time12,
            sum(r.time13) as time13,
            sum(r.time14) as time14,
            sum(r.time15) as time15,
            sum(r.time16) as time16,
            sum(r.time17) as time17,
            sum(r.time18) as time18,
            sum(r.time19) as time19,
            sum(r.time20) as time20,
            sum(r.time21) as time21,
            sum(r.time22) as time22,
            sum(r.time23) as time23,
            r.round_id,
            r.stop_item2round_id
        from report r
        group by rollup (r.round_id, r.stop_item2round_id)
    ), summary_time as (
        select round((
                         r.time1 +
                         r.time2 +
                         r.time3 +
                         r.time4 +
                         r.time5 +
                         r.time6 +
                         r.time7 +
                         r.time8 +
                         r.time9 +
                         r.time10 +
                         r.time11 +
                         r.time12 +
                         r.time13 +
                         r.time14 +
                         r.time15 +
                         r.time16 +
                         r.time17 +
                         r.time18 +
                         r.time19 +
                         r.time20 +
                         r.time21 +
                         r.time22 +
                         r.time23 +
                         r.time0) :: numeric / 24, 1) as avg_time
        from total_report r
        where r.stop_item2round_id isnull and r.round_id isnull
    )
        , summary_lenght1 as (
        select
            r.round_id,
            r.length
        from rts.round r
        where r.round_num = p_round_num :: integer and r.move_direction_id = 1 :: smallint
    )
        , summary_lenght2 as (
        select
            r.round_id,
            r.length
        from rts.round r
        where r.round_num = p_round_num :: integer and r.move_direction_id = 2 :: smallint
    )
    select
        tc.short_name :: text,
        p_round_code :: text,
        coalesce(rep.time0,0):: text,
        coalesce(rep.time1,0):: text,
        coalesce(rep.time2,0):: text,
        coalesce(rep.time3,0):: text,
        coalesce(rep.time4,0):: text,
        coalesce(rep.time5,0):: text,
        coalesce(rep.time6,0):: text,
        coalesce(rep.time7,0):: text,
        coalesce(rep.time8,0):: text,
        coalesce(rep.time9,0):: text,
        coalesce(rep.time10,0):: text,
        coalesce(rep.time11,0):: text,
        coalesce(rep.time12,0):: text,
        coalesce(rep.time13,0):: text,
        coalesce(rep.time14,0):: text,
        coalesce(rep.time15,0):: text,
        coalesce(rep.time16,0):: text,
        coalesce(rep.time17,0):: text,
        coalesce(rep.time18,0):: text,
        coalesce(rep.time19,0):: text,
        coalesce(rep.time20,0):: text,
        coalesce(rep.time21,0):: text,
        coalesce(rep.time22,0):: text,
        coalesce(rep.time23,0):: text,
        case when si2r.order_num isnull then '' else si2r.order_num:: text end,
        case when (rep.round_id is null and rep.stop_item2round_id is null)
            then 'Время оборотного рейса'
        when rep.stop_item2round_id is null
            then 'Итого в направлении ' ||
                 case when rr.move_direction_id = 1
                     then 'АБ'
                 else 'БА' end
        when rep.stop_item2round_id = 0
            then 'Стоянка'
        else rs.name :: text end                                                                          as stop_name,
        st.avg_time :: text                                                                               as avg_time,
        round((coalesce(lenght1.length, 0) +
               coalesce(lenght2.length, 0)) :: numeric * 0.06 / st.avg_time, 2) :: text                   as speed,
        case when lenght1.length isnull
            then ''
        else round((lenght1.length) :: numeric / 1000, 3) :: text end                                     as ab,
        case when lenght2.length isnull
            then ''
        else round((lenght2.length) :: numeric / 1000, 3) :: text end                                     as ba,
        (round(coalesce(lenght1.length ,0):: numeric / 1000, 3) + round(coalesce(lenght2.length ,0) :: numeric / 1000, 3)) :: text as abba
    from total_report rep
        left join rts.round rr on rr.round_id = rep.round_id
        left join rts.stop_item2round si2r on si2r.stop_item2round_id = rep.stop_item2round_id
        left join rts.stop_item si on si2r.stop_item_id = si.stop_item_id
        left join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
        left join rts.stop rs on sl.stop_id = rs.stop_id
        left join core.tr_capacity tc on tc.tr_capacity_id = p_capacity_id :: smallint
        left join summary_time st on 1 = 1
        left join summary_lenght1 lenght1 on 1 = 1
        left join summary_lenght2 lenght2 on 1 = 1
    order by rr.move_direction_id, si2r.order_num, rep.stop_item2round_id;
end;
$function$;
