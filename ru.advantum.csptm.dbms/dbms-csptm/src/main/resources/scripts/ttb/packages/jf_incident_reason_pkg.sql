CREATE OR REPLACE FUNCTION ttb.jf_incident_reason_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$

declare
begin
 open p_rows for
    select
            incident_reason.incident_reason_id
            ,incident_reason.incident_reason_parent_id
            ,coalesce(ir_code,'')||' '||incident_reason_item.name as name
            ,coalesce(ir_code,'')||' '||ttb.jf_incident_reason_pkg$get_name(incident_reason.incident_reason_id) full_name
    from ttb.incident_reason
    join ttb.incident_reason_item on incident_reason.incident_reason_item_id = incident_reason_item.incident_reason_item_id
    ORDER by ir_code ASC ;
end;
$$;

CREATE OR REPLACE FUNCTION ttb.jf_incident_reason_pkg$get_name(p_incident_reason_id integer) RETURNS text
	LANGUAGE plpgsql STABLE
AS $$

declare
    l_name text;
begin
    WITH RECURSIVE  tree as
    (
      select ir.incident_reason_id
        ,ir.incident_reason_parent_id
        ,ir.incident_reason_item_id
        ,iri.name
      from ttb.incident_reason ir
        join ttb.incident_reason_item iri on ir.incident_reason_item_id = iri.incident_reason_item_id
      where ir.incident_reason_parent_id is null
      UNION ALL
      SELECT
        ir.incident_reason_id
        ,ir.incident_reason_parent_id
        ,ir.incident_reason_item_id
        ,tree.name||'/'||iri.name as name
      FROM
        tree
        JOIN ttb.incident_reason ir ON ir.incident_reason_parent_id = tree.incident_reason_id
        join ttb.incident_reason_item iri on ir.incident_reason_item_id = iri.incident_reason_item_id

    )
    select name into l_name from tree where incident_reason_id = p_incident_reason_id;
    return l_name;
end;
$$;

CREATE OR REPLACE FUNCTION ttb.jf_incident_reason_pkg$get_root_tt_oper_problems_list_s()
  RETURNS jsonb[] as
$$
declare
 la jsonb[];
 begin
 raise notice 'abc';
WITH RECURSIVE  tree as
(
  select ir.incident_reason_id
    ,ir.incident_reason_parent_id
    ,ir.incident_reason_item_id
  from ttb.incident_reason ir
  where ir.incident_reason_parent_id is null
        and ir.incident_reason_item_id = ttb.jf_incident_reason_pkg$get_root_tt_oper_problems()
  UNION ALL
  SELECT
    ir.incident_reason_id
    ,ir.incident_reason_parent_id
    ,ir.incident_reason_item_id
  FROM
    tree
    JOIN ttb.incident_reason ir ON ir.incident_reason_parent_id = tree.incident_reason_id
    join ttb.incident_reason_item iri on ir.incident_reason_item_id = iri.incident_reason_item_id

)
select array_agg(incident_reason_id::text::jsonb) into la from tree;
return la;

end;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION ttb.jf_incident_reason_pkg$get_root_tt_oper_problems_list()
  RETURNS setof int as
$$
WITH RECURSIVE  tree as
(
  select ir.incident_reason_id
    ,ir.incident_reason_parent_id
    ,ir.incident_reason_item_id
  from ttb.incident_reason ir
  where ir.incident_reason_parent_id is null
        and ir.incident_reason_item_id = ttb.jf_incident_reason_pkg$get_root_tt_oper_problems()
  UNION ALL
  SELECT
    ir.incident_reason_id
    ,ir.incident_reason_parent_id
    ,ir.incident_reason_item_id
  FROM
    tree
    JOIN ttb.incident_reason ir ON ir.incident_reason_parent_id = tree.incident_reason_id
    join ttb.incident_reason_item iri on ir.incident_reason_item_id = iri.incident_reason_item_id

)
select incident_reason_id from tree;
$$
LANGUAGE sql STABLE;

create or replace function ttb.jf_incident_reason_pkg$get_name_start_from_level(p_incident_reason_id integer, p_level integer) returns text
	language plpgsql STABLE
as $$

declare
    l_name text;
begin
    with recursive  tree as
    (
      select ir.incident_reason_id
        ,ir.incident_reason_parent_id
        ,ir.incident_reason_item_id
        ,iri.name as name
        ,1 as level
      from ttb.incident_reason ir
        join ttb.incident_reason_item iri on ir.incident_reason_item_id = iri.incident_reason_item_id
      where incident_reason_id = p_incident_reason_id
      union all
      select
        ir.incident_reason_id
        ,ir.incident_reason_parent_id
        ,ir.incident_reason_item_id
        ,iri.name as name
        ,tree.level + 1 as level
      from
        tree
        join ttb.incident_reason ir on ir.incident_reason_id = tree.incident_reason_parent_id
        join ttb.incident_reason_item iri on ir.incident_reason_item_id = iri.incident_reason_item_id

    )
    ,a as (
        select name
        into l_name
           from tree
           order by level desc
           offset p_level - 1
    )
    select string_agg(name, '/') from a;
    return l_name;
end;
$$;


CREATE OR REPLACE FUNCTION ttb.jf_incident_reason_pkg$get_name_only_level(p_incident_reason_id integer, p_level integer) RETURNS text
	LANGUAGE plpgsql STABLE
AS $$

declare
    l_name text;
begin
    with recursive  tree as
    (
      select ir.incident_reason_id
        ,ir.incident_reason_parent_id
        ,ir.incident_reason_item_id
        ,iri.name as name
        ,1 as level
      from ttb.incident_reason ir
        join ttb.incident_reason_item iri on ir.incident_reason_item_id = iri.incident_reason_item_id
      where incident_reason_id = p_incident_reason_id
      union all
      select
        ir.incident_reason_id
        ,ir.incident_reason_parent_id
        ,ir.incident_reason_item_id
        ,iri.name as name
        ,tree.level + 1 as level
      from
        tree
        join ttb.incident_reason ir on ir.incident_reason_id = tree.incident_reason_parent_id
        join ttb.incident_reason_item iri on ir.incident_reason_item_id = iri.incident_reason_item_id

    )
    select (core.array_reverse(array_agg(name)))[p_level] a
    into l_name
    from tree;

    return l_name;
end;
$$;


CREATE OR REPLACE FUNCTION ttb.jf_incident_reason_pkg$get_name4ribbon(p_incident_reason_id integer) RETURNS text
	LANGUAGE plpgsql STABLE
AS $$
begin
    return ttb.jf_incident_reason_pkg$get_name_only_level(p_incident_reason_id, 3);
end;
$$;