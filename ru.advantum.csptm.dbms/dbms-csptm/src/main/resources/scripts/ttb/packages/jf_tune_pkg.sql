CREATE OR REPLACE FUNCTION ttb."jf_tune_pkg$attr_to_rowtype"(p_attr text)
 RETURNS ttb.tune
 LANGUAGE plpgsql
AS $function$
declare
   l_r ttb.tune%rowtype;
begin
   l_r.tune_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tune_id', true);
   l_r.tune_param := jofl.jofl_pkg$extract_varchar(p_attr, 'tune_param', true);
   l_r.dt_begin := jofl.jofl_pkg$extract_date(p_attr, 'dt_begin', true);
   l_r.tune_value := jofl.jofl_pkg$extract_varchar(p_attr, 'tune_value', true);
   l_r.tune_type := jofl.jofl_pkg$extract_varchar(p_attr, 'tune_type', true);
   l_r.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true);

   return l_r;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tune_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare 
begin 
 open p_rows for 
      select 
        tune_id, 
        tune_param, 
        dt_begin, 
        tune_value, 
        tune_type, 
        comment
      from ttb.tune; 
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tune_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare 
   l_r ttb.tune%rowtype;
begin 
   l_r := ttb.jf_tune_pkg$attr_to_rowtype(p_attr);

   update ttb.tune set 
          tune_param = l_r.tune_param, 
          dt_begin = l_r.dt_begin, 
          tune_value = l_r.tune_value, 
          tune_type = l_r.tune_type, 
          comment = l_r.comment
   where 
          tune_id = l_r.tune_id;

   return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tune_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare 
   l_r ttb.tune%rowtype;
begin 
   l_r := ttb.jf_tune_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.tune where  tune_id = l_r.tune_id;

   return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tune_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare 
   l_r ttb.tune%rowtype;
begin 
   l_r := ttb.jf_tune_pkg$attr_to_rowtype(p_attr);

   insert into ttb.tune select l_r.*;

   return null;
end;
$function$
;