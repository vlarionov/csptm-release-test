CREATE OR REPLACE FUNCTION ttb.jf_tt_out_oper_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  ln_tt_variant_id ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
  l_incident_id bigint := jofl.jofl_pkg$extract_number(p_attr, 'incident_id', true)::bigint;
  l_time_start  timestamp := jofl.jofl_pkg$extract_date(p_attr,'time_start', true);

begin

  if l_incident_id is not null then
    select a
    into ln_tt_variant_id
    from oud.jf_order_list_violation_pkg$get_tt_variant_ids(l_incident_id, l_time_start) a;
  end if;



 open p_rows for
     with act as (
         select vma.action_type_id
           ,vma.action_type_code
           ,vma.action_type_name
           ,vma.is_production_round
           ,vma.is_technical_round
           ,vma.is_dinner
           ,vma.is_break
           ,vma.is_round
           --,ttb.action_type_pkg$is_bn_round_break(act.action_type_id) as is_bn_round_break
           ,vma.action_type_id = ttb.action_type_pkg$bn_round_break()  as is_bn_round_break
         from ttb.vm_action_type vma
     )
   ,q as (
           select distinct
             out.tt_out_id
             ,out.tt_out_num
             ,tai.time_begin
             ,tai.time_end
             ,ta.action_dur
             ,at.action_type_code
             ,at.action_type_name
             ,at.action_type_id
             ,at.is_production_round
             ,at.is_technical_round
             ,at.is_bn_round_break
             ,at.is_dinner
             ,at.is_break
             ,at.is_round
             ,md.move_direction_id
             ,md.move_direction_name
             ,r.code as r_code
             ,first_value(s.name) over (partition by ta.tt_action_id order by si2r.order_num) as stop_b
             ,first_value(s.name) over (partition by ta.tt_action_id order by si2r.order_num desc) as stop_e
             ,first_value(s.stop_id) over (partition by ta.tt_action_id order by si2r.order_num) as stop_id_b
             ,first_value(s.stop_id) over (partition by ta.tt_action_id order by si2r.order_num desc) as stop_id_e
             ,ta.tt_action_id
             ,out.tt_variant_id
             ,tai.dr_shift_id
             ,out.mode_id
             ,out.tr_id
             ,tai.driver_id
           from ttb.tt_out out
             join ttb.tt_variant on tt_variant.tt_variant_id = out.tt_variant_id
             join rts.route_variant on route_variant.route_variant_id = tt_variant.route_variant_id
             join ttb.tt_action ta on ta.tt_out_id = out.tt_out_id
             join act at on at.action_type_id = ta.action_type_id
             join ttb.tt_action_item tai on tai.tt_action_id = ta.tt_action_id
             join rts.stop_item2round si2r on si2r.stop_item2round_id = tai.stop_item2round_id
             join rts.stop_item si on si.stop_item_id = si2r.stop_item_id
             join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
             join rts.stop s on s.stop_id = sl.stop_id
             join rts.stop_item2round_type si2rt on si2rt.stop_item2round_id = si2r.stop_item2round_id
             join rts.stop_item_type sit on sit.stop_item_id = si.stop_item_id
                                            and sit.stop_item_type_id = si2rt.stop_item_type_id
             left join rts.stop_type st on st.stop_type_id = sit.stop_type_id and st.stop_type_id = rts.stop_type_pkg$finish()/*7*/
             left join ttb.tt_action_round tar on tar.tt_action_id = ta.tt_action_id
             left join rts.round r on r.round_id = tar.round_id
                                      and si2r.round_id = r.round_id
                                      and r.action_type_id = at.action_type_id  and not r.sign_deleted
             left join rts.move_direction md on md.move_direction_id = r.move_direction_id

           where out.tt_variant_id = ln_tt_variant_id
                  and route_variant.route_id in (select adm.account_data_realm_pkg$get_routes(p_id_account))
                  and not out.sign_deleted
                  and not tt_variant.sign_deleted
                  and not route_variant.sign_deleted
                  and not ta.sign_deleted
                  and not s.sign_deleted
                  and not sl.sign_deleted
                  and not si.sign_deleted
                  and not si2r.sign_deleted
                          --incidents
       )
         , qq as (
           select distinct
             q.tt_out_id
             ,q.tt_out_num
             ,row_number() over (partition by q.tt_action_id order by q.time_end ,q.time_begin) as rn
             ,q.time_begin
             ,q.time_end
             ,min(q.time_begin) over (partition by q.tt_action_id) as agg_time_begin
             ,max(q.time_end) over (partition by q.tt_action_id) as agg_time_end
             ,case
              when q.is_bn_round_break then
                extract(epoch from
                        q.time_end -
                        lag(q.time_begin) over (partition by q.tt_action_id order by q.time_end ,q.time_begin))
              when q.is_dinner then
                extract(epoch from
                        ttb.jf_mode_pkg$of_get_dinner_time(q.mode_id, q.dr_shift_id, 'min'))
              else null
              end min_bn_int
             ,case
              when q.is_bn_round_break then
                ttb.rv_tune_pkg$get_break_round_time(ttv.route_variant_id)
              when q.is_dinner then
                extract(epoch from
                        ttb.jf_mode_pkg$of_get_dinner_time(q.mode_id, q.dr_shift_id, 'max'))
              else null
              end max_bn_int
             ,q.action_dur
             ,q.action_type_code
             ,q.action_type_name
             ,q.move_direction_id
             ,q.move_direction_name
             ,q.r_code
             ,q.action_type_id
             ,q.is_production_round
             ,q.is_technical_round
             ,q.is_bn_round_break
             ,q.is_dinner
             ,q.stop_b
             ,q.stop_e
             ,q.stop_id_b
             ,q.stop_id_e
             ,q.tt_action_id
             ,q.tt_variant_id
             ,q.dr_shift_id
             ,q.mode_id
             ,q.tr_id
             ,q.driver_id
           from
             q join ttb.tt_variant ttv on ttv.tt_variant_id = q.tt_variant_id
           where not ttv.sign_deleted
       )
       select
         qq.tt_out_id
         ,qq.tt_out_num
         ,qq.agg_time_begin as time_begin
         ,qq.agg_time_end as time_end
         ,qq.min_bn_int
         ,qq.max_bn_int
         ,extract (epoch from (qq.agg_time_end - qq.agg_time_begin)) as action_dur
         ,extract (epoch from (qq.agg_time_end - qq.agg_time_begin)) as counted_dur
         ,qq.action_type_code
         ,qq.action_type_name
         ,qq.move_direction_id
         ,qq.move_direction_name
         ,qq.r_code
         ,qq.action_type_id
         ,qq.is_production_round
         ,qq.is_technical_round
         ,qq.is_bn_round_break
         ,qq.is_dinner
         ,qq.stop_b
         ,qq.stop_e
         ,qq.stop_id_b
         ,qq.stop_id_e
         ,qq.tt_action_id
         ,qq.tt_variant_id
         ,ds.dr_shift_id
         ,ds.dr_shift_num
         ,case
          when ttb.action_type_pkg$is_technical_round(action_type_id) then 'P{A},D{OF_UPDATE}'
          when ttb.action_type_pkg$is_round(action_type_id) then 'P{A},D{OF_UPDATE, OF_DELETE}'
          when action_type_id = ttb.action_type_pkg$bn_round_break() then 'P{A},D{ OF_MAKE_DINNER, OF_MAKE_PAUSE, OF_MAKE_TO}'
          when ttb.action_type_pkg$is_break(action_type_id) then 'P{A},D{ OF_DELETE}'
          else '' end "ROW$POLICY"
          ,tr.licence
          ,tr.garage_num
          ,driver_last_name||' '||driver_name||' '||driver_middle_name fio
          ,tr_capacity_short_name tr_capacity_full_name
          ,qq.driver_id
       from qq
         join ttb.dr_shift ds on qq.dr_shift_id = ds.dr_shift_id
         left join core.driver on driver.driver_id = qq.driver_id
         left join core.v_tr_ref tr on tr.tr_id = qq.tr_id
       where (not qq.is_bn_round_break and rn = 1)
             or (qq.is_bn_round_break and rn = 2)
       order by qq.tt_out_id, qq.agg_time_begin, qq.time_begin;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

-------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_out_oper_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
  RETURNS text AS
$body$
declare
  l_r ttb.tt_out%rowtype;
  l_tt_action_id ttb.tt_action.tt_action_id%type;
  l_time_begin timestamp;
begin
  l_r := ttb.jf_tt_out_pkg$attr_to_rowtype(p_attr);
  l_tt_action_id	:= jofl.jofl_pkg$extract_number(p_attr, 'tt_action_id', true);
  l_time_begin		:= jofl.jofl_pkg$extract_date(p_attr, 'time_begin', true);
  l_r.tt_variant_id	:= jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);

  if l_time_begin < now() then
    raise exception 'Изменять можно только будущие рейсы ...';
  end if;

  select ttb.timetable_edit_pkg$update_out(l_tt_action_id, l_time_begin) into l_time_begin;

  perform ttb.jf_ttv_check_condition_pkg$of_close_prev_check_cond(l_r.tt_variant_id, null::integer);

  return null;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
COST 100;