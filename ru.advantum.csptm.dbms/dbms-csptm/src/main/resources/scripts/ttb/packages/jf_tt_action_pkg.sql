CREATE OR REPLACE FUNCTION ttb.jf_tt_action_pkg$get_prev_tt_action_id (
  p_tt_out_id integer,
  p_tt_action_id integer,
  p_action_type_id smallint = ttb."action_type_pkg$bn_round_break"()
)
RETURNS integer AS
$body$
declare 
	ln_tt_action_id 	ttb.tt_action.tt_action_id%type;
begin 
	select tat.tt_action_id 
      into ln_tt_action_id
      from ttb.tt_action tat
    where tat.tt_out_id = p_tt_out_id
      and tat.action_type_id = p_action_type_id
      and tat.dt_action <= 
         ( select tat_t.dt_action 
          from ttb.tt_action tat_t
          where tat_t.tt_out_id = p_tt_out_id
            and tat_t.tt_action_id = p_tt_action_id
             )
     order by tat.dt_action desc
     limit 1;

   return ln_tt_action_id;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_action_pkg$get_next_action_code (
  p_tt_out_id integer,
  p_tt_action_id integer,
  p_action_type_id smallint = ttb."action_type_pkg$bn_round_break"()
)
RETURNS text AS
$body$
declare 
	lt_action_type_code 	ttb.action_type.action_type_code%type;
begin 
	select right(at.action_type_code, 2) --убираем признаки укоротов и удлинотов и прочая
      into lt_action_type_code
      from ttb.tt_action tat
      join ttb.action_type at on at.action_type_id = tat.action_type_id
    where tat.tt_out_id = p_tt_out_id
      and (tat.action_type_id = p_action_type_id or ttb.action_type_pkg$is_production_round(tat.action_type_id))
      and tat.dt_action >=
         ( select tat_t.dt_action 
          from ttb.tt_action tat_t
          where tat_t.tt_out_id = p_tt_out_id
            and tat_t.tt_action_id = p_tt_action_id
             )
     order by tat.dt_action asc
     limit 1;

   return coalesce(lt_action_type_code, '');
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_action_pkg$get_prev_action_code (
  p_tt_out_id integer,
  p_tt_action_id integer,
  p_action_type_id smallint = ttb."action_type_pkg$bn_round_break"()
)
RETURNS text AS
$body$
declare 
	lt_action_type_code 	ttb.action_type.action_type_code%type;
begin 
	select right(at.action_type_code, 2) --убираем признаки укоротов и удлинотов и прочая
      into lt_action_type_code
      from ttb.tt_action tat
      join ttb.action_type at on at.action_type_id = tat.action_type_id
    where tat.tt_out_id = p_tt_out_id
      and (tat.action_type_id = p_action_type_id or ttb.action_type_pkg$is_production_round(tat.action_type_id))
      and tat.dt_action <=
         ( select tat_t.dt_action 
          from ttb.tt_action tat_t
          where tat_t.tt_out_id = p_tt_out_id
            and tat_t.tt_action_id = p_tt_action_id
             )
     order by tat.dt_action desc
     limit 1;

   return coalesce(lt_action_type_code, '');
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;