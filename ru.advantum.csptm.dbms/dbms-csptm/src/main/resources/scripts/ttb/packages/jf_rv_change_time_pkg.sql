CREATE OR REPLACE FUNCTION ttb.jf_rv_change_time_pkg$attr_to_rowtype(p_attr TEXT)
    RETURNS ttb.RV_CHANGE_TIME
LANGUAGE plpgsql
AS $function$
DECLARE
    l_r ttb.rv_change_time%ROWTYPE;
BEGIN
    l_r.route_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'route_variant_id', TRUE);
    l_r.change_time := jofl.jofl_pkg$extract_number(p_attr, 'change_time', TRUE);
    l_r.rv_change_time_id := jofl.jofl_pkg$extract_number(p_attr, 'rv_change_time_id', TRUE);
    l_r.tr_capacity_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_capacity_id', TRUE);
    IF (l_r.change_time < 0)
    THEN
        RAISE EXCEPTION '<<Время на пересменку не должно быть отрицательным!>>';
    END IF;
    RETURN l_r;
END;
$function$;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_rv_change_time_pkg$of_rows(p_id_account NUMERIC,
    OUT                                                      p_rows       REFCURSOR, p_attr TEXT)
    RETURNS REFCURSOR
LANGUAGE plpgsql
AS $function$
DECLARE
    l_route_variant_id ttb.rv_change_time.route_variant_id%TYPE:=jofl.jofl_pkg$extract_number(p_attr, 'route_variant_id', TRUE);
BEGIN
    OPEN p_rows FOR
    SELECT
        trct.route_variant_id AS route_variant_id,
        ctr.tr_capacity_id,
        ctr.short_name        AS tr_capacity_name,
        trct.rv_change_time_id,
        trct.change_time
    FROM ttb.rv_change_time trct
        JOIN core.tr_capacity ctr ON ctr.tr_capacity_id = trct.tr_capacity_id
    WHERE trct.route_variant_id = l_route_variant_id;
END;
$function$;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_rv_change_time_pkg$of_update(p_id_account NUMERIC, p_attr TEXT)
    RETURNS TEXT
LANGUAGE plpgsql
AS $function$
DECLARE
    l_r ttb.rv_change_time%ROWTYPE;
BEGIN
    l_r := ttb.jf_rv_change_time_pkg$attr_to_rowtype(p_attr);

    UPDATE ttb.rv_change_time
    SET
        change_time    = l_r.change_time,
        tr_capacity_id = l_r.tr_capacity_id
    WHERE
        rv_change_time_id = l_r.rv_change_time_id;
    RETURN NULL;
END;
$function$;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_rv_change_time_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
    RETURNS TEXT
LANGUAGE plpgsql
AS $function$
DECLARE
    l_rv_change_time_id ttb.rv_change_time.rv_change_time_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'rv_change_time_id', TRUE);
BEGIN
    DELETE FROM ttb.rv_change_time
    WHERE rv_change_time_id = l_rv_change_time_id;
    RETURN NULL;
END;
$function$;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_rv_change_time_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
    RETURNS TEXT
LANGUAGE plpgsql
AS $function$
DECLARE
    l_r ttb.rv_change_time%ROWTYPE;
BEGIN
    l_r := ttb.jf_rv_change_time_pkg$attr_to_rowtype(p_attr);

    INSERT INTO ttb.rv_change_time (route_variant_id, tr_capacity_id, change_time)
    VALUES (l_r.route_variant_id, l_r.tr_capacity_id, l_r.change_time);
    RETURN NULL;
END;
$function$;