CREATE OR REPLACE FUNCTION ttb.jf_tt_dispersal_parms_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 ln_dispersal_task_id	integer	:= jofl.jofl_pkg$extract_number(p_attr, 'dispersal_task_id', true);
begin 

 open p_rows for 
 	select dt.dispersal_task_id,	
        jofl.jofl_pkg$extract_number(dt.dt_parms::text, 'f_stop_item_id', true) stop_item_id,
        s.name as stop_item_name,
        jofl.jofl_pkg$extract_date(dt.dt_parms::text, 'f_disp_DT', true) disp_DT,
        jofl.jofl_pkg$extract_date(dt.dt_parms::text, 'b_disp_time_DT', true) b_disp_time_DT,
        jofl.jofl_pkg$extract_date(dt.dt_parms::text, 'e_disp_time_DT', true) e_disp_time_DT,
        to_char(jofl.jofl_pkg$extract_date(dt.dt_parms::text, 'b_disp_time_DT', true), 'hh24:mi') || ' - ' ||
        to_char(jofl.jofl_pkg$extract_date(dt.dt_parms::text, 'e_disp_time_DT', true), 'hh24:mi') as disp_period,
        jofl.jofl_pkg$extract_number(dt.dt_parms::text, 'f_min_disp_NUMBER', TRUE) min_disp_int,
        jofl.jofl_pkg$extract_number(dt.dt_parms::text, 'f_max_work_NUMBER', TRUE) max_work_minute,
        --jofl.jofl_pkg$extract_narray(dt.dt_parms::text, 'f_list_tt_variant_id', true) list_tt_variant_id, 
        (select string_agg(ttv.ttv_name, ',') from ttb.tt_variant ttv where ttv.tt_variant_id = any (jofl.jofl_pkg$extract_narray(dt.dt_parms::text, 'f_list_tt_variant_id', true))) ttv_list	
    from ttb.dispersal_task dt
    join rts.stop_item si on si.stop_item_id = jsonb_extract_path_text(dt.dt_parms, 'f_stop_item_id')::int
    join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
    join rts.stop s on s.stop_id = sl.stop_id
    where dt.dispersal_task_id = ln_dispersal_task_id;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;