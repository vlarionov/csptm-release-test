CREATE OR REPLACE FUNCTION ttb.jf_rep_transwork_violation_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare

begin

 open p_rows for 
     select
null as depo_name, /* Наименование ТП */
 null as tr_type_name, /* Вид ТС */
 null as route_num, /* Номер маршрута */
 null as out_num, /* Номер выхода */
 null as garage_num, /* Гар. номер ТС */
 null as dr_tab_num, /* Водитель */
 null as round_type, /* Тип произв. рейса */
 null as stop_name, /* Наименование ОП */
 null as dt_time, /* Дата и время */
 null as move_direct, /* Направление движения */
 null as violation_time, /* Время нарушения */
 null as violation_tp /* Тип нарушения */	 
;
end;
$function$
;
