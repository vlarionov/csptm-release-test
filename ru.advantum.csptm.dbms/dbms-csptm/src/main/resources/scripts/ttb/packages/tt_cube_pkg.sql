﻿------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Создать временную таблицу
***************************************************************************/
create or replace function ttb.tt_cube_pkg$make_tt_cube(p_tt_variant_id numeric)
  returns void
as
$body$
declare
begin
  discard temp;

  create  temporary sequence  if not exists  tt_cube_tt_cube_id_seq
   start  with 1;

  create temporary table if not exists  tt_cube
  --with  (fillfactor  = 90)
  on commit preserve rows

tablespace ttb_data  as
    select * from ttb.tt_cube
    where tt_variant_id = p_tt_variant_id
    --and tt_tr_id is null
    with data;

  alter table tt_cube alter column tt_cube_id set default nextval('tt_cube_tt_cube_id_seq'::regclass);


  alter table tt_cube add constraint pk_tt_cube primary key (tt_cube_id);

 -- create unique index uq_tt_cube on tt_cube(tt_variant_id, tt_schedule_id, tr_capacity_id,  action_type_id,time_begin, time_end, order_num, tmp_id, one_id)  tablespace ttb_idx;

  alter table tt_cube add  constraint uq_tt_cube unique (tt_variant_id, tt_schedule_id, tr_capacity_id,  action_type_id,time_begin, time_end, order_num, tmp_id, one_id);

  perform ttb.tt_cube_pkg$make_tt_cube_idx();
end;
$body$
language plpgsql volatile
cost 100;


------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Добавить данные во временную таблицу
***************************************************************************/
create or replace function ttb.tt_cube_pkg$append_tt_cube(p_tt_variant_id numeric)
  returns void
as
$body$
declare
begin
    insert into tt_cube
    select * from ttb.tt_cube
    where tt_variant_id = p_tt_variant_id
    and tt_tr_id is not null
    and not exists (select 1 from tt_cube where tt_variant_id = p_tt_variant_id );

end;
$body$
language plpgsql volatile
cost 100;

------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Индексирование и анализ
***************************************************************************/

create or replace function ttb.tt_cube_pkg$make_tt_cube_idx()
  returns void
as
$body$
declare
begin



  create index if not exists ix_tt_cube_id_tt_var_sched
    on tt_cube
    using btree
    (tt_variant_id, tt_schedule_id)
  tablespace ttb_idx;

  create index if not exists ix_tt_cube_id_tt_var_tr
    on tt_cube
    using btree
    (tt_variant_id)
  tablespace ttb_idx;

  create index if not exists ix_tt_cube_id_tt_schedule_item_id
    on tt_cube
    using btree
    (tt_schedule_item_id)
  tablespace ttb_idx
    ;

  create index if not exists ix_tt_cube_sch_id_tmp_id_one_id
    on tt_cube
    using btree
    (tt_schedule_id, tmp_id, one_id)
  tablespace ttb_idx;



  create index if not exists ix_tt_cube_tt_schedule_id
    on tt_cube
    using btree    ( tt_schedule_id)
  tablespace ttb_idx
    ;
  create index if not exists ix_tt_cube_tt_tr_id
    on tt_cube
    using btree    ( tt_tr_id)
  tablespace ttb_idx
  ;

/*  create index if not exists ix_tt_cube_stop_item2round_id
    on tt_cube
    using btree    (stop_item2round_id)
  tablespace ttb_idx;*/

  create index if not exists ix_tt_cube_action_type_id
    on tt_cube
    using btree    (action_type_id)
  tablespace ttb_idx;

  create index if not exists ix_tt_cube_tm
    on tt_cube
    using btree    (time_end, time_begin)
  tablespace ttb_idx;

  create index if not exists ix_tt_cube_tm_desc
    on tt_cube
    using btree    (time_end desc, time_begin desc)
  tablespace ttb_idx;


  create index if not exists ix_tt_cube_id_tt_schedule_id_tr_not_null
    on tt_cube
    using btree    (tt_schedule_id,  order_num)
  tablespace ttb_idx
    where  tt_tr_id is not null;


  create index if not exists ix_tt_cube_id_tt_schedule_id_tr_null
    on tt_cube
    USING btree
    (tt_schedule_id, order_num)
  TABLESPACE ttb_idx
    where  tt_tr_id is null;

  create index if not exists ix_tt_cube_one_id
    on tt_cube
    using btree    ( one_id)
  tablespace ttb_idx
  ;

  create index if not exists ix_tt_cube_tmp_tr_capacity_id
    on tt_cube
    using btree    ( tmp_id, tr_capacity_id)
  tablespace ttb_idx
  ;
  analyze  tt_cube;
end;
$body$
language plpgsql volatile
cost 100;

------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Заполнение структуры в БД из временной
если запись в таких id есть, то это назначение ТС - обновляем
***************************************************************************/
create or replace function ttb.tt_cube_pkg$fill_db_table(p_tt_variant_id numeric,
                                                         p_tt_tr_id numeric default null    )
  returns void
as
$body$
declare
  l_cnt numeric;
begin

    insert into  ttb.tt_cube(stop_item2round_id, tr_capacity_id, tt_variant_id, action_type_id, time_begin, time_end, tt_at_round_id, dur_from_beg, dur_round, tt_schedule_id, order_num, tt_schedule_item_id, tmp_id, round_id, tt_action_type2group_id, is_finish_stop, tr_type_id, tt_tr_id, stop_item_id, order_num_action, dr_shift_id, one_id, parent_one_id, is_require, is_intvl, tt_driver_id)

    select DISTINCT stop_item2round_id, tr_capacity_id, tt_variant_id, action_type_id, time_begin, time_end, tt_at_round_id, dur_from_beg, dur_round, tt_schedule_id, order_num, tt_schedule_item_id, tmp_id, round_id, tt_action_type2group_id, is_finish_stop, tr_type_id, tt_tr_id, stop_item_id, order_num_action, dr_shift_id, one_id, parent_one_id, is_require, coalesce(is_intvl, false), tt_driver_id
    from  tt_cube
    --where p_tt_tr_id is null          or  tt_tr_id = p_tt_tr_id
  ON CONFLICT ON CONSTRAINT  uq_tt_cube DO UPDATE SET tt_tr_id = EXCLUDED.tt_tr_id;


end;
$body$
language plpgsql volatile
cost 100;

---------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.tt_cube_pkg$clear_tt_cube (
  p_tt_variant_id numeric
)
RETURNS void AS
$body$
declare
begin

  delete from ttb.tt_cube
  where tt_variant_id = p_tt_variant_id;
 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.tt_cube_pkg$clear_tt_cube_all (
)
RETURNS void AS
$body$
declare
  ttv_rec record;
begin
  
  for ttv_rec in
  (
  select tv.tt_variant_id
  from ttb.tt_variant tv
  where not ttb.jf_tt_variant_pkg$of_get_policy(tv.tt_variant_id)
  and tv.parent_tt_variant_id is null)
  loop
  	perform ttb.tt_cube_pkg$clear_tt_cube(ttv_rec.tt_variant_id);
  end loop;
  
  analyze ttb.tt_cube;
  
  perform ttb.tt_out_pkg$clear_deleted_out();
  
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;



create or replace function ttb.tt_cube_pkg$prepare_query()  returns VOID
as
$body$
declare
begin

PREPARE prepare_sql (
  /*1 p_tt_variant_id */numeric,
  /*2 p_tt_schedule_id */numeric,
  /*3 l_is_has_child*/ boolean,
  /*4 p_tt_tr_id */numeric,
  /*5 p_tm_start */timestamp,
  /*6 p_finish_min */numeric,
  /*7 p_finish_max */numeric,
  /*8 p_break_round_time  */numeric,
  /*9 p_calc_type  */numeric,
  /*10 p_ref_group_kind_id */numeric,
  /*11 p_start_stop_item_id */numeric,
  /*12 p_is_start */boolean,
  /*13 p_sh_part */numeric,
  /*14_p_break_dur */numeric) as


with recursive cb as (

    select
       tt_schedule_id::text ||'-'|| cb.tmp_id::text ||'-'|| cb.one_id::text  as id
      ,cb.one_id
      ,cb.tmp_id
      ,cb.parent_one_id
      ,cb.stop_item_id
      ,cb.time_begin
      ,cb.time_end
      ,cb.action_type_id
      ,cb.is_require
      ,cb.tt_schedule_id
      ,cb.is_intvl
      ,first_value(cb.stop_item_id) over(partition by cb.tt_schedule_id ,cb.tmp_id , cb.one_id order by cb.time_end, cb.time_begin ) as sp_b
      ,first_value(cb.stop_item_id) over(partition by cb.tt_schedule_id ,cb.tmp_id , cb.one_id order by cb.time_end desc,  cb.time_begin desc) as sp_e
      ,vma.is_production_round::int as has_rnd
      ,vma.is_bn_round_break as is_bn_round_break
    from tt_cube cb
      join ttb.tt_tr on cb.tr_capacity_id =tt_tr.tr_capacity_id and  tt_tr.tt_tr_id = $4
      join unnest(ttb.tt_schedule_pkg$get_schedule_list($2)) sch on sch = cb.tt_schedule_id
      join ttb.vm_action_type vma on vma.action_type_id = cb.action_type_id
    where
      cb.tt_variant_id = $1
      and ($3 or cb.tt_schedule_id = $2)
      and (cb.is_finish_stop or cb.order_num =1)
      and cb.tt_tr_id is null
)
  /*уже распределенное по ТС время*/
  ,ex_tr as (
    select time_begin, stop_item_id
    from  tt_cube cb
      join ttb.vm_action_type vma on vma.action_type_id = cb.action_type_id and vma.is_production_round
    where
      tt_schedule_id = $2  /*по основной последовательности!*/
      and cb.order_num =1
      and tt_tr_id is not null
)
  /*уже распределенное по ТС время на межрейсы (для устранения перехлеста проводов)*/
  ,ex_tr_bn as (
  /*группируем межрейсы из частичек*/
    select stop_item_id  ,
      min (time_begin)  as time_begin,
      max (time_end) as time_end
    from(
          select   t.*,  sum((t.time_end = lead_r or time_begin !=lag_r)::int) over (partition by tt_tr_id, stop_item_id order by time_end, time_begin) rs
          from(
                select time_begin, time_end ,stop_item_id, tt_tr_id, lead(time_begin, 1 , time_end) over (partition by tt_tr_id, stop_item_id order by time_end, time_begin  ) as lead_r
                  , lag(time_end, 1 , time_begin) over (partition by tt_tr_id, stop_item_id order by time_end, time_begin  ) as lag_r
                from  tt_cube cb
                  join ttb.vm_action_type vma on vma.action_type_id = cb.action_type_id and vma.is_bn_round_break
                where
                  tt_schedule_id = $2  /*по основной последовательности!*/
                  and tt_tr_id is not null
              ) t
        ) d
    group by stop_item_id  ,   tt_tr_id , rs
)


  , tt_act_group_stop as (
    select fstop_item_id as stop_item_id
    from ttb.timetable_pkg$get_ref_group_kind_data ($2, $10)
    where  faction_type_id   != ttb.action_type_pkg$bn_round_break() /*КОСТЫЛЬ нужно корректно получать ОП, на котором заканчивается группа*/
)
  , last_rnd as (

    select atg.start_time,  sir.stop_item_id
    from ttb.atgrs_time atg
      join ttb.tt_at_round_stop ars on ars.tt_at_round_stop_id = atg.tt_at_round_stop_id
      join ttb.tt_at_round ar on ar.tt_at_round_id = ars.tt_at_round_id
      join ttb.tt_action_type tact on tact.tt_action_type_id = ar.tt_action_type_id
      join rts.stop_item2round sir on sir.stop_item2round_id = ars.stop_item2round_id and not sir.sign_deleted
    where atg.fix_arrival_type_id = ttb.fix_arrival_type_pkg$finish_time()
          and sir.order_num =1
          and tact.tt_variant_id = $1
)
  /*период последовательности*/
  , per as (
    select  time_begin as tm_b,  time_end as tm_e, interval_time as int_tm from  ttb.tt_period p where   p.tt_schedule_id = $2
)
  , adata as (
    select
      cb.one_id
      ,cb.tmp_id
      ,cb.id
      ,min(cb.time_begin) as time_b
      ,max(cb.time_end) as time_e
      ,max(cb.time_end) filter (where not cb.is_bn_round_break) as time_e_rnd
      ,sp_b
      ,sp_e
      ,parent_one_id
      ,is_require
      ,is_intvl
      ,cb.tt_schedule_id
      /*--приоритет для дочерних последовательностей (инверсия)*/
      ,(cb.tt_schedule_id = $2)
       /*--приоритет последнего рейса*/
       and coalesce((select 1 from  last_rnd where last_rnd.stop_item_id = sp_b and last_rnd.start_time= min(cb.time_begin)  ),0)= 1 as is_main
      , $5 as tm_start
      , min(p.int_tm) as int_tm
     /* --получаем секунды между ближайшими рейсами*/
      ,coalesce((select abs(extract(epoch from  min(ex_tr.time_begin-min(cb.time_begin))))  from  ex_tr
    where stop_item_id = cb.sp_b
          and min(cb.time_begin) between ex_tr.time_begin - make_interval(secs:=min(p.int_tm))
          and ex_tr.time_begin + make_interval(secs:= min(p.int_tm))),99999) as delta

    from cb
      , lateral (select  round(min(int_tm))-1 as int_tm from  per where  cb.time_begin between per.tm_b and per.tm_e) p
    group by cb.one_id
      ,cb.tmp_id
      ,cb.id
      ,sp_b
      ,sp_e
      ,parent_one_id
      ,is_require
      ,is_intvl
      ,cb.tt_schedule_id
    /*--ограничиваем выборку для быстродействия*/
    having  min(cb.time_begin) between  $5 and $5 + ($7 +$8) * interval '1 second'
            and sum (has_rnd) >0
    order by 4
)
  /*--пункты выравниывния*/
  , sp_alig as (
    select fstop_item_id from  ttb.timetable_pkg$get_stop_interval_alignment($1) t
)
  , min_tm as (
  select $5 tm where  $12
  union all
  (
    select time_b tm
    from adata
    where sp_b = $11
          and not  $12
          and  adata.parent_one_id is null
          /*--проверка на то чтобы начало не превышало время, на которое можно перерыв увеличивать
          --после пересменки не более получаса просто так поставила*/
          and ( time_b between  $5 and  $5 +  make_interval(secs:= case $13 when 1 then $8/2 else  $14 end))
          --проверка на перехлест проводов
          and ( $13 > 1
                or  not exists ( select 1 from ex_tr_bn
    where stop_item_id = adata.sp_b
          and ((adata.tm_start between ex_tr_bn.time_begin and ex_tr_bn.time_end
                and  adata.time_b< ex_tr_bn.time_end
               ) or
               ( adata.tm_start >= ex_tr_bn.time_begin
                 and adata.time_b between ex_tr_bn.time_begin and ex_tr_bn.time_end
               )
               or
               ( adata.tm_start < ex_tr_bn.time_begin
                 and adata.time_b > ex_tr_bn.time_end
               )
          )
    )
          )
    order by
      /*--укороты вперед!*/
      (adata.tt_schedule_id  = $2)
      , delta desc
      , not (adata.is_require or adata.is_intvl or adata.is_main), time_b
    limit 1

  )
)
  ,r1 as  (
    select
                row_number() over w as r
      , d1.time_b tm , d2.time_b as next_tm
      , d1.id , d2.id  as next_id
      , d1.tmp_id, d1.one_id
      , d2.time_b-d1.time_b as dur
      , d1.sp_b,  d1.sp_e
      , d1.time_e_rnd-d1.time_b as dur_rnd
      , d2.time_b- d1.time_e_rnd as bn
      , d1.time_e time_e_rnd
      , d2.tt_schedule_id
    from adata d1
      , adata d2
    where d1.time_b >= (select tm from min_tm)
          and d1.sp_e = d2.sp_b
          and  d2.time_b  between d1.time_e and  d1.time_e + case (select count(1) from sp_alig  where fstop_item_id = d1.sp_e) when 0 then  interval '0 second' else $8* interval '1 second'  - (d2.time_b- d1.time_e_rnd) end
          and (d2.parent_one_id = d1.one_id or d2.parent_one_id is null)
          /*--если это пункт выравнивания проверяем чтобы не накладывались рейсы друг на друга*/
          and ((select count(1) from sp_alig  where fstop_item_id = d1.sp_e) = 0
               or
               d1.tt_schedule_id  != $2
               or(
                 not exists ( select 1 from ex_tr_bn
                 where stop_item_id = d2.sp_b
                       and (
                         (d1.time_e_rnd between ex_tr_bn.time_begin and ex_tr_bn.time_end
                          and d2.time_b< ex_tr_bn.time_end) or

                         ( d1.time_e_rnd >= ex_tr_bn.time_begin
                           and d2.time_b between ex_tr_bn.time_begin and ex_tr_bn.time_end
                         )
                         or
                         ( d1.time_e_rnd < ex_tr_bn.time_begin
                           and d2.time_b > ex_tr_bn.time_end
                         )
                       )
                 )
               )

          )

    window w as (partition by d1.id
      order by
        /*--укороты вперед!*/
        (d2.tt_schedule_id  = $2)
        /*--подальше от других рейсов*/
        ,d2.delta desc
        ,d1.delta desc
        /*--если из-за межрейса пропускаем интервальный старт рейса, то снимаем ограничения обязательности*/
        ,   not ( not exists (select 1 from adata d_inbn where d1.sp_e = d_inbn.sp_b
                                                               and  d_inbn.time_b  between d1.time_e_rnd and d1.time_e
                                                               and  coalesce(d_inbn.is_intvl, false)
                                                               and (d_inbn.parent_one_id = d1.one_id or d_inbn.parent_one_id is null))
                  or d2.is_require or d2.is_intvl or d2.is_main)
        , d2.time_b )

)

  ,r as (
  select  * from r1 where r=1
  union all
  /*--захватываем последние рейсы*/
  select    2 as r
    , adata.time_b tm , adata.time_e as next_tm
    , adata.id , null  as next_id
    , adata.tmp_id, adata.one_id
    , adata.time_e-adata.time_b as dur
    , adata.sp_b,  adata.sp_e
    , adata.time_e_rnd-adata.time_b as dur_rnd
    , adata.time_e- adata.time_e_rnd as bn
    , adata.time_e_rnd
    , adata.tt_schedule_id
  from r1
    join adata on adata.id = r1.next_id
  where r1.r < 2
        and not exists (select 1 from r1 t_r where t_r.id = r1.next_id and t_r.r=1)

)


  --цикл для нахождения последовательности
  , m (tm, next_tm,id, next_id, tmp_id, one_id,  dur, bn, time_e_rnd, sp_b, sp_e,  tt_schedule_id,  level, path, path2, cycle ) as(
  select  tm, next_tm,r.id, next_id, tmp_id, one_id,   dur, bn, time_e_rnd, sp_b, sp_e, tt_schedule_id,   1, ARRAY[r.tm], ARRAY[r.id], false
  from r
  union all
  select r.tm, r.next_tm,r.id, r.next_id, r.tmp_id, r.one_id, r.dur + m.dur, r.bn, r.time_e_rnd,  m.sp_b, r.sp_e, r.tt_schedule_id, m.level + 1,  path || r.tm, path2 || r.id,  r.tm = ANY(path)
  from m
    join r on r.tm= m.next_tm and r.id= m.next_id AND NOT cycle
)
  /*-- получаем возможные последовательности*/
  ,prev_itog as  (
    select
        m.path f_tm
      ,m.path2 f
      ,(select tm from min_tm) as min_tm
      ,m.*
      /*--межрейса нет до обеда на КП и до пересменки
      --  , case $10 when ttb.ref_group_kind_pkg$dinner_action() then
      --(select ttb.norm_pkg$get_schedule_tm ( m.tt_schedule_id, ttb.ref_group_kind_pkg$dinner_action(), tm::time, tm::time+ interval 'hours:=1', sp_e)) else 0 end * make_interval(secs:=1) as dur_rnd_dinner
      --продолжительность произв. рейса до обеда*/
      , case when $10!=ttb.ref_group_kind_pkg$last_action() then bn else interval '0' end as dur_bn
    from m
    where  sp_e in (select stop_item_id  from tt_act_group_stop)
           and sp_b = $11
)
  ,itog as  (
    select *
    from prev_itog
    where dur - bn
    between $6 * interval '1 second' and  $7 * interval '1 second'
)

select adata.tmp_id, adata.one_id, adata.tt_schedule_id,  adata.time_b
from unnest(array (select f
                   from itog
                   where   array_length(itog.f,1) >0 and
                           (($13 = 1 and array_position(path, min_tm)=1)
                            or ($13 != 1
                                and path[1] between min_tm and min_tm+ make_interval (secs:=least (600, $14-extract(epoch from (min_tm- $5))) )))
                   order by  cardinality(f_tm) desc limit 1 )) f
  join adata on adata.id = f.f
where $9 = 1  /*--по первому выходу*/
union all
select adata.tmp_id, adata.one_id, adata.tt_schedule_id,  adata.time_b
from unnest(array (select f
                   from itog
                   where   array_length(itog.f,1) >0 and
                           (($13 = 1 and array_position(path, min_tm)=1)
                            or ($13 != 1
                                and path[1] between min_tm and min_tm+ make_interval (secs:=least (600, $14-extract(epoch from (min_tm- $5))) )))
                   order by cardinality(f_tm)  limit 1 offset 1 )) f
  join adata on adata.id = f.f
where $9 = 2  /*--по первому выходу минимально возможное количество рейсов*/
union all
select adata.tmp_id, adata.one_id, adata.tt_schedule_id,  adata.time_b
from  unnest(array (select f from itog where   array_length(itog.f,1) >0 and f_tm=path order by cardinality(f_tm) desc  limit 1) ) f
  join adata on adata.id = f.f
where  $9 =3 /*--по макс кол-ву рейсов*/
union all
select adata.tmp_id, adata.one_id, adata.tt_schedule_id,  adata.time_b
from  unnest(array (select f from itog where  (select path from itog where   array_length(itog.f,1) >0 order by  cardinality(f_tm) desc limit 1)<@ path  limit 1) ) f
  join adata on adata.id = f.f
where  $9 =4 /*--по первому максимальному количеству*/
union all
select adata.tmp_id, adata.one_id, adata.tt_schedule_id,  adata.time_b
from unnest(array (select f
                   from itog
                   where   array_length(itog.f,1) >0 and
                           (($13 = 1 and array_position(path, min_tm)=1)
                            or ($13 != 1
                                and path[1] between min_tm and min_tm+ make_interval (secs:=least (600, $14-extract(epoch from (min_tm- $5))) )))
                   order by random() limit 1 )) f
  join adata on adata.id = f.f
where $9 = 5  /*--по первому выходу случайным образом*/
;

/*
  execute prepare_sql(2682526, 460, false, 3979,      '1900-01-01 06:50:00'::timestamp
  , 0
  , 20040
  , 3600
  , 4
  , 99
  , 89156
  , false
  , 3
  , 6360)


    return query
  EXECUTE prepare_sql (


    p_tt_variant_id::int,
    p_tt_schedule_id::int,
    l_is_has_child::bool,
    p_tt_tr_id::int,
    p_tm_start::timestamp,
    p_finish_min::numeric,
    p_finish_max::numeric,
    p_break_round_time::numeric,
    p_calc_type::int,
    p_ref_group_kind_id::int,
    p_start_stop_item_id::int,
    p_is_start::bool,
    p_sh_part::int,
    p_break_dur::int)

*/
end;
$body$
language plpgsql volatile
;