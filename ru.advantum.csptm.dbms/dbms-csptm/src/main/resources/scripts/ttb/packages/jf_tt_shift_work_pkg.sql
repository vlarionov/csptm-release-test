CREATE OR REPLACE FUNCTION ttb.jf_tt_shift_work_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 ln_tt_variant_id  ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
 ln_tt_out_id	   ttb.tt_out.tt_out_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_out_id', true);
begin 
 open p_rows for 
 with t as (
 		select --distinct 
        		qq.tt_out_id  
       		   ,ds.dr_shift_num   
               ,(select tt_out_num from ttb.tt_out where tt_out_id = qq.tt_out_id) as tt_out_num 
               ,min(qq.agg_time_begin) over (partition by qq.tt_out_id, qq.dr_shift_id) as time_begin_shift
               ,max(qq.agg_time_end) over (partition by qq.tt_out_id, qq.dr_shift_id) as time_end_shift
               ,extract(epoch from (max(qq.agg_time_end) over (partition by qq.tt_out_id, qq.dr_shift_id) -
                min(qq.agg_time_begin) over (partition by qq.tt_out_id, qq.dr_shift_id))) as time_work_shift
               ,min(qq.agg_time_begin) over (partition by qq.tt_out_id) as time_begin_out
               ,max(qq.agg_time_end) over (partition by qq.tt_out_id) as time_end_out
               ,extract(epoch from (max(qq.agg_time_end) over (partition by qq.tt_out_id) -
                min(qq.agg_time_begin) over (partition by qq.tt_out_id))) as time_work_out
               ,qq.action_type_id
               ,qq.agg_time_begin
               ,qq.agg_time_end
               ,coalesce(td.is_сhanged_driver, false) as is_сhanged_driver
               from ttb.tt_out_pkg$get_set4out_action_agg (ln_tt_variant_id) qq
               join ttb.dr_shift ds on qq.dr_shift_id = ds.dr_shift_id
               left join ttb.tt_driver_action tda on tda.tt_action_id = qq.tt_action_id
         	   left join ttb.tt_driver td on td.tt_driver_id = tda.tt_driver_id
               where qq.tt_out_id = ln_tt_out_id
               ) 
     ,out_work as (
       select  distinct
       		   t.tt_out_id  
              ,t.dr_shift_num   
              ,t.tt_out_num 
              ,t.time_begin_shift
              ,t.time_end_shift
              ,t.time_work_shift
              ,t.time_begin_out
              ,t.time_end_out
              ,t.time_work_out
              --,ttb.jf_tt_shift_work_pkg$of_get_break_time(t.tt_out_id, t.dr_shift_num, 2683837) as break_time
       from t
       ) 
     ,out_break_main_dr as (     
             select t.tt_out_id
                   ,t.dr_shift_num
                   ,sum (case 
                          when extract (epoch from (t.agg_time_end - t.agg_time_begin)) > asd.get_constant('TIME_BREAK_IN_SHIFT') 
                            then extract (epoch from (t.agg_time_end - t.agg_time_begin)) 
                          else 0
                        end
                        ) res_main_dr
           from t                   
             where ttb.action_type_pkg$is_break(t.action_type_id) 
             and t.action_type_id <> ttb.action_type_pkg$bn_round_break()
           group by t.tt_out_id
                   ,t.dr_shift_num
     )
     ,out_break_chng_dr as (
             select t.tt_out_id
                   ,t.dr_shift_num
                   ,sum (case 
                          when extract (epoch from (t.agg_time_end - t.agg_time_begin)) > asd.get_constant('TIME_BREAK_IN_SHIFT') 
                            then extract (epoch from (t.agg_time_end - t.agg_time_begin)) 
                          else 0
                        end
                        ) res_chng_dr
           from t                   
             where t.is_сhanged_driver
           group by t.tt_out_id
                   ,t.dr_shift_num
     )
       select   ow.tt_out_num
               ,max(case when ow.dr_shift_num = 1 then ow.time_work_shift - (coalesce(obm.res_main_dr, 0) + coalesce(obc.res_chng_dr, 0)) else null end) as time_work_first_shift
               ,max(case when ow.dr_shift_num = 2 then ow.time_work_shift - (coalesce(obm.res_main_dr, 0) + coalesce(obc.res_chng_dr, 0)) else null end) as time_work_second_shift
               ,max(case when ow.dr_shift_num = 3 then ow.time_work_shift - (coalesce(obm.res_main_dr, 0) + coalesce(obc.res_chng_dr, 0)) else null end) as time_work_third_shift
               ,max(ow.time_work_out) - sum(coalesce(obm.res_main_dr, 0) + coalesce(obc.res_chng_dr, 0)) as time_work_all_shift   
       from out_work ow 
       left join out_break_main_dr obm on ow.tt_out_id = obm.tt_out_id
       							 	  and ow.dr_shift_num = obm.dr_shift_num
       left join out_break_chng_dr obc on ow.tt_out_id = obc.tt_out_id
       							 	  and ow.dr_shift_num = obc.dr_shift_num
       group by ow.tt_out_num
       ;
            
      
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_shift_work_pkg$of_get_break_time (
  p_tt_out_id integer,
  p_dr_shift_num smallint
)
RETURNS integer AS
$body$
declare 
   ln_res_main_dr 	integer;
   ln_res_change_dr integer;
begin 
	select  sum (case 
          		  when extract (epoch from (qq.agg_time_end - qq.agg_time_begin)) > asd.get_constant('TIME_BREAK_IN_SHIFT') 
                    then extract (epoch from (qq.agg_time_end - qq.agg_time_begin)) 
                  else 0
           		end
          		) 
   into ln_res_main_dr
   from ttb.tt_out_pkg$get_set4out_action_agg ((select tt_variant_id from ttb.tt_out where tt_out_id = p_tt_out_id limit 1)) qq 
    join ttb.dr_shift ds on qq.dr_shift_id = ds.dr_shift_id                   
     where ttb.action_type_pkg$is_break(qq.action_type_id) 
     and qq.action_type_id <> ttb.action_type_pkg$bn_round_break()
     and ds.dr_shift_num = p_dr_shift_num 
     and qq.tt_out_id = p_tt_out_id
   group by qq.tt_out_id
  		   ,ds.dr_shift_num;
           
   select sum(extract(epoch from(t.res_sum)))
   into ln_res_change_dr
   from (
        select max(tai.time_end)
              ,min(tai.time_begin)
              ,max(tai.time_end) - min(tai.time_begin) res_sum
         from ttb.tt_out out 
         join ttb.tt_action ta on ta.tt_out_id = out.tt_out_id
         join ttb.action_type at on at.action_type_id = ta.action_type_id
         join ttb.tt_action_item tai on tai.tt_action_id = ta.tt_action_id        
         join ttb.dr_shift ds on tai.dr_shift_id = ds.dr_shift_id   
         join ttb.tt_driver_action tda on tda.tt_action_id = ta.tt_action_id
         join ttb.tt_driver td on td.tt_driver_id = tda.tt_driver_id  
                              and td.is_сhanged_driver                         
           and ds.dr_shift_num = p_dr_shift_num 
           and out.tt_out_id = p_tt_out_id
         group by tai.tt_action_id
     ) t;           
           

   return coalesce(ln_res_main_dr, 0) + coalesce(ln_res_change_dr, 0);
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;