CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_approved_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
begin 
 open p_rows for 
            select  ttv.tt_variant_id
      		 ,ttv.ttv_name  
             ,'[' || json_build_object('CAPTION', ttv.ttv_name, 'JUMP',
               		 json_build_object('METHOD', 'ttb.tt_variant_view_only', 'ATTR', '{"f_tt_variant_id":"' || ttv.tt_variant_id || '"}')) || ']' as jump_to_tt_variant
             ,r.route_num
             ,trt.name as tr_type_name
             ,lower(ttv.action_period) as ap_b
             ,upper(ttv.action_period) as ap_e
             ,string_agg(ct.tag_name, ';') as agg_tag_name
             ,string_agg(ct.tag_name_short, ';') as agg_tag_name_short             
            from ttb.tt_variant ttv
            join ttb.norm n on n.norm_id=ttv.norm_id
            join ttb.tt_status st on st.tt_status_id = ttv.tt_status_id
            join rts.route_variant rv on ttv.route_variant_id = rv.route_variant_id
            join rts.route r on rv.route_id = r.route_id
            join core.tr_type trt on trt.tr_type_id = r.tr_type_id
            join ttb.tt_calendar c on c.tt_variant_id = ttv.tt_variant_id
            join ttb.calendar_tag ct on ct.calendar_tag_id = c.calendar_tag_id
        where ttv.tt_status_id = ttb.jf_tt_status_pkg$cn_approved_status_id()
 		  and not ttv.sign_deleted
		  and ttv.parent_tt_variant_id is null
        group by 
        	  ttv.tt_variant_id
      		 ,ttv.ttv_name  
             ,r.route_num
             ,trt.name
             ,lower(ttv.action_period)
             ,upper(ttv.action_period);
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;