CREATE OR REPLACE FUNCTION ttb.jf_tt_out_stops_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 ln_tt_action_id		ttb.tt_action.tt_action_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_action_id', true);	
begin 

 open p_rows for 
     select distinct
            /* out.tt_out_id
            ,out.tt_out_num
            ,ta.action_dur
            ,at.action_type_code
            ,at.action_type_name
            ,md.move_direction_name
            ,r.code
            ,*/
             ta.tt_action_id
            ,at.action_type_id
            ,si2r.order_num
            ,s.name stop_name
            ,tai.time_begin
            ,tai.time_end
            ,si2r.length_sector len
            ,r.round_id
            ,s.stop_id
    from ttb.tt_out out 
    join ttb.tt_action ta on ta.tt_out_id = out.tt_out_id
    join ttb.action_type at on at.action_type_id = ta.action_type_id
    join ttb.tt_action_item tai on tai.tt_action_id = ta.tt_action_id
    join ttb.tt_action_round tar on tar.tt_action_id = ta.tt_action_id
    join rts.round r on r.round_id = tar.round_id
                     and r.action_type_id = at.action_type_id
    join rts.move_direction md on md.move_direction_id = r.move_direction_id
    join rts.stop_item2round si2r on si2r.round_id = r.round_id 
                                  and si2r.stop_item2round_id = tai.stop_item2round_id   
    join rts.stop_item si on si.stop_item_id = si2r.stop_item_id								  
    join rts.stop_location sl on sl.stop_location_id = si.stop_location_id                               
    join rts.stop_item2round_type si2rt on si2rt.stop_item2round_id = si2r.stop_item2round_id    
    join rts.stop s on s.stop_id = sl.stop_id
    where ta.tt_action_id = ln_tt_action_id
    order by si2r.order_num;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;