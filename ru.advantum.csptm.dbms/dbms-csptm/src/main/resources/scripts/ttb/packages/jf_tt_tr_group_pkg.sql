CREATE OR REPLACE FUNCTION ttb.jf_tt_tr_group_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.tt_tr AS
$body$
declare 
   l_r ttb.tt_tr%rowtype; 
begin 
   l_r.tt_tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_tr_id', true); 
   l_r.tt_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true); 
   l_r.tr_mode_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_mode_id', true); 
   l_r.depo2territory_id := jofl.jofl_pkg$extract_varchar(p_attr, 'depo2territory_id', true); 
   l_r.tr_capacity_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tr_capacity_id', true); 
   l_r.order_num := jofl.jofl_pkg$extract_varchar(p_attr, 'order_num', true); 
   l_r.tt_tr_group_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_tr_group_id', true); 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_tr_group_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   ln_tt_tr_group_id 	ttb.tt_tr.tt_tr_group_id%type;
   ln_cnt 				smallint;
   l_r 					ttb.tt_tr%rowtype; 
   ln_tt_variant_id ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
begin 

   if not ttb.jf_tt_variant_pkg$of_get_policy(ln_tt_variant_id)
     then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if;  

   l_r := ttb.jf_tt_tr_group_pkg$attr_to_rowtype(p_attr);
   ln_tt_tr_group_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_tr_group_id', true);
   
   select count(1)
   into ln_cnt
   from ttb.tt_tr_group trg
   join ttb.tt_tr tr on tr.tt_tr_group_id = trg.tt_tr_group_id
   join ttb.tt_tr_period trp on trp.tt_tr_id = tr.tt_tr_id
   where trg.tt_tr_group_id = ln_tt_tr_group_id;
   
   if ln_cnt > 0 then
     return  jofl.jofl_util$cover_result('error', 'ТС из группы назначены на выходы в периодах. <br><center><font color="red"> Удаление группы запрещено!</font></center>'); 
   end if;
   
   delete from ttb.tt_driver_action where tt_driver_id in (
      select tt_driver_id
      from ttb.tt_driver_tr where tt_tr_id in (select tt_tr_id from  ttb.tt_tr where  tt_tr_group_id = ln_tt_tr_group_id)
      )
	;
   
   with del_tdt as (
   	delete from ttb.tt_driver_tr where tt_tr_id in (select tt_tr_id from  ttb.tt_tr where  tt_tr_group_id = ln_tt_tr_group_id)
    returning *
    )
   delete from ttb.tt_driver dr where dr.tt_driver_id in (select tt_driver_id from del_tdt);   

   delete from  ttb.tt_tr where  tt_tr_group_id = ln_tt_tr_group_id;   
   delete from  ttb.tt_tr_group where tt_tr_group_id = ln_tt_tr_group_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_tr_group_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r 						ttb.tt_tr%rowtype;
   ln_cnt_tr_in_gr 			smallint;
   ln_tt_tr_group_prior_id 	smallint;
   ln_tt_variant_id ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
begin 

   if not ttb.jf_tt_variant_pkg$of_get_policy(ln_tt_variant_id)
     then RAISE EXCEPTION '<<Редактировать можно только вариант расписания в статусе «В разработке» и «Отменено»!>>';
   end if;  

   l_r := ttb.jf_tt_tr_pkg$attr_to_rowtype(p_attr);
   
   ln_cnt_tr_in_gr := jofl.jofl_pkg$extract_number(p_attr, 'cnt_tr_in_gr', true);  
   ln_tt_tr_group_prior_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_tr_group_prior_id', true);   
   
   insert into ttb.tt_tr_group (tt_tr_group_prior_id)
   select ln_tt_tr_group_prior_id   
   returning tt_tr_group_id
   into l_r.tt_tr_group_id;

   perform ttb.jf_tt_tr_group_pkg$of_insert_tt_tr ( p_id_account,
                                                    l_r,
                                                    ln_cnt_tr_in_gr
                                                  );
												  
   perform ttb.jf_tt_driver_pkg$of_insert_dr_tr ( p_id_account,  
   												  ln_tt_variant_id,  
   												  l_r.tt_tr_group_id
   												  );												  

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_tr_group_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r 					ttb.tt_tr%rowtype;
   ln_tt_tr_group_id 	ttb.tt_tr_group.tt_tr_group_id%type;
   ln_cnt_tr_in_gr 		smallint;
   ln_cnt				smallint;
   ln_tt_variant_id ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
begin 

   if not ttb.jf_tt_variant_pkg$of_get_policy(ln_tt_variant_id)
     then RAISE EXCEPTION '<<Редактировать можно только вариант расписания в статусе «В разработке» и «Отменено»!>>';
   end if;     

   l_r 					:= ttb.jf_tt_tr_pkg$attr_to_rowtype(p_attr);
   ln_tt_tr_group_id 	:= jofl.jofl_pkg$extract_number(p_attr, 'tt_tr_group_id', true);   
   ln_cnt_tr_in_gr 		:= jofl.jofl_pkg$extract_number(p_attr, 'cnt_tr_in_gr', true);   
   
   select count(1)
   into ln_cnt
   from ttb.tt_tr_group trg
   join ttb.tt_tr tr on tr.tt_tr_group_id = trg.tt_tr_group_id
   join ttb.tt_tr_period trp on trp.tt_tr_id = tr.tt_tr_id
   where trg.tt_tr_group_id = ln_tt_tr_group_id;
   
   if ln_cnt > 0 then
     return  jofl.jofl_util$cover_result('error', 'ТС из группы назначены на выходы в периодах. <br><center><font color="red"> Изменение группы запрещено!</font></center>'); 
   end if;
   
   perform ttb.jf_tt_tr_group_pkg$of_delete (p_id_account, p_attr);
   perform ttb.jf_tt_tr_group_pkg$of_insert (p_id_account, p_attr);   

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_tr_group_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_tt_variant_id ttb.tt_variant.tt_variant_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
begin 
 open p_rows for
 select
   --tt_tr.tt_tr_id,
   tt_tr.tt_variant_id,
   tt_tr.tr_mode_id,
   tt_tr.depo2territory_id,
   tt_tr.tr_capacity_id,
   --tt_tr.order_num,
   depo.name_short as depo_name_short,
   depo.name_full as depo_name_full,
   terr.name_short as terr_name_short,
   terr.name_full as terr_name_full,
   m.mode_name,
   cap.short_name as capacity_short_name,
   tt_tr.tt_tr_group_id,
   ttg.tt_tr_group_prior_id,
   count(1) as cnt_tr_in_gr
 from ttb.tt_tr
   join ttb.mode m on m.mode_id = tt_tr.tr_mode_id
   join core.depo2territory d2t on d2t.depo2territory_id = tt_tr.depo2territory_id
   join core.entity depo on depo.entity_id =d2t.depo_id
   join core.entity terr on terr.entity_id = d2t.territory_id
   join core.tr_capacity cap on cap.tr_capacity_id = tt_tr.tr_capacity_id
   join ttb.tt_tr_group ttg on ttg.tt_tr_group_id = tt_tr.tt_tr_group_id
  where tt_tr.tt_variant_id = l_tt_variant_id
  group by
   tt_tr.tt_variant_id,
   tt_tr.tr_mode_id,
   tt_tr.depo2territory_id,
   tt_tr.tr_capacity_id,
   depo.name_short,
   depo.name_full,
   terr.name_short,
   terr.name_full,
   m.mode_name,
   cap.short_name,
   tt_tr.tt_tr_group_id,
   ttg.tt_tr_group_prior_id
  order by ttg.tt_tr_group_prior_id
  ;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-----------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_tr_group_pkg$of_insert_tt_tr (
  p_id_account numeric,
  p_tt_tr_r ttb.tt_tr,
  p_cnt_tr_in_gr smallint
)
RETURNS text AS
$body$
declare    
begin   
   
   insert into ttb.tt_tr (
                     tt_variant_id
                    ,tr_mode_id
                    ,depo2territory_id
                    ,tr_capacity_id
                    ,tt_tr_group_id
                    ,is_second_car
                    )
    select p_tt_tr_r.tt_variant_id
    	  ,p_tt_tr_r.tr_mode_id
          ,p_tt_tr_r.depo2territory_id
          ,p_tt_tr_r.tr_capacity_id
          ,p_tt_tr_r.tt_tr_group_id
          ,p_tt_tr_r.is_second_car
    from generate_series(1, p_cnt_tr_in_gr);

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;