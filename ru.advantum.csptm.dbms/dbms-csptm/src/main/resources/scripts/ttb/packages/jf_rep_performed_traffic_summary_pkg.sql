CREATE OR REPLACE FUNCTION ttb.jf_rep_performed_traffic_summary_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare

begin

 open p_rows for 
     select
null as depo_name, /* Наименование ТП */
 null as tr_type_name, /* Вид ТС */
 null as route_num, /* Номер маршрута */
 null as break_time_fact, /* Общее время на обедах/отстоях факт. */
 null as break_time_plan, /* Общее время на обеды/отстои план. */
 null as drwork_time_fact, /* Общее время работы водителя факт. */
 null as drwork_time_plan, /* Общее время работы водителя план. */
 null as mile_prod_round, /* Пробег всех произв. рейсов факт. */
 null as mile_tech_round, /* Пробег всех технол.рейсов факт. */
 null as outstand_round_time, /* Общее суммарное время невыполн. рейсов */
 null as prod_round_cnt_fact, /* Количество произв. рейсов факт. */
 null as prod_round_cnt_plan, /* Количество произв. рейсов план. */
 null as prod_round_outstand_cnt /* Количество невыполненных произв.рейсов  */
	 ;
end;
$function$
;
