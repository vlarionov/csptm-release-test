CREATE OR REPLACE FUNCTION ttb.jf_tt_check_condition_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.tt_check_condition AS
$body$
declare 
   l_r ttb.tt_check_condition%rowtype; 
begin 
   l_r.tt_check_condition_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tt_check_condition_id', true); 
   l_r.check_num := jofl.jofl_pkg$extract_varchar(p_attr, 'check_num', true); 
   l_r.check_name := jofl.jofl_pkg$extract_varchar(p_attr, 'check_name', true); 
   l_r.is_doable := jofl.jofl_pkg$extract_boolean(p_attr, 'is_doable', true); 
   l_r.sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'sign_deleted', true); 
   l_r.sys_period := jofl.jofl_pkg$extract_varchar(p_attr, 'sys_period', true); 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_check_condition_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
begin 
 open p_rows for 
      select 
        tcc.tt_check_condition_id, 
        tcc.check_num, 
        tcc.check_name, 
        tcc.is_doable
      from ttb.tt_check_condition tcc
      where not tcc.sign_deleted
	  order by tcc.check_num; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_check_condition_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.tt_check_condition%rowtype;
begin 
   l_r := ttb.jf_tt_check_condition_pkg$attr_to_rowtype(p_attr);

   update ttb.tt_check_condition set 
          /*check_num = l_r.check_num, 
          check_name = l_r.check_name, */
          is_doable = l_r.is_doable
   where 
          tt_check_condition_id = l_r.tt_check_condition_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------