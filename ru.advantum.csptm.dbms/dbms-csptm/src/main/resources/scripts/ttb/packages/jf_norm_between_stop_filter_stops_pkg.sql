CREATE OR REPLACE FUNCTION ttb."jf_norm_between_stop_filter_stops_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare
  l_norm_id ttb.norm.norm_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true);
begin
  open p_rows for
  select DISTINCT on (nbs.stop_item_1_id, nbs.stop_item_2_id)
    s_1.name||' - '||s_2.name as stop_names,
    nbs.stop_item_1_id||'-'||nbs.stop_item_2_id as ids
  from ttb.norm_between_stop nbs
    join rts.stop_item si_1 on si_1.stop_item_id = nbs.stop_item_1_id
    join rts.stop_location sl_1 on sl_1.stop_location_id = si_1.stop_location_id
    join rts.stop s_1 on s_1.stop_id = sl_1.stop_id
    join rts.stop_item si_2 on si_2.stop_item_id = nbs.stop_item_2_id
    join rts.stop_location sl_2 on sl_2.stop_location_id = si_2.stop_location_id
    join rts.stop s_2 on s_2.stop_id = sl_2.stop_id
  WHERE nbs.norm_id = l_norm_id;
end;
$function$;