CREATE OR REPLACE FUNCTION ttb."jf_fix_arrival_type_pkg$attr_to_rowtype"(p_attr text)
 RETURNS ttb.fix_arrival_type
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.fix_arrival_type%rowtype; 
begin 
   l_r.fix_arrival_type_name := jofl.jofl_pkg$extract_varchar(p_attr, 'fix_arrival_type_name', true); 
   l_r.fix_arrival_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'fix_arrival_type_id', true); 

   return l_r;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_fix_arrival_type_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$ 
declare 
begin 
 open p_rows for
      select
        fix_arrival_type_name,
        fix_arrival_type_id
      from ttb.fix_arrival_type;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_fix_arrival_type_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.fix_arrival_type%rowtype;
begin 
   l_r := ttb.jf_fix_arrival_type_pkg$attr_to_rowtype(p_attr);

   /*update ttb.fix_arrival_type set
          fix_arrival_type_name = l_r.fix_arrival_type_name
   where 
          fix_arrival_type_id = l_r.fix_arrival_type_id;
*/
   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_fix_arrival_type_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.fix_arrival_type%rowtype;
begin 
   l_r := ttb.jf_fix_arrival_type_pkg$attr_to_rowtype(p_attr);

  -- delete from  ttb.fix_arrival_type where  fix_arrival_type_id = l_r.fix_arrival_type_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_fix_arrival_type_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.fix_arrival_type%rowtype;
begin 
   l_r := ttb.jf_fix_arrival_type_pkg$attr_to_rowtype(p_attr);
/*
   insert into ttb.fix_arrival_type select l_r.*;
*/
   return null;
end;
 $function$
;