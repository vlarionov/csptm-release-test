CREATE OR REPLACE FUNCTION ttb.jf_tt_at_stop_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.tt_at_stop AS
$body$
declare 
   l_r ttb.tt_at_stop%rowtype; 
   ln_tt_variant_id ttb.tt_variant.tt_variant_id%type;
begin 
   l_r.tt_action_type_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_action_type_id', true); 
   l_r.stop_item2round_id := jofl.jofl_pkg$extract_number(p_attr, 'stop_item2round_id', true); 
   l_r.depo2territory_id := jofl.jofl_pkg$extract_varchar(p_attr, 'depo2territory_id', true); 
   l_r.time_begin := jofl.jofl_pkg$extract_date(p_attr, 'time_begin', true); 
   l_r.tt_at_stop_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_at_stop_id', true); 
   
   select tat.tt_variant_id
   into ln_tt_variant_id 
   from ttb.tt_action_type tat
   where tat.tt_action_type_id = l_r.tt_action_type_id;
   
   if not ttb.jf_tt_variant_pkg$of_get_policy(ln_tt_variant_id)
    then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if;      

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_at_stop_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_tt_action_type_id ttb.tt_action_type.tt_action_type_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'tt_action_type_id', true);
begin 
 open p_rows for
 select
   tats.tt_action_type_id,
   tats.stop_item2round_id,
   tats.depo2territory_id,
   tats.time_begin,
   tats.tt_at_stop_id,
   s.name as stop_name,
   rnd.code as round_code,
   rnd.round_id,
   depo_ent.name_short as depo_name_short,
   terr_ent.name_short as terr_name_short,
   s2t.depo_id,
   s2t.territory_id,
   act.action_type_name,
   tact.tt_variant_id,
   --sia.tt_at_stop_id::integer::boolean is_aligment,
   case when sia.tt_at_stop_id is null then false
   		else true
   end is_aligment,
   case tact.action_type_id 
   		when ttb.action_type_pkg$change_driver() then 'P{A},D{OF_INSERT, OF_DELETE}'
        when ttb.action_type_pkg$dinner() then 'P{A},D{OF_INSERT, OF_DELETE}'
        when ttb.action_type_pkg$pause() then 'P{A},D{OF_INSERT, OF_DELETE}' 
        else  '' 
   end "ROW$POLICY"
 from ttb.tt_at_stop tats
   join ttb.tt_action_type tact on tact.tt_action_type_id =  tats.tt_action_type_id
   join rts.stop_item2round sir on sir.stop_item2round_id = tats.stop_item2round_id
   join rts.round rnd on rnd.round_id = sir.round_id
   join rts.stop_item si on si.stop_item_id =sir.stop_item_id
   join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
   join rts.stop s on s.stop_id = sl.stop_id
   join ttb.action_type act on act.action_type_id = rnd.action_type_id
   left join core.depo2territory s2t on s2t.depo2territory_id  =tats.depo2territory_id
   left join core.entity depo_ent on depo_ent.entity_id = s2t.depo_id
   left join core.entity terr_ent on terr_ent.entity_id = s2t.territory_id
   left join ttb.stop_interval_alignment sia on sia.tt_at_stop_id = tats.tt_at_stop_id
 where tats.tt_action_type_id = l_tt_action_type_id;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_at_stop_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r 				ttb.tt_at_stop%rowtype;
   lb_is_alignment	boolean := coalesce(jofl.jofl_pkg$extract_boolean(p_attr, 'is_aligment', true), 0 )::boolean; 
begin 
   l_r := ttb.jf_tt_at_stop_pkg$attr_to_rowtype(p_attr);

   update ttb.tt_at_stop set 
          tt_action_type_id = l_r.tt_action_type_id, 
          stop_item2round_id = l_r.stop_item2round_id, 
          depo2territory_id = l_r.depo2territory_id, 
          time_begin = l_r.time_begin
   where 
          tt_at_stop_id = l_r.tt_at_stop_id;
          
   if lb_is_alignment then
     insert into ttb.stop_interval_alignment 
     select l_r.tt_at_stop_id;
   else
     delete from ttb.stop_interval_alignment 
     where tt_at_stop_id = l_r.tt_at_stop_id;
   end if;
   
   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_at_stop_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tt_at_stop%rowtype;
begin 
   l_r := ttb.jf_tt_at_stop_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.tt_at_stop where  tt_at_stop_id = l_r.tt_at_stop_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_at_stop_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tt_at_stop%rowtype;
begin 
   l_r := ttb.jf_tt_at_stop_pkg$attr_to_rowtype(p_attr);

   insert into ttb.tt_at_stop(tt_action_type_id, stop_item2round_id, depo2territory_id, time_begin)
   values(l_r.tt_action_type_id, l_r.stop_item2round_id, l_r.depo2territory_id, l_r.time_begin);

   return null;
end;
 $function$
;