create or replace function ttb.jf_route_production_rounds_report_pkg$of_rows(p_id_account numeric, out p_rows refcursor, p_attr text)
    returns refcursor
language plpgsql
as $function$
declare
    l_f_depo_id    integer := jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', true);
    l_f_tr_type_id integer := jofl.jofl_pkg$extract_number(p_attr, 'F_tr_type_id', true);
    l_f_DT         date := jofl.jofl_pkg$extract_date(p_attr, 'f_DT', true);
begin
    open p_rows for
    with day_type as (
        select ct.tag_name
        from ttb.calendar cal
            join ttb.calendar_item ci on cal.calendar_id = ci.calendar_id
            join ttb.calendar_tag ct on ct.calendar_tag_id = ci.calendar_tag_id
            left join ttb.calendar_tag_group ctg on ctg.calendar_tag_group_id = ct.calendar_tag_group_id
        where cal.dt = l_f_DT and ct.calendar_tag_group_id = 7
    )
    select
        to_char(l_f_DT, 'dd.mm.yyyy') p_date,
        e.entity_id :: text           p_depo_id,
        case when t.tr_type_id = 1
            then 'автобусных маршрутов'
        when t.tr_type_id = 3
            then 'трамвайных маршрутов'
        when t.tr_type_id = 2
            then 'троллейбусных маршрутов'
        else ' ' end                  p_tr_type_name,
        t.name,
        t.tr_type_id                  p_tr_type_id,
        e.name_full                   p_depo_name,
        dt.tag_name                   p_type_day,
        case when ce.name_short isnull
            then ''
        else ce.name_short end        p_depo_ter,
        ce.entity_id                  p_depo_ter_id
    from core.tr_type t
        join day_type dt on 1 = 1
        join core.depo cd on cd.depo_id = l_f_depo_id
        join core.entity e on cd.depo_id = e.entity_id
        left join core.depo2territory d2t on cd.depo_id = d2t.depo_id
        left join core.entity ce on ce.entity_id = d2t.territory_id
    where t.tr_type_id = l_f_tr_type_id;
end;
$function$;


create or replace function ttb.jf_route_production_rounds_report_pkg$report(
    p_id_account   text,
    P_TIMEZONE     text,
    p_tr_type_name text,
    p_date         text,
    p_type_day     text,
    p_depo_id      text,
    p_depo_name    text,
    p_depo_ter_id  text,
    p_depo_ter     text,
    p_tr_type_id   text
)
    returns table(
        tr_type_name   text,
        date           text,
        type_day       text,
        depo_name      text,
        depo_ter       text,
        route_num      text,
        round_00_count text,
        round_D_count  text,
        round_Y_count  text,
        round_E_count  text,
        round_I_count  text,
        round_count    text
    )
language plpgsql
as $function$
declare
    l_date date :=to_timestamp(p_date, 'DD.MM.YYYY') :: date;
begin
    return query
    with action_arr as (
        select
            ttb.action_type_pkg$get_action_type_list(1 :: smallint, false)  as p1,
            ttb.action_type_pkg$get_action_type_list(2 :: smallint, false)  as p2,
            ttb.action_type_pkg$get_action_type_list(3 :: smallint, false)  as p3,
            ttb.action_type_pkg$get_action_type_list(4 :: smallint, false)  as p4,
            ttb.action_type_pkg$get_action_type_list(7 :: smallint, false)  as p5,
            ttb.action_type_pkg$get_action_type_list(25 :: smallint, false) as p6
    ) , type_day_variant as (
        select
            tv.tt_variant_id,
            tv.route_variant_id
        from ttb.tt_variant tv
            join ttb.tt_calendar cl on tv.tt_variant_id = cl.tt_variant_id
            join ttb.calendar_tag ct on ct.calendar_tag_id = cl.calendar_tag_id
            join ttb.calendar_item ci on ct.calendar_tag_id = ci.calendar_tag_id
            join ttb.calendar cal on ci.calendar_id = cal.calendar_id
            join ttb.tt_out tto on tv.tt_variant_id = tto.tt_variant_id
            join ttb.tt_out2tt_tr to2tt on tto.tt_out_id = to2tt.tt_out_id
            join ttb.tt_tr tr on to2tt.tt_tr_id = tr.tt_tr_id
            join core.depo2territory d2t on tr.depo2territory_id = d2t.depo2territory_id
            join core.depo cd on d2t.depo_id = cd.depo_id
        where
            cal.dt = l_date
            and cd.depo_id = p_depo_id::bigint
            and (tv.tt_status_id = ttb.jf_tt_status_pkg$cn_arch_status_id() or tv.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id())
            and ct.calendar_tag_group_id = ttb.calendar_pkg$get_weekday_type_tag_group_id()
            and tv.parent_tt_variant_id isnull
            and lower(tv.action_period) :: date <= l_date and
            coalesce(upper(tv.action_period), l_date) :: date >= l_date
        group by tv.tt_variant_id
    )
    select
        p_tr_type_name :: text                                 as tr_type_name,
        p_date :: text                                         as date,
        p_type_day :: text                                     as type_day,
        p_depo_name :: text                                    as depo_name,
        case when p_tr_type_id = core.tr_type_pkg$tm() :: text
            then p_depo_ter :: text
        else '' end                                            as depo_ter,
        case when (r.route_num is null)
            then 'Итого: '
        else r.route_num :: text end                              route_num,
        (count(distinct ta.tt_action_id)
             filter (where ta.action_type_id = any (p.p1)) +
         count(distinct ta.tt_action_id)
             filter (where tt_tr.is_second_car and ta.action_type_id = any (p.p1))) :: text as is_second_car_round_00_count,

        (count(distinct ta.tt_action_id)
             filter (where ta.action_type_id = any (p.p3)) +
         count(distinct ta.tt_action_id)
             filter (where tt_tr.is_second_car and ta.action_type_id = any (p.p3))) :: text as is_second_car_round_D_count,

        (count(distinct ta.tt_action_id)
             filter (where ta.action_type_id = any (p.p2)) +
         count(distinct ta.tt_action_id)
             filter (where tt_tr.is_second_car and ta.action_type_id = any (p.p2))) :: text as is_second_car_round_Y_count,

        (count(distinct ta.tt_action_id)
             filter (where ta.action_type_id = any (p.p4)) +
         count(distinct ta.tt_action_id)
             filter (where tt_tr.is_second_car and ta.action_type_id = any (p.p4))) :: text as is_second_car_round_E_count,

        (count(distinct ta.tt_action_id)
             filter (where ta.action_type_id = any (p.p5)) +
         count(distinct ta.tt_action_id)
             filter (where tt_tr.is_second_car and ta.action_type_id = any (p.p5))) :: text as is_second_car_round_I_count,

        (count(distinct ta.tt_action_id)
             filter (where ta.action_type_id = any (p.p6)) +
         count(distinct ta.tt_action_id)
             filter (where tt_tr.is_second_car and ta.action_type_id = any (p.p6))) :: text as is_second_car_round_count
    from type_day_variant tv
        join rts.route_variant rv on tv.route_variant_id = rv.route_variant_id
        join rts.route r on rv.route_variant_id = r.current_route_variant_id
        join ttb.tt_out o on tv.tt_variant_id = o.tt_variant_id
        join ttb.tt_action ta on o.tt_out_id = ta.tt_out_id
        join ttb.tt_out2tt_tr to2tt on o.tt_out_id = to2tt.tt_out_id
        join ttb.tt_tr tt_tr on to2tt.tt_tr_id = tt_tr.tt_tr_id
        join core.depo2territory d2t on d2t.depo2territory_id = tt_tr.depo2territory_id
        join action_arr p on 1 = 1
    where
        r.tr_type_id = p_tr_type_id :: smallint
        and (
            (p_tr_type_id :: smallint = core.tr_type_pkg$tm() and d2t.territory_id = p_depo_ter_id :: bigint) or
            (p_tr_type_id :: smallint <> core.tr_type_pkg$tm() and d2t.depo_id = p_depo_id :: bigint)
        )
    group by rollup (r.route_num)
    order by r.route_num;
end;
$function$;