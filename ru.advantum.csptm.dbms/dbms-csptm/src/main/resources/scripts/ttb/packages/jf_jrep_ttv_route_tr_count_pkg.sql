create or replace function ttb."jf_jrep_ttv_route_tr_count_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
language plpgsql
as $$
declare
  l_tt_variant_id ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'F_tt_variant_id', true);
begin
 open p_rows for
       select distinct
         tt_tr.tt_variant_id p_tt_variant,
         tt_tr.depo2territory_id p_depo2territory,
         depo.name_short as p_depo_name_short,
         terr.name_short as p_terr_name_short,
         v.route_variant_id p_route_variant,
         v.route_num p_route_num,
         v.ttv_name p_ttv_name,
         to_char(v.dfrom::date, 'dd.mm.yyyy') p_dfrom,
         v.tr_type_id p_tr_type_id,
         q.tag_name p_tag_name,
         q.calend_type p_calend_type
       from ( select v.route_variant_id, lower(v.action_period) dfrom,
              r.route_num, v.ttv_name,
              r.tr_type_id
              from ttb.tt_variant v join rts.route_variant rv on v.route_variant_id = rv.route_variant_id
                   join rts.route r on rv.route_id = r.route_id
              where v.tt_variant_id = l_tt_variant_id
            ) v, (select (q).t.tag_name, (q).t.calend_type from (select ttb.ttv_calend_type(l_tt_variant_id) t) q) q,
          ttb.tt_tr join core.depo2territory d2t on d2t.depo2territory_id = tt_tr.depo2territory_id
          join core.entity depo on depo.entity_id =d2t.depo_id
          join core.entity terr on terr.entity_id = d2t.territory_id
          join core.tr_capacity cap on cap.tr_capacity_id = tt_tr.tr_capacity_id
          join ttb.tt_tr_group ttg on ttg.tt_tr_group_id = tt_tr.tt_tr_group_id
        where tt_tr.tt_variant_id = l_tt_variant_id;
end;
$$;

create or replace function  ttb."jf_jrep_ttv_route_tr_count_pkg$time_point"(p_tr_type int, p_calend_type int) returns
table(
  time_point timestamp,
  is_capacity_detail boolean,
  tr_type_id int,
  calend_type int
)
language sql
as $$
  select *
  from
    (values  -- будни автобус
            (ttb.ttv_tstamp(0, '07:15'::time), false, 1, 1),
            (ttb.ttv_tstamp(0, '08:30'::time), true,  1, 1),
            (ttb.ttv_tstamp(0, '12:00'::time), false, 1, 1),
            (ttb.ttv_tstamp(0, '15:00'::time), false, 1, 1),
            (ttb.ttv_tstamp(0, '17:00'::time), true,  1, 1),
            (ttb.ttv_tstamp(0, '19:00'::time), false, 1, 1),
            (ttb.ttv_tstamp(0, '21:00'::time), false, 1, 1),
            (ttb.ttv_tstamp(1, '00:00'::time), false, 1, 1),
            (ttb.ttv_tstamp(1, '03:00'::time), false, 1, 1),
             -- будни троллейбус
            (ttb.ttv_tstamp(0, '07:15'::time), false, 2, 1),
            (ttb.ttv_tstamp(0, '08:30'::time), true,  2, 1),
            (ttb.ttv_tstamp(0, '12:00'::time), false, 2, 1),
            (ttb.ttv_tstamp(0, '15:00'::time), false, 2, 1),
            (ttb.ttv_tstamp(0, '17:00'::time), true,  2, 1),
            (ttb.ttv_tstamp(0, '18:00'::time), false, 2, 1),
            (ttb.ttv_tstamp(0, '21:00'::time), false, 2, 1),
            (ttb.ttv_tstamp(1, '00:00'::time), false, 2, 1),
            (ttb.ttv_tstamp(1, '03:00'::time), false, 2, 1),
            -- будни трамвай
            (ttb.ttv_tstamp(0, '07:15'::time), true,  3, 1),
            (ttb.ttv_tstamp(0, '08:30'::time), true,  3, 1),
            (ttb.ttv_tstamp(0, '12:00'::time), true,  3, 1),
            (ttb.ttv_tstamp(0, '15:00'::time), true,  3, 1),
            (ttb.ttv_tstamp(0, '17:00'::time), true,  3, 1),
            (ttb.ttv_tstamp(0, '18:00'::time), true,  3, 1),
            (ttb.ttv_tstamp(0, '21:00'::time), true,  3, 1),
            (ttb.ttv_tstamp(1, '00:00'::time), true,  3, 1),
            (ttb.ttv_tstamp(1, '03:00'::time), true,  3, 1),
              -- вых автобус 08:00, 10:00, 13:00, 17:00, 19:00, 21:00, 00:00, 03:00
            (ttb.ttv_tstamp(0, '08:00'::time), false, 1, 2),
            (ttb.ttv_tstamp(0, '10:00'::time), true,  1, 2),
            (ttb.ttv_tstamp(0, '13:00'::time), false, 1, 2),
            (ttb.ttv_tstamp(0, '17:00'::time), false, 1, 2),
            (ttb.ttv_tstamp(0, '19:00'::time), false, 1, 2),
            (ttb.ttv_tstamp(0, '21:00'::time), false, 1, 2),
            (ttb.ttv_tstamp(1, '00:00'::time), false, 1, 2),
            (ttb.ttv_tstamp(1, '03:00'::time), false, 1, 2),
             -- вых троллейбус
            (ttb.ttv_tstamp(0, '08:00'::time), false, 2, 2),
            (ttb.ttv_tstamp(0, '10:00'::time), true,  2, 2),
            (ttb.ttv_tstamp(0, '13:00'::time), false, 2, 2),
            (ttb.ttv_tstamp(0, '17:00'::time), false, 2, 2),
            (ttb.ttv_tstamp(0, '18:00'::time), false, 2, 2),
            (ttb.ttv_tstamp(0, '21:00'::time), false, 2, 2),
            (ttb.ttv_tstamp(1, '00:00'::time), false, 2, 2),
            (ttb.ttv_tstamp(1, '03:00'::time), false, 2, 2),
            -- вых трамвай
            (ttb.ttv_tstamp(0, '08:00'::time), true, 3, 2),
            (ttb.ttv_tstamp(0, '10:00'::time), true,  3, 2),
            (ttb.ttv_tstamp(0, '13:00'::time), true, 3, 2),
            (ttb.ttv_tstamp(0, '17:00'::time), true, 3, 2),
            (ttb.ttv_tstamp(0, '18:00'::time), true, 3, 2),
            (ttb.ttv_tstamp(0, '21:00'::time), true, 3, 2),
            (ttb.ttv_tstamp(1, '00:00'::time), true, 3, 2),
            (ttb.ttv_tstamp(1, '03:00'::time), true, 3, 2),
             -- единое автобус --07:15, 08:00, 08:30, 10:00, 12:00, 13:00, 15:00, 17:00, 19:00, 21:00, 00:00, 03:00;
            (ttb.ttv_tstamp(0, '07:15'::time), false, 1, 3),
            (ttb.ttv_tstamp(0, '08:00'::time), false, 1, 3),
            (ttb.ttv_tstamp(0, '08:30'::time), true,  1, 3),
            (ttb.ttv_tstamp(0, '10:00'::time), true,  1, 3),
            (ttb.ttv_tstamp(0, '12:00'::time), false, 1, 3),
            (ttb.ttv_tstamp(0, '13:00'::time), false, 1, 3),
            (ttb.ttv_tstamp(0, '15:00'::time), false, 1, 3),
            (ttb.ttv_tstamp(0, '17:00'::time), true,  1, 3),
            (ttb.ttv_tstamp(0, '19:00'::time), false, 1, 3),
            (ttb.ttv_tstamp(0, '21:00'::time), false, 1, 3),
            (ttb.ttv_tstamp(1, '00:00'::time), false, 1, 3),
            (ttb.ttv_tstamp(1, '03:00'::time), false, 1, 3),
             -- -- единое троллейбус 07:15, 08:00, 08:30, 10:00, 12:00, 13:00, 15:00, 17:00, 18:00, 21:00, 00:00, 03:00
            (ttb.ttv_tstamp(0, '07:15'::time), false, 2, 3),
            (ttb.ttv_tstamp(0, '08:00'::time), false, 2, 3),
            (ttb.ttv_tstamp(0, '08:30'::time), false, 2, 3),
            (ttb.ttv_tstamp(0, '10:00'::time), true,  2, 3),
            (ttb.ttv_tstamp(0, '12:00'::time), false, 2, 3),
            (ttb.ttv_tstamp(0, '13:00'::time), false, 2, 3),
            (ttb.ttv_tstamp(0, '15:00'::time), false, 2, 3),
            (ttb.ttv_tstamp(0, '17:00'::time), false, 2, 3),
            (ttb.ttv_tstamp(0, '18:00'::time), false, 2, 3),
            (ttb.ttv_tstamp(0, '21:00'::time), false, 2, 3),
            (ttb.ttv_tstamp(1, '00:00'::time), false, 2, 3),
            (ttb.ttv_tstamp(1, '03:00'::time), false, 2, 3),
             -- -- единое трамвай 07:15, 08:00, 08:30, 10:00, 12:00, 13:00, 15:00, 17:00, 18:00, 21:00, 00:00, 03:00
            (ttb.ttv_tstamp(0, '07:15'::time), true, 3, 3),
            (ttb.ttv_tstamp(0, '08:00'::time), true, 3, 3),
            (ttb.ttv_tstamp(0, '08:30'::time), true, 3, 3),
            (ttb.ttv_tstamp(0, '10:00'::time), true, 3, 3),
            (ttb.ttv_tstamp(0, '12:00'::time), true, 3, 3),
            (ttb.ttv_tstamp(0, '13:00'::time), true, 3, 3),
            (ttb.ttv_tstamp(0, '15:00'::time), true, 3, 3),
            (ttb.ttv_tstamp(0, '17:00'::time), true, 3, 3),
            (ttb.ttv_tstamp(0, '18:00'::time), true, 3, 3),
            (ttb.ttv_tstamp(0, '21:00'::time), true, 3, 3),
            (ttb.ttv_tstamp(1, '00:00'::time), true, 3, 3),
            (ttb.ttv_tstamp(1, '03:00'::time), true, 3, 3)

    ) as t(time_point, is_capacity_detail, tr_type_id, calend_type)
  where tr_type_id = p_tr_type
    and calend_type = p_calend_type
$$;

create or replace function  ttb."jf_jrep_ttv_route_tr_count_pkg$of_report"(p_tt_variant int, p_tr_type int, p_calend_type int, p_depo2territory int) returns
table(
  row_name text,
  time_point_str text,
  capacity_name text,
  out_cnt bigint,
  round_cnt bigint
)
language sql
as $$
  with tp as ( select * from ttb."jf_jrep_ttv_route_tr_count_pkg$time_point"(p_tr_type, p_calend_type) ),
       tt_out as
    (
      select act.action_type_code, act.action_type_name,
             a.dt_action, a.dt_action + make_interval(secs := a.action_dur ) dt_act_end,
             tsrange(a.dt_action, a.dt_action + make_interval(secs := a.action_dur) , '[)') dt_range,
             ttb.action_type_pkg$is_production_round(a.action_type_id) is_prod_round,
             ttr.is_second_car,
             case
               when p_tr_type = 3
               then case
                      when t.tr_capacity_id = core.tr_capacity_pkg$huge()
                      then t.tr_capacity_id
                      else case when ttr.is_second_car then -2 else -1 end
                    end
               else t.tr_capacity_id
             end as tr_capacity_id,
             t.tt_out_id
      from ttb.tt_out t join ttb.tt_action a on t.tt_out_id = a.tt_out_id
           join ttb.action_type act on act.action_type_id = a.action_type_id
           join ttb.tt_out2tt_tr t2t on t.tt_out_id = t2t.tt_out_id
           join ttb.tt_tr ttr on t2t.tt_tr_id = ttr.tt_tr_id

      where not a.sign_deleted
        and t.tt_variant_id = p_tt_variant
        and ttr.depo2territory_id = p_depo2territory
    ),
       c as
    (
      select tr_capacity_id, short_name, qty
      from core.tr_capacity c
      where c.tr_capacity_id not in (56, -1)
        and p_tr_type <> 3
          union all
      select tr_capacity_id, short_name, 1 qty
      from core.tr_capacity
      where tr_capacity_id =  core.tr_capacity_pkg$huge()
        and p_tr_type = 3
          union all
      select tr_capacity_id, short_name, qty
      from (values (-1, 'No report', 1000), (-2, 'СИСТ', 2)) as t(tr_capacity_id, short_name,qty)
      where  p_tr_type = 3
    ),
       q as
    ( select tp.*,
             c.short_name capacity_name, c.tr_capacity_id, c.qty
      from tp, c
    )
  select row_name,
         case when grp in (0, 3) then to_char(q2.time_point, 'hh24:mi') else 'Сутки' end time_point_str,
         case when grp in (0, 4) then q2.capacity_name else 'Всего' end capacity_name,
         q2.out_cnt*rn.k out_cnt,
         case grp when 7 then round_cnt.round_cnt*rn.k end round_cnt

  from
    (
      select grouping(q.time_point, q.capacity_name, q.qty) grp,
             grouping(q.qty) qtygrp,
             q.time_point time_point_sort,
             q.time_point,
             max(q.tr_capacity_id) tr_capacity_id,
             q.capacity_name capacity_name,
             q.qty,
             max(q.is_capacity_detail::int) is_capacity_detail,
             nullif(count(distinct tt_out.tt_out_id), 0) out_cnt
          from  q left join tt_out on (q.time_point <@ tt_out.dt_range and q.tr_capacity_id = tt_out.tr_capacity_id)
      group by cube(q.capacity_name, q.time_point, q.qty)

    ) q2, (select count(*) filter(where is_prod_round) round_cnt from tt_out) round_cnt,
          (select row_name, k, skey from (values ('    ', 1, 0), ('Чужие(+)', 0, 1), ('Перекл.(-)', 0, 2)) as t(row_name, k, skey)) rn
  where (grp in (3, 4, 7) or (grp = 0 and is_capacity_detail = 1)) and tr_capacity_id <> -1
  order by rn.skey, (grp = 4)::int, (grp = 4 and grp <> 7)::int, q2.time_point,  q2.qty desc nulls first
$$;
