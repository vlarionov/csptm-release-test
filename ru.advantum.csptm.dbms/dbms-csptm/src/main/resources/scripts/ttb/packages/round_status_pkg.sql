create or replace function ttb.round_status_pkg$error()
    returns ttb.round_status.round_status_id%type
immutable
language sql as 'select 0::smallint';

create or replace function ttb.round_status_pkg$passed()
    returns ttb.round_status.round_status_id%type
immutable
language sql as 'select 1::smallint';

create or replace function ttb.round_status_pkg$planned()
    returns ttb.round_status.round_status_id%type
immutable
language sql as 'select 2::smallint';

create or replace function ttb.round_status_pkg$in_progress()
    returns ttb.round_status.round_status_id%type
immutable
language sql as 'select 3::smallint';

create or replace function ttb.round_status_pkg$passed_partial()
    returns ttb.round_status.round_status_id%type
immutable
language sql as 'select 4::smallint';

create or replace function ttb.round_status_pkg$cancelled()
    returns ttb.round_status.round_status_id%type
immutable
language sql as 'select 5::smallint';
