create or replace function ttb.jf_depo_out_report_pkg$of_rows(p_id_account numeric, out p_rows refcursor,
                                                              p_attr       text)
  returns refcursor
language plpgsql
as $function$
declare
  v_attr text;
begin
  select p_attr :: jsonb || '{
    "P_REPORT_TYPE": "date"
  }' :: jsonb
  into v_attr;
  select ttb.operational_indicator_reports_pkg$of_rows(p_id_account, v_attr)
  into p_rows;
end;
$function$;


/*
подготовка данных для выбора версии отчета
*/

create or replace function ttb.depo_out_report_pkg$report_core(p_date text)
  returns text
as $$
declare
begin
  if (ttb.calendar_pkg$is_working_day(p_date :: date))
  then return 'wrk';
  else return 'hld';
  end if;
end;
$$
language 'plpgsql';


create or replace function ttb.depo_out_report_pkg$report_header(
  p_depo_id  text,
  p_day_type text)
  returns table(depo_name text,
                day_type  text)
as $$
declare
  l_depo_name text;

begin
  l_depo_name = '11 Восточный филиал';
  return query
  select
    en.name_full,
    (case when (p_day_type = 'wrk')
      then 'рабочий день'
     else 'выходной'
     end) l_day_type
  from core.depo dp
    join core.entity en on dp.depo_id = en.entity_id
  where dp.depo_id = p_depo_id :: bigint;
end;
$$
language 'plpgsql';


create or replace function ttb.depo_out_report_pkg$list_routes(p_id_account numeric, p_attr text)
  returns refcursor
language plpgsql
as $$
declare
  res_cursor       refcursor;
  l_report_type    text;
  l_rep_dt         text;
  l_tr_type_id     smallint;
  l_tr_type_prefix text;
begin
  l_report_type := jofl.jofl_pkg$extract_varchar(p_attr, 'P_REPORT_TYPE', true);
  l_tr_type_id := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id', true);
  l_rep_dt := jofl.jofl_pkg$extract_varchar(p_attr, 'F_REP_DT', true);

  open res_cursor for
  select
    tr_tp.name               as p_filter_text,
    l_report_type            as p_report_type,
    tr_tp.tr_type_id :: text as p_tr_type_id,
    'тестовое депо'          as depo_name,
    '13 July 1428 AD'        as period_str,
    null :: text             as p_depo_id,
    null :: text             as p_territory_id,
    l_rep_dt                 as p_rep_dt,
    case
    when tr_tp.tr_type_id = 3
      then 'tram'
    when tr_tp.tr_type_id in (1, 2)
      then 'bus'
    else ''
    end                      as p_tr_type_prefix
  from core.tr_type tr_tp
  where tr_tp.tr_type_id = any (array [1, 2, 3]);
  return res_cursor;
end;
$$;


create or replace function ttb.depo_out_report_pkg$get_row_data(p_depo_id text, p_date_DT text)
  returns table(
    out_num          smallint,
    tt_out_id        bigint,
    tt_action_id     int,
    tt_tr_id         int,
    daily_out        bigint,
    time_from        timestamp,
    time_to          timestamp,
    route_num        text,
    route_id         int,
    route_variant_id int,
    tr_capacity_id   smallint
  )
as $$
declare
  l_depo_id bigint := p_depo_id :: bigint;
  l_date    date := p_date_DT :: date;
begin

  return query
  with footing as (
      select
        d2r.depo_id,
        tto.tt_out_num,
        tto.tt_out_id,
        tt_tr.tt_tr_id,
        tta.tt_action_id,
        lower(tta.action_range) time_from,
        upper(tta.action_range) time_to,
        rt.route_num,
        rt.route_id,
        rt.current_route_variant_id,
        tto.tr_capacity_id
      from ttb.tt_variant ttv
        join ttb.tt_out tto on ttv.tt_variant_id = tto.tt_variant_id
        left join ttb.tt_action tta on tto.tt_out_id = tta.tt_out_id
        join rts.route rt on ttv.route_variant_id = rt.current_route_variant_id
        join rts.depo2route d2r on rt.route_id = d2r.route_id
        left join ttb.tt_out2tt_tr to2tt on tto.tt_out_id = to2tt.tt_out_id
        left join ttb.tt_tr on to2tt.tt_tr_id = tt_tr.tt_tr_id
      where tta.dt_action :: date = l_date
            and d2r.depo_id = l_depo_id
            and ttv.order_date = l_date
            and tta.action_type_id not in (select ttat.action_type_id
                                           from
                                             ttb.tt_action_type ttat
                                           where ttat.r_code = '00')
      order by tt_out_num
  )

    -- посчитать кол-во пар out-tr_id
    , s2 as (
      select
        footing.tt_out_num,
        count(footing.tt_out_num) as daily_out
      from footing
      group by footing.tt_out_num)
  -- вернуть даты из footing -  они нужны для подсчета ТСок в выходах на конкретный момент времени
  select
    s2.tt_out_num,
    footing.tt_out_id :: bigint,
    footing.tt_action_id,
    footing.tt_tr_id,
    s2.daily_out,
    footing.time_from,
    footing.time_to,
    footing.route_num,
    footing.route_id,
    footing.current_route_variant_id,
    footing.tr_capacity_id
  from s2
    join footing on footing.tt_out_num = s2.tt_out_num
  group by footing.tt_action_id, footing.tt_out_id, footing.tt_tr_id, s2.tt_out_num, s2.daily_out, footing.time_from,
    footing.time_to,
    footing.route_num, footing.route_id, footing.current_route_variant_id, footing.tr_capacity_id
  order by s2.tt_out_num;
end;
$$
language 'plpgsql';

create or replace function ttb.depo_out_report_pkg$get_row_data_tram(p_territory_id text, p_date_DT text)
  returns table(
    out_num          smallint,
    tt_out_id        bigint,
    tt_action_id     int,
    tt_tr_id         int,
    daily_out        bigint,
    time_from        timestamp,
    time_to          timestamp,
    route_num        text,
    route_id         int,
    route_variant_id int,
    tr_capacity_id   smallint,
    is_second_car    boolean
  )
as $$
declare
  l_territory_id bigint := p_territory_id :: bigint;
  l_date         date := p_date_DT :: date;
begin
  return query
    with footing as (
  select
    d2r.depo_id,
    tto.tt_out_num,
    tto.tt_out_id,
    tt_tr.tt_tr_id,
    tta.tt_action_id,
    lower(tta.action_range) time_from,
    upper(tta.action_range) time_to,
    rt.route_num,
    rt.route_id,
    rt.current_route_variant_id,
    tto.tr_capacity_id,
    tt_tr.is_second_car
  from ttb.tt_variant ttv
    join ttb.tt_out tto on ttv.tt_variant_id = tto.tt_variant_id
    left join ttb.tt_action tta on tto.tt_out_id = tta.tt_out_id
    join rts.route rt on ttv.route_variant_id = rt.current_route_variant_id
    join rts.depo2route d2r on rt.route_id = d2r.route_id
    left join ttb.tt_out2tt_tr to2tt on tto.tt_out_id = to2tt.tt_out_id
    left join ttb.tt_tr on to2tt.tt_tr_id = tt_tr.tt_tr_id
    left join core.depo2territory d2t on d2r.depo_id = d2t.depo_id
  where tta.dt_action :: date = l_date
        and d2t.depo_id = l_territory_id
        and ttv.order_date = l_date
        and tta.action_type_id not in (select ttat.action_type_id
                                       from
                                         ttb.tt_action_type ttat
                                       where ttat.r_code = '00')
  order by tt_out_num
  )
    -- посчитать кол-во пар out-tr_id
    , s1 as (
      select
        footing.tt_out_num,
        count(footing.tt_out_num) as daily_out
      from footing
      group by footing.tt_out_num)
  -- вернуть даты из footing -  они нужны для подсчета ТСок в выходах на конкретный момент времени
  select
    s1.tt_out_num,
    footing.tt_out_id :: bigint,
    footing.tt_action_id,
    footing.tt_tr_id,
    s1.daily_out,
    footing.time_from,
    footing.time_to,
    footing.route_num :: text,
    footing.route_id,
    footing.current_route_variant_id,
    footing.tr_capacity_id,
    footing.is_second_car
  from s1
    join footing on footing.tt_out_num = s1.tt_out_num
  group by footing.tt_action_id, footing.tt_out_id, footing.is_second_car, footing.tt_tr_id, s1.tt_out_num,
    s1.daily_out,
    footing.time_from, footing.time_to,
    footing.route_num, footing.route_id, footing.current_route_variant_id, footing.tr_capacity_id
  order by s1.tt_out_num;

end;
$$
language 'plpgsql';

/*
TODO сделать одну функцию,для depo_id и territory_id
*/

create or replace function ttb.depo_out_report_pkg$get_bus_wrk_data(p_depo_id text, p_date_DT text)
  returns table(route_num        text,
                daily_out        text,
                ending_stopplace text,
                t0715            int,
                t0830            int,
                t1200            int,
                t1500            int,
                t1700            int,
                t1900            int,
                t2100            int,
                t0000            int,
                t0300            int,
                obk              int,
                sk               int,
                mk               int,
                inv              int,
                x300             int,
                x302             int,
                x800             int,
                x802             int,
                x100m            int,
                x100e            int,
                x200             int,
                x700m            int,
                x700e            int,
                x600             int,
                x602             int,
                x603             int,
                x900             int)
as $$
declare
  p_depo_id bigint := p_depo_id :: bigint;
  l_date    date := p_date_DT :: date;
begin
  return query
  with heading as (select *
                   from ttb.depo_out_report_pkg$get_row_data(p_depo_id :: text, l_date :: text)
  ),
      c11 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num
          )                                                              daily_out,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()) as obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$big()
                 and h2.route_num = heading.route_num)                as bk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$middle()
                 and heading.route_num = h2.route_num)                as sk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$small()
                 and heading.route_num = h2.route_num)                as mk
        from heading
        group by heading.route_num
        order by heading.route_num
    ),
      c21 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('07:15', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t0715,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:30', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t0830,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('12:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num) as t1200,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('15:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t1500,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t1700,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('19:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t1900,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t2100,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t0000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t0300
        from heading
        group by heading.route_num
    ),
      c31 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '3'
                 and h2.route_num = heading.route_num
          )                                            as x300,
          0                                            as x302,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '8'
                 and h2.route_num = heading.route_num
          )                                            as x800,
          0                                            as x802,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '1'
                 and h2.time_from :: time >= '13:00'
                 and h2.route_num = heading.route_num
          )                                            as x100m,
          (select count(distinct heading.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '1'
                 and h2.time_from :: time <= '13:00'
                 and h2.route_num = heading.route_num
          )                                            as x100e,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '2'
                 and h2.route_num = heading.route_num) as x200,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '7'
                 and h2.time_from :: time >= '13:00'
                 and h2.route_num = heading.route_num
          )                                            as x700m,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '7'
                 and h2.time_from :: time <= '13:00'
                 and h2.route_num = heading.route_num
          )                                            as x700e,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '6'
                 and h2.route_num = heading.route_num) as x600,
          0                                            as x602,
          0                                            as x603,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '9'
                 and h2.route_num = heading.route_num) as x900
        from heading
        group by heading.route_num ),
      c41 as (
        select
          rnd.round_id,
          heading.route_num
        from heading
          join rts.round rnd on heading.route_variant_id = rnd.route_variant_id
        where rnd.code = '00'
              and rnd.move_direction_id = 1
              and not rnd.sign_deleted
        group by rnd.round_id, heading.route_num
    )
  select
    heading.route_num :: text,
    c11.daily_out :: text                                                                 daily_out,
    concat(rts.jf_route_reference_rpt_pkg$getEndingStation(c41.round_id, 'FIRST') :: text, ' - ',
           rts.jf_route_reference_rpt_pkg$getEndingStation(c41.round_id, 'LAST')) :: text ending_stopplace,
    c21.t0715 :: int,
    c21.t0830 :: int,
    c21.t1200 :: int,
    c21.t1500 :: int,
    c21.t1700 :: int,
    c21.t1900 :: int,
    c21.t2100 :: int,
    c21.t0000 :: int,
    c21.t0300 :: int,
    c11.obk :: int,
    c11.sk :: int,
    c11.mk :: int,
    0 :: int                                                                              inv,
    c31.x300 :: int  as                                                                   x300,
    c31.x302 :: int  as                                                                   x302,
    c31.x800 :: int  as                                                                   x800,
    c31.x802 :: int  as                                                                   x802,
    c31.x100m :: int as                                                                   x100m,
    c31.x100e :: int as                                                                   x100e,
    c31.x200 :: int  as                                                                   x200,
    c31.x700m :: int as                                                                   x700m,
    c31.x700e :: int as                                                                   x700e,
    c31.x600 :: int  as                                                                   x600,
    c31.x602 :: int  as                                                                   x602,
    c31.x603 :: int  as                                                                   x603,
    c31.x900 :: int  as                                                                   x900

  from heading
    full outer join c21 on c21.route_num = heading.route_num
    full outer join c11 on c11.route_num = heading.route_num
    full outer join c31 on heading.route_num = c31.route_num
    full outer join c41 on heading.route_num = c41.route_num
  group by heading.route_num, c41.round_id,
    c11.daily_out,
    c21.t0715,
    c21.t0830,
    c21.t1200,
    c21.t1500,
    c21.t1700,
    c21.t1900,
    c21.t2100,
    c21.t0000,
    c21.t0300,
    c11.obk,
    c11.sk,
    c11.mk,
    c31.x300,
    c31.x302,
    c31.x800,
    c31.x802,
    c31.x100m,
    c31.x100e,
    c31.x200,
    c31.x700m,
    c31.x700e,
    c31.x600,
    c31.x602,
    c31.x603,
    c31.x900
  order by heading.route_num;
end;
$$
language 'plpgsql';

create or replace function ttb.depo_out_report_pkg$get_trol_wrk_data(p_depo_id text, p_date_DT text)
  returns table(route_num        text,
                daily_out        text,
                ending_stopplace text,
                t0715            int,
                t0830            int,
                t1200            int,
                t1500            int,
                t1700            int,
                t1800            int,
                t2100            int,
                t0000            int,
                t0300            int,
                obk              int,
                sk               int,
                mk               int,
                inv              int,
                x300             int,
                x302             int,
                x800             int,
                x802             int,
                x100m            int,
                x100e            int,
                x200             int,
                x700m            int,
                x700e            int,
                x600             int,
                x602             int,
                x603             int,
                x900             int)
as $$
declare
  p_depo_id bigint := p_depo_id :: bigint;
  l_date    date := p_date_DT :: date;
begin
  return query
  with heading as (select *
                   from ttb.depo_out_report_pkg$get_row_data(p_depo_id :: text, l_date :: text)
  ),
      c11 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num
          )                                                              daily_out,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()) as obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$big()
                 and h2.route_num = heading.route_num)                as bk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$middle()
                 and heading.route_num = h2.route_num)                as sk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$small()
                 and heading.route_num = h2.route_num)                as mk
        from heading
        group by heading.route_num
        order by heading.route_num
    ),
      c21 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('07:15', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t0715,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:30', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t0830,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('12:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num) as t1200,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('15:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t1500,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t1700,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('18:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t1800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t2100,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t0000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t0300
        from heading
        group by heading.route_num
    ),
      c31 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '3'
                 and h2.route_num = heading.route_num
          )                                            as x300,
          0                                            as x302,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '8'
                 and h2.route_num = heading.route_num
          )                                            as x800,
          0                                            as x802,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '1'
                 and h2.time_from :: time >= '13:00'
                 and h2.route_num = heading.route_num
          )                                            as x100m,
          (select count(distinct heading.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '1'
                 and h2.time_from :: time <= '13:00'
                 and h2.route_num = heading.route_num
          )                                            as x100e,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '2'
                 and h2.route_num = heading.route_num) as x200,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '7'
                 and h2.time_from :: time >= '13:00'
                 and h2.route_num = heading.route_num
          )                                            as x700m,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '7'
                 and h2.time_from :: time <= '13:00'
                 and h2.route_num = heading.route_num
          )                                            as x700e,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '6'
                 and h2.route_num = heading.route_num) as x600,
          0                                            as x602,
          0                                            as x603,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '9'
                 and h2.route_num = heading.route_num) as x900
        from heading
        group by heading.route_num ),
      c41 as (
        select
          rnd.round_id,
          heading.route_num
        from heading
          join rts.round rnd on heading.route_variant_id = rnd.route_variant_id
        where rnd.code = '00'
              and rnd.move_direction_id = 1
              and not rnd.sign_deleted
        group by rnd.round_id, heading.route_num
    )
  select
    heading.route_num :: text,
    c11.daily_out :: text                                                                 daily_out,
    concat(rts.jf_route_reference_rpt_pkg$getEndingStation(c41.round_id, 'FIRST') :: text, ' - ',
           rts.jf_route_reference_rpt_pkg$getEndingStation(c41.round_id, 'LAST')) :: text ending_stopplace,
    c21.t0715 :: int,
    c21.t0830 :: int,
    c21.t1200 :: int,
    c21.t1500 :: int,
    c21.t1700 :: int,
    c21.t1800 :: int,
    c21.t2100 :: int,
    c21.t0000 :: int,
    c21.t0300 :: int,
    c11.obk :: int,
    c11.sk :: int,
    c11.mk :: int,
    0 :: int                                                                              inv,
    c31.x300 :: int  as                                                                   x300,
    c31.x302 :: int  as                                                                   x302,
    c31.x800 :: int  as                                                                   x800,
    c31.x802 :: int  as                                                                   x802,
    c31.x100m :: int as                                                                   x100m,
    c31.x100e :: int as                                                                   x100e,
    c31.x200 :: int  as                                                                   x200,
    c31.x700m :: int as                                                                   x700m,
    c31.x700e :: int as                                                                   x700e,
    c31.x600 :: int  as                                                                   x600,
    c31.x602 :: int  as                                                                   x602,
    c31.x603 :: int  as                                                                   x603,
    c31.x900 :: int  as                                                                   x900

  from heading
    full outer join c21 on c21.route_num = heading.route_num
    full outer join c11 on c11.route_num = heading.route_num
    full outer join c31 on heading.route_num = c31.route_num
    full outer join c41 on heading.route_num = c41.route_num
  group by heading.route_num, c41.round_id,
    c11.daily_out,
    c21.t0715,
    c21.t0830,
    c21.t1200,
    c21.t1500,
    c21.t1700,
    c21.t1800,
    c21.t2100,
    c21.t0000,
    c21.t0300,
    c11.obk,
    c11.sk,
    c11.mk,
    c31.x300,
    c31.x302,
    c31.x800,
    c31.x802,
    c31.x100m,
    c31.x100e,
    c31.x200,
    c31.x700m,
    c31.x700e,
    c31.x600,
    c31.x602,
    c31.x603,
    c31.x900
  order by heading.route_num;
end;
$$
language 'plpgsql';

create or replace function ttb.depo_out_report_pkg$get_bus_hld_data(p_depo_id text, p_date_DT text)
  returns table(route_num        text,
                daily_out        text,
                ending_stopplace text,
                t0800            int,
                t1000            int,
                t1300            int,
                t1700            int,
                t1900            int,
                t2100            int,
                t0000            int,
                t0300            int,
                obk              int,
                sk               int,
                mk               int,
                inv              int,
                x300             int,
                x302             int,
                x800             int,
                x802             int,
                x100m            int,
                x100e            int,
                x200             int,
                x700m            int,
                x700e            int,
                x600             int,
                x602             int,
                x603             int,
                x900             int)
as $$
declare
  p_depo_id bigint := p_depo_id :: bigint;
  l_date    date := p_date_DT :: date;
begin
  return query
  with heading as (select *
                   from ttb.depo_out_report_pkg$get_row_data(p_depo_id :: text, l_date :: text)
  ),
      c11 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num
          )                                                              daily_out,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()) as obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$big()
                 and h2.route_num = heading.route_num)                as bk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$middle()
                 and heading.route_num = h2.route_num)                as sk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$small()
                 and heading.route_num = h2.route_num)                as mk
        from heading
        group by heading.route_num
        order by heading.route_num
    ),
      c21 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t0800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('10:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num) as t1000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('13:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t1300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t1700,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('19:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t1900,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t2100,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t0000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t0300
        from heading
        group by heading.route_num
    ),
      c31 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '3'
                 and h2.route_num = heading.route_num
          )                                            as x300,
          0                                            as x302,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '8'
                 and h2.route_num = heading.route_num
          )                                            as x800,
          0                                            as x802,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '1'
                 and h2.time_from :: time >= '13:00'
                 and h2.route_num = heading.route_num
          )                                            as x100m,
          (select count(distinct heading.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '1'
                 and h2.time_from :: time <= '13:00'
                 and h2.route_num = heading.route_num
          )                                            as x100e,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '2'
                 and h2.route_num = heading.route_num) as x200,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '7'
                 and h2.time_from :: time >= '13:00'
                 and h2.route_num = heading.route_num
          )                                            as x700m,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '7'
                 and h2.time_from :: time <= '13:00'
                 and h2.route_num = heading.route_num
          )                                            as x700e,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '6'
                 and h2.route_num = heading.route_num) as x600,
          0                                            as x602,
          0                                            as x603,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '9'
                 and h2.route_num = heading.route_num) as x900
        from heading
        group by heading.route_num ),
      c41 as (
        select
          rnd.round_id,
          heading.route_num
        from heading
          join rts.round rnd on heading.route_variant_id = rnd.route_variant_id
        where rnd.code = '00'
              and rnd.move_direction_id = 1
              and not rnd.sign_deleted
        group by rnd.round_id, heading.route_num
    )
  select
    heading.route_num :: text,
    c11.daily_out :: text                                                                 daily_out,
    concat(rts.jf_route_reference_rpt_pkg$getEndingStation(c41.round_id, 'FIRST') :: text, ' - ',
           rts.jf_route_reference_rpt_pkg$getEndingStation(c41.round_id, 'LAST')) :: text ending_stopplace,
    c21.t0800 :: int,
    c21.t1000 :: int,
    c21.t1300 :: int,
    c21.t1700 :: int,
    c21.t1900 :: int,
    c21.t2100 :: int,
    c21.t0000 :: int,
    c21.t0300 :: int,
    c11.obk :: int,
    c11.sk :: int,
    c11.mk :: int,
    0 :: int                                                                              inv,
    c31.x300 :: int  as                                                                   x300,
    c31.x302 :: int  as                                                                   x302,
    c31.x800 :: int  as                                                                   x800,
    c31.x802 :: int  as                                                                   x802,
    c31.x100m :: int as                                                                   x100m,
    c31.x100e :: int as                                                                   x100e,
    c31.x200 :: int  as                                                                   x200,
    c31.x700m :: int as                                                                   x700m,
    c31.x700e :: int as                                                                   x700e,
    c31.x600 :: int  as                                                                   x600,
    c31.x602 :: int  as                                                                   x602,
    c31.x603 :: int  as                                                                   x603,
    c31.x900 :: int  as                                                                   x900

  from heading
    full outer join c21 on c21.route_num = heading.route_num
    full outer join c11 on c11.route_num = heading.route_num
    full outer join c31 on heading.route_num = c31.route_num
    full outer join c41 on heading.route_num = c41.route_num
  group by heading.route_num, c41.round_id,
    c11.daily_out,
    c21.t0800,
    c21.t1000,
    c21.t1300,
    c21.t1700,
    c21.t1900,
    c21.t2100,
    c21.t0000,
    c21.t0300,
    c11.obk,
    c11.sk,
    c11.mk,
    c31.x300,
    c31.x302,
    c31.x800,
    c31.x802,
    c31.x100m,
    c31.x100e,
    c31.x200,
    c31.x700m,
    c31.x700e,
    c31.x600,
    c31.x602,
    c31.x603,
    c31.x900
  order by heading.route_num;
end;
$$
language 'plpgsql';

create or replace function ttb.depo_out_report_pkg$get_trol_hld_data(p_depo_id text, p_date_DT text)
  returns table(route_num        text,
                daily_out        text,
                ending_stopplace text,
                t0800            int,
                t1000            int,
                t1300            int,
                t1700            int,
                t1800            int,
                t2100            int,
                t0000            int,
                t0300            int,
                obk              int,
                sk               int,
                mk               int,
                inv              int,
                x300             int,
                x302             int,
                x800             int,
                x802             int,
                x100m            int,
                x100e            int,
                x200             int,
                x700m            int,
                x700e            int,
                x600             int,
                x602             int,
                x603             int,
                x900             int)
as $$
declare
  p_depo_id bigint := p_depo_id :: bigint;
  l_date    date := p_date_DT :: date;
begin
  return query
  with heading as (select *
                   from ttb.depo_out_report_pkg$get_row_data(p_depo_id :: text, l_date :: text)
  ),
      c11 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num
          )                                                              daily_out,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()) as obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$big()
                 and h2.route_num = heading.route_num)                as bk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$middle()
                 and heading.route_num = h2.route_num)                as sk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$small()
                 and heading.route_num = h2.route_num)                as mk
        from heading
        group by heading.route_num
        order by heading.route_num
    ),
      c21 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t0800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('10:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num) as t1000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('13:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t1300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t1700,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('18:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t1800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t2100,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t0000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          )                                            as t0300
        from heading
        group by heading.route_num
    ),
      c31 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '3'
                 and h2.route_num = heading.route_num
          )                                            as x300,
          0                                            as x302,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '8'
                 and h2.route_num = heading.route_num
          )                                            as x800,
          0                                            as x802,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '1'
                 and h2.time_from :: time >= '13:00'
                 and h2.route_num = heading.route_num
          )                                            as x100m,
          (select count(distinct heading.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '1'
                 and h2.time_from :: time <= '13:00'
                 and h2.route_num = heading.route_num
          )                                            as x100e,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '2'
                 and h2.route_num = heading.route_num) as x200,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '7'
                 and h2.time_from :: time >= '13:00'
                 and h2.route_num = heading.route_num
          )                                            as x700m,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '7'
                 and h2.time_from :: time <= '13:00'
                 and h2.route_num = heading.route_num
          )                                            as x700e,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '6'
                 and h2.route_num = heading.route_num) as x600,
          0                                            as x602,
          0                                            as x603,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '9'
                 and h2.route_num = heading.route_num) as x900
        from heading
        group by heading.route_num ),
      c41 as (
        select
          rnd.round_id,
          heading.route_num
        from heading
          join rts.round rnd on heading.route_variant_id = rnd.route_variant_id
        where rnd.code = '00'
              and rnd.move_direction_id = 1
              and not rnd.sign_deleted
        group by rnd.round_id, heading.route_num
    )
  select
    heading.route_num :: text,
    c11.daily_out :: text                                                                 daily_out,
    concat(rts.jf_route_reference_rpt_pkg$getEndingStation(c41.round_id, 'FIRST') :: text, ' - ',
           rts.jf_route_reference_rpt_pkg$getEndingStation(c41.round_id, 'LAST')) :: text ending_stopplace,
    c21.t0800 :: int,
    c21.t1000 :: int,
    c21.t1300 :: int,
    c21.t1700 :: int,
    c21.t1800 :: int,
    c21.t2100 :: int,
    c21.t0000 :: int,
    c21.t0300 :: int,
    c11.obk :: int,
    c11.sk :: int,
    c11.mk :: int,
    0 :: int                                                                              inv,
    c31.x300 :: int  as                                                                   x300,
    c31.x302 :: int  as                                                                   x302,
    c31.x800 :: int  as                                                                   x800,
    c31.x802 :: int  as                                                                   x802,
    c31.x100m :: int as                                                                   x100m,
    c31.x100e :: int as                                                                   x100e,
    c31.x200 :: int  as                                                                   x200,
    c31.x700m :: int as                                                                   x700m,
    c31.x700e :: int as                                                                   x700e,
    c31.x600 :: int  as                                                                   x600,
    c31.x602 :: int  as                                                                   x602,
    c31.x603 :: int  as                                                                   x603,
    c31.x900 :: int  as                                                                   x900

  from heading
    full outer join c21 on c21.route_num = heading.route_num
    full outer join c11 on c11.route_num = heading.route_num
    full outer join c31 on heading.route_num = c31.route_num
    full outer join c41 on heading.route_num = c41.route_num
  group by heading.route_num, c41.round_id,
    c11.daily_out,
    c21.t0800,
    c21.t1000,
    c21.t1300,
    c21.t1700,
    c21.t1800,
    c21.t2100,
    c21.t0000,
    c21.t0300,
    c11.obk,
    c11.sk,
    c11.mk,
    c31.x300,
    c31.x302,
    c31.x800,
    c31.x802,
    c31.x100m,
    c31.x100e,
    c31.x200,
    c31.x700m,
    c31.x700e,
    c31.x600,
    c31.x602,
    c31.x603,
    c31.x900
  order by heading.route_num;
end;
$$
language 'plpgsql';

create or replace function ttb.depo_out_report_pkg$get_tram_wrk_data(p_territory_id text, p_date_DT text)
  returns table(route_num   text,
                daily_out   int,
                t0800       int,
                t0800obk    int,
                tp0800      int,
                tp0800t     int,
                t1000       int,
                t1000obk    int,
                tp1000      int,
                tp1000t     int,
                t1300       int,
                t1300obk    int,
                tp1300      int,
                tp1300t     int,
                t1700       int,
                t1700obk    int,
                tp1700      int,
                tp1700t     int,
                t1800       int,
                t1800obk    int,
                tp1800      int,
                tp1800t     int,
                t2100       int,
                t2100obk    int,
                tp2100      int,
                tp2100t     int,
                t0000       int,
                t0000obk    int,
                tp0000      int,
                tp0000t     int,
                t0300       int,
                t0300obk    int,
                tp0300      int,
                tp0300t     int,
                out_counter int,
                x200        int,
                x700        int,
                x300        int,
                x800        int,
                x900        int,
                driver      text,
                obrt        int)
as $$
declare
  p_territory_id bigint := p_territory_id :: bigint;
  l_date         date := p_date_DT :: date;
begin
  return query
  with heading as (select *
                   from ttb.depo_out_report_pkg$get_row_data_tram(p_territory_id :: text, l_date :: text)
  ),
      c11 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num)                       daily_out,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()) as obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$big()
                 and h2.route_num = heading.route_num)                as bk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$middle()
                 and heading.route_num = h2.route_num)                as sk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$small()
                 and heading.route_num = h2.route_num)                as mk
        from heading
        group by heading.route_num
        order by heading.route_num
    ),
      c21 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('07:15', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t0715,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('07:15', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t0715obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('07:15', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp0715,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('07:15', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp0715obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:30', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t0830,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:30', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t0830obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:30', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp0830,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:30', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp0830obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('12:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t1200,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('12:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t1200obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('12:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp1200,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('12:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp1200obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('15:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t1500,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('15:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t1500obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('15:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp1500,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('15:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp1500obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t1700,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t1700obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp1700,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp1700obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('18:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t1800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('18:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t1800obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('18:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp1800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('18:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp1800obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t2100,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t2100obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp2100,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp2100obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t0000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t0000obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp0000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp0000obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t0300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t0300obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp0300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp0300obk
        from heading
        group by heading.route_num
    ),
      c31 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '3'
                 and h2.route_num = heading.route_num
          ) as x300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '3'
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as x300obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '3'
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as xp300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '3'
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as xp300obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '2'
                 and h2.route_num = heading.route_num
          ) as x200,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '2'
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as x200obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '2'
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as xp200,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '2'
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as xp200obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '7'
                 and h2.route_num = heading.route_num
          ) as x700,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '7'
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as x700obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '7'
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as xp700,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '7'
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as xp700obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '8'
                 and h2.route_num = heading.route_num
          ) as x800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '8'
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as x800obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '8'
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as xp800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '8'
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as xp800obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '9'
                 and h2.route_num = heading.route_num
          ) as x900,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '9'
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as x900obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '9'
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as xp900,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '9'
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as xp900obk
        from heading
        group by heading.route_num  ),

      c41 as ( select distinct
                 extract(epoch from (heading.time_to - heading.time_from) :: interval) :: int / 60 / 2 as time_length,
                 heading.route_num
               from heading
                 join rts.route_variant rrv on heading.route_variant_id = rrv.route_variant_id
                 join rts.round rnd on rrv.route_variant_id = rnd.route_variant_id
               where rnd.code = '00'
                     and (l_date :: date +
                          TO_TIMESTAMP('07:00', 'HH24:MI') :: time between heading.time_from and heading.time_to
                          or l_date :: date +
                             TO_TIMESTAMP('08:00', 'HH24:MI') :: time between heading.time_from and heading.time_to)
    ),
      c42 as (
        select
          c41.route_num,
          avg(c41.time_length) time_length
        from c41
        group by c41.route_num)

  select
    heading.route_num :: text,
    c11.daily_out :: int,
    c21.t0715 :: int            as t0715,
    c21.t0715obk :: int         as t0715obk,
    c21.tp0715 :: int           as tp0715,
    c21.tp0715obk :: int        as tp0715obk,
    c21.t0830 :: int            as t0830,
    c21.t0830obk :: int         as t0830obk,
    c21.tp0830 :: int           as tp0830,
    c21.tp0830obk :: int        as tp0830obk,
    c21.t1200 :: int            as t1200,
    c21.t1200obk :: int         as t1200obk,
    c21.tp1200 :: int           as tp1200,
    c21.tp1200obk :: int        as tp1200obk,
    c21.t1500 :: int            as t1500,
    c21.t1500obk :: int         as t1500obk,
    c21.tp1500 :: int           as tp1500,
    c21.tp1500obk :: int        as tp1500obk,
    c21.t1700 :: int            as t1700,
    c21.t1700obk :: int         as t1700obk,
    c21.tp1700 :: int           as tp1700,
    c21.tp1700obk :: int        as tp1700obk,
    c21.t1800 :: int            as t1800,
    c21.t1800obk :: int         as t1800obk,
    c21.tp1800 :: int           as tp1800,
    c21.tp1800obk :: int        as tp1800obk,
    c21.t2100 :: int            as t2100,
    c21.t2100obk :: int         as t2100obk,
    c21.tp2100 :: int           as tp2100,
    c21.tp2100obk :: int        as tp2100obk,
    c21.t0000 :: int            as t0000,
    c21.t0000obk :: int         as t0000obk,
    c21.tp0000 :: int           as tp0000,
    c21.tp0000obk :: int        as tp0000obk,
    c21.t0300 :: int            as t0300,
    c21.t0300obk :: int         as t0300obk,
    c21.tp0300 :: int           as tp0300,
    c21.tp0300obk :: int        as tp0300obk,
    0 :: int                    as output_count,
    c31.x300 :: int             as x300,
    c31.x300obk :: int          as x300obk,
    c31.xp300 :: int            as xp300,
    c31.xp300obk :: int         as xp300obk,
    c31.x700 :: int             as x700,
    c31.x700obk :: int          as x700obk,
    c31.xp700 :: int            as xp700,
    c31.xp700obk :: int         as xp700obk,
    c31.x800 :: int             as x800,
    c31.x800obk :: int          as x800obk,
    c31.xp800 :: int            as xp800,
    c31.xp800obk :: int         as xp800obk,
    c31.x200 :: int             as x200,
    c31.x200obk :: int          as x200obk,
    c31.xp200 :: int            as xp200,
    c31.xp200obk :: int         as xp200obk,
    c31.x900 :: int             as x900,
    c31.x900obk :: int          as x900obk,
    c31.xp900 :: int            as xp900,
    c31.xp900obk :: int         as xp900obk,
    '' :: text                     driver,
    --два раза avg - непорядок
    avg(c42.time_length) :: int as obrt
  --оборот
  from heading
    full outer join c21 on c21.route_num = heading.route_num
    full outer join c11 on c11.route_num = heading.route_num
    full outer join c31 on heading.route_num = c31.route_num
    full outer join c42 on heading.route_num = c42.route_num
  group by heading.route_num, heading.is_second_car,
    c11.daily_out,
    c21.t0715,
    c21.t0715obk,
    c21.tp0715,
    c21.tp0715obk,
    c21.t0830,
    c21.t0830obk,
    c21.tp0830,
    c21.tp0830obk,
    c21.t1200,
    c21.t1200obk,
    c21.tp1200,
    c21.tp1200obk,
    c21.t1500,
    c21.t1500obk,
    c21.tp1500,
    c21.tp1500obk,
    c21.t1700,
    c21.t1700obk,
    c21.tp1700,
    c21.tp1700obk,
    c21.t1800,
    c21.t1800obk,
    c21.tp1800,
    c21.tp1800obk,
    c21.t2100,
    c21.t2100obk,
    c21.tp2100,
    c21.tp2100obk,
    c21.t0000,
    c21.t0000obk,
    c21.tp0000,
    c21.tp0000obk,
    c21.t0300,
    c21.t0300obk,
    c21.tp0300,
    c21.tp0300obk,
    c31.x300,
    c31.x300obk,
    c31.xp300,
    c31.xp300obk,
    c31.x700,
    c31.x700obk,
    c31.xp700,
    c31.xp700obk,
    c31.x800,
    c31.x800obk,
    c31.xp800,
    c31.xp800obk,
    c31.x200,
    c31.x200obk,
    c31.xp200,
    c31.xp200obk,
    c31.x900,
    c31.x900obk,
    c31.xp900,
    c31.xp900obk
  order by heading.route_num;
end;
$$
language 'plpgsql';

create or replace function ttb.depo_out_report_pkg$get_tram_hld_data(p_territory_id text, p_date_DT text)
  returns table(route_num   text,
                daily_out   int,
                t0800       int,
                t0800obk    int,
                tp0800      int,
                tp0800t     int,
                t1000       int,
                t1000obk    int,
                tp1000      int,
                tp1000t     int,
                t1300       int,
                t1300obk    int,
                tp1300      int,
                tp1300t     int,
                t1700       int,
                t1700obk    int,
                tp1700      int,
                tp1700t     int,
                t1800       int,
                t1800obk    int,
                tp1800      int,
                tp1800t     int,
                t2100       int,
                t2100obk    int,
                tp2100      int,
                tp2100t     int,
                t0000       int,
                t0000obk    int,
                tp0000      int,
                tp0000t     int,
                t0300       int,
                t0300obk    int,
                tp0300      int,
                tp0300t     int,
                out_counter int,
                x200        int,
                x700        int,
                x300        int,
                x800        int,
                x900        int,
                driver      text,
                obrt        int)
as $$
declare
  l_territory_id bigint := p_territory_id :: bigint;
  l_date         date := p_date_DT :: date;
begin
  return query
  with heading as (select *
                   from ttb.depo_out_report_pkg$get_row_data_tram(l_territory_id :: text, l_date :: text)
  ),
      c11 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num)                       daily_out,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()) as obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$big()
                 and h2.route_num = heading.route_num)                as bk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$middle()
                 and heading.route_num = h2.route_num)                as sk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$small()
                 and heading.route_num = h2.route_num)                as mk
        from heading
        group by heading.route_num
        order by heading.route_num
    ),
      c21 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t0800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t0800obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp0800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp0800obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('10:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t1000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('10:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t1000obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('10:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp1000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('10:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp1000obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('13:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t1300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('13:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t1300obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('13:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp1300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('13:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp1300obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t1700,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t1700obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp1700,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp1700obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('18:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t1800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('18:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t1800obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('18:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp1800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('18:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp1800obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t2100,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t2100obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp2100,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp2100obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t0000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t0000obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp0000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp0000obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t0300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t0300obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp0300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp0300obk
        from heading
        group by heading.route_num
    ),
      c31 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '3'
                 and h2.route_num = heading.route_num
          ) as x300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '2'
                 and h2.route_num = heading.route_num
          ) as x200,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '7'
                 and h2.route_num = heading.route_num
          ) as x700,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '8'
                 and h2.route_num = heading.route_num
          ) as x800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where left(h2.out_num :: text, 1) = '9'
                 and h2.route_num = heading.route_num
          ) as x900
        from heading
        group by heading.route_num  ),
      c41 as ( select distinct
                 extract(epoch from (heading.time_to - heading.time_from) :: interval) :: int / 60 / 3 as time_length,
                 heading.route_num
               from heading
                 join rts.route_variant rrv on heading.route_variant_id = rrv.route_variant_id
                 join rts.round rnd on rrv.route_variant_id = rnd.route_variant_id
               where rnd.code = '00'
                     and not heading.is_second_car
                     and (l_date :: date +
                          TO_TIMESTAMP('11:00', 'HH24:MI') :: time between heading.time_from and heading.time_to
                          or l_date :: date +
                             TO_TIMESTAMP('12:00', 'HH24:MI') :: time between heading.time_from and heading.time_to
                          or l_date :: date +
                             TO_TIMESTAMP('13:00', 'HH24:MI') :: time between heading.time_from and heading.time_to)
    ),
      c42 as (
        select
          c41.route_num,
          avg(c41.time_length) time_length
        from c41
        group by c41.route_num)

  select
    heading.route_num :: text,
    c11.daily_out :: int,
    c21.t0800 :: int            as t0800,
    c21.t0800obk :: int         as t0800obk,
    c21.tp0800 :: int           as tp0800,
    c21.tp0800obk :: int        as tp0800obk,
    c21.t1000 :: int            as t1000,
    c21.t1000obk :: int         as t1000obk,
    c21.tp1000 :: int           as tp1000,
    c21.tp1000obk :: int        as tp1000obk,
    c21.t1300 :: int            as t1300,
    c21.t1300obk :: int         as t1300obk,
    c21.tp1300 :: int           as tp1300,
    c21.tp1300obk :: int        as tp1300obk,
    c21.t1700 :: int            as t1700,
    c21.t1700obk :: int         as t1700obk,
    c21.tp1700 :: int           as tp1700,
    c21.tp1700obk :: int        as tp1700obk,
    c21.t1800 :: int            as t1800,
    c21.t1800obk :: int         as t1800obk,
    c21.tp1800 :: int           as tp1800,
    c21.tp1800obk :: int        as tp1800obk,
    c21.t2100 :: int            as t2100,
    c21.t2100obk :: int         as t2100obk,
    c21.tp2100 :: int           as tp2100,
    c21.tp2100obk :: int        as tp2100obk,
    c21.t0000 :: int            as t0000,
    c21.t0000obk :: int         as t0000obk,
    c21.tp0000 :: int           as tp0000,
    c21.tp0000obk :: int        as tp0000obk,
    c21.t0300 :: int            as t0300,
    c21.t0300obk :: int         as t0300obk,
    c21.tp0300 :: int           as tp0300,
    c21.tp0300obk :: int        as tp0300obk,
    0 :: int                    as output_count,
    c31.x300 :: int             as x300,
    c31.x700 :: int             as x700,
    c31.x800 :: int             as x800,
    c31.x200 :: int             as x200,
    c31.x900 :: int             as x900,
    '' :: text                     driver,
    --два раза avg - непорядок
    avg(c42.time_length) :: int as obrt
  --оборот
  from heading
    full outer join c21 on c21.route_num = heading.route_num
    full outer join c11 on c11.route_num = heading.route_num
    full outer join c31 on heading.route_num = c31.route_num
    full outer join c42 on heading.route_num = c42.route_num
  group by heading.route_num, c11.daily_out, c31.x300, c31.x700, c31.x800, c31.x900, c31.x200,
    c21.t0800,
    c21.t0800obk,
    c21.tp0800,
    c21.tp0800obk,
    c21.t1000,
    c21.t1000obk,
    c21.tp1000,
    c21.tp1000obk,
    c21.t1300,
    c21.t1300obk,
    c21.tp1300,
    c21.tp1300obk,
    c21.t1700,
    c21.t1700obk,
    c21.tp1700,
    c21.tp1700obk,
    c21.t1800,
    c21.t1800obk,
    c21.tp1800,
    c21.tp1800obk,
    c21.t2100,
    c21.t2100obk,
    c21.tp2100,
    c21.tp2100obk,
    c21.t0000,
    c21.t0000obk,
    c21.tp0000,
    c21.tp0000obk,
    c21.t0300,
    c21.t0300obk,
    c21.tp0300,
    c21.tp0300obk
  order by heading.route_num;
end;
$$
language 'plpgsql';


/*
запросы для "Сводка по маршрутам/парку
*/


create or replace function ttb.route_report_pkg$get_bus_wrk_data(p_depo_id text, p_date_DT text)
  returns table(route_num           text,
                average_time_length text,
                t0715               int,
                t0830               int,
                t0830obk            int,
                t0830sk             int,
                t0830mk             int,
                t0830inv            int,
                t1200               int,
                t1500               int,
                t1700               int,
                t1700obk            int,
                t1700sk             int,
                t1700mk             int,
                t1700inv            int,
                t1900               int,
                t2100               int,
                t0000               int,
                t0300               int,
                daily_out           int,
                daily_obk           int,
                daily_sk            int,
                daily_mk            int,
                daily_inv           int,
                count_rounds        int
  )
as $$
declare
  p_depo_id bigint := p_depo_id :: bigint;
  l_date    date := p_date_DT :: date;
begin
  return query
  with heading as (select *
                   from ttb.depo_out_report_pkg$get_row_data(p_depo_id :: text, l_date :: text) h
  ),
      s11 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num) as count_rounds
        from heading
        group by heading.route_num),
      s21 as (
        select
          heading.route_num,
          trunc(avg(extract(epoch from (heading.time_to - heading.time_from) :: interval) / 60 / 2) :: int,
                0) as average_time_length
        from heading
        where heading.route_num = heading.route_num
        group by heading.route_num
    ),
      s31 as (
        select
          heading.route_num :: text,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('07:15', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t0715,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:30', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t0830,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:30', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge())   as t0830obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:30', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$middle()) as t0830sk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:30', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$small())  as t0830mk,
          0 :: text                                                     as t0830inv,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('12:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t1200,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('15:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t1500,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t1700,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.route_num = heading.route_num)                  as t1700obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$middle()) as t1700sk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$small())  as t1700mk,
          0 :: text                                                     as t1700inv,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('19:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t1900,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t2100,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t0000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t0300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num)                         daily_out,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.route_num = heading.route_num)                     daily_obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$middle()
                 and h2.route_num = heading.route_num)                     daily_sk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$small()
                 and h2.route_num = heading.route_num)                     daily_mk,
          0                                                             as daily_inv
        from heading
        group by heading.route_num
    )
  select
    s11.route_num :: text,
    s21.average_time_length :: text,
    s31.t0715 :: int,
    s31.t0830 :: int,
    s31.t0830obk :: int,
    s31.t0830sk :: int,
    s31.t0830mk :: int,
    s31.t0830inv :: int,
    s31.t1200 :: int,
    s31.t1500 :: int,
    s31.t1700 :: int,
    s31.t1700obk :: int,
    s31.t1700sk :: int,
    s31.t1700mk :: int,
    s31.t1700inv :: int,
    s31.t1900 :: int,
    s31.t2100 :: int,
    s31.t0000 :: int,
    s31.t0300 :: int,
    s31.daily_out :: int,
    s31.daily_obk :: int,
    s31.daily_sk :: int,
    s31.daily_mk :: int,
    s31.daily_inv :: int,
    s11.count_rounds :: int
  from s11
    join s21 on s11.route_num = s21.route_num
    join s31 on s11.route_num :: text = s31.route_num;
end;
$$
language 'plpgsql';

create or replace function ttb.route_report_pkg$get_trol_wrk_data(p_depo_id text, p_date_DT text)
  returns table(route_num           text,
                average_time_length text,
                t0715               int,
                t0830               int,
                t0830obk            int,
                t0830sk             int,
                t0830mk             int,
                t0830inv            int,
                t1200               int,
                t1500               int,
                t1700               int,
                t1700obk            int,
                t1700sk             int,
                t1700mk             int,
                t1700inv            int,
                t1800               int,
                t2100               int,
                t0000               int,
                t0300               int,
                daily_out           int,
                daily_obk           int,
                daily_sk            int,
                daily_mk            int,
                daily_inv           int,
                count_rounds        int
  )
as $$
declare
  p_depo_id bigint := p_depo_id :: bigint;
  l_date    date := p_date_DT :: date;
begin
  return query
  with heading as (select *
                   from ttb.depo_out_report_pkg$get_row_data(p_depo_id :: text, l_date :: text)
  ),
      s11 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num) as count_rounds
        from heading
        group by heading.route_num),
      s21 as (
        select
          heading.route_num,
          trunc(avg(extract(epoch from (heading.time_to - heading.time_from) :: interval) / 60 / 2) :: int,
                0) as average_time_length
        from heading
        where heading.route_num = heading.route_num
        group by heading.route_num
    ),
      s31 as (
        select
          heading.route_num :: text,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('07:15', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t0715,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:30', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t0830,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:30', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge())   as t0830obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:30', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$middle()) as t0830sk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:30', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$small())  as t0830mk,
          0 :: text                                                     as t0830inv,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('12:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t1200,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('15:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t1500,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t1700,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.route_num = heading.route_num)                  as t1700obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$middle()) as t1700sk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$small())  as t1700mk,
          0 :: text                                                     as t1700inv,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('18:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t1800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t2100,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t0000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t0300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num)                         daily_out,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.route_num = heading.route_num)                     daily_obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$middle()
                 and h2.route_num = heading.route_num)                     daily_sk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$small()
                 and h2.route_num = heading.route_num)                     daily_mk,
          0                                                             as daily_inv
        from heading
        group by heading.route_num
    )
  select
    s11.route_num :: text,
    s21.average_time_length :: text,
    s31.t0715 :: int,
    s31.t0830 :: int,
    s31.t0830obk :: int,
    s31.t0830sk :: int,
    s31.t0830mk :: int,
    s31.t0830inv :: int,
    s31.t1200 :: int,
    s31.t1500 :: int,
    s31.t1700 :: int,
    s31.t1700obk :: int,
    s31.t1700sk :: int,
    s31.t1700mk :: int,
    s31.t1700inv :: int,
    s31.t1800 :: int,
    s31.t2100 :: int,
    s31.t0000 :: int,
    s31.t0300 :: int,
    s31.daily_out :: int,
    s31.daily_obk :: int,
    s31.daily_sk :: int,
    s31.daily_mk :: int,
    s31.daily_inv :: int,
    s11.count_rounds :: int
  from s11
    join s21 on s11.route_num = s21.route_num
    join s31 on s11.route_num :: text = s31.route_num;
end;
$$
language 'plpgsql';

create or replace function ttb.route_report_pkg$get_bus_hld_data(p_depo_id text, p_date_DT text)
  returns table(route_num           text,
                average_time_length text,
                t0800               int,
                t0800obk            int,
                t0800sk             int,
                t0800mk             int,
                t0800inv            int,
                t1100               int,
                t1300               int,
                t1700               int,
                t1700obk            int,
                t1700sk             int,
                t1700mk             int,
                t1700inv            int,
                t1900               int,
                t2100               int,
                t0000               int,
                t0300               int,
                daily_out           int,
                daily_obk           int,
                daily_sk            int,
                daily_mk            int,
                daily_inv           int,
                count_rounds        int
  )
as $$
declare
  p_depo_id bigint := p_depo_id :: bigint;
  l_date    date := p_date_DT :: date;
begin
  return query
  with heading as (select *
                   from ttb.depo_out_report_pkg$get_row_data(p_depo_id :: text, l_date :: text)
  ),
      s11 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num) as count_rounds
        from heading
        group by heading.route_num),
      s21 as (
        select
          heading.route_num,
          trunc(avg(extract(epoch from (heading.time_to - heading.time_from) :: interval) / 60 / 2) :: int,
                0) as average_time_length
        from heading
        where heading.route_num = heading.route_num
        group by heading.route_num
    ),
      s31 as (
        select
          heading.route_num :: text,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t0800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge())   as t0800obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$middle()) as t0800sk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$small())  as t0800mk,
          0 :: text                                                     as t0800inv,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('10:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t1000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('10:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge())   as t1000obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('10:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$middle()) as t1000sk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('10:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$small())  as t1000mk,
          0 :: text                                                     as t1000inv,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('13:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t1300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t1700,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.route_num = heading.route_num)                  as t1700obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$middle()) as t1700sk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$small())  as t1700mk,
          0 :: text                                                     as t1700inv,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('19:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t1900,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t2100,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t0000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t0300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num)                         daily_out,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.route_num = heading.route_num)                     daily_obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$middle()
                 and h2.route_num = heading.route_num)                     daily_sk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$small()
                 and h2.route_num = heading.route_num)                     daily_mk,
          0                                                             as daily_inv
        from heading
        group by heading.route_num
    )
  select
    s11.route_num :: text,
    s21.average_time_length :: text,
    s31.t0800 :: int,
    s31.t0800obk :: int,
    s31.t0800sk :: int,
    s31.t0800mk :: int,
    s31.t0800inv :: int,
    s31.t1000 :: int,
    s31.t1300 :: int,
    s31.t1700 :: int,
    s31.t1700obk :: int,
    s31.t1700sk :: int,
    s31.t1700mk :: int,
    s31.t1700inv :: int,
    s31.t1900 :: int,
    s31.t2100 :: int,
    s31.t0000 :: int,
    s31.t0300 :: int,
    s31.daily_out :: int,
    s31.daily_obk :: int,
    s31.daily_sk :: int,
    s31.daily_mk :: int,
    s31.daily_inv :: int,
    s11.count_rounds :: int
  from s11
    join s21 on s11.route_num = s21.route_num
    join s31 on s11.route_num :: text = s31.route_num;
end;
$$
language 'plpgsql';

create or replace function ttb.route_report_pkg$get_trol_hld_data(p_depo_id text, p_date_DT text)
  returns table(route_num           text,
                average_time_length text,
                t0800               int,
                t0800obk            int,
                t0800sk             int,
                t0800mk             int,
                t0800inv            int,
                t1100               int,
                t1300               int,
                t1700               int,
                t1700obk            int,
                t1700sk             int,
                t1700mk             int,
                t1700inv            int,
                t1800               int,
                t2100               int,
                t0000               int,
                t0300               int,
                daily_out           int,
                daily_obk           int,
                daily_sk            int,
                daily_mk            int,
                daily_inv           int,
                count_rounds        int
  )
as $$
declare
  p_depo_id bigint := p_depo_id :: bigint;
  l_date    date := p_date_DT :: date;
begin
  return query
  with heading as (select *
                   from ttb.depo_out_report_pkg$get_row_data(p_depo_id :: text, l_date :: text)
  ),
      s11 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num) as count_rounds
        from heading
        group by heading.route_num),
      s21 as (
        select
          heading.route_num,
          trunc(avg(extract(epoch from (heading.time_to - heading.time_from) :: interval) / 2) :: int,
                0) as average_time_length
        from heading
        where heading.route_num = heading.route_num
        group by heading.route_num
    ),
      s31 as (
        select
          heading.route_num :: text,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t0800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge())   as t0800obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$middle()) as t0800sk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$small())  as t0800mk,
          0 :: text                                                     as t0800inv,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('10:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t1000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('10:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge())   as t1000obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('10:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$middle()) as t1000sk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('10:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$small())  as t1000mk,
          0 :: text                                                     as t1000inv,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('13:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t1300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t1700,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.route_num = heading.route_num)                  as t1700obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$middle()) as t1700sk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$small())  as t1700mk,
          0 :: text                                                     as t1700inv,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('18:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t1800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t2100,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t0000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num)                  as t0300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num)                         daily_out,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.route_num = heading.route_num)                     daily_obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$middle()
                 and h2.route_num = heading.route_num)                     daily_sk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.tr_capacity_id = core.tr_capacity_pkg$small()
                 and h2.route_num = heading.route_num)                     daily_mk,
          0                                                             as daily_inv
        from heading
        group by heading.route_num
    )
  select
    s11.route_num :: text,
    s21.average_time_length :: text,
    s31.t0800 :: int,
    s31.t0800obk :: int,
    s31.t0800sk :: int,
    s31.t0800mk :: int,
    s31.t0800inv :: int,
    s31.t1000 :: int,
    s31.t1300 :: int,
    s31.t1700 :: int,
    s31.t1700obk :: int,
    s31.t1700sk :: int,
    s31.t1700mk :: int,
    s31.t1700inv :: int,
    s31.t1800 :: int,
    s31.t2100 :: int,
    s31.t0000 :: int,
    s31.t0300 :: int,
    s31.daily_out :: int,
    s31.daily_obk :: int,
    s31.daily_sk :: int,
    s31.daily_mk :: int,
    s31.daily_inv :: int,
    s11.count_rounds :: int
  from s11
    join s21 on s11.route_num = s21.route_num
    join s31 on s11.route_num :: text = s31.route_num;
end;
$$
language 'plpgsql';

create or replace function ttb.route_report_pkg$get_tram_wrk_data(p_territory_id text, p_date_DT text)
  returns table(out_num             text,
                average_time_length text,
                t0715               integer,
                t0715obk            integer,
                t0715tp             integer,
                t0830               integer,
                t0830obk            integer,
                t0830tp             integer,
                t1200               integer,
                t1200obk            integer,
                t1200tp             integer,
                t1500               integer,
                t1500obk            integer,
                t1500tp             integer,
                t1700               integer,
                t1700obk            integer,
                t1700tp             integer,
                t1800               integer,
                t1800obk            integer,
                t1800tp             integer,
                t2100               integer,
                t2100obk            integer,
                t2100tp             integer,
                t0000               integer,
                t0000obk            integer,
                t0000tp             integer,
                t0300               integer,
                t0300obk            integer,
                t0300tp             integer,
                daily_out           integer,
                daily_obk           integer,
                daily_tp            integer,
                count_rounds        integer
  )
as $$
declare
  l_territory_id bigint := p_territory_id :: bigint;
  l_date         date := p_date_DT :: date;
begin
  return query
  with heading as (select *
                   from ttb.depo_out_report_pkg$get_row_data_tram(l_territory_id :: text, l_date :: text)
  ),
      s11 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num) count_rounds
        from heading
        group by heading.route_num
        order by heading.route_num),
      s21 as (
        select
          heading.route_num,
          trunc(avg(extract(epoch from (heading.time_to - heading.time_from) :: interval) / 60 / 2) :: int,
                0) as average_time_length
        from heading
        where not heading.is_second_car
        group by heading.route_num
    ),
      s31 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('07:15', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t0715,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('07:15', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t0715obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('07:15', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp0715,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('07:15', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp0715obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:30', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t0830,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:30', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t0830obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:30', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp0830,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:30', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp0830obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('12:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t1200,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('12:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t1200obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('12:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp1200,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('12:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp1200obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('15:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t1500,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('15:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t1500obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('15:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp1500,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('15:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp1500obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t1700,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t1700obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp1700,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp1700obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('18:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t1800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('18:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t1800obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('18:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp1800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('18:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp1800obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t2100,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t2100obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp2100,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp2100obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t0000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t0000obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp0000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp0000obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t0300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t0300obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp0300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp0300obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp0300_obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num
          ) as daily_out,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as daily_obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as daily_tp
        from heading
        group by heading.route_num
    )

  select
    s11.route_num :: text,
    s21.average_time_length :: text,
    s31.t0715 :: integer,
    s31.t0715obk :: int,
    s31.tp0715 :: int,
    s31.t0830 :: int,
    s31.t0830obk :: int,
    s31.tp0830 :: int,
    s31.t1200 :: int,
    s31.t1200obk :: int,
    s31.tp1200 :: int,
    s31.t1500 :: int,
    s31.t1500obk :: int,
    s31.tp1500 :: int,
    s31.t1700 :: int,
    s31.t1700obk :: int,
    s31.tp1700 :: int,
    s31.t1800 :: int,
    s31.t1800obk :: int,
    s31.tp1800 :: int,
    s31.t2100 :: int,
    s31.t2100obk :: int,
    s31.tp2100 :: int,
    s31.t0000 :: int,
    s31.t0000obk :: int,
    s31.tp0000 :: int,
    s31.t0300 :: int,
    s31.t0300obk :: int,
    s31.tp0300obk :: int,
    s31.daily_out :: int,
    s31.daily_obk :: int,
    s31.daily_tp :: int,
    s11.count_rounds :: int
  from s11
    join s21 on s11.route_num = s21.route_num
    join s31 on s11.route_num :: text = s31.route_num;
end;
$$
language 'plpgsql';

create or replace function ttb.route_report_pkg$get_tram_hld_data(p_territory_id text, p_date_DT text)
  returns table(out_num             text,
                average_time_length text,
                t0800               int,
                t0800obk            int,
                t0800tp             int,
                t1000               int,
                t1000obk            int,
                t1000tp             int,
                t1300               int,
                t1300obk            int,
                t1300tp             int,
                t1700               int,
                t1700obk            int,
                t1700tp             int,
                t1800               int,
                t1800obk            int,
                t1800tp             int,
                t2100               int,
                t2100obk            int,
                t2100tp             int,
                t0000               int,
                t0000obk            int,
                t0000tp             int,
                t0300               int,
                t0300obk            int,
                t0300tp             int,
                daily_out           int,
                daily_obk           int,
                daily_tp            int,
                count_rounds        int
  )
as $$
declare
  l_territory_id bigint := p_territory_id :: bigint;
  l_date    date := p_date_DT :: date;
begin
  return query
  with heading as (select *
                   from ttb.depo_out_report_pkg$get_row_data_tram(l_territory_id :: text, l_date :: text)
  ),
      s11 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num) count_rounds
        from heading
        group by heading.route_num
        order by heading.route_num),
      s21 as (
        select
          heading.route_num,
          trunc(avg(extract(epoch from (heading.time_to - heading.time_from) :: interval) / 60 / 2) :: int,
                0) as average_time_length
        from heading
        where not heading.is_second_car
        group by heading.route_num
    ),
      s31 as (
        select
          heading.route_num,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:30', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t0800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t0800obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp0800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('08:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp0800obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('10:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t1000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('10:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t1000obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('10:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp1000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('10:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp1000obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('13:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t1300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('13:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t1300obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('13:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp1300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('13:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp1300obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t1700,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t1700obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp1700,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('17:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp1700obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('18:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t1800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('18:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t1800obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('18:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp1800,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('18:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp1800obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t2100,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t2100obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp2100,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('21:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp2100obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t0000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t0000obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp0000,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('00:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp0000obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
          ) as t0300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as t0300obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as tp0300,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where (l_date :: date + TO_TIMESTAMP('03:00', 'HH24:MI') :: time)
                 between lower(tto.action_range) and upper(tto.action_range)
                 and h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp0300obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
                 and h2.is_second_car
          ) as tp0300_obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num
          ) as daily_out,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num
                 and h2.tr_capacity_id = core.tr_capacity_pkg$huge()
          ) as daily_obk,
          (select count(distinct h2.out_num)
           from heading h2
             join ttb.tt_out tto on h2.tt_out_id = tto.tt_out_id
           where h2.route_num = heading.route_num
                 and h2.is_second_car
          ) as daily_tp
        from heading
        group by heading.route_num
    )
  select
    s11.route_num :: text,
    s21.average_time_length :: text,
    s31.t0800 :: int,
    s31.t0800obk :: int,
    s31.tp0800 :: int,
    s31.t1000 :: int,
    s31.t1000obk :: int,
    s31.tp1000 :: int,
    s31.t1300 :: int,
    s31.t1300obk :: int,
    s31.tp1300 :: int,
    s31.t1700 :: int,
    s31.t1700obk :: int,
    s31.tp1700 :: int,
    s31.t1800 :: int,
    s31.t1800obk :: int,
    s31.tp1800 :: int,
    s31.t2100 :: int,
    s31.t2100obk :: int,
    s31.tp2100 :: int,
    s31.t0000 :: int,
    s31.t0000obk :: int,
    s31.tp0000 :: int,
    s31.t0300 :: int,
    s31.t0300obk :: int,
    s31.tp0300 :: int,
    s31.daily_out :: int,
    s31.daily_obk :: int,
    s31.daily_tp :: int,
    s11.count_rounds :: int
  from s11
    join s21 on s11.route_num = s21.route_num
    join s31 on s11.route_num :: text = s31.route_num
  where 1 = 1;
end;
$$
language 'plpgsql';
