CREATE OR REPLACE FUNCTION ttb.jf_tt_out_work_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
begin 
 open p_rows for 
      select 
		 '' as shift_num
        ,0 as time_work_first_shift
        ,0 as time_work_second_shift
        ,0 as time_work_third_shift
        ,0 as time_work_all_shift
      ; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;