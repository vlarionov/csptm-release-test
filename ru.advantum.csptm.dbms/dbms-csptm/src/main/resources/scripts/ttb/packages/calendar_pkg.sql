﻿CREATE OR REPLACE FUNCTION ttb.calendar_pkg$is_working_day(inc_dt DATE)
  RETURNS BOOLEAN
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN (SELECT (EXISTS
  (SELECT 1
   FROM ttb.calendar AS l_cal
     JOIN ttb.calendar_item c ON l_cal.calendar_id = c.calendar_id
     JOIN ttb.calendar_tag ct ON c.calendar_tag_id = ct.calendar_tag_id
     JOIN ttb.calendar_tag_group ctg ON ct.calendar_tag_group_id = ctg.calendar_tag_group_id
   WHERE l_cal.dt = inc_dt
         --      проверка на наличие тэга "Будни"
         AND ct.calendar_tag_id = ttb.calendar_pkg$get_weekday_tag_id()
         --      проверка на отстутвие тэга в группе "праздник"
         AND ctg.calendar_tag_group_id != ttb.calendar_pkg$get_holiday_tag_group_id()
  )) :: INT);
END;
$$;

CREATE OR REPLACE FUNCTION ttb.calendar_pkg$get_weekday_tag_id()
  RETURNS INTEGER
LANGUAGE plpgsql immutable
AS $$
BEGIN
  RETURN 8;
END;
  $$;

CREATE OR REPLACE FUNCTION ttb.calendar_pkg$get_holiday_tag_id()
  RETURNS INTEGER
LANGUAGE plpgsql immutable
AS $$
BEGIN
  RETURN 9;
END;
  $$;

CREATE OR REPLACE FUNCTION ttb.calendar_pkg$get_holiday_tag_group_id()
  RETURNS INTEGER
LANGUAGE plpgsql immutable
AS $$
BEGIN
  RETURN 2;
END;
$$;

CREATE OR REPLACE FUNCTION ttb.calendar_pkg$get_daysofweek_tag_group_id()
  RETURNS INTEGER
LANGUAGE plpgsql immutable
AS $$
BEGIN
  RETURN 1;
END;
$$;

CREATE OR REPLACE FUNCTION ttb.calendar_pkg$get_season_tag_group_id()
  RETURNS INTEGER
LANGUAGE plpgsql immutable
AS $$
BEGIN
  RETURN 4;
END;
$$;

CREATE OR REPLACE FUNCTION ttb.calendar_pkg$get_weekday_type_tag_group_id()
  RETURNS INTEGER
LANGUAGE plpgsql immutable
AS $$
BEGIN
  RETURN 7;
END;
$$;
