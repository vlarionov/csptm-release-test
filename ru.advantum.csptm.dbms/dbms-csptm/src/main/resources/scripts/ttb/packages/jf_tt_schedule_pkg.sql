CREATE OR REPLACE FUNCTION ttb.jf_tt_schedule_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.tt_schedule AS
$body$
declare 
   l_r ttb.tt_schedule%rowtype; 
begin 
   l_r.tt_schedule_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_schedule_id', true); 
   l_r.tt_schedule_name := jofl.jofl_pkg$extract_varchar(p_attr, 'tt_schedule_name', true); 
   l_r.tt_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true); 
   l_r.parent_schedule_id := jofl.jofl_pkg$extract_number(p_attr, 'parent_schedule_id', true); 
   
   if not ttb.jf_tt_variant_pkg$of_get_policy(l_r.tt_variant_id)
     then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if;     

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_schedule_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.tt_schedule%rowtype;
begin 
   l_r := ttb.jf_tt_schedule_pkg$attr_to_rowtype(p_attr);
   
   l_r.tt_schedule_id := nextval( 'ttb.tt_schedule_tt_schedule_id_seq' );

   insert into ttb.tt_schedule select l_r.*;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_schedule_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.tt_schedule%rowtype;
begin 
   l_r := ttb.jf_tt_schedule_pkg$attr_to_rowtype(p_attr);
   
   delete from ttb.tt_schedule
   where tt_schedule_id = l_r.tt_schedule_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_schedule_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 l_tt_variant_id 	ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
 lt_rf_db_method 	text := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true);  
 ln_tt_schedule_id 	ttb.tt_schedule.tt_schedule_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_schedule_id', true);
 l_tt_variant_id_to ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id_to', true);
begin 
 open p_rows for 
      select 
        tsch.tt_schedule_id, 
        tsch.tt_schedule_name, 
        tsch.tt_variant_id,
        tsch.parent_schedule_id,
        tschp.tt_schedule_name as parent_tt_schedule_name,
        case 
          when tsch.parent_schedule_id is null
            then 'P{A},D{OF_CREATE_OUT}'
            else 'P{A},D{}'
        end "ROW$POLICY"
      from ttb.tt_schedule tsch
      left join ttb.tt_schedule tschp on tsch.parent_schedule_id = tschp.tt_schedule_id
     where (tsch.tt_variant_id = l_tt_variant_id
		   and ((  lt_rf_db_method = 'ttb.tt_schedule' and (tsch.tt_schedule_id != ln_tt_schedule_id or ln_tt_schedule_id is null))
				or lt_rf_db_method is null				
				)
			)
		or (lt_rf_db_method = 'ttb.tt_switch' and tsch.tt_variant_id = l_tt_variant_id_to); 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_schedule_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.tt_schedule%rowtype;
begin 
   l_r := ttb.jf_tt_schedule_pkg$attr_to_rowtype(p_attr);
   
   update ttb.tt_schedule
      set tt_schedule_name = l_r.tt_schedule_name
         ,parent_schedule_id = l_r.parent_schedule_id
    where tt_schedule_id = l_r.tt_schedule_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_schedule_pkg$of_create_out (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
  l_r_return 		ttb.jf_tt_tr_period_type;
  l_r 				ttb.tt_tr_period%rowtype;
  l_r_tr 			ttb.tt_tr%rowtype;
  ln_out_cnt		smallint := jofl.jofl_pkg$extract_number(p_attr, 'out_cnt', true);
  ln_ord_num		smallint;
  ln_gr_tr_cnt  	smallint;
  ln_per_tr_cnt 	smallint;
  l_tt_variant_id 	ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
  ln_tt_schedule_id integer := jofl.jofl_pkg$extract_number(p_attr, 'tt_schedule_id', true); 
  l_r_sch 			ttb.tt_schedule%rowtype;

begin
  l_r_sch		:= ttb.jf_tt_schedule_pkg$attr_to_rowtype(p_attr);
  --l_r_return 	:= ttb.jf_tt_tr_period_pkg$attr_to_rowtype(p_attr);
  l_r 			:= l_r_return.r_tr_per;
  l_r_tr 		:= l_r_return.r_tr;
  l_r_tr.tt_tr_id := l_r.tt_tr_id;
  ln_ord_num 	:= l_r_tr.order_num;	
  
  /*Кол-во в группах ТС должно быть больше или равно кол-ву ТС в периодах*/
  select count(1)
  into ln_gr_tr_cnt
  from ttb.tt_tr
   join ttb.tr_mode m on m.tr_mode_id = tt_tr.tr_mode_id
   join core.depo2territory d2t on d2t.depo2territory_id = tt_tr.depo2territory_id
   join core.entity depo on depo.entity_id =d2t.depo_id
   join core.entity terr on terr.entity_id = d2t.territory_id
   join ttb.tr_capacity_prior tcp on tcp.tr_capacity_id = tt_tr.tr_capacity_id
   join ttb.tt_tr_group ttg on ttg.tt_tr_group_id = tt_tr.tt_tr_group_id
  where tt_tr.tt_variant_id = l_tt_variant_id;
  

  select
    sum(p.out_cnt)
  into ln_per_tr_cnt
  from ttb.tt_period p
    join ttb.tt_schedule tts on tts.tt_schedule_id = p.tt_schedule_id and tts.parent_schedule_id is null
  where tts.tt_variant_id = l_tt_variant_id


  ;
  
  if ln_gr_tr_cnt < ln_per_tr_cnt then
  	return jofl.jofl_util$cover_result('error', 'Кол-во ТС в группах должно быть больше или равно кол-ву выходов в периодах.<br>Увеличьте кол-во ТС в группах или уменьшите кол-во выходов в периодах.');
  end if;
  
    /*Сохраним переключенные на нас и от нас ТС*/
    create temporary table if not exists  tt_switch_temp
  	on commit preserve rows
	tablespace ttb_data  
    as
       select  sw.tt_switch_id
              ,sw.tt_schedule_item_id_from
              ,sw.tt_schedule_id_to
              ,sw.tt_tr_period_id_from
              ,sw.tt_tr_period_id_to
              ,sw.switch_time
              ,ttp.tt_tr_id
              ,ttp.tt_period_id
              ,case 
              	when (sw.tt_tr_period_id_from = ttp.tt_tr_period_id) 
                  then false 
                else true 
               end is_change_from
              ,case 
              	when (sw.tt_tr_period_id_to = ttp.tt_tr_period_id) 
                  then false 
                else true 
               end is_change_to
              ,ttp.is_own
	from ttb.tt_switch sw
    join ttb.tt_tr_period ttp on sw.tt_tr_period_id_to = ttp.tt_tr_period_id
    						  or sw.tt_tr_period_id_from = ttp.tt_tr_period_id
    where ttp.tt_period_id in
       (select tp.tt_period_id
          from ttb.tt_schedule ts
          join ttb.tt_period tp on tp.tt_schedule_id = ts.tt_schedule_id
          where ts.tt_schedule_id in (select tt_schedule_id 
                                      from ttb.tt_schedule
                                      where tt_variant_id = l_tt_variant_id))
    with data;  
  
  delete from ttb.tt_switch sw
  where sw.tt_switch_id in (select tmp.tt_switch_id from tt_switch_temp tmp);
  
  delete from ttb.tt_tr_period
  where tt_period_id in
 (select tp.tt_period_id
  from ttb.tt_schedule ts
  join ttb.tt_period tp on tp.tt_schedule_id = ts.tt_schedule_id
  where ts.tt_schedule_id in (select tt_schedule_id 
  							  from ttb.tt_schedule
                              where tt_variant_id = l_tt_variant_id))
  and is_own
 ;
  
     with trt as (
          select tr.*
                ,ttg.tt_tr_group_prior_id
                ,tm.tr_mode_prior_id 
                ,tcp.tr_capacity_prior_id 
                ,tm.tr_mode_id  
                ,ttv.tt_variant_id as trt_tt_variant_id
                ,row_number() over (partition by tr.tt_variant_id, ttg.tt_tr_group_prior_id, tm.tr_mode_prior_id, tcp.tr_capacity_prior_id) cap_ord_gr     
                ,row_number() over (order by tr.tt_variant_id, ttg.tt_tr_group_prior_id, tm.tr_mode_prior_id, tcp.tr_capacity_prior_id) gr_ord_all      
          from ttb.tt_variant ttv
          join ttb.tt_tr tr on tr.tt_variant_id = ttv.tt_variant_id
          join ttb.tt_tr_group ttg on ttg.tt_tr_group_id = tr.tt_tr_group_id
          join ttb.tr_mode tm on tm.tr_mode_id = tr.tr_mode_id
          join ttb.tr_capacity_prior tcp on tcp.tr_capacity_id = tr.tr_capacity_id
          where ttv.tt_variant_id = l_tt_variant_id
          order by 
              ttg.tt_tr_group_prior_id
             ,tm.tr_mode_prior_id 
             ,tcp.tr_capacity_prior_id)
       ,tper_pred_pred as (
          select  tp.*
                 ,generate_series(1, tp.out_cnt) out_ord
                 ,row_number() over (partition by ts.tt_variant_id order by tp.time_begin) tper_ord
                 ,count(1) over (partition by tp.time_begin) cnt_time_beg
          from ttb.tt_period tp 
          join ttb.tt_schedule ts on tp.tt_schedule_id = ts.tt_schedule_id and ts.parent_schedule_id is null
          where ts.tt_variant_id =  l_tt_variant_id )
       ,tper_pred as (
        select tper_pred_pred.*
        	  ,max(tper_pred_pred.tper_ord) over () tper_cnt
              ,row_number() over () tper_ord_all
        from tper_pred_pred
       )         
       ,tper as (
       	 select t.* 
       		   ,row_number() over (order by t.time_begin, t.out_ord, t.tper_ord_all) as tper_ord_all_new
         from tper_pred t)
       ,ins_ttp as (         
         insert into ttb.tt_tr_period  
                    ( tt_period_id
                     ,tt_tr_id
                     ,tt_tr_order_num )      
            select tper.tt_period_id 
                  ,trt.tt_tr_id  
                  ,row_number() over ()                 
            from trt 
            join tper on --trt.gr_ord_all = tper.tper_ord_all  
            trt.gr_ord_all = tper.tper_ord_all_new
         returning * 
          )
        insert into ttb.tt_switch (tt_switch_id
        						  ,tt_schedule_item_id_from
                                  ,tt_schedule_id_to
                                  ,tt_tr_period_id_from
                                  ,tt_tr_period_id_to
                                  ,switch_time)
         select  tst.tt_switch_id
                ,tst.tt_schedule_item_id_from
                ,tst.tt_schedule_id_to
                ,case 
                   when tst.is_own and tst.is_change_to then ittp.tt_tr_period_id
                   when tst.is_own and tst.is_change_from then tst.tt_tr_period_id_from
                   else tst.tt_tr_period_id_from 
                 end
                ,case 
                   when tst.is_own and tst.is_change_to then tst.tt_tr_period_id_to 
                   when not tst.is_own and tst.is_change_to then tst.tt_tr_period_id_to 
                   when not tst.is_own and not tst.is_change_to then tst.tt_tr_period_id_to
                   else ittp.tt_tr_period_id
                 end	
                ,tst.switch_time
           from tt_switch_temp tst
           left join ins_ttp ittp on tst.tt_tr_id = ittp.tt_tr_id                                                                         
          ;
        
/*
	   select max(tt_tr_order_num)
       into ln_ord_num
        from ttb.tt_tr_period ttp
        where ttp.tt_period_id in
       (select tp.tt_period_id
        from ttb.tt_schedule ts
        join ttb.tt_period tp on tp.tt_schedule_id = ts.tt_schedule_id
        where ts.tt_schedule_id in (select tt_schedule_id 
                                    from ttb.tt_schedule
                                    where tt_variant_id = l_tt_variant_id))
        and ttp.is_own;                          

	with ins_tt_tr_per as (
       insert into ttb.tt_tr_period (tt_period_id
                            ,tt_tr_id
                            ,tt_tr_order_num
                            ,is_own)    
        select tst.tt_period_id
        	  ,tst.tt_tr_id
              ,coalesce(ln_ord_num, 0) + row_number() over () 
              ,tst.is_own
          from tt_switch_temp tst
          where tst.is_own
        returning *
        )          
       insert into ttb.tt_switch (tt_switch_id
                                ,tt_schedule_item_id_from
                                ,tt_schedule_id_to
                                ,tt_tr_period_id_from
                                ,tt_tr_period_id_to
                                ,switch_time)
       select  tst.tt_switch_id
              ,tst.tt_schedule_item_id_from
              ,tst.tt_schedule_id_to
              ,case 
              	when tst.is_own 
                  then ittp.tt_tr_period_id 
                else tst.tt_tr_period_id_from 
               end
              ,tst.tt_tr_period_id_to
              ,tst.switch_time
         from tt_switch_temp tst
         left join ins_tt_tr_per ittp on tst.tt_tr_id = ittp.tt_tr_id;
*/

  return null;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
----------------------------------------------------------