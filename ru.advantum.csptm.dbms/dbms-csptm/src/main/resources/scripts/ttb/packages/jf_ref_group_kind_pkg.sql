CREATE OR REPLACE FUNCTION ttb.jf_ref_group_kind_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.ref_group_kind AS
$body$
declare 
   l_r ttb.ref_group_kind%rowtype; 
begin 
   l_r.ref_group_kind_name := jofl.jofl_pkg$extract_varchar(p_attr, 'ref_group_kind_name', true); 
   l_r.ref_group_kind_id := jofl.jofl_pkg$extract_varchar(p_attr, 'ref_group_kind_id', true); 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ref_group_kind_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
begin 
 open p_rows for 
      select 
        ref_group_kind_name, 
        ref_group_kind_id
      from ttb.ref_group_kind; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------------------------
