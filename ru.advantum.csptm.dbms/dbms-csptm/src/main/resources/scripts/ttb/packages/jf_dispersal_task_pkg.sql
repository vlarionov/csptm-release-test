CREATE OR REPLACE FUNCTION ttb.jf_dispersal_task_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 f_arr_tt_variant_id 	integer[] 	:= jofl.jofl_pkg$extract_narray(p_attr, 'f_list_tt_variant_id', true);
 f_stop_item_id		 	integer		:= jofl.jofl_pkg$extract_number(p_attr, 'f_stop_item_id', true);
 f_dt				 	date		:= jofl.jofl_pkg$extract_date(p_attr, 'f_DT', true);
 f_disp_dt				date		:= jofl.jofl_pkg$extract_date(p_attr, 'f_disp_DT', true);
 b_disp_time_DT			timestamp	:= jofl.jofl_pkg$extract_date(p_attr, 'b_disp_time_DT', true);
 e_disp_time_DT			timestamp	:= jofl.jofl_pkg$extract_date(p_attr, 'e_disp_time_DT', true);
 f_min_disp_NUMBER		smallint	:= jofl.jofl_pkg$extract_number(p_attr, 'f_min_disp_NUMBER', TRUE);	
 f_max_work_NUMBER		smallint	:= jofl.jofl_pkg$extract_number(p_attr, 'f_max_work_NUMBER', TRUE);	
 ln_cnt					smallint;
 lt_stop_name			text;
begin 

 open p_rows for 
      select 
         dt.dispersal_task_id
        ,dt.account_id 
        ,concat_ws(' ',acc.login, '(' , acc.first_name, acc.middle_name, acc.last_name, ')') create_user
        ,dt.dt_status_id 
        ,ds.name dt_status_name
        ,dt.dt_name 
        ,dt.time_create 
        ,dt.time_calc_begin 
        ,dt.time_calc_end 
        ,dt.dt_parms::text
        ,case 
        	when dt.dt_status_id = ttb.dt_status_pkg$cn_ready()
              then '[' || json_build_object('CAPTION', s.name, 'JUMP',
          				  json_build_object('METHOD', 'ttb.tt_dispersal', 'ATTR', dt.dt_parms::text)) || ']' 
         end "JUMP_TO_RES" 
        ,'[' || json_build_object('CAPTION', 'Просмотр', 'JUMP',
          				  json_build_object('METHOD', 'ttb.tt_dispersal_parms', 'ATTR', '{"dispersal_task_id":"' || dt.dispersal_task_id || '"}')) || ']' as "JUMP_TO_PARMS"
        ,case 
            when dt.time_calc_begin is null and dt.dt_status_id = ttb.dt_status_pkg$cn_new()  
              then 'P{A},D{OF_START_DISPERSAL}'
            else 'P{A},D{}'
         end "ROW$POLICY"    
      from ttb.dispersal_task dt
      join ttb.dt_status ds on ds.dt_status_id = dt.dt_status_id
      join core.account acc on acc.account_id = dt.account_id
      join rts.stop_item si on si.stop_item_id = jsonb_extract_path_text(dt.dt_parms, 'f_stop_item_id')::int
      join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
      join rts.stop s on s.stop_id = sl.stop_id
      order by dt.time_create desc
      ; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_dispersal_task_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.dispersal_task%rowtype;
   ln_cnt	int;
begin 
   --l_r := ttb.jf_dispersal_task_pkg$attr_to_rowtype(p_attr);
   
   select count(1)
   into ln_cnt
   from ttb.dispersal_task;

   insert into ttb.dispersal_task
      (account_id
      ,dt_status_id
      ,dt_name
      ,time_create
      ,dt_parms)   
    select p_id_account
    	  ,ttb.dt_status_pkg$cn_new ()
          ,'DispTask №' || ln_cnt
          ,clock_timestamp()
          ,jsonb(p_attr);

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_dispersal_task_pkg$of_start (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   ln_cnt				int;
   f_arr_tt_variant_id 	integer[] 	:= jofl.jofl_pkg$extract_narray(p_attr, 'f_list_tt_variant_id', true);
   f_stop_item_id		integer		:= jofl.jofl_pkg$extract_number(p_attr, 'f_stop_item_id', true);
begin 
   select count(1)
   into ln_cnt
   from ttb.tt_variant
   where tt_variant_id = any(f_arr_tt_variant_id)
   group by route_variant_id
   having count(1) > 1;
   
   if ln_cnt > 1 then
      raise exception '<<Проверьте введенные данные. Выбранные варианты расписаний должны относиться к различным маршрутам.>>'; 
   end if;

   if f_stop_item_id is not null and array_length(f_arr_tt_variant_id, 1) is not null
      then perform ttb.jf_dispersal_task_pkg$of_insert(p_id_account, p_attr);
   else
   	  raise exception '<<Проверьте введенные данные. Не все параметры заполнены.>>'; 
   end if;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_dispersal_task_pkg$of_start_dispersal (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
    ln_dispersal_task_id integer := jofl.jofl_pkg$extract_number(p_attr, 'dispersal_task_id', true);	
begin
  insert into ttb.dispersal_task_queue (dispersal_task_id)
  values (ln_dispersal_task_id);
         
  update ttb.dispersal_task
     set dt_status_id = ttb.dt_status_pkg$cn_process ()
   where dispersal_task_id = ln_dispersal_task_id;	
   
   return null; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------