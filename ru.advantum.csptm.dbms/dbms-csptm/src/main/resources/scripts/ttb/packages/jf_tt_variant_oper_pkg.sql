CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_oper_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$
declare
    l_f_order_date         date := jofl.jofl_pkg$extract_date(p_attr, 'f_order_date_DT', true);
begin

 open p_rows for
select
  tt_variant_id,
  route_variant.route_variant_id,
  tt_variant.tt_status_id,
  ttv_name,
  ttv_comment,
  order_date,
  '[' || json_build_object('CAPTION', 'Выходы', 'JUMP',
               json_build_object('METHOD', 'ttb.tt_out_oper', 'ATTR', '{"tt_variant_id":"' || tt_variant.tt_variant_id || '"}')) || ']' "JUMP_TO_OUT",
   route.route_num ||' ('|| to_char(lower(route_variant.action_period),'dd.mm.yyyy') || '-'||coalesce(to_char(upper(route_variant.action_period),'dd.mm.yyyy'),'нвр')||')'   as rv_name,
   norm.norm_name,
   tt_status.tt_status_name
from ttb.tt_variant
  join rts.route_variant on tt_variant.route_variant_id =  route_variant.route_variant_id
  join rts.route on route_variant.route_id =  route.route_id
  join ttb.norm  on norm.norm_id = tt_variant.norm_id
  join ttb.tt_status on tt_status.tt_status_id = tt_variant.tt_status_id
where parent_tt_variant_id is not null
      and order_date = l_f_order_date
      and not tt_variant.sign_deleted
      and route_variant.route_id in (select adm.account_data_realm_pkg$get_routes(p_id_account));
end;

$$
