CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$send_incident(
        p_incidents_array json[]
        ) RETURNS json
	LANGUAGE plpgsql
AS $$
declare
begin

    perform paa.paa_api_pkg$basic_publish(
        cmnd.jf_cmnd_ref4unit_pkg$cn_command_rmq_broker()
        ,'incident_router'
        ,p_incidents_array[1] ->> 'incidentTypeId'
        ,json_build_object('incidentBundle', array_to_json(p_incidents_array))::text
        ,'IncidentBundle');
    return json_build_object('return', 'ok');
end;
$$;


CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$new_incident(
        p_timeStart timestamp
        ,p_incident_initiator_id integer
        ,p_incident_status_id integer
        ,p_incident_type_id integer
        ,p_parent_incident_id bigint
        ,p_account_id integer
        ,p_incident_payload json
        ,p_time_finish  timestamp default null
        ) RETURNS json
	LANGUAGE plpgsql
AS $$
begin

   return json_build_object(
            'incidentId', ttb.jf_incident_pkg$get_incident_id()
            ,'timeStart', core.timestamp2json_string(p_timeStart)
            ,'incidentInitiatorId', p_incident_initiator_id
            ,'incidentStatusId', p_incident_status_id
            ,'incidentTypeId', p_incident_type_id
            ,'parentIncidentId', p_parent_incident_id
            ,'accountId', p_account_id
            ,'timeFinish', core.timestamp2json_string(p_time_finish)
            ,'incidentPayload', p_incident_payload
   );
end;
$$;

CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$new_payload (
    p_trList integer[] default null,
    p_driverList integer[] default null,
    p_routeVarList integer[] default null,
    p_stopItemList integer[] default null,
    p_orderList integer[] default null,
    p_ttActionList integer[] default null,
    p_ttOutList integer[] default null,
    p_ttVariantList integer[] default null,
    p_unitList  integer[] default null,
    p_pointStart json default null,
    p_pointFinish json default null,
    p_factVal integer default null,
    p_deviationVal integer default null,
    p_incidentReason integer default null

) RETURNS json
	LANGUAGE plpgsql
AS $$
begin
   return json_build_object(
           'trList', array_to_json(p_trList)
           ,'driverList', array_to_json(p_driverList)
           ,'routeVarList', array_to_json(p_routeVarList)
           ,'stopItemList', array_to_json(p_stopItemList)
           ,'ttActionList', array_to_json(p_ttActionList)
           ,'orderList', array_to_json(p_orderList)
           ,'ttOutList' , array_to_json(p_ttOutList)
           ,'ttVariantList', array_to_json(p_ttVariantList)
           ,'unitList', array_to_json(p_unitList)
           ,'pointStart', p_pointStart
           ,'pointFinish', p_pointFinish
           ,'factVal', p_factVal
           ,'deviationVal', p_deviationVal
           ,'incidentReason', p_incidentReason
           );
end;
$$;

CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$new_point (
    p_lon float,
    p_lat float
) RETURNS json
	LANGUAGE plpgsql
AS $$
begin
   return json_build_object('lon', p_lon, 'lat', p_lat);
end;
$$;

CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$cn_initiator_system()
  RETURNS integer AS
  'select 0::integer;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$cn_initiator_user()
  RETURNS integer AS
  'select 1::integer;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$cn_status_new()
  RETURNS integer AS
  'select 20::integer;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$cn_timetable_violate()
  RETURNS integer AS
  'select 18::integer;'
LANGUAGE SQL IMMUTABLE;


CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$cn_timetable_update()
  RETURNS integer AS
  'select 19::integer;'
LANGUAGE SQL IMMUTABLE;


CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$cn_interval_violate()
  RETURNS integer AS
  'select 20::integer;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$cn_voice_call()
  RETURNS integer AS
  'select 21::integer;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$cn_incomming_message()
  RETURNS integer AS
  'select 22::integer;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$cn_comming_off()
  RETURNS integer AS
  'select 23::integer;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$cn_unit_disabled_by_invalid_packages_numbers()
  RETURNS integer AS
  'select 14::integer;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$cn_unit_disabled_by_invalid_packages_rate()
  RETURNS integer AS
  'select 15::integer;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$cn_unit_disabled_by_packages_time_rate()
  RETURNS integer AS
  'select 16::integer;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$cn_ok()
  RETURNS integer AS
  'select 17::integer;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$cn_manual()
  RETURNS integer AS
  'select 24::integer;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$cn_congestion()
  RETURNS integer AS
  'select 25::integer;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$cn_late_from_depo()
  RETURNS integer AS
  'select 26::integer;'
LANGUAGE SQL IMMUTABLE;


CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$cn_order_shortage()
  RETURNS integer AS
  'select 28::integer;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$cn_action_late()
  RETURNS integer AS
  'select 29::integer;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$cn_red_button()
  RETURNS integer AS
  'select 1::integer;'
LANGUAGE SQL IMMUTABLE;


CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$get_incident_id() RETURNS bigint
LANGUAGE plpgsql
AS $$
declare
  lnext_id bigint;
  l_statement text := 'ALTER SEQUENCE ttb.incident_id_db_seq MAXVALUE ';
begin
  return nextval('ttb.incident_id_db_seq');
  EXCEPTION
  WHEN object_not_in_prerequisite_state or sequence_generator_limit_exceeded THEN
begin
  lnext_id := nextval('ttb.incident_handler_id_generator_seq');
  execute 'ALTER SEQUENCE ttb.incident_id_db_seq RESTART WITH '|| lnext_id::text ||' MAXVALUE ' || (lnext_id + 1000)::text;
  return nextval('ttb.incident_id_db_seq');
end;
end;
$$;

CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$get_incident_array(incidentBundle json) RETURNS json[] AS $f$
SELECT array_agg(a)::json[]
from json_array_elements_text(incidentBundle) a;
$f$ LANGUAGE sql IMMUTABLE;


CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$cn_plan_tt_date () RETURNS timestamp without time zone
	LANGUAGE sql IMMUTABLE
AS $$
select '1900-01-01'::timestamp;
$$;


CREATE OR REPLACE FUNCTION ttb.jf_incident_pkg$get_comment_interval_violate(p_tt_vatiant int) RETURNS text
	LANGUAGE sql
AS $$
  select
      (select name
       from rts.stop
         join rts.stop_location on stop_location.stop_id = stop.stop_id
         join rts.stop_item on stop_location.stop_location_id = stop_item.stop_location_id
       where stop_item_id = (incident_params -> 'stopItemList' ->> 0)::int
      )||' '|| (incident_params ->> 'factVal')||' мин. '
FROM ttb.incident
WHERE incident.time_start BETWEEN now() - interval '1 hours' and now()
      and incident.incident_params-> 'ttVariantList' @> jsonb_build_array(p_tt_vatiant)
      and incident.incident_type_id = ttb.jf_incident_pkg$cn_interval_violate()
order by incident.time_start limit 1;
$$