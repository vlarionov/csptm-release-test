/*
возвращает первое время переключение
после p_tm_start
*/
create or replace function ttb.tt_switch_pkg$get_switch_time(in p_tt_schedule_id numeric,
                                                             in p_tt_tr_id numeric,
                                                             in p_tm_start timestamp default null)
  returns timestamp
as
$body$
declare
  l_res timestamp;
begin

  select min(sw.switch_time)
  into l_res
  from ttb.tt_switch sw
    join ttb.tt_schedule_item sch_it on sch_it.tt_schedule_item_id = sw.tt_schedule_item_id_from
    join ttb.tt_tr_period tp on tp.tt_tr_period_id = sw.tt_tr_period_id_from
  where sch_it.tt_schedule_id =p_tt_schedule_id
        and tp.tt_tr_id = p_tt_tr_id
  --and sw.switch_time > время возврата с переключения recursive?
  ;

  return l_res;
end;

$body$
language plpgsql volatile
cost 100;

/*
Данные о переключении своего выхода
когда закончить работу
*/

create or replace function ttb.tt_switch_pkg$get_switch_from(in p_tt_schedule_id numeric,
                                                        in p_tt_tr_id numeric,
                                                        in p_tm_start timestamp default null)

  returns table(switch_time timestamp , tt_schedule_item_id_from bigint , tt_variant_id_to int, tt_tr_order_num_from smallint,
                tt_tr_order_num_to smallint, tt_tr_id integer, is_switch_from_park boolean) as $$
  select sw.switch_time
         - make_interval( secs:= ttb.norm_pkg$get_schedule_tm ( p_tt_sсhedule_id=> sch_it.tt_schedule_id
                                                              , p_ref_group_kind_id => ttb.ref_group_kind_pkg$switch_action()
                                                              , p_tm_b =>  (date_trunc ('hour', sw.switch_time) - make_interval(hours:=1))::time
                                                              , p_tm_e => date_trunc ('hour', sw.switch_time)::time
                                                              , p_tr_capacity_id=>  ttr.tr_capacity_id ) ) as switch_time


    , sw.tt_schedule_item_id_from, sch.tt_variant_id, tp.tt_tr_order_num, tp_to.tt_tr_order_num, tp.tt_tr_id
    , agr.ref_group_kind_id  = ttb.ref_group_kind_pkg$switch_from_park_action() as is_switch_from_park
  from ttb.tt_switch sw
    join ttb.tt_schedule_item sch_it on sch_it.tt_schedule_item_id = sw.tt_schedule_item_id_from
    join ttb.tt_action_group agr on agr.tt_action_group_id = sch_it.tt_action_group_id
    join ttb.tt_tr_period tp on tp.tt_tr_period_id = sw.tt_tr_period_id_from
    join ttb.tt_tr_period tp_to on tp_to.tt_tr_period_id = sw.tt_tr_period_id_to
    join ttb.tt_schedule sch on sch.tt_schedule_id = sw.tt_schedule_id_to
    join ttb.tt_tr ttr on ttr.tt_tr_id = tp.tt_tr_id
  where sch_it.tt_schedule_id =p_tt_schedule_id
        and tp.tt_tr_id = p_tt_tr_id
  --and sw.switch_time > время возврата с переключения recursive?
  order by  sw.switch_time
  limit 1
  ;

$$ language sql;



/*Данные о чужом выходе
  время появления и с какого расписания пришло
*/
create or replace function ttb.tt_switch_pkg$get_switch_to(in p_tt_schedule_id numeric,
                                                             in p_tt_tr_id numeric
                                                            )

  returns table(switch_time timestamp , tt_schedule_id_from int, tt_variant_id int, tt_tr_order_num_from smallint,
                tt_tr_order_num_to smallint, tt_tr_id integer,
                tt_schedule_item_id_from bigint ,  is_switch_from_park boolean) as $$
select sw.switch_time, sch_it.tt_schedule_id, sch.tt_variant_id, tp_fr.tt_tr_order_num,  tp.tt_tr_order_num,  tp.tt_tr_id
  , sch_it.tt_schedule_item_id
  , agr.ref_group_kind_id  = ttb.ref_group_kind_pkg$switch_from_park_action() as is_switch_from_park
from ttb.tt_switch sw
  join ttb.tt_schedule_item sch_it on sch_it.tt_schedule_item_id = sw.tt_schedule_item_id_from
  join ttb.tt_action_group agr on agr.tt_action_group_id = sch_it.tt_action_group_id
  join ttb.tt_schedule sch on sch.tt_schedule_id = sch_it.tt_schedule_id
  join ttb.tt_tr_period tp on tp.tt_tr_period_id = sw.tt_tr_period_id_to
  join ttb.tt_tr_period tp_fr on tp_fr.tt_tr_period_id = sw.tt_tr_period_id_from
where sw.tt_schedule_id_to = p_tt_schedule_id
      and (p_tt_tr_id is null or tp.tt_tr_id = p_tt_tr_id)
order by  sw.switch_time;

$$ language sql;


/*Данные о расписаниях, на которые есть переключения
*/
create or replace function ttb.tt_switch_pkg$get_tt_variant_to(in p_tt_variant_id numeric)
  returns table(tt_variant_id int) as $$
select sch.tt_variant_id
from ttb.tt_switch sw
  join ttb.tt_schedule_item sch_it_fr on sch_it_fr.tt_schedule_item_id = sw.tt_schedule_item_id_from
  join ttb.tt_schedule sch_fr on sch_fr.tt_schedule_id =  sch_it_fr.tt_schedule_id
  join ttb.tt_schedule sch on sch.tt_schedule_id =  sw.tt_schedule_id_to
where sch_fr.tt_variant_id = p_tt_variant_id;
$$ language sql;



/*По расписанию вернуть его и все с ним связанное
*/
create or replace function ttb.tt_switch_pkg$get_tt_variant_bound(in p_tt_variant_id numeric)
  returns table(tt_variant_id int,  tt_schedule_id int) as $$

with recursive adata as (
    select   sch_fr.tt_variant_id as tt_variant_id_fr, sch_to.tt_variant_id as tt_variant_id_to, sw.tt_schedule_id_to,  sch_fr.tt_schedule_id as tt_schedule_id_fr
    from ttb.tt_switch sw
      join ttb.tt_schedule sch_to on sch_to.tt_schedule_id =  sw.tt_schedule_id_to
      join ttb.tt_schedule_item sch_it_fr on sch_it_fr.tt_schedule_item_id = sw.tt_schedule_item_id_from
      join ttb.tt_schedule sch_fr on sch_fr.tt_schedule_id = sch_it_fr.tt_schedule_id
)
  , v (tt_variant_id_fr, tt_variant_id_to, tt_schedule_id_to, tt_schedule_id_fr) as
(
  select adata.tt_variant_id_fr, adata.tt_variant_id_to , adata.tt_schedule_id_to, adata.tt_schedule_id_fr
  from adata
  union
  select v.tt_variant_id_fr, adata.tt_variant_id_to , adata.tt_schedule_id_to, v.tt_schedule_id_fr
  from v
    join adata  on adata.tt_variant_id_fr = v.tt_variant_id_to
)
select tt_variant_id_to, tt_schedule_id_to   from v where tt_variant_id_fr = p_tt_variant_id
union
select tt_variant_id_fr, tt_schedule_id_fr   from v where tt_variant_id_to = p_tt_variant_id
union
select tt_variant_id, tt_schedule_id   from ttb.tt_schedule sch where sch.tt_variant_id = p_tt_variant_id;
$$ language sql
stable ;