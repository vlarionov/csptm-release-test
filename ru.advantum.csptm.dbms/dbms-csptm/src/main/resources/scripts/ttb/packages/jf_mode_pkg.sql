
do $$
begin
  if not exists(select * from pg_type where typname = 'jf_mode_type') THEN

    CREATE TYPE ttb.jf_mode_type AS
      (r_dr ttb.mode,
       r_tr ttb.tr_mode);

    ALTER TYPE ttb.jf_mode_type
      OWNER TO adv;

  end if;
end$$;

------
--DROP FUNCTION ttb."jf_mode_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION ttb.jf_mode_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.jf_mode_type AS
$body$
declare
   l_r_return ttb.jf_mode_type;
   l_r ttb.mode%rowtype;
   l_r_tr ttb.tr_mode%rowtype;
begin 
   l_r.mode_id := jofl.jofl_pkg$extract_varchar(p_attr, 'mode_id', true); 
   l_r.tr_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tr_type_id', true); 
   l_r.mode_name := jofl.jofl_pkg$extract_varchar(p_attr, 'mode_name', true); 
   l_r.mode_desc := jofl.jofl_pkg$extract_varchar(p_attr, 'mode_desc', true);

   l_r_tr.tr_mode_id :=  l_r.mode_id;
   l_r_tr.break_count := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'break_count', true),0);
   l_r_tr.to_duration_max := jofl.jofl_pkg$extract_number(p_attr, 'to_duration_max', true);
   l_r_tr.to_duration_min := jofl.jofl_pkg$extract_number(p_attr, 'to_duration_min', true);
   l_r_tr.tr_mode_prior_id := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'tr_mode_prior_id', true), 1000);


   l_r_return.r_dr := l_r;
   l_r_return.r_tr := l_r_tr;

  if l_r_tr.break_count > 0 and l_r_tr.to_duration_min = 0 then
    raise exception '<<Продолжительность перерыва на ТО должна быть указана >>';
  end if;

  if l_r_tr.to_duration_max <  l_r_tr.to_duration_min then
    raise exception '<<Продолжительность перерыва на ТО должна быть указана, при этом максимальная продолжительность должна быть больше или равна минимальной >>';
  end if;

  if l_r_tr.break_count =0 then
    l_r_tr.to_duration_max := 0;
    l_r_tr.to_duration_min := 0;
  end if;

  if l_r_tr.to_duration_max <  l_r_tr.to_duration_min then
     raise exception '<<Продолжительность перерыва на ТО должна быть указана, при этом максимальная продолжительность должна быть больше или равна минимальной >>';
  end if;

  return l_r_return;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_mode_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_tr_type_id 		core.tr_type.tr_type_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tr_type_id', true);
  l_rf_db_method 	text := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true), 'xxx');
  ln_tr_capacity_id core.tr_capacity.tr_capacity_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tr_capacity_id', true);

begin
 open p_rows for
 select
   md.mode_id,
   md.tr_type_id,
   md.mode_name,
   md.mode_desc,
   tt.name as tr_type_name,
   t.tr_mode_id,
   t.break_count,
   t.to_duration_max,
   t.to_duration_min,
   m2tc.mode2tr_capacity_id,
   t.tr_mode_prior_id
 from ttb.mode md
   join ttb.tr_mode t on t.tr_mode_id = md.mode_id
   join core.tr_type tt on tt.tr_type_id = md.tr_type_id
   left join ttb.mode2tr_capacity m2tc on m2tc.mode_id = md.mode_id
  where ( l_rf_db_method  = 'xxx'
          or (
               (l_rf_db_method in ('ttb.tt_tr_group', 'ttb.tt_tr_period') and ln_tr_capacity_id is null)
               or
               (l_rf_db_method in ('ttb.tt_tr_group', 'ttb.tt_tr_period') 
               									and exists (select null
                                                            from ttb.mode2tr_capacity m2c
                                                            where m2c.mode_id = md.mode_id
                                                              and m2c.tr_capacity_id = ln_tr_capacity_id
                                                              )
               										and m2tc.mode2tr_capacity_id is not null)
               or
               (l_rf_db_method in ('ttb.tt_tr_group', 'ttb.tt_tr_period') 
               									and not exists (select null
                                                            from ttb.mode2tr_capacity m2c
                                                            where m2c.mode_id = md.mode_id
                                                              and m2c.tr_capacity_id = ln_tr_capacity_id
                                                              )
               										and m2tc.mode2tr_capacity_id is null)                                                            
           )
  		 )
    and (l_tr_type_id is null or md.tr_type_id = l_tr_type_id)
  ;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_mode_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
  l_r_return ttb.jf_mode_type;
  l_r ttb.mode%rowtype;
  l_r_tr ttb.tr_mode%rowtype;
begin
   l_r_return := ttb.jf_mode_pkg$attr_to_rowtype(p_attr);
   l_r:= l_r_return.r_dr;
   l_r_tr := l_r_return.r_tr;
   l_r_tr.tr_mode_prior_id := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'tr_mode_prior_id', true), 1000);


   update ttb.mode set 
          tr_type_id = l_r.tr_type_id, 
          mode_name = l_r.mode_name, 
          mode_desc = l_r.mode_desc
   where 
          mode_id = l_r.mode_id;

  return ttb.jf_tr_mode_pkg$of_update(p_id_account, row_to_json(l_r_tr)::text);

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_mode_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare
  declare
  l_r_return ttb.jf_mode_type;
  l_r ttb.mode%rowtype;
  l_r_tr ttb.tr_mode%rowtype;
begin
  l_r_return := ttb.jf_mode_pkg$attr_to_rowtype(p_attr);
  l_r:= l_r_return.r_dr;
  l_r_tr := l_r_return.r_tr;


   perform ttb.jf_tr_mode_pkg$of_delete(p_id_account, row_to_json(l_r_tr )::text);

   delete from  ttb.mode where  mode_id = l_r.mode_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_mode_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
  declare
  l_r_return ttb.jf_mode_type;
  l_r ttb.mode%rowtype;
  l_r_tr ttb.tr_mode%rowtype;
begin
  l_r_return 	:= ttb.jf_mode_pkg$attr_to_rowtype(p_attr);
  l_r			:= l_r_return.r_dr;
  l_r_tr 		:= l_r_return.r_tr;
  l_r_tr.tr_mode_prior_id := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'tr_mode_prior_id', true), 1000);

  l_r.mode_id 	:= nextval('ttb.mode_mode_id_seq');
  l_r_tr.tr_mode_id := l_r.mode_id;


   insert into ttb.mode select l_r.*;

   return ttb.jf_tr_mode_pkg$of_insert(p_id_account, row_to_json(l_r_tr)::text);
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------
CREATE OR REPLACE FUNCTION ttb.jf_mode_pkg$of_get_dinner_time (
  p_mode_id numeric,
  p_dr_shift_id numeric,
  p_type text
)
RETURNS interval AS
$body$
declare
 ln_time_max integer;
 ln_time_min integer;
begin

  select drm.time_max
  		,drm.time_min
  into ln_time_max
  	  ,ln_time_min
  from ttb.dr_shift drs 
  join ttb.mode m on m.mode_id = drs.mode_id  
  join ttb.dr_mode drm on drm.dr_shift_id = drs.dr_shift_id      
     where m.mode_id = p_mode_id
       and drs.dr_shift_id = p_dr_shift_id
       and drm.action_type_id = ttb.action_type_pkg$dinner();
   
  if 	lower(p_type) = 'max' then
  	return  ln_time_max * interval '1 sec';
  elsif lower(p_type) = 'min' then  
	return  ln_time_min * interval '1 sec';
  else
  	return null;
  end if;
  
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;