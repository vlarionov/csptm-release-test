create or replace function ttb.jf_rep_oper_indicator_route4period_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare 
 l_attr text;
begin
 /* добавляем параметр: тип отчета */
 select p_attr::jsonb || '{"P_REPORT_TYPE": "period"}'::jsonb into l_attr;
 select ttb.operational_indicator_reports_pkg$of_rows(p_id_account, l_attr) into p_rows;
end;
$$;
