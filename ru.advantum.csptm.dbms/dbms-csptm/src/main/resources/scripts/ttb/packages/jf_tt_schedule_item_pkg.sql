CREATE OR REPLACE FUNCTION ttb.jf_tt_schedule_item_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.tt_schedule_item AS
$body$
declare 
   l_r ttb.tt_schedule_item%rowtype; 
   ln_tt_variant_id ttb.tt_variant.tt_variant_id%type; 
begin 
   l_r.tt_schedule_item_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_schedule_item_id', true); 
   l_r.tt_schedule_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_schedule_id', true); 
   l_r.tt_action_group_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_action_group_id', true);
   l_r.repeat_time := jofl.jofl_pkg$extract_number(p_attr, 'repeat_time', true); 
   l_r.order_num := jofl.jofl_pkg$extract_varchar(p_attr, 'order_num', true); 
   l_r.is_required := jofl.jofl_pkg$extract_boolean(p_attr, 'is_required', true);
   
   select sch.tt_variant_id
   into ln_tt_variant_id
   from ttb.tt_schedule sch 
   where sch.tt_schedule_id = l_r.tt_schedule_id; 
   
   if not ttb.jf_tt_variant_pkg$of_get_policy(ln_tt_variant_id)
     then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if;     

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_schedule_item_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.tt_schedule_item%rowtype;
begin 
   l_r := ttb.jf_tt_schedule_item_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.tt_schedule_item where  tt_schedule_item_id = l_r.tt_schedule_item_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_schedule_item_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r 			ttb.tt_schedule_item%rowtype;
   ln_cnt		smallint;
begin 
   l_r := ttb.jf_tt_schedule_item_pkg$attr_to_rowtype(p_attr);
   /*
   select count(1)
   into ln_cnt
   from ttb.tt_schedule_item
   where tt_schedule_id = l_r.tt_schedule_id
     and order_num = l_r.order_num;
     
   if ln_cnt > 0 then
     RAISE EXCEPTION '<<Данное значение последовательности уже присутствует!>>';
   else     
   	l_r.tt_schedule_item_id := nextval( 'ttb.tt_schedule_item_tt_schedule_item_id_seq' ); 
   	insert into ttb.tt_schedule_item select l_r.*;
   end if;
	*/
   select coalesce(max(order_num), 0) + 1
   into l_r.order_num
   from ttb.tt_schedule_item
   where tt_schedule_id = l_r.tt_schedule_id;
   
   l_r.tt_schedule_item_id := nextval( 'ttb.tt_schedule_item_tt_schedule_item_id_seq' ); 
   insert into ttb.tt_schedule_item select l_r.*;
    
   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_schedule_item_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 l_tt_schedule_id ttb.tt_schedule.tt_schedule_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_schedule_id', true);
begin 
 open p_rows for 
      select 
        tschi.tt_schedule_item_id, 
        tschi.tt_schedule_id, 
        tschi.tt_action_group_id,
        tschi.repeat_time, 
        tschi.order_num, 
        tschi.is_required,
        tag.tt_action_group_name as action_group_name 
      from ttb.tt_schedule_item tschi
      join ttb.tt_action_group tag on tag.tt_action_group_id = tschi.tt_action_group_id
      where tschi.tt_schedule_id = l_tt_schedule_id
      order by tschi.order_num; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_schedule_item_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r 		ttb.tt_schedule_item%rowtype;
   ln_cnt	smallint;
begin 
   l_r := ttb.jf_tt_schedule_item_pkg$attr_to_rowtype(p_attr);
   /*
   select count(1)
   into ln_cnt
   from ttb.tt_schedule_item
   where tt_schedule_id = l_r.tt_schedule_id
     and order_num = l_r.order_num
     and tt_schedule_item_id <> l_r.tt_schedule_item_id;
     
   if ln_cnt > 0 then
     RAISE EXCEPTION '<<Данное значение последовательности уже присутствует!>>';
   else */ 
   if l_r.order_num is null then
     select coalesce(max(order_num), 0) + 1
     into l_r.order_num
     from ttb.tt_schedule_item
     where tt_schedule_id = l_r.tt_schedule_id;
   end if;
   
   update ttb.tt_schedule_item set 
          /*tt_schedule_id = l_r.tt_schedule_id,*/ 
          tt_action_group_id = l_r.tt_action_group_id,
          repeat_time = l_r.repeat_time, 
          order_num = l_r.order_num, 
          is_required = l_r.is_required
   where 
   		  tt_schedule_item_id = l_r.tt_schedule_item_id;
  /* end if;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
----------------------------------------------------------