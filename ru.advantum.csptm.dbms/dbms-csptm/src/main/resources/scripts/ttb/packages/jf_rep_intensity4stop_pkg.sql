
CREATE OR REPLACE FUNCTION ttb.jf_rep_intensity4stop_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare
  /*
  lf_rep_dt timestamp WITHOUT TIME ZONE := jofl.jofl_pkg$extract_date(p_attr, 'F_REP_DT', true);
  l_rep_dt date;
  lf_timestart timestamp WITHOUT TIME ZONE := jofl.jofl_pkg$extract_date(p_attr, 'F_TIMESTART_DT', true);
  lf_timeend timestamp WITHOUT TIME ZONE  := jofl.jofl_pkg$extract_date(p_attr, 'F_TIMEEND_DT', true);
  l_timestart time;
  l_timeend time;
  lf_stop_item_id bigint := jofl.jofl_pkg$extract_number(p_attr, 'f_stop_item_id', true);
  lf_depo_arr bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_arr_depo_id', true);
  lf_tr_type_arr bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_arr_tr_type_id', true);
  lf_route_arr bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_arr_route_id', true);
  lf_interval smallint := jofl.jofl_pkg$extract_number(p_attr, 'f_interval', true);
  l_interval interval;
  */
begin
  /*
 l_rep_dt := lf_rep_dt::date;
 l_timestart := lf_timestart::time;
 l_timeend := lf_timeend::time;
 l_interval := make_interval(0, 0, 0, 0, 0, 0, coalesce(lf_interval, 900));
*/
 -- временно: "пустой" отчет
  open p_rows for 
   select
null as route_num,
null as stop_name,
null::smallint as move_dir,
null::smallint as departure_cnt_plan,
null::smallint as departure_cnt_00_plan,
null::smallint as departure_cnt_fact,
null::smallint as departure_cnt_00_fact,
null::smallint as departure_prc,
null::smallint as departure_00_prc,
null::smallint as departure_intetrval_plan_avg,
null::smallint as departure_intetrval_plan_min,
null::smallint as departure_intetrval_plan_max,
null::smallint as departure_intetrval_fact_avg,
null::smallint as departure_intetrval_fact_min,
null::smallint as departure_intetrval_fact_max,
null::smallint as depart_cnt_fact_less_2min,
null::smallint as depart_prc_fact_less_2min,
null::smallint as depart_cnt_fact_gr_plan,
null::smallint as depart_prc_fact_gr_plan;

/*	 
  open p_rows for 
     select
       (select s.name
         from rts.stop s
              join rts.stop_item si on si.stop_id = s.stop_id
         where si.stop_item_id = lf_stop_item_id) as stop_name,
       r.route_num as route_num,
       release_dt.cnt_avg as release_plan,
       null::smallint as release_fact,
       null::smallint as depart_plan,
       null::smallint as depart_fact,
       null::real as depart_perc,
       null::smallint as intensity_plan_avg,
       null::smallint as intensity_fact_avg,
       null::smallint as intensity_fact_max,
       null::smallint as intensity_fact_current,
       null::real as run_plan_avg,
       null::real as run_fact_avg
     from (select *
           from ttb.tt_variant v
           \* не учитываем статус и время действия расписания из-за отсутствия данных
           where v.tt_status_id in (ttb.tt_status_pkg$active(), ttb.tt_status_pkg$arch()) \* "действующее" или "архивное" *\
                 andv.action_period @> lf_rep_dt \* фильтр по дате *\
           *\
          ) ttv
          join rts.route_variant rv on rv.route_variant_id = ttv.route_variant_id
          join rts.route r on r.route_id = rv.route_id
          join (select rdata.tt_variant_id,
                       round(avg(rdata.cnt)) as cnt_avg
                from (select outs.tt_variant_id,
                             tml.timepoint,
                             count(1) as cnt
                      from (select tto.tt_variant_id,
                                   tto.tt_out_id,
                                   tto.tt_out_num,
                                   min(ttai.time_begin) as action_time_begin,
                                   max(ttai.time_end) as action_time_end
                            from ttb.tt_out tto
                                 join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
                                 join ttb.tt_action_item ttai on ttai.tt_action_id = tta.tt_action_id
                            where ttb.action_type_pkg$is_production_round(tta.action_type_id)
                            group by tto.tt_variant_id,
                                     tto.tt_out_id,
                                     tto.tt_out_num) outs
                           join (with recursive timeline as (select l_interval as cut_interval,
                                                                    l_rep_dt + l_timestart::interval as start_dt,
                                                                    case l_timestart
                                                                     when l_timeend then l_rep_dt + interval '1 day'
                                                                     else l_rep_dt + l_timeend::interval
                                                                    end as end_dt,
                                                                    l_rep_dt + l_timestart::interval as timepoint
                                                             union
                                                             SELECT timeline.cut_interval,
                                                                    timeline.start_dt,
                                                                    timeline.end_dt,
                                                                    timeline.timepoint + timeline.cut_interval as timepoint
                                                             from timeline
                                                             where timeline.timepoint + timeline.cut_interval between start_dt and end_dt)
                                 select tl.timepoint::time
                                 from timeline tl) tml on tml.timepoint between outs.action_time_begin::time and outs.action_time_end::time
                      group by outs.tt_variant_id,
                               tml.timepoint) rdata
                group by rdata.tt_variant_id) release_dt on ttv.tt_variant_id = release_dt.tt_variant_id
     where \* фильтр по маршруту *\
           (array_length(lf_route_arr, 1) is null or r.route_id = any(lf_route_arr))
           \* фильтр по виду ТС *\
           and (array_length(lf_tr_type_arr, 1) is null or r.tr_type_id = any(lf_tr_type_arr))
           \* фильтр по ОП *\
           and (lf_stop_item_id is null or exists(select 1
                                                  from rts.round rd
                                                       join rts.stop_item2round si2rd on si2rd.round_id = rd.round_id
                                                  where not rd.sign_deleted
                                                        and ttb.action_type_pkg$is_production_round(rd.action_type_id)
                                                        and rd.route_variant_id = rv.route_variant_id
                                                        and si2rd.stop_item_id = lf_stop_item_id))
           \* фильтр по ТП *\
           AND (array_length(lf_depo_arr, 1) is null or exists(select 1
                                                               from ttb.tt_tr tttr
                                                                    join core.depo2territory d2t on d2t.depo2territory_id = tttr.depo2territory_id
                                                               where tttr.tt_variant_id = ttv.tt_variant_id
                                                                     and d2t.depo_id = any (lf_depo_arr)))
     ;
	 */

end;
$function$
;
