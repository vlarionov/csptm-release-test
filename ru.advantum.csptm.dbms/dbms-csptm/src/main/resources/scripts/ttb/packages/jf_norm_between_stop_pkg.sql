CREATE OR REPLACE FUNCTION ttb."jf_norm_between_stop_pkg$attr_to_rowtype"(p_attr text)
 RETURNS ttb.norm_between_stop
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.norm_between_stop%rowtype; 
begin 
   l_r.norm_between_stop_id := jofl.jofl_pkg$extract_number(p_attr, 'norm_between_stop_id', true); 
   l_r.norm_id := jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true); 
   l_r.hour_from := jofl.jofl_pkg$extract_varchar(p_attr, 'hour_from', true); 
   l_r.hour_to := jofl.jofl_pkg$extract_varchar(p_attr, 'hour_to', true); 
   l_r.between_stop_dur := jofl.jofl_pkg$extract_number(p_attr, 'between_stop_dur', true); 
   l_r.tr_capacity_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tr_capacity_id', true); 
   l_r.stop_item_1_id := jofl.jofl_pkg$extract_number(p_attr, 'stop_item_1_id', true); 
   l_r.stop_item_2_id := jofl.jofl_pkg$extract_number(p_attr, 'stop_item_2_id', true); 
   l_r.tr_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tr_type_id', true);
   return l_r;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_norm_between_stop_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$ 
declare
  l_norm_id ttb.norm.norm_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true);

  l_time_from TIME :=jofl.jofl_pkg$extract_varchar(p_attr, 'F_time_from', true)::TIME/*- INTERVAL '3 hour'*/;
  l_time_to TIME :=jofl.jofl_pkg$extract_varchar(p_attr, 'F_time_to', true)::TIME/* - INTERVAL '3 hour'*/;
  l_tr_type_names TEXT[] := jofl.jofl_pkg$extract_tarray(p_attr, 'F_tr_type_name', true);
  l_tr_type_id BIGINT[] := array(select tr_type_id from core.tr_type where name = ANY(l_tr_type_names));
  l_stops TEXT[] := jofl.jofl_pkg$extract_tarray(p_attr, 'F_ids', true);
  l_capacity_id BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_tr_capacity_id', true);
begin 
 open p_rows for
 WITH t1 AS (select
   nbs.norm_between_stop_id,
   nbs.norm_id,
   (nbs.hour_from /*+ INTERVAL '3 hour'*/)::text as hour_from,
   (nbs.hour_to /*+ INTERVAL '3 hour'*/)::text as hour_to,
   nbs.between_stop_dur,
   nbs.tr_capacity_id,
   nbs.stop_item_1_id,
   nbs.stop_item_2_id,
   nbs.tr_type_id,
   tt.name as tr_type_name,
   cp.short_name as tr_capacity_short_name,
   s_1.name as stop_name_1,
   s_2.name as stop_name_2,
   net.norm_edit_type_name,
   nbs.stop_item_1_id||'-'||nbs.stop_item_2_id as ids
 from ttb.norm_between_stop nbs
   join ttb.norm_edit_type net on net.norm_edit_type_id = nbs.norm_edit_type_id
   join core.tr_type tt on tt.tr_type_id = nbs.tr_type_id
   join core.tr_capacity cp on cp.tr_capacity_id = nbs.tr_capacity_id
   join rts.stop_item si_1 on si_1.stop_item_id = nbs.stop_item_1_id
   join rts.stop_location sl_1 on sl_1.stop_location_id = si_1.stop_location_id
   join rts.stop s_1 on s_1.stop_id = sl_1.stop_id
   join rts.stop_item si_2 on si_2.stop_item_id = nbs.stop_item_2_id
   join rts.stop_location sl_2 on sl_2.stop_location_id = si_2.stop_location_id
   join rts.stop s_2 on s_2.stop_id = sl_2.stop_id
 where nbs.norm_id = l_norm_id
 AND ((l_time_from IS NULL AND l_time_to IS NULL) OR (nbs.hour_from >=l_time_from AND nbs.hour_to <=l_time_to AND nbs.hour_to > l_time_from) )
 AND ((nbs.tr_type_id = ANY(l_tr_type_id)) OR (array_length(l_tr_type_names, 1) IS NULL))
 AND (nbs.tr_capacity_id = ANY(l_capacity_id) OR (array_length(l_capacity_id, 1) IS NULL) )
 )
 SELECT * from t1 WHERE (ids = ANY(l_stops) OR array_length(l_stops, 1) IS NULL)
 ORDER BY hour_from::TIME, hour_to::TIME;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_norm_between_stop_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.norm_between_stop%rowtype;
begin 
   l_r := ttb.jf_norm_between_stop_pkg$attr_to_rowtype(p_attr);

   update ttb.norm_between_stop set 
          norm_id = l_r.norm_id, 
          hour_from = l_r.hour_from, 
          hour_to = l_r.hour_to, 
          between_stop_dur = l_r.between_stop_dur, 
          tr_capacity_id = l_r.tr_capacity_id, 
          stop_item_1_id = l_r.stop_item_1_id, 
          stop_item_2_id = l_r.stop_item_2_id, 
          tr_type_id = l_r.tr_type_id,
          norm_edit_type_id = ttb.norm_edit_type$hand()
   where 
          norm_between_stop_id = l_r.norm_between_stop_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_norm_between_stop_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.norm_between_stop%rowtype;
begin 
   l_r := ttb.jf_norm_between_stop_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.norm_between_stop where  norm_between_stop_id = l_r.norm_between_stop_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_norm_between_stop_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.norm_between_stop%rowtype;
begin 
   l_r := ttb.jf_norm_between_stop_pkg$attr_to_rowtype(p_attr);
   l_r.norm_between_stop_id := nextval('ttb.norm_stop.norm_between_stop_id_seq');
   l_r.norm_edit_type_id = ttb.norm_edit_type$hand();

   insert into ttb.norm_between_stop select l_r.*;

   return null;
end;
 $function$
;

create or REPLACE function ttb."jf_norm_between_stop_pkg$of_set_new"  (p_id_account numeric, p_attr text) returns boolean
LANGUAGE plpgsql
AS $$
DECLARE
  l_dur BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'time_between_stops', true);
  l_norm_id BIGINT;

  l_id_for_update BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'arr_id_TEXT', true);
BEGIN
  UPDATE ttb.norm_between_stop SET between_stop_dur = l_dur * 60, norm_edit_type_id = 3 -- ручная правка
    WHERE norm_between_stop_id = ANY(l_id_for_update);
  -- любой norm_id
  select norm_id INTO l_norm_id from ttb.norm_between_stop where norm_between_stop_id = l_id_for_update[1];
  -- заполнить продолжительность рейса суммой продолжительности м/у ОП
  with a AS(
      select DISTINCT on (round_id) round_id, code  from ttb.norm_round
        JOIN rts.round USING(round_id)
      where norm_id = l_norm_id
  ),
      t2 AS(
        select round_id, order_num, stop_item_id,
          lead(stop_item_id) OVER (PARTITION BY round_id order by order_num), code
        from a
          JOIN rts.stop_item2round USING (round_id)
    ),
      t3 AS(
        select * from t2
          JOIN ttb.norm_between_stop nbs ON t2.stop_item_id = nbs.stop_item_1_id
                                            AND t2.lead = nbs.stop_item_2_id
                                            AND nbs.norm_id = l_norm_id
    ),
      d AS(
        select distinct on (round_id, tr_capacity_id, hour_from, hour_to)
          tr_type_id tr_type_id1, tr_capacity_id tr_capacity_id1, hour_from hour_from1, round_id round_id1, norm_id norm_id1,
          sum(between_stop_dur) over (PARTITION BY round_id, tr_capacity_id, tr_type_id, hour_from, hour_to) round_duration_NEW
        from t3
    )
  update ttb.norm_round t
  set round_duration =
  coalesce((
             select  d.round_duration_NEW from d
             where
               norm_id = d.norm_id1
               and round_id  = d.round_id1
               and tr_type_id= d.tr_type_id1
               and tr_capacity_id =d.tr_capacity_id1
               and hour_from  = d.hour_from1
           ),0)
  where norm_id = l_norm_id;

  return TRUE;
END;
$$;

create or REPLACE function ttb."jf_norm_between_stop_pkg$of_calc_mean"  (p_id_account numeric, p_attr text) returns boolean
LANGUAGE plpgsql
AS $$
DECLARE
  all_id BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'all_id_TEXT', true);
  l_norm_id BIGINT;
BEGIN
  WITH t1 AS (
      select *, stop_item_1_id::TEXT||'-'||stop_item_2_id::TEXT as ids
      from ttb.norm_between_stop where norm_between_stop_id = ANY(all_id)
  ),
  t2 as(
      select ids, avg(between_stop_dur) from t1 group by ids
  )
  UPDATE ttb.norm_between_stop
  -- время в секундах, округленное до минут
  SET between_stop_dur =
  (select asd.round_seconds2min(avg::NUMERIC) from t2 where t2.ids = stop_item_1_id::TEXT||'-'||stop_item_2_id::TEXT), norm_edit_type_id = 3
    -- ручная правка
  WHERE norm_between_stop_id = ANY(all_id);

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++==============================================
  select norm_id INTO l_norm_id from ttb.norm_between_stop where norm_between_stop_id = all_id[1];
  -- заполнить продолжительность рейса суммой продолжительности м/у ОП
  with a AS(
      select DISTINCT on (round_id) round_id, code  from ttb.norm_round
        JOIN rts.round USING(round_id)
      where norm_id = l_norm_id
  ),
      t2 AS(
        select round_id, order_num, stop_item_id,
          lead(stop_item_id) OVER (PARTITION BY round_id order by order_num), code
        from a
          JOIN rts.stop_item2round USING (round_id)
    ),
      t3 AS(
        select * from t2
          JOIN ttb.norm_between_stop nbs ON t2.stop_item_id = nbs.stop_item_1_id
                                            AND t2.lead = nbs.stop_item_2_id
                                            AND nbs.norm_id = l_norm_id
    ),
      d AS(
        select distinct on (round_id, tr_capacity_id, hour_from, hour_to)
          tr_type_id tr_type_id1, tr_capacity_id tr_capacity_id1, hour_from hour_from1, round_id round_id1, norm_id norm_id1,
          sum(between_stop_dur) over (PARTITION BY round_id, tr_capacity_id, tr_type_id, hour_from, hour_to) round_duration_NEW
        from t3
    )
  update ttb.norm_round t
  set round_duration =
  coalesce((
             select  d.round_duration_NEW from d
             where
               norm_id = d.norm_id1
               and round_id  = d.round_id1
               and tr_type_id= d.tr_type_id1
               and tr_capacity_id =d.tr_capacity_id1
               and hour_from  = d.hour_from1
           ),0)
  where norm_id = l_norm_id;

  return TRUE;
END;
$$;


create or REPLACE function ttb."jf_norm_between_stop_pkg$of_norm" (p_id_account numeric, p_attr text) returns boolean
LANGUAGE plpgsql
AS $$
DECLARE
  l_norm_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true);
  l_round_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'round_id', true);
  l_duration BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'duration', true);
  l_time_from TIME := jofl.jofl_pkg$extract_varchar(p_attr, 'F_time_from', true)::TIME;
  l_time_to TIME := jofl.jofl_pkg$extract_varchar(p_attr, 'F_time_to', true)::TIME;
  l_capacity_id BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'capacity', true);

  l_total_length NUMERIC;
  l_tr_type_id BIGINT;
  l_date DATE := '1900-01-01'::DATE;
  l_sys_period TSTZRANGE;
BEGIN
  -- расчет времени между остановками
  -- длина всей траектории в метрах
  select SUM(length_sector) INTO l_total_length from rts.stop_item2round where round_id = l_round_id;
  -- тип ТС
  select tr_type_id INTO l_tr_type_id from ttb.norm_round where norm_id = l_norm_id LIMIT 1;
  -- sys_period
  select sys_period INTO l_sys_period from ttb.norm_between_stop where norm_id = l_norm_id LIMIT 1;

  WITH a AS(
  select lag(stop_item_id) OVER (ORDER BY order_num) as "from_stop_item",*,
    length_sector::NUMERIC / (select SUM(length_sector) from rts.stop_item2round where round_id = l_round_id) as part
  from rts.stop_item2round
  where round_id = l_round_id order by order_num),
  b AS (
      select * , round(part * l_duration * 60) from a where from_stop_item IS NOT NULL
  ),
  t1 AS (
        SELECT nextval('ttb.norm_between_stop_norm_between_stop_id_seq'::regclass) as norm_between_stop_id,
               l_norm_id as "norm_id",
               s::TIME as "hour_from",
               CASE WHEN (s::TIME + INTERVAL '1 hour') = '00:00:00' THEN '24:00:00'
                 ELSE s::TIME + INTERVAL '1 hour' END as "hour_to",
               asd.round_seconds2min(b.round::NUMERIC) as "between_stop_dur",
               /*l_capacity_id*/ cap as "tr_capacity_id",
               b.from_stop_item as "stop_item_1_id",
               b.stop_item_id as "stop_item_2_id",
               l_tr_type_id as "tr_type_id",
               3 as "norm_edit_type_id", -- 3 - ручная правка
               l_sys_period as "sys_period"
        from  generate_series(l_date + l_time_from, l_date + l_time_to - INTERVAL '1 hour', INTERVAL '1 hour') s, b,
              unnest(l_capacity_id) as "cap"
  )
  INSERT into ttb.norm_between_stop AS nbs SELECT * from t1
  ON CONFLICT ON CONSTRAINT "UNIQUE_row"
    DO UPDATE SET between_stop_dur = EXCLUDED.between_stop_dur, norm_edit_type_id = 3
     WHERE nbs.stop_item_1_id = EXCLUDED.stop_item_1_id
     AND   nbs.stop_item_2_id = EXCLUDED.stop_item_2_id
     AND   nbs.hour_from = EXCLUDED.hour_from
     AND   nbs.hour_to = EXCLUDED.hour_to
     AND   nbs.tr_type_id = EXCLUDED.tr_type_id
     AND   nbs.tr_capacity_id = EXCLUDED.tr_capacity_id
     AND   nbs.norm_id =  EXCLUDED.norm_id;
  -- +++++++++++++++++++++++++++++++++++++++============================================================================
  -- заполнить продолжительность рейса суммой продолжительности м/у ОП
  with a AS(
      select DISTINCT on (round_id) round_id, code  from ttb.norm_round
        JOIN rts.round USING(round_id)
      where norm_id = l_norm_id
  ),
      t2 AS(
        select round_id, order_num, stop_item_id,
          lead(stop_item_id) OVER (PARTITION BY round_id order by order_num), code
        from a
          JOIN rts.stop_item2round USING (round_id)
    ),
      t3 AS(
        select * from t2
          JOIN ttb.norm_between_stop nbs ON t2.stop_item_id = nbs.stop_item_1_id
                                            AND t2.lead = nbs.stop_item_2_id
                                            AND nbs.norm_id = l_norm_id
    ),
      d AS(
        select distinct on (round_id, tr_capacity_id, hour_from, hour_to)
          tr_type_id tr_type_id1, tr_capacity_id tr_capacity_id1, hour_from hour_from1, round_id round_id1, norm_id norm_id1,
          sum(between_stop_dur) over (PARTITION BY round_id, tr_capacity_id, tr_type_id, hour_from, hour_to) round_duration_NEW
        from t3
    )
  update ttb.norm_round t
  set round_duration =
  coalesce((
             select  d.round_duration_NEW from d
             where
               norm_id = d.norm_id1
               and round_id  = d.round_id1
               and tr_type_id= d.tr_type_id1
               and tr_capacity_id =d.tr_capacity_id1
               and hour_from  = d.hour_from1
           ),0)
  where norm_id = l_norm_id;

  return TRUE;
END;
$$;

-- #23439 Изменения в правилах округления
create or REPLACE function asd.round_seconds2min(p_seconds NUMERIC) returns BIGINT
LANGUAGE plpgsql
AS $$
DECLARE
  l_result NUMERIC;
BEGIN
  l_result := CASE WHEN (p_seconds < 60) AND (p_seconds > 0) THEN 60
              ELSE round(p_seconds / 60.0) * 60
              END;
  RETURN l_result;
END;
$$;


create or replace function ttb.jf_norm_between_stop_pkg$MSG_SET_NEW(p_id_account numeric, p_attr text)
    returns text
language plpgsql
as $function$
declare
    tt_variants text;
    l_r_norm_id integer := jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true);
begin
    if (exists(select *
               from ttb.tt_variant tv
               where tv.norm_id = l_r_norm_id)
    )
    then
        select 'Изменяемый норматив используется в следующих расписаниях<br><table>  <tr> <td>Вариант расписания </td>  <td> Маршрут </td> </tr>' || string_agg(' <tr>' || '<td>' || tv.ttv_name || '</td>  <td>' || r.route_num || '</td> </tr>', '') || '</table><br>Продолжить?'
        into tt_variants
        from ttb.tt_variant tv
            join rts.route_variant rv on tv.route_variant_id = rv.route_variant_id
            join rts.route r on rv.route_variant_id = r.current_route_variant_id
        where tv.norm_id = l_r_norm_id  and tv.tt_status_id<>ttb.jf_tt_status_pkg$cn_cancel_status_id();
    else tt_variants :=null ;
    end if;
    return '{"title": "Варианты расписания",
        "message": "' || tt_variants || '"}';
end;
$function$;