﻿------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Очистить итоговое расписание
***************************************************************************/
create or replace function ttb."timetable_test_pkg$clean_tt"(in p_tt_variant_id numeric)
  returns void
as
$body$
declare
begin
  --delete from ttb.tt_out where parent_tt_out_id  in (select tt_out_id from ttb.tt_out where tt_variant_id = p_tt_variant_id);
  discard temp;
  delete from ttb.tt_cube
  using ttb.tt_switch_pkg$get_tt_variant_bound(p_tt_variant_id) vb
  where tt_cube.tt_variant_id = vb.tt_variant_id;
  -- delete from ttb.tt_out where tt_variant_id = p_tt_variant_id;
  -- update ttb.tt_out tout set sign_deleted =true where tt_variant_id = p_tt_variant_id;
  with del_out as (
    update ttb.tt_out tout
    set sign_deleted =true
    from  ttb.tt_switch_pkg$get_tt_variant_bound(p_tt_variant_id) vb
    where tout.tt_variant_id = vb.tt_variant_id
    returning *)
    , del_act as (
    update ttb.tt_action
    set sign_deleted =true
    from del_out
    where del_out.tt_out_id = tt_action.tt_out_id
          and not tt_action.sign_deleted
    returning *
  )
  update ttb.tt_action_item
  set sign_deleted =true
  from del_act
  where del_act.tt_action_id= tt_action_item.tt_action_id
        and not tt_action_item.sign_deleted;


  /* ttb.tt_switch_pkg$get_tt_variant_to(in p_tt_variant_id numeric)
  скорее всего нужно будет удалять расписания, на которые переключения
  */

end;
$body$
language plpgsql volatile
cost 100;



------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Очистить назначение ТС
***************************************************************************/
create or replace function ttb."timetable_test_pkg$clean_tr"(in p_tt_variant_id numeric)
  returns void
as
$body$
declare
begin
  delete from   ttb. tt_cube where tt_variant_id = p_tt_variant_id and ttb.action_type_pkg$is_depo(action_type_id);
  update  ttb. tt_cube set tt_tr_id = null where tt_variant_id = p_tt_variant_id;
end;
$body$
language plpgsql volatile
cost 100;

------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Время первого появления ТС на маршруте (время первого рейса)
***************************************************************************/
create or replace function ttb."timetable_test_pkg$get_start_time"(in p_tt_at_round_id numeric)
  returns ttb.atgrs_time.start_time%type as
$body$
declare
  l_res ttb.atgrs_time.start_time%type;
begin
  select  min(atg.start_time) as start_time
  into l_res
  from ttb.atgrs_time atg
    join ttb.tt_at_round_stop ars on ars.tt_at_round_stop_id = atg.tt_at_round_stop_id
    join ttb.tt_at_round ar on ar.tt_at_round_id = ars.tt_at_round_id
    join rts.stop_item2round sir on sir.stop_item2round_id = ars.stop_item2round_id and not sir.sign_deleted
  where atg.fix_arrival_type_id = ttb.fix_arrival_type_pkg$start_time()
        and sir.order_num =1
        and  ar.tt_at_round_id = p_tt_at_round_id;

  /*if l_res is null then
     raise exception '<<Не указано время первого рейса!>>';
  end if;*/
  return l_res;
end;

$body$
language plpgsql volatile
cost 100;

------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Время начала последнего рейса ТС на маршруте
***************************************************************************/
create or replace function ttb."timetable_test_pkg$get_finish_time"(in p_tt_at_round_id numeric)
  returns ttb.atgrs_time.start_time%type as
$body$
declare
  l_res ttb.atgrs_time.start_time%type;
begin
  select  max(atg.start_time) as start_time
  into l_res
  from ttb.atgrs_time atg
    join ttb.tt_at_round_stop ars on ars.tt_at_round_stop_id = atg.tt_at_round_stop_id
    join ttb.tt_at_round ar on ar.tt_at_round_id = ars.tt_at_round_id
    join rts.stop_item2round sir on sir.stop_item2round_id = ars.stop_item2round_id and not sir.sign_deleted
  where atg.fix_arrival_type_id = ttb.fix_arrival_type_pkg$finish_time()
        and sir.order_num =1
        and  ar.tt_at_round_id = p_tt_at_round_id;

  /*if l_res is null then
     raise exception '<<Не указано время первого рейса!>>';
  end if;*/

  return l_res;
end;

$body$
language plpgsql volatile
cost 100;
alter function ttb."timetable_test_pkg$get_finish_time"(numeric)
owner to adv;


------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
--времена рейса
 p_tt_at_round_id id рейса расписания
 p_tr_capacity_id вместимость
 p_tm_start время от которого считать
 p_is_last boolean default true направление расчета true -вперед false - назад
 p_order_num от какой остановки считаем
***************************************************************************/

create or replace function ttb."timetable_test_pkg$get_round_tm"(in p_tt_at_round_id numeric,
                                                            in p_tr_capacity_id numeric,
                                                            in p_tm_start timestamp,
                                                            in p_is_last boolean default true,
                                                            in p_order_num numeric default null)
  returns TABLE(
    fstop_item2round_id bigint,
    fround_id integer,
    fstop_item_id integer,
    ftr_capacity_id smallint,
    ftt_variant_id integer,
    faction_type_id smallint,
    ftime_begin timestamp ,
    forder_num smallint,
    fdur_from_beg integer, -- Продолжительность от начала рейса
    fdur_round integer -- Продолжительность рейса
  )
as
$body$
declare
begin
  if p_is_last then
    --по началу рейса получаем расписание
    return query
    with recursive
        sir as (
          select
             row_number() over(order by  sir.order_num)  as rn
            ,lag(sir.stop_item_id) over(order by  sir.order_num) as stop_item_1_id, sir.stop_item_id as stop_item_2_id , sir.order_num
            ,sir.stop_item2round_id, v.norm_id, r.tr_type_id, v.tt_variant_id, act.action_type_id, sir.round_id, sir.stop_item_id
          from ttb.tt_at_round_stop ars
            join rts.stop_item2round sir on sir.stop_item2round_id = ars.stop_item2round_id
            join ttb.tt_at_round ar on ar.tt_at_round_id = ars.tt_at_round_id
            join ttb.tt_action_type act on act.tt_action_type_id = ar.tt_action_type_id
            join ttb.tt_variant v on v.tt_variant_id = act.tt_variant_id
            join rts.route_variant rv on rv.route_variant_id = v.route_variant_id
            join rts.route r on r.route_id = rv.route_id
          where ar.tt_at_round_id = p_tt_at_round_id
                and  (p_order_num  is null or sir.order_num >= p_order_num)

      )
      , nr as (
        select t.*, sir.rn
        from ttb.norm_between_stop t
          join sir on t.stop_item_1_id= sir.stop_item_1_id and  t.stop_item_2_id= sir.stop_item_2_id and t.norm_id = sir.norm_id and t.tr_type_id = sir.tr_type_id
        where  t.tr_capacity_id =p_tr_capacity_id
    )
      ,   sp as (
      select
        nr.rn,
        nr.between_stop_dur,
        p_tm_start  +  nr.between_stop_dur*interval '1 second' as tm
      from nr
      where rn=2
            and hour_from= date_trunc('hour',p_tm_start::time)
      union
      select
        nr.rn,
        nr.between_stop_dur,
        sp.tm  +  nr.between_stop_dur*interval '1 second' as tm
      from nr
        join sp on date_trunc('hour',sp.tm::time) = nr.hour_from and nr.rn = sp.rn+1
    )
    select
      sir.stop_item2round_id
      ,sir.round_id
      ,sir.stop_item_id
      ,p_tr_capacity_id::smallint as tr_capacity_id
      ,sir.tt_variant_id
      ,sir.action_type_id
      ,p_tm_start  as tm
      ,sir.order_num::smallint
      ,0::integer as between_stop_dur
      ,(select sum(between_stop_dur) from sp)::integer
    from sir
    where stop_item_1_id is null
    union all
    select
      sir.stop_item2round_id
      ,sir.round_id
      ,sir.stop_item_id
      ,p_tr_capacity_id::smallint as tr_capacity_id
      ,sir.tt_variant_id
      ,sir.action_type_id
      ,sp.tm::timestamp as tm
      ,sir.order_num::smallint
      ,sp.between_stop_dur
      ,(select sum(sp.between_stop_dur) from sp)::integer
    from sp
      join sir  on sp.rn =sir.rn

    ;

  else ---p_is_last=false ---разворот в обратном направлении


    --по ОКОНЧАНИЮ рейса получаем расписание
    return query
    with recursive
        sir as (
          select
             row_number() over(order by  sir.order_num)  as rn
            ,lag(sir.stop_item_id) over(order by  sir.order_num ) as stop_item_1_id, sir.stop_item_id as stop_item_2_id , sir.order_num
            ,sir.stop_item2round_id, v.norm_id, r.tr_type_id, v.tt_variant_id, act.action_type_id, sir.round_id, sir.stop_item_id
            ,max(sir.order_num) over () as max_num
          from ttb.tt_at_round_stop ars
            join rts.stop_item2round sir on sir.stop_item2round_id = ars.stop_item2round_id
            join ttb.tt_at_round ar on ar.tt_at_round_id = ars.tt_at_round_id
            join ttb.tt_action_type act on act.tt_action_type_id = ar.tt_action_type_id
            join ttb.tt_variant v on v.tt_variant_id = act.tt_variant_id
            join rts.route_variant rv on rv.route_variant_id = v.route_variant_id
            join rts.route r on r.route_id = rv.route_id
          where ar.tt_at_round_id = p_tt_at_round_id
                and  (p_order_num  is null or sir.order_num >= p_order_num)

      )
      , nr as (
        select t.*, sir.rn, sir.max_num
        from ttb.norm_between_stop t
          join sir on t.stop_item_1_id= sir.stop_item_1_id and  t.stop_item_2_id= sir.stop_item_2_id and t.norm_id = sir.norm_id and t.tr_type_id = sir.tr_type_id
        where  t.tr_capacity_id = p_tr_capacity_id
    )
      ,sp as (
      select
        nr.rn,
        nr.between_stop_dur,
        p_tm_start  -  nr.between_stop_dur*interval '1 second' as tm
      from nr
      where rn =  max_num
            and hour_from= date_trunc('hour',p_tm_start::time)
      union
      select
        nr.rn,
        nr.between_stop_dur,
        sp.tm  -  nr.between_stop_dur*interval '1 second' as tm
      from nr
        join sp on date_trunc('hour',sp.tm::time) = nr.hour_from and nr.rn = sp.rn -1
    )


    select
      sir.stop_item2round_id
      ,sir.round_id
      ,sir.stop_item_id
      ,p_tr_capacity_id::smallint as tr_capacity_id
      ,sir.tt_variant_id
      ,sir.action_type_id
      ,p_tm_start  as tm
      ,sir.order_num::smallint
      ,0::integer as between_stop_dur
      ,(select sum(between_stop_dur) from sp)::integer
    from sir
    where rn= max_num
    union all
    select
      sir.stop_item2round_id
      ,sir.round_id
      ,sir.stop_item_id
      ,p_tr_capacity_id::smallint as tr_capacity_id
      ,sir.tt_variant_id
      ,sir.action_type_id
      ,sp.tm::timestamp as tm
      ,sir.order_num::smallint
      ,sp.between_stop_dur
      ,(select sum(sp.between_stop_dur) from sp)::integer
    from sp
      join sir  on sp.rn-1 =sir.rn
    ;

  end if;
end;
$body$
language plpgsql immutable;


/***************************************************************************
NEW!!! --время отправки с ОП с учетом настройки периодов
p_is_add_itv  создать дополнительные итервалы
***************************************************************************/

create or replace function ttb."timetable_test_pkg$get_start_tm"(in p_tt_variant_id numeric,
                                                            in p_tt_schedule_id numeric,
                                                            in p_tt_at_round_id numeric,
                                                            in p_tm_start timestamp,
                                                            in p_is_add_itv boolean default false)
  returns TABLE(
    start_tm_id integer,
    start_time  timestamp,
    is_require  boolean,
    sit_id integer
  )
as
$body$
declare
begin
  return query
  with recursive itl(tt_period_id, time_start, time_finish, interval_time, itrv, limit_time, delta_time,movement_type_id
    , time_begin, time_end, is_interval_alignment, stop_item2round_id, stop_item_id)  as (
    select
      p.tt_period_id
      --сдвиг дополнительных рейсов на час, чтобы не мешали стартовать в интервальности
      ,greatest(p.time_begin,p_tm_start /*+ case when p_is_add_itv then INTERVAL '1 hours' else INTERVAL '0 hours' end*/) as time_start
      ,least (p.time_end, ttb.timetable_test_pkg$get_finish_time( at_rnd.tt_at_round_id  )) as time_finish
      ,p.interval_time
      --увеличиваем матрицу максимально до 1 минуты
      -- ,case when p_is_add_itv then 120  else p.interval_time end * interval '1 second' as itrv
      ,date_trunc('minute',
                  case when p_is_add_itv and p.movement_type_id !=ttb.jf_movement_type_pkg$cn_type_clock()
                    then case when  p.interval_time/3  < 60 then 60 else least(180, p.interval_time/3) end
                  else p.interval_time end  * interval '1 second') as itrv

      /*  ,date_trunc('minute',
                    case when p.interval_time/case when p_is_add_itv then 3 else 1 end < 60 then 60 else
                      p.interval_time/case when p_is_add_itv then 3 else 1 end end * interval '1 second')  as itrv*/

      ,p.limit_time
      ,null::interval
      ,p.movement_type_id
      ,p.time_begin
      ,p.time_end
      ,not p_is_add_itv and (select count(1) from ttb.timetable_test_pkg$get_stop_interval_alignment(p_tt_variant_id) where fstop_item_id =sir.stop_item_id)::int::boolean as is_interval_alignment
      ,sir.stop_item2round_id
      ,sir.stop_item_id
    from ttb.tt_variant v
      join ttb.tt_schedule sch on sch.tt_variant_id = v.tt_variant_id
      join ttb.tt_period p on p.tt_schedule_id =sch.tt_schedule_id
      join ttb.tt_at_round at_rnd on at_rnd.tt_at_round_id  = p_tt_at_round_id
      join rts.round rnd on rnd.round_id = at_rnd.round_id  and not rnd.sign_deleted
      join rts.stop_item2round sir on sir.round_id = rnd.round_id and sir.order_num =1 and not sir.sign_deleted
    where sch.tt_variant_id = p_tt_variant_id
          and sch.tt_schedule_id = p_tt_schedule_id

    union all
    select
      tt_period_id
      , itl.time_start + itl.itrv
      ,time_finish
      ,interval_time
      ,itrv
      ,limit_time
      ,case when time_finish - (itl.time_start + itl.itrv) < itl.itrv then  itl.itrv  - (time_finish - (itl.time_start + itl.itrv )) end
      ,itl.movement_type_id
      ,itl.time_begin
      ,itl.time_end
      ,itl.is_interval_alignment
      ,itl.stop_item2round_id
      ,itl.stop_item_id
    from itl
    where itl.time_start + itl.itrv  <itl.time_finish
          and itl.movement_type_id  != ttb.jf_movement_type_pkg$cn_type_street() --исключая уличное
          and itl.interval_time>0
          and itl.time_end > itl.time_start
  )
    , itog as (
      select  itl.*,
        lag(delta_time,1, interval '0 second') over(order by time_start) dt
      from itl
      order by tt_period_id, time_start
  )
    -- время, обязательное для выполнения
    , street as
  (select atg.start_time, itl.stop_item_id
   from
     ttb.atgrs_time atg
     join ttb.tt_at_round_stop ars on ars.tt_at_round_stop_id = atg.tt_at_round_stop_id
     join itl on itl.stop_item2round_id = ars.stop_item2round_id and  atg.start_time between itl.time_start and itl.time_finish
   where ars.tt_at_round_id  = p_tt_at_round_id
         and not  p_is_add_itv
  )
    --время для переключения
    , switch  as (
      select switch_time as start_time , (select stop_item_id from  itl limit 1) as stop_item_id
      from  ttb.tt_switch_pkg$get_switch_to(p_tt_schedule_id, null)
  )
    , heap as (
    select time_start as time_start_sp-- case when time_start + sum(dt) over(order by time_start)<= time_finish then time_start + sum(dt) over(order by time_start) else null end  as time_start_sp
      , is_interval_alignment as is_require
      , stop_item_id
    from itog
    where not exists (select 1 from tt_cube t where t.order_num =1 and t.stop_item_id = itog.stop_item_id and t.time_begin = itog.time_start and t.tt_schedule_id = p_tt_schedule_id)
    union
    select  street.start_time as time_start_sp, true as is_require, street.stop_item_id
    from street
    union
    select  switch.start_time as time_start_sp, false as is_require, switch.stop_item_id
    from switch
  )
  select
    (row_number () over(order by heap.time_start_sp))::int as rn,
    heap.time_start_sp,
    heap.is_require,
    heap.stop_item_id
  from heap
  where heap.is_require or not exists (select 1 from heap h where h.time_start_sp  = heap.time_start_sp and h.is_require )

  ;


end;
$body$
language plpgsql volatile
cost 100;

-----------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Набор времени для осуществления действий
p_stop_item_id ОП, с которой начинаем
***************************************************************************/


create or replace function ttb."timetable_test_pkg$get_time_start_arr"(in p_tt_variant_id numeric,
                                                                  in p_tt_schedule_id numeric,
                                                                  in p_is_last boolean default true,
                                                                  in p_time_start timestamp default null,
                                                                  in p_tt_tr_id numeric default null,
                                                                  in p_tt_at_round_id numeric default null,
                                                                  in p_stop_item_id  numeric default null,
                                                                  in p_one_id numeric default null,
                                                                  in p_is_add_itv boolean default false
)
  returns TABLE(
    fstart_time     timestamp ,
    fid             integer, --порядковый номер
    ftr_capacity_id smallint,  -- вместимость
    fis_require      boolean   --обязательный рейс
  )
as
$body$
declare
  l_cnt smallint;
begin
  select coalesce(max(cb.tmp_id),0)  + 1 into l_cnt
  from  tt_cube cb
  where cb.tt_schedule_id = p_tt_schedule_id;

  --if l_cnt =0 then
  if p_time_start is not null then
    return query
    select start_time, start_tm_id + l_cnt, cap.tr_capacity_id, is_require
    from ttb.timetable_test_pkg$get_start_tm(p_tt_variant_id,  p_tt_schedule_id,   p_tt_at_round_id, p_time_start, p_is_add_itv )
      join core.tr_capacity cap on 1=1 and cap.tr_capacity_id in (select tt_tr.tr_capacity_id
                                                                  from
                                                                    ttb.tt_schedule sch
                                                                    join ttb.tt_period p on p.tt_schedule_id = sch.tt_schedule_id
                                                                    join ttb.tt_tr_period tr_per on tr_per.tt_period_id = p.tt_period_id
                                                                    join ttb.tt_tr on tt_tr.tt_tr_id = tr_per.tt_tr_id
                                                                  where  sch.tt_variant_id = p_tt_variant_id
                                                                         --смотрим на предков
                                                                         and sch.tt_schedule_id = ANY(  ttb.tt_schedule_pkg$get_schedule_parent_list(p_tt_schedule_id))
                                                                  group by tt_tr.tr_capacity_id)
    where start_time is not null;
  elsif p_is_last then
    --получаем последнее время выполнения последнего действия
    if p_tt_tr_id is null then
      return query
      select max(cb.time_end) as start_time,  cb.tmp_id , cb.tr_capacity_id, cb.is_require
      from  tt_cube cb
        join ttb.tt_period p on p.tt_schedule_id = cb.tt_schedule_id
      where cb.is_finish_stop
            and cb.tt_schedule_id = p_tt_schedule_id
            and cb.stop_item_id   = p_stop_item_id
            and cb.one_id = p_one_id
            --для рейсов период проверяем, а для перерывов нет, они же в конец рейса попадают, могут выйти за пределы периода
            and (p_tt_at_round_id is null or cb.time_end between p.time_begin and p.time_end)
      group by cb.tt_schedule_id, cb.tr_capacity_id, cb.tmp_id, cb.is_require;
    else
      return query
      select cb.time_end as start_time, cb.tmp_id , cb.tr_capacity_id, cb.is_require
      from  tt_cube cb
        join ttb.tt_tr tt_tr on tt_tr.tt_tr_id = p_tt_tr_id
      where cb.is_finish_stop
            and cb.tt_tr_id = tt_tr.tt_tr_id
            and cb.tr_capacity_id = tt_tr.tr_capacity_id
      order by cb.time_end desc, cb.time_begin desc
      limit 1;
    end if;
  else
    --получаем первое время выполнения первого действия  p_one_id не учитывается! проверку на период пока не делаем
    if p_tt_tr_id is null then
      return query
      select min(cb.time_begin) as start_time,  cb.tmp_id , cb.tr_capacity_id, cb.is_require
      from  tt_cube cb
      where cb.order_num =1
            and cb.tt_schedule_id = p_tt_schedule_id
            and cb.stop_item_id   = p_stop_item_id
      group by cb.tt_schedule_id, cb.tr_capacity_id, cb.tmp_id, cb.is_require;
    else
      return query
      select cb.time_begin as start_time,  cb.tmp_id , cb.tr_capacity_id, cb.is_require
      from  tt_cube cb
      where cb.order_num =1
            and cb.tt_tr_id = p_tt_tr_id
      order by cb.time_end, cb.time_begin
      limit 1
      ;
    end if;
  end if;

end;
$body$
language plpgsql volatile
cost 100;




/***************************************************************************
--выпуск всех рейсов на каждое время
-- p_is_last = true выполнить рейс начиная с последнего времени последнего действия по этому варианту расписания
-- p_is_last = false выполнить рейс до первого времени первого действия
-- p_is_start начать с первого времени выпуска или добавить к предыдущему действию
--p_stop_item_id
***************************************************************************/

create or replace function ttb."timetable_test_pkg$get_round_tm_full"(in p_tt_variant_id numeric,
                                                                 in p_tt_schedule_id numeric,
                                                                 in p_tt_at_round_id numeric,
                                                                 in p_is_last boolean default true,
                                                                 in p_tt_tr_id numeric default null,
                                                                 in p_is_start boolean default true,
                                                                 in p_stop_item_id numeric default null,
                                                                 in p_one_id numeric default null,
                                                                 in p_is_add_itv boolean default false
)
  returns TABLE(
    fstop_item2round_id bigint,
    fround_id integer,
    fstop_item_id integer,
    ftr_capacity_id smallint,
    ftt_variant_id integer,
    faction_type_id smallint,
    ftime_begin timestamp ,
    ftime_end timestamp ,
    forder_num smallint,
    fdur_from_beg integer, -- Продолжительность от начала рейса
    fdur_round integer, -- Продолжительность рейса
    fid integer,
    fis_require boolean,
    fis_finish_stop boolean,
    ftt_tr_id integer,
    fdr_shift_id smallint,
    ftt_driver_id integer
  )
as
$body$
declare
  l_tm timestamp;
begin
  --если это начало
  if p_is_start then
    /*l_tm  := ttb.timetable_test_pkg$get_start_time( p_tt_at_round_id);
    if l_tm is null then
      select min(p.time_begin) into l_tm
      from ttb.tt_schedule sch
        join ttb.tt_period p on p.tt_schedule_id = sch.tt_schedule_id
      where sch.tt_schedule_id = p_tt_schedule_id;
    end if;*/
    l_tm:= ttb.timetable_test_pkg$get_start_time4sch (p_tt_schedule_id,  p_tt_at_round_id, 'START', p_tt_tr_id);
  end if;

  RAISE NOTICE 'get_round_tm_full ---------------l_tm= % p_tt_schedule_id =%, p_tt_at_round_id = %, p_stop_item_id =%, p_tt_tr_id = %, p_is_start =%', l_tm, p_tt_schedule_id , p_tt_at_round_id,p_stop_item_id , p_tt_tr_id, p_is_start;
  return query
  with adata as (
      select distinct
        t.fstop_item2round_id,
        t.fround_id,
        t.fstop_item_id,
        t.ftr_capacity_id,
        t.ftt_variant_id,
        t.faction_type_id,
        t.ftime_begin ,
        t.ftime_begin as time_end,
        t.forder_num,
        t.fdur_from_beg,
        t.fdur_round,
        dt.fid as tmp_id,
        dt.fis_require,
        max(cb.stop_item_id) over () as cb_stop_item_id,
        max(cb.dr_shift_id) over() as dr_shift_id,
        max(cb.tt_driver_id) over() as tt_driver_id,
        /*(select count(1)
         from rts.stop_item2round_type it2
           join rts.stop_item_type it on it.stop_item_type_id = it2.stop_item_type_id
         where t.forder_num>1
               and it2.stop_item2round_id = t.fstop_item2round_id
               and it.stop_type_id= rts.stop_type_pkg$finish()) <> 0 as is_finish_stop*/
        (select count(1)
         from rts.stop_item2round sir
         where  sir.stop_item2round_id = t.fstop_item2round_id
                and not sir.sign_deleted
                and sir.order_num =(select max(order_num) from rts.stop_item2round g where g.round_id = sir.round_id and not g.sign_deleted))>0 as is_finish_stop
      --(select count(1) from  tt_cube cb where cb.tt_variant_id = p_tt_variant_id and cb.tt_schedule_id  = p_tt_schedule_id ) as is_empty
      from  ttb.timetable_test_pkg$get_time_start_arr(p_tt_variant_id, p_tt_schedule_id,  p_is_last, l_tm , p_tt_tr_id, p_tt_at_round_id,  p_stop_item_id, p_one_id,p_is_add_itv) dt
        join  ttb.timetable_test_pkg$get_round_tm(p_tt_at_round_id, dt.ftr_capacity_id, dt.fstart_time, p_is_last) t on 1=1
        left join  tt_cube cb on case when p_is_last then cb.time_end else cb.time_begin end = dt.fstart_time
                                 and cb.tmp_id = dt.fid
                                 and cb.tr_capacity_id =  dt.ftr_capacity_id
                                 and cb.tt_schedule_id = p_tt_schedule_id
                                 and  case when p_is_last then cb.is_finish_stop else cb.order_num=1  end =true
                                 and cb.stop_item_id = t.fstop_item_id
                                 and (p_tt_tr_id is null or  cb.tt_tr_id = p_tt_tr_id)
                                 and (p_one_id is null or p_tt_tr_id is not null or cb.one_id = p_one_id)
                                 and cb.action_type_id != t.faction_type_id
        left join ttb.tt_tr ttr on ttr.tt_tr_id = cb.tt_tr_id
        left join ttb.tt_at_round atr on atr.tt_at_round_id = p_tt_at_round_id
      --если определена территория, то выполняем только рейсы этой территории
      where ((p_tt_tr_id is not null and (atr.depo2territory_id = ttr.depo2territory_id or atr.depo2territory_id is null))
             or dt.fstart_time <= least ((select max(p.time_end)  from  ttb.tt_period p   where p.tt_schedule_id =  p_tt_schedule_id)
      ,ttb.timetable_test_pkg$get_finish_time( p_tt_at_round_id)))

  )
  select
    adata.fstop_item2round_id,
    adata.fround_id,
    adata.fstop_item_id,
    adata.ftr_capacity_id,
    adata.ftt_variant_id,
    adata.faction_type_id,
    adata.ftime_begin ,
    adata.time_end,
    adata.forder_num,
    adata.fdur_from_beg,
    adata.fdur_round,
    adata.tmp_id,
    adata.fis_require,
    adata.is_finish_stop,
    p_tt_tr_id::int,
    adata.dr_shift_id::smallint,
    adata.tt_driver_id
  from adata
  --проверка на то чтобы рейс начинался с ОП, на которой закончено предыдущее действие
  where exists (select 1 from adata d where   d.fstop_item_id = d.cb_stop_item_id       and  d.fround_id=adata.fround_id)
        or p_is_start;

end;
$body$
language plpgsql volatile
cost 100;


------------------------------------------------------------------------------------------------------------------------

--DROP FUNCTION ttb."timetable_test_pkg$get_break_tm_full"(numeric,numeric,numeric,numeric,boolean,numeric,numeric,numeric,boolean)

/***************************************************************************
-- Проставляем перерывы для всех времен
-- p_is_last = true вперед с последнего времени последнего действия
-- p_is_last = false назад до первого времени первого действия
   p_tt_action_type_id numeric,  тип перерыва
   in p_tt_at_round_id numeric на каком рейсе все происходит
 in p_stop_item_id numeric,          оп, на которой будет перерыв
 in p_tt_at_round_id numeric default null,  рейс, на котором будет ОП (для формирования в обратную сторону)
 in p_is_last boolean default true,  направление
 in p_tt_tr_id                       виртуальное ТС
***************************************************************************/

create or replace function ttb."timetable_test_pkg$get_break_tm_full"(in p_tt_variant_id numeric,
                                                                 in p_tt_schedule_id numeric,
                                                                 in p_tt_action_type_id numeric,
                                                                 in p_stop_item_id numeric ,
                                                                 in p_is_last boolean default true,
                                                                 in p_tt_at_round_id numeric default null,
                                                                 in p_tt_tr_id numeric default null,
                                                                 in p_one_id numeric default null,
                                                                 in p_is_add_itv boolean default false

)
  returns TABLE(
    fstop_item2round_id bigint,
    fround_id integer,
    fstop_item_id integer,
    ftr_capacity_id smallint,
    ftt_variant_id integer,
    faction_type_id smallint,
    ftime_begin timestamp ,
    ftime_end timestamp ,
    forder_num smallint,
    fdur_from_beg integer, -- Продолжительность от начала рейса
    fdur_round integer, -- Продолжительность рейса
    fid integer,
    fis_require boolean,
    fis_finish_stop boolean,
    ftt_tr_id integer,
    fdr_shift_id smallint,
    ftt_driver_id integer
  )
as
$body$
declare
  l_action_type_id ttb.tt_action_type.action_type_id%type;
begin
  select tact.action_type_id into l_action_type_id
  from ttb.tt_action_type tact
  where tact.tt_action_type_id =  p_tt_action_type_id;
  RAISE NOTICE '% ----------------------get_break_tm_full---------- p_tt_schedule_id =%, p_tt_action_type_id = %,  p_stop_item_id=%, p_tt_at_round_id=%', clock_timestamp(), p_tt_schedule_id , p_tt_action_type_id,  p_stop_item_id, p_tt_at_round_id;
  --для переключения
  if l_action_type_id in (  ttb.action_type_pkg$switch()) then

    return query
    with adata as (
        select distinct
          cb.stop_item2round_id,
          cb.round_id,
          cb.stop_item_id::int,
          cb.tr_capacity_id,
          p_tt_variant_id::int as tt_variant_id,
          cb.time_end as time_begin ,
          cb.time_end as time_end,
          cb.order_num,
          ttb.timetable_test_pkg$get_break_dur(l_action_type_id::numeric,  p_tt_variant_id,p_tt_tr_id, cb.dr_shift_id::numeric) as action_dur,
          dt.fid as tmp_id,
          cb.is_require,
          cb.is_finish_stop,
          cb.dr_shift_id,
          cb.tt_driver_id
        /*тут специально функция без параметра P_start_time, для перерывов уже время рейсов должно быть*/
        from  ttb.timetable_test_pkg$get_time_start_arr(p_tt_variant_id, p_tt_schedule_id, p_is_last, null, p_tt_tr_id, null/*p_tt_at_round_id*/,  p_stop_item_id, p_one_id , p_is_add_itv) dt
          join  tt_cube cb on  case when p_is_last then cb.time_end else cb.time_begin end = dt.fstart_time
                               and  case when p_is_last then cb.is_finish_stop else cb.order_num=1  end = true
                               and cb.tmp_id = dt.fid
                               and cb.tr_capacity_id =  dt.ftr_capacity_id
                               and cb.tt_tr_id = p_tt_tr_id
                               and cb.action_type_id !=l_action_type_id

          --          join unnest(ttb.tt_schedule_pkg$get_schedule_list(p_tt_schedule_id)) sch on sch = cb.tt_schedule_id
          join ttb.tt_tr on tt_tr.tt_tr_id = cb.tt_tr_id
          join ttb.tt_action_type tact on  tact.tt_action_type_id =  p_tt_action_type_id
          join ttb.tt_at_stop at_stp on at_stp.tt_action_type_id = tact.tt_action_type_id
          join rts.stop_item2round sir on sir.stop_item2round_id = at_stp.stop_item2round_id
        where  cb.stop_item_id = sir.stop_item_id
               --перерыв только на ОП, опред для территории ТС или терр не определена
               and (at_stp.depo2territory_id = tt_tr.depo2territory_id or at_stp.depo2territory_id is null)
    )
    select

      stop_item2round_id,
      round_id,
      stop_item_id::int,
      tr_capacity_id,
      tt_variant_id,
      l_action_type_id as action_type_id,
      time_begin ,
      time_begin  as time_end,  ---time_end + make_interval(secs:=action_dur) as time_end,
      order_num,
      action_dur,
      action_dur,
      tmp_id,
      is_require,
      is_finish_stop,
      p_tt_tr_id::int,
      dr_shift_id,
      tt_driver_id
    from adata
    ;

  end if;

  --для обеда и пересменки
  if l_action_type_id in ( ttb.action_type_pkg$dinner()
    , ttb.action_type_pkg$change_driver()
    , ttb.action_type_pkg$maintenance()
  ) then

    return query
    with adata as (
        select distinct
          cb.stop_item2round_id,
          cb.round_id,
          cb.stop_item_id::int,
          cb.tr_capacity_id,
          cb.tt_variant_id,
          cb.time_end as time_begin ,
          cb.time_end as time_end,
          cb.order_num,
          ttb.timetable_test_pkg$get_break_dur(l_action_type_id::numeric,  p_tt_variant_id,p_tt_tr_id, cb.dr_shift_id::numeric) as action_dur,
          dt.fid as tmp_id,
          cb.is_require,
          cb.is_finish_stop,
          cb.dr_shift_id,
          cb.tt_driver_id
        /*тут специально функция без параметра P_start_time, для перерывов уже время рейсов должно быть*/
        from  ttb.timetable_test_pkg$get_time_start_arr(p_tt_variant_id, p_tt_schedule_id, p_is_last, null, p_tt_tr_id, null/*p_tt_at_round_id*/,  p_stop_item_id, p_one_id , p_is_add_itv) dt
          join  tt_cube cb on  case when p_is_last then cb.time_end else cb.time_begin end = dt.fstart_time
                               and  case when p_is_last then cb.is_finish_stop else cb.order_num=1  end = true
                               and cb.tmp_id = dt.fid
                               and cb.tr_capacity_id =  dt.ftr_capacity_id
                               and cb.tt_tr_id = p_tt_tr_id
                               and cb.action_type_id !=l_action_type_id
          --and cb.stop_item_id = p_stop_item_id
          join unnest(ttb.tt_schedule_pkg$get_schedule_list(p_tt_schedule_id)) sch on sch = cb.tt_schedule_id
          join ttb.tt_tr on tt_tr.tt_tr_id = cb.tt_tr_id
          join ttb.tt_action_type tact on  tact.tt_action_type_id =  p_tt_action_type_id
          join ttb.tt_at_stop at_stp on at_stp.tt_action_type_id = tact.tt_action_type_id and cb.stop_item2round_id = at_stp.stop_item2round_id
                                        --перерыв только на ОП, опред для территории ТС или терр не определена
                                        and (at_stp.depo2territory_id = tt_tr.depo2territory_id or at_stp.depo2territory_id is null)
    )
    select

      stop_item2round_id,
      round_id,
      stop_item_id::int,
      tr_capacity_id,
      tt_variant_id,
      l_action_type_id as action_type_id,
      time_begin ,
      time_end + make_interval(secs:=action_dur) as time_end,
      order_num,
      action_dur,
      action_dur,
      tmp_id,
      is_require,
      is_finish_stop,
      p_tt_tr_id::int,
      dr_shift_id,
      tt_driver_id
    from adata
    ;

  end if;

  --для межрейсового отстоя
  if l_action_type_id = ttb.action_type_pkg$bn_round_break() then
    RAISE NOTICE ' --------';
    return query
    with  is_last_T as (
        select
          cb.stop_item2round_id,
          cb.round_id,
          cb.stop_item_id,
          cb.tr_capacity_id,
          cb.tt_variant_id,
          tact.action_type_id,
          cb.time_end as time_begin ,
          cb.time_end ,
          cb.order_num,
          --Значение “по умолчанию” минимальной межрейсовой стоянки для новых маршрутов, сек, если  0
          --case coalesce(nr.min_inter_round_stop_dur,0)  when 0 then asd.get_constant('MIN_INTER_RACE_STOP')::int   else nr.min_inter_round_stop_dur end  as min_inter_round_stop_dur,
          coalesce(nr.min_inter_round_stop_dur,0)  as min_inter_round_stop_dur,
          coalesce(nr.reg_inter_round_stop_dur, 0) as reg_inter_round_stop_dur,
          dt.fid as tmp_id,
          cb.is_require,
          cb.is_finish_stop,
          cb.dr_shift_id,
          vma.is_bn_round_break as is_bn,
          cb.tt_driver_id
        /*тут специально функция без параметра P_start_time, для перерывов уже время рейсов должно быть*/
        from  ttb.timetable_test_pkg$get_time_start_arr(p_tt_variant_id, p_tt_schedule_id, p_is_last, null, p_tt_tr_id, null/*p_tt_at_round_id*/,  p_stop_item_id , p_one_id, p_is_add_itv) dt
          join ttb.tt_action_type tact on  tact.tt_action_type_id =  p_tt_action_type_id
          join  tt_cube cb on  cb.time_end = dt.fstart_time
                               and cb.is_finish_stop
                               and cb.tmp_id = dt.fid
                               and cb.tr_capacity_id =  dt.ftr_capacity_id
                               and cb.tt_schedule_id = p_tt_schedule_id
                               and (p_tt_tr_id is null or  cb.tt_tr_id = p_tt_tr_id)
                               and (p_one_id is null or p_tt_tr_id is not null or  cb.one_id = p_one_id)
          --and (p_stop_item_id is null or cb.stop_item_id = p_stop_item_id)
          join ttb.vm_action_type vma on vma.action_type_id = cb.action_type_id
          join ttb.tt_variant v on v.tt_variant_id = p_tt_variant_id
          join ttb.norm_round nr on nr.tr_type_id =cb.tr_type_id
                                    and nr.tr_capacity_id = cb.tr_capacity_id
                                    and nr.round_id =  cb.round_id
                                    and nr.stop_item_id = cb.stop_item_id
                                    and nr.norm_id = v.norm_id
                                    and nr.hour_from = date_trunc('hour',dt.fstart_time)::time
        where p_is_last
    )
      ,
        is_last_F as (

          select
            sir.stop_item2round_id,
            sir.round_id,
            sir.stop_item_id,
            cb.tr_capacity_id,
            cb.tt_variant_id,
            tact.action_type_id,
            cb.time_end as time_begin ,
            cb.time_end ,
            cb.order_num,
            --Значение “по умолчанию” минимальной межрейсовой стоянки для новых маршрутов, сек, если  0
            --case coalesce(nr.min_inter_round_stop_dur,0)  when 0 then asd.get_constant('MIN_INTER_RACE_STOP')::int   else nr.min_inter_round_stop_dur end  as min_inter_round_stop_dur,
            coalesce(nr.min_inter_round_stop_dur,0)  as min_inter_round_stop_dur,
            coalesce(nr.reg_inter_round_stop_dur, 0) as reg_inter_round_stop_dur,
            dt.fid as tmp_id,
            cb.is_require,
            cb.is_finish_stop,
            cb.dr_shift_id,
            (cb.action_type_id = tact.action_type_id ) as is_bn,
            cb.tt_driver_id

          /*тут специально функция без параметра P_start_time, для перерывов уже время рейсов должно быть*/
          from  ttb.timetable_test_pkg$get_time_start_arr(p_tt_variant_id, p_tt_schedule_id, p_is_last, null, p_tt_tr_id, null/*p_tt_at_round_id*/, p_stop_item_id,null,  p_is_add_itv ) dt
            join ttb.tt_action_type tact on  tact.tt_action_type_id =  p_tt_action_type_id
            join ttb.tt_at_stop tas on tas.tt_action_type_id =tact.tt_action_type_id
            join tt_cube cb on cb.time_begin  = dt.fstart_time
                               and  cb.order_num=1
                               and cb.tmp_id = dt.fid
                               and cb.tr_capacity_id =  dt.ftr_capacity_id
                               -- and cb.tt_schedule_id = p_tt_schedule_id
                               and ((p_tt_tr_id is null and cb.tt_schedule_id = p_tt_schedule_id ) or  cb.tt_tr_id = p_tt_tr_id)
                               and cb.action_type_id != tact.action_type_id
            join ttb.tt_at_round at_rnd on at_rnd.tt_at_round_id = p_tt_at_round_id
            join rts.stop_item2round sir on sir.round_id = at_rnd.round_id and sir.stop_item2round_id = tas.stop_item2round_id and not sir.sign_deleted
            join ttb.tt_variant v on v.tt_variant_id = p_tt_variant_id
            join ttb.norm_round nr on nr.tr_type_id =cb.tr_type_id
                                      and nr.tr_capacity_id = cb.tr_capacity_id
                                      and nr.round_id =  sir.round_id
                                      and nr.stop_item_id = sir.stop_item_id
                                      and nr.norm_id = v.norm_id
                                      and nr.hour_from = date_trunc('hour', dt.fstart_time)::time
          where not p_is_last
          order by sir.order_num desc
          limit 1
      )
      ,
        adata as (
        select * from  is_last_T where not is_bn
        union ALL
        select * from  is_last_F where not is_bn
      )
    --добавляем базовый мо
    select
      stop_item2round_id,
      round_id,
      stop_item_id::int,
      tr_capacity_id,
      tt_variant_id,
      ttb.action_type_pkg$bn_round_break_base() as action_type_id,
      time_begin + case when  p_is_last then 0 else  (min_inter_round_stop_dur+reg_inter_round_stop_dur)*-1 end * interval '1 second' as time_begin,
      time_end + case when  p_is_last then min_inter_round_stop_dur else reg_inter_round_stop_dur*-1 end * interval '1 second' as time_end,

      order_num,
      min_inter_round_stop_dur as dur_from_beg,
      min_inter_round_stop_dur as dur_round,
      tmp_id,
      is_require,
      is_finish_stop,
      p_tt_tr_id::int,
      dr_shift_id,
      tt_driver_id
    from adata
    union all
    --добавляем регулировочный мо
    select
      stop_item2round_id,
      round_id,
      stop_item_id::int,
      tr_capacity_id,
      tt_variant_id,
      ttb.action_type_pkg$bn_round_break_reg() as action_type_id,
      time_begin + case  when  p_is_last  then min_inter_round_stop_dur else    reg_inter_round_stop_dur*-1 end * interval '1 second' as time_begin,
      time_end + case when p_is_last  then min_inter_round_stop_dur + reg_inter_round_stop_dur else 0 end * interval '1 second' as time_end,
      order_num,
      reg_inter_round_stop_dur as dur_from_beg,
      reg_inter_round_stop_dur as dur_round,
      tmp_id,
      is_require,
      is_finish_stop,
      p_tt_tr_id::int,
      dr_shift_id,
      tt_driver_id
    from adata
    where reg_inter_round_stop_dur !=0
    ;
  end if;



end;
$body$
language plpgsql volatile
cost 100;


------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
--обработка последовательности действий внутри группы
   ,in p_is_last boolean default true   направление
  ,in p_tt_tr_id numeric default null   ТС
  ,in p_stop_item_id  numeric default null   ОП который нужно проверять (для p_is_last =true проверим первую, иначе последнюю)
   ,p_one_id -действие родитель
)
***************************************************************************/

create or replace function ttb."timetable_test_pkg$make_schedule_item"(in p_tt_schedule_item_id numeric
  ,in p_is_last boolean default true
  ,in p_tt_tr_id numeric default null
  ,in p_stop_item_id  numeric default null
  ,in p_one_id  numeric default null
  ,in p_is_add_itv boolean default false
)
  returns text
as $function$
declare
  l_it record;
  l_tt_schedule_item_id numeric;
  l_one_id numeric;
  l_cnt numeric;
begin

  RAISE NOTICE 'p_tt_schedule_item_id =% , p_stop_item_id =% ,  p_is_last=%, p_tt_tr_id= % , p_one_id = % -------------------------------make_schedule_item', p_tt_schedule_item_id, p_stop_item_id,  p_is_last, p_tt_tr_id, p_one_id;

  select coalesce (max(one_id) ,1) + 1  into l_one_id
  from tt_cube cb
    join ttb.tt_schedule_item it on it.tt_schedule_id = cb.tt_schedule_id
  where it.tt_schedule_item_id =  p_tt_schedule_item_id
        and (p_tt_tr_id is null or cb.tt_tr_id = p_tt_tr_id);


  for l_it in
  (with adata as (
      select
        v.action_type_id
        ,v.tt_action_type_id
        ,v.tt_variant_id
        ,v.tt_action_type2group_id
        ,v.tt_schedule_id
        ,v.tt_schedule_item_id
        ,v.tr_type_id
        ,v.order_num_action
        ,v.order_num
        ,v.sp_b_id
        ,v.sir_b_id
        ,v.sp_e_id
        ,v.sir_e_id
        ,v.ref_group_kind_id
        --,ttb.timetable_test_pkg$get_start_time4sch (v.tt_schedule_id,  v.tt_at_round_id, 'START', p_tt_tr_id)
        --,ttb.timetable_test_pkg$get_start_time( v.tt_at_round_id) as start_time
        ,v.round_id
        ,v.tt_at_round_id
        ,v.sp_e_id  as sp_id  --оп, на котором заканчиваем
        ,v.parent_schedule_id
        --,case when p_is_last then v.sp_b_id  else v.sp_e_id end as sp_id
        , first_value (case when p_is_last then v.sp_b_id else v.sp_e_id end) over (order by (case when p_is_last then 1 else -1 end *  v.order_num * (round_id>0)::int) nulls last ) as spit_id
      from
        ttb.v_tt_schedule_item_det  v
      where v.tt_schedule_item_id = p_tt_schedule_item_id
    --   and tt_action_type2group_id in ( 11986 , 11987 )
  )
  select adata.*
    ,lag(round_id) over (partition by tt_schedule_item_id  order by order_num  ) as prev_round_id
    ,lag(tt_at_round_id) over (partition by tt_schedule_item_id  order by order_num  ) as prev_tt_at_round_id
    --,coalesce(sp_id,lag(sp_id) over (partition by tt_schedule_item_id  order by order_num  )) as stop_item_id
    , case when p_is_last and p_tt_tr_id is not null and order_num>1 and round_id is null then null
      else coalesce(sp_id,lag(sp_id) over (partition by tt_schedule_item_id  order by order_num  )) end as stop_item_id
    , vma.is_round
    ,vma.is_break
  from adata
    join ttb.vm_action_type vma on vma.action_type_id = adata.action_type_id
  where    p_stop_item_id is null
           or adata.spit_id = p_stop_item_id
           or round_id is null
  /* or (not p_is_last  and p_stop_item_id=  sp_e_id ) --для ПА
   or (p_is_last  and   p_stop_item_id=  sp_b_id )  --для АП
   or (p_is_last  and exists( select 1 from adata where  p_stop_item_id =  sp_b_id and  order_num=1))  --для групп
   or round_id is null*/

  order by (case when p_is_last then 1 else -1 end) *  order_num )                  loop


    if  l_it.ref_group_kind_id != ttb.ref_group_kind_pkg$main_action() then
      l_one_id := coalesce(p_one_id, 1);
      --RAISE NOTICE 'p_one_id = % ----l_one_id = %---------------------------make_schedule_item', p_one_id ,l_one_id;
    end if;


    RAISE NOTICE ' l_it.tt_at_round_id =%, l_one_id = %, p_one_id = %,p_is_last = % --------------------make_schedule_item inside--------------------------------------',  l_it.tt_at_round_id, l_one_id , p_one_id, p_is_last;
    if  l_it.is_round then
      raise notice '********************** make_schedule_item round ---------------' ;
      with ins as (
        insert into  tt_cube
        (
          stop_item2round_id,
          round_id,
          stop_item_id,
          tr_capacity_id,
          tt_variant_id,
          action_type_id,
          time_begin,
          time_end,
          order_num,
          dur_from_beg,
          dur_round,
          tmp_id,
          is_require,
          is_finish_stop ,
          tt_tr_id,

          tt_action_type2group_id,
          tt_schedule_id,
          tt_schedule_item_id,
          tr_type_id ,
          tt_at_round_id,
          order_num_action,
          dr_shift_id,
          tt_driver_id,

          one_id,
          parent_one_id,
          is_intvl
        )

          select distinct
            fstop_item2round_id,
            fround_id,
            fstop_item_id,
            ftr_capacity_id,
            ftt_variant_id,
            faction_type_id,
            ftime_begin,
            ftime_end,
            forder_num,
            fdur_from_beg,
            fdur_round,
            fid,
            fis_require,
            fis_finish_stop,
            ftt_tr_id,
            l_it.tt_action_type2group_id,
            l_it.tt_schedule_id,
            l_it.tt_schedule_item_id,
            l_it.tr_type_id,
            l_it.tt_at_round_id,
            l_it.order_num_action,
            fdr_shift_id,
            ftt_driver_id,

            l_one_id,
            case l_one_id when p_one_id then null else p_one_id end
            ,not p_is_add_itv
          from
              ttb.timetable_test_pkg$get_round_tm_full( p_tt_variant_id  => l_it.tt_variant_id,
                                                   p_tt_schedule_id => l_it.tt_schedule_id,
                                                   p_tt_at_round_id => l_it.tt_at_round_id,
                                                   p_is_last  => p_is_last,
                                                   p_tt_tr_id => p_tt_tr_id,
                                                   p_is_start  => (p_stop_item_id is null
                                                                   and l_it.ref_group_kind_id = ttb.ref_group_kind_pkg$main_action() --основное действие
                                                   ),
                                                   p_stop_item_id =>case when l_it.order_num = 1 and p_tt_tr_id is null
                                                     then coalesce(p_stop_item_id,l_it.stop_item_id)
                                                                    else case when  p_is_last then l_it.sp_b_id else l_it.stop_item_id end end,
                                                   p_one_id => p_one_id,
                                                   p_is_add_itv => p_is_add_itv
              )
        ON CONFLICT ON CONSTRAINT  uq_tt_cube DO UPDATE SET tt_tr_id = EXCLUDED.tt_tr_id
        returning *
      )
      select count(1) into l_cnt from ins

      ;
      RAISE NOTICE ' insert row round l_cnt = % ',  l_cnt;

    elsif l_it.is_break then
      raise notice 'создать действие перерыв get_break_tm_full(l_it.tt_variant_id,l_it.tt_schedule_id,  l_it.tt_action_type_id, l_it.prev_round_id, p_is_last, p_tt_tr_id) l_it(%)' , l_it;
      /*RAISE NOTICE ' p_tt_variant_id  => %,
                                                   p_tt_schedule_id =>%,
                                                   p_tt_action_type_id =>%,
                                                   p_stop_item_id => %,
                                                   p_is_last  => %,
                                                   p_tt_tr_id => %,
                                                   p_one_id => %',
       l_it.tt_variant_id,
       l_it.tt_schedule_id,
       l_it.tt_action_type_id,
       l_it.stop_item_id,
       p_is_last,
       p_tt_tr_id,
       l_one_id;

*/
      with ins as (
        insert into  tt_cube
        (
          stop_item2round_id,
          round_id,
          stop_item_id,
          tr_capacity_id,
          tt_variant_id,
          action_type_id,
          time_begin,
          time_end,
          order_num,
          dur_from_beg,
          dur_round,
          tmp_id,
          is_require,
          is_finish_stop ,
          tt_tr_id,

          tt_action_type2group_id,
          tt_schedule_id,
          tt_schedule_item_id,
          tr_type_id ,
          tt_at_round_id,
          order_num_action,
          dr_shift_id,
          tt_driver_id,

          one_id,
          parent_one_id,
          is_intvl
        )

          select
            fstop_item2round_id,
            fround_id,
            fstop_item_id,
            ftr_capacity_id,
            ftt_variant_id,
            faction_type_id,
            ftime_begin,
            ftime_end ,
            forder_num,
            fdur_from_beg,
            fdur_round,
            fid,
            fis_require,
            fis_finish_stop,
            ftt_tr_id,
            l_it.tt_action_type2group_id,
            l_it.tt_schedule_id,
            l_it.tt_schedule_item_id,
            l_it.tr_type_id,
            l_it.tt_at_round_id,
            l_it.order_num_action,
            fdr_shift_id,
            ftt_driver_id,
            l_one_id,
            case l_one_id when p_one_id then null else p_one_id end,
            not p_is_add_itv
          from
              ttb.timetable_test_pkg$get_break_tm_full(p_tt_variant_id  => l_it.tt_variant_id,
                                                  p_tt_schedule_id =>l_it.tt_schedule_id,
                                                  p_tt_action_type_id =>l_it.tt_action_type_id,
                                                  p_stop_item_id => l_it.stop_item_id,
                                                  p_is_last  => p_is_last,
                                                  p_tt_at_round_id => l_it.prev_tt_at_round_id,
                                                  p_tt_tr_id => p_tt_tr_id,
                                                  p_one_id => l_one_id,
                                                  p_is_add_itv => p_is_add_itv
              )
            join ttb.vm_action_type vma on vma.action_type_id = faction_type_id
          /*если это не пункт выравнивания, то нечего и выравнивать!
            не записываем регулировочное время в межрейс*/
          --where not ((l_one_id != p_one_id or l_it.parent_schedule_id is not null) and faction_type_id = ttb.action_type_pkg$bn_round_break_reg())
          where not ( faction_type_id = ttb.action_type_pkg$bn_round_break_reg() and
                      (select count(1) from  ttb.timetable_test_pkg$get_stop_interval_alignment(l_it.tt_variant_id) where  fstop_item_id=l_it.stop_item_id) =0)
        ON CONFLICT ON CONSTRAINT  uq_tt_cube DO NOTHING
        returning *
        --where ftime_begin < ftime_end
      )
      select count(1) into l_cnt from ins
      ;

      RAISE NOTICE ' l_cnt = % ',  l_cnt;
    end if;


  end loop;

  raise notice 'НЕЛЬЗЯ ВЫРАВНИВАНИТЬ? l_it.stop_item_id = %  , l_it.prev_round_id=%, l_cnt = % ', l_it.stop_item_id, l_it.prev_round_id, l_cnt ;
  if   l_cnt>0 and
       l_it.ref_group_kind_id = ttb.ref_group_kind_pkg$main_action() --основное действие
       and p_tt_tr_id is null
       --and not p_is_add_itv
       and  (select count(1) from ttb.timetable_test_pkg$get_stop_interval_alignment(l_it.tt_variant_id)
  where fstop_item_id = l_it.stop_item_id and fround_id = l_it.prev_round_id ) =0 then

    --смотрим на периоды, чтобы не уйти в бесконечность
    with d as (
        select min(cb.time_end) tm
        from  tt_cube cb
        where cb.is_finish_stop
              and cb.tt_schedule_id = l_it.tt_schedule_id
              and cb.one_id = l_one_id
    )
    select count(1) into l_cnt
    from ttb.tt_period p
      , d
    where p.tt_schedule_id =l_it.tt_schedule_id
          and  d.tm between p.time_begin and p.time_end ;

    raise notice 'Время в периоде ! l_cnt = % ', l_cnt ;
    if l_cnt >0 then
      /*если в основных действиях, конечный пункт НЕ является пунктом выравнивания
        создаем рейсы, которые нужно обязательно выполнять*/
      select sch_it.tt_schedule_item_id into l_tt_schedule_item_id
      from  ttb.tt_schedule_item sch_it
        join ttb.tt_action_group act_gr on act_gr.tt_action_group_id = sch_it.tt_action_group_id
        join ttb.tt_action_type2group act2gr on act2gr.tt_action_group_id = act_gr.tt_action_group_id
        join ttb.tt_action_type tact on tact.tt_action_type_id = act2gr.tt_action_type_id
        join ttb.tt_at_round at_rnd on at_rnd.tt_action_type_id = tact.tt_action_type_id and not sign_deleted
        join rts.round rnd on rnd.round_id = at_rnd.round_id  and not rnd.sign_deleted
        join rts.stop_item2round sir on sir.round_id     = rnd.round_id
                                        and sir.order_num    =  case when p_is_last then 1 else (select count(1) from  rts.stop_item2round sir where  sir.round_id = at_rnd.round_id)  end
                                        and sir.stop_item_id = l_it.stop_item_id  and not sir.sign_deleted
      where sch_it.tt_schedule_id = l_it.tt_schedule_id
            and  act_gr.ref_group_kind_id = ttb.ref_group_kind_pkg$main_action() --основное действие
      ;
      if l_tt_schedule_item_id is not null then
        --raise notice 'создаем рейс для пунктов, где нечего выравнивать';
        perform ttb.timetable_test_pkg$make_schedule_item(p_tt_schedule_item_id => l_tt_schedule_item_id
        ,p_is_last =>p_is_last
        ,p_tt_tr_id => p_tt_tr_id
        ,p_stop_item_id  => l_it.stop_item_id
        ,p_one_id => l_one_id
        ,p_is_add_itv=>  p_is_add_itv
        );
      end if;
    end if;
  end if;

  return '';
end;
$function$
language plpgsql volatile
cost 100;


------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
--Формирование варианта расписания для всех последовательностей


***************************************************************************/

create or replace function ttb."timetable_test_pkg$make_tt_variant"(in p_tt_variant_id numeric)
  returns void

as
$body$
declare
begin

  perform ttb.timetable_test_pkg$make_schedule(p_tt_variant_id);


  /*КОСТЫЛЬ!
   при сдвиге вправо из-за различной длительности рейсов по нормативам иногда не происходит "попадания" в последний рейс
   быстро решена задача путем последовательных приближений
  */
  --begin
  perform ttb.timetable_test_pkg$update_last_rnd(p_tt_variant_id);
  perform ttb.timetable_test_pkg$update_last_rnd(p_tt_variant_id);
  perform ttb.timetable_test_pkg$update_last_rnd(p_tt_variant_id);
  --exception when others then
  --        null;
  --  end;


  --!!!КОСТЫЛЬ
  RAISE NOTICE '--пробуем менять длинные обеды на отстой'  ;
  -- вызываем функцию замены обеда на отстой
  perform ttb.timetable_edit_pkg$change_action2pause (p_tt_variant_id);



end;
$body$
language plpgsql volatile
cost 100;

------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
--обработка последовательности действий
***************************************************************************/
create or replace function ttb."timetable_test_pkg$make_schedule"(in p_tt_variant_id numeric,
                                                             in p_tt_tr_id      numeric default null)
  returns void

as
$body$
declare
  l_sch record;
  l_it record;
  l_rec record;
  l_var record;
  l_cnt numeric;
begin
  --готовим табличку
  perform ttb.tt_cube_pkg$make_tt_cube(p_tt_variant_id);

  --будем формировать сразу все связанные расписания
  for l_var in (
    select vb.tt_variant_id, sch.tt_schedule_id
    from ttb.tt_switch_pkg$get_tt_variant_bound(p_tt_variant_id) vb
      join ttb.tt_schedule sch on sch.tt_variant_id = vb.tt_variant_id
    where sch.parent_schedule_id is null
  ) loop

    if l_var.tt_variant_id != p_tt_variant_id then
      perform ttb.tt_cube_pkg$append_tt_cube(l_var.tt_variant_id);
    end if;


    RAISE NOTICE '--- make_schedule l_var.tt_variant_id =%, l_var.tt_schedule_id = %, p_tt_tr_id = %', l_var.tt_variant_id , l_var.tt_schedule_id ,  p_tt_tr_id;

    --формируем все последовательности, связанные с основной и основную. Последовательности формируются независимо
    for l_sch in (
      select tt_schedule_id
      from unnest(ttb.tt_schedule_pkg$get_schedule_list(l_var.tt_schedule_id )) tt_schedule_id
    )loop

      select count(1) into l_cnt from tt_cube where  tt_schedule_id= l_sch.tt_schedule_id;


      --если ничего не пришло из БД, то формируем с нуля
      RAISE NOTICE '--- l_cnt=0 = %', l_cnt=0;
      if l_cnt=0  then

        /*формируем последовательно действия. Первое действие будет сформировано по дате первого выхода*/
        for l_it in
        (
          select tt_schedule_item_id
          from ttb.v_tt_schedule_item v
          where tt_schedule_id = l_sch.tt_schedule_id
                and ref_group_kind_id = ttb.ref_group_kind_pkg$main_action()  --основные действия
          order by tt_schedule_id, order_num
        )
        loop
          RAISE NOTICE '% 1. tt_schedule_item_id =%, --------------------------------------------------STEP NEW MATRIX', clock_timestamp(), l_it.tt_schedule_item_id;
          perform ttb.timetable_test_pkg$make_schedule_item(p_tt_schedule_item_id => l_it.tt_schedule_item_id);


          --if l_var.tt_schedule_id = l_sch.tt_schedule_id then
          RAISE NOTICE '% 1. tt_schedule_item_id =%, --------------------------------------------------STEP ADD MATRIX ROUND', clock_timestamp(), l_it.tt_schedule_item_id;

          perform ttb.timetable_test_pkg$make_schedule_item(p_tt_schedule_item_id => l_it.tt_schedule_item_id,
                                                       p_is_add_itv => true);
          --end if;
        end loop;  --l_it

      end if;
    end loop; --l_sch
  end loop;  --l_var

  analyze  tt_cube;

  RAISE NOTICE 'ttb.timetable_test_pkg$set_tr_all';

  /*Распределяем имеющиеся ТС,
    берем основную последовательность,  считаем что в зависимых последовательностях машин нет*/
  perform ttb.timetable_test_pkg$set_tr_all( p_tt_variant_id, p_tt_tr_id  );

  perform ttb.tt_cube_pkg$fill_db_table(p_tt_variant_id,  p_tt_tr_id );
  --пробуем добавить кругорейсы
  begin
    --костыль, 2 раза, чтобы в одном выходе обработать 2 смены
    perform ttb.tt_out_pkg$add_main_action2out (  p_tt_variant_id );
    perform ttb.tt_out_pkg$add_main_action2out (  p_tt_variant_id );

    exception when others then
    null;
  end;



end;
$body$
language plpgsql volatile
cost 100;

------------------------------------------------------------------------------------------------------------------------

--DROP FUNCTION ttb."timetable_test_pkg$get_current_data"(numeric,numeric)

/***************************************************************************
-- набор последних промежуточных значений
***************************************************************************/
create or replace function ttb."timetable_test_pkg$get_current_data" (in p_tt_tr_id       numeric,
                                                                 in p_dr_shift_id    numeric)
  returns TABLE(
    tr_dt_start     timestamp,
    tr_dt_begin     timestamp,
    tr_dt_end       timestamp,
    tr_dur_work     integer,
    tr_dur_line     integer,
    tr_dur_shift    integer,
    tr_stop_item_id integer,
    tr_one_id       integer,
    tr_tmp_id       integer,
    tr_dur_work_after_pause integer,
    tr_dt_shift_begin timestamp,
    tr_dt_shift_pause integer
  )
as
$body$
declare
begin
  return query
  with adata as (
      select
         rank() over (partition by t.tmp_id, t.order_num_action ORDER BY  t.time_end,  t.time_begin, t.action_type_id  ) r
        ,sum(t.dur_from_beg)  over (partition by t.tmp_id, t.order_num_action )  as dur_full
        ,min(t.time_begin)  over (partition by t.tmp_id, t.order_num_action ) as min_time_begin
        ,max(t.time_end)  over (partition by t.tmp_id, t.order_num_action ) as max_time_end
        ,case when vma.is_bn_round_break then ttb.action_type_pkg$bn_round_break() else t.action_type_id end as new_action_type_id
        ,sch_it.tt_action_group_id
        ,first_value(t.stop_item_id) over (order by t.time_end desc, t.order_num_action) as last_stop_item_id
        ,first_value(t.one_id) over (order by t.time_end desc, t.order_num_action) as last_one_id
        ,first_value(t.tmp_id) over (order by t.time_end desc, t.order_num_action) as last_tmp_id
        ,vma.is_production_round  as is_production_round
        ,vma.is_round             as is_round
        ,vma.is_bn_round_break    as is_bn_break
        ,vma.is_all_pause         as is_break
        ,vma.action_type_id = ttb.action_type_pkg$maintenance() as is_maintenance
        ,asd.get_constant('TIME_BREAK_IN_SHIFT')  as max_tm_break
        ,max(t.time_end)  FILTER ( WHERE is_all_pause or is_change_driver or is_сhanged_driver ) over (partition by t.tmp_id, t.order_num_action ) as max_time_pause
        ,t.*
        ,dr.is_сhanged_driver

      from   tt_cube t
        join ttb.vm_action_type vma on vma.action_type_id = t.action_type_id
        join ttb.tt_schedule_item sch_it on sch_it.tt_schedule_item_id  = t.tt_schedule_item_id
        join ttb.tt_tr ttr on ttr.tt_tr_id = t.tt_tr_id and ttr.tr_capacity_id = t.tr_capacity_id
        left join ttb.tt_driver dr on dr.tt_driver_id = t.tt_driver_id
      where t.tt_tr_id = p_tt_tr_id
      ORDER BY  t.time_end,  time_begin, action_type_id
  )
    , d as (
      select adata.*
        ,( (is_сhanged_driver!=lag(is_сhanged_driver) over (partition by dr_shift_id order by  time_begin, time_end) and is_сhanged_driver  ) or  is_break)::int as pause
      from adata where r=1
  )
    ,agg as (
      select
         min(min_time_begin) as ftr_dt_start
        ,min(max_time_end) FILTER ( WHERE is_round ) as ftr_dt_begin
        ,max(max_time_end) as ftr_dt_end
        ,coalesce(sum(case when is_break and not is_bn_break and dur_full>max_tm_break then 0/*max_tm_break*/ else dur_full end)  filter (where not is_maintenance and NOT is_сhanged_driver)  ,0)::int as ftr_dur_work
        ,coalesce(sum(dur_full) FILTER ( WHERE is_round),0)::int as ftr_dur_line
        ,coalesce(sum(case when is_break and not is_bn_break and dur_full>max_tm_break then 0/*max_tm_break*/ else dur_full end) FILTER ( WHERE not is_maintenance and NOT is_сhanged_driver and dr_shift_id = p_dr_shift_id),0)::int as ftr_dur_shift
        ,last_stop_item_id as flast_stop_item_id
        ,last_one_id as flast_one_id
        ,last_tmp_id as flast_tmp_id
        ,coalesce(extract(epoch from max(max_time_end)  - max(max_time_pause)  ),0 )::int as ftr_dur_work_after_pause  --отработка после точки обеда или пересменки
        -- ,sum(case when is_break and not is_bn_break and dur_full>max_tm_break then 0/*max_tm_break*/ else dur_full end)::int as tr_dur_work
        ,min(min_time_begin) FILTER ( WHERE dr_shift_id = p_dr_shift_id) ftr_dt_shift_begin
        ,sum(pause) FILTER ( WHERE is_production_round and  dr_shift_id = p_dr_shift_id)::int as ftr_dt_shift_pause
        ,sum(dur_full) FILTER (WHERE is_maintenance ) as fmaintenance_dur
        ,max(max_time_end) FILTER ( WHERE is_maintenance) as fmaintenance_dt_end
        ,max(max_time_end) FILTER ( WHERE is_break) as flast_break_dt_end
      from d
      group by last_stop_item_id, last_one_id ,last_tmp_id
  )

  select
    ftr_dt_start::timestamp
    ,ftr_dt_begin::timestamp
    ,ftr_dt_end::timestamp
    ,ftr_dur_work::int
    ,ftr_dur_line::int
    ,ftr_dur_shift::int
    ,flast_stop_item_id::int
    ,flast_one_id::int
    ,flast_tmp_id::int
    ,(ftr_dur_work_after_pause - case when fmaintenance_dt_end>flast_break_dt_end then fmaintenance_dur else 0 end)::int as tr_dur_work_after_pause--отработка после точки обеда или пересменки
    ,ftr_dt_shift_begin::timestamp
    ,ftr_dt_shift_pause::int
  from agg;

end;
$body$
language plpgsql volatile
cost 100;

------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- набор доступных ТС
***************************************************************************/
create or replace function ttb."timetable_test_pkg$set_tr_all"(in p_tt_variant_id numeric,
                                                          in p_tt_tr_id numeric default null)
  returns void
as
$body$
declare
  l_all_tr record;
begin
  RAISE NOTICE 'Верхний уровень timetable_test_pkg$set_tr_all ';
  for l_all_tr in
  (
    with adata as (
        select  ttp.tt_tr_id, ttp.tt_tr_order_num, sch.tt_schedule_id , sch.tt_variant_id, is_own
        from ttb.tt_switch_pkg$get_tt_variant_bound( p_tt_variant_id ) vb
          join ttb.tt_schedule sch on sch.tt_variant_id = vb.tt_variant_id and sch.parent_schedule_id is null
          join ttb.tt_period p on p.tt_schedule_id = sch.tt_schedule_id
          join ttb.tt_tr_period ttp on ttp.tt_period_id = p.tt_period_id
        where  (p_tt_tr_id is null  or ttp.tt_tr_id = p_tt_tr_id )
        group by ttp.tt_tr_id, ttp.tt_tr_order_num,  sch.tt_schedule_id , sch.tt_variant_id,is_own
    )

    select *
    from adata d_own
      left join adata d_other on d_other.tt_tr_id = d_own.tt_tr_id and not d_other.is_own
    where d_own.is_own
    order by  d_other.is_own nulls last, d_own.tt_tr_order_num

  )    loop

    perform ttb.timetable_test_pkg$set_tr(p_tt_variant_id => l_all_tr.tt_variant_id,
                                     p_tt_tr_id => l_all_tr.tt_tr_id);
  end loop;

end;
$body$
language plpgsql volatile
cost 100;


create or replace function ttb."timetable_test_pkg$set_tr"(in p_tt_variant_id numeric,
                                                      in p_tt_tr_id numeric)
  returns void
as
$body$
declare
  l_tr record;
  l_it record;
  l_ch_dr record;
  l_shift_tm  record;
  l_tr_dt_start  timestamp;
  l_tr_dt_begin  timestamp;
  l_tr_dt_end    timestamp;
  l_tr_dur_shift integer;
  l_tr_dur_line  integer;
  l_tr_dur_work  integer;
  l_tr_dur_work_after_pause integer;
  l_dr_shift_id  smallint;
  l_dr_shift_id_switch  smallint;
  l_tr_stop_item_id integer;
  l_tr_one_id    integer;
  l_tr_tmp_id    integer;
  l_cnt_uprow integer;
  l_break_dur    integer;
  l_cnt_change_driver int;
  l_cnt int;
  l_tt_driver_id_main numeric;
  l_tt_driver_id_add numeric;
  l_finish_min numeric;
  l_finish_max numeric;
  l_tr_dt_shift_begin timestamp;
  l_tr_dt_shift_pause integer;

begin
  select count(1) into l_cnt_change_driver   from ttb.tt_driver td  where td.tt_variant_id = p_tt_variant_id     and td.is_сhanged_driver;
  select tt_driver_id into l_tt_driver_id_add   from ttb.tt_driver td  where td.tt_variant_id = p_tt_variant_id     and td.is_сhanged_driver limit 1;


  RAISE NOTICE ' --------STEP 0----------  tt_tr.tt_tr_id =% ', p_tt_tr_id;

  for l_tr in
  (
    with calc_sh as (
        select
          v.tt_schedule_id
          ,v.tt_variant_id
          ,(select ttb.rv_tune_pkg$get_break_round_time(ttv.route_variant_id)   from ttb.tt_variant ttv where ttv.tt_variant_id = v.tt_variant_id) as break_round_time
          ,greatest(v.time_begin,tm.ftime_start) as time_begin
          ,v.time_end + make_interval(secs:= ttb.norm_pkg$get_schedule_tm ( v.tt_schedule_id, ttb.ref_group_kind_pkg$main_action(),
                                                                            date_trunc ('hour',v.time_end)::time - interval '1 hour', date_trunc ('hour',v.time_end)::time )/2) as time_end

          ,v.tt_period_id
          ,v.movement_type_id
          ,v.tt_tr_id
          ,v.tr_capacity_id
          ,v.depo2territory_id
          ,v.order_num
          ,v.mode_id
          ,v.mode_name
          ,v.count_shift
          ,v.wrk_min
          ,v.wrk_max
          ,v.is_own
          ,ttb.norm_pkg$get_schedule_tm ( v.tt_schedule_id, ttb.ref_group_kind_pkg$first_action(),
                                          date_trunc ('hour',v.time_begin)::time - interval '1 hour', date_trunc ('hour',v.time_begin)::time , tm.fstop_item_id, false) as dur_rnd_first
          ,ttb.norm_pkg$get_schedule_tm ( v.tt_schedule_id, ttb.ref_group_kind_pkg$last_action(),
                                          date_trunc ('hour',v.time_end)::time - interval '1 hour', date_trunc ('hour',v.time_end)::time ) as dur_rnd_last

          ,case  when  v.tt_schedule_item_id_fr is not null
          then (select max(sp_e_id)
                from ttb.v_tt_schedule_item_det
                where tt_schedule_item_id = v.tt_schedule_item_id_fr
                      and action_type_id = ttb.action_type_pkg$park_to_stop())
           else tm.fstop_item_id end as stop_item_id
          ,v.switch_time_fr as switch_tm
          ,v.switch_time_to
          ,v.tt_schedule_item_id_fr as switch_schedule_item_id_fr
          ,v.tt_schedule_item_id_to as switch_schedule_item_id_to
          ,v.tt_schedule_id_fr
          ,v.tt_variant_id_fr
          ,coalesce(v.is_switch_from_park_fr, false)  as is_switch_from_park_fr
          ,vr.is_change_driver
          ,coalesce(v.is_to, false) as is_to
        from ttb.v_tt_tr_shift v
          join ttb.tt_variant vr on vr.tt_variant_id = v.tt_variant_id
          left join ttb.timetable_test_pkg$get_tm_start4tr( p_tt_schedule_id => v.tt_schedule_id,
                                                       p_tt_tr_id => v.tt_tr_id,
                                                       p_time_begin => v.time_begin,
                                                       p_rn => 0::numeric,
                                                       p_intvl => null ) as tm on 1=1
        where v.tt_tr_id = p_tt_tr_id
    )
    select  calc_sh.*
      --имеем продолжительность первого технологического рейса,
      --но нет прод-ти последнего производственного, поэтому вычитаем один раз
      ,   greatest (least (wrk_max  - (dur_rnd_first+dur_rnd_last),  extract (EPOCH from (time_end - time_begin))+ (dur_rnd_first+dur_rnd_last))/count_shift   , wrk_min/count_shift)   as avg_dur
      ,  wrk_max /count_shift - (dur_rnd_first+dur_rnd_last)  as avg_dur_max
      , wrk_max - (dur_rnd_first+dur_rnd_last)  as all_dur
    /* ,   greatest (least(wrk_max/count_shift ,  extract (EPOCH from (time_end - time_begin))/count_shift)   , wrk_min/count_shift)   as avg_dur
     ,  wrk_max /count_shift  as avg_dur_max
     ,  wrk_max as all_dur*/

    from calc_sh
    order by   is_switch_from_park_fr, time_begin, order_num

  ) loop

      <<tr_proc>>
    begin
      --если будет выход переключение из другого парка, то пропускаем
      if l_tr.is_switch_from_park_fr then
        exit tr_proc;
      end if;


      l_dr_shift_id := 0;
      RAISE NOTICE ' --------STEP 0000----------   is_own = %  l_tr.stop_item_id=%', l_tr.is_own, l_tr.stop_item_id;

      for l_shift_tm in
      (
        with wrk as (
            select
               generate_series(1, drs.break_count_max+1)  wrk_per
              ,drs.*
              ,(select count(1)>0 from ttb.dr_mode m where m.dr_shift_id = drs.dr_shift_id and m.action_type_id =ttb.action_type_pkg$pause()) as can_pause_in_drs
            from  ttb.dr_shift drs
            where drs.mode_id = l_tr.mode_id
        ),
            sh as (
            select 0 as twrk_b , before_time_max twrk_e, wrk_per, dr_shift_id , mode_id, can_pause_in_drs
            from wrk
            where wrk_per  = 1
            union all
            select work_break_between_min  as twrk_b , work_break_between_max twrk_e, wrk_per, dr_shift_id , mode_id, can_pause_in_drs
            from wrk
            where wrk_per  between 2 and (select max(w.wrk_per)-1 from wrk w )
            union all
            select 0  as twrk_b , after_time_max twrk_e, wrk_per, dr_shift_id , mode_id, can_pause_in_drs
            from wrk
            where wrk_per  =(select max(w.wrk_per) from wrk w )
          )
          ,adata  as (
            select sh.twrk_b, sh.twrk_e, sh.wrk_per, sh.dr_shift_id, sh.mode_id,/*case sh.wrk_per when 1 then 1 else 2 end */ 1 as calc_type
              ,dense_rank() over (order by  sh.dr_shift_id) as sh_num
              ,row_number() over (order by  sh.dr_shift_id) as rn_all
              ,row_number() over (partition by dr_shift_id order by  sh.dr_shift_id) as rn_sh
              ,count(1) over() as cnt_all
              ,count(1) over(partition by dr_shift_id) as sh_all
              ,lead(sh.twrk_e) over (partition by dr_shift_id order by  sh.dr_shift_id) as twrk_next
              ,can_pause_in_drs
            from  ttb.mode m
              join sh on sh.mode_id = m.mode_id
            where m.mode_id = l_tr.mode_id
        )

        select adata.twrk_b
          /*-- из первой и последней частей вычитаем время на рейсы АП и ПА
              + проверка, чтобы обед все-таки состоялся
              !!!!ВРЕМЕННО
              первую часть уменьшаем на 2 времени техн рейса чтобы нивелировать техн. рейс в парк и сутуацию когда первая часть становится последней*/
          ,case
           when rn_all =1 then  adata.twrk_e - l_tr.dur_rnd_first
           when rn_sh =1  then  adata.twrk_e - l_tr.dur_rnd_last
           when rn_all = cnt_all then  adata.twrk_e - l_tr.dur_rnd_last
           else  adata.twrk_e end  as twrk_e
          ,adata.wrk_per
          ,adata.dr_shift_id
          ,adata.mode_id
          ,adata.calc_type
          ,case sh_num when 1 then l_tr.avg_dur else l_tr.avg_dur_max end as avg_sh
          ,adata.sh_num
          ,adata.rn_all
          ,adata.rn_sh
          ,adata.cnt_all
          ,adata.sh_all
          ,adata.twrk_next
          ,adata.can_pause_in_drs
          ,lead(adata.dr_shift_id) over (order by  adata.rn_all) as dr_shift_id_next
          , case  rn_all when cnt_all then ttb.ref_group_kind_pkg$last_action()-- окончание
            else
              case sh_all when rn_sh then ttb.ref_group_kind_pkg$change_dr_action()---пересменка
              else ttb.ref_group_kind_pkg$dinner_action() --обед
              end  end as ref_group_kind_id
        from adata
        where  sh_num <=   ceil (l_tr.all_dur/l_tr.avg_dur_max::real)


        order by dr_shift_id,  wrk_per
      ) loop

        l_cnt:=0;
        l_tt_driver_id_main :=ttb.tt_driver_pkg$get_main_driver(p_tt_tr_id,  l_shift_tm.dr_shift_id);

        --   RAISE NOTICE ' %--------STEP 0----------  tt_tr.tt_tr_id =%  -- ref_group_kind_id = % -- dr_shift_id = %, l_tr.stop_item_id=%',clock_timestamp(), l_tr.tt_tr_id, l_shift_tm.ref_group_kind_id, l_shift_tm.dr_shift_id,l_tr.stop_item_id;
        l_dr_shift_id := l_shift_tm.dr_shift_id;
        --если это переключение проверим что мы попали в правильную смену
        if l_tr.switch_schedule_item_id_fr is not null then
          select dr_shift_id into l_dr_shift_id_switch
          from tt_cube
          where tt_tr_id = p_tt_tr_id
          order by time_end desc limit 1;
          RAISE NOTICE ' continue when l_dr_shift_id(%) <> l_dr_shift_id_switch (%)',  l_dr_shift_id, l_dr_shift_id_switch ;

          continue when l_dr_shift_id <> l_dr_shift_id_switch;
        end if;


        /*        if l_cnt_change_driver >0 then
                  RAISE NOTICE 'change_driver l_shift_tm.rn_sh (%)   l_shift_tm.sh_all (%) ', l_shift_tm.rn_sh  , l_shift_tm.sh_all;
                  continue when l_shift_tm.rn_sh  != l_shift_tm.sh_all   ;
                  l_shift_tm.twrk_e:= l_tr.avg_dur_max +  ttb.timetable_test_pkg$get_dinner_dur(p_tt_variant_id, p_tt_tr_id, l_shift_tm.dr_shift_id, true);
                  l_shift_tm.avg_sh:= l_shift_tm.twrk_e;
                end if;
        */
        --определить фактические  продолжительности работ и смен
        select tr_dt_start,  tr_dt_begin,   tr_dt_end,  tr_dur_work,   tr_dur_line,    tr_dur_shift, tr_stop_item_id, tr_dur_work_after_pause, tr_dt_shift_pause
        into l_tr_dt_start,  l_tr_dt_begin, l_tr_dt_end,l_tr_dur_work, l_tr_dur_line,  l_tr_dur_shift, l_tr_stop_item_id, l_tr_dur_work_after_pause, l_tr_dt_shift_pause
        from ttb.timetable_test_pkg$get_current_data( l_tr.tt_tr_id,l_shift_tm.dr_shift_id);
        l_tr_dur_work:=coalesce(l_tr_dur_work,0);
        l_tr_dur_line:= coalesce(l_tr_dur_line,0);
        l_tr_dur_shift:= coalesce(l_tr_dur_shift,0);
        l_tr_dur_work_after_pause:= coalesce(l_tr_dur_work_after_pause, 0);
        l_tr_dt_shift_pause:= coalesce(l_tr_dt_shift_pause,0);

        RAISE NOTICE ' -----------------STEP 1------ sh_num = % -- l_shift_tm.twrk_e =% -- l_tr_stop_item_id = %, ---l_tr_dt_end =% , tr_dur_work_after_pause = %',  l_shift_tm.sh_num, l_shift_tm.twrk_e, l_tr_stop_item_id, l_tr_dt_end, l_tr_dur_work_after_pause;

        RAISE NOTICE 'NEW continue (l_shift_tm.avg_sh <  l_tr_dur_shift) or ( l_tr.mode_id=% rn_all = %) when l_shift_tm.avg_sh(%)-coalesce(l_tr_dur_shift (%),0) < l_shift_tm.twrk_next=% and  l_tr.switch_tm is null (%)  and l_shift_tm.rn_sh (%) >1 ',
        l_tr.mode_id, l_shift_tm.rn_all, l_shift_tm.avg_sh,l_tr_dur_shift,l_shift_tm.twrk_next,  l_tr.switch_tm is null, l_shift_tm.rn_sh;
        continue when (l_shift_tm.avg_sh <  l_tr_dur_shift
                       and
                       l_shift_tm.ref_group_kind_id != ttb.ref_group_kind_pkg$change_dr_action())   ;
        RAISE NOTICE 'continue when ltr_dt_shift_pause(%) >= l_shift_tm.rn_sh(%)',l_tr_dt_shift_pause , l_shift_tm.rn_sh;
        continue when l_tr_dt_shift_pause >= l_shift_tm.rn_sh;

        RAISE NOTICE 'least(l_shift_tm.twrk_e (%)- case l_shift_tm.rn_sh(%) when 1 then l_tr_dur_shift (%) else 0 end
        ,  l_shift_tm.avg_sh (%)-l_tr_dur_shift(%)
        ,  l_tr.all_dur(%) - l_tr_dur_work(%) ) - l_tr_dur_work_after_pause(%)
                     l_shift_tm.twrk_b (%)' ,
        l_shift_tm.twrk_e, l_shift_tm.rn_sh, l_tr_dur_shift ,  l_shift_tm.avg_sh, l_tr_dur_shift,  l_tr.all_dur , l_tr_dur_work , l_tr_dur_work_after_pause, l_shift_tm.twrk_b ;
        --пропускаем блок, потому что нужно выпустить на пересменку в любом случае

        RAISE NOTICE 'l_tr_dur_work_after_pause (%), l_tr_dt_end (%),l_tr_dur_work (%), l_tr_dur_line (%),  l_tr_dur_shift (%), l_tr_stop_item_id (%), l_tr_one_id (%), l_tr_tmp_id (%), l_tr_dt_shift_begin (%)' ,
        l_tr_dur_work_after_pause, l_tr_dt_end,l_tr_dur_work, l_tr_dur_line,  l_tr_dur_shift, l_tr_stop_item_id, l_tr_one_id, l_tr_tmp_id, l_tr_dt_shift_begin;

        l_break_dur := ttb.timetable_test_pkg$get_break_dur(ttb.action_type_pkg$dinner(),
                                                       l_tr.tt_variant_id,
                                                       l_tr.tt_tr_id,
                                                       l_shift_tm.dr_shift_id);

        if l_shift_tm.avg_sh  >= l_tr_dur_shift then
          l_finish_min := least(l_shift_tm.twrk_b , l_shift_tm.avg_sh-l_tr_dur_shift);
          if l_finish_min<= l_tr_dur_work_after_pause then
            l_finish_min:= 0;
          else
            l_finish_min:= l_finish_min-l_tr_dur_work_after_pause;
          end if;

          l_finish_max:= least(l_shift_tm.twrk_e- case l_shift_tm.rn_sh when 1 then l_tr_dur_shift else 0 end
          ,  l_shift_tm.avg_sh-l_tr_dur_shift
          ,  l_tr.all_dur - l_tr_dur_work - case l_shift_tm.rn_all when l_shift_tm.cnt_all then l_tr.dur_rnd_last else 0 end
                         )::numeric - l_tr_dur_work_after_pause
          ;
          -------------------------------------------------------------------------------------------------------------------------------------------
          if l_tr.is_to and  l_shift_tm.rn_sh = 2 then
            --если это машина с ТО, то после первого обеда пытаемся его выпустить туда
            l_finish_min :=1800;
            l_cnt_uprow:= ttb.timetable_test_pkg$make_tr(
                p_tt_variant_id      =>  l_tr.tt_variant_id,
                p_tt_schedule_id     =>  l_tr.tt_schedule_id,
                p_tt_tr_id           =>  l_tr.tt_tr_id,
                p_tm_start           =>  greatest(l_tr.time_begin, l_tr_dt_end ), -- p_tm_start
                p_finish_min         =>  l_finish_min,
                p_finish_max         =>  l_finish_max ,
                p_break_round_time   =>  l_tr.break_round_time,
                p_calc_type          =>  2,
                p_dr_shift_id        =>  l_shift_tm.dr_shift_id,
                p_ref_group_kind_id  =>  ttb.ref_group_kind_pkg$to_action(),
                p_start_stop_item_id =>  coalesce(l_tr_stop_item_id, l_tr.stop_item_id), -- ОП, с которого стартуем
                p_is_start           =>  (l_shift_tm.rn_all =1) or coalesce (l_tr.switch_time_to = l_tr_dt_end, false),-- старт выхода
                p_sh_part            =>  l_shift_tm.rn_sh,
                p_is_add_driver      =>  false
            );
            RAISE NOTICE 'ttb.timetable_test_pkg$make_tr - перед ТО                Добавлено l_cnt_uprow % ', l_cnt_uprow;

            if l_cnt_uprow > 0 then
              RAISE NOTICE 'выпускаем на ТО';
              for l_it in
              (
                select it.tt_schedule_item_id, pnt.fstop_item_id
                from  ttb.timetable_test_pkg$get_cube_rec(p_tt_variant_id => l_tr.tt_variant_id,
                                                     p_tt_tr_id => l_tr.tt_tr_id,
                                                     p_is_last => true) pnt
                  join ttb.v_tt_schedule_item it on it.tt_schedule_id = pnt.ftt_schedule_id
                where ref_group_kind_id =  ttb.ref_group_kind_pkg$to_action()
                order by order_num
              )
              loop

                RAISE NOTICE '--------';
                RAISE NOTICE '--------------------BREAK--50----  l_it.tt_schedule_item_id= % ', l_it.tt_schedule_item_id;
                perform ttb.timetable_test_pkg$make_schedule_item
                (p_tt_schedule_item_id =>l_it.tt_schedule_item_id
                    ,p_is_last  =>true
                    ,p_tt_tr_id => l_tr.tt_tr_id
                    ,p_stop_item_id  => l_it.fstop_item_id
                    ,p_one_id   => l_tr_one_id
                );
              end loop;

              --снова определить фактические  продолжительности работ и смен
              select tr_dt_start,  tr_dt_begin,   tr_dt_end,  tr_dur_work,   tr_dur_line,    tr_dur_shift, tr_stop_item_id, tr_dur_work_after_pause, tr_dt_shift_pause
              into l_tr_dt_start,  l_tr_dt_begin, l_tr_dt_end,l_tr_dur_work, l_tr_dur_line,  l_tr_dur_shift, l_tr_stop_item_id, l_tr_dur_work_after_pause, l_tr_dt_shift_pause
              from ttb.timetable_test_pkg$get_current_data( l_tr.tt_tr_id,l_shift_tm.dr_shift_id);
              l_tr_dur_work:=coalesce(l_tr_dur_work,0);
              l_tr_dur_line:= coalesce(l_tr_dur_line,0);
              l_tr_dur_shift:= coalesce(l_tr_dur_shift,0);
              l_tr_dur_work_after_pause:= coalesce(l_tr_dur_work_after_pause, 0);
              l_tr_dt_shift_pause:= coalesce(l_tr_dt_shift_pause,0);

              RAISE NOTICE 'l_tr_dur_work_after_pause (%), l_tr_dt_end (%),l_tr_dur_work (%), l_tr_dur_line (%),  l_tr_dur_shift (%), l_tr_stop_item_id (%), l_tr_one_id (%), l_tr_tmp_id (%), l_tr_dt_shift_begin (%)' ,
              l_tr_dur_work_after_pause, l_tr_dt_end,l_tr_dur_work, l_tr_dur_line,  l_tr_dur_shift, l_tr_stop_item_id, l_tr_one_id, l_tr_tmp_id, l_tr_dt_shift_begin;

            end if;

            l_finish_min := least(l_shift_tm.twrk_b , l_shift_tm.avg_sh-l_tr_dur_shift);
            if l_finish_min<= l_tr_dur_work_after_pause then
              l_finish_min:= 0;
            else
              l_finish_min:= l_finish_min-l_tr_dur_work_after_pause;
            end if;

            l_finish_max:= least(l_shift_tm.twrk_e- case l_shift_tm.rn_sh when 1 then l_tr_dur_shift else 0 end
            ,  l_shift_tm.avg_sh-l_tr_dur_shift
            ,  l_tr.all_dur - l_tr_dur_work - case l_shift_tm.rn_all when l_shift_tm.cnt_all then l_tr.dur_rnd_last else 0 end
                           )::numeric - l_tr_dur_work_after_pause
            ;

          end if;
          -------------------------------------------------------------------------------------------------------------------------------------------------

          --если переключение смотрим, чтобы обед влез
          if l_tr.switch_tm  between  greatest(l_tr.time_begin, l_tr_dt_end ) + make_interval(secs:=l_finish_max) and
             greatest(l_tr.time_begin, l_tr_dt_end ) + make_interval(secs:=l_finish_max +   l_break_dur)
             and l_shift_tm.ref_group_kind_id = ttb.ref_group_kind_pkg$dinner_action()  then

            l_finish_max:= l_finish_max - l_break_dur;
            RAISE NOTICE 'уменьшаем работу, чтобы обеду хватило времени';
          end if;

          l_cnt_uprow:= ttb.timetable_test_pkg$make_tr(
              p_tt_variant_id      =>  l_tr.tt_variant_id,
              p_tt_schedule_id     =>  l_tr.tt_schedule_id,
              p_tt_tr_id           =>  l_tr.tt_tr_id,
              p_tm_start           =>  greatest(l_tr.time_begin, l_tr_dt_end ), -- p_tm_start
              p_finish_min         =>  l_finish_min,
              p_finish_max         =>  l_finish_max ,
              p_break_round_time   =>  l_tr.break_round_time,
              p_calc_type          =>  l_shift_tm.calc_type,
              p_dr_shift_id        =>  l_shift_tm.dr_shift_id,
              p_ref_group_kind_id  =>  l_shift_tm.ref_group_kind_id, -- следующая группа действий расписания
              p_start_stop_item_id =>  coalesce(l_tr_stop_item_id, l_tr.stop_item_id), -- ОП, с которого стартуем
              p_is_start           =>  (l_shift_tm.rn_all =1) or coalesce (l_tr.switch_time_to = l_tr_dt_end, false),-- старт выхода
              p_sh_part            =>  l_shift_tm.rn_sh,
              p_is_add_driver      =>  false
          );
          RAISE NOTICE 'ttb.timetable_test_pkg$make_tr - Добавлено l_cnt_uprow % ', l_cnt_uprow;

          if l_cnt_uprow >0 then
            --добавляем корректировочную часть межрейса
            perform ttb.timetable_test_pkg$make_tr_bn_corr(l_tr.tt_schedule_id, l_tr.tt_tr_id);
          end if;

          --снова определить фактические  продолжительности работ и смен
          select tr_dt_start,  tr_dt_begin,   tr_dt_end,  tr_dur_work,   tr_dur_line,    tr_dur_shift, tr_stop_item_id, tr_one_id, tr_tmp_id, tr_dt_shift_begin
          into l_tr_dt_start,  l_tr_dt_begin, l_tr_dt_end,l_tr_dur_work, l_tr_dur_line,  l_tr_dur_shift, l_tr_stop_item_id, l_tr_one_id, l_tr_tmp_id, l_tr_dt_shift_begin
          from ttb.timetable_test_pkg$get_current_data(l_tr.tt_tr_id, l_shift_tm.dr_shift_id);

          ----------------------------------------------------------------------------------------------------------------------------------
          ---переключение
          if l_tr.switch_tm is not null and l_tr_dt_end between  l_tr.switch_tm - make_interval(secs:=1740) and  l_tr.switch_tm + make_interval(secs:=l_finish_max)
          /*and l_tr_dt_end > l_tr.switch_tm */ then
            RAISE NOTICE '-----------------STEP 20 --- переключение! l_tr.switch_tm  = %',  l_tr.switch_tm;
            RAISE NOTICE 'удалим  все что мешает уйти на переключение ';
            perform  ttb.timetable_test_pkg$delete_all_before_switch(p_tt_schedule_id => l_tr.tt_schedule_id,
                                                                p_tt_tr_id => l_tr.tt_tr_id,
                                                                p_switch_tm => l_tr.switch_tm,
                                                                p_tt_schedule_item_id => l_tr.switch_schedule_item_id_to);

            --создаем действие переключение
            for l_it in
            (
              select l_tr.switch_schedule_item_id_to as tt_schedule_item_id, pnt.fstop_item_id
              from  ttb.timetable_test_pkg$get_cube_rec(p_tt_variant_id => l_tr.tt_variant_id,
                                                   p_tt_tr_id => l_tr.tt_tr_id,
                                                   p_is_last => true) pnt
            )
            loop

              RAISE NOTICE '--------';
              RAISE NOTICE '--------------------SWITCH-----  l_tr.switch_schedule_item_id__fr= % ', l_tr.switch_schedule_item_id_to;
              perform ttb.timetable_test_pkg$make_schedule_item
              (p_tt_schedule_item_id =>l_tr.switch_schedule_item_id_to
                  ,p_is_last  =>true
                  ,p_tt_tr_id => l_tr.tt_tr_id
                  ,p_stop_item_id  => l_it.fstop_item_id
                  ,p_one_id   => l_tr_one_id
              );
            end loop;

            RAISE NOTICE '--------  END WORK ON THIS ROUND';
            exit  tr_proc;  --уходим из цикла
          end if;
          -----------------------------------------------------------------------------------------------------------------------------------------

          RAISE NOTICE '-----------------STEP 2------ l_tr.all_dur = % -- l_tr_dur_work=% -- l_shift_tm.twrk_e =% -- l_tr_stop_item_id = %, ---l_tr_dt_end =% ---l_cnt_uprow = %', l_tr.all_dur, l_tr_dur_work, l_shift_tm.twrk_e, l_tr_stop_item_id, l_tr_dt_end ,l_cnt_uprow;

          continue when l_cnt_uprow <= 0 and l_shift_tm.ref_group_kind_id!=ttb.ref_group_kind_pkg$change_dr_action();
          --exit;
          RAISE NOTICE 'NEXT exit when l_tr.all_dur - l_tr_dur_work < 1800';
          exit when (l_tr.all_dur - l_tr_dur_work <  1800);


          RAISE NOTICE 'NEXT continue when (l_shift_tm.avg_sh %- l_tr_dur_shift %) < 1800    and l_shift_tm.ref_group_kind_id != ttb.ref_group_kind_pkg$change_dr_action()',l_shift_tm.avg_sh,l_tr_dur_shift;
          continue when (l_shift_tm.avg_sh-l_tr_dur_shift) < 1800 and
                        l_shift_tm.ref_group_kind_id != ttb.ref_group_kind_pkg$change_dr_action();



          --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
          RAISE NOTICE '--------------------STEP BREAK------ выпуск l_shift_tm.ref_group_kind_id = % ', l_shift_tm.ref_group_kind_id;
          if l_shift_tm.ref_group_kind_id = ttb.ref_group_kind_pkg$dinner_action() then

            if l_tr.is_change_driver  then

              RAISE NOTICE 'change_driver l_shift_tm.rn_sh (%)   l_shift_tm.sh_all (%) ', l_shift_tm.rn_sh  , l_shift_tm.sh_all;
              l_finish_max:=  ttb.timetable_test_pkg$get_dinner_dur(p_tt_variant_id, p_tt_tr_id, l_shift_tm.dr_shift_id, true);
              l_finish_min:= asd.get_constant('OPTIMAL_TIME_DINNER')::numeric;
              l_cnt:= ttb.timetable_test_pkg$make_tr(
                  p_tt_variant_id      =>  l_tr.tt_variant_id,
                  p_tt_schedule_id     =>  l_tr.tt_schedule_id,
                  p_tt_tr_id           =>  l_tr.tt_tr_id,
                  p_tm_start           =>  l_tr_dt_end,
                  p_finish_min         =>  l_finish_min,
                  p_finish_max         =>  l_finish_max,
                  p_break_round_time   =>  l_tr.break_round_time,
                  p_calc_type          =>  22,
                  p_dr_shift_id        =>  l_shift_tm.dr_shift_id,
                  p_ref_group_kind_id  =>  ttb.ref_group_kind_pkg$dinner_action(), -- следующая группа действий расписания-обед
                  p_start_stop_item_id =>  coalesce(l_tr_stop_item_id, l_tr.stop_item_id), -- ОП, с которого стартуем
                  p_is_start           =>  false,-- старт выхода
                  p_sh_part            =>  l_shift_tm.rn_sh,
                  p_is_add_driver      =>  true
              );

              RAISE NOTICE 'l_cnt (%) ' ,l_cnt;
              --если не получилось подменить в этот момент, для начального периода пробуем найти уже имеющиеся действия,
              --которым можно дать подменного водителя
              if l_cnt = 0 then
                for l_ch_dr in (
                  with adata as (
                      select
                         lag(cb.time_begin) over (order by cb.time_begin) as tm_b
                        ,lag(tt_cube_id) over (order by cb.time_begin) as lag_tt_cube_id
                        --,(select max(t.time_end) from tt_cube t where ( t.tt_tr_id, t.tt_schedule_id, t.tmp_id, t.one_id) = (cb.tt_tr_id, cb.tt_schedule_id, cb.tmp_id, cb.one_id)) as tm_e
                        ,cb.time_begin as tm_e
                        ,cb.*
                      from tt_cube  cb
                        join ttb.vm_action_type vma on vma.action_type_id = cb.action_type_id and vma.is_production_round
                      where cb.tt_tr_id = l_tr.tt_tr_id
                            and cb.dr_shift_id = l_shift_tm.dr_shift_id
                            and cb.order_num=1
                            and cb.stop_item_id = coalesce(l_tr_stop_item_id, l_tr.stop_item_id)
                            and l_shift_tm.rn_sh = 1
                  )
                  select
                    f.tt_driver_id,
                    adata.tm_b, adata.tm_e
                    ,extract (epoch from adata.tm_e-adata.tm_b) as dur
                  from adata
                    ,lateral  ttb.tt_driver_pkg$get_free_driver(p_tt_variant_id, adata.tm_b, adata.tm_e) f
                  --магические полчаса чтобы обед не начинался прямо сразу после начала смены
                  where adata.tm_b > l_tr_dt_shift_begin + make_interval (secs:= 1800)
                        and extract (epoch from adata.tm_e-adata.tm_b)between l_finish_min and l_finish_max
                  order by time_begin desc
                  limit 1) loop

                  RAISE NOTICE 'ttb.tt_driver_pkg$update_driver  %  - %  , l_ch_dr.tt_driver_id(%)' ,l_ch_dr.tm_b, l_ch_dr.tm_e, l_ch_dr.tt_driver_id;
                  l_cnt := ttb.tt_driver_pkg$update_driver(p_tt_tr_id => l_tr.tt_tr_id
                  ,p_tt_driver_id => l_ch_dr.tt_driver_id
                  ,p_tm_b => l_ch_dr.tm_b
                  ,p_tm_e => l_ch_dr.tm_e);

                end loop;
              end if;
            end if;

            RAISE NOTICE 'l_cnt (%) ' ,l_cnt;

            if l_cnt <= 0 then
              /*--увеличиваем время обеда*/
              perform ttb.timetable_test_pkg$update_time_dinner(p_tt_variant_id =>l_tr.tt_variant_id,
                                                           p_tt_tr_id =>l_tr.tt_tr_id
              );

              --проверим получается ли обед
              if ttb.timetable_test_pkg$get_break_dur(ttb.action_type_pkg$dinner(),
                                                 l_tr.tt_variant_id,
                                                 l_tr.tt_tr_id,
                                                 l_shift_tm.dr_shift_id) =0 then
                --не получится ничего
                RAISE NOTICE 'Нет времени на обед!';

                continue ;
              end if;

              --перед обедом удаляем межрейсы, если первое действие обеда не рейс
              if (select count(1)
                  from ttb.timetable_test_pkg$get_ref_group_kind_data (l_tr.tt_schedule_id, l_shift_tm.ref_group_kind_id) t
                    join ttb.vm_action_type vma on vma.action_type_id = faction_type_id and vma.is_round  ) =0 then

                perform ttb.timetable_test_pkg$delete_bn_before_dinner(l_tr.tt_schedule_id, l_tr.tt_tr_id, l_tr_one_id, l_tr_tmp_id);
              end if;
            end if;--l_cnt
          end if;
          ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



        end if;

        if l_shift_tm.ref_group_kind_id = ttb.ref_group_kind_pkg$change_dr_action()    then
          --удалим обед, если он перед пересменкой!
          RAISE NOTICE 'удалим обед перед пересменкой + все что мешает уйти на пересменку  ';
          perform ttb.timetable_test_pkg$delete_all_before_ref_gr(l_tr.tt_schedule_id,
                                                             l_tr.tt_tr_id,
                                                             p_ref_group_kind_id => l_shift_tm.ref_group_kind_id );
        end if;

        --выпускаем на куда ему положено
        if l_cnt <= 0 then
          RAISE NOTICE 'выпускаем на перерыв';
          for l_it in
          (
            select it.tt_schedule_item_id, pnt.fstop_item_id
            from  ttb.timetable_test_pkg$get_cube_rec(p_tt_variant_id => l_tr.tt_variant_id,
                                                 p_tt_tr_id => l_tr.tt_tr_id,
                                                 p_is_last => true) pnt
              join ttb.v_tt_schedule_item it on it.tt_schedule_id = pnt.ftt_schedule_id
            where ref_group_kind_id =  l_shift_tm.ref_group_kind_id
                  and ref_group_kind_id != ttb.ref_group_kind_pkg$last_action()  --кроме группы в парк
            order by order_num
          )
          loop

            RAISE NOTICE '--------';
            RAISE NOTICE '--------------------BREAK--%----  l_it.tt_schedule_item_id= % ',l_shift_tm.ref_group_kind_id, l_it.tt_schedule_item_id;
            perform ttb.timetable_test_pkg$make_schedule_item
            (p_tt_schedule_item_id =>l_it.tt_schedule_item_id
                ,p_is_last  =>true
                ,p_tt_tr_id => l_tr.tt_tr_id
                ,p_stop_item_id  => l_it.fstop_item_id
                ,p_one_id   => l_tr_one_id
            );


          end loop;
        end if;


        if l_shift_tm.ref_group_kind_id = ttb.ref_group_kind_pkg$change_dr_action() then
          RAISE NOTICE 'пересменка с межрейсом ';
          l_cnt_uprow:= ttb.timetable_test_pkg$update_action_change_driver(l_tr.tt_schedule_id,
                                                                      l_tr.tt_tr_id,
                                                                      l_shift_tm.dr_shift_id_next);
          RAISE NOTICE 'пересменка с межрейсом  - обновлено %', l_cnt_uprow;
        end if;

      end loop;


      RAISE NOTICE '--------------------STEP finish------ выпуск АП';
      --удаляем все рейс, если после него нельзя отправить в парк

      perform ttb.timetable_test_pkg$delete_all_before_ref_gr(p_tt_schedule_id => l_tr.tt_schedule_id,
                                                         p_tt_tr_id => l_tr.tt_tr_id,
                                                         p_ref_group_kind_id => ttb.ref_group_kind_pkg$last_action(),
                                                         p_is_with_dinner => true);

      --добавляем рейс в парк в конец
      for l_it in
      (

        select it.tt_schedule_item_id, pnt.fstop_item_id, pnt.ftt_schedule_id
        from   ttb.timetable_test_pkg$get_cube_rec(p_tt_variant_id => l_tr.tt_variant_id,
                                              p_tt_tr_id => l_tr.tt_tr_id,
                                              p_is_last => true) pnt
          join ttb.v_tt_schedule_item it on it.tt_schedule_id = pnt.ftt_schedule_id
        where ref_group_kind_id = ttb.ref_group_kind_pkg$last_action() --завершающее действие
        order by order_num
      )
      loop
        perform ttb.timetable_test_pkg$make_schedule_item(
            p_tt_schedule_item_id =>l_it.tt_schedule_item_id
            ,p_is_last  =>true
            ,p_tt_tr_id => l_tr.tt_tr_id
            ,p_stop_item_id  => l_it.fstop_item_id
            ,p_one_id   => l_tr_one_id
        );
      end loop;

    end ; --<<tr_proc>>



    RAISE NOTICE '%  tt_tr.tt_tr_id =%, ---------------------------------------------------------------------STEP end', clock_timestamp(), l_tr.tt_tr_id;
    RAISE NOTICE '--------------------STEP start------ выпуск ПА';
    /*добавляем рейс в из парка перед началом*/


    --если переключение из парка, то создаем по элементу последовательности другого расписания
    if l_tr.is_switch_from_park_fr then
      RAISE NOTICE '-----------------STEP 21 --- переключение из парка! l_tr.switch_tm  = %',  l_tr.switch_tm;
      --создаем действие переключение из парка
      for l_it in
      (
        select l_tr.switch_schedule_item_id_to as tt_schedule_item_id, pnt.fstop_item_id
        from  ttb.timetable_test_pkg$get_cube_rec(p_tt_variant_id => l_tr.tt_variant_id,
                                             p_tt_tr_id => l_tr.tt_tr_id,
                                             p_is_last => false) pnt
      )
      loop

        RAISE NOTICE '--------';
        RAISE NOTICE '--------------------SWITCH FROM PARK-----  l_tr.switch_schedule_item_id__fr= % ', l_tr.switch_schedule_item_id_to;
        perform ttb.timetable_test_pkg$make_schedule_item
        (p_tt_schedule_item_id =>l_tr.switch_schedule_item_id_to
            ,p_is_last  =>false
            ,p_tt_tr_id => l_tr.tt_tr_id
            ,p_stop_item_id  => null--l_it.fstop_item_id
            ,p_one_id   => l_tr_one_id
        );
      end loop;

    else
      for l_it in
      (
        select it.tt_schedule_item_id, pnt.fstop_item_id, pnt.ftt_schedule_id
        from   ttb.timetable_test_pkg$get_cube_rec(p_tt_variant_id => l_tr.tt_variant_id,
                                                   p_tt_tr_id => l_tr.tt_tr_id,
                                                   p_is_last => false) pnt
          join ttb.v_tt_schedule_item it on it.tt_schedule_id = l_tr.tt_schedule_id
        where ref_group_kind_id = ttb.ref_group_kind_pkg$first_action()
              and l_tr.is_own --только свои
              and l_tr.switch_schedule_item_id_fr is null --не переключен
              and not l_tr.is_switch_from_park_fr --не переключен из парка
        order by order_num
      )
      loop
        perform ttb.timetable_test_pkg$make_schedule_item(
            p_tt_schedule_item_id =>l_it.tt_schedule_item_id
            ,p_is_last  =>false
            ,p_tt_tr_id => l_tr.tt_tr_id
            ,p_stop_item_id  => l_it.fstop_item_id
            ,p_one_id   => l_tr_one_id
        );

      end loop;
    end if;


    /*--добавляем корректировочную часть межрейса*/
    perform ttb.timetable_test_pkg$make_tr_bn_corr(l_tr.tt_schedule_id, l_tr.tt_tr_id);


    /*--увеличиваем время обеда для последних обедов*/
    perform ttb.timetable_test_pkg$update_time_dinner(p_tt_variant_id => l_tr.tt_variant_id,
                                                 p_tt_tr_id =>  l_tr.tt_tr_id
    );
  end loop;

  RAISE NOTICE 'Все переносим в окончательную структуру';
  perform  ttb.timetable_test_pkg$make_tt_out(p_tt_variant_id,  p_tt_tr_id);
  if l_tr.tt_schedule_id_fr is not null then
    perform  ttb.timetable_test_pkg$make_tt_out_switch(p_tt_variant_id,  p_tt_tr_id);
  end if;

end;

$body$
language plpgsql volatile
cost 100;


/*
вернуть первую или поледнюю остановку рейса
 p_is_last = true последнюю
*/

create or replace function ttb.timetable_test_pkg$get_cube_rec(in p_tt_variant_id numeric,
                                                          in p_tt_tr_id numeric,
                                                          in p_is_last boolean default true)
  returns table
  (
    fstop_item_id integer,
    ftt_schedule_id integer,
    ftm timestamp
  )
as
$body$
begin

  if  p_is_last then
    return query
    select t.stop_item_id, t.tt_schedule_id, t.time_begin
    from  tt_cube t
    where t.tt_tr_id = p_tt_tr_id
    -- and ttb.action_type_pkg$is_round(t.action_type_id)
    order by time_end desc, time_begin desc
    limit 1
    ;
  else
    return query
    select  t.stop_item_id, t.tt_schedule_id, t.time_end
    from  tt_cube t
    where t.tt_tr_id = p_tt_tr_id
    --        and ttb.action_type_pkg$is_round(t.action_type_id)
    order by time_end, time_begin
    limit 1
    ;

  end if;

end;
$body$
language plpgsql volatile
cost 100;
------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- набор действий
in p_sh_part numeric, часть смены
in p_is_add_driver выпустить подменного водителя
***************************************************************************/

create or replace function ttb.timetable_test_pkg$make_tr(in p_tt_variant_id numeric,
                                                     in p_tt_schedule_id numeric,
                                                     in p_tt_tr_id numeric,
                                                     in p_tm_start timestamp,
                                                     in p_finish_min numeric,
                                                     in p_finish_max numeric,
                                                     in p_break_round_time numeric,
                                                     in p_calc_type numeric,
                                                     in p_dr_shift_id numeric,
                                                     in p_ref_group_kind_id numeric,
                                                     in p_start_stop_item_id numeric,
                                                     in p_is_start boolean,
                                                     in p_sh_part numeric,
                                                     in p_recursive boolean default false,
                                                     in p_is_add_driver boolean default false)
  returns numeric

as
$body$
declare
  l_res numeric:=0;
  l_x  record;
  c_x record;
  l_dr_shift_tm_min numeric;
  l_break_round_time numeric:= p_break_round_time;
  --l_cnt numeric:=0;

begin
  if  p_finish_min > p_finish_max then
    return -1;
  end if;
  --КОСТЫЛЬ!!! Если попали в уличное расписание, увеличиваем продолжительность межрейсовой стоянки, чтобы хоть как-то попасть

  if exists (select  1 from ttb.tt_period p where p.tt_schedule_id = p_tt_schedule_id and p_tm_start between p.time_begin and p.time_end
                                                  and p.movement_type_id = ttb.jf_movement_type_pkg$cn_type_street() ) then
    l_break_round_time := p_break_round_time*20;
  end if;


  RAISE NOTICE '-------$make_tr-----------p_tt_tr_id=%    p_dr_shift_id = %  p_is_start= %',p_tt_tr_id,    p_dr_shift_id, p_is_start;
  RAISE NOTICE '-------finish_min = %    finish_max  =  %, break_round_time = %, l_shift_,tm.rn_sh= %,  p_recursive =% , p_is_add_driver=%',  p_finish_min,  p_finish_max, p_break_round_time, p_sh_part,  p_recursive, p_is_add_driver;


  /*пробуем учесть рекомендуемое время работы до обеда*/
  select before_time_min into l_dr_shift_tm_min from ttb.dr_shift where dr_shift_id = p_dr_shift_id;

  if p_sh_part=1 and p_finish_min < l_dr_shift_tm_min and l_dr_shift_tm_min < p_finish_max and not p_is_add_driver then
    l_res:=ttb.timetable_test_pkg$make_tr(p_tt_variant_id  => p_tt_variant_id,
                                     p_tt_schedule_id => p_tt_schedule_id,
                                     p_tt_tr_id       => p_tt_tr_id,
                                     p_tm_start       => p_tm_start,
                                     p_finish_min     => l_dr_shift_tm_min,
                                     p_finish_max     => p_finish_max,
                                     p_break_round_time => p_break_round_time,
                                     p_calc_type      => p_calc_type,
                                     p_dr_shift_id    => p_dr_shift_id,
                                     p_ref_group_kind_id  => p_ref_group_kind_id,
                                     p_start_stop_item_id => p_start_stop_item_id,
                                     p_is_start           => p_is_start,
                                     p_sh_part            => p_sh_part,
                                     p_recursive          => true,
                                     p_is_add_driver      => p_is_add_driver
    );

  end if;

  RAISE NOTICE '-------Количество добавленных строк %', l_res;
  if coalesce(l_res,0) = 0 then
    --до этого не выпустилось пытаемся дальше

    for l_x in(
      with recursive  d as
      (select tsrange(p_tm_start + p_finish_min * interval '1 second', p_tm_start + p_finish_max * interval '1 second' ,'[)') as d_range,
              p_tm_start + p_finish_min * interval '1 second' as dt_b, p_tm_start + p_finish_max * interval '1 second' as dt_e


      )
        , tph as (
          select tph.time_begin-  asd.get_constant('OPTIMAL_TIME_DINNER')* interval '1 second' as time_begin,
            time_end,
                 tsrange(tph.time_begin-  asd.get_constant('OPTIMAL_TIME_DINNER')* interval '1 second' , time_end,'[)') as peak_range

          from ttb.tt_peak_hour tph
            join ttb.tt_variant v on v.tt_variant_id = tph.tt_variant_id
          where tph.tt_variant_id = p_tt_variant_id
                and v.is_all_tr_online
                and p_ref_group_kind_id = ttb.ref_group_kind_pkg$dinner_action()
                and not v.is_change_driver  -- если нет подменного
      )
        ,ph as  (
          select tph.*,
            least(tph.time_begin, d.dt_b) as dt_b, greatest(tph.time_end, d.dt_e) as dt_e

          from tph
            join d on  d_range && peak_range
      )
        ,itog as (

        select   max(time_end) as dt_b, max(dt_e) as dt_e, true as is_peak
        from ph
        union  all
        select min(dt_b), min(time_begin), true as is_peak
        from ph
        union  all
        select time_end , lead(time_begin) over (order by time_begin), true as is_peak
        from ph
        union all
        select dt_b, dt_e, false as is_peak
        from d
        --where not exists (select 1 from tph where d_range && tsrange(time_begin, time_end)   )
      )
      select extract (EPOCH from (dt_b - p_tm_start))::int as finish_min
        ,extract (EPOCH from (dt_e - p_tm_start))::int as finish_max
        ,l_break_round_time  as break_round_time --asd.get_constant('MAX_INTER_RACE_STOP')::int as break_round_time-- p_break_round_time * 2
        ,case
         when  exists (select 1 from  ttb.tt_tr_period ttp where ttp.tt_tr_id = p_tt_tr_id and not ttp.is_own) then 1
         when p_is_add_driver then p_calc_type
         when p_ref_group_kind_id= ttb.ref_group_kind_pkg$to_action() then p_calc_type
         when (p_ref_group_kind_id= ttb.ref_group_kind_pkg$dinner_action()
               and p_sh_part =1
               and p_finish_min=0
         )/* and
                   (select count(1) from ttb.timetable_test_pkg$get_stop_start(p_tt_schedule_id)) <2 */then

           (select  min(ttp.tt_tr_order_num) %2 +1
            from  ttb.tt_tr tt_tr
              join ttb.tt_tr_period ttp on tt_tr.tt_tr_id = ttp.tt_tr_id
              join ttb.tt_period p on p.tt_period_id = ttp.tt_period_id
            where tt_tr.tt_tr_id = p_tt_tr_id
           )

         else 1 end  as calc_type
        , is_peak
      from itog
      where dt_b< dt_e
            and dt_e is not null
      order by is_peak desc, dt_b
    ) loop
      RAISE NOTICE ' !!!';
      l_res:= ttb.timetable_test_pkg$save_tr( p_tt_variant_id,
                                         p_tt_schedule_id,
                                         p_tt_tr_id,
                                         p_tm_start,
                                         l_x.finish_min,
                                         l_x.finish_max,
                                         l_x.break_round_time,
                                         --case p_sh_part when 3 then 3 else l_x.calc_type end,
                                         l_x.calc_type,
                                         p_dr_shift_id,
                                         p_ref_group_kind_id ,
                                         p_start_stop_item_id,
                                         p_is_start,
                                         p_sh_part,
                                         p_is_add_driver
      );

      /*   if l_res <1 and not p_recursive then
           l_res:= ttb.timetable_test_pkg$save_tr( p_tt_variant_id,
                                              p_tt_schedule_id,
                                              p_tt_tr_id,
                                              p_tm_start,
                                              l_x.finish_min,
                                              l_x.finish_max,
                                              l_x.break_round_time,
                                              4,
                                              p_dr_shift_id,
                                              p_ref_group_kind_id ,
                                              p_start_stop_item_id,
                                              p_is_start);

         end if;*/

      exit when l_res>0;
    end loop;

    --если не получилось выпустить в правильное время, то пробуем как повезет
    if l_res<1 and p_sh_part>1 and not p_recursive and not p_is_add_driver then
      RAISE NOTICE ' не получилось выпустить в правильное время, пробуем как повезет';
      l_res:= ttb.timetable_test_pkg$save_tr( p_tt_variant_id,
                                         p_tt_schedule_id,
                                         p_tt_tr_id,
                                         p_tm_start,
                                         p_finish_min,
                                         p_finish_max,
                                         l_break_round_time,
                                         case l_break_round_time when p_break_round_time then 3 else 4 end::numeric,
                                         p_dr_shift_id,
                                         p_ref_group_kind_id ,
                                         p_start_stop_item_id,
                                         p_is_start,
                                         p_sh_part,
                                         p_is_add_driver);
    end if;
    if l_res<1 and not p_recursive and not p_is_add_driver  then
      RAISE NOTICE ' не получилось выпустить как повезет пробуем финтить еще';
      l_res:= ttb.timetable_test_pkg$save_tr( p_tt_variant_id,
                                         p_tt_schedule_id,
                                         p_tt_tr_id,
                                         p_tm_start,
                                         p_finish_min,
                                         p_finish_max,
                                         l_break_round_time,
                                         case l_x.calc_type when 1 then 2 else 1 end::numeric,
                                         p_dr_shift_id,
                                         p_ref_group_kind_id ,
                                         p_start_stop_item_id,
                                         p_is_start,
                                         p_sh_part,
                                         p_is_add_driver);
    end if;

  end if;
  return l_res;



end;
$body$
language plpgsql volatile
cost 100;

------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- сохранить распределение ТС в структуру
***************************************************************************/

create or replace function ttb."timetable_test_pkg$save_tr"(in p_tt_variant_id numeric,
                                                       in p_tt_schedule_id numeric,
                                                       in p_tt_tr_id numeric,
                                                       in p_tm_start timestamp,
                                                       in p_finish_min numeric,
                                                       in p_finish_max numeric,
                                                       in p_break_round_time numeric,
                                                       in p_calc_type numeric,
                                                       in p_dr_shift_id numeric,
                                                       in p_ref_group_kind_id numeric,
                                                       in p_start_stop_item_id numeric,
                                                       in p_is_start boolean,
                                                       in p_sh_part numeric,
                                                       in p_is_add_driver boolean )
  returns numeric

as
$body$
declare
  l_res integer:=0;
  l_rec record;
  l_dur integer;
  l_avg00 integer;
  l_cnt integer:=0;
  --l_max_break_dur integer;
  l_break_dur integer;
  l_x record;
  l_it record;
  l_tt_driver_id numeric;
  l_tm_b TIMESTAMP;
  l_tm_e TIMESTAMP;
begin
  RAISE NOTICE ' save_tr';
  RAISE NOTICE '-------1 %', clock_timestamp();

  create temporary table  if not exists tr
  (
    ftmp_id integer,
    fone_id integer,
    ftt_schedule_id integer,
    ftm_b timestamp)
  on commit   drop
  tablespace ttb_data;

  truncate tr;

  l_break_dur:= ttb.timetable_test_pkg$get_dinner_dur(p_tt_variant_id, p_tt_tr_id, p_dr_shift_id, true);
  --не давать максимальный отстой, чтобы можно было увеличить выход потом
  if p_sh_part = 2 and l_break_dur > 7200 then
    l_break_dur:= l_break_dur - asd.get_constant('OPTIMAL_TIME_DINNER')::int;
  end if;

  if p_ref_group_kind_id  = ttb.ref_group_kind_pkg$to_action() then
    --ТО
    l_break_dur:= graetest (l_break_dur, asd.get_constant('MAX_INTER_RACE_STOP')::int);
  end if;

  insert into tr
    select ftmp_id, fone_id , ftt_schedule_id , ftm_b
    from  ttb.timetable_test_pkg$prepare_tr( p_tt_variant_id,
                                        p_tt_schedule_id,
                                        p_tt_tr_id,
                                        p_tm_start,
                                        p_finish_min,
                                        p_finish_max,
                                        p_break_round_time,
                                        p_calc_type,
                                        p_ref_group_kind_id ,
                                        p_start_stop_item_id,
                                        p_is_start,
                                        p_sh_part,
                                        l_break_dur)
  ;

  RAISE NOTICE 'tr (%) ', (select array_agg(ftm_b) from tr);


  --если не пришел подменный водитель ставим основного
  if not p_is_add_driver then
    l_tt_driver_id := ttb.tt_driver_pkg$get_main_driver(p_tt_tr_id, p_dr_shift_id);
  else
    with adata as (
        select  min(tt_cube.time_begin) tm_b, max(tt_cube.time_end) tm_e
        from tr
          join ttb.tt_tr ttr on ttr.tt_tr_id = p_tt_tr_id
          join  tt_cube on (tt_cube.tmp_id, tt_cube.one_id, tt_cube.tt_schedule_id, tt_cube.tr_capacity_id)  =(tr.ftmp_id, tr.fone_id, tr.ftt_schedule_id, ttr.tr_capacity_id)
    )
    select t.tt_driver_id, adata.tm_b, adata.tm_e into l_tt_driver_id, l_tm_b, l_tm_e
    from
      adata
      , lateral ttb.tt_driver_pkg$get_free_driver(p_tt_variant_id, adata.tm_b, adata.tm_e) t
    where adata.tm_b is not null and adata.tm_e is not null
    order by t.tm_e desc
    limit 1;
  end if;
  RAISE NOTICE '-------l_tt_driver_id (%) (%-%)', l_tt_driver_id, l_tm_b, l_tm_e;


  if not p_is_start and not p_is_add_driver then
    --предобработка
    RAISE NOTICE 'предобработка ';
    ---------------------------------------------------------------------------------
    select t.*, vma.is_bn_round_break, vma.is_all_pause into l_rec
    from tt_cube t
      join ttb.vm_action_type vma on vma.action_type_id = t.action_type_id
    where  tt_tr_id =p_tt_tr_id and time_end = p_tm_start
    order by time_end, time_begin
    limit 1;

    RAISE NOTICE ' Действие %', l_rec;
    l_cnt:=0;




    if  l_rec.is_bn_round_break then
      select extract(EPOCH from min(ftm_b) - p_tm_start) into l_dur from tr;

      /*if  l_dur> p_break_round_time then
        RAISE NOTICE 'предобработка межрейс превысил';
        --плохо! межрейс превысил все
        --смотрим может ли он стать отстоем - это потом!!!
        --так не пойдет
        /*l_avg00:= ttb.norm_pkg$get_schedule_tm (p_tt_sсhedule_id =>p_tt_schedule_id
        ,p_ref_group_kind_id => ttb.ref_group_kind_pkg$main_action()::numeric
        );
        l_cnt:= ceil(l_dur/l_avg00) ;
        if l_cnt <1 then l_cnt:=1; end if;*/
        l_res := -1;
      end if;
*/
    elseif l_rec.is_all_pause then
      RAISE NOTICE ' Обед и отстой %', l_rec;

      --select  drs. break_duration_max into   l_max_break_dur from  ttb.dr_shift drs where drs.dr_shift_id = p_dr_shift_id;
      --это обед
      select extract(EPOCH from min(ftm_b) - p_tm_start) into l_dur from tr;

      RAISE NOTICE 'l_break_dur % , l_dur %, tm= %',  l_break_dur, coalesce(l_dur,0), (select min(ftm_b)  from tr);
      if l_break_dur < coalesce(l_dur,0) then
        --случилось страшное: превышено общее время перерывов
        RAISE NOTICE 'предобработка перерыв превысил';
        l_res:=-1;
        if l_rec.dur_from_beg >  asd.get_constant('OPTIMAL_TIME_DINNER')::int then
          --попытаемся исправить, выпустим с минимальной продлжительностью
          RAISE NOTICE 'предобработка выпуск  с минимальным обедом';
          truncate tr;
          /*--Корректируем время обеда*/
          update tt_cube
          set time_end = l_rec.time_begin  + make_interval(secs :=  asd.get_constant('OPTIMAL_TIME_DINNER')::int)
            , dur_from_beg = asd.get_constant('OPTIMAL_TIME_DINNER')::int
            , dur_round    = asd.get_constant('OPTIMAL_TIME_DINNER')::int
          where tt_cube_id = l_rec.tt_cube_id;

          l_res:= ttb."timetable_test_pkg$save_tr"( p_tt_variant_id,
                                               p_tt_schedule_id,
                                               p_tt_tr_id,
                                               l_rec.time_begin  + make_interval(secs :=  asd.get_constant('OPTIMAL_TIME_DINNER')::int),--p_tm_start timestamp,
                                               p_finish_min,
                                               p_finish_max,
                                               p_break_round_time ,
                                               4 ,--p_calc_type ,
                                               p_dr_shift_id,
                                               p_ref_group_kind_id ,
                                               p_start_stop_item_id ,
                                               p_is_start,
                                               p_sh_part,
                                               p_is_add_driver);

        end if;


      end if;

    end if;


  end if;
  ----------------------------------------------------------------------------------
  if l_res=0 and l_tt_driver_id is not null then
    with upd as (
      update  tt_cube
      set tt_tr_id =p_tt_tr_id, dr_shift_id = p_dr_shift_id, tt_driver_id = l_tt_driver_id
      from tr
        join ttb.tt_tr ttr on ttr.tt_tr_id =p_tt_tr_id
      where
        (tt_cube.tmp_id, tt_cube.one_id, tt_cube.tt_schedule_id, tt_cube.tr_capacity_id)  =(tr.ftmp_id, tr.fone_id, tr.ftt_schedule_id, ttr.tr_capacity_id)
      returning *
    )
    select  count(1) into l_res
    from upd;

    if p_is_add_driver then
      --отдаем предыдущий межрейс подменному водителю
      with adata as (
          select cb.tt_schedule_id, cb.tmp_id, cb.one_id
          from tt_cube cb
          where cb.tt_tr_id = p_tt_tr_id
                and  cb.time_end = p_tm_start
          group by cb.tt_schedule_id, cb.tmp_id, cb.one_id
      )
      update tt_cube
      set tt_driver_id = l_tt_driver_id
      from adata
      where tt_cube.tt_tr_id = p_tt_tr_id
            and (tt_cube.tt_schedule_id, tt_cube.tmp_id, tt_cube.one_id) = (adata.tt_schedule_id, adata.tmp_id, adata.one_id)
            and exists (select 1 from ttb.vm_action_type vma where vma.action_type_id = tt_cube.action_type_id and vma.is_bn_round_break);

    end if;
  end if;
  RAISE NOTICE '-------2';

  truncate tr;
  return l_res;
end;
$body$
language plpgsql volatile
cost 100;


------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Добавляем корректировочную составляющую межрейсов
после распределения действий по ТС
***************************************************************************/
create or replace function ttb.timetable_test_pkg$make_tr_bn_corr(in p_tt_schedule_id numeric,
                                                             in p_tt_tr_id numeric
)
  returns void
as
$body$
declare
begin
  RAISE NOTICE 'ttb.timetable_test_pkg$make_tr_bn_corr in p_tt_tr_id ( % )', p_tt_tr_id;
  with
      sch as (
        select id from  unnest(ttb.tt_schedule_pkg$get_schedule_list(p_tt_schedule_id)) id
    )
    , adata as (
      select
          rank() over (partition by t.tt_tr_id, tt_schedule_id, tmp_id, one_id, order_num_action ORDER BY  time_end,  time_begin , tt_at_round_id nulls last) r
        , lag(tt_cube_id) over w as lag_tt_cube_id
        -- , lag(tt_driver_id) over w as lag_tt_driver_id
        , first_value(time_begin) over(partition by t.tt_schedule_id,t.tmp_id,t.one_id, t.tt_schedule_item_id  order by time_begin) as tm_b
        , t.*
      from  tt_cube t
        join ttb.tt_tr ttr on ttr.tt_tr_id = t.tt_tr_id and ttr.tr_capacity_id = t.tr_capacity_id
        join sch on sch.id = t.tt_schedule_id
      where  t.tt_tr_id = p_tt_tr_id
      window w as (partition by t.tt_tr_id ORDER BY  time_end,  time_begin, tt_at_round_id nulls last)
      ORDER BY  time_end,  time_begin
  )
    ,dinner_dur as (
      select dr_shift_id ,
        ttb.timetable_test_pkg$get_dinner_dur(tt_variant_id, tt_tr_id ,dr_shift_id, true ) as max_tm
      from adata
      group by dr_shift_id , tt_variant_id, tt_tr_id ,dr_shift_id
  )
    , bn_corr as (
      select adata.time_end as new_time_end,  act_gr.ref_group_kind_id= ttb.ref_group_kind_pkg$dinner_action() is_dinner
        , ld.*
        , extract (epoch from (adata.time_begin- ld.time_end )) as   calc_dur
        , sum(extract (epoch from (adata.time_begin- ld.time_end ))) filter (where act_gr.ref_group_kind_id= ttb.ref_group_kind_pkg$dinner_action() ) over (partition by adata.dr_shift_id order by adata.tm_b)  as add_tm_dinner
        , vma.is_all_pause
        , dinner_dur.max_tm
      from adata
        join adata ld on ld.tt_cube_id = adata.lag_tt_cube_id
        join ttb.tt_schedule_item sch_it on sch_it.tt_schedule_item_id  = ld.tt_schedule_item_id
        join ttb.tt_action_group act_gr on act_gr.tt_action_group_id = sch_it.tt_action_group_id
        join ttb.vm_action_type vma on vma.action_type_id = ld.action_type_id and vma.is_bn_round_break
        join dinner_dur on dinner_dur.dr_shift_id = ld.dr_shift_id
      where adata.r =1
            and  ld.time_end != adata.time_end
            and  ld.time_end != adata.time_begin

  )
    , for_dinner as (
    --обеденные группы перед межрейсом, продолжительность обеда в ктороых можно увеличить
      select
        t.calc_dur,
        dinner.tt_cube_id  as dinner_tt_cube_id ,
        adata.tt_cube_id  as dg_tt_cube_id ,
        adata.time_begin,
        adata.time_end,
        adata.dur_from_beg,
        adata.dur_round
      from  bn_corr t,
        lateral (select  time_end, time_begin, tt_cube_id   from adata where  adata.tm_b = t.tm_b and adata.action_type_id  = ttb.action_type_pkg$dinner() ) dinner,
        lateral (select * from adata where  adata.tm_b = t.tm_b and adata.time_begin  >= dinner.time_begin and adata.time_end >= dinner.time_end) adata
      where is_dinner
            and t.add_tm_dinner <= t.max_tm
  )

    , upd_cube as  (
    -- увеличиваем обед, сдвигаем все после него без обращения к нормативам
    update tt_cube
    set
      time_begin   = for_dinner.time_begin + make_interval(secs := case for_dinner.dinner_tt_cube_id when  for_dinner.dg_tt_cube_id then 0 else calc_dur end) ,
      time_end     = for_dinner.time_end + make_interval(secs :=  calc_dur  ) ,
      dur_from_beg = for_dinner.dur_from_beg + case for_dinner.dinner_tt_cube_id when for_dinner.dg_tt_cube_id then calc_dur else  0 end ,
      dur_round    = for_dinner.dur_round  + case for_dinner.dinner_tt_cube_id when  for_dinner.dg_tt_cube_id then calc_dur else  0 end
    from for_dinner
    where for_dinner.dg_tt_cube_id = tt_cube.tt_cube_id
  )
  --добавляем корректировочную часть межрейса, если ничего сделать больше нельзя

  insert into tt_cube (stop_item2round_id , tr_capacity_id , tt_variant_id , action_type_id , time_begin ,  time_end
    , tt_at_round_id , dur_from_beg , dur_round , tt_schedule_id , order_num , tt_schedule_item_id
    , tmp_id , round_id , tt_action_type2group_id , is_finish_stop , tr_type_id , tt_tr_id , stop_item_id
    , order_num_action , dr_shift_id , one_id, is_require, tt_driver_id )

    select
      t.stop_item2round_id , t.tr_capacity_id , t.tt_variant_id
      , ttb.action_type_pkg$bn_round_break_corr() as action_type_id
      , t.time_end      as time_begin
      , t.new_time_end  as time_end
      , t.tt_at_round_id
      , t.calc_dur as dur_from_beg
      , t.calc_dur as dur_round
      , t.tt_schedule_id , t.order_num , t.tt_schedule_item_id , t.tmp_id , t.round_id , t.tt_action_type2group_id , t.is_finish_stop , t.tr_type_id , t.tt_tr_id , t.stop_item_id
      , t.order_num_action , t.dr_shift_id , t.one_id, t.is_require, t.tt_driver_id
    from  bn_corr t
    where not is_dinner
          or t.add_tm_dinner > t.max_tm
  ON CONFLICT ON CONSTRAINT  uq_tt_cube DO NOTHING;


end;

$body$
language plpgsql volatile
cost 100;

------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- набор действий, подходящих фильтрам
  p_tt_variant_id вариант расписания
in p_tt_tr_id ТС
in p_tm_start время начала
in p_tm_finish_min время окончания min
in p_tm_finish_max время окончания max
in p_break_round_time межрейсовый интервал
in p_calc_type тип вывода : 1 - начиная с первого времени, 2 - использую максимальное количество действий)
in p_ref_group_kind_id - следующая группа действий расписания
in p_start_stop_item_id - ОП, с которого стартуем
in p_is_start boolean,  - начала выхода
in p_sh_part numeric,  - часть внутри смены
in p_break_dur numeric   - допустимое увеличение для перерыва
***************************************************************************/

create or replace function ttb.timetable_test_pkg$prepare_tr(in p_tt_variant_id numeric,
                                                        in p_tt_schedule_id numeric,
                                                        in p_tt_tr_id numeric,
                                                        in p_tm_start timestamp,
                                                        in p_finish_min numeric,
                                                        in p_finish_max numeric,
                                                        in p_break_round_time  numeric,
                                                        in p_calc_type  numeric,
                                                        in p_ref_group_kind_id numeric,
                                                        in p_start_stop_item_id numeric,
                                                        in p_is_start boolean,
                                                        in p_sh_part numeric,
                                                        in p_break_dur numeric

)
  returns TABLE(
    ftmp_id integer,
    fone_id integer,
    ftt_schedule_id integer,
    ftm_b timestamp)
as
$body$
declare
  l_is_has_child boolean:= true;

begin
  if p_sh_part = 2 and p_ref_group_kind_id in (ttb.ref_group_kind_pkg$last_action() , ttb.ref_group_kind_pkg$change_dr_action()) then
    p_sh_part := 3;
  end if;
  --определим возможность выполнения дочерних последовательностей выходом
  with adata as (
      select sch  as tt_schedule_id
      from  unnest(ttb.tt_schedule_pkg$get_schedule_list(p_tt_schedule_id)) sch
      where  sch != p_tt_schedule_id
  )
  select
    coalesce(

        (select max((select  sum(out_cnt)  from ttb.tt_period pd where pd.tt_schedule_id = p.tt_schedule_id and pd.time_begin<=p.time_begin))  as out_cnt
         from  adata
           join ttb.tt_period p on p.tt_schedule_id = adata.tt_schedule_id
         where tsrange(p_tm_start, p_tm_start+make_interval(secs:=p_finish_max))&&  tsrange(p.time_begin, p.time_end)
        )
        >
        (select count (distinct tt_tr_id) cnt
         from tt_cube cb
           join adata on adata.tt_schedule_id = cb.tt_schedule_id
         where cb.tt_variant_id = p_tt_variant_id
               and cb.tt_tr_id is not null
               and cb.tt_tr_id !=p_tt_tr_id )
        , false)
  into l_is_has_child;



  RAISE NOTICE '-------$$prepare_tr  new  -----------
                in p_tt_variant_id = %
                in p_tt_schedule_id = %
                in p_tt_tr_id = __________________%
                in p_tm_start  = %
                in p_finish_min  = %
                in p_finish_max = %
                in p_break_round_time  =%
                in p_calc_type   = %
                in p_ref_group_kind_id  =%
                in p_start_stop_item_id  =%
                in p_is_start =%
                l_is_has_child = %
                in p_sh_part =%
                in p_break_dur = %',

  p_tt_variant_id,
  p_tt_schedule_id,
  p_tt_tr_id,
  p_tm_start,
  p_finish_min,
  p_finish_max,
  p_break_round_time,
  p_calc_type,
  p_ref_group_kind_id,
  p_start_stop_item_id,
  p_is_start,
  l_is_has_child,
  p_sh_part,
  p_break_dur;

  return query
  EXECUTE

  'with recursive cb as (  '
  || '  '
  || '    select  '
  || '       tt_schedule_id::text ||''-''|| cb.tmp_id::text ||''-''|| cb.one_id::text  as id  '
  || '      ,cb.one_id  '
  || '      ,cb.tmp_id  '
  || '      ,cb.parent_one_id  '
  || '      ,cb.stop_item_id  '
  || '      ,cb.time_begin  '
  || '      ,cb.time_end  '
  || '      ,cb.action_type_id  '
  || '      ,cb.is_require  '
  || '      ,cb.tt_schedule_id  '
  || '      ,cb.is_intvl  '
  || '      ,cb.tr_type_id  '
  || '      ,first_value(cb.stop_item_id) over(partition by cb.tt_schedule_id ,cb.tmp_id , cb.one_id order by cb.time_end, cb.time_begin ) as sp_b  '
  || '      ,first_value(cb.stop_item_id) over(partition by cb.tt_schedule_id ,cb.tmp_id , cb.one_id order by cb.time_end desc,  cb.time_begin desc) as sp_e  '
  || '      ,vma.is_production_round::int as has_rnd  '
  || '      ,vma.is_bn_round_break as is_bn_round_break  '
  || '    from tt_cube cb  '
  || '      join ttb.tt_tr on cb.tr_capacity_id =tt_tr.tr_capacity_id and  tt_tr.tt_tr_id = $4  '
  || '      join unnest(ttb.tt_schedule_pkg$get_schedule_list( $2 )) sch on sch = cb.tt_schedule_id  '
  || '      join ttb.vm_action_type vma on vma.action_type_id = cb.action_type_id  '
  || '    where  '
  || '      cb.tt_variant_id = $1  '
  || '      and ($3 or cb.tt_schedule_id = $2)  '
  || '      and (cb.is_finish_stop or cb.order_num =1)  '
  || '      and cb.tt_tr_id is null  '
  || ') '
  --|| '  --уже распределенное по ТС время  '
  || '  ,ex_tr as (  '
  || '    select time_begin, stop_item_id  '
  || '    from  tt_cube cb  '
  || '      join ttb.vm_action_type vma on vma.action_type_id = cb.action_type_id and vma.is_production_round  '
  || '    where  '
  || '      tt_schedule_id = $2   '
  || '      and cb.order_num = 1  '
  || '      and tt_tr_id is not null  '
  || ') '
  --|| '  --уже распределенное по ТС время на межрейсы (для устранения перехлеста проводов)  '
  || '  ,ex_tr_bn as (  '
  --|| '  --группируем межрейсы из частичек  '
  || '    select stop_item_id  ,  '
  || '      min (time_begin)  as time_begin,  '
  || '      max (time_end) as time_end  '
  || '    from(  '
  || '          select   t.*,  sum((t.time_end = lead_r or time_begin !=lag_r)::int) over (partition by tt_tr_id, stop_item_id order by time_end, time_begin) rs  '
  || '          from(  '
  || '                select time_begin, time_end ,stop_item_id, tt_tr_id, lead(time_begin, 1 , time_end) over (partition by tt_tr_id, stop_item_id order by time_end, time_begin  ) as lead_r  '
  || '                  , lag(time_end, 1 , time_begin) over (partition by tt_tr_id, stop_item_id order by time_end, time_begin  ) as lag_r  '
  || '                from  tt_cube cb  '
  || '                  join ttb.vm_action_type vma on vma.action_type_id = cb.action_type_id and vma.is_bn_round_break  '
  || '                where  '
  || '                  tt_schedule_id = $2    '
  || '                  and tt_tr_id is not null  '
  || '              ) t  '
  || '        ) d  '
  || '    group by stop_item_id  ,   tt_tr_id , rs  '
  || ')  '
  || '  '
  || '  '
  || '  , tt_act_group_stop as (  '
  || '    select distinct sch,  '
  || '                    first_value(fstop_item_id) over (partition by sch order by forder_num) as stop_item_id  '
  || '    from unnest(ttb.tt_schedule_pkg$get_schedule_list($2)) sch  '
  || '         , lateral ttb.timetable_test_pkg$get_ref_group_kind_data (sch, $10) r '
  || '    where  faction_type_id   != ttb.action_type_pkg$bn_round_break()  '
  || ')  '
  || '  , last_rnd as (  '
  || '  '
  || '    select atg.start_time,  sir.stop_item_id  '
  || '    from ttb.atgrs_time atg  '
  || '      join ttb.tt_at_round_stop ars on ars.tt_at_round_stop_id = atg.tt_at_round_stop_id  '
  || '      join ttb.tt_at_round ar on ar.tt_at_round_id = ars.tt_at_round_id  '
  || '      join ttb.tt_action_type tact on tact.tt_action_type_id = ar.tt_action_type_id  '
  || '      join rts.stop_item2round sir on sir.stop_item2round_id = ars.stop_item2round_id and not sir.sign_deleted  '
  || '    where atg.fix_arrival_type_id = ttb.fix_arrival_type_pkg$finish_time()  '
  || '          and sir.order_num =1  '
  || '          and tact.tt_variant_id = $1  '
  || ')  '
  --|| '  --период последовательности  '
  || '  , per as (  '
  || '    select  time_begin as tm_b,  time_end as tm_e, interval_time as int_tm from  ttb.tt_period p where   p.tt_schedule_id = $2  '
  || ')  '
  || '  , adata as (  '
  || '    select  '
  || '      cb.one_id  '
  || '      ,cb.tmp_id  '
  || '      ,cb.id  '
  || '      ,min(cb.time_begin) as time_b  '
  || '      ,max(cb.time_end) as time_e  '
  || '      ,max(cb.time_end) filter (where not cb.is_bn_round_break) as time_e_rnd  '
  || '      ,sp_b  '
  || '      ,sp_e  '
  || '      ,parent_one_id  '
  || '      ,is_require  '
  || '      ,is_intvl  '
  || '      ,cb.tt_schedule_id  '
  || '      ,cb.tr_type_id  '
  || '      ,(cb.tt_schedule_id = $2)  as is_my'
  --|| '      --приоритет для дочерних последовательностей (инверсия)  '
  || '      ,(cb.tt_schedule_id = $2)  '
  --|| '       --приоритет последнего рейса  '
  || '       and coalesce((select 1 from  last_rnd where last_rnd.stop_item_id = sp_b and last_rnd.start_time= min(cb.time_begin)  ),0)= 1 as is_main  '
  || '      , $5 as tm_start  '
  || '      , min(p.int_tm) as int_tm  '
  --|| '      --получаем секунды между ближайшими рейсами  '
  || '      ,coalesce((select abs(extract(epoch from  min(ex_tr.time_begin-min(cb.time_begin))))  from  ex_tr  '
  || '    where stop_item_id = cb.sp_b  '
  || '          and min(cb.time_begin) between ex_tr.time_begin - make_interval(secs:=min(p.int_tm))  '
  || '          and ex_tr.time_begin + make_interval(secs:= min(p.int_tm))), 99999) as delta  '
  || '  '
  || '    from cb  '
  || '      , lateral (select  round(min(int_tm))-1 as int_tm from  per where  cb.time_begin between per.tm_b and per.tm_e) p  '
  || '    group by cb.one_id  '
  || '      ,cb.tmp_id  '
  || '      ,cb.id  '
  || '      ,sp_b  '
  || '      ,sp_e  '
  || '      ,parent_one_id  '
  || '      ,is_require  '
  || '      ,is_intvl  '
  || '      ,cb.tt_schedule_id  '
  || '      ,cb.tr_type_id  '
  --|| '    --ограничиваем выборку для быстродействия  '
  || '    having  min(cb.time_begin) between  $5 and $5 + ($7 +$8 + $14) * make_interval (secs:=1) '
  || '            and sum (has_rnd) >0  '
  || '    order by 4  '
  || ')  '
  --|| '  --пункты выравниывния  '
  || '  , sp_alig as (  '
  || '    select fstop_item_id from  ttb.timetable_test_pkg$get_stop_interval_alignment($1) t  '
  || ')  '
  || '  , min_tm as (  '
  || '  select $5 tm where  $12  '
  || '  union all  '
  || '  (  '
  || '    select time_b tm  '
  || '    from adata  '
  || '    where sp_b = $11  '
  || '          and not  $12  '
  || '          and  adata.parent_one_id is null  '
  --|| '          --проверка на то чтобы начало не превышало время, на которое можно перерыв увеличивать  '
  --|| '          --после пересменки не более получаса просто так поставила  '
  || '          and ( time_b between  $5 and  $5 +  make_interval(secs:= case $13 when 1 then $8/2 else  case when exists(select 1 from ttb.tt_variant where tt_variant_id = $1 and is_change_driver ) then $8 else $14 end end))  '
  || '          and  not exists ( select 1 from tt_cube  t'
  || '                            where t.stop_item_id = adata.sp_b  and t.order_num =1 and t.tt_tr_id is not null'
  || '                              and adata.tm_start = t.time_begin  '
  || '               )  '

  --|| '          --проверка на перехлест проводов  пока отключаю
  --|| '          and ( $13 > 1  '
  --отключаю!!!! пример 6-БК 211 выход ломается нужно понять как поступать в таких ситуациях
  || '          and ( 1= 1  '
  || '                or tr_type_id =1  '
  || '                or  not exists ( select 1 from ex_tr_bn  '
  || '    where stop_item_id = adata.sp_b  '
  || '          and ((adata.tm_start between ex_tr_bn.time_begin and ex_tr_bn.time_end  '
  || '                and  adata.time_b< ex_tr_bn.time_end  '
  || '               ) or  '
  || '               ( adata.tm_start >= ex_tr_bn.time_begin  '
  || '                 and adata.time_b between ex_tr_bn.time_begin and ex_tr_bn.time_end  '
  || '               )  '
  || '               or  '
  || '               ( adata.tm_start < ex_tr_bn.time_begin  '
  || '                 and adata.time_b > ex_tr_bn.time_end  '
  || '               )  '
  || '          )  '
  || '    )  '
  || '          )  '
  || '    order by  '
  --|| '      --укороты вперед!  '
  || '      adata.is_my  '
  --|| '      --у пересменки не проверяем  '
  || '    , (case $13 when 1 then 1 else delta end)  desc '
  --|| '    ,  delta desc  '
  || '      , not (adata.is_require or adata.is_intvl or adata.is_main or $13 = 3), time_b  '
  || '    limit 1  '
  || '  '
  || '  )  '
  || ')  '
  || '  ,r1 as  (  '
  || '    select  '
  || '                row_number() over w as r  '
  || '      , d1.time_b tm , d2.time_b as next_tm  '
  || '      , d1.id , d2.id  as next_id  '
  || '      , d1.tmp_id, d1.one_id  '
  || '      , d2.time_b-d1.time_b as dur  '
  || '      , d1.sp_b,  d1.sp_e  '
  || '      , d1.time_e_rnd-d1.time_b as dur_rnd  '
  || '      , d2.time_b- d1.time_e_rnd as bn  '
  || '      , d1.time_e time_e_rnd  '
  || '      , d2.tt_schedule_id  '
  || '      , d1.is_my  '
  || '    from adata d1  '
  || '      , adata d2  '
  || '    where d1.time_b >= (select tm from min_tm)  '
  || '          and d1.sp_e = d2.sp_b  '
  || '          and  d2.time_b  between d1.time_e and  d1.time_e + case (select count(1) from sp_alig  where fstop_item_id = d1.sp_e) when 0 then  interval ''0 second'' else $8* make_interval (secs:=1)  - (d2.time_b- d1.time_e_rnd) end  '
  || '          and (d2.parent_one_id = d1.one_id or d2.parent_one_id is null)  '
  --|| '          --если это пункт выравнивания проверяем чтобы не накладывались рейсы друг на друга  '
  || '          and ((select count(1) from sp_alig  where fstop_item_id = d1.sp_e) = 0  '
  || '               or not d1.is_my  '
  || '               or d1.tr_type_id =1  '
  || '               or(  '
  || '                 not exists ( select 1 from ex_tr_bn  '
  || '                 where stop_item_id = d2.sp_b  '
  || '                       and (  '
  || '                         (d1.time_e_rnd between ex_tr_bn.time_begin and ex_tr_bn.time_end  '
  || '                          and d2.time_b< ex_tr_bn.time_end) or  '
  || '  '
  || '                         ( d1.time_e_rnd >= ex_tr_bn.time_begin  '
  || '                           and d2.time_b between ex_tr_bn.time_begin and ex_tr_bn.time_end  '
  || '                         )  '
  || '                         or  '
  || '                         ( d1.time_e_rnd < ex_tr_bn.time_begin  '
  || '                           and d2.time_b > ex_tr_bn.time_end  '
  || '                         )  '
  || '                       )  '
  || '                 )  '
  || '               )  '
  || '  '
  || '          )  '
  || '  '
  || '    window w as (partition by d1.id  '
  || '      order by  '
  --|| '        --укороты вперед!  '
  || '         d2.is_my , d1.is_my  '
  --|| '        --подальше от других рейсов  '
  || '        ,d2.delta desc  '
  || '        ,d1.delta desc  '
  --|| '        --если из-за межрейса пропускаем интервальный старт рейса, то снимаем ограничения обязательности  '
  || '        ,   not ( not exists (select 1 from adata d_inbn where d1.sp_e = d_inbn.sp_b  '
  || '                                                               and  d_inbn.time_b  between d1.time_e_rnd and d1.time_e  '
  || '                                                               and  coalesce(d_inbn.is_intvl, false)  '
  || '                                                               and (d_inbn.parent_one_id = d1.one_id or d_inbn.parent_one_id is null))  '
  || '                  or d2.is_require or d2.is_intvl or d2.is_main)  '
  || '        , d2.time_b )  '
  || '  '
  || ') '
  || '  '
  || '  ,r as (  '
  || '  select  * from r1 where r=1  '
  || '  union all  '
  --|| '  --захватываем последние рейсы  '
  || '  select    2 as r  '
  || '    , adata.time_b tm , adata.time_e as next_tm  '
  || '    , adata.id , null  as next_id  '
  || '    , adata.tmp_id, adata.one_id  '
  || '    , adata.time_e-adata.time_b as dur  '
  || '    , adata.sp_b,  adata.sp_e  '
  || '    , adata.time_e_rnd-adata.time_b as dur_rnd  '
  || '    , adata.time_e- adata.time_e_rnd as bn  '
  || '    , adata.time_e_rnd  '
  || '    , adata.tt_schedule_id  '
  || '    , adata.is_my  '
  || '  from r1  '
  || '    join adata on adata.id = r1.next_id  '
  || '  where r1.r < 2  '
  || '        and not exists (select 1 from r1 t_r where t_r.id = r1.next_id and t_r.r=1)  '
  || '  '
  || ')  '
  || '  '
  || '  '
  --|| '  --цикл для нахождения последовательности  '
  || '  , m (tm, next_tm,id, next_id, tmp_id, one_id,  dur, bn, time_e_rnd, sp_b, sp_e,  tt_schedule_id,  is_my, level, path, path2, cycle ) as(  '
  || '  select  tm, next_tm,r.id, next_id, tmp_id, one_id,   dur, bn, time_e_rnd, sp_b, sp_e, tt_schedule_id, (is_my)::int,  1, ARRAY[r.tm], ARRAY[r.id], false  '
  || '  from r  '
  || '  union all  '
  || '  select r.tm, r.next_tm,r.id, r.next_id, r.tmp_id, r.one_id, r.dur + m.dur, r.bn, r.time_e_rnd,  m.sp_b, r.sp_e, r.tt_schedule_id, r.is_my::int + m.is_my as is_my, m.level + 1,  path || r.tm, path2 || r.id,  r.tm = ANY(path)  '
  || '  from m  '
  || '    join r on r.tm= m.next_tm and r.id= m.next_id AND NOT cycle  '
  || '    where m.level<30  '
  || ')  '
  --|| '  -- получаем возможные последовательности  '
  || '  ,prev_itog as  (  '
  || '    select  '
  || '        m.path f_tm  '
  || '      ,m.path2 f  '
  || '      ,(select tm from min_tm) as min_tm  '
  || '      ,m.*  '
  --|| '      --межрейса нет до обеда на КП и до пересменки  '
  --|| '      --продолжительность произв. рейса до обеда  '
  || '      , case when $10!=ttb.ref_group_kind_pkg$last_action() then bn else interval ''0'' end as dur_bn  '
  || '    from m  '
  || '    where  exists (select stop_item_id  from tt_act_group_stop where sch = m.tt_schedule_id and stop_item_id = m.sp_e)  '
  || '           and sp_b = $11  '
  || ')  '
  || '  ,itog as  (  '
  || '    select *  '
  || '    from prev_itog  '
  || '    where dur - bn '
  || '    between $6 * make_interval (secs:=1) and  $7 * make_interval (secs:=1)  '
  || ')  '
  || '  '
  || 'select adata.tmp_id::int, adata.one_id::int, adata.tt_schedule_id::int,  adata.time_b::timestamp  '
  || 'from unnest(array (select f  '
  || '                   from itog  '
  || '                   where   array_length(itog.f,1) >0 and  '
  || '                           ((($12) and array_position(path, min_tm)=1)  '
  || '                            or  ($13 = 1 and not $12 and path[1] between min_tm and min_tm+ make_interval (secs:=$8/2))  '
  || '                            or (($13 != 1 and not $12)  '
  || '                                and path[1] between min_tm and min_tm+ make_interval (secs:=least ($8, $14-extract(epoch from (min_tm- $5))) )))  '
  || '                   order by  cardinality(f_tm) desc, is_my,  path[1], dur desc limit 1 )) f  '
  || '  join adata on adata.id = f.f  '
  || 'where $9 = 1   '
  || ' union   '
  || 'select adata.tmp_id::int, adata.one_id::int, adata.tt_schedule_id::int,  adata.time_b::timestamp  '
  || 'from unnest(array (select f  '
  || '                   from itog  '
  || '                   where   array_length(itog.f,1) >0 and  '
  || '                           (( $12 and array_position(path, min_tm)=1)  '
  || '                            or ($13 = 1 and not $12 and path[1] between min_tm and min_tm+ make_interval (secs:=$8/2))  '
  || '                            or (($13 != 1 and not $12)  '
  || '                                and path[1] between min_tm and min_tm+ make_interval (secs:=least ($8, $14-extract(epoch from (min_tm- $5))) )))  '
  || '                   order by  cardinality(f_tm) , is_my, path[1], dur desc limit 1 offset 1 )) f  '
  || '  join adata on adata.id = f.f  '
  || 'where $9 = 2   '
  || 'union all  '
  || 'select adata.tmp_id, adata.one_id, adata.tt_schedule_id,  adata.time_b  '
  || 'from  unnest(array (select f from itog where   array_length(itog.f,1) >0 and f_tm=path order by cardinality(f_tm) desc  limit 1) ) f  '
  || '  join adata on adata.id = f.f  '
  || 'where  $9 =3  '
  || 'union all  '
  || 'select adata.tmp_id::int, adata.one_id::int, adata.tt_schedule_id::int,  adata.time_b::timestamp  '
  || 'from  unnest(array (select f from itog where  (select path from itog where   array_length(itog.f,1) >0 order by  cardinality(f_tm) desc limit 1)<@ path  limit 1) ) f  '
  || '  join adata on adata.id = f.f  '
  || 'where  $9 =4  '
  || ' union   '
  || 'select adata.tmp_id::int, adata.one_id::int, adata.tt_schedule_id::int,  adata.time_b::timestamp  '
  || 'from unnest(array (select f  '
  || '                   from itog  '
  || '                   where   array_length(itog.f,1) >0 and  '
  || '                           ((($13 = 1 or $12) and array_position(path, min_tm)=1)  '
  || '                            or (($13 != 1 and not $12)  '
  || '                                and path[1] between min_tm and min_tm+ make_interval (secs:=600)))  '
  || '                   order by  cardinality(f_tm) , is_my, dur  limit 1 )) f  '
  || '  join adata on adata.id = f.f  '
  || 'where $9 = 22   '

  using
    p_tt_variant_id::int,
    p_tt_schedule_id::int,
    l_is_has_child::bool,
    p_tt_tr_id::int,
    p_tm_start::timestamp,
    p_finish_min::numeric,
    p_finish_max::numeric,
    p_break_round_time::numeric,
    p_calc_type::int,
    p_ref_group_kind_id::int,
    p_start_stop_item_id::int,
    p_is_start::bool,
    p_sh_part::int,
    p_break_dur::int

  ;
  RETURN;
end;
$body$
language plpgsql volatile
cost 20
rows 20
;

------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Перенос в окончательные структуры
***************************************************************************/

create or replace function ttb.timetable_test_pkg$make_tt_out(p_tt_variant_id numeric,
                                                         p_tt_tr_id numeric)
  returns void
as
$body$
declare
begin

  with my_out as (
    insert into  ttb.tt_out(tt_variant_id , tr_capacity_id , mode_id , tt_out_num )
      select cb.tt_variant_id, cb.tr_capacity_id, m.mode_id , substring(m.mode_name,1,1)::int*100 + ttp.tt_tr_order_num as out_num
      from tt_cube cb
        join ttb.tt_tr ttr on ttr.tt_tr_id = cb.tt_tr_id
        join ttb.mode m on m.mode_id = ttr.tr_mode_id
        join ttb.tt_tr_period ttp on ttp.tt_tr_id = ttr.tt_tr_id
        join ttb.tt_period p on p.tt_period_id = ttp.tt_period_id
        join ttb.tt_schedule s on s.tt_schedule_id = p.tt_schedule_id and s.tt_variant_id = cb.tt_variant_id
      where ttr.tt_tr_id = p_tt_tr_id
      group by cb.tt_variant_id, cb.tr_capacity_id, m.mode_id , m.mode_name, ttp.tt_tr_order_num
    returning  *
  )

    , my_tt_out2tt_tr as (
    insert into  ttb.tt_out2tt_tr(tt_out_id, tt_tr_id)
      select tt_out_id, p_tt_tr_id
      from my_out
    --where not exists (select 1 from ttb.tt_out2tt_tr t where (t.tt_out_id, t.tt_tr_id) = (my_out.tt_out_id, p_tt_tr_id))
  )
    , my_data as (
      select
          t.tt_schedule_id||'-'||t.tmp_id||'-'||t.one_id||'-'||sch_it.tt_action_group_id as id
        ,case when vma.is_bn_round_break then ttb.action_type_pkg$bn_round_break() else t.action_type_id end as new_action_type_id
        , t.*
        ,sch_it.tt_action_group_id
        ,vma.is_round
        , first_value(time_begin) over(partition by t.tt_schedule_id,t.tmp_id,t.one_id,sch_it.tt_action_group_id order by time_end, time_begin, at2g.order_num) as tm_b
      from   tt_cube t
        join ttb.tt_tr ttr on ttr.tt_tr_id = t.tt_tr_id and t.tr_capacity_id = ttr.tr_capacity_id
        join ttb.tt_schedule_item sch_it on sch_it.tt_schedule_item_id  = t.tt_schedule_item_id
        join ttb.tt_action_type2group at2g on at2g.tt_action_type2group_id = t.tt_action_type2group_id
        join ttb.vm_action_type vma on vma.action_type_id = t.action_type_id
      where  t.tt_tr_id = p_tt_tr_id
  )
    , gr as (select
               sum(d.start_of_group) over (partition by d.tt_variant_id  order by tm_b,order_num_action) rn
               , d.*
             from (
                    select  case lag(my_data.new_action_type_id) over ( partition by my_data.tt_variant_id order by my_data.tm_b,order_num_action  ) when my_data.new_action_type_id then 0 else 1 end as start_of_group
                      ,my_data.*
                    from my_data
                  ) d
  )
    ,my_action as (
    insert into  ttb.tt_action( tt_out_id, action_dur, dt_action, action_type_id, tt_action_group_id, action_gr_id, action_range )
      select  my_out.tt_out_id, coalesce(sum(dur_from_beg),0) as action_dur, min(time_begin)  as dt_action ,  my_data.new_action_type_id
        , my_data.tt_action_group_id
        -- , dense_rank() over(order by tm_b) as action_gr_id
        , my_data.rn as action_gr_id
        , tsrange(min(time_begin) , min(time_begin) + make_interval(secs:= sum(dur_from_beg)),'[)' ) as action_range
      from my_out
        join gr my_data on my_out.tt_variant_id = my_data.tt_variant_id
      --where not exists (select 1 from ttb.tt_action t where t.tt_out_id = my_out.tt_out_id and not t.sign_deleted)
      group by  my_out.tt_out_id,my_data.new_action_type_id , my_data.new_action_type_id, my_data.tt_action_group_id,my_data.rn
    returning *
  )
    ,my_round as (
    insert into ttb.tt_action_round (tt_action_id, round_id)
      select act.tt_action_id,  d.round_id
      from my_action act
        join my_data d on d.time_begin  = act.dt_action
                          and d.action_type_id = act.action_type_id
        join my_out tout on tout.tt_out_id = act.tt_out_id
      where  d.is_round
      --      and not exists (select 1 from ttb.tt_action_round t where (t.tt_action_id,  t.round_id) = (act.tt_action_id,  d.round_id))
      group by act.tt_action_id,  d.round_id
  )
    ,my_driver as (
    insert into ttb.tt_driver_action (tt_action_id, tt_driver_id)
      select act.tt_action_id,  gr.tt_driver_id
      from my_action act
        join gr  on gr.rn = act.action_gr_id
      where gr.tt_driver_id is not null
      group by act.tt_action_id,  gr.tt_driver_id

  )

  insert into ttb.tt_action_item( tt_action_id , time_begin , time_end , stop_item2round_id , action_type_id, dr_shift_id)
    select act.tt_action_id , d.time_begin , coalesce(d.time_end,d.time_begin) , d.stop_item2round_id , d.action_type_id, d.dr_shift_id
    from my_action act
      join my_data d on  d.new_action_type_id = act.action_type_id
                         and d.tt_action_group_id = act.tt_action_group_id
                         and d.time_begin >= act.dt_action
                         and d.time_end <=dt_action + make_interval(secs:=action_dur)
    order by 2

  ;

end;

$body$
language plpgsql volatile
cost 100;


------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Проставляем связи
***************************************************************************/

create or replace function ttb.timetable_test_pkg$make_tt_out_switch (p_tt_variant_id numeric,
                                                                 p_tt_tr_id numeric)
  returns void
as
$body$
declare
begin

  with sw as(
      select s.tt_variant_id , v.tr_capacity_id, v.mode_id , substring(v.mode_name,1,1)::int*100 + v.order_num as out_num , max(v.tt_variant_id_fr) tt_variant_id_fr
      from ttb.v_tt_tr_shift v
        join ttb.tt_schedule  s on s.tt_schedule_id = v.tt_schedule_id
        join ttb.tt_switch_pkg$get_tt_variant_bound(p_tt_variant_id) vb on vb.tt_schedule_id = s.tt_schedule_id
      where  v.tt_tr_id =  p_tt_tr_id
      group by  s.tt_variant_id , v.tr_capacity_id, v.mode_id , substring(v.mode_name,1,1)::int*100 + v.order_num
  )

  update ttb.tt_out
  set parent_tt_out_id =  my_out.tt_out_id

  from sw
    join sw sparent on sparent.tt_variant_id = sw.tt_variant_id_fr
    join ttb.tt_out my_out on (my_out.tt_variant_id, my_out.tr_capacity_id, my_out.mode_id, my_out.tt_out_num) = (sparent.tt_variant_id , sparent.tr_capacity_id, sparent.mode_id , sparent.out_num )
  where    (tt_out.tt_variant_id, tt_out.tr_capacity_id, tt_out.mode_id, tt_out.tt_out_num) =
           (sw.tt_variant_id , sw.tr_capacity_id, sw.mode_id , sw.out_num )
           and not tt_out.sign_deleted
           and  not  my_out.sign_deleted;

end;

$body$
language plpgsql volatile
cost 100;

------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Удалить межрейсовую стоянку из предыдущего действия, считаем, что предыдущее действие не содержит технологическо
***************************************************************************/
create or replace function ttb.timetable_test_pkg$delete_bn_before_dinner(p_tt_schedule_id numeric,
                                                                     p_tt_tr_id numeric,
                                                                     p_one_id numeric,
                                                                     p_tmp_id numeric)
  returns void
as
$body$
declare
  l_cnt numeric;
begin
  RAISE NOTICE '--delete_bn_before_dinner p_tt_tr_id= %-----p_one_id=%, p_tmp_id=%', p_tt_tr_id,  p_one_id, p_tmp_id ;

  with t as (
      select
        vma.is_bn_round_break as is_bn,
        t.time_end, t.time_begin, t.tt_cube_id
      from  tt_cube t
        join  ttb.tt_tr ttr on ttr.tt_tr_id = t.tt_tr_id and t.tr_capacity_id = ttr.tr_capacity_id
        join unnest(ttb.tt_schedule_pkg$get_schedule_list(p_tt_schedule_id)) sch on sch = t.tt_schedule_id
        join ttb.vm_action_type vma on vma.action_type_id = t.action_type_id
      where ttr.tt_tr_id = p_tt_tr_id  and t.one_id=p_one_id and t.tmp_id = p_tmp_id)
    , adata as (
      select
        row_number() over(order by time_end desc, time_begin desc) -
        row_number() over(partition by is_bn order by time_end desc, time_begin desc) rn
        ,t.* from t
      order by time_end desc
  )
    , del as (
    --delete from tt_cube
    update tt_cube
    set tt_tr_id = null
    where tt_cube_id in ( select tt_cube_id from adata where rn = 0 and is_bn)
    returning *
  )
  select count(*) into l_cnt from del;

  RAISE NOTICE 'delete l_cnt=%', l_cnt;
end;

$body$
language plpgsql volatile
cost 100;

------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Вернуть межрейсовую стоянку из предыдущего действия, считаем, что предыдущее действие не содержит технологическо
***************************************************************************/
create or replace function ttb.timetable_test_pkg$restore_bn(p_tt_schedule_id numeric,
                                                        p_tt_tr_id numeric,
                                                        p_one_id numeric,
                                                        p_tmp_id numeric,
                                                        p_round_id numeric)
  returns void
as
$body$
declare
  l_cnt numeric;
begin
  RAISE NOTICE '--restore_bn p_tt_schedule_id  =%, p_tt_tr_id= %-----p_one_id=%, p_tmp_id=%,  p_round_id =%', p_tt_schedule_id , p_tt_tr_id,  p_one_id, p_tmp_id,  p_round_id  ;

  with upd as (
    update tt_cube
    set tt_tr_id = p_tt_tr_id
    from (    select
                t.tt_cube_id
              from  tt_cube t
                join unnest(ttb.tt_schedule_pkg$get_schedule_list(p_tt_schedule_id)) sch on sch = t.tt_schedule_id
                join ttb.tt_tr on tt_tr.tt_tr_id = p_tt_tr_id and tt_tr.tr_capacity_id = t.tr_capacity_id
                join ttb.vm_action_type vma on vma.action_type_id = t.action_type_id and vma.is_bn_round_break
              where t.tt_tr_id is null
                    and t.one_id = p_one_id
                    and t.tmp_id = p_tmp_id
                    and t.round_id = p_round_id
         ) t
    where tt_cube.tt_cube_id =t.tt_cube_id
          and exists (select 1 from tt_cube cb where cb.tt_tr_id = p_tt_tr_id and cb.one_id = p_one_id and cb.tmp_id = p_tmp_id)
    returning *
  )
  select count(*) into l_cnt from upd;

  RAISE NOTICE 'restore l_cnt=%', l_cnt;
end;

$body$
language plpgsql volatile
cost 100;



------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- данные по группе действий
    ftt_schedule_item_id   bigint,
    ftt_action_type_id integer  - тип действия группы
    fref_group_kind_id     smallint, -- тип последовательности
    fstop_item_id          integer -- список ОП, с которого начнется последовательность
    faction_type_id    smallint тип первого действия группы
***************************************************************************/
create or replace function ttb."timetable_test_pkg$get_ref_group_kind_data" (in p_tt_schedule_id       numeric,
                                                                        in p_ref_group_kind_id    numeric,
                                                                        in p_tt_schedule_item_id  numeric default null)
  returns TABLE(
    ftt_schedule_item_id   bigint,
    ftt_action_type_id integer,
    fref_group_kind_id     smallint,
    fstop_item_id          integer,
    faction_type_id        smallint,
    forder_num             smallint
  )
as
$body$
declare
begin
  return query
  with gr as (
      select
        at2g.tt_action_type_id
        ,sch_it.tt_schedule_item_id
        ,act_gr.tt_action_group_name
        ,act_gr.ref_group_kind_id
        ,at2g.order_num
      from  ttb.tt_schedule_item sch_it
        join ttb.tt_action_group act_gr on act_gr.tt_action_group_id = sch_it.tt_action_group_id
        join ttb.tt_action_type2group at2g on at2g.tt_action_group_id = act_gr.tt_action_group_id
        join unnest(ttb.tt_schedule_pkg$get_schedule_list(p_tt_schedule_id)) sch on sch = sch_it.tt_schedule_id
      where p_tt_schedule_item_id is null or sch_it.tt_schedule_item_id = p_tt_schedule_item_id
  )
  select distinct
    gr.tt_schedule_item_id ,
    gr.tt_action_type_id,
    gr.ref_group_kind_id,
    coalesce(rsir.stop_item_id, tsir.stop_item_id) as stop_item_id,
    att.action_type_id,
    gr.order_num
  from gr
    join ttb.tt_action_type att on att.tt_action_type_id = gr.tt_action_type_id
    left join ttb.tt_at_stop tsp on tsp.tt_action_type_id = att.tt_action_type_id
    left join rts.stop_item2round tsir on tsir.stop_item2round_id = tsp.stop_item2round_id and not tsir.sign_deleted-- and tsir.order_num =1
    left join ttb.tt_at_round trnd on trnd.tt_action_type_id = att.tt_action_type_id
    left join ttb.tt_at_round_stop rsp on rsp.tt_at_round_id= trnd.tt_at_round_id
    left join rts.stop_item2round rsir on rsir.stop_item2round_id = rsp.stop_item2round_id and rsir.order_num =1 and not rsir.sign_deleted
  where  coalesce(rsir.stop_item_id, tsir.stop_item_id) is not null
         and gr.ref_group_kind_id = p_ref_group_kind_id;


end;
$body$
language plpgsql immutable
rows 10;



------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Удалить все последние действия после ОП, с которой может начаться следущая группа действий
Пример:
планируем выпустить на обед с одного ОП, а после обеда нет большей рейсов
и нужно отправить в парк с другого ОП.
 Для возможности осуществления данного действия - удалим хвост
 p_is_with_dinner -- удалять обед
***************************************************************************/
create or replace function ttb.timetable_test_pkg$delete_all_before_ref_gr(in p_tt_schedule_id numeric,
                                                                      in p_tt_tr_id numeric,
                                                                      in p_ref_group_kind_id numeric,
                                                                      in p_is_with_dinner boolean default true)
  returns void
as
$body$
declare
  l_rec record;
  l_cnt smallint;
  l_cnt_restore smallint;
begin
  RAISE NOTICE '--delete_all_before_ref_gr p_tt_tr_id= %---- p_ref_group_kind_id =%', p_tt_tr_id,  p_ref_group_kind_id  ;

  with
      sw as (
        select max(t.time_end)  as tm from  tt_cube t where t.action_type_id = ttb.action_type_pkg$switch()
    )
    , adata as (
      select t.tt_schedule_id, t.tt_schedule_item_id, t.tt_tr_id , t.tr_capacity_id,
        t.one_id, t.tmp_id, t.parent_one_id, t.time_end as tm_e, t.stop_item_id, t.tt_cube_id, t.action_type_id, ag.ref_group_kind_id,t.tt_action_type2group_id
      from  tt_cube t
        join  ttb.tt_tr ttr on ttr.tt_tr_id = t.tt_tr_id and t.tr_capacity_id = ttr.tr_capacity_id
        join ttb.tt_action_type2group agg   on agg.tt_action_type2group_id = t.tt_action_type2group_id
        join ttb.tt_action_group ag on ag.tt_action_group_id = agg.tt_action_group_id
        left join sw on 1=1
      --join unnest(ttb.tt_schedule_pkg$get_schedule_list(p_tt_schedule_id)) sch on sch = t.tt_schedule_id
      where  t.tt_tr_id = p_tt_tr_id
             and exists (select 1 from  ttb.timetable_test_pkg$get_ref_group_kind_data (p_tt_schedule_id, p_ref_group_kind_id)  where fstop_item_id = t.stop_item_id )
             and is_finish_stop
             and (not p_is_with_dinner or ag.ref_group_kind_id <> ttb.ref_group_kind_pkg$dinner_action())
             and (sw.tm is null or t.time_begin >= sw.tm)
      order by t.time_end desc, t.time_begin desc
      limit 1
  )
    , del as (
    update tt_cube
    set tt_tr_id = null
    from  adata where adata.tt_schedule_id = tt_cube.tt_schedule_id
                      and  adata.tt_tr_id = tt_cube.tt_tr_id
                      and  adata.tr_capacity_id =  tt_cube.tr_capacity_id
                      and tt_cube.time_begin >= adata.tm_e
                      and tt_cube.tt_schedule_item_id !=adata.tt_schedule_item_id

    returning tt_cube.*
  )
    , restore_bn as (
    --восстанавливаем межрейс перед обедами и отстоями
      select ttb.timetable_test_pkg$restore_bn(d.tt_schedule_id ,p_tt_tr_id , d.one_id , d.tmp_id,
                                          (select t.round_id from tt_cube t
                                          where t.tt_schedule_id = d.tt_schedule_id
                                                and t.one_id = d.one_id
                                                and t.tmp_id =d.tmp_id
                                                and t.round_id is not null
                                           order by t.time_end , t.time_begin
                                           limit 1))
      from (
             select del.action_type_id, del.tt_schedule_id , del.one_id , del.tmp_id
             from del

             --where p_ref_group_kind_id != ttb.ref_group_kind_pkg$last_action()  --нельзя! пропадают
             order by time_end desc, time_begin desc
             limit 1 ) d
  )
  select (select count (1) from del),(select count (1) from restore_bn)  into l_cnt, l_cnt_restore
  ;

  RAISE NOTICE '-- $delete_all_before_ref_gr = %, $restore_bn = %', l_cnt,  l_cnt_restore ;



end;

$body$
language plpgsql volatile
cost 100;


------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Удалить все последние действия после ОП, с которой может начаться переключение
***************************************************************************/
create or replace function ttb.timetable_test_pkg$delete_all_before_switch(in p_tt_schedule_id numeric,
                                                                      in p_tt_tr_id numeric,
                                                                      in p_switch_tm timestamp,
                                                                      in p_tt_schedule_item_id numeric default null
)
  returns void
as
$body$
declare
  l_cnt smallint;
begin

  RAISE NOTICE '--delete_all_before_switch p_tt_tr_id= %, p_switch_tm =% , p_tt_schedule_item_id =%', p_tt_tr_id, p_switch_tm , p_tt_schedule_item_id;
  with adata as (
      select
         t.tt_schedule_id||'-'||t.tmp_id||'-'||t.one_id||'-'||sch_it.tt_action_group_id as id
        ,case when vma.is_bn_round_break then ttb.action_type_pkg$bn_round_break() else t.action_type_id end as new_action_type_id
        , t.*
        ,sch_it.tt_action_group_id
        ,vma.is_all_pause
        ,first_value(time_end)over(partition by t.tt_schedule_id,t.tmp_id,t.one_id,sch_it.tt_action_group_id order by time_begin desc) as tm_e
      from   tt_cube t
        join ttb.tt_tr ttr on ttr.tt_tr_id = t.tt_tr_id and t.tr_capacity_id = ttr.tr_capacity_id
        join ttb.tt_schedule_item sch_it on sch_it.tt_schedule_item_id  = t.tt_schedule_item_id
        join ttb.vm_action_type vma on vma.action_type_id = t.action_type_id and (vma.is_all_pause or vma.is_bn_round_break)
        join ttb.timetable_test_pkg$get_ref_group_kind_data (p_tt_schedule_id, ttb.ref_group_kind_pkg$switch_action(), p_tt_schedule_item_id ) d on 1=1
      where  t.tt_tr_id = p_tt_tr_id
             and t.stop_item_id = d.fstop_item_id
      order by time_begin
  )
    , last_row as (
      select *
      from adata
      where tm_e < p_switch_tm
      order by time_begin desc
      limit 1
  )
    --увеличить перерыв или межрейс до времени переключения
    , upd_dur as (

    update tt_cube
    set time_end = p_switch_tm
      , dur_from_beg = extract(epoch from  p_switch_tm - lr.time_begin)
      , dur_round =  extract(epoch from  p_switch_tm - lr.time_begin)
    from last_row lr
    where tt_cube.tt_cube_id = lr.tt_cube_id
          and lr.time_end != p_switch_tm
          and (lr.action_type_id = ttb.action_type_pkg$bn_round_break_corr() or lr.is_all_pause)
    returning *
  )
    -- если не удалось увеличить, добавить корректировочный межрейс
    ,ins_dur as (
    insert into tt_cube (
      time_begin,          time_end,         dur_from_beg,          dur_round,      action_type_id,
      stop_item2round_id, round_id, stop_item_id, tr_capacity_id, tt_variant_id,
      order_num,  tmp_id, is_require,   is_finish_stop ,  tt_tr_id,  tt_action_type2group_id,   tt_schedule_id, tt_schedule_item_id,
      tr_type_id , tt_at_round_id,  order_num_action, dr_shift_id, one_id, parent_one_id,  is_intvl, tt_driver_id
    )
      select
        time_end as time_begin,       p_switch_tm as    time_end,
        extract(epoch from  p_switch_tm - lr.time_end) as dur_from_beg,         extract(epoch from  p_switch_tm - lr.time_end) as  dur_round,
        ttb.action_type_pkg$bn_round_break_corr(),
        stop_item2round_id, round_id, stop_item_id, tr_capacity_id, tt_variant_id,
        order_num,  tmp_id, is_require,   is_finish_stop ,  tt_tr_id,  tt_action_type2group_id,   tt_schedule_id, tt_schedule_item_id,
        tr_type_id , tt_at_round_id,  order_num_action, dr_shift_id, one_id, parent_one_id,  is_intvl, tt_driver_id
      from last_row lr
      where
        lr.time_end != p_switch_tm
        and not exists (select 1 from  upd_dur)
  )
    , upd as (
    update tt_cube
    set tt_tr_id = null
    from tt_cube t
      join last_row on 1=1
    where t.tt_tr_id = p_tt_tr_id
          and t.tt_cube_id = tt_cube.tt_cube_id
          and t.time_begin >=last_row.time_end
          /*and t.time_begin >=last_row.time_begin
          and t.time_end >=last_row.time_end*/
          and t.tt_cube_id != last_row.tt_cube_id
    returning *
  )
  select count (1) from upd into l_cnt;

  RAISE NOTICE '--update tt_tr_id = null % row', l_cnt;

end;

$body$
language plpgsql volatile
cost 100;


/*
ВРЕМЕННО!!!
для показа 24.01.2018

ищется первый близкий к последнему выход, который можно увеличить
и последний рейс сдвигается до последнего


*/
create or replace function ttb.timetable_test_pkg$update_last_rnd(in p_tt_variant_id numeric)
  returns timestamp
as
$body$
declare
  l_tt_action_id numeric;
  l_tt_action_break_id numeric;
  l_tm timestamp;
  l_x record;
  l_rnd record;
  l_sch record;
  l_time_begin timestamp;
  l_tm_min integer:= asd.get_constant('MAX_INTER_RACE_STOP')::int*2;
  l_tt_out_id numeric := 0;
  l_cnt smallint;
begin
  perform ttb.tt_out_pkg$make_tmp_tt4var( p_tt_variant_id);

  --ttb.tt_out_pkg$make_tmp_tt4var(p_tt_variant_id numeric)
  RAISE NOTICE '--ttb.timetable_test_pkg$update_last_rnd'  ;
  --пытаемся захватить последний рейс везде где он задан
  for l_rnd in (

    select act.action_type_id, atg.start_time
    from tt_action act
      join tt_action_item it on it.tt_action_id =  act.tt_action_id
      join tt_out tout on tout.tt_out_id  = act.tt_out_id
      join ttb.vm_action_type vma on vma.action_type_id = act.action_type_id and vma.is_production_round
      join rts.stop_item2round sir on sir.stop_item2round_id = it.stop_item2round_id and sir.order_num =1
      join ttb.tt_action_type tact on tact.tt_variant_id = tout.tt_variant_id
      join ttb.tt_at_round tar on tar.tt_action_type_id = tact.tt_action_type_id
      join ttb.tt_at_round_stop atsp on atsp.stop_item2round_id =sir.stop_item2round_id and atsp.tt_at_round_id = tar.tt_at_round_id
      join ttb.atgrs_time atg on atg.tt_at_round_stop_id = atsp.tt_at_round_stop_id and atg.fix_arrival_type_id = ttb.fix_arrival_type_pkg$finish_time()

    where tout.tt_variant_id = p_tt_variant_id
    group by  act.action_type_id , atg.start_time
    order by atg.start_time
  ) loop
    RAISE NOTICE ' l_rnd.action_type_id (%) l_rnd.start_time (%)' ,  l_rnd.action_type_id , l_rnd.start_time;

    for l_x in (
      with ss as (
          select it.time_begin, it.dr_shift_id, act.tt_action_id, tout.tt_out_id , l_rnd.start_time as finish_time
            --Находим максимальную продолшительность перерыва
            --КОСТЫЛЬ! нет разделения на обеды и отстои
            , (select max(time_max)  from ttb.dr_mode dm where dm.dr_shift_id = it.dr_shift_id  and ttb.action_type_pkg$is_all_pause( action_type_id)) as tm_max_pause
            , (select 1  from ttb.dr_mode dm where dm.dr_shift_id = it.dr_shift_id  and action_type_id = ttb.action_type_pkg$pause()) =1 as is_make_pause
            ,  extract(EPOCH from l_rnd.start_time - it.time_begin) as change_dur
            ,sir.stop_item_id

          from tt_action_item it
            join rts.stop_item2round sir on sir.stop_item2round_id = it.stop_item2round_id
            join tt_action act on act.tt_action_id = it.tt_action_id
            join tt_out tout on tout.tt_out_id  = act.tt_out_id
          where tout.tt_variant_id = p_tt_variant_id
                and it.action_type_id = l_rnd.action_type_id
                --and tout.tt_out_id != l_tt_out_id
                and sir.order_num =1
      )
        --отбираем только те, где нет рейсов уже после указанного времени
        ,adata as (
          select *
          from ss
          where not  exists (select 1 from ss where  time_begin>=finish_time)
      )
        , dur_pause as (
          select it.dr_shift_id, act.tt_out_id,  sum(extract (epoch from it.time_end - it.time_begin)) as sec
          from tt_action_item it
            join tt_action act on act.tt_action_id = it.tt_action_id
            join ttb.vm_action_type vma on vma.action_type_id = act.action_type_id and  vma.is_all_pause
            join tt_out tout on tout.tt_out_id  = act.tt_out_id
          where tout.tt_variant_id = p_tt_variant_id
          group by  it.dr_shift_id, act.tt_out_id)
        --пункты выравнивания
        , inv_alig as (
          select * from ttb.timetable_test_pkg$get_stop_interval_alignment(p_tt_variant_id)
      )
        --вычисляем на какое время можно дать отстой (мах время - уже существующие перерывы в смене)
        , itog as (
          select
            adata.*
            , adata.tm_max_pause - dur_pause.sec>0 as is_break
            , case when adata.tm_max_pause - dur_pause.sec> 0 then
            adata.tm_max_pause - dur_pause.sec
              else change_dur  end           as can_break
            , change_dur< l_tm_min and exists (select 1 from inv_alig where  fstop_item_id = adata.stop_item_id) as is_bn
          from adata
            join dur_pause on dur_pause.dr_shift_id =adata.dr_shift_id and dur_pause. tt_out_id =adata.tt_out_id
          where change_dur >0
      )
      --отбираем те, где может быть отстой или  увеличение межрейса не более чем на полчаса
      select least(can_break, change_dur) as change_dur
        ,time_begin
        ,dr_shift_id
        ,tt_action_id
        ,tt_out_id
        ,finish_time
        ,tm_max_pause
        ,is_make_pause
        ,stop_item_id
        ,is_break
        ,can_break
        ,is_bn
      from itog
      --where (can_break>0   and is_break) or is_bn
      where (change_dur <=can_break   and is_break) or is_bn
      order by not  is_bn ,   change_dur
      limit 1


    )loop


      if l_x.change_dur>0 then
        RAISE NOTICE '--двигаем l_x.tt_out_id (%), l_rnd.action_type_id (%) l_rnd.start_time (%)' , l_x.tt_out_id, l_rnd.action_type_id , l_rnd.start_time;
        --        perform ttb.tt_out_pkg$make_tmp_tt(l_x.tt_out_id);

        if l_x.is_break and not l_x.is_bn then --заменить предыдущее действие обед или отстой на отстой

          select act.tt_action_id into l_tt_action_break_id
          from tt_action act
            join ttb.vm_action_type vma on vma.action_type_id = act.action_type_id and vma.is_all_pause
          where act.tt_out_id =l_x.tt_out_id
                and exists (select 1 from tt_action_item it where it.tt_action_id =act.tt_action_id and it.dr_shift_id = l_x.dr_shift_id)
          order by act.dt_action desc
          limit 1;

          select fnext_tt_action_id , fdt_action_end
          into l_tt_action_id, l_tm  from ttb.timetable_edit_pkg$get_near_action(l_tt_action_break_id);

          RAISE NOTICE '--l_tt_action_break_id=% , l_tt_action_id =%', l_tt_action_break_id, l_tt_action_id;

          select ttb.timetable_edit_pkg$update_out(l_tt_action_id, l_tm + make_interval(secs:= l_x.change_dur) ) into l_time_begin;

          if l_x.is_make_pause then

            --проверяем возможность дать отстой
            with act_pause as (
                select it.stop_item2round_id
                from ttb.tt_action_item it where it.tt_action_id = l_tt_action_break_id
                order by time_end desc
                limit 1
            )
            select count(1) into l_cnt
            from
              ttb.tt_action_type act
              join ttb.tt_at_stop asp on asp.tt_action_type_id = act.tt_action_type_id
              left join act_pause on act_pause.stop_item2round_id  = asp.stop_item2round_id
            where act.tt_variant_id = p_tt_variant_id
                  and act.action_type_id = ttb.action_type_pkg$pause();

            if l_cnt>0 then
              --Пункт отстоя определен, отстой внесен в последовательность действий
              RAISE NOTICE '--отстой будет';

              for l_sch in(
                select sch_it.tt_schedule_item_id
                  ,l_x.tt_out_id
                  ,l_x.dr_shift_id
                  ,l_x.tt_action_id as tt_action_id
                  ,p_tt_variant_id as tt_variant_id
                from ttb.v_tt_schedule_item sch_it
                where sch_it.tt_variant_id = p_tt_variant_id
                      and  sch_it.ref_group_kind_id = ttb.ref_group_kind_pkg$pause_action() --действие отстой
                      and not exists (select 1
                                      from  ttb.tt_action tact
                                      where tact.tt_action_id = l_tt_action_break_id
                                            and tact.action_type_id = ttb.action_type_pkg$pause())
                order by sch_it.order_num
              ) loop

                --меняем действие на отстой


                with upd as (
                  update tt_action
                  set action_type_id = ttb.action_type_pkg$pause()
                  where tt_action_id = l_tt_action_break_id
                        and ttb.action_type_pkg$is_break(action_type_id)
                  returning *
                )
                  ,upd_it as (
                  update tt_action_item
                  set (time_begin , time_end, action_type_id) = (select upd.dt_action, upd.dt_action + upd.action_dur * interval '1 second', ttb.action_type_pkg$pause()from upd)
                  where tt_action_id =l_tt_action_break_id
                  returning *

                )
                delete from tt_action_item
                where tt_action_id = l_tt_action_break_id
                      and tt_action_item_id !=(select min( t.tt_action_item_id) from ttb.tt_action_item t where t.tt_action_id = l_tt_action_break_id)

              ;
              end loop;
            end if;
          end if;
        else
          --раздвигаем межрейс
          RAISE NOTICE 'межрейс';
          select ttb.timetable_edit_pkg$update_out(l_x.tt_action_id, l_x.finish_time ) into l_time_begin;

        end if;

        l_tt_out_id := l_x.tt_out_id;


        --обратно в БД
        perform ttb.tt_out_pkg$fill_db_table(l_tt_out_id);
      end if;
    end loop;

  end loop;

  if  l_time_begin is null and l_tt_action_id is null then
    RAISE NOTICE 'Последний рейс не распределен';
  end if;
  return l_time_begin;

end;

$body$
language plpgsql volatile
cost 100;


/*
--массив  ОП выравнивания
*/
create or replace function ttb.timetable_test_pkg$get_stop_interval_alignment(in p_tt_variant_id numeric)
  returns TABLE(
    fstop_item_id          integer,
    fstop_item2round_id    bigint,
    fround_id              integer
  )
as
$body$
declare
begin
  --ищем в межрейсах  ОП выравнивания
  return query
  select sir.stop_item_id , sir.stop_item2round_id, sir.round_id
  from ttb.tt_action_type tact
    join ttb.tt_at_stop tats on tats.tt_action_type_id =tact.tt_action_type_id
    join rts.stop_item2round sir on sir.stop_item2round_id = tats.stop_item2round_id and not sir.sign_deleted
    join ttb.stop_interval_alignment sia on sia.tt_at_stop_id = tats.tt_at_stop_id
  where tact.tt_variant_id  =p_tt_variant_id
        and tact.action_type_id = ttb.action_type_pkg$bn_round_break();

end;

$body$
language plpgsql  stable
cost 100
rows 5;


/*
--массив  ОП выпуска
*/
create or replace function ttb.timetable_test_pkg$get_stop_start(in p_tt_schedule_id numeric)
  returns TABLE(
    fstop_item_id          integer,
    fstop_item2round_id    bigint
  )
as
$body$
declare
begin
  --ищем в действии выпуска на линию последние остановки
  return query
  select sir.stop_item_id, sir.stop_item2round_id
  from  ttb.tt_schedule_item sch_it
    join ttb.tt_action_group act_gr on act_gr.tt_action_group_id = sch_it.tt_action_group_id
    join ttb.tt_action_type2group act2gr on act2gr.tt_action_group_id = act_gr.tt_action_group_id
    join ttb.tt_action_type tact on tact.tt_action_type_id = act2gr.tt_action_type_id
    join ttb.tt_at_round at_rnd on at_rnd.tt_action_type_id = tact.tt_action_type_id and not sign_deleted
    join rts.round rnd on rnd.round_id = at_rnd.round_id  and not rnd.sign_deleted
    join rts.stop_item2round sir on sir.round_id = rnd.round_id and sir.order_num in
                                                                    (select max(t.order_num) from rts.stop_item2round t
                                                                    where t.round_id = sir.round_id and not t.sign_deleted) and not sir.sign_deleted
  where sch_it.tt_schedule_id = p_tt_schedule_id
        and  act_gr.ref_group_kind_id = ttb.ref_group_kind_pkg$first_action()
  ;

end;

$body$
language plpgsql stable
cost 100;




/*
--массив  времен начала рейса из матрицы рейсов
  in p_tt_schedule_id numeric,  - последовательность
  in p_tt_tr_id numeric, ТС
  in p_time_begin timestamp, время начала
  in p_rn  numeric  - порядковый номер выходв из последовательности
  in p_delta numeric -  количество секунд, для избежания кучкообразования
*/
create or replace function ttb.timetable_test_pkg$get_tm_start4tr(in p_tt_schedule_id numeric,
                                                             in p_tt_tr_id numeric,
                                                             in p_time_begin timestamp,
                                                             in p_rn  numeric,
                                                             in p_intvl numeric
)
  returns TABLE(
    fstop_item_id          integer,
    ftime_start            timestamp
  )
as
$body$
declare
begin
  --ищем в матрице времена выпуска выходов для пунктов выпуска
  return query

  select cb.stop_item_id, cb.time_end as time_start

  from tt_cube cb
    join  ttb.tt_tr ttr on ttr.tt_tr_id = p_tt_tr_id and cb.tr_capacity_id = ttr.tr_capacity_id
    join ttb.vm_action_type vma on vma.action_type_id = cb.action_type_id and vma.is_production_round
    --не всегда есть ОП выхода!!!!
    --НУЖНО ЧТОБЫ БЫЛИ!
    join unnest(ttb.tt_schedule_pkg$get_schedule_list(p_tt_schedule_id)) sch on sch = cb.tt_schedule_id
    join ttb.timetable_test_pkg$get_stop_start( sch ) ss on ss.fstop_item_id = cb.stop_item_id
    join ttb.tt_period p on p.tt_schedule_id = cb.tt_schedule_id and cb.time_begin between p.time_begin and  p.time_end
  where cb.order_num = 1
        and cb.tt_tr_id is null
        and cb.parent_one_id is null
        and cb.time_end >= p_time_begin
        and cb.is_intvl
        --рядом нет выпущенных выходов
        and not exists (
      select 1
      from tt_cube t
        join ttb.vm_action_type vma on vma.action_type_id = t.action_type_id and vma.is_production_round
      where t.tt_tr_id is not NULL
            and t.order_num  = 1
            and cb.time_begin  between t.time_begin -  make_interval(secs:= round(p.interval_time*0.3))  and t.time_begin +  make_interval(secs:= round(p.interval_time*0.3))
            and t.stop_item_id = cb.stop_item_id
  )
        --на этом пункте нет ожидающих межрейса выходов
        and not exists ( select 1
                         from  tt_cube t
                           join ttb.vm_action_type vma on vma.action_type_id = t.action_type_id and vma.is_bn_round_break
                         where
                           t.tt_tr_id is not NULL
                           and t.stop_item_id = cb.stop_item_id
                           and cb.time_end  < t.time_begin
                           and cb.time_begin between t.time_begin and t.time_end)
  order by  cb.time_end, cb.time_begin, cb.is_require desc, cb.stop_item_id
  limit 1
  offset p_rn
  ;

end;

$body$
language plpgsql volatile
cost 100;


create or replace function ttb.timetable_test_pkg$get_break_dur(in p_action_type_id  numeric,
                                                           in p_tt_variant_id numeric,
                                                           in p_tt_tr_id numeric,
                                                           in p_dr_shift_id numeric
)
  returns integer
as
$body$
declare
  l_res integer;
begin
  if p_action_type_id = ttb.action_type_pkg$dinner()  then
    --обед
    l_res:= ttb.timetable_test_pkg$get_dinner_dur(p_tt_variant_id ,
                                             p_tt_tr_id ,
                                             p_dr_shift_id);
  elsif p_action_type_id = ttb.action_type_pkg$change_driver()  then
    --пересменка
    l_res:=ttb.tt_change_time_pkg$get_change_time(p_tt_variant_id, (select tr_capacity_id from ttb.tt_tr where tt_tr_id = p_tt_tr_id))::int;
  elsif p_action_type_id = ttb.action_type_pkg$maintenance() then
    select to_duration_min into l_res
    from ttb.tt_tr ttr
      join ttb.tr_mode tm on tm.tr_mode_id = ttr.tr_mode_id
    where ttr.tt_tr_id = p_tt_tr_id;

    select coalesce(tact.plan_time,l_res) into l_res
    from  ttb.tt_action_type tact
    where tact.tt_variant_id = p_tt_variant_id
          and tact.action_type_id = ttb.action_type_pkg$maintenance();

  else
    l_res:=0;
  end if;

  return l_res;



end;

$body$
language plpgsql volatile
cost 100;

/*
--время перерыва по умолчанию для варианта расписания и вместимости
  in p_tt_capacity_id numeric, вместимость
  in p_tt_variant_id numeric,
  in p_action_type_id  numeric тип действия
*/
create or replace function ttb.timetable_test_pkg$get_break_dur_default(in p_action_type_id  numeric,
                                                                   in p_tt_variant_id numeric,
                                                                   in p_tr_capacity_id numeric
)
  returns integer
as
$body$
declare
  l_res integer;
begin
  if p_action_type_id = ttb.action_type_pkg$dinner()  then
    --обед
    l_res:= asd.get_constant('OPTIMAL_TIME_DINNER')::int;
  elsif p_action_type_id = ttb.action_type_pkg$change_driver()  then
    --пересменка
    l_res:=ttb.tt_change_time_pkg$get_change_time(p_tt_variant_id, p_tr_capacity_id)::int;
  else
    l_res:=0;
  end if;

  return l_res;

end;

$body$
language plpgsql immutable;

/*
--время перерыва
p_is_tail-вернуть остаток времени
иначе возможное время перерыва
*/
create or replace function ttb.timetable_test_pkg$get_dinner_dur(in p_tt_variant_id numeric,
                                                            in p_tt_tr_id numeric,
                                                            in p_dr_shift_id numeric,
                                                            in p_is_tail boolean default false
)
  returns integer
as
$body$
declare
  l_res integer;
  l_break_dur int;
  l_break_plan int;
  l_max_dur int;
begin
  select  drs.break_duration_max into l_res from  ttb.dr_shift drs where drs.dr_shift_id = p_dr_shift_id;
  --select  sum(drm.time_max) into l_res from  ttb.dr_mode drm where drm.dr_shift_id = p_dr_shift_id;
  select  max(drm.time_max) into l_max_dur from  ttb.dr_mode drm where drm.dr_shift_id = p_dr_shift_id;

  select coalesce(sum (dur_from_beg),0)  into l_break_dur
  from tt_cube t
    join ttb.vm_action_type vma on vma.action_type_id = t.action_type_id
    join ttb.tt_driver tdr on tdr.tt_driver_id = t.tt_driver_id
  where tt_tr_id = p_tt_tr_id
        and t.dr_shift_id = p_dr_shift_id
        and (vma.is_all_pause or tdr.is_сhanged_driver)
  ;


  select coalesce(tact.plan_time,asd.get_constant('OPTIMAL_TIME_DINNER')::int) into l_break_plan
  from  ttb.tt_action_type tact
  where tact.tt_variant_id = p_tt_variant_id
        and tact.action_type_id = ttb.action_type_pkg$dinner();

  RAISE NOTICE 'l_res %, l_break_dur %, l_break_plan % l_max_dur %', l_res , l_break_dur , l_break_plan , l_max_dur ;

  --сколько можно прибавить к перерыву
  if p_is_tail then
    if  l_max_dur > coalesce(l_break_dur,0)  then
      return l_max_dur - coalesce(l_break_dur,0);
    else
      return 0;
    end if;
  end if;

  --проверим максимальную продолжительность перерыва
  l_res:= l_res - coalesce(l_break_dur,0);

  if l_res >= l_break_plan then
    return least(l_max_dur,l_break_plan);

  elsif  l_res  >= asd.get_constant('OPTIMAL_TIME_DINNER')::int then
    return least(l_max_dur,asd.get_constant('OPTIMAL_TIME_DINNER')::int);
  else
    return 0;
  end if;


end;

$body$
language plpgsql volatile ;

/*
  Если пересменка после межрейса, то поставить ее до межрейса и межрейс становится другой сменой
*/

create or replace function ttb.timetable_test_pkg$update_action_change_driver(in p_tt_schedule_id numeric,
                                                                         in p_tt_tr_id numeric,
                                                                         in p_dr_shift_id numeric)
  returns numeric
as
$body$
declare
  l_res numeric;
begin
  with recursive  adata  as (
      select
          lag(time_end) over w as lag_time_end
        , lag(tt_cube_id) over w as lag_tt_cube_id
        , lag(action_type_id) over w as lag_action_type_id
        , lag(tt_action_type2group_id ) over w as lag_tt_action_type2group_id
        , t.*
      from  tt_cube t
        join ttb.tt_tr ttr on ttr.tt_tr_id = t.tt_tr_id and ttr.tr_capacity_id = t.tr_capacity_id
        join unnest(ttb.tt_schedule_pkg$get_schedule_list(p_tt_schedule_id)) sch on sch = t.tt_schedule_id
      where t.tt_tr_id = p_tt_tr_id
      window w as (order by  time_end,  time_begin, tt_at_round_id nulls last)
    --ORDER BY  time_end,  time_begin, action_type_id
  )
    ,bn (id, tm_begin, tm_end, dur, drs_id, action_type_id  ,tt_action_type2group_id , lag_time_end, lag_tt_action_type2group_id , is_change,  path, cycle)  as (
    select
      adata.tt_cube_id, adata.time_begin, adata.time_end,adata.time_end-adata.time_begin as dur, adata.dr_shift_id, adata.action_type_id  ,adata.tt_action_type2group_id ,  adata.lag_time_end, adata.lag_tt_action_type2group_id , true is_change,  ARRAY[adata.tt_cube_id], false
    from adata
      join ttb.vm_action_type vma on vma.action_type_id = lag_action_type_id and vma.is_bn_round_break
    where  adata.action_type_id = ttb.action_type_pkg$change_driver()
    union all
    select adata.tt_cube_id, adata.time_begin, adata.time_end, adata.time_end-adata.time_begin as dur,adata.dr_shift_id, adata.action_type_id,adata.tt_action_type2group_id ,  adata.lag_time_end , adata.lag_tt_action_type2group_id , false is_change,
      path || adata.tt_cube_id,   adata.tt_cube_id = ANY(path)
    from bn
      join adata on  adata.time_end = bn.lag_time_end and  adata.tt_action_type2group_id = bn.lag_tt_action_type2group_id
      join ttb.vm_action_type vma on vma.action_type_id = adata.action_type_id and vma.is_bn_round_break
    where adata.time_end<= bn.tm_end  AND NOT cycle
  )
    ,dur as (select dur from bn where is_change)
    ,cng as (select min(tm_begin) tm from bn where not is_change)

    ,bn_upd as (
    update tt_cube
    set time_begin = time_begin  + (select dur from dur),
      time_end = time_end  + (select dur from dur),
      dr_shift_id = p_dr_shift_id,
      tt_driver_id = (select dr.tt_driver_id
                      from ttb.tt_driver dr
                        join ttb.tt_driver_tr drtr on drtr.tt_driver_id = dr.tt_driver_id  and drtr.tt_tr_id = p_tt_tr_id
                      where dr.tt_variant_id = tt_cube.tt_variant_id and dr.dr_shift_id = p_dr_shift_id and not dr.is_сhanged_driver)
    from bn
    where tt_cube.tt_cube_id  = bn.id
          and not is_change
  )
    ,ch_upd as (
    update tt_cube
    set time_begin = (select tm from cng),
      time_end = (select tm from cng)  + (select dur from dur),
      dr_shift_id = p_dr_shift_id
    from bn
    where tt_cube.tt_cube_id  = bn.id
          and  is_change
    returning *
  )
  select count(1) into l_res from ch_upd;

  return l_res;
end;

$body$
language plpgsql volatile
cost 100;


create or replace function ttb.timetable_test_pkg$update_time_dinner(in p_tt_variant_id numeric,
                                                                in p_tt_tr_id numeric,
                                                                in p_is_more boolean default true,
                                                                in p_tt_cube_id numeric default null)
  returns void
as
$body$
declare

begin
  if p_is_more then
    with adata as (
        select lead (time_begin) over (partition  by tt_variant_id , tt_tr_id, tr_capacity_id order by time_end, time_begin, order_num_action  ) as time_next
          ,cb.time_begin, cb.time_end, cb.action_type_id , cb.tt_cube_id
        from  tt_cube cb where tt_variant_id = p_tt_variant_id and  tt_tr_id =p_tt_tr_id
    )
    update tt_cube
    set time_end     = adata.time_next
      , dur_from_beg = extract (EPOCH from adata.time_next- adata.time_begin)
      , dur_round    = extract (EPOCH from adata.time_next- adata.time_begin)
    from adata
    where adata.action_type_id = ttb.action_type_pkg$dinner()
          and adata.tt_cube_id= tt_cube.tt_cube_id
          and tt_cube.time_begin < adata.time_next
          and (p_tt_cube_id is null or tt_cube.tt_cube_id =p_tt_cube_id);

  else
    with adata as (
        select lead (time_begin) over (partition  by tt_variant_id , tt_tr_id, tr_capacity_id order by time_begin, time_end,  order_num_action  ) as time_next
          ,cb.time_begin, cb.time_end, cb.action_type_id , cb.tt_cube_id
        from  tt_cube cb where tt_variant_id = p_tt_variant_id and  tt_tr_id =p_tt_tr_id
    )
    update tt_cube
    set time_end     = adata.time_next
      , dur_from_beg = extract (EPOCH from adata.time_next- adata.time_begin)
      , dur_round    = extract (EPOCH from adata.time_next- adata.time_begin)
    from adata
    where adata.action_type_id = ttb.action_type_pkg$dinner()
          and adata.tt_cube_id= tt_cube.tt_cube_id
          and tt_cube.time_begin < adata.time_next
          and (p_tt_cube_id is null or tt_cube.tt_cube_id =p_tt_cube_id);
  end if;
end;

$body$
language plpgsql volatile
cost 100;
-----------------------------------------------------------------------------
create or replace function ttb.timetable_test_pkg$get_min_fix_time (
  p_tt_at_round_id numeric
)
  returns timestamp without time zone as
$body$
declare
  l_res ttb.atgrs_time.start_time%type;
begin
  select  min(atg.start_time) as start_time
  into l_res
  from ttb.atgrs_time atg
    join ttb.tt_at_round_stop ars on ars.tt_at_round_stop_id = atg.tt_at_round_stop_id
    join ttb.tt_at_round ar on ar.tt_at_round_id = ars.tt_at_round_id
    join rts.stop_item2round sir on sir.stop_item2round_id = ars.stop_item2round_id and not sir.sign_deleted
  where atg.fix_arrival_type_id = ttb.fix_arrival_type_pkg$fix_time()
        and sir.order_num = 1
        and  ar.tt_at_round_id = p_tt_at_round_id;

  return l_res;
end;
$body$
language 'plpgsql'
volatile
called on null input
security invoker
cost 100;
----------------------------------------------
create or replace function ttb.timetable_test_pkg$get_max_fix_time (
  p_tt_at_round_id numeric
)
  returns timestamp without time zone as
$body$
declare
  l_res ttb.atgrs_time.start_time%type;
begin
  select  max(atg.start_time) as start_time
  into l_res
  from ttb.atgrs_time atg
    join ttb.tt_at_round_stop ars on ars.tt_at_round_stop_id = atg.tt_at_round_stop_id
    join ttb.tt_at_round ar on ar.tt_at_round_id = ars.tt_at_round_id
    join rts.stop_item2round sir on sir.stop_item2round_id = ars.stop_item2round_id and not sir.sign_deleted
  where atg.fix_arrival_type_id = ttb.fix_arrival_type_pkg$fix_time()
        and sir.order_num = 1
        and  ar.tt_at_round_id = p_tt_at_round_id;

  return l_res;
end;
$body$
language 'plpgsql'
volatile
called on null input
security invoker
cost 100;


/*Возвращает первое время для основных рейсов последовательности
  1. Указаноое пользователем
  2. если нет 1, то время с отступом в рейс от кругорейса
  3. если нет 2, то первое время первого (последнее последнего) периода последовательности*/
create or replace function ttb.timetable_test_pkg$get_start_time4sch (in  p_tt_schedule_id numeric,
                                                                 in  p_tt_at_round_id numeric,
                                                                 in  p_what text default 'START',
                                                                 in  p_tt_tr_id numeric default null
)
  returns timestamp as
$body$
declare
  l_res timestamp;
begin

  if p_what =  'START' then

    l_res  := ttb.timetable_test_pkg$get_start_time( p_tt_at_round_id);
    if l_res is null then

      with adata as (
          select
            ttb.timetable_test_pkg$get_start_time (ar.tt_at_round_id) as tm_start
            ,v.norm_id
            ,ar.round_id
            ,tt_tr.tr_capacity_id
          from  ttb.v_tt_schedule_item it
            join ttb.tt_variant v on v.tt_variant_id = it.tt_variant_id
            join ttb.tt_action_type2group at2g on at2g.tt_action_group_id = it.tt_action_group_id
            join ttb.tt_at_round ar on ar.tt_action_type_id = at2g.tt_action_type_id
            left join ttb.tt_tr on tt_tr.tt_tr_id = p_tt_tr_id
          where it.tt_schedule_id = p_tt_schedule_id and ref_group_kind_id = ttb.ref_group_kind_pkg$main_action()
                and ar.tt_at_round_id != p_tt_at_round_id
      )
      select
        tm_start + make_interval(secs:= sum(ttb.norm_pkg$get_avg_bn_break(norm_id, round_id, date_trunc ('hour',tm_start)::time , date_trunc ('hour',tm_start)::time + interval '1 hour' ,tr_capacity_id)
                                            + ttb.norm_pkg$get_avg_rnd(norm_id, round_id, date_trunc ('hour',tm_start)::time , date_trunc ('hour',tm_start)::time + interval '1 hour' ,tr_capacity_id))) as tm_next_rnd_start
      into l_res
      from adata
      group by tm_start;


      if l_res is null then
        select min(p.time_begin) into l_res
        from ttb.tt_schedule sch
          join ttb.tt_period p on p.tt_schedule_id = sch.tt_schedule_id
        where sch.tt_schedule_id = p_tt_schedule_id;
      else
        --если нашли время по нормативу и не задано tt_tr_id норматив усредняет и возможно ошиблись
        --прибавим 7 минут магическое число !!!КОСТЫЛЬ нужно понять как правильно
        if p_tt_tr_id is null then
          l_res:= l_res + make_interval (mins:= 7);
        end if;
      end if;

    end if;
  elseif p_what = 'FINISH' then

    l_res  := ttb.timetable_test_pkg$get_finish_time ( p_tt_at_round_id);
    if l_res is null then

      with adata as (
          select
            ttb.timetable_test_pkg$get_finish_time (ar.tt_at_round_id) as tm_fin
            ,v.norm_id
            ,ar.round_id
            ,tt_tr.tr_capacity_id
          from  ttb.v_tt_schedule_item it
            join ttb.tt_variant v on v.tt_variant_id = it.tt_variant_id
            join ttb.tt_action_type2group at2g on at2g.tt_action_group_id = it.tt_action_group_id
            join ttb.tt_at_round ar on ar.tt_action_type_id = at2g.tt_action_type_id
            left join ttb.tt_tr on tt_tr.tt_tr_id = p_tt_tr_id
          where it.tt_schedule_id = p_tt_schedule_id and ref_group_kind_id = ttb.ref_group_kind_pkg$main_action()
                and ar.tt_at_round_id != p_tt_at_round_id
      )
      select
        tm_fin + make_interval(secs:= sum(ttb.norm_pkg$get_avg_bn_break(norm_id, round_id, date_trunc ('hour',tm_fin)::time , date_trunc ('hour',tm_fin)::time + interval '1 hour' ,tr_capacity_id )
                                          + ttb.norm_pkg$get_avg_rnd(norm_id, round_id, date_trunc ('hour',tm_fin)::time , date_trunc ('hour',tm_fin)::time + interval '1 hour' ,tr_capacity_id))) as tm_next_rnd_fin
      into l_res
      from adata
      group by tm_start;


      if l_res is null then
        select max(p.time_end) into l_res
        from ttb.tt_schedule sch
          join ttb.tt_period p on p.tt_schedule_id = sch.tt_schedule_id
        where sch.tt_schedule_id = p_tt_schedule_id;
      else
        --если нашли время по нормативу и не задано tt_tr_id норматив усредняет и возможно ошиблись
        --прибавим 7 минут !!!КОСТЫЛЬ нужно понять как правильно
        if p_tt_tr_id is null then
          l_res:= l_res + make_interval (mins:= 7);
        end if;
      end if;
    end if;
  else
    l_res:= null;
  end if;

  return l_res;
end;
$body$
language 'plpgsql'
volatile

