--http://redmine.advantum.ru/issues/22452

CREATE OR REPLACE FUNCTION ttb.tt_oper_pkg$create_tt_oper_now(p_param text)
  RETURNS INTEGER
LANGUAGE plpgsql
AS $$
BEGIN
    perform ttb.tt_oper_pkg$create_tt_oper(order_date)
    from ttb.order_list
    where not tt_out_created
    group by order_date;

    return 1;
END;
$$;

CREATE OR REPLACE FUNCTION ttb.tt_oper_pkg$create_tt_oper(p_date date)
  RETURNS INTEGER
LANGUAGE plpgsql
AS $$
DECLARE
  l_route_variant_id_out_count_corr_violate bigint[];
  l_orders_id_class_corr_violate bigint[];
  l_orders_shift_time_violate bigint[];
  l_orders_tr_overlap bigint[];
  l_orders_driver_overlap bigint[];
  l_tt_variant_id_new int[];
  l_tt_out_array int[];
  l_order_list_id_array bigint[];
  l_rv_order_shortage bigint[];
  l_text_var1 text;
BEGIN
  RAISE LOG 'create_tt_oper.begin';

  select array_agg(order_list_id)
  into l_order_list_id_array
  from ttb.order_list
  where not tt_out_created
        and order_date = p_date;

  if l_order_list_id_array is null then
    return 0;
  end if;

  RAISE LOG 'create_tt_oper.create new tt_variants';

  with a as (
      SELECT DISTINCT (order_list.route_variant_id) route_variant_id
      FROM ttb.order_list
      WHERE order_list_id = ANY(l_order_list_id_array)
            AND NOT exists(SELECT 1
                           FROM ttb.tt_variant
                           WHERE tt_variant.route_variant_id = order_list.route_variant_id
                                 AND order_date = p_date
                                 AND tt_variant.parent_tt_variant_id IS NOT NULL
                                 AND NOT sign_deleted)
  ),
      tt_variants_new as
    (SELECT ttb.tt_oper_pkg$create_tt_variant(route_variant_id, p_date) tt_variant_id
     FROM a
    )
  select array_agg(tt_variant_id)
  into l_tt_variant_id_new
  from tt_variants_new
  where tt_variant_id is not null;

  --проверки делаем при первом формировании оперативного расписания по варианту маршрута

  if array_length(l_tt_variant_id_new, 1) > 0 then
    --Количество выходов в плановом расписании НЕ равно количеству выходов в наряде.
    l_route_variant_id_out_count_corr_violate = array_remove(ttb.tt_oper_pkg$rv_out_count_corr_violate(p_date), null);
    --Класс ТС в плановом расписании и класс ТС в наряде НЕ совпадают
    l_orders_id_class_corr_violate = array_remove(ttb.tt_oper_pkg$orders_class_corr_violate(p_date), null);
    --По каждому выходу Общее время смены в наряде равно общему времени смены в плановом расписании.
    l_orders_shift_time_violate = array_remove(ttb.tt_oper_pkg$orders_shift_time_violate(p_date), null);
    --Пересечений времени работы ТС
    l_orders_tr_overlap := array_remove(ttb.tt_oper_pkg$orders_tr_overlap(p_date), null);
    --Пересечений времени работы водителей
    l_orders_driver_overlap := array_remove(ttb.tt_oper_pkg$orders_driver_overlap(p_date), null);
    --Количество выходов в плановом расписании больше количества выходов в наряде.
    l_rv_order_shortage := array_remove(ttb.tt_oper_pkg$rv_order_shortage(p_date), null);

    if array_length(l_orders_shift_time_violate, 1) > 0 then
      declare
        l_send_result json;
      begin
        l_send_result := ttb.tt_oper_pkg$send_incident(
            p_incidentReason := 125
            ,p_orderList := l_orders_shift_time_violate::int[]
            ,p_ttVariantList := l_tt_variant_id_new::int[]
        );
      end;
    end if;

    if array_length(l_orders_shift_time_violate, 1) > 0 then
      declare
        l_send_result json;
      begin

        l_send_result := ttb.tt_oper_pkg$send_incident(
            p_incidentReason := 126
            ,p_orderList := l_orders_tr_overlap::int[]
            ,p_ttVariantList := l_tt_variant_id_new::int[]
        );


      end;
    end if;

    if array_length(l_orders_shift_time_violate, 1) > 0 then
      declare
        l_send_result json;
      begin
        l_send_result := ttb.tt_oper_pkg$send_incident(
            p_incidentReason := 127
            ,p_orderList := l_orders_driver_overlap::int[]
            ,p_ttVariantList := l_tt_variant_id_new::int[]
        );
      end;
    end if;

    if array_length(l_orders_id_class_corr_violate, 1) > 0 then
      declare
        l_send_result json;
      begin
        l_send_result := ttb.tt_oper_pkg$send_incident(
            p_incidentReason := 128
            ,p_orderList := l_orders_id_class_corr_violate::int[]
            ,p_ttVariantList := l_tt_variant_id_new::int[]
        );
      end;
    end if;

    if array_length(l_route_variant_id_out_count_corr_violate, 1) > 0 then
      declare
        l_send_result json;
      begin
        l_send_result := ttb.tt_oper_pkg$send_incident(
            p_incidentReason := 129
            ,p_ttVariantList := l_tt_variant_id_new::int[]
            ,p_routeVarList := l_route_variant_id_out_count_corr_violate::int[]
        );
      end;
    end if;

    if array_length(l_rv_order_shortage, 1) > 0 then
      declare
        l_send_result json;
        l_tt_variant_id_a int;
      begin
         for i in array_lower(l_rv_order_shortage, 1)..array_upper(l_rv_order_shortage, 1) loop

            select tt_variant_id
            into l_tt_variant_id_a
            from ttb.tt_variant
            where route_variant_id = l_rv_order_shortage[i]
                  and   order_date =  p_date
                  AND tt_variant.parent_tt_variant_id IS NOT NULL
                  AND NOT sign_deleted;

            l_send_result := ttb.tt_oper_pkg$send_incident(
                p_incidentReason := 129
                ,p_routeVarList := ARRAY[l_rv_order_shortage[i]]::int[]
                ,p_ttVariantList := ARRAY[l_tt_variant_id_a]::int[]
                ,p_incident_type_id := ttb.jf_incident_pkg$cn_order_shortage()::int
            );
               RAISE LOG 'create_tt_oper.create send incidents';
         end loop;
      end;
    end if;

  end if;

  --*****************************************************************************************************

  with a as (
      select
        ttb.tt_oper_pkg$create_tt_oper_by_order_list(order_list.*) tt_out_id
      from ttb.order_list
      where order_list_id = ANY(l_order_list_id_array)
  )
  select array_agg(distinct tt_out_id)
  into l_tt_out_array
  from a
  where tt_out_id is not null;

  declare
    l_send_result json;
  begin
    --создадим инцидент

    if array_length(l_tt_variant_id_new, 1) > 0 or array_length(l_tt_out_array, 1) >0 then
      l_send_result := ttb.tt_oper_pkg$send_incident(
          p_ttVariantList := l_tt_variant_id_new::int[]
          ,p_ttOutList := l_tt_out_array
      );

    end if;
  end;

  --Зафиксируем обработку наряда
  update ttb.order_list
  set tt_out_created = true
  where order_list_id = ANY(l_order_list_id_array);

  RAISE LOG 'create_tt_oper.end';

  return 1;

  exception
  when others then
    GET STACKED DIAGNOSTICS l_text_var1 = MESSAGE_TEXT;
    RAISE LOG 'create_tt_oper.error: %', l_text_var1;
    return 0;

END;
$$;





CREATE OR REPLACE FUNCTION ttb.tt_oper_pkg$create_tt_variant(
    p_route_variant_id ttb.order_list.route_variant_id%type
    ,p_dt date
)
  RETURNS INTEGER
LANGUAGE plpgsql
AS $$
DECLARE
    l_tt_variant_id ttb.tt_variant.tt_variant_id%type;
    l_send_result json;
BEGIN

WITH inserted_tt_variant as (
  INSERT INTO ttb.tt_variant
  (norm_id
    , route_variant_id
    , is_all_tr_online
    , sign_deleted
    , sys_period
    , tt_status_id
    , action_period
    , ttv_name
    , is_calc_indicator
    , parent_tt_variant_id
    , order_date
  )
    SELECT
      norm_id,
      route_variant_id,
      is_all_tr_online,
      sign_deleted,
      sys_period,
      tt_status_id,
      tsrange(p_dt::timestamp, p_dt + interval '23:23:59'),
      ttv_name,
      is_calc_indicator,
      tt_variant.tt_variant_id,
      p_dt
    FROM ttb.tt_variant join ttb.v_tt_calendar_date
            on v_tt_calendar_date.tt_variant_id = tt_variant.tt_variant_id
    WHERE route_variant_id = p_route_variant_id
          AND NOT sign_deleted
          AND parent_tt_variant_id is null
          AND tt_variant.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id()
          AND tt_variant.action_period @> p_dt::timestamp
          AND v_tt_calendar_date.dt = p_dt
          AND tt_variant.tt_variant_id in  (
                                    SELECT min(tt_variant.tt_variant_id)
                                    FROM ttb.tt_variant join ttb.v_tt_calendar_date
                                            on v_tt_calendar_date.tt_variant_id = tt_variant.tt_variant_id
                                    WHERE route_variant_id = p_route_variant_id
                                          AND NOT sign_deleted
                                          AND tt_variant.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id()
                                          AND parent_tt_variant_id is null
                                          AND tt_variant.action_period @> p_dt::timestamp
                                          AND v_tt_calendar_date.dt = p_dt
                                 )
  RETURNING tt_variant_id, parent_tt_variant_id
),
inserted_tt_out as
  (INSERT INTO ttb.tt_out
  (tt_variant_id
    , tr_capacity_id
    , mode_id
    , tt_out_num
    , sign_deleted
    , parent_tt_out_id)
    SELECT
      inserted_tt_variant.tt_variant_id
      ,tt_out.tr_capacity_id
      ,tt_out.mode_id
      ,tt_out.tt_out_num
      ,tt_out.sign_deleted
      ,tt_out.tt_out_id
    FROM ttb.tt_out
      JOIN inserted_tt_variant ON tt_out.tt_variant_id = inserted_tt_variant.parent_tt_variant_id
    WHERE NOT tt_out.sign_deleted
      AND parent_tt_out_id is null
  RETURNING tt_out_id, parent_tt_out_id
  ),
inserted_tt_action as (
insert into ttb.tt_action
      (tt_out_id
      ,action_dur
      ,dt_action
      ,action_type_id
      ,sign_deleted
      ,tt_action_group_id
      ,action_gr_id
      ,parent_tt_action_id)
      select
        inserted_tt_out.tt_out_id
        ,action_dur
        ,dt_action
        ,action_type_id
        ,sign_deleted
        ,tt_action_group_id
        ,action_gr_id
        ,tt_action_id
      from ttb.tt_action
        join inserted_tt_out on tt_action.tt_out_id = inserted_tt_out.parent_tt_out_id
      where not sign_deleted
            and parent_tt_action_id is null
    RETURNING parent_tt_action_id, tt_action_id
),
inserted_tt_action_item as (
    insert into ttb.tt_action_item
            (tt_action_id
            ,time_begin
            ,time_end
            ,stop_item2round_id
            ,action_type_id
            ,sign_deleted
            ,dr_shift_id)
        select inserted_tt_action.tt_action_id
            ,ttb.get_local_timestamp(p_dt  + (time_begin - ttb.jf_incident_pkg$cn_plan_tt_date()))
            ,ttb.get_local_timestamp(p_dt  + (time_end - ttb.jf_incident_pkg$cn_plan_tt_date()))
            ,stop_item2round_id
            ,action_type_id
            ,sign_deleted
            ,dr_shift_id
    from inserted_tt_action
         join ttb.tt_action_item on inserted_tt_action.parent_tt_action_id = tt_action_item.tt_action_id
    where not sign_deleted
),
inserted_tt_action_round as (
    insert into ttb.tt_action_round
        (tt_action_id
        ,round_id
        ,round_status_id
        )
    select inserted_tt_action.tt_action_id
        ,round_id
        ,round_status_id
    from inserted_tt_action join ttb.tt_action_round on inserted_tt_action.parent_tt_action_id =  tt_action_round.tt_action_id
)
select tt_variant_id
into l_tt_variant_id
from inserted_tt_variant;

perform ttb.tt_oper_pkg$update_dt_action(l_tt_variant_id);

return l_tt_variant_id;
END;
$$;


CREATE OR REPLACE FUNCTION ttb.tt_oper_pkg$orders_shift_time_violate(p_date date)
  RETURNS bigint[]
LANGUAGE plpgsql
AS $$
DECLARE
    l_array_order_list_id bigint[];
BEGIN
--1. По каждому выходу Общее время смены в наряде равно общему времени смены в плановом расписании.
with plan as (
    SELECT
      tt_variant.route_variant_id,
      tt_out.tt_out_num,
      dr_shift.dr_shift_num,
      max(tt_action_item.time_end) - min(tt_action_item.time_begin) period
    FROM
      ttb.tt_variant
      JOIN ttb.tt_out ON tt_out.tt_variant_id = tt_variant.tt_variant_id
      JOIN ttb.tt_action ON tt_out.tt_out_id = tt_action.tt_out_id
      JOIN ttb.tt_action_item ON tt_action_item.tt_action_id = tt_action.tt_action_id
      JOIN ttb.dr_shift ON dr_shift.dr_shift_id = tt_action_item.dr_shift_id
    WHERE NOT tt_out.sign_deleted
          AND NOT tt_action.sign_deleted
          AND NOT tt_action_item.sign_deleted
          AND NOT tt_variant.sign_deleted
          AND tt_variant.action_period @> p_date::timestamp
    GROUP BY
      tt_variant.route_variant_id
      , tt_out.tt_out_num
      , dr_shift.dr_shift_num
),
orders as (
      SELECT
        order_list_id,
        route_variant_id,
        out_num,
        dr_shift.dr_shift_num,
        time_to - time_from period
      FROM ttb.order_list
        JOIN ttb.dr_shift ON order_list.dr_shift_id = dr_shift.dr_shift_id
      WHERE order_date = p_date

  )
      SELECT array_agg(order_list_id)
      INTO l_array_order_list_id
      FROM orders
        JOIN plan ON
                    orders.route_variant_id = plan.route_variant_id
                    AND orders.out_num = plan.tt_out_num
                    AND orders.dr_shift_num = plan.dr_shift_num
                    AND orders.period != orders.period
;
   return l_array_order_list_id;
END;
$$;
--Пересечений времени работы ТС
CREATE OR REPLACE FUNCTION ttb.tt_oper_pkg$orders_tr_overlap(p_date date)
  RETURNS bigint[]
LANGUAGE plpgsql
AS $$
DECLARE
    l_array_order_list_id bigint[];
BEGIN
WITH orders as (
    SELECT *
    FROM ttb.order_list
    WHERE order_date = p_date
)
      SELECT array_agg(order_list.order_list_id)
      INTO l_array_order_list_id
      FROM ttb.order_list
        JOIN orders ON order_list.tr_id = orders.tr_id
      WHERE order_list.order_date = p_date
            AND order_list.order_list_id != orders.order_list_id
            AND tsrange(order_list.time_from, order_list.time_to) &&
                    tsrange(orders.time_from, orders.time_to);
   return l_array_order_list_id;
END;
$$;

--Пересечений времени работы водителя
CREATE OR REPLACE FUNCTION ttb.tt_oper_pkg$orders_driver_overlap(p_date date)
  RETURNS bigint[]
LANGUAGE plpgsql
AS $$
DECLARE
    l_array_order_list_id bigint[];
BEGIN
WITH orders as (
    SELECT *
    FROM ttb.order_list
    WHERE order_date = p_date
)
      SELECT array_agg(order_list.order_list_id)
      INTO l_array_order_list_id
      FROM ttb.order_list
        JOIN orders ON order_list.driver_id = orders.driver_id
      WHERE order_list.order_date = p_date
            AND order_list.order_list_id != orders.order_list_id
            AND tsrange(order_list.time_from, order_list.time_to) &&
                    tsrange(orders.time_from, orders.time_to);
   return l_array_order_list_id;
END;
$$;

--Класс ТС в плановом расписании и класс ТС в наряде НЕ совпадают
CREATE OR REPLACE FUNCTION ttb.tt_oper_pkg$orders_class_corr_violate(p_date date)
  RETURNS bigint[]
LANGUAGE plpgsql
AS $$
DECLARE
    l_array_order_list_id bigint[];
BEGIN
with plan as (
    SELECT
      tt_variant.route_variant_id,
      tt_out.tt_out_num,
      tt_out.tr_capacity_id
    FROM
      ttb.tt_variant
      JOIN ttb.tt_out ON tt_out.tt_variant_id = tt_variant.tt_variant_id
    WHERE NOT tt_out.sign_deleted
          AND not tt_variant.sign_deleted
          AND tt_variant.parent_tt_variant_id is null
          AND tt_variant.action_period @> p_date::timestamp
),
    orders as (
      SELECT
        order_list_id,
        route_variant_id,
        out_num,
        tr_model.tr_capacity_id
      FROM ttb.order_list
            join core.tr on order_list.tr_id = tr.tr_id
            join core.tr_model on tr.tr_model_id = tr_model.tr_model_id
      WHERE order_date = p_date

  )
    SELECT array_agg(order_list_id)
    INTO l_array_order_list_id
    FROM orders
      JOIN plan ON
                  orders.route_variant_id = plan.route_variant_id
                  AND orders.out_num = plan.tt_out_num
    WHERE orders.tr_capacity_id != plan.tr_capacity_id;

   return l_array_order_list_id;
END;
$$;

--6. Количество выходов в плановом расписании НЕ равно количеству выходов в наряде.
CREATE OR REPLACE FUNCTION ttb.tt_oper_pkg$rv_out_count_corr_violate(p_date date)
  RETURNS bigint[]
LANGUAGE plpgsql
AS $$
DECLARE
    l_array_route_variant_id bigint[];
BEGIN
with plan as (
    SELECT
      tt_variant.route_variant_id,
      count(*) cnt
    FROM
      ttb.tt_variant
      JOIN ttb.tt_out ON tt_out.tt_variant_id = tt_variant.tt_variant_id
    WHERE NOT tt_out.sign_deleted
          AND not tt_variant.sign_deleted
          AND tt_variant.action_period @> p_date::timestamp
          AND tt_variant.parent_tt_variant_id is null
          AND tt_out.parent_tt_out_id is null
    GROUP BY tt_variant.route_variant_id
),
    orders as (
      SELECT
        route_variant_id,
        count(distinct out_num) cnt
      FROM ttb.order_list
      WHERE order_date = p_date
    GROUP BY route_variant_id)
SELECT array_agg(orders.route_variant_id)
INTO l_array_route_variant_id
FROM plan
  LEFT JOIN orders ON
              orders.route_variant_id = plan.route_variant_id
              AND orders.cnt != plan.cnt
              AND orders.route_variant_id is not null;
   return l_array_route_variant_id;
END;
$$;


--6. Количество выходов в плановом расписании больше количества выходов в наряде.
CREATE OR REPLACE FUNCTION ttb.tt_oper_pkg$rv_order_shortage(p_date date)
  RETURNS bigint[]
LANGUAGE plpgsql
AS $$
DECLARE
    l_array_route_variant_id bigint[];
BEGIN
with plan as (
    SELECT
      tt_variant.route_variant_id,
      count(*) cnt
    FROM
      ttb.tt_variant
      JOIN ttb.tt_out ON tt_out.tt_variant_id = tt_variant.tt_variant_id
    WHERE NOT tt_out.sign_deleted
          AND not tt_variant.sign_deleted
          AND tt_variant.action_period @> p_date::timestamp
          AND tt_variant.parent_tt_variant_id is null
          AND tt_out.parent_tt_out_id is null
    GROUP BY tt_variant.route_variant_id
),
    orders as (
      SELECT
        route_variant_id,
        count(distinct out_num) cnt
      FROM ttb.order_list
      WHERE order_date = p_date
    GROUP BY route_variant_id)
SELECT array_agg(orders.route_variant_id)
INTO l_array_route_variant_id
FROM plan
  LEFT JOIN orders ON
              orders.route_variant_id = plan.route_variant_id
              AND orders.cnt < plan.cnt
              AND orders.route_variant_id is not null;
   return l_array_route_variant_id;
END;
$$;


CREATE OR REPLACE FUNCTION ttb.tt_oper_pkg$send_incident(
p_incidentReason int default null
, p_routeVarList int[] default null
, p_orderList int[] default null
, p_ttActionList int[] default null
, p_ttOutList int[] default null
, p_ttVariantList int[] default null
, p_incident_type_id int default ttb.jf_incident_pkg$cn_timetable_update()

)
  RETURNS json
LANGUAGE plpgsql
AS $$
BEGIN
    return ttb.jf_incident_pkg$send_incident(
                    p_incidents_array :=
                            ARRAY[ttb.jf_incident_pkg$new_incident(
                                    p_timeStart := now()::timestamp
                                    ,p_incident_initiator_id := ttb.jf_incident_pkg$cn_initiator_system()
                                    ,p_incident_status_id := ttb.jf_incident_pkg$cn_status_new()
                                    ,p_incident_type_id := p_incident_type_id
                                    ,p_parent_incident_id := null
                                    ,p_account_id := null
                                    ,p_incident_payload :=
                                    ttb.jf_incident_pkg$new_payload (
                                            p_routeVarList := p_routeVarList
                                            ,p_orderList := p_orderList
                                            ,p_ttActionList := p_ttActionList
                                            ,p_incidentReason := p_incidentReason
                                            ,p_ttVariantList := p_ttVariantList
                                            ,p_ttOutList := p_ttOutList
                                            )
                        )]
            );
END;
$$;


CREATE OR REPLACE FUNCTION ttb.tt_oper_pkg$update_dt_action(
    p_tt_variant_id ttb.tt_action.tt_action_id%type
    )
returns void
LANGUAGE sql
AS $$
    update ttb.tt_action
    set dt_action = time_start,
        action_dur = duration,
        action_range = ar
    from (
      SELECT
        tt_action.tt_action_id,
        min(time_begin) time_start,
        EXTRACT(EPOCH FROM max(time_end) - min(time_begin)) duration,
         core.emptyif(tsrange(min(time_begin), max(time_end))) ar
      FROM ttb.tt_action_item
        JOIN ttb.tt_action ON tt_action_item.tt_action_id = tt_action.tt_action_id
        JOIN ttb.tt_out ON tt_action.tt_out_id = tt_out.tt_out_id
      WHERE tt_out.tt_variant_id = p_tt_variant_id
            and not tt_action_item.sign_deleted
            and not tt_action.sign_deleted
            and not tt_out.sign_deleted
      GROUP BY tt_action.tt_action_id
    ) a
    where a.tt_action_id = tt_action.tt_action_id;

    with u as (
          update ttb.tt_out
              set action_range = ar
              from (
                     SELECT tt_out.tt_out_id,
                        core.emptyif(tsrange(min(lower(tt_action.action_range)), max(upper(tt_action.action_range))))  ar
                     FROM ttb.tt_action
                       JOIN ttb.tt_out ON tt_action.tt_out_id = tt_out.tt_out_id
                     WHERE tt_out.tt_variant_id = p_tt_variant_id
                           and not tt_action.sign_deleted
                           and not tt_out.sign_deleted
                     GROUP BY tt_out.tt_out_id
                   ) a
              where a.tt_out_id = tt_out.tt_out_id
              returning tt_out.action_range out_range
    )
    update ttb.tt_variant
    set action_period = (select tsrange(min(lower(u.out_range)), max(upper(u.out_range))) from u)
    where tt_variant_id = p_tt_variant_id
          and parent_tt_variant_id is not null;
$$;

CREATE OR REPLACE FUNCTION ttb.tt_oper_pkg$delete_by_date(
    p_date date
    )
returns integer
LANGUAGE sql
AS $$
with a AS
(
    SELECT tt_action.tt_action_id, tt_out.tt_out_id, tt_variant.tt_variant_id
    FROM ttb.tt_variant
      LEFT JOIN ttb.tt_out ON tt_variant.tt_variant_id = tt_out.tt_variant_id
      LEFT JOIN ttb.tt_action ON tt_out.tt_out_id = tt_action.tt_out_id
      LEFT JOIN ttb.tt_action_item ON tt_action.tt_action_id = tt_action_item.tt_action_id
      LEFT JOIN ttb.tt_action_round ON tt_action.tt_action_id = tt_action_round.tt_action_id
    WHERE order_date = p_date
)
,tt_action_fact as (
    DELETE
    FROM ttb.tt_action_fact
    using a
    where tt_action_fact.tt_action_id = a.tt_action_id
)
,tt_action_round as (
    DELETE
    FROM ttb.tt_action_round
    USING a
    WHERE tt_action_round.tt_action_id = a.tt_action_id)
,tt_action_item as (
    DELETE
    FROM ttb.tt_action_item
    using a
    WHERE tt_action_item.tt_action_id  = a.tt_action_id
)
,tt_action as (
  DELETE
  FROM ttb.tt_action
  using a
  WHERE tt_action.tt_action_id  = a.tt_action_id
)
,tt_out as (
  DELETE
  FROM ttb.tt_out
  using a
  WHERE tt_out.tt_out_id  = a.tt_out_id
)
,tt_variant as (
  DELETE
  FROM ttb.tt_variant
  using a
  WHERE tt_variant.tt_variant_id  = a.tt_variant_id
)
select tt_variant_id
from a;
$$;




CREATE OR REPLACE FUNCTION ttb.tt_oper_pkg$clone_tt_out(
    p_tt_out_id_from ttb.tt_out.tt_out_id%type
    ,p_time_from timestamp
)
  RETURNS INTEGER
LANGUAGE plpgsql
AS $$
DECLARE
    l_tt_out_id ttb.tt_out.tt_out_id%type;
BEGIN

with inserted_tt_out as
  (
  INSERT INTO ttb.tt_out
  (tt_variant_id
    , tr_capacity_id
    , mode_id
    , tt_out_num
    , sign_deleted
    , parent_tt_out_id)
    SELECT
      tt_out.tt_variant_id
      ,tt_out.tr_capacity_id
      ,tt_out.mode_id
      ,tt_out.tt_out_num
      ,tt_out.sign_deleted
      ,tt_out.parent_tt_out_id
    FROM ttb.tt_out
    WHERE tt_out_id = p_tt_out_id_from
  RETURNING tt_out_id, parent_tt_out_id
  ),
inserted_tt_action as (
insert into ttb.tt_action
      (tt_out_id
      ,action_dur
      ,dt_action
      ,action_type_id
      ,sign_deleted
      ,tt_action_group_id
      ,action_gr_id
      ,parent_tt_action_id)
      select
        inserted_tt_out.tt_out_id
        ,action_dur
        ,dt_action
        ,action_type_id
        ,sign_deleted
        ,tt_action_group_id
        ,action_gr_id
        ,tt_action_id
      from ttb.tt_action
        join inserted_tt_out on tt_action.tt_out_id = inserted_tt_out.parent_tt_out_id
      where not sign_deleted
            and parent_tt_action_id is null
    RETURNING parent_tt_action_id, tt_action_id
),
inserted_tt_action_item as (
    insert into ttb.tt_action_item
            (tt_action_id
            ,time_begin
            ,time_end
            ,stop_item2round_id
            ,action_type_id
            ,sign_deleted
            ,dr_shift_id)
        select inserted_tt_action.tt_action_id
            ,ttb.get_local_timestamp(p_time_from::date  + (time_begin - ttb.jf_incident_pkg$cn_plan_tt_date()))
            ,ttb.get_local_timestamp(p_time_from::date  + (time_end - ttb.jf_incident_pkg$cn_plan_tt_date()))
            ,stop_item2round_id
            ,action_type_id
            ,sign_deleted
            ,dr_shift_id
    from inserted_tt_action
         join ttb.tt_action_item on inserted_tt_action.parent_tt_action_id = tt_action_item.tt_action_id
    where not sign_deleted
          and ttb.get_local_timestamp(p_time_from::date  + (time_begin - ttb.jf_incident_pkg$cn_plan_tt_date())) > p_time_from
),
inserted_tt_action_round as (
    insert into ttb.tt_action_round
        (tt_action_id
        ,round_id
        ,round_status_id
        )
    select inserted_tt_action.tt_action_id
        ,round_id
        ,round_status_id
    from inserted_tt_action join ttb.tt_action_round on inserted_tt_action.parent_tt_action_id =  tt_action_round.tt_action_id
)
select tt_out_id
into l_tt_out_id
from inserted_tt_out;

perform ttb.tt_oper_pkg$update_dt_action(l_tt_variant_id);

return l_tt_out_id;
END;
$$;


CREATE OR REPLACE FUNCTION ttb.tt_oper_pkg$is_order_list_accepted(  p_tt_out_id ttb.tt_out.tt_out_id%type
                                                            ,p_order_list ttb.order_list
                                                         )
RETURNS boolean
AS $$
declare
    l_is_out_started boolean;
    l_15_00_Msk timestamp = now()::date + '12:00:00'::time;
    l_dr_shift_num smallint;
    l_tr_id bigint;
begin


  --Нужно проверять, если меняется ТС для смены 2, то смотреть время начала работы ТС в смене 2 - 15:00,
  --и если изменение придет ранее, чем 15:00 принимать его.
  --http://redmine.advantum.ru/issues/24381
  select dr_shift_num
  into l_dr_shift_num
  from ttb.dr_shift
  where dr_shift_id = p_order_list.dr_shift_id;

  select tr_id
  into l_tr_id
  from ttb.tt_out
  where tt_out_id =  p_tt_out_id;

  if  l_dr_shift_num = 2
            and now() < l_15_00_Msk
            and l_tr_id <> p_order_list.tr_id  then
    return true;

  else

    select count(*) > 0
    into l_is_out_started
    from ttb.tt_action
    JOIN ttb.tt_action_round on tt_action.tt_action_id = tt_action_round.tt_action_id
    where round_status_id != ttb.round_status_pkg$planned()
    and tt_out_id = p_tt_out_id limit 1;

  end if;

  return not l_is_out_started;
end;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE function  tt_oper_pkg$create_tt_oper_by_order_list(
  p_order_list ttb.order_list
)
  returns integer as
$$
declare
  l_tt_out_id int;
begin
  --Найдем выход
  with tt_out_for_order as (

      select tt_out_id, tr_id
      from ttb.tt_variant join ttb.tt_out on tt_out.tt_variant_id = tt_variant.tt_variant_id
      where route_variant_id = p_order_list.route_variant_id
            and not tt_variant.sign_deleted
            and order_date = p_order_list.order_date
            and parent_tt_variant_id is not null
            and tt_out_num = p_order_list.out_num
            and not tt_out.sign_deleted
            and parent_tt_out_id is not null
            and (tr_id is null or not ttb.tt_oper_pkg$is_order_list_ignored(tt_out.tt_out_id, p_order_list))

  ),
      tt_out_updated as (
      update ttb.tt_out
      set tr_id = p_order_list.tr_id
      from tt_out_for_order
      where tt_out.tt_out_id = tt_out_for_order.tt_out_id
      returning tt_out.tt_out_id
    )
  SELECT tt_out_id
  into l_tt_out_id
  from tt_out_updated;


  update ttb.tt_action_item
  set driver_id = p_order_list.driver_id
  from ttb.tt_variant join ttb.tt_out on tt_out.tt_variant_id = tt_variant.tt_variant_id
    join ttb.tt_action on tt_out.tt_out_id = tt_action.tt_out_id
  where
    tt_variant.order_date = p_order_list.order_date
    and tt_out.tt_out_num = p_order_list.out_num
    and tt_variant.route_variant_id = p_order_list.route_variant_id
    and not tt_variant.sign_deleted
    and parent_tt_variant_id is not null
    and parent_tt_out_id is not null
    and tt_action_item.dr_shift_id = p_order_list.dr_shift_id
    and tt_action_item.tt_action_id = tt_action.tt_action_id
    and not tt_action_item.sign_deleted
    and (tt_out.tr_id is null or not ttb.tt_oper_pkg$is_order_list_ignored(tt_out.tt_out_id, p_order_list));


  update ttb.tt_action
  set driver_id = p_order_list.driver_id
  from ttb.tt_variant join ttb.tt_out on tt_out.tt_variant_id = tt_variant.tt_variant_id
  where
    tt_variant.order_date = p_order_list.order_date
    and tt_out.tt_out_num = p_order_list.out_num
    and tt_action.tt_out_id = tt_out.tt_out_id
    and tt_variant.route_variant_id = p_order_list.route_variant_id
    and parent_tt_variant_id is not null
    and parent_tt_out_id is not null
    and not tt_variant.sign_deleted
    and (tt_out.tr_id is null or not ttb.tt_oper_pkg$is_order_list_ignored(tt_out.tt_out_id, p_order_list));
  return l_tt_out_id;
end;
$$ LANGUAGE plpgsql;




--1. пересечение времени начала и окончания работы одного водителя на различных ТС;
--2. пересечение времени начала и окончания работы одного водителя на разных маршрутах;
--3. пересечение времени работы одного ТС при управлении различными водителями;
--4. пересечение времени работы одного ТС одновременно на разных маршрутах;
--5. наличие разрывов во времени работы водителей, назначенных на управление одним ТС.

