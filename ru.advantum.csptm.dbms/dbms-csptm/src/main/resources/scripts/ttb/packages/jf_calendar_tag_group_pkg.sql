CREATE OR REPLACE FUNCTION ttb."jf_calendar_tag_group_pkg$attr_to_rowtype"(p_attr text)
 RETURNS ttb.calendar_tag_group
 LANGUAGE plpgsql
AS $function$
 
declare 
   l_r ttb.calendar_tag_group%rowtype; 
begin 
   l_r.group_name := jofl.jofl_pkg$extract_varchar(p_attr, 'group_name', true); 
   l_r.calendar_tag_group_id := jofl.jofl_pkg$extract_varchar(p_attr, 'calendar_tag_group_id', true); 
   l_r.is_alone_group := jofl.jofl_pkg$extract_boolean(p_attr, 'is_alone_group', true);

   return l_r;
end;
 
$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_calendar_tag_group_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
 
declare 
begin 
 open p_rows for 
      select 
        group_name, 
        is_alone_group,
        calendar_tag_group_id
      from ttb.calendar_tag_group; 
end;
 
$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_calendar_tag_group_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
 
declare 
   l_r ttb.calendar_tag_group%rowtype;
begin 
   l_r := ttb.jf_calendar_tag_group_pkg$attr_to_rowtype(p_attr);

   update ttb.calendar_tag_group set 
          group_name = l_r.group_name
     ,is_alone_group = l_r.is_alone_group
   where 
          calendar_tag_group_id = l_r.calendar_tag_group_id;

   return null;
end;
 
$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_calendar_tag_group_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
 
declare 
   l_r ttb.calendar_tag_group%rowtype;
begin 
   l_r := ttb.jf_calendar_tag_group_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.calendar_tag_group where  calendar_tag_group_id = l_r.calendar_tag_group_id;

   return null;
end;
 
$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_calendar_tag_group_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$

declare 
   l_r ttb.calendar_tag_group%rowtype;
begin 
   l_r := ttb.jf_calendar_tag_group_pkg$attr_to_rowtype(p_attr);
   l_r.calendar_tag_group_id := nextval( 'ttb.calendar_tag_group_calendar_tag_group_id_seq' );

   insert into ttb.calendar_tag_group select l_r.*;

   return null;
end;

$function$
;