create or replace function ttb.tt_out_mode_report_pkg$of_rows(p_id_account numeric, out p_rows refcursor,
                                                              p_attr       text)
  returns refcursor as
$function$
declare
  l_tt_variant_id ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_tt_variant_id', true);
  ln_tr_type_id   rts.route.tr_type_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id', true);
  ln_route_id     rts.route.route_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', true);
  ln_tt_status_id ttb.tt_variant.tt_status_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_tt_status_id', true);
  l_rf_db_method  text := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true), 'xxx');
  f_disp_dt       timestamp := jofl.jofl_pkg$extract_date(p_attr, 'f_disp_DT', true);
begin
  open p_rows for
  select
    l_tt_variant_id                                         p_tt_variant,
    r.route_num                                             p_route_num,
    to_char(lower(ttv.action_period) :: date, 'dd.mm.yyyy') p_date

  --не уверен, что теперь  это где-то нужно, но пока оставлю
  /* ttv.tt_variant_id,
   ttv.norm_id,
   ttv.route_variant_id,
   ttv.is_all_tr_online,
   ttv.sign_deleted,
   --ttv.sys_period,
   ttv.tt_status_id,
   ttv.action_period,
   ttv.ttv_name,
   ttv.ttv_comment,
   r.route_num || ' (' || to_char(lower(rv.action_period), 'dd.mm.yyyy') || '-' ||
   coalesce(to_char(upper(rv.action_period), 'dd.mm.yyyy'), 'нвр') || ')' as                             p_filter_text,
   ttv.is_calc_indicator,
   n.norm_name,
   r.route_num,
   lower(ttv.action_period)                                                                              ap_b,
   upper(ttv.action_period)                                                                              ap_e,
   rv.action_period :: text                                                                              rv_action_period,
   st.tt_status_name,
   r.tr_type_id,
   r.route_id,
   case
   when ttb.agreement_pkg$is_available_to_agreement(p_id_account, ttv.tt_variant_id)
     then case when ttb.jf_tt_variant_pkg$available_to_cancel(ttv.tt_variant_id)
       then 'P{A},D{OF_TO_AGREEMENT, OF_DELETE, OF_CLEAR}'
          when ttb.jf_tt_variant_pkg$available_to_terminate(ttv.tt_variant_id)
            then 'P{A},D{OF_TO_AGREEMENT, OF_TERMINATE},P{RO},D{ap_e}'
          else 'P{A},D{OF_TO_AGREEMENT}'
          end
   else case when ttb.jf_tt_variant_pkg$available_to_cancel(ttv.tt_variant_id)
     then 'P{A},D{OF_DELETE, OF_CLEAR}'
        when ttb.jf_tt_variant_pkg$available_to_terminate(ttv.tt_variant_id)
          then 'P{A},D{OF_TERMINATE},P{RO},D{ap_e}'
        else null
        end
   end                                                                                                   "ROW$POLICY",
   '[' || json_build_object('CAPTION', 'Выходы', 'JUMP',
                            json_build_object('METHOD', 'ttb.tt_out', 'ATTR',
                                              '{"tt_variant_id":"' || ttv.tt_variant_id || '"}')) || ']' "JUMP_TO_OUT"*/
  from ttb.tt_variant ttv
    join ttb.norm n on n.norm_id = ttv.norm_id
    join ttb.tt_status st on st.tt_status_id = ttv.tt_status_id
    join rts.route_variant rv on ttv.route_variant_id = rv.route_variant_id
    join rts.route r on rv.route_id = r.route_id
  where ttv.parent_tt_variant_id is null
        --         and ttv.tt_status_id in (ttb.jf_tt_status_pkg$cn_work_status_id())
        and ttv.tt_variant_id = l_tt_variant_id;
end;
$function$
language plpgsql;

create or replace function ttb.tt_out_mode_report_pkg$report_header(p_tt_variant_id text)
  returns table(
    route_number  text,
    route_variant text
  )
as $$
declare
begin
  return query
  select
    split_part(left(ttv.ttv_name, strpos(ttv.ttv_name, '/') - 1), '-', 2) :: text as route_num,
    left(ttv.ttv_name, strpos(ttv.ttv_name, '-') - 1) :: text                     as route_variant
  from ttb.tt_variant ttv
  where ttv.tt_variant_id = p_tt_variant_id :: int
  group by
    ttv.ttv_name;
  --       rrv.route_variant_id;
end;
$$
language plpgsql;

create or replace function ttb.tt_out_mode_report_pkg$report(p_tt_variant_id text)
  returns table(
    out_number                text,
    first_prod_interval_sh1   text,
    first_break_interval_sh1  text,
    second_prod_interval_sh1  text,
    second_break_interval_sh1 text,
    last_prod_interval_sh1    text,
    total_prod_sh1            text,
    first_prod_interval_sh2   text,
    first_break_interval_sh2  text,
    second_prod_interval_sh2  text,
    second_break_interval_sh2 text,
    last_prod_interval_sh2    text,
    total_prod_sh2            text,
    total_prod                text

  ) as $$
declare
begin
  return query
  with s1 as (
      select
        footing.tt_out_num,
        footing.agg_time_begin,
        footing.agg_time_end,
        footing.is_technical_round,
        footing.is_dinner,
        (select sh.dr_shift_num
         from ttb.dr_shift sh
         where sh.dr_shift_id = footing.dr_shift_id) shift_num
      from ttb.tt_out_pkg$get_set4out_action_agg(p_tt_variant_id :: integer) footing
      order by tt_out_num, agg_time_begin
  ),
      a0 as (select *
             from s1
             where s1.shift_num = 1
    ),
      a1 as (select *
             from (
                    select
                      a0.tt_out_num,
                      min(a0.agg_time_begin)
                      over (
                        partition by tt_out_num ) start_time,
                      max(a0.agg_time_end)
                      over (
                        partition by tt_out_num ) end_time
                    from a0
                    union
                    select
                      a0.tt_out_num,
                      a0.agg_time_begin start_time,
                      a0.agg_time_end   end_time
                    from a0
                    where is_dinner
                  ) tmp
             order by tt_out_num, tmp.start_time ),
      a2 as (
        select
          row_number()
          over (
            partition by a1.tt_out_num ) row_num,
          *
        from a1
        order by tt_out_num, a1.start_time),
      a3 as (
        select
          a2.tt_out_num,
          (select tmp.start_time
           from a2 tmp
           where a2.tt_out_num = tmp.tt_out_num and tmp.row_num = 1) as out_start,
          (select tmp.start_time
           from a2 tmp
           where a2.tt_out_num = tmp.tt_out_num and tmp.row_num = 2) as first_dinner_start,
          (select tmp.end_time
           from a2 tmp
           where a2.tt_out_num = tmp.tt_out_num and tmp.row_num = 2) as first_dinner_end,
          (select tmp.start_time
           from a2 tmp
           where a2.tt_out_num = tmp.tt_out_num and tmp.row_num = 3) as second_dinner_start,
          (select tmp.end_time
           from a2 tmp
           where a2.tt_out_num = tmp.tt_out_num and tmp.row_num = 3) as second_dinner_end,
          (select tmp.end_time
           from a2 tmp
           where a2.tt_out_num = tmp.tt_out_num and tmp.row_num = 1) as out_end
        from a2
        group by
          a2.tt_out_num,
          out_start
    ),
      b0 as (select *
             from s1
             where s1.shift_num = 2
    ),
      b1 as (select *
             from (
                    select
                      b0.tt_out_num,
                      min(b0.agg_time_begin)
                      over (
                        partition by tt_out_num ) start_time,
                      max(b0.agg_time_end)
                      over (
                        partition by tt_out_num ) end_time
                    from b0
                    union
                    select
                      b0.tt_out_num,
                      b0.agg_time_begin start_time,
                      b0.agg_time_end   end_time
                    from b0
                    where is_dinner
                  ) tmp
             order by tt_out_num, tmp.start_time ),
      b2 as (
        select
          row_number()
          over (
            partition by b1.tt_out_num ) row_num,
          *
        from b1
        order by tt_out_num, b1.start_time),
      b3 as (
        select
          b2.tt_out_num,
          (select tmp.start_time
           from b2 tmp
           where b2.tt_out_num = tmp.tt_out_num and tmp.row_num = 1) as out_start,
          (select tmp.start_time
           from b2 tmp
           where b2.tt_out_num = tmp.tt_out_num and tmp.row_num = 2) as first_dinner_start,
          (select tmp.end_time
           from b2 tmp
           where b2.tt_out_num = tmp.tt_out_num and tmp.row_num = 2) as first_dinner_end,
          (select tmp.start_time
           from b2 tmp
           where b2.tt_out_num = tmp.tt_out_num and tmp.row_num = 3) as second_dinner_start,
          (select tmp.end_time
           from b2 tmp
           where b2.tt_out_num = tmp.tt_out_num and tmp.row_num = 3) as second_dinner_end,
          (select tmp.end_time
           from b2 tmp
           where b2.tt_out_num = tmp.tt_out_num and tmp.row_num = 1) as out_end
        from b2
        group by
          b2.tt_out_num,
          out_start
    ),
      f1 as (
        select
          a3.tt_out_num,
          (case when (a3.first_dinner_start notnull)
            then (a3.first_dinner_start - a3.out_start)
           else (a3.out_end - a3.out_start)
           end
          )                                               first_interval_sh1,
          (a3.first_dinner_end - a3.first_dinner_start)   first_break_sh1,
          (case when (a3.second_dinner_start notnull)
            then (a3.second_dinner_start - a3.first_dinner_end)
            else null
           end
          )                                               second_interval_sh1,
          (a3.second_dinner_end - a3.second_dinner_start) second_break_sh1,
          (case when (a3.second_dinner_start notnull )
            then a3.out_end - a3.second_dinner_end
                      else (a3.out_end - a3.first_dinner_end)
            end
          )             last_interval_sh1,
          (case when (b3.first_dinner_start notnull)
            then (b3.first_dinner_start - b3.out_start)
           else (b3.out_end - b3.out_start)
           end
          )                                               first_interval_sh2,
          (b3.first_dinner_end - b3.first_dinner_start)   first_break_sh2,
          (case when (b3.second_dinner_start notnull)
            then (b3.second_dinner_start - b3.first_dinner_end)
           else null
           end
          )                                               second_interval_sh2,
          (b3.second_dinner_end - b3.second_dinner_start) second_break_sh2,
          (case when (b3.second_dinner_start notnull )
            then b3.out_end - b3.second_dinner_end
           else (b3.out_end - b3.first_dinner_end)
           end
          )             last_interval_sh2,
          (b3.out_end - b3.out_start)                     total_sh2
        from a3
          full join b3 on a3.tt_out_num = b3.tt_out_num
        order by a3.tt_out_num
    )
  select
    f1.tt_out_num :: text,
    (to_char(f1.first_interval_sh1, 'HH24:MI')) :: text                                    first_interval_sh1,
    (to_char(f1.first_break_sh1, 'HH24:MI')) :: text     as                                first_break_sh1,
    (to_char(f1.second_interval_sh1, 'HH24:MI')) :: text as                                second_interval_sh1,
    (to_char(f1.second_break_sh1, 'HH24:MI')) :: text    as                                second_break_sh1,
    (to_char(f1.last_interval_sh1, 'HH24:MI')) :: text   as                                last_interval_sh1,
    (to_char((coalesce(f1.first_interval_sh1, '0 minute' :: interval) +
              coalesce(f1.second_interval_sh1, '0 minute' :: interval) +
              coalesce(f1.last_interval_sh1, '0 minute' :: interval)), 'HH24:MI')) :: text total_sh1,
    (to_char(f1.first_interval_sh2, 'HH24:MI')) :: text                                    first_interval_sh2,
    (to_char(f1.first_break_sh2, 'HH24:MI')) :: text     as                                first_break_sh2,
    (to_char(f1.second_interval_sh2, 'HH24:MI')) :: text as                                second_interval_sh2,
    (to_char(f1.second_break_sh2, 'HH24:MI')) :: text    as                                second_break_sh2,
    (to_char(f1.last_interval_sh2, 'HH24:MI')) :: text   as                                last_interval_sh2,
    (to_char((coalesce(f1.first_interval_sh2, '0 minute' :: interval) +
              coalesce(f1.second_interval_sh2, '0 minute' :: interval) +
              coalesce(f1.last_interval_sh2, '0 minute' :: interval)), 'HH24:MI')) :: text total_sh2,
    (to_char(
        (coalesce(f1.first_interval_sh1, '0 minute' :: interval)) +
        (coalesce(f1.second_interval_sh1, '0 minute' :: interval)) +
        (coalesce(f1.last_interval_sh1, '0 minute' :: interval)) +
        (coalesce(f1.first_interval_sh2, '0 minute' :: interval)) +
        (coalesce(f1.second_interval_sh2, '0 minute' :: interval)) +
        (coalesce(f1.last_interval_sh2, '0 minute' :: interval)), 'HH24:MI')) :: text      total
  from f1;
end;
$$
language plpgsql;


