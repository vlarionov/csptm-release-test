CREATE OR REPLACE FUNCTION  ttb.jf_tt_out_pkg$attr_to_rowtype (
  p_attr text
)
  RETURNS ttb.tt_out AS
$body$
declare
  l_r 					ttb.tt_out%rowtype;

begin
  l_r.tt_out_id 		:= jofl.jofl_pkg$extract_number(p_attr, 'tt_out_id', true);
  l_r.tt_variant_id 	:= jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
  l_r.tr_capacity_id 	:= jofl.jofl_pkg$extract_number(p_attr, 'tr_capacity_id', true);
  l_r.mode_id 		:= jofl.jofl_pkg$extract_number(p_attr, 'mode_id', true);
  l_r.tt_out_num 	:= jofl.jofl_pkg$extract_number(p_attr, 'tt_out_num', true);
  l_r.tr_id 		:= jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
  
   if not ttb.jf_tt_variant_pkg$of_get_policy(l_r.tt_variant_id)
     then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if;

  return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-----------------

CREATE OR REPLACE FUNCTION ttb.jf_tt_out_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 ln_tt_variant_id ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
begin 

 open p_rows for
       select
         qq.tt_out_id
         --,qq.tt_out_num
         ,(select tt_out_num from ttb.tt_out where tt_out_id = qq.tt_out_id) as tt_out_num --для показа переключенных выходов
         --,qq.rn
         --,qq.time_begin
         --,qq.time_end
         ,qq.agg_time_begin as time_begin
         ,qq.agg_time_end as time_end
         ,extract( epoch from (qq.agg_time_begin - ttb.jf_incident_pkg$cn_plan_tt_date())) as tm_b
         ,extract( epoch from (qq.agg_time_end - ttb.jf_incident_pkg$cn_plan_tt_date()))   as tm_e
         ,qq.min_bn_int
         ,qq.max_bn_int
         --,qq.action_dur
         ,extract (epoch from (qq.agg_time_end - qq.agg_time_begin)) as action_dur
		 /*,case 
             	when not ttb.action_type_pkg$is_break(qq.action_type_id) 
                	 or qq.action_type_id = ttb.action_type_pkg$bn_round_break() 
                then extract (epoch from (qq.agg_time_end - qq.agg_time_begin)) 
                else case 
                       when extract (epoch from (qq.agg_time_end - qq.agg_time_begin)) > asd.get_constant('TIME_BREAK_IN_SHIFT') 
                       then 0 
                       else extract (epoch from (qq.agg_time_end - qq.agg_time_begin))
                     end
          end counted_dur*/
         ,extract (epoch from (qq.agg_time_end - qq.agg_time_begin)) as counted_dur
         ,qq.action_type_code
         ,qq.action_type_name
         ,qq.move_direction_id
         ,qq.move_direction_name
         ,qq.r_code
         ,qq.action_type_id
         ,qq.is_production_round
         ,qq.is_technical_round
         ,qq.is_bn_round_break
         ,qq.is_dinner
         ,qq.stop_b
         ,qq.stop_e
         ,qq.stop_id_b
         ,qq.stop_id_e
         ,qq.tt_action_id
         ,qq.tt_variant_id
         ,qq.tt_action_group_code
         ,qq.tt_action_group_nn
         ,qq.ref_group_kind_id
         ,qq.round_id
         ,qq.norm_id
         ,qq.stop_item_id
         ,ds.dr_shift_id
         ,ds.dr_shift_num
         ,(qq.tt_variant_id = ln_tt_variant_id) as is_mine
         ,case
          when action_type_id = ttb.action_type_pkg$park_to_stop() then 'P{A},D{OF_UPDATE, OF_CHANGE_STOP_BEGIN, OF_MAKE_ACTION, OF_MAKE_MAIN_ACTION}'
          when action_type_id = ttb.action_type_pkg$stop_to_park() then 'P{A},D{OF_UPDATE, OF_DELETE_ACTION}'
          when qq.is_technical_round then 'P{A},D{OF_UPDATE, OF_MAKE_ACTION, OF_MAKE_MAIN_ACTION}'
          when qq.is_round then 'P{A},D{OF_UPDATE, OF_DELETE_ACTION, OF_MAKE_ACTION, OF_MAKE_MAIN_ACTION}'
          when qq.is_break and action_type_id!=ttb.action_type_pkg$change_driver() then 'P{A},D{ OF_DELETE_ACTION, OF_MAKE_ACTION, OF_MAKE_MAIN_ACTION}'
          else '' end "ROW$POLICY"
       from ttb.tt_out_pkg$get_set4out_action_agg (ln_tt_variant_id) qq
         join ttb.dr_shift ds on qq.dr_shift_id = ds.dr_shift_id
       where 1 = 1
       order by qq.tt_out_id
       		   ,qq.agg_time_end
               ,qq.agg_time_begin;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

-------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_out_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
  RETURNS text AS
$body$
declare
  l_r ttb.tt_out%rowtype;
  l_tt_action_id ttb.tt_action.tt_action_id%type;
  l_time_begin timestamp;
begin
  l_r := ttb.jf_tt_out_pkg$attr_to_rowtype(p_attr);
  l_tt_action_id	:= jofl.jofl_pkg$extract_number(p_attr, 'tt_action_id', true);
  l_time_begin		:= jofl.jofl_pkg$extract_date(p_attr, 'time_begin', true);
  l_r.tt_variant_id	:= jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
  l_r.tt_out_id	  := jofl.jofl_pkg$extract_number(p_attr, 'tt_out_id', true);

  perform ttb.tt_out_pkg$make_tmp_tt(l_r.tt_out_id);
  select ttb.timetable_edit_pkg$update_out(l_tt_action_id, l_time_begin) into l_time_begin;
  perform ttb.tt_out_pkg$fill_db_table(l_r.tt_out_id);

  perform ttb.jf_ttv_check_condition_pkg$of_close_prev_check_cond(l_r.tt_variant_id, null::integer);		
  
  return null;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
COST 100;

-------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_out_pkg$of_delete_action (
  p_id_account numeric,
  p_attr text
)
  RETURNS text AS
$body$
declare
  l_tt_action_id ttb.tt_action.tt_action_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'tt_action_id', true);
  l_tt_variant_id ttb.tt_variant.tt_variant_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
  l_tt_out_id ttb.tt_variant.tt_variant_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'tt_out_id', true);
  l_dr_shift_id ttb.tt_variant.tt_variant_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'dr_shift_id', true);
  l_is_last boolean:= coalesce(jofl.jofl_pkg$extract_boolean(p_attr, 'f_is_last_INPLACE_K', true)::boolean, true);
  --l_norm_id ttb.norm.norm_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true);
  l_dt ttb.tt_action.dt_action%type;
  l_x   record;
  l_act record;
  l_del_tt_action_id ttb.tt_action.tt_action_id%type;
  l_move_tt_action_id ttb.tt_action.tt_action_id%type;
  l_r ttb.tt_out%rowtype;
begin

   if not ttb.jf_tt_variant_pkg$of_get_policy(l_tt_variant_id)
     then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if;

  perform ttb.tt_out_pkg$make_tmp_tt(l_tt_out_id);
    l_del_tt_action_id:=l_tt_action_id;


    --находим первое действие группы
    l_tt_action_id:=ttb.jf_tt_out_pkg$get_group_action(l_tt_action_id, 'MIN');

    select fprev_tt_action_id ,    fprev_action_type_id ,    fdt_action_begin ,    fdt_action_end  ,     faction_type_id  ,   fnext_tt_action_id
    into l_act
    from  ttb.timetable_edit_pkg$get_near_action(l_tt_action_id);

    l_tt_action_id:= l_act.fprev_tt_action_id;
    l_dt:=l_act.fdt_action_begin;

  --raise notice 'l_tt_action_id  (%), l_is_last( % ), l_dt ( % )',l_tt_action_id, l_is_last , l_dt;
  --вставить межрейс, если предыдущее действие рейс и удаляемое действие не пересменка

  for l_x in (
    select
      act.tt_action_group_id
      ,act.action_gr_id
      ,tar.tt_at_round_id
      ,it.tt_action_id
      ,bn_tat.tt_action_type_id as bn_tt_action_type_id
    from
      tt_action_item it
      join tt_action act on act.tt_action_id = it.tt_action_id
      join rts.stop_item2round sir on sir.stop_item2round_id = it.stop_item2round_id
      join ttb.tt_action_type tat on tat.tt_variant_id = l_tt_variant_id and tat.action_type_id = l_act.fprev_action_type_id
      join ttb.tt_at_round tar on tar.tt_action_type_id = tat.tt_action_type_id and tar.round_id =sir.round_id
      join ttb.tt_action_type bn_tat on bn_tat.tt_variant_id = l_tt_variant_id and bn_tat.action_type_id = ttb.action_type_pkg$bn_round_break()
      --join ttb.timetable_pkg$get_stop_interval_alignment(l_tt_variant_id) alig on alig.fstop_item2round_id = sir.stop_item2round_id
      join ttb.vm_action_type vma on vma.action_type_id = l_act.fprev_action_type_id and vma.is_round
    where it.tt_action_id = l_tt_action_id
          and l_act.faction_type_id != ttb.action_type_pkg$change_driver()
    order by sir.order_num desc
    limit 1
  ) loop
    raise notice 'Cоздание действия межрейсовая стоянка l_x( % )',l_x;
    --Cоздание действия межрейсовая стоянка
    l_tt_action_id:= ttb.timetable_edit_pkg$make_bn_break(
        p_tt_out_id           => l_tt_out_id,
        p_dr_shift_id         => l_dr_shift_id,
        p_tm_start            => l_act.fdt_action_begin,--l_dt,
        p_tt_action_group_id  => l_x.tt_action_group_id,
        p_action_gr_id        => l_x.action_gr_id,
        p_tt_action_type_id   => l_x.bn_tt_action_type_id,
        p_is_last             => true,
        p_link_tt_at_round_id => l_x.tt_at_round_id,
        p_tt_action_id        => l_x.tt_action_id
    );

    select fprev_tt_action_id ,    fprev_action_type_id ,    fdt_action_begin ,    fdt_action_end  ,     faction_type_id  ,   fnext_tt_action_id
    into l_act
    from  ttb.timetable_edit_pkg$get_near_action(l_tt_action_id);

    if l_is_last then
       l_dt:=coalesce(l_act.fdt_action_end, l_dt);
    end if;

  end loop;

    l_move_tt_action_id:=l_tt_action_id;

    --находим последнее действие груупы
    l_tt_action_id:=ttb.jf_tt_out_pkg$get_group_action(l_del_tt_action_id, 'MAX');

    select fprev_tt_action_id ,    fprev_action_type_id ,    fdt_action_begin ,    fdt_action_end  ,     faction_type_id  ,   fnext_tt_action_id
    into l_act
    from  ttb.timetable_edit_pkg$get_near_action(l_tt_action_id);

  raise notice 'l_tt_action_id = %, l_dt ( % )',l_tt_action_id,  l_dt;

  if l_is_last then
    l_move_tt_action_id:=l_act.fnext_tt_action_id;
  else
    select act.dt_action
    into  l_dt
    from tt_action act
    where act.tt_action_id = l_act.fnext_tt_action_id
    order by dt_action
    limit 1 ;

  end if;

  raise notice 'l_move_tt_action_id = %, l_dt ( % )',l_move_tt_action_id,  l_dt;



  raise notice 'удаляем l_tt_action_id = %, l_is_last (%), l_act ( % )',l_del_tt_action_id, l_is_last, l_act;

  delete from tt_action
  where (tt_out_id, tt_action_id) in(
    select act.tt_out_id, act.tt_action_id
    from tt_action act
      join tt_action s on s.tt_out_id = act.tt_out_id
                          and s.tt_action_group_id = act.tt_action_group_id
                          and s.action_gr_id = act.action_gr_id
    where s.tt_action_id =l_del_tt_action_id
    group by act.tt_out_id, act.tt_action_id
  );


  raise notice 'Cдвигаем  l_dt = %,  l_move_tt_action_id= % ', l_dt,  l_move_tt_action_id;
  --сдвигаем
  perform ttb.timetable_edit_pkg$update_out(p_tt_action_id     => l_move_tt_action_id,
                                               p_dt_action_new => l_dt,
                                               p_is_auto       => false,
                                               p_is_last       => l_is_last);

  --обратно в БД
  perform ttb.tt_out_pkg$fill_db_table(l_tt_out_id);
  delete from ttb.tt_action
    where tt_out_id = l_tt_out_id
      and not exists (select 1 from tt_action tact  where tact.tt_action_id = tt_action.tt_action_id);
  perform ttb.jf_ttv_check_condition_pkg$of_close_prev_check_cond(l_tt_variant_id, null::integer);

  return null;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
COST 100;



-------------------выпустить выход в ручном режиме
CREATE OR REPLACE FUNCTION ttb.jf_tt_out_pkg$of_make_out (
  p_id_account numeric,
  p_attr text
)
  RETURNS text AS
  $body$
declare
  l_r ttb.tt_out%rowtype;
  l_tt_variant_id numeric:= jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
  l_tm_start timestamp:= jofl.jofl_pkg$extract_date(p_attr, 'tm_start', true);
  l_stop_item_id numeric :=jofl.jofl_pkg$extract_number(p_attr, 'stop_item_id', true);
  l_tt_schedule_id numeric;
  l_cnt numeric;
begin

   if not ttb.jf_tt_variant_pkg$of_get_policy(l_tt_variant_id)
     then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if;

  l_tt_schedule_id:= ttb.tt_schedule_pkg$get_main_schedule_id (l_tt_variant_id);

  select count(1)
  into l_cnt
  from ttb.jf_tt_out_pkg$get_free_out(l_tt_variant_id, l_tt_schedule_id)
  ; --берем первый свободный выход

  if l_cnt = 0 then
    raise exception '<<Все доступные выходы выпущены>>';
  end if;

  raise notice '******tt_out_pkg$of_make_out ****  l_tt_variant_id = %, l_tt_schedule_id=%, l_tm_start=%' , l_tt_variant_id, l_tt_schedule_id, l_tm_start;
  l_tm_start:= coalesce(l_tm_start, ttb.jf_tt_out_pkg$of_set_tm_start(l_tt_variant_id, l_tt_schedule_id));

  raise notice '******tt_out_pkg$of_make_out ****  l_tt_variant_id = %, l_tt_schedule_id=%, l_tm_start=%' , l_tt_variant_id, l_tt_schedule_id, l_tm_start;
  
  perform ttb.jf_ttv_check_condition_pkg$of_close_prev_check_cond(l_tt_variant_id::int, null::int);

  return ttb.timetable_edit_pkg$make_out(p_tt_variant_id  => l_tt_variant_id,
                                            p_tt_schedule_id =>l_tt_schedule_id,
                                            p_tm_start     => l_tm_start::timestamp,
                                            p_stop_item_id =>  l_stop_item_id
                                             )::text;
  --RAISE  EXCEPTION '%',l_time_begin;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
COST 100;

-------------------выпустить выход в автоматизированном режиме
CREATE OR REPLACE FUNCTION ttb.jf_tt_out_pkg$of_create_out (
  p_id_account numeric,
  p_attr text
)
  RETURNS text AS
$body$
declare
  l_r ttb.tt_out%rowtype;
  l_tt_variant_id  ttb.tt_variant.tt_variant_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
  l_tt_schedule_id ttb.tt_schedule.tt_schedule_id%type;
  l_tt_tr_id       ttb.tt_tr.tt_tr_id%type;
begin

   if not ttb.jf_tt_variant_pkg$of_get_policy(l_tt_variant_id)
     then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if;
   l_tt_schedule_id:= ttb.tt_schedule_pkg$get_main_schedule_id (l_tt_variant_id);

  raise notice '******tt_out_pkg$of_create_out ****  l_tt_variant_id = %, l_tt_schedule_id=%, l_tt_tr_id=%' , l_tt_variant_id, l_tt_schedule_id, l_tt_tr_id;

  select ftt_tr_id
  into l_tt_tr_id
  from ttb.jf_tt_out_pkg$get_free_out(l_tt_variant_id, l_tt_schedule_id)
  order by  ftt_tr_order_num asc
  limit 1; --берем первый свободный выход

  if l_tt_tr_id is null then
    raise exception '<<Все доступные выходы выпущены>>';
  end if;



      perform ttb.timetable_test_pkg$make_schedule(p_tt_variant_id  => l_tt_variant_id,
                                                   p_tt_schedule_id => l_tt_schedule_id,
                                                   p_tt_tr_id       => l_tt_tr_id);


      perform ttb.jf_ttv_check_condition_pkg$of_close_prev_check_cond(l_tt_variant_id, null::integer);

  return null;


end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
COST 100;


-------------------

CREATE OR REPLACE FUNCTION ttb.jf_tt_out_pkg$of_set_tm_start (
   in p_tt_variant_id numeric,
   in p_tt_schedule_id numeric
)
  RETURNS timestamp AS
$body$
declare
   l_tm timestamp;
begin
  --возвращает время следующего выезда
  --не учитывает уличное расписание ВРЕМЕННО!!!
  with p as (
      select p.time_begin, p.interval_time, p.movement_type_id, ttr.tt_variant_id
      from ttb.tt_tr ttr
        join ttb.mode m on m.mode_id = ttr.tr_mode_id
        join ttb.tt_tr_period trp on trp.tt_tr_id = ttr.tt_tr_id
        join  ttb.tt_period p on p.tt_period_id = trp.tt_period_id

      where ttr.tt_variant_id = p_tt_variant_id
            and p.tt_schedule_id =p_tt_schedule_id
            and trp.tt_tr_order_num is not null
            and not exists (
          select  1
          from ttb.tt_out tout
            join ttb.tt_out2tt_tr totr on totr.tt_out_id = tout.tt_out_id
          where tout.tt_variant_id = ttr.tt_variant_id
                and totr.tt_tr_id = ttr.tt_tr_id
                and not tout.sign_deleted
      )
      order by  trp.tt_tr_order_num asc
      limit 1

  )
    ,adata as (
    select max(it.time_end)  + max(p.interval_time)*interval '1 second' as tm_start
    from ttb.tt_out tt_out
      join ttb.tt_action act on act.tt_out_id = tt_out.tt_out_id
      join ttb.tt_action_group g on g.tt_action_group_id = act.tt_action_group_id
      join ttb.tt_action_item it on it.tt_action_id = act.tt_action_id
      join p on 1=1
    where tt_out.tt_variant_id = p.tt_variant_id
          --and act.dt_action >= p.time_begin
          and g.ref_group_kind_id = ttb.ref_group_kind_pkg$first_action()
          and not tt_out.sign_deleted
    union all
      --из  ttb."timetable_pkg$set_tr" нужно потом как-то причесать
      select min(case atg.fix_arrival_type_id when ttb.fix_arrival_type_pkg$start_time() then atg.start_time end) as  tm_start
      from
        ttb.tt_schedule_item schi
        join ttb.tt_action_group agr on agr.tt_action_group_id = schi.tt_action_group_id
        join ttb.tt_action_type2group act2gr on act2gr.tt_action_group_id = agr.tt_action_group_id
        join ttb.tt_action_type tact on  tact.tt_action_type_id = act2gr.tt_action_type_id
        join ttb.tt_at_round ar on ar.tt_action_type_id = tact.tt_action_type_id
        join ttb.tt_at_round_stop ars on  ars.tt_at_round_id = ar.tt_at_round_id
        join ttb.atgrs_time atg on  atg.tt_at_round_stop_id= ars.tt_at_round_stop_id
      where atg.fix_arrival_type_id in( ttb.fix_arrival_type_pkg$start_time())
        and schi.tt_schedule_id = p_tt_schedule_id
      group by schi.tt_schedule_id
    )

  select max( tm_start)  as tm_start into l_tm
  from adata
  ;
  return l_tm;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
COST 100;


------------------- добавить  выхода группу действий
CREATE OR REPLACE FUNCTION ttb.jf_tt_out_pkg$of_make_action (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
  l_tt_schedule_item_id ttb.tt_schedule_item.tt_schedule_item_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'tt_schedule_item_id', true);
  l_tt_action_id ttb.tt_action.tt_action_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'tt_action_id', true);
  l_action_type_id ttb.tt_action.tt_action_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'action_type_id', true);
  l_tt_out_id ttb.tt_out.tt_out_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'tt_out_id', true);
  l_dr_shift_id ttb.dr_shift.dr_shift_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'dr_shift_id', true);
  --l_norm_id ttb.norm.norm_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true);
  l_ref_group_kind_id ttb.ref_group_kind.ref_group_kind_id%type;
  l_tt_schedule_id ttb.tt_schedule.tt_schedule_id%type;
  l_place text;
  l_right boolean;
  l_res numeric;
  l_rnd_action_type_id smallint;
  l_tm timestamp;
  l_cnt smallint;
  l_tt_variant_id ttb.tt_variant.tt_variant_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
begin
  raise notice 'l_tt_variant_id= %', l_tt_variant_id;
   if not ttb.jf_tt_variant_pkg$of_get_policy(l_tt_variant_id)
     then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if;

  raise notice '******jf_tt_out_pkg$of_make_action ****  l_tt_schedule_item_id = %' , l_tt_schedule_item_id ;

  select ag.ref_group_kind_id, schi.tt_schedule_id
  into l_ref_group_kind_id, l_tt_schedule_id
  from  ttb.tt_schedule_item schi
    join ttb.tt_action_group ag on ag.tt_action_group_id = schi.tt_action_group_id
    where schi.tt_schedule_item_id = l_tt_schedule_item_id ;

  --выпуск на линию происходит по make_out
  if l_ref_group_kind_id = ttb.ref_group_kind_pkg$first_action() then
    raise exception '<<Невозможно добавить выпуск на линию. ТС уже на линии!>>';
    return null;
  end if;

  if l_ref_group_kind_id = ttb.ref_group_kind_pkg$change_dr_action() then
      if  ((select max(dr_shift_num)
            from ttb.dr_shift dr
              join ttb.mode m on m.mode_id = dr.mode_id
              join ttb.tt_out tout on tout.mode_id = m.mode_id
            where tout.tt_out_id = l_tt_out_id
                  and not tout.sign_deleted) =   (select dr_shift_num from ttb.dr_shift dr where dr_shift_id  =l_dr_shift_id)
              ) then
                  raise exception '<<Невозможно добавить смену. Это последняя смена в этом режиме работы>>';
                  return null;
      elsif not ttb.action_type_pkg$is_break(l_action_type_id::smallint) then
                  raise exception '<<Пересменка может быть добавлена только при выборе межрейсовой стоянки или перерыва>>';
                  return null;
      end if;
  end if;

  --проверка, что действия добавляются только после или вместо межрейса
  if l_action_type_id != ttb.action_type_pkg$bn_round_break()
     and l_ref_group_kind_id in (ttb.ref_group_kind_pkg$dinner_action(), --обед,
                                 ttb.ref_group_kind_pkg$to_action(), --то
                                 ttb.ref_group_kind_pkg$pause_action() --отстой

                                 ) then
    raise exception '<<Перерывы  можно добавлять только при выборе межрейсовой стоянки>>';
    return null;
  end if;

  --select t.frnd_action_type_id into l_rnd_action_type_id
  --from ttb.jf_tt_out_pkg$get_next_action(l_tt_action_id) t where t.ftt_action_id = l_tt_action_id;

  --если обед начинается не с рейса, тогда заменяем межрейс, иначе пусть остается
  raise notice 'l_action_type_id =% l_ref_group_kind_id= %', l_action_type_id, l_ref_group_kind_id;

  with adata as
  (select * from ttb.timetable_pkg$get_ref_group_kind_data (l_tt_schedule_id, l_ref_group_kind_id)
   where ftt_schedule_item_id  = l_tt_schedule_item_id)
  select count(1) into l_cnt
  from adata
    join ttb.vm_action_type vma on vma.action_type_id = adata.faction_type_id and is_round
  where adata.forder_num = (select min(forder_num) from adata d );


  if (l_ref_group_kind_id = ttb.ref_group_kind_pkg$dinner_action() and  l_cnt = 0)
     or
      l_ref_group_kind_id in (ttb.ref_group_kind_pkg$to_action(), --то
                              ttb.ref_group_kind_pkg$pause_action() --отстой
                             ) then

     l_place:= 'REPLACE';
   elseif l_ref_group_kind_id = ttb.ref_group_kind_pkg$change_dr_action()  then
     if l_action_type_id = ttb.action_type_pkg$bn_round_break() then
        l_place:= 'PRIOR';
     else
       l_place:= 'FIX';
     end if;
   else
     l_place:= 'NEXT';
   end if;

  --perform ttb.norm_pkg$make_tmp_norm(l_norm_id);
  perform ttb.tt_out_pkg$make_tmp_tt(l_tt_out_id);

  l_res:=  ttb.jf_tt_out_pkg$add_action (p_id_account => p_id_account ,
                                        p_attr => p_attr,
                                        p_place => l_place
                                        );
  raise notice '--постобработка  l_res= %', l_res;
  --постобработка
  perform ttb.tt_out_pkg$fill_db_table(l_tt_out_id);

  if l_res > 0 then
      --завершающее действие
      raise notice '--l_ref_group_kind_id = %', l_ref_group_kind_id ;
      if l_ref_group_kind_id = ttb.ref_group_kind_pkg$last_action() then
        raise notice 'удаляем хвост' ;
        --!!! из временной удаляется, а из постоянной нет!!! разобраться
        delete
        from tt_action t
        using ttb.jf_tt_out_pkg$get_next_action(l_res) tail
        where tail.ftt_action_id  = t.tt_action_id
              and tail.ftt_action_id  != l_res;

/*        delete from tt_action where tt_action_id in ( select ftt_action_id from ttb.jf_tt_out_pkg$get_next_action(l_res)
                                                            where  ftt_action_id != l_res         group by ftt_action_id);*/
      end if;


    /*если пересменка, то
    #22430
    действие "Пересменка" должно добавиться ДО стоянки, а в случае перерыва - ПОСЛЕ перерыва.
    Все действия выхода после пересменки начинают относиться к новой смене;
    При добавлении смены между действиями первой смены, если существуют действия второй смены, то все действия после добавленной пересменки необходимо
     отнести ко второй смене.
    При добавлении пересменки в последнюю смену режима работы, выпускается новая смена, если она определена для режима. В противном случае действие запрещается.
    Пересменка добавляется ТОЛЬКО на межрейсовой стоянке и во времея перерыва (описание приведено в постановке).*/

    if l_ref_group_kind_id = ttb.ref_group_kind_pkg$change_dr_action() then
      return ttb.jf_tt_out_pkg$make_new_dr_shift(l_res);

    end if;


    /*если не замена межрейса и новое действие перерыв, то корректировочное время удаляем сдвигом*/
     if l_place= 'NEXT' and
        l_ref_group_kind_id in (ttb.ref_group_kind_pkg$to_action(), --то
                                ttb.ref_group_kind_pkg$pause_action(), --отстой
                                ttb.ref_group_kind_pkg$dinner_action()  --обед
                                ) then
       raise notice '--постобработка  если не замена межрейса и новое действие перерыв, то корректировочное время удаляем сдвигом l_res= %', l_res;
       l_tm:= ttb.timetable_edit_pkg$update_out(p_tt_action_id => l_res,
                                                p_dt_action_new => (
                                                                         select ftime_begin
                                                                         from ttb.timetable_edit_pkg$get_next_action(l_tt_action_id)
                                                                         where ftt_action_id = l_tt_action_id
                                                                               and fitem_action_type_id = ttb.action_type_pkg$bn_round_break_corr()
                                                                       ),
                                                p_is_last => true);
       raise notice '--постобработка l_tm = %', l_tm;

    end if;

  else
    raise exception '<<Действие не было добавлено. Проверьте совпадение КП>>';
  end if;

  perform ttb.tt_out_pkg$fill_db_table(l_tt_out_id);

  return l_res;

  end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;


/*  добавление действия с заменой или после текущего действия
p_place
  NEXT - после действия
  PRIOR - до действия
  REPLACE - вместо действия
*/
CREATE OR REPLACE FUNCTION ttb.jf_tt_out_pkg$add_action (
  p_id_account numeric,
  p_attr text,
  p_place text default 'NEXT'
)
  RETURNS numeric AS
$body$
declare
  l_tt_schedule_item_id ttb.tt_schedule_item.tt_schedule_item_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'tt_schedule_item_id', true);
  l_tt_out_id ttb.tt_out.tt_out_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'tt_out_id', true);
  l_dr_shift_id ttb.dr_shift.dr_shift_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'dr_shift_id', true);
  l_tt_action_id ttb.tt_action.tt_action_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'tt_action_id', true);
  l_tt_variant_id	ttb.tt_variant.tt_variant_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
  l_res  numeric;
  l_prev_action_id ttb.tt_action.tt_action_id%type;
  l_dt_action_b ttb.tt_action.dt_action%type;
  l_dt_e ttb.tt_action.dt_action%type;
  l_dt_action_e ttb.tt_action.dt_action%type;
  l_next_tt_action_id ttb.tt_action.tt_action_id%type;
  l_id ttb.tt_action.tt_action_id%type;
  l_tm timestamp;
  l_r ttb.tt_out%rowtype;
  l_cnt smallint;
begin

  l_r := ttb.jf_tt_out_pkg$attr_to_rowtype(p_attr);
  --raise EXCEPTION 'stop!';
  raise notice '******jf_tt_out_pkg$of_add_action **** l_tt_action_id=%, p_place= %,l_tt_out_id =% ,l_dr_shift_id numeric=%' ,
  l_tt_action_id, p_place    ,l_tt_out_id    ,l_dr_shift_id;

  if p_place = 'NEXT' then
    l_tt_action_id:= ttb.jf_tt_out_pkg$get_group_action(l_tt_action_id,'MAX');
    raise notice 'NEXT**** l_tt_action_id=%' ,l_tt_action_id;
  end if;

  select fprev_tt_action_id ,    fdt_action_begin  ,   fdt_action_end  ,     fnext_tt_action_id
  into   l_prev_action_id, l_dt_action_b ,l_dt_action_e, l_next_tt_action_id
  from ttb.timetable_edit_pkg$get_near_action(l_tt_action_id)
  ;

  if p_place = 'PRIOR' then
    l_id:= l_next_tt_action_id;
    l_dt_e:=l_dt_action_b ;
  elsif p_place = 'REPLACE' then
    l_id:= l_next_tt_action_id;
    l_dt_e:=l_dt_action_b ;
  elsif p_place = 'NEXT' then
    l_id:= l_next_tt_action_id;
    l_dt_e:=l_dt_action_e ;
    select count(1) into l_cnt
    from tt_action
    join ttb.vm_action_type vma on vma.action_type_id = tt_action.action_type_id and vma.is_all_pause
    where tt_action_id = l_id;
    if l_cnt>0 then
      raise exception '<<Действие не было добавлено. Необходимо предварительно удалить обед либо отстой для корректной обработки>>';
    end if;
  elsif p_place = 'FIX' then
    l_id:= l_next_tt_action_id;
    l_dt_e:=l_dt_action_e ;
  end if;


    --сдвигаем остальные действия
  raise notice '******предварительный сдвиг****  p_place=% ,l_id= %, l_dt_action_e =%, l_dt_e = %' , p_place, l_id, l_dt_action_e,   l_dt_e;
  perform ttb.timetable_edit_pkg$update_out(p_tt_action_id => l_id,
                                               p_dt_action_new => l_dt_e + interval '4 hour',
                                               p_is_auto => false,
                                               p_is_last => true);


  if p_place = 'PRIOR' then
    l_id:= l_prev_action_id;
  elsif p_place = 'REPLACE' then
    -- удаляем действие
    raise notice 'удаляем действие l_tt_action_id = % ', l_tt_action_id;
    delete from ttb.tt_action where tt_action_id = l_tt_action_id and not sign_deleted;
    --update ttb.tt_action set sign_deleted=true where tt_action_id = l_tt_action_id;
    delete from tt_action where tt_action_id = l_tt_action_id;
    l_id:=l_prev_action_id;
  elsif p_place = 'NEXT' then
    l_id:= l_tt_action_id;
  elsif p_place = 'FIX' then
    l_id:= l_tt_action_id;
  end if;

  raise notice '******добавляем действие ****  l_tt_out_id=% ,l_id= %, l_dt_action_e =%, l_dt_e = %' , l_tt_out_id, l_id, l_dt_action_e,   l_dt_e;

  l_res:= ttb.timetable_edit_pkg$make_schedule_item(
      p_tt_schedule_item_id => l_tt_schedule_item_id
      ,p_tt_out_id => l_tt_out_id
      ,p_dr_shift_id => l_dr_shift_id
      ,p_tm_start => l_dt_e + interval '1 minute'  /*все сдвинули, а вставляем куда нужно + 1 минута, чтобы не запутаться потом при сдвиге*/
      ,p_tt_action_id => l_id
  );


  if l_res>0 then
    perform ttb.jf_ttv_check_condition_pkg$of_close_prev_check_cond(l_tt_variant_id, null::integer);

    raise notice ' l_res= %' ,l_res;

    --находим время окончания нового действия
    if p_place = 'PRIOR' then
      --select max(it.time_end)  into l_dt_e from tt_action_item it where it.tt_action_id = l_res;

      raise notice '******вернуть для пересменки! ****  l_id= %, p_dt_action_new =%' , l_res,    l_dt_e;
      l_tm:= ttb.timetable_edit_pkg$update_out(p_tt_action_id => l_res,
                                               p_dt_action_new => l_dt_e,
                                               p_is_last => true);


      select ( t1.fdt_action_end - t1.fdt_action_begin) + (l_dt_action_e- l_dt_action_b)  + l_dt_e
      into   l_dt_e
      from ttb.timetable_edit_pkg$get_near_action(l_res) t1

      ;

      l_id:= l_next_tt_action_id;
    elsif p_place = 'REPLACE' then
      l_id:= ttb.jf_tt_out_pkg$get_group_action(l_res);  --первое действие группы
      l_dt_e:=l_dt_action_b;
    elsif p_place = 'NEXT' then
      l_id:= ttb.jf_tt_out_pkg$get_group_action(l_res,'MIN');  --первое действие группы
      --select max(it.time_end)  into l_dt_e from ttb.tt_action_item it where it.tt_action_id = l_id;
      l_dt_e:=l_dt_action_e;
      --l_id:= l_next_tt_action_id;
    elsif p_place = 'FIX' then
      l_id:= ttb.jf_tt_out_pkg$get_group_action(l_res);  --первое действие группы
      l_dt_e:=l_dt_action_e;
    end if;

    raise notice '******вернуть по месту сдвиг****  l_id= %, p_dt_action_new =%' , l_id,    l_dt_e;
       l_tm:= ttb.timetable_edit_pkg$update_out(p_tt_action_id => l_id,
                                                p_dt_action_new => l_dt_e,
                                                p_is_last => true);

    raise notice 'время  l_dt_e =%',  l_dt_e  ;

    if p_place = 'REPLACE'  then

      --возвращаем следующее действие
      return coalesce(l_next_tt_action_id, l_res);
    else
        --возвращаем добавленное действие
        return l_res;
    end if;
  else
    raise notice '******вернуть, ничего не было!';
    l_tm:= ttb.timetable_edit_pkg$update_out(p_tt_action_id => l_next_tt_action_id,
                                             p_dt_action_new => l_dt_action_e,
                                             p_is_last => true);
    return null;
  end if;


end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

/* по действию найти
in p_what text default 'MIN' - первое действие группы
                       'MAX' - последнее действие группы
*/
create or replace function ttb.jf_tt_out_pkg$get_group_action(in p_tt_action_id numeric,
                                                              in p_what text default 'MIN'  )
  returns numeric
as
$body$
declare
  l_res numeric;
begin
    select
      case p_what when 'MIN' then
           first_value(t.tt_action_id ) over (order by t.dt_action)
      else
           first_value(t.tt_action_id ) over (order by t.dt_action desc)
      end

     into l_res
     from tt_action t
     join tt_action act on (t.tt_out_id, t.action_gr_id, t.tt_action_group_id )=
                               (act.tt_out_id, act.action_gr_id, act.tt_action_group_id)

    where act.tt_action_id = p_tt_action_id
    limit 1;


  return l_res;
 end;
$body$
language plpgsql volatile
cost 100;


/*
 по действию найти предудущее и последующее
*/
create or replace function ttb.jf_tt_out_pkg$get_near_action(in p_tt_action_id numeric)
  returns TABLE(
    fprev_tt_action_id integer,
    fprev_action_type_id smallint,
    fdt_action_begin timestamp,
    fdt_action_end timestamp,
    faction_type_id smallint,
    fnext_tt_action_id integer
  )
as
  $body$
  declare
  begin
      return query
      with adata as (
          select lag(act.tt_action_id) over (order by act.dt_action, it.time_end) as prev_tt_action_id
             ,lag(act.action_type_id) over (order by act.dt_action, it.time_end) as prev_action_type_id
            , act.tt_action_id, act.action_type_id, act.dt_action,  act.action_range
          from ttb.tt_action act
            join ttb.tt_action_item it on it.tt_action_id = act.tt_action_id
          where tt_out_id = (select tt_out_id from ttb.tt_action a where a.tt_action_id = p_tt_action_id )
      )
      select prev_tt_action_id
        ,  prev_action_type_id
        ,  dt_action as dt_action_begin
        ,  coalesce(upper(action_range), dt_action) as dt_action_end
        ,  action_type_id
        , (select tt_action_id from adata d where  d.prev_tt_action_id = adata.tt_action_id and  d.prev_tt_action_id !=d.tt_action_id group by tt_action_id) next_tt_action_id
      from adata
      where
           tt_action_id =p_tt_action_id
       and prev_tt_action_id !=tt_action_id;
   end;
$body$
language plpgsql volatile
cost 100;


/*
 по действию найти
 p_what = 'PREV' предыдущую смену
 p_what = 'NEXT' следующую смену
 #22430
 при выборе любого действия не первой смены - все действия текущей смены начинают относиться к предыдущей смене
*/
create or replace function ttb.jf_tt_out_pkg$get_out_dr_shift(in p_tt_action_id numeric,
                                                              in p_what text )
  returns numeric
as
  $body$
  declare
    l_res numeric;
  begin

  with dr as (
      select
        lag(drs.dr_shift_id) over (order by dr_shift_num) prev_dr_shift_id,
        lead(drs.dr_shift_id) over (order by dr_shift_num) next_dr_shift_id,
        drs.dr_shift_id,
        tt_out.tt_out_id
      from ttb.tt_out tt_out
        join ttb.dr_shift drs on drs.mode_id  = tt_out.mode_id
      where tt_out.tt_out_id = (select tt_out_id from ttb.tt_action where tt_action_id =p_tt_action_id)
         and not tt_out.sign_deleted
  )
  select
    case p_what
    when 'NEXT' then min(dr.next_dr_shift_id)
    when 'PREV' then max(dr.prev_dr_shift_id)
    else null
    end
  into l_res
  from ttb.tt_action act
    join dr on dr.tt_out_id = act.tt_out_id
    join ttb.tt_action_item it on it.tt_action_id = act.tt_action_id and it.dr_shift_id = dr.dr_shift_id and not it.sign_deleted
  where act.tt_action_id = p_tt_action_id
    and not act.sign_deleted
    ;

  return l_res;


end;
$body$
language plpgsql volatile
cost 100;

-------------------
--#22430 при выборе любого действия не первой смены - все действия текущей смены начинают относиться к предыдущей смене
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_out_pkg$of_drop_shift (
  p_id_account numeric,
  p_attr text
)
  RETURNS text AS
$body$
declare
  l_tt_action_id ttb.tt_action.tt_action_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_action_id', true);
  l_tt_out_id ttb.tt_out.tt_out_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'tt_out_id', true);
  l_tt_variant_id ttb.tt_variant.tt_variant_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
  l_dr_shift_id ttb.dr_shift.dr_shift_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'dr_shift_id', true);
  l_prev_dr_shift_id numeric;
  l_rec record;
  l_res text;
begin

   if not ttb.jf_tt_variant_pkg$of_get_policy(l_tt_variant_id)
     then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if;   

  l_prev_dr_shift_id := ttb.jf_tt_out_pkg$get_out_dr_shift(l_tt_action_id, 'PREV');

  if l_prev_dr_shift_id is not null then
    perform ttb.jf_ttv_check_condition_pkg$of_close_prev_check_cond(l_tt_variant_id, null::integer);

    --найдем пересменку, если она есть
    select t.tt_action_id into l_tt_action_id
    from ttb.tt_action act
      join ttb.tt_action t on t.tt_out_id= act.tt_out_id and t.dt_action<= act.dt_action and t.action_type_id = ttb.action_type_pkg$change_driver()
    where act.tt_action_id = l_tt_action_id
    order by t.dt_action desc
    limit 1;

     if l_tt_action_id is not null then
      l_res:= ttb.jf_tt_out_pkg$of_delete_action(p_id_account, jsonb_set(p_attr::jsonb, '{"tt_action_id"}',l_tt_action_id::text::jsonb, false)::text);
    end if ;


     update ttb.tt_action_item  set dr_shift_id = l_prev_dr_shift_id where  dr_shift_id = l_dr_shift_id
     and exists (select 1 from ttb.tt_action act where act.tt_action_id = tt_action_item.tt_action_id and act.tt_out_id  = l_tt_out_id);
  end if;

  return null;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
COST 100;


/*Список действий после заданного действия*/
create or replace function ttb.jf_tt_out_pkg$get_next_action(in p_tt_action_id numeric)
  returns TABLE(
    ftt_action_id integer,
    ftt_action_item_id bigint,
    frnd_action_type_id smallint,
    faction_type_id smallint,
    fitem_action_type_id smallint,
    ftime_begin timestamp,
    ftime_end  timestamp,
    fdr_shift_id smallint
  )
as
  $body$
  declare
  begin
      return query
      select    act.tt_action_id, it.tt_action_item_id, rnd.action_type_id as rnd_action_type_id, act.action_type_id, it.action_type_id as item_action_type_id
                ,it.time_begin, it.time_end, it.dr_shift_id
      from  ttb.tt_action_item it
        join rts.stop_item2round sir on sir.stop_item2round_id = it.stop_item2round_id
        join rts.round rnd on rnd.round_id = sir.round_id
        join ttb.tt_action act on act.tt_action_id = it.tt_action_id
      where act.dt_action >=(
        select max(t.time_begin)
        from  ttb.tt_action_item t
        where t.tt_action_id = p_tt_action_id
           and not t.sign_deleted)
            and act.tt_out_id = (select tt_out_id from ttb.tt_action where tt_action_id =p_tt_action_id)
            and not act.sign_deleted
            ;
  end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
COST 100;


/*Проставить новыq номер смены
считаем, что  p_tt_action_id пересменка с правильным номером смены
*/
create or replace function ttb.jf_tt_out_pkg$make_new_dr_shift(in p_tt_action_id numeric)
  returns numeric
as
  $body$
  declare
    l_res  numeric;
  begin
    raise notice 'ttb.jf_tt_out_pkg$make_new_dr_shift(in p_tt_action_id  =%)',  p_tt_action_id;
    with adata as (
        select * from ttb.jf_tt_out_pkg$get_next_action(p_tt_action_id)
    )
       ,del as (
       delete from ttb.tt_action_item
       using adata
       where tt_action_id = ftt_action_id
        and  fdr_shift_id = dr_shift_id and faction_type_id =ttb.action_type_pkg$change_driver()
        and  tt_action_id !=p_tt_action_id
    )
      ,next_shift as (
        select adata.ftime_end
        from adata
          join adata d on d.ftt_action_id = p_tt_action_id
        where  adata.faction_type_id =  ttb.action_type_pkg$change_driver()
               and adata.ftime_end >= d.ftime_end
        order by 1
        limit 1
    )

    update ttb.tt_action_item
    set dr_shift_id = (select fdr_shift_id from adata where  ftt_action_id= p_tt_action_id limit 1 )

    from adata
      left join next_shift on 1=1

    where     tt_action_item.tt_action_item_id =   adata.ftt_action_item_id
              and (tt_action_item.time_begin  >= next_shift.ftime_end or next_shift.ftime_end  is null)
              and exists (select 1 from ttb.tt_action act where act.tt_action_id = tt_action_item.tt_action_id and act.dt_action >= (select ftime_begin from adata where  ftt_action_id= p_tt_action_id limit 1 ))
    ;

    return l_res;
  end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
COST 100;


------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- набор последних промежуточных значений
***************************************************************************/
create or replace function ttb."jf_tt_out_pkg$get_free_out" (in p_tt_variant_id numeric,
                                                             in p_tt_schedule_id numeric)
  returns TABLE(
    fout_num         smallint,
    ftr_mode_id      smallint,
    ftr_capacity_id  smallint,
    ftt_tr_id        integer,
    ftt_tr_order_num smallint,
    fdr_shift_id     smallint
    )
as
  $body$
  declare
  begin
    return query


        select
            (substring(mode_name,1,1)::int*100  + trp.tt_tr_order_num)::smallint as out_num,
            ttr.tr_mode_id::smallint,
            ttr.tr_capacity_id::smallint,
            ttr.tt_tr_id::int ,
            trp.tt_tr_order_num ::smallint,
          (select ds.dr_shift_id from ttb.dr_shift ds where ds.mode_id = m.mode_id order by dr_shift_num limit 1)::smallint as dr_shift_id
        from ttb.tt_tr ttr
        join ttb.mode m on m.mode_id = ttr.tr_mode_id
        join ttb.tt_tr_period trp on trp.tt_tr_id = ttr.tt_tr_id
        join ttb.tt_period p on p.tt_period_id = trp.tt_period_id
        join ttb.tt_schedule sch on sch.tt_schedule_id = p.tt_schedule_id
        where sch.tt_variant_id =p_tt_variant_id
          and sch.tt_schedule_id = p_tt_schedule_id
          and trp.tt_tr_order_num is not null
          and not exists (
                          select  1
                          from ttb.tt_out tout
                          join ttb.tt_out2tt_tr totr on totr.tt_out_id = tout.tt_out_id
                          where tout.tt_variant_id = ttr.tt_variant_id
                          and totr.tt_tr_id = ttr.tt_tr_id
                          and not tout.sign_deleted
                          );


  end;
$body$
language plpgsql volatile
cost 100;



CREATE OR REPLACE FUNCTION ttb.jf_tt_out_pkg$of_change_stop_begin (
  p_id_account numeric,
  p_attr text
)
  RETURNS text AS
  $body$
declare
  l_tt_action_id ttb.tt_action.tt_action_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_action_id', true);
  l_tt_out_id ttb.tt_action.tt_action_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_out_id', true);
  l_tm timestamp;
  l_tt_variant_id ttb.tt_variant.tt_variant_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
begin

   if not ttb.jf_tt_variant_pkg$of_get_policy(l_tt_variant_id)
     then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if;

  select max(dt_action + action_dur * interval '1 second')
  into l_tm
  from ttb.tt_action
  where tt_action_id in (
  select act.tt_action_id
  from ttb.tt_action act
  join ttb.tt_action s on s.tt_out_id = act.tt_out_id
                        and not s.sign_deleted
                        and s.tt_action_group_id = act.tt_action_group_id
                        and s.action_gr_id = act.action_gr_id
  where s.tt_action_id = l_tt_action_id);

  delete from ttb.tt_out where tt_out_id = l_tt_out_id;
  p_attr := json_build_object('tm_start',l_tm::text)::jsonb || p_attr::jsonb;

  raise notice '%',p_attr;
  return  ttb.jf_tt_out_pkg$of_make_out (p_id_account,  p_attr );

end;
$body$
language plpgsql volatile
cost 100;

--создать основное действие
CREATE OR REPLACE FUNCTION ttb.jf_tt_out_pkg$of_make_main_action (
  p_id_account numeric,
  p_attr text
)
  RETURNS text AS
$body$
declare
  l_tt_variant_id numeric:= jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
  l_stop_item_id numeric;
  l_tt_action_id ttb.tt_action.tt_action_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'tt_action_id', true);
  l_action_type_id ttb.tt_action.action_type_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'action_type_id', true);
  l_count_full_rnd smallint:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_count_full_rnd', true), 1);
  l_tt_schedule_id numeric;
  l_rec record;
  l_cnt record;
begin

   l_tt_schedule_id:= ttb.tt_schedule_pkg$get_main_schedule_id (l_tt_variant_id);

  raise notice 'of_make_main_action : l_tt_schedule_id ( % ), l_count_full_rnd ( % )',  l_tt_schedule_id, l_count_full_rnd;
  raise notice 'l_tt_action_id (%)',l_tt_action_id;


  select
      first_value(t.tt_action_id ) over (order by t.dt_action desc)
  into l_tt_action_id
  from ttb.tt_action t
    join ttb.tt_action act on (t.tt_out_id, t.action_gr_id, t.tt_action_group_id )=
                          (act.tt_out_id, act.action_gr_id, act.tt_action_group_id) and not act.sign_deleted

  where act.tt_action_id = l_tt_action_id
  limit 1;

  raise notice 'l_tt_action_id (%)',l_tt_action_id;

  select r.stop_item_id into l_stop_item_id
  from ttb.tt_action_item it
  join rts.stop_item2round r on  r.stop_item2round_id= it.stop_item2round_id
  where tt_action_id = l_tt_action_id
    and not it.sign_deleted
  order by it.time_end desc, it.time_begin desc
  limit 1;

  raise notice 'l_stop_item_id (%)',l_stop_item_id;
  --добавляем заданное пользователем количество основного действия последовательности
  for l_cnt in (select  generate_series (1, l_count_full_rnd)) loop

    for l_rec in ( select sid.tt_schedule_item_id
                      from ttb.v_tt_schedule_item_det sid
                      where sid.tt_schedule_id = l_tt_schedule_id
                            and sid.ref_group_kind_id = ttb.ref_group_kind_pkg$main_action() --основное действие
                            and sid.round_id is not null
                      order by not (sid.sp_b_id = l_stop_item_id ) )loop

             if p_attr::jsonb ? 'tt_schedule_item_id' then
                p_attr:=jsonb_set(p_attr::jsonb, '{"tt_schedule_item_id"}',l_rec.tt_schedule_item_id::text::jsonb, false)::text;
             else
               p_attr:=jsonb_insert(p_attr::jsonb, '{"tt_schedule_item_id"}',l_rec.tt_schedule_item_id::text::jsonb, false)::text;
             end if;

             p_attr:=jsonb_set(p_attr::jsonb, '{"tt_action_id"}',l_tt_action_id::text::jsonb, false)::text;

             raise notice 'p_attr ( % )',  p_attr;

             l_tt_action_id:= ttb.jf_tt_out_pkg$of_make_action(p_id_account, p_attr)::numeric;


       end loop;
  end loop;
  return  l_tt_action_id;


end;
$body$
language plpgsql volatile
cost 100;


-------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_out_pkg$of_delete_out (
  p_id_account numeric,
  p_attr text
)
  RETURNS text AS
$body$
declare
  l_r ttb.tt_out%rowtype;
  l_tt_action_id ttb.tt_action.tt_action_id%type;
  l_time_begin timestamp;
begin
  l_r := ttb.jf_tt_out_pkg$attr_to_rowtype(p_attr);

  --perform ttb.tt_out_pkg$make_tmp_tt(l_r.tt_out_id);
  update ttb.tt_cube
  set tt_tr_id = null
  from ttb.tt_out2tt_tr ttr
  where
    tt_cube.tt_variant_id = l_r.tt_variant_id
    and tt_cube.tt_tr_id = ttr.tt_tr_id
    and ttr.tt_out_id = l_r.tt_out_id;
  delete from  ttb.tt_cube where  tt_variant_id =  l_r.tt_variant_id
                              and tt_tr_id is null and (order_num_action  in (1001, 1002) or  action_type_id in (
                                                                                                ttb.action_type_pkg$park_to_stop(),
                                                                                                ttb.action_type_pkg$stop_to_park(),
                                                                                                ttb.action_type_pkg$bn_round_break_corr(),
                                                                                                ttb.action_type_pkg$dinner()
                                                                                                ));

  with del_out as (
    update ttb.tt_out tout set sign_deleted =true where tt_out_id = l_r.tt_out_id
    returning *)
    , del_parent as (
      update ttb.tt_out tout
      set sign_deleted =true
      where parent_tt_out_id = l_r.tt_out_id
      )
    , del_act as (
    update ttb.tt_action
    set sign_deleted =true
    from del_out
    where del_out.tt_out_id = tt_action.tt_out_id
          and not tt_action.sign_deleted
    returning *
  )
  update ttb.tt_action_item
  set sign_deleted =true
  from del_act
  where del_act.tt_action_id= tt_action_item.tt_action_id
        and not tt_action_item.sign_deleted;






--  perform ttb.tt_out_pkg$fill_db_table(l_r.tt_out_id);

  perform ttb.jf_ttv_check_condition_pkg$of_close_prev_check_cond(l_r.tt_variant_id, null::integer);

  return null;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
COST 100;

-------------------