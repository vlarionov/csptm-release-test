create or replace function ttb.jf_ref_numbers_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
  l_tr_type SMALLINT := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id', true);
begin
  OPEN p_rows FOR
  select route_num, current_route_variant_id as p_current_route_variant_id
  from rts.route where tr_type_id = l_tr_type
  AND current_route_variant_id IS NOT NULL order by route_num asc;
end;
$$;