CREATE OR REPLACE FUNCTION ttb.jf_ref_action_group_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.ref_action_group AS
$body$
declare 
   l_r ttb.ref_action_group%rowtype; 
begin 
   l_r.ref_action_group_id := jofl.jofl_pkg$extract_varchar(p_attr, 'ref_action_group_id', true); 
   l_r.gr_name := jofl.jofl_pkg$extract_varchar(p_attr, 'gr_name', true); 
   /*l_r.sys_period := jofl.jofl_pkg$extract_varchar(p_attr, 'sys_period', true); 
   l_r.sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'sign_deleted', true);*/

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ref_action_group_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
begin 
 open p_rows for 
      select 
        rag.ref_action_group_id, 
        rag.gr_name, 
        rag.sys_period, 
        rag.sign_deleted
      from ttb.ref_action_group rag; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ref_action_group_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.ref_action_group%rowtype;
begin 
   l_r := ttb.jf_ref_action_group_pkg$attr_to_rowtype(p_attr);

   update ttb.ref_action_group set 
          gr_name = l_r.gr_name/*, 
          sys_period = l_r.sys_period, 
          sign_deleted = l_r.sign_deleted*/
   where 
          ref_action_group_id = l_r.ref_action_group_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ref_action_group_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.ref_action_group%rowtype;
begin 
   l_r := ttb.jf_ref_action_group_pkg$attr_to_rowtype(p_attr);
   
   l_r.ref_action_group_id := nextval('ttb.ref_action_group_ref_action_group_id_seq');

   insert into ttb.ref_action_group (ref_action_group_id, gr_name)
   select l_r.ref_action_group_id, l_r.gr_name;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ref_action_group_pkg$is_main_traj (
  p_tt_action_group_name text
)
RETURNS boolean AS
$body$
declare 
begin 

  return (
      select rag.ref_group_subkind_id = 0
      from ttb.tt_action_group tag 
      join ttb.ref_action_group rag on rag.gr_name = tag.tt_action_group_name
      where tag.tt_action_group_name = p_tt_action_group_name
      limit 1);
  
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ref_action_group_pkg$is_not_main_traj (
  p_tt_action_group_name text
)
RETURNS boolean AS
$body$
declare 
begin 

  return (
      select rag.ref_group_subkind_id = 1
      from ttb.tt_action_group tag 
      join ttb.ref_action_group rag on rag.gr_name = tag.tt_action_group_name
      where tag.tt_action_group_name = p_tt_action_group_name
      limit 1);
  
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;