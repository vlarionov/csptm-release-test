CREATE OR REPLACE FUNCTION ttb."jf_norm_between_stop_filter_capacity_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare
  l_norm_id ttb.norm.norm_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true);
begin
  open p_rows for
  select DISTINCT on (nbs.tr_capacity_id) cp.short_name as tr_capacity_short_name, cp.tr_capacity_id
  from ttb.norm_between_stop nbs
  join core.tr_capacity cp on cp.tr_capacity_id = nbs.tr_capacity_id
  WHERE nbs.norm_id = l_norm_id;
end;
$function$;