CREATE OR REPLACE FUNCTION ttb.jf_stop_item4disp_task_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_f_move_direction_id smallint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_move_direction_id', true);
  l_f_route_id 			rts.route.route_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', true);
  l_f_tr_type_id		rts.stop_item.tr_type_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id', true);
begin
 open p_rows for
     select  si.stop_item_id,
             rt.route_num,
             rnd.code as round_code,
             rnd.round_id,
             rnd.move_direction_id,
             sir.order_num,
             s.name as stop_name,
             sir.stop_item2round_id,
             mv.move_direction_name,
			 coalesce(sl.building_address, ' ') || ' (' || coalesce(sl.building_distance, 0) || ' м.)' as stop_address
           from   rts.stop_item2round sir 
             join rts.stop_item si on si.stop_item_id = sir.stop_item_id
             join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
             join rts.stop s on s.stop_id = sl.stop_id
             join rts.round rnd on rnd.round_id = sir.round_id
             join rts.route rt on rt.current_route_variant_id = rnd.route_variant_id
             join rts.move_direction mv on mv.move_direction_id = rnd.move_direction_id
           where ( rt.route_id = l_f_route_id )
             and ( si.tr_type_id = l_f_tr_type_id )
             and ( rnd.move_direction_id = any (l_f_move_direction_id) )
           order by rt.route_id
                   ,rnd.round_id
           		   ,mv.move_direction_id
                   ,sir.order_num
                   ;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;