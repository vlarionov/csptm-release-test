CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_condition_pkg$of_ins_default_check (
  p_tt_variant_id numeric
)
RETURNS text AS
$body$
declare 
begin 
 insert into ttb.ttv_check_condition (
 							tt_variant_id,
 							tt_check_condition_id,
                            is_doable)
  select p_tt_variant_id
  		,tcc.tt_check_condition_id
	    ,tcc.is_doable        
  from ttb.tt_check_condition tcc 
  where not tcc.sign_deleted;
                             
  return null;                             
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;   
-------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_condition_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.ttv_check_condition%rowtype;
begin 
   l_r := ttb.jf_ttv_check_condition_pkg$attr_to_rowtype(p_attr);

   update ttb.ttv_check_condition set  
          is_active = l_r.is_active
   		 ,is_doable = l_r.is_doable
   where 
          ttv_check_condition_id = l_r.ttv_check_condition_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_condition_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 ln_tt_variant_id ttb.tt_variant.tt_variant_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
begin 
 open p_rows for 
   with q as (
      select 
      	tcc.check_num,
        tcc.check_name,        
        tvcc.ttv_check_condition_id, 
        tvcc.tt_variant_id, 
        tvcc.tt_check_condition_id, 
        tvcc.is_active, 
        tvcc.is_doable,
        case 
        	when tvcc.check_result = 0 then 'не выполнялась'
            when tvcc.check_result = 1 then 'пройдена'
            when tvcc.check_result = 2 then 'не пройдена'
            else null
        end check_result
      from ttb.ttv_check_condition tvcc
      join ttb.tt_check_condition tcc on tcc.tt_check_condition_id = tvcc.tt_check_condition_id
      where tvcc.tt_variant_id = ln_tt_variant_id
      order by tcc.check_num)
     select q.check_num,
        	q.check_name,        
        	q.ttv_check_condition_id, 
        	q.tt_variant_id, 
        	q.tt_check_condition_id, 
        	q.is_active, 
        	q.is_doable,
            '[' || json_build_object('CAPTION', q.check_result, 'JUMP',
                   json_build_object('METHOD', 'ttb.ttv_check_result', 'ATTR', '{"ttv_check_condition_id":"' || q.ttv_check_condition_id || '"}')) || ']' "JUMP_TO_CHECK_RESULT"
      from q      
      ; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_condition_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.ttv_check_condition AS
$body$
declare 
   l_r ttb.ttv_check_condition%rowtype; 
begin 
   l_r.ttv_check_condition_id := jofl.jofl_pkg$extract_number(p_attr, 'ttv_check_condition_id', true); 
   l_r.tt_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true); 
   l_r.tt_check_condition_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tt_check_condition_id', true); 
   l_r.is_active := jofl.jofl_pkg$extract_boolean(p_attr, 'is_active', true); 
   l_r.is_doable := jofl.jofl_pkg$extract_boolean(p_attr, 'is_doable', true); 
   l_r.sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'sign_deleted', true); 
   l_r.sys_period := jofl.jofl_pkg$extract_varchar(p_attr, 'sys_period', true); 
   
   if not ttb.jf_tt_variant_pkg$of_get_policy(l_r.tt_variant_id)
     then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if;       

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_condition_pkg$cn_not_performed()
RETURNS ttb.ttv_check_condition.check_result%type AS
$body$
declare 
begin 
    return 0;
end;
$body$
LANGUAGE 'plpgsql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-----------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_condition_pkg$cn_passed()
RETURNS ttb.ttv_check_condition.check_result%type AS
$body$
declare 
begin 
    return 1;
end;
$body$
LANGUAGE 'plpgsql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-----------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_condition_pkg$cn_not_passed()
RETURNS ttb.ttv_check_condition.check_result%type AS
$body$
declare 
begin 
    return 2;
end;
$body$
LANGUAGE 'plpgsql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_condition_pkg$of_start_ttv_check (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
 ln_tt_variant_id			ttb.ttv_check_condition.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
 check_cur					record;
 lt_msg						text;
 lt_ttv_name				text;
 ln_route_variant_id  		ttb.tt_variant.route_variant_id%type :=  jofl.jofl_pkg$extract_number(p_attr, 'route_variant_id', true);
 ln_cnt						smallint;
 l_r 						ttb.ttv_check_condition%rowtype;
begin 

 l_r := ttb.jf_ttv_check_condition_pkg$attr_to_rowtype(p_attr); 

 select tv.ttv_name
 into lt_ttv_name
 from ttb.tt_variant tv
 where tv.tt_variant_id = ln_tt_variant_id;
 
 lt_msg := '<i><b>Вариант расписания № ' || lt_ttv_name || '</b></i>';

 select count(1)
 into ln_cnt
 from ttb.tt_out o
 where o.tt_variant_id = ln_tt_variant_id;
 
 if ln_cnt < 1 then
 	return  jofl.jofl_util$cover_result('error', lt_msg || '<br>Для варианта расписания не сформированы выходы, проверка невозможна.');
 end if;

 for check_cur in (
 	select tvcc.ttv_check_condition_id
          ,tcc.check_num
    	  ,tcc.check_name
      from ttb.ttv_check_condition tvcc
      join ttb.tt_check_condition tcc on tcc.tt_check_condition_id = tvcc.tt_check_condition_id
      where tvcc.tt_variant_id = ln_tt_variant_id
        and tvcc.is_active
   order by tcc.check_num
 ) loop
     case 
        when check_cur.check_num = 1
          then perform ttb.jf_ttv_check_condition_pkg$of_check_pass_trans(ln_tt_variant_id,
          																check_cur.ttv_check_condition_id::integer,
                                                                        check_cur.check_num::smallint,	 
                                                                        check_cur.check_name);
          lt_msg := concat_ws('<br>', lt_msg, 'Проверка № '|| check_cur.check_num || ' <b> ' || check_cur.check_name || '</b> проведена.');
        when check_cur.check_num = 2
          then perform ttb.jf_ttv_check_condition_pkg$of_check_intervals( ln_tt_variant_id,
                                                                          check_cur.ttv_check_condition_id::integer,
                                                                          check_cur.check_num::smallint,	 
                                                                          check_cur.check_name);
          lt_msg := concat_ws('<br>', lt_msg, 'Проверка № '|| check_cur.check_num || ' <b> ' || check_cur.check_name || '</b> проведена.');
        when check_cur.check_num = 3
          then perform ttb.jf_ttv_check_condition_pkg$of_check_action_dates( ln_tt_variant_id,
          																	 check_cur.ttv_check_condition_id::integer,
                                                                             check_cur.check_num::smallint,	 
                                                                             check_cur.check_name,
                                                                             ln_route_variant_id );
		  lt_msg := concat_ws('<br>', lt_msg, 'Проверка № '|| check_cur.check_num || ' <b> ' || check_cur.check_name || '</b> проведена.');                                                                             
        when check_cur.check_num = 4
          then perform ttb.jf_ttv_check_condition_pkg$of_check_tr_pick_hours(ln_tt_variant_id,
          																	check_cur.ttv_check_condition_id::integer,
                                                                            check_cur.check_num::smallint,	 
                                                                            check_cur.check_name);
          lt_msg := concat_ws('<br>', lt_msg, 'Проверка № '|| check_cur.check_num || ' <b> ' || check_cur.check_name || '</b> проведена.'); 
        when check_cur.check_num = 5
          then perform ttb.jf_ttv_check_condition_pkg$of_check_norm(ln_tt_variant_id,
          															check_cur.ttv_check_condition_id::integer,
                                                                    check_cur.check_num::smallint,	 
                                                                    check_cur.check_name);
          lt_msg := concat_ws('<br>', lt_msg, 'Проверка № '|| check_cur.check_num || ' <b> ' || check_cur.check_name || '</b> проведена.');
        when check_cur.check_num = 6
          then perform ttb.jf_ttv_check_condition_pkg$of_check_stops_intervals( ln_tt_variant_id,
          																		check_cur.ttv_check_condition_id::integer,
                                                                                check_cur.check_num::smallint,	 
                                                                                check_cur.check_name); 
          lt_msg := concat_ws('<br>', lt_msg, 'Проверка № '|| check_cur.check_num || ' <b> ' || check_cur.check_name || '</b> проведена.');
        when check_cur.check_num = 7
          then perform ttb.jf_ttv_check_condition_pkg$of_check_doable(ln_tt_variant_id,
          															check_cur.ttv_check_condition_id::integer,
                                                                    check_cur.check_num::smallint,	 
                                                                    check_cur.check_name);
          lt_msg := concat_ws('<br>', lt_msg, 'Проверка № '|| check_cur.check_num || ' <b> ' || check_cur.check_name || '</b> проведена.');
        else 
          return  jofl.jofl_util$cover_result('error', 'Проверка № ' || check_cur.check_num::smallint || ' не реализована.');
     end case;  
  end loop;                                   
      
  return  jofl.jofl_util$cover_result('message', coalesce(lt_msg, 'no message'));
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_condition_pkg$of_close_prev_check_cond (
  p_tt_variant_id integer,
  p_ttv_check_condition_id integer
)
RETURNS text AS
$body$
declare 

begin
   if not p_ttv_check_condition_id is null then
       --Закроем результаты предыдущей проверки по этому условию
       update ttb.ttv_check_result
          set active_period =  tstzrange(lower(active_period), now())
        where tt_variant_id = p_tt_variant_id
          and ttv_check_condition_id = p_ttv_check_condition_id        
          and upper(active_period) is null
          ;    
   else
       --Закроем результаты предыдущей проверки по этому варианту и установим "не проводилась" (вызов после редактирования)
       update ttb.ttv_check_result
          set active_period =  tstzrange(lower(active_period), now())
        where tt_variant_id = p_tt_variant_id       
          and upper(active_period) is null
          ; 
          
       update ttb.ttv_check_condition 
          set check_result = 0
        where tt_variant_id = p_tt_variant_id;
   end if;          
      
  return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_condition_pkg$of_set_check_cond_result (
  p_ttv_check_condition_id integer
)
RETURNS text AS
$body$
declare 
 ln_cnt						smallint;
begin

   select count(1)
   	 into ln_cnt
   	 from ttb.ttv_check_result
    where ttv_check_condition_id = p_ttv_check_condition_id
      and upper(active_period) is null;
   
   update ttb.ttv_check_condition 
   	  set check_result = case 
        					when ln_cnt > 0
                            then ttb.jf_ttv_check_condition_pkg$cn_not_passed() 
                            else ttb.jf_ttv_check_condition_pkg$cn_passed()
      					 end     
    where ttv_check_condition_id = p_ttv_check_condition_id;                
      
  return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_condition_pkg$of_check_norm (
  p_tt_variant_id integer,
  p_ttv_check_condition_id integer,
  p_check_num smallint,
  p_check_name text
)
RETURNS text AS
$body$
declare 
begin
--Закроем результаты предыдущей проверки по этому условию
    perform ttb.jf_ttv_check_condition_pkg$of_close_prev_check_cond(p_tt_variant_id, p_ttv_check_condition_id);
        
--Продолжительность работы смен
  	perform ttb.jf_ttv_check_condition_pkg$of_check_norm_shift_work(p_tt_variant_id, p_ttv_check_condition_id,
    																p_check_num, p_check_name);
    
--Проверка перерывов
  	perform ttb.jf_ttv_check_condition_pkg$of_check_norm_breaks(p_tt_variant_id, p_ttv_check_condition_id,
    															p_check_num, p_check_name); 
     
--Установим результат проверки
   perform ttb.jf_ttv_check_condition_pkg$of_set_check_cond_result(p_ttv_check_condition_id);                 
      
   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-----------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_condition_pkg$of_check_norm_shift_work (
  p_tt_variant_id integer,
  p_ttv_check_condition_id integer,
  p_check_num smallint,
  p_check_name text
)
RETURNS text AS
$body$
declare 
begin
--Продолжительность работы смен
   insert into ttb.ttv_check_result (
   				tt_variant_id, 
                ttv_check_condition_id, 
                check_parms, 
                check_result)
    select 
      p_tt_variant_id,
      p_ttv_check_condition_id,
      json_build_object('check_num',
      					p_check_num,
      					'check_name',      
                        p_check_name),
      json_build_object('check_result_id',
      					ttb.jf_ttv_check_condition_pkg$cn_not_passed(),
      					'check_result_text',      
                        'Выход ' || t.tt_out_num || ', смена ' || t.ds_shift_num ||': время работы смены - ' ||
                        to_char(t.shift_work_dur * interval '1 second', 'HH24:MI') ||'. Норматив от ' || 
                        to_char(t.work_duration_min * interval '1 second', 'HH24:MI') || ' до ' || 
                        to_char(t.work_duration_max * interval '1 second', 'HH24:MI')|| '.')      
      from ttb.tt_check_pkg$get_set4mode (p_tt_variant_id)t
      where not t.is_check_break;                            
      
     return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-----------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_condition_pkg$of_check_norm_breaks (
  p_tt_variant_id integer,
  p_ttv_check_condition_id integer,
  p_check_num smallint,
  p_check_name text
)
RETURNS text AS
$body$
declare 
begin
--Проверка перерывов
 with check_break as (
 	   select t.tt_out_num
             ,t.action_dur
             ,t.shift_work_dur
             ,t.before_time
             ,t.after_time
             ,t.work_break_between
             ,t.is_check_break_num
             ,t.break_cnt             
             ,t.break_dur
             ,t.action_type_id
             ,t.action_type_name             
             ,t.is_check_break
             ,t.ds_shift_num             
             ,t.dr_shift_name             
             ,t.break_duration_max
             ,t.stop_time
             ,t.work_duration_min
             ,t.work_duration_max
             ,t.work_break_between_min
             ,t.work_break_between_max
             ,t.before_time_min
             ,t.before_time_max
             ,t.break_count_min
             ,t.break_count_max
             ,t.after_time_max
             ,t.before_time_short_break_max
             ,t.drm_action_type_id
			 ,t.drm_time_min
             ,t.drm_time_max 
      from ttb.tt_check_pkg$get_set4mode (p_tt_variant_id)t
      where t.is_check_break or (t.break_cnt > t.break_count_max or
                       		     t.break_cnt < t.break_count_min)
                                 )
  ,ins_break_cnt as (
            insert into ttb.ttv_check_result (
                        tt_variant_id, 
                        ttv_check_condition_id, 
                        check_parms, 
                        check_result)
            select 
              p_tt_variant_id,
              p_ttv_check_condition_id,
              json_build_object('check_num',
                                p_check_num,
                                'check_name',      
                                p_check_name),
              json_build_object('check_result_id',
                                ttb.jf_ttv_check_condition_pkg$cn_not_passed(),
                                'check_result_text',      
                                'Выход ' || i.tt_out_num || ', смена ' || i.ds_shift_num ||
                                ': количество перерывов смены - ' ||
                                i.break_cnt ||'. Норматив от ' || 
                                i.break_count_min || ' до ' || 
                                i.break_count_max|| '.')      
              from (select distinct  cb.tt_out_num
              						,cb.ds_shift_num
                                    ,cb.break_cnt
                                    ,cb.break_count_min
                                    ,cb.break_count_max
              		from check_break cb
                    where cb.break_cnt > cb.break_count_max 
                       or cb.break_cnt < cb.break_count_min ) i              
              returning * )
  ,ins_break_dur as (
            insert into ttb.ttv_check_result (
                        tt_variant_id, 
                        ttv_check_condition_id, 
                        check_parms, 
                        check_result)
            select 
              p_tt_variant_id,
              p_ttv_check_condition_id,
              json_build_object('check_num',
                                p_check_num,
                                'check_name',      
                                p_check_name),
              json_build_object('check_result_id',
                                ttb.jf_ttv_check_condition_pkg$cn_not_passed(),
                                'check_result_text',      
                                'Выход ' || i.tt_out_num || ', смена ' || i.ds_shift_num ||
                                ': продолжительность перерывов смены - ' ||
                                to_char(i.break_dur * interval '1 second', 'HH24:MI') ||'. Норматив до ' || 
                                to_char(i.break_duration_max * interval '1 second', 'HH24:MI')|| '.')      
              from (select distinct  cb.tt_out_num
              						,cb.ds_shift_num
                                    ,cb.break_dur
                                    ,cb.break_duration_max
              		from check_break cb
                   where cb.break_dur > cb.break_duration_max ) i              
              returning * )
  ,ins_break_before as (
            insert into ttb.ttv_check_result (
                        tt_variant_id, 
                        ttv_check_condition_id, 
                        check_parms, 
                        check_result)
            select 
              p_tt_variant_id,
              p_ttv_check_condition_id,
              json_build_object('check_num',
                                p_check_num,
                                'check_name',      
                                p_check_name),
              json_build_object('check_result_id',
                                ttb.jf_ttv_check_condition_pkg$cn_not_passed(),
                                'check_result_text',      
                                'Выход ' || cb.tt_out_num || ', смена ' || cb.ds_shift_num ||
                                ': время работы до первого перерыва - ' ||
 								to_char(cb.before_time * interval '1 second', 'HH24:MI') ||'. Норматив от ' || 
                        		to_char(cb.before_time_min * interval '1 second', 'HH24:MI') || ' до ' || 
                        		to_char(cb.before_time_max * interval '1 second', 'HH24:MI')|| '.')          
               from check_break cb
              where  cb.is_check_break_num = 1
                and (cb.before_time > cb.before_time_max /*or cb.before_time < cb.before_time_min*/)
              
              returning * )  
  ,ins_break_after as (
            insert into ttb.ttv_check_result (
                        tt_variant_id, 
                        ttv_check_condition_id, 
                        check_parms, 
                        check_result)
            select 
              p_tt_variant_id,
              p_ttv_check_condition_id,
              json_build_object('check_num',
                                p_check_num,
                                'check_name',      
                                p_check_name),
              json_build_object('check_result_id',
                                ttb.jf_ttv_check_condition_pkg$cn_not_passed(),
                                'check_result_text',      
                                'Выход ' || cb.tt_out_num || ', смена ' || cb.ds_shift_num ||
                                ': время работы после последнего перерыва до окончания смены - ' ||
 								to_char(cb.after_time * interval '1 second', 'HH24:MI') ||'. Норматив до ' || 
                        		to_char(cb.after_time_max * interval '1 second', 'HH24:MI')|| '.')          
               from check_break cb
              where cb.after_time > cb.after_time_max and cb.is_check_break_num = cb.break_cnt
              
              returning * )    
  ,ins_break_between as (
            insert into ttb.ttv_check_result (
                        tt_variant_id, 
                        ttv_check_condition_id, 
                        check_parms, 
                        check_result)
            select 
              p_tt_variant_id,
              p_ttv_check_condition_id,
              json_build_object('check_num',
                                p_check_num,
                                'check_name',      
                                p_check_name),
              json_build_object('check_result_id',
                                ttb.jf_ttv_check_condition_pkg$cn_not_passed(),
                                'check_result_text',      
                                'Выход ' || cb.tt_out_num || ', смена ' || cb.ds_shift_num ||
                                ': время работы между перерывами - ' ||
 								to_char(cb.work_break_between * interval '1 second', 'HH24:MI') ||'. Норматив от ' || 
                        		to_char(cb.work_break_between_min * interval '1 second', 'HH24:MI') || ' до ' || 
                        		to_char(cb.work_break_between_max * interval '1 second', 'HH24:MI')|| '.')           
               from check_break cb
              where cb.is_check_break_num > 1 
                and cb.work_break_between > 0
               and (cb.work_break_between > cb.work_break_between_max or cb.work_break_between < cb.work_break_between_min)
              
              returning * )   

    insert into ttb.ttv_check_result (
               tt_variant_id, 
               ttv_check_condition_id, 
               check_parms, 
               check_result)
    select 
     p_tt_variant_id,
     p_ttv_check_condition_id,
     json_build_object('check_num',
                       p_check_num,
                       'check_name',      
                       p_check_name),
     json_build_object('check_result_id',
                       ttb.jf_ttv_check_condition_pkg$cn_not_passed(),
                       'check_result_text',      
                       'Выход ' || cb.tt_out_num || ', смена ' || cb.ds_shift_num ||
                       ': длительность перерыва с типом ' || action_type_name || ' - ' ||
 						to_char(cb.action_dur * interval '1 second', 'HH24:MI') ||'. Норматив от ' || 
               			to_char(cb.drm_time_min * interval '1 second', 'HH24:MI') || ' до ' || 
               			to_char(cb.drm_time_max * interval '1 second', 'HH24:MI')|| '.')           
      from check_break cb
     where cb.action_type_id = cb.drm_action_type_id 
               and (cb.action_dur > cb.drm_time_max or cb.action_dur < cb.drm_time_min);             
                                     
     return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-----------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_condition_pkg$of_check_stops_intervals (
  p_tt_variant_id integer,
  p_ttv_check_condition_id integer,
  p_check_num smallint,
  p_check_name text
)
RETURNS text AS
$body$
declare 
  ln_cnt						smallint;
begin
--Закроем результаты предыдущей проверки по этому условию
    perform ttb.jf_ttv_check_condition_pkg$of_close_prev_check_cond(p_tt_variant_id, p_ttv_check_condition_id);
        
    with t_norm as ( 
         select  nbs.stop_item_1_id
                ,nbs.stop_item_2_id 
                ,nbs.between_stop_dur
                ,nbs.hour_from
                ,nbs.hour_to
                ,nbs.tr_type_id
                ,nbs.tr_capacity_id
          from ttb.tt_variant ttv
          join ttb.norm n on n.norm_id = ttv.norm_id 
          join ttb.norm_between_stop nbs on nbs.norm_id = n.norm_id
          where ttv.tt_variant_id = p_tt_variant_id
          )
       ,t_ttv_stops as (
          select distinct
                       out.tt_out_num
                      ,out.tr_capacity_id 
                      ,ds.dr_shift_num
                      ,ta.tt_action_id
                      ,at.action_type_id
                      ,si2r.order_num
                      ,s.name stop_name
                      ,lag(s.name, 1, s.name) over (partition by ta.tt_action_id order by tai.time_begin, si2r.order_num) prev_stop_name
                      ,tai.time_begin
                      ,lag(tai.time_begin, 1, tai.time_begin) over (partition by ta.tt_action_id order by tai.time_begin, si2r.order_num) prev_time_begin
                      ,tai.time_end
                      ,si2r.length_sector len
                      ,r.round_id
                      ,md.move_direction_name
                      ,s.stop_id
                      ,si.stop_item_id
                      ,lag(si.stop_item_id, 1, si.stop_item_id) over (partition by ta.tt_action_id order by tai.time_begin, si2r.order_num) prev_stop_item_id
                      ,extract( epoch from 
                       tai.time_begin -
                       lag(tai.time_end, 1, tai.time_end) over (partition by ta.tt_action_id order by tai.time_begin, si2r.order_num)) ttv_between_stop_dur
              from ttb.tt_out out 
              join ttb.tt_action ta on ta.tt_out_id = out.tt_out_id
              join ttb.action_type at on at.action_type_id = ta.action_type_id
              join ttb.tt_action_item tai on tai.tt_action_id = ta.tt_action_id
              join ttb.tt_action_round tar on tar.tt_action_id = ta.tt_action_id
              join rts.round r on r.round_id = tar.round_id
                               and r.action_type_id = at.action_type_id
              join rts.move_direction md on md.move_direction_id = r.move_direction_id
              join rts.stop_item2round si2r on si2r.round_id = r.round_id 
                                            and si2r.stop_item2round_id = tai.stop_item2round_id                                  
              join rts.stop_item2round_type si2rt on si2rt.stop_item2round_id = si2r.stop_item2round_id
              join rts.stop_item si on si.stop_item_id = si2r.stop_item_id
              join rts.stop_item_type sit on sit.stop_item_id = si.stop_item_id
                                         and sit.stop_item_type_id = si2rt.stop_item_type_id
                                         and sit.stop_type_id = 1 /*Пункт посадки-высадки*/
              join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
              join rts.stop s on s.stop_id = sl.stop_id
              join ttb.dr_shift ds on tai.dr_shift_id = ds.dr_shift_id
              where 1 = 1 
              and out.tt_variant_id = p_tt_variant_id      
			  and not out.sign_deleted 			  
              )
       ,t_discrepancy as (
          select tvs.tt_out_num
                ,tvs.dr_shift_num
                ,tvs.prev_stop_name
                ,tvs.stop_name
                ,tvs.ttv_between_stop_dur
                ,n.between_stop_dur
         from t_norm n 
         join t_ttv_stops tvs on  n.stop_item_2_id = tvs.stop_item_id
                              and n.stop_item_1_id = tvs.prev_stop_item_id
                              --and tvs.time_begin::time between n.hour_from and n.hour_to
                              and tvs.prev_time_begin::time between n.hour_from and n.hour_to - 1 * interval '1 sec'
                              and n.tr_capacity_id = tvs.tr_capacity_id
         where n.between_stop_dur != tvs.ttv_between_stop_dur
         )
         insert into ttb.ttv_check_result (
                   tt_variant_id, 
                   ttv_check_condition_id, 
                   check_parms, 
                   check_result)
          select 
           p_tt_variant_id,
           p_ttv_check_condition_id,
           json_build_object('check_num',
                             p_check_num,
                             'check_name',      
                             p_check_name),
           json_build_object('check_result_id',
                             ttb.jf_ttv_check_condition_pkg$cn_not_passed(),
                             'check_result_text',      
                             'Выход ' || d.tt_out_num || ', смена ' || d.dr_shift_num ||
                             ': ' || d.prev_stop_name || ' - ' || d.stop_name || '. Время прохождения - ' ||
                              to_char(d.ttv_between_stop_dur * interval '1 second', 'HH24:MI') ||', норматив - ' || 
                              to_char(d.between_stop_dur * interval '1 second', 'HH24:MI')|| '.')           
            from t_discrepancy d
            ; 
            
--Установим результат проверки
   perform ttb.jf_ttv_check_condition_pkg$of_set_check_cond_result(p_ttv_check_condition_id);                
      
   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_condition_pkg$of_check_pass_trans (
  p_tt_variant_id integer,
  p_ttv_check_condition_id integer,
  p_check_num smallint,
  p_check_name text
)
RETURNS text AS
$body$
declare 

begin
--Закроем результаты предыдущей проверки по этому условию
    perform ttb.jf_ttv_check_condition_pkg$of_close_prev_check_cond(p_tt_variant_id, p_ttv_check_condition_id);

--Тогда временно для первой проверки установи статус «пройдена», 
--Сергей Абрамов

--Установим результат проверки
    perform ttb.jf_ttv_check_condition_pkg$of_set_check_cond_result(p_ttv_check_condition_id);              
      
   return  null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_condition_pkg$of_check_intervals (
  p_tt_variant_id integer,
  p_ttv_check_condition_id integer,
  p_check_num smallint,
  p_check_name text
)
RETURNS text AS
$body$
declare 

begin
--Закроем результаты предыдущей проверки по этому условию
	perform ttb.jf_ttv_check_condition_pkg$of_close_prev_check_cond(p_tt_variant_id, p_ttv_check_condition_id);
    
   	insert into ttb.ttv_check_result (
   				tt_variant_id, 
                ttv_check_condition_id, 
                check_parms, 
                check_result)
    select 
      p_tt_variant_id,
      p_ttv_check_condition_id,
      json_build_object('check_num',
      					p_check_num,
      					'check_name',      
                        p_check_name),
      json_build_object('check_result_id',
      					ttb.jf_ttv_check_condition_pkg$cn_not_passed(),
      					'check_result_text',      
                        'Норматив ' || t.parm_interval || ', факт ' || t.avg_fact_interval ||'. Отклонение - ' ||
                        t.dev_prc_all ||' % ' )      
      from ttb.tt_check_pkg$get_set4intervals (p_tt_variant_id)t
      ;                                
        
--Установим результат проверки
    perform ttb.jf_ttv_check_condition_pkg$of_set_check_cond_result(p_ttv_check_condition_id);              
      
    return  null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_condition_pkg$of_check_action_dates (
  p_tt_variant_id integer,
  p_ttv_check_condition_id integer,
  p_check_num smallint,
  p_check_name text,
  p_route_variant_id integer
)
RETURNS text AS
$body$
declare 

begin
--Закроем результаты предыдущей проверки по этому условию
  perform ttb.jf_ttv_check_condition_pkg$of_close_prev_check_cond(p_tt_variant_id, p_ttv_check_condition_id);
     
  insert into ttb.ttv_check_result (
               tt_variant_id, 
               ttv_check_condition_id, 
               check_parms, 
               check_result)
        select p_tt_variant_id,
               p_ttv_check_condition_id,
               json_build_object('check_num',
                                 p_check_num,
                                 'check_name',      
                                 p_check_name),
               json_build_object('check_result_id',
                                 ttb.jf_ttv_check_condition_pkg$cn_not_passed(),
                                 'check_result_text',      
                                 'Номер варианта расписания: ' || r.ttv_name_other || 
                                 ', дата ' || to_char(r.dt_err, 'dd.mm.yyyy') || '.') 
          from ttb.tt_check_pkg$get_set4action_date (p_tt_variant_id)r;
                                        
--Установим результат проверки
   perform ttb.jf_ttv_check_condition_pkg$of_set_check_cond_result(p_ttv_check_condition_id);              
      
   return  null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_condition_pkg$of_check_tr_pick_hours (
  p_tt_variant_id integer,
  p_ttv_check_condition_id integer,
  p_check_num smallint,
  p_check_name text
)
RETURNS text AS
$body$
declare 

begin
--Закроем результаты предыдущей проверки по этому условию
   perform ttb.jf_ttv_check_condition_pkg$of_close_prev_check_cond(p_tt_variant_id, p_ttv_check_condition_id);
   
   	insert into ttb.ttv_check_result (
   				tt_variant_id, 
                ttv_check_condition_id, 
                check_parms, 
                check_result)
    select 
      p_tt_variant_id,
      p_ttv_check_condition_id,
      json_build_object('check_num',
      					p_check_num,
      					'check_name',      
                        p_check_name),
      json_build_object('check_result_id',
      					ttb.jf_ttv_check_condition_pkg$cn_not_passed(),
      					'check_result_text',      
                        'Часы пик с ' || to_char(lower(t.ph_per)::time, 'hh24:mm') || ' по ' || to_char(upper(t.ph_per)::time, 'hh24:mm') ||
                        '. План - '   || t.out_cnt_plan || ', факт - ' || t.out_cnt_fact ||'.' )      
      from ttb.tt_check_pkg$get_set4peakhours (p_tt_variant_id)t
      where t.out_cnt_fact < t.out_cnt_plan
      ;    
        
--Установим результат проверки
   perform ttb.jf_ttv_check_condition_pkg$of_set_check_cond_result(p_ttv_check_condition_id);              
      
   return  null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_condition_pkg$of_check_doable (
  p_tt_variant_id integer,
  p_ttv_check_condition_id integer,
  p_check_num smallint,
  p_check_name text
)
RETURNS text AS
$body$
declare 

begin
--Закроем результаты предыдущей проверки по этому условию
    perform ttb.jf_ttv_check_condition_pkg$of_close_prev_check_cond(p_tt_variant_id, p_ttv_check_condition_id);
    
--Тогда временно для первой проверки установи статус «пройдена», а для последней – «не пройдена».
--Сергей Абрамов
   insert into ttb.ttv_check_result (
   				tt_variant_id, 
                ttv_check_condition_id, 
                check_parms, 
                check_result)
    select 
      p_tt_variant_id,
      p_ttv_check_condition_id,
      json_build_object('check_num',
      					p_check_num,
      					'check_name',      
                        p_check_name),
      json_build_object('check_result_id',
      					ttb.jf_ttv_check_condition_pkg$cn_not_passed(),
      					'check_result_text',      
                        'Выход ' || t.tt_out_num || ', смена ' || t.dr_shift_num ||'. ' ||
                        t.prev_stop_name || ' - ' || t.stop_name || '. План - ' ||
                        to_char(btn_stop_dur, 'HH24:MI') ||', факт - ' || 
                        to_char(btn_stop_dur_plan, 'HH24:MI')|| '.')      
      from ttb.tt_check_pkg$get_set4doable (p_tt_variant_id)t
      ;   
        
--Установим результат проверки
    perform ttb.jf_ttv_check_condition_pkg$of_set_check_cond_result(p_ttv_check_condition_id);              
      
    return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------------------