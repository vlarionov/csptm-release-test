CREATE OR REPLACE FUNCTION ttb."jf_norm_round_filter_to_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare
  l_norm_id ttb.norm.norm_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true);
  l_time_from TEXT :=jofl.jofl_pkg$extract_varchar(p_attr, 'F_time_from', true)::TIME;
begin
  open p_rows for
  WITH t1 AS(
  select distinct on (nr.hour_to) CASE WHEN nr.hour_to = '00:00:00' THEN '24:00:00'::TIME ELSE nr.hour_to END time_to
  from ttb.norm_round nr
  WHERE norm_id = l_norm_id AND nr.hour_to > l_time_from::TIME OR hour_to = '00:00:00')
  select to_char(time_to, 'HH24:MI') time_to from t1 Order by time_to asc;
end;
$function$;