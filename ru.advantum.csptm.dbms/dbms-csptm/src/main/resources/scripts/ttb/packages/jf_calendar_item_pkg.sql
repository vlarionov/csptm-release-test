CREATE OR REPLACE FUNCTION ttb."jf_calendar_item_pkg$attr_to_rowtype"(p_attr text)
 RETURNS ttb.calendar_item
 LANGUAGE plpgsql
AS $function$
 
declare 
   l_r ttb.calendar_item%rowtype; 
begin 
   l_r.calendar_id := jofl.jofl_pkg$extract_number(p_attr, 'calendar_id', true); 
   l_r.calendar_tag_id := jofl.jofl_pkg$extract_varchar(p_attr, 'calendar_tag_id', true); 
   l_r.is_fixed := jofl.jofl_pkg$extract_boolean(p_attr, 'is_fixed', true); 
   l_r.calendar_item_id := jofl.jofl_pkg$extract_number(p_attr, 'calendar_item_id', true);

   return l_r;
end;
 
$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_calendar_item_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$

declare
  l_calendar_id bigint;
begin
  l_calendar_id := jofl.jofl_pkg$extract_number(p_attr, 'calendar_id', true);
 open p_rows for 
      select 
        ci.calendar_id,
        ci.calendar_tag_id,
        ci.is_fixed,
        ci.calendar_item_id,
        ct.tag_name,
        ctg.group_name as tag_group_name,
        case
        when not ci.is_fixed then 'P{A},D{OF_DELETE}'
          else 'P{RO},D{}'
        end as "ROW$POLICY"
      from ttb.calendar_item ci
        join ttb.calendar_tag ct on ct.calendar_tag_id = ci.calendar_tag_id
        left join ttb.calendar_tag_group ctg on ctg.calendar_tag_group_id = ct.calendar_tag_group_id
      where ci.calendar_id = l_calendar_id;
end;

$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_calendar_item_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
 
declare 
   l_r ttb.calendar_item%rowtype;
begin 
   l_r := ttb.jf_calendar_item_pkg$attr_to_rowtype(p_attr);

   update ttb.calendar_item set 
--           calendar_id = l_r.calendar_id,
--           calendar_tag_id = l_r.calendar_tag_id,
          is_fixed = l_r.is_fixed, 
          is_alone_group = l_r.is_alone_group
   where 
          calendar_item_id = l_r.calendar_item_id;

   return null;
end;
 
$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_calendar_item_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r 				ttb.calendar_item%rowtype;
   l_is_fixed 		boolean;
   ln_cal_tag_gr_id ttb.calendar_tag.calendar_tag_group_id%type;
begin 
   l_r := ttb.jf_calendar_item_pkg$attr_to_rowtype(p_attr);
   select ci.is_fixed 
   		 ,ct.calendar_tag_group_id
   into  l_is_fixed 
   	    ,ln_cal_tag_gr_id
   from ttb.calendar_item ci
   join ttb.calendar_tag ct on ct.calendar_tag_id = ci.calendar_tag_id
   where ci.calendar_item_id = l_r.calendar_item_id;

   if l_is_fixed and ln_cal_tag_gr_id <> ttb.calendar_pkg$get_daysofweek_tag_group_id () then
      RAISE EXCEPTION '<<Нельзя удалять этот признак!>>';
   else
      delete from  ttb.calendar_item where  calendar_item_id = l_r.calendar_item_id;
   end if;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_calendar_item_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$

declare 
   l_r ttb.calendar_item%rowtype;
begin 
   l_r := ttb.jf_calendar_item_pkg$attr_to_rowtype(p_attr);
   l_r.calendar_item_id := nextval( 'ttb.calendar_item_calendar_item_id_seq' );

  insert into ttb.calendar_item select l_r.*;

   return null;
end;

$function$
;