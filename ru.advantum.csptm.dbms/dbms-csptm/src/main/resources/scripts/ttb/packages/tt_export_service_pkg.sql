﻿
create or replace function ttb.tt_export_service_pkg$check_export_need()
returns integer
-- ========================================================================
-- Проверка на необходимость экспорта
-- ========================================================================
as $$ declare
    v_result      integer := 0;
    v_last_export timestamptz;
begin
    v_last_export := coalesce((select to_timestamp(value) from asd.constants where name = 'TTB_SERVICE_LAST_EXPORT'), '1900-01-01'::timestamptz);
    raise notice 'v_last_export = %', v_last_export;
    select 1 into v_result from ttb.tt_variant where lower(sys_period) > v_last_export limit 1;
    return coalesce(v_result, 0);
end;
$$ language plpgsql;


create or replace function ttb.tt_export_service_pkg$save_last_export_date()
returns integer
-- ========================================================================
-- Установить дату последнего экспорта
-- ========================================================================
as $$ declare
   v_result integer := 0;
begin
   insert into asd.constants(name, description, value, subsystem)
   values ('TTB_SERVICE_LAST_EXPORT', 'Дата последнего экспорта расписаний на сайт Мосгортранс', cast(extract(epoch from localtimestamp) as integer) - 30, 'TTB')
   on conflict (name)
   do update set value = excluded.value;
   return v_result;
end;
$$ language plpgsql;


create or replace function ttb.tt_export_service_pkg$convert_calendar_tag_to_bit
  (v_tag_name_short in text)
returns bit(7)
-- ========================================================================
-- Перевод тегов календаря в битовую маску
-- ========================================================================
as $$ declare
   v_result    bit(7);
begin
   select case
             when v_tag_name_short = 'Л' or v_tag_name_short = 'З' then B'1111111'
             when v_tag_name_short = 'Будни' then B'1111100'
             when v_tag_name_short = 'Выходные'then B'0000011'
             when v_tag_name_short = 'Пн' then B'1000000'
             when v_tag_name_short = 'Вт' then B'0100000'
             when v_tag_name_short = 'Ср' then B'0010000'
             when v_tag_name_short = 'Чт' then B'0001000'
             when v_tag_name_short = 'Пт' then B'0000100'
             when v_tag_name_short = 'Сб' then B'0000010'
             when v_tag_name_short = 'Вс' then B'0000001'
             else B'0000000'
          end
     into v_result;
   return v_result;
end;
$$ language plpgsql immutable;


create or replace function ttb.tt_export_service_pkg$convert_calendar_to_bit
  (v_tt_variant_id   in integer)
returns text
-- ========================================================================
-- Перевод календаря равианта расписания в битовую маску
-- ========================================================================
as $$ declare
   v_result    text;
begin
   select bit_and(week_mask)::text into v_result
     from (select bit_or(week_mask) as week_mask
             from (select ttb.tt_export_service_pkg$convert_calendar_tag_to_bit(tag_name_short) as week_mask
                     from ttb.tt_calendar ttc
                     join ttb.calendar_tag ctg on ctg.calendar_tag_id = ttc.calendar_tag_id
                    where ctg.calendar_tag_group_id in (1, 7)
                      and ttc.tt_variant_id = v_tt_variant_id
                      and not ttc.is_include
                  ) days_f
           union all
           select bit_and(week_mask) as week_mask
             from (select ttb.tt_export_service_pkg$convert_calendar_tag_to_bit(tag_name_short) as week_mask
                     from ttb.tt_calendar ttc
                     join ttb.calendar_tag ctg on ctg.calendar_tag_id = ttc.calendar_tag_id
                    where ctg.calendar_tag_group_id in (1, 7)
                      and ttc.tt_variant_id = v_tt_variant_id
                      and ttc.is_include
                  ) days_t
          ) q;

   return coalesce(v_result, '0000000');
end;
$$ language plpgsql immutable;


create or replace function ttb.tt_export_service_pkg$get_round_data
   (p_route_id   in integer)
returns table(route_id int4, route_num text, route_transport text, tt_variant_id int4,
              round_id int4, round_code text, move_direction_id int2, route_begin_date timestamp,
              route_end_date timestamp, route_direction text, route_days text, route_from text, route_to text)
-- ========================================================================
-- Данные маршрута
-- ========================================================================
as $$ declare
begin
   return query
   select rut.route_id, rut.route_num,
          case when rut.tr_type_id = 1 then 'avto'
               when rut.tr_type_id = 2 then 'trol'
               when rut.tr_type_id = 3 then 'tram'
               when rut.tr_type_id = 4 then 'tram'
          end as route_transport,
          rds.tt_variant_id,
          rds.round_id, rds.round_code,
          rds.move_direction_id,
          lower(rds.action_period) route_begin_date,
          upper(rds.action_period) route_end_date,
          case when rds.move_direction_id = 1 then 'AB' else 'BA' end as route_direction,
          ttb.tt_export_service_pkg$convert_calendar_to_bit(rds.tt_variant_id) as route_days,
          (select qtp.name as stop_name from rts.stop_item2round qsi
             join rts.stop_item qpi on qpi.stop_item_id = qsi.stop_item_id
             join rts.stop_location qsl on qsl.stop_location_id = qpi.stop_location_id
             join rts.stop qtp on qtp.stop_id = qsl.stop_id
            where not qsi.sign_deleted
              and qsi.round_id = rds.round_id
              and order_num = (select min(qir.order_num) from rts.stop_item2round qir where not qir.sign_deleted and qir.round_id = qsi.round_id)
          ) as route_from,
          (select qtp.name as stop_name
             from rts.stop_item2round qsi
             join rts.stop_item qpi on qpi.stop_item_id = qsi.stop_item_id
             join rts.stop_location qsl on qsl.stop_location_id = qpi.stop_location_id
             join rts.stop qtp on qtp.stop_id = qsl.stop_id
            where not qsi.sign_deleted
              and qsi.round_id = rds.round_id
              and order_num = (select max(qir.order_num) from rts.stop_item2round qir where not qir.sign_deleted and qir.round_id = qsi.round_id)
          ) as route_to
     from (select distinct rnd.round_id, rnd.code as round_code, rnd.move_direction_id, rnd.route_variant_id,
                  tto.tt_variant_id, ttv.action_period
             from ttb.tt_out tto
             join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id and tta.parent_tt_action_id is null
             join ttb.tt_action_item tti on tti.tt_action_id = tta.tt_action_id
             join rts.stop_item2round sir on sir.stop_item2round_id = tti.stop_item2round_id
             join rts.round rnd on rnd.round_id = sir.round_id
             join ttb.tt_variant ttv on ttv.tt_variant_id = tto.tt_variant_id
             join rts.route_variant rtv on rtv.route_variant_id = ttv.route_variant_id
            where not tto.sign_deleted
              and not tta.sign_deleted
              and not tti.sign_deleted
              and not sir.sign_deleted
              and rnd.code = '00'
              and ttb.action_type_pkg$is_production_round(rnd.action_type_id)
              and ttv.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id()
              and ttv.parent_tt_variant_id is null
              and ttv.action_period @> localtimestamp
              and (p_route_id is null or rtv.route_id = p_route_id)
          ) rds
     join rts.route_variant rtv on rtv.route_variant_id = rds.route_variant_id
     join rts.route rut on rut.route_id = rtv.route_id
    order by rds.round_code, rds.move_direction_id;
end;
$$ language plpgsql;


create or replace function ttb.tt_export_service_pkg$get_variant_data
   (p_tt_variant_id       in integer,
    p_move_direction_id   in smallint)
returns table(round_id int4, round_code text, move_direction_id int2, stop_from text, stop_to text)
-- ========================================================================
-- Дополнительные рейсы
-- ========================================================================
as $$ declare
begin
   return query
   select rds.round_id, rds.round_code, rds.move_direction_id,
          (select qtp.name as stop_name
             from rts.stop_item2round qsi
             join rts.stop_item qpi on qpi.stop_item_id = qsi.stop_item_id
             join rts.stop_location qsl on qsl.stop_location_id = qpi.stop_location_id
             join rts.stop qtp on qtp.stop_id = qsl.stop_id
            where not qsi.sign_deleted
              and qsi.round_id = rds.round_id
              and order_num = (select min(qir.order_num) from rts.stop_item2round qir where not qir.sign_deleted and qir.round_id = qsi.round_id)
          ) as stop_from,
          (select qtp.name as stop_name
             from rts.stop_item2round qsi
             join rts.stop_item qpi on qpi.stop_item_id = qsi.stop_item_id
             join rts.stop_location qsl on qsl.stop_location_id = qpi.stop_location_id
             join rts.stop qtp on qtp.stop_id = qsl.stop_id
            where not qsi.sign_deleted
              and qsi.round_id = rds.round_id
              and order_num = (select max(qir.order_num) from rts.stop_item2round qir where not qir.sign_deleted and qir.round_id = qsi.round_id)
          ) as stop_to
     from (select distinct rnd.round_id, rnd.code as round_code, rnd.move_direction_id
             from ttb.tt_out tto
             join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id and tta.parent_tt_action_id is null
             join ttb.tt_action_item tti on tti.tt_action_id = tta.tt_action_id
             join rts.stop_item2round sir on sir.stop_item2round_id = tti.stop_item2round_id
             join rts.round rnd on rnd.round_id = sir.round_id
            where not tto.sign_deleted
              and not tta.sign_deleted
              and not tti.sign_deleted
              and not sir.sign_deleted
              and rnd.code <> '00'
              and ttb.action_type_pkg$is_production_round(rnd.action_type_id)
              and tto.tt_variant_id = p_tt_variant_id
              and rnd.move_direction_id = p_move_direction_id
          ) rds
    order by rds.round_code;
end;
$$ language plpgsql;


create or replace function ttb.tt_export_service_pkg$get_station_data
   (p_tt_variant_id       in integer,
    p_move_direction_id   in smallint)
returns table(stop_id int4, order_num int2, hour_begin int, minute_begin text, stop_name text)
-- ========================================================================
-- Время по остановкам
-- ========================================================================
as $$ declare
begin
   return query
   with action_houres as (
      select q.tt_action_item_id, extract(hour from q.time_begin)::int hour_begin,
             case when extract(second from q.time_begin) > 30 then extract(minute from q.time_begin) + 1
                  else extract(minute from q.time_begin)
             end minute_begin
        from (select tti.tt_action_item_id, tti.time_begin
                from ttb.tt_out tto
                join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id and tta.parent_tt_action_id is null
                join ttb.tt_action_item tti on tti.tt_action_id = tta.tt_action_id
               where not tto.sign_deleted
                 and not tta.sign_deleted
                 and not tti.sign_deleted
                 and tto.tt_variant_id = p_tt_variant_id
                 and ttb.action_type_pkg$is_production_round(tta.action_type_id)
             ) q
   ),
   stop_times as (
      select stl.stop_id, act.hour_begin,
             array_to_string(array(select distinct unnest(array_agg(lpad(act.minute_begin::text, 2, '0') || case when rnd.code <> '00' then ' ' || rnd.code else '' end )) order by 1), ',') as minute_begin,
             min(sir.order_num) as order_num
        from action_houres act
        join ttb.tt_action_item tti on tti.tt_action_item_id = act.tt_action_item_id
        join rts.stop_item2round sir on sir.stop_item2round_id = tti.stop_item2round_id
        join rts.stop_item spi on spi.stop_item_id = sir.stop_item_id
        join rts.stop_location stl on stl.stop_location_id = spi.stop_location_id
        join rts.round rnd on rnd.round_id = sir.round_id
       where not tti.sign_deleted
         and not sir.sign_deleted
         and not stl.sign_deleted
         and ttb.action_type_pkg$is_production_round(rnd.action_type_id)
         and rnd.move_direction_id = p_move_direction_id
       group by stl.stop_id, rnd.move_direction_id, act.hour_begin
   )
   select stt.stop_id, stt.order_num, stt.hour_begin, stt.minute_begin, stp.name as stop_name
     from stop_times stt
     join rts.stop stp on stp.stop_id = stt.stop_id
    order by stt.order_num, stt.hour_begin;
end;
$$ language plpgsql;


create or replace function ttb.tt_export_service_pkg$get_route_first_stop_name
  (p_route_id   in integer)
returns text
-- ========================================================================
-- Наименование первой остановки маршрута
-- ========================================================================
as $$ declare
   v_result    text;
begin
   select stp.name into v_result
     from rts.stop_item2round sir
     join rts.stop_item sti on sti.stop_item_id = sir.stop_item_id
     join rts.stop_location stl on stl.stop_location_id = sti.stop_location_id
     join rts.stop stp on stp.stop_id = stl.stop_id
     join rts.round rnd on rnd.round_id = sir.round_id
     join rts.route_variant rtv on rtv.route_variant_id = rnd.route_variant_id
    where rtv.route_id = p_route_id
      and not sir.sign_deleted
      and not sti.sign_deleted
      and rnd.code = '00'
      and rnd.move_direction_id = 1
      and sir.order_num =
          (select min(qir.order_num) from rts.stop_item2round qir where qir.round_id = sir.round_id and not qir.sign_deleted);

   return v_result;
end;
$$ language plpgsql;


create or replace function ttb.tt_export_service_pkg$get_route_last_stop_name
  (p_route_id   in integer)
returns text
-- ========================================================================
-- Наименование последней остановки маршрута
-- ========================================================================
as $$ declare
   v_result    text;
begin
   select stp.name into v_result
     from rts.stop_item2round sir
     join rts.stop_item sti on sti.stop_item_id = sir.stop_item_id
     join rts.stop_location stl on stl.stop_location_id = sti.stop_location_id
     join rts.stop stp on stp.stop_id = stl.stop_id
     join rts.round rnd on rnd.round_id = sir.round_id
     join rts.route_variant rtv on rtv.route_variant_id = rnd.route_variant_id
    where rtv.route_id = p_route_id
      and not sir.sign_deleted
      and not sti.sign_deleted
      and rnd.code = '00'
      and rnd.move_direction_id = 1
      and sir.order_num =
          (select max(qir.order_num) from rts.stop_item2round qir where qir.round_id = sir.round_id and not qir.sign_deleted);

   return v_result;
end;
$$ language plpgsql;


create or replace function ttb.tt_export_service_pkg$get_round_last_stop_name
  (p_round_id   in integer)
returns text
-- ========================================================================
-- Наименование последней остановки рейса
-- ========================================================================
as $$ declare
   v_result    text;
begin
   select stp.name into v_result
     from rts.stop_item2round sir
     join rts.stop_item sti on sti.stop_item_id = sir.stop_item_id
     join rts.stop_location stl on stl.stop_location_id = sti.stop_location_id
     join rts.stop stp on stp.stop_id = stl.stop_id
    where sir.round_id = p_round_id
      and not sir.sign_deleted
      and not sti.sign_deleted
      and sir.order_num =
          (select max(qir.order_num) from rts.stop_item2round qir where qir.round_id = sir.round_id and not qir.sign_deleted);

   return v_result;
end;
$$ language plpgsql;


create or replace function ttb.tt_export_service_pkg$get_round_section_len_list
  (p_round_id           in integer,
   p_graph_section_id   in integer)
returns text
-- ========================================================================
-- Список расстояний до остановок на секции графа
-- ========================================================================
as $$ declare
   v_result    text;
begin
  select s.round_len_list into v_result
    from (select q.order_num, q.last_order_num,
                 string_agg(q.length_sector::text, ',') over (order by q.order_num) as round_len_list
            from (select sir.order_num, stl.stop_id,
                         sum(sir.length_sector) over (partition by sir.round_id order by sir.order_num) as length_sector,
                         first_value(sir.order_num) over (partition by sir.round_id order by sir.order_num desc) as last_order_num
                    from rts.stop_item2round sir
                    join rts.stop_item stp on stp.stop_item_id = sir.stop_item_id
                    join rts.stop_location stl on stl.stop_location_id = stp.stop_location_id
                   where not sir.sign_deleted
                     and not stp.sign_deleted
                     and sir.round_id = p_round_id
                     and stl.graph_section_id = p_graph_section_id
                 ) q
              ) s
        where s.order_num = s.last_order_num;
   
   return v_result;
end;
$$ language plpgsql;

create or replace function ttb.tt_export_service_pkg$get_round_section_stop_id_list
  (p_round_id           in integer,
   p_graph_section_id   in integer)
returns text
-- ========================================================================
-- Список id остановок на секции графа
-- ========================================================================
as $$ declare
   v_result    text;
begin
  select s.stop_id_list into v_result
    from (select q.order_num, q.last_order_num,
                 string_agg(q.stop_id::text, ',') over (order by q.order_num) as stop_id_list
            from (select sir.order_num, stl.stop_id,
                         sum(sir.length_sector) over (partition by sir.round_id order by sir.order_num) as length_sector,
                         first_value(sir.order_num) over (partition by sir.round_id order by sir.order_num desc) as last_order_num
                    from rts.stop_item2round sir
                    join rts.stop_item stp on stp.stop_item_id = sir.stop_item_id
                    join rts.stop_location stl on stl.stop_location_id = stp.stop_location_id
                   where not sir.sign_deleted
                     and not stp.sign_deleted
                     and sir.round_id = p_round_id
                     and stl.graph_section_id = p_graph_section_id
                 ) q
              ) s
        where s.order_num = s.last_order_num;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function ttb.tt_export_service_pkg$get_any_shift_and_driver_of_out
  (p_tt_out_id   in integer)
returns table(driver_id int4, dr_shift_id int2, dr_shift_num int2)
-- ========================================================================
-- Водитель и смена выхода (любая)
-- ========================================================================
as $$ declare
begin
   return query
   select tta.driver_id, tta.dr_shift_id, sht.dr_shift_num
     from ttb.tt_action_item tta
     join ttb.dr_shift sht on sht.dr_shift_id = tta.dr_shift_id
    where tt_action_id = (select tt_action_id from ttb.tt_action where tt_out_id = p_tt_out_id limit 1)
    limit 1;
end;
$$ language plpgsql;


create or replace function ttb.tt_export_service_pkg$get_first_shift_and_driver_of_out
  (p_tt_out_id   in integer)
returns table(driver_id int4, dr_shift_id int2, dr_shift_num int2)
-- ========================================================================
-- Водитель и смена начала выхода
-- ========================================================================
as $$ declare
begin
   return query
   with first_action as (
      select tt_action_id from ttb.tt_action 
       where not sign_deleted
         and tt_out_id = p_tt_out_id
         and dt_action = (select min(dt_action) from ttb.tt_action where not sign_deleted and tt_out_id = p_tt_out_id)
      limit 1
   )
   select tti.driver_id, tti.dr_shift_id, sht.dr_shift_num
     from ttb.tt_action_item tti
     join first_action fst on fst.tt_action_id = tti.tt_action_id
     join ttb.dr_shift sht on sht.dr_shift_id = tti.dr_shift_id
    where not tti.sign_deleted
      and tti.time_begin = (select min(qti.time_begin) 
                              from ttb.tt_action_item qti
                              join first_action qst on qst.tt_action_id = qti.tt_action_id
                             where not qti.sign_deleted)
    limit 1;
end;
$$ language plpgsql;


create or replace function ttb.tt_export_service_pkg$get_shift_and_driver_of_out
  (p_tt_action_id   in integer,
   p_action_date    in timestamp)
returns table(driver_id int4, dr_shift_id int2, dr_shift_num int2)
-- ========================================================================
-- Водитель и смена для action
-- ========================================================================
as $$ declare
begin
   return query
   select tti.driver_id, tti.dr_shift_id, sht.dr_shift_num
     from ttb.tt_action_item tti
     join ttb.dr_shift sht on sht.dr_shift_id = tti.dr_shift_id
    where not tti.sign_deleted
      and tti.tt_action_id = p_tt_action_id
      and tti.time_begin = (select max(qti.time_begin) 
                              from ttb.tt_action_item qti
                             where not qti.sign_deleted
                               and qti.tt_action_id = p_tt_action_id
                               and qti.time_begin <= p_action_date)
    limit 1;
end;
$$ language plpgsql;


create or replace function ttb.tt_export_service_pkg$get_shift_and_driver_of_out
  (p_tt_action_id   in integer)
returns integer
-- ========================================================================
-- Порядковый номер рейса
-- ========================================================================
as $$ declare
   v_result   integer;
begin
   select q.round_num into v_result
     from (select tta.tt_action_id,
                  row_number() over (order by tta.dt_action) round_num
             from ttb.tt_action tta
             join ttb.tt_action_round ttr on ttr.tt_action_id = tta.tt_action_id
            where tta.tt_out_id = (select tt_out_id from ttb.tt_action where tt_action_id = p_tt_action_id)
              and not tta.sign_deleted
          ) q
    where q.tt_action_id = p_tt_action_id;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function ttb.tt_export_service_pkg$get_shift_and_driver_of_tr
  (p_tr_id         in bigint,
   p_action_time   in timestamp)
returns table(trip_date timestamp, trip_id int4, grafic int2, shift_num int2, route_num int4)
-- ========================================================================
-- Водитель и смена для TC
-- ========================================================================
as $$ declare
begin
   return query
   select tta.dt_action  as trip_date,
          ttr.round_id   as trip_id,
          tto.tt_out_num as grafic,
          (ttb.tt_export_service_pkg$get_shift_and_driver_of_out(tta.tt_action_id, now()::timestamp)).dr_shift_num as shift_num,
          ttb.tt_export_service_pkg$get_shift_and_driver_of_out(tta.tt_action_id) as route_num
     from ttb.tt_out tto
     join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
     left join ttb.tt_action_round ttr on ttr.tt_action_id = tta.tt_action_id
    where not tto.sign_deleted
      and not tta.sign_deleted
      and tto.tr_id = p_tr_id
      and tta.action_range @> p_action_time
    limit 1;
end;
$$ language plpgsql;
