CREATE OR REPLACE FUNCTION ttb."jf_norm_stop_pkg$attr_to_rowtype"(p_attr text)
 RETURNS ttb.norm_stop
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.norm_stop%rowtype; 
begin 
   l_r.norm_stop_id := jofl.jofl_pkg$extract_number(p_attr, 'norm_stop_id', true); 
   l_r.norm_id := jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true); 
   l_r.hour_from := jofl.jofl_pkg$extract_varchar(p_attr, 'hour_from', true); 
   l_r.hour_to := jofl.jofl_pkg$extract_varchar(p_attr, 'hour_to', true); 
   l_r.stop_dur := jofl.jofl_pkg$extract_number(p_attr, 'stop_dur', true); 
   l_r.tr_capacity_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tr_capacity_id', true); 
   l_r.stop_item_id := jofl.jofl_pkg$extract_number(p_attr, 'stop_item_id', true); 
   l_r.tr_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tr_type_id', true); 

   return l_r;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_norm_stop_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$ 
declare
  l_norm_id ttb.norm.norm_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true);
begin 
 open p_rows for
 select
   ns.norm_stop_id,
   ns.norm_id,
   ns.hour_from::text as hour_from,
   ns.hour_to::text as hour_to,
   ns.stop_dur,
   ns.tr_capacity_id,
   ns.stop_item_id,
   ns.tr_type_id,
   tt.name as tr_type_name,
   cp.short_name as tr_capacity_short_name,
   s.name as stop_name,
   net.norm_edit_type_name
 from ttb.norm_stop ns
   join ttb.norm_edit_type net on net.norm_edit_type_id = ns.norm_edit_type_id
   join core.tr_type tt on tt.tr_type_id = ns.tr_type_id
   join core.tr_capacity cp on cp.tr_capacity_id = ns.tr_capacity_id
   join rts.stop_item si on si.stop_item_id = ns.stop_item_id
   join rts.stop s on s.stop_id = si.stop_id
 where ns.norm_id = l_norm_id;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_norm_stop_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.norm_stop%rowtype;
begin 
   l_r := ttb.jf_norm_stop_pkg$attr_to_rowtype(p_attr);

   update ttb.norm_stop set 
          norm_id = l_r.norm_id, 
          hour_from = l_r.hour_from, 
          hour_to = l_r.hour_to, 
          stop_dur = l_r.stop_dur, 
          tr_capacity_id = l_r.tr_capacity_id, 
          stop_item_id = l_r.stop_item_id, 
          tr_type_id = l_r.tr_type_id,
          norm_edit_type_id = ttb.norm_edit_type$hand()
   where 
          norm_stop_id = l_r.norm_stop_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_norm_stop_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.norm_stop%rowtype;
begin 
   l_r := ttb.jf_norm_stop_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.norm_stop where  norm_stop_id = l_r.norm_stop_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_norm_stop_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.norm_stop%rowtype;
begin 
   l_r := ttb.jf_norm_stop_pkg$attr_to_rowtype(p_attr);
   l_r.norm_stop_id := nextval('ttb.norm_stop.norm_stop_id_seq');
   l_r.norm_edit_type_id := ttb.norm_edit_type$hand();

   insert into ttb.norm_stop select l_r.*;

   return null;
end;
 $function$
;