create or replace function  ttb.tt_create_status_pkg$new() returns smallint language sql as $$select 0::smallint$$;
create or replace function  ttb.tt_create_status_pkg$queued() returns smallint language sql as $$select 1::smallint$$;
create or replace function  ttb.tt_create_status_pkg$in_progress() returns smallint language sql as $$select 2::smallint$$;
create or replace function  ttb.tt_create_status_pkg$ready() returns smallint language sql as $$select 3::smallint$$;
create or replace function  ttb.tt_create_status_pkg$error() returns smallint language sql as $$select 4::smallint$$;
create or replace function  ttb.tt_create_status_pkg$smoothing() returns smallint language sql as $$select 5::smallint$$;


create or replace function ttb.tt_create_status_pkg$calc_tt_variant()
  returns void
as $$
declare
  l_calc_task record;
  l_ct_status_id ttb.tt_create_status.tt_create_status_id%type;
  l_rec record;
  p_attr text;
  lt_res_check text;
begin
  select * into l_calc_task
  from ttb.tt_variant v
  where v.tt_create_status_id = ttb.tt_create_status_pkg$queued()
  order by v.tt_variant_id
  limit 1;

  if l_calc_task is null then
    return;
  end if;

  update ttb.tt_variant
  set tt_create_status_id = ttb.tt_create_status_pkg$in_progress()
  where tt_variant_id = l_calc_task.tt_variant_id;

  begin
    perform ttb.timetable_test_pkg$clean_tt(l_calc_task.tt_variant_id);
    perform ttb.timetable_test_pkg$make_tt_variant(l_calc_task.tt_variant_id);

    l_ct_status_id := ttb.tt_create_status_pkg$ready();
    
    update ttb.tt_variant
    set tt_create_status_id = l_ct_status_id
    where tt_variant_id = l_calc_task.tt_variant_id
    returning * into l_rec;



    --проверки
    p_attr:='{"tt_variant_id":"'|| l_rec.tt_variant_id || '","route_variant_id":"'|| l_rec.route_variant_id ||'"}';
    lt_res_check := ttb.jf_ttv_check_condition_pkg$of_start_ttv_check(2, p_attr);


    exception
    when others then
      raise notice '% % %', l_calc_task, SQLERRM, SQLSTATE;
      l_ct_status_id := ttb.tt_create_status_pkg$error();
	  
	  update ttb.tt_variant
      set tt_create_status_id = l_ct_status_id
      where tt_variant_id = l_calc_task.tt_variant_id;
	  
  end;






end;$$
language plpgsql;

