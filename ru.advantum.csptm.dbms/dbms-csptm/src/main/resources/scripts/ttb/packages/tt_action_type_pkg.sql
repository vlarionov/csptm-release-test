CREATE OR REPLACE FUNCTION ttb.tt_action_type_pkg$get_depo2terr (p_tt_action_type_id numeric)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tt_action_type%rowtype; 
begin 
   l_r.tt_action_type_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_action_type_id', true); 
   l_r.tt_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true); 
   l_r.action_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'action_type_id', true); 
   l_r.plan_time := jofl.jofl_pkg$extract_number(p_attr, 'plan_time', true); 
   
   if not ttb.jf_tt_variant_pkg$of_get_policy(l_r.tt_variant_id)
    then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if;       

   return l_r;
end;
 $function$
;
