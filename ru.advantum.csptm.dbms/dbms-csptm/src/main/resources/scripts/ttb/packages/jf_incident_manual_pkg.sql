CREATE OR REPLACE  FUNCTION ttb.jf_incident_manual_pkg$attr_to_rowtype (p_attr text) RETURNS ttb.incident
	LANGUAGE plpgsql
AS $$

declare
   l_r ttb.incident%rowtype;
begin
   l_r.incident_id := jofl.jofl_pkg$extract_number(p_attr, 'incident_id', true);
   l_r.incident_initiator_id := jofl.jofl_pkg$extract_varchar(p_attr, 'incident_initiator_id', true);
   l_r.incident_status_id := jofl.jofl_pkg$extract_varchar(p_attr, 'incident_status_id', true);
   l_r.incident_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'incident_type_id', true);
   l_r.time_start := jofl.jofl_pkg$extract_date(p_attr, 'time_start', true);
   l_r.time_finish := jofl.jofl_pkg$extract_date(p_attr, 'time_finish', true);
   l_r.tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
   l_r.unit_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_id', true);
   l_r.account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', true);
   l_r.parent_incident_id := jofl.jofl_pkg$extract_number(p_attr, 'parent_incident_id', true);
   l_r.incident_params := jofl.jofl_pkg$extract_varchar(p_attr, 'incident_params', true);

   return l_r;
end;

$$;


CREATE OR REPLACE FUNCTION ttb.jf_incident_manual_pkg$of_delete (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r ttb.incident%rowtype;
begin
   l_r := ttb.jf_incident_manual_pkg$attr_to_rowtype(p_attr);

   return null;
end;

$$;


CREATE OR REPLACE FUNCTION ttb.jf_incident_manual_pkg$of_insert (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r ttb.incident%rowtype;
   l_incident_reason_id int;
begin
    l_r := ttb.jf_incident_manual_pkg$attr_to_rowtype(p_attr);
    l_incident_reason_id := jofl.jofl_pkg$extract_number(p_attr, 'incident_reason_id', false);

    return ttb.jf_incident_pkg$send_incident(
                        p_incidents_array :=
                                ARRAY[ttb.jf_incident_pkg$new_incident(
                                        p_timeStart := l_r.time_start
                                        ,p_incident_initiator_id := ttb.jf_incident_pkg$cn_initiator_user()
                                        ,p_incident_status_id := ttb.jf_incident_pkg$cn_status_new()
                                        ,p_incident_type_id := ttb.jf_incident_pkg$cn_timetable_violate()
                                        ,p_parent_incident_id := null
                                        ,p_account_id := p_id_account::int
                                        ,p_incident_payload :=
                                        ttb.jf_incident_pkg$new_payload (
                                                p_trList := array[l_r.tr_id]::int[]
                                                ,p_incidentReason := l_incident_reason_id
                                                )
                            )]
                );
end;
$$;


CREATE OR REPLACE FUNCTION ttb.jf_incident_manual_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$

declare
  l_begin_dt     timestamp without time zone         := coalesce(jofl.jofl_pkg$extract_date(p_attr,    'F_BEGIN_DT', true), now()::date::timestamp);
  l_end_dt       timestamp without time zone         := coalesce (jofl.jofl_pkg$extract_date(p_attr,    'F_END_DT',   true), (now() + interval '1 day')::date::timestamp);
begin
 open p_rows for
      select
        incident_id,
        incident_initiator_id,
        incident_status_id,
        incident_type_id,
        time_start,
        time_finish,
        incident.tr_id,
        unit_id,
        account_id,
        parent_incident_id,
        tr.garage_num as tr_name,
        incident_params::text,
        (incident_params ->> 'incidentReason')::integer as incident_reason_id,
        ttb.jf_incident_reason_pkg$get_name((incident_params ->> 'incidentReason')::integer) as incident_reason
      from ttb.incident left join core.tr on incident.tr_id = tr.tr_id
      where incident_type_id = ttb.jf_incident_pkg$cn_timetable_violate()
            and time_start between l_begin_dt and l_end_dt;
end;
$$;


CREATE OR REPLACE FUNCTION ttb.jf_incident_manual_pkg$of_update (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r ttb.incident%rowtype;
begin
   l_r := ttb.jf_incident_manual_pkg$attr_to_rowtype(p_attr);

   return null;
end;
$$;


