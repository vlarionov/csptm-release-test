CREATE OR REPLACE FUNCTION ttb.jf_action_type_production_rounds_pkg$of_rows
  (p_id_account IN  NUMERIC,
   p_attr       IN  TEXT,
   p_rows       OUT REFCURSOR)
  RETURNS REFCURSOR AS $$
-- ========================================================================
-- Типы производственных рейсов
-- ========================================================================
DECLARE
BEGIN
  OPEN p_rows FOR
  WITH RECURSIVE round_type AS (
    SELECT
      action_type_id,
      action_type_name,
      action_type_code,
      parent_type_id,
      action_type_desc
    FROM ttb.action_type
    WHERE parent_type_id = ttb.action_type_pkg$production_round()
    UNION
    SELECT
      e.action_type_id,
      e.action_type_name,
      e.action_type_code,
      e.parent_type_id,
      e.action_type_desc
    FROM ttb.action_type e
      INNER JOIN round_type s ON s.action_type_id = e.parent_type_id
  )
  SELECT
    rt.action_type_id,
    rt.action_type_name,
    rt.action_type_code,
    rt.parent_type_id,
    rt.action_type_desc,
    coalesce(rt.action_type_desc, concat_ws(' ', prt.action_type_name, rt.action_type_name)) AS action_type_description
  FROM ttb.action_type rt
    JOIN ttb.action_type prt ON prt.action_type_id = rt.parent_type_id
  WHERE rt.parent_type_id = ttb.action_type_pkg$production_round()
  ORDER BY rt.action_type_id;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION ttb.jf_action_type_production_rounds_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS $$
DECLARE
  l_r ttb.action_type%ROWTYPE;
BEGIN
  l_r := ttb.jf_action_type_production_rounds_pkg$attr_to_rowtype(p_attr);
  INSERT INTO ttb.action_type ( action_type_id , action_type_name, action_type_code, parent_type_id, action_type_desc) VALUES
(    nextval('ttb.action_type_action_type_id_seq'),
     l_r.action_type_name,
     l_r.action_type_code,
     ttb.action_type_pkg$production_round(),
     l_r.action_type_desc )
    ;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION ttb.jf_action_type_production_rounds_pkg$of_update(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
AS $$
DECLARE
  l_r ttb.action_type%ROWTYPE;
BEGIN
  l_r := ttb.jf_action_type_production_rounds_pkg$attr_to_rowtype(p_attr);

  UPDATE ttb.action_type
  SET
    action_type_name = l_r.action_type_name,
    action_type_code = l_r.action_type_code,
    action_type_desc= l_r.action_type_desc
  WHERE
    ttb.action_type.action_type_id = l_r.action_type_id;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION ttb.jf_action_type_production_rounds_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_action_type_id ttb.action_type.action_type_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'action_type_id', FALSE);
BEGIN
  DELETE FROM ttb.action_type
  WHERE action_type_id= l_action_type_id;
  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION ttb.jf_action_type_production_rounds_pkg$attr_to_rowtype(p_attr TEXT)
  RETURNS ttb.ACTION_TYPE
LANGUAGE plpgsql
AS $$
DECLARE
  l_r ttb.action_type%ROWTYPE;
BEGIN
  l_r.action_type_id :=  jofl.jofl_pkg$extract_number(p_attr, 'action_type_id', TRUE);
  l_r.action_type_name:= jofl.jofl_pkg$extract_varchar(p_attr, 'action_type_name', TRUE);
  l_r.action_type_code:= jofl.jofl_pkg$extract_varchar(p_attr, 'action_type_code', TRUE);
  l_r.parent_type_id := jofl.jofl_pkg$extract_number(p_attr, 'parent_type_id', TRUE);
  l_r.action_type_desc:= jofl.jofl_pkg$extract_varchar(p_attr, 'action_type_desc', TRUE);
  RETURN l_r;
END;
$$;