﻿-- фиксированное время
create or replace function ttb.fix_arrival_type_pkg$fix_time()
  returns smallint as 'select 1:: smallint' language sql immutable;
-- первый рейс
create or replace function ttb.fix_arrival_type_pkg$start_time()
  returns smallint as 'select 2:: smallint' language sql immutable;
-- последний рейс
create or replace function ttb.fix_arrival_type_pkg$finish_time()
  returns smallint as 'select 3:: smallint' language sql immutable;
