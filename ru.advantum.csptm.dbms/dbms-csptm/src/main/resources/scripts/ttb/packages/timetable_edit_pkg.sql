﻿/* синхронизировать tt_action и tt_action_item*/
create or replace function ttb.timetable_edit_pkg$update_tt_action(in p_tt_action_id numeric,
                                                                   in p_is_last boolean default true)
  returns timestamp
as
$body$
declare
  l_res timestamp;
begin

  with  upd as (
        update tt_action
        set dt_action = i.tm_b
          ,action_dur = coalesce(extract(EPOCH from i.tm_e - i.tm_b), 0)
          ,action_range =  tsrange( i.tm_b,
                                    i.tm_e, '[)' )
        from
          (select  max(it.time_end) tm_e , min (it.time_begin) tm_b
           from tt_action_item it
           where it.tt_action_id  = p_tt_action_id) i
        where tt_action.tt_action_id = p_tt_action_id
        returning  *
      )
  select case when p_is_last then  coalesce (upper(upd.action_range), upd.dt_action) else upd.dt_action end  into l_res
  from upd;

  return  l_res;



end;
$body$
language plpgsql volatile
cost 100;

------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Обновить времена рейса
p_dt_action_new - новое время
p_is_last       - направление (true - вперед, иначе в обратную сторону от окончания действия)
***************************************************************************/

create or replace function ttb."timetable_edit_pkg$update_round"(in p_tt_action_id numeric,
                                                                 in p_dt_action_new timestamp,
                                                                 in p_is_last boolean default true)
  returns timestamp
as
$body$
declare
  l_rec record;
  l_res timestamp;
begin
  RAISE NOTICE 'timetable_edit_pkg$update_round-------1 %', clock_timestamp();
  for l_rec in (
    with adata as (
        select tout.tr_capacity_id, tac.tt_action_id,tac.dt_action , tac.action_type_id,  coalesce(v.parent_tt_variant_id, v.tt_variant_id) as tt_variant_id
        from tt_action  tac
          join ttb.tt_out tout on tout.tt_out_id = tac.tt_out_id and not tout.sign_deleted
          join ttb.tt_variant v on v.tt_variant_id = tout.tt_variant_id

        where tac.tt_action_id = p_tt_action_id
          and  case when p_is_last then tac.dt_action else coalesce(upper(tac.action_range), tac.dt_action) end != p_dt_action_new
        )
        select n.*, trnd.tt_at_round_id, adata.tr_capacity_id, adata.dt_action
        from adata
          join ttb.tt_action_type tact on tact.tt_variant_id =adata.tt_variant_id and tact.action_type_id = adata.action_type_id
          join ttb.tt_at_round trnd on trnd.tt_action_type_id = tact.tt_action_type_id
          join tt_action_round arnd on arnd.tt_action_id = adata.tt_action_id and arnd.round_id = trnd.round_id
          join ttb.timetable_pkg$get_round_tm(trnd.tt_at_round_id, adata.tr_capacity_id,  p_dt_action_new, p_is_last) n on 1=1
        order by  ftime_begin
       )
    loop

    update tt_action_item
    set time_begin = l_rec. ftime_begin
       ,time_end = l_rec. ftime_begin
    where tt_action_id = p_tt_action_id
      and stop_item2round_id = l_rec.fstop_item2round_id ;

  end loop;


   --синхронизируем tt_action
  return ttb.timetable_edit_pkg$update_tt_action(p_tt_action_id , p_is_last);


  RAISE NOTICE 'timetable_edit_pkg$update_round-------2 %', clock_timestamp();




end;
$body$
language plpgsql volatile
cost 100;


------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Обновить времена межрейсовой стоянки
***************************************************************************/

create or replace function ttb."timetable_edit_pkg$update_bn_round_break"(in p_tt_action_id numeric,
                                                                          in p_dt_action_new timestamp,
                                                                          in p_action_dur_new numeric,
                                                                          in p_is_last boolean default true)
  returns timestamp
as
  $body$
  declare
    l_rec record;
    l_cnt smallint;
  begin
    RAISE NOTICE 'timetable_edit_pkg$update_bn-------1 %', clock_timestamp();
    RAISE NOTICE ' p_tt_action_id = %, p_action_dur_new = % p_dt_action_new  = %', p_tt_action_id , p_action_dur_new , p_dt_action_new ;
    if coalesce(p_action_dur_new, 0) = 0 then

        update tt_action_item
        set time_begin = p_dt_action_new
           ,time_end = p_dt_action_new
        where tt_action_id = p_tt_action_id        ;

    else

          for l_rec in (
                with  adata as (
                    select  r.tr_type_id ,  it.stop_item2round_id, it.dr_shift_id
                      --,case coalesce(nr.min_inter_round_stop_dur,0)  when 0 then asd.get_constant('MIN_INTER_RACE_STOP')::int   else nr.min_inter_round_stop_dur end  as min_inter_round_stop_dur
                      ,coalesce(nr.min_inter_round_stop_dur,0) as min_inter_round_stop_dur
                      ,coalesce(nr.reg_inter_round_stop_dur,0) as reg_inter_round_stop_dur
                      ,coalesce(extract(EPOCH from (select sum(it.time_end - it.time_begin) from tt_action_item it where it.tt_action_id  = tac.tt_action_id and it.action_type_id = ttb.action_type_pkg$bn_round_break_corr())),0) as corr_dur
                      ,extract(EPOCH from (select max(it.time_end) -min(it.time_begin) from tt_action_item it where it.tt_action_id  = tac.tt_action_id )) as action_dur_old

                    from tt_action  tac
                      join ttb.tt_out tout on tout.tt_out_id = tac.tt_out_id and not tout.sign_deleted
                      join ttb.tt_variant ttv on ttv.tt_variant_id = tout.tt_variant_id
                      join rts.route_variant rv on rv.route_variant_id = ttv.route_variant_id
                      join rts.route r on r.route_id = rv.route_id
                      join (select it.tt_action_id, sir.round_id, sir.stop_item_id , sir.stop_item2round_id, it.dr_shift_id
                            from tt_action_item it
                              join rts.stop_item2round sir on sir.stop_item2round_id = it.stop_item2round_id
                            group by it.tt_action_id, sir.round_id, sir.stop_item_id, sir.stop_item2round_id, it.dr_shift_id) it
                        on  it.tt_action_id = tac.tt_action_id
                      left join ttb.norm_round nr on nr.tr_type_id =r.tr_type_id
                                                     and nr.tr_capacity_id = tout.tr_capacity_id
                                                     and nr.round_id = it.round_id
                                                     and nr.norm_id = ttv.norm_id
                                                     and nr.stop_item_id = it.stop_item_id
                                                     and nr.hour_from = date_trunc('hour',p_dt_action_new::time)

                    where tac.tt_action_id = p_tt_action_id
                          and tac.action_type_id = ttb.action_type_pkg$bn_round_break()

                )
                  , d as (
                    select tr_type_id, stop_item2round_id,     dr_shift_id,     min_inter_round_stop_dur,
                      case when p_action_dur_new > min_inter_round_stop_dur then least((p_action_dur_new - min_inter_round_stop_dur) , reg_inter_round_stop_dur )
                      else 0 end as reg_inter_round_stop_dur,
                      case when p_action_dur_new > (min_inter_round_stop_dur +reg_inter_round_stop_dur) then p_action_dur_new - min_inter_round_stop_dur -reg_inter_round_stop_dur
                      else 0 end as corr_dur
                    from adata
                    --where action_dur_old>0 -- если межрейса не было, так и оставляем
                )
                --Базовое время
                select
                  stop_item2round_id,
                  ttb.action_type_pkg$bn_round_break_base() as action_type_id,
                  case when p_is_last then p_dt_action_new
                  else p_dt_action_new - (corr_dur + min_inter_round_stop_dur+ reg_inter_round_stop_dur) * interval '1 second' end as time_begin ,

                  case when p_is_last then p_dt_action_new +  min_inter_round_stop_dur * interval '1 second'
                  else p_dt_action_new - (corr_dur +reg_inter_round_stop_dur) * interval '1 second' end as time_end,
                  dr_shift_id
                from d


                union all
                --регулировочное время
                select
                  stop_item2round_id,
                  ttb.action_type_pkg$bn_round_break_reg() as action_type_id,
                  case when p_is_last then p_dt_action_new +  min_inter_round_stop_dur * interval '1 second'
                  else p_dt_action_new - (corr_dur + reg_inter_round_stop_dur) * interval '1 second' end as time_begin ,

                  case when p_is_last then p_dt_action_new +  (min_inter_round_stop_dur+reg_inter_round_stop_dur) * interval '1 second'
                  else p_dt_action_new - corr_dur  * interval '1 second' end as time_end,
                  dr_shift_id
                from d

                union all
                --корректировочное время
                select
                  stop_item2round_id,
                  ttb.action_type_pkg$bn_round_break_corr() as action_type_id,

                  case when p_is_last then p_dt_action_new +  (min_inter_round_stop_dur +reg_inter_round_stop_dur)   * interval '1 second'
                  else p_dt_action_new - corr_dur * interval '1 second'    end  as time_begin ,

                  case when p_is_last then p_dt_action_new + (corr_dur + min_inter_round_stop_dur+ reg_inter_round_stop_dur) * interval '1 second'
                  else  p_dt_action_new end    as time_end,

                  dr_shift_id
                from d
          )    loop
            RAISE NOTICE 'update % -- %---%---%---%',l_rec.time_begin, l_rec.time_end, p_tt_action_id, l_rec.action_type_id, l_rec.stop_item2round_id;
               if l_rec.time_begin = l_rec.time_end and l_rec.action_type_id != ttb.action_type_pkg$bn_round_break_base() then
                 RAISE NOTICE 'delete';
                  delete from  tt_action_item
                  where tt_action_id = p_tt_action_id
                  and action_type_id = l_rec.action_type_id
                  and stop_item2round_id = l_rec.stop_item2round_id
                 ;
                else
                  with upd as (
                      update tt_action_item
                      set time_begin = l_rec.time_begin
                         ,time_end = l_rec.time_end
                      where tt_action_id = p_tt_action_id
                            and action_type_id = l_rec.action_type_id
                            and stop_item2round_id = l_rec.stop_item2round_id
                       returning *
                  )
                  insert into tt_action_item(tt_action_id ,  time_begin, time_end, stop_item2round_id,  action_type_id, dr_shift_id)
                  select p_tt_action_id,  l_rec.time_begin, l_rec.time_end, l_rec.stop_item2round_id, l_rec.action_type_id, l_rec.dr_shift_id
                  where not exists (select 1 from upd where action_type_id = l_rec.action_type_id)
                     and l_rec.time_begin< l_rec.time_end
                  returning 1 into l_cnt;


                  RAISE NOTICE ' insert % rows', l_cnt;




               end if;
          end loop;
    end if;

    RAISE NOTICE '--синхронизируем (%) p_tt_action_id', p_tt_action_id;
    --синхронизируем tt_action
    return ttb.timetable_edit_pkg$update_tt_action(p_tt_action_id , p_is_last);



  end;
$body$
language plpgsql volatile
cost 100;


------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Обновить времена перерывов
***************************************************************************/

create or replace function ttb."timetable_edit_pkg$update_break"(in p_tt_action_id numeric,
                                                                 in p_dt_action_new timestamp,
                                                                 in p_action_dur_new numeric,
                                                                 in p_is_last boolean default true
)
  returns timestamp
as
  $body$
  declare
    l_rec record;
    l_cnt smallint;
    l_res timestamp;
    l_dur integer;
  begin
    RAISE NOTICE 'timetable_edit_pkg$update_break-------1 %', clock_timestamp();

    --находим минимально возможное время перерыва
    select
      ttb.timetable_pkg$get_break_dur_default( p_action_type_id=> act.action_type_id,
                                               p_tt_variant_id => tout.tt_variant_id,
                                               p_tr_capacity_id=>tout.tr_capacity_id)  as min_action_dur
    into l_dur
    from tt_action act
    join ttb.tt_out tout on tout.tt_out_id = act.tt_out_id and not tout.sign_deleted
     where act.tt_action_id = p_tt_action_id;
    --RAISE NOTICE 'l_dur ( % ),  p_action_dur_new( % )', l_dur , p_action_dur_new;

    if coalesce(l_dur,0) < p_action_dur_new then
       l_dur:=p_action_dur_new;
    end if;


    for l_rec in (
        select l_dur as action_dur, tac.action_type_id, it.stop_item2round_id, it.dr_shift_id
        from tt_action  tac
        join (select it.tt_action_id, sir.round_id, sir.stop_item_id , sir.stop_item2round_id, it.dr_shift_id
              from tt_action_item it
              join rts.stop_item2round sir on sir.stop_item2round_id = it.stop_item2round_id
              group by it.tt_action_id, sir.round_id, sir.stop_item_id, sir.stop_item2round_id, it.dr_shift_id) it
                       on  it.tt_action_id = tac.tt_action_id
        where tac.tt_action_id = p_tt_action_id
              ) loop

      update tt_action_item
      set time_begin = case when p_is_last  then p_dt_action_new
                       else p_dt_action_new  - l_rec.action_dur* interval '1 second' end
         ,time_end = case when p_is_last  then p_dt_action_new  + l_rec.action_dur* interval '1 second'
                     else p_dt_action_new  end
      where tt_action_id = p_tt_action_id
            and action_type_id = l_rec.action_type_id
            and stop_item2round_id = l_rec.stop_item2round_id   ;

    end loop;

    RAISE NOTICE 'timetable_edit_pkg$update_break-------2 %', clock_timestamp();
    --синхронизируем tt_action
    return ttb.timetable_edit_pkg$update_tt_action(p_tt_action_id , p_is_last);


  end;
$body$
language plpgsql volatile
cost 100;


------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Запуск редактирования по номеру выхода
***************************************************************************/

create or replace function ttb."timetable_edit_pkg$update_out"(in p_tt_action_id numeric,
                                                                  in p_dt_action_new timestamp,
                                                                  in p_is_auto boolean default true,
                                                                  in p_is_last boolean default true
)
  returns timestamp
as
$body$
declare
  l_rec record;
  l_new_dt timestamp;
  l_new_dur numeric;
begin
  if p_tt_action_id is null then return null; end if;


  if p_is_last then
    -- сдвиг налево

        for l_rec in (
                      with tout as (
                          select tac.tt_out_id, tac.tt_action_id , p_dt_action_new -dt_action as delta , p_dt_action_new as dt_new
                          from   tt_action  tac
                          where  tac.tt_action_id =  p_tt_action_id
                      )
                        , adata as (
                          select
                              row_number() over w rn
                            , tout.tt_action_id = t.tt_action_id as is_edit
                            , t.*
                            , coalesce(upper(t.action_range), t.dt_action) =lead(dt_action,1,  coalesce(upper(t.action_range), t.dt_action)) over w as is_good
                            ,lead(dt_action,1, dt_action) over w as dt_action_end
                            , tout.delta, tout.dt_new
                            , t.action_type_id =ttb.action_type_pkg$bn_round_break() as is_bn , vma.is_round, vma.is_break
                          from tout
                            join tt_action t on t.tt_out_id = tout.tt_out_id
                            join ttb.vm_action_type vma on vma.action_type_id =t.action_type_id
                          window w as (order by  t.dt_action, coalesce(upper(t.action_range), t.dt_action) )
                      )

                      select


                         case when  d.rn = rn_edit.rn and (d.is_break or not d.action_type_id = ttb.action_type_pkg$change_driver())
                           then  case when is_good then extract(epoch from d.delta ) + d.action_dur  else extract (epoch from( dt_new - dt_action)) end
                         else d.action_dur  end::numeric as new_action_dur
                        ,extract(epoch from d.delta ) + d.action_dur
                        ,case when  d.rn = rn_edit.rn  and (d.is_break or not d.action_type_id = ttb.action_type_pkg$change_driver())
                           then d.dt_action  else d.dt_action  + d.delta end  as new_dt_action
                        ,d.*

                      from adata d
                        , (select rn - 1  as rn  from adata where is_edit) as rn_edit

                      where d.rn >=  rn_edit.rn


        ) loop

          RAISE NOTICE ' p_dt_action_new ( % ), l_rec ( % )  ',p_dt_action_new , l_rec ;

          l_new_dt:= coalesce (l_new_dt, l_rec.new_dt_action  )  ;
          if l_rec.new_action_dur <0 then
            RAISE EXCEPTION '<<Сдвиг превышает возможные значение>>';
          end if;


          RAISE NOTICE 'в прямом направлении --------------l_new_dt before-%  ',l_new_dt;
             if l_rec.is_round then
               --рейсы
               l_new_dt:= ttb.timetable_edit_pkg$update_round(l_rec.tt_action_id, l_new_dt, p_is_last);
             elseif l_rec.is_bn and  (p_is_auto or l_rec.rn>1) then
               --межрейсы
               l_new_dt:= ttb.timetable_edit_pkg$update_bn_round_break(l_rec.tt_action_id, l_new_dt, l_rec.new_action_dur, p_is_last);
             elseif l_rec.is_break then
               --перерывы
               l_new_dt:= ttb.timetable_edit_pkg$update_break(l_rec.tt_action_id, l_new_dt, l_rec.new_action_dur, p_is_last);
             end if;
          RAISE NOTICE ' l_new_dt ( % )  ',l_new_dt ;
        end loop;

    else
      ---сдвиг направо

      for l_rec in (
        with tout as (
            select tac.tt_out_id, tac.tt_action_id , p_dt_action_new -dt_action as delta , p_dt_action_new as dt_new
            from   tt_action  tac
            where  tac.tt_action_id =  p_tt_action_id
        )
          , adata as (
            select
                row_number() over w rn
              , tout.tt_action_id = t.tt_action_id as is_edit
              , t.*
              , coalesce(upper(t.action_range), t.dt_action) =lead(dt_action,1,  coalesce(upper(t.action_range), t.dt_action)) over w as is_good
              ,lead(dt_action,1, dt_action) over w as dt_action_end
              , tout.delta, tout.dt_new
              , t.action_type_id =ttb.action_type_pkg$bn_round_break() as is_bn , vma.is_round, vma.is_break
            from tout
              join tt_action t on t.tt_out_id = tout.tt_out_id
              join ttb.vm_action_type vma on vma.action_type_id =t.action_type_id
            window w as (order by   coalesce(upper(t.action_range), t.dt_action) desc , t.dt_action desc)
        )
        select
           case when  d.rn = rn_edit.rn and d.is_break then
             case when is_good then extract(epoch from d.delta ) + d.action_dur  else extract (epoch from( dt_new - dt_action)) end
           else d.action_dur  end::numeric as new_action_dur

          ,case when  d.rn = rn_edit.rn  and d.is_break   then d.dt_action  else d.dt_action  + d.delta end  as new_dt_action
          ,d.*
        from adata d
          , (select rn - 1  as rn  from adata where is_edit) as rn_edit
        where d.rn >  rn_edit.rn


      ) loop
        l_new_dt:= coalesce (l_new_dt, l_rec.new_dt_action  )  ;

        RAISE NOTICE '%  rn = % ,tt_action_id=%, l_new_dt = %, l_new_dur=%, STEP 1', clock_timestamp(), l_rec.rn, l_rec.tt_action_id, l_new_dt, l_rec.new_action_dur;
        RAISE NOTICE 'в обратном направлении---------------%  ',l_rec;
        if l_rec.is_round then
          --рейсы
          l_new_dt:= ttb.timetable_edit_pkg$update_round(l_rec.tt_action_id, l_new_dt, p_is_last);
        elseif l_rec.is_bn then
          --межрейсы
          l_new_dt:= ttb.timetable_edit_pkg$update_bn_round_break(l_rec.tt_action_id, l_new_dt, l_rec.new_action_dur, p_is_last);
        elseif l_rec.is_break then
          --перерывы
          l_new_dt:= ttb.timetable_edit_pkg$update_break(l_rec.tt_action_id, l_new_dt, l_rec.new_action_dur, p_is_last);
        end if;

      end loop;

    end if;
   return l_new_dt;

end;
$body$
language plpgsql volatile
cost 100;


------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Удаление
***************************************************************************/

create or replace function ttb.timetable_edit_pkg$delete_action(in p_tt_action_id numeric)
  returns timestamp
as
$body$
declare
  l_rec record;
  l_new_dt timestamp;
  l_new_dur numeric;
begin
  RAISE NOTICE '************delete_action*********  % ', p_tt_action_id;
  for l_rec in (

      with adata as (
            select tac.tt_action_id,     tac.tt_out_id ,    tac.action_dur,     tac.dt_action, tac.action_type_id    ,
              told.min_dt as old_dt_action, told.max_dt as next_dt,  tac.tt_action_group_id
            from tt_action  tac
              join (select act.tt_out_id, min(act.dt_action) as min_dt, max(act.dt_action) as max_dt
                    from tt_action act
                      join tt_action s on s.tt_out_id = act.tt_out_id
                                              and s.tt_action_group_id = act.tt_action_group_id
                                              and s.action_gr_id = act.action_gr_id
                    where s.tt_action_id =p_tt_action_id
                    group by act.tt_out_id ) told on told.tt_out_id = tac.tt_out_id
            order by  dt_action
        )
        select adata.tt_action_id,  adata.tt_out_id,adata.dt_action , adata.action_type_id,  adata.old_dt_action
          , it.action_dur, adata.tt_action_group_id , row_number() over (order by dt_action) rn
          , vma.is_round, vma.is_break
        from  adata
          join (select it.tt_action_id, it.dr_shift_id, extract(EPOCH from(max(it.time_end) - min(it.time_begin))) as  action_dur, max(it.time_end) dt1,  min(it.time_begin) dt2

                from tt_action_item it
                  join rts.stop_item2round sir on sir.stop_item2round_id = it.stop_item2round_id
                group by it.tt_action_id, it.dr_shift_id
               ) it     on  it.tt_action_id = adata.tt_action_id
          join ttb.vm_action_type vma on vma.action_type_id = adata.action_type_id
        where dt_action > next_dt
        order by adata.dt_action
      ) loop
              if l_rec.rn =1 then
                l_new_dt:= l_rec.old_dt_action    ;
                l_new_dur:= extract (EPOCH from l_rec.dt_action - l_rec.old_dt_action) ;
              else
                l_new_dur:= l_rec.action_dur;
              end if;
              RAISE NOTICE '%  rn = % ,tt_action_id=%, l_new_dt = %, l_new_dur=%, STEP 1', clock_timestamp(), l_rec.rn, l_rec.tt_action_id, l_new_dt, l_new_dur;
              if l_rec.is_round then
                --рейсы
                l_new_dt:= ttb.timetable_edit_pkg$update_round(l_rec.tt_action_id, l_new_dt);
              elseif l_rec.action_type_id = ttb.action_type_pkg$bn_round_break() then
                --межрейсы
                l_new_dt:= ttb.timetable_edit_pkg$update_bn_round_break(l_rec.tt_action_id, l_new_dt, l_new_dur);
              elseif l_rec.is_break then
                --перерывы
                l_new_dt:= ttb.timetable_edit_pkg$update_break(l_rec.tt_action_id, l_new_dt, l_new_dur);
              end if;
        end loop;

      --удаляем группу
      perform  ttb.timetable_edit_pkg$delete_action_group(p_tt_action_id);

return l_new_dt;

end;
$body$
language plpgsql volatile
cost 100;

------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Удалить группу действия полностью
***************************************************************************/

create or replace function ttb.timetable_edit_pkg$delete_action_group(in p_tt_action_id numeric)
  returns void
as
$body$
declare
begin
  delete from tt_action
        where tt_action_id in (
                            select act.tt_action_id
                            from tt_action act
                              join tt_action s on s.tt_out_id = act.tt_out_id
                                                      and s.tt_action_group_id = act.tt_action_group_id
                                                      and s.action_gr_id = act.action_gr_id
                            where s.tt_action_id = p_tt_action_id);

end;
$body$
language plpgsql volatile
cost 100;


------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Выпуск на линию вручную
***************************************************************************/

create or replace function ttb."timetable_edit_pkg$make_out"(in p_tt_variant_id numeric,
                                                                in p_tt_schedule_id numeric,
                                                                in p_tm_start timestamp,
                                                                in p_stop_item_id numeric default null)
  returns void
as
  $body$
  declare
    l_rec record;
    l_tm_start timestamp := p_tm_start;
  begin

  for l_rec in (
    with adata as (
        select
          fout_num as out_num,
          ftr_mode_id as tr_mode_id,
          ftr_capacity_id as tr_capacity_id,
          ftt_tr_id as tt_tr_id,
          fdr_shift_id as  dr_shift_id
        from ttb.jf_tt_out_pkg$get_free_out(p_tt_variant_id, p_tt_schedule_id)
        order by  ftt_tr_order_num asc
        limit 1
    ) , my_out as (
        insert into  ttb.tt_out(tt_variant_id , tr_capacity_id , mode_id , tt_out_num )
       select p_tt_variant_id, adata.tr_capacity_id , adata.tr_mode_id, adata.out_num
          from adata
        returning *
    )
     , my_tt_out2tt_tr as (
        insert into  ttb.tt_out2tt_tr(tt_out_id, tt_tr_id)
        select my_out.tt_out_id, adata.tt_tr_id
        from my_out, adata
        )
      select adata.dr_shift_id, my_out.tt_out_id ,sch_it.tt_schedule_item_id
      from adata
        join my_out on my_out.tt_out_num= adata.out_num
        join ttb.tt_schedule_item sch_it on 1=1
        join ttb.tt_action_group act_gr on act_gr.tt_action_group_id = sch_it.tt_action_group_id
      where sch_it.tt_schedule_id = p_tt_schedule_id
            and  act_gr.ref_group_kind_id = ttb.ref_group_kind_pkg$first_action() --первое действие из парка
      order by sch_it.order_num
    )
    loop
      perform ttb.tt_out_pkg$make_tmp_tt(l_rec.tt_out_id);
      perform ttb.timetable_edit_pkg$make_schedule_item(p_tt_schedule_item_id => l_rec.tt_schedule_item_id
                                                          ,p_tt_out_id   => l_rec.tt_out_id
                                                          ,p_dr_shift_id => l_rec.dr_shift_id
                                                          ,p_tm_start => l_tm_start
                                                          ,p_is_last  => false
                                                          ,p_stop_item_id => p_stop_item_id);
      perform ttb.tt_out_pkg$fill_db_table(l_rec.tt_out_id);

    end loop;




  end;
$body$
language plpgsql volatile
cost 100;



------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
--обработка последовательности действий внутри группы
***************************************************************************/

create or replace function ttb."timetable_edit_pkg$make_schedule_item"(in p_tt_schedule_item_id numeric
                                                                         ,in p_tt_out_id numeric
                                                                         ,in p_dr_shift_id numeric
                                                                         ,in p_tm_start timestamp
                                                                         ,in p_is_last boolean default true
                                                                         ,in p_tt_action_id  numeric default null
                                                                         ,in p_stop_item_id  numeric default null
                                                                         )
  returns  numeric
as $function$
declare
  l_it record;
  l_res numeric;
begin
  RAISE NOTICE '-----make_schedule_item---------p_tt_schedule_item_id= %  p_tt_out_id =%, p_stop_item_id=%, p_tt_action_id=%, p_tm_start=%'
                                              , p_tt_schedule_item_id, p_tt_out_id, p_stop_item_id, p_tt_action_id, p_tm_start;
  for l_it in
  (
    with sp as (
      select sir.stop_item_id, sir.order_num, r.tt_action_type_id, max(sir.order_num) over(partition by tt_action_type_id) as last_order_num, rs.tt_at_round_id
      from ttb.tt_at_round_stop rs
        join ttb.tt_at_round r on r.tt_at_round_id = rs.tt_at_round_id
        join rts.stop_item2round sir on sir.stop_item2round_id = rs.stop_item2round_id
      union all
      select sir.stop_item_id, sir.order_num, ts.tt_action_type_id,  max(sir.order_num) over(partition by tt_action_type_id) as last_order_num, null
      from ttb.tt_at_stop ts
        join rts.stop_item2round sir on sir.stop_item2round_id = ts.stop_item2round_id
    )
    ,first_sp as (
        select sch_it.tt_schedule_id, sir.stop_item_id
        from ttb.tt_schedule_item sch_it
          join ttb.tt_action_group act_gr on act_gr.tt_action_group_id = sch_it.tt_action_group_id
          join ttb.tt_action_type2group at2g on at2g.tt_action_group_id = act_gr.tt_action_group_id
          join ttb.tt_at_round r on r.tt_action_type_id = at2g.tt_action_type_id
          join ttb.tt_at_round_stop rs on rs.tt_at_round_id = r.tt_at_round_id
          join rts.stop_item2round sir on sir.stop_item2round_id = rs.stop_item2round_id
        where  act_gr.ref_group_kind_id = ttb.ref_group_kind_pkg$main_action()
               and sch_it.tt_schedule_id = (select tt_schedule_id from ttb.tt_schedule_item it where it.tt_schedule_item_id= p_tt_schedule_item_id group by tt_schedule_id)
               and sir.order_num =1
               and (p_stop_item_id is null or sir.stop_item_id = p_stop_item_id)
        limit 1

    )

    select
       tact.action_type_id
      ,tact.tt_action_type_id
      ,act2gr.tt_action_type2group_id
      ,act_gr.tt_action_group_id
      ,sch_it.tt_schedule_id
      ,sch_it.tt_schedule_item_id
      ,sp.tt_at_round_id
      --,case when p_is_last then null else lead(sp.tt_at_round_id) over(order by act2gr.order_num desc) end  as link_tt_at_round_id
      ,lead(sp.tt_at_round_id) over(order by act2gr.order_num desc)  as link_tt_at_round_id
      ,coalesce(last_data.fdr_shift_id,p_dr_shift_id) as dr_shift_id
      ,(select coalesce(max(action_gr_id)+1,1) from tt_action where tt_out_id = p_tt_out_id) as action_gr_id
      ,vma.is_round
      ,vma.is_break
     from ttb.tt_schedule_item sch_it
     join ttb.tt_action_group act_gr on act_gr.tt_action_group_id = sch_it.tt_action_group_id
     join ttb.tt_variant tv on tv.tt_variant_id = act_gr.tt_variant_id
     join rts.route_variant rv on rv.route_variant_id =tv.route_variant_id
     join rts.route r on r.route_id = rv.route_id
     join ttb.tt_action_type2group act2gr on act2gr.tt_action_group_id = act_gr.tt_action_group_id
     join ttb.tt_action_type tact on tact.tt_action_type_id = act2gr.tt_action_type_id
    join ttb.vm_action_type vma on vma.action_type_id = tact.action_type_id
     --left join ttb.tt_at_round tatr on tatr.tt_action_type_id = tact.tt_action_type_id
     left join first_sp on first_sp.tt_schedule_id = sch_it.tt_schedule_id
     left join ttb.timetable_edit_pkg$get_tm_out( p_tt_out_id,p_is_last ,p_tt_action_id=>p_tt_action_id) last_data on 1=1
     left join rts.stop_item2round sir on sir.stop_item2round_id = last_data.fstop_item2round_id
     left join sp on sp.tt_action_type_id = tact.tt_action_type_id  and coalesce(sir.stop_item_id, first_sp.stop_item_id) = sp.stop_item_id
                     --and sp.order_num=case when p_is_last then 1 else sp.last_order_num end
   where sch_it.tt_schedule_item_id = p_tt_schedule_item_id
   --   and tt_action_type2group_id in ( 11986 , 11987 )
     group by
       tact.action_type_id
       ,tact.tt_action_type_id
       ,act2gr.tt_action_type2group_id
       ,act_gr.tt_action_group_id
       ,sch_it.tt_schedule_id
       ,sch_it.tt_schedule_item_id
       ,sp.tt_at_round_id
       ,last_data.fdr_shift_id
       ,act2gr.order_num
       ,vma.is_round
       ,vma.is_break
   order by (case when p_is_last then 1 else -1 end) *  act2gr.order_num )                  loop
    --если рейс с данной ОП невозможен, то пропускаем его
    continue when l_it.is_round and l_it.tt_at_round_id is null; 

    if  l_it.is_round then
      RAISE NOTICE 'p_tt_schedule_item_id= %  l_it.tt_at_round_id =%, p_is_last = % -----РЕЙС-------------------------', p_tt_schedule_item_id, l_it.tt_at_round_id, p_is_last ;
      l_res:=  ttb.timetable_edit_pkg$make_round(
                                             p_tt_out_id,
                                             l_it.dr_shift_id,
                                             case when not p_is_last or l_res is not null then null else p_tm_start end ,
                                             l_it.tt_action_group_id,
                                             l_it.action_gr_id,
                                             l_it.tt_at_round_id,
                                             p_is_last,
                                             p_tt_action_id =>coalesce(l_res, p_tt_action_id)
                                            --in p_order_num numeric default null
                                               );
      raise notice ' round l_res = %',l_res;
     exit when coalesce(l_res,0) =0 ;

    elseif l_it.action_type_id = ttb.action_type_pkg$bn_round_break()  then
      raise notice '********межрейсовая стоянка p_tt_out_id = %  tt_action_type2group_id =  %, p_is_last = %' ,p_tt_out_id, l_it.tt_action_type2group_id, p_is_last;
      l_res:= ttb.timetable_edit_pkg$make_bn_break(
                                              p_tt_out_id,
                                              l_it.dr_shift_id,
                                              p_tm_start,
                                              l_it.tt_action_group_id,
                                              l_it.action_gr_id,
                                              l_it.tt_action_type_id,
                                              p_is_last,
                                              l_it.link_tt_at_round_id,
                                              p_tt_action_id =>coalesce(l_res, p_tt_action_id)
                                                );
      raise notice ' bn_round_break l_res = %',l_res;
      exit when coalesce(l_res,0) =0 ;

    elsif l_it.is_break then
      raise notice '********перерыв tt_action_type2group_id =  %, tt_action_type_id = % ,p_is_last = %, p_tt_action_id=%' , l_it.tt_action_type2group_id, l_it.tt_action_type_id, p_is_last, p_tt_action_id;
      raise notice '**********************  coalesce(l_res, p_tt_action_id) = %' , coalesce(l_res, p_tt_action_id);
      l_res:= ttb.timetable_edit_pkg$make_break(
                                              p_tt_out_id,
                                              l_it.dr_shift_id,
                                              l_it.tt_action_group_id,
                                              l_it.action_gr_id,
                                              l_it.tt_action_type_id,
                                              p_is_last,
                                              p_tt_action_id =>coalesce(l_res, p_tt_action_id)
                                              );
      raise notice ' break l_res = %',l_res;
      exit when coalesce(l_res,0) =0 ;
    end if;


  end loop;
  return l_res;

end;
$function$
language plpgsql volatile
cost 100;




------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
--Cоздание действия рейс
 in p_tt_out_id numeric,  -выход
  in p_dr_shift_id numeric, смена
  in p_tt_action_group_id numeric, группа действий
  in p_tt_at_round_id numeric, рейс действия
  in p_tm_start timestamp,     время начала
  in p_is_last boolean default true,  вектор
  in p_order_num numeric default null  номер остановки, с которой начинать
***************************************************************************/

create or replace function ttb.timetable_edit_pkg$make_round(
  in p_tt_out_id numeric,
  in p_dr_shift_id numeric,
  in p_tm_start timestamp,
  in p_tt_action_group_id numeric,
  in p_action_gr_id numeric,
  in p_tt_at_round_id numeric,
  in p_is_last boolean default true,
  in p_order_num numeric default null,
  in p_tt_action_id  numeric default null
  )
  returns numeric
as $function$
declare
  l_tr_capacity_id numeric;
  l_tm_start timestamp;
  l_stop_item_id numeric;
  l_res numeric;

begin
 select tout.tr_capacity_id  ,last_data.ftm, sir.stop_item_id
 into l_tr_capacity_id, l_tm_start, l_stop_item_id
 from ttb.tt_out tout
 left join ttb.timetable_edit_pkg$get_tm_out( p_tt_out_id, p_is_last , p_tt_action_id=>p_tt_action_id) last_data on 1=1
 left join rts.stop_item2round sir on sir.stop_item2round_id = last_data.fstop_item2round_id
 where tout.tt_out_id = p_tt_out_id;

  l_tm_start:= coalesce( p_tm_start, l_tm_start);
  raise notice 'make_round ********l_tr_capacity_id = %, p_action_gr_id = %, l_tm_start = %, (p_tm_start % )l_stop_item_id = %' , l_tr_capacity_id, p_action_gr_id, l_tm_start,p_tm_start, l_stop_item_id;
  raise notice 'make_round  p_tt_out_id=%,  p_dr_shift_id =%, p_tm_start=%,p_tt_action_group_id =%,p_tt_at_round_id =%, p_is_last =%, p_order_num=%, p_tt_action_id=%',
  p_tt_out_id,  p_dr_shift_id, p_tm_start,p_tt_action_group_id ,p_tt_at_round_id , p_is_last , p_order_num, p_tt_action_id;

  with my_data as (
          select fstop_item2round_id  ,  fround_id  ,   fstop_item_id ,    ftr_capacity_id   ,  ftt_variant_id   ,  faction_type_id
                ,ftime_begin   ,      forder_num   ,  fdur_from_beg ,   sum(fdur_from_beg) over() as   fdur_round
          from  ttb.timetable_pkg$get_round_tm(p_tt_at_round_id, l_tr_capacity_id, l_tm_start, p_is_last, p_order_num)
      )

        ,my_action as (
         insert into  tt_action( tt_out_id, action_dur, dt_action, action_type_id, tt_action_group_id, action_gr_id,  action_range )
          select p_tt_out_id, fdur_round as action_dur, min(ftime_begin) as dt_action , faction_type_id as faction_type_id, p_tt_action_group_id, p_action_gr_id
               , tsrange(min(ftime_begin) , min(ftime_begin) + fdur_round * interval '1 second','[)' ) as action_range
          from my_data
          where l_stop_item_id is null or not  p_is_last or exists(select 1 from my_data where  forder_num =1 and l_stop_item_id = fstop_item_id)
          group by fdur_round , faction_type_id

         returning *
      )
        ,my_round as (
          insert into tt_action_round (tt_action_id, round_id)
          select act.tt_action_id,  my_data.fround_id
          from my_action act
          join my_data on 1=1
          group by act.tt_action_id,  my_data.fround_id
      )
      , my_item as (
           insert into tt_action_item( tt_action_id , time_begin , time_end , stop_item2round_id , action_type_id, dr_shift_id)
           select act.tt_action_id , d.ftime_begin ,d.ftime_begin as time_end , d.fstop_item2round_id , d.faction_type_id, p_dr_shift_id
           from my_action act
           join my_data d on  1=1
       )
        select tt_action_id into l_res from my_action;

       return l_res;

end;
$function$
language plpgsql volatile
cost 100;


------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
--Cоздание действия перерыв (кроме межрейсовой стоянки)
  in p_tt_out_id numeric,  -выход
  in p_dr_shift_id numeric, смена
  in p_tt_action_group_id numeric, группа действий
  in p_tt_action_type_id numeric, тип действия
  in p_is_last boolean default true,  вектор
  in p_tt_action_id  numeric default null --действие, после которого вставляется новое действие
***************************************************************************/


create or replace function ttb.timetable_edit_pkg$make_break(
  in p_tt_out_id numeric,
  in p_dr_shift_id numeric,
  in p_tt_action_group_id numeric,
  in p_action_gr_id numeric,
  in p_tt_action_type_id numeric,
  in p_is_last boolean default true,
  in p_tt_action_id  numeric default null
)
  returns numeric
as $function$
declare
  l_tr_capacity_id numeric;
  l_res numeric;
begin

  with my_data as (
        select distinct  last_data.fstop_item2round_id, last_data.ftm, tact.action_type_id, tact.plan_time,
               last_data.ftt_action_id, last_data.faction_type_id, last_data.faction_gr_id,
               tout.tt_out_id, tout.tr_capacity_id, tout.tt_variant_id
        from ttb.timetable_edit_pkg$get_tm_out(p_tt_out_id =>p_tt_out_id , p_is_last=>p_is_last, p_tt_action_id=>p_tt_action_id) last_data
          join ttb.tt_action_type tact on  tact.tt_action_type_id =  p_tt_action_type_id
          join ttb.tt_at_stop at_stp on at_stp.tt_action_type_id = tact.tt_action_type_id and at_stp.stop_item2round_id = last_data.fstop_item2round_id
          join ttb.tt_out tout on tout.tt_out_id = p_tt_out_id
        limit 1
    )
      ,my_action as (
       insert into  tt_action( tt_out_id, action_dur, dt_action, action_type_id, tt_action_group_id, action_gr_id, action_range )
        select my_data.tt_out_id
          , coalesce(plan_time, ttb.timetable_pkg$get_break_dur_default( p_action_type_id=> my_data.action_type_id, p_tt_variant_id => my_data.tt_variant_id, p_tr_capacity_id=>my_data.tr_capacity_id))  as action_dur
          , ftm as dt_action , my_data.action_type_id , p_tt_action_group_id
          , p_action_gr_id
          , tsrange(ftm, ftm + coalesce(plan_time,coalesce(plan_time, ttb.timetable_pkg$get_break_dur_default( p_action_type_id=> my_data.action_type_id, p_tt_variant_id => my_data.tt_variant_id,  p_tr_capacity_id=>my_data.tr_capacity_id))) * interval '1 second','[)') action_range
        from my_data
       returning *
    )
    ,my_shift as (
       select dr_shift_id
       from
         (select drs.dr_shift_id
         from ttb.tt_out
           join ttb.dr_shift drs on drs.mode_id = tt_out.mode_id
           join my_action on 1=1
         where tt_out.tt_out_id = p_tt_out_id
               and drs.dr_shift_id != p_dr_shift_id
               and my_action.action_type_id = ttb.action_type_pkg$change_driver()
               and not tt_out.sign_deleted
         order by dr_shift_num limit 1) d
       union all
       select p_dr_shift_id from my_action where my_action.action_type_id != ttb.action_type_pkg$change_driver()
       )
    ,my_item as (
       insert into tt_action_item( tt_action_id , time_begin , time_end , stop_item2round_id , action_type_id, dr_shift_id)
       select act.tt_action_id , act.dt_action  ,dt_action  +   action_dur * interval '1 second' as time_end , d.fstop_item2round_id , d.action_type_id, my_shift.dr_shift_id
       from my_action act
       join my_data d on  1=1
       join my_shift on 1=1
       returning *
    )
    select tt_action_id into l_res from my_action;

    raise notice 'ttb.timetable_edit_pkg$make_break l_res(%)', l_res;

    return l_res;

end;
$function$
language plpgsql volatile
cost 100;

------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
--Cоздание действия межрейсовая стоянка
  in p_tt_out_id numeric,  -выход
  in p_dr_shift_id numeric, смена
  in p_tm_start timestamp,
  in p_tt_action_group_id numeric, группа действий
  in p_tt_action_type_id numeric, тип действия
  in p_is_last boolean default true,  вектор
  in link_tt_at_round_id связанный рейс для обратного формирования
***************************************************************************/


create or replace function ttb.timetable_edit_pkg$make_bn_break(
  in p_tt_out_id numeric,
  in p_dr_shift_id numeric,
  in p_tm_start timestamp,
  in p_tt_action_group_id numeric,
  in p_action_gr_id numeric,
  in p_tt_action_type_id numeric,
  in p_is_last boolean default true,
  in p_link_tt_at_round_id numeric default null,
  in p_tt_action_id  numeric default null
)
  returns numeric
as $function$
declare
  l_tr_capacity_id numeric;
  l_res numeric;
begin
  raise notice 'ttb.timetable_edit_pkg$make_bn_break(
      p_tt_out_id (%)
      ,p_dr_shift_id (%)
      ,p_tm_start (%)
      ,p_tt_action_group_id (%)
      ,p_action_gr_id (%)
      ,p_tt_action_type_id (%)
      ,p_is_last (%)
      ,p_link_tt_at_round_id (%)
      ,p_tt_action_id (%)
               '
      ,p_tt_out_id
      ,p_dr_shift_id
      ,p_tm_start
      ,p_tt_action_group_id
      ,p_action_gr_id
      ,p_tt_action_type_id
      ,p_is_last
      ,p_link_tt_at_round_id
      ,p_tt_action_id  ;


  with adata as (
      select distinct last_data.fstop_item2round_id, coalesce(last_data.ftm,  p_tm_start) ftm, tact.action_type_id, tact.plan_time, nr.min_inter_round_stop_dur, nr.reg_inter_round_stop_dur
      from ttb.timetable_edit_pkg$get_tm_out(p_tt_out_id, p_is_last,p_dr_shift_id,  p_tm_start, p_link_tt_at_round_id, p_tt_action_id=>p_tt_action_id ) last_data
        join ttb.tt_action_type tact on  tact.tt_action_type_id =  p_tt_action_type_id
        join ttb.tt_at_stop at_stp on at_stp.tt_action_type_id = tact.tt_action_type_id and at_stp.stop_item2round_id = last_data.fstop_item2round_id
        join ttb.tt_out tt_out on tt_out.tt_out_id = p_tt_out_id and not tt_out.sign_deleted
        join ttb.tt_variant v on v.tt_variant_id = tt_out.tt_variant_id
        join rts.route_variant rv on rv.route_variant_id = v.route_variant_id
        join rts.route r on r.route_id = rv.route_id
        join rts.stop_item2round sir on sir.stop_item2round_id = last_data.fstop_item2round_id
        join ttb.norm_round nr on nr.tr_type_id =r.tr_type_id
                                        and nr.tr_capacity_id = tt_out.tr_capacity_id
                                        and nr.round_id =  sir.round_id
                                        and nr.stop_item_id = sir.stop_item_id
                                        and nr.norm_id = v.norm_id
                                        and nr.hour_from = date_trunc('hour',last_data.ftm)::time
  )
    ,my_data as (
    select
      fstop_item2round_id,
      ttb.action_type_pkg$bn_round_break_base() as action_type_id,
      ftm + case when  p_is_last then 0 else  (min_inter_round_stop_dur+reg_inter_round_stop_dur)*-1 end * interval '1 second' as time_begin,
      ftm + case when  p_is_last then min_inter_round_stop_dur else reg_inter_round_stop_dur*-1 end * interval '1 second' as time_end,
      min_inter_round_stop_dur as dur_from_beg,
      min_inter_round_stop_dur as dur_round
    from adata
    union all
    --добавляем регулировочный мо
    select
      fstop_item2round_id,
      ttb.action_type_pkg$bn_round_break_reg() as action_type_id,
      ftm + case  when p_is_last  then min_inter_round_stop_dur else    reg_inter_round_stop_dur*-1 end * interval '1 second' as time_begin,
      ftm +case when p_is_last  then min_inter_round_stop_dur+reg_inter_round_stop_dur else 0 end * interval '1 second' as time_end,
      reg_inter_round_stop_dur as dur_from_beg,
      reg_inter_round_stop_dur as dur_round
    from adata
    where reg_inter_round_stop_dur>0
  )
    ,my_action as (
     insert into  tt_action( tt_out_id, action_dur, dt_action, action_type_id, tt_action_group_id, action_gr_id, action_range )
      select p_tt_out_id, min_inter_round_stop_dur + reg_inter_round_stop_dur as action_dur
        , ftm - case when p_is_last then 0 else min_inter_round_stop_dur + reg_inter_round_stop_dur end * interval '1 second' as dt_action
        , ttb.action_type_pkg$bn_round_break() as action_type_id
        , p_tt_action_group_id
        , p_action_gr_id
        , tsrange(ftm - case when p_is_last then 0 else min_inter_round_stop_dur + reg_inter_round_stop_dur end * interval '1 second',
                  ftm + case when p_is_last then min_inter_round_stop_dur + reg_inter_round_stop_dur else 0 end * interval '1 second','[)')
      from adata
     returning *
  )
    ,my_item as (
      insert into tt_action_item( tt_action_id , time_begin , time_end , stop_item2round_id , action_type_id, dr_shift_id)
      select act.tt_action_id , d.time_begin  , d.time_end , d.fstop_item2round_id , d.action_type_id, p_dr_shift_id
      from my_action act
      join my_data d on  1=1
      returning *
  )
  select tt_action_id into l_res from my_action;

  --возвращаем id созданного межрейса или тот, что на вход
  return coalesce(l_res, p_tt_action_id);

end;
$function$
language plpgsql volatile
cost 100;


/*
in p_tt_out_id numeric,
in p_is_last boolean default true, вектор
in p_dr_shift_id numeric default null, смена
in p_tm_start timestamp default null, время начала для первого действия
in p_tt_at_round_id numeric default null, рейс для определения последней ОП, на которой д.б стояка, если еще нет рейса и вектор в обратном напрвлении
in p_tt_action_id  numeric default null --действие, после которого вставляется новое действие
                                                                  )

*/
create or replace function ttb."timetable_edit_pkg$get_tm_out"(in p_tt_out_id numeric,
                                                                  in p_is_last boolean default true,
                                                                  in p_dr_shift_id numeric default null,
                                                                  in p_tm_start timestamp default null,
                                                                  in p_tt_at_round_id numeric default null,
                                                                  in p_tt_action_id  numeric default null
                                                                  )
  returns TABLE(
    fstop_item2round_id  bigint, --остановка
    ftm     timestamp,
    fdr_shift_id smallint,
    ftt_action_id integer,
    faction_type_id smallint,
    faction_gr_id   integer
  )
as $body$
declare
  l_cnt smallint;
begin

  select count(1) into l_cnt
  from tt_action act where act.tt_out_id = p_tt_out_id;

  if l_cnt = 0 then
    RAISE NOTICE 'timetable_edit_pkg$get_tm_out l_cnt = 0' ;
    return query
    with adata as (
        select sir.stop_item_id, sir.order_num, r.tt_action_type_id,
          max(sir.order_num) over(partition by tt_action_type_id) as last_order_num, rs.tt_at_round_id, sir.stop_item2round_id
        from ttb.tt_at_round_stop rs
          join ttb.tt_at_round r on r.tt_at_round_id = rs.tt_at_round_id
          join rts.stop_item2round sir on sir.stop_item2round_id = rs.stop_item2round_id
        where r.tt_at_round_id = p_tt_at_round_id
    )
    select stop_item2round_id, p_tm_start, p_dr_shift_id::smallint
         , null::integer as tt_action_id, null::smallint  as action_type_id, null::integer as action_gr_id
    from adata
    where order_num = case when p_is_last then 1 else last_order_num end;
  end if;

  if  p_is_last then
    RAISE NOTICE 'get_tm_out !!' ;
          return query
          select distinct
              first_value(it.stop_item2round_id) over w as stop_item2round_id
            , first_value(it.time_end) over w as time_end
            , first_value(it.dr_shift_id) over w as dr_shift_id
            , first_value(act.tt_action_id) over w as tt_action_id
            , first_value(act.action_type_id) over w as action_type_id
            , first_value(act.action_gr_id) over w as action_gr_id
          from tt_action act
            join tt_action_item it on it.tt_action_id = act.tt_action_id
          where act.tt_out_id = p_tt_out_id
            and (p_tt_action_id is null or act.tt_action_id=p_tt_action_id )
          window w as (order by it.time_end desc,  it.time_begin desc);
      else
        return query
        select distinct
            first_value(it.stop_item2round_id) over w as stop_item2round_id
          , first_value(it.time_begin) over w as time_end
          , first_value(it.dr_shift_id) over w as dr_shift_id
          , first_value(act.tt_action_id) over w as tt_action_id
          , first_value(act.action_type_id) over w as action_type_id
          , first_value(act.action_gr_id) over w as action_gr_id
        from tt_action act
          join tt_action_item it on it.tt_action_id = act.tt_action_id
        where act.tt_out_id = p_tt_out_id
         and (p_tt_action_id is null or act.tt_action_id=p_tt_action_id )
        window w as (order by it.time_end, it.time_begin);
      end if;
  end;
$body$
  language plpgsql volatile
  cost 100;



/*
 по действию найти предудущее и последующее на временных таблицах!
*/
create or replace function  ttb.timetable_edit_pkg$get_near_action(in p_tt_action_id numeric)
  returns TABLE(
    fprev_tt_action_id integer,
    fprev_action_type_id smallint,
    fdt_action_begin timestamp,
    fdt_action_end timestamp,
    faction_type_id smallint,
    fnext_tt_action_id integer
  )
as
$body$
declare
begin
  return query
  with adata as (
      select lag(act.tt_action_id) over (order by  coalesce(upper(act.action_range), act.dt_action), act.dt_action) as prev_tt_action_id
            ,lag(act.action_type_id) over (order by coalesce(upper(act.action_range), act.dt_action), act.dt_action) as prev_action_type_id
            ,act.tt_action_id, act.action_type_id, act.dt_action,  act.action_range,  coalesce(upper(act.action_range), act.dt_action) dt_action_end

      from tt_action act
      where tt_out_id = (select tt_out_id from tt_action a where a.tt_action_id = p_tt_action_id )
  )
  select prev_tt_action_id
    ,  prev_action_type_id
    ,  dt_action as dt_action_begin
    ,  dt_action_end
    ,  action_type_id
    , (select tt_action_id from adata d where  d.prev_tt_action_id = adata.tt_action_id
                                               and  d.prev_tt_action_id !=d.tt_action_id group by tt_action_id) next_tt_action_id
  from adata
  where
    tt_action_id =p_tt_action_id
    and prev_tt_action_id !=tt_action_id;
end;
$body$
language plpgsql volatile
cost 100;

/*Список действий после заданного действия*/
create or replace function ttb.timetable_edit_pkg$get_next_action(in p_tt_action_id numeric)
  returns TABLE(
    ftt_action_id integer,
    ftt_action_item_id bigint,
    frnd_action_type_id smallint,
    faction_type_id smallint,
    fitem_action_type_id smallint,
    ftime_begin timestamp,
    ftime_end  timestamp,
    fdr_shift_id smallint
  )
as
$body$
declare
begin
  return query
  select    act.tt_action_id, it.tt_action_item_id, rnd.action_type_id as rnd_action_type_id, act.action_type_id, it.action_type_id as item_action_type_id
    ,it.time_begin, it.time_end, it.dr_shift_id
  from  tt_action_item it
    join rts.stop_item2round sir on sir.stop_item2round_id = it.stop_item2round_id
    join rts.round rnd on rnd.round_id = sir.round_id
    join tt_action act on act.tt_action_id = it.tt_action_id
  where act.dt_action >=(
    select max(t.time_begin)
    from  tt_action_item t
    where t.tt_action_id = p_tt_action_id
          and not t.sign_deleted)
        and act.tt_out_id = (select tt_out_id from tt_action where tt_action_id =p_tt_action_id)
        and not act.sign_deleted
  ;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
COST 100;


------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Удалить межрейсовую стоянку из предыдущего действия, считаем, что предыдущее действие не содержит технологическо
***************************************************************************/
create or replace function ttb.timetable_edit_pkg$delete_bn_before_dinner(p_tt_schedule_id numeric,
                                                                             p_tt_tr_id numeric,
                                                                             p_tr_capacity_id numeric,
                                                                        p_one_id numeric)
  returns void
as
$body$
declare
begin
  RAISE NOTICE '--delete_bn_before_dinner p_tt_tr_id= %-----p_tr_capacity_id = %----p_one_id=%', p_tt_tr_id, p_tr_capacity_id, p_one_id ;

  with t as (
      select
        vma.is_bn_round_break as is_bn,
        t.time_end, t.tt_cube_id
      from  ttb.tt_cube t
      join ttb.vm_action_type vma on vma.action_type_id = t.action_type_id
      where tt_schedule_id=p_tt_schedule_id and tt_tr_id = p_tt_tr_id and tr_capacity_id = p_tr_capacity_id and one_id=p_one_id)
    , adata as (
      select
        row_number() over(order by time_end desc) -
        row_number() over(partition by is_bn order by time_end desc) rn
        ,t.* from t
      order by time_end desc
  )
  delete from ttb.tt_cube
  where tt_cube_id in ( select tt_cube_id from adata where rn = 0);


end;

$body$
language plpgsql volatile
cost 100;



/* зАМЕНА ДЕЙСТВИЯ ОТСТОЙ*/

create or replace function ttb.timetable_edit_pkg$change_action2pause (in  p_tt_variant_id numeric,
                                                                       in  p_tt_action_id numeric
)
  returns void as
$body$
declare
  l_cnt smallint;
  l_sch record;
begin

  --проверяем возможность дать отстой
  with act_pause as (
      select it.stop_item2round_id
      from ttb.tt_action_item it where it.tt_action_id = p_tt_action_id and not it.sign_deleted
      order by time_end desc
      limit 1
  )
  select count(1) into l_cnt
  from
    ttb.tt_action_type act
    join ttb.tt_at_stop asp on asp.tt_action_type_id = act.tt_action_type_id
    left join act_pause on act_pause.stop_item2round_id  = asp.stop_item2round_id
  where act.tt_variant_id = p_tt_variant_id
        and act.action_type_id = ttb.action_type_pkg$pause();


  if l_cnt>0 then
    --Пункт отстоя определен, отстой внесен в последовательность действий
    RAISE NOTICE '--отстой будет';

    for l_sch in(
      select sch_it.tt_schedule_item_id
      from ttb.v_tt_schedule_item sch_it
      where sch_it.tt_variant_id = p_tt_variant_id
            and  sch_it.ref_group_kind_id = ttb.ref_group_kind_pkg$pause_action() --действие отстой
            and not exists (select 1
                            from  ttb.tt_action tact
                            where tact.tt_action_id = p_tt_action_id
                                  and tact.action_type_id = ttb.action_type_pkg$pause())
      order by sch_it.order_num
    ) loop

      --меняем действие на отстой


      with upd as (
        update ttb.tt_action
        set action_type_id = ttb.action_type_pkg$pause()
        where tt_action_id = p_tt_action_id
              and ttb.action_type_pkg$is_break(action_type_id)
        returning *
      )
        ,upd_it as (
        update ttb.tt_action_item
        set (time_begin , time_end, action_type_id) = (select upd.dt_action, upd.dt_action + upd.action_dur * interval '1 second', ttb.action_type_pkg$pause()from upd)
        where tt_action_id =p_tt_action_id
          and not sign_deleted
        returning *

      )
      delete from ttb.tt_action_item
      where tt_action_id = p_tt_action_id
            and tt_action_item_id !=(select min( t.tt_action_item_id) from ttb.tt_action_item t where t.tt_action_id = p_tt_action_id and not t.sign_deleted)
            and not sign_deleted

    ;
    end loop;
  end if;
end;

$body$
language plpgsql volatile
cost 100;


create or replace function ttb.timetable_edit_pkg$change_action2pause (in  p_tt_variant_id numeric
)
  returns void as
$body$
declare
  l_x record;

begin
  ---находим все обеды больше 2-х часов
  --!!! КОСТЫЛЬ нужна проверка на режимы, что можно отстой давать, продолжительность и бла-бла-бла
  for l_x in (
        select
          act.tt_action_id
        from ttb.tt_out tout
          join ttb.tt_action act on act.tt_out_id = tout.tt_out_id and not act.sign_deleted
        where not tout.sign_deleted
              and tout.tt_variant_id = p_tt_variant_id
              and extract(epoch from coalesce(upper(act.action_range),act.dt_action) - act.dt_action) > 7200
              and act.action_type_id = ttb.action_type_pkg$dinner()
    ) loop

      perform ttb.timetable_edit_pkg$change_action2pause (p_tt_variant_id, l_x.tt_action_id);
   end loop;
end;

$body$
language plpgsql volatile
cost 100;
