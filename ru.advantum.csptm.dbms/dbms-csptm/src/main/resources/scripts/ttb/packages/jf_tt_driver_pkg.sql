CREATE OR REPLACE FUNCTION ttb.jf_tt_driver_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.tt_driver AS
$body$
declare 
   l_r 			ttb.tt_driver%rowtype; 
   l_sec_a  	int:= jofl.jofl_pkg$extract_number(p_attr, 'sec_a', true);
   l_sec_b  	int:= jofl.jofl_pkg$extract_number(p_attr, 'sec_b', true);
   l_dt_action 	date;
begin 
   l_r.tt_driver_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_driver_id', true); 
   l_r.dr_shift_id := jofl.jofl_pkg$extract_varchar(p_attr, 'dr_shift_id', true); 
   l_r.is_сhanged_driver := jofl.jofl_pkg$extract_boolean(p_attr, 'is_сhanged_driver', true); 
   l_r.tt_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true); 
   /*l_r.time_begin := jofl.jofl_pkg$extract_date(p_attr, 'time_begin', true); 
   l_r.time_end := jofl.jofl_pkg$extract_date(p_attr, 'time_end', true);*/ 
   l_r.driver_name := jofl.jofl_pkg$extract_varchar(p_attr, 'driver_name', true); 
   
   select  case 
             when ttv.parent_tt_variant_id is null 
               then '1900-01-01'::date 
             else lower(ttv.action_period)::date 
           end 
   into l_dt_action 
   from ttb.tt_variant ttv
   where ttv.tt_variant_id = l_r.tt_variant_id;
   
   l_r.time_begin := l_dt_action  + l_sec_a * interval '1 sec';
   l_r.time_end   := l_dt_action  + l_sec_b * interval '1 sec';
   
   if not ttb.jf_tt_variant_pkg$of_get_policy(l_r.tt_variant_id)
     then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if;    

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

------------

CREATE OR REPLACE FUNCTION ttb."jf_tt_driver_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r ttb.tt_driver%rowtype;
begin 
   l_r := ttb.jf_tt_driver_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.tt_driver where  tt_driver_id = l_r.tt_driver_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION ttb."jf_tt_driver_pkg$of_delete"(numeric, text)
  OWNER TO adv;

------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_driver_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r 				ttb.tt_driver%rowtype;
   ln_driver_num	int;
begin 
   l_r 						:= ttb.jf_tt_driver_pkg$attr_to_rowtype(p_attr);
   l_r.tt_driver_id 		:= nextval('ttb.tt_driver_tt_driver_id_seq');
   l_r.is_сhanged_driver 	:= 1;
   
   if trim(l_r.driver_name, ' ') is null then
   
      select coalesce(max(trim(replace(driver_name ,'Водитель',''))::int), 0)
      into ln_driver_num
      from ttb.tt_driver td
      where td.tt_variant_id = l_r.tt_variant_id
      and td.is_сhanged_driver;
      
   	  l_r.driver_name := 'Водитель ' || ln_driver_num + 1;
      
   end if;

   insert into ttb.tt_driver select l_r.*;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

------------

CREATE OR REPLACE FUNCTION ttb.jf_tt_driver_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 ln_tt_variant_id bigint := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
begin 
 open p_rows for 
      select 
        td.tt_driver_id, 
        td.dr_shift_id, 
        td.is_сhanged_driver, 
        td.tt_variant_id, 
        td.time_begin, 
        td.time_end,
        ds.dr_shift_name,
        ds.dr_shift_num,
        tm.mode_id,
        tm.mode_name,
        tm.mode_desc,
        td.driver_name,
        EXTRACT(hour from td.time_begin)::INTEGER*60*60 + EXTRACT(minute from td.time_begin)::INTEGER*60 as sec_a,
        EXTRACT(hour from td.time_end)::INTEGER*60*60   + EXTRACT(minute from td.time_end)::INTEGER*60   as sec_b
      from ttb.tt_driver td 
      join ttb.dr_shift ds on ds.dr_shift_id = td.dr_shift_id
      join ttb.tt_variant tv on tv.tt_variant_id = td.tt_variant_id
      join ttb.mode tm on tm.mode_id = ds.mode_id
     where td.tt_variant_id = ln_tt_variant_id
	   and td.is_сhanged_driver
     ;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

------------

CREATE OR REPLACE FUNCTION ttb.jf_tt_driver_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.tt_driver%rowtype;
begin 
   l_r := ttb.jf_tt_driver_pkg$attr_to_rowtype(p_attr);

   update ttb.tt_driver set 
          dr_shift_id = l_r.dr_shift_id, 
          is_сhanged_driver = l_r.is_сhanged_driver, 
          tt_variant_id = l_r.tt_variant_id, 
          time_begin = l_r.time_begin, 
          time_end = l_r.time_end,
          driver_name = l_r.driver_name
   where 
          tt_driver_id = l_r.tt_driver_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION ttb.jf_tt_driver_pkg$of_insert_dr_tr (
  p_id_account numeric,
  p_tt_variant_id integer,
  p_tt_tr_group_id integer
)
RETURNS text AS
$body$
declare 

begin 
    with 
    ins_tt_driver as ( 
    insert into ttb.tt_driver 
    		(dr_shift_id
            ,tt_variant_id
            ,driver_name)
      select ds.dr_shift_id
            ,tt_tr.tt_variant_id
            ,'Водитель ' || row_number() over () driver_name
      from ttb.tt_tr tt_tr
      join ttb.mode m on m.mode_id = tt_tr.tr_mode_id
      join ttb.dr_shift ds on ds.mode_id = m.mode_id
      where tt_tr.tt_variant_id = p_tt_variant_id
        and tt_tr.tt_tr_group_id = p_tt_tr_group_id
      returning *
      )
    insert into ttb.tt_driver_tr 
    		(tt_driver_id
            ,tt_tr_id)
    select tt.tt_driver_id
    	  ,tt.tt_tr_id
    from (
            with t_tr as (  
              select tt_tr.tt_tr_id
                    ,tt_tr.tt_variant_id
                    ,ds.dr_shift_id
                    ,row_number() over (order by ds.dr_shift_id) ord
            from ttb.tt_tr tt_tr
            join ttb.dr_shift ds on ds.mode_id = tt_tr.tr_mode_id 
            where tt_tr.tt_variant_id = p_tt_variant_id
              and tt_tr.tt_tr_group_id = p_tt_tr_group_id
            )
            ,t_dr as (
              select itd.tt_driver_id
                    ,itd.tt_variant_id
                    ,itd.dr_shift_id
                    ,row_number() over (order by itd.dr_shift_id) ord
            from ins_tt_driver itd
            )  
            select d.tt_driver_id
            	  ,t.tt_tr_id
            from t_tr t 
            join t_dr d on t.tt_variant_id = d.tt_variant_id
                       and t.dr_shift_id = d.dr_shift_id
                       and t.ord = d.ord
    ) tt
    ;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;