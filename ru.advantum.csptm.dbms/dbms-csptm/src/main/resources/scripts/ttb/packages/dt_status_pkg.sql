CREATE OR REPLACE FUNCTION ttb.dt_status_pkg$cn_new (
)
RETURNS smallint AS
$body$
select 1:: smallint
$body$
LANGUAGE 'sql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.dt_status_pkg$cn_process (
)
RETURNS smallint AS
$body$
select 2:: smallint
$body$
LANGUAGE 'sql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.dt_status_pkg$cn_ready (
)
RETURNS smallint AS
$body$
select 3:: smallint
$body$
LANGUAGE 'sql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.dt_status_pkg$cn_err_time_limit (
)
RETURNS smallint AS
$body$
select 4:: smallint
$body$
LANGUAGE 'sql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.dt_status_pkg$cn_err (
)
RETURNS smallint AS
$body$
select 5:: smallint
$body$
LANGUAGE 'sql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------
