CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_result_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 ln_ttv_check_condition_id ttb.ttv_check_condition.ttv_check_condition_id%type := jofl.jofl_pkg$extract_number(p_attr, 'ttv_check_condition_id', true);
begin 
 open p_rows for 
      select 
        tt_ttv_check_result_id, 
        tt_variant_id, 
        ttv_check_condition_id, 
        check_parms, 
        /*check_result,*/
        jsonb_extract_path_text(check_result, 'check_result_text') as check_result, 
        active_period
      from ttb.ttv_check_result
      where ttv_check_condition_id = ln_ttv_check_condition_id
        and upper(active_period) is null
      order by tt_ttv_check_result_id; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;