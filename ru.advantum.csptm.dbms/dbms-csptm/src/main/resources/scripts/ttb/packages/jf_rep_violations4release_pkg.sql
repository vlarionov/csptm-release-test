CREATE OR REPLACE FUNCTION ttb.jf_rep_violations4release_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare

begin

 open p_rows for 
     select
null as depo_name, /* Наименование ТП */
 null as tr_type_name, /* Вид ТС */
 null as route_num, /* Номер маршрута */
 null as out_num, /* Номер выхода */
 null as dr_shift_num, /* Номер смены */
 null as garage_num, /* Гар. номер ТС */
 null as dr_tab_num, /* Водитель */
 null as round_type, /* Тип рейса */
 null as stop_name, /* Наименование ОП */
 null as event_name, /* Наименование события */
 null as stop_time_deviation, /* Отклонение  факт. времени прохождения ОП от опер. расписания, мин */
 null as stop_time_fact, /* Факт. время прохождения ОП */
 null as stop_time_schedule /* Опер. время прохождения ОП  */	 
;
end;
$function$
;
