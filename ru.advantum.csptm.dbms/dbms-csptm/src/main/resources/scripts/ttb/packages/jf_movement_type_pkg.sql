CREATE OR REPLACE FUNCTION ttb."jf_movement_type_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$ 
declare 
begin 
 open p_rows for 
      select 
        movement_type_id, 
        movement_type_name
      from ttb.movement_type; 
end;
 $function$
;

--интервальное
create or replace function ttb.jf_movement_type_pkg$cn_type_interval()
  returns integer as
'select 1;'
language sql immutable;

--тактовое
create or replace function ttb.jf_movement_type_pkg$cn_type_clock()
  returns integer as
'select 2;'
language sql immutable;

--уличное
create or replace function ttb.jf_movement_type_pkg$cn_type_street()
  returns integer as
'select 3;'
language sql immutable;

--фиксированное количество ТС
create or replace function ttb.jf_movement_type_pkg$cn_type_trfix()
  returns integer as
'select 4;'
language sql immutable;
