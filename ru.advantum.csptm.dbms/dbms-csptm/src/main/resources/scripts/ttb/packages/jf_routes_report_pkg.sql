/*create or replace function ttb."jf_routes_report_pkg$of_rows"(p_id_account numeric, out p_rows refcursor, p_attr text)
  returns refcursor
language plpgsql
as $function$
declare
  v_attr text;
begin
  select p_attr :: jsonb || '{
    "P_REPORT_TYPE": "depo"
  }' :: jsonb
  into v_attr;
  select ttb.routes_reports_pkg$list_depo(p_id_account, v_attr)
  into p_rows;
end;
$function$;
*/

create or replace function ttb.jf_routes_report_pkg$of_rows(p_id_account numeric, out p_rows refcursor,
                                                              p_attr       text)
  returns refcursor
language plpgsql
as $function$
declare
  v_attr text;
begin
  select p_attr :: jsonb || '{
    "P_REPORT_TYPE": "date"
  }' :: jsonb
  into v_attr;
  select ttb.operational_indicator_reports_pkg$of_rows(p_id_account, v_attr)
  into p_rows;
end;
$function$;



create or replace function ttb.routes_reports_pkg$list_depo(p_id_account numeric, p_attr text)
  returns refcursor
language plpgsql
as $$
declare
  res_cursor       refcursor;
  l_report_type    text;
  l_rep_dt         text;
  l_tr_type_id     smallint;
  l_tr_type_prefix text;
begin
  l_report_type := jofl.jofl_pkg$extract_varchar(p_attr, 'P_REPORT_TYPE', true);
  l_tr_type_id := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id', true);
  l_rep_dt := jofl.jofl_pkg$extract_varchar(p_attr, 'F_REP_DT', true);
  case
    when l_tr_type_id = 3
    then l_tr_type_prefix := 'tram';
    when l_tr_type_id in (1, 2)
    then l_tr_type_prefix := 'bus';
  else l_tr_type_prefix := '';
  end case;

  /* открываем список для метода */
  if l_report_type = 'depo'
  then
    open res_cursor for
    select
      tr_tp.name               as p_filter_text,
      l_report_type            as p_report_type,
      tr_tp.tr_type_id :: text as p_tr_type_id,
      null :: text             as p_depo_id,
      null :: text             as p_territory_id,
      l_rep_dt                 as p_rep_dt,
      case
      when tr_tp.tr_type_id = 3
        then 'tram'
      when tr_tp.tr_type_id in (1, 2)
        then 'bus'
      else ''
      end                      as p_tr_type_prefix
    from core.tr_type tr_tp
    where tr_tp.tr_type_id = any (array [1, 2, 3]);
  else
    open res_cursor for
    select
      coalesce(t.territory_name, '') || coalesce(ed.name_full, ed.name_short) as p_filter_text,
      l_report_type                                                           as p_report_type,
      l_tr_type_id :: text                                                    as p_tr_type_id,
      d.depo_id                                                               as p_depo_id,
      t.territory_id                                                          as p_territory_id,
      l_rep_dt                                                                as p_rep_dt,
      l_tr_type_prefix                                                        as p_tr_type_prefix
    from core.depo d
      join core.entity ed on ed.entity_id = d.depo_id
      left join (select
                   d2t.depo_id,
                   d2t.territory_id,
                   coalesce(et.name_full, et.name_short) || ' ' as territory_name
                 from core.depo2territory d2t
                   join core.entity et on et.entity_id = d2t.territory_id
                   join core.depo dt on dt.depo_id = d2t.depo_id
                 where dt.tr_type_id = 3) t on d.depo_id = t.depo_id
    where d.tr_type_id = l_tr_type_id
          and d.sign_deleted = 0;
  end if;

  return res_cursor;
end;
$$;


create or replace function ttb.routes_report_pkg$depo_detail(P_ID_ACCOUNT text,
                                                                             p_timezone text,
                                                                             p_tr_type_id text,
                                                                             p_depo_id text,
                                                                             p_territory_id text,
                                                                             p_begin_date text,
                                                                             p_end_date text,
                                                                             p_rep_date text,
                                                                             P_REPORT_TYPE text)
  RETURNS TABLE (route_number text,
                 return_route text,
    "830_t" text,
    "830_o" text,
    "830_s" text,
    "830_m" text,
    "830_i" text,
    "12_t" text,
    "15_t" text,
    "17_t" text,
    "19_t" text,
    "21_t" text,
    "00_t" text,
    "03_t" text,
    "day_t" text,
    "day_o" text,
    "day_s" text,
    "day_m" text,
    "day_i" text,
    "routes_count" text
  ) AS
$body$
declare

begin

  return query
  select
    'XXX'::text as route_number,
    null::text as return_route,
    null::text as "830_t",
    null::text as "830_o",
    null::text as "830_s",
    null::text as "830_m",
    null::text as "830_i",
    null::text as "12_t",
    null::text as "15_t",
    null::text as "17_t",
    null::text as "19_t",
    null::text as "21_t",
    null::text as "00_t",
    null::text as "03_t",
    null::text as "day_t",
    null::text as "day_o",
    null::text as "day_s",
    null::text as "day_m",
    null::text as "day_i",
    null::text as "routes_count"
  where 1 = 0;

end;
$body$
LANGUAGE 'plpgsql';