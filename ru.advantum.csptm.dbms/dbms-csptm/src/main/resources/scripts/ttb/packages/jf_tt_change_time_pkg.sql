/*
CREATE SEQUENCE ttb.tt_change_time_id_seq
  INCREMENT 1 MINVALUE 1
  MAXVALUE 9223372036854775807 START 1
  CACHE 1;
 */
------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_change_time_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.tt_change_time AS
$body$
declare 
   l_r ttb.tt_change_time%rowtype; 
begin 
   l_r.tt_change_time_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_change_time_id', true); 
   l_r.tt_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true); 
   l_r.tr_capacity_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tr_capacity_id', true); 
   l_r.change_time := jofl.jofl_pkg$extract_number(p_attr, 'change_time', true); 
   
   if not ttb.jf_tt_variant_pkg$of_get_policy(l_r.tt_variant_id)
     then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if;     

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_change_time_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.tt_change_time%rowtype;
begin 
   l_r := ttb.jf_tt_change_time_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.tt_change_time where  tt_change_time_id = l_r.tt_change_time_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_change_time_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.tt_change_time%rowtype;
begin 
   l_r := ttb.jf_tt_change_time_pkg$attr_to_rowtype(p_attr);
   l_r.tt_change_time_id := nextval('ttb.tt_change_time_tt_change_time_id_seq');

   insert into ttb.tt_change_time select l_r.*;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_change_time_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 ln_tt_variant_id bigint := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
begin 
 open p_rows for 
      select 
        tct.tt_change_time_id, 
        tct.tt_variant_id, 
        tct.tr_capacity_id, 
        tct.change_time,
        tc.short_name,
        tc.full_name as tr_capacity_name
      from ttb.tt_change_time tct
      join core.tr_capacity tc on tct.tr_capacity_id = tc.tr_capacity_id
      where tct.tt_variant_id = ln_tt_variant_id; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_change_time_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.tt_change_time%rowtype;
begin 
   l_r := ttb.jf_tt_change_time_pkg$attr_to_rowtype(p_attr);

   update ttb.tt_change_time set 
          tt_variant_id = l_r.tt_variant_id, 
          tr_capacity_id = l_r.tr_capacity_id, 
          change_time = l_r.change_time
   where 
          tt_change_time_id = l_r.tt_change_time_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------------------------------------------