CREATE OR REPLACE FUNCTION ttb."jf_calendar_pkg$attr_to_rowtype"(p_attr text)
 RETURNS ttb.calendar
 LANGUAGE plpgsql
AS $function$

declare
   l_r ttb.calendar%rowtype;
begin
   l_r.dt := jofl.jofl_pkg$extract_date(p_attr, 'dt', true);
   l_r.calendar_id := jofl.jofl_pkg$extract_number(p_attr, 'calendar_id', true);

   return l_r;
end;

$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_calendar_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$

declare
  l_begin_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_begin_DT', true);
  l_end_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_end_DT', true);
begin
 open p_rows for
      select
        dt,
        calendar_id
      from ttb.calendar
      where
      --подключаем фильтры
        (l_begin_dt is null or calendar.dt >= l_begin_dt) and
        (l_end_dt is null or calendar.dt <= l_end_dt)
      order by dt
      ;
end;

$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_calendar_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$

declare
   l_r ttb.calendar%rowtype;
begin
   l_r := ttb.jf_calendar_pkg$attr_to_rowtype(p_attr);

   update ttb.calendar set
          dt = l_r.dt
   where
          calendar_id = l_r.calendar_id;

   return null;
end;

$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_calendar_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$

declare
   l_r ttb.calendar%rowtype;
begin
   l_r := ttb.jf_calendar_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.calendar where  calendar_id = l_r.calendar_id;

   return null;
end;

$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_calendar_pkg$of_insert_interval"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$

declare
  l_r ttb.calendar%rowtype;
  l_dt_begin timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_begin_DT', true);
  l_dt_end timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_end_DT', true);
  cur timestamp;
  l_new_calendar_id ttb.calendar.calendar_id%type;
  l_exist_calendar_id SMALLINT;
  l_numofweek double precision;
  l_year double precision;
begin
  if l_dt_begin > l_dt_end then
    RAISE EXCEPTION '<<Дата начала периода не должна превышать дату окончания!>>';
  end if;
  for cur in
  select generate_series(l_dt_begin, l_dt_end, '1 day')
  LOOP
    select cur::date - date'1970-01-01' into l_new_calendar_id;

    select count(calendar_id)
    into l_exist_calendar_id
    from ttb.calendar where calendar_id = l_new_calendar_id;
    --Если такой даты нет в календаре, добавим
    if l_exist_calendar_id = 0 then
      --Добавим дату
      insert into ttb.calendar(calendar_id, dt)
      values(l_new_calendar_id, cur::date);
      --Получим номер дня в неделе (соответствует calendar_tag_id)
      select extract(isodow from cur) into l_numofweek;
      --RAISE EXCEPTION '<<cur: %, l_new_calendar_id: %, l_numofweek: %>>', cur, l_new_calendar_id, l_numofweek;
      --Добавим детализацию даты
      insert into ttb.calendar_item(calendar_item_id, calendar_id, calendar_tag_id, is_fixed)
      values (nextval('ttb.calendar_item_calendar_item_id_seq'), l_new_calendar_id, l_numofweek, true);

      --Добавим признак будни или выходные
      if l_numofweek between 1 and 5
        then l_numofweek:= 8; --будни
        else l_numofweek:= 9; --выходные
      end if;
      insert into ttb.calendar_item(calendar_item_id, calendar_id, calendar_tag_id, is_fixed)
      values (nextval('ttb.calendar_item_calendar_item_id_seq'), l_new_calendar_id, l_numofweek, false);
    end if;
  END LOOP;

  return null;
end;

$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_calendar_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$

declare
   l_r ttb.calendar%rowtype;
   l_new_calendar_id ttb.calendar.calendar_id%type;
   l_exist_calendar_id SMALLINT;
   l_numofweek double precision;
   l_year double precision;
begin
   l_r := ttb.jf_calendar_pkg$attr_to_rowtype(p_attr);
   --добавляются только те даты, которых еще нет в календаре, остальные игнорируются
   --(id должно быть сгенерировано след образом - Количество дней с 01.01.1970)
   select l_r.dt - date'1970-01-01' into l_new_calendar_id;

   select count(calendar_id)
   into l_exist_calendar_id
   from ttb.calendar where calendar_id = l_new_calendar_id;
   --Получим номер дня в неделе
   select extract(isodow from l_r.dt) into l_numofweek;

   --RAISE EXCEPTION '<<l_new_calendar_id % l_r.dt: % l_numofweek: % >>', l_new_calendar_id, l_r.dt, l_numofweek;

   IF l_exist_calendar_id > 0 THEN
      RAISE EXCEPTION '<<Дата % уже есть в календаре!>>', to_char(l_r.dt,'dd.mm.yyyy');
   ELSE
      --Добавим дату
      insert into ttb.calendar(calendar_id, dt)
      values(l_new_calendar_id, l_r.dt);
      --Добавим детализацию даты
      insert into ttb.calendar_item(calendar_item_id, calendar_id, calendar_tag_id, is_fixed)
      values (nextval('ttb.calendar_item_calendar_item_id_seq'), l_new_calendar_id, l_numofweek, true);
      --Добавим признак будни или выходные
      if l_numofweek between 1 and 5
      then l_numofweek:= 8; --будни
      else l_numofweek:= 9; --выходные
      end if;
      insert into ttb.calendar_item(calendar_item_id, calendar_id, calendar_tag_id, is_fixed)
      values (nextval('ttb.calendar_item_calendar_item_id_seq'), l_new_calendar_id, l_numofweek, true);

   END IF;

   return null;
end;

$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_calendar_pkg$of_add_tag"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$

declare
  l_calendar_id ttb.calendar.calendar_id%type := jofl.jofl_pkg$extract_number(p_attr, 'calendar_id', true);
  l_calendar_tag_id ttb.calendar_tag.calendar_tag_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_calendar_tag_id', true);
  l_r ttb.calendar_item%rowtype;
  l_attr text;
begin
  l_r.calendar_item_id := nextval( 'ttb.calendar_item_calendar_item_id_seq' );
  l_r.calendar_id := l_calendar_id;
  l_r.calendar_tag_id := l_calendar_tag_id;
  l_r.is_fixed := false;

  l_attr:=row_to_json(l_r);

  l_attr := ttb.jf_calendar_item_pkg$of_insert(p_id_account, l_attr);

  return null;
end;

$function$
;