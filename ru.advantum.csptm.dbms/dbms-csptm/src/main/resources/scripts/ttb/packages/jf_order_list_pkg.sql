create or replace function ttb.jf_order_list_pkg$of_rows
  (p_id_account in  numeric,
   p_rows       out refcursor,
   p_attr       in  text)
  returns refcursor
as
$$
declare
  f_order_dt       ttb.order_list.order_date%type := jofl.jofl_pkg$extract_date(p_attr, 'ORDER_DT', true);
  f_route_id      rts.route.route_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', true);
  f_garage_num     core.tr.garage_num%type := jofl.jofl_pkg$extract_number(p_attr, 'f_garage_num', true);
  f_list_depo_id   smallint [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', true);
  f_list_driver_id bigint [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_driver_id', true);
  l_incident_id bigint := jofl.jofl_pkg$extract_number(p_attr, 'incident_id', true)::bigint;
  l_time_start  timestamp := jofl.jofl_pkg$extract_date(p_attr,'time_start', true);
  l_orders_ids bigint[];

begin

  if l_incident_id is not null then
    select array_agg(a)
    into l_orders_ids
    from oud.jf_order_list_violation_pkg$get_order_list_ids(l_incident_id, l_time_start) a;
  end if;

  open p_rows for
  select
    ol.order_date,
    ol.time_from,
    ol.time_to,
    tr.garage_num                            tr_garage_num,
    tr_type_name                             tr_type,
    en.name_full                             depo_name,
    r.route_num,
    ol.out_num,
    ds.dr_shift_num,
    dr.tab_num                               dr_tab_num,
    (coalesce(dr.driver_last_name, '') || ' '
     || coalesce(dr.driver_name, '') || ' '
     || coalesce(driver_middle_name, '')) as dr_full_name
     ,tr_capacity_short_name as tr_capacity_full_name
    ,(
        select short_name
        from ttb.v_tt_out_oper
        join core.tr_capacity on v_tt_out_oper.tr_capacity_id = tr_capacity.tr_capacity_id
        where
                v_tt_out_oper.route_variant_id = ol.route_variant_id
                and v_tt_out_oper.order_date = ol.order_date
                and v_tt_out_oper.tt_out_num = ol.out_num limit 1
     ) as  tr_capacity_full_name_tt_oper
     ,groups.create_time
  from ttb.order_list ol
    left join ttb.dr_shift ds on ol.dr_shift_id = ds.dr_shift_id
    join rts.route_variant rv on ol.route_variant_id = rv.route_variant_id
    join rts.route r on rv.route_id = r.route_id
    join core.driver dr on ol.driver_id = dr.driver_id
    join core.depo dp on ol.depo_id = dp.depo_id
    join core.entity en on dp.depo_id = en.entity_id
    join core.v_tr_ref tr on ol.tr_id = tr.tr_id
    join easu.groups on groups.group_id = ol.group_id
  where (f_order_dt = ol.order_date or array_length(l_orders_ids, 1) is not null)
        and (array_length(f_list_depo_id, 1) is null or ol.depo_id = any (f_list_depo_id))
        and (f_route_id = r.route_id or f_route_id is null )
        and (f_garage_num = tr.garage_num or f_garage_num is null or
             f_garage_num = 0)
        and (array_length(f_list_driver_id, 1) is null or
             dr.driver_id = any (f_list_driver_id))
        --incidents
        and
        (ol.order_list_id = any(l_orders_ids) or array_length(l_orders_ids, 1) is null)
  order by order_date, route_num, time_from;
end;
$$
language plpgsql;
