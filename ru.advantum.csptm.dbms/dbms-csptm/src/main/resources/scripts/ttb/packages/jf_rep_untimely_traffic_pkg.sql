CREATE OR REPLACE FUNCTION ttb.jf_rep_untimely_traffic_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare

begin

 open p_rows for 
     select
null as depo_name, /* Наименование ТП */
 null as tr_type_name, /* Вид ТС */
 null as route_num, /* Номер маршрута */
 null as deviation_reason, /* Причина отклонений */
 null as open_dt_deviation, /* Отклонение открытия/ закрытия, мин */
 null as open_dt_fact, /* Время открытия/ закрытия факт. */
 null as open_dt_plan, /* Время открытия/ закрытия план. */
 null as rdate, /* Дата */
 null as take_action /* Принятые меры */	 
;
end;
$function$
;
