CREATE OR REPLACE FUNCTION ttb."jf_norm_round_filter_from_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare
  l_norm_id ttb.norm.norm_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true);

begin
  open p_rows for
  WITH t1 AS (select distinct on (nr.hour_from) to_char(nr.hour_from, 'HH24:MI') time_from
  from ttb.norm_round nr
  WHERE norm_id = l_norm_id )
  select * from t1 Order by time_from;
end;
$function$;