CREATE OR REPLACE FUNCTION ttb."jf_spr_change_time_pkg$attr_to_rowtype"(p_attr text)
 RETURNS ttb.spr_change_time
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.spr_change_time%rowtype; 
begin 
   l_r.spr_change_time_id := jofl.jofl_pkg$extract_varchar(p_attr, 'spr_change_time_id', true); 
   l_r.change_time := jofl.jofl_pkg$extract_number(p_attr, 'change_time', true); 
   l_r.tr_capacity_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tr_capacity_id', true); 
   l_r.tr_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tr_type_id', true);
  IF (l_r.change_time<0) THEN
    RAISE EXCEPTION '<<Время на пересменку не должно быть отрицательным!>>';
  END IF;
   return l_r;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_spr_change_time_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$ 
declare 
begin 
 open p_rows for
 select
   spr.spr_change_time_id,
   spr.change_time,
   spr.tr_capacity_id,
   spr.tr_type_id,
   cap.short_name tr_capacity_short_name,
   tt.name tr_type_name
 from ttb.spr_change_time spr
   join core.tr_capacity  cap on cap.tr_capacity_id = spr.tr_capacity_id
   join core.tr_type tt on tt.tr_type_id =spr.tr_type_id;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_spr_change_time_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.spr_change_time%rowtype;
begin 
   l_r := ttb.jf_spr_change_time_pkg$attr_to_rowtype(p_attr);
   update ttb.spr_change_time set 
          change_time = l_r.change_time, 
          tr_capacity_id = l_r.tr_capacity_id, 
          tr_type_id = l_r.tr_type_id
   where 
          spr_change_time_id = l_r.spr_change_time_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_spr_change_time_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.spr_change_time%rowtype;
begin 
   l_r := ttb.jf_spr_change_time_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.spr_change_time where  spr_change_time_id = l_r.spr_change_time_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_spr_change_time_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.spr_change_time%rowtype;
begin 
   l_r := ttb.jf_spr_change_time_pkg$attr_to_rowtype(p_attr);
  l_r.spr_change_time_id:=   nextval('ttb.spr_change_time_spr_change_time_id_seq');

   insert into ttb.spr_change_time select l_r.*;
   return null;



  exception
  when UNIQUE_VIOLATION then
    raise exception '<<Для указанного вида ТС и  вместимости  время на пересменку уже задано>>';
    return null;


end;
 $function$
;