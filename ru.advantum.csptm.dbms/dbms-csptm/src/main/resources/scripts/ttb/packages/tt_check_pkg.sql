--DROP FUNCTION ttb.tt_check_pkg$get_set4mode (
--p_tt_variant_id integer
--);

CREATE OR REPLACE FUNCTION ttb.tt_check_pkg$get_set4mode (
  p_tt_variant_id integer
)
RETURNS TABLE (
  tt_out_id integer,
  tt_out_num smallint,
  action_dur integer,
  shift_work_dur bigint,
  before_time bigint,
  after_time bigint,
  work_break_between bigint,
  is_check_break_num bigint,
  break_cnt bigint,
  break_dur bigint,
  action_type_id smallint,
  action_type_name text,
  is_check_break boolean,
  ds_shift_num smallint,
  dr_shift_name text,
  break_duration_max integer,
  stop_time integer,
  work_duration_min integer,
  work_duration_max integer,
  work_break_between_min integer,
  work_break_between_max integer,
  before_time_min integer,
  before_time_max integer,
  break_count_min smallint,
  break_count_max smallint,
  after_time_max integer,
  before_time_short_break_max integer,
  drm_action_type_id smallint,
  drm_time_min integer,
  drm_time_max integer,
  dr_shift_id smallint
) AS
$body$
begin
  RETURN QUERY
   with tsub_checks as (
		select --distinct
              qq.tt_out_id
             ,qq.tt_out_num
             ,qq.agg_time_begin as time_begin
             ,qq.agg_time_end as time_end
             ,qq.min_bn_int
             ,qq.max_bn_int
             ,extract (epoch from (qq.agg_time_end - qq.agg_time_begin)) as action_dur
             ,sum(case 
                        when not qq.is_break
                             or qq.is_bn_round_break
                        then case when not coalesce(td.is_сhanged_driver, false) 
                        			then extract (epoch from (qq.agg_time_end - qq.agg_time_begin)) 
                        		    else 0
                             end
                        else
                             case when not coalesce(td.is_сhanged_driver, false)  
                              then
                                 case 
                                   when extract (epoch from (qq.agg_time_end - qq.agg_time_begin)) > asd.get_constant('TIME_BREAK_IN_SHIFT') 
                                   then 0 /*asd.get_constant('TIME_BREAK_IN_SHIFT')*/ 
                                   else extract (epoch from (qq.agg_time_end - qq.agg_time_begin))
                                 end
                              else 0
                             end
                  end) over (partition by qq.tt_out_id, ds.dr_shift_num) shift_work_dur
             ,sum(case 
                        when not qq.is_break 
                             or qq.is_bn_round_break 
                        then case when not coalesce(td.is_сhanged_driver, false)  
                                	then extract (epoch from (qq.agg_time_end - qq.agg_time_begin)) 
                                    else 0
                             end
                        else 
                        	 case when not coalesce(td.is_сhanged_driver, false)  
                              then
                                 case 
                                   when extract (epoch from (qq.agg_time_end - qq.agg_time_begin)) > asd.get_constant('TIME_BREAK_IN_SHIFT') 
                                   then 0 /*asd.get_constant('TIME_BREAK_IN_SHIFT')*/ 
                                   else extract (epoch from (qq.agg_time_end - qq.agg_time_begin))
                                 end
                              else 0
                             end
                  end) over (partition by qq.tt_out_id, ds.dr_shift_num order by qq.agg_time_begin) shift_work_order_dur  
             ,qq.action_type_id
             ,qq.action_type_code
             ,qq.action_type_name
             ,qq.move_direction_id
             ,qq.move_direction_name
             ,qq.r_code
             ,qq.is_production_round
             ,qq.is_technical_round
             ,qq.is_bn_round_break
             ,qq.is_dinner
             ,qq.is_pause
             ,(qq.is_dinner or qq.is_pause) as is_check_break
             ,qq.stop_b
             ,qq.stop_e
             ,qq.stop_id_b
             ,qq.stop_id_e
             ,qq.tt_action_id
             ,qq.tt_variant_id
             ,qq.tt_action_group_code
             ,qq.tt_action_group_nn
             ,ds.dr_shift_num  as ds_shift_num          
             ,ds.*
             ,drm.action_type_id as drm_action_type_id
             ,drm.time_min as drm_time_min
             ,drm.time_max as drm_time_max
             ,(select count(distinct drm.action_type_id) 
                 from ttb.dr_mode drm 
                where drm.dr_shift_id = qq.dr_shift_id 
                  and drm.action_type_id = any(array[ttb.action_type_pkg$dinner(), ttb.action_type_pkg$pause()])
              ) drm_dinner_pause
              ,coalesce(td.is_сhanged_driver, false) as is_сhanged_driver
              ,case 
                 when lead(coalesce(td.is_сhanged_driver, false), 1, coalesce(td.is_сhanged_driver, false)) over (partition by qq.tt_out_id, ds.dr_shift_num order by qq.agg_time_begin) =
                  	  coalesce(td.is_сhanged_driver, false) and coalesce(td.is_сhanged_driver, false)
                 then true
                 else false
               end sog_change_driver
               ,case 
                 when not coalesce(td.is_сhanged_driver, false) then (qq.is_dinner or qq.is_pause)
                 else coalesce(td.is_сhanged_driver, false) and case 
                                                 when lag(coalesce(td.is_сhanged_driver, false), 1, coalesce(td.is_сhanged_driver, false)) 
                                                     over (partition by qq.tt_out_id, ds.dr_shift_num order by qq.agg_time_begin) is false
                                                      and coalesce(td.is_сhanged_driver, false)                  
                                                 then true
                                                 else false
                                               end
                 							and (qq.is_dinner or qq.is_pause)
               end is_check_break_anal
           from ttb.tt_out_pkg$get_set4out_action_agg (  p_tt_variant_id )qq
             join ttb.dr_shift ds on qq.dr_shift_id = ds.dr_shift_id
             left join ttb.tt_driver_action tda on tda.tt_action_id = qq.tt_action_id
             left join ttb.tt_driver td on tda.tt_driver_id = td.tt_driver_id 
             					  --and not td.is_сhanged_driver
             left join ttb.dr_mode drm on drm.action_type_id = qq.action_type_id
                                      and drm.dr_shift_id = qq.dr_shift_id
           order by qq.tt_out_id, qq.agg_time_begin
           )
       ,t_checks as (
           select  
		      tc.tt_out_id
             ,tc.tt_out_num
             ,tc.time_begin
             ,tc.time_end
             ,tc.action_dur
             ,tc.shift_work_dur
             ,tc.shift_work_order_dur
             ,case 
             	when tc.is_check_break_anal then lag(tc.shift_work_order_dur) over (partition by tc.tt_out_num, tc.ds_shift_num order by tc.time_begin)
                else null 
              end before_time
             ,case 
             	when tc.is_check_break_anal then 
                tc.shift_work_dur -
                /*tc.action_dur -*/
                  case 
             		when tc.is_check_break_anal then lag(tc.shift_work_order_dur) over (partition by tc.tt_out_num, tc.ds_shift_num order by tc.time_begin)
                	else null 
              	  end
                else null 
              end as after_time
             ,case 
               when tc.is_check_break_anal 
               then row_number() over (partition by tc.tt_out_num, tc.ds_shift_num, tc.is_check_break_anal order by tc.time_begin) 
               else null
              end is_check_break_num
             ,sum(case when tc.is_check_break_anal then 1 else 0 end) over (partition by tc.tt_out_num, tc.ds_shift_num) break_cnt
             ,sum(case when tc.is_check_break_anal then tc.action_dur else 0 end) over (partition by tc.tt_out_num, tc.ds_shift_num) break_dur
             ,tc.action_type_id
             ,tc.action_type_name
             ,tc.is_check_break
             ,tc.ds_shift_num             
             ,tc.dr_shift_name
             ,tc.break_count_max
             ,tc.break_duration_max
             ,tc.stop_time
             ,tc.work_duration_min
             ,tc.work_duration_max
             ,tc.work_break_between_min
             ,tc.work_break_between_max
             ,tc.before_time_min
             ,tc.before_time_max
             ,tc.break_count_min
             ,tc.after_time_max
             ,tc.before_time_short_break_max
             ,tc.drm_action_type_id
			 ,tc.drm_time_min
             ,tc.drm_time_max
             ,tc.dr_shift_id
             ,tc.drm_dinner_pause
             ,tc.is_сhanged_driver
             ,tc.sog_change_driver
             ,tc.is_dinner
             ,tc.is_pause   
             ,tc.is_check_break_anal         
           from tsub_checks tc
           order by tc.tt_out_num, tc.ds_shift_num, tc.time_begin)
       ,checks as (
           select 
			  cc.tt_out_id
           	 ,cc.tt_out_num
             ,cc.time_begin
             ,cc.time_end
             ,cc.action_dur
             ,cc.shift_work_dur
             ,cc.shift_work_order_dur
             ,cc.before_time
             ,cc.after_time
             ,case 
             	when cc.is_check_break_num > 1 
                then cc.before_time -
                	 lag(cc.before_time) over (partition by cc.tt_out_num, cc.ds_shift_num, cc.is_check_break_anal order by cc.time_begin)/* -
                     lag(cc.action_dur) over (partition by cc.tt_out_num, cc.ds_shift_num, cc.is_check_break order by cc.time_begin)*/
                else null
              end work_break_between
             ,cc.is_check_break_num
             ,cc.break_cnt             
             ,cc.break_dur
             ,cc.action_type_id
             ,cc.action_type_name
             ,case 
             	when cc.is_check_break then cc.is_check_break
                else 
                  case 
                    when (cc.shift_work_dur > cc.work_duration_max or cc.shift_work_dur < cc.work_duration_min) then false
                	else
                		(cc.break_cnt > cc.break_count_max or cc.break_cnt < cc.break_count_min)
                  end
              end is_check_break
             ,cc.ds_shift_num             
             ,cc.dr_shift_name             
             ,cc.break_duration_max
             ,cc.stop_time
             ,cc.work_duration_min
             ,cc.work_duration_max
             ,cc.work_break_between_min
             ,cc.work_break_between_max
             ,cc.before_time_min
             ,cc.before_time_max
             ,cc.break_count_min
             ,cc.break_count_max
             ,cc.after_time_max
             ,cc.before_time_short_break_max
             ,cc.drm_action_type_id
			 ,cc.drm_time_min
             ,cc.drm_time_max
             ,cc.dr_shift_id
             ,cc.drm_dinner_pause
             ,cc.is_сhanged_driver
             ,cc.sog_change_driver
             ,cc.is_dinner
             ,cc.is_pause
             ,cc.is_check_break_anal
             ,sum((cc.is_check_break_anal and cc.is_check_break)::int) over (partition by cc.tt_out_num, cc.ds_shift_num) break_cnt_anal
           from t_checks cc
      	  where 1 = 1 /*cc.is_check_break*/ )
          select distinct
		      c.tt_out_id
             ,c.tt_out_num
             --,c.time_begin
             --,c.time_end
             ,case 
             	when c.is_check_break then c.action_dur::integer
                else null
              end action_dur
             ,c.shift_work_dur::bigint
             --,c.shift_work_order_dur
             ,c.before_time::bigint
             ,c.after_time::bigint
             ,c.work_break_between::bigint
             ,c.is_check_break_num
             --,c.break_cnt 
             ,c.break_cnt_anal as break_cnt         
             ,c.break_dur::bigint
             ,case 
             	when c.is_check_break then c.action_type_id
                else null
              end action_type_id
             ,case 
             	when c.is_check_break then c.action_type_name
                else null
              end action_type_name             
             ,c.is_check_break
             ,c.ds_shift_num             
             ,c.dr_shift_name             
             ,c.break_duration_max
             ,c.stop_time
             ,c.work_duration_min
             ,c.work_duration_max
             ,c.work_break_between_min
             ,c.work_break_between_max
             ,c.before_time_min
             ,c.before_time_max
             ,c.break_count_min
             ,c.break_count_max
             ,c.after_time_max
             ,c.before_time_short_break_max
             ,c.drm_action_type_id
			 ,c.drm_time_min
             ,c.drm_time_max
             ,c.dr_shift_id
             --,c.is_check_break_anal             
          from checks c
          where 1 = 1
             and (not c.is_check_break and (c.shift_work_dur > c.work_duration_max or c.shift_work_dur < c.work_duration_min)) --время работы смены общее
              or (c.is_check_break and c.is_check_break_anal 
              						and ((c.break_cnt_anal > c.break_count_max or c.break_cnt_anal < c.break_count_min) --кол-во перерывов
              						or ( c.break_dur > c.break_duration_max) --прод-ть перерывов
              						or ((c.before_time > c.before_time_max /*or c.before_time < c.before_time_min*/) and c.is_check_break_num = 1) --время работы до первого перерыва
           	  						or ( c.after_time > c.after_time_max and c.is_check_break_num = c.break_cnt_anal) --время работы после последнего перерыва до окончания смены
           	  						or ( c.is_check_break_num > 1 and c.work_break_between > 0 and (c.work_break_between > c.work_break_between_max or c.work_break_between < c.work_break_between_min)) --время работы между перерывами
              						or ( c.action_type_id = c.drm_action_type_id and c.drm_dinner_pause = 1 and 
                                    	 c.action_type_id = any(array[ttb.action_type_pkg$dinner(), ttb.action_type_pkg$pause()]) and
                                    	(c.action_dur > c.drm_time_max or c.action_dur < c.drm_time_min)) --длительность пер-в по типам для обеда и отстоя. Если в ТиО есть и обед и отстой, то не считаем.
                                    or ( c.action_type_id = c.drm_action_type_id and 
                                    	 c.action_type_id not in (ttb.action_type_pkg$dinner(), ttb.action_type_pkg$pause()) and 
                                    	(c.action_dur > c.drm_time_max or c.action_dur < c.drm_time_min)) --длительность пер-в по типам кроме обеда и отстоя.
                                        ) 
                 )
          order by c.tt_out_num,
          		   c.ds_shift_num	
           ;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 100;
-----------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.tt_check_pkg$get_set4intervals (
  p_tt_variant_id integer
)
RETURNS TABLE (
  tt_variant_id integer,
  allow_dev_prc integer,
  avg_fact_interval numeric,
  parm_interval integer,
  dev_prc_all numeric
) AS
$body$
begin
  RETURN QUERY
	with perds as (
               select hr.hr as hr_set
                        from generate_series
                        ('01.01.1900'::date + '00:00:00'::interval
                        ,'01.01.1900'::date + '23:00:00'::interval
                        ,'1 hour'::interval) hr
       )
       ,fact as (
               select
                      p.hr_set,
                      count(distinct(qq.tt_out_num)) out_cnt    
                   from perds p 
                   join ttb.tt_out_pkg$get_set4out_action_agg (  p_tt_variant_id )qq on 1 = 1 and qq.r_code = '00'
                   where  ((qq.agg_time_begin >= p.hr_set and qq.agg_time_end < p.hr_set + make_interval(hours => 1))
                            or (tsrange(qq.agg_time_begin, qq.agg_time_end) @> p.hr_set)
                            or (tsrange(qq.agg_time_begin, qq.agg_time_end) @> p.hr_set + make_interval(hours => 1) - make_interval(secs => 1))
                          )                   
                   group by p.hr_set
       )
       ,ttv_data as (
    			 select  ttv.tt_variant_id
                        ,ttv.norm_id
                        ,ttv.route_variant_id
                        ,tp.*
                        ,tsrange(tp.time_begin, tp.time_end) ttv_per
                 from ttb.tt_variant ttv 
                 join ttb.tt_schedule sch on sch.tt_variant_id = ttv.tt_variant_id
                 join ttb.tt_period tp on tp.tt_schedule_id = sch.tt_schedule_id
                 					  and tp.movement_type_id <> ttb.jf_movement_type_pkg$cn_type_street () /*уличное*/
                 where ttv.tt_variant_id = p_tt_variant_id
       )
      ,perds_dur as (
                 select  ttb.over_round_duration (
                                                     ttv_data.norm_id,
                                                     ttv_data.route_variant_id,
                                                     perds.hr_set::time without time zone,
                                                     null::bigint /* p_tr_capacity */
                                                   ) t,
                        perds.*,
                        ttv_data.*
                 from perds join ttv_data on ttv_data.ttv_per  @>  perds.hr_set::timestamp
       )
      ,perds_set as (
                 select p.hr_set				
                      , case when p.interval_time = 0 then 1 else p.interval_time end interval_time
                      , p.limit_time
                      , p.tt_variant_id
                      ,(t).p_first_stop_item 	as first_stop_item
                      ,(t).p_last_stop_item	as last_stop_item
                      ,(t).p_duration			as duration
                      
                from perds_dur p)
      ,t_res as (
                select p.tt_variant_id
                      ,p.hr_set 		
                      ,p.interval_time
                      ,p.limit_time
                      ,p.duration
                      ,f.out_cnt
                      ,(p.duration / f.out_cnt) * 60 as fact_interval
                      ,(((p.duration / f.out_cnt) * 60) - p.interval_time ) * 100 / p.interval_time
                       as dev_prc
                      ,tcp.parm_value as allow_dev_prc
                      ,tcpi.parm_value parm_interval
                      ,avg(p.duration) over () avg_dur
                      ,max(f.out_cnt) over () max_out_cnt
                from fact f 
                join perds_set p on p.hr_set = f.hr_set
                join ttb.ttv_check_condition tcc on tcc.tt_variant_id = p.tt_variant_id
                join ttb.ttv_check_parm tcp on tcp.ttv_check_condition_id = tcc.ttv_check_condition_id
                join ttb.tt_check_parm cpp on cpp.tt_check_parm_id = tcp.tt_check_parm_id
                                         and cpp.parm_tag = 'INT_DEV_PRC' /*Допустимое отклонение интервалов в процентах*/  
                join ttb.ttv_check_parm tcpi on tcpi.ttv_check_condition_id = tcc.ttv_check_condition_id
                join ttb.tt_check_parm cppi on cppi.tt_check_parm_id = tcpi.tt_check_parm_id
                                         and cppi.parm_tag = 'INT_INTERVAL' /*Интервал*/ 
                join ttb.ttv_check_parm tcpf on tcpf.ttv_check_condition_id = tcc.ttv_check_condition_id  
                join ttb.tt_check_parm cpf on cpf.tt_check_parm_id = tcpf.tt_check_parm_id  
                                           and cpf.parm_tag = 'INT_INT_BGN' /*Начало периода*/    
                join ttb.ttv_check_parm tcpt on tcpt.ttv_check_condition_id = tcc.ttv_check_condition_id  
                join ttb.tt_check_parm cpt on cpt.tt_check_parm_id = tcpt.tt_check_parm_id  
                                           and cpt.parm_tag = 'INT_INT_END' /*Окончание периода*/  
                where extract(epoch from p.hr_set::time) between tcpf.parm_value and tcpt.parm_value                  
                )
      ,t_res_all as 
            (select res.tt_variant_id
                  ,res.hr_set
                  ,res.interval_time
                  ,res.limit_time
                  ,res.duration
                  ,res.out_cnt
                  ,res.fact_interval
                  ,res.dev_prc
                  ,res.allow_dev_prc
                  ,res.avg_dur
                  ,res.max_out_cnt
                  ,round(res.avg_dur / res.max_out_cnt) as avg_fact_interval
                  ,res.parm_interval
                  ,round(abs((((res.avg_dur / res.max_out_cnt)) - res.parm_interval) * 100 / res.parm_interval), 2) dev_prc_all
            from t_res res)
      select distinct
      		  tra.tt_variant_id
      		 ,tra.allow_dev_prc
             ,tra.avg_fact_interval
             ,tra.parm_interval
             ,tra.dev_prc_all
      from t_res_all tra
      where tra.dev_prc_all > tra.allow_dev_prc
      ;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;
------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.tt_check_pkg$get_set4peakhours (
  p_tt_variant_id integer
)
RETURNS TABLE (
  tt_variant_id integer,
  upper_ph_per timestamp,
  ph_per pg_catalog.tsrange,
  out_cnt_plan bigint,
  out_cnt_fact bigint
) AS
$body$
begin
  RETURN QUERY
	with peak_hours as (
          select tph.tt_peak_hour_id
                ,tph.tt_variant_id
                ,tph.time_begin
                ,tph.time_end
                ,tph.peak_hour_name
                ,tsrange(tph.time_begin, tph.time_end) ph_per
          from ttb.tt_peak_hour tph 
          )
      ,perds as (
            select  ttv.tt_variant_id
                   ,ttv.norm_id
                   ,ttv.route_variant_id
                   ,tp.*
                   ,sum(tp.out_cnt) over (partition by ttv.tt_variant_id order by tp.time_begin) sum_out_cnt
                   ,tsrange(tp.time_begin, tp.time_end) ttv_per
            from ttb.tt_variant ttv 
            join ttb.tt_schedule sch on sch.tt_variant_id = ttv.tt_variant_id
            join ttb.tt_period tp on tp.tt_schedule_id = sch.tt_schedule_id
            )
      ,php as (
           select ph.tt_variant_id
                 ,ph.ph_per
                 ,p.ttv_per
                 ,p.sum_out_cnt
                 ,max(p.time_begin) over (partition by ph.tt_peak_hour_id) max_ttv_time_begin
            from peak_hours ph
            join perds p on ph.tt_variant_id = p.tt_variant_id
            where p.tt_variant_id = p_tt_variant_id
              and ph.ph_per && p.ttv_per
            )
      ,plan as (
            select php.tt_variant_id
                  ,php.ph_per
                  ,php.ttv_per
                  ,php.sum_out_cnt
                  ,php.max_ttv_time_begin
            from php 
            where lower(php.ttv_per) = php.max_ttv_time_begin
          )
       select p.tt_variant_id,
              upper(p.ph_per) upper_ph_per,
              p.ph_per,
              p.sum_out_cnt out_cnt_plan,
              count(distinct(qq.tt_out_num)) out_cnt_fact   
           from plan p 
           join ttb.tt_out_pkg$get_set4out_action_agg (  p_tt_variant_id ) qq on 1 = 1 
           where qq.r_code = '00'
           	 and   ((qq.agg_time_begin >= upper(p.ph_per) and qq.agg_time_end < upper(p.ph_per) + make_interval(hours => 1))
                    or (tsrange(qq.agg_time_begin, qq.agg_time_end) @> upper(p.ph_per))
                    or (tsrange(qq.agg_time_begin, qq.agg_time_end) @> upper(p.ph_per) + make_interval(hours => 1) - make_interval(secs => 1))
                    )
           group by  p.tt_variant_id
           			,upper(p.ph_per)
           			,p.ph_per
           		    ,sum_out_cnt;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;
----------------------------------------------
--drop FUNCTION ttb.tt_check_pkg$get_set4doable (
--  p_tt_variant_id integer
--);


CREATE OR REPLACE FUNCTION ttb.tt_check_pkg$get_set4doable (
  p_tt_variant_id integer
)
RETURNS TABLE (
  tt_variant_id 	integer,
  tt_out_num 		smallint,
  dr_shift_num 		smallint,
  stop_name 		text,
  prev_stop_name 	text,
  btn_stop_dur 		interval,
  btn_stop_dur_plan	interval
) AS

$body$
begin
  RETURN QUERY
	 with q as (
       select distinct
                   out.tt_out_id
                  ,out.tt_out_num
                  ,r.move_direction_id
                  ,ta.tt_action_id
                  ,at.action_type_id
                  ,si2r.order_num
                  ,s.name stop_name
                  ,lag(s.name, 1, s.name) over (partition by out.tt_out_id, ta.tt_action_id order by r.round_id, out.tt_out_id, tai.time_begin) prev_stop_name
                  ,tai.time_begin
                  ,lag(tai.time_begin, 1, tai.time_begin) over (partition by out.tt_out_id, ta.tt_action_id order by r.round_id, out.tt_out_id, tai.time_begin) prev_time_begin
                  ,tai.time_end
                  ,si2r.length_sector len
                  ,r.round_id
                  ,s.stop_id
                  ,ds.dr_shift_num
                  ,out.tt_variant_id
          from ttb.tt_out out 
          join ttb.tt_action ta on ta.tt_out_id = out.tt_out_id
          join ttb.action_type at on at.action_type_id = ta.action_type_id
          join ttb.tt_action_item tai on tai.tt_action_id = ta.tt_action_id
          join ttb.tt_action_round tar on tar.tt_action_id = ta.tt_action_id
          join rts.round r on r.round_id = tar.round_id
                           and r.action_type_id = at.action_type_id
          join rts.move_direction md on md.move_direction_id = r.move_direction_id
          join rts.stop_item2round si2r on si2r.round_id = r.round_id 
                                        and si2r.stop_item2round_id = tai.stop_item2round_id                                  
          join rts.stop_item2round_type si2rt on si2rt.stop_item2round_id = si2r.stop_item2round_id
          join rts.stop_item si on si.stop_item_id = si2r.stop_item_id
          join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
          join rts.stop s on s.stop_id = sl.stop_id
          join ttb.dr_shift ds on ds.dr_shift_id = tai.dr_shift_id
          where out.tt_variant_id = p_tt_variant_id
          and r.code = '00'
          )
    ,qq as (
          select q.tt_variant_id
          		,q.tt_out_num
                ,q.dr_shift_num
                ,q.stop_name
                ,q.prev_stop_name
                ,q.order_num
                ,min(q.order_num) over (partition by q.tt_out_num, q.tt_action_id) as min_ord_num
                ,max(q.order_num) over (partition by q.tt_out_num, q.tt_action_id) as max_ord_num
                ,ttb.jf_tt_variant4check_pkg$get_rnd_val( min(q.order_num) over (partition by q.tt_out_num, q.tt_action_id),
                										  max(q.order_num) over (partition by q.tt_out_num, q.tt_action_id) ) as rnd_ord_num
                ,(q.time_begin - q.prev_time_begin)::time::interval as btn_stop_dur 
          from q
          where q.stop_name <> q.prev_stop_name
          )
     select qq.tt_variant_id
           ,qq.tt_out_num
           ,qq.dr_shift_num
           ,qq.stop_name
           ,qq.prev_stop_name
           --,qq.order_num
           --,qq.min_ord_num
           --,qq.max_ord_num
           --,qq.rnd_ord_num
           ,qq.btn_stop_dur
           ,qq.btn_stop_dur + make_interval(mins => ttb.jf_tt_variant4check_pkg$get_rnd_val(1, 10)::integer)  as btn_stop_dur_plan   
    from qq
    where qq.order_num = qq.rnd_ord_num
    order by ttb.jf_tt_variant4check_pkg$get_rnd_val(1, 9)
    limit ttb.jf_tt_variant4check_pkg$get_rnd_val(1, 7);
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;
----------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.tt_check_pkg$get_set4action_date (
  p_tt_variant_id integer
)
RETURNS TABLE (
  tt_variant_id integer,
  ttv_name_other text,
  tt_variant_id_other integer,
  dt_err date
) AS
$body$
begin
RETURN QUERY
 with 
       curr_tv as (    
       select  tv.tt_variant_id as tt_variant_id_curr
              ,tv.route_variant_id
              ,tt_status_id
              ,tv.action_period
              ,tv.ttv_name as ttv_name_curr
              ,rv.route_id
          from ttb.tt_variant tv
          join rts.route_variant rv on rv.route_variant_id = tv.route_variant_id
         where tv.tt_variant_id = p_tt_variant_id
           and not tv.sign_deleted
          )
      ,other_tv as (
       select  tv.tt_variant_id as tt_variant_id_other
              ,tv.route_variant_id
              ,tt_status_id
              ,tv.action_period
              ,tv.ttv_name as ttv_name_other
              ,rv.route_id 
          from ttb.tt_variant tv
          join rts.route_variant rv on rv.route_variant_id = tv.route_variant_id
          where rv.route_variant_id = (select route_variant_id from curr_tv limit 1)/*p_route_variant_id*/
            and tv.tt_variant_id != p_tt_variant_id
            and not tv.sign_deleted
            and tv.tt_status_id in (ttb.tt_status_pkg$agreed()
                                   ,ttb.tt_status_pkg$active() )
            and tv.parent_tt_variant_id is null
            )
       ,intersects_ttv as (
        select c.tt_variant_id_curr
              ,c.route_variant_id
              ,c.action_period action_period_curr
              ,c.ttv_name_curr
              ,c.route_id
              ,o.tt_variant_id_other
              ,o.route_variant_id
              ,o.action_period action_period_curr
              ,o.ttv_name_other
              ,c.action_period * o.action_period as action_period_intersect
              ,tstzrange(min(lower(c.action_period)) over (),
                         max(upper(o.action_period)) over ()) as action_period_union             
          from curr_tv c
          join other_tv o on 1 = 1 
          where c.action_period && o.action_period
          )
       ,t_series as (
        select date_trunc('day', dd)::date as dt_set
          from generate_series
          ( (select min(lower(action_period_union)) from intersects_ttv)
          , (select coalesce(max((upper(action_period_union))), now() + make_interval(years => 20)) from intersects_ttv)
          , '1 day'::interval) dd
        )
      ,t_calendar as (
       select *
         from ttb.calendar c 
         join ttb.calendar_item ci on ci.calendar_id = c.calendar_id
         join ttb.tt_calendar tc on tc.calendar_tag_id = ci.calendar_tag_id    
       )
      ,result as (       
       select its.ttv_name_other
       		 ,its.tt_variant_id_other
             ,ser.dt_set as dt_err 
       from t_series ser
       join intersects_ttv its on its.action_period_intersect @> ser.dt_set::timestamp
       join t_calendar tc on its.action_period_intersect @> tc.dt::timestamp   					 
                         and ser.dt_set = tc.dt	
                         and tc.tt_variant_id = its.tt_variant_id_other
       join t_calendar tcc on its.action_period_intersect @> tcc.dt::timestamp   					 
                         and ser.dt_set = tcc.dt	
                         and tcc.tt_variant_id = its.tt_variant_id_curr 
	   order by ser.dt_set asc
       limit 1
       ) 
            select p_tt_variant_id as tt_variant_id
              		,r.ttv_name_other
                    ,r.tt_variant_id_other
                    ,r.dt_err
                from result r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;
---------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.tt_check_pkg$get_set4fst_si_sp (
  p_tt_variant_id integer
)
RETURNS TABLE (
  tt_at_round_id bigint,
  tt_schedule_id integer,
  stop_item_id integer,
  stop_item2round_id bigint,
  fstop_item_id integer,
  fstop_item2round_id bigint
) AS
$body$
declare
begin
  --Возвращаем набор данных для первых ОП на рейсе, являющихся пунктами выпуска
  return query
  with rnd_fst_stop as (
          select at_rnd.tt_at_round_id
                ,sch.tt_schedule_id
                ,sir.stop_item_id
                ,sir.stop_item2round_id
          from ttb.tt_schedule sch 
          join ttb.tt_schedule_item sch_it on sch_it.tt_schedule_id = sch.tt_schedule_id
          join ttb.tt_action_group act_gr on act_gr.tt_action_group_id = sch_it.tt_action_group_id and not act_gr.sign_deleted
          join ttb.tt_variant tv on tv.tt_variant_id = act_gr.tt_variant_id
          join rts.route_variant rv on rv.route_variant_id =tv.route_variant_id
          join rts.route r on r.route_id = rv.route_id
          join ttb.tt_action_type2group act2gr on act2gr.tt_action_group_id = act_gr.tt_action_group_id
          join ttb.tt_action_type tact on tact.tt_action_type_id = act2gr.tt_action_type_id
          join ttb.tt_at_round at_rnd on at_rnd.tt_action_type_id = tact.tt_action_type_id
          join ttb.tt_at_round_stop ars on ars.tt_at_round_id = at_rnd.tt_at_round_id
          join rts.stop_item2round sir on sir.stop_item2round_id = ars.stop_item2round_id and not sir.sign_deleted  
          where 1 = 1
          and tv.tt_variant_id = p_tt_variant_id
          and sir.order_num = 1)
      select rfs.tt_at_round_id
      		,rfs.tt_schedule_id
            ,rfs.stop_item_id
            ,rfs.stop_item2round_id
            ,t.fstop_item_id
            ,t.fstop_item2round_id
      from rnd_fst_stop rfs
      join ttb.timetable_pkg$get_stop_start(rfs.tt_schedule_id) t on t.fstop_item_id = rfs.stop_item_id;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;
---------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.tt_check_pkg$of_check_sp_tech2prod (
  p_tt_variant_id integer
)
RETURNS smallint AS
$body$
declare
  ln_cnt smallint;
begin
  --Проверяем состыковку ОП технологических и продуктовых рейсов
  with t_rnd_sp as (
        select at_rnd.round_id
              ,sir.stop_item_id          
              ,rnd.code
              ,sir.order_num
              ,max(sir.order_num) over (partition by sir.round_id) max_ord_num
              ,ttb.action_type_pkg$is_ab(tact.action_type_id) as is_ab
              ,ttb.action_type_pkg$is_ba(tact.action_type_id) as is_ba
              ,ttb.action_type_pkg$to_sp(tact.action_type_id) as to_sp
              ,ttb.action_type_pkg$from_sp(tact.action_type_id) as from_sp
        from ttb.tt_schedule sch 
        join ttb.tt_schedule_item sch_it on sch_it.tt_schedule_id = sch.tt_schedule_id
        join ttb.tt_action_group act_gr on act_gr.tt_action_group_id = sch_it.tt_action_group_id and not act_gr.sign_deleted
        join ttb.tt_variant tv on tv.tt_variant_id = act_gr.tt_variant_id
        join rts.route_variant rv on rv.route_variant_id =tv.route_variant_id
        join rts.route r on r.route_id = rv.route_id
        join ttb.tt_action_type2group act2gr on act2gr.tt_action_group_id = act_gr.tt_action_group_id
        join ttb.tt_action_type tact on tact.tt_action_type_id = act2gr.tt_action_type_id
        join ttb.tt_at_round at_rnd on at_rnd.tt_action_type_id = tact.tt_action_type_id
        join ttb.tt_at_round_stop ars on ars.tt_at_round_id = at_rnd.tt_at_round_id
        join rts.stop_item2round sir on sir.stop_item2round_id = ars.stop_item2round_id and not sir.sign_deleted  
        join rts.round rnd on rnd.round_id = sir.round_id
        where tv.tt_variant_id = p_tt_variant_id
	)
    ,t_prep as (
        select t.*
        from t_rnd_sp t
        where ((t.is_ab or t.is_ba) and (t.order_num = 1 or t.order_num = t.max_ord_num))
           or ( t.from_sp and t.order_num = 1 )
           or ( t.to_sp and t.order_num = max_ord_num )
    )
    ,to_sp as (
        select *
        from t_prep
        where t_prep.to_sp
    )
    ,from_sp as (
        select *
        from t_prep
        where t_prep.from_sp
    )
    ,prod_rnd as (
        select *
        from t_prep
        where t_prep.is_ab or t_prep.is_ba   
    )
    select count(1) 
    into ln_cnt
    from
     (
      select *
      from to_sp t
      left join prod_rnd p on t.stop_item_id = p.stop_item_id
      where p.stop_item_id is null
      union all
      select *
      from from_sp f 
      left join prod_rnd p on f.stop_item_id = p.stop_item_id
      where p.stop_item_id is null
     ) tt;
    
    return ln_cnt;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.tt_check_pkg$get_set_si4main_schi (
  p_tt_variant_id integer
)
RETURNS TABLE (
  prev_stop_item_id integer,
  stop_item_id integer,
  si_order_num smallint,
  prev_vshi_ordnum smallint,
  vshi_order_num smallint,
  first_stop_item integer,
  last_stop_item integer
) AS
$body$
declare
begin
  --Возвращаем набор данных по ОП в основной последовательности действий
  return query
  with t as (                            
   select lag(sir.stop_item_id, 1, sir.stop_item_id) over (partition by vshi.tt_schedule_id, sir.round_id order by sir.order_num) prev_stop_item_id
          ,sir.stop_item_id 
          ,sir.order_num as si_order_num
          ,lag(vshi.order_num, 1, vshi.order_num) over (partition by vshi.tt_schedule_id order by vshi.order_num, sir.order_num) prev_vshi_ordnum
          ,vshi.order_num as vshi_order_num
          ,first_value(sir.stop_item_id) over (partition by vshi.tt_schedule_id order by vshi.order_num, sir.order_num) first_stop_item
          ,first_value(sir.stop_item_id) over (partition by vshi.tt_schedule_id order by vshi.order_num desc, sir.order_num desc) last_stop_item
          from ttb.v_tt_schedule_item vshi
          join ttb.tt_action_type2group at2g on at2g.tt_action_group_id = vshi.tt_action_group_id
          join ttb.tt_at_round tar on tar.tt_action_type_id = at2g.tt_action_type_id
          join ttb.tt_at_round_stop tars on tars.tt_at_round_id = tar.tt_at_round_id
          join rts.stop_item2round sir on sir.stop_item2round_id = tars.stop_item2round_id
          where vshi.tt_variant_id = p_tt_variant_id
           and  vshi.ref_group_kind_id = ttb.ref_group_kind_pkg$main_action()
           )
   select  t.prev_stop_item_id
          ,t.stop_item_id 
          ,t.si_order_num
          ,t.prev_vshi_ordnum
          ,t.vshi_order_num
          ,t.first_stop_item
          ,t.last_stop_item
   from t
   order by t.vshi_order_num
   		   ,t.si_order_num;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;
----------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.tt_check_pkg$of_check_period_holes (
  p_tt_variant_id integer,
  p_tt_schedule_id integer
)
RETURNS boolean AS
$body$
declare
  lb_res boolean := true;
  ln_cnt smallint;
begin
with t_per as (
  select  tp.*
          ,ts.tt_schedule_id
          ,tsrange(tp.time_begin, tp.time_end, '[]')
          ,lag(tsrange(tp.time_begin, tp.time_end, '[]'), 1, tsrange(tp.time_begin, tp.time_end)) over (order by tp.time_begin, tp.time_end)
          +   (tsrange(tp.time_begin, tp.time_end, '[]'))
            from ttb.tt_period tp 
            join ttb.tt_schedule ts on tp.tt_schedule_id = ts.tt_schedule_id and ts.parent_schedule_id is null
            where ts.tt_variant_id =  p_tt_variant_id
              and ts.tt_schedule_id = p_tt_schedule_id
              )
  select count(1)
  into ln_cnt
  from t_per;  
    
  return lb_res;
  
exception
  when others
  then return false;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.tt_check_pkg$of_check_period_holes (
  p_tt_variant_id integer
)
RETURNS boolean AS
$body$
declare
  lb_res boolean := true;
  ln_cnt smallint;
begin
with t_per as (
  select  tp.*
          ,ts.tt_schedule_id
          ,tsrange(tp.time_begin, tp.time_end, '[]')
          ,lag(tsrange(tp.time_begin, tp.time_end, '[]'), 1, tsrange(tp.time_begin, tp.time_end)) over (order by tp.time_begin, tp.time_end)
          +   (tsrange(tp.time_begin, tp.time_end, '[]'))
            from ttb.tt_period tp 
            join ttb.tt_schedule ts on tp.tt_schedule_id = ts.tt_schedule_id
            where ts.tt_variant_id =  p_tt_variant_id
              )
  select count(1)
  into ln_cnt
  from t_per;  
    
  return lb_res;
  
exception
  when others
  then return false;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.tt_check_pkg$of_check_period_holes (
  p_tt_schedule_id_list integer []
)
RETURNS boolean AS
$body$
declare
  lb_res boolean := true;
  ln_cnt smallint;
begin
with t_per as (
  select  tp.*
          ,ts.tt_schedule_id
          ,tsrange(tp.time_begin, tp.time_end, '[]')
          ,lag(tsrange(tp.time_begin, tp.time_end, '[]'), 1, tsrange(tp.time_begin, tp.time_end)) over (order by tp.time_begin, tp.time_end)
          +   (tsrange(tp.time_begin, tp.time_end, '[]'))
            from ttb.tt_period tp 
            join ttb.tt_schedule ts on tp.tt_schedule_id = ts.tt_schedule_id 
            where ts.tt_schedule_id =  any(p_tt_schedule_id_list)
              )
  select count(1)
  into ln_cnt
  from t_per;  
    
  return lb_res;
  
exception
  when others
  then return false;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.tt_check_pkg$of_check_times2perds (
  p_tt_variant_id integer
)
RETURNS smallint AS
$body$
declare
  ln_cnt smallint;
begin
  with st_gr as (
          select
                 tact.action_type_id
                ,tact.tt_action_type_id
                ,tact.tt_variant_id
                ,at_rnd.round_id
                ,ttb.timetable_pkg$get_start_time( at_rnd.tt_at_round_id) as start_time
                ,ttb.timetable_pkg$get_finish_time( at_rnd.tt_at_round_id) as finish_time
                ,ttb.timetable_pkg$get_min_fix_time( at_rnd.tt_at_round_id) as min_fix_time
                ,ttb.timetable_pkg$get_max_fix_time( at_rnd.tt_at_round_id) as max_fix_time            
                ,sch_it.tt_schedule_item_id
                ,act2gr.tt_action_type2group_id
                ,sch_it.tt_schedule_id
                ,r.tr_type_id
                ,at_rnd.tt_at_round_id
                ,act2gr.order_num + sch_it.order_num * 1000 as order_num_action
              from   ttb.tt_schedule sch 
                join ttb.tt_schedule_item sch_it on sch_it.tt_schedule_id = sch.tt_schedule_id
                join ttb.tt_action_group act_gr on act_gr.tt_action_group_id = sch_it.tt_action_group_id and not act_gr.sign_deleted
                join ttb.tt_variant tv on tv.tt_variant_id = act_gr.tt_variant_id
                join rts.route_variant rv on rv.route_variant_id =tv.route_variant_id
                join rts.route r on r.route_id = rv.route_id
                join ttb.tt_action_type2group act2gr on act2gr.tt_action_group_id = act_gr.tt_action_group_id
                join ttb.tt_action_type tact on tact.tt_action_type_id = act2gr.tt_action_type_id
                join ttb.tt_at_round at_rnd on at_rnd.tt_action_type_id = tact.tt_action_type_id
                join ttb.tt_check_pkg$get_set4fst_si_sp(p_tt_variant_id) sss on sss.tt_at_round_id = at_rnd.tt_at_round_id /*Только рейсы с совпадающими первыми ОП и пунктами выпуска*/
              where 1 = 1 
                 and ttb.action_type_pkg$is_production_round(tact.action_type_id)
                 and sch.parent_schedule_id is null /*Для подчиненных последовательностей не проверять*/
                 and sch.tt_variant_id = p_tt_variant_id
            )
 	,first_round_time as (
          select st_gr.tt_variant_id
          		,max(st_gr.start_time) over (partition by st_gr.round_id) first_round_time
            from st_gr)
 	,last_round_time as (
          select st_gr.tt_variant_id
          		,max(st_gr.finish_time) over (partition by st_gr.round_id) last_round_time
            from st_gr)
 	,min_fix_time as (
          select st_gr.tt_variant_id
          		,min(st_gr.min_fix_time) over (partition by st_gr.round_id) min_fix_time
            from st_gr)
 	,max_fix_time as (
          select st_gr.tt_variant_id
          		,max(st_gr.max_fix_time) over (partition by st_gr.round_id) max_fix_time
            from st_gr)   
    ,check_per as ( 
    select distinct  
    		 (tsrange(tp.time_begin, tp.time_end, '[]')) @> frt.first_round_time as frt
    		,(tsrange(tp.time_begin, tp.time_end, '[]')) @> lrt.last_round_time as lrt
    		,(tsrange(tp.time_begin, tp.time_end, '[]')) @> mnft.min_fix_time as mnft
    		,(tsrange(tp.time_begin, tp.time_end, '[]')) @> mxft.max_fix_time as mxft
            ,tsrange(tp.time_begin, tp.time_end, '[]')
    from ttb.tt_period tp 
    join ttb.tt_schedule ts on tp.tt_schedule_id = ts.tt_schedule_id 
                           and ts.parent_schedule_id is null
    join first_round_time frt on frt.tt_variant_id = ts.tt_variant_id
    join last_round_time lrt on lrt.tt_variant_id = ts.tt_variant_id
    join min_fix_time mnft on mnft.tt_variant_id = ts.tt_variant_id
    join max_fix_time mxft on mxft.tt_variant_id = ts.tt_variant_id
    where ts.tt_variant_id = p_tt_variant_id    
    )
    select distinct --cp.*
    	   coalesce(max(cp.frt::int) over (), 1) +
           coalesce(max(cp.lrt::int) over (), 1) +
           coalesce(max(cp.mnft::int) over (), 1) +
           coalesce(max(cp.mxft::int) over (), 1) 
    into ln_cnt
    from check_per cp
    ;                  
    
    return ln_cnt;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------------------------------------------------------