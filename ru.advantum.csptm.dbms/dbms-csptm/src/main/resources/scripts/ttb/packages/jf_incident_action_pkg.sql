CREATE OR REPLACE FUNCTION ttb.jf_incident_action_pkg$attr_to_rowtype (p_attr text) RETURNS ttb.incident_action
	LANGUAGE plpgsql
AS $$
declare
   l_r ttb.incident_action%rowtype;
begin
   l_r.incident_action_id := jofl.jofl_pkg$extract_number(p_attr, 'incident_action_id', true);
   l_r.incident_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'incident_type_id', true);
   l_r.incident_handler_id := jofl.jofl_pkg$extract_varchar(p_attr, 'incident_handler_id', true);

   return l_r;
end;
$$;


CREATE OR REPLACE FUNCTION ttb.jf_incident_action_pkg$of_update (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
declare
   l_r ttb.incident_action%rowtype;
begin
   l_r := ttb.jf_incident_action_pkg$attr_to_rowtype(p_attr);

   update ttb.incident_action set
          incident_type_id = l_r.incident_type_id,
          incident_handler_id = l_r.incident_handler_id
   where
          incident_action_id = l_r.incident_action_id;

   return null;
end;
$$;


CREATE OR REPLACE FUNCTION ttb.jf_incident_action_pkg$of_insert (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r ttb.incident_action%rowtype;
begin
   l_r := ttb.jf_incident_action_pkg$attr_to_rowtype(p_attr);

   insert into ttb.incident_action select l_r.*;

   return null;
end;
$$;


CREATE OR REPLACE FUNCTION ttb.jf_incident_action_pkg$of_delete (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
declare
   l_r ttb.incident_action%rowtype;
begin
   l_r := ttb.jf_incident_action_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.incident_action where  incident_action_id = l_r.incident_action_id;

   return null;
end;
$$;

CREATE OR REPLACE FUNCTION ttb.jf_incident_action_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$
declare
begin
 open p_rows for
select
  incident_action.incident_action_id,
  incident_action.incident_type_id,
  incident_action.incident_handler_id,
  incident_type_name,
  incident_handler_name,
  incident_handler_comment
from ttb.incident_action
  join ttb.incident_handler on incident_action.incident_handler_id = incident_handler.incident_handler_id
  join ttb.incident_type on incident_action.incident_type_id = incident_type.incident_type_id;
end;
$$;
