CREATE OR REPLACE FUNCTION ttb.jf_rep_appointed_tasks_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare

begin
 open p_rows for
     select
 null as depo_name, /* Наименование ТП */
 null as tr_type_name, /* Вид ТС */
 null as route_num, /* Номер маршрута */
 null as out_num, /* Номер выхода */
 null as dr_shift_num, /* Номер смены */
 null as garage_num, /* Гар. номер ТС */
 null as tr_licence, /* Гос. номер ТС */
 null as dr_tab_num, /* Водитель */
 null as round_type, /* Тип рейса */
 null as stop_name, /* Наименование ОП */
 null as change_reason, /* Причина изменения расписания */
 null as exectime_deviation, /* Отклонение  факт. времени выполнения задания */
 null as task_action, /* Управляющее воздействие  */
 null as task_endtime, /* Время окончания управляющего воздействия */
 null as task_initiator, /* Инициатор задания/назначения управляющего воздействия */
 null as task_regtime, /* Время регистрации задания */
 null as task_starttime /* Время начала управляющего воздействия  */
;
end;
$function$
;
