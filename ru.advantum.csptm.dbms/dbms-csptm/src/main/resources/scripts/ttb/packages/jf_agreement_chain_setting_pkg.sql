CREATE OR REPLACE FUNCTION ttb.jf_agreement_chain_setting_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
AS $$
begin
    open p_rows for
    SELECT ctt.tr_type_id as tr_type_id,ctt.name as name,ctt.short_name  as short_name from core.tr_type ctt;
end;
$$ LANGUAGE plpgsql;