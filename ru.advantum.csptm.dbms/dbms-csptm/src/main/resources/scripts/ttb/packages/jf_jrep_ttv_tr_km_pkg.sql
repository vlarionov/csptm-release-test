create or replace function ttb."jf_jrep_ttv_tr_km_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
language plpgsql
as $$
declare
  l_tt_variant_id ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'F_tt_variant_id', true);
begin
 open p_rows for
       select distinct
         tt_tr.tt_variant_id p_tt_variant,
         tt_tr.depo2territory_id p_depo2territory,
         depo.name_short as p_depo_name_short,
         terr.name_short as p_terr_name_short,
         v.route_variant_id p_route_variant,
         v.route_num p_route_num,
         to_char(v.dfrom::date, 'dd.mm.yyyy') p_dfrom,
         v.p_is_tram::text p_is_tram
       from ( select v.route_variant_id, lower(v.action_period) dfrom, r.route_num,
              case when r.tr_type_id = 3 then 1 else 0 end p_is_tram
              from ttb.tt_variant v join rts.route_variant rv on v.route_variant_id = rv.route_variant_id
                   join rts.route r on rv.route_id = r.route_id
              where v.tt_variant_id = l_tt_variant_id
            ) v,
          ttb.tt_tr join core.depo2territory d2t on d2t.depo2territory_id = tt_tr.depo2territory_id
          join core.entity depo on depo.entity_id =d2t.depo_id
          join core.entity terr on terr.entity_id = d2t.territory_id
          join core.tr_capacity cap on cap.tr_capacity_id = tt_tr.tr_capacity_id
          join ttb.tt_tr_group ttg on ttg.tt_tr_group_id = tt_tr.tt_tr_group_id
        where tt_tr.tt_variant_id = l_tt_variant_id;
end;
$$;

--drop function ttb."jf_jrep_ttv_tr_km_pkg$of_jrep"(numeric, numeric, text)

create or replace function ttb."jf_jrep_ttv_tr_km_pkg$of_jrep"(p_tt_variant numeric, p_depo2territory numeric, p_is_tram text)
  returns table(
    round_num int,
    code text,
    is_production int,
    tr_capacity_id smallint,
    capacity_name text,
    out_cnt bigint,
    length_ab numeric,
    length_ba numeric,
    length_tech numeric,
    cnt_ab bigint,
    cnt_ba bigint,
    cnt_total bigint,
    total_ab numeric,
    total_ba numeric,
    total_km_prod numeric,
    total_km_tech numeric,
    first_stop text,
    last_stop text
  )
language plpgsql
as $$
begin
  if p_is_tram = '0' then
    return query
      with
        rpt_head as (
          select r.tr_type_id = 3 is_tram,
                 ttv.route_variant_id,
                 d2t.depo2territory_id,
                 ttv.tt_variant_id,
                 d2r.depo_id,
                 d2t.territory_id
          from ttb.tt_variant ttv join rts.route_variant rv on ttv.route_variant_id = rv.route_variant_id
               join rts.route r on rv.route_id = r.route_id
               join core.tr_type trt on r.tr_type_id = trt.tr_type_id
               join rts.depo2route d2r on r.route_id = d2r.route_id
               join core.depo d on d2r.depo_id = d.depo_id
               join core.entity ed on d.depo_id = ed.entity_id
               join core.depo2territory d2t on d.depo_id = d2t.depo_id
               join core.entity et on d2t.territory_id = et.entity_id
          where ttv.tt_variant_id = p_tt_variant
            and d2t.depo2territory_id = p_depo2territory
       ),
        rnd as (
          select q.round_id, q.round_num, q.move_direction_id, q.code, q.is_production,
                 case when q.is_production = 1 then q.round_num else q.round_id end grp_key,
                 max(q.stop_item_id) filter(where q.order_num = 1) first_stop_id,
                 max(q.stop_item_id) filter(where q.order_num = q.last_stop_num) last_stop_id,
                 case when q.move_direction_id = 1 then sum(length_sector) else 0 end length_ab,
                 case when q.move_direction_id = 2 then sum(length_sector) else 0 end length_ba,
                 sum(length_sector) length_tech
         from
            (
              select rnd.round_id, rnd.move_direction_id, rnd.code, rnd.round_num,
                     case when ttb.action_type_pkg$is_production_round(rnd.action_type_id) then 1 else 0 end is_production,
                     st2r.order_num,
                     first_value(st2r.order_num) over(PARTITION BY st2r.round_id order by order_num desc) last_stop_num,
                     st2r.stop_item_id,
                     st2r.length_sector
              from rpt_head,
                   rts.round rnd join rts.stop_item2round st2r on rnd.round_id = st2r.round_id
              where rnd.route_variant_id = rpt_head.route_variant_id
                and not rnd.sign_deleted
            ) q
          group by q.round_id, q.round_num, q.move_direction_id, q.code, q.is_production
        ),
    t as
      (
        select tt_tr.is_second_car, t.tt_out_id, t.tr_capacity_id
        from ttb.tt_out t join ttb.tt_out2tt_tr t2t on t.tt_out_id = t2t.tt_out_id
             join ttb.tt_tr tt_tr on t2t.tt_tr_id = tt_tr.tt_tr_id
        where not t.sign_deleted
          and t.tt_variant_id = p_tt_variant
          and tt_tr.depo2territory_id = p_depo2territory
      )
      select
             rnd.round_num, rnd.code, rnd.is_production, t.tr_capacity_id::smallint, tc.short_name capacity_name,
             count(distinct t.tt_out_id) out_cnt,
             max(rnd.length_ab::numeric/1000) length_ab,
             max(rnd.length_ba::numeric/1000) length_ba,
             max(rnd.length_tech::numeric/1000) length_tech,
             count(*) filter(where rnd.move_direction_id = 1) cnt_ab,
             count(*) filter(where rnd.move_direction_id = 2) cnt_ba,
             count(*) cnt_total,
             sum(rnd.length_ab)/1000 total_ab,
             sum(rnd.length_ba)/1000 total_ba,
             case when rnd.is_production = 1 then (sum(rnd.length_ab) + sum(rnd.length_ba))/1000 else 0 end total_km_prod,
             case when rnd.is_production = 0 then (sum(rnd.length_ab) + sum(rnd.length_ba))/1000 else 0 end total_km_tech,
             max(st1.name) first_stop,
             max(st2.name) last_stop
      from rpt_head,
           rnd  join ttb.tt_action_round ar on rnd.round_id = ar.round_id
           join ttb.tt_action a on ar.tt_action_id = a.tt_action_id and not a.sign_deleted
           join t on a.tt_out_id = t.tt_out_id
           join ttb.tt_out2tt_tr t2t on t.tt_out_id = t2t.tt_out_id
           join ttb.tt_tr tt_tr on t2t.tt_tr_id = tt_tr.tt_tr_id
           join core.tr_capacity tc on t.tr_capacity_id = tc.tr_capacity_id
           join rts.stop_item sti1 on sti1.stop_item_id = rnd.first_stop_id
           join rts.stop_location stl1 on stl1.stop_location_id = sti1.stop_location_id
           join rts.stop st1 on stl1.stop_id = st1.stop_id
           join rts.stop_item sti2 on sti2.stop_item_id = rnd.last_stop_id
           join rts.stop_location stl2 on stl2.stop_location_id = sti2.stop_location_id
           join rts.stop st2 on stl2.stop_id = st2.stop_id
      group by rnd.grp_key, rnd.round_num, rnd.code, rnd.is_production, t.tr_capacity_id, tc.short_name
      order by tc.short_name, rnd.is_production desc, rnd.code;
  else
    return query
    with
      rpt_head as (
        select ttv.route_variant_id,
               d2t.depo2territory_id,
               ttv.tt_variant_id,
               d2r.depo_id,
               d2t.territory_id
        from ttb.tt_variant ttv join rts.route_variant rv on ttv.route_variant_id = rv.route_variant_id
             join rts.route r on rv.route_id = r.route_id
             join core.tr_type trt on r.tr_type_id = trt.tr_type_id
             join rts.depo2route d2r on r.route_id = d2r.route_id
             join core.depo d on d2r.depo_id = d.depo_id
             join core.entity ed on d.depo_id = ed.entity_id
             join core.depo2territory d2t on d.depo_id = d2t.depo_id
             join core.entity et on d2t.territory_id = et.entity_id
        where ttv.tt_variant_id = p_tt_variant
          and d2t.depo2territory_id = p_depo2territory
      ),
      rnd as (
        select q.round_id, q.round_num, q.move_direction_id, q.code, q.is_production,
                 case when q.is_production = 1 then q.round_num else q.round_id end grp_key,
                 max(q.stop_item_id) filter(where q.order_num = 1) first_stop_id,
                 max(q.stop_item_id) filter(where q.order_num = q.last_stop_num) last_stop_id,
                 case when q.move_direction_id = 1 then sum(length_sector) else 0 end length_ab,
                 case when q.move_direction_id = 2 then sum(length_sector) else 0 end length_ba,
                 sum(length_sector) length_tech
        from
          (
            select rnd.round_id, rnd.move_direction_id, rnd.code, rnd.round_num,
                   case when ttb.action_type_pkg$is_production_round(rnd.action_type_id) then 1 else 0 end is_production,
                   st2r.order_num,
                   first_value(st2r.order_num) over(PARTITION BY st2r.round_id order by order_num desc) last_stop_num,
                   st2r.stop_item_id,
                   st2r.length_sector
            from rpt_head,
                 rts.round rnd join rts.stop_item2round st2r on rnd.round_id = st2r.round_id
            where rnd.route_variant_id = rpt_head.route_variant_id
              and not rnd.sign_deleted
          ) q
        group by q.round_id, q.round_num, q.move_direction_id, q.code, q.is_production
      ),
    t as
      (
        select tt_tr.is_second_car, t.tt_out_id,
               case when t.tr_capacity_id <> core.tr_capacity_pkg$huge() and not tt_tr.is_second_car
                    then 1 else case when not tt_tr.is_second_car then 2 else 3 end
               end tr_capacity_id
        from ttb.tt_out t join ttb.tt_out2tt_tr t2t on t.tt_out_id = t2t.tt_out_id
             join ttb.tt_tr tt_tr on t2t.tt_tr_id = tt_tr.tt_tr_id
        where not t.sign_deleted
          and t.tt_variant_id = p_tt_variant
          and tt_tr.depo2territory_id = p_depo2territory
      )
    select rnd.round_num, rnd.code, rnd.is_production, tt_out.tr_capacity_id::smallint,
           case tt_out.tr_capacity_id when 1 then 'Головной' when 2 then 'ОБК' when 3 then 'Прицеп' end capacity_name,
           max(tt_out.out_cnt) out_cnt,
             max(rnd.length_ab::numeric/1000) length_ab,
             max(rnd.length_ba::numeric/1000) length_ba,
             max(rnd.length_tech::numeric/1000) length_tech,
             count(*) filter(where rnd.move_direction_id = 1) cnt_ab,
             count(*) filter(where rnd.move_direction_id = 2) cnt_ba,
             count(*) cnt_total,
             sum(rnd.length_ab)/1000 total_ab,
             sum(rnd.length_ba)/1000 total_ba,
             case when rnd.is_production = 1 then (sum(rnd.length_ab) + sum(rnd.length_ba))/1000 else 0 end total_km_prod,
             case when rnd.is_production = 0 then (sum(rnd.length_ab) + sum(rnd.length_ba))/1000 else 0 end total_km_tech,
             max(st1.name) first_stop,
             max(st2.name) last_stop
    from rpt_head,
        (
          select case when ext_car then 3 else case when q.tr_capacity_id <> core.tr_capacity_pkg$huge() then 1 else 2 end end tr_capacity_id,
                 count(*) over(partition by case when ext_car then 3 else case when q.tr_capacity_id <> core.tr_capacity_pkg$huge() then 1 else 2 end end) out_cnt,
                 q.tt_out_id
          from
            (
              select false ext_car, t.* from t
                union all
              select true ext_car, t.* from t where t.is_second_car
            ) q
         )  tt_out join ttb.tt_action a on a.tt_out_id = tt_out.tt_out_id
         join ttb.tt_action_round ar on a.tt_action_id = ar.tt_action_id and not a.sign_deleted
         join rnd on rnd.round_id = ar.round_id
         join rts.stop_item sti1 on sti1.stop_item_id = rnd.first_stop_id
         join rts.stop_location stl1 on stl1.stop_location_id = sti1.stop_location_id
         join rts.stop st1 on stl1.stop_id = st1.stop_id
         join rts.stop_item sti2 on sti2.stop_item_id = rnd.last_stop_id
         join rts.stop_location stl2 on stl2.stop_location_id = sti2.stop_location_id
         join rts.stop st2 on stl2.stop_id = st2.stop_id
      group by rnd.grp_key, rnd.round_num, rnd.code, rnd.is_production, tt_out.tr_capacity_id, capacity_name
      order by capacity_name, rnd.is_production desc, rnd.code;
  end if;
end;
$$

--select * from ttb."jf_jrep_ttv_tr_km_pkg$of_jrep"(277, 12, '1')

--select * from ttb."jf_jrep_ttv_tr_km_pkg$of_jrep"(255, 13, '0')
