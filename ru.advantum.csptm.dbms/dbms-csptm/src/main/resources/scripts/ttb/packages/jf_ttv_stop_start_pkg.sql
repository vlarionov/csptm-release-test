-------------------
CREATE OR REPLACE FUNCTION ttb."jf_ttv_stop_start_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$ 
declare
  l_tt_variant_id ttb.tt_variant.tt_variant_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
  l_round_id rts.round.round_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'round_id', true);
begin 
 open p_rows for
 select
   sir.stop_item_id,
   sir.stop_item2round_id,
   rnd.code as round_code,
   rnd.round_id,
   s.name as stop_name,
   rnd.action_type_id
 from ttb.tt_schedule sch
   join  ttb.tt_schedule_item sch_it on sch_it.tt_schedule_id = sch.tt_schedule_id
   join ttb.tt_action_group act_gr on act_gr.tt_action_group_id = sch_it.tt_action_group_id
   join ttb.tt_action_type2group act2gr on act2gr.tt_action_group_id = act_gr.tt_action_group_id
   join ttb.tt_action_type tact on tact.tt_action_type_id = act2gr.tt_action_type_id
   join ttb.tt_at_round at_rnd on at_rnd.tt_action_type_id = tact.tt_action_type_id and not sign_deleted
   join rts.round rnd on rnd.round_id = at_rnd.round_id  and not rnd.sign_deleted
   join rts.stop_item2round sir on sir.round_id = rnd.round_id and sir.order_num in
                                                                   (select max(t.order_num) from rts.stop_item2round t
                                                                   where t.round_id = sir.round_id and not t.sign_deleted) and not sir.sign_deleted
   join rts.stop_item si on si.stop_item_id =sir.stop_item_id
   join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
   join rts.stop s on s.stop_id = sl.stop_id
 where sch.tt_variant_id = l_tt_variant_id
       and  act_gr.ref_group_kind_id = ttb.ref_group_kind_pkg$first_action()
       and  not exists (select 1
                        from rts.stop_item2round sss
                        where sss.stop_item2round_id = sir.stop_item2round_id
                          and sss.order_num =2
                          and sss.round_id =  l_round_id
                          and not sss.sign_deleted)
  ;



end;
 $function$
;
