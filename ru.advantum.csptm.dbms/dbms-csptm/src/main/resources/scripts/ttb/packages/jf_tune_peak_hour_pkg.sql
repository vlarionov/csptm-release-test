CREATE OR REPLACE FUNCTION ttb."jf_tune_peak_hour_pkg$attr_to_rowtype"(p_attr TEXT)
    RETURNS ttb.TUNE_PEAK_HOUR
LANGUAGE plpgsql
AS $function$
DECLARE
    l_r ttb.tune_peak_hour%ROWTYPE;
BEGIN
    l_r.tune_peak_hour_id := jofl.jofl_pkg$extract_number(p_attr, 'tune_peak_hour_id', TRUE);
    l_r.calendar_tag_id := jofl.jofl_pkg$extract_number(p_attr, 'calendar_tag_id', TRUE);
    l_r.ph_name := jofl.jofl_pkg$extract_varchar(p_attr, 'ph_name', TRUE);
    l_r.time_begin := jofl.jofl_pkg$extract_number(p_attr, 'time_begin', TRUE);
    l_r.time_end := jofl.jofl_pkg$extract_number(p_attr, 'time_end', TRUE);
    l_r.peak_hour_name := jofl.jofl_pkg$extract_varchar(p_attr, 'peak_hour_name', TRUE);

    IF (l_r.time_begin > l_r.time_end)
    THEN
        RAISE EXCEPTION '<<Время окончание должно быть быльше времени начала!>>';
    END IF;
    RETURN l_r;
END;
$function$;
-------------------

CREATE OR REPLACE FUNCTION ttb."jf_tune_peak_hour_pkg$of_rows"(p_id_account NUMERIC,
    OUT                                                        p_rows       REFCURSOR, p_attr TEXT)
    RETURNS REFCURSOR
LANGUAGE plpgsql
AS $function$
BEGIN
    OPEN p_rows FOR
    SELECT
        ttph.tune_peak_hour_id AS tune_peak_hour_id,
        tct.tag_name           AS ph_name,
        tct.calendar_tag_id,
        ttph.time_begin,
        ttph.time_end,
        ttph.peak_hour_name
    FROM ttb.tune_peak_hour ttph
        JOIN ttb.calendar_tag tct ON tct.calendar_tag_id = ttph.calendar_tag_id;
END;
$function$;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tune_peak_hour_pkg$of_update"(p_id_account NUMERIC, p_attr TEXT)
    RETURNS TEXT
LANGUAGE plpgsql
AS $function$
DECLARE
    l_r ttb.tune_peak_hour%ROWTYPE;
BEGIN
    l_r := ttb.jf_tune_peak_hour_pkg$attr_to_rowtype(p_attr);

    IF exists(
        SELECT
            t.calendar_tag_id,
            t.peak_hour_name,
            t.tune_peak_hour_id
        FROM ttb.tune_peak_hour t
        WHERE t.calendar_tag_id = l_r.calendar_tag_id AND
              t.peak_hour_name = l_r.peak_hour_name AND
              t.tune_peak_hour_id <> l_r.tune_peak_hour_id
    )
    THEN
        RAISE EXCEPTION '<<Такая пара Признак-Наименование уже существует!>>';
    ELSE
        UPDATE ttb.tune_peak_hour
        SET
            calendar_tag_id = l_r.calendar_tag_id,
            ph_name         = l_r.ph_name,
            time_begin      = l_r.time_begin,
            time_end        = l_r.time_end,
            peak_hour_name  = l_r.peak_hour_name
        WHERE
            tune_peak_hour_id = l_r.tune_peak_hour_id;
    END IF;
    RETURN NULL;
END;
$function$;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tune_peak_hour_pkg$of_delete"(p_id_account NUMERIC, p_attr TEXT)
    RETURNS TEXT
LANGUAGE plpgsql
AS $function$
DECLARE
    l_tune_peak_hour_id ttb.tune_peak_hour.tune_peak_hour_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'tune_peak_hour_id', TRUE);
BEGIN
    DELETE FROM ttb.tune_peak_hour
    WHERE tune_peak_hour_id = l_tune_peak_hour_id;
    RETURN NULL;
END;
$function$;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tune_peak_hour_pkg$of_insert"(p_id_account NUMERIC, p_attr TEXT)
    RETURNS TEXT
LANGUAGE plpgsql
AS $function$
DECLARE
    l_r ttb.tune_peak_hour%ROWTYPE;
BEGIN
    l_r := ttb.jf_tune_peak_hour_pkg$attr_to_rowtype(p_attr);

    IF exists(
        SELECT
            t.peak_hour_name,
            t.calendar_tag_id
        FROM ttb.tune_peak_hour t
        WHERE t.calendar_tag_id = l_r.calendar_tag_id AND
              t.peak_hour_name = l_r.peak_hour_name
    )
    THEN
        RAISE EXCEPTION '<<Такая пара Признак-Наименование уже существует!>>';
    ELSE
        INSERT INTO ttb.tune_peak_hour (calendar_tag_id, ph_name, time_begin, time_end, peak_hour_name)
        VALUES (l_r.calendar_tag_id, l_r.ph_name, l_r.time_begin, l_r.time_end, l_r.peak_hour_name);
    END IF;
    RETURN NULL;
END;
$function$;