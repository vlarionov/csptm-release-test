CREATE OR REPLACE FUNCTION ttb.jf_tt_out_arr_dep_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 ln_tt_variant_id 	ttb.tt_variant.tt_variant_id%type 			:= jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
 ln_move_dir		rts.move_direction.move_direction_id%type 	:= jofl.jofl_pkg$extract_number(p_attr, 'move_dir', true);
 ln_stop_id_from	rts.stop.stop_id%type						:= jofl.jofl_pkg$extract_number(p_attr, 'stop_id_from', true);
begin 

 --ln_move_dir := 1;
perform ttb.tt_out_pkg$make_tmp_tt4var(ln_tt_variant_id);
 open p_rows for
 with act as (
     select vma.action_type_id
       ,vma.action_type_code
       ,vma.action_type_name
       ,vma.is_production_round
       ,vma.is_technical_round
       ,vma.is_dinner
       ,vma.is_break
       ,vma.is_round
       --,ttb.action_type_pkg$is_bn_round_break(act.action_type_id) as is_bn_round_break
       ,vma.action_type_id = ttb.action_type_pkg$bn_round_break()  as is_bn_round_break
       ,vma.is_all_pause
     from ttb.vm_action_type vma
 ),
      q as (
                    select distinct
                       out.tt_out_id
                      ,out.tt_out_num
                      ,min(tai.time_begin) over (partition by ta.tt_action_id) as time_begin
                      ,max(tai.time_end) over (partition by ta.tt_action_id) as time_end
                      ,ta.action_dur
                      ,at.action_type_code
                      ,at.action_type_name
                      ,at.is_production_round
                      ,at.is_technical_round
                      ,at.is_bn_round_break
                      ,at.is_dinner
                      ,at.is_break
                      ,at.is_round
                      ,md.move_direction_id
                      ,md.move_direction_name 
                      ,r.code as r_code
                      ,first_value(s.name) over (partition by ta.tt_action_id order by si2r.order_num) as stop_b
                      ,first_value(s.name) over (partition by ta.tt_action_id order by si2r.order_num desc) as stop_e
                      ,first_value(s.stop_id) over (partition by ta.tt_action_id order by si2r.order_num) as stop_id_b
                      ,first_value(s.stop_id) over (partition by ta.tt_action_id order by si2r.order_num desc) as stop_id_e
                      ,ta.tt_action_id
                      ,out.tt_variant_id
                      ,at.action_type_id                
              from tt_out out 
              join tt_action ta on ta.tt_out_id = out.tt_out_id
              join act at on at.action_type_id = ta.action_type_id
              join tt_action_item tai on tai.tt_action_id = ta.tt_action_id        
              join rts.stop_item2round si2r on si2r.stop_item2round_id = tai.stop_item2round_id          							 
              join rts.stop_item si on si.stop_item_id = si2r.stop_item_id
              join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
              join rts.stop s on s.stop_id = sl.stop_id
              join rts.stop_item2round_type si2rt on si2rt.stop_item2round_id = si2r.stop_item2round_id
              join rts.stop_item_type sit on sit.stop_item_id = si.stop_item_id
                                          and sit.stop_item_type_id = si2rt.stop_item_type_id  
              left join rts.stop_type st on st.stop_type_id = sit.stop_type_id and st.stop_type_id = rts.stop_type_pkg$finish()/*7*/                                          
              left join tt_action_round tar on tar.tt_action_id = ta.tt_action_id         
              left join rts.round r on r.round_id = tar.round_id
                                   and si2r.round_id = r.round_id 
                                   and r.action_type_id = at.action_type_id    
              left join rts.move_direction md on md.move_direction_id = r.move_direction_id                             
              where out.tt_variant_id = ln_tt_variant_id
              ),
              qq as (
              select   q.tt_out_id
                      ,q.tt_out_num
                      ,lag(q.time_end, 1, q.time_begin) over (partition by q.tt_out_id order by q.time_end) as arr_time
                      ,q.time_begin
                      ,q.time_end
                      /*,case when ttb.action_type_pkg$is_production_round(q.action_type_id)
                      		then lag(q.time_end, 1, q.time_begin) over (partition by q.tt_out_id order by q.time_end) 
                            else q.time_end
                       end arr_time */                                         
                      ,q.action_dur
                      ,q.action_type_code
                      ,q.action_type_name
                      ,q.move_direction_id
                      ,lead(q.move_direction_id, 1, q.move_direction_id) over () as lead_move_direction_id
                      ,coalesce(q.move_direction_id, 
                               lead(q.move_direction_id, 1, q.move_direction_id) over ()) as agg_move_direction_id
                      ,q.move_direction_name
                      ,q.r_code
                      ,q.is_production_round
                      ,q.is_technical_round
                      ,q.is_bn_round_break
                      ,q.is_dinner
                      ,coalesce(q.stop_b, q.stop_e) as stop_b
                      ,q.stop_e
                      ,coalesce(q.stop_id_b, q.stop_id_e) as stop_id_b
                      ,q.stop_id_e
                      ,q.tt_action_id
                      ,q.tt_variant_id
                      ,q.action_type_id
          from q 
              where q.is_production_round
              	 or q.is_technical_round
              )
              select   qq.tt_out_id
                      ,qq.tt_out_num
                      ,qq.arr_time
                      ,qq.time_begin
                      ,qq.time_end                                      
                      ,qq.action_dur
                      ,qq.action_type_code
                      ,qq.action_type_name
                      ,qq.move_direction_id
                      ,qq.lead_move_direction_id
                      ,qq.agg_move_direction_id
                      ,qq.move_direction_name
                      ,case 
                         when qq.is_production_round 
                           then qq.r_code
                         else qq.action_type_name
                       end r_code
                      ,qq.is_production_round
                      ,qq.is_technical_round
                      ,qq.is_bn_round_break
                      ,qq.is_dinner
                      ,qq.stop_b
                      ,qq.stop_e
                      ,qq.stop_id_b
                      ,qq.stop_id_e
                      ,qq.tt_action_id
                      ,qq.tt_variant_id
                      ,qq.action_type_id
                      ,qq.time_begin::date + (qq.time_begin - qq.arr_time) as dur
                      ,case
                       when qq.is_production_round and qq.r_code='00'
                         then
                           qq.time_begin::date +
                           coalesce(qq.time_begin - lag(qq.time_begin) over (partition by qq.is_production_round,qq.stop_id_b,qq.r_code order by qq.time_begin)
                           ,interval '0 sec')
                       else null
                       end
                      as int
                      ,case 
                      	when                      
                           (select at.is_dinner
                              from tt_action ta 
                                join act at on at.action_type_id = ta.action_type_id
                             where ta.tt_out_id = qq.tt_out_id
                               and ta.dt_action < qq.time_begin
                          order by ta.dt_action desc
                             limit 1) then 11790079 /*#b3e6ff*/
                       when                      
                           (select at.is_all_pause
                              from tt_action ta 
                                join act at on at.action_type_id = ta.action_type_id
                             where ta.tt_out_id = qq.tt_out_id
                               and ta.dt_action < qq.time_begin
                          order by ta.dt_action desc
                             limit 1) then 16770688 /*ffe680*/
                       end "ROW$COLOR"
              from qq
              where 1 = 1
                and qq.stop_id_b = ln_stop_id_from
               -- and qq.agg_move_direction_id = ln_move_dir 
              order by qq.time_begin
              ;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;