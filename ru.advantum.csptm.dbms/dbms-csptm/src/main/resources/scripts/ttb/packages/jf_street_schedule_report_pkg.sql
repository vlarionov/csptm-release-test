create or replace function ttb.street_schedule$get_schedule(p_tt_variant_id text)
  returns table(
  tt_out_num text ,
  tt_out_id text ,
  time_begin text ,
  code text ,
  action_type_id text ,
  round_id text

  ) as
$body$
declare
begin
  return query
    with r1 as (
select
  tto.tt_out_num ,
  tto.tt_out_id ,
  ttai.time_begin ,
  rnd.code ,
  rnd.action_type_id ,
  rnd.round_id 
from
  ttb.tt_variant ttv
  join ttb.tt_out tto on ttv.tt_variant_id = tto.tt_variant_id
  join ttb.tt_action tta on tto.tt_out_id = tta.tt_out_id
  join ttb.tt_action_item ttai on tta.tt_action_id = ttai.tt_action_id
  join rts.stop_item2round si2r on ttai.stop_item2round_id = si2r.stop_item2round_id
  join rts.round rnd on si2r.round_id = rnd.round_id
where tto.tt_variant_id = p_tt_variant_id :: int
      and rnd.move_direction_id = 1
group by rnd.code, rnd.round_id, tto.tt_out_num, ttai.time_begin, tto.tt_out_id
),
rounds as (
select *
from
r1
where
ttb.action_type_pkg$is_production_round(r1.action_type_id)
)
, stops as (
select
--       chr(si2r2.order_num) || '$' || s.name                      stop_name,
si2r2.order_num,
si2r2.order_num || ' ' || s.name stop_name,
si2r2.stop_item2round_id
from
rounds
join rts.round rnd on rounds.round_id = rnd.round_id
join rts.stop_item2round si2r2 on si2r2.round_id = rnd.round_id
join rts.stop_item si on si2r2.stop_item_id = si.stop_item_id
join rts.stop_location sl on si.stop_location_id = sl.stop_location_id
join rts.stop s on sl.stop_id = s.stop_id
where true = true
group by s.name, si2r2.order_num, si2r2.stop_item2round_id, rounds.tt_out_id
),
f11 as (
select
rounds.tt_out_num,
rounds.tt_out_id,
rounds.code
from rounds
group by rounds.tt_out_num, rounds.code, rounds.tt_out_id
),
f12 as (
select *
from f11
full outer join stops on true = true
),
f21 as (
select
f12.tt_out_num,
f12.order_num,
f12.stop_name,
f12.stop_item2round_id,
f12.code,
tta.tt_out_id,
tta.tt_action_id
from f12
left join ttb.tt_action tta on tta.tt_out_id = f12.tt_out_id
group by f12.tt_out_num, f12.tt_out_id, f12.code, f12.order_num, f12.stop_name, f12.stop_item2round_id,
tta.tt_out_id, tta.tt_action_id
)
select
  f21.tt_out_num,
  f21.tt_out_id,
  f21.stop_name,
  f21.code,
  to_char(ttai.time_begin , 'HH24:MI') :: text as time_begin
from f21
  left join ttb.tt_action_item ttai
    on f21.stop_item2round_id = ttai.stop_item2round_id and f21.tt_action_id = ttai.tt_action_id
group by f21.tt_out_id, f21.tt_out_num, f21.stop_name, ttai.time_begin , f21.code
order by (trim (f21.stop_name ) similar to '[0-9]+' ):: int ;
end;
$body$
language 'plpgsql';