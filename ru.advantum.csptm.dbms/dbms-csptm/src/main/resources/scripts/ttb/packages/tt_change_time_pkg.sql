create or replace function ttb.tt_change_time_pkg$get_change_time(
  p_tt_variant_id numeric,
  p_tr_capacity_id numeric
)
  returns numeric
language plpgsql immutable
as $function$
declare
  l_res ttb.tt_change_time.change_time%type;
begin
  select change_time
  into l_res
  from ttb.tt_change_time
  where tt_variant_id =p_tt_variant_id
        and tr_capacity_id = p_tr_capacity_id;

  return l_res;
end;
$function$
;
