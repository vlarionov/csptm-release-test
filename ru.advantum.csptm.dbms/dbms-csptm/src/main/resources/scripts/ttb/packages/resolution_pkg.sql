CREATE OR REPLACE FUNCTION ttb.resolution_pkg$agreed()
  RETURNS ttb.resolution.resolution_id%TYPE
IMMUTABLE
LANGUAGE plpgsql
AS
$$
BEGIN
  RETURN 1;
END;
$$;

CREATE OR REPLACE FUNCTION ttb.resolution_pkg$disagreed()
  RETURNS ttb.resolution.resolution_id%TYPE
IMMUTABLE
LANGUAGE plpgsql
AS
$$
BEGIN
  RETURN 2;
END;
$$;