CREATE OR REPLACE FUNCTION ttb.jf_tr_mode_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.tr_mode AS
$body$
declare
   l_r ttb.tr_mode%rowtype;
begin
  /*mode_id И tr_mode_id сделаны так специально в надежде на разумное отделения режимов ТС от режимов труда водителей*/


   l_r.tr_mode_id 		:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'tr_mode_id', true), jofl.jofl_pkg$extract_number(p_attr, 'mode_id', true));
   l_r.break_count 		:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'break_count', true),0);
   l_r.to_duration_max 	:= jofl.jofl_pkg$extract_number(p_attr, 'to_duration_max', true);
   l_r.to_duration_min 	:= jofl.jofl_pkg$extract_number(p_attr, 'to_duration_min', true);
   l_r.tr_mode_prior_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_mode_prior_id', true);

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tr_mode_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$ 
declare
  l_mode_id ttb.mode.mode_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'mode_id', true);
begin 
 open p_rows for 
      select
        md.mode_id,
        md.mode_name,
        t.tr_mode_id,
        t.break_count,
        t.to_duration_max,
        t.to_duration_min,
		t.tr_mode_prior_id
      from ttb.tr_mode t
      join ttb.mode md on md.mode_id = t.tr_mode_id
      where t.tr_mode_id = l_mode_id;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tr_mode_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tr_mode%rowtype;
begin 
   l_r := ttb.jf_tr_mode_pkg$attr_to_rowtype(p_attr);

   update ttb.tr_mode set 
          break_count = l_r.break_count,
          to_duration_max = l_r.to_duration_max, 
          to_duration_min = l_r.to_duration_min,
		  tr_mode_prior_id = l_r.tr_mode_prior_id
   where 
          tr_mode_id = l_r.tr_mode_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tr_mode_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tr_mode%rowtype;
begin 
   l_r := ttb.jf_tr_mode_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.tr_mode where  tr_mode_id = l_r.tr_mode_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tr_mode_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tr_mode%rowtype;
begin 
   l_r := ttb.jf_tr_mode_pkg$attr_to_rowtype(p_attr);

   insert into ttb.tr_mode select l_r.*;

   return null;
end;
 $function$
;