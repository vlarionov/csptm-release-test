CREATE OR REPLACE FUNCTION ttb.jf_tt_variant4check_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
begin 
 open p_rows for 
            select  
              ttv.tt_variant_id
             ,ttv.route_variant_id 
      		 ,ttv.ttv_name  
             ,'[' || json_build_object('CAPTION', ttv.ttv_name, 'JUMP',
               		 json_build_object('METHOD', 'ttb.tt_variant_view_only', 'ATTR', '{"f_tt_variant_id":"' || ttv.tt_variant_id || '"}')) || ']' as jump_to_tt_variant
             ,r.route_num
             ,trt.name as tr_type_name
             ,lower(ttv.action_period) as ap_b
             ,upper(ttv.action_period) as ap_e
             ,string_agg(ct.tag_name, ';') as agg_tag_name
             ,string_agg(ct.tag_name_short, ';') as agg_tag_name_short  
             ,st.tt_status_name  
             ,ttb.jf_tt_variant4check_pkg$of_get_ttv_check_result(ttv.tt_variant_id) as check_result
             ,ttb.jf_tt_variant4check_pkg$of_get_ttv_doable(ttv.tt_variant_id) as doable       
            from ttb.tt_variant ttv
            join ttb.norm n on n.norm_id=ttv.norm_id
            join ttb.tt_status st on st.tt_status_id = ttv.tt_status_id
            join rts.route_variant rv on ttv.route_variant_id = rv.route_variant_id
            join rts.route r on rv.route_id = r.route_id
            join core.tr_type trt on trt.tr_type_id = r.tr_type_id
            left join ttb.tt_calendar c on c.tt_variant_id = ttv.tt_variant_id
            left join ttb.calendar_tag ct on ct.calendar_tag_id = c.calendar_tag_id
        where ttv.tt_status_id not in  ( ttb.jf_tt_status_pkg$cn_cancel_status_id()
        								,ttb.jf_tt_status_pkg$cn_arch_status_id() )
 		  and not ttv.sign_deleted
		  and ttv.parent_tt_variant_id is null
        group by 
        	  ttv.tt_variant_id
      		 ,ttv.ttv_name  
             ,r.route_num
             ,trt.name
             ,lower(ttv.action_period)
             ,upper(ttv.action_period)
             ,st.tt_status_name;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-----------------------------------------------------------------------------------------Варианты расписаний для проверки
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant4check_pkg$of_get_ttv_check_result (
  p_tt_variant_id numeric
)
RETURNS text AS
$body$
declare 
 lt_check_result text;
begin 
  with q as (
      select 
      		case ttvcc.check_result 
            	when ttb.jf_ttv_check_condition_pkg$cn_passed() then 1
                else 0
            end check_result  
       from ttb.ttv_check_condition ttvcc
       join ttb.tt_check_condition ttcc on ttcc.tt_check_condition_id = ttvcc.tt_check_condition_id
      where ttvcc.tt_variant_id = p_tt_variant_id
        and not ttvcc.sign_deleted
      order by ttcc.check_num
      )
  select string_agg(q.check_result::text, ' ')
       into lt_check_result            
       from q
           ;
        
    return lt_check_result;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-----------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant4check_pkg$of_get_ttv_doable (
  p_tt_variant_id numeric
)
RETURNS text AS
$body$
declare 
 lt_doable 	  	text;
 larr_check   	boolean[];
 lb_not_define 	boolean;
begin 
   select array_agg(array[case ttvcc.check_result 
            	      when ttb.jf_ttv_check_condition_pkg$cn_passed() then true
                      when ttb.jf_ttv_check_condition_pkg$cn_not_passed() then false
                      else null::boolean
            	    end])
     into larr_check         
     from ttb.ttv_check_condition ttvcc
    where ttvcc.tt_variant_id = p_tt_variant_id
      and ttvcc.is_doable
	  and not ttvcc.sign_deleted
  	;
    
    select count(1)::int::boolean
    into lb_not_define
    from unnest(larr_check) s(a)
    where a is null; 
    
   if lb_not_define then
   	lt_doable := 'Неопределенно';    
   elsif array[false] <@ (larr_check) then	
    lt_doable := 'Неисполнимое';
   else
 	lt_doable := 'Исполнимое'; 
   end if;
         
   return lt_doable;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-----------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant4check_pkg$of_start_ttv_check (
	p_id_account numeric
   ,p_attr text)
RETURNS text AS
$body$
declare 
 ln_rand_result 	smallint;
 cur_cnd			record;
 ln_tt_variant_id	ttb.ttv_check_condition.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true); 
 lt_ttv_name		text := jofl.jofl_pkg$extract_varchar(p_attr, 'ttv_name', true); 
begin  
 for cur_cnd in (
  select * from 
  ttb.ttv_check_condition ttvcc
  where ttvcc.is_active
     and ttvcc.tt_variant_id = ln_tt_variant_id
     )
     loop     
      update ttb.ttv_check_condition 
         set check_result = ttb.jf_tt_variant4check_pkg$get_rnd_val( ttb.jf_ttv_check_condition_pkg$cn_passed()
         															,ttb.jf_ttv_check_condition_pkg$cn_not_passed() )       
         where ttv_check_condition_id = cur_cnd.ttv_check_condition_id
      ;   
     end loop;   
       
     return  jofl.jofl_util$cover_result('message', 'Проверка варианта расписания № <b>' || lt_ttv_name || ' </b> проведена.');
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant4check_pkg$get_rnd_val (
  p_low 	numeric,
  p_hight	numeric
)
RETURNS numeric AS
$body$
declare 
 ln_rand_result smallint;
begin  
   	select floor(random()*(p_hight-p_low+1))+p_low
 	into ln_rand_result; 
        
    return  ln_rand_result;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;