CREATE OR REPLACE FUNCTION ttb.jf_ttv_oper_cp_type_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$

declare
begin
 open p_rows for
      select
        ttv_oper_cp_type_id,
        param_name,
        param_desc
      from ttb.ttv_oper_cp_type;
end;
$$;


CREATE OR REPLACE FUNCTION ttb.jf_ttv_oper_cp_type_pkg$cn_interval_tolerance()
RETURNS int
	LANGUAGE sql
AS $$
select 1::int;
$$;