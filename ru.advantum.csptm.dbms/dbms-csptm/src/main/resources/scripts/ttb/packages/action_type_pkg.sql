﻿-- из парка на ОП
create or replace function ttb.action_type_pkg$park_to_stop()
  returns smallint as 'select 11:: smallint' language sql immutable;
-- с ОП в парк
create or replace function ttb.action_type_pkg$stop_to_park()
  returns smallint as 'select 12:: smallint' language sql immutable;
-- из пункта обеда на ОП
create or replace function ttb.action_type_pkg$lunch_to_stop()
  returns smallint as 'select 13:: smallint' language sql immutable;
-- с ОП на обед
create or replace function ttb.action_type_pkg$stop_to_lunch()
  returns smallint as 'select 14:: smallint' language sql immutable;
-- из пункта ремонта на ОП
create or replace function ttb.action_type_pkg$repair_to_stop()
  returns smallint as 'select 15:: smallint' language sql immutable;
-- с ОП на пункт ремонта
create or replace function ttb.action_type_pkg$stop_to_repair()
  returns smallint as 'select 16:: smallint' language sql immutable;
-- с АЗС на ОП
create or replace function ttb.action_type_pkg$gs_to_stop()
  returns smallint as 'select 17:: smallint' language sql immutable;
-- с ОП на АЗС
create or replace function ttb.action_type_pkg$stop_to_gs()
  returns smallint as 'select 18:: smallint' language sql immutable;
-- с отстойной площадки на ОП
create or replace function ttb.action_type_pkg$parking_to_stop()
  returns smallint as 'select 20:: smallint' language sql immutable;
-- с ОП на отстойную площадку
create or replace function ttb.action_type_pkg$stop_to_parking()
  returns smallint as 'select 21:: smallint' language sql immutable;
-- Производственный рейс
create or replace function ttb.action_type_pkg$production_round()
  returns smallint as 'select 25:: smallint' language sql immutable;
-- Технологический рейс
create or replace function ttb.action_type_pkg$technical_round()
  returns smallint as 'select 26:: smallint' language sql immutable;
-- Межрейсовая стоянка
create or replace function ttb.action_type_pkg$bn_round_break()
  returns smallint as 'select 24:: smallint' language sql immutable;
-- Обед (перерыв)
create or replace function ttb.action_type_pkg$dinner()
  returns smallint as 'select 22:: smallint' language sql immutable;
-- Обед (технологический рейс)
create or replace function ttb.action_type_pkg$lunch()
  returns smallint as 'select 58:: smallint' language sql immutable;
-- Отстой
create or replace function ttb.action_type_pkg$pause()
  returns smallint as 'select 23:: smallint' language sql immutable;
-- Отстой с обедом
create or replace function ttb.action_type_pkg$pause_with_dinner()
  returns smallint as 'select 35:: smallint' language sql immutable;
-- Межрейсовая стоянка(базовая)
create or replace function ttb.action_type_pkg$bn_round_break_base()
  returns smallint as 'select 27:: smallint' language sql immutable;
-- Межрейсовая стоянка(регулировочная)
create or replace function ttb.action_type_pkg$bn_round_break_reg()
  returns smallint as 'select 28:: smallint' language sql immutable;
-- Межрейсовая стоянка(корректировочная)
create or replace function ttb.action_type_pkg$bn_round_break_corr()
  returns smallint as 'select 29:: smallint' language sql immutable;  

-- Пересменка
create or replace function ttb.action_type_pkg$change_driver()
  returns smallint as 'select 64:: smallint' language sql immutable;
  
-- Переключение
create or replace function ttb.action_type_pkg$switch()
  returns smallint as 'select 67:: smallint' language sql immutable;



-- Рейс
create or replace function ttb.action_type_pkg$round()
  returns smallint as 'select 31:: smallint' language sql immutable;
-- Перерыв
create or replace function ttb.action_type_pkg$break()
  returns smallint as 'select 32:: smallint' language sql immutable;
-- ТО (перерыв)
create or replace function ttb.action_type_pkg$maintenance()
  returns smallint as 'select 33:: smallint' language sql immutable;  
-- Ремонт (перерыв)
create or replace function ttb.action_type_pkg$repair()
  returns smallint as 'select 73:: smallint' language sql immutable;
-- Ремонт (технологический рейс к/от место ремонта)
create or replace function ttb.action_type_pkg$repair_tech_round()
  returns smallint as 'select 59:: smallint' language sql immutable;

-- Основной АБ
create or replace function ttb.action_type_pkg$main_round_ab()
  returns smallint as 'select 37:: smallint' language sql immutable;
-- Основной БА
create or replace function ttb.action_type_pkg$main_round_ba()
  returns smallint as 'select 38:: smallint' language sql immutable;

-- Укороченный АБ
create or replace function ttb.action_type_pkg$short_round_ab()
  returns smallint as 'select 39:: smallint' language sql immutable;
-- Укороченный БА
create or replace function ttb.action_type_pkg$short_round_ba()
  returns smallint as 'select 40:: smallint' language sql immutable;
-- Удлиненный АБ
create or replace function ttb.action_type_pkg$long_round_ab()
  returns smallint as 'select 41:: smallint' language sql immutable;
-- Удлиненный БА
create or replace function ttb.action_type_pkg$long_round_ba()
  returns smallint as 'select 42:: smallint' language sql immutable;
-- Экспрессный АБ
create or replace function ttb.action_type_pkg$express_round_ab()
  returns smallint as 'select 43:: smallint' language sql immutable;
-- Экспрессный БА
create or replace function ttb.action_type_pkg$express_round_ba()
  returns smallint as 'select 44:: smallint' language sql immutable;
-- Переключенный АБ
create or replace function ttb.action_type_pkg$switch_round_ab()
  returns smallint as 'select 45:: smallint' language sql immutable;
-- Переключенный БА
create or replace function ttb.action_type_pkg$switch_round_ba()
  returns smallint as 'select 46:: smallint' language sql immutable; 
-- Маневровый АБ
create or replace function ttb.action_type_pkg$maneuver_ab()
  returns smallint as 'select 51:: smallint' language sql immutable;
-- Маневровый БА  
create or replace function ttb.action_type_pkg$maneuver_ba()
  returns smallint as 'select 52:: smallint' language sql immutable;    
  
  
  
  
-- Парк
create or replace function ttb.action_type_pkg$depo()
  returns smallint as 'select 57:: smallint' language sql immutable;
-- Маневровый
create or replace function ttb.action_type_pkg$maneuver()
  returns smallint as 'select 19:: smallint' language sql immutable;

-- Укороченный рейс
create or replace function ttb.action_type_pkg$short_round()
  returns smallint as 'select 2:: smallint' language sql immutable;
-- Удлинненый рейс
create or replace function ttb.action_type_pkg$long_round()
  returns smallint as 'select 3:: smallint' language sql immutable;
-- Экспрессный рейс
create or replace function ttb.action_type_pkg$express_round()
  returns smallint as 'select 4:: smallint' language sql immutable;


-- Основной рейс ('00') без направления
create or replace function ttb.action_type_pkg$main_round()
  returns smallint as 'select 1:: smallint' language sql immutable;


-- Простой по причине инцидента
create or replace function ttb.action_type_pkg$incident_downtime()
  returns smallint as 'select 74:: smallint' language sql immutable;

------------------------
DROP FUNCTION ttb."action_type_pkg$get_table_action_type"(smallint);

CREATE OR REPLACE FUNCTION ttb.action_type_pkg$get_table_action_type(p_type_id in smallint)
  RETURNS TABLE(
    actiontype_id smallint,
    parenttype_id smallint,
    actiontype_name  text,
    actiontype_code  text,
    actionlevel int,
    actionpath text,
    parentname text
  )
LANGUAGE plpgsql IMMUTABLE
AS $function$ 
declare

begin
  RETURN QUERY
  with recursive root_type (action_type_id, parent_type_id, action_type_name, action_type_desc, action_type_code, level, path, cycle) as (
    select t.action_type_id, t.parent_type_id, t.action_type_name, t.action_type_desc,t.action_type_code, 1, array[t.action_type_name], false
    from  ttb.action_type t
    where t.parent_type_id = p_type_id::SMALLINT
    union
    select atp.action_type_id, atp.parent_type_id, atp.action_type_name, atp.action_type_desc, atp.action_type_code, rt.level+1, rt.path||atp.action_type_name,   atp.action_type_name = ANY(path)
    from  ttb.action_type atp
      join root_type rt on  rt.action_type_id = atp.parent_type_id
  )
  select root_type.action_type_id,
    root_type.parent_type_id,
    root_type.action_type_name,
    --root_type.action_type_desc,
    root_type.action_type_code,
    root_type.level,
    root_type.path::text,
    (select action_type_name from ttb.action_type atp_parent where  atp_parent.action_type_id = root_type.parent_type_id ) as parent_name
  from root_type
  where  NOT cycle;


end;
$function$
;

-- возращает буквенный код типа рейса  для формирования URL
create or replace function ttb.action_type_pkg$get_url_round_type(p_type_id smallint)
  returns text
language plpgsql immutable
as
$function$
declare
  l_r text;
begin
  if p_type_id = ttb.action_type_pkg$production_round()
  then
    l_r := 'rr_code';
  else
    l_r := 'rnr_code';
  end if;
  return l_r;
end;
$function$;



------------- проверка принадлежности типа действия к родительскому типу
create or replace function ttb.action_type_pkg$is_parent_type(p_type_id in smallint, p_parent_type_id in smallint)  returns boolean
language plpgsql immutable
as $function$
declare
  l_res boolean;
begin
  select count(1) into  l_res
  from ttb.action_type_pkg$get_table_action_type(p_parent_type_id)
  where actiontype_id=p_type_id;

  return l_res;

end;
$function$
;
------------- проверка принадлежности типа действия к родительскому типу
create or replace function ttb.action_type_pkg$is_parent_type(p_type_id in smallint
  , p_parent_type_id in smallint[])  returns boolean
language plpgsql immutable
as $function$
declare
  l_res boolean;
begin
  with t as (
select unnest (p_parent_type_id) a)
select count(1)  into  l_res
  from t,
      ttb.action_type_pkg$get_table_action_type(a)
  where actiontype_id=p_type_id;

  return l_res;

end;
$function$
;
------------- проверка, что тип односится к производственным рейсам
create or replace function ttb.action_type_pkg$is_production_round(p_type_id in smallint)  returns boolean
language plpgsql immutable
as $function$
declare
begin
  return ttb.action_type_pkg$is_parent_type(p_type_id, ttb.action_type_pkg$production_round());

end;
$function$
;
------------- проверка, что тип односится к технологическим рейсам
create or replace function ttb.action_type_pkg$is_technical_round(p_type_id in smallint)  returns boolean
language plpgsql immutable
as $function$
declare
begin
  return ttb.action_type_pkg$is_parent_type(p_type_id, ttb.action_type_pkg$technical_round());

end;
$function$
;
------------- проверка, что тип односится к межрейсовым стоянкам
create or replace function ttb.action_type_pkg$is_bn_round_break(p_type_id in smallint)  returns boolean
language plpgsql immutable
as $function$
declare
begin
  return ttb.action_type_pkg$is_parent_type(p_type_id, ttb.action_type_pkg$bn_round_break());

end;
$function$
;

------------- проверка, что тип односится к обеду
create or replace function ttb.action_type_pkg$is_dinner(p_type_id in smallint)  returns boolean
language plpgsql immutable
as $function$
declare
begin
  return p_type_id = ttb.action_type_pkg$dinner()
;

end;
$function$
;

------------- проверка, что тип односится к обеду
create or replace function ttb.action_type_pkg$is_all_pause(p_type_id in smallint)  returns boolean
language plpgsql immutable
as $function$
declare
begin
  return p_type_id in ( ttb.action_type_pkg$dinner(), ttb.action_type_pkg$pause());



end;
$function$
;

------------- проверка, что тип односится к  рейсам
create or replace function ttb.action_type_pkg$is_round(p_type_id in smallint)  returns boolean
language plpgsql immutable
as $function$
declare
begin
  return ttb.action_type_pkg$is_parent_type(p_type_id, ttb.action_type_pkg$round());

end;
$function$
;

------------- проверка, что тип односится к  перерывам
create or replace function ttb.action_type_pkg$is_break(p_type_id in smallint)  returns boolean
language plpgsql immutable
as $function$
declare
begin
  return ttb.action_type_pkg$is_parent_type(p_type_id, ttb.action_type_pkg$break());

end;
$function$
;


------------- проверка, что тип односится к  рейсам в парк
create or replace function ttb.action_type_pkg$is_depo(p_type_id in smallint)  returns boolean
language plpgsql immutable
as $function$
declare
begin
  return ttb.action_type_pkg$is_parent_type(p_type_id, ttb.action_type_pkg$depo());

end;
$function$
;

------------- функция возвращает массив action_type_id, имеющих своим общим предком p_action_type
create or replace function ttb.action_type_pkg$get_action_type_list(p_action_type smallint, p_leaf_only boolean) RETURNS smallint[] 
AS $body$
declare
 l_result smallint[];
begin
/* 
возвращает массив с action_type_id, имеющих общим предком p_action_type;
p_leaf_only: возвращать только "листья" (true) или всех участников иерархии (false)
*/
 with recursive action_type_tree as (select ac.action_type_id,
                                            ac.parent_type_id,
                                            1 as row_level
                                      from ttb.action_type ac
                                      where ac.action_type_id = p_action_type
                                     union
                                     select acr.action_type_id,
                                            acr.parent_type_id,
                                            att.row_level+1 as row_level
                                      from ttb.action_type acr
                                           join action_type_tree att on att.action_type_id = acr.parent_type_id)
 select array(select att.action_type_id
 from action_type_tree att
 where not p_leaf_only
       or not exists (select 1
                      from action_type_tree lt
                      where lt.parent_type_id = att.action_type_id)) into l_result;
return l_result;

end;
$body$
LANGUAGE 'plpgsql';

------------------------
CREATE OR REPLACE FUNCTION ttb.action_type_pkg$get_child_action_type_list(p_parent_type_id in smallint)
  RETURNS smallint[]
LANGUAGE plpgsql IMMUTABLE
AS $function$
declare
    l_return smallint[];
begin
  with recursive root_type (action_type_id) as (
    select t.action_type_id
    from  ttb.action_type t
    where t.parent_type_id = p_parent_type_id
    union
    select atp.action_type_id
    from  ttb.action_type atp
      join root_type rt on  rt.action_type_id = atp.parent_type_id
  )
  select array_agg(root_type.action_type_id)
  into l_return
  from root_type;

  return l_return;
end;
$function$
;
---------------------------------
CREATE OR REPLACE FUNCTION ttb.action_type_pkg$to_sp (
  p_action_type_id smallint
)
RETURNS boolean AS
$body$
declare
  lb_res boolean;
begin

  select p_action_type_id 
  		in ( ttb.action_type_pkg$park_to_stop()
        	,ttb.action_type_pkg$lunch_to_stop()
            ,ttb.action_type_pkg$repair_to_stop()
  			,ttb.action_type_pkg$gs_to_stop()
            ,ttb.action_type_pkg$parking_to_stop()
            )
  into lb_res;
  
  return lb_res;

end;
$body$
LANGUAGE 'plpgsql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------
CREATE OR REPLACE FUNCTION ttb.action_type_pkg$from_sp (
  p_action_type_id smallint
)
RETURNS boolean AS
$body$
declare
  lb_res boolean;
begin

  select p_action_type_id 
  		in ( ttb.action_type_pkg$stop_to_park()
        	,ttb.action_type_pkg$stop_to_lunch()
            ,ttb.action_type_pkg$stop_to_repair()
  			,ttb.action_type_pkg$stop_to_gs()
            ,ttb.action_type_pkg$stop_to_parking()
            )
  into lb_res;
  
  return lb_res;

end;
$body$
LANGUAGE 'plpgsql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------
CREATE OR REPLACE FUNCTION ttb.action_type_pkg$is_ab (
  p_action_type_id smallint
)
RETURNS boolean AS
$body$
declare
  lb_res boolean;
begin

  select p_action_type_id 
  		in ( ttb.action_type_pkg$main_round_ab()
			,ttb.action_type_pkg$short_round_ab()
			,ttb.action_type_pkg$long_round_ab()
			,ttb.action_type_pkg$express_round_ab()
			,ttb.action_type_pkg$switch_round_ab()
			,ttb.action_type_pkg$maneuver_ab()    
            )
  into lb_res;
  
  return lb_res;

end;
$body$
LANGUAGE 'plpgsql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-----------------------------

CREATE OR REPLACE FUNCTION ttb.action_type_pkg$is_ba (
  p_action_type_id smallint
)
RETURNS boolean AS
$body$
declare
  lb_res boolean;
begin

  select p_action_type_id 
  		in ( ttb.action_type_pkg$main_round_ba()
			,ttb.action_type_pkg$short_round_ba()
			,ttb.action_type_pkg$long_round_ba()
			,ttb.action_type_pkg$express_round_ba()
			,ttb.action_type_pkg$switch_round_ba()
			,ttb.action_type_pkg$maneuver_ba()    
            )
  into lb_res;
  
  return lb_res;

end;
$body$
LANGUAGE 'plpgsql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;