﻿-- первое действие
create or replace function ttb.ref_group_kind_pkg$first_action()
  returns smallint as 'select 0:: smallint' language sql immutable;
-- завершающее действие
create or replace function ttb.ref_group_kind_pkg$last_action()
  returns smallint as 'select 99:: smallint' language sql immutable;
-- основное действие
create or replace function ttb.ref_group_kind_pkg$main_action()
  returns smallint as 'select 10:: smallint' language sql immutable;
--пересменка
create or replace function ttb.ref_group_kind_pkg$change_dr_action()
  returns smallint as 'select 11:: smallint' language sql immutable;
--переключение
create or replace function ttb.ref_group_kind_pkg$switch_action()
  returns smallint as 'select 20:: smallint' language sql immutable;
--переключение из парка
create or replace function ttb.ref_group_kind_pkg$switch_from_park_action()
  returns smallint as 'select 21:: smallint' language sql immutable;  
--переключение
create or replace function ttb.ref_group_kind_pkg$switch_from_park_action()
  returns smallint as 'select 21:: smallint' language sql immutable;

--обед
create or replace function ttb.ref_group_kind_pkg$dinner_action()
  returns smallint as 'select 30:: smallint' language sql immutable;
--отстой
create or replace function ttb.ref_group_kind_pkg$pause_action()
  returns smallint as 'select 70:: smallint' language sql immutable;
--ТО
create or replace function ttb.ref_group_kind_pkg$to_action()
  returns smallint as 'select 50:: smallint' language sql immutable;
