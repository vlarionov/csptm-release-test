CREATE OR REPLACE FUNCTION ttb."jf_norm_round_pkg$attr_to_rowtype"(p_attr text)
 RETURNS ttb.norm_round
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.norm_round%rowtype; 
begin 
   l_r.norm_round_id := jofl.jofl_pkg$extract_number(p_attr, 'norm_round_id', true); 
   l_r.norm_id := jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true); 
   l_r.round_id := jofl.jofl_pkg$extract_number(p_attr, 'round_id', true); 
   l_r.hour_from := jofl.jofl_pkg$extract_varchar(p_attr, 'hour_from', true); 
   l_r.hour_to := jofl.jofl_pkg$extract_varchar(p_attr, 'hour_to', true); 
   l_r.min_inter_round_stop_dur := jofl.jofl_pkg$extract_number(p_attr, 'min_inter_round_stop_dur', true); 
   l_r.reg_inter_round_stop_dur := jofl.jofl_pkg$extract_number(p_attr, 'reg_inter_round_stop_dur', true); 
   l_r.round_duration := jofl.jofl_pkg$extract_number(p_attr, 'round_duration', true);
   l_r.tr_capacity_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tr_capacity_id', true); 
   l_r.stop_item_id := jofl.jofl_pkg$extract_number(p_attr, 'stop_item_id', true); 
   l_r.tr_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tr_type_id', true); 

   return l_r;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_norm_round_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$ 
declare
  l_norm_id ttb.norm.norm_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true);

  l_time_from TIME := (jofl.jofl_pkg$extract_varchar(p_attr, 'F_time_from', true))::TIME;
  l_time_to TIME := (jofl.jofl_pkg$extract_varchar(p_attr, 'F_time_to', true))::TIME;

  l_time_to_min   INTEGER :=  EXTRACT(HOUR FROM l_time_to) * 60 + EXTRACT(HOUR FROM l_time_to);
  l_capacity_id BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_tr_capacity_id', true);
  l_tr_type_id BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_tr_type_id', true);
  l_round_code TEXT[] := jofl.jofl_pkg$extract_tarray(p_attr, 'F_code', true);
  l_move_dir BIGINT[] := jofl.jofl_pkg$extract_tarray(p_attr, 'F_move_direction_id', true);
begin
 IF l_time_to_min = 0 -- передано 00:00 но интерпретируем как 24 часа
   THEN l_time_to_min = 24 * 60;
 END IF;
 open p_rows for
 select
   nr.norm_round_id,
   nr.norm_id,
   nr.round_id,
   (nr.hour_from)::text as hour_from,
   (nr.hour_to)::text as hour_to,
   nr.min_inter_round_stop_dur,
   nr.reg_inter_round_stop_dur,
   nr.round_duration,
   nr.tr_capacity_id,
   nr.stop_item_id,
   nr.tr_type_id,
   tt.name as tr_type_name,
   cp.short_name as tr_capacity_short_name,
   rnd.code as round_code,
   md.move_direction_name,
   s.name as stop_name,
   net.norm_edit_type_name,
   (select distinct on (r.round_id) route.route_num
    from ttb.norm_round t1
      JOIN rts.round r ON r.round_id = t1.round_id
      JOIN rts.route route ON route.current_route_variant_id = r.route_variant_id
           AND route.tr_type_id = t1.tr_type_id
    where t1.norm_id = nr.norm_id AND t1.round_id = nr.round_id) as route_num,
    CASE WHEN ((round_duration IS NOT NULL) AND (round_duration<>0)) THEN round(rnd.length / round_duration * 3.6, 1) /*в километры в час */
         ELSE NULL
    END speed
 from ttb.norm_round nr
   join ttb.norm_edit_type net on net.norm_edit_type_id = nr.norm_edit_type_id
   join core.tr_type tt on tt.tr_type_id = nr.tr_type_id
   join core.tr_capacity cp on cp.tr_capacity_id = nr.tr_capacity_id
   join rts.round rnd on rnd.round_id = nr.round_id
   join ttb.action_type act on act.action_type_id = rnd.action_type_id
   join rts.move_direction md on md.move_direction_id = rnd.move_direction_id
   join rts.stop_item si on si.stop_item_id = nr.stop_item_id
   join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
   join rts.stop s on s.stop_id = sl.stop_id
  where nr.norm_id = l_norm_id
  AND ((l_time_from IS NULL AND l_time_to IS NULL) OR (nr.hour_from >=l_time_from
       AND CASE WHEN (EXTRACT(HOUR FROM nr.hour_to) * 60 + EXTRACT(HOUR FROM nr.hour_to)) = 0 THEN 24*60 <= l_time_to_min
                ELSE (EXTRACT(HOUR FROM nr.hour_to) * 60 + EXTRACT(HOUR FROM nr.hour_to)) <= l_time_to_min END ) )
  AND (nr.tr_capacity_id = ANY(l_capacity_id) OR (array_length(l_capacity_id, 1) IS NULL) )
  AND ((nr.tr_type_id = ANY(l_tr_type_id)) OR (array_length(l_tr_type_id, 1) IS NULL))
  AND ((rnd.code = ANY(l_round_code)) OR (array_length(l_round_code, 1) IS NULL))
  AND ((md.move_direction_id = ANY(l_move_dir)) OR (array_length(l_move_dir,1) IS NULL))
  ORDER BY
    cp.qty ASC,
    rnd.code ASC,
    md.move_direction_id ASC,
    s.name;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_norm_round_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.norm_round%rowtype;
begin 
   l_r := ttb.jf_norm_round_pkg$attr_to_rowtype(p_attr);

  IF l_r.round_duration IS NULL THEN
    SELECT round_duration INTO l_r.round_duration FROM ttb.norm_round where round_id = l_r.round_id;
  END IF;
  update ttb.norm_round set
          round_id = l_r.round_id,
          hour_from = l_r.hour_from, 
          hour_to = l_r.hour_to, 
          min_inter_round_stop_dur = l_r.min_inter_round_stop_dur, 
          reg_inter_round_stop_dur = l_r.reg_inter_round_stop_dur, 
          round_duration = l_r.round_duration, 
          tr_capacity_id = l_r.tr_capacity_id, 
          stop_item_id = l_r.stop_item_id, 
          tr_type_id = l_r.tr_type_id,
          norm_edit_type_id = ttb.norm_edit_type$hand()
   where 
          norm_round_id = l_r.norm_round_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_norm_round_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.norm_round%rowtype;
begin 
   l_r := ttb.jf_norm_round_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.norm_round where  norm_round_id = l_r.norm_round_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_norm_round_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.norm_round%rowtype;
begin 
   l_r := ttb.jf_norm_round_pkg$attr_to_rowtype(p_attr);
   l_r.norm_round_id := nextval('ttb.norm_round.norm_round_id_seq');
   l_r.norm_edit_type_id := ttb.norm_edit_type$hand();

   insert into ttb.norm_round select l_r.*;

   return null;
end;
 $function$
;



create or REPLACE function ttb."jf_norm_round_pkg$of_set_new"  (p_id_account numeric, p_attr text) returns boolean
LANGUAGE plpgsql
AS $$
DECLARE
  l_norm_round_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'norm_round_id', true);
  l_reg_inter_round_stop_dur BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'reg_inter_round_stop_dur', true);
  l_min_inter_round_stop_dur BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'min_inter_round_stop_dur', true);
BEGIN
  UPDATE ttb.norm_round SET
    reg_inter_round_stop_dur = l_reg_inter_round_stop_dur * 60,
    min_inter_round_stop_dur = l_min_inter_round_stop_dur * 60
  WHERE norm_round_id = l_norm_round_id;
  return TRUE;
END;
$$;

create or replace function ttb.jf_norm_round_pkg$MSG_SET_NEW(p_id_account numeric, p_attr text)
    returns text
language plpgsql
as $function$
declare
    tt_variants text;
    l_r_norm_id integer := jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true);
begin
    if (exists(select *
               from ttb.tt_variant tv
               where tv.norm_id = l_r_norm_id)
    )
    then
        select 'Изменяемый норматив используется в следующих расписаниях<br><table>  <tr> <td>Вариант расписания </td>  <td> Маршрут </td> </tr>' || string_agg(' <tr>' || '<td>' || tv.ttv_name || '</td>  <td>' || r.route_num || '</td> </tr>', '') || '</table><br>Продолжить?'
        into tt_variants
        from ttb.tt_variant tv
            join rts.route_variant rv on tv.route_variant_id = rv.route_variant_id
            join rts.route r on rv.route_variant_id = r.current_route_variant_id
        where tv.norm_id = l_r_norm_id and tv.tt_status_id<>ttb.jf_tt_status_pkg$cn_cancel_status_id();
    else tt_variants :=null;
    end if;
    return '{"title": "Варианты расписания",
        "message": "' || tt_variants || '"}';
end;
$function$;