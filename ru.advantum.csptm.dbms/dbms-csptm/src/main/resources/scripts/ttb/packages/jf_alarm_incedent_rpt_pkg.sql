create or replace function ttb.jf_alarm_incedent_rpt_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
	language plpgsql
as $$
declare
  dt_b_DT TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'dt_b_DT', true);
  dt_e_DT TIMESTAMP := jofl.jofl_pkg$extract_date(p_attr, 'dt_e_DT', true);
--  F_tr_type_id SMALLINT := jofl.jofl_pkg$extract_number(p_attr, 'F_tr_type_id', true);
  f_list_tr_type_id SMALLINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_tr_type_id', true);
  f_list_depo_id SMALLINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', true);
  f_list_route_id INTEGER[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_route_id', true);
  f_list_tr_id INTEGER[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_tr_id', true);

begin
/* 
 IF F_tr_type_id IS NULL  THEN
    OPEN p_rows FOR
    SELECT
      NULL    depo_name,
      NULL    route_num,
      NULL    garage_num,
      NULL    tab_num,
      NULL    incedent_time,
	  null    incident_finish_time,
      NULL    dispatcher_name
    where 1=2;
    RETURN;
  END IF;
  */
 open p_rows for
      select
        de.name_short depo_name,
        r.route_num route_num,
        tr.garage_num garage_num,
        dr.tab_num tab_num,
        i.time_start incedent_time,
		i.time_finish incident_finish_time,
        i.account_id dispatcher_name
      from ttb.incident i
        join core.tr tr on tr.tr_id=i.tr_id
        join core.entity de on de.entity_id=tr.depo_id
        left join ttb.order_list ol on ol.tr_id=i.tr_id and i.time_start between ol.time_from and ol.time_to
        left join rts.route_variant rv on rv.route_variant_id=ol.route_variant_id
        left join rts.route r on r.route_id=rv.route_id
        left join core.driver dr on dr.driver_id=ol.driver_id
      where i.incident_type_id=1
      and i.time_start between dt_b_DT and dt_e_DT
      and (array_length(f_list_tr_id , 1) is null or i.tr_id = any(f_list_tr_id))
      and (array_length(f_list_depo_id , 1) is null or tr.depo_id = any(f_list_depo_id))
        --------------
      and (array_length(f_list_route_id, 1) is null or r.route_id = any (f_list_route_id) or r.route_id is null)
      order by sign(coalesce(r.route_id, -1)) desc,
               incedent_time desc
-- временное ограничение на количество записей
  limit 10000
    ;
END ;
$$
