CREATE OR REPLACE FUNCTION ttb."jf_tt_calendar_pkg$attr_to_rowtype"(p_attr text)
 RETURNS ttb.tt_calendar
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r 				ttb.tt_calendar%rowtype; 
   ln_tt_status_id 	ttb.tt_variant.tt_status_id%type;
begin 
   l_r.tt_calendar_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_calendar_id', true); 
   l_r.calendar_tag_id := jofl.jofl_pkg$extract_varchar(p_attr, 'calendar_tag_id', true); 
   l_r.tt_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true); 
   l_r.is_include := jofl.jofl_pkg$extract_boolean(p_attr, 'is_include', true); 
   
   select tt_status_id
   into ln_tt_status_id
   from ttb.tt_variant
   where tt_variant_id = l_r.tt_variant_id;
   
   if not ttb.jf_tt_variant_pkg$of_get_policy(l_r.tt_variant_id) 
   	  and ln_tt_status_id not in ( ttb.jf_tt_status_pkg$cn_on_approving_status_id()
      							  ,ttb.jf_tt_status_pkg$cn_approved_status_id() )
    then perform ttb.jf_tt_variant_pkg$of_raise_edit_calendar_err();
   end if;       

   return l_r;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_calendar_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$ 
declare
  l_tt_variant_id ttb.tt_variant.tt_variant_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
begin 
 open p_rows for
 select
   cl.tt_calendar_id,
   cl.calendar_tag_id,
   cl.tt_variant_id,
   cl.is_include,
   ctag.tag_name,
   ctag.tag_name_short
 from ttb.tt_calendar cl
   join ttb.calendar_tag ctag on ctag.calendar_tag_id = cl.calendar_tag_id
 where cl.tt_variant_id = l_tt_variant_id;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_calendar_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tt_calendar%rowtype;
begin 
   l_r := ttb.jf_tt_calendar_pkg$attr_to_rowtype(p_attr);

   update ttb.tt_calendar set 
          calendar_tag_id = l_r.calendar_tag_id, 
          tt_variant_id = l_r.tt_variant_id, 
          is_include = l_r.is_include
   where 
          tt_calendar_id = l_r.tt_calendar_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_calendar_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tt_calendar%rowtype;
begin 
   l_r := ttb.jf_tt_calendar_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.tt_calendar where  tt_calendar_id = l_r.tt_calendar_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_calendar_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tt_calendar%rowtype;
begin 
   l_r := ttb.jf_tt_calendar_pkg$attr_to_rowtype(p_attr);
   l_r.tt_calendar_id := nextval( 'ttb.tt_calendar_tt_calendar_id_seq' );

   insert into ttb.tt_calendar select l_r.*;

   return null;
end;
 $function$
;