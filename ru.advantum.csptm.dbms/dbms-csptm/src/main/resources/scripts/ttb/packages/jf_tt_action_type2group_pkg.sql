CREATE OR REPLACE FUNCTION ttb.jf_tt_action_type2group_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.tt_action_type2group AS
$body$
declare 
   l_r 				ttb.tt_action_type2group%rowtype; 
   ln_tt_variant_id ttb.tt_variant.tt_variant_id%type;   
begin 
   l_r.tt_action_type2group_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_action_type2group_id', true); 
   l_r.tt_action_group_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_action_group_id', true); 
   l_r.tt_action_type_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_action_type_id', true); 
   l_r.order_num := jofl.jofl_pkg$extract_varchar(p_attr, 'order_num', true); 
   l_r.plan_time := case 
   					  when jofl.jofl_pkg$extract_number(p_attr, 'plan_time', true) = 0
   					  then null
                      else jofl.jofl_pkg$extract_number(p_attr, 'plan_time', true)
   					end; 
					
   select tv.tt_variant_id
   into ln_tt_variant_id
   from ttb.tt_variant tv
   join ttb.tt_action_group tag on tag.tt_variant_id = tv.tt_variant_id
   where tag.tt_action_group_id = l_r.tt_action_group_id;                    
                    
   if not ttb.jf_tt_variant_pkg$of_get_policy(ln_tt_variant_id)
     then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if;  					

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_action_type2group_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r 		ttb.tt_action_type2group%rowtype;
   ln_cnt	smallint;
begin 
   l_r := ttb.jf_tt_action_type2group_pkg$attr_to_rowtype(p_attr);
   
   l_r.tt_action_type2group_id := nextval ('ttb.tt_action_type2group_tt_action_type2group_id_seq');
   
   select count(1)
   into ln_cnt
   from ttb.tt_action_type2group tat2g
   where tat2g.tt_action_group_id = l_r.tt_action_group_id
   and tat2g.order_num = l_r.order_num;
   
   if ln_cnt > 0 then
   	RAISE EXCEPTION '<<Данное значение последовательности уже присутствует в группе!>>';
   else
   	insert into ttb.tt_action_type2group select l_r.*;
   end if;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_action_type2group_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 l_tt_action_group_id int := jofl.jofl_pkg$extract_number(p_attr, 'tt_action_group_id', true);  
begin 
 open p_rows for 
      select 
        tat2g.tt_action_type2group_id, 
        tat2g.tt_action_group_id, 
        tat2g.tt_action_type_id, 
        tat2g.order_num, 
        tat.plan_time,
        --at.action_type_name
        --concat_ws(' - ', at.action_type_name, tat.r_code) as action_type_name,
        case 
        	when ttb.action_type_pkg$is_technical_round(at.action_type_id) 
             then 
             concat_ws(' - ',(
               concat_ws(' - ', at.action_type_name, tat.r_code)),
                 (select s.name as stop_name
                   from ttb.tt_at_round_stop trs
                     join rts.stop_item2round sir on sir.stop_item2round_id = trs.stop_item2round_id
                     join rts.stop_item si on si.stop_item_id =sir.stop_item_id
                     join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
                     join rts.stop s on s.stop_id = sl.stop_id
                     join ttb.tt_at_round ar on ar.tt_at_round_id = trs.tt_at_round_id
                     --join ttb.tt_action_type act on act.tt_action_type_id = ar.tt_action_type_id
                   where ar.tt_action_type_id = tat2g.tt_action_type_id
                    order by sir.order_num desc
                    limit 1)
                    )
             else 
               concat_ws(' - ', at.action_type_name, tat.r_code)
        end as action_type_name
      from ttb.tt_action_type2group tat2g
      join ttb.tt_action_type tat on tat.tt_action_type_id = tat2g.tt_action_type_id
      join ttb.action_type at on at.action_type_id = tat.action_type_id
      where tat2g.tt_action_group_id = l_tt_action_group_id
  order by tat2g.order_num; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_action_type2group_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.tt_action_type2group%rowtype;
begin 
   l_r := ttb.jf_tt_action_type2group_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.tt_action_type2group where  tt_action_type2group_id = l_r.tt_action_type2group_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_action_type2group_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.tt_action_type2group%rowtype;
   ln_cnt	smallint;   
begin 
   l_r := ttb.jf_tt_action_type2group_pkg$attr_to_rowtype(p_attr);
   
   select count(1)
   into ln_cnt
   from ttb.tt_action_type2group tat2g
   where tat2g.tt_action_group_id = l_r.tt_action_group_id
   and tat2g.order_num = l_r.order_num
   and tat2g.tt_action_type2group_id != l_r.tt_action_type2group_id;

   if ln_cnt > 0 then
   	RAISE EXCEPTION '<<Данное значение последовательности уже присутствует в группе!>>';
   else
     update ttb.tt_action_type2group set 
            tt_action_group_id = l_r.tt_action_group_id, 
            tt_action_type_id = l_r.tt_action_type_id, 
            order_num = l_r.order_num, 
            plan_time = l_r.plan_time
     where 
            tt_action_type2group_id = l_r.tt_action_type2group_id;
     
     update ttb.tt_action_type set 
            plan_time = l_r.plan_time
     where 
            tt_action_type_id = l_r.tt_action_type_id;
   end if;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
----------------------------------------------------------