CREATE OR REPLACE FUNCTION ttb.jf_rep_average_intensity4period_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare

begin

 open p_rows for 
     select
	null as hour_int, /* Часовые интервалы */	 
 null as interval_fact_avg, /* Средн. факт. интервал */
 null as interval_fact_max, /* Макс. факт. интервал */
 null as interval_plan_avg, /* Средн. план. интервал */
 null as interval_plan_max, /* Макс. план. интервал */
 null as pass_count, /* Количество перевезенных пассажиров */
 null as passplace_count, /* Количество предоставленных мест для пассажиров */
 null as prod_round_cnt_fact, /* Количество произв. рейсов факт. */
 null as prod_round_cnt_plan /* Количество произв. рейсов план. */	 
;
end;
$function$
;

