CREATE OR REPLACE FUNCTION ttb."norm_edit_type$default"()
 RETURNS integer
 LANGUAGE sql
 IMMUTABLE
AS $function$select 2;$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."norm_edit_type$hand"()
 RETURNS integer
 LANGUAGE sql
 IMMUTABLE
AS $function$select 3;$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."norm_edit_type$auto"()
 RETURNS integer
 LANGUAGE sql
 IMMUTABLE
AS $function$select 1;$function$
;