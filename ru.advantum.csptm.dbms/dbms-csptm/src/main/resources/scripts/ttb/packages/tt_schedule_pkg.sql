﻿------------- функция возвращает массив  tt_schedule_id, имеющих своим общим предком p_tt_schedule_id
create or replace function ttb.tt_schedule_pkg$get_schedule_list(p_tt_schedule_id numeric) returns int[]
as $body$
declare
 l_res int[];
begin

  with recursive sch as (
    select t.tt_schedule_id, t.parent_schedule_id,  1 as row_level
    from ttb.tt_schedule t
    where t.tt_schedule_id = p_tt_schedule_id
    union
    select t.tt_schedule_id, t.parent_schedule_id,  sch.row_level+1 as row_level
    from ttb.tt_schedule t
      join sch on sch.tt_schedule_id = t.parent_schedule_id
  )
  select array(select tt_schedule_id from sch) into l_res;


  return l_res;

end;
$body$
language 'plpgsql' immutable
;


------------- функция возвращает массив  tt_schedule_id, имеющих своим общим потомком p_tt_schedule_id
create or replace function ttb.tt_schedule_pkg$get_schedule_parent_list(p_tt_schedule_id numeric) returns int[]
as $body$
declare
  l_res int[];
begin

  with recursive sch as (
    select t.tt_schedule_id, t.parent_schedule_id,  1 as row_level
    from ttb.tt_schedule t
    where t.tt_schedule_id = p_tt_schedule_id
    union
    select t.tt_schedule_id, t.parent_schedule_id,  sch.row_level+1 as row_level
    from ttb.tt_schedule t
      join sch on sch.parent_schedule_id = t.tt_schedule_id
  )
  select array(select tt_schedule_id from sch) into l_res;


  return l_res;

end;
$body$
language 'plpgsql' immutable ;


------------- функция возвращает основную последовательность для варианта расписания
create or replace function ttb.tt_schedule_pkg$get_main_schedule_id(p_tt_variant_id numeric) returns numeric
as $body$
declare
  l_res numeric;
begin

  select sh.tt_schedule_id
  into l_res
  from ttb.tt_schedule sh
  where sh.tt_variant_id = p_tt_variant_id
        and sh.parent_schedule_id is null
  limit 1;

  return l_res;

end;
$body$
language 'plpgsql' immutable ;

