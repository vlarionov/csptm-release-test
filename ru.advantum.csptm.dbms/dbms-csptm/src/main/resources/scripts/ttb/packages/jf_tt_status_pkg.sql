CREATE OR REPLACE FUNCTION ttb."jf_tt_status_pkg$attr_to_rowtype"(p_attr text)
 RETURNS ttb.tt_status
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tt_status%rowtype; 
begin 
   l_r.tt_status_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tt_status_id', true); 
   l_r.tt_status_name := jofl.jofl_pkg$extract_varchar(p_attr, 'tt_status_name', true); 

   return l_r;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_status_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare
begin
 open p_rows for
      select
        tt_status_id,
        tt_status_name
      from ttb.tt_status;
end;
$function$
;
-----------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_status_pkg$cn_cancel_status_id (
)
RETURNS smallint AS
$body$

begin   
   return 4;
end;
$body$
LANGUAGE 'plpgsql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-----------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_status_pkg$cn_work_status_id (
)
RETURNS smallint AS
$body$

begin   
   return 1;
end;
$body$
LANGUAGE 'plpgsql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-----------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_status_pkg$cn_approved_status_id (
)
RETURNS smallint AS
$body$

begin   
   return 3;
end;
$body$
LANGUAGE 'plpgsql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_status_pkg$cn_on_approving_status_id (
)
RETURNS smallint AS
$body$

begin   
   return 2;
end;
$body$
LANGUAGE 'plpgsql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_status_pkg$cn_arch_status_id (
)
RETURNS smallint AS
$body$

begin   
   return 5;
end;
$body$
LANGUAGE 'plpgsql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_status_pkg$cn_active_status_id (
)
RETURNS smallint AS
$body$

begin   
   return 6;
end;
$body$
LANGUAGE 'plpgsql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

----------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_status_pkg$cn_tt_oper_close_status_id (
)
RETURNS smallint AS
$body$

begin
   return 7;
end;
$body$
LANGUAGE 'plpgsql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;