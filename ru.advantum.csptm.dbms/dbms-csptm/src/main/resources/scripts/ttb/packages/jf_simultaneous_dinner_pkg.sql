create or replace function ttb."jf_simultaneous_dinner_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
language plpgsql
as $$
declare
  l_stop_id bigint := jofl.jofl_pkg$extract_number(p_attr, 'REP_stop_item_id', true);
  l_end_dt timestamp := jofl.jofl_pkg$extract_date(p_attr, 'END_DT', true);
begin
  open p_rows for
    with act as
      (  select t.ttv_name, t.tt_variant_id, t.route_variant_id, t.norm_id,
                stop_item_id,
                tt_action_id,
                t.tt_out_id, t.tt_out_num,
                out_tr,
                out_small ,
                out_middle,
                out_big,
                out_huge,
                item_range,
                time_begin,
                time_end,
                action_type_id,
                max(tt_out_07_09_count) filter(where tt_out_07_09 is not null) over(partition by t.tt_variant_id) tt_out_07_09_count
                 -- за неимением count(distinct) over()
         from
          (
            select t.ttv_name, t.tt_variant_id, t.route_variant_id, t.norm_id,
                   st2r.stop_item_id,
                   t.tt_out_id, t.tt_out_num,
                   nullif(t.out_tr, 0) out_tr,
                   nullif(t.out_small, 0) out_small ,
                   nullif(t.out_middle, 0) out_middle,
                   nullif(t.out_big, 0) out_big,
                   nullif(t.out_huge, 0) out_huge,
                   case when ti.item_range && range_07_09 then t.tt_out_id end tt_out_07_09,
                   dense_rank() over(partition by tt_variant_id order by case when ti.item_range && range_07_09 then t.tt_out_id end nulls last) tt_out_07_09_count,
                   ti.*
            from ( select t.*, v.ttv_name, v.action_period, v.route_variant_id, v.norm_id,
                          sum(1 + case when tr.is_second_car then 1 else 0 end)   over ttv out_tr,
                          sum(1 + case when tr.is_second_car then 1 else 0 end) filter(where t.tr_capacity_id = core.tr_capacity_pkg$small()) over ttv out_small,
                          sum(1 + case when tr.is_second_car then 1 else 0 end) filter(where t.tr_capacity_id = core.tr_capacity_pkg$middle()) over ttv out_middle,
                          sum(1 + case when tr.is_second_car then 1 else 0 end) filter(where t.tr_capacity_id = core.tr_capacity_pkg$big()) over ttv out_big,
                          sum(1 + case when tr.is_second_car then 1 else 0 end) filter(where t.tr_capacity_id = core.tr_capacity_pkg$huge()) over ttv out_huge
                   from ttb.tt_variant v join ttb.tt_out t on v.tt_variant_id = t.tt_variant_id
                        left join ttb.tt_out2tt_tr o2tr on o2tr.tt_out_id = t.tt_out_id
                        left join ttb.tt_tr tr on tr.tt_tr_id = o2tr.tt_tr_id
                   where core.from_utc(l_end_dt) <@ v.action_period
                     and v.parent_tt_variant_id is null
                     and v.tt_status_id in (ttb."tt_status_pkg$active"(),  ttb."tt_status_pkg$arch"())
                     and exists ( select 1
                                  from ttb.calendar c join ttb.calendar_item ci on c.calendar_id = ci.calendar_id
                                       join ttb.tt_calendar tc on tc.calendar_tag_id = ci.calendar_tag_id
                                  where c.dt = core.from_utc(l_end_dt)
                                    and tc.tt_variant_id = v.tt_variant_id
                                )
                   window ttv as (partition by t.tt_variant_id)
                 ) t
                 join ttb.tt_action ta on t.tt_out_id = ta.tt_out_id
                 join (select tsrange(ti.time_begin, ti.time_end, '[]') item_range, ti.* from  ttb.tt_action_item ti where time_begin < time_end) ti on ta.tt_action_id = ti.tt_action_id
                 join rts.stop_item2round st2r on ti.stop_item2round_id = st2r.stop_item2round_id,
                 (select tsrange('1900-01-01 07:00:00','1900-01-01 09:00:00') range_07_09) r0709
            where exists(select 1 from ttb.dr_shift sh where sh.dr_shift_id = ti.dr_shift_id)
           ) t
       )
  select q4.tt_variant_id,
         max(q4.route_variant_id) route_variant_id,
         max(q4.ttv_name) ttv_name,
         row_number() over() rownum,
         case when grouping(tt_variant_id) = 1 then 'Итого' else max(route_num) end route_num,
         case when grouping(tt_variant_id) = 0 then max(obed_route) else max(q4.max_obed_all) end obed_route,
         array_to_string(array[sum(q4.out_tr), sum(q4.out_huge), sum(q4.out_big), sum(q4.out_middle), sum(q4.out_small) ], '/', '-') tr_count,
         case when grouping(tt_variant_id) = 0 then concat(max(st1.name), ' - ', max(st2.name)) end end_stop,
         max(tt_out_07_09_count) tt_out_07_09_count,
         max(q4.dur) dur,
         case when grouping(tt_variant_id) = 0 then max(q4.route_interval) end route_interval,
         round(case when sum(q4.route_interval) <> 0 then sum(60/q4.route_interval) end, 1) route_freq
  from
    (
      select q3.tt_variant_id,
             q3.route_variant_id,
             q3.ttv_name,
             q3.obed_route,
             q3.max_obed_all,
             q3.out_tr, q3.out_small, q3.out_middle, q3.out_big, q3.out_huge,
             tt_out_07_09_count,
             (q3.rdur).p_duration dur,
             round(case when tt_out_07_09_count = 0 then 0.0 else (q3.rdur).p_duration/2/tt_out_07_09_count end, 1) route_interval,
             (q3.rdur).p_first_stop_item first_stop_item,
             (q3.rdur).p_last_stop_item last_stop_item
      from
        (
          select q2.tt_variant_id,
                 q2.route_variant_id,
                 q2.norm_id,
                 ttv_name,
                 q2.obed_route,
                 q2.max_obed_all,
                 q2.out_tr, q2.out_small, q2.out_middle, q2.out_big, q2.out_huge,
                 q2.tt_out_07_09_count,
                 ttb.over_round_duration(q2.norm_id ,q2.route_variant_id) rdur
          from
            (
              select q.*,
                     max(obed_route) over(partition by tt_variant_id) max_obed,
                     max(obed_all) over() max_obed_all,
                     first_value(time_begin) over(partition by tt_variant_id, obed_route order by time_begin) first_max_obed
                from (
                    select q1.ttv_name, q1.tt_variant_id, q1.route_variant_id, q1.norm_id, q1.tt_out_num,
                           q1.time_begin, q1.time_end,
                           count(*) obed_all,
                           count(*) filter(where q1.tt_variant_id = q2.tt_variant_id) obed_route,
                           q1.out_tr, q1.out_small, q1.out_middle, q1.out_big, q1.out_huge,
                           q1.tt_out_07_09_count
                    from act q1 left join act q2  on  q1.time_begin <@ q2.item_range
                    where q1.stop_item_id = l_stop_id
                      and q1.action_type_id = 22
                      and q2.stop_item_id = l_stop_id
                      and q2.action_type_id = 22
                    group by q1.tt_variant_id, q1.route_variant_id, q1.norm_id, q1.ttv_name,
                             q1.tt_out_id, q1.tt_out_num,
                             q1.time_begin, q1.time_end,
                             q1.out_tr, q1.out_small, q1.out_middle, q1.out_big, q1.out_huge, q1.tt_out_07_09_count
                ) q
            ) q2
          where obed_route = max_obed
            and time_begin = first_max_obed
        ) q3
    ) q4
    join rts.route_variant rv on rv.route_variant_id = q4.route_variant_id
    join rts.route r on r.route_id = rv.route_id
    join rts.stop_item sti1 on q4.first_stop_item = sti1.stop_item_id
    join rts.stop_item sti2 on q4.last_stop_item = sti2.stop_item_id
    join rts.stop_location stl1 on stl1.stop_location_id = sti1.stop_location_id
    join rts.stop_location stl2 on stl2.stop_location_id = sti2.stop_location_id
    join rts.stop st1 on stl1.stop_id = st1.stop_id
    join rts.stop st2 on stl2.stop_id = st2.stop_id
  group by rollup(tt_variant_id)
  having count(*) > 0;
end;
$$;
