create or replace function ttb.jf_stop_item_out_report_pkg$of_rows(p_id_account numeric, out p_rows refcursor, p_attr text)
    returns refcursor
language plpgsql
as $function$
declare
    l_f_INPLACE_k  integer := jofl.jofl_pkg$extract_number(p_attr, 'F_INPLACE_K', true);
    l_f_tr_type_id integer := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id', true);
    l_f_DT         date := jofl.jofl_pkg$extract_date(p_attr, 'f_DT', true);
    l_stop_type_id smallint;
begin
    if l_f_INPLACE_k = 0
    then l_stop_type_id = rts.stop_type_pkg$finish_a();
    else l_stop_type_id = rts.stop_type_pkg$finish_b();
    end if;
    open p_rows for
    with day_type as (
        select ct.tag_name
        from ttb.calendar cal
            join ttb.calendar_item ci on cal.calendar_id = ci.calendar_id
            join ttb.calendar_tag ct on ct.calendar_tag_id = ci.calendar_tag_id
            left join ttb.calendar_tag_group ctg on ctg.calendar_tag_group_id = ct.calendar_tag_group_id
        where cal.dt = l_f_DT and ct.calendar_tag_group_id = 7
    )
    select
        distinct
        case when l_f_DT isnull
            then current_date :: text
        else to_char(l_f_DT , 'dd.mm.yyyy') end as p_date,
        dt.tag_name                                p_type_day,
        t.name                                     tr_type_name,
        t.tr_type_id                               p_tr_type_id,
        depo.entity_id                             p_depo_id,
        depo.name_full                             p_depo_name,
        ter.entity_id                              p_ter_id,
        ter.name_short                             p_ter,
        l_stop_type_id                             p_stop_type_id
    from ttb.tt_variant tv
        join rts.route_variant r on tv.route_variant_id = r.route_variant_id
        join rts.round rr on r.route_variant_id = rr.route_variant_id
        join day_type dt on 1 = 1

        join ttb.tt_out tto on tv.tt_variant_id = tto.tt_variant_id
        join ttb.tt_tr tt on tv.tt_variant_id = tt.tt_variant_id
        join core.depo2territory d2t on tt.depo2territory_id = d2t.depo2territory_id
        join core.entity depo on depo.entity_id = d2t.depo_id
        join core.entity ter on ter.entity_id = d2t.territory_id

        join core.depo cd on d2t.depo_id = cd.depo_id
        join core.tr_type t on cd.tr_type_id = t.tr_type_id
    where
        lower(tv.action_period) :: date <= l_f_DT and coalesce(upper(tv.action_period), l_f_DT) :: date >= l_f_DT and
        rr.code = '00'
        and t.tr_type_id = l_f_tr_type_id;
end;
$function$;

create or replace function ttb.jf_stop_item_out_report_pkg$report(
    p_id_account   text,
    timezone       text,
    p_date         text,
    p_stop_type_id text,
    p_tr_type_id   text,
    p_ter_id       text,
    p_ter          text,
    p_depo_id      text,
    p_depo_name    text,
    p_type_day     text
)
    returns table(
        type_day  text,
        date      text,
        stop_name text,
        depo_ter  text,
        depo_name text,
        route_num text,
        time5     text,
        time6     text,
        time7     text,
        time8     text,
        time9     text,
        time10    text,
        time11    text,
        time12    text,
        time13    text,
        time14    text,
        time15    text,
        time16    text,
        time17    text,
        time18    text,
        time19    text,
        time20    text,
        time21    text,
        time22    text,
        time23    text,
        time0     text,
        time1     text,
        time2     text
    )
language plpgsql
as $function$
declare
    l_date date :=to_date(p_date, 'DD.MM.YYYY');
    l_action_type_id smallint;
begin
    select case when p_stop_type_id=10::text then 37::smallint else 38::smallint end into l_action_type_id;
    return query
    select
        p_type_day,
        p_date,
        case when p_stop_type_id=10::text then ' А' else ' Б' end ,
        case when p_tr_type_id::smallint=core.tr_type_pkg$tm() then p_ter else '' end,
        p_depo_name,
        case when route.route_num is null
            then 'Итого'
        else route.route_num :: text end as route_num,
        count(distinct tta.tt_action_id)
            filter (where tta.dt_action :: time between '5h' :: interval and '6h' :: interval) :: text   as time5,
        count(distinct tta.tt_action_id)
            filter (where tta.dt_action :: time between '6h' :: interval and '7h' :: interval) :: text   as time6,
        count(distinct tta.tt_action_id)
            filter (where tta.dt_action :: time between '7h' :: interval and '8h' :: interval) :: text   as time7,
        count(distinct tta.tt_action_id)
            filter (where tta.dt_action :: time between '8h' :: interval and '9h' :: interval) :: text   as time8,
        count(distinct tta.tt_action_id)
            filter (where tta.dt_action :: time between '9h' :: interval and '10h' :: interval) :: text  as time9,
        count(distinct tta.tt_action_id)
            filter (where tta.dt_action :: time between '10h' :: interval and '11h' :: interval) :: text as time10,
        count(distinct tta.tt_action_id)
            filter (where tta.dt_action :: time between '11h' :: interval and '12h' :: interval) :: text as time11,
        count(distinct tta.tt_action_id)
            filter (where tta.dt_action :: time between '12h' :: interval and '13h' :: interval) :: text as time12,
        count(distinct tta.tt_action_id)
            filter (where tta.dt_action :: time between '13h' :: interval and '14h' :: interval) :: text as time13,
        count(distinct tta.tt_action_id)
            filter (where tta.dt_action :: time between '14h' :: interval and '15h' :: interval) :: text as time14,
        count(distinct tta.tt_action_id)
            filter (where tta.dt_action :: time between '15h' :: interval and '16h' :: interval) :: text as time15,
        count(distinct tta.tt_action_id)
            filter (where tta.dt_action :: time between '16h' :: interval and '17h' :: interval) :: text as time16,
        count(distinct tta.tt_action_id)
            filter (where tta.dt_action :: time between '17h' :: interval and '18h' :: interval) :: text as time17,
        count(distinct tta.tt_action_id)
            filter (where tta.dt_action :: time between '18h' :: interval and '19h' :: interval) :: text as time18,
        count(distinct tta.tt_action_id)
            filter (where tta.dt_action :: time between '19h' :: interval and '20h' :: interval) :: text as time19,
        count(distinct tta.tt_action_id)
            filter (where tta.dt_action :: time between '20h' :: interval and '21h' :: interval) :: text as time20,
        count(distinct tta.tt_action_id)
            filter (where tta.dt_action :: time between '21h' :: interval and '22h' :: interval) :: text as time21,
        count(distinct tta.tt_action_id)
            filter (where tta.dt_action :: time between '22h' :: interval and '23h' :: interval) :: text as time22,
        count(distinct tta.tt_action_id)
            filter (where tta.dt_action :: time between '23h' :: interval and '24h' :: interval) :: text as time23,
        count(distinct tta.tt_action_id)
            filter (where tta.dt_action :: time between '0h' :: interval and '1h' :: interval) :: text   as time0,
        count(distinct tta.tt_action_id)
            filter (where tta.dt_action :: time between '1h' :: interval and '2h' :: interval) :: text   as time1,
        count(distinct tta.tt_action_id)
            filter (where tta.dt_action :: time between '2h' :: interval and '3h' :: interval) :: text   as time2
    from ttb.tt_variant tv
        join ttb.tt_out tto on tv.tt_variant_id = tto.tt_variant_id
        join ttb.tt_action tta on tto.tt_out_id = tta.tt_out_id

        join ttb.tt_out2tt_tr to2tt on tto.tt_out_id = to2tt.tt_out_id
        join ttb.tt_tr tr on to2tt.tt_tr_id = tr.tt_tr_id

        join core.depo2territory d2t on tr.depo2territory_id = d2t.depo2territory_id

        join rts.route_variant rv on tv.route_variant_id = rv.route_variant_id
        join rts.route route on rv.route_id = route.route_id

        join rts.round r on rv.route_variant_id = r.route_variant_id
        join rts.stop_item2round si2r on r.round_id = si2r.round_id
        join rts.stop_item2round_type si2rt on si2rt.stop_item2round_id = si2r.stop_item2round_id
        join rts.stop_item_type sit on si2rt.stop_item_type_id = sit.stop_item_type_id

        join ttb.tt_calendar cl on tv.tt_variant_id = cl.tt_variant_id
        join ttb.calendar_tag ct on ct.calendar_tag_id = cl.calendar_tag_id
        join ttb.calendar_item ci on ct.calendar_tag_id = ci.calendar_tag_id
        join ttb.calendar cal on ci.calendar_id = cal.calendar_id

    where
        r.code = '00' and
        (tv.tt_status_id = ttb.jf_tt_status_pkg$cn_arch_status_id() or tv.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id()) and
        cal.dt = l_date and
        ct.calendar_tag_group_id = ttb.calendar_pkg$get_weekday_type_tag_group_id() and
        tta.action_type_id = l_action_type_id and
        not tv.sign_deleted and
        sit.stop_type_id = p_stop_type_id :: smallint and
        tv.parent_tt_variant_id isnull and
        tto.parent_tt_out_id isnull and
        (lower(tv.action_period) :: date <= l_date and
         coalesce(upper(tv.action_period), l_date) :: date >= l_date)
        and (
            (p_tr_type_id :: smallint = core.tr_type_pkg$tm() and d2t.territory_id = p_ter_id :: bigint) or
            (p_tr_type_id :: smallint <> core.tr_type_pkg$tm() and d2t.depo_id = p_depo_id :: bigint)
        )
    group by rollup (route.route_num);
end;
$function$;

DROP FUNCTION ttb."jf_stop_item_out_report_pkg$report"(text,text,text,text,text,text,text,text,text,text)