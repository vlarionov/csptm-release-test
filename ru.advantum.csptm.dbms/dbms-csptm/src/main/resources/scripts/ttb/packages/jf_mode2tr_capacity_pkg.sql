CREATE OR REPLACE FUNCTION ttb.jf_mode2tr_capacity_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.mode2tr_capacity AS
$body$
declare 
   l_r ttb.mode2tr_capacity%rowtype; 
begin 
   l_r.mode2tr_capacity_id := jofl.jofl_pkg$extract_varchar(p_attr, 'mode2tr_capacity_id', true); 
   l_r.mode_id := jofl.jofl_pkg$extract_varchar(p_attr, 'mode_id', true); 
   l_r.tr_capacity_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tr_capacity_id', true); 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_mode2tr_capacity_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.mode2tr_capacity%rowtype;
begin 
   l_r := ttb.jf_mode2tr_capacity_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.mode2tr_capacity where  mode2tr_capacity_id = l_r.mode2tr_capacity_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_mode2tr_capacity_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.mode2tr_capacity%rowtype;
begin 
   l_r := ttb.jf_mode2tr_capacity_pkg$attr_to_rowtype(p_attr);
   
   l_r.mode2tr_capacity_id := nextval('ttb.mode2tr_capacity_mode2tr_capacity_id_seq');

   insert into ttb.mode2tr_capacity select l_r.*;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_mode2tr_capacity_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
   l_r ttb.mode2tr_capacity%rowtype;
begin 
   l_r := ttb.jf_mode2tr_capacity_pkg$attr_to_rowtype(p_attr);
 open p_rows for 
      select 
        m2tc.mode2tr_capacity_id, 
        m2tc.mode_id, 
        m2tc.tr_capacity_id,
        tc.qty as cap_qty,
        tc.short_name as cap_sname,
        tc.full_name  as cap_fname
      from ttb.mode2tr_capacity m2tc
      join core.tr_capacity tc on tc.tr_capacity_id = m2tc.tr_capacity_id
      where mode_id = l_r.mode_id; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_mode2tr_capacity_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.mode2tr_capacity%rowtype;
begin 
   l_r := ttb.jf_mode2tr_capacity_pkg$attr_to_rowtype(p_attr);

   update ttb.mode2tr_capacity set 
         /* mode_id = l_r.mode_id, */
          tr_capacity_id = l_r.tr_capacity_id
   where 
          mode2tr_capacity_id = l_r.mode2tr_capacity_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------------------------
--------------------------------------------------------------------------