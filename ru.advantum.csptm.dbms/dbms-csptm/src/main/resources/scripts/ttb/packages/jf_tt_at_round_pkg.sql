CREATE OR REPLACE FUNCTION ttb."jf_tt_at_round_pkg$attr_to_rowtype"(p_attr text)
 RETURNS ttb.tt_at_round
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tt_at_round%rowtype;
   ln_tt_variant_id ttb.tt_variant.tt_variant_id%type;    
begin 
   l_r.tt_at_round_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_at_round_id', true); 
   l_r.round_id := jofl.jofl_pkg$extract_number(p_attr, 'round_id', true); 
   l_r.tt_action_type_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_action_type_id', true); 
   l_r.depo2territory_id := jofl.jofl_pkg$extract_varchar(p_attr, 'depo2territory_id', true);
   
   select tat.tt_variant_id
   into ln_tt_variant_id
   from ttb.tt_at_round tar 
   join ttb.tt_action_type tat on tat.tt_action_type_id = tar.tt_action_type_id
   where tar.tt_at_round_id = l_r.tt_at_round_id
   limit 1;  
   
   if not ttb.jf_tt_variant_pkg$of_get_policy(ln_tt_variant_id)
    then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if;     

   return l_r;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_at_round_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$ 
declare
  l_tt_action_type_id ttb.tt_action_type.tt_action_type_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'tt_action_type_id', true);
begin 
 open p_rows for
 select
   ttar.tt_at_round_id,
   ttar.round_id,
   ttar.tt_action_type_id,
   ttar.depo2territory_id,
   rnd.code as round_code,
   rnd.move_direction_id,
   md.move_direction_name,
   depo_ent.name_short as depo_name_short,
   terr_ent.name_short as terr_name_short,
   s2t.depo_id,
   s2t.territory_id
 from ttb.tt_at_round ttar
   join rts.round rnd on rnd.round_id = ttar.round_id
   join rts.move_direction md on md.move_direction_id = rnd.move_direction_id
   left join core.depo2territory s2t on s2t.depo2territory_id  =ttar.depo2territory_id
   left join core.entity depo_ent on depo_ent.entity_id = s2t.depo_id
   left join core.entity terr_ent on terr_ent.entity_id = s2t.territory_id
 where ttar.tt_action_type_id = l_tt_action_type_id;

end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_at_round_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tt_at_round%rowtype;
begin 
   l_r := ttb.jf_tt_at_round_pkg$attr_to_rowtype(p_attr);

   update ttb.tt_at_round set 
          round_id = l_r.round_id, 
          tt_action_type_id = l_r.tt_action_type_id, 
          depo2territory_id = l_r.depo2territory_id
   where 
          tt_at_round_id = l_r.tt_at_round_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_at_round_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tt_at_round%rowtype;
begin 
   l_r := ttb.jf_tt_at_round_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.tt_at_round where  tt_at_round_id = l_r.tt_at_round_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_at_round_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tt_at_round%rowtype;
begin 
   l_r := ttb.jf_tt_at_round_pkg$attr_to_rowtype(p_attr);

   insert into ttb.tt_at_round select l_r.*;

   return null;
end;
 $function$
;