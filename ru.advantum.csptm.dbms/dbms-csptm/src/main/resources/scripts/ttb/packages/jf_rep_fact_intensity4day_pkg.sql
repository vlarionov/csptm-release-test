CREATE OR REPLACE FUNCTION ttb.jf_rep_fact_intensity4day_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare

begin

 open p_rows for 
     select
 null as depo_name, /* Наименование ТП */
 null as route_num, /* Номер маршрута */
 null as out_num, /* Номер выхода */
 null as stop_name, /* Наименование ОП */
 null as interval_pred_vehicle, /* Интервал между текущим ТС и ранее ушедшим ТС с ОП */
 null as route_closetime_plan, /* Время закрытия маршрута план. */
 null as route_opentime_plan, /* Время открытия маршрута план. */
 null as stop_depart_deviation, /* Отклонение от расписания, минуты  */
 null as stop_depart_fact, /* Фактическое время отправления ТС с ОП */
 null as stop_depart_schedule /* Плановое время отправления ТС с ОП */
;
end;
$function$
;
