CREATE OR REPLACE FUNCTION ttb.jf_tt_period_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.tt_period AS
$body$
declare 
  l_r 					ttb.tt_period%rowtype;
  l_sec_b  				int:= jofl.jofl_pkg$extract_number(p_attr, 'sec_b', true);
  l_sec_e  				int:= jofl.jofl_pkg$extract_number(p_attr, 'sec_e', true);
  l_dt_action 			date;
  l_cnt 				smallint;
  l_parent_schedule_id 	ttb.tt_schedule.parent_schedule_id%type := jofl.jofl_pkg$extract_number(p_attr, 'parent_schedule_id', true);
  ln_tt_variant_id 		ttb.tt_variant.tt_variant_id%type; 
begin
   l_r.tt_period_id 	:= jofl.jofl_pkg$extract_number(p_attr, 'tt_period_id', true);
--   l_r.time_begin 	:= jofl.jofl_pkg$extract_date(p_attr, 'time_begin', true);
--   l_r.time_end 		:= jofl.jofl_pkg$extract_date(p_attr, 'time_end', true);
   l_r.movement_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'movement_type_id', true); 
   l_r.limit_time 		:= jofl.jofl_pkg$extract_number(p_attr, 'limit_time', true); 
   l_r.tt_schedule_id 	:= jofl.jofl_pkg$extract_number(p_attr, 'tt_schedule_id', true);
   l_r.interval_time 	:= jofl.jofl_pkg$extract_number(p_attr, 'interval_time', true);
   l_r.out_cnt 			:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'out_cnt', true), 0);


  select  case 
  			when ttv.parent_tt_variant_id is null 
              then '1900-01-01'::date 
            else lower(ttv.action_period)::date 
  		  end dt_action
  		 ,ttv.tt_variant_id
  into l_dt_action
  	  ,ln_tt_variant_id
  from ttb.tt_variant ttv
  join ttb.tt_schedule tts on tts.tt_variant_id = ttv.tt_variant_id
  where tts.tt_schedule_id = l_r.tt_schedule_id;
  
  if not ttb.jf_tt_variant_pkg$of_get_policy(ln_tt_variant_id)
    then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
  end if;  

  --начение "Интервал движения" в случае интервального и тактового движения должен быть больще 0
  if l_r.movement_type_id  in( ttb.jf_movement_type_pkg$cn_type_clock(),
                               ttb.jf_movement_type_pkg$cn_type_interval()) and l_r.interval_time<1 then
    raise exception '<<Значение "Интервал движения" в случае интервального и тактового движения должен быть больше 0>>';
  end if;


  if l_r.limit_time = 0 and l_r.interval_time > 0 then
     l_r.limit_time := round(l_r.interval_time/3);
  end if;


  l_r.time_begin := l_dt_action  + l_sec_b * interval '1 sec';
  l_r.time_end := l_dt_action  + l_sec_e * interval '1 sec';

  if l_r.movement_type_id  = ttb.jf_movement_type_pkg$cn_type_clock() then
    l_r.limit_time:=null;
  elsif l_r.movement_type_id  = ttb.jf_movement_type_pkg$cn_type_trfix() then
    if (select count(1)
        from ttb.tt_period p
        where p.tt_schedule_id = l_r.tt_schedule_id
          and p.tt_period_id != l_r.tt_period_id
          and p.time_end  < l_r.time_end) != 0 then
      raise exception '<<Тип движения "Фиксированное количество ТС" временно реализован только для первого периода>>';
    end if;
    if l_r.out_cnt = 0 then
       raise exception '<<Для типа движения "Фиксированное количество ТС"  необходимо задать количество выходов>>';
    end if;
    l_r.interval_time := ttb.jf_tt_period_pkg$get_interval (l_r.tt_schedule_id, l_r.out_cnt);
  elsif l_r.movement_type_id = ttb.jf_movement_type_pkg$cn_type_street() then
    l_r.limit_time := null;
    l_r.interval_time := 0;
  elseif l_r.movement_type_id = ttb.jf_movement_type_pkg$cn_type_interval() then
    if l_r.out_cnt = 0  and l_parent_schedule_id is not null then
       l_r.out_cnt := ttb.jf_tt_period_pkg$get_out_cnt (l_r.tt_schedule_id, l_r.interval_time);
    end if;
  end if;

  select count(1) 
  into l_cnt
  from ttb.tt_period per
  where tsrange(per.time_begin, per.time_end) && tsrange(l_r.time_begin, l_r.time_end)
        and per.tt_schedule_id = l_r.tt_schedule_id
        and per.tt_period_id != l_r.tt_period_id;

  if l_cnt > 0 then
    raise exception '<<Периоды не должны пересекаться>>';
  end if;

  return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_period_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  ln_tt_schedule_id ttb.tt_period.tt_schedule_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_schedule_id', true);
begin 
 open p_rows for

 with dt as
 (select  case when ttv.parent_tt_variant_id is null then '1900-01-01'::date else lower(ttv.action_period)::date end dt_action
  from ttb.tt_variant ttv
    join ttb.tt_schedule tts on tts.tt_variant_id = ttv.tt_variant_id
  where tts.tt_schedule_id = ln_tt_schedule_id
 )
 select
   p.tt_period_id,
   p.time_begin ,
   p.time_end,
   p.movement_type_id,
   p.limit_time,
   p.interval_time,
   mt.movement_type_name,
   extract(epoch from p.time_begin - dt.dt_action)  as sec_b,
   extract(epoch from p.time_end - dt.dt_action ) as sec_e,
   r.tr_type_id,
   r.route_id,
   p.tt_schedule_id,
   p.out_cnt,
   tts.parent_schedule_id,
   /*(select count(1) from ttb.tt_tr_period ttp where ttp.tt_period_id = p.tt_period_id) as out_cnt,*/
   case p.movement_type_id  when ttb.jf_movement_type_pkg$cn_type_clock() then 'P{RO},D{limit_time}'::text
   when ttb.jf_movement_type_pkg$cn_type_trfix() then 'P{RO},D{interval_time}'::text end as "ROW$POLICY"
 from ttb.tt_period p
   join ttb.tt_schedule tts on tts.tt_schedule_id = p.tt_schedule_id
   join ttb.movement_type mt on mt.movement_type_id = p.movement_type_id
   join ttb.tt_variant ttv on ttv.tt_variant_id = tts.tt_variant_id
   join rts.route_variant rv on ttv.route_variant_id = rv.route_variant_id
   join rts.route r on rv.route_id = r.route_id
   join dt on 1=1
 where tts.tt_schedule_id = ln_tt_schedule_id
 order by p.time_begin;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_period_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
  l_r ttb.tt_period%rowtype;
begin
   l_r := ttb.jf_tt_period_pkg$attr_to_rowtype(p_attr);

   update ttb.tt_period set 
          time_begin = l_r.time_begin, 
          time_end = l_r.time_end, 
          movement_type_id = l_r.movement_type_id, 
          limit_time = l_r.limit_time,  
          interval_time = l_r.interval_time,
          out_cnt = l_r.out_cnt
   where 
          tt_period_id = l_r.tt_period_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_period_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.tt_period%rowtype;
begin 
   l_r := ttb.jf_tt_period_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.tt_period where  tt_period_id = l_r.tt_period_id;
   delete from  ttb.tt_tr_period where tt_period_id = l_r.tt_period_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_period_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.tt_period%rowtype;
begin
   l_r := ttb.jf_tt_period_pkg$attr_to_rowtype(p_attr);
   l_r.tt_period_id := nextval('ttb.tt_period_tt_period_id_seq');

   insert into ttb.tt_period select l_r.*;

   return null;


end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-----------------------
--Рассчитать интервал из количества машин
----------------------
create or replace function ttb.jf_tt_period_pkg$get_interval (in p_tt_shedule_id numeric
                                                            , in p_out_cnt numeric
                                                            )
  returns numeric
as
$body$
declare
  l_dur smallint;
begin
  --средняя продолжительность основного действия
  l_dur:= ttb.norm_pkg$get_schedule_tm (p_tt_sсhedule_id =>p_tt_shedule_id
                                       ,p_ref_group_kind_id => ttb.ref_group_kind_pkg$main_action()::numeric
                                       );

  return coalesce(ceil((l_dur/p_out_cnt)/60::real)*60 , 0);
end;
$body$
language plpgsql volatile
cost 100;

-----------------------
--Рассчитать  количество машин из интервала
----------------------
create or replace function ttb.jf_tt_period_pkg$get_out_cnt (in p_tt_shedule_id numeric
                                                            , in p_interv numeric
                                                              )
  returns numeric
as
$body$
declare
  l_dur smallint;
begin
  --средняя продолжительность основного действия
  l_dur:= ttb.norm_pkg$get_schedule_tm (p_tt_sсhedule_id =>p_tt_shedule_id
  ,p_ref_group_kind_id => ttb.ref_group_kind_pkg$main_action()::numeric
  );

  return coalesce(ceil(l_dur/p_interv) , 0);
end;
$body$
language plpgsql volatile
cost 100;
