-- create or replace function ttb.jf_tt_performing_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
-- LANGUAGE plpgsql
-- AS $$
-- declare
-- begin
--   OPEN p_rows FOR
--   select null;
-- end;
-- $$;
create or replace function ttb.jf_tt_performing_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
  l_tr_type SMALLINT := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id', true);
  l_date_from DATE := jofl.jofl_pkg$extract_date(p_attr, 'P_REP_FROM_DT', true);
  l_date_to DATE := jofl.jofl_pkg$extract_date(p_attr, 'P_REP_TO_DT', true);
  l_rv BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'f_p_current_route_variant_id', true);
begin
  OPEN p_rows FOR
  select route_num, current_route_variant_id as rv_id, l_date_from as p_date_from, l_date_to as p_date_to
  from rts.route where tr_type_id = l_tr_type
  AND current_route_variant_id = l_rv/*IS NOT NULL*/ order by route_num asc;
end;
$$;

create or replace function ttb.jf_tt_performing_pkg$report(P_ID_ACCOUNT TEXT,
                                                           rv_id text,
                                                           P_REP_FROM_DT text,
                                                           P_REP_TO_DT text
                                                           )
  RETURNS TABLE (date_ text,
                 total text,
                 main text,
                 not_main text
  ) AS
$body$
declare
begin
  return query
  WITH RECURSIVE  a AS(
    select action_type_name, action_type_id, action_type_id::text idd  from ttb.action_type where parent_type_id IS NULL
    UNION ALL
    select a.action_type_name||' \ '||t.action_type_name as action_type_name,
           t.action_type_id, t.action_type_id::text||' \ '||idd as idd
    from ttb.action_type t
    JOIN a ON t.parent_type_id = a.action_type_id
  ),
      proiz AS (
        SELECT *, array_length(regexp_split_to_array(idd, '\\'),1) FROM a
        WHERE idd LIKE '% 25 \\%' AND array_length(regexp_split_to_array(idd, '\\'),1) = 4
    ),
      arrs AS (
        SELECT
          array(SELECT action_type_id FROM proiz WHERE idd ~ '\D1 \\')  AS main,
          -- основной
          array(SELECT action_type_id FROM proiz WHERE idd !~ '\D1 \\') AS not_main -- не основной
    ),
      days_series AS(
        select i::date from generate_series(P_REP_FROM_DT::DATE, P_REP_TO_DT::DATE, '1 day'::interval) i
    ),
      bb AS (select days_series.i,
               (select min(tt_variant.tt_variant_id)
                FROM ttb.tt_variant join ttb.v_tt_calendar_date
                    on v_tt_calendar_date.tt_variant_id = tt_variant.tt_variant_id
                where route_variant_id = rv_id::INTEGER
                      /* 203 маршрут*/ -- вариант маршрута --l_order_list.route_variant_id
                      and not sign_deleted
                      and parent_tt_variant_id is null
                      AND tt_variant.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id()
                      and tt_variant.action_period @> i::TIMESTAMP
                      and v_tt_calendar_date.dt = i
               ) from days_series
    ),
  final_table As (select bb.i as date_,
    (select count(*) from ttb.tt_variant v
      JOIN ttb.tt_out o ON o.tt_variant_id = v.tt_variant_id
      JOIN ttb.tt_action a ON a.tt_out_id = o.tt_out_id
      JOIN rts.route_variant rv ON rv.route_variant_id = v.route_variant_id
      JOIN rts.route r ON r.route_id = rv.route_id
    where parent_tt_variant_id IS NULL
          AND v.tt_variant_id = bb.min -- для примера (203 автобус)
          AND a.action_type_id = ANY(arrs.main)) as main,

    (select count(*) from ttb.tt_variant v
                      JOIN ttb.tt_out o ON o.tt_variant_id = v.tt_variant_id
                      JOIN ttb.tt_action a ON a.tt_out_id = o.tt_out_id
                      JOIN rts.route_variant rv ON rv.route_variant_id = v.route_variant_id
                      JOIN rts.route r ON r.route_id = rv.route_id
                    where parent_tt_variant_id IS NULL
                          AND v.tt_variant_id = bb.min -- для примера (203 автобус)
                          AND a.action_type_id = ANY(arrs.not_main)) as not_main
  from bb, arrs)
  select to_char(final_table.date_, 'YYYY.MM.DD') , (final_table.main + final_table.not_main)::TEXT as total,
    final_table.main::TEXT,
    final_table.not_main::TEXT
  from final_table;
end;
$body$
LANGUAGE 'plpgsql';

/*
-- рейсы и их статусы
WITH RECURSIVE a AS(
  select action_type_name, action_type_id, action_type_id::text idd  from ttb.action_type where parent_type_id IS NULL
  UNION ALL
  select a.action_type_name||' \ '||t.action_type_name as action_type_name,
    t.action_type_id,
         t.action_type_id::text||' \ '||idd as idd
  from ttb.action_type t
    JOIN a ON t.parent_type_id = a.action_type_id
),
    proiz AS (
      SELECT *, array_length(regexp_split_to_array(idd, '\\'),1) FROM a
      WHERE idd LIKE '% 25 \\%' AND array_length(regexp_split_to_array(idd, '\\'),1) = 4
  ),
    arrs AS (
      SELECT
        array(SELECT action_type_id FROM proiz WHERE idd ~ '\D1 \\')  AS main,
        -- основной
        array(SELECT action_type_id FROM proiz WHERE idd !~ '\D1 \\') AS not_main -- не основной
  )
-- Перечень выходов расписания оперативного
select ai.time_begin "Плановое", '2',si2r.order_num, '1',af.* ,rs.round_status_name, ar.round_status_id, a.tt_action_id, a.action_type_id ,v_oo.*
from ttb.v_tt_out_oper v_oo
  JOIN ttb.tt_action a ON a.tt_out_id = v_oo.tt_out_id
  JOIN ttb.tt_action_round ar ON ar.tt_action_id = a.tt_action_id
  JOIN ttb.round_status rs ON rs.round_status_id = ar.round_status_id

  JOIN ttb.tt_action_fact af ON af.tt_action_id = a.tt_action_id
  JOIN rts.stop_item2round si2r ON si2r.stop_item2round_id = af.stop_item2round_id
  JOIN ttb.tt_action_item ai ON ai.tt_action_id = a.tt_action_id
       AND ai.stop_item2round_id = af.stop_item2round_id
where v_oo.order_date > '2018-02-01' --BETWEEN '2018-01-05' AND '2018-01-15'
  AND a.action_type_id IN (SELECT unnest(main) FROM arrs)
  AND ar.round_status_id = 1
ORDER BY a.tt_action_id, af.time_fact_begin;

select * from ttb.tt_action_round where time_packet_begin::DATE > '2018-01-15'

select * from ttb.tt_action_round
where time_packet_begin IS NOT NUll
*/