create or replace function ttb.agreement_pkg$get_next_agreement(
      p_tt_variant_agreement_id ttb.tt_variant_agreement.tt_variant_agreement_id%type,
  out p_result                  ttb.tt_variant_agreement.tt_variant_agreement_id%type
)
stable
language plpgsql
as $$
begin
  select next_tva.tt_variant_agreement_id
  from ttb.tt_variant_agreement current_tva
    join ttb.agreement_group current_ag on current_tva.agreement_group_id = current_ag.agreement_group_id
    join ttb.tt_variant_agreement next_tva on current_tva.tt_variant_id = next_tva.tt_variant_id and
                                              current_tva.iteration_num = next_tva.iteration_num
    join ttb.agreement_group next_ag on next_tva.agreement_group_id = next_ag.agreement_group_id and
                                        current_ag.order_num < next_ag.order_num
  where current_tva.tt_variant_agreement_id = p_tt_variant_agreement_id
  order by next_ag.order_num
  limit 1
  into p_result;
end;
$$;

create or replace function ttb.agreement_pkg$get_previous_agreement(
      p_tt_variant_agreement_id ttb.tt_variant_agreement.tt_variant_agreement_id%type,
  out p_result                  ttb.tt_variant_agreement.tt_variant_agreement_id%type
)
stable
language plpgsql
as $$
begin
  select previous_tva.tt_variant_agreement_id
  from ttb.tt_variant_agreement current_tva
    join ttb.agreement_group current_ag on current_tva.agreement_group_id = current_ag.agreement_group_id
    join ttb.tt_variant_agreement previous_tva on current_tva.tt_variant_id = previous_tva.tt_variant_id and
                                                  current_tva.iteration_num = previous_tva.iteration_num
    join ttb.agreement_group previous_ag on previous_tva.agreement_group_id = previous_ag.agreement_group_id and
                                            current_ag.order_num > previous_ag.order_num
  where current_tva.tt_variant_agreement_id = p_tt_variant_agreement_id
  order by previous_ag.order_num desc
  limit 1
  into p_result;
end;
$$;

create or replace function ttb.agreement_pkg$get_agreement_end_date(
  p_tt_variant_agreement_id ttb.tt_variant_agreement.tt_variant_agreement_id%type)
  returns timestamp
language plpgsql
as $$
declare
  result_day            timestamp;
  agreement_days_number integer := (extract(epoch from ttb.agreement_pkg$get_agreement_interval(p_tt_variant_agreement_id)) / 86400) :: integer;
  i                     integer := 0;
begin
  select previous_tva.agreement_date
  into result_day
  from ttb.tt_variant_agreement current_tva
    join ttb.tt_variant_agreement previous_tva on ttb.agreement_pkg$get_previous_agreement(current_tva.tt_variant_agreement_id) = previous_tva.tt_variant_agreement_id
  where current_tva.tt_variant_agreement_id = p_tt_variant_agreement_id
        and previous_tva.resolution_id <> ttb.resolution_pkg$disagreed();

  if result_day isnull
  then return null;
  end if;

  while i < agreement_days_number
  loop
    result_day = result_day + interval '1 day';
    if ttb.calendar_pkg$is_working_day(result_day :: date)
    then
      i = i + 1;
    end if;
  end loop;
  return result_day;
end;
$$;

create or replace function ttb.agreement_pkg$get_agreement_interval(
      p_tt_variant_agreement_id ttb.tt_variant_agreement.tt_variant_agreement_id%type,
  out p_result                  interval
)
language plpgsql
as $$
begin
  select ag.agreement_period
  from ttb.tt_variant_agreement tva
    join ttb.agreement_group ag on tva.agreement_group_id = ag.agreement_group_id
  where tva.tt_variant_agreement_id = p_tt_variant_agreement_id
  into p_result;
end;
$$;

create or replace function ttb.agreement_pkg$get_order_num(
      p_tt_variant_agreement_id ttb.tt_variant_agreement.tt_variant_agreement_id%type,
  out p_result                  integer
)
language plpgsql
stable
as $$
begin
  select ag.order_num
  from ttb.tt_variant_agreement current_tva
    join ttb.agreement_group ag on current_tva.agreement_group_id = ag.agreement_group_id
  where current_tva.tt_variant_agreement_id = p_tt_variant_agreement_id
  into p_result;
end;
$$;

create or replace function ttb.agreement_pkg$get_last_order_num(
      p_tt_variant_id ttb.tt_variant.tt_variant_id%type,
  out p_result        integer
)
language plpgsql
as $$
declare
  l_current_iteration integer := ttb.agreement_pkg$get_current_iteration(p_tt_variant_id);
begin
  select max(ag.order_num)
  from ttb.tt_variant ttv
    join ttb.tt_variant_agreement tva on ttv.tt_variant_id = tva.tt_variant_id
    join ttb.agreement_group ag on tva.agreement_group_id = ag.agreement_group_id
  where ttv.tt_variant_id = p_tt_variant_id
        and tva.iteration_num = l_current_iteration
  into p_result;
end;
$$;

create or replace function ttb.agreement_pkg$get_current_iteration(
      p_tt_variant_id ttb.tt_variant.tt_variant_id%type,
  out p_result        integer
)
language plpgsql
as $$
begin
  select max(tva.iteration_num)
  from ttb.tt_variant_agreement tva
    join ttb.agreement_group ag on tva.agreement_group_id = ag.agreement_group_id
  where tva.tt_variant_id = p_tt_variant_id
        and ag.order_num = 1
  into p_result;

  if p_result is null
  then p_result := 0;
  end if;
end;
$$;

create or replace function ttb.agreement_pkg$get_current_agreement(
      p_tt_variant_id ttb.tt_variant.tt_variant_id%type,
  out p_result        ttb.tt_variant_agreement.tt_variant_agreement_id%type
)
language plpgsql
as $$
declare
  l_current_iteration ttb.tt_variant_agreement.iteration_num%type := ttb.agreement_pkg$get_current_iteration(p_tt_variant_id);
begin
  select current_tva.tt_variant_agreement_id
  from ttb.tt_variant_agreement previous_tva
    join ttb.tt_variant_agreement current_tva on ttb.agreement_pkg$get_next_agreement(previous_tva.tt_variant_agreement_id) = current_tva.tt_variant_agreement_id
  where previous_tva.iteration_num = l_current_iteration
        and previous_tva.tt_variant_id = p_tt_variant_id
        and previous_tva.resolution_id = ttb.resolution_pkg$agreed() and current_tva.resolution_id is null
  into p_result;
end;
$$;

create or replace function ttb.agreement_pkg$is_available_to_agreement(p_account_id    numeric,
                                                                       p_tt_variant_id ttb.tt_variant.tt_variant_id%type)
  returns boolean
language plpgsql
as $$
begin
  return exists(
      select 1
      from ttb.tt_variant tv
        join rts.route_variant rv on tv.route_variant_id = rv.route_variant_id
        join rts.route r on rv.route_id = r.route_id
        join ttb.agreement_group ag on r.tr_type_id = ag.tr_type_id
        join adm.account2group a2g on ag.group_id = a2g.group_id
      where tv.tt_variant_id = p_tt_variant_id
            and tv.tt_status_id = ttb.tt_status_pkg$in_work()
            and ag.order_num = 1
            and a2g.account_id = p_account_id
  );
end;
$$;

create or replace function ttb.agreement_pkg$get_initiator(
      p_tt_variant_agreement_id ttb.tt_variant_agreement.tt_variant_agreement_id%type,
  out p_result                  core.account.account_id%type
)
language plpgsql
as $$
begin
  select initial_tva.account_id
  from ttb.tt_variant_agreement current_tva
    join ttb.tt_variant_agreement initial_tva on current_tva.tt_variant_id = initial_tva.tt_variant_id
                                                 and current_tva.iteration_num = initial_tva.iteration_num
    join ttb.agreement_group initial_ag on initial_tva.agreement_group_id = initial_ag.agreement_group_id
  where current_tva.tt_variant_agreement_id = p_tt_variant_agreement_id
        and initial_ag.order_num = 1
  into p_result;
end;
$$;

create or replace function ttb.agreement_pkg$get_calendar(
  p_tt_variant_id ttb.tt_variant.tt_variant_id%type
)
  returns text
language plpgsql
as $$
declare
  l_result text;
begin
  select string_agg(ct.tag_name_short, ',')
  into l_result
  from ttb.tt_calendar c
    join ttb.calendar_tag ct on ct.calendar_tag_id = c.calendar_tag_id
  where c.tt_variant_id = p_tt_variant_id;

  return l_result;
end;
$$;

create or replace function ttb.agreement_pkg$assert_status(
  p_tt_variant_id ttb.tt_variant.tt_variant_id%type,
  p_tt_status_id  ttb.tt_status.tt_status_id%type
)
  returns void
language plpgsql
as $$
begin
  if not exists(
      select 1
      from ttb.tt_variant
      where tt_variant_id = p_tt_variant_id
            and tt_status_id = p_tt_status_id
  )
  then
    raise exception 'Некорректный статус варианта расписания. Обновите страницу.';
  end if;
end;
$$;

create or replace function ttb.agreement_pkg$to_agreement(
  p_account_id    core.account.account_id%type,
  p_tt_variant_id ttb.tt_variant.tt_variant_id%type
)
  returns void
language plpgsql
as $$
begin
  perform ttb.agreement_pkg$assert_status(p_tt_variant_id, ttb.tt_status_pkg$in_work());

  insert into ttb.tt_variant_agreement (tt_variant_id, agreement_group_id, iteration_num, resolution_id, agreement_date, comment, account_id)
    select
      tv.tt_variant_id,
      ag.agreement_group_id,
      ttb.agreement_pkg$get_current_iteration(tv.tt_variant_id) + 1,
      case when ag.order_num = 1
        then 1
      else null end,
      case when ag.order_num = 1
        then now()
      else null end,
      null,
      case when ag.order_num = 1
        then p_account_id
      else null end
    from ttb.tt_variant tv
      join rts.route_variant rv on tv.route_variant_id = rv.route_variant_id
      join rts.route r on rv.route_id = r.route_id
      join ttb.agreement_group ag on r.tr_type_id = ag.tr_type_id
    where tv.tt_variant_id = p_tt_variant_id
          and ag.sys_period @> now();

  update ttb.tt_variant
  set tt_status_id = ttb.tt_status_pkg$on_agreement()
  where tt_variant_id = p_tt_variant_id;

  perform ttb.agreement_notice_pkg$notify_group(p_tt_variant_id);
end;
$$;

create or replace function ttb.agreement_pkg$agree(
  p_account_id    core.account.account_id%type,
  p_tt_variant_id ttb.tt_variant.tt_variant_id%type
)
  returns void
language plpgsql
as $$
declare
  l_current_iteration    integer := ttb.agreement_pkg$get_current_iteration(p_tt_variant_id);
  l_current_agreement_id ttb.tt_variant_agreement.tt_variant_agreement_id%type := ttb.agreement_pkg$get_current_agreement(p_tt_variant_id);
  l_current_order_num    integer :=  ttb.agreement_pkg$get_order_num(l_current_agreement_id);
  l_last_order_num       integer := ttb.agreement_pkg$get_last_order_num(p_tt_variant_id);
begin
  perform ttb.agreement_pkg$assert_status(p_tt_variant_id, ttb.tt_status_pkg$on_agreement());

  update ttb.tt_variant_agreement
  set
    resolution_id  = ttb.resolution_pkg$agreed(),
    account_id     = p_account_id,
    agreement_date = now()
  where tt_variant_agreement_id in (
    select tva.tt_variant_agreement_id
    from ttb.tt_variant_agreement tva
      join ttb.agreement_group ag on tva.agreement_group_id = ag.agreement_group_id
    where tva.tt_variant_id = p_tt_variant_id
          and tva.iteration_num = l_current_iteration
          and (
            (l_current_order_num <> l_last_order_num and ag.order_num >= l_current_order_num and ag.order_num < l_last_order_num)
            or (l_current_order_num = l_last_order_num and ag.order_num = l_last_order_num)
          )
  );

  if ttb.agreement_pkg$get_current_agreement(p_tt_variant_id) is null
  then
    update ttb.tt_variant
    set tt_status_id = ttb.tt_status_pkg$agreed()
    where tt_variant_id = p_tt_variant_id;

    perform ttb.agreement_notice_pkg$notify_initiator(l_current_agreement_id);
    perform ttb.jf_tt_variant_pkg$of_actualize_ttv_status(p_tt_variant_id);
  else
    perform ttb.agreement_notice_pkg$notify_group(p_tt_variant_id);
  end if;

end;
$$;

create or replace function ttb.agreement_pkg$disagree(
  p_account_id    core.account.account_id%type,
  p_tt_variant_id ttb.tt_variant.tt_variant_id%type,
  p_comment       ttb.tt_variant_agreement.comment%type
)
  returns void
language plpgsql
as $$
declare
  l_current_agreement_id ttb.tt_variant_agreement.tt_variant_agreement_id%type;
begin
  perform ttb.agreement_pkg$assert_status(p_tt_variant_id, ttb.tt_status_pkg$on_agreement());

  l_current_agreement_id := ttb.agreement_pkg$get_current_agreement(p_tt_variant_id);
  update ttb.tt_variant_agreement
  set
    resolution_id  = ttb.resolution_pkg$disagreed(),
    account_id     = p_account_id,
    agreement_date = now(),
    comment        = p_comment
  where tt_variant_agreement_id = l_current_agreement_id;

  update ttb.tt_variant
  set tt_status_id = ttb.tt_status_pkg$in_work()
  where tt_variant_id = p_tt_variant_id;

  perform ttb.agreement_notice_pkg$notify_initiator(l_current_agreement_id);
end;
$$;

create or replace function ttb.agreement_pkg$check_expired()
  returns void
language plpgsql
as $$
declare
  l_expired_agreement record;
begin
  for l_expired_agreement in
  select tva.*
  from ttb.tt_variant tv
    join ttb.tt_variant_agreement tva on ttb.agreement_pkg$get_current_agreement(tv.tt_variant_id) = tva.tt_variant_agreement_id
  where tv.tt_status_id = ttb.tt_status_pkg$on_agreement()
        and ttb.agreement_pkg$get_agreement_end_date(tva.tt_variant_agreement_id) < now()
        and ttb.agreement_pkg$get_order_num(tva.tt_variant_agreement_id) <> ttb.agreement_pkg$get_last_order_num(tv.tt_variant_id)
  loop
    update ttb.tt_variant_agreement
    set
      resolution_id  = ttb.resolution_pkg$agreed(),
      agreement_date = now()
    where tt_variant_agreement_id = l_expired_agreement.tt_variant_agreement_id;

    perform ttb.agreement_notice_pkg$notify_expiration(l_expired_agreement.tt_variant_agreement_id);
  end loop;
end;
$$;