create or replace function ttb."jf_route_movement_schedule_pkg$of_rows"(p_id_account numeric, out p_rows refcursor,
                                                                        p_attr       text)
  returns refcursor as $$
declare
  l_tt_variant_id ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_tt_variant_id', true);
  l_tr_type_id    rts.route.tr_type_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id', true);
  l_route_id      rts.route.route_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', true);
  l_tt_status_id  ttb.tt_variant.tt_status_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_tt_status_id', true);
  l_rf_db_method  text := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true), 'xxx');
  l_f_disp_dt     timestamp := jofl.jofl_pkg$extract_date(p_attr, 'f_disp_DT', true);
begin
  open p_rows for
  select distinct
    p_id_account,
    l_tt_variant_id                                         p_tt_variant,
    ttv.route_variant_id                                    p_route_variant,
    ttv.ttv_name                                            p_ttv_name,
    r.route_id                                              p_route_id,
    r.route_num                                             p_route_num,
    to_char(lower(ttv.action_period) :: date, 'dd.mm.yyyy') p_date
  from ttb.tt_variant ttv
    join ttb.norm n on n.norm_id = ttv.norm_id
    join ttb.tt_status st on st.tt_status_id = ttv.tt_status_id
    join rts.route_variant rv on ttv.route_variant_id = rv.route_variant_id
    join rts.route r on rv.route_id = r.route_id
  where ttv.parent_tt_variant_id is null
        and ttv.tt_variant_id = l_tt_variant_id;
  --         and ttv.tt_status_id in (ttb.jf_tt_status_pkg$cn_work_status_id());
end;
$$
language plpgsql;

create or replace function ttb.route_movement_schedule_pkg$report_header(p_tt_variant_id text)
  returns table
  (depo_name  text,
   day_type   text,
   begin_date text,
   end_date   text,
   route_num  text,
   route_var  text)
as $$
declare
  l_day_type text;
  /* "за период с ... по .... "/"по состоянию на дату ..." */

begin
  l_day_type = 'будни';
  return query
  select
    (case when dpe.name_full notnull
      then
        dpe.name_full :: text
     else '-' :: text
     end) :: text as                                depo_name,
    'будни' :: text,
    to_char(lower(ttv.action_period), 'DD.MM.YYYY') begin_date,
    (case when upper(ttv.action_period) notnull
      then
        to_char(upper(ttv.action_period), 'DD.MM.YYYY') :: text
     else '-' :: text
     end) :: text                                   end_date,
    rt.route_num :: text,
    ttv.ttv_name :: text
  from ttb.tt_variant ttv
    join rts.route_variant rrv on ttv.route_variant_id = rrv.route_variant_id
    join rts.round rnd on rrv.route_variant_id = rnd.route_variant_id
    join rts.route rt on rrv.route_id = rt.route_id
    left join ttb.tt_tr ttr on ttv.tt_variant_id = ttr.tt_variant_id
    left join core.depo2territory d2t on ttr.depo2territory_id = d2t.depo2territory_id
    left join core.depo dp on d2t.depo_id = dp.depo_id
    left join core.entity dpe on dp.depo_id = dpe.entity_id
  where ttv.tt_variant_id = p_tt_variant_id :: int
        and ttb.action_type_pkg$is_production_round(rnd.action_type_id)
  group by
    dpe.name_full,
    l_day_type,
    lower(ttv.action_period),
    upper(ttv.action_period),
    rt.route_num,
    ttv.ttv_name;
end;
$$
language 'plpgsql';

create or replace function ttb.route_movement_schedule_pkg$overview(p_tt_variant_id text)
  returns table(
    route_type   text,
    route_length text,
    source_point text,
    dest_point   text
  )
as $$
begin
  return query
  select
    rnd.code :: text                                                               as route_type,
--     ((rnd.length:: float / 1000 ) :: numeric(4,3)) :: text                                                             as route_length,
    null :: text   as route_length,
    rts.jf_route_reference_rpt_pkg$getEndingStation(rnd.round_id, 'FIRST') :: text as source_point,
    rts.jf_route_reference_rpt_pkg$getEndingStation(rnd.round_id, 'LAST') :: text  as dest_point
  from ttb.tt_variant ttv
    join rts.route_variant rrv on ttv.route_variant_id = rrv.route_variant_id
    join rts.round rnd on rrv.route_variant_id = rnd.route_variant_id
  where ttv.tt_variant_id = p_tt_variant_id :: int
        and ttb.action_type_pkg$is_production_round(rnd.action_type_id)
  group by
    rnd.code,
    rnd.length,
    rnd.round_id;
end;
$$
language plpgsql;

create or replace function ttb.route_movement_schedule_pkg$schedule(p_tt_variant_id text)
  returns table(
    a_row_number     text,
    a_out_number     text,
    a_arrival_time   text,
    a_departure_time text,
    a_route_type     text,
    a_stop_length    text,
    a_mv_interval    text,
    b_row_number     text,
    b_out_number     text,
    b_arrival_time   text,
    b_departure_time text,
    b_route_type     text,
    b_stop_length    text,
    b_mv_interval    text

  ) as
$body$
declare
begin
  return query
  with act as (
      select
        vma.action_type_id,
        vma.action_type_code,
        vma.action_type_name,
        vma.is_production_round,
        vma.is_technical_round,
        vma.is_dinner,
        vma.is_break,
        vma.is_round,
        vma.action_type_id = ttb.action_type_pkg$bn_round_break() as is_bn_round_break,
        vma.is_all_pause
      from ttb.vm_action_type vma
  ),
      d1 as (
        select distinct
          tto.tt_variant_id,
          tto.tt_out_num,
          r.code,
          order_num,
          s.stop_id,
          move_direction_id,
          max(order_num)
          over (
            partition by tt_out_num, r.code ) max_order_num

        from ttb.tt_variant
          left join ttb.tt_out tto on tt_variant.tt_variant_id = tto.tt_variant_id
          left join ttb.tt_action ta on ta.tt_out_id = tto.tt_out_id
          left join ttb.tt_action_item tai on tai.tt_action_id = ta.tt_action_id
          left join rts.stop_item2round si2r on tai.stop_item2round_id = si2r.stop_item2round_id
          left join ttb.tt_action_round tar on tar.tt_action_id = ta.tt_action_id
          left join rts.round r on r.round_id = tar.round_id
                                   and si2r.round_id = r.round_id
          join rts.route_variant rrv on r.route_variant_id = rrv.route_variant_id
          join rts.route rt on rrv.route_id = rt.route_id
          left join rts.stop_item si on si2r.stop_item_id = si.stop_item_id
          left join rts.stop_location sl on si.stop_location_id = sl.stop_location_id
          left join rts.stop s on sl.stop_id = s.stop_id
        where tto.tt_variant_id = p_tt_variant_id :: int
    ),
      stop_id_dictionary as (
        select *
        from d1
    ),
      q as (
        select distinct
          out.tt_out_id,
          out.tt_out_num,
          min(tai.time_begin)
          over (
            partition by ta.tt_action_id ) as time_begin,
          max(tai.time_end)
          over (
            partition by ta.tt_action_id ) as time_end,
          ta.action_dur,
          at.action_type_code,
          at.action_type_name,
          at.is_production_round,
          at.is_technical_round,
          at.is_bn_round_break,
          at.is_dinner,
          lead(at.is_dinner, 1, false)
          over (
            order by time_begin )             next_is_dinner,
          at.is_break,
          at.is_round,
          md.move_direction_id,
          md.move_direction_name,
          r.code                           as r_code,
          first_value(s.name)
          over (
            partition by ta.tt_action_id
            order by si2r.order_num )      as stop_b,
          first_value(s.name)
          over (
            partition by ta.tt_action_id
            order by si2r.order_num desc ) as stop_e,
          first_value(s.stop_id)
          over (
            partition by ta.tt_action_id
            order by si2r.order_num )      as stop_id_b,
          first_value(s.stop_id)
          over (
            partition by ta.tt_action_id
            order by si2r.order_num desc ) as stop_id_e,
          ta.tt_action_id,
          out.tt_variant_id,
          at.action_type_id
        from ttb.tt_out out
          join ttb.tt_action ta on ta.tt_out_id = out.tt_out_id
          join act at on at.action_type_id = ta.action_type_id
          join ttb.tt_action_item tai on tai.tt_action_id = ta.tt_action_id
          join rts.stop_item2round si2r on si2r.stop_item2round_id = tai.stop_item2round_id
          left join rts.stop_item si on si.stop_item_id = si2r.stop_item_id
          left join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
          left join rts.stop s on s.stop_id = sl.stop_id
          join rts.stop_item2round_type si2rt on si2rt.stop_item2round_id = si2r.stop_item2round_id
          join rts.stop_item_type sit on sit.stop_item_id = si.stop_item_id
                                         and sit.stop_item_type_id = si2rt.stop_item_type_id
          left join rts.stop_type st
            on st.stop_type_id = sit.stop_type_id and st.stop_type_id = rts.stop_type_pkg$finish()
          /*7*/
          left join ttb.tt_action_round tar on tar.tt_action_id = ta.tt_action_id
          left join rts.round r on r.round_id = tar.round_id
                                   and si2r.round_id = r.round_id
                                   and r.action_type_id = at.action_type_id
          left join rts.move_direction md on md.move_direction_id = r.move_direction_id
        where out.tt_variant_id = p_tt_variant_id :: int
        order by time_begin

    ),
      qq as (
        select
          q.tt_out_id,
          q.tt_out_num,
          lag(q.time_end, 1, q.time_begin)
          over (
            partition by q.tt_out_id
            order by q.time_end )            as arr_time,
          q.time_begin,
          q.time_end,
          q.action_dur,
          q.action_type_code,
          q.action_type_name,
          q.move_direction_id,
          lead(q.move_direction_id, 1, q.move_direction_id)
          over ()                            as lead_move_direction_id,
          coalesce(q.move_direction_id,
                   lead(q.move_direction_id, 1, q.move_direction_id)
                   over ())                  as agg_move_direction_id,
          q.move_direction_name,
          q.r_code,
          q.is_production_round,
          q.is_technical_round,
          q.is_bn_round_break,
          q.is_dinner,
          q.next_is_dinner,
          coalesce(q.stop_b, q.stop_e)       as stop_b,
          q.stop_e,
          coalesce(q.stop_id_b, q.stop_id_e) as stop_id_b,
          q.stop_id_e,
          q.tt_action_id,
          q.tt_variant_id,
          q.action_type_id
        from q
        where q.is_production_round
              or q.is_technical_round
        order by arr_time
    ),
      a as (
        select
          row_number()
          over (
            order by arr_time )                                                               a_row_number,
          qq.tt_out_num :: text                                                               a_out_number,
          to_char(qq.arr_time, 'HH24:MI') :: text                                             a_arrival_time,
          to_char(qq.time_begin, 'HH24:MI') :: text                                           a_departure_time,
          qq.r_code :: text                                                                   a_route_type,
          to_char((qq.time_begin :: date + (qq.time_begin - qq.arr_time)), 'HH24:MI') :: text a_stop_length,
          (case
          /* when qq.next_is_dinner
             then 'Обед'
           when lead(qq.time_begin)
                over (
                  partition by qq.tt_out_num
                  order by qq.time_begin ) is null
             then 'КР'*/
           when qq.is_production_round and qq.r_code = '00'
             then
               to_char(qq.time_begin :: date +
                       coalesce(qq.time_begin - lag(qq.time_begin)
                       over (
                         partition by qq.is_production_round, qq.stop_id_b, qq.r_code
                         order by qq.time_begin )
                       , interval '0 sec'), 'HH24:MI') :: text
           else ''
           end
          ) :: text as                                                                        a_mv_interval
        from qq
        where 1 = 1
              and qq.stop_id_b = any (select stop_id
                                      from stop_id_dictionary
                                      where qq.tt_out_num = stop_id_dictionary.tt_out_num
                                            and order_num = 1
                                            and stop_id_dictionary.move_direction_id = 1)
              and qq.is_production_round
        group by qq.tt_out_num , qq.arr_time , qq.time_begin , qq.r_code , qq.stop_id_b , qq.is_production_round , qq.next_is_dinner
        order by qq.time_begin)
    ,
      b as (
        select
          row_number()
          over (
            order by qq.time_begin )                                                          b_row_number,
          qq.tt_out_num :: text                                                               b_out_number,
          to_char(qq.arr_time, 'HH24:MI') :: text                                             b_arrival_time,
          to_char(qq.time_begin, 'HH24:MI') :: text                                           b_departure_time,
          qq.r_code :: text                                                                   b_route_type,
          to_char((qq.time_begin :: date + (qq.time_begin - qq.arr_time)), 'HH24:MI') :: text b_stop_length,
          (case
           when qq.next_is_dinner
             then 'Обед'
           when lead(qq.time_begin)
                over (
                  partition by qq.tt_out_num
                  order by qq.time_begin ) is null
             then 'КР'
           when qq.is_production_round and qq.r_code = '00'
             then
               to_char(qq.time_begin :: date +
                       coalesce(qq.time_begin - lag(qq.time_begin)
                       over (
                         partition by qq.is_production_round, qq.stop_id_b, qq.r_code
                         order by qq.time_begin )
                       , interval '0 sec'), 'HH24:MI') :: text
           else ''
           end
          ) :: text as                                                                        b_mv_interval
        from qq
        where 1 = 1
              and qq.stop_id_b = any (select stop_id
                                      from stop_id_dictionary
                                      where qq.tt_out_num = stop_id_dictionary.tt_out_num
                                            and order_num = max_order_num
                                            and stop_id_dictionary.move_direction_id = 1)
              and qq.is_production_round
        group by qq.tt_out_num , qq.arr_time , qq.time_begin , qq.r_code , qq.stop_id_b , qq.is_production_round , qq.next_is_dinner
        order by qq.time_begin
    )
  select
    a.a_row_number :: text,
    a.a_out_number,
    a.a_arrival_time,
    a.a_departure_time,
    a.a_route_type,
    a.a_stop_length,
    a.a_mv_interval,
    b.b_row_number :: text,
    b.b_out_number,
    b.b_arrival_time,
    b.b_departure_time,
    b.b_route_type,
    b.b_stop_length,
    b.b_mv_interval
  from a
    full join b on a.a_row_number = b.b_row_number
  order by a.a_row_number, b.b_row_number
  ;
end;
$body$
language 'plpgsql';

create or replace function ttb.route_movement_schedule_pkg$summary(p_tt_variant_id text)
  returns  table
  (
    out_counter  text,
    avg_interval text
  )as
  $body$
begin
  return query
  with footing as (
      select
        a_row_number,
        a_out_number,
        a_arrival_time,
        a_departure_time,
        a_route_type,
        a_stop_length,
        a_mv_interval
      from ttb.route_movement_schedule_pkg$schedule(p_tt_variant_id)
  )
  select count(*) :: text out_count, ((avg(extract( epoch from footing.a_mv_interval :: interval ))/ 60 ) :: numeric(10 , 2)) :: text avg_interval
  from footing
  where footing.a_route_type = '00'
        and a_arrival_time between '09:00' and '15:59'
        and not (a_mv_interval = 'КР' or a_mv_interval = 'Обед');
  end;
$body$
  language 'plpgsql';