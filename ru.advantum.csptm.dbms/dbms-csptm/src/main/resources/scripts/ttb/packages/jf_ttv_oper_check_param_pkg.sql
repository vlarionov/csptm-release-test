CREATE OR REPLACE FUNCTION ttb.jf_ttv_oper_check_param_pkg$attr_to_rowtype (p_attr text) RETURNS ttb.ttv_oper_check_param
	LANGUAGE plpgsql
AS $$

declare
   l_r ttb.ttv_oper_check_param%rowtype;
begin
   l_r.ttv_oper_check_param_id := jofl.jofl_pkg$extract_number(p_attr, 'ttv_oper_check_param_id', true);
   l_r.tt_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
   l_r.ttv_oper_cp_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'ttv_oper_cp_type_id', true);
   l_r.time_range := jofl.jofl_pkg$extract_varchar(p_attr, 'time_range', true);
   l_r.cp_val := jofl.jofl_pkg$extract_number(p_attr, 'cp_val', true);
   l_r.cp_val_plan := jofl.jofl_pkg$extract_number(p_attr, 'cp_val_plan', true);

   return l_r;
end;
$$
;

CREATE OR REPLACE FUNCTION ttb.jf_ttv_oper_check_param_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$

declare
   l_r ttb.ttv_oper_check_param%rowtype;
begin
    l_r.tt_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
 open p_rows for
    select
      cp.ttv_oper_check_param_id,
      cp.tt_variant_id,
      cp.ttv_oper_cp_type_id,
      lower(cp.time_range) start_time,
      upper(cp.time_range) finish_time,
      cp.cp_val,
      cpt.param_name,
      cp_val_plan
    from ttb.ttv_oper_check_param cp
      join ttb.ttv_oper_cp_type cpt on cpt.ttv_oper_cp_type_id = cp.ttv_oper_cp_type_id
    where tt_variant_id =l_r.tt_variant_id;
end;
$$
;


CREATE OR REPLACE FUNCTION ttb.jf_ttv_oper_check_param_pkg$of_insert (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r ttb.ttv_oper_check_param%rowtype;
   l_start_time integer;
   l_finish_time integer;
   l_movement_type_id smallint;
begin
   l_r := ttb.jf_ttv_oper_check_param_pkg$attr_to_rowtype(p_attr);

   l_start_time := jofl.jofl_pkg$extract_number(p_attr, 'start_time', true);
   l_finish_time := jofl.jofl_pkg$extract_number(p_attr, 'finish_time', true);
   l_r.time_range := int4range(l_start_time, l_finish_time);



   select movement_type_id
   into l_movement_type_id
   from ttb.ttv_oper_check_movement_type
   where tt_variant_id = l_r.tt_variant_id;

   if l_movement_type_id is null then
    return jofl.jofl_util$cover_result('error', 'Не указан параметр Приоритетный тип движения!');
   end if;

   l_r.ttv_oper_check_param_id := nextval('ttb.ttv_oper_check_param_ttv_oper_check_param_seq');

   insert into ttb.ttv_oper_check_param select l_r.*;

   return null;
end;
$$
;

CREATE OR REPLACE FUNCTION ttb.jf_ttv_oper_check_param_pkg$of_update (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r ttb.ttv_oper_check_param%rowtype;
   l_start_time integer;
   l_finish_time integer;
begin
   l_r := ttb.jf_ttv_oper_check_param_pkg$attr_to_rowtype(p_attr);
   l_start_time := jofl.jofl_pkg$extract_number(p_attr, 'start_time', true);
   l_finish_time := jofl.jofl_pkg$extract_number(p_attr, 'finish_time', true);
   l_r.time_range := int4range(l_start_time, l_finish_time);

   update ttb.ttv_oper_check_param set
          tt_variant_id = l_r.tt_variant_id,
          ttv_oper_cp_type_id = l_r.ttv_oper_cp_type_id,
          time_range = l_r.time_range,
          cp_val = l_r.cp_val,
          cp_val_plan = l_r.cp_val_plan
   where
          ttv_oper_check_param_id = l_r.ttv_oper_check_param_id;

   return null;
end;

$$;


CREATE OR REPLACE FUNCTION ttb.jf_ttv_oper_check_param_pkg$of_delete (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r ttb.ttv_oper_check_param%rowtype;
begin
   l_r := ttb.jf_ttv_oper_check_param_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.ttv_oper_check_param where  ttv_oper_check_param_id = l_r.ttv_oper_check_param_id;

   return null;
end;

$$;

CREATE OR REPLACE FUNCTION ttb.jf_ttv_oper_check_param_pkg$of_insert_default (p_tt_variant_id numeric) RETURNS text
	LANGUAGE plpgsql
AS $$
begin

    INSERT INTO ttb.ttv_oper_check_param
    (tt_variant_id,
     ttv_oper_cp_type_id,
     time_range,
     cp_val)
    select
        p_tt_variant_id,
        ttv_oper_cp_type_id,
        int4range(0, 86399),
        default_val
    from ttb.ttv_oper_cp_type;

   return null;
end;

$$;