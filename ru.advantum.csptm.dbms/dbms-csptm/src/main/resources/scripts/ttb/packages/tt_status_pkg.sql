CREATE OR REPLACE FUNCTION ttb.tt_status_pkg$in_work()
  RETURNS ttb.tt_status.tt_status_id%TYPE
IMMUTABLE
LANGUAGE plpgsql
AS
$$
BEGIN
  RETURN 1;
END;
$$;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.tt_status_pkg$on_agreement()
  RETURNS ttb.tt_status.tt_status_id%TYPE
IMMUTABLE
LANGUAGE plpgsql
AS
$$
BEGIN
  RETURN 2;
END;
$$;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.tt_status_pkg$agreed()
  RETURNS ttb.tt_status.tt_status_id%TYPE
IMMUTABLE
LANGUAGE plpgsql
AS
$$
BEGIN
  RETURN 3;
END;
$$;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.tt_status_pkg$cancel (
)
RETURNS smallint AS
$body$
  select  4::smallint;
$body$
LANGUAGE 'sql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------

create or replace function ttb."tt_status_pkg$arch"() returns smallint
IMMUTABLE
LANGUAGE sql
AS $$
  select  5::smallint;
$$;
-------------------------------------------------
create or replace function ttb."tt_status_pkg$active"() returns smallint
IMMUTABLE
LANGUAGE sql
AS $$
  select  6::smallint;
$$;
