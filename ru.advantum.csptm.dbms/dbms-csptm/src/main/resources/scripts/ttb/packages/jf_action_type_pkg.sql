CREATE OR REPLACE FUNCTION ttb."jf_action_type_pkg$attr_to_rowtype"(p_attr text)
 RETURNS ttb.action_type
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.action_type%rowtype; 
begin 
   l_r.action_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'action_type_id', true); 
   l_r.action_type_name := jofl.jofl_pkg$extract_varchar(p_attr, 'action_type_name', true); 
   l_r.action_type_code := jofl.jofl_pkg$extract_varchar(p_attr, 'action_type_code', true); 
   l_r.parent_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'parent_type_id', true); 
   l_r.action_type_desc := jofl.jofl_pkg$extract_varchar(p_attr, 'action_desc', true);

   return l_r;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_action_type_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 l_rf_db_method text := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true), 'xxx');
 ln_dr_shift_id ttb.dr_mode.dr_shift_id%type := jofl.jofl_pkg$extract_number(p_attr, 'dr_shift_id', true);
begin 
 open p_rows for 
  with recursive root_type (action_type_id, parent_type_id, action_type_name, action_type_desc, action_type_code, level, path) as (
    select action_type_id, parent_type_id, action_type_name, action_type_desc,action_type_code, 1, array[action_type_name]
    from  ttb.action_type
    where parent_type_id is null
    union
    select atp.action_type_id, atp.parent_type_id, atp.action_type_name, atp.action_type_desc, atp.action_type_code, rt.level+1, rt.path||atp.action_type_name
    from  ttb.action_type atp
      join root_type rt on  rt.action_type_id = atp.parent_type_id
  )
  select root_type.action_type_id, 
         root_type.parent_type_id, 
         root_type.action_type_name, 
         root_type.action_type_desc, 
         root_type.action_type_code, 
         root_type.level, 
         /*root_type.path,*/
         (select action_type_name from ttb.action_type atp_parent where  atp_parent.action_type_id = root_type.parent_type_id ) as parent_name
  from root_type
  where (l_rf_db_method in ('xxx', 'ttb.ref_action_type')) or
  	    (l_rf_db_method = 'ttb.dr_mode' and 
         action_type_id in (  ttb.action_type_pkg$dinner()
        					 ,ttb.action_type_pkg$pause() ) and
         not exists (select null from ttb.dr_mode drm 
         						 join ttb.action_type at on drm.action_type_id = at.action_type_id
         			 where at.action_type_id = root_type.action_type_id
                       and drm.dr_shift_id = ln_dr_shift_id) ) 
;
  
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_action_type_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.action_type%rowtype;
begin 
   l_r := ttb.jf_action_type_pkg$attr_to_rowtype(p_attr);

   update ttb.action_type set 
          action_type_name = l_r.action_type_name, 
          action_type_code = l_r.action_type_code, 
          parent_type_id = l_r.parent_type_id, 
          action_desc = l_r.action_desc
   where 
          action_type_id = l_r.action_type_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_action_type_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.action_type%rowtype;
begin 
   l_r := ttb.jf_action_type_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.action_type where  action_type_id = l_r.action_type_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_action_type_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.action_type%rowtype;
begin 
   l_r := ttb.jf_action_type_pkg$attr_to_rowtype(p_attr);

   insert into ttb.action_type select l_r.*;

   return null;
end;
 $function$
;