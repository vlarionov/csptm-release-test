create or replace function  ttb.jf_driver2route_pkg$attr_to_rowtype(p_attr text)
    returns ttb.driver2route
language plpgsql
as $BODY$
declare
    l_r ttb.driver2route%rowtype;
begin
    l_r.driver_id := jofl.jofl_pkg$extract_number(p_attr, 'driver_id', true);
    l_r.driver2route_id := jofl.jofl_pkg$extract_number(p_attr, 'driver2route_id', true);
    l_r.route_id := jofl.jofl_pkg$extract_number(p_attr, 'route_id', true);
    return l_r;
end;
$BODY$;

create or replace function ttb.jf_driver2route_pkg$of_rows(p_id_account numeric, out p_rows refcursor, p_attr text)
    returns refcursor
language plpgsql
as $function$
declare
    l_f_dt_begin date;
    l_f_dt_end   date;
    l_driver_id  core.driver.driver_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'driver_id', true);
begin
    open p_rows for
    select
        d2r.driver2route_id,
        d2r.driver_id,
        d2r.route_id,
        r.route_num,
        ct.name
    from ttb.driver2route d2r
        join rts.route r on d2r.route_id = r.route_id
        join core.tr_type ct on ct.tr_type_id = r.tr_type_id
    where d2r.driver_id = l_driver_id;
end;
$function$;

create or replace function ttb.jf_driver2route_pkg$of_insert(p_id_account numeric, p_attr text)
    returns text
language plpgsql
as $function$
declare
    l_r ttb.driver2route%rowtype;
begin
    l_r := ttb.jf_driver2route_pkg$attr_to_rowtype(p_attr);

    insert into ttb.driver2route (driver_id, route_id)  select l_r.driver_id,l_r.route_id;
    return null;
end;
$function$;


create or replace function ttb.jf_driver2route_pkg$of_delete(p_id_account numeric, p_attr text)
    returns text
language plpgsql
as $function$
declare
    l_r ttb.driver2route%rowtype;
begin
    l_r := ttb.jf_driver2route_pkg$attr_to_rowtype(p_attr);

    delete from ttb.driver2route where driver2route_id=l_r.driver2route_id;
    return null;
end;
$function$;


CREATE OR REPLACE FUNCTION ttb.jf_driver2route_pkg$of_action_insert(p_id_account NUMERIC, p_attr TEXT)
    RETURNS text
LANGUAGE plpgsql
AS $function$
DECLARE
    l_driver_id ttb.driver2route.driver_id%TYPE :=jofl.jofl_pkg$extract_number(p_attr, 'driver_id', TRUE);
    l_f_route_id          integer [] :=jofl.jofl_pkg$extract_narray(p_attr, 'f_route_id', TRUE);
BEGIN
    insert into ttb.driver2route ( route_id,driver_id)
        SELECT route_ids,l_driver_id
        from unnest(l_f_route_id) route_ids
        where route_ids not in (
            select cd.route_id
            from ttb.driver2route cd
            where cd.driver_id = l_driver_id
        );
    return null;
END;
$function$;
