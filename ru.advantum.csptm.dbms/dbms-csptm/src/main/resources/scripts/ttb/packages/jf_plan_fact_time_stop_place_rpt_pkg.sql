
create or replace function ttb.jf_plan_fact_time_stop_place_rpt_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
	language plpgsql
as $$
declare
--   {"dt_b_DT":"2017-12-18 21:00:00.0","dt_e_DT":"2017-12-19 20:59:59.0"
   --F_tr_type_id SMALLINT := jofl.jofl_pkg$extract_number(p_attr, 'F_tr_type_id', true);
  F_list_tr_type_id SMALLINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_list_tr_type_id', true);
  f_list_depo_id SMALLINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', true);
-- ,"f_list_route_id":"$array[4526]","
  f_out_num_TEXT SMALLINT;
  --f_list_depo_id":"$array[498]",
  f_list_dr_shift_id SMALLINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_dr_shift_id', true);
  f_list_stop_item2round_id INTEGER[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_stop_item2round_id', true);
  f_list_action_type_id SMALLINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_action_type_id', true);

  /* список out_id */
  -- f_list_out_id smallint[] := string_to_array(jofl.jofl_pkg$extract_varcharr, 'f_tt_out_id_list', true), ',');
  
  l_text text; -- := trim(jofl.jofl_pkg$extract_varchar(p_attr, 'f_out_num_TEXT' ,TRUE));
begin

  IF array_length(F_list_tr_type_id, 1) is null THEN
 open p_rows for
      select
        null interval_fact,
        null depo_name,
        null tr_type_name,
        null route_num,
        null out_num,
        null dr_shift_num,
        null garage_num,
        null tab_num,
        null stop_name,
        null stop_b_plan,
        null stop_b_oper,
        null stop_b_fact,
        null fact_oper_delta,
        null action_type,
        null move_dir,
        null oper_plan_delta,
        null fact_oper_delta,
        null interval_plan,
        null interval_delta,
        null interval_delta_prc
	-- where 1=2
    ;
    RETURN ;
  END IF;

  if l_text = '' or l_text is null then
  f_out_num_TEXT := NULL ;
  ELSE
    f_out_num_TEXT := l_text::SMALLINT;
  end IF ;
 open p_rows for
      select distinct
        null interval_fact,
        null depo_name,
        trt.short_name tr_type_name,
        rt.route_num route_num,
        out.tt_out_num out_num,
        ds.dr_shift_num dr_shift_num,
        null garage_num,
        null tab_num,
        s.name stop_name,
        tai.time_begin stop_b_plan,
        null stop_b_oper,
        null stop_b_fact,
        null fact_oper_delta,
        at.action_type_name action_type,
        md.move_direction_name move_dir,
        null oper_plan_delta,
        null fact_oper_delta,
        null interval_plan,
        null interval_delta,
        null interval_delta_prc
           from ttb.tt_out out
             join ttb.tt_action ta on ta.tt_out_id = out.tt_out_id
             join ttb.action_type at on at.action_type_id = ta.action_type_id --and at.parent_type_id in (1, 2)
             join ttb.tt_action_item tai on tai.tt_action_id = ta.tt_action_id
             join rts.stop_item2round si2r on si2r.stop_item2round_id = tai.stop_item2round_id
             join rts.stop_item si on si.stop_item_id = si2r.stop_item_id
             join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
             join rts.stop s on s.stop_id = sl.stop_id
             join rts.stop_item2round_type si2rt on si2rt.stop_item2round_id = si2r.stop_item2round_id
             join rts.stop_item_type sit on sit.stop_item_id = si.stop_item_id
                                            and sit.stop_item_type_id = si2rt.stop_item_type_id
             join ttb.tt_action_group ag on ag.tt_action_group_id = ta.tt_action_group_id
             join ttb.tt_action_round tar on tar.tt_action_id = ta.tt_action_id
             join rts.round r on r.round_id = tar.round_id and si2r.round_id = r.round_id and r.action_type_id = at.action_type_id
             join rts.move_direction md on md.move_direction_id = r.move_direction_id
             join rts.route_variant rv on r.route_variant_id = rv.route_variant_id
             join rts.route rt on rv.route_variant_id = rt.current_route_variant_id
             join core.tr_type trt on rt.tr_type_id = trt.tr_type_id
             join ttb.dr_shift ds on tai.dr_shift_id = ds.dr_shift_id
where trt.tr_type_id = any(F_list_tr_type_id)
--   and s.stop_id
and ttb.action_type_pkg$is_production_round(r.action_type_id)
    and si2r.stop_item2round_id =any(f_list_stop_item2round_id)
    and ( ttb.action_type_pkg$is_parent_type(p_type_id => ta.action_type_id
                                          ,  p_parent_type_id => f_list_action_type_id)
        or array_length(f_list_action_type_id, 1) is null)
    and (f_out_num_TEXT is null or out.tt_out_num=f_out_num_TEXT)
    and (array_length(f_list_dr_shift_id, 1) is null or tai.dr_shift_id = any(f_list_dr_shift_id))
           ORDER BY tai.time_begin
           ;
end;
$$
;

