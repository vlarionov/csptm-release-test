CREATE OR REPLACE FUNCTION ttb."jf_tt_action_type_pkg$attr_to_rowtype"(p_attr text)
 RETURNS ttb.tt_action_type
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tt_action_type%rowtype; 
begin 
   l_r.tt_action_type_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_action_type_id', true); 
   l_r.tt_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true); 
   l_r.action_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'action_type_id', true); 
   l_r.plan_time := jofl.jofl_pkg$extract_number(p_attr, 'plan_time', true); 
   
   if not ttb.jf_tt_variant_pkg$of_get_policy(l_r.tt_variant_id)
    then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if;       

   return l_r;
end;
 $function$
;
----------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_action_type_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_tt_variant_id ttb.tt_variant.tt_variant_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
begin 
 open p_rows for
     select
       ttat.tt_action_type_id,
       ttat.tt_variant_id,
       ttat.action_type_id,
       ttat.plan_time,
       act.path::text || case 
       						when ttat.r_code is not null 
                              then ' - ' || 
                              r_code || case 
                                          when ttat.action_type_id = ttb.action_type_pkg$park_to_stop() 
                                           then ' - ' ||
                                             coalesce (
                                               (select s.name as stop_name
                                                 from ttb.tt_at_round_stop trs
                                                   join rts.stop_item2round sir on sir.stop_item2round_id = trs.stop_item2round_id
                                                   join rts.stop_item si on si.stop_item_id =sir.stop_item_id
                                                   join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
                                                   join rts.stop s on s.stop_id = sl.stop_id
                                                   join ttb.tt_at_round ar on ar.tt_at_round_id = trs.tt_at_round_id
                                                   --join ttb.tt_action_type act on act.tt_action_type_id = ar.tt_action_type_id
                                                 where ar.tt_action_type_id = ttat.tt_action_type_id
                                                  order by sir.order_num desc
                                                  limit 1), ' ')
                                          when ttat.action_type_id = ttb.action_type_pkg$stop_to_park() 
                                           then ' - ' ||
                                             coalesce (
                                               (select s.name as stop_name
                                                 from ttb.tt_at_round_stop trs
                                                   join rts.stop_item2round sir on sir.stop_item2round_id = trs.stop_item2round_id
                                                   join rts.stop_item si on si.stop_item_id =sir.stop_item_id
                                                   join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
                                                   join rts.stop s on s.stop_id = sl.stop_id
                                                   join ttb.tt_at_round ar on ar.tt_at_round_id = trs.tt_at_round_id
                                                   --join ttb.tt_action_type act on act.tt_action_type_id = ar.tt_action_type_id
                                                 where ar.tt_action_type_id = ttat.tt_action_type_id
                                                  order by sir.order_num asc
                                                  limit 1), ' ')                                                  
                                           else 
                                             ' '
                                          end                                                        
                              else ' '
       					 end
       as action_type_full_name,
       act.action_type_code,
       case 
       	when ttb.action_type_pkg$is_round(ttat.action_type_id) 
        	then 'P{D},D{ttb.tt_at_round}' 
        when ttb.action_type_pkg$bn_round_break() = ttat.action_type_id
        	then 'P{D},D{ttb.tt_at_stop}' 
        else 'P{D},D{ttb.tt_at_stop_wo_aligment}' 
       end "ROW$POLICY",
       rv.route_id
     from ttb.tt_action_type ttat
       join ttb.tt_variant v on ttat.tt_variant_id = v.tt_variant_id
       join rts.route_variant rv on rv.route_variant_id = v.route_variant_id
       join ttb.v_action_type act on act.action_type_id = ttat.action_type_id
     where ttat.tt_variant_id = l_tt_variant_id;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_action_type_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tt_action_type%rowtype;
begin 
   l_r := ttb.jf_tt_action_type_pkg$attr_to_rowtype(p_attr);

   update ttb.tt_action_type set 
          tt_variant_id = l_r.tt_variant_id, 
          action_type_id = l_r.action_type_id, 
          plan_time = l_r.plan_time
   where 
          tt_action_type_id = l_r.tt_action_type_id;

   return null;
end;
 $function$
;
----------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_action_type_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tt_action_type%rowtype;
begin 
   l_r := ttb.jf_tt_action_type_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.tt_action_type where  tt_action_type_id = l_r.tt_action_type_id;

   return null;
end;
 $function$
;
----------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_action_type_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tt_action_type%rowtype;
begin 
   l_r := ttb.jf_tt_action_type_pkg$attr_to_rowtype(p_attr);

   insert into ttb.tt_action_type select l_r.*;

   return null;
end;
 $function$
;
----------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_action_type_pkg$of_refresh (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   --l_tt_variant_id  ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
   l_r ttb.tt_action_type%rowtype;
begin 

  l_r := ttb.jf_tt_action_type_pkg$attr_to_rowtype(p_attr);

  create temporary table if not exists  atgrs_time_temp
  	on commit preserve rows
	tablespace ttb_data  
    as
      select atg.fix_arrival_type_id
        	,atg.start_time
        	,tar.round_id
        	,tars.stop_item2round_id
        	,tat.action_type_id
        	,tat.r_code
    from ttb.atgrs_time atg
    join ttb.tt_at_round_stop tars on tars.tt_at_round_stop_id = atg.tt_at_round_stop_id
    join ttb.tt_at_round tar on tar.tt_at_round_id = tars.tt_at_round_id
    join ttb.tt_action_type tat on tat.tt_action_type_id = tar.tt_action_type_id
    where tat.tt_variant_id = l_r.tt_variant_id
    with data;
  
  delete from ttb.tt_at_stop
  where tt_action_type_id in (
      select tt_action_type_id
      from ttb.tt_action_type 
      where tt_variant_id = l_r.tt_variant_id
      );
  
  with del_tar as (
  delete from ttb.tt_at_round 
  where tt_action_type_id in (
      select tt_action_type_id
      from ttb.tt_action_type 
      where tt_variant_id = l_r.tt_variant_id)
      returning *
  )
  delete from ttb.tt_at_round_stop 
  where tt_at_round_stop_id in (
      select tars.tt_at_round_stop_id
      from ttb.tt_at_round_stop  tars
      join del_tar tar on tars.tt_at_round_id = tar.tt_at_round_id
      );
      
  delete 
  from ttb.tt_action_type2group
  where tt_action_type_id in (
      select tt_action_type_id
      from ttb.tt_action_type 
      where tt_variant_id = l_r.tt_variant_id
      );
  
  delete from ttb.tt_action_type 
  where tt_variant_id = l_r.tt_variant_id;

   with prep_adata as (
     select  rnd.action_type_id
            ,rnd.round_id
            ,rnd.code
            ,v.tt_variant_id 
            ,sir.stop_item2round_id
            ,sir.order_num
            ,rnd.route_variant_id
            ,case 
              when sit.stop_type_id in ( rts.stop_type_pkg$finish()
              							,rts.stop_type_pkg$finish_a()
                                        ,rts.stop_type_pkg$finish_b() )
              then true
              else false
             end is_kp
            ,case 
              when sit.stop_type_id = rts.stop_type_pkg$lunch()
              then true
              else false
             end is_dinner
            ,case 
              when sit.stop_type_id = rts.stop_type_pkg$repair()
              then true
              else false
             end is_repair
          from ttb.tt_variant v
            join rts.route_variant rv on rv.route_variant_id = v.route_variant_id and not rv.sign_deleted
            join rts.round rnd on rnd.route_variant_id = rv.route_variant_id and not rnd.sign_deleted
            join rts.stop_item2round sir on sir.round_id = rnd.round_id /*and sir.order_num=1*/ and not sir.sign_deleted
            join rts.stop_item2round_type rt on rt.stop_item2round_id = sir.stop_item2round_id and not rt.sign_deleted
            join rts.stop_item_type sit on sit.stop_item_type_id = rt.stop_item_type_id and not sit.sign_deleted
            join rts.stop_item si on si.stop_item_id = sir.stop_item_id and not si.sign_deleted
            join rts.stop_location sl on sl.stop_location_id = si.stop_location_id and not sl.sign_deleted
            join rts.stop s on s.stop_id = sl.stop_id and not s.sign_deleted
          where v.tt_variant_id = l_r.tt_variant_id 
            and rnd.action_type_id <> 70 /*Оперативный*/
      )
 	,adata as (
      select prep_adata.action_type_id
            ,prep_adata.round_id
            ,prep_adata.code
            ,prep_adata.tt_variant_id 
            ,prep_adata.stop_item2round_id
            ,prep_adata.order_num
            ,prep_adata.route_variant_id
            ,max(prep_adata.is_kp::int)::boolean as is_kp
            ,max(prep_adata.is_dinner::int)::boolean as is_dinner
            ,max(prep_adata.is_repair::int)::boolean as is_repair
      from prep_adata
      group by prep_adata.action_type_id
              ,prep_adata.round_id
              ,prep_adata.code
              ,prep_adata.tt_variant_id 
              ,prep_adata.stop_item2round_id
              ,prep_adata.order_num
              ,prep_adata.tt_variant_id
              ,prep_adata.route_variant_id)               
  --добавим доступные типы перерывов
 ,insert_type_break as (
      insert into ttb.tt_action_type(tt_variant_id, action_type_id, plan_time)
        select adata.tt_variant_id, st.action_type_id, 
        	   case sit.stop_type_id
               	when rts.stop_type_pkg$lunch()
                  then ttb.jf_rv_tune_pkg$get_break_dinner_time_by_rv(adata.route_variant_id) 
                when rts.stop_type_pkg$repair()
                  then ttb.jf_rv_tune_pkg$get_break_tr_time_by_rv(adata.route_variant_id) 
                else null
               end
        from adata
          join rts.stop_item2round_type sirt on sirt.stop_item2round_id = adata.stop_item2round_id
          join rts.stop_item_type sit on sit.stop_item_type_id = sirt.stop_item_type_id
          join rts.stop_type st on st.stop_type_id = sit.stop_type_id
        where st.action_type_id is not null
              and  not exists  (select null from ttb.tt_action_type ttt where  (ttt.tt_variant_id, ttt.action_type_id) = (adata.tt_variant_id , st.action_type_id) )
        group by adata.tt_variant_id, st.action_type_id, case sit.stop_type_id
               	when rts.stop_type_pkg$lunch()
                  then ttb.jf_rv_tune_pkg$get_break_dinner_time_by_rv(adata.route_variant_id) 
                when rts.stop_type_pkg$repair()
                  then ttb.jf_rv_tune_pkg$get_break_tr_time_by_rv(adata.route_variant_id) 
                else null
               end
      returning *
    )
  --добавим доступные типы ОП для перерывов
  , insert_type_break_stop as (
      insert into ttb.tt_at_stop(tt_action_type_id, stop_item2round_id)
      select it.tt_action_type_id, adata.stop_item2round_id
      from adata
        join rts.stop_item2round_type sirt on sirt.stop_item2round_id = adata.stop_item2round_id
        join rts.stop_item_type sit on sit.stop_item_type_id = sirt.stop_item_type_id
        join rts.stop_type st on st.stop_type_id = sit.stop_type_id
        join insert_type_break it on it.action_type_id =  st.action_type_id
      where adata.order_num <> 1
        )
  ,
    --добавим новые доступные типы для расписания (если есть)
 insert_type as (
      insert into ttb.tt_action_type(tt_variant_id, action_type_id, r_code)
      select tt_variant_id, action_type_id, code
      from adata
      where not exists  (select null 
      					from ttb.tt_action_type ttt 
                        join ttb.tt_at_round tar on ttt.tt_action_type_id = tar.tt_action_type_id
                        where  (ttt.tt_variant_id, ttt.action_type_id, tar.round_id) = (adata.tt_variant_id , adata.action_type_id, adata.round_id) )
      group by tt_variant_id, action_type_id, code  
      order by tt_variant_id, action_type_id, code
 returning *
 ) 
  , 
  --добавим рейсы к новым типам
  insert_at_round as (
    insert into ttb.tt_at_round (round_id, tt_action_type_id)
      select adata.round_id, it.tt_action_type_id
      from adata
        join insert_type it on it.action_type_id = adata.action_type_id
        join rts.round rnd on rnd.round_id = adata.round_id
        				  and it.r_code = rnd.code
      where  not exists  (select null 
      					from ttb.tt_at_round ttt 
                        join ttb.tt_action_type tat on ttt.tt_action_type_id = tat.tt_action_type_id
                        where  (ttt.round_id, ttt.tt_action_type_id) = (adata.round_id, it.tt_action_type_id) )
      group by adata.round_id, it.tt_action_type_id
    returning *
  )
    , 
  --добавим новые рейсы к типам
  insert_at_round_new as (
    insert into ttb.tt_at_round (round_id, tt_action_type_id)
      select adata.round_id, tat.tt_action_type_id
      from adata
        join ttb.tt_action_type tat on tat.action_type_id = adata.action_type_id 
        							and tat.tt_variant_id = l_r.tt_variant_id
        join rts.round rnd on rnd.round_id = adata.round_id
        				  and tat.r_code = rnd.code
      where not exists  (select null 
      					from ttb.tt_at_round ttt 
                        join ttb.tt_action_type tat on ttt.tt_action_type_id = tat.tt_action_type_id
                        where  (ttt.round_id, ttt.tt_action_type_id) = (adata.round_id, tat.tt_action_type_id) )
      group by adata.round_id, tat.tt_action_type_id
    returning *
  )
  ,
  --добавим время на пересменку
  insert_change_time as (
    insert into ttb.tt_change_time (tt_variant_id, tr_capacity_id, change_time)    
      select adata.tt_variant_id, sct.tr_capacity_id, sct.change_time
      from adata
        join rts.route r on r.current_route_variant_id = adata.route_variant_id
        join ttb.spr_change_time sct on sct.tr_type_id = r.tr_type_id
      where not exists  (select null from ttb.tt_change_time tct where  (tct.tt_variant_id, tct.tr_capacity_id) = (adata.tt_variant_id, sct.tr_capacity_id) )
      group by adata.tt_variant_id, sct.tr_capacity_id, sct.change_time
  )
  ,
  --добавим к доступным типам для расписания Межрейсовую стоянку
  insert_bn_round_break as (
      insert into ttb.tt_action_type(tt_variant_id,action_type_id)
        select l_r.tt_variant_id , ttb.action_type_pkg$bn_round_break()
        where not exists (select null from ttb.tt_action_type where (tt_variant_id, action_type_id) = (l_r.tt_variant_id , ttb.action_type_pkg$bn_round_break()) )
      returning *
    )
    ,
  --добавим к доступным типам для расписания Пересменку
  insert_cd as (
    insert into ttb.tt_action_type(tt_variant_id, action_type_id)
	select l_r.tt_variant_id , ttb.action_type_pkg$change_driver()         
         where not exists (select null from ttb.tt_action_type where (tt_variant_id, action_type_id) = 
                                	 								( l_r.tt_variant_id , ttb.action_type_pkg$change_driver() )                                                                                                     
        				  )
             and not exists (select null from insert_type_break where (insert_type_break.tt_variant_id, insert_type_break.action_type_id) =                            
                                                              ( l_r.tt_variant_id , ttb.action_type_pkg$change_driver() )
                )                          
      returning *
    )
  ,--а также Обед
   insert_dnr as (
    insert into ttb.tt_action_type(tt_variant_id,action_type_id)
	select l_r.tt_variant_id , ttb.action_type_pkg$dinner()          
         where not exists (select null from ttb.tt_action_type where (tt_variant_id, action_type_id) =                            
                                 									( l_r.tt_variant_id , ttb.action_type_pkg$dinner() )
        				  )
           and not exists (select null from insert_type_break where (insert_type_break.tt_variant_id, insert_type_break.action_type_id) =                            
                                 									( l_r.tt_variant_id , ttb.action_type_pkg$dinner() )
           				  )
      returning *
    )
  ,--, Отстой http://redmine.advantum.ru/issues/22441
   insert_ps as (
    insert into ttb.tt_action_type(tt_variant_id,action_type_id)	
         select l_r.tt_variant_id , ttb.action_type_pkg$pause()
         where not exists (select * from ttb.tt_action_type where (tt_variant_id, action_type_id) =                                
                                 									( l_r.tt_variant_id , ttb.action_type_pkg$pause() )
        				  )
             and not exists (select null from insert_type_break where (insert_type_break.tt_variant_id, insert_type_break.action_type_id) =                            
                                                              ( l_r.tt_variant_id , ttb.action_type_pkg$pause() )
                )
      returning *
    )
 ,--, Переключение http://redmine.advantum.ru/issues/23677
   insert_switch as (
    insert into ttb.tt_action_type(tt_variant_id,action_type_id)	
         select l_r.tt_variant_id , ttb.action_type_pkg$switch()
         where not exists (select * from ttb.tt_action_type where (tt_variant_id, action_type_id) =                                
                                 									( l_r.tt_variant_id , ttb.action_type_pkg$switch() )
        				  )
             and not exists (select null from insert_type_break where (insert_type_break.tt_variant_id, insert_type_break.action_type_id) =                            
                                                              ( l_r.tt_variant_id , ttb.action_type_pkg$switch() )
                )
      returning *
    )
  ,--, ТО http://redmine.advantum.ru/issues/24302
   insert_maintenance as (
    insert into ttb.tt_action_type(tt_variant_id,action_type_id)	
         select l_r.tt_variant_id , ttb.action_type_pkg$maintenance()
         where not exists (select * from ttb.tt_action_type where (tt_variant_id, action_type_id) =                                
                                 									( l_r.tt_variant_id , ttb.action_type_pkg$maintenance() )
        				  )
             and not exists (select null from insert_type_break where (insert_type_break.tt_variant_id, insert_type_break.action_type_id) =                            
                                                              ( l_r.tt_variant_id , ttb.action_type_pkg$maintenance() )
                )
      returning *
    )
  ,
  insert_bn_stop as ( 
  --добавляем все остановки для межрейсовых стоянок новые
  insert into ttb.tt_at_stop(tt_action_type_id, stop_item2round_id)
    select it.tt_action_type_id, adata.stop_item2round_id
    from adata
      join insert_bn_round_break it on 1 = 1
    where is_kp and order_num != 1
      and not exists (select null from ttb.tt_at_stop where (tt_action_type_id, stop_item2round_id) = (it.tt_action_type_id, adata.stop_item2round_id))
    )
  ,
  insert_dnr_stop as ( 
  --добавляем все остановки для новых обедов
  insert into ttb.tt_at_stop(tt_action_type_id, stop_item2round_id)
    select dnr.tt_action_type_id, adata.stop_item2round_id
    from adata
      join insert_dnr dnr on 1 = 1
    where is_dinner and order_num != 1
      and not exists (select null from ttb.tt_at_stop where (tt_action_type_id, stop_item2round_id) = (dnr.tt_action_type_id, adata.stop_item2round_id))
    )
  ,
  insert_dnr_stop_old as ( 
  --добавляем все остановки для обедов
  insert into ttb.tt_at_stop(tt_action_type_id, stop_item2round_id)
        select distinct tat.tt_action_type_id, (select adata.stop_item2round_id from adata where adata.is_dinner and adata.order_num != 1 limit 1 )
    from  ttb.tt_action_type tat 
    where tat.action_type_id = ttb.action_type_pkg$dinner ()
      and tat.tt_variant_id = l_r.tt_variant_id
      and not exists (select null from ttb.tt_at_stop where (tt_action_type_id, stop_item2round_id) = (tat.tt_action_type_id, (select adata.stop_item2round_id from adata where adata.is_dinner and adata.order_num != 1 limit 1 )))
	  and (select adata.stop_item2round_id from adata where adata.is_dinner and adata.order_num != 1 limit 1) is not null    
    )
    ,
  insert_bn_stop_old as ( 
  --добавляем все остановки для МО
  insert into ttb.tt_at_stop(tt_action_type_id, stop_item2round_id)
        select distinct tat.tt_action_type_id, (select adata.stop_item2round_id from adata where adata.is_kp and adata.order_num != 1 limit 1 )
    from  ttb.tt_action_type tat 
    where tat.action_type_id = ttb.action_type_pkg$bn_round_break()
      and tat.tt_variant_id = l_r.tt_variant_id
      and not exists (select null from ttb.tt_at_stop where (tt_action_type_id, stop_item2round_id) = (tat.tt_action_type_id, (select adata.stop_item2round_id from adata where adata.is_kp and adata.order_num != 1 limit 1 )))
	  and (select adata.stop_item2round_id from adata where adata.is_kp and adata.order_num != 1 limit 1) is not null    
    )
  /*,insert_all_stops as (
  insert into ttb.tt_at_round_stop(stop_item2round_id, tt_at_round_id)
    select adata.stop_item2round_id, irn.tt_at_round_id
    from adata
      join ttb.tt_action_type tat on tat.action_type_id = adata.action_type_id
      							 and tat.tt_variant_id = l_r.tt_variant_id
      join insert_at_round_new irn on irn.tt_action_type_id = tat.tt_action_type_id and irn.round_id = adata.round_id
    where not exists  (select null from ttb.tt_at_round_stop ttt where  (ttt.stop_item2round_id, ttt.tt_at_round_id) = (adata.stop_item2round_id, irn.tt_at_round_id) )
   
  ) */ 
  --добавляем все остановки
  ,insert_all_stops_new as (
  insert into ttb.tt_at_round_stop(stop_item2round_id, tt_at_round_id)
    select adata.stop_item2round_id, ir.tt_at_round_id
    from adata
      join insert_type it on it.action_type_id = adata.action_type_id
      join insert_at_round ir on ir.tt_action_type_id = it.tt_action_type_id and ir.round_id = adata.round_id
    where not exists  (select null from ttb.tt_at_round_stop ttt where  (ttt.stop_item2round_id, ttt.tt_at_round_id) = (adata.stop_item2round_id, ir.tt_at_round_id) )
  returning *
  )
  insert into ttb.atgrs_time(
		 tt_at_round_stop_id
		,start_time
		,fix_arrival_type_id
  		) 
  select  tars.tt_at_round_stop_id
         ,atg.start_time
         ,atg.fix_arrival_type_id
    from insert_all_stops_new tars 
    join atgrs_time_temp atg on atg.stop_item2round_id = tars.stop_item2round_id
    ;
    
    perform ttb.jf_tt_action_group_pkg$of_refresh (
    p_id_account,
    '{"tt_variant_id":"'|| l_r.tt_variant_id ||'"}'
    );
    
   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;