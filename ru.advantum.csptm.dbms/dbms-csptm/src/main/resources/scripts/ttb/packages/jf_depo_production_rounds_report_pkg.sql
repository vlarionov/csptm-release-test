create or replace function ttb.jf_depo_production_rounds_report_pkg$of_rows(p_id_account numeric, out p_rows refcursor, p_attr text)
    returns refcursor
language plpgsql
as $function$
declare
    l_f_INPLACE_K integer := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_INPLACE_K', true), 1);
    l_f_DT        date := jofl.jofl_pkg$extract_date(p_attr, 'f_DT', true);
begin
    open p_rows for
    with day_type as (
        select ct.tag_name
        from ttb.calendar cal
            join ttb.calendar_item ci on cal.calendar_id = ci.calendar_id
            join ttb.calendar_tag ct on ct.calendar_tag_id = ci.calendar_tag_id
        where cal.dt = l_f_DT and ct.calendar_tag_group_id = 7
    )
    select
        t.name                        p_tr_type_name,
        lower(t.name)                 p_tr_type_name,
        to_char(l_f_DT, 'dd.mm.yyyy') p_date,
        dt.tag_name                   p_type_day,
        t.tr_type_id                  p_tr_type_id
    from core.tr_type t
        join day_type dt on 1 = 1
    where t.tr_type_id = l_f_INPLACE_K;
end;
$function$;


create or replace function ttb.jf_depo_production_rounds_report_pkg$report(
    p_id_account   text,
    P_TIMEZONE     text,
    p_tr_type_name text,
    p_date         text,
    p_type_day     text,
    p_tr_type_id   text
)
    returns table(
        pp_tr_type_name   text,
        pp_date           text,
        pp_type_day       text,
        pp_depo_name      text,
        pp_00_round_count text,
        pp_D_round_count  text,
        pp_Y_round_count  text,
        pp_E_round_count  text,
        pp_I_round_count  text,
        pp_round_count    text
    )
language plpgsql
as $function$
begin
    if (p_tr_type_id = core.tr_type_pkg$tm() :: text)
    then
        return query
        select
            p_tr_type_name,
            p_date,
            p_type_day,
            case when (t.pp_ter_name is null)
                then 'Итого: '
            else t.pp_ter_name :: text end,
            sum(t.pp_00_round_count) :: text,
            sum(t.pp_D_round_count) :: text,
            sum(t.pp_Y_round_count) :: text,
            sum(t.pp_E_round_count) :: text,
            sum(t.pp_I_round_count) :: text,
            sum(t.pp_round_count) :: text as prc
        from ttb.jf_depo_production_rounds_report_pkg$depo_ter_report(p_date, p_tr_type_id) t
        group by rollup (t.pp_ter_name)
        order by t.pp_ter_name, prc;
    else
        return query
        select
            p_tr_type_name,
            p_date,
            p_type_day,
            case when (t.pp_depo_name is null)
                then 'Итого: '
            else t.pp_depo_name :: text end,
            sum(t.pp_00_round_count) :: text,
            sum(t.pp_D_round_count) :: text,
            sum(t.pp_Y_round_count) :: text,
            sum(t.pp_E_round_count) :: text,
            sum(t.pp_I_round_count) :: text,
            sum(t.pp_round_count) :: text as prc
        from ttb.jf_depo_production_rounds_report_pkg$depo_ter_report(p_date, p_tr_type_id) t
        group by rollup (t.pp_depo_name)
        order by t.pp_depo_name, prc;
    end if;
end;
$function$;

create or replace function ttb.jf_depo_production_rounds_report_pkg$depo_ter_report(
    p_date       text,
    p_tr_type_id text
)
    returns table(
        pp_ter_name       text,
        pp_depo_name      text,
        pp_00_round_count integer,
        pp_D_round_count  integer,
        pp_Y_round_count  integer,
        pp_E_round_count  integer,
        pp_I_round_count  integer,
        pp_round_count    integer
    )
language plpgsql
as $function$
declare
    l_date date :=to_date(p_date, 'DD.MM.YYYY');
begin
    return query
    with action_arr as (
        select
            ttb.action_type_pkg$get_action_type_list(1 :: smallint, false)  as p1,
            ttb.action_type_pkg$get_action_type_list(2 :: smallint, false)  as p2,
            ttb.action_type_pkg$get_action_type_list(3 :: smallint, false)  as p3,
            ttb.action_type_pkg$get_action_type_list(4 :: smallint, false)  as p4,
            ttb.action_type_pkg$get_action_type_list(7 :: smallint, false)  as p5,
            ttb.action_type_pkg$get_action_type_list(25 :: smallint, false) as p6
    ), type_day_variant as (
        select
            tv.tt_variant_id,
            tv.route_variant_id
        from ttb.tt_variant tv
            join ttb.tt_calendar cl on tv.tt_variant_id = cl.tt_variant_id
            join ttb.calendar_tag ct on ct.calendar_tag_id = cl.calendar_tag_id
            join ttb.calendar_item ci on ct.calendar_tag_id = ci.calendar_tag_id
            join ttb.calendar cal on ci.calendar_id = cal.calendar_id
            join ttb.tt_out tto on tv.tt_variant_id = tto.tt_variant_id
            join ttb.tt_out2tt_tr to2tt on tto.tt_out_id = to2tt.tt_out_id
            join ttb.tt_tr tr on to2tt.tt_tr_id = tr.tt_tr_id
            join core.depo2territory d2t on tr.depo2territory_id = d2t.depo2territory_id
            join core.depo cd on d2t.depo_id = cd.depo_id
        where
            cal.dt = l_date
            and (tv.tt_status_id = ttb.jf_tt_status_pkg$cn_arch_status_id() or tv.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id())
            and ct.calendar_tag_group_id = ttb.calendar_pkg$get_weekday_type_tag_group_id()
            and tv.parent_tt_variant_id isnull
            and lower(tv.action_period) :: date <= l_date and
            coalesce(upper(tv.action_period), l_date) :: date >= l_date
        group by tv.tt_variant_id
    )
    select
        case when (t.name_short is null)
            then 'Итого: '
        else t.name_short :: text end                                                             pp_ter_name,
        case when (e.name_full is null)
            then 'Итого: '
        else e.name_full :: text end                                                              pp_depo_name,

        (count(distinct ta.tt_action_id)
             filter (where ta.action_type_id = any (p.p1)) +
         count(distinct ta.tt_action_id)
             filter (where tt_tr.is_second_car and ta.action_type_id = any (p.p1))) :: integer as is_second_car_round_00_count,

        (count(distinct ta.tt_action_id)
             filter (where ta.action_type_id = any (p.p3)) +
         count(distinct ta.tt_action_id)
             filter (where tt_tr.is_second_car and ta.action_type_id = any (p.p3))) :: integer as is_second_car_round_D_count,

        (count(distinct ta.tt_action_id)
             filter (where ta.action_type_id = any (p.p2)) +
         count(distinct ta.tt_action_id)
             filter (where tt_tr.is_second_car and ta.action_type_id = any (p.p2))) :: integer as is_second_car_round_Y_count,

        (count(distinct ta.tt_action_id)
             filter (where ta.action_type_id = any (p.p4)) +
         count(distinct ta.tt_action_id)
             filter (where tt_tr.is_second_car and ta.action_type_id = any (p.p4))) :: integer as is_second_car_round_E_count,

        (count(distinct ta.tt_action_id)
             filter (where ta.action_type_id = any (p.p5)) +
         count(distinct ta.tt_action_id)
             filter (where tt_tr.is_second_car and ta.action_type_id = any (p.p5))) :: integer as is_second_car_round_I_count,

        (count(distinct ta.tt_action_id)
             filter (where ta.action_type_id = any (p.p6)) +
         count(distinct ta.tt_action_id)
             filter (where tt_tr.is_second_car and ta.action_type_id = any (p.p6))) :: integer as is_second_car_round_count
    from type_day_variant tv
        join rts.route_variant rv on tv.route_variant_id = rv.route_variant_id
        join rts.route r on rv.route_variant_id = r.current_route_variant_id
        join ttb.tt_out o on tv.tt_variant_id = o.tt_variant_id
        join ttb.tt_action ta on o.tt_out_id = ta.tt_out_id
        join ttb.tt_out2tt_tr to2tt on o.tt_out_id = to2tt.tt_out_id
        join ttb.tt_tr tt_tr on to2tt.tt_tr_id = tt_tr.tt_tr_id
        join core.depo2territory d2t on d2t.depo2territory_id = tt_tr.depo2territory_id
        join core.depo cd on d2t.depo_id = cd.depo_id
        join core.entity e on cd.depo_id = e.entity_id
        join core.territory ter on d2t.territory_id = ter.territory_id
        join core.entity t on ter.territory_id = t.entity_id
        join action_arr p on 1 = 1
    where
        r.tr_type_id = p_tr_type_id :: smallint
    group by t.name_short, e.name_full
    order by e.name_full, is_second_car_round_count;
end;
$function$;

--drop function ttb."jf_depo_production_rounds_report_pkg$depo_ter_report"( text, text, text, text, text, text );