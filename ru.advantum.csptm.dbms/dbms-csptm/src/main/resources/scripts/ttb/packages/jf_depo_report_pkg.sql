create or replace function ttb.jf_depo_report_pkg$of_rows(p_id_account numeric, out p_rows refcursor, p_attr text)
    returns refcursor
language plpgsql
as $function$
declare
    l_f_dt timestamp := jofl.jofl_pkg$extract_date(p_attr, 'f_DT', true);
begin
    open p_rows for
    select
        case when l_f_dt isnull
            then to_char(current_date :: date, 'dd.mm.yyyy')
        else to_char(l_f_dt :: date, 'dd.mm.yyyy') end as var_date,
        t.name                                         as tr_type_name,
        t.tr_type_id                                   as tr_type_id,
        ct.tag_name                                    as day_type,
        case when ct.calendar_tag_id = 8 :: smallint
            then '_work'
        else '_weekend' end                            as day_part
    from core.tr_type t
        join ttb.calendar cal on 1 = 1
        join ttb.calendar_item ci on cal.calendar_id = ci.calendar_id
        join ttb.calendar_tag ct on ct.calendar_tag_id = ci.calendar_tag_id
        left join ttb.calendar_tag_group ctg on ctg.calendar_tag_group_id = ct.calendar_tag_group_id
    where t.tr_type_id <> 4 and (cal.dt = l_f_DT or (cal.dt = current_date and l_f_DT isnull)) and ct.calendar_tag_group_id = 7;
end;
$function$;


create or replace function ttb.jf_depo_report_pkg$report_header(
    p_id_account text,
    P_TIMEZONE   text,
    p_tr_type_id text,
    p_date       text,
    p_day_type   text
)
    returns table(
        id_account   text,
        timezone     text,
        date         text,
        tr_type_id   text,
        tr_type_name text,
        path_sub     text,
        day_type     text
    )
language plpgsql
as $function$
declare
begin
    return query
    select
        p_id_account :: text,
        P_TIMEZONE :: text,
        p_date :: text,
        t.tr_type_id :: text,
        t.name :: text,
        t.tr_type_id :: text as path_sub,
        p_day_type
    from core.tr_type t
    where t.tr_type_id = p_tr_type_id :: smallint;
end;
$function$;


create or replace function ttb.jf_depo_report_pkg$report_detail_bus_weekend(
    p_id_account text,
    p_date       text,
    P_TIMEZONE   text,
    p_tr_type_id text
)
    returns table(
        p_park      text,
        p_OBK       text,
        p_BK9       text,
        p_MK        text,
        p_inv       text,
        p_line8     text,
        p_order_b   text,
        p_total     text,
        p_time1000  text,
        p_time1300  text,
        p_time1700  text,
        p_num3sm_sp text,
        p_num700    text,
        p_num700l   text,
        p_type2x_el text,
        p_num600_2  text,
        p_num600_3  text,
        p_num600    text,
        p_num800_2  text,
        p_num800    text,
        p_num900    text,
        p_num1sm_m  text,
        p_num1sm_n  text,
        p_line      text,
        p_order_e   text,
        p_educ      text,
        p_all       text
    )
language plpgsql
as $function$
declare
    l_date date :=to_date(p_date, 'DD.MM.YYYY');
begin
    return query
    with action_arr as (
        select (ttb.action_type_pkg$get_child_action_type_list(25 :: smallint) || array [22, 23, 24] :: smallint []) as p
    )
        , type_day_variant as (
        select tv.tt_variant_id
        from ttb.tt_variant tv
            join ttb.tt_calendar cl on tv.tt_variant_id = cl.tt_variant_id
            join ttb.calendar_tag ct on ct.calendar_tag_id = cl.calendar_tag_id
            join ttb.calendar_item ci on ct.calendar_tag_id = ci.calendar_tag_id
            join ttb.calendar cal on ci.calendar_id = cal.calendar_id
            join ttb.tt_out tto on tv.tt_variant_id = tto.tt_variant_id
            join ttb.tt_out2tt_tr to2tt on tto.tt_out_id = to2tt.tt_out_id
            join ttb.tt_tr tr on to2tt.tt_tr_id = tr.tt_tr_id
            join core.depo2territory d2t on tr.depo2territory_id = d2t.depo2territory_id
            join core.depo cd on d2t.depo_id = cd.depo_id
        where
            cal.dt = l_date and
            cd.tr_type_id = core.tr_type_pkg$bus()
            and (tv.tt_status_id = ttb.jf_tt_status_pkg$cn_arch_status_id() or tv.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id())
            and ct.calendar_tag_group_id = ttb.calendar_pkg$get_weekday_type_tag_group_id()
            and tv.parent_tt_variant_id isnull
            and lower(tv.action_period) :: date <= l_date and
            coalesce(upper(tv.action_period), l_date) :: date >= l_date
        group by tv.tt_variant_id
    )
        , tr_park as (
        select
            tr.tt_tr_id,
            d2t.depo_id,
            tr.tr_capacity_id
        from type_day_variant tv
            join ttb.tt_out tto on tv.tt_variant_id = tto.tt_variant_id
            join ttb.tt_out2tt_tr to2tt on tto.tt_out_id = to2tt.tt_out_id
            join ttb.tt_tr tr on to2tt.tt_tr_id = tr.tt_tr_id
            join core.depo2territory d2t on tr.depo2territory_id = d2t.depo2territory_id
        group by
            tr.tt_tr_id,
            d2t.depo_id,
            tr.tr_capacity_id
    )
        , tr_park_count as (
        select
            tp.depo_id,
            count(distinct tp.tt_tr_id)
                filter (where tp.tr_capacity_id = core.tr_capacity_pkg$huge())   obk,
            count(distinct tp.tt_tr_id)
                filter (where tp.tr_capacity_id = core.tr_capacity_pkg$big())    bk,
            count(distinct tp.tt_tr_id)
                filter (where tp.tr_capacity_id = core.tr_capacity_pkg$small())  mk,
            count(distinct tp.tt_tr_id)
                filter (where tp.tr_capacity_id = core.tr_capacity_pkg$middle()) ck,
            count(distinct tp.tt_tr_id)                                            all_type
        from tr_park tp
        group by tp.depo_id
    )
        , max_min_time_action as (
        select
            distinct
            ta.tt_action_id,
            min(tai.time_begin)
            over (
                partition by tai.tt_action_id ) :: time as agg_time_begin,
            max(tai.time_end)
            over (
                partition by tai.tt_action_id ) :: time as agg_time_end
        from type_day_variant tv
            join ttb.tt_out tto on tv.tt_variant_id = tto.tt_variant_id
            join ttb.tt_action ta on tto.tt_out_id = ta.tt_out_id
            join action_arr aa on 1=1
            join ttb.tt_action_item tai on ta.tt_action_id = tai.tt_action_id
        where ta.action_type_id = any(aa.p)

    )
        , get_tr_in_time as (
        select
            d2t.depo_id,
            count(distinct tto.tt_out_id)
                filter (where TO_TIMESTAMP('08:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end) time800,
            count(distinct tto.tt_out_id)
                filter (where TO_TIMESTAMP('10:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end) time1000,
            count(distinct tto.tt_out_id)
                filter (where TO_TIMESTAMP('13:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end) time1300,
            count(distinct tto.tt_out_id)
                filter (where TO_TIMESTAMP('17:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end) time1700
        from type_day_variant tv
            join ttb.tt_out tto on tv.tt_variant_id = tto.tt_variant_id
            join ttb.tt_action tta on tto.tt_out_id = tta.tt_out_id
            join max_min_time_action mmta on mmta.tt_action_id = tta.tt_action_id
            join action_arr aa on 1 = 1
            join ttb.tt_out2tt_tr to2tt on tto.tt_out_id = to2tt.tt_out_id
            join ttb.tt_tr tr on to2tt.tt_tr_id = tr.tt_tr_id
            join core.depo2territory d2t on tr.depo2territory_id = d2t.depo2territory_id
        where 1 = 1
              and tta.action_type_id = any (aa.p)
        group by
            d2t.depo_id
    )
        , out_tr_park as (
        select
            d2t.depo_id,
            count(distinct tto.tt_out_id)
                filter (where tto.tt_out_num between 300 and 399)                                                           p_num300,
            count(distinct tto.tt_out_id)
                filter (where tto.tt_out_num between 700 and 799 and (tta.dt_action :: time
                between TO_TIMESTAMP('00:00:00', 'HH24:MI:SS') :: time and TO_TIMESTAMP('13:00:00', 'HH24:MI:SS') :: time)) p_num700,
            (count(distinct tto.tt_out_id)
                filter (where tto.tt_out_num between 700 and 799)) -
            (count(distinct tto.tt_out_id)
                filter (where (tto.tt_out_num between 700 and 799) and (tta.dt_action :: time
                between TO_TIMESTAMP('00:00:00', 'HH24:MI:SS') :: time and TO_TIMESTAMP('13:00:00', 'HH24:MI:SS')::time)))    as    p_num700l,
            count(distinct tto.tt_out_id)
                filter (where tto.tt_out_num between 800 and 899)                                                           p_num800,
            count(distinct tto.tt_out_id)
                filter (where tto.tt_out_num between 900 and 999)                                                           p_num900,
            count(distinct tto.tt_out_id)
                filter (where tto.tt_out_num between 600 and 699)                                                           p_num600,
            count(distinct tto.tt_out_id)
                filter (where tto.tt_out_num between 200 and 299)                                                           p_num200,
            count(distinct tto.tt_out_id)
                filter (where tto.tt_out_num between 100 and 199)                                                           p_num100,
            count(distinct tto.tt_out_id)                                                                                                        p_num_all
        from type_day_variant tv
            join ttb.tt_out tto on tv.tt_variant_id = tto.tt_variant_id
            join ttb.tt_action tta on tto.tt_out_id = tta.tt_out_id
            join action_arr arr on 1 = 1
            join ttb.tt_out2tt_tr to2tt on tto.tt_out_id = to2tt.tt_out_id
            join ttb.tt_tr tr on to2tt.tt_tr_id = tr.tt_tr_id
            join core.depo2territory d2t on tr.depo2territory_id = d2t.depo2territory_id
        where tta.action_type_id =any (arr.p)
        group by d2t.depo_id
    )
    select
        case when e.name_full isnull
            then 'Итого: '
        else e.name_full :: text end as p_park,
        sum(tpc.obk) :: text         as p_OBK,
        sum(tpc.bk) :: text          as p_BK9,
        sum(tpc.mk) :: text          as p_MK,
        sum(0) :: text               as p_inv,
        sum(gtit.time800) :: text    as p_line8,
        sum(0) :: text               as p_order_b,
        sum(gtit.time800) :: text    as p_total,
        sum(gtit.time1000) :: text   as p_time1000,
        sum(gtit.time1300) :: text   as p_time1300,
        sum(gtit.time1700) :: text   as p_time1700,
        sum(otp.p_num300) :: text    as p_num3sm_sp,
        sum(otp.p_num700) :: text    as p_num700,
        sum(otp.p_num700l) :: text   as p_num700l,
        sum(otp.p_num200) :: text    as p_type2x_el,
        sum(otp.p_num600) :: text    as p_num600_2,
        sum(otp.p_num600) :: text    as p_num600_3,
        sum(otp.p_num600) :: text    as p_num600,
        sum(otp.p_num800) :: text    as p_num800_2,
        sum(otp.p_num800) :: text    as p_num800,
        sum(otp.p_num900) :: text    as p_num900,
        sum(otp.p_num100) :: text    as p_num1sm_m,
        sum(otp.p_num100) :: text    as p_num1sm_n,
        sum(otp.p_num_all) :: text   as p_line,
        sum(0) :: text               as p_order_e,
        sum(0) :: text               as p_educ,
        sum(otp.p_num_all) :: text   as p_all
    from tr_park_count tpc
        join get_tr_in_time gtit on gtit.depo_id = tpc.depo_id
        join out_tr_park otp on otp.depo_id = tpc.depo_id
        join core.entity e on e.entity_id = tpc.depo_id
    group by rollup (e.name_full);
end;
$function$;


create or replace function ttb.jf_depo_report_pkg$report_detail_bus_work(
    p_id_account text,
    p_date       text,
    P_TIMEZONE   text,
    p_tr_type_id text
)
    returns table(
        p_park      text,
        p_OBK       text,
        p_BK9       text,
        p_MK        text,
        p_inv       text,
        p_line7     text,
        p_order_b   text,
        p_total     text,
        p_time830   text,
        p_time1500  text,
        p_time1700  text,
        p_num3sm_sp text,
        p_num700    text,
        p_num700l   text,
        p_type2x_el text,
        p_num600_2  text,
        p_num600_3  text,
        p_num600    text,
        p_num800_2  text,
        p_num800    text,
        p_num900    text,
        p_num1sm_m  text,
        p_num1sm_n  text,
        p_line      text,
        p_order_e   text,
        p_educ      text,
        p_all       text
    )
language plpgsql
as $function$
declare
    l_date date :=to_date(p_date, 'DD.MM.YYYY');
begin
    return query
    with action_arr as (
        select (ttb.action_type_pkg$get_child_action_type_list(25 :: smallint) || array [22, 23, 24] :: smallint []) as p
    ), type_day_variant as (
        select tv.tt_variant_id
        from ttb.tt_variant tv
            join ttb.tt_calendar cl on tv.tt_variant_id = cl.tt_variant_id
            join ttb.calendar_tag ct on ct.calendar_tag_id = cl.calendar_tag_id
            join ttb.calendar_item ci on ct.calendar_tag_id = ci.calendar_tag_id
            join ttb.calendar cal on ci.calendar_id = cal.calendar_id
            join ttb.tt_out tto on tv.tt_variant_id = tto.tt_variant_id
            join ttb.tt_out2tt_tr to2tt on tto.tt_out_id = to2tt.tt_out_id
            join ttb.tt_tr tr on to2tt.tt_tr_id = tr.tt_tr_id
            join core.depo2territory d2t on tr.depo2territory_id = d2t.depo2territory_id
            join core.depo cd on d2t.depo_id = cd.depo_id
        where
            cal.dt = l_date and
            cd.tr_type_id = core.tr_type_pkg$bus()
            and (tv.tt_status_id = ttb.jf_tt_status_pkg$cn_arch_status_id() or tv.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id())
            and ct.calendar_tag_group_id = ttb.calendar_pkg$get_weekday_type_tag_group_id()
            and tv.parent_tt_variant_id isnull
            and lower(tv.action_period) :: date <= l_date and
            coalesce(upper(tv.action_period), l_date) :: date >= l_date
        group by tv.tt_variant_id
    )
        , tr_park as (
        select
            tr.tt_tr_id,
            d2t.depo_id,
            tr.tr_capacity_id
        from type_day_variant tv
            join ttb.tt_out tto on tv.tt_variant_id = tto.tt_variant_id
            join ttb.tt_out2tt_tr to2tt on tto.tt_out_id = to2tt.tt_out_id
            join ttb.tt_tr tr on to2tt.tt_tr_id = tr.tt_tr_id
            join core.depo2territory d2t on tr.depo2territory_id = d2t.depo2territory_id
        group by
            tr.tt_tr_id,
            d2t.depo_id,
            tr.tr_capacity_id
    )
        , tr_park_count as (
        select
            tp.depo_id,
            count(distinct tp.tt_tr_id)
                filter (where tp.tr_capacity_id = core.tr_capacity_pkg$huge())   obk,
            count(distinct tp.tt_tr_id)
                filter (where tp.tr_capacity_id = core.tr_capacity_pkg$big())    bk,
            count(distinct tp.tt_tr_id)
                filter (where tp.tr_capacity_id = core.tr_capacity_pkg$small())  mk,
            count(distinct tp.tt_tr_id)
                filter (where tp.tr_capacity_id = core.tr_capacity_pkg$middle()) ck,
            count(distinct tp.tt_tr_id)                                          all_type
        from tr_park tp
        group by tp.depo_id
    )
        , max_min_time_action as (
        select
            distinct
            ta.tt_action_id,
            min(tai.time_begin)
            over (
                partition by tai.tt_action_id ) :: time as agg_time_begin,
            max(tai.time_end)
            over (
                partition by tai.tt_action_id ) :: time as agg_time_end
        from type_day_variant tv
            join ttb.tt_out tto on tv.tt_variant_id = tto.tt_variant_id
            join ttb.tt_action ta on tto.tt_out_id = ta.tt_out_id
            join action_arr aa on 1=1
            join ttb.tt_action_item tai on ta.tt_action_id = tai.tt_action_id
        where ta.action_type_id = any(aa.p)

    )
        , get_tr_in_time as (
        select
            d2t.depo_id,
            count(distinct tto.tt_out_id)
                filter (where TO_TIMESTAMP('07:15:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end) time715,
            count(distinct tto.tt_out_id)
                filter (where TO_TIMESTAMP('08:30:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end) time830,
            count(distinct tto.tt_out_id)
                filter (where TO_TIMESTAMP('15:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end) time1500,
            count(distinct tto.tt_out_id)
                filter (where TO_TIMESTAMP('17:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end) time1700
        from type_day_variant tv
            join ttb.tt_out tto on tv.tt_variant_id = tto.tt_variant_id
            join ttb.tt_action tta on tto.tt_out_id = tta.tt_out_id
            join max_min_time_action mmta on mmta.tt_action_id = tta.tt_action_id
            join action_arr aa on 1 = 1
            join ttb.tt_out2tt_tr to2tt on tto.tt_out_id = to2tt.tt_out_id
            join ttb.tt_tr tr on to2tt.tt_tr_id = tr.tt_tr_id
            join core.depo2territory d2t on tr.depo2territory_id = d2t.depo2territory_id
        where 1 = 1
              and tta.action_type_id = any (aa.p)
        group by
            d2t.depo_id
    )
        , out_tr_park as (
        select
            d2t.depo_id,
            count(distinct tto.tt_out_id)
                filter (where tto.tt_out_num between 300 and 399)                                                           p_num300,
            count(distinct tto.tt_out_id)
                filter (where tto.tt_out_num between 700 and 799 and (tta.dt_action :: time
                between TO_TIMESTAMP('00:00:00', 'HH24:MI:SS') :: time and TO_TIMESTAMP('13:00:00', 'HH24:MI:SS') :: time)) p_num700,
            (count(distinct tto.tt_out_id)
                filter (where tto.tt_out_num between 700 and 799)) -
            (count(distinct tto.tt_out_id)
                filter (where (tto.tt_out_num between 700 and 799) and (tta.dt_action :: time
                between TO_TIMESTAMP('00:00:00', 'HH24:MI:SS') :: time and TO_TIMESTAMP('13:00:00', 'HH24:MI:SS')::time)))    as    p_num700l,
            count(distinct tto.tt_out_id)
                filter (where tto.tt_out_num between 800 and 899)                                                           p_num800,
            count(distinct tto.tt_out_id)
                filter (where tto.tt_out_num between 900 and 999)                                                           p_num900,
            count(distinct tto.tt_out_id)
                filter (where tto.tt_out_num between 600 and 699)                                                           p_num600,
            count(distinct tto.tt_out_id)
                filter (where tto.tt_out_num between 200 and 299)                                                           p_num200,
            count(distinct tto.tt_out_id)
                filter (where tto.tt_out_num between 100 and 199)                                                           p_num100,
            count(distinct tto.tt_out_id)                                                                                                        p_num_all
        from type_day_variant tv
            join ttb.tt_out tto on tv.tt_variant_id = tto.tt_variant_id
            join ttb.tt_action tta on tto.tt_out_id = tta.tt_out_id
            join action_arr arr on 1 = 1
            join ttb.tt_out2tt_tr to2tt on tto.tt_out_id = to2tt.tt_out_id
            join ttb.tt_tr tr on to2tt.tt_tr_id = tr.tt_tr_id
            join core.depo2territory d2t on tr.depo2territory_id = d2t.depo2territory_id
        where tta.action_type_id =any (arr.p)
        group by d2t.depo_id
    )
    select
        case when e.name_full isnull
            then 'Итого: '
        else e.name_full :: text end as p_park,
        sum(tpc.obk) :: text         as p_OBK,
        sum(tpc.bk) :: text          as p_BK9,
        sum(tpc.mk) :: text          as p_MK,
        sum(0) :: text               as p_inv,
        sum(gtit.time715) :: text    as p_line7,
        sum(0) :: text               as p_order_b,
        sum(gtit.time715) :: text    as p_total,
        sum(gtit.time830) :: text    as p_time830,
        sum(gtit.time1500) :: text   as p_time1500,
        sum(gtit.time1700) :: text   as p_time1700,
        sum(otp.p_num300) :: text    as p_num3sm_sp,
        sum(otp.p_num700) :: text    as p_num700,
        sum(otp.p_num700l) :: text   as p_num700l,
        sum(otp.p_num200) :: text    as p_type2x_el,
        sum(otp.p_num600) :: text    as p_num600_2,
        sum(otp.p_num600) :: text    as p_num600_3,
        sum(otp.p_num600) :: text    as p_num600,
        sum(otp.p_num800) :: text    as p_num800_2,
        sum(otp.p_num800) :: text    as p_num800,
        sum(otp.p_num900) :: text    as p_num900,
        sum(otp.p_num100) :: text    as p_num1sm_m,
        sum(otp.p_num100) :: text    as p_num1sm_n,
        sum(otp.p_num_all) :: text   as p_line,
        sum(0) :: text               as p_order_e,
        sum(0) :: text               as p_educ,
        sum(otp.p_num_all) :: text   as p_all
    from tr_park_count tpc
        join get_tr_in_time gtit on gtit.depo_id = tpc.depo_id
        join out_tr_park otp on otp.depo_id = tpc.depo_id
        join core.entity e on e.entity_id = tpc.depo_id
    group by rollup (e.name_full);
end;
$function$;


create or replace function ttb.jf_depo_report_pkg$report_detail_trol(
    p_id_account text,
    p_date       text,
    P_TIMEZONE   text,
    p_tr_type_id text
)
    returns table(
        p_park         text,
        num_tr_day     text,
        num_tr_obk_day text,
        num_tr715      text,
        num_tr_obk715  text,
        num_tr830      text,
        num_tr_obk830  text,
        num_tr1200     text,
        num_tr_obk1200 text,
        num_tr1500     text,
        num_tr_obk1500 text,
        num_tr1700     text,
        num_tr_obk1700 text,
        num_tr1800     text,
        num_tr_obk1800 text,
        num_tr2100     text,
        num_tr_obk2100 text,
        num_tr0000     text,
        num_tr_obk0000 text,
        num_tr0300     text,
        num_tr_obk0300 text,
        park_rounds    text
    )
language plpgsql
as $function$
declare
    l_date date :=to_date(p_date, 'DD.MM.YYYY');
begin
    return query
    with action_arr as (
        select (ttb.action_type_pkg$get_child_action_type_list(25 :: smallint) || array [22, 23, 24] :: smallint []) as p,
               ttb.action_type_pkg$get_child_action_type_list(25 :: smallint) as prod
    ), type_day_variant as (
        select tv.tt_variant_id, tv.ttv_name
        from ttb.tt_variant tv
            join ttb.tt_calendar cl on tv.tt_variant_id = cl.tt_variant_id
            join ttb.calendar_tag ct on ct.calendar_tag_id = cl.calendar_tag_id
            join ttb.calendar_item ci on ct.calendar_tag_id = ci.calendar_tag_id
            join ttb.calendar cal on ci.calendar_id = cal.calendar_id
            join ttb.tt_out tto on tv.tt_variant_id = tto.tt_variant_id
            join ttb.tt_out2tt_tr to2tt on tto.tt_out_id = to2tt.tt_out_id
            join ttb.tt_tr tr on to2tt.tt_tr_id = tr.tt_tr_id
            join core.depo2territory d2t on tr.depo2territory_id = d2t.depo2territory_id
            join core.depo cd on d2t.depo_id = cd.depo_id
        where
            cal.dt = l_date and
            cd.tr_type_id = core.tr_type_pkg$tb()
            and (tv.tt_status_id = ttb.jf_tt_status_pkg$cn_arch_status_id() or tv.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id())
            and ct.calendar_tag_group_id = ttb.calendar_pkg$get_weekday_type_tag_group_id()
            and tv.parent_tt_variant_id isnull
            and lower(tv.action_period) :: date <= l_date and
            coalesce(upper(tv.action_period), l_date) :: date >= l_date
        group by tv.tt_variant_id
    )
        , tr_park as (
        select
            tr.tt_tr_id,
            d2t.depo_id,
            tr.tr_capacity_id
        from type_day_variant tv
            join ttb.tt_out tto on tv.tt_variant_id = tto.tt_variant_id
            join ttb.tt_out2tt_tr to2tt on tto.tt_out_id = to2tt.tt_out_id
            join ttb.tt_tr tr on to2tt.tt_tr_id = tr.tt_tr_id
            join core.depo2territory d2t on tr.depo2territory_id = d2t.depo2territory_id
        group by
            tr.tt_tr_id,
            d2t.depo_id,
            tr.tr_capacity_id
    ) , max_min_time_action as (
        select
            distinct
            ta.tt_action_id,
            min(tai.time_begin)
            over (
                partition by tai.tt_action_id ) :: time as agg_time_begin,
            max(tai.time_end)
            over (
                partition by tai.tt_action_id ) :: time as agg_time_end
        from type_day_variant tv
            join ttb.tt_out tto on tv.tt_variant_id = tto.tt_variant_id
            join ttb.tt_action ta on tto.tt_out_id = ta.tt_out_id
            join action_arr aa on 1=1
            join ttb.tt_action_item tai on ta.tt_action_id = tai.tt_action_id
        where ta.action_type_id = any(aa.p)
    )
    select
        case when e.name_full is null
            then 'ИТОГО'
        else e.name_full end as                                                                                     p_park,
        count(distinct tr.tt_tr_id) :: text                                                                            num_tr_day,
        count(distinct tr.tt_tr_id)
            filter (where tto.tr_capacity_id = core.tr_capacity_pkg$huge()) :: text                                 num_tr_obk_day,
        count(distinct tr.tt_tr_id)
            filter (where (TO_TIMESTAMP('07:15:00', 'HH24:MI:SS') :: time
            between mmta.agg_time_begin and mmta.agg_time_end)) :: text num_tr715,
        count(distinct tr.tt_tr_id)
            filter (where (TO_TIMESTAMP('07:15:00', 'HH24:MI:SS') :: time
            between mmta.agg_time_begin and mmta.agg_time_end) and
                          tto.tr_capacity_id = core.tr_capacity_pkg$huge()) :: text                                 num_tr_obk715,
        count(distinct tr.tt_tr_id)
            filter (where TO_TIMESTAMP('08:30:00', 'HH24:MI:SS') :: time
            between mmta.agg_time_begin and mmta.agg_time_end) :: text  num_tr830,
        count(distinct tr.tt_tr_id)
            filter (where TO_TIMESTAMP('08:30:00', 'HH24:MI:SS') :: time
                          between mmta.agg_time_begin and mmta.agg_time_end and
                          tto.tr_capacity_id = core.tr_capacity_pkg$huge()) :: text                                 num_tr_obk830,
        count(distinct tr.tt_tr_id)
            filter (where TO_TIMESTAMP('12:00:00', 'HH24:MI:SS') :: time
            between mmta.agg_time_begin and mmta.agg_time_end) :: text  num_tr1200,
        count(distinct tr.tt_tr_id)
            filter (where TO_TIMESTAMP('12:00:00', 'HH24:MI:SS') :: time
                          between mmta.agg_time_begin and mmta.agg_time_end and
                          tto.tr_capacity_id = core.tr_capacity_pkg$huge()) :: text                                 num_tr_obk1200,
        count(distinct tr.tt_tr_id)
            filter (where TO_TIMESTAMP('15:00:00', 'HH24:MI:SS') :: time
            between mmta.agg_time_begin and mmta.agg_time_end) :: text  num_tr1500,
        count(distinct tr.tt_tr_id)
            filter (where TO_TIMESTAMP('15:00:00', 'HH24:MI:SS') :: time
                          between mmta.agg_time_begin and mmta.agg_time_end and
                          tto.tr_capacity_id = core.tr_capacity_pkg$huge()) :: text                                 num_tr_obk1500,
        count(distinct tr.tt_tr_id)
            filter (where TO_TIMESTAMP('17:00:00', 'HH24:MI:SS') :: time
            between mmta.agg_time_begin and mmta.agg_time_end) :: text  num_tr1700,
        count(distinct tr.tt_tr_id)
            filter (where TO_TIMESTAMP('17:00:00', 'HH24:MI:SS') :: time
                          between mmta.agg_time_begin and mmta.agg_time_end and
                          tto.tr_capacity_id = core.tr_capacity_pkg$huge()) :: text                                 num_tr_obk1700,
        count(distinct tr.tt_tr_id)
            filter (where TO_TIMESTAMP('18:00:00', 'HH24:MI:SS') :: time
            between mmta.agg_time_begin and mmta.agg_time_end) :: text  num_tr1800,
        count(distinct tr.tt_tr_id)
            filter (where TO_TIMESTAMP('18:00:00', 'HH24:MI:SS') :: time
                          between mmta.agg_time_begin and mmta.agg_time_end and
                          tto.tr_capacity_id = core.tr_capacity_pkg$huge()) :: text                                 num_tr_obk1800,
        count(distinct tr.tt_tr_id)
            filter (where TO_TIMESTAMP('21:00:00', 'HH24:MI:SS') :: time
            between mmta.agg_time_begin and mmta.agg_time_end) :: text  num_tr2100,
        count(distinct tr.tt_tr_id)
            filter (where TO_TIMESTAMP('21:00:00', 'HH24:MI:SS') :: time
                          between mmta.agg_time_begin and mmta.agg_time_end and
                          tto.tr_capacity_id = core.tr_capacity_pkg$huge()) :: text                                 num_tr_obk2100,
        count(distinct tr.tt_tr_id)
            filter (where TO_TIMESTAMP('00:00:00', 'HH24:MI:SS') :: time
            between mmta.agg_time_begin and mmta.agg_time_end) :: text  num_tr0000,
        count(distinct tr.tt_tr_id)
            filter (where TO_TIMESTAMP('00:00:00', 'HH24:MI:SS') :: time
                          between mmta.agg_time_begin and mmta.agg_time_end and
                          tto.tr_capacity_id = core.tr_capacity_pkg$huge()) :: text                                 num_tr_obk0000,
        count(distinct tr.tt_tr_id)
            filter (where TO_TIMESTAMP('03:00:00', 'HH24:MI:SS') :: time
            between mmta.agg_time_begin and mmta.agg_time_end) :: text  num_tr0300,
        count(distinct tr.tt_tr_id)
            filter (where TO_TIMESTAMP('03:00:00', 'HH24:MI:SS') :: time
                          between mmta.agg_time_begin and mmta.agg_time_end and
                          tto.tr_capacity_id = core.tr_capacity_pkg$huge()) :: text                                 num_tr_obk0300,
        count(distinct ta.tt_action_id)
            filter (where ta.action_type_id= any(arr.prod)) :: text                      park_rounds
    from type_day_variant tv
        join ttb.tt_out tto on tv.tt_variant_id = tto.tt_variant_id
        join ttb.tt_out2tt_tr to2tt on tto.tt_out_id = to2tt.tt_out_id
        join ttb.tt_tr tr on to2tt.tt_tr_id = tr.tt_tr_id
        join core.depo2territory d2t on tr.depo2territory_id = d2t.depo2territory_id
        join action_arr arr on 1=1
        join ttb.tt_action ta on tto.tt_out_id = ta.tt_out_id
        join max_min_time_action mmta on mmta.tt_action_id=ta.tt_action_id
        join ttb.tt_action_group tg on ta.tt_action_group_id = tg.tt_action_group_id
        join core.entity e on e.entity_id = d2t.depo_id
    group by rollup (e.name_full);
end;
$function$;

create or replace function ttb.jf_depo_report_pkg$report_detail_tram(
    p_id_account text,
    p_date       text,
    P_TIMEZONE   text,
    p_tr_type_id text
)
    returns table(
        p_park                  text,
        num_entry_total_day     text,
        num_entry_tr_day        text,
        num_entry_obk_day       text,
        num_entry_total_tr_day  text,
        obk_day                 text,
        num_entry_total_0715    text,
        num_entry_tr_0715       text,
        num_entry_obk_0715      text,
        num_entry_total_tr_0715 text,
        obk_0715                text,
        num_entry_total_0800    text,
        num_entry_tr_0800       text,
        num_entry_obk_0800      text,
        num_entry_total_tr_0800 text,
        obk_0800                text,
        num_entry_total_0830    text,
        num_entry_tr_0830       text,
        num_entry_obk_0830      text,
        num_entry_total_tr_0830 text,
        obk_0830                text,
        num_entry_total_1000    text,
        num_entry_tr_1000       text,
        num_entry_obk_1000      text,
        num_entry_total_tr_1000 text,
        obk_1000                text,
        num_entry_total_1200    text,
        num_entry_tr_1200       text,
        num_entry_obk_1200      text,
        num_entry_total_tr_1200 text,
        obk_1200                text,
        num_entry_total_1300    text,
        num_entry_tr_1300       text,
        num_entry_obk_1300      text,
        num_entry_total_tr_1300 text,
        obk_1300                text,
        num_entry_total_1500    text,
        num_entry_tr_1500       text,
        num_entry_obk_1500      text,
        num_entry_total_tr_1500 text,
        obk_1500                text,
        num_entry_total_1700    text,
        num_entry_tr_1700       text,
        num_entry_obk_1700      text,
        num_entry_total_tr_1700 text,
        obk_1700                text,
        num_entry_total_1800    text,
        num_entry_tr_1800       text,
        num_entry_obk_1800      text,
        num_entry_total_tr_1800 text,
        obk_1800                text,
        num_entry_total_2100    text,
        num_entry_tr_2100       text,
        num_entry_obk_2100      text,
        num_entry_total_tr_2100 text,
        obk_2100                text,
        num_entry_total_0000    text,
        num_entry_tr_0000       text,
        num_entry_obk_0000      text,
        num_entry_total_tr_0000 text,
        obk_0000                text,
        num_entry_total_0300    text,
        num_entry_tr_0300       text,
        num_entry_obk_0300      text,
        num_entry_total_tr_0300 text,
        obk_0300                text,
        park_rounds             text
    )
language plpgsql
as $function$
declare
    l_date date :=to_date(p_date, 'DD.MM.YYYY');
begin
    return query
    with action_arr as (
        select (ttb.action_type_pkg$get_child_action_type_list(25 :: smallint) || array [22, 23, 24] :: smallint []) as p,
               ttb.action_type_pkg$get_child_action_type_list(25 :: smallint) as prod
    )
        , type_day_variant as (
        select tv.tt_variant_id
        from ttb.tt_variant tv
            join ttb.tt_calendar cl on tv.tt_variant_id = cl.tt_variant_id
            join ttb.calendar_tag ct on ct.calendar_tag_id = cl.calendar_tag_id
            join ttb.calendar_item ci on ct.calendar_tag_id = ci.calendar_tag_id
            join ttb.calendar cal on ci.calendar_id = cal.calendar_id
            join ttb.tt_out tto on tv.tt_variant_id = tto.tt_variant_id
            join ttb.tt_out2tt_tr to2tt on tto.tt_out_id = to2tt.tt_out_id
            join ttb.tt_tr tr on to2tt.tt_tr_id = tr.tt_tr_id
            join core.depo2territory d2t on tr.depo2territory_id = d2t.depo2territory_id
            join core.depo cd on d2t.depo_id = cd.depo_id
        where
            cal.dt = l_date and
            cd.tr_type_id = core.tr_type_pkg$tm()
            and (tv.tt_status_id = ttb.jf_tt_status_pkg$cn_arch_status_id() or tv.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id())
            and ct.calendar_tag_group_id = ttb.calendar_pkg$get_weekday_type_tag_group_id()
            and tv.parent_tt_variant_id isnull
            and not tto.sign_deleted
            and lower(tv.action_period) :: date <= l_date and
            coalesce(upper(tv.action_period), l_date) :: date >= l_date
        group by tv.tt_variant_id
    ), max_min_time_action as (
        select
            distinct
            ta.tt_action_id,
            min(tai.time_begin)
            over (
                partition by tai.tt_action_id ) :: time as agg_time_begin,
            max(tai.time_end)
            over (
                partition by tai.tt_action_id ) :: time as agg_time_end
        from type_day_variant tv
            join ttb.tt_out tto on tv.tt_variant_id = tto.tt_variant_id
            join ttb.tt_action ta on tto.tt_out_id = ta.tt_out_id
            join action_arr aa on 1=1
            join ttb.tt_action_item tai on ta.tt_action_id = tai.tt_action_id
        where ta.action_type_id = any(aa.p)
    )
        ,out_count as (
        select
            ce.name_short,

            count(distinct tto.tt_out_id)                                                                          num_entry_total_day,

            count(distinct tto.tt_out_id)
                filter (where tr.is_second_car)                                                            num_entry_tr_day,

            count(distinct tto.tt_out_id)
                filter (where tto.tr_capacity_id = core.tr_capacity_pkg$huge())                                    num_entry_obk_day,

            count(distinct tto.tt_out_id)
                filter (where (TO_TIMESTAMP('07:15:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_0715,

            count(distinct tto.tt_out_id)
                filter (where tr.is_second_car and (TO_TIMESTAMP('07:15:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) with_tr_0715,

            count(distinct tto.tt_out_id)
                filter (where tto.tr_capacity_id = core.tr_capacity_pkg$huge() and (TO_TIMESTAMP('07:15:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_obk_0715,


            count(distinct tto.tt_out_id)
                filter (where (TO_TIMESTAMP('08:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_0800,

            count(distinct tto.tt_out_id)
                filter (where tr.is_second_car and (TO_TIMESTAMP('08:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) with_tr_0800,

            count(distinct tto.tt_out_id)
                filter (where tto.tr_capacity_id = core.tr_capacity_pkg$huge() and (TO_TIMESTAMP('08:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_obk_0800,


            count(distinct tto.tt_out_id)
                filter (where (TO_TIMESTAMP('08:30:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_0830,

            count(distinct tto.tt_out_id)
                filter (where tr.is_second_car and (TO_TIMESTAMP('08:30:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) with_tr_0830,

            count(distinct tto.tt_out_id)
                filter (where tto.tr_capacity_id = core.tr_capacity_pkg$huge() and (TO_TIMESTAMP('08:30:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_obk_0830,


            count(distinct tto.tt_out_id)
                filter (where (TO_TIMESTAMP('10:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_1000,

            count(distinct tto.tt_out_id)
                filter (where tr.is_second_car and (TO_TIMESTAMP('10:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) with_tr_1000,

            count(distinct tto.tt_out_id)
                filter (where tto.tr_capacity_id = core.tr_capacity_pkg$huge() and (TO_TIMESTAMP('10:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_obk_1000,


            count(distinct tto.tt_out_id)
                filter (where (TO_TIMESTAMP('12:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_1200,

            count(distinct tto.tt_out_id)
                filter (where tr.is_second_car and (TO_TIMESTAMP('12:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) with_tr_1200,

            count(distinct tto.tt_out_id)
                filter (where tto.tr_capacity_id = core.tr_capacity_pkg$huge() and (TO_TIMESTAMP('12:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_obk_1200,


            count(distinct tto.tt_out_id)
                filter (where (TO_TIMESTAMP('13:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_1300,

            count(distinct tto.tt_out_id)
                filter (where tr.is_second_car and (TO_TIMESTAMP('13:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) with_tr_1300,

            count(distinct tto.tt_out_id)
                filter (where tto.tr_capacity_id = core.tr_capacity_pkg$huge() and (TO_TIMESTAMP('13:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_obk_1300,


            count(distinct tto.tt_out_id)
                filter (where (TO_TIMESTAMP('15:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_1500,

            count(distinct tto.tt_out_id)
                filter (where tr.is_second_car and (TO_TIMESTAMP('15:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) with_tr_1500,

            count(distinct tto.tt_out_id)
                filter (where tto.tr_capacity_id = core.tr_capacity_pkg$huge() and (TO_TIMESTAMP('15:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_obk_1500,


            count(distinct tto.tt_out_id)
                filter (where (TO_TIMESTAMP('17:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_1700,

            count(distinct tto.tt_out_id)
                filter (where tr.is_second_car and (TO_TIMESTAMP('17:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) with_tr_1700,

            count(distinct tto.tt_out_id)
                filter (where tto.tr_capacity_id = core.tr_capacity_pkg$huge() and (TO_TIMESTAMP('17:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_obk_1700,


            count(distinct tto.tt_out_id)
                filter (where (TO_TIMESTAMP('18:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_1800,

            count(distinct tto.tt_out_id)
                filter (where tr.is_second_car and (TO_TIMESTAMP('18:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) with_tr_1800,

            count(distinct tto.tt_out_id)
                filter (where tto.tr_capacity_id = core.tr_capacity_pkg$huge() and (TO_TIMESTAMP('18:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_obk_1800,


            count(distinct tto.tt_out_id)
                filter (where (TO_TIMESTAMP('21:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_2100,

            count(distinct tto.tt_out_id)
                filter (where tr.is_second_car and (TO_TIMESTAMP('21:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) with_tr_2100,

            count(distinct tto.tt_out_id)
                filter (where tto.tr_capacity_id = core.tr_capacity_pkg$huge() and (TO_TIMESTAMP('21:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_obk_2100,


            count(distinct tto.tt_out_id)
                filter (where (TO_TIMESTAMP('00:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_0000,

            count(distinct tto.tt_out_id)
                filter (where tr.is_second_car and (TO_TIMESTAMP('00:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) with_tr_0000,

            count(distinct tto.tt_out_id)
                filter (where tto.tr_capacity_id = core.tr_capacity_pkg$huge() and (TO_TIMESTAMP('00:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_obk_0000,


            count(distinct tto.tt_out_id)
                filter (where (TO_TIMESTAMP('03:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_0300,

            count(distinct tto.tt_out_id)
                filter (where tr.is_second_car notnull and (TO_TIMESTAMP('03:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) with_tr_0300,

            count(distinct tto.tt_out_id)
                filter (where tto.tr_capacity_id = core.tr_capacity_pkg$huge() and (TO_TIMESTAMP('03:00:00', 'HH24:MI:SS') :: time
                between mmta.agg_time_begin and mmta.agg_time_end)) count_out_obk_0300,

            count(distinct tta.tt_action_id)
                filter (where tta.action_type_id = any (arr.prod))
            + count(distinct tta.tt_action_id)
                filter (where tr.is_second_car and tta.action_type_id = any (arr.prod)) as round_count

        from type_day_variant av
            join ttb.tt_out tto on av.tt_variant_id = tto.tt_variant_id
            join ttb.tt_action tta on tto.tt_out_id = tta.tt_out_id
            join max_min_time_action mmta on mmta.tt_action_id=tta.tt_action_id
            join action_arr arr on 1=1
            join ttb.tt_out2tt_tr to2tt on tto.tt_out_id = to2tt.tt_out_id
            join ttb.tt_tr tr on to2tt.tt_tr_id = tr.tt_tr_id
            join core.depo2territory d2t on tr.depo2territory_id = d2t.depo2territory_id

            join core.territory ter on d2t.territory_id = ter.territory_id
            join core.entity ce on ter.territory_id = ce.entity_id
        group by rollup (ce.name_short)
    )
    select
        case when o.name_short isnull
            then 'ИТОГО'
        else o.name_short :: text end                        as park,
        o.num_entry_total_day :: text                        as num_entry_total_day,
        o.num_entry_tr_day :: text                           as num_entry_tr_day,
        o.num_entry_obk_day :: text                          as num_entry_obk_day,
        (o.num_entry_total_day + o.num_entry_tr_day) :: text as num_entry_total_tr_day,
        case when o.num_entry_obk_day = 0
            then ''
        else 'ОБК' end :: text                               as obk_day,
        o.count_out_0715 :: text                             as num_entry_total_0715,
        o.with_tr_0715 :: text                               as num_entry_tr_0715,
        o.count_out_obk_0715 :: text                         as num_entry_obk_0715,
        (o.count_out_0715 + o.with_tr_0715) :: text          as num_entry_total_tr_0715,
        case when o.count_out_obk_0715 = 0
            then ''
        else 'ОБК' end :: text                               as obk_0715,
        o.count_out_0800 :: text                             as num_entry_total_0800,
        o.with_tr_0800 :: text                               as num_entry_tr_0800,
        o.count_out_obk_0800 :: text                         as num_entry_obk_0800,
        (o.count_out_0800 + o.with_tr_0800) :: text          as num_entry_total_tr_0800,
        case when o.count_out_obk_0800 = 0
            then ''
        else 'ОБК' end :: text                               as obk_0800,
        o.count_out_0830 :: text                             as num_entry_total_0830,
        o.with_tr_0830 :: text                               as num_entry_tr_0830,
        o.count_out_obk_0830 :: text                         as num_entry_obk_0830,
        (o.count_out_0830 + o.with_tr_0830) :: text          as num_entry_total_tr_0830,
        case when o.count_out_obk_0830 = 0
            then ''
        else 'ОБК' end :: text                               as obk_0830,
        o.count_out_1000 :: text                             as num_entry_total_1000,
        o.with_tr_1000 :: text                               as num_entry_tr_1000,
        o.count_out_obk_1000 :: text                         as num_entry_obk_1000,
        (o.count_out_1000 + o.with_tr_1000) :: text          as num_entry_total_tr_1000,
        case when o.count_out_obk_1000 = 0
            then ''
        else 'ОБК' end :: text                               as obk_1000,
        o.count_out_1200 :: text                             as num_entry_total_1200,
        o.with_tr_1200 :: text                               as num_entry_tr_1200,
        o.count_out_obk_1200 :: text                         as num_entry_obk_1200,
        (o.count_out_1200 + o.with_tr_1200) :: text          as num_entry_total_tr_1200,
        case when o.count_out_obk_1200 = 0
            then ''
        else 'ОБК' end :: text                               as obk_1200,
        o.count_out_1300 :: text                             as num_entry_total_1300,
        o.with_tr_1300 :: text                               as num_entry_tr_1300,
        o.count_out_obk_1300 :: text                         as num_entry_obk_1300,
        (o.count_out_1300 + o.with_tr_1300) :: text          as num_entry_total_tr_1300,
        case when o.count_out_obk_1300 = 0
            then ''
        else 'ОБК' end :: text                               as obk_1300,
        o.count_out_1500 :: text                             as num_entry_total_1500,
        o.with_tr_1500 :: text                               as num_entry_tr_1500,
        o.count_out_obk_1500 :: text                         as num_entry_obk_1500,
        (o.count_out_1500 + o.with_tr_1500) :: text          as num_entry_total_tr_1500,
        case when o.count_out_obk_1500 = 0
            then ''
        else 'ОБК' end :: text                               as obk_1500,
        o.count_out_1700 :: text                             as num_entry_total_1700,
        o.with_tr_1700 :: text                               as num_entry_tr_1700,
        o.count_out_obk_1700 :: text                         as num_entry_obk_1700,
        (o.count_out_1700 + o.with_tr_1700) :: text          as num_entry_total_tr_1700,
        case when o.count_out_obk_1700 = 0
            then ''
        else 'ОБК' end :: text                               as obk_1700,
        o.count_out_1800 :: text                             as num_entry_total_1800,
        o.with_tr_1800 :: text                               as num_entry_tr_1800,
        o.count_out_obk_1800 :: text                         as num_entry_obk_1800,
        (o.count_out_1800 + o.with_tr_1800) :: text          as num_entry_total_tr_1800,
        case when o.count_out_obk_1800 = 0
            then ''
        else 'ОБК' end :: text                               as obk_1800,
        o.count_out_2100 :: text                             as num_entry_total_2100,
        o.with_tr_2100 :: text                               as num_entry_tr_2100,
        o.count_out_obk_2100 :: text                         as num_entry_obk_2100,
        (o.count_out_2100 + o.with_tr_2100) :: text          as num_entry_total_tr_2100,
        case when o.count_out_obk_2100 = 0
            then ''
        else 'ОБК' end :: text                               as obk_2100,
        o.count_out_0000 :: text                             as num_entry_total_0000,
        o.with_tr_0000 :: text                               as num_entry_tr_0000,
        o.count_out_obk_0000 :: text                         as num_entry_obk_0000,
        (o.count_out_0000 + o.with_tr_0000) :: text          as num_entry_total_tr_0000,
        case when o.count_out_obk_0000 = 0
            then ''
        else 'ОБК' end :: text                               as obk_0000,
        o.count_out_0300 :: text                             as num_entry_total_0300,
        o.with_tr_0300 :: text                               as num_entry_tr_0300,
        o.count_out_obk_0300 :: text                         as num_entry_obk_0300,
        (o.count_out_0300 + o.with_tr_0300) :: text          as num_entry_total_tr_0300,
        case when o.count_out_obk_0300 = 0
            then ''
        else 'ОБК' end :: text                               as obk_0300,
        o.round_count :: text                                as park_rounds
    from out_count o;
end;
$function$;