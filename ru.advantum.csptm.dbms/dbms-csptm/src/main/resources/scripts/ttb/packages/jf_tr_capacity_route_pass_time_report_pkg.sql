create or replace function ttb.jf_tr_capacity_route_pass_time_report_pkg$of_rows(p_id_account numeric, out p_rows refcursor, p_attr text)
    returns refcursor
language plpgsql
as $function$
declare
    l_tt_variant_id integer := jofl.jofl_pkg$extract_number(p_attr, 'f_tt_variant_id', true);
begin
    open p_rows for
    select distinct
        p_id_account,
        l_tt_variant_id                                       p_tt_variant,
        v.route_variant_id                                    p_route_variant,
        r.route_id                                            p_route_id,
        r.route_num                                           p_route_num_str,
        to_char(lower(v.action_period) :: date, 'dd.mm.yyyy') p_date
    from ttb.tt_variant v
        join rts.route_variant rv on v.route_variant_id = rv.route_variant_id
        join rts.route r on rv.route_id = r.route_id
    where v.tt_variant_id = l_tt_variant_id;
end;
$function$;


create or replace function ttb.jf_tr_capacity_route_pass_time_report_pkg$report_header(
    p_id_account text,
    p_timezone   text,
    p_tt_variant text
)
    returns table(
        id_account         text,
        timezone           text,
        route_num          text,
        action_period_date text,
        tr_capacity_id     text,
        round_num          text,
        round_code         text,
        tt_variant_id      text
    )
language plpgsql
as $function$
declare
begin
    return query
    select
        p_id_account :: text,
        p_timezone :: text,
        r.route_num :: text,
        case when tv.order_date isnull then to_char(lower(tv.action_period) :: date, 'dd.mm.yyyy') else to_char(tv.order_date :: date, 'dd.mm.yyyy')end,
        tc.tr_capacity_id :: text,
        rr.round_num :: text,
        rr.code :: text as round_code,
        p_tt_variant
    from ttb.tt_variant tv
        join rts.route_variant rv on tv.route_variant_id = rv.route_variant_id
        join rts.route r on rv.route_id = r.route_id
        join rts.round rr on rv.route_variant_id = rr.route_variant_id
        join ttb.action_type a on rr.action_type_id = a.action_type_id
        join ttb.tt_out t on tv.tt_variant_id = t.tt_variant_id
        join core.tr_capacity tc on t.tr_capacity_id = tc.tr_capacity_id
    where
        rr.action_type_id = a.action_type_id and
        tv.tt_variant_id = p_tt_variant :: integer and
        ttb.action_type_pkg$is_production_round(rr.action_type_id)
    group by r.route_num, tc.short_name, tc.tr_capacity_id, r.route_id, tv.action_period, tv.order_date,rr.round_num, rr.code
    order by rr.code;
end;
$function$;


create or replace function ttb.jf_tr_capacity_route_pass_time_report_pkg$report_detail(
    p_id_account    text,
    P_TIMEZONE      text,
    p_capacity_id   text,
    p_round_code    text,
    p_tt_variant_id text,
    p_round_num     text
)
    returns table(
        capacity         text,
        action_type_name text,
        period           text,
        ab               text,
        ba               text,
        abba             text,
        stop_num_b       text,
        stop_num_a       text
    )
language plpgsql
as $function$
begin
    return query
    with stop2round_dur as (
        select
            tn.norm_id,
            extract(hour from nr.hour_from)                                     num_hour,
            (coalesce(nr.reg_inter_round_stop_dur, 0) +
              coalesce(nr.min_inter_round_stop_dur, 0)) / 60 as dur_time,
            rr.move_direction_id
        from ttb.tt_variant tv
            join ttb.norm tn on tv.norm_id = tn.norm_id
            join ttb.norm_round nr on tn.norm_id = nr.norm_id
            join rts.round rr on nr.round_id = rr.round_id
        where
            tv.tt_variant_id = p_tt_variant_id :: integer and rr.round_num = p_round_num :: integer and nr.tr_capacity_id = p_capacity_id :: smallint
        order by nr.round_id, num_hour
    )
        ,stop_item_round_num as(
        select
            si2rb.round_id,
            si2rb.stop_item_id stop_a,
            si2re.stop_item_id stop_b,
            r.move_direction_id
        from rts.stop_item2round si2rb
            join rts.stop_item2round si2re on si2re.order_num=1+si2rb.order_num
            join rts.round r on si2rb.round_id = r.round_id and si2re.round_id = r.round_id
        where r.round_num=p_round_num::integer
        order by si2rb.round_id,si2rb.order_num
    )
        , stops_hour as (
        select
            r.move_direction_id,
            extract(hour from nbs.hour_from)  num_hour,
            sum(nbs.between_stop_dur / 60) as duration
        from ttb.tt_variant tv
            join rts.route_variant rv on tv.route_variant_id = rv.route_variant_id
            join rts.round r on rv.route_variant_id = r.route_variant_id
            join ttb.norm tn on tv.norm_id = tn.norm_id
            join ttb.norm_between_stop nbs on tn.norm_id = nbs.norm_id
            join stop_item_round_num sirn on sirn.move_direction_id=r.move_direction_id
        where tv.tt_variant_id = p_tt_variant_id :: integer
              and r.round_num = p_round_num :: integer
              and nbs.tr_capacity_id = p_capacity_id :: smallint
              and sirn.stop_a=nbs.stop_item_1_id
              and sirn.stop_b=nbs.stop_item_2_id
        group by r.round_id, num_hour
        order by r.round_id, num_hour
    )
    select
        tc.short_name :: text                            as capacity,
        p_round_code :: text                             as action_type_name,
        sh1.num_hour || ':00' :: text                    as period,
        case when (sh1.duration + s2rd1.dur_time) isnull
            then ''
        else (sh1.duration + s2rd1.dur_time) :: text end as ab,
        case when (sh2.duration + s2rd2.dur_time) isnull
            then ''
        else (sh2.duration + s2rd2.dur_time) :: text end as ba,
        (
            coalesce(sh1.duration, 0) +
            coalesce(sh2.duration, 0) +
            coalesce(s2rd1.dur_time, 0) +
            coalesce(s2rd2.dur_time, 0)
        ) :: text                                        as abba,
        case when (s2rd1.dur_time) isnull
            then ''
        else (s2rd1.dur_time) :: text end                as stop_time_b,
        case when (s2rd2.dur_time) isnull
            then ''
        else (s2rd2.dur_time) :: text end                as stop_time_a
    from stops_hour sh1
        left join stops_hour sh2 on sh2.num_hour = sh1.num_hour and sh2.move_direction_id = 2 :: smallint
        left join stop2round_dur s2rd1 on s2rd1.num_hour = sh1.num_hour and s2rd1.move_direction_id = 1 :: smallint
        left join stop2round_dur s2rd2 on s2rd2.num_hour = sh1.num_hour and s2rd2.move_direction_id = 2 :: smallint
        left join core.tr_capacity tc on tc.tr_capacity_id = p_capacity_id :: smallint
        where sh1.move_direction_id = 1 :: smallint
    order by sh1.num_hour;
end;
$function$;