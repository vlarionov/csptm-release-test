CREATE OR REPLACE FUNCTION ttb.jf_tt_action_group_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.tt_action_group AS
$body$
declare 
   l_r 					ttb.tt_action_group%rowtype; 
   ln_tt_variant_id  	ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
begin 
   l_r.tt_action_group_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_action_group_id', true); 
   l_r.tt_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true); 
   l_r.tt_action_group_name := jofl.jofl_pkg$extract_varchar(p_attr, 'tt_action_group_name', true); 
   l_r.tt_action_group_code := jofl.jofl_pkg$extract_varchar(p_attr, 'tt_action_group_code', true); 
   l_r.sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'sign_deleted', true); 
   l_r.ref_group_kind_id := jofl.jofl_pkg$extract_number(p_attr, 'ref_group_kind_id', true); 
   
   if not ttb.jf_tt_variant_pkg$of_get_policy(ln_tt_variant_id)
     then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if;     

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_action_group_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.tt_action_group%rowtype;
begin 
   l_r := ttb.jf_tt_action_group_pkg$attr_to_rowtype(p_attr);
   
   delete from ttb.tt_action_type2group where tt_action_group_id = l_r.tt_action_group_id;

   delete from  ttb.tt_action_group where  tt_action_group_id = l_r.tt_action_group_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_action_group_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.tt_action_group%rowtype;
begin 
   l_r := ttb.jf_tt_action_group_pkg$attr_to_rowtype(p_attr);
   
   l_r.tt_action_group_id := nextval('ttb.tt_action_group_tt_action_group_id_seq');

   insert into ttb.tt_action_group select l_r.*;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_action_group_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 l_tt_variant_id ttb.tt_variant.tt_variant_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
begin 
 open p_rows for 
      select 
        tag.tt_action_group_id, 
        tag.tt_variant_id, 
        tag.tt_action_group_name, 
        tag.tt_action_group_code, 
        tag.sign_deleted,
        tag.ref_group_kind_id,
        rgk.ref_group_kind_name
      from ttb.tt_action_group tag
      join ttb.ref_group_kind rgk on rgk.ref_group_kind_id = tag.ref_group_kind_id
      where tag.tt_variant_id = l_tt_variant_id; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_action_group_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.tt_action_group%rowtype;
begin 
   l_r := ttb.jf_tt_action_group_pkg$attr_to_rowtype(p_attr);

   update ttb.tt_action_group set 
          tt_variant_id = l_r.tt_variant_id, 
          tt_action_group_name = l_r.tt_action_group_name, 
          tt_action_group_code = l_r.tt_action_group_code, 
          sign_deleted = l_r.sign_deleted
   where 
          tt_action_group_id = l_r.tt_action_group_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_action_group_pkg$of_refresh (
  p_id_account numeric,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
    ln_tt_variant_id  	ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
    l_r 				ttb.tt_action_group%rowtype; 
begin
    l_r := ttb.jf_tt_action_group_pkg$attr_to_rowtype(p_attr);

     --Очистить существующие группы
     delete from ttb.tt_action_type2group
     where tt_action_group_id in (select tag.tt_action_group_id 
     								from ttb.tt_action_group tag 
                                    where tag.tt_variant_id = ln_tt_variant_id);
     /*                               
     delete from ttb.tt_action_group 
     where tt_variant_id = ln_tt_variant_id;
     */
   
	 --Справочник групп-шаблонов
    with ref_gr as (
            select rag.ref_action_group_id
                  ,rag.gr_name 
                  ,rat.action_type_id
                  ,rag.ref_group_kind_id
            from ttb.ref_action_group rag 
            join ttb.ref_group_kind rgk on rgk.ref_group_kind_id = rag.ref_group_kind_id
            left join ttb.ref_action_type rat  on rag.ref_action_group_id = rat.ref_action_group_id            
        ) 
        , --Доступные типы действий для варианта расписания
          ttv_at as (      
            select *
            from ttb.tt_action_type tat 
            where tat.tt_variant_id = ln_tt_variant_id
        ) --Сравниваем
        , gr_at_agg as (
        select 
                rag.ref_action_group_id
               ,rag.gr_name
               ,rag.ref_group_kind_id
               ,case when 
                    	rag.ref_group_kind_id in (ttb.ref_group_kind_pkg$change_dr_action()
                        						, ttb.ref_group_kind_pkg$switch_from_park_action()
                                                , ttb.ref_group_kind_pkg$to_action()
                                                ) then 1 
               	     when 
                     	tat.action_type_id is null then 0
                   	 else 1
                end is_available
        from ref_gr rag
        left join ttv_at tat on rag.action_type_id = tat.action_type_id
        group by
               rag.ref_action_group_id
              ,rag.gr_name
              ,rag.ref_group_kind_id
              ,case when 
                    	rag.ref_group_kind_id in (ttb.ref_group_kind_pkg$change_dr_action()
                        						, ttb.ref_group_kind_pkg$switch_from_park_action()
                                                , ttb.ref_group_kind_pkg$to_action()
                                                )  then 1 
               	     when 
                     	tat.action_type_id is null then 0
                   	 else 1
                end 
         ),--Выбираем только доступные для данного варианта группы из шаблонов
           gr_at_avail as 
        (select gr_at_agg.ref_action_group_id
               ,gr_at_agg.gr_name
               ,gr_at_agg.ref_group_kind_id
               ,min(gr_at_agg.is_available) is_av
           from gr_at_agg 
          group by gr_at_agg.ref_action_group_id
                  ,gr_at_agg.gr_name 
                  ,gr_at_agg.ref_group_kind_id 
               
         ),--Добавляем группы действий
           ins_tt_gr as (
         insert into ttb.tt_action_group (
                           tt_variant_id
                          ,tt_action_group_name
                          ,ref_group_kind_id)
          select ln_tt_variant_id
                ,gr_at_avail.gr_name
                ,gr_at_avail.ref_group_kind_id
          from gr_at_avail 
          where gr_at_avail.is_av = 1
          and not exists (select null 
          				from ttb.tt_action_group tag 
                        where (tag.tt_variant_id, tag.tt_action_group_name, tag.ref_group_kind_id) = (ln_tt_variant_id, gr_at_avail.gr_name, gr_at_avail.ref_group_kind_id))
          group by ln_tt_variant_id
                  ,gr_at_avail.gr_name 
                  ,gr_at_avail.ref_group_kind_id   
          
         returning * 
         ),--Добавляем связь м/д группой и типом для данного варианта старые
           ins_tat2gr_old as (
         insert into ttb.tt_action_type2group (
                           tt_action_group_id
                          ,tt_action_type_id
                          ,order_num )
         select tag.tt_action_group_id
               ,tat.tt_action_type_id
               ,rat.order_num
         from ttb.tt_action_group tag 
         join gr_at_avail gaa on tag.tt_action_group_name = gaa.gr_name
         					 and tag.tt_variant_id = ln_tt_variant_id
         join ttb.ref_action_type rat on rat.ref_action_group_id = gaa.ref_action_group_id
         join ttb.tt_action_type tat on tat.action_type_id = rat.action_type_id
         							and tat.tt_variant_id = tag.tt_variant_id
                                    and tat.tt_action_type_id = 
                                    				case 
                                                      when ttb.jf_ref_action_group_pkg$is_main_traj(tag.tt_action_group_name)
                                                      then
                                    					case 
                                                          when tat.action_type_id = ttb.action_type_pkg$park_to_stop()
                                                          then
                                                            case 
                                                              when ttb.jf_tt_action_group_pkg$of_get_at_park2op_main(ln_tt_variant_id) is not null
                                                              then ttb.jf_tt_action_group_pkg$of_get_at_park2op_main(ln_tt_variant_id)
                                                              else ( select min(tat1.tt_action_type_id)
                                                                         from ttb.tt_action_type tat1
                                                                        where tat1.tt_variant_id = tat.tt_variant_id
                                                                          and tat1.action_type_id = tat.action_type_id)
                                                            end  
                                                          else ( select min(tat1.tt_action_type_id)
                                                                         from ttb.tt_action_type tat1
                                                                        where tat1.tt_variant_id = tat.tt_variant_id
                                                                          and tat1.action_type_id = tat.action_type_id)
                                                          end
                                                        when ttb.jf_ref_action_group_pkg$is_not_main_traj(tag.tt_action_group_name)
                                                        then
                                    					case 
                                                          when tat.action_type_id = ttb.action_type_pkg$park_to_stop()
                                                          then
                                                            case 
                                                              when ttb.jf_tt_action_group_pkg$of_get_at_park2op_not_main(ln_tt_variant_id) is not null
                                                              then ttb.jf_tt_action_group_pkg$of_get_at_park2op_not_main(ln_tt_variant_id)
                                                              else ( select min(tat1.tt_action_type_id)
                                                                         from ttb.tt_action_type tat1
                                                                        where tat1.tt_variant_id = tat.tt_variant_id
                                                                          and tat1.action_type_id = tat.action_type_id)
                                                            end  
                                                          else ( select min(tat1.tt_action_type_id)
                                                                         from ttb.tt_action_type tat1
                                                                        where tat1.tt_variant_id = tat.tt_variant_id
                                                                          and tat1.action_type_id = tat.action_type_id)
                                                          end
                                                        end /*main-not_main*/
         where not exists (select null 
         					from ttb.tt_action_type2group at2g 
                            where (at2g.tt_action_group_id, at2g.tt_action_type_id) = (tag.tt_action_group_id, tat.tt_action_type_id))
         group by tag.tt_action_group_id
               ,tat.tt_action_type_id
               ,rat.order_num  
         ) 
         --Добавляем связь м/д группой и типом для данного варианта новые
         insert into ttb.tt_action_type2group (
                           tt_action_group_id
                          ,tt_action_type_id
                          ,order_num )
         select itg.tt_action_group_id
               ,tat.tt_action_type_id
               ,rat.order_num
         from ins_tt_gr itg 
         join gr_at_avail gaa on itg.tt_action_group_name = gaa.gr_name
         join ttb.ref_action_type rat on rat.ref_action_group_id = gaa.ref_action_group_id
         join ttb.tt_action_type tat on tat.action_type_id = rat.action_type_id
         							and tat.tt_variant_id = ln_tt_variant_id
                                    and tat.tt_action_type_id = 
                                    					case 
                                                          when tat.action_type_id = ttb.action_type_pkg$park_to_stop()
                                                          then
                                                            case 
                                                              when ttb.jf_tt_action_group_pkg$of_get_at_park2op_main(ln_tt_variant_id) is not null
                                                              then ttb.jf_tt_action_group_pkg$of_get_at_park2op_main(ln_tt_variant_id)
                                                              else ( select min(tat1.tt_action_type_id)
                                                                         from ttb.tt_action_type tat1
                                                                        where tat1.tt_variant_id = tat.tt_variant_id
                                                                          and tat1.action_type_id = tat.action_type_id)
                                                            end  
                                                          else ( select min(tat1.tt_action_type_id)
                                                                         from ttb.tt_action_type tat1
                                                                        where tat1.tt_variant_id = tat.tt_variant_id
                                                                          and tat1.action_type_id = tat.action_type_id)
                                                          end                                   
         where not exists (select null 
         					from ttb.tt_action_type2group at2g 
                            where (at2g.tt_action_group_id, at2g.tt_action_type_id) = (itg.tt_action_group_id, tat.tt_action_type_id))
         group by itg.tt_action_group_id
               ,tat.tt_action_type_id
               ,rat.order_num;

  return null;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_action_group_pkg$of_copy (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r 		ttb.tt_action_group%rowtype;
   ln_cnt 	smallint;
   tt_action_group_id_old ttb.tt_action_group.tt_action_group_id%type;
begin 
   l_r := ttb.jf_tt_action_group_pkg$attr_to_rowtype(p_attr);
   
   select count(1)
   into ln_cnt
   from ttb.tt_action_group tag
   where tag.tt_action_group_name = l_r.tt_action_group_name
   	 and tag.tt_variant_id = l_r.tt_variant_id;
   
   tt_action_group_id_old 	:= l_r.tt_action_group_id;
   l_r.tt_action_group_id 	:= nextval('ttb.tt_action_group_tt_action_group_id_seq');
   l_r.tt_action_group_name := l_r.tt_action_group_name || ' - ' || (ln_cnt + 1)::text;
	
   	insert into ttb.tt_action_group 
    select l_r.*
    ;
 
    insert into ttb.tt_action_type2group
          (tt_action_group_id
          ,tt_action_type_id
          ,order_num
          ,plan_time)
    select l_r.tt_action_group_id
    	  ,at2g.tt_action_type_id
          ,at2g.order_num
          ,at2g.plan_time
    from ttb.tt_action_type2group at2g
	where at2g.tt_action_group_id = tt_action_group_id_old;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_action_group_pkg$of_get_at_park2op_main (
  p_tt_variant_id integer
)
RETURNS integer AS
$body$
declare
  ln_tt_action_type_id ttb.tt_action_type.tt_action_type_id%type;
begin
    
with at_park2op as (
  select tact.action_type_id, tact.tt_action_type_id, sir.stop_item_id, sir.stop_item2round_id
  from ttb.tt_action_type tact 
    join ttb.tt_at_round at_rnd on at_rnd.tt_action_type_id = tact.tt_action_type_id 
    join rts.round rnd on rnd.round_id = at_rnd.round_id  and not rnd.sign_deleted and rnd.move_direction_id = 1 /*AБ*/
    join rts.stop_item2round sir on sir.round_id = rnd.round_id and sir.order_num in
                                                                    (select max(t.order_num) from rts.stop_item2round t
                                                                    where t.round_id = sir.round_id and not t.sign_deleted) 
    															and not sir.sign_deleted
  where tact.tt_variant_id = p_tt_variant_id
    and tact.action_type_id = ttb.action_type_pkg$park_to_stop()
  )
  ,main_action as (
  
  select tact.action_type_id, tact.tt_action_type_id, sir.stop_item_id, sir.stop_item2round_id
  from ttb.tt_action_type tact 
    join ttb.tt_at_round at_rnd on at_rnd.tt_action_type_id = tact.tt_action_type_id 
    join rts.round rnd on rnd.round_id = at_rnd.round_id  and not rnd.sign_deleted and rnd.move_direction_id = 1 /*AБ*/
    join rts.stop_item2round sir on sir.round_id = rnd.round_id and sir.order_num in
                                                                    (select min(t.order_num) from rts.stop_item2round t
                                                                    where t.round_id = sir.round_id and not t.sign_deleted) 
    															and not sir.sign_deleted
  where tact.tt_variant_id = p_tt_variant_id
    and tact.action_type_id = ttb.action_type_pkg$main_round_ab()
  )
  select at_park2op.tt_action_type_id
  into ln_tt_action_type_id
  from at_park2op join main_action on at_park2op.stop_item_id = main_action.stop_item_id;
   
  return ln_tt_action_type_id;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_action_group_pkg$of_get_at_park2op_not_main (
  p_tt_variant_id integer
)
RETURNS integer AS
$body$
declare
  ln_tt_action_type_id ttb.tt_action_type.tt_action_type_id%type;
begin
    
with at_park2op as (
  select tact.action_type_id, tact.tt_action_type_id, sir.stop_item_id, sir.stop_item2round_id
  from ttb.tt_action_type tact 
    join ttb.tt_at_round at_rnd on at_rnd.tt_action_type_id = tact.tt_action_type_id 
    join rts.round rnd on rnd.round_id = at_rnd.round_id  and not rnd.sign_deleted and rnd.move_direction_id = 1 /*АБ*/
    join rts.stop_item2round sir on sir.round_id = rnd.round_id and sir.order_num in
                                                                    (select max(t.order_num) from rts.stop_item2round t
                                                                    where t.round_id = sir.round_id and not t.sign_deleted) 
    															and not sir.sign_deleted
  where tact.tt_variant_id = p_tt_variant_id
    and tact.action_type_id = ttb.action_type_pkg$park_to_stop()
  )
  ,main_action as (
  
  select tact.action_type_id, tact.tt_action_type_id, sir.stop_item_id, sir.stop_item2round_id
  from ttb.tt_action_type tact 
    join ttb.tt_at_round at_rnd on at_rnd.tt_action_type_id = tact.tt_action_type_id 
    join rts.round rnd on rnd.round_id = at_rnd.round_id  and not rnd.sign_deleted and rnd.move_direction_id = 2 /*БА*/
    join rts.stop_item2round sir on sir.round_id = rnd.round_id and sir.order_num in
                                                                    (select min(t.order_num) from rts.stop_item2round t
                                                                    where t.round_id = sir.round_id and not t.sign_deleted) 
    															and not sir.sign_deleted
  where tact.tt_variant_id = p_tt_variant_id
    and tact.action_type_id = ttb.action_type_pkg$main_round_ba()
  )
  select at_park2op.tt_action_type_id
  into ln_tt_action_type_id
  from at_park2op join main_action on at_park2op.stop_item_id = main_action.stop_item_id;
   
  return ln_tt_action_type_id;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------------