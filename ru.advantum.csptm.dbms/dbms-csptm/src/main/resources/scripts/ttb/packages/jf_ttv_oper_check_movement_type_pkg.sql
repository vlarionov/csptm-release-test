CREATE OR REPLACE FUNCTION ttb.jf_ttv_oper_check_movement_type_pkg$attr_to_rowtype (p_attr text) RETURNS ttb.ttv_oper_check_movement_type
	LANGUAGE plpgsql
AS $$

declare
   l_r ttb.ttv_oper_check_movement_type%rowtype;
begin
   l_r.tt_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
   l_r.movement_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'movement_type_id', true);

   return l_r;
end;

$$;



CREATE OR REPLACE FUNCTION ttb.jf_ttv_oper_check_movement_type_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$

declare
begin
 open p_rows for
      select
        tt_variant.tt_variant_id,
        ttv_oper_check_movement_type.movement_type_id,
        movement_type_name,
        tt_variant.ttv_name,
        tt_variant.ttv_comment,
        route.route_num
      from ttb.tt_variant
            join rts.route_variant on tt_variant.route_variant_id = route_variant.route_variant_id
            join rts.route on route_variant.route_id = route.route_id
            left join ttb.ttv_oper_check_movement_type on tt_variant.tt_variant_id = ttv_oper_check_movement_type.tt_variant_id
            left join ttb.movement_type on movement_type.movement_type_id = ttv_oper_check_movement_type.movement_type_id
      where parent_tt_variant_id is null
            and not tt_variant.sign_deleted
            and tt_variant.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id();
end;

$$;

CREATE OR REPLACE FUNCTION ttb.jf_ttv_oper_check_movement_type_pkg$of_update (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r ttb.ttv_oper_check_movement_type%rowtype;
begin
   l_r := ttb.jf_ttv_oper_check_movement_type_pkg$attr_to_rowtype(p_attr);

   if l_r.movement_type_id is null then
      delete from ttb.ttv_oper_check_movement_type
      where tt_variant_id = l_r.tt_variant_id;
   else
       insert into ttb.ttv_oper_check_movement_type select l_r.*
       on conflict (tt_variant_id)
       do update set movement_type_id = EXCLUDED.movement_type_id;
    end if;

   return null;
end;

$$;

