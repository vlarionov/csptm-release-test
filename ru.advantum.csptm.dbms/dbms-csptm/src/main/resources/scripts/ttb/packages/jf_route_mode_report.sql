create or replace function ttb."jf_route_mode_report_pkg$of_rows"(p_id_account numeric, out p_rows refcursor,
                                                                  p_attr       text)
  returns refcursor as
$function$
declare
  l_tt_variant_id ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'F_tt_variant_id', true);
  ln_tr_type_id   rts.route.tr_type_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id', true);
  ln_route_id     rts.route.route_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', true);
  ln_tt_status_id ttb.tt_variant.tt_status_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_tt_status_id', true);
  l_rf_db_method  text := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true), 'xxx');
  f_disp_dt       timestamp := jofl.jofl_pkg$extract_date(p_attr, 'f_disp_DT', true);
begin
  open p_rows for
  select
    ttv.tt_variant_id,
    ttv.norm_id,
    ttv.route_variant_id,
    ttv.is_all_tr_online,
    ttv.sign_deleted,
    --ttv.sys_period,
    ttv.tt_status_id,
    ttv.action_period,
    ttv.ttv_name,
    ttv.ttv_comment,
    r.route_num || ' (' || to_char(lower(rv.action_period), 'dd.mm.yyyy') || '-' ||
    coalesce(to_char(upper(rv.action_period), 'dd.mm.yyyy'), 'нвр') || ')' as                             p_filter_text,
    ttv.is_calc_indicator,
    n.norm_name,
    r.route_num,
    lower(ttv.action_period)                                                                              ap_b,
    upper(ttv.action_period)                                                                              ap_e,
    rv.action_period :: text                                                                              rv_action_period,
    st.tt_status_name,
    r.tr_type_id,
    r.route_id,
    case
    when ttb.agreement_pkg$is_available_to_agreement(p_id_account, ttv.tt_variant_id)
      then case when ttb.jf_tt_variant_pkg$available_to_cancel(ttv.tt_variant_id)
        then 'P{A},D{OF_TO_AGREEMENT, OF_DELETE, OF_CLEAR}'
           when ttb.jf_tt_variant_pkg$available_to_terminate(ttv.tt_variant_id)
             then 'P{A},D{OF_TO_AGREEMENT, OF_TERMINATE},P{RO},D{ap_e}'
           else 'P{A},D{OF_TO_AGREEMENT}'
           end
    else case when ttb.jf_tt_variant_pkg$available_to_cancel(ttv.tt_variant_id)
      then 'P{A},D{OF_DELETE, OF_CLEAR}'
         when ttb.jf_tt_variant_pkg$available_to_terminate(ttv.tt_variant_id)
           then 'P{A},D{OF_TERMINATE},P{RO},D{ap_e}'
         else null
         end
    end                                                                                                   "ROW$POLICY",
    '[' || json_build_object('CAPTION', 'Выходы', 'JUMP',
                             json_build_object('METHOD', 'ttb.tt_out', 'ATTR',
                                               '{"tt_variant_id":"' || ttv.tt_variant_id || '"}')) || ']' "JUMP_TO_OUT"
  from ttb.tt_variant ttv
    join ttb.norm n on n.norm_id = ttv.norm_id
    join ttb.tt_status st on st.tt_status_id = ttv.tt_status_id
    join rts.route_variant rv on ttv.route_variant_id = rv.route_variant_id
    join rts.route r on rv.route_id = r.route_id
  where ttv.parent_tt_variant_id is null
        and ttv.tt_status_id in (ttb.jf_tt_status_pkg$cn_work_status_id());
end;
/*declare
  l_tt_variant_id ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_tt_variant_id', true);
  ln_tr_type_id   rts.route.tr_type_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id', true);
  ln_route_id     rts.route.route_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', true);
  ln_tt_status_id ttb.tt_variant.tt_status_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_tt_status_id', true);
  f_disp_dt       timestamp := jofl.jofl_pkg$extract_date(p_attr, 'f_disp_DT', true);
begin
  open p_rows for
  select
    r.route_num || ' (' || to_char(lower(rv.action_period), 'dd.mm.yyyy') || '-' ||
    coalesce(to_char(upper(rv.action_period), 'dd.mm.yyyy'), 'нвр') || ')' as p_filter_text,
    tt_variant_id
  from ttb.tt_variant ttv
    join ttb.norm n on n.norm_id = ttv.norm_id
    join ttb.tt_status st on st.tt_status_id = ttv.tt_status_id
    join rts.route_variant rv on ttv.route_variant_id = rv.route_variant_id
    join rts.route r on rv.route_id = r.route_id
                        and ttv.tt_status_id in (ttb.jf_tt_status_pkg$cn_work_status_id())
                        and ttv.action_period @> f_disp_dt :: timestamp;

end;*/
$function$
language plpgsql;


/*create or replace function ttb.route_mode_report_pkg$get_data()
  returns table
  (out_num       text,
   "1_sh_1i"     text,
   "1_sh_1_br"   text,
   "1_sh_2i"     text,
   "1_sh_2_br"   text,
   "1_sh_last_i" text,
   "1_sh_total"  text,
   "2sh_1i"      text,
   "2sh_1b"      text,
   "2sh_2i"      text,
   "2sh_2br"     text,
   "2sh_li"      text,
   "2sh_total"   text,
   "total"       text
  )
language plpgsql
as $$
begin
  return query
  select
      text '301'   as out_num,
      text '5:17'  as "1_sh_1i",
      text '1:05'  as "1_sh_1_br",
      text ''      as "1_sh_2i",
      text ''      as "1_sh_2_br",
      text '3:57'  as "1_sh_last_i",
      text '9:14'  as "1_sh_total",
      text '4:21'  as "2sh_1i",
      text '1:00'  as "2sh_1_br",
      text ''      as "2sh_2i",
      text ''      as "2sh_2_br",
      text '5:13'  as "2sh_last_i",
      text '9:34'  as "2sh_total",
      text '18:48' as "total";
end;
$$;*/


