CREATE OR REPLACE FUNCTION ttb."jf_norm_between_stop_filter_from_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare
  l_norm_id ttb.norm.norm_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true);

begin
  open p_rows for
  WITh time_table AS(select distinct on (nbs.hour_from, nbs.hour_to) to_char(nbs.hour_from,'HH24:MI') time_from
  from ttb.norm_between_stop nbs
  WHERE norm_id = l_norm_id)
  select * from time_table ORDER BY time_from;
end;
$function$;