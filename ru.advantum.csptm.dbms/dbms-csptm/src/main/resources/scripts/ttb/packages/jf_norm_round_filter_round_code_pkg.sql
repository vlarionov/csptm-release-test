CREATE OR REPLACE FUNCTION ttb."jf_norm_round_filter_round_code_pkg$of_rows"(p_id_account numeric,
  OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare
  l_norm_id ttb.norm.norm_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true);
begin
  open p_rows for
  SELECT distinct on (rnd.code) rnd.code
  from ttb.norm_round nr
      JOIN rts.round rnd on rnd.round_id = nr.round_id
  WHERE nr.norm_id = l_norm_id;
end;
$function$;