create or replace function ttb."jf_rv_tune_pkg$attr_to_rowtype"(p_attr text)
    returns ttb.rv_tune
language plpgsql
as $$
declare
    l_r                  ttb.rv_tune%rowtype;
    l_break_duration_max integer;
    l_break_duration_min integer;
    l_to_duration_max    integer;
    l_break_round_time   integer;
    l_to_duration_min    integer;
    l_tr_type_id         smallint;
begin
    l_tr_type_id:=jofl.jofl_pkg$extract_number(p_attr, 'tr_type_id', true) :: smallint;
    select
        break_min_max.break_duration_min,
        break_min_max.break_duration_max,
        break_min_max.break_round_time
    into strict l_break_duration_min, l_break_duration_max, l_break_round_time
    from ttb.jf_rv_tune_pkg$get_break_min_max_time_by_type(l_tr_type_id) break_min_max;

    select
        to_min_max.to_duration_min,
        to_min_max.to_duration_max
    into strict l_to_duration_min, l_to_duration_max
    from ttb.jf_rv_tune_pkg$get_to_min_max_time_by_type(l_tr_type_id) to_min_max;

    l_r.rv_tune_id := jofl.jofl_pkg$extract_number(p_attr, 'rv_tune_id', true);
    l_r.route_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'route_variant_id', true);
    l_r.break_dinner_time := mod(jofl.jofl_pkg$extract_number(p_attr, 'break_dinner_time', true), 24 * 3600);
    l_r.break_tr_time := mod(jofl.jofl_pkg$extract_number(p_attr, 'break_tr_time', true), 24 * 3600);
    l_r.break_round_time := mod(jofl.jofl_pkg$extract_number(p_attr, 'break_round_time', true), 3600);
    if (l_r.break_dinner_time > l_break_duration_max and l_r.break_dinner_time <> 0)
    then raise exception '<<Время обеда (%ч : %мин) задано не корректно! Должно быть меньше допустимого значения: %ч : %мин>>', div(
        l_r.break_dinner_time / 60, 60), mod(l_r.break_dinner_time / 60, 60), div(l_break_duration_max / 60, 60), mod(
        l_break_duration_max / 60, 60);
    elsif ((l_r.break_tr_time not between l_to_duration_min and l_to_duration_max) and l_r.break_tr_time <> 0)
        then raise exception '<< Ориентировочная продолжительность перерыва на ТО (%ч : %мин) задано не корректно! Допустимый интервал: от %ч : %мин до %ч : %мин>>', div(
            l_r.break_tr_time / 60, 60), mod(l_r.break_tr_time / 60, 60), div(l_to_duration_min / 60, 60), mod(
            l_to_duration_min / 60, 60), div(l_to_duration_max / 60, 60), mod(l_to_duration_max / 60, 60);
    elseif (l_r.break_round_time > l_break_round_time and l_r.break_round_time <> 0)
        then raise exception '<< Максимальная продолжительность перерыва ( %мин) задано не корректно! Должно быть меньше допустимого значения: %мин>>',
        l_r.break_round_time / 60, l_break_round_time / 60;
    end if;
    return l_r;
end;
$$;
-----------------------------------
create or replace function ttb."jf_rv_tune_pkg$of_delete"(p_id_account numeric, p_attr text)
    returns text
language plpgsql
as $$
declare
    l_rv_tune_id ttb.rv_tune.rv_tune_id%type := jofl.jofl_pkg$extract_varchar(p_attr, 'rv_tune_id', true);
begin
    delete from ttb.rv_tune
    where rv_tune_id = l_rv_tune_id;
    return null;
end;
$$;

-----------------------------------
create or replace function ttb."jf_rv_tune_pkg$of_rows"(p_id_account numeric, out p_rows refcursor,
                                                        p_attr       text)
    returns refcursor
language plpgsql
as $$
declare
    l_route_variant_id rts.route_variant.route_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'route_variant_id', true);
    l_tr_type_id       rts.route_variant.route_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tr_type_id', true);
begin
    if exists(select *
              from ttb.rv_tune r
              where r.route_variant_id = l_route_variant_id)
    then
        open p_rows for
        select
            rv_tune_id,
            l_tr_type_id                                               tr_type_id,
            route_variant_id,
            case break_dinner_time
            when 0
                then ' '
            else
                ttb.jf_rv_tune_pkg$time_to_text(break_dinner_time)
            end                                                     as break_dinner_time_text,
            break_dinner_time,
            case break_tr_time
            when 0
                then ' '
            else ttb.jf_rv_tune_pkg$time_to_text(break_tr_time) end as break_tr_time_text,
            break_tr_time,
            case break_round_time
            when 0
                then ' '
            else
                case when div(div(break_round_time, 60), 10) > 0
                    then div(break_round_time, 60) :: text
                else '0' || div(break_round_time, 60)
                end
            end                                                     as break_round_time_text,
            break_round_time
        from ttb.rv_tune r
        where r.route_variant_id = l_route_variant_id;
    else
        open p_rows for
        select
            l_tr_type_id                     tr_type_id,
            null :: integer               as rv_tune_id,
            29 * 60 :: integer            as break_round_time,
            '29' :: text                  as break_round_time_text,
            l_route_variant_id :: integer as route_variant_id,
            ' ' :: text                   as break_dinner_time_text,
            ' ' :: text                   as break_tr_time_text
        limit 1;
    end if;
end;
$$;

-----------------------------------

create or replace function ttb."jf_rv_tune_pkg$of_update"(p_id_account numeric, p_attr text)
    returns text
language plpgsql
as $$
declare
    l_r ttb.rv_tune%rowtype;
begin
    l_r := ttb.jf_rv_tune_pkg$attr_to_rowtype(p_attr);

    if exists(select *
              from ttb.rv_tune r
              where r.route_variant_id = l_r.route_variant_id)
    then
        update ttb.rv_tune
        set
            break_dinner_time = l_r.break_dinner_time,
            break_tr_time     = l_r.break_tr_time,
            break_round_time  = l_r.break_round_time
        where
            rv_tune_id = l_r.rv_tune_id;
        return null;
    else
        insert into ttb.rv_tune (route_variant_id, break_dinner_time, break_tr_time, break_round_time)
        values (l_r.route_variant_id, l_r.break_dinner_time, l_r.break_tr_time, l_r.break_round_time);
        return null;
    end if;
end;
$$;

create or replace function ttb.jf_rv_tune_pkg$get_break_min_max_time_by_type(
    p_tr_type_id core.tr_type.tr_type_id%type
)
    returns table(break_duration_min int, break_duration_max int, break_round_time int)
language plpgsql
as $$
begin
    return query
    select
        min(tdm.time_min)          break_duration_min,
        max(tdm.time_max)          break_duration_max,
        max(ds.break_duration_max) break_round_time
    from core.tr_type t
        left join ttb.mode tm on t.tr_type_id = tm.tr_type_id
        left join ttb.dr_shift ds on tm.mode_id = ds.mode_id
        left join ttb.dr_mode tdm on ds.dr_shift_id = tdm.dr_shift_id
    where t.tr_type_id = p_tr_type_id and tdm.time_min <> 0;
end;
$$;

create or replace function ttb.jf_rv_tune_pkg$get_to_min_max_time_by_type(
    p_tr_type_id core.tr_type.tr_type_id%type
)
    returns table(to_duration_min int, to_duration_max int)
language plpgsql
as $$
begin
    return query
    select
        min(ttm.to_duration_min) to_duration_min,
        max(ttm.to_duration_max) to_duration_max
    from core.tr_type t
        join ttb.mode tm on t.tr_type_id = tm.tr_type_id
        left join ttb.tr_mode ttm on tm.mode_id = ttm.tr_mode_id
    where t.tr_type_id = p_tr_type_id and ttm.to_duration_min <> 0;
end;
$$;

create or replace function ttb.jf_rv_tune_pkg$time_to_text(
    p_time integer, out p_time_text text
)
    returns text
language plpgsql
as $$
declare
    l_hours  integer :=div(p_time / 60, 60);
    l_minuts integer :=mod(p_time / 60, 60);
begin
    select case when div(l_hours, 10) > 0
        then l_hours :: text
           else '0' || l_hours
           end
           || ':' ||
           case when div(l_minuts, 10) > 0
               then l_minuts :: text
           else '0' || l_minuts
           end
    into p_time_text;
end;
$$;
---------------------------------------------------------------------
create or replace function ttb.jf_rv_tune_pkg$get_break_dinner_time_by_rv(
    p_route_variant_id integer
)
    returns integer as
$body$
declare
    ln_time     integer;
    ln_time_def integer := 1800; --default value for dinner
begin
    select break_dinner_time
    into ln_time
    from ttb.rv_tune
    where route_variant_id = p_route_variant_id;

    return coalesce(ln_time, ln_time_def);

end;
$body$
language 'plpgsql'
volatile
called on null input
security invoker
cost 100;
----------------------------------------------------------------------
create or replace function ttb.jf_rv_tune_pkg$get_break_tr_time_by_rv(
    p_route_variant_id integer
)
    returns integer as
$body$
declare
    ln_time     integer;
    ln_time_def integer := 7260; --default value for tr
begin
    select break_tr_time
    into ln_time
    from ttb.rv_tune
    where route_variant_id = p_route_variant_id;

    return coalesce(ln_time, ln_time_def);

end;
$body$
language 'plpgsql'
volatile
called on null input
security invoker
cost 100;
---------------------------------------------------------------------
---------------------------------------------------------------------
