CREATE OR REPLACE FUNCTION ttb.jf_rep_mileage_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare

begin

 open p_rows for 
     select
	null as route_num, /* Номер маршрута */
 null as out_num, /* Номер выхода */
 null as dr_shift_num, /* Номер смены */
 null as garage_num, /* Гар. номер ТС */
 null as dr_tab_num, /* Водитель */
 null as all_linetime_fact, /* Общее линейное время факт */
 null as all_linetime_plan, /* Общее линейное время план. */
 null as all_round_cnt_fact, /* Общее количество рейсов факт. */
 null as all_round_cnt_plan, /* Общее количество рейсов план. */
 null as mile_prod_round_fact, /* Пробег произв. рейсов факт. */
 null as mile_prod_round_plan, /* Пробег произв. рейсов план. */
 null as prod_linetime_fact, /* Суммарное линейное время произв.рейсов факт. */
 null as prod_linetime_plan, /* Суммарное линейное время произв.рейсов план. */
 null as prod_round_cnt_fact, /* Количество произв. рейсов факт. */
 null as prod_round_cnt_plan, /* Количество произв. рейсов план. */
 null as tech_linetime_fact, /* Суммарное линейное время технологических рейсов факт. */
 null as tech_linetime_plan, /* Суммарное линейное время технологических рейсов план. */
 null as tech_round_cnt_fact, /* Количество технологических рейсов факт. */
 null as tech_round_cnt_plan /* Количество технологических рейсов план. */	 
;
end;
$function$
;
