﻿create or replace function ttb.norm_edit_type$auto()
returns integer as
  'select 1;'
language sql immutable;

create or replace function ttb.norm_edit_type$default()
  returns integer as
'select 2;'
language sql immutable;

create or replace function ttb.norm_edit_type$hand()
  returns integer as
'select 3;'
language sql immutable;