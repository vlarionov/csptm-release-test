create or replace function ttb.jf_rep_operreport4hours_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare 
 v_attr text;
begin
 /* добавляем параметр: тип отчета */
 v_attr := (p_attr::jsonb || jsonb_build_object('P_REPORT_TYPE', 'HOUR'))::text;
 -- select ttb.release_operreport_avg(p_id_account, v_attr) into p_rows;
 open p_rows for
  select r.depo_name,
         r.tr_type_name,
	       r.route_num,
		     r.order_date,
		     r.order_hours,
		     r.capacity_name,
		     r.tr_cnt
  from ttb.release_operreport(p_id_account, v_attr) r;
  
end;
$$;
