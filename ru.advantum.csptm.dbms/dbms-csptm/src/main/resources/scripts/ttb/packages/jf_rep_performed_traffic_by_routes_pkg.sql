CREATE OR REPLACE FUNCTION ttb.jf_rep_performed_traffic_by_routes_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare

begin

 open p_rows for 
     select
	null as depo_name, /* Наименование ТП */
 null as tr_type_name, /* Вид ТС */
 null as route_num, /* Номер маршрута */
 null as out_num, /* Номер выхода */
 null as dr_shift_num, /* Номер смены */
 null as garage_num, /* Гар. номер ТС */
 null as tr_licence, /* Гос. номер ТС */
 null as dr_tab_num, /* Водитель */
 null as round_type, /* Тип рейса */
 null as stop_name, /* Наименование ОП */
 null as hour_int, /* Часовые интервалы */
 null as outstand_round_cnt, /* Количество невыполн. рейсов */
 null as outstand_round_reason, /* Причина не выполнения рейсов */
 null as round_cnt_fact, /* Количество рейсов факт. */
 null as round_cnt_plan /* Количество рейсов план. */	 
;
end;
$function$
;
