CREATE OR REPLACE FUNCTION ttb."jf_tt_tr_pkg$attr_to_rowtype"(p_attr text)
 RETURNS ttb.tt_tr
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tt_tr%rowtype; 
begin 
   l_r.tt_tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_tr_id', true); 
   l_r.tt_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true); 
   l_r.tr_mode_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_mode_id', true); 
   l_r.depo2territory_id := jofl.jofl_pkg$extract_varchar(p_attr, 'depo2territory_id', true); 
   l_r.tr_capacity_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tr_capacity_id', true); 
   l_r.order_num := jofl.jofl_pkg$extract_varchar(p_attr, 'order_num', true); 
   l_r.is_second_car := jofl.jofl_pkg$extract_varchar(p_attr, 'is_second_car', true); 

   return l_r;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_tr_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$ 
declare
  l_tt_variant_id ttb.tt_variant.tt_variant_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
begin 
 open p_rows for
 select
   tt_tr.tt_tr_id,
   tt_tr.tt_variant_id,
   tt_tr.tr_mode_id,
   tt_tr.depo2territory_id,
   tt_tr.tr_capacity_id,
   tt_tr.order_num,
   depo.name_short as depo_name_short,
   depo.name_full as depo_name_full,
   terr.name_short as terr_name_short,
   terr.name_full as terr_name_full,
   m.mode_name,
   cap.short_name as capacity_short_name
 from ttb.tt_tr
   join ttb.mode m on m.mode_id = tt_tr.tr_mode_id
   join core.depo2territory d2t on d2t.depo2territory_id = tt_tr.depo2territory_id
   join core.entity depo on depo.entity_id =d2t.depo_id
   join core.entity terr on terr.entity_id = d2t.territory_id
   join core.tr_capacity cap on cap.tr_capacity_id = tt_tr.tr_capacity_id
  where tt_tr.tt_variant_id = l_tt_variant_id
  ;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_tr_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tt_tr%rowtype;
begin 
   l_r := ttb.jf_tt_tr_pkg$attr_to_rowtype(p_attr);

   update ttb.tt_tr set 
          /*tt_variant_id = l_r.tt_variant_id, 
          tr_mode_id = l_r.tr_mode_id, 
          depo2territory_id = l_r.depo2territory_id, 
          tr_capacity_id = l_r.tr_capacity_id, */
          order_num = l_r.order_num
   where 
          tt_tr_id = l_r.tt_tr_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_tr_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tt_tr%rowtype;
begin 
   l_r := ttb.jf_tt_tr_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.tt_tr where  tt_tr_id = l_r.tt_tr_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_tr_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.tt_tr%rowtype;
begin 
   l_r := ttb.jf_tt_tr_pkg$attr_to_rowtype(p_attr);
   
   l_r.is_second_car := coalesce(l_r.is_second_car, false);

   insert into ttb.tt_tr select l_r.*;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;