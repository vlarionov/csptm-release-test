/*
возвращает первое время переключение
после p_tm_start
*/
create or replace function ttb.tt_driver_pkg$get_main_driver(in p_tt_tr_id numeric,
                                                             in p_dr_shift_id numeric)
  returns numeric
as
$body$
declare
  l_res numeric;
begin

  select dr.tt_driver_id into l_res
  from ttb.tt_driver_tr drtr
  join ttb.tt_driver dr on dr.tt_driver_id = drtr.tt_driver_id
  where drtr.tt_tr_id = p_tt_tr_id
    and dr_shift_id = p_dr_shift_id;

  return l_res;
end;

$body$
language plpgsql stable
cost 100;



--периоды работы подменных водителей
create or replace function ttb.tt_driver_pkg$get_busy_driver(in p_tt_variant_id numeric,
                                                             in p_tm_b timestamp,
                                                             in p_tm_e timestamp)
  returns table(ftt_tr_id int,  ftt_driver_id int, ftm_b timestamp, ftm_e timestamp) as $$
begin
  return query
  with adata as  (
      select
        cb.*
        ,dr.is_сhanged_driver
      from tt_cube cb
        join ttb.tt_driver dr on dr.tt_driver_id = cb.tt_driver_id
      where cb.tt_variant_id = p_tt_variant_id
            and cb.tt_tr_id is not null
      --and cb.tt_tr_id = 9186
      ORDER BY time_end, time_begin, order_num desc, order_num_action
  )
    ,source_gr as (
      select sum (start_of_group) over ( order by tt_tr_id, time_end, time_begin, order_num desc, order_num_action) as  gr
        , d.*
      from (
             select
               (adata.tt_driver_id != lag(tt_driver_id,1,0) over ( partition by tt_tr_id order by  time_end, time_begin, order_num desc, order_num_action ))::int as start_of_group,
               lag(tt_driver_id,1,0) over ( partition by tt_tr_id order by  time_end, time_begin, order_num desc, order_num_action ),
               adata.*
             from adata
             --where order_num=1
             order by  time_end, time_begin, order_num desc, order_num_action
           ) d
  )
    , need as (
      select
        gr, tt_driver_id, tt_tr_id, is_сhanged_driver, tm_b as tm_s,
                                                       lead(tm_b) over ( order by tt_tr_id, tm_b) as tm_f
      from
        (select
           g.gr, g.tt_driver_id, g.is_сhanged_driver, g.tt_tr_id
           ,min(time_begin) as tm_b, max(time_end) as tm_e
         from
           source_gr g
         group by g.gr, g.tt_driver_id, g.is_сhanged_driver, g.tt_tr_id
         order by tt_tr_id, tm_b
        ) t
  )
  select tt_tr_id, tt_driver_id, tm_s, tm_f
  from need
  where is_сhanged_driver

  ;
end;

$$
language plpgsql;


--периоды, в которые подменные водители свободны от работы
create or replace function ttb.tt_driver_pkg$get_free_driver(in p_tt_variant_id numeric,
                                                             in p_tm_b timestamp,
                                                             in p_tm_e timestamp)
  returns table(tt_driver_id int, tm_b timestamp, tm_e timestamp) as $$

   with adata as
    (
    select
       dr.tt_driver_id
      ,lag(b.ftm_e, 1, dr.time_begin) over (partition by dr.tt_driver_id order by b.ftm_b)::timestamp as tm_b
      ,coalesce(b.ftm_b, dr.time_end)::timestamp  as tm_e
    from ttb.tt_driver dr
      left join ttb.tt_driver_pkg$get_busy_driver(p_tt_variant_id, null, null) b on b.ftt_driver_id = dr.tt_driver_id
    where dr.tt_variant_id = p_tt_variant_id
          and dr.is_сhanged_driver
    union
    select
      dr.tt_driver_id
      ,coalesce(max(b.ftm_e) over (partition by dr.tt_driver_id), dr.time_begin)::timestamp as tm_b
      ,dr.time_end::timestamp  as tm_e
    from ttb.tt_driver dr
      left join ttb.tt_driver_pkg$get_busy_driver(p_tt_variant_id, null, null) b on b.ftt_driver_id = dr.tt_driver_id
    where dr.tt_variant_id = p_tt_variant_id
          and dr.is_сhanged_driver
    )
    select tt_driver_id, tm_b, tm_e
    from adata
    where (p_tm_b is null and p_tm_e is null)
      or (adata.tm_b< adata.tm_e and tsrange(p_tm_b, p_tm_e, '[)') <@ tsrange(adata.tm_b, adata.tm_e, '[)'))
   ;

$$ language sql;



  --назначить водителя на имеющиеся дейсвия
  create or replace function ttb.tt_driver_pkg$update_driver(in p_tt_tr_id numeric,
                                                             in p_tt_driver_id numeric,
                                                             in p_tm_b timestamp,
                                                             in p_tm_e timestamp)
    returns numeric as $$
  declare
    l_res numeric;
  begin
    with adata as (
    update  tt_cube
    set tt_driver_id = p_tt_driver_id
    from ttb.vm_action_type vma
    where vma.action_type_id = tt_cube.action_type_id
          and tt_tr_id = p_tt_tr_id
          and
                   (time_begin > p_tm_b and time_end < p_tm_e
                or (time_begin = p_tm_b and is_finish_stop and vma.is_bn_round_break)
                or (time_begin = p_tm_b and order_num=1 and vma.is_round)
                or (time_end =  p_tm_e and is_finish_stop ))

    returning *)
    select count(1) into l_res from adata;

    return l_res;
end;
  $$ language plpgsql;



