/* Пакет функций для отчетов "Эксплуатационные показатели ..." */

/* Табличная функция возвращает средние значения фактической вместимости транспортных средств различной "Вместимости ТС"  во всех depo */

drop function ttb.operational_indicator_reports_pkg$avg_seats4tr_capacity();

create or replace function ttb.operational_indicator_reports_pkg$avg_seats4tr_capacity()
RETURNS TABLE (tr_type_id bigint,
               depo_id bigint,
               tr_capacity_id smallint,
               tr_count smallint,
               avg_seat_qty smallint) AS
$body$
declare
 begin
return query
 select tr.tr_type_id::bigint as tr_type_id,
        tr.depo_id::bigint as depo_id,
        trm.tr_capacity_id::smallint as tr_capacity_id,
        count(1)::smallint as tr_count,
        round(avg(tr.seat_qty_total))::smallint as avg_seat_qty
 from core.tr tr
      join core.tr_model trm on trm.tr_model_id = tr.tr_model_id
      join core.tr_capacity trc on trc.tr_capacity_id = trm.tr_capacity_id
 group by tr.tr_type_id,
          trm.tr_capacity_id,
          tr.depo_id;
end;
$body$
LANGUAGE 'plpgsql';


/* Функция для преобразивания реальной вместимости в "отчетную_вместимость" для трамваев */
drop function ttb.operational_indicator_reports_pkg$report_capacity(smallint);
create or replace function ttb.operational_indicator_reports_pkg$report_capacity(p_tr_type_id smallint)
RETURNS TABLE (tr_capacity_id smallint,
               short_name text,
               is_second_car boolean,
               rep_capacity_id smallint,
               rep_short_name text
) AS
$body$
declare
begin
 return query
 select c1.tr_capacity_id,
        c1.short_name,
        case
         when p_tr_type_id = core.tr_type_pkg$tm() then false
         else null::boolean
        end as is_second_car,
        case
         when p_tr_type_id = core.tr_type_pkg$tm() and c1.tr_capacity_id  = core.tr_capacity_pkg$huge() then core.tr_capacity_pkg$huge()
         when p_tr_type_id = core.tr_type_pkg$tm() and c1.tr_capacity_id != core.tr_capacity_pkg$huge() then core.tr_capacity_pkg$big()
         else c1.tr_capacity_id
        end as rep_capacity_id,
        case
         when p_tr_type_id = core.tr_type_pkg$tm() and c1.tr_capacity_id  = core.tr_capacity_pkg$huge() then c1.short_name
         when p_tr_type_id = core.tr_type_pkg$tm() and c1.tr_capacity_id != core.tr_capacity_pkg$huge() then 'Головной'
         else c1.short_name
        end as rep_short_name
 from core.tr_capacity c1
 union all
 select c2.tr_capacity_id,
        c2.short_name,
        true as is_second_car,
        -core.tr_capacity_pkg$big() as rep_capacity_id,
        'Прицеп' as rep_short_name
 from core.tr_capacity c2
 where p_tr_type_id = core.tr_type_pkg$tm() and
       c2.tr_capacity_id != core.tr_capacity_pkg$huge();
end;
$body$
LANGUAGE 'plpgsql';


/* Табличная функция, возвращает количество дней (рабочих, субботних, воскресных) в периоде */
drop function ttb.operational_indicator_reports_pkg$days_count (timestamp, timestamp);
create or replace function ttb.operational_indicator_reports_pkg$days_count(p_dt_from timestamp,
																                                            p_dt_to timestamp)
RETURNS TABLE (
 workdays smallint,
 saturdays smallint,
 sundays smallint) AS
$body$
declare
 l_dt_from timestamp;
 l_dt_to timestamp;
begin
 l_dt_from := (p_dt_from)::date;
 l_dt_to := (p_dt_to)::date;
 /* возвращается одна запись с количествами рабочих дней, суббот и воскресений в указанном периоде времени */
 return query
  select
   sum (case
         /* рабочие дни: пн.-пт. */
         when ci.calendar_tag_id between 1 and 5
         then 1
         else 0
        end)::smallint as workdays,
   sum (case ci.calendar_tag_id
         /* суббота */
         when 6 then 1
         else 0
        end)::smallint as saturdays,
   sum (case ci.calendar_tag_id
         /* воскресенье */
         when 7 then 1
         else 0
        end)::smallint as sundays
  from ttb.calendar_item ci
       join ttb.calendar_tag ct on ct.calendar_tag_id = ci.calendar_tag_id
  where ci.calendar_id between trunc(extract (epoch from l_dt_from)/(24*60*60)) and trunc(extract (epoch from l_dt_to)/(24*60*60))
        and ct.calendar_tag_group_id = ttb.calendar_pkg$get_daysofweek_tag_group_id();
end;
$body$
LANGUAGE 'plpgsql';

/* Функция, непосредственно возвращающая данные отчета (detail band) */
drop function ttb.operational_indicator_reports_pkg$report_detail(text, text, text, text, text, text, text, text, text);
create or replace function ttb.operational_indicator_reports_pkg$report_detail(P_ID_ACCOUNT text,
                                                                               p_timezone text,
                                                                               p_tr_type_id text,
                                                                               p_depo_id text,
                                                                               p_territory_id text,
                                                                               p_begin_date text,
                                                                               p_end_date text,
                                                                               p_rep_date text,
                                                                               P_REPORT_TYPE text)
RETURNS TABLE (field_name text,
               tr_capacity_name text,
               ttv_date text,
               route_length text,
               tr_release text,
               speed_avg text,
               oper_time_avg text,
               oper_time_dinner_avg text,
               m_hour text,
               m_hour_dinner text,
               m_km_prod text,
               m_km_tech text,
               m_km_summ text,
               p_km_prod text,
               p_km_tech text,
               p_km_summ text,
               round_cnt_prod text,
               round_cnt_short text,
               round_cnt_tech text,
               round_cnt_summ text,
               tr_capacity text) AS
$body$
declare
begin
 return query
select /* поля отчета с финальным округдением и преобразованием типа к text */
         case
          when src_rows.depo_id is null and src_rows.route_id is null and src_rows.tr_capacity_id is null then 'Итого'
          when src_rows.route_first_row = src_rows.tr_capacity_id and src_rows.report_type = 'depo' and src_rows.tr_type_id = 3 -- переделать на константу "трамвай"
            then  src_rows.territory_name||' '||src_rows.depo_name
           when src_rows.route_first_row = src_rows.tr_capacity_id and src_rows.report_type = 'depo' and src_rows.tr_type_id in (1, 2) -- -- переделать на константы "автобус", "троллейбус"
            then src_rows.depo_name
          when src_rows.route_first_row = src_rows.tr_capacity_id
           then src_rows.route_number
          else ''
         end::text as field_name,
         coalesce(src_rows.tr_capacity_name, 'сумма')::text,
         case
          when src_rows.depo_id is null and src_rows.route_id is null and src_rows.tr_capacity_id is null then ''
          when src_rows.tr_capacity_id is null then src_rows.ttv_date::text
          else ''
         end::text,
         case
          when src_rows.tr_capacity_id is null
           then round(src_rows.route_length, 3)::text
          else ''
         end::text,
         src_rows.tr_release::text,
         case
          when src_rows.tr_capacity_id is null
           then round(src_rows.speed_avg, 2)::text
          else ''
         end::text,
         case
          when src_rows.tr_capacity_id is null
           then round(src_rows.oper_time_avg, 2)::text
          else null
         end::text,
         case
          when src_rows.tr_capacity_id is null
           then round(src_rows.oper_time_dinner_avg, 2)::text
          else ''
         end::text,
         case
          when src_rows.tr_capacity_id is null
           then round(src_rows.m_hour, 2)::text
          else ''
         end::text,
         case
          when src_rows.tr_capacity_id is null
           then round(src_rows.m_hour_dinner, 2)::text
          else ''
         end::text,
         round(src_rows.m_km_prod, 2)::text,
         round(src_rows.m_km_tech, 2)::text,
         round(src_rows.m_km_summ, 2)::text,
         round(src_rows.p_km_prod)::text,
         round(src_rows.p_km_tech)::text,
         round(src_rows.p_km_summ)::text,
         src_rows.round_cnt_prod::text,
         src_rows.round_cnt_short::text,
         src_rows.round_cnt_tech::text,
         src_rows.round_cnt_summ::text,
         case
          when src_rows.report_type = 'date' and  src_rows.tr_capacity_id is not null
           then src_rows.tr_capacity::text
           else ''
         end::text
  from (with route_detail as (select P_REPORT_TYPE as report_type,
                                     /* добавляем названия депо и территории (необязательные) */
                                     d.name_full as depo_name,
                                     t.name_short as territory_name,
                                     /* исходные данные для финальной группировки в отчет */
                                     func_route_detail.*,
                                     /* обогащаем недостающими полями, которые будем рассчитывать на этом этапе */
                                     null::numeric as speed_avg,
                                     null::numeric as oper_time_avg,
                                     null::numeric as oper_time_dinner_avg
                              from ttb.operational_indicator_reports_pkg$route_detail(P_ID_ACCOUNT, p_timezone, p_tr_type_id, p_depo_id, p_territory_id, p_begin_date, p_end_date, p_rep_date, P_REPORT_TYPE) func_route_detail
                                   left join core.entity d on d.entity_id = func_route_detail.depo_id
                                   left join core.entity t on t.entity_id  = func_route_detail.territory_id)
        select /* детадьная информация по маршруту + итоговая строка */
              rd1.tr_type_id,
              rd1.depo_id,
              rd1.depo_name,
              rd1.territory_id,
              rd1.territory_name,
              rd1.route_id,
              rd1.route_number,
              rd1.tr_capacity_id,
              rd1.tr_capacity_name,
              to_char(min(rd1.ttv_date), 'DD.MM.YYYY') as ttv_date,
              avg(rd1.route_length) as route_length,
              sum(rd1.tr_release) as tr_release,
              sum(rd1.m_km_summ)/case when sum(rd1.m_hour)=0 then null else sum(rd1.m_hour) end  as speed_avg,
              sum(rd1.m_hour)/case when sum(rd1.tr_release)=0 then null else sum(rd1.tr_release) end as oper_time_avg,
              sum(rd1.m_hour_dinner) / case when sum(rd1.tr_release)=0 then null else sum(rd1.tr_release) end as oper_time_dinner_avg,
              sum(rd1.m_hour) as m_hour,
              sum(rd1.m_hour_dinner) as m_hour_dinner,
              sum(rd1.m_km_prod) as m_km_prod,
              sum(rd1.m_km_tech) as m_km_tech,
              sum(rd1.m_km_summ) as m_km_summ,
              sum(rd1.p_km_prod) as p_km_prod,
              sum(rd1.p_km_tech) as p_km_tech,
              sum(rd1.p_km_summ) as p_km_summ,
              sum(rd1.round_cnt_prod) as round_cnt_prod,
              sum(rd1.round_cnt_short) as round_cnt_short,
              sum(rd1.round_cnt_tech) as round_cnt_tech,
              sum(rd1.round_cnt_summ) as round_cnt_summ,
              round(avg(rd1.tr_capacity)) as tr_capacity,
              rd1.report_type,
              min(rd1.tr_capacity_id) over (partition by rd1.depo_id, rd1.territory_id, rd1.route_id) as route_first_row
        from route_detail rd1
        group by rd1.tr_type_id,
                 rd1.report_type,
                 grouping sets (/* данные для отчета по маршрутам - детализация по вместимости ТС + итоговая строка по мершруту */
                                (rd1.depo_id, rd1.depo_name, rd1.territory_id, rd1.territory_name, rd1.route_id, rd1.route_number, rd1.tr_capacity_id, rd1.tr_capacity_name, rd1.tr_capacity),
                                (rd1.depo_id, rd1.depo_name, rd1.territory_id, rd1.territory_name, rd1.route_id, rd1.route_number),
                                /* детальные данные по депо - детализация по депо и территории + итоговая строка по депо и территории */
                                (rd1.depo_id, rd1.depo_name, rd1.territory_id, rd1.territory_name, rd1.tr_capacity_id, rd1.tr_capacity_name, rd1.tr_capacity),
                                (rd1.depo_id, rd1.depo_name, rd1.territory_id, rd1.territory_name),
                                /* итоговые строки для любого отчета - в разрезе вместимости ТС + итоговая строка "Итого" */
                                (rd1.tr_capacity_id, rd1.tr_capacity_name), ())
        having (report_type in ('date', 'period') and route_id is not null) or         /* отчет по маршруту */
               (report_type = 'depo' and depo_id is not null and route_id is null) or  /* отчет по депо */
               (depo_id is null and territory_id is null and route_id is null)         /* итоги отчета */
       ) src_rows
order by src_rows.tr_type_id nulls last,
         src_rows.depo_id nulls last,
         src_rows.territory_id nulls last,
         src_rows.route_id nulls last,
         src_rows.tr_capacity_id nulls last;
end;
$body$
LANGUAGE 'plpgsql';

/* Детальные данные для отчетов в разрезе маршрутов и "вместимость ТС" */
drop function ttb.operational_indicator_reports_pkg$route_detail(text, text, text, text, text, text, text, text, text);
create or replace function ttb.operational_indicator_reports_pkg$route_detail(P_ID_ACCOUNT text,
                                                                              p_timezone text,
                                                                              p_tr_type_id text,
                                                                              p_depo_id text,
                                                                              p_territory_id text,
                                                                              p_begin_date text,
                                                                              p_end_date text,
                                                                              p_rep_date text,
                                                                              P_REPORT_TYPE text)
RETURNS TABLE (tr_type_id smallint,
               depo_id bigint,
               territory_id bigint,
               route_id bigint,
               route_number text,
               tr_capacity_id smallint,
               tr_capacity_name text,
               ttv_date date,
               route_length numeric,
               tr_release smallint,
               m_hour numeric,
               m_hour_dinner numeric,
               m_km_prod numeric,
               m_km_tech numeric,
               m_km_summ numeric,
               p_km_prod numeric,
               p_km_tech numeric,
               p_km_summ numeric,
               round_cnt_prod smallint,
               round_cnt_short smallint,
               round_cnt_tech smallint,
               round_cnt_summ smallint,
               tr_capacity smallint) AS
$body$
declare
  l_begin_date timestamp;
  l_end_date timestamp;
  l_rep_range tsrange;
  l_tr_type_id smallint := null;
  l_depo_id core.depo.depo_id%type := null;
  l_territory_id core.territory.territory_id%type := null;
  l_arr_round smallint[];
  l_arr_round_prod smallint[];
  l_arr_round_tech smallint[];
  l_arr_round_short smallint[];
  l_arr_break smallint[];
  l_arr_interround_break smallint[];
  l_arr_round_main smallint[];
begin
 -- инициалилизируем локальные переменные
 l_begin_date := p_begin_date::timestamp + make_interval (0,0,0,0, p_timezone::int,0,0);
 l_end_date := p_end_date::timestamp + make_interval (0,0,0,0, p_timezone::int,0,0);
 select tsrange(l_begin_date, l_end_date, '[]') into l_rep_range; -- период отчета

 l_tr_type_id := p_tr_type_id::smallint;
 if coalesce(p_depo_id, '') != '' then
  l_depo_id := p_depo_id::bigint;
  if coalesce(p_territory_id, '') != '' then
   l_territory_id := p_territory_id::bigint;
  end if;
 end if;

 -- собираем массивы action_type_id
 l_arr_round := ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$round(), false);
 l_arr_round_prod := ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$production_round(), false);
 l_arr_round_tech := ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$technical_round(), false);
 l_arr_round_short := ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$short_round(), false);
 l_arr_round_main := ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$main_round(), false);
 l_arr_interround_break := ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$bn_round_break(), false);

 select array((select unnest(ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$break(), false)) as rd_br
               EXCEPT
               select unnest(ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$bn_round_break(), false)) as rd_br))
  into l_arr_break;

 return query
  select
     ttv_data.tr_type_id::smallint,
     ttv_data.depo_id::bigint as depo_id,
     ttv_data.territory_id::bigint,
     ttv_data.route_id::bigint,
     ttv_data.route_number::text,
     ttv_data.tr_capacity_id::smallint,
     ttv_data.tr_capacity_name::text,
     case
      when P_REPORT_TYPE = 'date' then ttv_data.ttv_date
      else null::date
     end as ttv_date,
     case
      when P_REPORT_TYPE = 'date' then ttv_data.route_length::numeric
      else null::numeric
     end as route_length,
     sum(ttv_data.tr_release)::smallint as tr_release,
     sum(ttv_data.m_hour)::numeric as m_hour,
     sum(ttv_data.m_hour_dinner)::numeric as m_hour_dinner,
     sum(ttv_data.m_km_prod)::numeric as m_km_prod,
     sum(ttv_data.m_km_tech)::numeric as m_km_tech,
     sum(ttv_data.m_km_summ)::numeric as m_km_summ,
     sum(ttv_data.avg_seat_qty * ttv_data.m_km_prod)::numeric as p_km_prod,
     sum(ttv_data.avg_seat_qty * ttv_data.m_km_tech)::numeric as p_km_tech,
     sum(ttv_data.avg_seat_qty * ttv_data.m_km_summ)::numeric as p_km_summ,
     sum(ttv_data.round_cnt_prod)::smallint as round_cnt_prod,
     sum(ttv_data.round_cnt_short)::smallint as round_cnt_short,
     sum(ttv_data.round_cnt_tech)::smallint as round_cnt_tech,
     sum(ttv_data.round_cnt_summ)::smallint as round_cnt_summ,
     ttv_data.avg_seat_qty::smallint as tr_capacity
  from (
  with ttv_list as ( /* список вариантов расписания, по которым нужно собирать данные в отчет + количество дней этого варианта в отчете и длина основного рейса */
                    select w1_r.route_id,
                           w1_r.tr_type_id,
                           w1_r.route_num,
                           w1_rv.route_variant_id,
                           w1_d2r.depo_id,
                           w1_ttv.tt_variant_id,
                           lower(w1_ttv.action_period)::date as ttv_begin_action,
                           (select /* подсчет длины основного рейса для варианта маршрута, соответствующего варианту расписания */
                                   sum(w4_si2r.length_sector::numeric)/2 as main_round_length
                            from rts.round w4_r
                                 join rts.stop_item2round w4_si2r on w4_si2r.round_id = w4_r.round_id
                             where not w4_r.sign_deleted
                                   and w4_r.action_type_id = any(l_arr_round_main)
                                   and w4_r.route_variant_id = w1_rv.route_variant_id
                             group by w4_r.round_num) as route_length,
                           clnd.act_days as intersect_days
                    from  rts.route w1_r
                          join rts.route_variant w1_rv on w1_rv.route_id = w1_r.route_id
                          join rts.depo2route w1_d2r on w1_d2r.route_id = w1_r.route_id
                          join ttb.tt_variant w1_ttv on w1_ttv.route_variant_id = w1_rv.route_variant_id
                          /* учитываем период действия каждого варианта расписания */
                          join (select cd.tt_variant_id,
                                       count(1) as act_days
                                from ttb.v_tt_calendar_date cd
                                where l_rep_range @> cd.dt::timestamp
                                group by cd.tt_variant_id) clnd on clnd.tt_variant_id = w1_ttv.tt_variant_id
                    where     not w1_r.sign_deleted
                          and not w1_rv.sign_deleted
                          and not w1_d2r.sign_deleted
                          and not w1_ttv.sign_deleted
                          and w1_ttv.parent_tt_variant_id is null
                          and w1_r.tr_type_id = l_tr_type_id -- фильтр по "Виду ТС"
                          and w1_rv.rv_status_id = 1 /* заменить на константу !!! */
                          and w1_ttv.tt_status_id in (ttb.jf_tt_status_pkg$cn_arch_status_id(), ttb.jf_tt_status_pkg$cn_active_status_id())
                          and w1_d2r.depo_id = coalesce(l_depo_id, w1_d2r.depo_id) -- фильтр по ТП, если установлен
                          and w1_ttv.action_period && l_rep_range -- период отчета пересекается с периодом действия планового расписания
    /* 3. учесть переключения */
                    ),
       tto_list as ( /* данные по выходам для отчета - количетсво машино-часов, количество выходов по "вместимости ТС" */
                    select w2_tto.tt_variant_id,
                           w2_tto.tr_capacity_id,
                           w2_tto.territory_id,
                           count(w2_tto.tt_out_id) as out_count,
                           sum(out_rate.m_h/3600) as m_h,
                           sum(out_rate.m_h_d/3600) as m_h_d,
                           sum(out_rate.m_km_prod)/1000 as m_km_prod,
                           sum(out_rate.m_km_tech)/1000 as m_km_tech,
                           sum(out_rate.m_km_all)/1000 as m_km_all,
                           sum(out_rate.round_prod_count) as round_prod_count,
                           sum(out_rate.round_short_count) as round_short_count,
                           sum(out_rate.round_tech_count) as round_tech_count ,
                           sum(out_rate.round_all_count) as round_all_count
                    from (select w3_ttv.tt_variant_id as tt_variant_id
                          from ttb.tt_variant w3_ttv
                          where     not w3_ttv.sign_deleted
                                and w3_ttv.parent_tt_variant_id is null
                                and w3_ttv.tt_status_id in (ttb.jf_tt_status_pkg$cn_arch_status_id(), ttb.jf_tt_status_pkg$cn_active_status_id())
                                and w3_ttv.action_period && l_rep_range) w2_ttv
                         join (select out_in.tt_out_id,
                                      out_in.tt_variant_id,
                                      out_in.territory_id,
                                      rtrc.rep_capacity_id as tr_capacity_id,
                                      out_in.sign_deleted
                               from (select opr.*, /* выбираем выходы, в которых есть хотя бы один производственный рейс, и для трамвая - относятся к нужной "территории" */
                                            d2c.territory_id as territory_id,
                                            vtr.is_second_car as is_second_car
                                     from ttb.tt_out opr
                                          /* только для трамваев ищем территорию и признак прицепа */
                                          left join ttb.tt_out2tt_tr o2tr on o2tr.tt_out_id = opr.tt_out_id and l_tr_type_id = core.tr_type_pkg$tm()
                                          left join (select t1.tt_tr_id,
                                                            t1.tt_variant_id,
                                                            t1.tr_mode_id,
                                                            t1.depo2territory_id,
                                                            t1.tr_capacity_id,
                                                            t1.order_num,
                                                            t1.tt_tr_group_id,
                                                            m1.tsc as is_second_car
                                                     from ttb.tt_tr t1
                                                          /* "задваиваем" строки для трамвая с прицепом, но не ОБК */
                                                          join (select *
                                                                from unnest(array[false, true, true], array[false, false, true]) as sc (osc, tsc)
                                                                where l_tr_type_id = core.tr_type_pkg$tm() or
                                                                      sc.osc = sc.tsc) m1 on m1.osc = t1.is_second_car
                                                     where t1.tr_capacity_id != core.tr_capacity_pkg$huge() or
                                                           (t1.tr_capacity_id = core.tr_capacity_pkg$huge() and m1.osc = m1.tsc)) vtr on vtr.tt_tr_id = o2tr.tt_tr_id
                                          left join core.depo2territory d2c on d2c.depo2territory_id = vtr.depo2territory_id
                                     where l_tr_type_id in (core.tr_type_pkg$bus(), core.tr_type_pkg$tb()) or
                                           (l_tr_type_id = core.tr_type_pkg$tm() and d2c.territory_id = coalesce(l_territory_id, d2c.territory_id))
                                           /* -- условие "в выходе есть хотя бы один произведственный рейс"
                                              and exists (select 1
                                                       from ttb.tt_action apr
                                                       where apr.tt_out_id  = opr.tt_out_id and
                                                             apr.action_type_id = any (l_arr_round_prod)) */
                                    ) out_in
                                     join ttb.operational_indicator_reports_pkg$report_capacity(l_tr_type_id) rtrc on rtrc.tr_capacity_id = out_in.tr_capacity_id
                               where (rtrc.is_second_car is null and out_in.is_second_car is null) or
                                     rtrc.is_second_car = out_in.is_second_car) w2_tto on w2_tto.tt_variant_id = w2_ttv.tt_variant_id
                         join (select out_table.tt_out_id,
                                      sum(sign(out_table.prod_round+out_table.tech_round+out_table.interround_break) * out_table.action_dur) as m_h,
                                      sum(out_table.action_dur) as m_h_d,
                                      sum(out_table.prod_round * out_table.action_length) as m_km_prod,
                                      sum(out_table.tech_round * out_table.action_length) as m_km_tech,
                                      sum((out_table.prod_round + out_table.tech_round) * out_table.action_length) as m_km_all,
                                      sum(out_table.prod_round) as round_prod_count,
                                      sum(out_table.short_round) as round_short_count,
                                      sum(out_table.tech_round) as round_tech_count,
                                      sum(out_table.prod_round) + sum(out_table.tech_round) as round_all_count
                               from (select w6_tta.tt_out_id,
                                            sign(coalesce(array_position(l_arr_round_prod, w6_tta.action_type_id), 0)) as prod_round,
                                            sign(coalesce(array_position(l_arr_round_short, w6_tta.action_type_id), 0)) as short_round,
                                            sign(coalesce(array_position(l_arr_round_tech, w6_tta.action_type_id), 0)) as tech_round,
                                            sign(coalesce(array_position(l_arr_break, w6_tta.action_type_id), 0)) as break,
                                            sign(coalesce(array_position(l_arr_interround_break, w6_tta.action_type_id), 0)) as interround_break,
                                            CASE (w6_tta.action_type_id = ANY (l_arr_round_prod || l_arr_round_tech))
                                             WHEN TRUE
                                              THEN (SELECT sum(w5_si2r.length_sector)
                                                     FROM ttb.tt_action_item w5_ai
                                                          JOIN rts.stop_item2round w5_si2r ON w5_si2r.stop_item2round_id = w5_ai.stop_item2round_id
                                                     WHERE w5_ai.tt_action_id = w6_tta.tt_action_id AND
                                                           NOT w5_ai.sign_deleted AND
                                                           NOT w5_si2r.sign_deleted)
                                             ELSE 0
                                            END AS action_length,
                                            w6_tta.action_dur
                                     FROM ttb.tt_action w6_tta) out_table
                               group by out_table.tt_out_id) out_rate on out_rate.tt_out_id = w2_tto.tt_out_id
                         where not w2_tto.sign_deleted
                         group by w2_tto.tt_variant_id,
                                  w2_tto.tr_capacity_id,
                                  w2_tto.territory_id),
      tr_depo_capacity as (/* фактическая вместительность ТС в каждом depo */
                            select fr.*
                            from ttb.operational_indicator_reports_pkg$avg_seats4tr_capacity() fr
                            where     fr.tr_type_id = l_tr_type_id
                                  and fr.depo_id = coalesce(l_depo_id, fr.depo_id))
  select /* собираем данные до уровня маршрут-вместимость (без суммирования данных маршрута) за указанный период  */
     ttvl.tr_type_id as tr_type_id,
     ttvl.depo_id as depo_id,
     ttol_d.territory_id as territory_id,
     ttvl.route_id as route_id,
     ttvl.route_num as route_number,
     trc_d.rep_capacity_id as tr_capacity_id,
     trc_d.rep_short_name as tr_capacity_name,
     ttvl.ttv_begin_action as ttv_date,
     ttvl.route_length/1000 as route_length,
     (ttol_d.out_count * ttvl.intersect_days) as tr_release,
     (ttol_d.m_h * ttvl.intersect_days) as m_hour,
     (ttol_d.m_h_d * ttvl.intersect_days) as m_hour_dinner,
     (ttol_d.m_km_prod * ttvl.intersect_days) as m_km_prod,
     (ttol_d.m_km_tech * ttvl.intersect_days) as m_km_tech,
     (ttol_d.m_km_all * ttvl.intersect_days) as m_km_summ,
     (ttol_d.round_prod_count * ttvl.intersect_days) as round_cnt_prod,
     (ttol_d.round_short_count * ttvl.intersect_days) as round_cnt_short,
     (ttol_d.round_tech_count * ttvl.intersect_days) as round_cnt_tech,
     (ttol_d.round_all_count * ttvl.intersect_days) as round_cnt_summ,
     trdc.avg_seat_qty as avg_seat_qty
  from      ttv_list ttvl
       join tto_list ttol_d on ttol_d.tt_variant_id = ttvl.tt_variant_id
       join (select distinct
                   rep_capacity_id,
                   rep_short_name
             from ttb.operational_indicator_reports_pkg$report_capacity(l_tr_type_id)) trc_d on trc_d.rep_capacity_id = ttol_d.tr_capacity_id
       left join tr_depo_capacity trdc on trdc.tr_capacity_id = abs(ttol_d.tr_capacity_id)
                                          and trdc.tr_type_id = ttvl.tr_type_id
                                          and trdc.depo_id = ttvl.depo_id
  ) ttv_data
  group by
     ttv_data.tr_type_id,
     ttv_data.depo_id,
     ttv_data.territory_id,
     ttv_data.route_id,
     ttv_data.route_number,
     ttv_data.tr_capacity_id,
     ttv_data.tr_capacity_name,
     case
      when P_REPORT_TYPE = 'date' then ttv_data.ttv_date
      else null::date
     end,
     case
      when P_REPORT_TYPE = 'date' then ttv_data.route_length::numeric
      else null::numeric
     end,
     ttv_data.avg_seat_qty;
end;
$body$
LANGUAGE 'plpgsql';

---- --------------------------------------------------------------------------------
-- Функции для оконного метода и для суб-репортов
----------------------------------------------------------------------------------

/* Возвращает список для выбора депо/депо-территории */
create or replace function ttb.operational_indicator_reports_pkg$of_rows (p_id_account numeric, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
  res_cursor refcursor;
  l_report_type text;
  l_rep_dt text;
  l_rep_begin_dt text;
  l_rep_end_dt text;
  l_tr_type_id smallint;
  l_tr_type_prefix text;
 begin
  l_report_type := jofl.jofl_pkg$extract_varchar(p_attr, 'P_REPORT_TYPE', true);
  l_tr_type_id := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id', true);
  l_rep_dt := jofl.jofl_pkg$extract_varchar(p_attr, 'F_REP_DT', true);
  l_rep_begin_dt := jofl.jofl_pkg$extract_varchar(p_attr, 'F_REP_BEGIN_DT', true);
  l_rep_end_dt := jofl.jofl_pkg$extract_varchar(p_attr, 'F_REP_END_DT', true);

  case
   when l_tr_type_id = 3 then l_tr_type_prefix := 'tram';
   when l_tr_type_id in (1, 2) then l_tr_type_prefix := 'bus';
   else l_tr_type_prefix := '';
  end case;

  /* открываем список для метода */
  if l_report_type = 'depo' then
   -- для отчета по паркам выбранного "Вида ТС"
   open res_cursor for
    select tr_tp.name as p_filter_text,
           l_report_type as p_report_type,
           tr_tp.tr_type_id::text as p_tr_type_id,
           null::text as p_depo_id,
           null::text as p_territory_id,
           l_rep_dt as p_rep_dt,
           l_rep_begin_dt as p_period_start,
           l_rep_end_dt as p_period_end,
           case
            when tr_tp.tr_type_id = 3 then 'tram'
            when tr_tp.tr_type_id in (1, 2) then 'bus'
            else ''
            end  as p_tr_type_prefix
    from core.tr_type tr_tp
    where tr_tp.tr_type_id = any(array[1,2,3])
          and (l_rep_begin_dt is not null or l_rep_end_dt is not null);
   ELSE
    -- для отчетов по мершрутам (по дате или периоду) выбранного депо
    open res_cursor for
     select coalesce(t.territory_name, '') || coalesce(ed.name_full, ed.name_short) as p_filter_text,
            l_report_type as p_report_type,
            l_tr_type_id::text as p_tr_type_id,
            d.depo_id as p_depo_id,
            t.territory_id as p_territory_id,
            l_rep_dt as p_rep_dt,
            l_rep_begin_dt as p_period_start,
            l_rep_end_dt as p_period_end,
            l_tr_type_prefix as p_tr_type_prefix
     from core.depo d
          join core.entity ed on ed.entity_id = d.depo_id
          left join (select d2t.depo_id,
                            d2t.territory_id,
                            coalesce(et.name_full, et.name_short)||' ' as territory_name
                     from core.depo2territory d2t
                          join core.entity et on et.entity_id = d2t.territory_id
                          join core.depo dt on dt.depo_id = d2t.depo_id
                     where dt.tr_type_id = 3) t on d.depo_id = t.depo_id
     where d.tr_type_id = l_tr_type_id
           and d.sign_deleted = 0;
  end if;

 return res_cursor;
end;
$$;

/* Табличная функция, возвращает данные для заголовка отчета "Эксплуатационные показатели ..." */
drop function ttb.operational_indicator_reports_pkg$report_header(text, text, text, text, text, text, text, text);
create or replace function ttb.operational_indicator_reports_pkg$report_header(P_ID_ACCOUNT text, 
                                                                               p_timezone text, 
                                  											                       p_report_type text,
                                                                               p_dt_from text,
																                                               p_dt_to text,
																	                                             p_tr_type_id text,
																	                                             p_depo_id text,
																	                                             p_territory_id text)
  RETURNS TABLE (tr_type_str text, /* "автосусному"/"троллейбусному" / "трамвайному" */
                 rep_object text,  /* "маршруту" / "парку" */
                 period_str text,  /* "за период с ... по .... " / "по состоянию на дату ..." */
                 depo_str text,    /* "по парку ... " */
                 day_count_str text) AS /* рабочих дней, субботних, воскресных */

$body$
declare
 l_timezone smallint;
 l_dt_from timestamp;
 l_dt_to timestamp;
 l_tr_type_str text;
 l_rep_object text;
 l_period_str text;
 l_depo_str text;
 l_days_count text;
begin
 l_timezone := p_timezone::smallint;
 l_dt_from := (p_dt_from::timestamp + make_interval(0, 0, 0, 0, l_timezone, 0, 0))::date;
 l_dt_to := (p_dt_to::timestamp + make_interval(0, 0, 0, 0, l_timezone, 0, 0))::date;

 -- опраделеняем вид транспорта
 l_tr_type_str := '';
 case p_tr_type_id::smallint
  when 1 then l_tr_type_str := 'автобусных';
  when 2 then l_tr_type_str := 'троллейбусных';
  when 3 then l_tr_type_str := 'трамвайных';
  else l_tr_type_str := '';
 end case;
 l_rep_object := 'маршрутов';
 
 -- формируем строку с названием депо/территории
 l_depo_str := '';
 select 'по парку ' || dl.territory_name || dl.depo_name into l_depo_str
 from (select d.depo_id,
              d.tr_type_id,
              ed.name_short as depo_name,
              dt.territory_id,
              coalesce(dt.territory_name, '') as territory_name
       from core.depo d
            join core.entity ed on ed.entity_id = d.depo_id
            left join (select d2t.depo_id as depo_id,
                              t.territory_id as territory_id,
                              et.name_short||' ' as territory_name
                       from core.depo2territory d2t
                            join core.territory t on t.territory_id = d2t.territory_id
                            join core.entity et on et.entity_id = t.territory_id
                       where t.territory_id = p_territory_id::smallint) dt on dt.depo_id = d.depo_id
       where d.depo_id = p_depo_id::smallint) dl;
 
 l_period_str := '';
 l_days_count := null::text;
 -- в зависимости от отчета готовим переменные части заголовка
 case p_report_type
  when 'tram_date' then
   l_period_str := 'по состоянию на ' || to_char(l_dt_from, 'DD.MM.YYYY');
  when 'bus_date' then
   l_period_str := 'по состоянию на ' || to_char(l_dt_from, 'DD.MM.YYYY');
  when 'tram_period' then
   l_period_str := 'за период' || ' с '|| to_char(l_dt_from, 'DD.MM.YYYY') ||' по '|| to_char(l_dt_to, 'DD.MM.YYYY');
   select 'рабочих дней - '||coalesce(workdays::text, '')|| '  субботних дней - '||coalesce(saturdays::text, '')|| '  воскресных дней - '||coalesce(sundays::text, '')
    into l_days_count
    from  ttb.operational_indicator_reports_pkg$days_count(l_dt_from, l_dt_to);
  when 'bus_period' then
   l_period_str := 'за период' || ' с '|| to_char(l_dt_from, 'DD.MM.YYYY') ||' по '|| to_char(l_dt_to, 'DD.MM.YYYY');
   select 'рабочих дней - '||coalesce(workdays::text, '')|| '  субботних дней - '||coalesce(saturdays::text, '')|| '  воскресных дней - '||coalesce(sundays::text, '')
    into l_days_count
    from  ttb.operational_indicator_reports_pkg$days_count(l_dt_from, l_dt_to);
  when 'tram_depo' then
   l_rep_object := 'парков';
   l_period_str := 'за период' || ' с '|| to_char(l_dt_from, 'DD.MM.YYYY') ||' по '|| to_char(l_dt_to, 'DD.MM.YYYY');
   l_depo_str := '';
   select 'рабочих дней - '||coalesce(workdays::text, '')|| '  субботних дней - '||coalesce(saturdays::text, '')|| '  воскресных дней - '||coalesce(sundays::text, '')
    into l_days_count
    from  ttb.operational_indicator_reports_pkg$days_count(l_dt_from, l_dt_to);
  when 'bus_depo' then
   l_rep_object := 'парков';
   l_period_str := 'за период' || ' с '||to_char(l_dt_from, 'DD.MM.YYYY') ||' по '|| to_char(l_dt_to, 'DD.MM.YYYY');
   l_depo_str := '';
   select 'рабочих дней - '||coalesce(workdays::text, '')|| '  субботних дней - '||coalesce(saturdays::text, '')|| '  воскресных дней - '||coalesce(sundays::text, '')
    into l_days_count
    from  ttb.operational_indicator_reports_pkg$days_count(l_dt_from, l_dt_to);
  else
   /* сбрасываем все значения, если тип отчета указан неверно */
   l_tr_type_str := '';
   l_rep_object := '';
   l_period_str := '';
   l_depo_str := '';
   l_days_count := '';
 end case;
 
 return query
  select l_tr_type_str as tr_type_str,
         l_rep_object as rep_object,
		     l_period_str as period_str,
		     l_depo_str as depo_str,
         l_days_count as day_count_str;
end;
$body$
LANGUAGE 'plpgsql';
