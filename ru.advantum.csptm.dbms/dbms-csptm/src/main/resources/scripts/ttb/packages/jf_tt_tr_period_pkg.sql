
do $$
begin
  if not exists(select * from pg_type where typname = 'jf_tt_tr_period_type') THEN

    CREATE TYPE ttb.jf_tt_tr_period_type AS
      (r_tr ttb.tt_tr,
       r_tr_per ttb.tt_tr_period);

    ALTER TYPE ttb.jf_tt_tr_period_type
      OWNER TO adv;

  end if;
end$$;

------
CREATE OR REPLACE FUNCTION ttb.jf_tt_tr_period_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.tt_tr_period AS
$body$
declare    
  	l_r 				ttb.tt_tr_period%rowtype;
    ln_tt_variant_id 	ttb.tt_variant.tt_variant_id%type;     
begin 
  	l_r.tt_tr_period_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_tr_period_id', true); 
    l_r.tt_period_id 	:= jofl.jofl_pkg$extract_number(p_attr, 'tt_period_id', true); 
    l_r.tt_tr_id 		:= jofl.jofl_pkg$extract_number(p_attr, 'tt_tr_id', true);
    l_r.tt_tr_order_num := jofl.jofl_pkg$extract_number(p_attr, 'order_num', true);
    
    select sch.tt_variant_id
    into ln_tt_variant_id
    from ttb.tt_schedule sch
    join ttb.tt_period tp on tp.tt_schedule_id = sch.tt_schedule_id
    join ttb.tt_tr_period ttp on ttp.tt_period_id = tp.tt_period_id
    where ttp.tt_tr_period_id = l_r.tt_tr_period_id;
    
    if not ttb.jf_tt_variant_pkg$of_get_policy(ln_tt_variant_id)
      then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
    end if;      

    return l_r;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_tr_period_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  ln_tt_period_id 	ttb.tt_tr_period.tt_period_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_period_id', true);
  lt_rf_db_method 	text	:= jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true);
  ln_tt_schedule_id ttb.tt_period.tt_schedule_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'tt_schedule_id', true);
begin 
 open p_rows for
 select
   trper.tt_tr_period_id,
   trper.tt_period_id,
   trper.tt_tr_id,
   tt_tr.tt_variant_id,
   tt_tr.tr_mode_id,
   tt_tr.depo2territory_id,
   tt_tr.tr_capacity_id,
   trper.tt_tr_order_num as order_num,
   depo.name_short as depo_name_short,
   depo.name_full as depo_name_full,
   terr.name_short as terr_name_short,
   terr.name_full as terr_name_full,
   m.mode_name,
   cap.short_name as capacity_short_name,
   --m.mode_name::int + trper.tt_tr_order_num::int as out_num,
   substring(m.mode_name from 1 for 1) || lpad(trper.tt_tr_order_num::text, 2, 0::text) as out_num,
   tp.tt_schedule_id,
   'P{RO},D{out_cnt}' as "ROW$POLICY",
   case 
     when not trper.is_own
     then 16757759 /*ffb3ff*/
   end "ROW$COLOR"
 from ttb.tt_tr_period  trper
   join ttb.tt_period tp on tp.tt_period_id = trper.tt_period_id
   join ttb.tt_tr tt_tr on tt_tr.tt_tr_id = trper.tt_tr_id
   join ttb.mode m on m.mode_id = tt_tr.tr_mode_id
   join core.depo2territory d2t on d2t.depo2territory_id = tt_tr.depo2territory_id
   join core.entity depo on depo.entity_id =d2t.depo_id
   join core.entity terr on terr.entity_id = d2t.territory_id
   join core.tr_capacity cap on cap.tr_capacity_id = tt_tr.tr_capacity_id
 where (trper.tt_period_id = ln_tt_period_id and lt_rf_db_method is null)
 	or (lt_rf_db_method = 'ttb.tt_switch' and tp.tt_schedule_id = ln_tt_schedule_id)
 order by trper.tt_tr_order_num;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_tr_period_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
  l_r 				ttb.tt_tr_period%rowtype;
  l_cnt 			smallint;
  ln_tt_schedule_id	ttb.tt_schedule.tt_schedule_id%type;
begin
  l_r := ttb.jf_tt_tr_period_pkg$attr_to_rowtype(p_attr);
  ln_tt_schedule_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_schedule_id', true);
  
  select count(1) 
    into l_cnt
    from ttb.tt_schedule sch
    join ttb.tt_period tp on tp.tt_schedule_id = sch.tt_schedule_id
    join ttb.tt_tr_period trp on trp.tt_period_id = tp.tt_period_id
    where sch.tt_schedule_id = ln_tt_schedule_id
      and trp.tt_tr_order_num = l_r.tt_tr_order_num
      and trp.tt_tr_period_id != l_r.tt_tr_period_id;

  	if l_cnt > 0 then
    	raise exception '<<Данный порядковый номер уже используется в этой последовательности!>>';
  	end if;  
    
  update ttb.tt_tr_period
     set tt_tr_order_num = l_r.tt_tr_order_num
   where tt_tr_period_id = l_r.tt_tr_period_id
     and tt_tr_id = l_r.tt_tr_id;

  --return ttb.jf_tt_tr_pkg$of_update(p_id_account, row_to_json(l_r_tr)::text);
  
  return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_tr_period_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare
  l_r_return ttb.jf_tt_tr_period_type;
  l_r ttb.tt_tr_period%rowtype;
  l_r_tr ttb.tt_tr%rowtype;
begin
  l_r_return := ttb.jf_tt_tr_period_pkg$attr_to_rowtype(p_attr);
  l_r := l_r_return.r_tr_per;
  l_r_tr := l_r_return.r_tr;

  delete from  ttb.tt_tr_period where  tt_tr_period_id = l_r.tt_tr_period_id;

   return ttb.jf_tt_tr_pkg$of_delete(p_id_account, row_to_json(l_r_tr )::text);

end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_tr_period_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
  l_r_return 	ttb.jf_tt_tr_period_type;
  l_r 			ttb.tt_tr_period%rowtype;
  l_r_tr 		ttb.tt_tr%rowtype;
  ln_out_cnt	smallint := jofl.jofl_pkg$extract_number(p_attr, 'out_cnt', true);
  ln_ord_num	smallint;
begin
  l_r_return 	:= ttb.jf_tt_tr_period_pkg$attr_to_rowtype(p_attr);
  l_r 			:= l_r_return.r_tr_per;
  l_r_tr 		:= l_r_return.r_tr;
  l_r_tr.tt_tr_id := l_r.tt_tr_id;
  ln_ord_num 	:= l_r_tr.order_num;  

  	l_r.tt_tr_period_id 	:= nextval('ttb.tt_tr_period_tt_tr_period_id_seq');
  	l_r.tt_tr_id 			:= nextval('ttb.tt_tr_tt_tr_id_seq');
    l_r_tr.tt_tr_id 		:= l_r.tt_tr_id;
    l_r_tr.is_second_car 	:= false; /*****/
    
    select tt_variant_id
    into  l_r_tr.tt_variant_id
    from ttb.tt_schedule tts
    join ttb.tt_period tp on tp.tt_schedule_id = tts.tt_schedule_id
    where tp.tt_period_id = l_r.tt_period_id;
    
    if ln_ord_num is null then    
      select coalesce(max(tt_tr.order_num), 0) + 1
        into l_r_tr.order_num
        from ttb.tt_tr_period  trper
        join ttb.tt_tr tt_tr on tt_tr.tt_tr_id = trper.tt_tr_id
       where trper.tt_period_id = l_r.tt_period_id;
    else
       l_r_tr.order_num := ln_ord_num;
    end if;   

  	perform ttb.jf_tt_tr_pkg$of_insert(p_id_account, row_to_json(l_r_tr)::text);
  	insert into ttb.tt_tr_period select l_r.*;  

  return null;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_tr_period_pkg$of_get_depo2territory (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
 l_route_id rts.route.route_id%type;
 ln_cnt		smallint;
begin
 l_route_id := jofl.jofl_pkg$extract_number(p_attr, 'route_id', TRUE);
 
 select count(1) 
 into ln_cnt
 from  core.depo2territory d2t
     join core.depo d on d.depo_id = d2t.depo_id
     join core.tr_type tt on tt.tr_type_id = d.tr_type_id
     join core.entity ed on ed.entity_id = d.depo_id
     join core.territory t on t.territory_id = d2t.territory_id
     join core.entity et on et.entity_id = t.territory_id
   where  exists (select null from rts.depo2route d2r 
   							 where d2r.route_id = l_route_id 
                               and d2r.depo_id = d2t.depo_id)
  ;
 
 open p_rows for
   select
     --et.name_full as terr_name_full,
     case 
       when ln_cnt = 1 then  et.name_short 
       else null
     end terr_name_short 
   from  core.depo2territory d2t
     join core.depo d on d.depo_id = d2t.depo_id
     join core.tr_type tt on tt.tr_type_id = d.tr_type_id
     join core.entity ed on ed.entity_id = d.depo_id
     join core.territory t on t.territory_id = d2t.territory_id
     join core.entity et on et.entity_id = t.territory_id
   where  exists (select null from rts.depo2route d2r 
   							 where d2r.route_id = l_route_id 
                               and d2r.depo_id = d2t.depo_id)
  ;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------