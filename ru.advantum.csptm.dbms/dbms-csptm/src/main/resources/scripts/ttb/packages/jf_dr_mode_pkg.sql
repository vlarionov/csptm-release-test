CREATE OR REPLACE FUNCTION ttb.jf_dr_mode_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.dr_mode AS
$body$
declare 
   l_r 					ttb.dr_mode%rowtype; 

begin 
   l_r.dr_shift_id 		:= jofl.jofl_pkg$extract_number(p_attr, 'dr_shift_id', true);
   l_r.dr_mode_id 		:= jofl.jofl_pkg$extract_number(p_attr, 'dr_mode_id', true);
   l_r.action_type_id 	:= jofl.jofl_pkg$extract_number(p_attr, 'action_type_id', true);
   l_r.time_min 		:= jofl.jofl_pkg$extract_number(p_attr, 'time_min', true);
   l_r.time_max 		:= jofl.jofl_pkg$extract_number(p_attr, 'time_max', true);
   
   if l_r.time_min > l_r.time_max THEN
   	raise exception '<<Проверьте введенные данные. Максимальная продолжительность должна быть больше или равна минимальной.>>';
   end if;
  
   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_dr_mode_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_dr_shift_id ttb.dr_shift.dr_shift_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'dr_shift_id', true);
begin 
 open p_rows for 
      select
        drm.dr_mode_id,
        drm.dr_shift_id,
        drm.action_type_id,
        drm.time_min,
        drm.time_max,       
        at.action_type_name,
        at.action_type_code,
        ds.break_duration_max,
        case 
          when drm.action_type_id != ttb.action_type_pkg$dinner() then 'P{A},D{OF_DELETE}'
          else 'P{RO},D{action_type_name}'
        end "ROW$POLICY"
      from ttb.dr_mode drm 
      join ttb.action_type at on drm.action_type_id = at.action_type_id
      join ttb.dr_shift ds on ds.dr_shift_id = drm.dr_shift_id
      where drm.dr_shift_id = l_dr_shift_id;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_dr_mode_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.dr_mode%rowtype;
   ln_dr_mode_break_max		integer;
   ln_break_duration_max 	ttb.dr_shift.break_duration_max%type;
begin 
   l_r := ttb.jf_dr_mode_pkg$attr_to_rowtype(p_attr);
   ln_break_duration_max := jofl.jofl_pkg$extract_number(p_attr, 'break_duration_max', true);
   
   select max(dm.time_max)
   into ln_dr_mode_break_max
   from ttb.dr_mode dm
   where dm.dr_shift_id = l_r.dr_shift_id
   and dm.dr_mode_id <> l_r.dr_mode_id
   ;
   
   ln_dr_mode_break_max := greatest(coalesce(ln_dr_mode_break_max), l_r.time_max);
   
   if ln_dr_mode_break_max > ln_break_duration_max then
   	raise exception '<<Проверьте введенные данные. Значения "Max общая продолжительность перерывов" должно быть не меньше значения "Max продолжительность перерыва на обед" и не меньше значения "Max продолжительность перерыва на отстой". >>';
   end if;   

   update ttb.dr_mode set 
          time_min = l_r.time_min,
          time_max = l_r.time_max
   where
          dr_mode_id = l_r.dr_mode_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_dr_mode_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.dr_mode%rowtype;
begin 
   l_r := ttb.jf_dr_mode_pkg$attr_to_rowtype(p_attr);
   
   delete from  ttb.dr_mode   where  dr_mode_id = l_r.dr_mode_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_dr_mode_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r 						ttb.dr_mode%rowtype;
   ln_dr_mode_break_max		integer;
   ln_break_duration_max 	ttb.dr_shift.break_duration_max%type;
begin 
   l_r := ttb.jf_dr_mode_pkg$attr_to_rowtype(p_attr);
   l_r.dr_mode_id := nextval('ttb.dr_mode_dr_mode_id_seq');
   ln_break_duration_max := jofl.jofl_pkg$extract_number(p_attr, 'break_duration_max', true);
   
   select max(dm.time_max)
   into ln_dr_mode_break_max
   from ttb.dr_mode dm
   where dm.dr_shift_id = l_r.dr_shift_id
   ;
   
   ln_dr_mode_break_max := greatest(coalesce(ln_dr_mode_break_max, 0), l_r.time_max);
   
   if ln_dr_mode_break_max > ln_break_duration_max then
   	raise exception '<<Проверьте введенные данные. Значения "Max общая продолжительность перерывов" должно быть не меньше значения "Max продолжительность перерыва на обед" и не меньше значения "Max продолжительность перерыва на отстой". >>';
   end if;
   
   insert into ttb.dr_mode select l_r.*;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;