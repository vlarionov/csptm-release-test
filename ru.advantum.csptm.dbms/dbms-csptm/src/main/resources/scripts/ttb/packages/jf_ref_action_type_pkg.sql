CREATE OR REPLACE FUNCTION ttb.jf_ref_action_type_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.ref_action_type AS
$body$
declare 
   l_r ttb.ref_action_type%rowtype; 
begin 
   l_r.ref_action_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'ref_action_type_id', true); 
   l_r.ref_action_group_id := jofl.jofl_pkg$extract_varchar(p_attr, 'ref_action_group_id', true); 
   l_r.action_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'action_type_id', true); 
   l_r.order_num := jofl.jofl_pkg$extract_varchar(p_attr, 'order_num', true); 
   /*l_r.sys_period := jofl.jofl_pkg$extract_varchar(p_attr, 'sys_period', true); 
   l_r.sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'sign_deleted', true); */

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ref_action_type_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.ref_action_type%rowtype;
begin 
   l_r := ttb.jf_ref_action_type_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.ref_action_type where  ref_action_type_id = l_r.ref_action_type_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ref_action_type_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.ref_action_type%rowtype;
begin 
   l_r := ttb.jf_ref_action_type_pkg$attr_to_rowtype(p_attr);
   
   l_r.ref_action_type_id := nextval('ttb.ref_action_type_ref_action_type_id_seq');
   
   insert into ttb.ref_action_type ( ref_action_type_id
                                    ,ref_action_group_id
                                    ,action_type_id
                                    ,order_num)

   							select l_r.ref_action_type_id
                             	  ,l_r.ref_action_group_id
                            	  ,l_r.action_type_id
                                  ,l_r.order_num
                            ;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ref_action_type_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 ln_ref_action_group_id ttb.ref_action_type.ref_action_group_id%type := jofl.jofl_pkg$extract_number(p_attr, 'ref_action_group_id', true);
begin 
 open p_rows for 
      select 
        rat.ref_action_type_id, 
        rat.ref_action_group_id, 
        rat.action_type_id, 
        rat.order_num, 
        rat.sys_period, 
        rat.sign_deleted,
        at.action_type_name,
        at.action_type_code
      from ttb.ref_action_type rat
      join ttb.action_type at on at.action_type_id = rat.action_type_id
      and rat.ref_action_group_id = ln_ref_action_group_id
	  order by rat.order_num; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ref_action_type_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.ref_action_type%rowtype;
begin 
   l_r := ttb.jf_ref_action_type_pkg$attr_to_rowtype(p_attr);

   update ttb.ref_action_type set 
         /* ref_action_group_id = l_r.ref_action_group_id,*/ 
          action_type_id = l_r.action_type_id, 
          order_num = l_r.order_num 
         /* sys_period = l_r.sys_period, 
          sign_deleted = l_r.sign_deleted*/
   where 
          ref_action_type_id = l_r.ref_action_type_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ref_action_type_pkg$of_default_num (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 ln_ref_action_group_id ttb.ref_action_type.ref_action_group_id%type := jofl.jofl_pkg$extract_number(p_attr, 'ref_action_group_id', true);
begin 
 open p_rows for 
      select  
        coalesce(max(rat.order_num), 0) + 1 as  order_num
       from ttb.ref_action_type rat
      where rat.ref_action_group_id = ln_ref_action_group_id; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;