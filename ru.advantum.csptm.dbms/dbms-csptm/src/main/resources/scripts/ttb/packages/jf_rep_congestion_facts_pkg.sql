CREATE OR REPLACE FUNCTION ttb.jf_rep_congestion_facts_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare

begin

 open p_rows for 
     select
 null as depo_name, /* Наименование ТП */
 null as tr_type_name, /* Вид ТС */
 null as route_num, /* Номер маршрута */
 null as stop_name, /* Наименование ОП */
 null as stop_fact_time /* Фактическое время прохождения ОП */	 
;
end;
$function$
;
