CREATE OR REPLACE FUNCTION ttb.jf_f_tt_out_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
begin 

 open p_rows for
 select  distinct
        tto.tt_out_num as tt_out_num,
        array_to_string(array(select a_tto.tt_out_id
             from ttb.tt_out a_tto
                  join ttb.tt_variant a_ttv on a_ttv.tt_variant_id = a_tto.tt_variant_id
                  join rts.route_variant a_rv on a_rv.route_variant_id = a_ttv.route_variant_id
                  join rts.route a_r on a_r.route_id = a_rv.route_id
             where a_tto.tt_out_num = tto.tt_out_num and
                   not a_tto.sign_deleted and
                   not a_ttv.sign_deleted and
                   a_ttv.parent_tt_variant_id is null and
                   a_ttv.tt_status_id in (5,6) and
                   not a_rv.sign_deleted and
                   not a_r.sign_deleted
                   -- здесь будут условия по маршруту (по списку машррутов)
              ), ',') as tt_out_id_list
from ttb.tt_out tto
     join ttb.tt_variant ttv on ttv.tt_variant_id = tto.tt_variant_id
     join rts.route_variant rv on rv.route_variant_id = ttv.route_variant_id
     join rts.route r on r.route_id = rv.route_id
where not tto.sign_deleted and
      not ttv.sign_deleted and
      ttv.parent_tt_variant_id is null and
      ttv.tt_status_id in (5,6) and
      not rv.sign_deleted and
      not r.sign_deleted
      -- здесь будут условия по маршруту (по списку машррутов)
  ;
  
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;