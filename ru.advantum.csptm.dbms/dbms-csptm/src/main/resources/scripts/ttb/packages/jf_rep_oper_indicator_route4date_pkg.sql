create or replace function ttb.jf_rep_oper_indicator_route4date_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare 
 l_attr text;
 l_rep_dt text :=  jofl.jofl_pkg$extract_varchar(p_attr, 'F_REP_DT', true);
begin
 /* добавляем параметры: тип отчета, дата начала периода и дата окончания периода (равные дате отчета) */
 l_attr := p_attr;
 select l_attr::jsonb || '{"P_REPORT_TYPE": "date"}'::jsonb into l_attr;
 select l_attr::jsonb || ('{"F_REP_BEGIN_DT": "' || l_rep_dt || '"}')::jsonb into l_attr;
 select l_attr::jsonb ||   ('{"F_REP_END_DT": "' || l_rep_dt || '"}')::jsonb into l_attr;
 select ttb.operational_indicator_reports_pkg$of_rows(p_id_account, l_attr) into p_rows;
end;
$$;
