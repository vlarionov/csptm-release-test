CREATE OR REPLACE FUNCTION ttb.jf_detail_fact_time_stop_place_rpt_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  F_list_tr_type_id SMALLINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_tr_type_id', true);
  f_list_depo_id SMALLINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', true);
  f_route_id SMALLINT :=  jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', TRUE);

  /* список out_id */
  f_tt_out_id_list smallint[] :=  jofl.jofl_pkg$extract_narray(p_attr, 'f_list_tt_out_id_list', TRUE);

  f_list_dr_shift_id SMALLINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_dr_shift_id', true);
  f_list_stop_item2round_id INTEGER[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_stop_item2round_id', true);
  f_list_action_type_id SMALLINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_action_type_id', true);
BEGIN
/*
    OPEN p_rows FOR
    SELECT
      NULL interval_fact,
      NULL depo_name,
      NULL tr_type_name,
      NULL route_num,
      NULL out_num,
      NULL dr_shift_num,
      NULL garage_num,
      NULL tab_num,
      NULL stop_name,
      NULL stop_b_plan,
      NULL stop_b_oper,
      NULL stop_b_fact,
        null fact_oper_delta,
        null action_type,
        null move_dir,
        null oper_plan_delta,
        null fact_oper_delta,
        null interval_plan,
        null interval_delta;
    --WHERE 1 = 2;
    RETURN;
  */

 open p_rows for
      select distinct
        null interval_fact,
        null depo_name,
        trt.short_name tr_type_name,
        rt.route_num route_num,
        out.tt_out_num out_num,
        ds.dr_shift_num dr_shift_num,
        null garage_num,
        null tab_num,
        s.name stop_name,
        tai.time_begin stop_b_plan,
        null stop_b_oper,
        null stop_b_fact,
        null fact_oper_delta,
        at.action_type_name action_type,
        md.move_direction_name move_dir,
        null oper_plan_delta,
        null fact_oper_delta,
        null interval_plan,
        null interval_delta
           from ttb.tt_out out
             join ttb.tt_action ta on ta.tt_out_id = out.tt_out_id
             join ttb.action_type at on at.action_type_id = ta.action_type_id --and at.parent_type_id in (1, 2)
             join ttb.tt_action_item tai on tai.tt_action_id = ta.tt_action_id
             join rts.stop_item2round si2r on si2r.stop_item2round_id = tai.stop_item2round_id
             join rts.stop_item si on si.stop_item_id = si2r.stop_item_id
             join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
             join rts.stop s on s.stop_id = sl.stop_id
             join rts.stop_item2round_type si2rt on si2rt.stop_item2round_id = si2r.stop_item2round_id
             join rts.stop_item_type sit on sit.stop_item_id = si.stop_item_id
                                            and sit.stop_item_type_id = si2rt.stop_item_type_id
             join ttb.tt_action_group ag on ag.tt_action_group_id = ta.tt_action_group_id
             join ttb.tt_action_round tar on tar.tt_action_id = ta.tt_action_id
             join rts.round r on r.round_id = tar.round_id and si2r.round_id = r.round_id and r.action_type_id = at.action_type_id
             join rts.move_direction md on md.move_direction_id = r.move_direction_id
             join rts.route_variant rv on r.route_variant_id = rv.route_variant_id
             join rts.route rt on rv.route_variant_id = rt.current_route_variant_id
             join core.tr_type trt on rt.tr_type_id = trt.tr_type_id
             join ttb.dr_shift ds on tai.dr_shift_id = ds.dr_shift_id
where     (array_length(F_list_tr_type_id, 1) is null or trt.tr_type_id = any(F_list_tr_type_id))
      and rt.route_id=f_route_id
and ttb.action_type_pkg$is_production_round(r.action_type_id)
    and (array_length(f_list_stop_item2round_id, 1) is null or si2r.stop_item2round_id =any(f_list_stop_item2round_id))
    and ( ttb.action_type_pkg$is_parent_type(p_type_id => ta.action_type_id
                                          ,  p_parent_type_id => f_list_action_type_id)
        or array_length(f_list_action_type_id, 1) is null)
    and (array_length(f_tt_out_id_list, 1) is null or out.tt_out_num in (select distinct tto.tt_out_num
                                                                         from ttb.tt_out tto
                                                                         where tto.tt_out_id = any(f_tt_out_id_list)))
    and (array_length(f_list_dr_shift_id, 1) is null or tai.dr_shift_id = any(f_list_dr_shift_id))
           ORDER BY tai.time_begin;

END;
$$
