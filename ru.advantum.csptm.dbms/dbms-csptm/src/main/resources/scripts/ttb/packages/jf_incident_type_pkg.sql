CREATE OR REPLACE FUNCTION ttb.jf_incident_type_pkg$attr_to_rowtype (p_attr text) RETURNS ttb.incident_type
	LANGUAGE plpgsql
AS $$

declare
   l_r ttb.incident_type%rowtype;
begin
   l_r.incident_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'incident_type_id', true);
   l_r.incident_type_name := jofl.jofl_pkg$extract_varchar(p_attr, 'incident_type_name', true);
   l_r.incident_type_short_name := jofl.jofl_pkg$extract_varchar(p_attr, 'incident_type_short_name', true);

   return l_r;
end;

$$;
CREATE OR REPLACE FUNCTION ttb.jf_incident_type_pkg$of_delete (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r ttb.incident_type%rowtype;
begin
   l_r := ttb.jf_incident_type_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.incident_type where  incident_type_id = l_r.incident_type_id;

   return null;
end;

$$;
CREATE OR REPLACE FUNCTION ttb.jf_incident_type_pkg$of_insert (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r ttb.incident_type%rowtype;
begin
   l_r := ttb.jf_incident_type_pkg$attr_to_rowtype(p_attr);

   insert into ttb.incident_type select l_r.*;

   return null;
end;

$$;
CREATE OR REPLACE FUNCTION ttb.jf_incident_type_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$

declare
begin
 open p_rows for
      select
        incident_type_id,
        incident_type_name,
        incident_type_short_name
      from ttb.incident_type;
end;

$$;
CREATE OR REPLACE FUNCTION ttb.jf_incident_type_pkg$of_update (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r ttb.incident_type%rowtype;
begin
   l_r := ttb.jf_incident_type_pkg$attr_to_rowtype(p_attr);

   update ttb.incident_type set
          incident_type_name = l_r.incident_type_name,
          incident_type_short_name = l_r.incident_type_short_name
   where
          incident_type_id = l_r.incident_type_id;

   return null;
end;

$$;