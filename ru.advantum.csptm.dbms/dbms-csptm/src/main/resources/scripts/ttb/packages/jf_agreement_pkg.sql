create or replace function ttb.jf_agreement_pkg$of_rows(p_id_account numeric, out p_rows refcursor, p_attr text)
  returns refcursor
language plpgsql
as $$
declare
  l_f_is_show_all boolean := coalesce(jofl.jofl_pkg$extract_boolean(p_attr, 'f_INPLACE_K', true), 0);
begin
  open p_rows for
  select
    tv.tt_variant_id,
    tv.ttv_name                                                                                         tt_variant_num,
    ttb.jf_agreement_pkg$get_tt_variant_link(tv.tt_variant_id, tv.ttv_name)                             tt_variant_link,
    r.route_num,
    tt.name                                                                                             tr_type,
    lower(rv.action_period)                                                                             tt_variant_begin_date,
    upper(rv.action_period)                                                                             tt_variant_end_date,
    ttb.agreement_pkg$get_calendar(tv.tt_variant_id)                                                    calendar,
    tv.ttv_comment,
    ttb.agreement_pkg$get_agreement_end_date(ttb.agreement_pkg$get_current_agreement(tv.tt_variant_id)) agreement_end_date,
    ttb.jf_agreement_pkg$get_row_policy(p_id_account, tv.tt_variant_id)                                 "ROW$POLICY"
  from
    ttb.tt_variant tv
    join rts.route_variant rv on tv.route_variant_id = rv.route_variant_id
    join rts.route r on rv.route_id = r.route_id
    join core.tr_type tt on r.tr_type_id = tt.tr_type_id
  where tv.tt_status_id = ttb.tt_status_pkg$on_agreement() and
        tv.tt_variant_id in (
          select *
          from ttb.jf_agreement_pkg$get_available_tt_variants(p_id_account, not l_f_is_show_all)
        )
		and tv.parent_tt_variant_id is null;
end;
$$;

create or replace function ttb.jf_agreement_pkg$get_tt_variant_link(
  p_tt_variant_id   ttb.tt_variant.tt_variant_id%type,
  p_tt_variant_name ttb.tt_variant.ttv_name%type
)
  returns text
language plpgsql
as $$
begin
  return '[' ||
         json_build_object(
             'CAPTION', p_tt_variant_name,
             'JUMP', json_build_object(
                 'METHOD', 'ttb.tt_variant_view_only',
                 'ATTR', json_build_object('f_tt_variant_id', p_tt_variant_id) :: text
             )
         )
         || ']';
end;
$$;

create or replace function ttb.jf_agreement_pkg$get_row_policy(
  p_id_account    numeric,
  p_tt_variant_id ttb.tt_variant.tt_variant_id%type
)
  returns text
language plpgsql
as $$
begin
  if p_tt_variant_id in (
    select *
    from ttb.jf_agreement_pkg$get_available_tt_variants(p_id_account, false)
  )
  then
    return 'P{A},D{OF_AGREE, OF_DISAGREE}';
  end if;
  return null;
end;
$$;

create or replace function ttb.jf_agreement_pkg$of_agree(p_id_account numeric, p_attr text)
  returns text
as $$
declare
  l_tt_variant_id ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', false);
begin
  perform ttb.agreement_pkg$agree(p_id_account :: bigint, l_tt_variant_id);

  return null;
end;
$$ language plpgsql;

create or replace function ttb.jf_agreement_pkg$of_disagree(p_id_account numeric, p_attr text)
  returns text
as $$
declare
  l_tt_variant_id ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', false);
  l_comment       ttb.tt_variant_agreement.comment%type := jofl.jofl_pkg$extract_varchar(p_attr, 'comment_TEXT', true);
begin
  perform ttb.agreement_pkg$disagree(p_id_account :: bigint, l_tt_variant_id, l_comment);

  return null;
end;
$$ language plpgsql;

create or replace function ttb.jf_agreement_pkg$get_available_tt_variants(
  p_account_id         numeric,
  p_is_strict_by_group boolean
)
  returns setof integer
language plpgsql
as $$
begin
  return query
  select distinct tva.tt_variant_id
  from adm.account2group a2g
    join ttb.agreement_group strict_available_ag on a2g.group_id = strict_available_ag.group_id
    join ttb.agreement_group available_ag on strict_available_ag.tr_type_id = available_ag.tr_type_id
                                             and (strict_available_ag.order_num = available_ag.order_num
                                                  or (not p_is_strict_by_group and strict_available_ag.order_num > available_ag.order_num))
    join ttb.tt_variant_agreement tva on available_ag.agreement_group_id = tva.agreement_group_id
  where a2g.account_id = p_account_id and
        tva.resolution_id is null and
        ttb.agreement_pkg$get_current_agreement(tva.tt_variant_id) = tva.tt_variant_agreement_id;
end;
$$;