create or replace function ttb.jf_agreement_chain_pkg$attr_to_rowtype(p_attr text)
  returns ttb.agreement_group
language plpgsql
as $$
declare
  l_r ttb.agreement_group%rowtype;
begin
  l_r.agreement_group_id := jofl.jofl_pkg$extract_number(p_attr, 'agreement_group_id', true);
  l_r.group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', true);
  l_r.tr_type_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_type_id', true);
  l_r.order_num := jofl.jofl_pkg$extract_number(p_attr, 'order_num', true);
  l_r.agreement_period := '1 days' :: interval * jofl.jofl_pkg$extract_number(p_attr, 'agreement_period', true);

  if exists(
      select 1
      from ttb.agreement_group ag
      where ag.tr_type_id = l_r.tr_type_id
            and ag.order_num = l_r.order_num
            and ag.sys_period @> now()
  )
  then raise exception 'Запись с данным номером этапа уже существует';
  end if;

  if l_r.order_num <= 1
  then raise exception 'Некорректный № этапа';
  end if;

  if l_r.agreement_period < '1 days'
  then raise exception 'Некорректный срок согласования';
  end if;

  return l_r;
end;
$$;


create or replace function ttb.jf_agreement_chain_pkg$of_rows(p_id_account numeric, p_attr text, out p_rows refcursor)
  returns refcursor
as $$
declare
  l_tr_type_id ttb.agreement_group.tr_type_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'tr_type_id', true);
begin
  open p_rows for
  select
    taa.agreement_group_id,
    taa.tr_type_id,
    taa.order_num                                          as order_num,
    ag.group_id,
    ag.name                                                as name,
    EXTRACT(epoch from taa.agreement_period) / (24 * 3600) as agreement_period,
    pol."ROW$POLICY"
  from ttb.agreement_group taa
    left join (select 'P{A},D{OF_UPDATE, OF_DELETE}' as "ROW$POLICY") pol on taa.order_num <> 1
    join adm.group ag on taa.group_id = ag.group_id
  where taa.tr_type_id = l_tr_type_id
        and taa.sys_period @> now()
  order by order_num;
end;
$$ language plpgsql;


create or replace function ttb.jf_agreement_chain_pkg$of_insert(p_id_account numeric, p_attr text)
  returns text
as $$
declare
  l_r ttb.agreement_group%rowtype;
begin
  l_r := ttb.jf_agreement_chain_pkg$attr_to_rowtype(p_attr);
  insert into ttb.agreement_group (tr_type_id, order_num, group_id, agreement_period) values
    (l_r.tr_type_id,
     l_r.order_num,
     l_r.group_id,
     l_r.agreement_period
    );
  return null;
end;
$$ language plpgsql;


create or replace function ttb.jf_agreement_chain_pkg$of_update(p_id_account numeric, p_attr text)
  returns text
as $$
begin
  perform ttb.jf_agreement_chain_pkg$of_delete(p_id_account, p_attr);
  return ttb.jf_agreement_chain_pkg$of_insert(p_id_account, p_attr);
end;
$$ language plpgsql;

-------------------

create or replace function ttb.jf_agreement_chain_pkg$of_delete(p_id_account numeric, p_attr text)
  returns text
language plpgsql
as $$
declare
  l_agreement_group_id ttb.agreement_group.agreement_group_id%type := jofl.jofl_pkg$extract_number(p_attr, 'agreement_group_id', false);
begin
  update ttb.agreement_group
  set sys_period = tstzrange(lower(sys_period), now())
  where agreement_group_id = l_agreement_group_id;
  return null;
end;
$$;