﻿--возвращает среднюю продолжительность рейса по остановкам
create or replace function ttb.norm_pkg$get_avg_rnd(in p_norm_id numeric
                                                  , in p_round_id numeric
                                                  , in p_tm_b time default null
                                                  , in p_tm_e time default null
                                                  , in p_tr_capacity_id numeric default null)  returns numeric
language plpgsql immutable
as $function$
declare
  l_res integer;
begin
  if p_tm_b = '00:00:00'::time then
     p_tm_b := '24:00:00'::time;
  end if;
  if p_tm_e = '00:00:00'::time then
    p_tm_e := '24:00:00'::time;
  end if;
  with r as (
      select
      nbs.tr_capacity_id, nbs.tr_type_id, nbs.norm_id, sum(between_stop_dur) as rnd_dur, nbs.hour_from
      from
       rts.stop_item2round sir_b
      join rts.stop_item2round sir_e on sir_e.order_num = sir_b.order_num+1 and  sir_e.round_id = sir_b.round_id
       join ttb.norm_between_stop nbs on nbs.stop_item_1_id = sir_b.stop_item_id and nbs.stop_item_2_id =sir_e.stop_item_id
      where sir_b.round_id = p_round_id
        and norm_id = p_norm_id
        and (p_tm_b is  null or  nbs.hour_from>= p_tm_b)
        and (p_tm_e is  null or  nbs.hour_to<= p_tm_e)
        and (p_tr_capacity_id is null or nbs.tr_capacity_id = p_tr_capacity_id)
      group by nbs.tr_capacity_id,nbs.tr_type_id, nbs.norm_id, nbs.hour_from
      )
      select  coalesce(ceil(avg(rnd_dur)/60::real)*60 , 0)
      into l_res
      from r
      group by tr_type_id,norm_id;

  return l_res;

end;
$function$
;


--возвращает среднюю общуюю продолжительность межрейсовой стоянки
create or replace function ttb.norm_pkg$get_avg_bn_break(in p_norm_id numeric
                                                       , in p_round_id numeric
                                                       , in p_tm_b time default null
                                                       , in p_tm_e time default null
                                                       , in p_tr_capacity_id numeric default null)  returns numeric
language plpgsql immutable
as $function$
declare
  l_res integer;
begin
  if p_tm_b = '00:00:00'::time then
    p_tm_b := '24:00:00'::time;
  end if;
  if p_tm_e = '00:00:00'::time then
    p_tm_e := '24:00:00'::time;
  end if;
  select coalesce(ceil(avg(nr.min_inter_round_stop_dur)/60::real)*60 , 0) +
         coalesce(ceil(avg(nr.reg_inter_round_stop_dur)/60::real)*60 , 0)
  into l_res
  from ttb.norm_round  nr
  where nr.round_id = p_round_id
        and  nr.norm_id = p_norm_id
        and (p_tm_b is  null or  nr.hour_from>= p_tm_b)
        and (p_tm_e is  null or  nr.hour_to<= p_tm_e)
        and (p_tr_capacity_id is null or nr.tr_capacity_id = p_tr_capacity_id)
  group by nr.tr_type_id, nr.norm_id;

  return l_res;

end;
$function$
;


/*
Получить среднее значение времени выполнения действия по нормативу
in p_tt_shedule_id numeric -последовательность
, in p_ref_group_kind_id numeric группа действий
, in p_tm_b time default null  время начала
, in p_tm_e time default null время окончания
, in p_stop_item_id  numeric default null  ОП, с которой начинается или заканчивается рейс
, in p_is_start_st boolean default true   - true -начинается, false- заканчивается
*/
create or replace function ttb.norm_pkg$get_schedule_tm (in p_tt_sсhedule_id numeric
                                                       , in p_ref_group_kind_id numeric default null
                                                       , in p_tm_b time default null
                                                       , in p_tm_e time default null
                                                       , in p_stop_item_id  numeric default null
                                                       , in p_is_start_st boolean default true
                                                       , in p_tr_capacity_id numeric default null
                                                       )    returns numeric
as
$body$
declare
  l_dur smallint;
begin
  --средняя общая продолжительность элементов последовательности по номативу
  if p_tm_b = '00:00:00'::time then
    p_tm_b := '24:00:00'::time;
  end if;
  if p_tm_e = '00:00:00'::time then
    p_tm_e := '24:00:00'::time;
  end if;
  with ss as (
      select
        v.norm_id, coalesce(v.round_id, lag(v.round_id) over(order by order_num) ) as round_id, (v.action_type_id = ttb.action_type_pkg$bn_round_break()) is_mo_break
      from
        ttb.v_tt_schedule_item_det  v
      where tt_schedule_id = p_tt_sсhedule_id
            and (v.action_type_id = ttb.action_type_pkg$bn_round_break()
                 or ttb.action_type_pkg$is_round(v.action_type_id)) --только межрейсы и рейсы можно по нормативам посчитать
            and (p_ref_group_kind_id is null or v.ref_group_kind_id = p_ref_group_kind_id)
            and (p_stop_item_id is null
                 or
                 p_stop_item_id = case when p_is_start_st then sp_b_id else sp_e_id end
                 or v.round_id is null
                 )
              )
  select
    sum(case when is_mo_break then ttb.norm_pkg$get_avg_bn_break(norm_id, round_id, p_tm_b, p_tm_e, p_tr_capacity_id)
                              else ttb.norm_pkg$get_avg_rnd(norm_id, round_id, p_tm_b, p_tm_e, p_tr_capacity_id) end ) as dur
  into l_dur
  from ss
  where round_id is not null;

  return coalesce(l_dur,0);
end;
$body$
language plpgsql  immutable
cost 100;


------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Создать временную таблицу
***************************************************************************/
create or replace function ttb.norm_pkg$make_tmp_norm(p_norm_id numeric)
  returns void
as
$body$
declare
begin
  discard temp;

  create temporary table if not exists  norm_round
  on commit preserve rows
  tablespace ttb_data  as
    select * from ttb.norm_round
    where norm_id = p_norm_id
  with data;

  create index  if not exists ix_norm_round
    on norm_round
    using btree
    (tr_type_id, tr_capacity_id, round_id, stop_item_id, hour_from, min_inter_round_stop_dur, reg_inter_round_stop_dur  )
  tablespace ttb_idx;

  create temporary table if not exists  norm_between_stop
  on commit preserve rows
  tablespace ttb_data  as
    select * from ttb.norm_between_stop
    where norm_id = p_norm_id
  with data;

  create index  if not exists ix_norm_between_stop
    on ttb.norm_between_stop
    using btree
    (
     tr_type_id,
     tr_capacity_id,
     stop_item_1_id,
     stop_item_2_id ,
     hour_from,
     between_stop_dur)
  tablespace ttb_idx;

end;
$body$
language plpgsql volatile
cost 100;


