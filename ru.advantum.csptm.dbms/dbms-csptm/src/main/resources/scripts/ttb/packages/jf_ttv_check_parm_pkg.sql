CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_parm_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.ttv_check_parm AS
$body$
declare 
   l_r 				ttb.ttv_check_parm%rowtype; 
   ln_tt_variant_id ttb.tt_variant.tt_variant_id%type;    
begin 
   l_r.ttv_check_parm_id := jofl.jofl_pkg$extract_number(p_attr, 'ttv_check_parm_id', true); 
   l_r.tt_check_parm_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tt_check_parm_id', true); 
   l_r.parm_value := jofl.jofl_pkg$extract_number(p_attr, 'parm_value', true); 
   l_r.sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'sign_deleted', true); 
   l_r.sys_period := jofl.jofl_pkg$extract_varchar(p_attr, 'sys_period', true); 
   
   select ttcc.tt_variant_id
   into ln_tt_variant_id
   from ttb.ttv_check_condition ttcc
   join ttb.ttv_check_parm ttcp on ttcp.ttv_check_condition_id = ttcc.ttv_check_condition_id
   where ttcp.ttv_check_parm_id = l_r.ttv_check_parm_id;
   
   if not ttb.jf_tt_variant_pkg$of_get_policy(ln_tt_variant_id)
    then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if;    

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_parm_pkg$of_ins_default_check (
  p_tt_variant_id numeric
)
RETURNS text AS
$body$
declare 
begin 
  insert into ttb.ttv_check_parm (
  							ttv_check_condition_id,
                            tt_check_parm_id,
                            parm_value)
  select tvcc.ttv_check_condition_id
  		,tcp.tt_check_parm_id
        ,tcp.parm_value
  from ttb.tt_check_parm tcp 
  join ttb.ttv_check_condition tvcc on tvcc.tt_check_condition_id = tcp.tt_check_condition_id
  where tvcc.tt_variant_id = p_tt_variant_id
    and not tcp.sign_deleted;
                             
  return null;                             
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;   
----------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_parm_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 ln_ttv_check_condition_id ttb.ttv_check_parm.ttv_check_condition_id%type := jofl.jofl_pkg$extract_number(p_attr, 'ttv_check_condition_id', true);
begin 
 open p_rows for 
       with check_parm as (  
                  select 
                         coalesce(tvcpi.ttv_check_parm_id, tvcpn.ttv_check_parm_id) as ttv_check_parm_id
                        ,coalesce(tvcpi.ttv_check_condition_id, tvcpn.ttv_check_condition_id) as ttv_check_condition_id 
                        ,coalesce(tvcpi.tt_check_parm_id, tvcpn.tt_check_parm_id) as tt_check_condition_id 
                        ,coalesce(tcpi.parm_name,tcpn.parm_name) as parm_name
                        ,coalesce(tcpi.parm_type, tcpn.parm_type) as parm_type
                        ,tvcpi.parm_value as parm_value_i
                        ,tvcpn.parm_value as parm_value_n
                        ,coalesce(tcpi.order_num, tcpn.order_num) as order_num
                      from ttb.tt_check_parm_type tcpt 
                      left join  ttb.tt_check_parm tcpi on tcpt.parm_type = tcpi.parm_type 
                                                       and tcpi.parm_type = ttb.jf_tt_check_parm_pkg$cn_interval_type()
                                                       and not tcpi.sign_deleted
                      left join  ttb.ttv_check_parm  tvcpi on tvcpi.tt_check_parm_id = tcpi.tt_check_parm_id                             
                      left join  ttb.tt_check_parm tcpn on tcpt.parm_type = tcpn.parm_type 
                                                       and tcpn.parm_type = ttb.jf_tt_check_parm_pkg$cn_number_type() 
                                                       and not tcpn.sign_deleted
                      left join  ttb.ttv_check_parm  tvcpn on tvcpn.tt_check_parm_id = tcpn.tt_check_parm_id                                                   
                  	  
                      )
                select   cp.ttv_check_parm_id
                        ,cp.ttv_check_condition_id 
                        ,cp.tt_check_condition_id
                        ,cp.parm_name
                        ,cp.parm_type
                        ,cp.parm_value_i
                        ,cp.parm_value_n 
                        ,case
                              when cp.parm_type = ttb.jf_tt_check_parm_pkg$cn_interval_type() then
                              'P{RO},D{parm_value_n}'
                              when cp.parm_type = ttb.jf_tt_check_parm_pkg$cn_number_type() then
                              'P{RO},D{parm_value_i}'
                              else
                              'P{RO},D{parm_value_i,parm_value_n}'
                         end "ROW$POLICY"
                from check_parm cp 
                where cp.ttv_check_condition_id = ln_ttv_check_condition_id
                order by cp.order_num; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_ttv_check_parm_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.ttv_check_parm%rowtype;
   ln_parm_value_i 	integer 	:= jofl.jofl_pkg$extract_number(p_attr, 'parm_value_i', true); 
   ln_parm_value_n  smallint 	:= jofl.jofl_pkg$extract_number(p_attr, 'parm_value_n', true);
   ln_parm_type		text		:= jofl.jofl_pkg$extract_varchar(p_attr, 'parm_type', true);
begin 
   l_r := ttb.jf_ttv_check_parm_pkg$attr_to_rowtype(p_attr);

if ln_parm_type = ttb.jf_tt_check_parm_pkg$cn_interval_type() then
     if coalesce(ln_parm_value_i, 0) = 0 then
   		return  jofl.jofl_util$cover_result('error', 'Заполните значение параметра (время)'); 
     else
     	 update ttb.ttv_check_parm set 
          parm_value = ln_parm_value_i      
   		 where 
          ttv_check_parm_id = l_r.ttv_check_parm_id;
          
         return null;
     end if;
   end if;
   
   if ln_parm_type = ttb.jf_tt_check_parm_pkg$cn_number_type() then
     if ln_parm_value_n is null then
   		return  jofl.jofl_util$cover_result('error', 'Заполните значение параметра (число)');
     else
      	 update ttb.ttv_check_parm set 
          parm_value = ln_parm_value_n      
   		 where 
          ttv_check_parm_id = l_r.ttv_check_parm_id;
          
         return null;    
     end if;
   end if;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------
----------------------------------------
----------------------------------------