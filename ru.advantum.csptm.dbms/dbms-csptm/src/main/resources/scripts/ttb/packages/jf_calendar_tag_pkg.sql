CREATE OR REPLACE FUNCTION ttb."jf_calendar_tag_pkg$attr_to_rowtype"(p_attr text)
 RETURNS ttb.calendar_tag
 LANGUAGE plpgsql
AS $function$
 
declare 
   l_r ttb.calendar_tag%rowtype; 
begin 
   l_r.calendar_tag_id := jofl.jofl_pkg$extract_varchar(p_attr, 'calendar_tag_id', true); 
   l_r.tag_name := jofl.jofl_pkg$extract_varchar(p_attr, 'tag_name', true); 
   l_r.calendar_desc := jofl.jofl_pkg$extract_varchar(p_attr, 'calendar_desc', true); 
   l_r.calendar_tag_group_id := jofl.jofl_pkg$extract_varchar(p_attr, 'calendar_tag_group_id', true); 
   l_r.tag_name_short := jofl.jofl_pkg$extract_varchar(p_attr, 'tag_name_short', true); 

   return l_r;
end;
 
$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_calendar_tag_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$

declare
  l_calendar_tag_id SMALLINT;
  l_rf_db_method text;
  l_calendar_id ttb.calendar.calendar_id%type := jofl.jofl_pkg$extract_number(p_attr, 'calendar_id', true);
  l_calendar_tag_group_id ttb.calendar_tag_group.calendar_tag_group_id%type := jofl.jofl_pkg$extract_number(p_attr, 'calendar_tag_group_id', true);
  l_tt_variant_id ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
  begin
    l_rf_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
    l_calendar_tag_id := jofl.jofl_pkg$extract_number(p_attr, 'calendar_tag_id', true);
    open p_rows for
    select
      t.calendar_tag_id,
      t.tag_name,
   t.calendar_desc,
   t.calendar_tag_group_id,
   t.tag_name_short,
   g.group_name,
   g.is_alone_group
 from ttb.calendar_tag t
   left join ttb.calendar_tag_group g on g.calendar_tag_group_id = t.calendar_tag_group_id
 where ((l_rf_db_method is null or l_rf_db_method in('ttb.rv_peak_hour','ttb.tune_peak_hour'))
       and (l_calendar_tag_group_id is null or t.calendar_tag_group_id = l_calendar_tag_group_id))
       or
       (l_rf_db_method in ('ttb.calendar_item','ttb.calendar')
        and t.calendar_tag_id not in (select ci.calendar_tag_id
                                    from ttb.calendar_item ci
                                    where ci.calendar_id = l_calendar_id)
        and (t.calendar_tag_group_id is null 
            or
             (t.calendar_tag_group_id not in (select ctg.calendar_tag_group_id
                                             from ttb.calendar_tag_group ctg
                                             where ctg.calendar_tag_group_id in (select ct.calendar_tag_group_id
                                                                                 from ttb.calendar_item ci
                                                                                 join ttb.calendar_tag ct on ci.calendar_tag_id = ct.calendar_tag_id
                                                                                 join ttb.calendar_tag_group ctg on ct.calendar_tag_group_id = ctg.calendar_tag_group_id
                                                                                 where ci.calendar_id = l_calendar_id
                                                                                   and ctg.is_alone_group)
                                             )
            )
        )
       or
       (l_rf_db_method in ('ttb.tt_calendar')
        and   t.calendar_tag_id not in (select ttc.calendar_tag_id
                                        from ttb.tt_calendar ttc
                                        where ttc.tt_variant_id = l_tt_variant_id)
        )
     )



  ;
end;

$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_calendar_tag_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
 
declare 
   l_r ttb.calendar_tag%rowtype;
begin 
   l_r := ttb.jf_calendar_tag_pkg$attr_to_rowtype(p_attr);

   update ttb.calendar_tag set 
          tag_name = l_r.tag_name, 
          calendar_desc = l_r.calendar_desc, 
          calendar_tag_group_id = l_r.calendar_tag_group_id, 
          tag_name_short = l_r.tag_name_short
   where 
          calendar_tag_id = l_r.calendar_tag_id;

   return null;
end;
 
$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_calendar_tag_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
 
declare 
   l_r ttb.calendar_tag%rowtype;
begin 
   l_r := ttb.jf_calendar_tag_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.calendar_tag where  calendar_tag_id = l_r.calendar_tag_id;

   return null;
end;
 
$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_calendar_tag_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$

declare 
   l_r ttb.calendar_tag%rowtype;
begin 
   l_r := ttb.jf_calendar_tag_pkg$attr_to_rowtype(p_attr);
   l_r.calendar_tag_id := nextval( 'ttb.calendar_tag_calendar_tag_id_seq' );
  
   if l_r.calendar_tag_group_id < 0 then
      l_r.calendar_tag_group_id := null;
   end if;
  
   insert into ttb.calendar_tag select l_r.*;

   return null;
end;

$function$
;