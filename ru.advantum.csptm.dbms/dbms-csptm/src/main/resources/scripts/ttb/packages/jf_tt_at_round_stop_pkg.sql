CREATE OR REPLACE FUNCTION ttb.jf_tt_at_round_stop_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.tt_at_round_stop AS
$body$
declare 
   l_r 				ttb.tt_at_round_stop%rowtype; 
   ln_tt_variant_id ttb.tt_variant.tt_variant_id%type;      
begin 
   l_r.tt_at_round_stop_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_at_round_stop_id', true); 
   l_r.stop_item2round_id := jofl.jofl_pkg$extract_number(p_attr, 'stop_item2round_id', true); 
   l_r.tt_at_round_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_at_round_id', true); 
   
   select tat.tt_variant_id
   into ln_tt_variant_id
   from ttb.tt_at_round tar 
   join ttb.tt_action_type tat on tat.tt_action_type_id = tar.tt_action_type_id
   where tar.tt_at_round_id = l_r.tt_at_round_id
   limit 1;  
   
   if not ttb.jf_tt_variant_pkg$of_get_policy(ln_tt_variant_id)
    then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if;    

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_at_round_stop_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$ 
declare
  l_tt_at_round_id ttb.tt_at_round.tt_at_round_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'tt_at_round_id', true);
begin 
 open p_rows for
 select
   trs.tt_at_round_stop_id,
   trs.stop_item2round_id,
   trs.tt_at_round_id,
   sir.order_num as stop_order_num,
   s.name as stop_name,
   act.tt_variant_id,
   sir.stop_item_id,
   s.stop_id
 from ttb.tt_at_round_stop trs
   join rts.stop_item2round sir on sir.stop_item2round_id = trs.stop_item2round_id
   join rts.stop_item si on si.stop_item_id =sir.stop_item_id
   join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
   join rts.stop s on s.stop_id = sl.stop_id
   join ttb.tt_at_round ar on ar.tt_at_round_id = trs.tt_at_round_id
   join ttb.tt_action_type act on act.tt_action_type_id = ar.tt_action_type_id
 where trs.tt_at_round_id  = l_tt_at_round_id
  order by sir.order_num
  ;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_at_round_stop_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tt_at_round_stop%rowtype;
begin 
   l_r := ttb.jf_tt_at_round_stop_pkg$attr_to_rowtype(p_attr);

   update ttb.tt_at_round_stop set 
          stop_item2round_id = l_r.stop_item2round_id, 
          tt_at_round_id = l_r.tt_at_round_id
   where 
          tt_at_round_stop_id = l_r.tt_at_round_stop_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_at_round_stop_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tt_at_round_stop%rowtype;
begin 
   l_r := ttb.jf_tt_at_round_stop_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.tt_at_round_stop where  tt_at_round_stop_id = l_r.tt_at_round_stop_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_at_round_stop_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.tt_at_round_stop%rowtype;
begin 
   l_r := ttb.jf_tt_at_round_stop_pkg$attr_to_rowtype(p_attr);

   insert into ttb.tt_at_round_stop select l_r.*;

   return null;
end;
 $function$
;