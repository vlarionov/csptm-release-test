CREATE OR REPLACE FUNCTION ttb.jf_tt_peak_hour_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.tt_peak_hour AS
$body$
declare 
   l_r ttb.tt_peak_hour%rowtype; 
   l_sec_a  	int:= jofl.jofl_pkg$extract_number(p_attr, 'sec_a', true);
   l_sec_b  	int:= jofl.jofl_pkg$extract_number(p_attr, 'sec_b', true);
   l_dt_action 	date;   
begin 
   l_r.tt_peak_hour_id 	:= jofl.jofl_pkg$extract_number(p_attr, 'tt_peak_hour_id', true); 
   l_r.tt_variant_id 	:= jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true); 
   /*
   l_r.time_begin 		:= jofl.jofl_pkg$extract_date(p_attr, 'time_begin', true); 
   l_r.time_end 		:= jofl.jofl_pkg$extract_date(p_attr, 'time_end', true); 
   */
   l_r.peak_hour_name 	:= jofl.jofl_pkg$extract_varchar(p_attr, 'peak_hour_name', true); 
   
   select case when ttv.parent_tt_variant_id is null then '1900-01-01'::date else lower(ttv.action_period)::date end dt_action
   into l_dt_action 
   from ttb.tt_variant ttv
   where ttv.tt_variant_id = l_r.tt_variant_id;
   
   l_r.time_begin := l_dt_action  + l_sec_a * interval '1 sec';
   l_r.time_end   := l_dt_action  + l_sec_b * interval '1 sec'; 

   if not ttb.jf_tt_variant_pkg$of_get_policy(l_r.tt_variant_id)
    then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
   end if; 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_peak_hour_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.tt_peak_hour%rowtype;
begin 
   l_r := ttb.jf_tt_peak_hour_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.tt_peak_hour where  tt_peak_hour_id = l_r.tt_peak_hour_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_peak_hour_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r 				ttb.tt_peak_hour%rowtype;
   larr_b_overlaps 	boolean[];
   l_b_overlaps		boolean;
   l_b_cldr_exists	boolean;
   l_n_cnt			smallint;
begin 
   l_r := ttb.jf_tt_peak_hour_pkg$attr_to_rowtype(p_attr);
   
   select count(1) 
     into l_n_cnt
     from ttb.tt_peak_hour
   	where tt_variant_id = l_r.tt_variant_id
    limit 1;   
   
   if l_n_cnt > 0 then
   
     select  array_agg(array[tstzrange(tph.time_begin,tph.time_end) 
          				  && tstzrange(l_r.time_begin,l_r.time_end)])
     into larr_b_overlaps
     from ttb.tt_peak_hour tph
     where tt_variant_id = l_r.tt_variant_id;
     
   end if;
   
   select count(1)::int::boolean
   into l_b_overlaps
   from unnest(larr_b_overlaps) s(a)
   where a is true; 
   
   select count(1)::integer::boolean
   into l_b_cldr_exists
   from ttb.calendar_tag ctag
   join ttb.tt_calendar cl on ctag.calendar_tag_id = cl.calendar_tag_id    
   where cl.tt_variant_id = l_r.tt_variant_id
   limit 1;  

   if not l_b_overlaps and l_b_cldr_exists then
     insert into ttb.tt_peak_hour
     ( tt_variant_id
      ,time_begin
      ,time_end
      ,peak_hour_name
     )
      select l_r.tt_variant_id
            ,l_r.time_begin
            ,l_r.time_end
            ,l_r.peak_hour_name;
    else
     case 
       when l_b_overlaps then
     	RAISE EXCEPTION '<<Введенный интервал пересекается с существующим(и) для данного варианта расписания!>>';  
       when not l_b_cldr_exists then
     	RAISE EXCEPTION '<<Сначала заполните календарь расписания!>>'; 
       else 
        null;  
       end case;
    end if;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_peak_hour_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 ln_tt_variant_id bigint := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
begin 
 open p_rows for 
      select distinct
        tph.tt_peak_hour_id, 
        tph.tt_variant_id, 
        tph.time_begin, 
        tph.time_end,
        tph.peak_hour_name,
        EXTRACT(hour from tph.time_begin)::INTEGER*60*60 + EXTRACT(minute from tph.time_begin)::INTEGER*60 as sec_a,
        EXTRACT(hour from tph.time_end)::INTEGER*60*60   + EXTRACT(minute from tph.time_end)::INTEGER*60   as sec_b,
        'P{A},D{OF_INSERT}' as "ROW$POLICY"
      from ttb.tt_calendar cl
  	  join ttb.calendar_tag ctag on ctag.calendar_tag_id = cl.calendar_tag_id
  	  left join ttb.tt_peak_hour tph on tph.tt_variant_id = cl.tt_variant_id      
      where cl.tt_variant_id = ln_tt_variant_id; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_peak_hour_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r 				ttb.tt_peak_hour%rowtype;
   larr_b_overlaps 	boolean[];
   l_b_overlaps 	boolean;
begin 
   l_r := ttb.jf_tt_peak_hour_pkg$attr_to_rowtype(p_attr);
   
   select  array_agg(array[tstzrange(tph.time_begin,tph.time_end) 
						&& tstzrange(l_r.time_begin,l_r.time_end)])
   into larr_b_overlaps
   from ttb.tt_peak_hour tph
   where tt_variant_id = l_r.tt_variant_id
     and tph.tt_peak_hour_id <> l_r.tt_peak_hour_id;
     
   select count(1)::int::boolean
   into l_b_overlaps
   from unnest(larr_b_overlaps) s(a)
   where a is true; 

   if not l_b_overlaps then
     update ttb.tt_peak_hour set 
            tt_variant_id = l_r.tt_variant_id, 
            time_begin = l_r.time_begin, 
            time_end = l_r.time_end,
            peak_hour_name = l_r.peak_hour_name
     where 
            tt_peak_hour_id = l_r.tt_peak_hour_id;
   else
     RAISE EXCEPTION '<<Введенный интервал пересекается с существующим(и) для данного варианта расписания!>>';  
   end if;          

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_peak_hour_pkg$of_add_all (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r 				ttb.tt_peak_hour%rowtype;
   l_n_cnt			smallint;
   l_dt_action 		date;
   l_b_cldr_exists	boolean;
begin 
   l_r := ttb.jf_tt_peak_hour_pkg$attr_to_rowtype(p_attr);
   
   select count(1)::integer::boolean
   into l_b_cldr_exists
   from ttb.calendar_tag ctag
   join ttb.tt_calendar cl on ctag.calendar_tag_id = cl.calendar_tag_id    
   where cl.tt_variant_id = l_r.tt_variant_id
   limit 1;  
   
   if not l_b_cldr_exists 
    then RAISE EXCEPTION '<<Сначала заполните календарь расписания!>>'; 
   end if;
   
   select case 
   			when ttv.parent_tt_variant_id is null 
              then '1900-01-01'::date 
            else lower(ttv.action_period)::date 
          end 
   into l_dt_action
   from ttb.tt_variant ttv
   where ttv.tt_variant_id = l_r.tt_variant_id;
   
   delete from ttb.tt_peak_hour
   where tt_variant_id = l_r.tt_variant_id;
   
   insert into ttb.tt_peak_hour
     ( tt_variant_id
      ,time_begin
      ,time_end
      ,peak_hour_name
     )
      select agr.tt_variant_id
            ,agr.time_begin
            ,agr.time_end
            ,'Часы пик ' || row_number() over()::text as peak_hour_name             
             from ( 
                 with 
                     t as (   
                           select 
                           cl.tt_variant_id
                          ,tstzrange((l_dt_action + t.time_begin * interval '1 sec'),
                                     (l_dt_action + t.time_end * interval '1 sec')) r
                           from ttb.rv_peak_hour t
                           join ttb.calendar_tag ctag on ctag.calendar_tag_id = t.calendar_tag_id
                           join ttb.tt_calendar cl on ctag.calendar_tag_id = cl.calendar_tag_id 
						   join ttb.tt_variant tv on tv.route_variant_id = t.route_variant_id 
                           					     and tv.tt_variant_id = cl.tt_variant_id						   
                           where cl.tt_variant_id = l_r.tt_variant_id
                          group by cl.tt_variant_id      
                                  ,tstzrange((l_dt_action + t.time_begin * interval '1 sec'),
                                             (l_dt_action + t.time_end * interval '1 sec'))
                         )
                   ,tt as (
                   select t.r
                        ,case 
                           when lead(t.r) over (order by lower(t.r), upper(t.r)) && t.r
                           then 1
                           else 0
                         end sog
                        ,case 
                           when lag(t.r) over (order by lower(t.r), upper(t.r)) && t.r
                           then 1
                           else 0
                         end eog
                  from t
                   )
                   ,ttt as (
                   select tt.*
                      ,case
                        when tt.sog = tt.eog
                          then lower(tt.r)::text || ',' || upper(tt.r)::text
                        when tt.sog = 1 
                          then lower(tt.r)::text || ',' || (lead(upper(tt.r)) over (order by lower(tt.r), upper(tt.r)))::text
                        else null
                       end gr_range
                    from tt
                    where not (tt.sog::boolean and tt.eog::boolean)
                   )
                   select --ttt.* 
                         --,tstzrange('[' || ttt.gr_range || ']')
                          l_r.tt_variant_id as tt_variant_id
                         ,lower(tstzrange('[' || ttt.gr_range || ']')) as time_begin
                         ,upper(tstzrange('[' || ttt.gr_range || ']')) as time_end
                   from ttt
                   where ttt.gr_range is not null) agr;
      
     
   select count(1) 
     into l_n_cnt
     from ttb.tt_peak_hour
   	where tt_variant_id = l_r.tt_variant_id
    limit 1;   
    
    if l_n_cnt = 0 then            

         insert into ttb.tt_peak_hour
           ( tt_variant_id
            ,time_begin
            ,time_end
            ,peak_hour_name
           )
             select agr.tt_variant_id
            	   ,agr.time_begin
                   ,agr.time_end
                   ,'Часы пик ' || row_number() over()::text as peak_hour_name             
             from ( 
                 with 
                     t as (   
                           select 
                           cl.tt_variant_id
                          ,tstzrange((l_dt_action + ttph.time_begin * interval '1 sec'),
                                     (l_dt_action + ttph.time_end * interval '1 sec')) r
                           from ttb.tune_peak_hour ttph
                           join ttb.calendar_tag ctag on ctag.calendar_tag_id = ttph.calendar_tag_id
                           join ttb.tt_calendar cl on ctag.calendar_tag_id = cl.calendar_tag_id             
                          where cl.tt_variant_id = l_r.tt_variant_id
                          group by cl.tt_variant_id      
                                  ,tstzrange((l_dt_action + ttph.time_begin * interval '1 sec'),
                                             (l_dt_action + ttph.time_end * interval '1 sec'))
                         )
                   ,tt as (
                   select t.r
                        ,case 
                           when lead(t.r) over (order by lower(t.r), upper(t.r)) && t.r
                           then 1
                           else 0
                         end sog
                        ,case 
                           when lag(t.r) over (order by lower(t.r), upper(t.r)) && t.r
                           then 1
                           else 0
                         end eog
                  from t
                   )
                   ,ttt as (
                   select tt.*
                      ,case
                        when tt.sog = tt.eog
                          then lower(tt.r)::text || ',' || upper(tt.r)::text
                        when tt.sog = 1 
                          then lower(tt.r)::text || ',' || (lead(upper(tt.r)) over (order by lower(tt.r), upper(tt.r)))::text
                        else null
                       end gr_range
                    from tt
                    where not (tt.sog::boolean and tt.eog::boolean)
                   )
                   select --ttt.* 
                         --,tstzrange('[' || ttt.gr_range || ']')
                          l_r.tt_variant_id as tt_variant_id
                         ,lower(tstzrange('[' || ttt.gr_range || ']')) as time_begin
                         ,upper(tstzrange('[' || ttt.gr_range || ']')) as time_end
                   from ttt
                   where ttt.gr_range is not null) agr;
                            
    end if;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

----------------------------------------------------------------
----------------------------------------------------------------