CREATE OR REPLACE FUNCTION ttb."jf_tt_variant_pkg$attr_to_rowtype"(p_attr text)
 RETURNS ttb.tt_variant
 LANGUAGE plpgsql
AS $function$
declare
   l_r ttb.tt_variant%rowtype;
   l_ap_b 		date 	:= jofl.jofl_pkg$extract_date(p_attr, 'ap_b', true);
   l_ap_e 		date	:= jofl.jofl_pkg$extract_date(p_attr, 'ap_e', true);
begin
   l_r.tt_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
   l_r.norm_id := jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true);
   l_r.route_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'route_variant_id', true);
   l_r.is_all_tr_online := jofl.jofl_pkg$extract_boolean(p_attr, 'is_all_tr_online', true);
   l_r.sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'sign_deleted', true);
   l_r.sys_period := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'sys_period', true)
   							, tstzrange(now(), NULL::timestamp with time zone)::text);
   l_r.tt_status_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_status_id', true);
   l_r.ttv_name := jofl.jofl_pkg$extract_varchar(p_attr, 'ttv_name', true);
   l_r.ttv_comment := jofl.jofl_pkg$extract_varchar(p_attr, 'ttv_comment', true);
   l_r.is_calc_indicator := jofl.jofl_pkg$extract_boolean(p_attr, 'is_calc_indicator', true);
   l_r.is_change_driver := jofl.jofl_pkg$extract_boolean(p_attr, 'is_change_driver', true);

   l_r.action_period := tsrange(l_ap_b,l_ap_e,'[]');
   return l_r;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_tt_variant_id 	ttb.tt_variant.tt_variant_id%TYPE 	:= jofl.jofl_pkg$extract_number(p_attr, 'f_tt_variant_id', TRUE);
  ln_tr_type_id		rts.route.tr_type_id%type 			:= jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id', TRUE);
  ln_route_id		rts.route.route_id%type 			:= jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', TRUE);
  ln_tt_status_id	ttb.tt_variant.tt_status_id%type 	:= jofl.jofl_pkg$extract_number(p_attr, 'f_tt_status_id', TRUE);
  l_rf_db_method	text 								:= coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE), 'xxx');
  f_disp_dt			timestamp 							:= jofl.jofl_pkg$extract_date(p_attr, 'f_disp_DT', TRUE);
  f_stop_item_id	rts.stop_item.stop_item_id%type 	:= jofl.jofl_pkg$extract_number(p_attr, 'f_stop_item_id', TRUE);
  f_ttv_name		ttb.tt_variant.ttv_name%type 		:= jofl.jofl_pkg$extract_varchar(p_attr, 'f_ttv_name_TEXT', TRUE);
  ln_tt_variant_id 	ttb.tt_variant.tt_variant_id%TYPE 	:= jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', TRUE);
  ln_tt_tr_period_id_from ttb.tt_tr_period.tt_tr_period_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_tr_period_id_from', TRUE);
begin
 open p_rows for
      select
        ttv.tt_variant_id,
        ttv.norm_id,
        ttv.route_variant_id,
        ttv.is_all_tr_online,
        ttv.sign_deleted,
        --ttv.sys_period,
        ttv.tt_status_id,
        ttv.action_period,
        ttv.ttv_name,
        ttv.ttv_comment,
        r.route_num ||' ('|| to_char(lower(rv.action_period),'dd.mm.yyyy') || '-'||coalesce(to_char(upper(rv.action_period),'dd.mm.yyyy'),'нвр')||')'   as rv_name,
        ttv.is_calc_indicator
       ,n.norm_name
       ,r.route_num
       ,lower(ttv.action_period) ap_b, upper(ttv.action_period) ap_e
       ,rv.action_period::text rv_action_period
       ,st.tt_status_name
       ,r.tr_type_id
	   ,r.route_id
       ,crst.tt_create_status_id
       ,crst.tt_create_status_name
       --,(select count(1) from ttb.tt_driver td where td.tt_variant_id = ttv.tt_variant_id and td.is_сhanged_driver limit 1)::int::boolean as is_change_driver
	   ,ttv.is_change_driver
       ,case
       		when ttv.tt_status_id <> ttb.jf_tt_status_pkg$cn_arch_status_id()
              then case
                when ttb.agreement_pkg$is_available_to_agreement(p_id_account, ttv.tt_variant_id)
                  then case when ttb.jf_tt_variant_pkg$available_to_cancel(ttv.tt_variant_id)
                              then 'P{A},D{OF_TO_AGREEMENT, OF_DELETE, OF_CLEAR, OF_CREATE, OF_UPDATE, OF_LINING, OF_CREATE_TEST}'
                            when ttb.jf_tt_variant_pkg$available_to_terminate(ttv.tt_variant_id)
                              then 'P{A},D{OF_TO_AGREEMENT, OF_TERMINATE},P{RO},D{ap_e}'
                            else 'P{A},D{OF_TO_AGREEMENT, OF_UPDATE}'
                       end 
                else case when ttb.jf_tt_variant_pkg$available_to_cancel(ttv.tt_variant_id)
                              then 'P{A},D{OF_DELETE, OF_CLEAR, OF_CREATE, OF_UPDATE,  OF_LINING, , OF_CREATE_TEST}'
                          when ttb.jf_tt_variant_pkg$available_to_terminate(ttv.tt_variant_id)
                              then 'P{A},D{OF_TERMINATE},P{RO},D{ap_e}'
                          else 'P{A},D{OF_UPDATE}'
                       end  
               end 
            else 'P{A},D{OF_TERMINATE}'
       end 
       ||
       case
       		when ttv.tt_status_id in ( ttb.jf_tt_status_pkg$cn_on_approving_status_id()
            						  ,ttb.jf_tt_status_pkg$cn_approved_status_id() )
              then ',P{RO},D{rv_name, norm_name, ttv_name, is_change_driver, is_calc_indicator, is_all_tr_online, ttv_comment}'
            else ''
       end  "ROW$POLICY"
      ,case 
      	when exists (select null
                      from ttb.tt_switch sw
                      join ttb.tt_schedule_item sif on sif.tt_schedule_item_id = sw.tt_schedule_item_id_from
                      join ttb.tt_schedule sf on sf.tt_schedule_id = sif.tt_schedule_id
                      join ttb.tt_variant ttvf on ttvf.tt_variant_id = sf.tt_variant_id
                      join ttb.tt_schedule st on st.tt_schedule_id = sw.tt_schedule_id_to
                      join ttb.tt_variant ttvt on ttvt.tt_variant_id = st.tt_variant_id
                      join ttb.tt_switch_group tsg on tsg.tt_variant_id_to = ttvt.tt_variant_id
                                                  and tsg.tt_variant_id_from = ttvf.tt_variant_id
                                                  and (sw.tt_tr_period_id_to = ln_tt_tr_period_id_from or ln_tt_tr_period_id_from is null)
                                                  and tsg.tt_variant_id_to = ln_tt_variant_id 
                                                  and tsg.tt_variant_id_from = ttv.tt_variant_id
                                                  and l_rf_db_method = 'ttb.tt_switch')             
        then 16757759 /*ffb3ff*/
   	   end "ROW$COLOR"
      , '[' || json_build_object('CAPTION', 'Выходы', 'JUMP',
               json_build_object('METHOD', 'ttb.tt_out', 'ATTR', '{"tt_variant_id":"' || ttv.tt_variant_id || '"}')) || ']' "JUMP_TO_OUT"
      from ttb.tt_variant ttv
      join ttb.norm n on n.norm_id = ttv.norm_id
      join ttb.tt_status st on st.tt_status_id = ttv.tt_status_id
      join rts.route_variant rv on ttv.route_variant_id = rv.route_variant_id
      join rts.route r on rv.route_id = r.route_id
      join ttb.tt_create_status crst on crst.tt_create_status_id = ttv.tt_create_status_id
  where ttv.parent_tt_variant_id is null
  		and (
        	(l_rf_db_method in ('xxx', 'ttb.jrep_ttv_tr_km','ttb.tr_capacity_route_pass_time_report','ttb.route_pass_time_report', 'ttb.jrep_ttv_route_tr_count' , 'ttb.route_movement_schedule' , 'ttb.route_mode_report')
            and ( l_tt_variant_id is null or ttv.tt_variant_id = l_tt_variant_id )
            and ( ln_tr_type_id is null or r.tr_type_id = ln_tr_type_id )
            and ( ln_route_id is null or r.route_id = ln_route_id )
            and ( (ln_tt_status_id is null and ttv.tt_status_id != ttb.jf_tt_status_pkg$cn_cancel_status_id () ) 
                   or 
                  (ttv.tt_status_id = ln_tt_status_id )
                )
            and ( f_ttv_name is null or ttv.ttv_name like f_ttv_name || '%' )
            )
        or
            (l_rf_db_method in ('ttb.tt_dispersal', 'ttb.dispersal_task')
             and ttv.tt_status_id in (ttb.jf_tt_status_pkg$cn_work_status_id ())
             and exists (select null
                          from ttb.tt_out out 
                          join ttb.tt_action ta on ta.tt_out_id = out.tt_out_id
                          join ttb.action_type at on at.action_type_id = ta.action_type_id
                          join ttb.tt_action_item tai on tai.tt_action_id = ta.tt_action_id
                          join ttb.tt_action_round tar on tar.tt_action_id = ta.tt_action_id
                          join rts.round r on r.round_id = tar.round_id
                                           and r.action_type_id = at.action_type_id
                          join rts.route rt on rt.current_route_variant_id = r.route_variant_id 
                          --join rts.move_direction md on md.move_direction_id = r.move_direction_id
                          join rts.stop_item2round si2r on si2r.round_id = r.round_id 
                                                        and si2r.stop_item2round_id = tai.stop_item2round_id                                  
                          join rts.stop_item2round_type si2rt on si2rt.stop_item2round_id = si2r.stop_item2round_id
                          join rts.stop_item si on si.stop_item_id = si2r.stop_item_id
                          where 1 = 1 
                            and si.stop_item_id = f_stop_item_id
                            and out.tt_variant_id = ttv.tt_variant_id
                            )
             and ttv.action_period @> f_disp_dt::timestamp
            )
        )
  or (l_rf_db_method = 'ttb.route_mode_report'
      and ttv.tt_status_id in (ttb.jf_tt_status_pkg$cn_work_status_id ()))
  or (l_rf_db_method = 'ttb.route_movement_schedule'
      and ttv.tt_status_id in (ttb.jf_tt_status_pkg$cn_work_status_id ()))
  or (l_rf_db_method = 'ttb.tt_switch'
      and ttv.tt_status_id in (ttb.jf_tt_status_pkg$cn_work_status_id ()))
  ;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
   l_r 		ttb.tt_variant%rowtype;
   lt_res 	text;
begin
   l_r := ttb.jf_tt_variant_pkg$attr_to_rowtype(p_attr);

   update ttb.tt_variant set
          norm_id = l_r.norm_id,
          route_variant_id = l_r.route_variant_id,
          is_all_tr_online = l_r.is_all_tr_online,
          sign_deleted = l_r.sign_deleted,
          --sys_period = l_r.sys_period,
          /*tt_status_id = case when upper(l_r.action_period) < current_timestamp::date
          						then ttb.jf_tt_status_pkg$cn_arch_status_id ()
          					  else tt_status_id
          			     end,*/
          action_period = l_r.action_period,
          ttv_name = l_r.ttv_name,
          ttv_comment = l_r.ttv_comment,
          is_calc_indicator = l_r.is_calc_indicator,
          is_change_driver = l_r.is_change_driver		  
   where
          tt_variant_id = l_r.tt_variant_id;
          
   lt_res := ttb.jf_tt_variant_pkg$of_actualize_ttv_status (l_r.tt_variant_id);

   perform ttb.jf_ttv_check_condition_pkg$of_close_prev_check_cond(l_r.tt_variant_id, null::integer);

   if lt_res is not null and l_r.tt_status_id = ttb.jf_tt_status_pkg$cn_approved_status_id() then
      return jofl.jofl_util$cover_result('message', coalesce(lt_res, 'no message'));
   else
      return null;
   end if;
   
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_variant_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
   l_r ttb.tt_variant%rowtype;
begin
   /*l_r := ttb.jf_tt_variant_pkg$attr_to_rowtype(p_attr);*/

   l_r.tt_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
   /*delete from  ttb.tt_variant where  tt_variant_id = l_r.tt_variant_id;*/

   update ttb.tt_variant
      set tt_status_id = ttb.jf_tt_status_pkg$cn_cancel_status_id()
    where tt_variant_id = l_r.tt_variant_id;

   return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
   l_r 			ttb.tt_variant%rowtype;
   ln_ttv_num	smallint;
   lt_route_num text;
begin
  l_r := ttb.jf_tt_variant_pkg$attr_to_rowtype(p_attr);
  l_r.tt_status_id := ttb.jf_tt_status_pkg$cn_work_status_id();

  /*
  select count(1) + 1
  into ln_ttv_num
  from ttb.tt_variant ttv
  where ttv.route_variant_id = l_r.route_variant_id;
  */
  select count(1) + 1
  into ln_ttv_num
  from ttb.tt_variant ttv
  join rts.route_variant rv on ttv.route_variant_id = rv.route_variant_id
  join rts.route r on rv.route_id = r.route_id
  where exists (select null from rts.route_variant rvi 
  						join rts.route ri on rvi.route_id = ri.route_id 
                        where rvi.route_variant_id = l_r.route_variant_id
                          and ri.route_id = r.route_id);
  
  select r.route_num
  into lt_route_num
  from rts.route_variant rv 
  join rts.route r on rv.route_id = r.route_id and rv.route_variant_id = l_r.route_variant_id;
  
  l_r.ttv_name := ln_ttv_num::text || '-' || lt_route_num::text || '/' || to_char(lower(l_r.action_period), 'DDMMYYYY');

  insert into ttb.tt_variant (norm_id
  							 ,route_variant_id
                             ,tt_status_id
                             ,ttv_name
                             ,ttv_comment
                             ,action_period
                             ,is_all_tr_online
							 ,is_change_driver)
   					  values (l_r.norm_id
                      		 ,l_r.route_variant_id  
                      		 ,l_r.tt_status_id
                             ,l_r.ttv_name
                      		 ,l_r.ttv_comment
                             ,l_r.action_period
                             ,l_r.is_all_tr_online
							 ,coalesce(l_r.is_change_driver, false))
  returning tt_variant_id 
  into l_r.tt_variant_id;

  perform ttb.jf_ttv_check_condition_pkg$of_ins_default_check (l_r.tt_variant_id);
  
  perform ttb.jf_ttv_check_parm_pkg$of_ins_default_check (l_r.tt_variant_id);

  perform ttb.jf_tt_action_type_pkg$of_refresh(p_id_account,  row_to_json(l_r)::text) ;
  
  perform ttb.jf_tt_variant_pkg$insert_available_action_group (p_id_account , row_to_json(l_r)::text);
  
  return ttb.jf_tt_variant_pkg$insert_default_schedule (p_id_account , row_to_json(l_r)::text);


end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_variant_pkg$available_to_cancel"(p_tt_variant_id numeric)
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$
declare
    lb_res boolean := false;
begin
  select count(1)::integer::boolean
  into lb_res
  from ttb.tt_variant tv where tv.tt_variant_id = p_tt_variant_id
   and tv.tt_status_id = ttb.jf_tt_status_pkg$cn_work_status_id();

  return lb_res;

end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$available_to_terminate (
  p_tt_variant_id numeric
)
RETURNS boolean AS
$body$
declare
    lb_res boolean := false;
begin
  select count(1)::integer::boolean
  into lb_res
  from ttb.tt_variant tv where tv.tt_variant_id = p_tt_variant_id
   and tv.tt_status_id in ( ttb.jf_tt_status_pkg$cn_approved_status_id()
   						   ,ttb.jf_tt_status_pkg$cn_active_status_id() );

  return lb_res;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

-------------------
CREATE OR REPLACE FUNCTION ttb."jf_tt_variant_pkg$of_to_agreement"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
    l_tt_variant_id  ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
begin
  PERFORM ttb.agreement_pkg$to_agreement(p_id_account::BIGINT, l_tt_variant_id);

  return null;

end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$of_terminate (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
  l_ts_end_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'end_DT', true);
  ln_tt_variant_id 	bigint := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
  l_ap_b 			date;
  l_ap_e 			date;
  ln_cnt 			smallint;
begin

  select lower(ttv.action_period)
        ,upper(ttv.action_period)
   into l_ap_b
   	   ,l_ap_e
   from ttb.tt_variant ttv
   where ttv.tt_variant_id = ln_tt_variant_id;

  l_ap_e := l_ts_end_dt;

  update ttb.tt_variant
     set action_period = ('['||l_ap_b||','||l_ap_e||']')::tsrange,
         tt_status_id =
         		case when l_ap_e < current_timestamp::date
        				then ttb.jf_tt_status_pkg$cn_arch_status_id ()
                	 when ('['||l_ap_b||','||l_ap_e||']')::tsrange @> current_timestamp::timestamp
                        then ttb.jf_tt_status_pkg$cn_active_status_id()
        			  else tt_status_id
        	    end
   where tt_variant_id = ln_tt_variant_id;
   
   if l_ap_e >= now()::date then
     
        select count(1)
        into ln_cnt
        from ttb.tt_check_pkg$get_set4action_date (ln_tt_variant_id::integer);
        
        if ln_cnt > 0 then
           raise exception 'Пересечение даты действия расписания.';
        end if;

   end if;

  return null;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-----------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$of_check_settings (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
    l_tt_variant_id 	ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
    ln_tt_var_id		ttb.tt_variant.tt_variant_id%type;
    ln_cnt				smallint;
    lt_res				text;
    lts_start_time		timestamp;
    lts_finish_time		timestamp;
    lts_min_fix_time	timestamp;
    lts_max_fix_time	timestamp;
    lt_non_valid_rnd	text;
    larrb_res			boolean[];
    cur_st_gr			record;
    lt_norm_bs			text;
    cur_sch_main		record;
    lb_res				boolean;
    
begin

  with t_inv as (
  select distinct rnd.code
     from ttb.tt_schedule sch 
     join ttb.tt_schedule_item sch_it on sch_it.tt_schedule_id = sch.tt_schedule_id
     join ttb.tt_action_group act_gr on act_gr.tt_action_group_id = sch_it.tt_action_group_id and not act_gr.sign_deleted
     join ttb.tt_variant tv on tv.tt_variant_id = act_gr.tt_variant_id
     join ttb.tt_action_type2group act2gr on act2gr.tt_action_group_id = act_gr.tt_action_group_id
     join ttb.tt_action_type tact on tact.tt_action_type_id = act2gr.tt_action_type_id
     join ttb.tt_at_round at_rnd on at_rnd.tt_action_type_id = tact.tt_action_type_id
     join ttb.tt_at_round_stop ars on ars.tt_at_round_id = at_rnd.tt_at_round_id
     join rts.stop_item2round sir on sir.stop_item2round_id = ars.stop_item2round_id 
     join rts.round rnd on rnd.round_id = sir.round_id
    where tv.tt_variant_id = l_tt_variant_id
      and (sir.sign_deleted or rnd.sign_deleted) 
  )  
  select string_agg(t_inv.code, ', ')
 	into lt_non_valid_rnd
    from t_inv;  
    
  if lt_non_valid_rnd is not null
  	then lt_res := coalesce(lt_res, '') || ' - В ГИС изменились данные по рейсам ' || lt_non_valid_rnd || ', задействованным в варианте расписания. Необходимо обновить данные.<br>';
  	return  jofl.jofl_util$cover_result('error', lt_res);
  end if; 

  select count(1)
  into ln_cnt
  from ttb.tt_calendar cl
  where cl.tt_variant_id = l_tt_variant_id;
  
  if ln_cnt = 0
   then lt_res := '- Должен быть заполнен хотя бы один тег календаря.<br>';
  end if;
  
  select count(1)
  into ln_cnt
  from ttb.tt_period p
  join ttb.tt_schedule sch on sch.tt_schedule_id = p.tt_schedule_id
  where sch.tt_variant_id = l_tt_variant_id;
  
  if ln_cnt = 0
   then lt_res := coalesce(lt_res, '') || ' - Должен быть заполнен хотя бы один период.<br>';
  end if;
  
  for cur_sch_main in (
	select tt_schedule_id
      from ttb.tt_schedule 
     where parent_schedule_id is null
       and tt_variant_id = l_tt_variant_id )
       
     loop
      
        with recursive  
         r as (
            select 1 as lvl
                  ,sch.tt_schedule_id
                  ,sch.tt_schedule_name
                  ,sch.parent_schedule_id
            from ttb.tt_schedule sch
            where sch.tt_schedule_id = cur_sch_main.tt_schedule_id

            union
            
            select lvl + 1 as lvl
                  ,sch.tt_schedule_id
                  ,r.tt_schedule_name || ' - ' || sch.tt_schedule_name
                  ,sch.parent_schedule_id
            from ttb.tt_schedule sch
            join r on sch.parent_schedule_id = r.tt_schedule_id
          )
          select ttb.tt_check_pkg$of_check_period_holes(array_agg(r.tt_schedule_id))
          into lb_res
          from r;
          
          if not lb_res 
           then lt_res := coalesce(lt_res, '') || ' - В периодах внутри одной последовательности не должно быть временных разрывов.<br>';
           exit;
          end if;
          
     end loop;
  
  /*
  for lrec_per in (
   select distinct ts.tt_schedule_id
          from ttb.tt_period tp 
          join ttb.tt_schedule ts on tp.tt_schedule_id = ts.tt_schedule_id --and ts.parent_schedule_id is null
          where ts.tt_variant_id =  l_tt_variant_id
  ) loop
      lb_res := ttb.tt_check_pkg$of_check_period_holes(l_tt_variant_id, lrec_per.tt_schedule_id);
      if not lb_res 
       then lt_res := coalesce(lt_res, '') || ' - В периодах внутри одной последовательности не должно быть временных разрывов.<br>';
       exit;
      end if;
  	end loop;
  */
  
  select count(1)
  into ln_cnt
  from ttb.tt_tr_period  trper
  join ttb.tt_tr tt_tr on tt_tr.tt_tr_id = trper.tt_tr_id
  join ttb.tt_period p on p.tt_period_id = trper.tt_period_id
  join ttb.tt_schedule sch on sch.tt_schedule_id = p.tt_schedule_id
  where sch.tt_variant_id = l_tt_variant_id;

  if ln_cnt = 0
   then lt_res := coalesce(lt_res, '') || ' - Должно быть определено хотя бы одно ТС хотя бы в одном периоде.<br>';
  end if;  
  
  select count(1)
  into ln_cnt
  from ttb.tt_action_group tac
  join ttb.tt_schedule ts on ts.tt_variant_id = tac.tt_variant_id
  join ttb.tt_schedule_item tsi on tsi.tt_schedule_id = ts.tt_schedule_id
  where tac.tt_variant_id = l_tt_variant_id;
  
  if ln_cnt = 0
   then lt_res := coalesce(lt_res, '') || ' - Должна быть определена хотя бы одна последовательность действий, в которую входят группы, в которых определены сами действия.<br>';
  end if;
  
  select count(1)
  into ln_cnt
  from ttb.tt_tr tt_tr 
  join ttb.tr_mode tm on tm.tr_mode_id = tt_tr.tr_mode_id
  					 and tm.break_count > 0
  where tt_tr.tt_variant_id = l_tt_variant_id;
  
  if ln_cnt > 0 then
  	  select count(1)
      into ln_cnt
      from ttb.tt_schedule ts 
      join ttb.tt_schedule_item tsi on tsi.tt_schedule_id = ts.tt_schedule_id
      join ttb.tt_action_type tat on tat.tt_variant_id = ts.tt_variant_id
      join ttb.action_type at on at.action_type_id = tat.action_type_id
      						 and ttb.action_type_pkg$is_break(at.action_type_id)
      where ts.tt_variant_id = l_tt_variant_id;
      
      if ln_cnt = 0
   		then lt_res := coalesce(lt_res, '') || ' - Если режим работы ТС предполагает перерыв на обед или ТО, то действия перерыв и/или ТО должны быть включены в последовательность действий.<br>';
  	  end if;
      
  end if;
  
  select count(1)
  into ln_cnt
  from ttb.tt_variant tv
  where tv.tt_variant_id = l_tt_variant_id
    and tv.is_all_tr_online;
  
  if ln_cnt > 0 then
  	  select count(1)
      into ln_cnt
      from ttb.tt_peak_hour tph
      where tph.tt_variant_id = l_tt_variant_id;
      
      if ln_cnt = 0
   		then lt_res := coalesce(lt_res, '') || ' - Если в варианте расписания указано, что необходимо выпустить все ТС в часы, то необходимо задать диапазон часов пик.<br>';
  	  end if;
      
  end if;

  select count(1)
  into ln_cnt
  from ttb.tt_variant tv
  where tv.tt_variant_id = l_tt_variant_id
    and tv.is_change_driver;
  
  if ln_cnt > 0 then
  	  select count(1)
      into ln_cnt
      from ttb.tt_driver td
      where td.tt_variant_id = l_tt_variant_id
        and td.is_сhanged_driver;
      
      if ln_cnt = 0
   		then lt_res := coalesce(lt_res, '') || ' - Если в варианте расписания указано наличие подменных водителей, то необходимо их указать.<br>';
  	  end if;
      
  end if;  
  
  for cur_st_gr in (
      with st_gr as (
          select
                 tact.action_type_id
                ,tact.tt_action_type_id
                ,tact.tt_variant_id
                ,at_rnd.round_id
                ,ttb.timetable_pkg$get_start_time( at_rnd.tt_at_round_id) as start_time
                ,ttb.timetable_pkg$get_finish_time( at_rnd.tt_at_round_id) as finish_time
                ,ttb.timetable_pkg$get_min_fix_time( at_rnd.tt_at_round_id) as min_fix_time
                ,ttb.timetable_pkg$get_max_fix_time( at_rnd.tt_at_round_id) as max_fix_time            
                ,sch_it.tt_schedule_item_id
                ,act2gr.tt_action_type2group_id
                ,sch_it.tt_schedule_id
                ,r.tr_type_id
                ,at_rnd.tt_at_round_id
                ,act2gr.order_num + sch_it.order_num * 1000 as order_num_action
              from   ttb.tt_schedule sch 
                join ttb.tt_schedule_item sch_it on sch_it.tt_schedule_id = sch.tt_schedule_id
                join ttb.tt_action_group act_gr on act_gr.tt_action_group_id = sch_it.tt_action_group_id and not act_gr.sign_deleted
                join ttb.tt_variant tv on tv.tt_variant_id = act_gr.tt_variant_id
                join rts.route_variant rv on rv.route_variant_id =tv.route_variant_id
                join rts.route r on r.route_id = rv.route_id
                join ttb.tt_action_type2group act2gr on act2gr.tt_action_group_id = act_gr.tt_action_group_id
                join ttb.tt_action_type tact on tact.tt_action_type_id = act2gr.tt_action_type_id
                join ttb.tt_at_round at_rnd on at_rnd.tt_action_type_id = tact.tt_action_type_id
                join ttb.tt_check_pkg$get_set4fst_si_sp(l_tt_variant_id) sss on sss.tt_at_round_id = at_rnd.tt_at_round_id /*Только рейсы с совпадающими первыми ОП и пунктами выпуска*/
              where 1 = 1 
                 and ttb.action_type_pkg$is_production_round(tact.action_type_id)
                 and sch.parent_schedule_id is null /*Для подчиненных последовательностей не проверять*/
                 and sch.tt_variant_id = l_tt_variant_id
            )
          select distinct  st_gr.tt_variant_id
                          --,first_value(st_gr.start_time) over (partition by st_gr.tt_variant_id order by st_gr.order_num_action) first_round_time
                          --,first_value(st_gr.finish_time) over (partition by st_gr.tt_variant_id order by st_gr.order_num_action desc) last_round_time
						  ,max(st_gr.start_time) over () ttv_first_round_time
                          ,max(st_gr.finish_time) over () ttv_last_round_time
                          ,max(st_gr.start_time) over (partition by st_gr.round_id) first_round_time
                          ,max(st_gr.finish_time) over (partition by st_gr.round_id) last_round_time
                          ,min(st_gr.min_fix_time) over (partition by st_gr.round_id) min_fix_time
                          ,max(st_gr.max_fix_time) over (partition by st_gr.round_id) max_fix_time
                    /* into ln_tt_var_id
                         ,lts_start_time
                         ,lts_finish_time
                         ,lts_min_fix_time
                         ,lts_max_fix_time*/
            from st_gr)
      loop
      
        if cur_st_gr.ttv_first_round_time is null and cur_st_gr.ttv_last_round_time is null 
          then lt_res := coalesce(lt_res, '') || ' - Хотя бы для одного производственного рейса маршрута должно быть определено время отправления на ОП с признаком "Первый рейс" или время отправления на ОП с признаком "Последний рейс".<br>';
        end if;
        
        if cur_st_gr.first_round_time is not null and cur_st_gr.min_fix_time < cur_st_gr.first_round_time
          then lt_res := coalesce(lt_res, '') || ' - Фиксированные времена не должны выходить за время первого рейса.<br>';
        end if;
        
        if cur_st_gr.last_round_time is not null and cur_st_gr.max_fix_time > cur_st_gr.last_round_time
          then lt_res := coalesce(lt_res, '') || ' - Фиксированные времена не должны выходить за время последнего рейса.<br>';
        end if;
      
      end loop; 
  
  /*      
  if lts_start_time is null and ln_tt_var_id is not null 
  	then lt_res := coalesce(lt_res, '') || ' - Для всех производственных рейсов маршрута, с которых начинается последовательность действий должно быть определено время отравления на ОП с признаком "Первый рейс".<br>';
  end if;
  
  if lts_finish_time is null and ln_tt_var_id is not null
    then lt_res := coalesce(lt_res, '') || ' - Для всех производственных рейсов маршрута, которыми оканчивается последовательность действий должно быть определено время отравления на ОП с признаком "Последний рейс".<br>';
  end if;
  */
  
  /*
  if lts_start_time is null and lts_finish_time is null 
  	then lt_res := coalesce(lt_res, '') || ' - Хотя бы для одного производственного рейса маршрута должно быть определено время отправления на ОП с признаком "Первый рейс" или время отправления на ОП с признаком "Последний рейс".<br>';
  end if;
  
  if lts_start_time is not null and lts_min_fix_time < lts_start_time
    then lt_res := coalesce(lt_res, '') || ' - Фиксированные времена не должны выходить за время первого рейса.<br>';
  end if;
  
  if lts_finish_time is not null and lts_max_fix_time > lts_finish_time
    then lt_res := coalesce(lt_res, '') || ' - Фиксированные времена не должны выходить за время последнего рейса.<br>';
  end if;
  */
  with q as 
  		( select min(tp.time_begin) min_per_bgn
                ,min(ttb.timetable_pkg$get_start_time(tar.tt_at_round_id )) min_rnd_bgn
          from ttb.tt_variant tv
          join ttb.tt_schedule ts on ts.tt_variant_id = tv.tt_variant_id
          join ttb.tt_schedule_item tsi on tsi.tt_schedule_id = ts.tt_schedule_id
          join ttb.tt_period tp on tp.tt_schedule_id = ts.tt_schedule_id
          join ttb.tt_tr_period t2p on t2p.tt_period_id = tp.tt_period_id
          join ttb.tt_tr tr on tr.tt_tr_id = t2p.tt_tr_id
                           and tr.tt_variant_id = tv.tt_variant_id
          join ttb.tt_action_group tag on tag.tt_action_group_id = tsi.tt_action_group_id
                                      and tag.tt_variant_id = tv.tt_variant_id 
          join ttb.tt_action_type2group at2g on at2g.tt_action_group_id = tag.tt_action_group_id
          join ttb.tt_action_type tat on tat.tt_action_type_id = at2g.tt_action_type_id
                                     and tat.tt_variant_id = tv.tt_variant_id 
          join ttb.tt_at_round tar on tar.tt_action_type_id = tat.tt_action_type_id           
          where ts.tt_variant_id = l_tt_variant_id
          )
    select count(1)
    into ln_cnt
    from q
    where q.min_rnd_bgn < min_per_bgn
    ;
 
  if ln_cnt > 0
   	then lt_res := coalesce(lt_res, '') || ' - Время первого рейса в последовательности должно быть больше или равно времени первого периода, для которого определено ТС.<br>';
  end if;
  
  ln_cnt := ttb.jf_tt_variant_pkg$of_check_norm_rnd(l_tt_variant_id);
  if ln_cnt > 0
   	then lt_res := coalesce(lt_res, '') || ' - Не для всех рейсов найдены нормативы.<br>';
  end if;
  
  lt_norm_bs := ttb.jf_tt_variant_pkg$of_check_norm_bs_txt(l_tt_variant_id);  
  if lt_norm_bs is not null 
  	then lt_res := coalesce(lt_res, '') || ' - Не найдены нормативы прохождения между ОП: ' || lt_norm_bs || '<br>';
  end if;
  /*  
  ln_cnt := ttb.jf_tt_variant_pkg$of_check_norm_bs(l_tt_variant_id);
  if ln_cnt > 0
   	then lt_res := coalesce(lt_res, '') || ' - Не для всех рейсов найдены нормативы прохождения между ОП.<br>';
  end if;
  */
  
  select count(1)
  into ln_cnt
  from ttb.tt_check_pkg$get_set_si4main_schi(l_tt_variant_id) sc
  where (sc.prev_vshi_ordnum <> sc.vshi_order_num and
   	     sc.prev_stop_item_id <> sc.stop_item_id)
     or (sc.first_stop_item <> sc.last_stop_item)
       ;
       
  if ln_cnt > 0
   	then lt_res := coalesce(lt_res, '') || ' - Не пройдена проверка на совпадение конечных пунктов внутри последовательностей.<br>';
  end if;        
       
  select count(1)
  into ln_cnt
  from ttb.tt_schedule sch
  where sch.tt_variant_id = l_tt_variant_id
    and not exists (select null
                    from ttb.tt_period tp
                    where tp.tt_schedule_id = sch.tt_schedule_id  
                    );
       
  if ln_cnt > 0
   	then lt_res := coalesce(lt_res, '') || ' - Не для всех последовательностей действий заполнены периоды.<br>';
  end if;
  
   select count(1)
   into ln_cnt
   from ttb.tt_schedule ts 
   join ttb.tt_period tp on tp.tt_schedule_id = ts.tt_schedule_id                    
   where ts.tt_variant_id = l_tt_variant_id
     and tp.time_begin >= tp.time_end;
            
  if ln_cnt > 0
   	then lt_res := coalesce(lt_res, '') || ' - Время начала в периодах должно быть строго больше времени окончания.<br>';
  end if;            
  
  select count(1)
  into ln_cnt 
      from ttb.tt_tr tt_tr
        join ttb.mode m on m.mode_id = tt_tr.tr_mode_id
        join core.depo2territory d2t on d2t.depo2territory_id = tt_tr.depo2territory_id
        join ttb.tt_tr_group ttg on ttg.tt_tr_group_id = tt_tr.tt_tr_group_id
        join ttb.tr_mode trm on trm.tr_mode_id = tt_tr.tr_mode_id
        join ttb.dr_shift ds on ds.mode_id = trm.tr_mode_id
	 join ttb.dr_mode dm on dm.dr_shift_id = ds.dr_shift_id          					
        join ttb.tt_action_type tat on tat.tt_variant_id = tt_tr.tt_variant_id
        							and tat.action_type_id = dm.action_type_id       
       where tt_tr.tt_variant_id = l_tt_variant_id
         and dm.action_type_id = ttb.action_type_pkg$dinner()
         and tat.plan_time > dm.time_max;
         
  if ln_cnt > 0
   	then lt_res := coalesce(lt_res, '') || ' - Планируемая продолжительность обеда больше максимально допустимой.<br>';
  end if;  
   
  select count(1)
  into ln_cnt
  from ttb.tt_check_pkg$get_set4fst_si_sp(l_tt_variant_id);
  
  if ln_cnt < 1
   	then lt_res := coalesce(lt_res, '') || ' - Пункт выхода не соответствует конечным пунктам основного рейса.<br>';
  end if; 
  
  with t as (  
  select ttv.ttv_name
        ,tag.tt_action_group_name
        ,tr.tt_tr_id
        ,tas.tt_at_stop_id
        ,count(ds.dr_shift_id) shift_cnt
  from ttb.tt_variant ttv
  join ttb.tt_schedule sch on sch.tt_variant_id = ttv.tt_variant_id
  join ttb.tt_period tp on tp.tt_schedule_id = sch.tt_schedule_id
  join ttb.tt_tr_period ttp on ttp.tt_period_id = tp.tt_period_id
  join ttb.tt_tr tr on tr.tt_tr_id = ttp.tt_tr_id
  join ttb.tr_mode tm on tm.tr_mode_id = tr.tr_mode_id
  join ttb.mode m on m.mode_id = tm.tr_mode_id
  join ttb.dr_shift ds on ds.mode_id = m.mode_id
  join ttb.tt_action_type tat on tat.tt_variant_id = ttv.tt_variant_id
                              and tat.action_type_id = ttb.action_type_pkg$change_driver ()
  join ttb.tt_action_group tag on tag.tt_variant_id = ttv.tt_variant_id
  join ttb.tt_action_type2group tat2g on tat2g.tt_action_type_id = tat.tt_action_type_id
                                     and tat2g.tt_action_group_id = tag.tt_action_group_id
  join ttb.tt_schedule_item schi on schi.tt_schedule_id = sch.tt_schedule_id
                                and schi.tt_action_group_id = tag.tt_action_group_id	                                
  left join ttb.tt_at_stop tas on tas.tt_action_type_id = tat.tt_action_type_id                              				                              
  where ttv.tt_variant_id = l_tt_variant_id
  group by ttv.ttv_name
          ,tag.tt_action_group_name
          ,tr.tt_tr_id
          ,tas.tt_at_stop_id)
  select count(1)
  into ln_cnt
  from t
  where t.tt_at_stop_id is null
    and t.shift_cnt > 1; 
    
  if ln_cnt > 0
   	then lt_res := coalesce(lt_res, '') || ' - Для ТС с режимом работы в нескольких смен необходимо указание пункта пересменки.<br>';
  end if; 
  
  
  if ttb.tt_check_pkg$of_check_sp_tech2prod(l_tt_variant_id) > 0
   	then lt_res := coalesce(lt_res, '') || ' - ОП технологического рейса не соответствуют конечным пунктам производственного рейса.<br>';
  end if;     
   
  if ttb.tt_check_pkg$of_check_times2perds(l_tt_variant_id) < 4
   	then lt_res := coalesce(lt_res, '') || ' - Времена отправления первого, последнего рейса, а также фиксированные времена должны попадать хотя бы в один период действия основной последовательности действий.<br>';
  end if;  
  
  if lt_res is not null then
    return  jofl.jofl_util$cover_result('error', lt_res);
  else
    return  jofl.jofl_util$cover_result('message', 'Предварительная проверка настроек прошла успешно');
  end if;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$of_create (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
  declare
      l_tt_variant_id 	ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
      l_res 			text;
      lt_check_res		text;
  begin

    l_res:=ttb.jf_tt_variant_pkg$of_check_settings(p_id_account, p_attr);

    if l_res like '%error%'
      then  return l_res;
    end if ;

    update ttb.tt_variant
    set tt_create_status_id = ttb.tt_create_status_pkg$queued()
    where tt_variant_id = l_tt_variant_id;


    --lt_check_res := ttb.jf_ttv_check_condition_pkg$of_start_ttv_check(p_id_account, p_attr);

    /*lt_check_res := replace(
                        replace(lt_check_res, '{"message": "', ''),
       				'" }', '');

    return  jofl.jofl_util$cover_result('message', 'Расписание сформировано.<br>' || lt_check_res );*/
    return null;

  end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

---------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$of_create_test (
  p_id_account numeric,
  p_attr text
)
  RETURNS text AS
$body$
declare
  l_tt_variant_id 	ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
  l_res 			text;
  lt_check_res		text;
begin

  l_res:=ttb.jf_tt_variant_pkg$of_check_settings(p_id_account, p_attr);

  if l_res like '%error%'
  then  return l_res;
  end if ;

  perform ttb.timetable1_pkg$clean_tt(l_tt_variant_id);
  perform ttb.timetable1_pkg$make_tt_variant(l_tt_variant_id);



  /*lt_check_res := ttb.jf_ttv_check_condition_pkg$of_start_ttv_check(p_id_account, p_attr);

  lt_check_res := replace(
                      replace(lt_check_res, '{"message": "', ''),
             '" }', '');

  return  jofl.jofl_util$cover_result('message', 'Расписание сформировано.<br>' || lt_check_res );*/
  return null;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

---------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$of_create_with_bn (
  p_id_account numeric,
  p_attr text
)
  RETURNS text AS
$body$
declare
  l_tt_variant_id ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
  l_res text;
begin

  l_res:=ttb.jf_tt_variant_pkg$of_check_settings(p_id_account , p_attr);
  if l_res like '%error%' then   return l_res; end if ;


  PERFORM ttb.timetable_test_pkg$clean_tt(l_tt_variant_id);
  PERFORM ttb.timetable_test_pkg$make_tt_variant(l_tt_variant_id, false);

  return  jofl.jofl_util$cover_result('message', 'Расписание сформировано.');


end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

-------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$insert_available_action_group (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
    ln_tt_variant_id  ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
begin

    perform ttb.jf_tt_action_group_pkg$of_refresh (p_id_account, p_attr);

    return null;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

----------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$of_actualize_ttv_status (
)
RETURNS text AS
$body$
declare
  ttv_cur record;
begin
  
  for ttv_cur in 
  		(select tt_variant_id
         from ttb.tt_variant
         where tt_status_id in ( ttb.jf_tt_status_pkg$cn_active_status_id()
         						,ttb.jf_tt_status_pkg$cn_approved_status_id() )        
        
        ) loop
        
        	perform ttb.jf_tt_variant_pkg$of_actualize_ttv_status ( ttv_cur.tt_variant_id );
        
        end loop;

  return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$of_actualize_ttv_status (
  p_tt_variant_id integer
)
RETURNS text AS
$body$
declare
  ln_cnt 	smallint;
  lt_res	text;
begin
  select count(1)
  into ln_cnt
  from ttb.tt_check_pkg$get_set4action_date (p_tt_variant_id);
  
  if ln_cnt > 0 then
     lt_res := 'ERR_ACTIVATE';
  else
    update ttb.tt_variant
    set tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id()
    where tt_variant_id = p_tt_variant_id 
      and tt_status_id = ttb.jf_tt_status_pkg$cn_approved_status_id()
      and lower(action_period) <= current_timestamp;
     
  end if;

  update ttb.tt_variant
  set tt_status_id = ttb.jf_tt_status_pkg$cn_arch_status_id()
  where tt_variant_id = p_tt_variant_id 
    and tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id()
    and upper(action_period) < current_timestamp;

  return lt_res;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

----------------------------------------------------------------------------------------------
--удалить выходы
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$of_clear (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
  l_tt_variant_id  ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
begin

     perform ttb.timetable_pkg$clean_tt(l_tt_variant_id);
     perform ttb.jf_ttv_check_condition_pkg$of_close_prev_check_cond(l_tt_variant_id, null::integer);

      update ttb.tt_variant
      set tt_create_status_id = ttb.tt_create_status_pkg$new()
      where tt_variant_id = l_tt_variant_id;

  return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$insert_default_schedule (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
    ln_tt_variant_id  	ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
    lt_tt_schedule_name	text := 'Стандартное расписание с обедом на конечном пункте';
begin
    
    with 
        ins_shedule as (
              insert into ttb.tt_schedule
                  (tt_schedule_name
                  ,tt_variant_id)
              select lt_tt_schedule_name
                    ,ln_tt_variant_id
              returning *
        )
        insert into ttb.tt_schedule_item
                  (tt_schedule_id
                  ,tt_action_group_id
                  ,repeat_time
                  ,order_num)
        select ins.tt_schedule_id
              ,tag.tt_action_group_id
              ,def.repeat_time
              ,def.order_num 
        from ttb.ref_default_action_gr def
        join ttb.ref_action_group rag on rag.ref_action_group_id = def.ref_action_group_id 
        join ttb.tt_action_group tag on tag.tt_action_group_name = rag.gr_name
        join ins_shedule ins on 1 = 1
        where tag.tt_variant_id = ln_tt_variant_id; 

    return null;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$of_copy (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
   l_r 					ttb.tt_variant%rowtype;
   ln_ttv_num			smallint;
   lt_route_num 		text;
   lt_tt_status_name 	text;
   sch_cur				record;
   ln_new_tt_variant_id ttb.tt_variant.tt_variant_id%type;
   tr_cur				record;
   l_r_tt_tr			ttb.tt_tr%rowtype;
begin

  l_r := ttb.jf_tt_variant_pkg$attr_to_rowtype(p_attr);
  l_r.tt_status_id := 1;

  select count(1) + 1
  into ln_ttv_num
  from ttb.tt_variant ttv
  where ttv.route_variant_id = l_r.route_variant_id;
  
  select r.route_num
  into lt_route_num
  from rts.route_variant rv 
  join rts.route r on rv.route_id = r.route_id and rv.route_variant_id = l_r.route_variant_id;

  l_r.ttv_comment	:= coalesce(l_r.ttv_name, '') || ' (копия)';  
  l_r.ttv_name 		:= ln_ttv_num::text || '-' || lt_route_num::text || '/' || to_char(lower(l_r.action_period), 'DDMMYYYY');
  l_r.tt_status_id 	:= ttb.tt_status_pkg$in_work();
  
  select tt_status_name
  into lt_tt_status_name
  from ttb.tt_status
  where tt_status_id = l_r.tt_status_id; 

  insert into ttb.tt_variant (norm_id
                             ,route_variant_id
                             ,tt_status_id
                             ,ttv_name
                             ,ttv_comment
                             ,action_period
                             ,is_all_tr_online
                             ,is_change_driver)
                      values (l_r.norm_id
                             ,l_r.route_variant_id  
                             ,l_r.tt_status_id
                             ,l_r.ttv_name
                             ,l_r.ttv_comment
                             ,l_r.action_period
                             ,l_r.is_all_tr_online
                             ,l_r.is_change_driver)
  returning tt_variant_id 
  into ln_new_tt_variant_id; 
      
  with ins_calenadar as ( /*Календарь*/
      		insert into ttb.tt_calendar 
            	(calendar_tag_id
				,tt_variant_id
				,is_include)
            select ttc.calendar_tag_id
            	  ,ln_new_tt_variant_id
                  ,ttc.is_include
            from ttb.tt_calendar ttc
            where ttc.tt_variant_id = l_r.tt_variant_id 
      returning *
      )
      ,ins_peak_hour as ( /*Часы пик*/
      		insert into ttb.tt_peak_hour
            	(tt_variant_id
                ,time_begin
                ,time_end
                ,peak_hour_name)
            select   ln_new_tt_variant_id
            		,tph.time_begin
                    ,tph.time_end
                    ,tph.peak_hour_name
            from ttb.tt_peak_hour tph
            where tph.tt_variant_id = l_r.tt_variant_id 
      returning *
      )
      ,ins_tt_action_type as ( /*Типы действий*/
      		insert into ttb.tt_action_type
            	(tt_variant_id
                ,action_type_id
                ,plan_time
                ,r_code)
            select ln_new_tt_variant_id
            	  ,ttat.action_type_id
                  ,ttat.plan_time
                  ,ttat.r_code
            from ttb.tt_action_type ttat
            where ttat.tt_variant_id = l_r.tt_variant_id 
      returning *     
      )
      ,ins_tt_at_round as ( /*Рейсы*/
      		insert into ttb.tt_at_round
            	(round_id
                ,tt_action_type_id
                ,depo2territory_id)
            select tatr.round_id
            	  ,itat.tt_action_type_id
                  ,tatr.depo2territory_id
            from ttb.tt_action_type tat 
            join ttb.tt_at_round tatr on tat.tt_action_type_id = tatr.tt_action_type_id
            join ins_tt_action_type itat on itat.action_type_id = tat.action_type_id
            							and tat.r_code = itat.r_code
            join rts.round rnd on rnd.code = tat.r_code            				
            				  and rnd.round_id = tatr.round_id
            where tat.tt_variant_id = l_r.tt_variant_id  
      returning *
      )
      ,ins_tt_at_round_stop as ( /*Остановки в рейсе*/
      		insert into ttb.tt_at_round_stop
            	(stop_item2round_id
				,tt_at_round_id)
            select tars.stop_item2round_id
            	  ,itar.tt_at_round_id
            from ttb.tt_action_type tat 
            join ttb.tt_at_round tatr on tat.tt_action_type_id = tatr.tt_action_type_id
            join ttb.tt_at_round_stop tars on tars.tt_at_round_id = tatr.tt_at_round_id
            join ins_tt_at_round itar on itar.round_id = tatr.round_id
            where tat.tt_variant_id = l_r.tt_variant_id 
      returning *
      )
      ,ins_atgrs_time as ( /*Времена отправления*/
      		insert into ttb.atgrs_time
            	(tt_at_round_stop_id
                ,start_time
                ,fix_arrival_type_id)            
            select itars.tt_at_round_stop_id
            	  ,atgt.start_time
                  ,atgt.fix_arrival_type_id
            from ttb.tt_action_type tat 
            join ttb.tt_at_round tatr on tat.tt_action_type_id = tatr.tt_action_type_id
            join ttb.tt_at_round_stop tars on tars.tt_at_round_id = tatr.tt_at_round_id
            join ins_tt_at_round_stop itars on itars.stop_item2round_id = tars.stop_item2round_id
            join ttb.atgrs_time atgt on atgt.tt_at_round_stop_id = tars.tt_at_round_stop_id          
            where tat.tt_variant_id = l_r.tt_variant_id                   
      )
      ,ins_tt_at_stop as ( /*Перерывы*/
      		insert into ttb.tt_at_stop
            	(tt_action_type_id
                ,stop_item2round_id
                ,depo2territory_id
                ,time_begin)
            select itat.tt_action_type_id
            	  ,tas.stop_item2round_id
                  ,tas.depo2territory_id
                  ,tas.time_begin
            from ttb.tt_action_type tat 
            join ttb.tt_at_stop tas on tas.tt_action_type_id = tat.tt_action_type_id
            join ins_tt_action_type itat on itat.action_type_id = tat.action_type_id
            where tat.tt_variant_id = l_r.tt_variant_id 
      returning *
      )
      ,ins_st_int_aligment as ( /*ОП выравнивания*/
      		insert into ttb.stop_interval_alignment
            	(tt_at_stop_id)
            select itas.tt_at_stop_id
            from ttb.tt_action_type tat 
            join ttb.tt_at_stop tas on tas.tt_action_type_id = tat.tt_action_type_id
            join ins_tt_action_type itat on itat.action_type_id = tat.action_type_id
            join ttb.stop_interval_alignment sia on sia.tt_at_stop_id = tas.tt_at_stop_id   
            join ins_tt_at_stop itas on itas.tt_action_type_id = itat.tt_action_type_id
            						and itas.stop_item2round_id = tas.stop_item2round_id
                                    and (itas.depo2territory_id = tas.depo2territory_id or (itas.depo2territory_id is null and tas.depo2territory_id is null))
                                    and (itas.time_begin = tas.time_begin or (itas.time_begin is null and tas.time_begin is null))
            where tat.tt_variant_id = l_r.tt_variant_id         
      )         
      ,ins_tt_action_group as ( /*Группы действий*/
      		insert into ttb.tt_action_group
            	(tt_variant_id
                ,tt_action_group_name
                ,tt_action_group_code
                ,ref_group_kind_id)
            select   ln_new_tt_variant_id
            		,tag.tt_action_group_name
                    ,tag.tt_action_group_code
                    ,tag.ref_group_kind_id
            from ttb.tt_action_group tag 
            where tag.tt_variant_id = l_r.tt_variant_id 
            and not tag.sign_deleted
      returning *   
      )
      ,ins_tt_action_type2gr as ( /*Типы действий, объединенные в группу*/
      		insert into ttb.tt_action_type2group
            	(tt_action_group_id
                ,tt_action_type_id
                ,order_num
                ,plan_time)   
            select itag.tt_action_group_id
            	  ,itat.tt_action_type_id
                  ,at2g.order_num
                  ,at2g.plan_time
            from ttb.tt_action_group tag 
            join ttb.tt_action_type2group at2g on tag.tt_action_group_id = at2g.tt_action_group_id  
            join ttb.tt_action_type tat on tat.tt_action_type_id = at2g.tt_action_type_id
            join ins_tt_action_group itag on tag.tt_action_group_name = itag.tt_action_group_name
            join ins_tt_action_type itat on itat.action_type_id = tat.action_type_id   
            							and coalesce(tat.r_code, 'no_r_code') = coalesce(itat.r_code, 'no_r_code')
            where tag.tt_variant_id = l_r.tt_variant_id
            
      )
      ,ins_tt_schedule as (/*Последовательность действий шапка*/
      		insert into ttb.tt_schedule
            	(tt_schedule_name
                ,tt_variant_id
                ,parent_schedule_id)
            select ts.tt_schedule_name
            	  ,ln_new_tt_variant_id
                  ,ts.parent_schedule_id
            from ttb.tt_schedule ts
            where ts.tt_variant_id = l_r.tt_variant_id   
	  returning *   
      )
      ,ins_tt_schedule_item as (/*Последовательность действий строки*/
      	      	insert into ttb.tt_schedule_item 
            	(tt_schedule_id
                ,tt_action_group_id
                ,order_num
                ,is_required
                ,repeat_time
                ,item_type)
            select its.tt_schedule_id
            	  --,tsi.tt_action_group_id --д.б. новый из ins_tt_action_group
                  ,itag.tt_action_group_id -- new
                  ,tsi.order_num
                  ,tsi.is_required
                  ,tsi.repeat_time
                  ,tsi.item_type
            from ttb.tt_schedule ts
            join ins_tt_schedule its on ts.tt_schedule_name = its.tt_schedule_name
            join ttb.tt_action_group tag on tag.tt_variant_id = ts.tt_variant_id
            join ins_tt_action_group itag on itag.tt_action_group_name = tag.tt_action_group_name
            join ttb.tt_schedule_item tsi on tsi.tt_schedule_id = ts.tt_schedule_id
            						     and tag.tt_action_group_id = tsi.tt_action_group_id
            where ts.tt_variant_id = l_r.tt_variant_id       
      )
      ,ins_tt_period as (/*Периоды*/
      		insert into ttb.tt_period
            	(time_begin
                ,time_end
                ,movement_type_id
                ,limit_time
                ,interval_time
                ,tt_schedule_id
                ,out_cnt)
             select  tp.time_begin
            		,tp.time_end
                    ,tp.movement_type_id
                    ,tp.limit_time
                    ,tp.interval_time
                    ,its.tt_schedule_id
                    ,tp.out_cnt
            from ttb.tt_schedule ts
            join ins_tt_schedule its on ts.tt_schedule_name = its.tt_schedule_name
            join ttb.tt_period tp on tp.tt_schedule_id = ts.tt_schedule_id
			where ts.tt_variant_id = l_r.tt_variant_id 
	  returning *
      )
      ,ins_tt_change_time as (/*Время на пересменку*/
      		insert into ttb.tt_change_time
            	(tt_variant_id
                ,tr_capacity_id
                ,change_time)
            select   ln_new_tt_variant_id
            		,tct.tr_capacity_id
                    ,tct.change_time
            from ttb.tt_change_time tct
            where tct.tt_variant_id = l_r.tt_variant_id      
      )
      ,ins_tt_driver as (--Подменные водители
      		insert into ttb.tt_driver
            	(dr_shift_id
                ,is_сhanged_driver
                ,tt_variant_id
                ,time_begin
                ,time_end
                ,driver_name)
            select td.dr_shift_id
            	  ,td.is_сhanged_driver
                  ,ln_new_tt_variant_id
                  ,td.time_begin
                  ,td.time_end
                  ,td.driver_name
             from ttb.tt_driver td
            where td.tt_variant_id = l_r.tt_variant_id    
			  and td.is_сhanged_driver			
      )
      ,ins_ttv_check_condition as (/*Проверка*/
      		insert into ttb.ttv_check_condition
            	(tt_variant_id
                ,tt_check_condition_id
                ,is_doable
                ,check_result
                ,is_active)
            select   ln_new_tt_variant_id
            		,ttvcc.tt_check_condition_id
                    ,ttvcc.is_doable
                    ,ttb.jf_ttv_check_condition_pkg$cn_not_performed ()
                    ,ttvcc.is_active
            from ttb.ttv_check_condition ttvcc 
            where ttvcc.tt_variant_id = l_r.tt_variant_id 
            and not ttvcc.sign_deleted 
      returning *      
      )
     /*Параметры проверок*/
      		insert into ttb.ttv_check_parm
            	(tt_check_parm_id
                ,parm_value
                ,ttv_check_condition_id)
            select tcp.tt_check_parm_id
            	  ,tcp.parm_value
                  ,itcc.ttv_check_condition_id
            from ttb.ttv_check_parm tcp
            join ttb.ttv_check_condition tcc on tcp.ttv_check_condition_id = tcc.ttv_check_condition_id
            join ins_ttv_check_condition itcc on itcc.tt_check_condition_id = tcc.tt_check_condition_id
            where tcc.tt_variant_id = l_r.tt_variant_id  
              and not tcp.sign_deleted  
	 ;
  
  /*ТС, группы ТС*/   
  for tr_cur in (
			select tt_tr.tr_mode_id
				  ,tt_tr.depo2territory_id
				  ,tt_tr.tr_capacity_id
				  ,null::smallint as tt_tr_order_num /*ttrp.tt_tr_order_num*/
				  ,tt_tr.is_second_car
                  ,ttg.tt_tr_group_prior_id
                  ,count(1) cnt_in_gr
       		from ttb.tt_tr tt_tr
            join ttb.tt_tr_group ttg on ttg.tt_tr_group_id = tt_tr.tt_tr_group_id
            where tt_tr.tt_variant_id = l_r.tt_variant_id
            group by tt_tr.tr_mode_id
				  ,tt_tr.depo2territory_id
				  ,tt_tr.tr_capacity_id
				  ,tt_tr.is_second_car
                  ,ttg.tt_tr_group_prior_id
	 ) loop
     
        insert into ttb.tt_tr_group (tt_tr_group_prior_id) 
        select tr_cur.tt_tr_group_prior_id
        returning tt_tr_group_id
        into l_r_tt_tr.tt_tr_group_id;
       
        l_r_tt_tr.tt_variant_id		:= ln_new_tt_variant_id;
        l_r_tt_tr.tr_mode_id		:= tr_cur.tr_mode_id;
        l_r_tt_tr.depo2territory_id	:= tr_cur.depo2territory_id;
        l_r_tt_tr.tr_capacity_id 	:= tr_cur.tr_capacity_id; 
        l_r_tt_tr.is_second_car		:= tr_cur.is_second_car; 

       
       perform ttb.jf_tt_tr_group_pkg$of_insert_tt_tr ( p_id_account,
                                                        l_r_tt_tr,
                                                        tr_cur.cnt_in_gr::smallint
                                                       );
													   
       perform ttb.jf_tt_driver_pkg$of_insert_dr_tr ( p_id_account,  
                                                      ln_new_tt_variant_id,  
                                                      l_r_tt_tr.tt_tr_group_id
                                                      );													   
     
     end loop;
     

  /*Выходы в периодах*/    
  for sch_cur in (
  	select sch.tt_schedule_id
    from ttb.tt_schedule sch
    where sch.tt_variant_id = ln_new_tt_variant_id
  ) loop
  
      perform ttb.jf_tt_schedule_pkg$of_create_out (
                  p_id_account,
                  '{"tt_variant_id":"' || ln_new_tt_variant_id || '","tt_schedule_id":"' || sch_cur.tt_schedule_id || '"}'
      );
      
  end loop;
  
  perform ttb.jf_tt_variant_pkg$of_copy_out (ln_new_tt_variant_id, l_r.tt_variant_id);
  
  perform ttb.jf_tt_variant_pkg$of_copy_dr_action (ln_new_tt_variant_id, l_r.tt_variant_id);
  
    /*обновляем иерархию в последовательностях действий*/
  with parent_sch as (
    select tsc.tt_schedule_name, tsp.tt_schedule_name as par_schedule_name
    from ttb.tt_schedule tsc
    join ttb.tt_schedule tsp on tsp.tt_schedule_id = tsc.parent_schedule_id
    where tsc.tt_variant_id = l_r.tt_variant_id 
  )
  update ttb.tt_schedule tso
  set parent_schedule_id = 
      (select sch_ins.tt_schedule_id
      from ttb.tt_schedule sch_ins 
      join parent_sch ps on ps.par_schedule_name = sch_ins.tt_schedule_name
      where sch_ins.tt_variant_id = ln_new_tt_variant_id 
        and ps.tt_schedule_name = tso.tt_schedule_name)
  where tso.tt_variant_id = ln_new_tt_variant_id;

  return jofl.jofl_util$cover_result('Создан вариант расписания:<b>' || l_r.ttv_name || '</b><br>Статус:<b>'|| lt_tt_status_name ||'</b>');

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$of_copy_out (
  p_tt_variant_id_new integer,
  p_tt_variant_id integer
)
RETURNS text AS
$body$
declare
begin
  with ins_tt_out as (
		insert into ttb.tt_out
        	(tt_variant_id
            ,tr_capacity_id
            ,mode_id
            ,tt_out_num
            )
        select p_tt_variant_id_new
        	  ,out.tr_capacity_id
              ,out.mode_id
              ,out.tt_out_num
        from ttb.tt_out out
        where not out.sign_deleted
          and out.tt_variant_id = p_tt_variant_id
	returning *
	)
    ,old_o2tr as 
      (    
          select * 
          from ttb.tt_out2tt_tr o2tr
          join ttb.tt_tr ttr on o2tr.tt_tr_id = ttr.tt_tr_id
          join ttb.tt_tr_period ttp on o2tr.tt_tr_id = ttr.tt_tr_id
                                   and ttp.tt_tr_id = ttr.tt_tr_id
          join ttb.tt_out o on o.tt_out_id = o2tr.tt_out_id
          where ttr.tt_variant_id = p_tt_variant_id
      )
    ,ins_tt_out2tt_tr as (
        insert into ttb.tt_out2tt_tr
              (tt_tr_id
              ,tt_out_id
              )
        select ttr.tt_tr_id
              ,o.tt_out_id 
          from ttb.tt_tr ttr 
          join ttb.tt_tr_period ttp on ttp.tt_tr_id = ttr.tt_tr_id
          join old_o2tr on old_o2tr.tt_tr_order_num = ttp.tt_tr_order_num    
          join ins_tt_out o on o.tt_variant_id = ttr.tt_variant_id
                           and o.tt_out_num = old_o2tr.tt_out_num 				
        where ttr.tt_variant_id = p_tt_variant_id_new
		group by ttr.tt_tr_id
              	,o.tt_out_id 
		on conflict do nothing
	)
    ,ins_tt_action as (
    	insert into ttb.tt_action
        	(tt_out_id
            ,action_dur
            ,dt_action
            ,action_type_id
            ,tt_action_group_id
            ,action_gr_id
            ,parent_tt_action_id
            ,driver_id)
        select iout.tt_out_id
        	  ,ta.action_dur
              ,ta.dt_action
              ,ta.action_type_id
              ,ta.tt_action_group_id
              ,ta.action_gr_id
              ,ta.parent_tt_action_id
              ,ta.driver_id
        from ttb.tt_action ta 
        join ttb.tt_out out on out.tt_out_id = ta.tt_out_id
        join ins_tt_out iout on iout.tt_out_num = out.tt_out_num
        where not ta.sign_deleted
          and out.tt_variant_id = p_tt_variant_id
    returning *
	)
    ,ins_tt_action_item as (
    	insert into ttb.tt_action_item
        	(tt_action_id
            ,time_begin
            ,time_end
            ,stop_item2round_id
            ,action_type_id
            ,dr_shift_id
            ,driver_id)
        select ita.tt_action_id
        	  ,tai.time_begin
              ,tai.time_end
              ,tai.stop_item2round_id
              ,tai.action_type_id
              ,tai.dr_shift_id
              ,tai.driver_id
        from ttb.tt_action_item tai
        join ttb.tt_action ta on ta.tt_action_id = tai.tt_action_id
        join ttb.tt_out out on out.tt_out_id = ta.tt_out_id
        join ins_tt_out iout on iout.tt_out_num = out.tt_out_num
        join ins_tt_action ita on ita.tt_out_id = iout.tt_out_id 
        					  and ita.action_type_id = ta.action_type_id  
                              and ita.action_gr_id = ta.action_gr_id
                              and ita.tt_action_group_id = ta.tt_action_group_id
                              and ita.action_dur = ta.action_dur
         where not tai.sign_deleted
          and out.tt_variant_id = p_tt_variant_id
    )
    insert into ttb.tt_action_round
            	(tt_action_id
                ,round_id
                ,round_status_id
                /*,packet_id_begin
                ,packet_id_end
                ,time_packet_begin
                ,time_packet_end
                ,first_graph_section
                ,last_graph_section
                ,time_fact_begin
                ,time_fact_end*/)
      select ita.tt_action_id
    		,tar.round_id
            ,tar.round_status_id
            /*,tar.packet_id_begin
            ,tar.packet_id_end
            ,tar.time_packet_begin 
            ,tar.time_packet_end
            ,tar.first_graph_section
            ,tar.last_graph_section
            ,tar.time_fact_begin
            ,tar.time_fact_end*/
    from ttb.tt_action ta 
    join ttb.tt_out out on out.tt_out_id = ta.tt_out_id
    join ttb.tt_action_round tar on tar.tt_action_id = ta.tt_action_id
    join ins_tt_action ita on ita.action_type_id = ta.action_type_id  
    					  and ita.action_gr_id = ta.action_gr_id
                          and ita.tt_action_group_id = ta.tt_action_group_id
                          and ita.action_dur = ta.action_dur
    where not ta.sign_deleted
      and out.tt_variant_id = p_tt_variant_id
    group by ita.tt_action_id
    		,tar.round_id
            ,tar.round_status_id
	;
  
  return null;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$get_text(p_tt_vatiant int) RETURNS text
	LANGUAGE sql
AS $$
select route.route_num
from ttb.tt_variant
join rts.route_variant on tt_variant.route_variant_id = route_variant.route_variant_id
join rts.route on route_variant.route_id = route.route_id
where tt_variant_id = p_tt_vatiant;
$$;

------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$of_check_norm_bs (
  p_tt_variant_id integer
)
RETURNS smallint AS
$body$
declare
  ln_cnt 	smallint;
begin
  select count(1)
  into ln_cnt
  from (with tv_tr as (
          select tr.tr_capacity_id
                ,tp.time_begin--::time
                ,tp.time_end--::time
            from ttb.tt_variant tv
            join ttb.tt_schedule sch on sch.tt_variant_id = tv.tt_variant_id
            join ttb.tt_period tp on tp.tt_schedule_id = sch.tt_schedule_id
            join ttb.tt_tr_period trp on trp.tt_period_id = tp.tt_period_id
            join ttb.tt_tr tr on tr.tt_tr_id = trp.tt_tr_id 
                             and tr.tt_variant_id = tv.tt_variant_id 
            where 1 = 1 
            and tv.tt_variant_id = p_tt_variant_id        
            group by tr.tr_capacity_id
                    ,tp.time_begin--::time
                    ,tp.time_end--::time
            )
        ,tv_stops as (
          select sir.round_id
                ,lag(s.name, 1, s.name) over (partition by sir.round_id order by sir.order_num) prev_stop_name
                ,s.name stop_name 
                ,lag(si.stop_item_id, 1, si.stop_item_id) over (partition by sir.round_id order by sir.order_num) prev_stop_item_id
                ,si.stop_item_id   
         	from ttb.tt_at_round_stop trs         	
            join rts.stop_item2round sir on sir.stop_item2round_id = trs.stop_item2round_id
                                        and not sir.sign_deleted
            join rts.stop_item si on si.stop_item_id = sir.stop_item_id
                                 and not si.sign_deleted
            join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
                                    and not sl.sign_deleted
            join rts.stop s on s.stop_id = sl.stop_id
                           and not s.sign_deleted
            join ttb.tt_at_round ar on ar.tt_at_round_id = trs.tt_at_round_id
            join ttb.tt_action_type act on act.tt_action_type_id = ar.tt_action_type_id  
            join rts.round rnd on rnd.round_id = ar.round_id and not rnd.sign_deleted
            join ttb.tt_schedule sch on sch.tt_variant_id = act.tt_variant_id
            join ttb.tt_schedule_item schi on schi.tt_schedule_id = sch.tt_schedule_id
            join ttb.tt_action_group tag on tag.tt_action_group_id = schi.tt_action_group_id
            join ttb.tt_action_type2group at2g on at2g.tt_action_group_id = tag.tt_action_group_id
                                                   and at2g.tt_action_type_id = act.tt_action_type_id
            where act.tt_variant_id = p_tt_variant_id 
             ) 
       /*,perds as (
                   select hr.hr as hr_set
                            from generate_series
                            ('01.01.1900'::date + '00:00:00'::interval
                            ,'01.01.1900'::date + '23:00:00'::interval
                            ,'1 hour'::interval) hr
             )*/
        ,tv_stop_capacity as (
          select distinct tv_tr.tr_capacity_id
                         ,tv_tr.time_begin
                         ,tv_tr.time_end
                         --,perds.hr_set as hr_set_from
                         --,perds.hr_set + make_interval(hours => 1) as hr_set_to
                         ,tv_stops.round_id
                         ,tv_stops.prev_stop_name
                         ,tv_stops.stop_name 
                         ,tv_stops.prev_stop_item_id
                         ,tv_stops.stop_item_id  
          from tv_tr 
          --join perds on tsrange(tv_tr.time_begin, tv_tr.time_end) @> perds.hr_set
          join tv_stops on 1 = 1    
             ) 
        ,t_norm_bs as ( 
             select  nbs.stop_item_1_id
                    ,nbs.stop_item_2_id 
                    ,nbs.between_stop_dur
                    ,nbs.hour_from
                    ,nbs.hour_to
                    ,nbs.tr_type_id
                    ,nbs.tr_capacity_id
                    ,ttv.tt_variant_id
                    ,nbs.norm_between_stop_id
              from ttb.tt_variant ttv
              join ttb.norm n on n.norm_id = ttv.norm_id 
              join ttb.norm_between_stop nbs on nbs.norm_id = n.norm_id
              where ttv.tt_variant_id = p_tt_variant_id
              )
          select tsc.* 
          from tv_stop_capacity tsc
          where not exists (select null
                              from t_norm_bs tnbs 
                             where tnbs.stop_item_2_id = tsc.stop_item_id
                               and tnbs.stop_item_1_id = tsc.prev_stop_item_id
                               and tnbs.tr_capacity_id = tsc.tr_capacity_id 
                            /* and tnbs.hour_from = tsc.hr_set_from::time
                               and tnbs.hour_to = tsc.hr_set_to::time */
                            )						 
            and tsc.stop_item_id <> tsc.prev_stop_item_id
            limit 1) t;

  return ln_cnt;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$of_check_norm_rnd (
  p_tt_variant_id integer
)
RETURNS smallint AS
$body$
declare
  ln_cnt 	smallint;
begin
  select count(1)
  into ln_cnt
  from 
      (with tv_rnd as (
 		select tar.round_id
        	  ,rnd.code
         from ttb.tt_variant ttv
         join ttb.tt_action_type tat on ttv.tt_variant_id = tat.tt_variant_id
         join ttb.tt_at_round tar on tar.tt_action_type_id = tat.tt_action_type_id 
         join rts.round rnd on rnd.round_id = tar.round_id and not rnd.sign_deleted
         join ttb.tt_schedule sch on sch.tt_variant_id = ttv.tt_variant_id
         join ttb.tt_schedule_item schi on schi.tt_schedule_id = sch.tt_schedule_id
         join ttb.tt_action_group tag on tag.tt_action_group_id = schi.tt_action_group_id
         join ttb.tt_action_type2group at2g on at2g.tt_action_group_id = tag.tt_action_group_id
         								   and at2g.tt_action_type_id = tat.tt_action_type_id

         where ttv.tt_variant_id = p_tt_variant_id         
         group by tar.round_id,rnd.code
          )
          ,tv_tr as (
          select tr.tr_capacity_id
          from ttb.tt_variant tv
          join ttb.tt_schedule sch on sch.tt_variant_id = tv.tt_variant_id
          join ttb.tt_period tp on tp.tt_schedule_id = sch.tt_schedule_id
          join ttb.tt_tr_period trp on trp.tt_period_id = tp.tt_period_id
          join ttb.tt_tr tr on tr.tt_tr_id = trp.tt_tr_id 
                           and tr.tt_variant_id = tv.tt_variant_id 
          where 1 = 1 
          and tv.tt_variant_id = p_tt_variant_id        
          group by tr.tr_capacity_id
          )
          ,tv_tr_rnd as (
          select *
          from tv_rnd
          join tv_tr on 1 = 1
          ) --select * from tv_tr_rnd
          ,tv_norm as (
          select nr.round_id
                ,nr.tr_capacity_id
         from ttb.tt_variant ttv
         join ttb.norm n on n.norm_id = ttv.norm_id 
         join ttb.norm_round nr on nr.norm_id = n.norm_id
         where ttv.tt_variant_id = p_tt_variant_id 
         group by nr.round_id 
                 ,nr.tr_capacity_id  
          )
         select tv_tr_rnd.*
           from tv_tr_rnd
         where not exists (select null
                          from tv_norm
                          where tv_tr_rnd.round_id = tv_norm.round_id
                          and tv_tr_rnd.tr_capacity_id = tv_norm.tr_capacity_id)
         limit 1)t;

  return ln_cnt;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

-----------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$of_get_policy (
  p_tt_variant_id numeric
)
RETURNS boolean AS
$body$
declare
  ln_tt_status_id			smallint;  
  ln_parent_tt_variant_id 	ttb.tt_variant.parent_tt_variant_id%type; 
begin

  select tv.tt_status_id
  		,tv.parent_tt_variant_id
  into ln_tt_status_id 
  	  ,ln_parent_tt_variant_id
  from ttb.tt_variant tv
  where tv.tt_variant_id = p_tt_variant_id
  ;
  
  if ln_parent_tt_variant_id is null then
    return coalesce(ln_tt_status_id, -1)  
                in ( ttb.jf_tt_status_pkg$cn_work_status_id()
                    ,ttb.jf_tt_status_pkg$cn_cancel_status_id() );
  else
  	return true;
  end if
  ;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$of_raise_edit_err (
)
RETURNS void AS
$body$
begin  
	RAISE EXCEPTION '<<Редактировать можно только вариант расписания в статусе «В разработке» и «Отменено»!>>';
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$of_raise_edit_calendar_err (
)
RETURNS void AS
$body$
begin  
	RAISE EXCEPTION '<<Кадендарь можно редактировать только в статусе «В разработке», «Отменено», «На согласовании», «Согласовано»!>>';
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

----------------------------------------------------------------------------------------------
-- поставить в очередь на выполнения выравнивания интревальности
create or replace function ttb.jf_tt_variant_pkg$of_lining(
  p_id_account numeric,
  p_attr text
)
  returns text as
  $body$
  declare
    l_tt_variant_id  ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
  begin
    update ttb.tt_variant
    set tt_create_status_id = ttb.tt_create_status_pkg$smoothing()
    where tt_variant_id = l_tt_variant_id;

    perform oud.tt_oper_rebuilder_pkg$send_task(l_tt_variant_id);
    return null;
  end;
$body$
language 'plpgsql'
volatile
cost 100;
----------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$of_copy_dr_action (
  p_tt_variant_id_new integer,
  p_tt_variant_id integer
)
RETURNS text AS
$body$
declare
begin
  with old_dr as (
         select  td.tt_driver_id
         		,td.dr_shift_id
    			,ta.action_type_id
                ,o.tt_out_num
                ,ta.tt_action_group_id
                ,ta.action_gr_id
                ,td.is_сhanged_driver
                ,ta.tt_action_id
                ,row_number() over (partition by td.dr_shift_id
                                                ,ta.action_type_id
                                                ,o.tt_out_num
                                                ,ta.tt_action_group_id
                                                ,ta.action_gr_id
                                                ,td.is_сhanged_driver
                					order by td.tt_driver_id) ord
        from ttb.tt_variant ttv
        join ttb.tt_driver td on td.tt_variant_id = ttv.tt_variant_id
        join ttb.tt_driver_action tda on tda.tt_driver_id = td.tt_driver_id
        join ttb.tt_action ta on ta.tt_action_id = tda.tt_action_id
        join ttb.tt_out o on o.tt_out_id = ta.tt_out_id
        				and o.tt_variant_id = ttv.tt_variant_id
        where td.tt_variant_id = p_tt_variant_id
        )
	,new_dr as (
          select td.tt_driver_id
          		,td.dr_shift_id
    			,ta.action_type_id
                ,o.tt_out_num
                ,ta.tt_action_group_id
                ,ta.action_gr_id
                ,td.is_сhanged_driver
                ,ta.tt_action_id
                ,row_number() over (partition by td.dr_shift_id
                                                ,ta.action_type_id
                                                ,o.tt_out_num
                                                ,ta.tt_action_group_id
                                                ,ta.action_gr_id
                                                ,td.is_сhanged_driver
                					order by td.tt_driver_id)ord
        from ttb.tt_variant ttv
        join ttb.tt_driver td on td.tt_variant_id = ttv.tt_variant_id
        join ttb.tt_out o on td.tt_variant_id = o.tt_variant_id
        join ttb.tt_action ta on o.tt_out_id = ta.tt_out_id
        where td.tt_variant_id = p_tt_variant_id_new
        )
        insert into ttb.tt_driver_action 
        		(tt_driver_id
				,tt_action_id)
    	  select n.tt_driver_id
          		,n.tt_action_id
    from old_dr o
    join new_dr n on o.dr_shift_id = n.dr_shift_id
    			 and o.action_type_id = n.action_type_id
                 and o.tt_out_num = n.tt_out_num
                 and o.tt_action_group_id = n.tt_action_group_id
                 and o.action_gr_id = n.action_gr_id
                 and o.is_сhanged_driver = n.is_сhanged_driver
                 and o.ord = n.ord
	;
  
  return null;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_variant_pkg$of_check_norm_bs_txt (
  p_tt_variant_id integer
)
RETURNS text AS
$body$
declare
  lt_res 	text;
begin
  select t.prev_stop_name || ' - ' || t.stop_name 
  into lt_res
  from (with tv_tr as (
          select tr.tr_capacity_id
                ,tp.time_begin--::time
                ,tp.time_end--::time
            from ttb.tt_variant tv
            join ttb.tt_schedule sch on sch.tt_variant_id = tv.tt_variant_id
            join ttb.tt_period tp on tp.tt_schedule_id = sch.tt_schedule_id
            join ttb.tt_tr_period trp on trp.tt_period_id = tp.tt_period_id
            join ttb.tt_tr tr on tr.tt_tr_id = trp.tt_tr_id 
                             and tr.tt_variant_id = tv.tt_variant_id 
            where 1 = 1 
            and tv.tt_variant_id = p_tt_variant_id        
            group by tr.tr_capacity_id
                    ,tp.time_begin--::time
                    ,tp.time_end--::time
            )
        ,tv_stops as (
          select sir.round_id
                ,lag(s.name, 1, s.name) over (partition by sir.round_id order by sir.order_num) prev_stop_name
                ,s.name stop_name 
                ,lag(si.stop_item_id, 1, si.stop_item_id) over (partition by sir.round_id order by sir.order_num) prev_stop_item_id
                ,si.stop_item_id   
         	from ttb.tt_at_round_stop trs         	
            join rts.stop_item2round sir on sir.stop_item2round_id = trs.stop_item2round_id
                                        and not sir.sign_deleted
            join rts.stop_item si on si.stop_item_id = sir.stop_item_id
                                 and not si.sign_deleted
            join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
                                    and not sl.sign_deleted
            join rts.stop s on s.stop_id = sl.stop_id
                           and not s.sign_deleted
            join ttb.tt_at_round ar on ar.tt_at_round_id = trs.tt_at_round_id
            join ttb.tt_action_type act on act.tt_action_type_id = ar.tt_action_type_id  
            join rts.round rnd on rnd.round_id = ar.round_id and not rnd.sign_deleted
            join ttb.tt_schedule sch on sch.tt_variant_id = act.tt_variant_id
            join ttb.tt_schedule_item schi on schi.tt_schedule_id = sch.tt_schedule_id
            join ttb.tt_action_group tag on tag.tt_action_group_id = schi.tt_action_group_id
            join ttb.tt_action_type2group at2g on at2g.tt_action_group_id = tag.tt_action_group_id
                                                   and at2g.tt_action_type_id = act.tt_action_type_id
            where act.tt_variant_id = p_tt_variant_id 
             ) 
       /*,perds as (
                   select hr.hr as hr_set
                            from generate_series
                            ('01.01.1900'::date + '00:00:00'::interval
                            ,'01.01.1900'::date + '23:00:00'::interval
                            ,'1 hour'::interval) hr
             )*/
        ,tv_stop_capacity as (
          select distinct tv_tr.tr_capacity_id
                         ,tv_tr.time_begin
                         ,tv_tr.time_end
                         --,perds.hr_set as hr_set_from
                         --,perds.hr_set + make_interval(hours => 1) as hr_set_to
                         ,tv_stops.round_id
                         ,tv_stops.prev_stop_name
                         ,tv_stops.stop_name 
                         ,tv_stops.prev_stop_item_id
                         ,tv_stops.stop_item_id  
          from tv_tr 
          --join perds on tsrange(tv_tr.time_begin, tv_tr.time_end) @> perds.hr_set
          join tv_stops on 1 = 1    
             ) 
        ,t_norm_bs as ( 
             select  nbs.stop_item_1_id
                    ,nbs.stop_item_2_id 
                    ,nbs.between_stop_dur
                    ,nbs.hour_from
                    ,nbs.hour_to
                    ,nbs.tr_type_id
                    ,nbs.tr_capacity_id
                    ,ttv.tt_variant_id
                    ,nbs.norm_between_stop_id
              from ttb.tt_variant ttv
              join ttb.norm n on n.norm_id = ttv.norm_id 
              join ttb.norm_between_stop nbs on nbs.norm_id = n.norm_id
              where ttv.tt_variant_id = p_tt_variant_id
              )
          select tsc.* 
          from tv_stop_capacity tsc
          where not exists (select null
                              from t_norm_bs tnbs 
                             where tnbs.stop_item_2_id = tsc.stop_item_id
                               and tnbs.stop_item_1_id = tsc.prev_stop_item_id
                               and tnbs.tr_capacity_id = tsc.tr_capacity_id 
                            /* and tnbs.hour_from = tsc.hr_set_from::time
                               and tnbs.hour_to = tsc.hr_set_to::time */
                            )						 
            and tsc.stop_item_id <> tsc.prev_stop_item_id
            limit 1) t;

  return lt_res;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;