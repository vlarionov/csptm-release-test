CREATE OR REPLACE FUNCTION ttb.jf_f_hour_pkg$of_rows (p_id_account numeric, out p_rows refcursor, p_attr text)
RETURNS refcursor AS
$body$
declare
 l_dt timestamp;
 l_workday_hours smallint[]:= array[1, 7, 8, 9, 12, 15, 17, 18, 19, 20, 21, 24];
 l_holiday_hours smallint[]:= array[1, 8, 10, 13, 15, 17, 19, 21, 24];
begin 
 l_dt := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'f_DT', true), current_timestamp);
 open p_rows for
 select to_char(f.hr, 'DD.MM.YYYY HH24:MI') as hr_text,
        extract ('days' from f.hr - f.dt) * 24 + extract ('hour' from f.hr - f.dt) as hr_int
  from (select t.lvl,
               date_trunc('day', l_dt) as dt,
               date_trunc('day', l_dt) + (t.lvl * interval '1 hour') as hr,
               ttb.calendar_pkg$is_working_day(l_dt::date) as is_workday
          from (select generate_series(0, 48, 1) as lvl) t) f
  where (f.is_workday and f.lvl = any (l_workday_hours)) OR
        (not f.is_workday and f.lvl = any (l_holiday_hours));
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;