CREATE OR REPLACE FUNCTION ttb.jf_atgrs_time_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.atgrs_time AS
$body$
declare 
   l_r ttb.atgrs_time%rowtype;
   l_dt_action date;
   l_tt_variant_id ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
   l_sec_b  int:= jofl.jofl_pkg$extract_number(p_attr, 'sec_b', true);
begin 
   l_r.atgrs_time_id := jofl.jofl_pkg$extract_number(p_attr, 'atgrs_time_id', true); 
   l_r.tt_at_round_stop_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_at_round_stop_id', true); 
   l_r.fix_arrival_type_id := jofl.jofl_pkg$extract_number(p_attr, 'fix_arrival_type_id', true);
   --l_r.start_time := jofl.jofl_pkg$extract_date(p_attr, 'start_time', true);

  select  case when ttv.parent_tt_variant_id is null then '1900-01-01'::date else lower(ttv.action_period)::date end dt_action
  into l_dt_action
  from ttb.tt_variant ttv
  where ttv.tt_variant_id = l_tt_variant_id ;

  l_r.start_time := l_dt_action + l_sec_b * interval '1 sec';
  
  if not ttb.jf_tt_variant_pkg$of_get_policy(l_tt_variant_id)
    then perform ttb.jf_tt_variant_pkg$of_raise_edit_err();
  end if;    

  return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_atgrs_time_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  ln_tt_at_round_stop_id 	ttb.tt_at_round_stop.tt_at_round_stop_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'tt_at_round_stop_id', true);
  ln_tt_variant_id 			ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
  ld_dt_action 				date;
begin 
  select  case when ttv.parent_tt_variant_id is null then '1900-01-01'::date else lower(ttv.action_period)::date end dt_action
  into ld_dt_action
  from ttb.tt_variant ttv
  where ttv.tt_variant_id = ln_tt_variant_id;
  
 open p_rows for
     select
       atg.atgrs_time_id,
       atg.tt_at_round_stop_id,
       --atg.start_time,
       extract(epoch from atg.start_time - ld_dt_action)  as sec_b,
       f.fix_arrival_type_id,
       f.fix_arrival_type_name,
       act.tt_variant_id
     from ttb.atgrs_time atg
       join ttb.fix_arrival_type f on f.fix_arrival_type_id = atg.fix_arrival_type_id
       join ttb.tt_at_round_stop ars on ars.tt_at_round_stop_id = atg.tt_at_round_stop_id
       join ttb.tt_at_round ar on ar.tt_at_round_id = ars.tt_at_round_id
       join ttb.tt_action_type act on act.tt_action_type_id = ar.tt_action_type_id
      where atg.tt_at_round_stop_id = ln_tt_at_round_stop_id;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_atgrs_time_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.atgrs_time%rowtype;
begin 
   l_r := ttb.jf_atgrs_time_pkg$attr_to_rowtype(p_attr);

   update ttb.atgrs_time set 
          tt_at_round_stop_id = l_r.tt_at_round_stop_id,
          fix_arrival_type_id = l_r.fix_arrival_type_id,
          start_time = l_r.start_time
   where 
          atgrs_time_id = l_r.atgrs_time_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_atgrs_time_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r ttb.atgrs_time%rowtype;
begin 
   l_r := ttb.jf_atgrs_time_pkg$attr_to_rowtype(p_attr);

   delete from  ttb.atgrs_time where  atgrs_time_id = l_r.atgrs_time_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb.jf_atgrs_time_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r ttb.atgrs_time%rowtype;
begin 
   l_r := ttb.jf_atgrs_time_pkg$attr_to_rowtype(p_attr);

   insert into ttb.atgrs_time( tt_at_round_stop_id
  							  ,start_time 
                              ,fix_arrival_type_id )
  					values (l_r.tt_at_round_stop_id
                    	   ,l_r.start_time 
                           ,l_r.fix_arrival_type_id);

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------
CREATE OR REPLACE FUNCTION ttb.jf_atgrs_time_pkg$get_atgrs_time_set (
  p_schedule_id integer,
  p_tr_capacity_id integer = NULL::integer
)
RETURNS TABLE (
  is_ab_start boolean,
  is_start_a boolean,
  is_start_b boolean,
  is_ab_alig boolean,
  is_alig_a boolean,
  is_alig_b boolean,
  is_ab_start_time boolean,
  is_start_time_a boolean,
  is_start_time_b boolean,
  is_a boolean,
  is_b boolean,
  norm_id integer,
  round_id integer,
  stop_item_id integer,
  tt_at_round_id bigint,
  dur_cycle integer,
  tt_schedule_id integer,
  tt_variant_id integer,
  start_time_from_a timestamp,
  start_time_from_b timestamp,
  atgsr_start_time_source timestamp,
  atgsr_start_time timestamp
) AS
$body$
begin
  return query
  with t as (
select  distinct
            atg.start_time
           ,v.norm_id
           ,v.tt_variant_id
           ,ar.round_id
           ,r.code
           ,sir.stop_item_id
           ,ar.tt_at_round_id 
           ,(select exists (select 1 from ttb.timetable_pkg$get_stop_interval_alignment(v.tt_variant_id) where fstop_item_id = sir.stop_item_id) and r.move_direction_id = 1) as  is_alig_a
           ,(select exists (select 1 from ttb.timetable_pkg$get_stop_interval_alignment(v.tt_variant_id) where fstop_item_id = sir.stop_item_id) and r.move_direction_id = 2) as  is_alig_b
           ,(select exists (select 1 from ttb.timetable_pkg$get_stop_start(it.tt_schedule_id) where fstop_item_id = sir.stop_item_id) and r.move_direction_id = 1) as is_start_a
           ,(select exists (select 1 from ttb.timetable_pkg$get_stop_start(it.tt_schedule_id) where fstop_item_id = sir.stop_item_id) and r.move_direction_id = 2) as is_start_b
           ,r.move_direction_id = 1 as is_a
           ,r.move_direction_id = 2 as is_b
           ,r.move_direction_id
           ,it.tt_schedule_id
           ,ar.tt_action_type_id
           ,r.move_direction_id = 1 and atg.start_time is not null as is_start_time_a
           ,r.move_direction_id = 2 and atg.start_time is not null as is_start_time_b
           ,case when r.move_direction_id = 1 then atg.start_time end start_time_from_a
           ,case when r.move_direction_id = 2 then atg.start_time end start_time_from_b
           from ttb.v_tt_schedule_item it
           join ttb.tt_schedule sch on sch.tt_schedule_id = it.tt_schedule_id
           						   and it.tt_variant_id = sch.tt_variant_id
                                   and sch.parent_schedule_id is null
           join ttb.tt_variant v on v.tt_variant_id = it.tt_variant_id
           join ttb.tt_action_type2group at2g on at2g.tt_action_group_id = it.tt_action_group_id
           join ttb.tt_at_round ar on ar.tt_action_type_id = at2g.tt_action_type_id
           join ttb.tt_at_round_stop ars on ars.tt_at_round_id = ar.tt_at_round_id
           join rts.stop_item2round sir on sir.stop_item2round_id = ars.stop_item2round_id 
                                        and sir.order_num = 1
                                        and not sir.sign_deleted
           join rts.round r on r.round_id = sir.round_id and not r.sign_deleted
           left join ttb.atgrs_time atg on atg.tt_at_round_stop_id = ars.tt_at_round_stop_id 
                                       and atg.fix_arrival_type_id = ttb.fix_arrival_type_pkg$start_time()
        where it.tt_schedule_id = p_schedule_id  
        and it.ref_group_kind_id = ttb.ref_group_kind_pkg$main_action()
        )
  ,tt as (
   select max(t.is_start_a::int) over (partition by t.tt_schedule_id) +
   		  max(t.is_start_b::int) over (partition by t.tt_schedule_id) = 2 as is_ab_start
    	 ,t.is_start_a
         ,t.is_start_b
         ,max(t.is_alig_a::int) over (partition by t.tt_schedule_id) +
          max(t.is_alig_b::int) over (partition by t.tt_schedule_id) = 2 as is_ab_alig
         ,t.is_alig_a
         ,t.is_alig_b
         ,max(t.is_start_time_a::int) over (partition by t.tt_schedule_id) +
          max(t.is_start_time_b::int) over (partition by t.tt_schedule_id) = 2 as is_ab_start_time
         ,t.is_start_time_a 
         ,t.is_start_time_b          
         ,t.is_a
         ,t.is_b
         ,t.norm_id
         ,t.round_id
         ,t.stop_item_id
         ,t.tt_at_round_id 
		 ,t.tt_schedule_id
         ,t.tt_variant_id
         ,min(t.start_time_from_a) over (partition by t.tt_schedule_id) as start_time_from_a
         ,min(t.start_time_from_b) over (partition by t.tt_schedule_id) as start_time_from_b
         ,t.start_time as atgsr_start_time
	from t     
    )
    ,bn_break as (
      select tt.round_id
      		,coalesce(ceil(avg(nr.min_inter_round_stop_dur)/60::real)*60 , 0) +
         	 coalesce(ceil(avg(nr.reg_inter_round_stop_dur)/60::real)*60 , 0) bn_dur
      from ttb.norm_round  nr join tt on tt.round_id = nr.round_id
                                      and tt.norm_id = nr.norm_id
      where ((date_trunc ('hour',tt.atgsr_start_time)::time) is  null or  nr.hour_from >= date_trunc ('hour',tt.atgsr_start_time)::time)
            and ((date_trunc ('hour',tt.atgsr_start_time)::time + interval '1 hour') is  null or  nr.hour_to<= date_trunc ('hour',tt.atgsr_start_time)::time + interval '1 hour')
            and (p_tr_capacity_id is null or nr.tr_capacity_id = p_tr_capacity_id)
      group by tt.round_id, nr.tr_type_id, nr.norm_id
    )
    , rnd as (
      with r as (
          select
          nbs.tr_capacity_id, nbs.tr_type_id, nbs.norm_id, sum(between_stop_dur) as rnd_dur, nbs.hour_from,
          tt.round_id
          from rts.stop_item2round sir_b
          join rts.stop_item2round sir_e on sir_e.order_num = sir_b.order_num+1 and  sir_e.round_id = sir_b.round_id
          join ttb.norm_between_stop nbs on nbs.stop_item_1_id = sir_b.stop_item_id and nbs.stop_item_2_id =sir_e.stop_item_id
          join tt on tt.round_id = sir_b.round_id
                  and tt.norm_id = nbs.norm_id
          where ((date_trunc ('hour',tt.atgsr_start_time)::time) is  null or  nbs.hour_from >= date_trunc ('hour',tt.atgsr_start_time)::time)
            and ((date_trunc ('hour',tt.atgsr_start_time)::time + interval '1 hour') is  null or  nbs.hour_to <= date_trunc ('hour',tt.atgsr_start_time)::time + interval '1 hour')
            and (p_tr_capacity_id is null or nbs.tr_capacity_id = p_tr_capacity_id)
          group by tt.round_id, nbs.tr_capacity_id, nbs.tr_type_id, nbs.norm_id, nbs.hour_from
      )
      select r.round_id
      		,coalesce(ceil(avg(r.rnd_dur)/60::real)*60 , 0) as rnd_dur
      from r
      group by r.round_id, r.norm_id, r.tr_type_id, r.norm_id
    )
     select tt.is_ab_start
           ,tt.is_start_a
           ,tt.is_start_b
           ,tt.is_ab_alig
           ,tt.is_alig_a
           ,tt.is_alig_b
           ,tt.is_ab_start_time
           ,tt.is_start_time_a
           ,tt.is_start_time_b
           ,tt.is_a
           ,tt.is_b
           ,tt.norm_id
           ,tt.round_id
           ,tt.stop_item_id
           ,tt.tt_at_round_id 
           ,(bn_break.bn_dur + rnd.rnd_dur)::int as dur_cycle
           ,tt.tt_schedule_id
           ,tt.tt_variant_id
           ,tt.start_time_from_a
           ,tt.start_time_from_b
           ,tt.atgsr_start_time as atgsr_start_time_source
           ,case /*время не указано, бум искать*/
                when tt.atgsr_start_time is null then
                  case /*выпуск*/
                    when tt.is_ab_start then
                      case /*выравнивание*/
                        when tt.is_ab_alig then
                          case /*время выпуска*/
                            when tt.is_ab_start_time then least(tt.start_time_from_a, tt.start_time_from_b)
                            when tt.start_time_from_a is not null then tt.start_time_from_a + (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                            when tt.start_time_from_b is not null then tt.start_time_from_b + (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                            else null
                          end                     
                        when tt.is_alig_a then
                          case /*время выпуска*/
                            when tt.is_ab_start_time then least(tt.start_time_from_a, tt.start_time_from_b)
                            when tt.start_time_from_a is not null then tt.start_time_from_a + (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                            when tt.start_time_from_b is not null then tt.start_time_from_b - (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                            else null
                          end 
                        when tt.is_alig_b then
                          case /*время выпуска*/
                            when tt.is_ab_start_time then least(tt.start_time_from_a, tt.start_time_from_b)
                            when tt.start_time_from_a is not null then tt.start_time_from_a + (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                            when tt.start_time_from_b is not null then tt.start_time_from_b - (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                            else null
                          end   
                        else 
                          case /*время выпуска*/
                            when tt.is_ab_start_time then least(tt.start_time_from_a, tt.start_time_from_b)
                            when tt.start_time_from_a is not null then tt.start_time_from_a + (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                            when tt.start_time_from_b is not null then tt.start_time_from_b + (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                            else null
                          end   
                      end /*выравнивание*/
                    when tt.is_start_a then  
                      case /*время выпуска*/
                        when tt.is_ab_start_time then tt.start_time_from_a
                        when tt.start_time_from_a is not null then tt.start_time_from_a + (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                        when tt.start_time_from_b is not null then tt.start_time_from_b - (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                        else null
                      end
                    when tt.is_start_b then  
                      case /*время выпуска*/
                        when tt.is_ab_start_time then tt.start_time_from_b
                        when tt.start_time_from_a is not null then tt.start_time_from_a - (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                        when tt.start_time_from_b is not null then tt.start_time_from_b + (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                        else null
                      end 
                    else null  
                    end /*выпуск*/
                  else tt.atgsr_start_time  
                end /*время не указано, бум искать*/   
           from tt
           join bn_break on tt.round_id = bn_break.round_id
           join rnd on tt.round_id = rnd.round_id
           ;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;
-------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_atgrs_time_pkg$get_atgrs_time_set_finish (
  p_schedule_id integer,
  p_tr_capacity_id integer = NULL::integer
)
RETURNS TABLE (
  is_ab_start boolean,
  is_start_a boolean,
  is_start_b boolean,
  is_ab_alig boolean,
  is_alig_a boolean,
  is_alig_b boolean,
  is_ab_start_time boolean,
  is_start_time_a boolean,
  is_start_time_b boolean,
  is_a boolean,
  is_b boolean,
  norm_id integer,
  round_id integer,
  stop_item_id integer,
  tt_at_round_id bigint,
  dur_cycle integer,
  tt_schedule_id integer,
  tt_variant_id integer,
  start_time_from_a timestamp,
  start_time_from_b timestamp,
  atgsr_start_time_source timestamp,
  atgsr_start_time timestamp
) AS
$body$
begin
  return query
  with t as (
	select  distinct
            atg.start_time
           ,v.norm_id
           ,v.tt_variant_id
           ,ar.round_id
           ,r.code
           ,sir.stop_item_id
           ,ar.tt_at_round_id 
           ,(select exists (select 1 from ttb.timetable_pkg$get_stop_interval_alignment(v.tt_variant_id) where fstop_item_id = sir.stop_item_id) and r.move_direction_id = 1) as  is_alig_a
           ,(select exists (select 1 from ttb.timetable_pkg$get_stop_interval_alignment(v.tt_variant_id) where fstop_item_id = sir.stop_item_id) and r.move_direction_id = 2) as  is_alig_b
           ,(select exists (select 1 from ttb.timetable_pkg$get_stop_start(it.tt_schedule_id) where fstop_item_id = sir.stop_item_id) and r.move_direction_id = 1) as is_start_a
           ,(select exists (select 1 from ttb.timetable_pkg$get_stop_start(it.tt_schedule_id) where fstop_item_id = sir.stop_item_id) and r.move_direction_id = 2) as is_start_b
           ,r.move_direction_id = 1 as is_a
           ,r.move_direction_id = 2 as is_b
           ,r.move_direction_id
           ,it.tt_schedule_id
           ,ar.tt_action_type_id
           ,r.move_direction_id = 1 and atg.start_time is not null as is_start_time_a
           ,r.move_direction_id = 2 and atg.start_time is not null as is_start_time_b
           ,case when r.move_direction_id = 1 then atg.start_time end start_time_from_a
           ,case when r.move_direction_id = 2 then atg.start_time end start_time_from_b
           from ttb.v_tt_schedule_item it
           join ttb.tt_schedule sch on sch.tt_schedule_id = it.tt_schedule_id
           						   and it.tt_variant_id = sch.tt_variant_id
                                   and sch.parent_schedule_id is null
           join ttb.tt_variant v on v.tt_variant_id = it.tt_variant_id
           join ttb.tt_action_type2group at2g on at2g.tt_action_group_id = it.tt_action_group_id
           join ttb.tt_at_round ar on ar.tt_action_type_id = at2g.tt_action_type_id
           join ttb.tt_at_round_stop ars on ars.tt_at_round_id = ar.tt_at_round_id
           join rts.stop_item2round sir on sir.stop_item2round_id = ars.stop_item2round_id 
                                        and sir.order_num = 1
                                        and not sir.sign_deleted
           join rts.round r on r.round_id = sir.round_id and not r.sign_deleted
           left join ttb.atgrs_time atg on atg.tt_at_round_stop_id = ars.tt_at_round_stop_id 
                                       and atg.fix_arrival_type_id = ttb.fix_arrival_type_pkg$finish_time()
        where it.tt_schedule_id = p_schedule_id  
        and it.ref_group_kind_id = ttb.ref_group_kind_pkg$main_action()
        )
  ,tt as (
   select max(t.is_start_a::int) over (partition by t.tt_schedule_id) +
   		  max(t.is_start_b::int) over (partition by t.tt_schedule_id) = 2 as is_ab_start
    	 ,t.is_start_a
         ,t.is_start_b
         ,max(t.is_alig_a::int) over (partition by t.tt_schedule_id) +
          max(t.is_alig_b::int) over (partition by t.tt_schedule_id) = 2 as is_ab_alig
         ,t.is_alig_a
         ,t.is_alig_b
         ,max(t.is_start_time_a::int) over (partition by t.tt_schedule_id) +
          max(t.is_start_time_b::int) over (partition by t.tt_schedule_id) = 2 as is_ab_start_time
         ,t.is_start_time_a 
         ,t.is_start_time_b          
         ,t.is_a
         ,t.is_b
         ,t.norm_id
         ,t.round_id
         ,t.stop_item_id
         ,t.tt_at_round_id 
		 ,t.tt_schedule_id
         ,t.tt_variant_id
         ,min(t.start_time_from_a) over (partition by t.tt_schedule_id) as start_time_from_a
         ,min(t.start_time_from_b) over (partition by t.tt_schedule_id) as start_time_from_b
         ,t.start_time as atgsr_start_time
	from t     
    )
    ,bn_break as (
      select tt.round_id
      		,coalesce(ceil(avg(nr.min_inter_round_stop_dur)/60::real)*60 , 0) +
         	 coalesce(ceil(avg(nr.reg_inter_round_stop_dur)/60::real)*60 , 0) bn_dur
      from ttb.norm_round  nr join tt on tt.round_id = nr.round_id
                                      and tt.norm_id = nr.norm_id
      where ((date_trunc ('hour',tt.atgsr_start_time)::time) is  null or  nr.hour_from >= date_trunc ('hour',tt.atgsr_start_time)::time)
            and ((date_trunc ('hour',tt.atgsr_start_time)::time + interval '1 hour') is  null or  nr.hour_to<= date_trunc ('hour',tt.atgsr_start_time)::time + interval '1 hour')
            and (p_tr_capacity_id is null or nr.tr_capacity_id = p_tr_capacity_id)
      group by tt.round_id, nr.tr_type_id, nr.norm_id
    )
    , rnd as (
      with r as (
          select
          nbs.tr_capacity_id, nbs.tr_type_id, nbs.norm_id, sum(between_stop_dur) as rnd_dur, nbs.hour_from,
          tt.round_id
          from rts.stop_item2round sir_b
          join rts.stop_item2round sir_e on sir_e.order_num = sir_b.order_num+1 and  sir_e.round_id = sir_b.round_id
          join ttb.norm_between_stop nbs on nbs.stop_item_1_id = sir_b.stop_item_id and nbs.stop_item_2_id =sir_e.stop_item_id
          join tt on tt.round_id = sir_b.round_id
                  and tt.norm_id = nbs.norm_id
          where ((date_trunc ('hour',tt.atgsr_start_time)::time) is  null or  nbs.hour_from >= date_trunc ('hour',tt.atgsr_start_time)::time)
            and ((date_trunc ('hour',tt.atgsr_start_time)::time + interval '1 hour') is  null or  nbs.hour_to <= date_trunc ('hour',tt.atgsr_start_time)::time + interval '1 hour')
            and (p_tr_capacity_id is null or nbs.tr_capacity_id = p_tr_capacity_id)
          group by tt.round_id, nbs.tr_capacity_id, nbs.tr_type_id, nbs.norm_id, nbs.hour_from
      )
      select r.round_id
      		,coalesce(ceil(avg(r.rnd_dur)/60::real)*60 , 0) as rnd_dur
      from r
      group by r.round_id, r.norm_id, r.tr_type_id, r.norm_id
    )
     select tt.is_ab_start
           ,tt.is_start_a
           ,tt.is_start_b
           ,tt.is_ab_alig
           ,tt.is_alig_a
           ,tt.is_alig_b
           ,tt.is_ab_start_time
           ,tt.is_start_time_a
           ,tt.is_start_time_b
           ,tt.is_a
           ,tt.is_b
           ,tt.norm_id
           ,tt.round_id
           ,tt.stop_item_id
           ,tt.tt_at_round_id 
           ,(bn_break.bn_dur + rnd.rnd_dur)::int as dur_cycle
           ,tt.tt_schedule_id
           ,tt.tt_variant_id
           ,tt.start_time_from_a
           ,tt.start_time_from_b
           ,tt.atgsr_start_time as atgsr_start_time_source
           ,case /*время не указано, бум искать*/
                when tt.atgsr_start_time is null then
                  case /*выпуск*/
                    when tt.is_ab_start then
                      case /*выравнивание*/
                        when tt.is_ab_alig then
                          case /*время выпуска*/
                            when tt.is_ab_start_time then least(tt.start_time_from_a, tt.start_time_from_b)
                            when tt.start_time_from_a is not null then tt.start_time_from_a - (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                            when tt.start_time_from_b is not null then tt.start_time_from_b - (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                            else null
                          end                     
                        when tt.is_alig_a then
                          case /*время выпуска*/
                            when tt.is_ab_start_time then least(tt.start_time_from_a, tt.start_time_from_b)
                            when tt.start_time_from_a is not null then tt.start_time_from_a - (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                            when tt.start_time_from_b is not null then tt.start_time_from_b + (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                            else null
                          end 
                        when tt.is_alig_b then
                          case /*время выпуска*/
                            when tt.is_ab_start_time then least(tt.start_time_from_a, tt.start_time_from_b)
                            when tt.start_time_from_a is not null then tt.start_time_from_a - (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                            when tt.start_time_from_b is not null then tt.start_time_from_b + (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                            else null
                          end   
                        else 
                          case /*время выпуска*/
                            when tt.is_ab_start_time then least(tt.start_time_from_a, tt.start_time_from_b)
                            when tt.start_time_from_a is not null then tt.start_time_from_a - (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                            when tt.start_time_from_b is not null then tt.start_time_from_b - (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                            else null
                          end   
                      end /*выравнивание*/
                    when tt.is_start_a then  
                      case /*время выпуска*/
                        when tt.is_ab_start_time then tt.start_time_from_a
                        when tt.start_time_from_a is not null then tt.start_time_from_a - (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                        when tt.start_time_from_b is not null then tt.start_time_from_b + (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                        else null
                      end
                    when tt.is_start_b then  
                      case /*время выпуска*/
                        when tt.is_ab_start_time then tt.start_time_from_b
                        when tt.start_time_from_a is not null then tt.start_time_from_a + (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                        when tt.start_time_from_b is not null then tt.start_time_from_b - (bn_break.bn_dur + rnd.rnd_dur || ' sec')::interval
                        else null
                      end 
                    else null  
                    end /*выпуск*/
                  else tt.atgsr_start_time  
                end /*время не указано, бум искать*/   
           from tt
           join bn_break on tt.round_id = bn_break.round_id
           join rnd on tt.round_id = rnd.round_id
           ;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;
---------------------------------------------------------------------------------