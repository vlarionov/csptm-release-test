----------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_schedule_item4select_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 l_tt_schedule_id 	ttb.tt_schedule_item.tt_schedule_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_schedule_id', true);
 l_tt_variant_id 	ttb.tt_variant.tt_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
 lt_rf_db_method	text := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE), 'xxx');
begin
  raise notice '% ', l_tt_variant_id;
  open p_rows for
 select
   tschi.tt_schedule_item_id,
   tschi.tt_schedule_id,
   tschi.tt_action_group_id,
   tschi.repeat_time,
   tschi.order_num,
   tschi.is_required,
   tag.tt_action_group_name as action_group_name
 from
   ttb.tt_schedule sch
   join ttb.tt_schedule_item tschi on tschi.tt_schedule_id = sch.tt_schedule_id
   join ttb.tt_action_group tag on tag.tt_action_group_id = tschi.tt_action_group_id
 where (sch.tt_variant_id = l_tt_variant_id)
   and (lt_rf_db_method != 'ttb.tt_switch'
        or
       (  lt_rf_db_method = 'ttb.tt_switch'
       and tag.ref_group_kind_id in ( ttb.ref_group_kind_pkg$switch_action(), ttb.ref_group_kind_pkg$switch_from_park_action() )
       and sch.tt_schedule_id = l_tt_schedule_id
       ))
 order by tschi.order_num;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
