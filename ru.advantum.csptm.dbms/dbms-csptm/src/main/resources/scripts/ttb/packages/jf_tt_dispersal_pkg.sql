CREATE OR REPLACE FUNCTION ttb.jf_tt_dispersal_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 f_arr_tt_variant_id 	integer[] 	:= jofl.jofl_pkg$extract_narray(p_attr, 'f_list_tt_variant_id', true);
 f_stop_item_id		 	integer		:= jofl.jofl_pkg$extract_number(p_attr, 'f_stop_item_id', true);
 f_dt				 	date		:= jofl.jofl_pkg$extract_date(p_attr, 'f_DT', true);
 f_disp_dt				date		:= jofl.jofl_pkg$extract_date(p_attr, 'f_disp_DT', true);
 b_disp_time_DT			timestamp	:= jofl.jofl_pkg$extract_date(p_attr, 'b_disp_time_DT', true);
 e_disp_time_DT			timestamp	:= jofl.jofl_pkg$extract_date(p_attr, 'e_disp_time_DT', true);
 f_min_disp_NUMBER		smallint	:= jofl.jofl_pkg$extract_number(p_attr, 'f_min_disp_NUMBER', TRUE);	
 f_max_work_NUMBER		smallint	:= jofl.jofl_pkg$extract_number(p_attr, 'f_max_work_NUMBER', TRUE);	
 ln_cnt					smallint;
begin 
/*
 select count(1)
 into ln_cnt
 from ttb.tt_variant
 where tt_variant_id = any(f_arr_tt_variant_id)
 group by route_variant_id
 having count(1) > 1;
 
 if ln_cnt > 1 then
 	raise exception '<<Проверьте введенные данные. Выбранные варианты расписаний должны относиться к различным маршрутам.>>'; 
 end if;

 perform ttb.jf_tt_dispersal_pkg$of_start_dispersal(p_id_account
 												   ,f_stop_item_id
                                                   ,f_arr_tt_variant_id
                                                   ,b_disp_time_DT
                                                   ,e_disp_time_DT
                                                   ,f_min_disp_NUMBER);
*/
 open p_rows for 
   select distinct
			 out.tt_out_num
            ,ta.tt_action_id
            ,at.action_type_id
            ,si.stop_item_id
            ,si2r.order_num
            ,s.name stop_name
            ,tai.time_begin
            ,tai.time_end
            ,si2r.length_sector len
            ,r.round_id
            ,r.code
            ,r.round_num
            ,rt.route_num
            ,s.stop_id
            ,ds.dr_shift_num
    from ttb.tt_out out 
    join ttb.tt_action ta on ta.tt_out_id = out.tt_out_id
    join ttb.action_type at on at.action_type_id = ta.action_type_id
    join ttb.tt_action_item tai on tai.tt_action_id = ta.tt_action_id
    join ttb.tt_action_round tar on tar.tt_action_id = ta.tt_action_id
    join rts.round r on r.round_id = tar.round_id
                     and r.action_type_id = at.action_type_id
    --join rts.route_variant rv on rv.route_variant_id = r.route_variant_id
    join rts.route rt on rt.current_route_variant_id = r.route_variant_id 
    --join rts.move_direction md on md.move_direction_id = r.move_direction_id
    join rts.stop_item2round si2r on si2r.round_id = r.round_id 
                                  and si2r.stop_item2round_id = tai.stop_item2round_id                                  
    join rts.stop_item2round_type si2rt on si2rt.stop_item2round_id = si2r.stop_item2round_id
    join rts.stop_item si on si.stop_item_id = si2r.stop_item_id
    join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
    join rts.stop s on s.stop_id = sl.stop_id
    join ttb.dr_shift ds on ds.dr_shift_id = tai.dr_shift_id
    where at.parent_type_id <> ttb.action_type_pkg$break()
      and si2r.order_num <> 1
	  and not out.sign_deleted
      and si.stop_item_id = f_stop_item_id
      and out.tt_variant_id = any(f_arr_tt_variant_id)
      and tai.time_begin::time between b_disp_time_DT::time and e_disp_time_DT::time
    order by tai.time_begin;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_dispersal_pkg$of_start_dispersal (
  p_id_account numeric,
  p_stop_item_id integer,
  p_arr_tt_variant_id integer [],
  p_b_disp_time_dt timestamp,
  p_e_disp_time_dt timestamp,
  p_min_disp_number smallint,
  p_max_work_number smallint
)
RETURNS text AS
$body$
declare 
 ln_cnt			smallint;
 lt_res			text 		:= ttb.dt_status_pkg$cn_ready()::text;
 ldt_start_time timestamp 	:= clock_timestamp();
 ldt_end_time   timestamp;
begin 

  loop

    with tt as (  
             select distinct
             		 out.tt_out_id
                    ,out.tt_out_num
                    ,out.tt_variant_id
                    ,ta.tt_action_id
                    ,at.action_type_id
                    ,si.stop_item_id
                    ,si2r.order_num
                    ,s.name stop_name
                    ,tai.time_begin
                    --,lag(tai.time_begin, 1, tai.time_begin)  over (order by tai.time_begin, out.tt_variant_id) as prev_time_begin
                    --,lead(tai.time_begin, 1, tai.time_begin)  over (order by tai.time_begin, out.tt_variant_id) as next_time_begin
                    ,tai.time_end
                    ,si2r.length_sector len
                    ,r.round_id
                    ,r.code
                    ,r.round_num
                    ,rt.route_num
                    ,s.stop_id
                    ,ds.dr_shift_num
                    --,tai.time_begin - lag(tai.time_begin, 1, tai.time_begin) over (order by tai.time_begin, out.tt_variant_id) as stop_interval
                    --,lead(tai.time_begin, 1, tai.time_begin) over (order by tai.time_begin, out.tt_variant_id) - tai.time_begin as stop_interval_next
            from ttb.tt_out out 
            join ttb.tt_action ta on ta.tt_out_id = out.tt_out_id
            join ttb.action_type at on at.action_type_id = ta.action_type_id
            join ttb.tt_action_item tai on tai.tt_action_id = ta.tt_action_id
            join ttb.tt_action_round tar on tar.tt_action_id = ta.tt_action_id
            join rts.round r on r.round_id = tar.round_id
                             and r.action_type_id = at.action_type_id
            --join rts.route_variant rv on rv.route_variant_id = r.route_variant_id
            join rts.route rt on rt.current_route_variant_id = r.route_variant_id 
            --join rts.move_direction md on md.move_direction_id = r.move_direction_id
            join rts.stop_item2round si2r on si2r.round_id = r.round_id 
                                          and si2r.stop_item2round_id = tai.stop_item2round_id                                  
            join rts.stop_item2round_type si2rt on si2rt.stop_item2round_id = si2r.stop_item2round_id
            join rts.stop_item si on si.stop_item_id = si2r.stop_item_id
            join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
            join rts.stop s on s.stop_id = sl.stop_id
            join ttb.dr_shift ds on ds.dr_shift_id = tai.dr_shift_id
		   where at.parent_type_id <> ttb.action_type_pkg$break()
      		  and si2r.order_num <> 1
			  and not out.sign_deleted
              and si.stop_item_id = p_stop_item_id
              and out.tt_variant_id = any(p_arr_tt_variant_id)
              and tai.time_begin::time between p_b_disp_time_DT::time and p_e_disp_time_DT::time
            order by tai.time_begin
                    ,out.tt_variant_id
      )
      ,times as (
           select   tt.tt_out_id
                   ,tt.tt_out_num
                   ,tt.tt_variant_id
                   ,tt.tt_action_id
                   ,tt.action_type_id
                   ,tt.stop_item_id
                   ,tt.order_num
                   ,tt.stop_name
                   ,tt.time_begin
                   ,lag(tt.time_begin, 1, tt.time_begin)  over (order by tt.time_begin, tt.tt_variant_id) as prev_time_begin
                   ,lead(tt.time_begin, 1, tt.time_begin)  over (order by tt.time_begin, tt.tt_variant_id) as next_time_begin
                   ,tt.time_end
                   ,tt.len
                   ,tt.round_id
                   ,tt.code
                   ,tt.round_num
                   ,tt.route_num
                   ,tt.stop_id
                   ,tt.dr_shift_num
                   ,tt.time_begin - lag(tt.time_begin, 1, tt.time_begin) over (order by tt.time_begin, tt.tt_variant_id) as stop_interval
                   ,lead(tt.time_begin, 1, tt.time_begin) over (order by tt.time_begin, tt.tt_variant_id) - tt.time_begin as stop_interval_next
           from tt)
      ,prep_disp as (
          select  t.tt_variant_id
                 ,t.tt_out_id
                 ,t.tt_out_num
                 ,t.tt_action_id
                 ,ttb.jf_tt_action_pkg$get_prev_tt_action_id(t.tt_out_id, t.tt_action_id) as prev_tt_action_id
                 ,t.action_type_id
                 ,t.stop_item_id
                 ,t.order_num
                 ,t.stop_name
                 ,t.time_begin
                 ,t.prev_time_begin
                 ,t.next_time_begin
                 ,t.time_end
                 ,t.len
                 ,t.round_id
                 ,t.code
                 ,t.round_num
                 ,t.route_num
                 ,t.stop_id
                 ,t.dr_shift_num
                 ,t.stop_interval
                 ,t.stop_interval_next
               /*,('{"time_begin":"' 		|| t.time_begin + make_interval(mins => 1) 
                 || '","tt_action_id":"' 	|| t.tt_action_id 
                 || '","tt_variant_id":"' || t.tt_variant_id     || '"}') attr*/
                 ,row_number() over (partition by t.route_num, t.time_begin)::int as shift
                 ,row_number() over (partition by t.stop_interval < make_interval(mins => p_min_disp_NUMBER))::int as rn
                 ,case 
                      when t.stop_interval < make_interval(mins => p_min_disp_NUMBER) then 'fwd'
                      else 'bwd'
                  end direction
           from times t
          where coalesce(t.stop_interval, t.stop_interval_next) is not null 
            and ( t.stop_interval < make_interval(mins => p_min_disp_NUMBER) or
                  t.stop_interval_next < make_interval(mins => p_min_disp_NUMBER) )	
      )
           select count(1)
             into ln_cnt
      	 	 from prep_disp p
      		where p.direction = 'fwd'
              and p.rn > 1
      	;
 
	exit when ln_cnt = 0;

    perform ttb.jf_tt_dispersal_pkg$of_dispersing_fwd (
                                      p_id_account ,
                                      p_stop_item_id ,
                                      p_arr_tt_variant_id ,
                                      p_b_disp_time_dt ,
                                      p_e_disp_time_dt ,
                                      p_min_disp_number 
                                    ); 
                                    
    ldt_end_time := clock_timestamp();
    
    if extract (epoch from (ldt_end_time - ldt_start_time)) > p_max_work_NUMBER * 60 then
      lt_res := ttb.dt_status_pkg$cn_err_time_limit()::text;
      exit;
    else 
      lt_res := ttb.dt_status_pkg$cn_ready()::text;
    end if;  
    
    
  end  loop;

  return lt_res;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_dispersal_pkg$of_dispersing_fwd (
  p_id_account numeric,
  p_stop_item_id integer,
  p_arr_tt_variant_id integer [],
  p_b_disp_time_dt timestamp,
  p_e_disp_time_dt timestamp,
  p_min_disp_number smallint
)
RETURNS text AS
$body$
declare 
 lt_res	text;
begin 
    with tt as (  
             select distinct
             		 out.tt_out_id
                    ,out.tt_out_num
                    ,out.tt_variant_id
                    ,ta.tt_action_id
                    ,ta.action_range
                    ,at.action_type_id
                    ,si.stop_item_id
                    ,si2r.order_num
                    ,s.name stop_name
                    ,tai.time_begin
                    --,lag(tai.time_begin, 1, tai.time_begin)  over (order by tai.time_begin, out.tt_variant_id) as prev_time_begin
                    --,lead(tai.time_begin, 1, tai.time_begin)  over (order by tai.time_begin, out.tt_variant_id) as next_time_begin
                    ,tai.time_end
                    ,si2r.length_sector len
                    ,r.round_id
                    ,r.code
                    ,r.round_num
                    ,rt.route_num
                    ,s.stop_id
                    ,ds.dr_shift_num
                    --,tai.time_begin - lag(tai.time_begin, 1, tai.time_begin) over (order by tai.time_begin, out.tt_variant_id) as stop_interval
                    --,lead(tai.time_begin, 1, tai.time_begin) over (order by tai.time_begin, out.tt_variant_id) - tai.time_begin as stop_interval_next
            from ttb.tt_out out 
            join ttb.tt_action ta on ta.tt_out_id = out.tt_out_id
            join ttb.action_type at on at.action_type_id = ta.action_type_id
            join ttb.tt_action_item tai on tai.tt_action_id = ta.tt_action_id
            join ttb.tt_action_round tar on tar.tt_action_id = ta.tt_action_id
            join rts.round r on r.round_id = tar.round_id
                             and r.action_type_id = at.action_type_id
            --join rts.route_variant rv on rv.route_variant_id = r.route_variant_id
            join rts.route rt on rt.current_route_variant_id = r.route_variant_id 
            --join rts.move_direction md on md.move_direction_id = r.move_direction_id
            join rts.stop_item2round si2r on si2r.round_id = r.round_id 
                                          and si2r.stop_item2round_id = tai.stop_item2round_id                                  
            join rts.stop_item2round_type si2rt on si2rt.stop_item2round_id = si2r.stop_item2round_id
            join rts.stop_item si on si.stop_item_id = si2r.stop_item_id
            join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
            join rts.stop s on s.stop_id = sl.stop_id
            join ttb.dr_shift ds on ds.dr_shift_id = tai.dr_shift_id
		   where at.parent_type_id <> ttb.action_type_pkg$break()
      		  and si2r.order_num <> 1
			  and not out.sign_deleted
              and si.stop_item_id = p_stop_item_id
              and out.tt_variant_id = any(p_arr_tt_variant_id)
              and tai.time_begin::time between p_b_disp_time_DT::time and p_e_disp_time_DT::time
            order by tai.time_begin
                    ,out.tt_variant_id
      )
      ,times as (
           select   tt.tt_out_id
                   ,tt.tt_out_num
                   ,tt.tt_variant_id
                   ,tt.tt_action_id
                   ,tt.action_type_id
                   ,tt.stop_item_id
                   ,tt.order_num
                   ,tt.stop_name
                   ,tt.time_begin
                   ,lag(tt.time_begin, 1, tt.time_begin)  over (order by tt.time_begin, tt.tt_variant_id) as prev_time_begin
                   ,lead(tt.time_begin, 1, tt.time_begin)  over (order by tt.time_begin, tt.tt_variant_id) as next_time_begin
                   ,lower(tt.action_range) as act_range_begin  
                   ,tt.time_end
                   ,tt.len
                   ,tt.round_id
                   ,tt.code
                   ,tt.round_num
                   ,tt.route_num
                   ,tt.stop_id
                   ,tt.dr_shift_num
                   ,tt.time_begin - lag(tt.time_begin, 1, tt.time_begin) over (order by tt.time_begin, tt.tt_variant_id) as stop_interval
                   ,lead(tt.time_begin, 1, tt.time_begin) over (order by tt.time_begin, tt.tt_variant_id) - tt.time_begin as stop_interval_next
           from tt)
    ,prep_disp as (
        select  t.tt_variant_id
               ,t.tt_out_id
               ,t.tt_out_num
               ,t.tt_action_id
               ,ttb.jf_tt_action_pkg$get_prev_tt_action_id(t.tt_out_id, t.tt_action_id) as prev_tt_action_id
               /*,ttb.jf_tt_action_pkg$get_prev_tt_action_begin_time(t.tt_out_id, t.tt_action_id) as prev_tt_action_begin_time*/
               ,t.act_range_begin
               ,t.action_type_id
               ,t.stop_item_id
               ,t.order_num
               ,t.stop_name
               ,t.time_begin
               ,t.prev_time_begin
               ,t.next_time_begin
               ,t.time_end
               ,t.len
               ,t.round_id
               ,t.code
               ,t.round_num
               ,t.route_num
               ,t.stop_id
               ,t.dr_shift_num
               ,t.stop_interval
               ,t.stop_interval_next
        	 /*,('{"time_begin":"' 		|| t.time_begin + make_interval(mins => 1) 
               || '","tt_action_id":"' 	|| t.tt_action_id 
               || '","tt_variant_id":"' || t.tt_variant_id     || '"}') attr*/
               ,row_number() over (partition by t.route_num, t.time_begin)::int as shift
               ,row_number() over (partition by t.stop_interval < make_interval(mins => p_min_disp_NUMBER) order by t.time_begin)::int as rn
               ,case 
               		when t.stop_interval < make_interval(mins => p_min_disp_NUMBER) then 'fwd'
               		else 'bwd'
                end direction
         from times t
        where coalesce(t.stop_interval, t.stop_interval_next) is not null 
          and ( t.stop_interval < make_interval(mins => p_min_disp_NUMBER) or
                t.stop_interval_next < make_interval(mins => p_min_disp_NUMBER) )	
    )
         select ttb.jf_tt_out_pkg$of_update (
         		 p_id_account 
                ,'{"time_begin":"' 		 || p.act_range_begin + make_interval(mins => p.shift) 
                 || '","tt_action_id":"' || p.tt_action_id 
                 || '","tt_out_id":"' 	 || p.tt_out_id 
                 || '","tt_variant_id":"'|| p.tt_variant_id   || '"}')
           into lt_res
           from prep_disp p
          where p.direction = 'fwd'
            and p.rn > 1
          ;

  return null;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_dispersal_pkg$of_start (
)
RETURNS text AS
$body$
declare
  	a_id_account 		numeric;
  	a_stop_item_id 		integer;
  	a_arr_tt_variant_id integer [];
  	a_b_disp_time_dt 	timestamp;
  	a_e_disp_time_dt 	timestamp;
  	a_min_disp_number 	smallint;
  	a_max_work_number 	smallint;
    dt_cur				record;	
    lt_disp_res			text;
begin
   
	for dt_cur in (select dt.dispersal_task_id
    					 ,dt.account_id
    					 ,dt.dt_parms::text
    			   from ttb.dispersal_task dt
                   join ttb.dispersal_task_queue dtq on dtq.dispersal_task_id = dt.dispersal_task_id
    			   where dt.time_calc_begin is null
                   order by dt.time_create)
    	loop
        
         a_arr_tt_variant_id 	:= jofl.jofl_pkg$extract_narray(dt_cur.dt_parms, 'f_list_tt_variant_id', true);
         a_stop_item_id		 	:= jofl.jofl_pkg$extract_number(dt_cur.dt_parms, 'f_stop_item_id', true);
         a_b_disp_time_DT		:= jofl.jofl_pkg$extract_date(dt_cur.dt_parms, 'b_disp_time_DT', true);
         a_e_disp_time_DT		:= jofl.jofl_pkg$extract_date(dt_cur.dt_parms, 'e_disp_time_DT', true);
         a_min_disp_NUMBER		:= jofl.jofl_pkg$extract_number(dt_cur.dt_parms, 'f_min_disp_NUMBER', TRUE);	
         a_max_work_NUMBER		:= jofl.jofl_pkg$extract_number(dt_cur.dt_parms, 'f_max_work_NUMBER', TRUE);
         
         update ttb.dispersal_task
            set time_calc_begin = clock_timestamp(),
            	dt_status_id = ttb.dt_status_pkg$cn_process ()
          where dispersal_task_id = dt_cur.dispersal_task_id;	
                 
        
         lt_disp_res := ttb.jf_tt_dispersal_pkg$of_start_dispersal (
                                                  dt_cur.account_id,
                                                  a_stop_item_id,
                                                  a_arr_tt_variant_id,
                                                  a_b_disp_time_dt,
                                                  a_e_disp_time_dt,
                                                  a_min_disp_number,
                                                  a_max_work_number 
                                                );
                                                
         update ttb.dispersal_task
            set time_calc_end = clock_timestamp(),
            	dt_status_id = lt_disp_res::smallint
          where dispersal_task_id = dt_cur.dispersal_task_id;
          
         delete from ttb.dispersal_task_queue 
         where dispersal_task_id = dt_cur.dispersal_task_id;
                                      
        end loop;
   
   return null; --jofl.jofl_util$cover_result('message', coalesce(lt_res, 'no message'));
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;