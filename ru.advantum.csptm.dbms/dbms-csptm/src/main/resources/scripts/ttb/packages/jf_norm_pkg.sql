CREATE OR REPLACE FUNCTION ttb."jf_norm_pkg$attr_to_rowtype"(p_attr text)
 RETURNS ttb.norm
 LANGUAGE plpgsql
AS $function$
declare
   l_r ttb.norm%rowtype;
begin
   l_r.norm_id := jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true);
   l_r.calculation_task_id := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true);
   l_r.norm_name := jofl.jofl_pkg$extract_varchar(p_attr, 'norm_name', true);
   l_r.norm_desc := jofl.jofl_pkg$extract_varchar(p_attr, 'norm_desc', true);
   l_r.is_current := jofl.jofl_pkg$extract_boolean(p_attr, 'is_current', true);
   l_r.hour_period := jofl.jofl_pkg$extract_number(p_attr, 'hour_period', true);
   l_r.dt_begin := jofl.jofl_pkg$extract_date(p_attr, 'dt_begin', true)::TIMESTAMP;
   l_r.dt_end := jofl.jofl_pkg$extract_date(p_attr, 'dt_end', true)::TIMESTAMP;
   l_r.sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'sign_deleted', true);
   l_r.update_date := jofl.jofl_pkg$extract_date(p_attr, 'update_date', true);
   l_r.is_active := jofl.jofl_pkg$extract_boolean(p_attr, 'is_active', true);
 --l_r.route_num := jofl.jofl_pkg$extract_varchar(p_attr, 'route_num', true);
   return l_r;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_norm_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare
  l_route_variant_id rts.route_variant.route_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'route_variant_id', true);
begin
 open p_rows for
 select
   norm.norm_id,
   norm.calculation_task_id,
   norm.norm_name,
   norm.norm_desc,
   norm.is_current,
   norm.hour_period,
   norm.dt_begin,
   norm.dt_end,
   norm.sign_deleted,
   norm.sys_period,
   ct.name as calc_task_name,
   norm.update_date,
   is_active,
   (select string_agg(r.route_num, ', ')
     from asd.route_variant2ct r2ct
     join asd.calculation_task ct on ct.calculation_task_id = r2ct.calculation_task_id
     join rts.route_variant rv on rv.route_variant_id = r2ct.route_variant_id
     join rts.route r on r.route_id = rv.route_id
     join ttb.norm n ON n.calculation_task_id = ct.calculation_task_id
    WHERE norm_id = norm.norm_id) as route_num
 from ttb.norm norm
   left join asd.calculation_task ct on ct.calculation_task_id = norm.calculation_task_id
 where
   l_route_variant_id is null or
                               exists (select 1
                                       from asd.route_variant2ct rvct
                                       where rvct.route_variant_id= l_route_variant_id
                                         and rvct.calculation_task_id= norm.calculation_task_id)
  ;


end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_norm_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
   l_r ttb.norm%rowtype;
begin
  return null;

   l_r := ttb.jf_norm_pkg$attr_to_rowtype(p_attr);

   update ttb.norm set
          calculation_task_id = l_r.calculation_task_id,
          norm_name = l_r.norm_name,
          norm_desc = l_r.norm_desc,
          is_current = l_r.is_current,
          hour_period = l_r.hour_period,
          dt_begin = l_r.dt_begin,
          dt_end = l_r.dt_end,
          sign_deleted = l_r.sign_deleted,
          update_date = l_r.update_date,
          is_active = l_r.update_date
   where
          norm_id = l_r.norm_id;

   return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_norm_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
   l_r ttb.norm%rowtype;
begin
   l_r := ttb.jf_norm_pkg$attr_to_rowtype(p_attr);

   DELETE FROM ttb.norm_round where norm_id = l_r.norm_id;
   DELETE FROM ttb.norm_between_stop where norm_id = l_r.norm_id;
   DELETE FROM ttb.norm_stop where norm_id = l_r.norm_id;
   -- *******************************************************
   delete from  ttb.norm where  norm_id = l_r.norm_id;

   return null;
end;
$function$
;

CREATE OR REPLACE FUNCTION ttb."jf_norm_pkg$of_set_active"(p_id_account numeric, p_attr text)
  RETURNS text
LANGUAGE plpgsql
AS $function$
declare
  l_r ttb.norm%rowtype;
begin
  l_r := ttb.jf_norm_pkg$attr_to_rowtype(p_attr);

  UPDATE ttb.norm t1 SET is_active = CASE WHEN is_active = FALSE OR is_active is null THEN TRUE
                                       else FALSE
                                  END
  WHERE t1.norm_id = l_r.norm_id;

  return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_norm_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
   l_r ttb.norm%rowtype;
begin
   l_r := ttb.jf_norm_pkg$attr_to_rowtype(p_attr);

  INSERT INTO ttb.norm
  ( calculation_task_id, norm_name, is_current, hour_period, dt_begin, dt_end, norm_desc, update_date, is_active)
  VALUES
  ( l_r.calculation_task_id, l_r.norm_name, l_r.is_current,
    l_r.hour_period, l_r.dt_begin, l_r.dt_end, l_r.norm_desc, l_r.update_date, l_r.is_active);

   return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_norm_pkg$of_create_from_calc"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
 declare
   l_r ttb.norm%rowtype;

   l_utc_offset INTERVAL := ttb.get_utc_offset();
 begin
   l_r.calculation_task_id := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true);
   l_r.norm_name := jofl.jofl_pkg$extract_varchar(p_attr, 'norm_name', true);
   l_r.norm_desc := jofl.jofl_pkg$extract_varchar(p_attr, 'norm_desc', true);


   insert into ttb.norm(calculation_task_id,hour_period,dt_begin,dt_end,norm_name,norm_desc)
   select ct.calculation_task_id, ct.hour_period, n.calc_period_begin, n.calc_period_end, l_r.norm_name, l_r.norm_desc
   from asd.calculation_task ct
   join asd.ct_normative n on n.calculation_task_id = ct.calculation_task_id
   where ct.calculation_task_id = l_r.calculation_task_id
   returning norm_id into l_r.norm_id;


  insert into ttb.norm_round( norm_id,
                             round_id ,
                             tr_type_id,
                             tr_capacity_id,
                             stop_item_id ,
                             hour_from,
                             hour_to,
                             min_inter_round_stop_dur,
                             reg_inter_round_stop_dur,
                             round_duration,
                             norm_edit_type_id)

  select l_r.norm_id,
         c.round_id,
         c.tr_type_id,
         c.tr_capacity_id,
         sir.stop_item_id,
         (c.hour_from::time + l_utc_offset)::TIME hour_from,
         (c.hour_to::time + l_utc_offset)::TIME hour_to,
         /*21.02.2018 #23235
         При формировании нормативов отключить автоматический расчет минимальной межрейсовой стоянки.
          --case ceil(c.min_inter_round_stop_dur/60::real)*60 when 0 then asd.get_constant('MIN_INTER_RACE_STOP')::int else ceil(c.min_inter_round_stop_dur/60::real)*60 end, --округляем до минут
          */
         /* #23235 asd.round_seconds2min(c.min_inter_round_stop_dur::NUMERIC)*/
         CASE WHEN rnd.move_direction_id = 2 THEN asd.get_constant('MIN_INTER_RACE_STOP')
              ELSE 0
         END,
         asd.round_seconds2min(reg_inter_round_stop_dur::NUMERIC),
         --coalesce(c.round_duration,0) ,
         0,
         ttb.norm_edit_type$auto()
    from asd.ct_round_stop_result c
      join rts.round rnd on rnd.round_id = c.round_id and not rnd.sign_deleted
      join rts.stop_item2round sir on sir.round_id = rnd.round_id and sir.order_num = (select max(t.order_num) from rts.stop_item2round t where t.round_id = rnd.round_id and not t.sign_deleted) and not sir.sign_deleted
    where c.calculation_task_id = l_r.calculation_task_id;

/*-----------------------------ВРЕМЯ НА ОП ------------------------------------------*/
   --Заполним время на ОП из расчета
    insert into ttb.norm_stop(
                             norm_id,
                           tr_type_id,
                           tr_capacity_id,
                           stop_item_id,
                           hour_from,
                           hour_to,
                           stop_dur,
                           norm_edit_type_id)

    select
      l_r.norm_id,
      c.tr_type_id,
      c.tr_capacity_id,
      sir.stop_item_id,
      (c.hour_from + l_utc_offset)::TIME hour_from,
      (c.hour_to + l_utc_offset)::TIME hour_to,
      coalesce(asd.round_seconds2min((avg(c.stop_duration)/60::real*60)::NUMERIC), 0),
      ttb.norm_edit_type$auto()
    from asd.ct_stop_dur_result c
    join rts.stop_item2round sir on sir.stop_item2round_id = c.stop_item2round_id
    where c.calculation_task_id = l_r.calculation_task_id
  group by c.tr_type_id,
      c.tr_capacity_id,
      sir.stop_item_id,
      c.hour_from,
      c.hour_to;

   --заполним время на тех ОП, где нет расчета, значениеями по умолчанию
   insert into ttb.norm_stop(
     norm_id,
     tr_type_id,
     tr_capacity_id,
     stop_item_id,
     hour_from,
     hour_to,
     stop_dur,
     norm_edit_type_id)
   with setHour as (
       select (generate_series(1,(24/(ct.hour_period/60))::int, 1) * (ct.hour_period||' minute')::interval)::time as tm ,row_number() over() rn
       from  asd.calculation_task ct
       where ct.calculation_task_id= l_r.calculation_task_id
   ),
       per as (select tm
                 ,lead(tm,1,'24:00:00') over (Partition by rn order by tm) next_tm
               from setHour)

   select
     l_r.norm_id
     ,tr2ct.tr_type_id
     ,vt2ct.tr_capacity_id
     ,sir.stop_item_id
     ,(per.tm + l_utc_offset)::TIME tm
     ,(per.next_tm + l_utc_offset)::TIME next_tm
     ,asd.round_seconds2min(((asd.get_constant('MIN_STOP_TIME')/60::real)*60)::NUMERIC)
     ,ttb.norm_edit_type$default()
   from asd.calculation_task ct
     join asd.ct_normative norm on norm.calculation_task_id = ct.calculation_task_id
     join asd.tr_type2ct tr2ct on tr2ct.calculation_task_id = ct.calculation_task_id
     join asd.route_variant2ct r2ct on r2ct.calculation_task_id = ct.calculation_task_id
     join rts.round rnd on rnd.route_variant_id = r2ct.route_variant_id and not rnd.sign_deleted
     join rts.stop_item2round sir on sir.round_id = rnd.round_id and not sir.sign_deleted
     join asd.tr_capacity2ct vt2ct on vt2ct.calculation_task_id = ct.calculation_task_id
     join per on 1=1
   where ct.calculation_task_id = l_r.calculation_task_id
         and not exists (select 1
                         from ttb.norm_stop ns
                         where ns.hour_from = (per.tm + l_utc_offset)::TIME
                               and ns.hour_to = (per.next_tm + l_utc_offset)::TIME
                               and ns.tr_capacity_id = vt2ct.tr_capacity_id
                               and ns.stop_item_id = sir.stop_item_id
                               and ns.tr_type_id = tr2ct.tr_type_id
                               and ns.norm_id = l_r.norm_id )
   group by
     per.tm
     ,per.next_tm
     ,tr2ct.tr_type_id
     ,vt2ct.tr_capacity_id
     ,sir.stop_item_id;
   /*-----------------------------------------------------------------------------------*/


   /*-----------------------------ВРЕМЯ МЕЖДУ ДВУМЯ ОП ------------------------------------------*/
   insert into ttb.norm_between_stop(
                                   norm_id,
                                   tr_type_id,
                                   tr_capacity_id,
                                   stop_item_1_id ,
                                   stop_item_2_id ,
                                   hour_from,
                                   hour_to,
                                   between_stop_dur,
                                   norm_edit_type_id)
    select
      l_r.norm_id,
      c.tr_type_id,
      c.tr_capacity_id,
      sir_1.stop_item_id,
      sir_2.stop_item_id,
      (c.hour_from::time + l_utc_offset)::TIME hour_from,
      (c.hour_to::time + l_utc_offset)::TIME hour_to,
      coalesce( asd.round_seconds2min(((avg(c.norm_duration)/60::real)*60)::NUMERIC), 0),
      ttb.norm_edit_type$auto()
    from asd.ct_stop_visit_result c
    join rts.stop_item2round sir_1 on sir_1.stop_item2round_id = c.stop_item2round_1_id
    join rts.stop_item2round sir_2 on sir_2.stop_item2round_id = c.stop_item2round_2_id
    where c.calculation_task_id = l_r.calculation_task_id
  group by c.tr_type_id,
      c.tr_capacity_id,
      sir_1.stop_item_id,
      sir_2.stop_item_id,
      c.hour_from,
      c.hour_to;

   /*---------------------------------------------------------------------------------*/
   insert into ttb.norm_between_stop(
               norm_id,
               tr_type_id,
               tr_capacity_id,
               stop_item_1_id ,
               stop_item_2_id ,
               hour_from,
               hour_to,
               between_stop_dur,
               norm_edit_type_id)
   with setHour as (
       select (generate_series(1,(24/(ct.hour_period/60))::int, 1) * (ct.hour_period||' minute')::interval)::time as tm ,row_number() over() rn
       from  asd.calculation_task ct
       where ct.calculation_task_id= l_r.calculation_task_id
   ),
       per as (select tm
                 ,lead(tm,1,'24:00:00') over (Partition by rn order by tm) next_tm
               from setHour)
     ,
       adata as
     (
         select

           tr2ct.tr_type_id
           ,vt2ct.tr_capacity_id
           ,lag(sir.stop_item_id)  over (partition by tr2ct.tr_type_id ,vt2ct.tr_capacity_id, sir.round_id order by sir.order_num) as stop_item_1_id
           ,sir.stop_item_id as stop_item_2_id
           ,rnd.round_id
           ,sir.order_num
           ,sir.length_sector
         from asd.calculation_task ct
           join asd.ct_normative norm on norm.calculation_task_id = ct.calculation_task_id
           join asd.tr_type2ct tr2ct on tr2ct.calculation_task_id = ct.calculation_task_id
           join asd.route_variant2ct r2ct on r2ct.calculation_task_id = ct.calculation_task_id
           join rts.round rnd on rnd.route_variant_id = r2ct.route_variant_id and not rnd.sign_deleted
           join rts.stop_item2round sir on sir.round_id = rnd.round_id and not sir.sign_deleted
           join asd.tr_capacity2ct vt2ct on vt2ct.calculation_task_id = ct.calculation_task_id
         where ct.calculation_task_id = l_r.calculation_task_id
     )
     , itog as (
       select tr_type_id
         , tr_capacity_id
         , stop_item_1_id
         , stop_item_2_id
         , min (length_sector) as length_sector
       /*11.01.18 выявлено, что на разных траекториях длина м/у ОП может быть различной
         по согласованию с Ефимовой Марией принято решение брать минимальную длину*/
       from adata
       where stop_item_1_id is not null
       group by  tr_type_id,     tr_capacity_id,     stop_item_1_id,     stop_item_2_id
      )
   select  l_r.norm_id
     , tr_type_id
     , tr_capacity_id
     , stop_item_1_id
     , stop_item_2_id
     , (per.tm + l_utc_offset)::TIME as tm
     , (per.next_tm + l_utc_offset)::TIME as next_tm
     , --asd.round_seconds2min( ((length_sector/asd.get_constant('DEFAULT_SPEED')::real)*60 )::NUMERIC )
       CASE WHEN asd."jf_norm_pkg$calc_time_between_stops"(l_r.calculation_task_id, stop_item_1_id, stop_item_2_id, tr_type_id, per.tm, per.next_tm, tr_type_id)
                 IS NOT NULL THEN asd."jf_norm_pkg$calc_time_between_stops"(l_r.calculation_task_id, stop_item_1_id, stop_item_2_id, tr_type_id, per.tm, per.next_tm, tr_type_id)
            ELSE asd.round_seconds2min( ((length_sector/asd.get_constant('DEFAULT_SPEED')::real)*60 )::NUMERIC )
        END
     , ttb.norm_edit_type$default()
   from itog adata
     join per on 1=1
                 and not exists (select 1
                                 from ttb.norm_between_stop nbs
                                 where nbs.hour_from = (per.tm + l_utc_offset)::TIME
                                       and nbs.hour_to = (per.next_tm + l_utc_offset)::TIME
                                       and nbs.tr_capacity_id = adata.tr_capacity_id
                                       and nbs.stop_item_1_id = adata.stop_item_1_id
                                       and nbs.stop_item_2_id = adata.stop_item_2_id
                                       and nbs.tr_type_id = adata.tr_type_id
                                       and nbs.norm_id = l_r.norm_id
     );


   /*--------------------------------------------------------------------------*/
   -- заполнить продолжительность рейса суммой продолжительности м/у ОП

   /*with nr as (
       select *
       from ttb.norm_round nr
       where nr.norm_id  = l_r.norm_id
   ),
       d as
     (
         select sum(t.between_stop_dur) AS round_duration_NEW,   nr.tr_type_id tr_type_id1 ,
         nr.tr_capacity_id tr_capacity_id1, nr.hour_from hour_from1 , nr.round_id round_id1,  nr.norm_id norm_id1
         from ttb.norm_between_stop t
           join nr on nr.norm_id  = t.norm_id
                      and t.tr_type_id= nr.tr_type_id
                      and t.tr_capacity_id =nr.tr_capacity_id
                      and t.hour_from  = nr.hour_from

           join rts.stop_item2round sir1 on sir1.round_id =nr.round_id and sir1.stop_item_id = t.stop_item_1_id
           join rts.stop_item2round sir2 on sir2.round_id =nr.round_id and sir2.stop_item_id = t.stop_item_2_id
         group by nr.tr_type_id , nr.tr_capacity_id, nr.hour_from , nr.round_id, t.hour_from, nr.norm_id

     )*/
   with a AS(
       select DISTINCT on (round_id) round_id, code  from ttb.norm_round
         JOIN rts.round USING(round_id)
       where norm_id = l_r.norm_id
   ),
       t2 AS(
         select round_id, order_num, stop_item_id,
           lead(stop_item_id) OVER (PARTITION BY round_id order by order_num), code
         from a
           JOIN rts.stop_item2round USING (round_id)
     ),
       t3 AS(
         select * from t2
           JOIN ttb.norm_between_stop nbs ON t2.stop_item_id = nbs.stop_item_1_id
           AND t2.lead = nbs.stop_item_2_id
          AND nbs.norm_id = l_r.norm_id
     ),
     d AS(
   select distinct on (round_id, tr_capacity_id, hour_from, hour_to)
     tr_type_id tr_type_id1, tr_capacity_id tr_capacity_id1, hour_from hour_from1, round_id round_id1, norm_id norm_id1,
     sum(between_stop_dur) over (PARTITION BY round_id, tr_capacity_id, tr_type_id, hour_from, hour_to) round_duration_NEW
   from t3
   )

   update ttb.norm_round t
   set round_duration =
   coalesce((
              select  d.round_duration_NEW from d
              where
                norm_id = d.norm_id1
                and round_id  = d.round_id1
                and tr_type_id= d.tr_type_id1
                and tr_capacity_id =d.tr_capacity_id1
                and hour_from  = d.hour_from1
            ),0)
   where norm_id = l_r.norm_id;
   return null;
 end;
 $function$
;

create or REPLACE function ttb."jf_norm_pkg$of_copy"  (p_id_account numeric, p_attr text) returns boolean
LANGUAGE plpgsql
AS $$
DECLARE
  l_norm_id ttb.norm.norm_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true);
  l_next_norm_id INTEGER = nextval('ttb.norm_norm_id_seq'::regclass);
BEGIN

  INSERT INTO ttb.norm
  select l_next_norm_id,calculation_task_id,'Копия. '|| norm_name as norm_name,is_current,hour_period,
         dt_begin,dt_end,sign_deleted,sys_period,'Копия. '||norm_desc as norm_desc,now() as update_date FROM ttb.norm
  WHERE norm_id = l_norm_id;

  --- ------------------------------------ --------------------------------------------------------------
  INSERT INTO ttb.norm_between_stop
  select nextval('ttb.norm_between_stop_norm_between_stop_id_seq'::regclass) as norm_between_stop_id,
    l_next_norm_id as norm_id, hour_from,hour_to,between_stop_dur,tr_capacity_id,stop_item_1_id,
    stop_item_2_id,tr_type_id,norm_edit_type_id,sys_period
  from ttb.norm_between_stop
  where norm_id = l_norm_id;

  INSERT INTO ttb.norm_round
  select nextval('ttb.norm_round_norm_round_id_seq'::regclass) as norm_round_id, l_next_norm_id as norm_id,
    round_id,hour_from,hour_to,min_inter_round_stop_dur,reg_inter_round_stop_dur,round_duration,
    tr_capacity_id,stop_item_id,tr_type_id,norm_edit_type_id,sys_period
  FROM ttb.norm_round
  where norm_id = l_norm_id;

  return TRUE;
END;
$$;

-- расчете среднего времени посещения ребра графа
-- входные параметры - id секции графа, время в часа С и ПО, за которое вычисляется
-- дни недели и период берутся из задачи расчета нормативов
CREATE OR REPLACE FUNCTION asd."jf_norm_pkg$calc_avg_dur_on_graph_section"(
  p_ct_id BIGINT, p_graph_section_id BIGINT, p_hour_from TIME, p_hour_to TIME, p_tr_capacity_id BIGINT, p_tr_type_id BIGINT)
  RETURNS float
LANGUAGE plpgsql
AS $function$
declare
  l_date_from DATE;
  l_date_to DATE;
  l_avg_dur FLOAT;
  l_days SMALLINT[];
begin
  -- период (даты) за который усредняется скорость
  select hour_from, hour_to INTO l_date_from, l_date_to from asd.hours2ct where calculation_task_id = p_ct_id;
  -- дни недели для расчета
  l_days = array(select week_day_id from asd.week_day2ct where calculation_task_id = p_ct_id);

  WITH c AS(
      select graph_section_id ,unnest(visit_duration) as dur
      from asd.agg_gs_speed gss
      JOIN rts.round round USING(round_id)
      JOIN rts.route_variant rv USING(route_variant_id)
      JOIN rts.route r USING(route_id)
      where gss.visit_hour BETWEEN l_date_from AND l_date_to
            AND visit_hour::TIME BETWEEN p_hour_from/*'10:00:00'*/ AND p_hour_to/*'10:00:00' + INTERVAL '1 hour'*/
            AND EXTRACT(ISODOW FROM gss.visit_hour) = ANY(l_days)
            AND graph_section_id = p_graph_section_id
            AND tr_capacity_id = p_tr_capacity_id
            AND r.tr_type_id = p_tr_type_id
  )
  select avg(dur) INTO l_avg_dur from c GROUP BY graph_section_id;
  return l_avg_dur;
end;
$function$;

-- вычисляет время на прохождение перегона между остановкаи, используя для этого время посещения ребер графа НГПТ,
-- соединяющие две здананные остановки (на определенном round_id)
-- остальные параметры берутся задачи расчета норматива (дни недели, период и прочее)
CREATE OR REPLACE FUNCTION asd."jf_norm_pkg$calc_time_between_stops"(
  p_ct_id BIGINT, p_stop_item_id1 BIGINT, p_stop_item_id2 BIGINT, /*p_round_id bigint,*/
  p_tr_capacity_id BIGINT, p_t_from TIME, p_t_to TIME, p_tr_type_id BIGINT)
  RETURNS float
LANGUAGE plpgsql
AS $function$
declare
  /*l_date_from DATE;
  l_date_to DATE;
  l_avg_dur FLOAT;
  l_days SMALLINT[];*/
  l_time_between float;
  l_round_id BIGINT;
begin
  -- какая нибудь траектория, на которой лежат эти две остановки
  with A As(
      select rnd.round_id, array_agg(stop_item_id) /*rnd.code, sir.**/
      from asd.calculation_task ct
        join asd.ct_normative norm on norm.calculation_task_id = ct.calculation_task_id
        join asd.tr_type2ct tr2ct on tr2ct.calculation_task_id = ct.calculation_task_id
        join asd.route_variant2ct r2ct on r2ct.calculation_task_id = ct.calculation_task_id
        join rts.round rnd on rnd.route_variant_id = r2ct.route_variant_id and not rnd.sign_deleted
        join rts.stop_item2round sir on sir.round_id = rnd.round_id and not sir.sign_deleted
      where ct.calculation_task_id = p_ct_id
      GROUP by rnd.round_id
  )
  select round_id INTO l_round_id from A
  where p_stop_item_id1 = ANY(array_agg) AND p_stop_item_id2 = ANY(array_agg) LIMIT 1;
  IF l_round_id IS NULL THEN
    RETURN NULL;
  END IF;
  with a AS (
      SELECT
        r.route_num,
        round.route_variant_id,
        round.round_id,
        code,
        move_direction_id,
        gs2r.graph_section_id,
        gs2r.order_num,
        gs.length
      --sum(gs.length) -- 9739 м для round_id = 31858, больше потому что не учтено смещение на 1 и последней остановках
      FROM rts.route r
        JOIN rts.round round ON round.route_variant_id = r.current_route_variant_id
        JOIN rts.graph_sec2round gs2r USING(round_id)
        JOIN rts.graph_section gs USING(graph_section_id)
      WHERE round_id = l_round_id
      --ORDER BY gs2r.order_num
  ),
      b AS (
        select order_num as stop_order_num, graph_section_id,
               lead(graph_section_id) OVER (PARTITION BY round_id order by order_num) as lead_graph_section_id,
               lead(graph_section_offset) OVER (PARTITION BY round_id order by order_num) as lead_gs_offset,
          length_sector, graph_section_offset,
          stop_item_id
        from rts.stop_item2round si2r
          JOIN rts.stop_item si USING(stop_item_id)
          JOIN rts.stop_location sl USING(stop_location_id)
        WHERE round_id = l_round_id
    ),
    --select * from  b;
      c AS (
        select *,
          (select order_num from a where graph_section_id = b.graph_section_id LIMIT 1) gs_order_num,
          (select order_num from a where graph_section_id = b.lead_graph_section_id LIMIT 1) next_gs_order_num from b
        where stop_item_id = p_stop_item_id1
    ),
      d AS (
        select *, array(select graph_section_id from a where order_num BETWEEN gs_order_num AND next_gs_order_num order by order_num) gs_ids
        from c where next_gs_order_num IS NOT NULL
    ),
      f AS(
        select *, array_length(d.gs_ids, 1) a_size from d, unnest(d.gs_ids) WITH ORDINALITY
    ),
      g AS(
        select graph_section_id,stop_order_num, length_sector, graph_section_offset, lead_gs_offset, gs_order_num,
          next_gs_order_num, unnest, ordinality, a_size,
          (select gs.length from rts.graph_section gs where gs.graph_section_id = unnest AND NOT gs.sign_deleted LIMIT 1),
          asd."jf_norm_pkg$calc_avg_dur_on_graph_section"(p_ct_id, unnest, p_t_from, p_t_to, p_tr_capacity_id, p_tr_type_id) as avg_time
        --asd."jf_norm_pkg$calc_agg_gss"(p_ct_id, unnest, '10:00:00', '14:00:00') as avg_time
        from f),
      gg AS (select *,
               -- две остановке на одной дуге
               CASE when a_size = 1 THEN lead_gs_offset - graph_section_offset
               -- ОП на последней дуге
               WHEN a_size = ordinality THEN lead_gs_offset
               -- первая остановка на дуге
               WHEN (a_size > 1) AND (ordinality = 1) THEN length - graph_section_offset
               ELSE length
               END as dlina from g
    ),
      ff AS(
        select *, coalesce(dlina/length,1) * avg_time as time_on_gs from gg
    ),
      final_table AS(
        select *, sum(time_on_gs) OVER w as time_between,
          -- нужно чтобы понять, есть среди суммируемых элементов NULL
                  sum(CASE WHEN time_on_gs IS NULL THEN 0 ELSE 1 END ) OVER w as num_not_null
        from ff WINDOW w AS (PARTITION BY stop_order_num)
    ),
    --select sum(dlina) from final_table ; -- проверка длины траектории
      time_between AS(
        select distinct on (stop_order_num) * from final_table
    )
  select /*sum(time_between)*/
    -- сумма не нужна, потому, что для одной остановки
    CASE WHEN num_not_null <> a_size THEN NULL
    ELSE time_between END
  INTO l_time_between from time_between;
  RETURN l_time_between;
end;
$function$;