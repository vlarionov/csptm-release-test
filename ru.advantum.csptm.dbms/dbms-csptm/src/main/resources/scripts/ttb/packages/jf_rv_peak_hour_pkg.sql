CREATE OR REPLACE FUNCTION ttb.jf_rv_peak_hour_pkg$attr_to_rowtype(p_attr TEXT)
    RETURNS ttb.RV_PEAK_HOUR
LANGUAGE plpgsql
AS $function$
DECLARE
    l_r ttb.rv_peak_hour%ROWTYPE;
BEGIN
    l_r.rv_peak_hour_id := jofl.jofl_pkg$extract_number(p_attr, 'rv_peak_hour_id', TRUE);
    l_r.calendar_tag_id := jofl.jofl_pkg$extract_number(p_attr, 'calendar_tag_id', TRUE);
    l_r.route_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'route_variant_id', TRUE);
    l_r.time_begin := jofl.jofl_pkg$extract_number(p_attr, 'time_begin', TRUE);
    l_r.time_end := jofl.jofl_pkg$extract_number(p_attr, 'time_end', TRUE);
    l_r.peak_hour_name := jofl.jofl_pkg$extract_varchar(p_attr, 'peak_hour_name', TRUE);
    IF (l_r.time_begin > l_r.time_end)
    THEN
        RAISE EXCEPTION '<<Время окончание должно быть быльше времени начала!>>';
    END IF;
    RETURN l_r;
END;
$function$;

-------------------
CREATE OR REPLACE FUNCTION ttb."jf_rv_peak_hour_pkg$of_rows"(p_id_account NUMERIC,
    OUT                                                      p_rows       REFCURSOR, p_attr TEXT)
    RETURNS REFCURSOR
LANGUAGE plpgsql
AS $function$
DECLARE
    l_route_variant_id ttb.rv_peak_hour.route_variant_id%TYPE:=jofl.jofl_pkg$extract_number(p_attr, 'route_variant_id', TRUE);
BEGIN
    OPEN p_rows FOR
    SELECT
        t.rv_peak_hour_id AS rv_peak_hour_id,
        tc.tag_name       AS ph_name,
        t.route_variant_id,
        t.time_begin,
        tc.calendar_tag_id,
        t.time_end,
        t.peak_hour_name
    FROM ttb.rv_peak_hour t
        JOIN ttb.calendar_tag tc ON tc.calendar_tag_id = t.calendar_tag_id
    WHERE t.route_variant_id = l_route_variant_id;
END;
$function$;
---------------------------------------------

CREATE OR REPLACE FUNCTION ttb."jf_rv_peak_hour_pkg$of_update"(p_id_account NUMERIC, p_attr TEXT)
    RETURNS TEXT
LANGUAGE plpgsql
AS $function$
DECLARE
    l_r ttb.rv_peak_hour%ROWTYPE;
BEGIN
    l_r := ttb.jf_rv_peak_hour_pkg$attr_to_rowtype(p_attr);

    IF exists(
        SELECT
            t.calendar_tag_id,
            t.peak_hour_name,
            t.route_variant_id,
            t.rv_peak_hour_id
        FROM ttb.rv_peak_hour t
        WHERE t.calendar_tag_id = l_r.calendar_tag_id AND
              t.peak_hour_name = l_r.peak_hour_name AND
              t.route_variant_id = l_r.route_variant_id AND
              t.rv_peak_hour_id <> l_r.rv_peak_hour_id
    )
    THEN
        RAISE EXCEPTION '<<Такая пара Признак-Наименование уже существует в данном маршруте!>>';
    ELSE
        UPDATE ttb.rv_peak_hour
        SET
            calendar_tag_id = l_r.calendar_tag_id,
            time_begin      = l_r.time_begin,
            time_end        = l_r.time_end,
            peak_hour_name  = l_r.peak_hour_name
        WHERE
            rv_peak_hour_id = l_r.rv_peak_hour_id;
    END IF;
    RETURN NULL;
END;
$function$;
-------------------
CREATE OR REPLACE FUNCTION ttb."jf_rv_peak_hour_pkg$of_delete"(p_id_account NUMERIC, p_attr TEXT)
    RETURNS TEXT
LANGUAGE plpgsql
AS $function$
DECLARE
    l_rv_peak_hour_id ttb.rv_peak_hour.rv_peak_hour_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'rv_peak_hour_id', TRUE);
BEGIN
    DELETE FROM ttb.rv_peak_hour
    WHERE rv_peak_hour_id = l_rv_peak_hour_id;
    RETURN NULL;
END;
$function$;
-------------------

CREATE OR REPLACE FUNCTION ttb."jf_rv_peak_hour_pkg$of_insert"(p_id_account NUMERIC, p_attr TEXT)
    RETURNS TEXT
LANGUAGE plpgsql
AS $function$
DECLARE
    l_r ttb.rv_peak_hour%ROWTYPE;
BEGIN
    l_r := ttb.jf_rv_peak_hour_pkg$attr_to_rowtype(p_attr);

    IF exists(
        SELECT
            r.peak_hour_name,
            r.calendar_tag_id,
            r.route_variant_id
        FROM
            ttb.rv_peak_hour r
        WHERE r.calendar_tag_id = l_r.calendar_tag_id AND
              r.peak_hour_name = l_r.peak_hour_name AND
              r.route_variant_id = l_r.route_variant_id
    )
    THEN
        RAISE EXCEPTION '<<Такая пара Признак-Наименование уже существует в этом маршруте!>>';
    ELSE
        INSERT INTO ttb.rv_peak_hour (calendar_tag_id, route_variant_id, time_begin, time_end, peak_hour_name)
        VALUES (l_r.calendar_tag_id, l_r.route_variant_id, l_r.time_begin, l_r.time_end,
                l_r.peak_hour_name);
    END IF;
    RETURN NULL;
END;
$function$;