﻿CREATE OR REPLACE FUNCTION ttb.jf_stop_item2round4select_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
  l_tt_variant_id 	ttb.tt_variant.tt_variant_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
  l_action_type_id 	ttb.action_type.action_type_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'action_type_id', true);
begin
 open p_rows for
    with q as ( 
            select
             act.action_type_name,
             rnd.code as round_code,
             rnd.round_id,
             rnd.move_direction_id,
             sir.order_num,
             max(sir.order_num) over (partition by sir.round_id) last_order_num,
             s.name as stop_name,
             sir.stop_item2round_id,
             mv.move_direction_name,
             rnd.action_type_id
           from ttb.tt_at_round_stop trnd_sp
             join ttb.tt_at_round tat_rnd on trnd_sp.tt_at_round_id = tat_rnd.tt_at_round_id
             join ttb.tt_action_type tact on tact.tt_action_type_id =  tat_rnd.tt_action_type_id
             join ttb.action_type act on act.action_type_id = tact.action_type_id
             join rts.stop_item2round sir on sir.stop_item2round_id = trnd_sp.stop_item2round_id
             join rts.stop_item si on si.stop_item_id =sir.stop_item_id
             join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
             join rts.stop s on s.stop_id = sl.stop_id
             join rts.round rnd on rnd.round_id = sir.round_id
             join rts.move_direction mv on mv.move_direction_id = rnd.move_direction_id
           where 	   tact.action_type_id = rnd.action_type_id
                 and tact.tt_variant_id = l_tt_variant_id
                 and not exists (select 1 from ttb.tt_at_stop tat_sp
                           join ttb.tt_action_type tat on tat_sp.tt_action_type_id = tat.tt_action_type_id  
                                                      and tat.tt_variant_id = l_tt_variant_id
                           where tat.action_type_id = rnd.action_type_id
                           and tat_sp.stop_item2round_id = sir.stop_item2round_id)
           order by tact.action_type_id, tat_rnd.tt_at_round_id, sir.order_num)
       select  q.action_type_name,
               q.round_code,
               q.round_id,
               q.move_direction_id,
               q.order_num,
               q.last_order_num,
               q.stop_name,
               q.stop_item2round_id,
               q.move_direction_name,
               q.action_type_id
        from q
       where q.order_num = q.last_order_num
       ;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;