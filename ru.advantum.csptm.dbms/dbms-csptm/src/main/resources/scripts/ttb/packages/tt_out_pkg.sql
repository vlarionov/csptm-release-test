/*ВНИМАНИЕ! ВНИЗУ ПЕРЕОПРЕДЕЛЯЕТСЯ В ДИНАМИЧЕСКИЙ SQL*/
CREATE OR REPLACE FUNCTION ttb.tt_out_pkg$get_set4out_action_agg (
  p_tt_variant_id integer
)
RETURNS TABLE (
  tt_variant_id integer,
  tt_out_id integer,
  tt_out_num smallint,
  agg_time_begin timestamp,
  agg_time_end timestamp,
  min_bn_int double precision,
  max_bn_int double precision,
  tt_action_id integer,
  action_dur integer,
  action_type_code text,
  action_type_name text,
  move_direction_id smallint,
  move_direction_name text,
  r_code text,
  action_type_id smallint,
  is_production_round boolean,
  is_technical_round boolean,
  is_bn_round_break boolean,
  is_dinner boolean,
  is_pause boolean,
  is_break boolean,
  is_round boolean,
  stop_b text,
  stop_e text,
  stop_id_b integer,
  stop_id_e integer,
  dr_shift_id smallint,
  mode_id smallint,
  tt_action_group_code text,
  tt_action_group_nn integer,
  ref_group_kind_id smallint,
  round_id integer,
  norm_id integer,
  stop_item_id integer
) AS
$body$
begin
  RETURN QUERY
  with recursive t (tt_variant_id , tt_out_id ,       tr_capacity_id ,    mode_id  ,   tt_out_num   ,  parent_tt_out_id ,  path, cycle, level)  as (
    select tout.tt_variant_id ,  tout.tt_out_id ,       tout.tr_capacity_id ,    tout.mode_id  ,   tout.tt_out_num   ,  tout.parent_tt_out_id,  ARRAY[tout.tt_out_id], false , 1
    from    ttb.tt_out tout
    where tout.tt_variant_id = p_tt_variant_id
          and not tout.sign_deleted
    union
    select tout.tt_variant_id ,  tout.tt_out_id ,       tout.tr_capacity_id ,    tout.mode_id  ,   tout.tt_out_num   ,  tout.parent_tt_out_id,  path || tout.tt_out_id, tout.tt_out_id = ANY(path), t.level + 1
    from ttb.tt_variant v
      join ttb.tt_out tout on tout.tt_variant_id = v.tt_variant_id
      join t on  tout.parent_tt_out_id = t.tt_out_id
    where not tout.sign_deleted
          and v.parent_tt_variant_id is null
          and t.level<10
  )

    ,
      trev (tt_variant_id , tt_out_id ,       tr_capacity_id ,    mode_id  ,   tt_out_num   ,  parent_tt_out_id ,   path, cycle, level)    as (
      select tout.tt_variant_id ,  tout.tt_out_id ,       tout.tr_capacity_id ,    tout.mode_id  ,   tout.tt_out_num   ,  tout.parent_tt_out_id,   ARRAY[tout.tt_out_id], false, 1
      from ttb.tt_out tout
      where tout.tt_variant_id = p_tt_variant_id
            and not tout.sign_deleted
      union
      select tout.tt_variant_id ,  tout.tt_out_id ,       tout.tr_capacity_id ,    tout.mode_id  ,   tout.tt_out_num   ,  tout.parent_tt_out_id,  path || tout.tt_out_id, tout.tt_out_id = ANY(path), level+1
      from ttb.tt_variant v
        join ttb.tt_out tout on tout.tt_variant_id = v.tt_variant_id
        join trev on trev.parent_tt_out_id = tout.tt_out_id
      where not tout.sign_deleted
            and v.parent_tt_variant_id is null
            and level<10

    )
    ,all_out as (
    select * from t
    where not cycle
    union
    select * from trev
    where not cycle
  )
    ,
      act as (
        select vma.action_type_id
          ,vma.action_type_code
          ,vma.action_type_name
          ,vma.is_production_round
          ,vma.is_technical_round
          ,vma.is_dinner
          ,vma.is_break
          ,vma.is_round
          --,ttb.action_type_pkg$is_bn_round_break(act.action_type_id) as is_bn_round_break
          ,vma.action_type_id = ttb.action_type_pkg$bn_round_break()  as is_bn_round_break
          --,vma.is_bn_round_break
          --,vma.action_type_id = ttb.action_type_pkg$change_driver() as is_change_driver
          ,vma.is_change_driver
          --,vma.action_type_id = ttb.action_type_pkg$pause() as is_pause
          ,vma.is_pause
        from ttb.vm_action_type vma
    ),

      q as (
        select --distinct
           out.path[1] as tt_out_id
          ,out.tt_out_num
          ,tai.time_begin
          ,tai.time_end
          ,ta.action_dur
          ,at.action_type_code
          ,at.action_type_name
          ,at.action_type_id
          ,at.is_production_round
          ,at.is_technical_round
          ,at.is_bn_round_break
          ,coalesce(tdr.is_сhanged_driver, false) or at.is_dinner as  is_dinner
          ,at.is_break
          ,at.is_round
          ,at.is_change_driver
          ,at.is_pause
          ,md.move_direction_id
          ,md.move_direction_name
          ,r.code as r_code
          ,first_value(s.name) over (partition by ta.tt_action_id order by si2r.order_num) as stop_b
          ,first_value(s.name) over (partition by ta.tt_action_id order by si2r.order_num desc) as stop_e
          ,first_value(s.stop_id) over (partition by ta.tt_action_id order by si2r.order_num) as stop_id_b
          ,first_value(s.stop_id) over (partition by ta.tt_action_id order by si2r.order_num desc) as stop_id_e
          --,si2r.order_num
          ,ta.tt_action_id
          ,out.tt_variant_id
          ,tai.dr_shift_id
          ,out.mode_id
          ,ag.tt_action_group_code
          ,ta.tt_action_group_id + ta.action_gr_id as tt_action_group_nn
          ,ag.ref_group_kind_id
          ,r.code
          ,ttv.route_variant_id
          ,r.round_id
          ,ttv.norm_id
          ,si2r.stop_item_id
        from all_out out
          join ttb.tt_variant ttv on ttv.tt_variant_id = out.tt_variant_id and not ttv.sign_deleted
          join ttb.tt_action ta on ta.tt_out_id = out.tt_out_id and not ta.sign_deleted
          join act at on at.action_type_id = ta.action_type_id
          join ttb.tt_action_item tai on tai.tt_action_id = ta.tt_action_id and not tai.sign_deleted
          join rts.stop_item2round si2r on si2r.stop_item2round_id = tai.stop_item2round_id
          join rts.stop_item si on si.stop_item_id = si2r.stop_item_id
          join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
          join rts.stop s on s.stop_id = sl.stop_id
          join rts.stop_item2round_type si2rt on si2rt.stop_item2round_id = si2r.stop_item2round_id
          join rts.stop_item_type sit on sit.stop_item_id = si.stop_item_id
                                         and sit.stop_item_type_id = si2rt.stop_item_type_id
          join ttb.tt_action_group ag on ag.tt_action_group_id = ta.tt_action_group_id
          left join rts.stop_type st on st.stop_type_id = sit.stop_type_id and st.stop_type_id = rts.stop_type_pkg$finish()/*7*/
          left join ttb.tt_action_round tar on tar.tt_action_id = ta.tt_action_id
          left join rts.round r on r.round_id = tar.round_id
                                   and si2r.round_id = r.round_id
                                   and r.action_type_id = at.action_type_id
          left join rts.move_direction md on md.move_direction_id = r.move_direction_id
          left join ttb.tt_driver_action act_dr  on act_dr.tt_action_id = ta.tt_action_id
          left join ttb.tt_driver tdr on tdr.tt_driver_id = act_dr.tt_driver_id
    )
    , qq as (
      select
        q.tt_out_id
        ,q.tt_out_num
        ,row_number() over (partition by q.tt_action_id order by q.time_end ,q.time_begin) as rn
        ,q.time_begin
        ,q.time_end
        ,min(q.time_begin) over (partition by q.tt_action_id) as agg_time_begin
        ,max(q.time_end) over (partition by q.tt_action_id) as agg_time_end
        ,case
         when q.is_bn_round_break then
           extract(epoch from
                   q.time_end -
                   lag(q.time_begin) over (partition by q.tt_action_id order by q.time_end ,q.time_begin))
         when q.is_dinner then
           extract(epoch from
                   ttb.jf_mode_pkg$of_get_dinner_time(q.mode_id, q.dr_shift_id, 'min'))
         when q.is_change_driver then
           extract(epoch from
                   max(q.time_end::time) over (partition by q.tt_action_id) -
                   min(q.time_begin::time) over (partition by q.tt_action_id) )
         else null
         end min_bn_int
        ,case
         when q.is_bn_round_break then
           ttb.rv_tune_pkg$get_break_round_time(q.route_variant_id)
         when q.is_dinner then
           extract(epoch from
                   ttb.jf_mode_pkg$of_get_dinner_time(q.mode_id, q.dr_shift_id, 'max'))
         when q.is_change_driver then
           extract(epoch from
                   max(q.time_end::time) over (partition by q.tt_action_id) -
                   min(q.time_begin::time) over (partition by q.tt_action_id) )
         else null
         end max_bn_int
        ,q.action_dur
        ,q.action_type_code
        ,q.action_type_name
        ,q.move_direction_id
        ,q.move_direction_name
        ,q.r_code
        ,q.action_type_id
        ,q.is_production_round
        ,q.is_technical_round
        ,q.is_bn_round_break
        ,q.is_dinner
        ,q.is_pause
        ,q.is_break
        ,q.is_round
        ,q.stop_b
        ,q.stop_e
        ,q.stop_id_b
        ,q.stop_id_e
        ,q.tt_action_id
        ,q.tt_variant_id
        ,q.dr_shift_id
        ,q.mode_id
        ,q.tt_action_group_code
        ,q.tt_action_group_nn
        ,q.ref_group_kind_id
        ,q.round_id
        ,q.norm_id
        ,q.stop_item_id
      from
        q /*join ttb.tt_variant ttv on ttv.tt_variant_id = q.tt_variant_id*/
  )
  select --distinct
    qq.tt_variant_id
    ,qq.tt_out_id
    ,qq.tt_out_num
    --,qq.rn
    ,qq.agg_time_begin
    ,qq.agg_time_end
    ,qq.min_bn_int
    ,qq.max_bn_int
    ,qq.tt_action_id
    ,qq.action_dur
    --,qq.action_type_code
    ,case
     when qq.is_technical_round then
       case
       when qq.action_type_id in ( ttb.action_type_pkg$stop_to_park()
         ,ttb.action_type_pkg$stop_to_lunch()
         ,ttb.action_type_pkg$stop_to_gs()
         ,ttb.action_type_pkg$stop_to_parking()
         ,ttb.action_type_pkg$stop_to_repair()
       )
         then
           substring(
               ttb.jf_tt_action_pkg$get_prev_action_code (
                   qq.tt_out_id,
                   qq.tt_action_id,
                   ttb.action_type_pkg$production_round()
               ), 2, 1)
           ||
           substring(qq.action_type_code, 2, 1)
       when qq.action_type_id in ( ttb.action_type_pkg$park_to_stop()
         ,ttb.action_type_pkg$lunch_to_stop()
         ,ttb.action_type_pkg$gs_to_stop()
         ,ttb.action_type_pkg$parking_to_stop()
         ,ttb.action_type_pkg$repair_to_stop()
       )
         then
           substring(qq.action_type_code, 1, 1)
           ||
           substring(
               ttb.jf_tt_action_pkg$get_next_action_code (
                   qq.tt_out_id,
                   qq.tt_action_id,
                   ttb.action_type_pkg$production_round()
               ), 1, 1)
       else
         qq.action_type_code
       end
     else
       qq.action_type_code
     end action_type_code
    ,qq.action_type_name
    ,qq.move_direction_id
    ,qq.move_direction_name
    ,qq.r_code
    ,qq.action_type_id
    ,qq.is_production_round
    ,qq.is_technical_round
    ,qq.is_bn_round_break
    ,qq.is_dinner
    ,qq.is_pause
    ,qq.is_break
    ,qq.is_round
    ,qq.stop_b
    ,qq.stop_e
    ,qq.stop_id_b
    ,qq.stop_id_e
    ,qq.dr_shift_id
    ,qq.mode_id
    ,qq.tt_action_group_code
    ,qq.tt_action_group_nn
    ,qq.ref_group_kind_id
    ,qq.round_id
    ,qq.norm_id
    ,qq.stop_item_id
  from qq
  where ((not qq.is_bn_round_break and qq.rn = 1)
         or (qq.is_bn_round_break and qq.rn = 2))
  order by qq.tt_out_id
    ,qq.agg_time_end
    ,qq.agg_time_begin;



end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;



------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Создать временную таблицу
***************************************************************************/
create or replace function ttb.tt_out_pkg$make_tmp_tt(p_tt_out_id numeric)
  returns void
as
$body$
declare
begin
  discard temp;

  create temporary table if not exists  tt_action
  on commit preserve rows
  tablespace ttb_data  as
    select * from ttb.tt_action
    where tt_out_id = p_tt_out_id
      and not sign_deleted
  with data;

  alter table tt_action alter column tt_action_id set default nextval('ttb.tt_action_tt_action_id_seq'::regclass);
  alter table tt_action add constraint pk_tt_action primary key (tt_action_id);

  create index  if not exists ix_tt_action
    on tt_action
    using btree
    (action_type_id, dt_action, upper(action_range ), action_gr_id)
  tablespace ttb_idx
  where parent_tt_action_id is null;

  -----------------------------------
  create temporary table if not exists  tt_action_round
  on commit preserve rows
  tablespace ttb_data  as
    select * from ttb.tt_action_round
    where exists (select 1 from ttb.tt_action where tt_out_id = p_tt_out_id and tt_action.tt_action_id = tt_action_round.tt_action_id)
  with data;

  alter table tt_action_round add constraint pk_tt_action_round primary key (tt_action_id);

----------------------------------------------
  create temporary table if not exists  tt_action_item
  on commit preserve rows
  tablespace ttb_data  as
    select * from ttb.tt_action_item
    where exists (select 1 from ttb.tt_action where tt_out_id = p_tt_out_id and tt_action.tt_action_id = tt_action_item.tt_action_id)
     and not sign_deleted
  with data;

  alter table tt_action_item alter column tt_action_item_id set default nextval('ttb.tt_action_item_tt_action_item_id_seq'::regclass);
  alter table tt_action_item add constraint pk_tt_action_item primary key (tt_action_item_id);


  create index  if not exists ix_tt_action_item
    on tt_action_item
    using btree
    (
      tt_action_id ,
      time_begin,
      time_end)
  tablespace ttb_idx;

  create index  if not exists ix_tt_action_item
    on tt_action_item
    using btree
    ( tt_action_id ,action_type_id)
  tablespace ttb_idx;

end;
$body$
language plpgsql volatile
cost 100;

------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Создать временную таблицу
***************************************************************************/
create or replace function ttb.tt_out_pkg$make_tmp_tt4var(p_tt_variant_id numeric)
  returns void
as
$body$
declare
begin
  discard temp;

  create temporary table if not exists  tt_out
  on commit preserve rows
  tablespace ttb_data  as
    select * from ttb.tt_out
    where  tt_out.tt_variant_id = p_tt_variant_id
          and not sign_deleted
  with data;

  create index  if not exists ix_tt_out
    on tt_out
    using btree
    (tt_out_id)
  tablespace ttb_idx;


  create temporary table if not exists  tt_action
  on commit preserve rows
  tablespace ttb_data  as
    select * from ttb.tt_action
    where exists (select 1 from tt_out where   tt_out.tt_out_id = tt_action.tt_out_id)
          and not sign_deleted
  with data;

  alter table tt_action alter column tt_action_id set default nextval('ttb.tt_action_tt_action_id_seq'::regclass);
  alter table tt_action add constraint pk_tt_action primary key (tt_action_id);

  create index  if not exists ix_tt_action
    on tt_action
    using btree
    (action_type_id, dt_action, upper(action_range ), action_gr_id)
  tablespace ttb_idx
    where parent_tt_action_id is null;

  -----------------------------------
  create temporary table if not exists  tt_action_round
  on commit preserve rows
  tablespace ttb_data  as
    select * from ttb.tt_action_round
    where exists (select 1 from tt_action where  tt_action.tt_action_id = tt_action_round.tt_action_id)
  with data;

  alter table tt_action_round add constraint pk_tt_action_round primary key (tt_action_id);

  ----------------------------------------------
  create temporary table if not exists  tt_action_item
  on commit preserve rows
  tablespace ttb_data  as
    select * from ttb.tt_action_item
    where exists (select 1 from tt_action where  tt_action.tt_action_id = tt_action_item.tt_action_id)
          and not sign_deleted
  with data;

  alter table tt_action_item alter column tt_action_item_id set default nextval('ttb.tt_action_item_tt_action_item_id_seq'::regclass);
  alter table tt_action_item add constraint pk_tt_action_item primary key (tt_action_item_id);

  create index  if not exists ix_tt_action_item
    on tt_action_item
    using btree
    (
      tt_action_id ,
      time_begin,
      time_end)
  tablespace ttb_idx;

  create index  if not exists ix_tt_action_item
    on tt_action_item
    using btree
    ( tt_action_id ,action_type_id)
  tablespace ttb_idx;

end;
$body$
language plpgsql volatile
cost 100;



------------------------------------------------------------------------------------------------------------------------
/***************************************************************************
-- Заполнение структуры в БД из временной
если запись в таких id есть, то это назначение ТС - обновляем
***************************************************************************/
create or replace function ttb.tt_out_pkg$fill_db_table(p_tt_out_id numeric)
  returns void
as
$body$
declare
  l_cnt numeric;
  l_rec record;
begin
  RAISE NOTICE  'fill_db_table( p_tt_out_id = %)', p_tt_out_id;

  delete from tt_action_item
  where not exists (select 1 from tt_action t where t.tt_action_id = tt_action_item.tt_action_id);

  delete from tt_action_round
  where not exists (select 1 from tt_action t where  t.tt_action_id = tt_action_round.tt_action_id);


  delete from ttb.tt_action_item
  where exists (select 1 from ttb.tt_action t where t.tt_out_id = p_tt_out_id and t.tt_action_id = tt_action_item.tt_action_id)
        and not exists (select 1 from tt_action_item t where t.tt_action_item_id = tt_action_item.tt_action_item_id);

  delete from ttb.tt_action_round
  where exists (select 1 from ttb.tt_action t where t.tt_out_id = p_tt_out_id and t.tt_action_id = tt_action_round.tt_action_id)
  and not exists (select 1 from tt_action_round t where t.tt_action_id = tt_action_round.tt_action_id);


  delete from ttb.tt_action
  where tt_out_id = p_tt_out_id
        and not exists (select 1 from tt_action t where t.tt_action_id = tt_action.tt_action_id);



  insert into ttb.tt_action(tt_action_id, tt_out_id, action_dur, dt_action, action_type_id, tt_action_group_id, action_gr_id, action_range)
  select tt_action_id, tt_out_id, action_dur, dt_action, action_type_id, tt_action_group_id, action_gr_id, action_range
  from tt_action
  on conflict on constraint  pk_tt_action
       do update set action_dur   = EXCLUDED.action_dur,
                     dt_action    = EXCLUDED.dt_action,
                     action_range = EXCLUDED.action_range
                     ;

   insert into ttb.tt_action_round(tt_action_id, round_id)
     select tt_action_id, round_id
     from tt_action_round
   on conflict on constraint  pk_tt_action_round
     do update set round_id   = EXCLUDED.round_id
                    ;

  /*for l_rec in (
  select tt_action_item_id,    tt_action_id,    time_begin,   time_end,  stop_item2round_id,   action_type_id, dr_shift_id
  from tt_action_item) loop
    raise notice '%
                 --------------------', l_rec;
  end loop;*/

   insert into ttb.tt_action_item(tt_action_item_id,    tt_action_id,    time_begin,   time_end,  stop_item2round_id,   action_type_id, dr_shift_id)
   select tt_action_item_id,    tt_action_id,    time_begin,   time_end,  stop_item2round_id,   action_type_id, dr_shift_id
   from tt_action_item
   on conflict on constraint  pk_tt_action_item
     do update set time_begin  = EXCLUDED.time_begin,
                   time_end    = EXCLUDED.time_end,
                   dr_shift_id = EXCLUDED.dr_shift_id
                   ;

 end;
 $body$
language plpgsql volatile
cost 100;

----------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION ttb.tt_out_pkg$clear_deleted_out (
)
RETURNS void AS
$body$
declare

begin

 delete from ttb.tt_out outo 
 where outo.tt_out_id = any ( array  (
                  select outi.tt_out_id from ttb.tt_out outi
                   where sign_deleted
                   and exists (select null from  ttb.tt_variant tv
                                           where tv.parent_tt_variant_id is null
                                             and tv.tt_variant_id = outi.tt_variant_id)
                   and not exists (select null from ttb.tt_out pout
                                         where pout.tt_out_id = outi.parent_tt_out_id) 
                   and not exists (select null from ttb.tt_out pout
                                         where outi.tt_out_id = pout.parent_tt_out_id) 
                   limit 2500
                   ) 
                   );
 
  --analyze ttb.tt_out;
  
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

/*Если не выполнены нормы труда, то будем тупо продлевать выходы*/
create or replace function ttb.tt_out_pkg$add_main_action2out (
   in p_tt_variant_id numeric
)
  returns void as
$body$
declare
  l_tt_schedule_id numeric;
begin
  l_tt_schedule_id:= ttb.tt_schedule_pkg$get_main_schedule_id (p_tt_variant_id);
  perform  ttb.tt_out_pkg$add_main_action2out (p_tt_variant_id, l_tt_schedule_id );
end;
$body$
language 'plpgsql'
volatile;




/*Если не выполнены нормы труда, то будем тупо продлевать выходы*/
create or replace function ttb.tt_out_pkg$add_main_action2out (
   in p_tt_variant_id numeric
  ,in p_tt_schedule_id numeric
  ,in p_tt_out_id numeric default null
)
  returns void as
$body$
declare
   l_out record;
   l_act record;
   l_tt_action_id numeric;
   p_id_account numeric:= 2;
   p_attr jsonb;
   l_tt_schedule_item_id numeric;
   l_tt_out_id numeric;
begin
  raise notice 'ttb.tt_out_pkg$add_main_action2out';
  select sit.tt_schedule_item_id into l_tt_schedule_item_id
  from ttb.tt_action_group agr
    join ttb.tt_schedule_item sit on sit.tt_action_group_id = agr.tt_action_group_id
  where agr.tt_variant_id = p_tt_variant_id
        and sit.tt_schedule_id = p_tt_schedule_id
        and agr.ref_group_kind_id = ttb.ref_group_kind_pkg$dinner_action()
  limit 1;
  DISCARD temp;
  l_tt_out_id:=0;
  for l_out in (

            with
              dur_cycle as (
                select ttb.norm_pkg$get_schedule_tm (p_tt_sсhedule_id => p_tt_schedule_id
                ,p_ref_group_kind_id => ttb.ref_group_kind_pkg$main_action()::numeric
                       ) dur)

             , max_tm as (

              select sir.stop_item_id, max(atg.start_time) as tm
              from ttb.atgrs_time atg
              join ttb.tt_at_round_stop ars on ars.tt_at_round_stop_id = atg.tt_at_round_stop_id
              join ttb.tt_at_round ar on ar.tt_at_round_id = ars.tt_at_round_id
              join rts.stop_item2round sir on sir.stop_item2round_id = ars.stop_item2round_id and sir.order_num =1 and not sir.sign_deleted
              join ttb.tt_action_type2group at2g on at2g.tt_action_type_id = ar.tt_action_type_id
              join ttb.tt_action_group ag on ag.tt_action_group_id = at2g.tt_action_group_id
              join ttb.tt_schedule_item sit on sit.tt_action_group_id = ag.tt_action_group_id
              where atg.fix_arrival_type_id = ttb.fix_arrival_type_pkg$finish_time()
              and sit.tt_schedule_id = p_tt_schedule_id
              group by sir.stop_item_id
            )
            , adata as (
              select
             /* least ((drs.work_duration_max - all_time),extract(epoch from max_tm.tm- wrk.last_rnd_tm),
                       --если даем обед последнее время не проверяем
                      case when (p_is_with_pause and break_count_max > pause_cnt and   break_duration_max - wrk.pause_all -asd.get_constant('OPTIMAL_TIME_DINNER')::int>0) then null else after_time_max - last_time end ) as can_dur,
*/
              least ((drs.work_duration_max - all_time),extract(epoch from max_tm.tm- wrk.last_rnd_tm),after_time_max - last_time) as can_dur,
              max_tm.tm as max_tm_tm,
              (break_count_max > pause_cnt and   break_duration_max - wrk.pause_all -asd.get_constant('OPTIMAL_TIME_DINNER')::int>0)  as is_can_pause,
              wrk.last_tm_b + make_interval(secs:= work_break_between_min) as min_tm4pause,
              wrk.last_tm_b + make_interval(secs:=  work_break_between_max) as max_tm4pause,
                dur_cycle.dur,
              tout.tt_out_id,
              wrk.*, drs.*
              from ttb.tt_out tout
              join lateral(select * from ttb.tt_out_pkg$get_out_wrk (tout.tt_out_id)) wrk on 1=1
              join ttb.dr_shift drs on drs.dr_shift_id = wrk.dr_shift_id
              /*join lateral (select ttb.norm_pkg$get_schedule_tm (p_tt_sсhedule_id => p_tt_schedule_id
                                                                ,p_ref_group_kind_id => ttb.ref_group_kind_pkg$main_action()::numeric
                                                                ,p_tm_b=> last_tm_e::time) dur) dur_cycle on 1=1*/
              join dur_cycle on 1=1
              join lateral(select tm from max_tm order by  max_tm.stop_item_id = last_rnd_stop_item_id limit 1) max_tm on 1=1
              where  (wrk.all_time< drs.work_duration_max or pause_cnt=0)
              and not tout.sign_deleted
              and tout.tt_variant_id  = p_tt_variant_id
              and (p_tt_out_id is null or tout.tt_out_id = p_tt_out_id)
              and not wrk.is_switch
            )
            select
            div(can_dur::numeric, dur) as can_cnt
            ,case when  is_can_pause then div((least ((work_duration_max - all_time),extract(epoch from max_tm_tm- last_rnd_tm))-asd.get_constant('OPTIMAL_TIME_DINNER')::int)::numeric, dur) else 0 end as can_cnt_after_pause
            ,adata.*
            from adata
    ) loop
                if l_tt_out_id != l_out.tt_out_id and  (l_out.can_cnt!=0 or l_out.can_cnt_after_pause !=0) then
                   l_tt_out_id := l_out.tt_out_id;

                         raise notice 'l_out.tt_out_id (%), l_out.dr_shift_id (%), l_out.can_cnt (%), l_out.can_cnt_after_pause (%), l_out.is_can_pause(%)', l_out.tt_out_id, l_out.dr_shift_id, l_out.can_cnt, l_out.can_cnt_after_pause, l_out.is_can_pause;

                        if l_out.can_cnt > 0  or (l_out.can_cnt_after_pause>0 and l_out.is_can_pause ) then

                          raise notice '1';
                            for l_act in (
                              select act.tt_action_id
                              from
                                ttb.tt_action act
                                join ttb.tt_action_group agr on agr.tt_action_group_id = act.tt_action_group_id
                                join ttb.vm_action_type vma on vma.action_type_id = act.action_type_id and (vma.is_bn_round_break or vma.is_all_pause)
                                join ttb.tt_action_item it on act.tt_action_id =it.tt_action_id and not it.sign_deleted
                                join rts.stop_item2round sir on sir.stop_item2round_id = it.stop_item2round_id
                              where act.tt_out_id = l_out.tt_out_id and not act.sign_deleted

                                    and (agr.ref_group_kind_id = ttb.ref_group_kind_pkg$main_action() or vma.is_all_pause)
                                    and ((l_out.is_can_pause and l_out.can_cnt=0 and  l_out.can_cnt_after_pause> l_out.can_cnt
                                          --and  act.dt_action between  l_out.min_tm4pause and l_out.max_tm4pause
                                          and  act.dt_action >=  l_out.min_tm4pause
                                           )
                                      or (not (l_out.is_can_pause and l_out.can_cnt=0 and  l_out.can_cnt_after_pause> l_out.can_cnt) and  (act.dt_action>=l_out.last_tm_b)
                                          )
                                       or
                                         (upper(act.action_range)= l_out.last_tm_b and vma.is_all_pause and l_out.can_cnt>0)
                                        )
                                    and sir.stop_item_id = case when  l_out.is_can_pause and l_out.can_cnt=0 then l_out.pause_stop_item_id else  sir.stop_item_id end

                                    and it.dr_shift_id  = l_out.dr_shift_id
                              order by   dt_action desc
                              limit 1
                              )  loop
                              raise notice '2';
                                    --add dinner
                               if  l_out.is_can_pause and l_out.can_cnt=0 then
                                    raise notice 'try add dinner l_act.tt_action_id = %', l_act.tt_action_id;
                                    p_attr:=json_build_object('tt_variant_id',p_tt_variant_id::text
                                                            , 'tt_out_id',l_out.tt_out_id::text
                                                            , 'tt_action_id',l_act.tt_action_id::text
                                                            , 'action_type_id', ttb.action_type_pkg$bn_round_break()::text
                                                            , 'tt_schedule_item_id', l_tt_schedule_item_id
                                                            );
                                     begin
                                        l_tt_action_id:= ttb.jf_tt_out_pkg$of_make_action (p_id_account, p_attr::text);
                                     exception when others then
                                        raise notice 'ОШИБКА tt_out_id( % )',l_out.tt_out_id;
                                     end;

                                    raise notice 'добавлен обед l_tt_action_id( % )', l_tt_action_id;
                                    if l_tt_action_id>0 then

                                        perform ttb.tt_out_pkg$add_main_action2out (p_tt_variant_id, p_tt_schedule_id, l_out.tt_out_id);
                                        raise notice 'добавлены рейсы поле обеда ';
                                    end if;
                               else
                                  --add round
                                    p_attr:=json_build_object('tt_variant_id',p_tt_variant_id::text
                                                            , 'tt_out_id',l_out.tt_out_id::text
                                                            , 'tt_action_id',l_act.tt_action_id::text
                                                            , 'action_type_id', ttb.action_type_pkg$bn_round_break()::text
                                                            , 'f_count_full_rnd', l_out.can_cnt::text);

                                    raise notice 'p_attr ( % )',  p_attr;

                                    l_tt_action_id:= ttb.jf_tt_out_pkg$of_make_main_action (p_id_account, p_attr::text);
                                    raise notice 'добавлено кругорейсов %, последний l_tt_action_id( % )', l_out.can_cnt,  l_tt_action_id;

                              end if;
                              if l_out.is_can_pause and l_out.can_cnt> 0 then

                                perform ttb.tt_out_pkg$add_main_action2out (p_tt_variant_id, p_tt_schedule_id, l_out.tt_out_id);

                              end if;
                    end loop;
                 end if;
       end if;
  end loop;


  return;

end;
$body$
language 'plpgsql'
volatile
cost 100;

--DROP FUNCTION ttb."tt_out_pkg$get_out_wrk"(integer)

create or replace function ttb.tt_out_pkg$get_out_wrk ( p_tt_out_id integer)
  returns table (
   before_tm_b timestamp
  ,before_tm_e timestamp
  ,before_time bigint
  ,between_tm_b timestamp
  ,between_tm_e timestamp
  ,between_time bigint
  ,last_tm_b timestamp
  ,last_tm_e timestamp
  ,last_time bigint
  ,all_time  bigint
  ,dr_shift_id smallint
  ,last_rnd_stop_item_id integer
  ,last_rnd_tm timestamp
  ,pause_all bigint
  ,pause_stop_item_id integer
  ,pause_cnt integer
  ,is_switch boolean
  ) as
$body$
begin
  return query
  with adata as (
      select
        lag(vma.is_all_pause) over (partition by it.dr_shift_id order by act.dt_action),
                          coalesce((vma.is_all_pause != lag(vma.is_all_pause) over (partition by it.dr_shift_id order by act.dt_action) or
                                    coalesce(tdr.is_сhanged_driver,false) != lag( coalesce(tdr.is_сhanged_driver,false) ) over (partition by it.dr_shift_id order by act.dt_action, coalesce(upper(act.action_range),act.dt_action)) )::int,1)  as start_of_group
        , it.dr_shift_id, it.stop_item_id as rnd_stop_item_id, it.time_begin as rnd_tm_b
        , act.dt_action
        , act.action_range
        , vma.*
        ,tout.tt_variant_id
        ,vma.is_all_pause or  coalesce(tdr.is_сhanged_driver,false)  as is_br
        , agr.ref_group_kind_id = ttb.ref_group_kind_pkg$last_action() as is_last_kind
        , agr.ref_group_kind_id = ttb.ref_group_kind_pkg$main_action() as is_main_kind
      from ttb.tt_action act
        join ttb.tt_action_group agr on agr.tt_action_group_id = act.tt_action_group_id
        join ttb.tt_out tout on tout.tt_out_id = act.tt_out_id and not tout.sign_deleted
        join ttb.vm_action_type vma on vma.action_type_id = act.action_type_id
        join lateral  (select it.dr_shift_id , sir.stop_item_id, it.time_begin from ttb.tt_action_item it join rts.stop_item2round sir on sir.stop_item2round_id = it.stop_item2round_id
        where it.tt_action_id = act.tt_action_id  and not it.sign_deleted order by it.time_end  limit 1 ) it on 1=1
        left join ttb.tt_driver_action act_dr  on act_dr.tt_action_id = act.tt_action_id
        left join ttb.tt_driver tdr on tdr.tt_driver_id = act_dr.tt_driver_id
      where act.tt_out_id = p_tt_out_id and not act.sign_deleted

  )
    , switch as (
      select count(1) cnt
      from adata
      where adata.action_type_id = ttb.action_type_pkg$switch()
  )
    , d as (
      select
        sum(start_of_group) over (order by dt_action, coalesce(upper(action_range),dt_action))as grp
        ,adata.*
      from adata
  )
    , rdata as (
      select
        row_number() over (partition by d.dr_shift_id,  is_br order by grp)  rn
        ,d.*
      from d
      where start_of_group = 1
  )
    ,sp_dinner as (
      select sir.stop_item_id
      from rdata r
        join ttb.tt_action_type tact on tact.tt_variant_id = r.tt_variant_id and tact.action_type_id =ttb.action_type_pkg$dinner()
        join  ttb.tt_at_stop tats on   tact.tt_action_type_id =  tats.tt_action_type_id
        join rts.stop_item2round sir on sir.stop_item2round_id = tats.stop_item2round_id

      limit 1)

    , ps as (
      select  r.dr_shift_id, extract(epoch from max(upper(d.action_range)) - min(d.dt_action) ) as pause_all,
                             max(d.rnd_stop_item_id) as pause_stop_item_id, max(rn) as pause_cnt
      from rdata r
        join d on d.dr_shift_id =r.dr_shift_id and d.grp = r.grp
      where r.is_br
      group by r.dr_shift_id
  )


    ,before_tm as (
      select r.dr_shift_id,
         extract(epoch from it.dt_action - r.dt_action) before_time
        ,r.dt_action as tm_b, it.dt_action as tm_e
      from rdata r
        left join rdata it on   r.dr_shift_id = it.dr_shift_id and it.is_br and it.rn = r.rn
      where  not  r.is_br and r.rn =1

  )

    ,between_tm as (
      select r.dr_shift_id,
         extract(epoch from it.dt_action - r.dt_action) between_time
        ,r.dt_action as tm_b, it.dt_action as tm_e
      from rdata r
        join rdata it on   r.dr_shift_id = it.dr_shift_id and it.is_br and it.rn = r.rn
      where  not  r.is_br and r.rn =2

  )

    ,last_tm as (
      select  r.dr_shift_id,
         extract(epoch from max(upper(it.action_range)) - min(it.dt_action) ) last_time
        ,min(it.dt_action) as tm_b, max(upper(it.action_range)) as tm_e
      from rdata r
        join d it on r.grp = it.grp   and r.dr_shift_id = it.dr_shift_id
      where not r.is_br
            and r.rn =  (select max(rn) from rdata where not rdata.is_br and  rdata.dr_shift_id= r.dr_shift_id)
      group by r.dr_shift_id
  )
    , last_rnd as (
      select adata.rnd_stop_item_id  ,   adata.rnd_tm_b , adata.dr_shift_id     from adata where  adata.is_main_kind and adata.is_round order by dt_action desc limit 1
  )
  select
     before_tm.tm_b as before_tm_b, before_tm.tm_e as before_tm_e,  before_tm.before_time::bigint
    ,between_tm.tm_b as between_tm_b  ,between_tm.tm_e as between_tm_e, coalesce(between_tm.between_time, 0)::bigint as between_time
    ,last_tm.tm_b as last_tm_b  ,last_tm.tm_e as last_tm_e,  last_tm.last_time::bigint
    ,(coalesce(before_tm.before_time,0) +  coalesce(between_tm.between_time, 0) +  last_tm.last_time)::bigint as all_time
    ,before_tm.dr_shift_id::smallint
    ,last_rnd.rnd_stop_item_id as last_rnd_stop_item_id
    ,last_rnd.rnd_tm_b as last_rnd_tm
    ,coalesce(ps.pause_all,0)::bigint
    ,coalesce(ps.pause_stop_item_id, sp_dinner.stop_item_id)::int
    ,coalesce(ps.pause_cnt,0)::int
    ,switch.cnt>0 as is_switch
  from before_tm
    join sp_dinner on 1=1
    left join last_tm on before_tm.dr_shift_id = last_tm.dr_shift_id
    left join last_rnd on 1=1
    left join between_tm on before_tm.dr_shift_id = between_tm.dr_shift_id
    left join ps on before_tm.dr_shift_id =  ps.dr_shift_id
    left join switch  on 1=1
  ;


end;
$body$
language 'plpgsql'
volatile
called on null input
security invoker
cost 100;
