CREATE OR REPLACE FUNCTION ttb.jf_rep_waste_lineartime_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare

begin

 open p_rows for 
     select
 null as depo_name, /* Наименование ТП */
 null as tr_type_name, /* Вид ТС */
 null as route_num, /* Номер маршрута */
 null as out_num, /* Номер выхода */
 null as garage_num, /* Гар. номер ТС */
 null as dr_tab_num, /* Водитель */
 null as linetime_waste_amount, /* Общая длительность потерь линейного времени */
 null as outstand_round_cnt, /* Количество невыполн.рейсов */
 null as outstand_round_reason /* Причины не выполнения рейсов */
	 
;
end;
$function$
;
