CREATE OR REPLACE FUNCTION ttb.jf_tt_check_parm_pkg$cn_interval_type (
)
RETURNS text AS
$body$
begin 
   return 'I';
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_check_parm_pkg$cn_number_type (
)
RETURNS text AS
$body$
begin 
   return 'N';
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_check_parm_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
  declare 
    ln_tt_check_condition_id ttb.tt_check_condition.tt_check_condition_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_check_condition_id', true);	
  begin 
   open p_rows for 
       with check_parm as (  
    select 
           coalesce(tcpi.tt_check_parm_id, tcpn.tt_check_parm_id) as tt_check_parm_id
          ,coalesce(tcpi.tt_check_condition_id, tcpn.tt_check_condition_id) as tt_check_condition_id 
          ,coalesce(tcpi.parm_name,tcpn.parm_name) as parm_name
          ,coalesce(tcpi.parm_type, tcpn.parm_type) as parm_type
          ,tcpi.parm_value as parm_value_i
          ,tcpn.parm_value as parm_value_n
          ,coalesce(tcpi.order_num, tcpn.order_num) as order_num
        from ttb.tt_check_parm_type tcpt
        left join  ttb.tt_check_parm tcpi on tcpt.parm_type = tcpi.parm_type 
                                         and tcpi.parm_type = ttb.jf_tt_check_parm_pkg$cn_interval_type()
                                         and not tcpi.sign_deleted
        left join  ttb.tt_check_parm tcpn on tcpt.parm_type = tcpn.parm_type 
                                         and tcpn.parm_type = ttb.jf_tt_check_parm_pkg$cn_number_type() 
                                         and not tcpn.sign_deleted
    	  
        )
  select   cp.tt_check_parm_id
          ,cp.tt_check_condition_id 
          ,cp.parm_name
          ,cp.parm_type
          ,cp.parm_value_i
          ,cp.parm_value_n 
          ,case
          		when cp.parm_type = ttb.jf_tt_check_parm_pkg$cn_interval_type() then
              	'P{RO},D{parm_value_n}'
            	when cp.parm_type = ttb.jf_tt_check_parm_pkg$cn_number_type() then
              	'P{RO},D{parm_value_i}'
            	else
              	'P{RO},D{parm_value_i,parm_value_n}'
           end "ROW$POLICY"
  from  check_parm cp  
  where cp.tt_check_condition_id = ln_tt_check_condition_id
  order by cp.order_num;    
        
        
  end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

----------------------------------------

CREATE OR REPLACE FUNCTION ttb.jf_tt_check_parm_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r 				ttb.tt_check_parm%rowtype;
   ln_parm_value_i 	integer 	:= jofl.jofl_pkg$extract_number(p_attr, 'parm_value_i', true); 
   ln_parm_value_n  smallint 	:= jofl.jofl_pkg$extract_number(p_attr, 'parm_value_n', true);
begin 
   l_r := ttb.jf_tt_check_parm_pkg$attr_to_rowtype(p_attr);
   
   if l_r.parm_type = ttb.jf_tt_check_parm_pkg$cn_interval_type() then
     if coalesce(ln_parm_value_i, 0) = 0 then
   		return  jofl.jofl_util$cover_result('error', 'Заполните значение параметра (время)'); 
     else
     	 update ttb.tt_check_parm set 
          parm_value = ln_parm_value_i      
   		 where 
          tt_check_parm_id = l_r.tt_check_parm_id;
          
         return null;
     end if;
   end if;
   
   if l_r.parm_type = ttb.jf_tt_check_parm_pkg$cn_number_type() then
     if ln_parm_value_n is null then
   		return  jofl.jofl_util$cover_result('error', 'Заполните значение параметра (число)');
     else
      	 update ttb.tt_check_parm set 
          parm_value = ln_parm_value_n      
   		 where 
          tt_check_parm_id = l_r.tt_check_parm_id;
          
         return null;    
     end if;
   end if;

   
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

----------------------------------------
CREATE OR REPLACE FUNCTION ttb.jf_tt_check_parm_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS ttb.tt_check_parm AS
$body$
declare 
   l_r ttb.tt_check_parm%rowtype; 
begin 
   l_r.tt_check_parm_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tt_check_parm_id', true); 
   l_r.tt_check_condition_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tt_check_condition_id', true); 
   l_r.parm_name := jofl.jofl_pkg$extract_varchar(p_attr, 'parm_name', true); 
   l_r.parm_type := jofl.jofl_pkg$extract_varchar(p_attr, 'parm_type', true); 
   l_r.parm_value := jofl.jofl_pkg$extract_number(p_attr, 'parm_value', true); 
   l_r.sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'sign_deleted', true); 
   l_r.sys_period := jofl.jofl_pkg$extract_varchar(p_attr, 'sys_period', true); 
   l_r.parm_tag := jofl.jofl_pkg$extract_varchar(p_attr, 'parm_tag', true); 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;