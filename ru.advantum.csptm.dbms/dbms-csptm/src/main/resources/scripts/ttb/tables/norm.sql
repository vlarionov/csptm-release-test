
-- Table ttb.norm

CREATE TABLE ttb.norm(
 norm_id Serial NOT NULL,
 tr_type_id Smallint NOT NULL,
 calculation_task_id Integer,
 norm_name Text NOT NULL,
 is_current Boolean DEFAULT false NOT NULL,
 hour_period Double precision DEFAULT 60 NOT NULL,
 dt_begin Timestamp NOT NULL,
 dt_end Timestamp NOT NULL,
 sign_deleted Boolean DEFAULT false NOT NULL,
 sys_period tstzrange DEFAULT tstzrange(now(), NULL::timestamp with time zone) NOT NULL
)
TABLESPACE ttb_data
;

COMMENT ON TABLE ttb.norm IS 'Нормативы'
;
COMMENT ON COLUMN ttb.norm.norm_id IS 'ID'
;
COMMENT ON COLUMN ttb.norm.tr_type_id IS 'Вид ТС'
;
COMMENT ON COLUMN ttb.norm.calculation_task_id IS 'Исходный расчет'
;
COMMENT ON COLUMN ttb.norm.norm_name IS 'Наименование '
;
COMMENT ON COLUMN ttb.norm.is_current IS 'Норматив может использоваться при расчете расписания'
;
COMMENT ON COLUMN ttb.norm.hour_period IS 'Временные периоды для расчета, мин'
;
COMMENT ON COLUMN ttb.norm.dt_begin IS 'Дата начала периода'
;
COMMENT ON COLUMN ttb.norm.dt_end IS 'Дата окончания периода'
;
COMMENT ON COLUMN ttb.norm.sign_deleted IS 'Удален'
;
COMMENT ON COLUMN ttb.norm.sys_period IS 'Текущий период'
;

-- Create indexes for table ttb.norm

CREATE INDEX ix_norm_tr_type_id ON ttb.norm (tr_type_id)
TABLESPACE ttb_idx
;

CREATE INDEX ix_norm_calculation_task_id ON ttb.norm (calculation_task_id)
TABLESPACE ttb_idx
;

-- Add keys for table ttb.norm

ALTER TABLE ttb.norm ADD CONSTRAINT pk_norm PRIMARY KEY (norm_id)
 USING INDEX TABLESPACE ttb_idx
;


CREATE TABLE hist.ttb_norm (LIKE ttb.norm)  tablespace hist_data;


CREATE TRIGGER versioning_trigger
BEFORE INSERT OR UPDATE OR DELETE ON ttb.norm
FOR EACH ROW EXECUTE PROCEDURE public.versioning(
    'sys_period', 'hist.ttb_norm', true
);


CREATE VIEW hist.v_norm_hist AS
    SELECT * FROM ttb.norm
  UNION ALL
    SELECT * FROM hist.ttb_norm;


CREATE INDEX ix_norm_sys_period ON ttb.norm USING gist (sys_period) tablespace ttb_idx;
CREATE INDEX ON hist.ttb_norm USING gist (sys_period) tablespace hist_idx;
