--select ttb.get_local_timestamp('2018-01-01'::timestamp);


--select now();


select * from
  ttb.order_list2out JOIN ttb.order_list on order_list2out.order_list_id = order_list.order_list_id;



select * from ttb.tt_variant where parent_tt_variant_id = 266;

--оперативный вариант расписания
SELECT
  norm_id,
  route_variant_id,
  is_all_tr_online,
  sign_deleted,
  sys_period,
  tt_status_id,
  ttv_name,
  is_calc_indicator,
  tt_variant.tt_variant_id
FROM ttb.tt_variant join ttb.v_tt_calendar_date
    on v_tt_calendar_date.tt_variant_id = tt_variant.tt_variant_id
WHERE NOT sign_deleted
      AND parent_tt_variant_id is null
      AND tt_variant.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id()
      AND tt_variant.action_period @> now()::timestamp
      AND v_tt_calendar_date.dt = now()::date
      AND tt_variant.tt_variant_id in  (
  SELECT min(tt_variant.tt_variant_id)
  FROM ttb.tt_variant join ttb.v_tt_calendar_date
      on v_tt_calendar_date.tt_variant_id = tt_variant.tt_variant_id
  WHERE NOT sign_deleted
        AND tt_variant.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id()
        AND parent_tt_variant_id is null
        AND tt_variant.action_period @> now()::timestamp
        AND v_tt_calendar_date.dt = now()::date
);


delete from ttb.tt_variant where tt_variant_id = 284;

  INSERT INTO ttb.tt_variant
  (norm_id
    , route_variant_id
    , is_all_tr_online
    , sign_deleted
    , sys_period
    , tt_status_id
    , action_period
    , ttv_name
    , ttv_comment
    , is_calc_indicator
    , parent_tt_variant_id
  )
    SELECT
      norm_id,
      route_variant_id,
      is_all_tr_online,
      sign_deleted,
      sys_period,
      tt_status_id,
      tsrange( now()::date::timestamp,  now()::date + interval '23:23:59'),
      ttv_name,
      '',
      is_calc_indicator,
      tt_variant.tt_variant_id
    FROM ttb.tt_variant join ttb.v_tt_calendar_date
            on v_tt_calendar_date.tt_variant_id = tt_variant.tt_variant_id
    WHERE route_variant_id = 4517
          AND NOT sign_deleted
          AND parent_tt_variant_id is null
          AND tt_variant.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id()
          AND tt_variant.action_period @>  now()::timestamp
          AND v_tt_calendar_date.dt =  now()::date
          AND tt_variant.tt_variant_id in  (
                                    SELECT min(tt_variant.tt_variant_id)
                                    FROM ttb.tt_variant join ttb.v_tt_calendar_date
                                            on v_tt_calendar_date.tt_variant_id = tt_variant.tt_variant_id
                                    WHERE route_variant_id = 4517
                                          AND NOT sign_deleted
                                          AND tt_variant.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id()
                                          AND parent_tt_variant_id is null
                                          AND tt_variant.action_period @>  now()::timestamp
                                          AND v_tt_calendar_date.dt =  now()::date
                                 )



select * from ttb.tt_variant;

--оперативный вариант расписания
select *
FROM ttb.tt_variant join ttb.v_tt_calendar_date
        on v_tt_calendar_date.tt_variant_id = tt_variant.tt_variant_id
where route_variant_id = 4517
      and not sign_deleted
      and parent_tt_variant_id is not null
      and tt_variant.action_period @> now()::timestamp
      and v_tt_calendar_date.dt = now()::date
      ;

--Найдем выходы
select *
from ttb.tt_out
where tt_variant_id = 266
      and tt_out_num in
      (
select * from ttb.order_list where group_id in
                                   (
                                     select group_id from easu.groups where out_date = now()::date and route_number = '203'
                                   )
      )
      and not sign_deleted;


select *
from ttb.tt_out2tr



            select tt_variant_id
            from ttb.tt_variant
            where parent_tt_variant_id = 4517
                  and not sign_deleted
                  and action_period =  tsrange(now()::date::timestamp, now()::date + interval '23:23:59')
                  and parent_tt_variant_id is not null;

select * from ttb.order_list
  join ttb.order_list2out on order_list.order_list_id = order_list2out.order_list_id
where order_date = now()::date::timestamp and route_variant_id = 4517;

select ARRAY[1];


select ttb.tt_oper_pkg$create_tt_oper(now()::timestamp);

select * from easu.groups where out_date = now()::date and route_number = '203';



select * from ttb.order_list
where group_id in
                                   (
                                     select group_id from easu.groups where out_date = now()::date
                                   );

--Класс ТС в плановом расписании и класс ТС в наряде НЕ совпадают
select * from unnest(ttb.tt_oper_pkg$orders_class_corr_violate(now()::timestamp));
--По каждому выходу Общее время смены в наряде равно общему времени смены в плановом расписании.
select * from unnest(ttb.tt_oper_pkg$orders_shift_time_violate(now()::timestamp));
--Пересечений времени работы ТС
select * from unnest(ttb.tt_oper_pkg$orders_tr_overlap(now()::timestamp));
--Пересечений времени работы водителей
select * from unnest(ttb.tt_oper_pkg$orders_driver_overlap(now()::timestamp));




