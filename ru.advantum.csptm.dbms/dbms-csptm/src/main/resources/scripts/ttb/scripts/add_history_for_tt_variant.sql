﻿select adm."history_pkg$create_trigger"('ttb', 'tt_out', 'tt_out_id');
select adm."history_pkg$create_trigger"('ttb', 'tt_action_type', 'tt_action_type_id');
select adm."history_pkg$create_trigger"('ttb', 'tt_at_round', 'tt_at_round_id');
select adm."history_pkg$create_trigger"('ttb', 'tt_at_round_stop', 'tt_at_round_stop_id');
select adm."history_pkg$create_trigger"('ttb', 'atgrs_time', 'atgrs_time_id');
select adm."history_pkg$create_trigger"('ttb', 'tt_at_stop', 'tt_at_stop_id');
select adm."history_pkg$create_trigger"('ttb', 'tt_action_group', 'tt_action_group_id');
select adm."history_pkg$create_trigger"('ttb', 'tt_action_type2group', 'tt_action_type2group_id');
select adm."history_pkg$create_trigger"('ttb', 'tt_schedule', 'tt_schedule_id');
select adm."history_pkg$create_trigger"('ttb', 'tt_schedule_item', 'tt_schedule_item_id');
select adm."history_pkg$create_trigger"('ttb', 'tt_period', 'tt_period_id');
select adm."history_pkg$create_trigger"('ttb', 'tt_tr_period', 'tt_tr_period_id');
select adm."history_pkg$create_trigger"('ttb', 'tt_tr', 'tt_tr_id');
select adm."history_pkg$create_trigger"('ttb', 'tt_calendar', 'tt_calendar_id');
select adm."history_pkg$create_trigger"('ttb', 'tt_peak_hour', 'tt_peak_hour_id');
select adm."history_pkg$create_trigger"('ttb', 'tt_change_time', 'tt_change_time_id');
select adm."history_pkg$create_trigger"('ttb', 'tt_driver', 'tt_driver_id');
select adm."history_pkg$create_trigger"('ttb', 'tt_action', 'tt_action_id');
select adm."history_pkg$create_trigger"('ttb', 'tt_action_item', 'tt_action_item_id');

insert into adm.win_table( db_method ,  table_schema,  table_name)
select 'ttb.tt_variant', 'ttb', 'tt_action' union
select 'ttb.tt_variant', 'ttb', 'tt_action_item' union
select 'ttb.tt_variant', 'ttb', 'tt_tr_period' union
select 'ttb.tt_variant', 'ttb', 'tt_out' union
select 'ttb.tt_variant', 'ttb', 'tt_action_type' union
select 'ttb.tt_variant', 'ttb', 'tt_at_round' union
select 'ttb.tt_variant', 'ttb', 'tt_at_round_stop' union
select 'ttb.tt_variant', 'ttb', 'atgrs_time' union
select 'ttb.tt_variant', 'ttb', 'tt_at_stop' union
select 'ttb.tt_variant', 'ttb', 'tt_action_group' union
select 'ttb.tt_variant', 'ttb', 'tt_action_type2group' union
select 'ttb.tt_variant', 'ttb', 'tt_schedule' union
select 'ttb.tt_variant', 'ttb', 'tt_schedule_item' union
select 'ttb.tt_variant', 'ttb', 'tt_period' union
select 'ttb.tt_variant', 'ttb', 'tt_tr' union
select 'ttb.tt_variant', 'ttb', 'tt_calendar' union
select 'ttb.tt_variant', 'ttb', 'tt_peak_hour' union
select 'ttb.tt_variant', 'ttb', 'tt_change_time' union
select 'ttb.tt_variant', 'ttb', 'tt_driver' 

