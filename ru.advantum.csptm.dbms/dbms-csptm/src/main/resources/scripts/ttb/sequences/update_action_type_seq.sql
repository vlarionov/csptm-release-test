BEGIN;
LOCK TABLE ttb.action_type IN EXCLUSIVE MODE;
SELECT setval('ttb.action_type_action_type_id_seq', coalesce((SELECT max(ttb.action_type.action_type_id) + 1
                                                              FROM ttb.action_type), 1), FALSE);
COMMIT;
