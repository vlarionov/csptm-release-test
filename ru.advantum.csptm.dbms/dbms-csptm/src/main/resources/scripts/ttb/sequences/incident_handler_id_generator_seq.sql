drop SEQUENCE ttb.incident_handler_id_generator_seq;

CREATE SEQUENCE ttb.incident_handler_id_generator_seq
INCREMENT 1000
START 1
CYCLE;