create or replace function ttb.get_avg_interval_plan_for_route(
  p_order_date date
  ,p_route_id integer
  ,p_period_start timestamp
  ,p_period_finish timestamp
  ,p_move_direction_id integer)
  returns interval as
$body$
select avg(time_begin - prev_time_begin)
from (
  select
    route_variant.route_id,
    tai.time_begin,
    lag(tai.time_begin, 1, tai.time_begin)
    over (partition by route_variant.route_id
      order by tai.time_begin) as prev_time_begin
  from rts.route_variant
    join ttb.tt_variant on route_variant.route_variant_id = tt_variant.route_variant_id 
                and not tt_variant.sign_deleted
                and not route_variant.sign_deleted
    join ttb.tt_out on tt_variant.tt_variant_id = tt_out.tt_variant_id and not tt_out.sign_deleted
    join ttb.tt_action on tt_action.tt_out_id = tt_out.tt_out_id and not tt_action.sign_deleted
    join ttb.tt_action_item tai on tai.tt_action_id = tt_action.tt_action_id and not tai.sign_deleted
    join ttb.tt_action_round tar on tar.tt_action_id = tt_action.tt_action_id
    join rts.round r on r.round_id = tar.round_id
                        and r.action_type_id = tt_action.action_type_id
    join rts.stop_item2round si2r on si2r.round_id = r.round_id
                                     and si2r.stop_item2round_id = tai.stop_item2round_id and not si2r.sign_deleted
  where tai.time_begin between p_period_start and p_period_finish
        and tt_variant.order_date = p_order_date
        and si2r.order_num = 1
        and r.move_direction_id = p_move_direction_id
        and route_variant.route_id = p_route_id
) a
where time_begin - prev_time_begin >= interval '1 minute'
$body$
language sql immutable;