﻿-- ExtGridCrosstab##ctmeta,col:capacity_name;row:depo_name|tr_type_name|route_num|order_date|order_hours;val:avg_out

drop function ttb.release_operreport (numeric, text);
create or replace function ttb.release_operreport (p_id_account numeric, p_attr text) 
RETURNS TABLE (depo_name text,
               tr_type_name text,
               route_num text,
               order_date text,
               order_hours text,
               capacity_name text,
               tr_cnt smallint,
               line_order smallint) AS
$$
declare
  -- тип отчета - по "транспортным суткам" или по часу
  l_report_type text := jofl.jofl_pkg$extract_varchar(p_attr, 'P_REPORT_TYPE', true);
  -- даты и часы отчета
  l_rep_dt timestamp := jofl.jofl_pkg$extract_date(p_attr, 'f_DT', true);
  l_rep_hr smallint  := (jofl.jofl_pkg$extract_number(p_attr, 'f_hr_int', true))::smallint;
  l_hr_str smallint;
  l_utc_offset interval;

  -- фильтры по Вид ТС -> ТП -> маршрут
  l_tr_type_list smallint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_tr_type_id', true);
  l_depo_list    smallint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', true);
  l_route_list   smallint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_route_id', true);
  -- список активностей, включаемых в отчет
  la_action_incl smallint[];
  l_round smallint[];
 begin
  la_action_incl := ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$production_round(), false)  || -- промышленный рейс
                    ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$bn_round_break(), false)    || -- межрейсовая стоянка
                    ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$dinner(), false)            || -- обед
                    ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$lunch(), false)             || -- технологический рейс на обед
                    ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$maneuver(), false)          || -- маневровый рейс
                    ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$pause(), false)             || -- отстой
                    ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$pause_with_dinner(), false) ;  -- отстой с обедом
  l_round := ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$round(), false);

  l_hr_str := l_rep_hr;
  l_utc_offset := ttb.get_utc_offset();
  l_rep_hr := l_rep_hr - extract('hour' from l_utc_offset);

  return query
with ttv_list as ( /* варианты расписаний для которых будем строить отчет */
                  SELECT ttv.tt_variant_id as oper_ttv_id,
                         ttv.parent_tt_variant_id as plan_ttv_id,
                         rt.route_num,
                         to_char(ttv.order_date, 'DD.MM.YYYY') AS order_date,
                         to_char(date_trunc('day', current_timestamp) + l_hr_str * interval '1 hours', 'HH24:MI') as order_hours,
                         trc.tr_capacity_id,
                         trc.short_name,
                         rt.route_id,
                         d2rt.depo_id,
                         dp.name_full as depo_name,
                         rt.tr_type_id,
                         trt.name as tr_type_name
                    FROM ttb.tt_variant ttv
                    JOIN rts.route_variant rtv ON rtv.route_variant_id = ttv.route_variant_id
                    JOIN rts.route rt ON rt.route_id = rtv.route_id
                    JOIN rts.depo2route d2rt ON d2rt.route_id = rt.route_id
                    JOIN core.tr_capacity trc ON 1 = 1
                    join core.entity dp on dp.entity_id = d2rt.depo_id
                    join core.tr_type trt on trt.tr_type_id = rt.tr_type_id
                   WHERE NOT ttv.sign_deleted AND
                         ttv.parent_tt_variant_id IS NOT NULL and
                         ttv.tt_status_id in (ttb.tt_status_pkg$arch(), ttb.tt_status_pkg$active())
                         -- дата отчета
                         and ttv.order_date = date_trunc('day', l_rep_dt)
                         -- фильтр по Виду ТС
                         and (array_length(l_tr_type_list, 1) is null or rt.tr_type_id = any(l_tr_type_list))
                         -- фильтр по ТП
                         and (array_length(l_depo_list, 1) is null or d2rt.depo_id = any(l_depo_list))
                         -- фильтр по маршрутам
                         and (array_length(l_route_list, 1) is null or rt.route_id = any(l_route_list))),
plan_source as ( /* сырые данные из планового расписания */
                     select distinct
                            ttvl_p.plan_ttv_id as ttv_id,
                            out_p.tt_out_id,
                            null::bigint as tr_id,
                            out_p.tr_capacity_id,
                            out_p.tt_out_num,
                            out_p.parent_tt_out_id,
                            act_p.tt_action_id,
                            act_p.action_type_id,
                            date_trunc('day', min(acti_p.time_begin - l_utc_offset) over (partition by out_p.tt_out_id)) as out_date_begin,
                            min(acti_p.time_begin - l_utc_offset) over (partition by acti_p.tt_action_id) as action_time_begin,
                            max(acti_p.time_end - l_utc_offset) over (partition by acti_p.tt_action_id) as action_time_end,
                            int8range(extract ('epoch' from min(acti_p.time_begin - l_utc_offset) over (partition by acti_p.tt_action_id) - date_trunc('day', min(acti_p.time_begin - l_utc_offset) over (partition by out_p.tt_out_id)))::bigint,
                                      extract ('epoch' from max(acti_p.time_end - l_utc_offset) over (partition by acti_p.tt_action_id) - date_trunc('day', min(acti_p.time_begin - l_utc_offset) over (partition by out_p.tt_out_id)))::bigint,
                                      '[)') as action_item_range
                     from (select distinct t1.plan_ttv_id, t1.tr_capacity_id from  ttv_list t1) ttvl_p
                     join ttb.tt_out out_p on  out_p.tt_variant_id  = ttvl_p.plan_ttv_id and
                                               out_p.tr_capacity_id = ttvl_p.tr_capacity_id
                     join ttb.tt_action act_p on act_p.tt_out_id = out_p.tt_out_id
                     join ttb.tt_action_item acti_p on acti_p.tt_action_id = act_p.tt_action_id
                     where not out_p.sign_deleted and
                           not act_p.sign_deleted and
                           not acti_p.sign_deleted and
                           act_p.action_type_id = any (la_action_incl)
  ),
plan_tr as (select psrc.ttv_id,
                       psrc.tr_capacity_id,
                       count(distinct psrc.tt_out_id) as tr_cnt
                from plan_source psrc
                where l_rep_hr is null or
                      (l_rep_hr is not null and
                       extract('epoch' from l_rep_hr * interval '1 hour')::bigint <@ psrc.action_item_range)
                group by psrc.ttv_id,
                         grouping sets ((psrc.tr_capacity_id), ())),
oper_source as ( /* сырые данные из оперативного расписания */
                     select distinct
                            ttvl_o.oper_ttv_id as ttv_id,
                            out_o.tt_out_id,
                            out_o.tr_id,
                            out_o.tr_capacity_id,
                            out_o.tt_out_num,
                            out_o.parent_tt_out_id,
                            act_o.tt_action_id,
                            act_o.action_type_id,
                            date_trunc('day', min(acti_o.time_begin) over (partition by out_o.tt_out_id)) as out_date_begin,
                            min(acti_o.time_begin) over (partition by acti_o.tt_action_id) as action_time_begin_plan,
                            max(acti_o.time_end) over (partition by acti_o.tt_action_id) as action_time_end_plan,
                            /* используем фактические данные */
                            min(ttaf.time_fact_begin) over (partition by acti_o.tt_action_id) as action_time_begin_fact,
                            max(ttaf.time_fact_end) over (partition by acti_o.tt_action_id) as action_time_end_fact,
                            ttar.round_status_id,
                            ttar.time_fact_begin as action_time_begin_round,
                            ttar.time_fact_end as action_time_end_round
                     from (select distinct t2.oper_ttv_id, t2.tr_capacity_id from  ttv_list t2) ttvl_o
                     join ttb.tt_out out_o on  out_o.tt_variant_id  = ttvl_o.oper_ttv_id and
                                               out_o.tr_capacity_id = ttvl_o.tr_capacity_id
                     join ttb.tt_action act_o on act_o.tt_out_id = out_o.tt_out_id
                     join ttb.tt_action_item acti_o on acti_o.tt_action_id = act_o.tt_action_id
                      /* учитываем фактические данные */
                     left join ttb.tt_action_round ttar on ttar.tt_action_id = act_o.tt_action_id
                     left join ttb.tt_action_fact ttaf on ttaf.stop_item2round_id = acti_o.stop_item2round_id and
                                                     ttaf.tt_action_id = acti_o.tt_action_id
                     where not out_o.sign_deleted and
                           not act_o.sign_deleted and
                           not acti_o.sign_deleted),
oper_source_ex as (select ose.ttv_id,
                          ose.tt_out_id,
                          ose.parent_tt_out_id,
                          ose.tr_id,
                          ose.tr_capacity_id,
                          ose.tt_out_num,
                          ose.out_date_begin,
                          ose.tt_action_id,
                          ose.action_type_id,
                          ose.round_status_id,
                          case
                           when ose.action_type_id = any (l_round)
                            then ose.action_time_begin
                           else coalesce(pred_os.action_time_end_round, pred_os.action_time_end_fact)
                          end as action_time_begin,
                          case
                           when ose.action_type_id = any (l_round)
                            then ose.action_time_end
                           else coalesce(next_os.action_time_begin_round, next_os.action_time_begin_fact)
                          end as action_time_end,
                          extract ('epoch' from  (case
                                                   when ose.action_type_id = any (l_round)
                                                    then ose.action_time_begin
                                                   else coalesce(pred_os.action_time_end_round, pred_os.action_time_end_fact)
                                                  end) - ose.out_date_begin)::bigint as start_sec,
                           extract ('epoch' from (case
                                                   when ose.action_type_id = any (l_round)
                                                    then ose.action_time_end
                                                   else coalesce(next_os.action_time_begin_round, next_os.action_time_begin_fact)
                                                  end) - ose.out_date_begin)::bigint as end_sec
                   from (select os.ttv_id,
                                os.tt_out_id,
                                os.parent_tt_out_id,
                                os.tr_id,
                                os.tr_capacity_id,
                                os.tt_out_num,
                                os.out_date_begin,
                                os.tt_action_id,
                                os.action_type_id,
                                os.round_status_id,
                                coalesce(os.action_time_begin_round, os.action_time_begin_fact) as action_time_begin,
                                coalesce(os.action_time_end_round, os.action_time_end_fact) as action_time_end,
                                lag(os.tt_action_id)  over (partition by os.tt_out_id order by os.action_time_begin_plan, os.action_time_end_plan rows between unbounded preceding and unbounded following) as pred_action_id,
                                lead(os.tt_action_id) over (partition by os.tt_out_id order by os.action_time_begin_plan, os.action_time_end_plan rows between unbounded preceding and unbounded following) as next_action_id
                         from oper_source os) ose
                        left join oper_source pred_os on pred_os.tt_action_id = ose.pred_action_id and pred_os.round_status_id in (1, 4, 6)
                        left join oper_source next_os on next_os.tt_action_id = ose.next_action_id and next_os.round_status_id in (1, 4, 6)
                   where  ose.action_type_id = any (la_action_incl)
),
oper_tr as (select osrc.ttv_id,
                       osrc.tr_capacity_id,
                       count(distinct osrc.tr_id) as tr_cnt
                from oper_source_ex osrc
                where l_rep_hr is null or
                      (l_rep_hr is not null and
                       osrc.start_sec is not null and
                       osrc.end_sec is not null and
                       osrc.start_sec <= osrc.end_sec and
                       extract('epoch' from l_rep_hr * interval '1 hour')::bigint <@  int8range(osrc.start_sec, osrc.end_sec, '[)'))
                group by osrc.ttv_id,
                         grouping sets ((osrc.tr_capacity_id), ())
  )
/*
oper_source as ( /* сырые данные из оперативного расписания */
                     select distinct
                            ttvl_o.oper_ttv_id as ttv_id,
                            out_o.tt_out_id,
                            out_o.tr_id,
                            out_o.tr_capacity_id,
                            out_o.tt_out_num,
                            out_o.parent_tt_out_id,
                            act_o.tt_action_id,
                            act_o.action_type_id,
                            date_trunc('day', min(acti_o.time_begin) over (partition by out_o.tt_out_id)) as out_date_begin,
/* используем фактические данные */
min(ttaf.time_fact_begin) over (partition by acti_o.tt_action_id) as action_time_begin,
max(ttaf.time_fact_end) over (partition by acti_o.tt_action_id) as action_time_end,
int8range(extract ('epoch' from min(ttaf.time_fact_begin) over (partition by acti_o.tt_action_id) - date_trunc('day', min(acti_o.time_begin) over (partition by out_o.tt_out_id)))::bigint,
          extract ('epoch' from max(ttaf.time_fact_end) over (partition by acti_o.tt_action_id) - date_trunc('day', min(acti_o.time_begin) over (partition by out_o.tt_out_id)))::bigint,
          '[)') as action_item_range

--                            min(acti_o.time_begin) over (partition by acti_o.tt_action_id) as action_time_begin,
--                            max(acti_o.time_end) over (partition by acti_o.tt_action_id) as action_time_end,
--                            int8range(extract ('epoch' from min(acti_o.time_begin) over (partition by acti_o.tt_action_id) - date_trunc('day', min(acti_o.time_begin) over (partition by out_o.tt_out_id)))::bigint,
--                                      extract ('epoch' from max(acti_o.time_end) over (partition by acti_o.tt_action_id) - date_trunc('day', min(acti_o.time_begin) over (partition by out_o.tt_out_id)))::bigint,
--                                      '[)') as action_item_range
                     from (select distinct t2.oper_ttv_id, t2.tr_capacity_id from  ttv_list t2) ttvl_o
                     join ttb.tt_out out_o on  out_o.tt_variant_id  = ttvl_o.oper_ttv_id and
                                               out_o.tr_capacity_id = ttvl_o.tr_capacity_id
                     join ttb.tt_action act_o on act_o.tt_out_id = out_o.tt_out_id
                     join ttb.tt_action_item acti_o on acti_o.tt_action_id = act_o.tt_action_id
                      /* учитываем фактические данные */
                     join ttb.tt_action_round ttar on ttar.tt_action_id = act_o.tt_action_id and
                                                      ttar.round_status_id in (ttb.round_status_pkg$passed(), ttb.round_status_pkg$in_progress(), ttb.round_status_pkg$passed_partial())
                     join ttb.tt_action_fact ttaf on ttaf.stop_item2round_id = acti_o.stop_item2round_id and
                                                     ttaf.tt_action_id = acti_o.tt_action_id
                     where not out_o.sign_deleted and
                           not act_o.sign_deleted and
                           not acti_o.sign_deleted and
                           act_o.action_type_id = any (la_action_incl)
  ),
oper_tr as (select osrc.ttv_id,
                       osrc.tr_capacity_id,
                       count(distinct osrc.tr_id) as tr_cnt
                from oper_source osrc
                where l_rep_hr is null or
                      (l_rep_hr is not null and
                       extract('epoch' from l_rep_hr * interval '1 hour')::bigint <@ osrc.action_item_range)
                group by osrc.ttv_id,
                         grouping sets ((osrc.tr_capacity_id), ()))
*/
-- главный select отчета
select ttvl1.depo_name,
       ttvl1.tr_type_name,
       ttvl1.route_num,
       ttvl1.order_date,
       ttvl1.order_hours,
       'Класс ' || ttvl1.short_name || ' факт.' as capacity_name,
       oa.tr_cnt::smallint as avg_out,
       10::smallint as order_part
from ttv_list ttvl1
     left join oper_tr oa on oa.ttv_id = ttvl1.oper_ttv_id and
                             oa.tr_capacity_id = ttvl1.tr_capacity_id
union all
select ttvl2.depo_name,
       ttvl2.tr_type_name,
       ttvl2.route_num,
       ttvl2.order_date,
       ttvl2.order_hours,
       'Класс ' || ttvl2.short_name || ' план.' as capacity_name,
       pa.tr_cnt::smallint as avg_out,
       10::smallint as order_part
from ttv_list ttvl2
     left join plan_tr pa on pa.ttv_id = ttvl2.plan_ttv_id and
                             pa.tr_capacity_id = ttvl2.tr_capacity_id
union all
select ttvl3.depo_name,
       ttvl3.tr_type_name,
       ttvl3.route_num,
       ttvl3.order_date,
       ttvl3.order_hours,
       'Фактический выпуск ТС' as capacity_name,
       oas.tr_cnt::smallint as avg_out,
       40::smallint as order_part
from (select distinct l1.oper_ttv_id,
                      l1.route_num,
                      l1.order_date,
                      l1.order_hours,
                      l1.route_id,
                      l1.depo_id,
                      l1.tr_type_id,
                      l1.depo_name,
                      l1.tr_type_name
                 from ttv_list l1) ttvl3
     left join oper_tr oas on oas.ttv_id = ttvl3.oper_ttv_id and
                              oas.tr_capacity_id is null
union all
select ttvl4.depo_name,
       ttvl4.tr_type_name,
       ttvl4.route_num,
       ttvl4.order_date,
       ttvl4.order_hours,
       'Плановый выпуск ТС' as capacity_name,
       pas.tr_cnt::smallint as avg_out,
       30::smallint as order_part
from (select distinct l2.oper_ttv_id,
                      l2.plan_ttv_id,
                      l2.route_num,
                      l2.order_date,
                      l2.order_hours,
                      l2.route_id,
                      l2.depo_id,
                      l2.tr_type_id,
                      l2.depo_name,
                      l2.tr_type_name
                 from ttv_list l2) ttvl4
     left join plan_tr pas on pas.ttv_id = ttvl4.plan_ttv_id and
                               pas.tr_capacity_id is null
union all
select ttvl5.depo_name,
       ttvl5.tr_type_name,
       ttvl5.route_num,
       ttvl5.order_date,
       ttvl5.order_hours,
       'Отклонение по выпуску' as capacity_name,
       (paf.tr_cnt - oaf.tr_cnt)::smallint as avg_out,
       50::smallint as order_part
from (select distinct l3.oper_ttv_id,
                      l3.plan_ttv_id,
                      l3.route_num,
                      l3.order_date,
                      l3.order_hours,
                      l3.route_id,
                      l3.depo_id,
                      l3.tr_type_id,
                      l3.depo_name,
                      l3.tr_type_name
                 from ttv_list l3) ttvl5
     left join plan_tr paf on paf.ttv_id = ttvl5.plan_ttv_id and
                               paf.tr_capacity_id is null
     left join oper_tr oaf on oaf.ttv_id = ttvl5.oper_ttv_id and
                              oaf.tr_capacity_id is null
order by route_num, tr_type_name, depo_name, order_date, order_part, capacity_name;

end;
$$
LANGUAGE 'plpgsql';
