create or replace function ttb.get_avg_interval_fact_for_route_a(
  p_order_date date
  ,p_route_id integer
  ,p_period_start timestamp
  ,p_period_finish timestamp
  ,p_move_direction_id integer)
  returns interval as
$body$
declare
    l_return interval;
begin
  select avg(time_fact_begin - prev_time_fact_begin) fact_diff_a
  into l_return
  from (
    SELECT
      taf.time_fact_begin,
      lag(taf.time_fact_begin, 1, taf.time_fact_begin)
      OVER (PARTITION BY route_variant.route_id
        ORDER BY taf.time_fact_begin) AS prev_time_fact_begin
    FROM rts.route_variant
      JOIN ttb.tt_variant ON route_variant.route_variant_id = tt_variant.route_variant_id
                AND NOT tt_variant.sign_deleted
                AND not route_variant.sign_deleted
      JOIN ttb.tt_out ON tt_variant.tt_variant_id = tt_out.tt_variant_id AND NOT tt_out.sign_deleted
      JOIN ttb.tt_action ON tt_action.tt_out_id = tt_out.tt_out_id AND NOT tt_action.sign_deleted
      JOIN ttb.tt_action_item tai ON tai.tt_action_id = tt_action.tt_action_id
      JOIN ttb.tt_action_round tar ON tar.tt_action_id = tt_action.tt_action_id
      JOIN rts.round r ON r.round_id = tar.round_id
                          AND r.action_type_id = tt_action.action_type_id AND NOT r.sign_deleted
      JOIN rts.stop_item2round si2r ON si2r.round_id = r.round_id
                                       AND si2r.stop_item2round_id = tai.stop_item2round_id AND NOT si2r.sign_deleted
      JOIN ttb.tt_action_fact taf ON tt_action.tt_action_id = taf.tt_action_id
                                     AND tai.stop_item2round_id = taf.stop_item2round_id
    WHERE taf.time_fact_begin BETWEEN p_period_start AND p_period_finish
          AND tt_variant.order_date = p_order_date
          AND si2r.order_num = 1
          AND r.move_direction_id = p_move_direction_id
          and route_variant.route_id = p_route_id
  ) a
  where time_fact_begin - prev_time_fact_begin >= interval '1 minute';

  return l_return;
end;
$body$
language plpgsql immutable;