create or replace function ttb.get_min_interval_fact_for_route(p_order_date date, p_route_id integer, p_period_start timestamp without time zone, p_period_finish timestamp without time zone, p_move_direction_id integer) returns interval
immutable
language sql
as $$
select min(time_fact_begin - prev_time_fact_begin) fact_diff_a
  from (
    SELECT
      taf.time_fact_begin,
      lag(taf.time_fact_begin, 1, taf.time_fact_begin)
      OVER (PARTITION BY route_variant.route_id
        ORDER BY taf.time_fact_begin) AS prev_time_fact_begin
    FROM rts.route_variant
      JOIN ttb.tt_variant ON route_variant.route_variant_id = tt_variant.route_variant_id
      JOIN ttb.tt_out ON tt_variant.tt_variant_id = tt_out.tt_variant_id
      JOIN ttb.tt_action ON tt_action.tt_out_id = tt_out.tt_out_id
      JOIN ttb.tt_action_item tai ON tai.tt_action_id = tt_action.tt_action_id
      JOIN ttb.tt_action_round tar ON tar.tt_action_id = tt_action.tt_action_id
      JOIN rts.round r ON r.round_id = tar.round_id
                          AND r.action_type_id = tt_action.action_type_id
      JOIN rts.stop_item2round si2r ON si2r.round_id = r.round_id
                                       AND si2r.stop_item2round_id = tai.stop_item2round_id
      JOIN rts.stop_item si ON si.stop_item_id = si2r.stop_item_id
      JOIN rts.stop_location sl on sl.stop_location_id = si.stop_location_id
      JOIN rts.stop s ON s.stop_id = sl.stop_id
      JOIN ttb.tt_action_fact taf ON tt_action.tt_action_id = taf.tt_action_id
                                     AND tai.stop_item2round_id = taf.stop_item2round_id
    WHERE taf.time_fact_begin BETWEEN p_period_start AND p_period_finish
          AND tt_variant.order_date = p_order_date
          AND NOT tt_variant.sign_deleted
          AND NOT tt_action.sign_deleted
          AND si2r.order_num = 1
          AND r.move_direction_id = p_move_direction_id
          and route_variant.route_id = p_route_id
  ) a
  where time_fact_begin - prev_time_fact_begin >= interval '1 minute';
$$;
