create or replace function ttb.get_local_timestamp(
       p_timestamp timestamp
) RETURNS timestamp AS
$$
select (p_timestamp at time zone tune_value)::timestamp
from ttb.tune where tune_id = 1;
$$
LANGUAGE SQL STABLE;

create or replace function ttb.get_utc_offset() RETURNS INTERVAL AS
$$
declare
  l_time_zone TEXT;
  l_now TIMESTAMP = now();
BEGIN
  select tune_value INTO l_time_zone from ttb.tune where tune_id = 1;
  return ((l_now at time zone 'UTC')::timestamp) - ((l_now at time zone l_time_zone)::timestamp);
end;$$
language plpgsql;