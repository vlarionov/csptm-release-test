create or replace function ttb.ttv_calend_type(in_ttv int, out tag_name text, out calend_type int) returns record
-- tag_name - список тегов через зпт
-- calend_type - тип календаря, 1-будни, 2-вых, 3-единый
language sql
as $$
  select tag_name,
         case when calend_type = 0 then 3 else calend_type end caland_type
  from
    (
      select string_agg(tag_name_short, ', ' order by calendar_tag_id) tag_name,
              -- 1 - будни, 2 - вых, 3 - единое
            (count(distinct 1) filter(where is_work) + 2*(count(distinct 1) filter(where is_holiday)))::int  calend_type
      from
        (
          select ct.calendar_tag_group_id = 2 or ct.calendar_tag_id = 9 or t.calendar_tag_id in (6, 7) is_holiday, -- праздники/выходние/сб/вс
                 ct.calendar_tag_id = 8 or ct.calendar_tag_id in (1, 2, 3, 4, 5) is_work, -- будни/пн-пт
                 t.*, ct.tag_name_short
          from ttb.tt_calendar t join ttb.calendar_tag ct on t.calendar_tag_id = ct.calendar_tag_id
          where t.tt_variant_id = in_ttv --in (279, 459)
        ) q
    ) q
$$;
