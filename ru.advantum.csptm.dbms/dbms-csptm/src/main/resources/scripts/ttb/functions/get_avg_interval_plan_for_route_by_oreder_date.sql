create or replace function ttb.get_avg_interval_plan_for_route_by_oreder_date(
  p_order_date date
  ,p_route_id integer
  ,p_move_direction_id integer)
  returns interval as
$body$
select avg(time_begin - prev_time_begin)
from (
  SELECT
    route_variant.route_id,
    tai.time_begin,
    lag(tai.time_begin, 1, tai.time_begin)
    OVER (PARTITION BY route_variant.route_id
      ORDER BY tai.time_begin) AS prev_time_begin
  FROM rts.route_variant
    JOIN ttb.tt_variant ON route_variant.route_variant_id = tt_variant.route_variant_id
            AND NOT tt_variant.sign_deleted
            AND NOT route_variant.sign_deleted
    JOIN ttb.tt_out ON tt_variant.tt_variant_id = tt_out.tt_variant_id
            AND NOT tt_out.sign_deleted
    JOIN ttb.tt_action ON tt_action.tt_out_id = tt_out.tt_out_id
            AND NOT tt_action.sign_deleted
    JOIN ttb.tt_action_item tai ON tai.tt_action_id = tt_action.tt_action_id
    JOIN ttb.tt_action_round tar ON tar.tt_action_id = tt_action.tt_action_id
    JOIN rts.round r ON r.round_id = tar.round_id
                        AND r.action_type_id = tt_action.action_type_id
                        AND NOT r.sign_deleted
    JOIN rts.stop_item2round si2r ON si2r.round_id = r.round_id
                                     AND si2r.stop_item2round_id = tai.stop_item2round_id
                                     AND NOT si2r.sign_deleted
  WHERE tt_variant.order_date = p_order_date
        AND si2r.order_num = 1
        AND r.move_direction_id = p_move_direction_id
        and route_variant.route_id = p_route_id
) a
where time_begin - prev_time_begin >= interval '1 minute'
$body$
language sql immutable;