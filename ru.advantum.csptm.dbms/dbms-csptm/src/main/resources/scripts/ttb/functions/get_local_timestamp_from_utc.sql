create or replace function ttb.get_local_timestamp_from_utc(
       p_timestamp timestamp
) RETURNS timestamp AS
$$
select (p_timestamp::timestamptz at time zone tune_value)::timestamp
from ttb.tune where tune_id = 1;
$$
LANGUAGE SQL STABLE;