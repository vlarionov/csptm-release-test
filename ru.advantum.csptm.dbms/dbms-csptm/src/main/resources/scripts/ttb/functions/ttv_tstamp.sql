create or replace function ttb.ttv_tstamp(d int , h int, m int, s int = 0) returns timestamp
language sql as
$$
  select '1900-01-01'::timestamp + make_interval(days => d, hours => h, mins => m, secs => s)
$$;

create or replace function ttb.ttv_tstamp(d int, t time) returns timestamp
language sql as
$$
  select '1900-01-01'::timestamp +  make_interval(days => d) + t
$$;
