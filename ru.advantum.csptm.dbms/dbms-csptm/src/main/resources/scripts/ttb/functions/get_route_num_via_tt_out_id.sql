create or replace function ttb.get_route_num_via_tt_out_id(
       p_tt_out_id int
) RETURNS text AS
$$
SELECT route_num
from rts.route
  JOIN rts.route_variant on route.current_route_variant_id = route_variant.route_variant_id
  join ttb.tt_variant ON tt_variant.route_variant_id = route_variant.route_variant_id
  join ttb.tt_out on tt_variant.tt_variant_id = tt_out.tt_variant_id
where tt_out.tt_out_id = p_tt_out_id;
$$
LANGUAGE SQL STABLE;