﻿create or replace function ttb.ttv_round_list(p_tt_variant int, p_route_variant int) returns
-- шапка отчета - список рейсов варианта расписания
table(
  code text,
  round_length numeric,
  first_stop text,
  last_stop text
)
language sql
as $$
  select q.code, q.round_length, st1.name fisrt_stop, st2.name last_stop
  from
    (
      select q.code,
             max(q.stop_item_id) filter(where q.order_num = q.first_stop_num and q.move_direction_id = q.move_dir) first_stop_id,
             max(q.stop_item_id) filter(where q.order_num = q.last_stop_num and q.move_direction_id = q.move_dir) last_stop_id,
             round_count cnt,
             sum(length_sector::numeric)/1000 round_length2,
             round(sum(length_sector::numeric)/q.round_count/1000, 3) round_length
      from
        (
          select rnd.round_id, rnd.move_direction_id, rnd.code, rnd.round_num, rnd.round_count, rnd.move_dir,
                 case when ttb.action_type_pkg$is_production_round(rnd.action_type_id) then 1 else 0 end is_production,
                 st2r.order_num,
                 case when move_dir = 1 then 1 else first_value(st2r.order_num) over order_num_desc end first_stop_num,
                 case when move_dir = 1 then first_value(st2r.order_num) over order_num_desc else 1 end last_stop_num,
                 st2r.stop_item_id,
                 st2r.length_sector
          from
           (
             with ttr as
             (
               select rnd.round_num
               from rts.round rnd join ttb.tt_action_round ar on rnd.round_id = ar.round_id
                 join ttb.tt_action a on ar.tt_action_id = a.tt_action_id
                 join ttb.tt_out t on a.tt_out_id = t.tt_out_id
               where rnd.route_variant_id =  p_route_variant
                 and t.tt_variant_id = p_tt_variant
             )
             select rnd.*,
                    min(rnd.move_direction_id) over(PARTITION BY rnd.round_num) move_dir,
                    count(rnd.round_id) over(PARTITION BY rnd.round_num) round_count
             from rts.round rnd
             where rnd.route_variant_id =  p_route_variant
               and ttb.action_type_pkg$is_production_round(rnd.action_type_id)
               and not rnd.sign_deleted
               and exists (select 1 from ttr where ttr.round_num = rnd.round_num)
           ) rnd join rts.stop_item2round st2r on rnd.round_id = st2r.round_id
          --where rnd.move_dir = rnd.move_direction_id
            and not st2r.sign_deleted
          window order_num_desc as (PARTITION BY st2r.round_id order by order_num desc)
        ) q
      group by q.round_num, q.code, q.round_count
    ) q join rts.stop_item sti1 on sti1.stop_item_id = q.first_stop_id
        join rts.stop_location stl1 on stl1.stop_location_id = sti1.stop_location_id
        join rts.stop st1 on stl1.stop_id = st1.stop_id
        join rts.stop_item sti2 on sti2.stop_item_id = q.last_stop_id
        join rts.stop_location stl2 on stl2.stop_location_id = sti2.stop_location_id
        join rts.stop st2 on stl2.stop_id = st2.stop_id
  order by q.code
$$
