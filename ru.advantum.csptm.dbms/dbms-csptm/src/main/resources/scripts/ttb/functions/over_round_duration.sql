create or replace function ttb.over_round_duration(
       p_norm_id bigint,
       p_rv_id bigint,
       p_start_time time = '07:00:00'::time,
       p_tr_capacity bigint = null,
    out p_first_stop_item int,
    out p_last_stop_item int,
    out p_duration numeric
) LANGUAGE sql as $$
    with rnd as (
        select case when q.move_direction_id = 1 then q.ordernum else q.ordernum - 1 end ordernum,
               q.end_of_route,
               q.round_id, q.stop_item_id,
               q.next_stop_item,
               q.first_stop_item, q.last_stop_item,
               nst.hour_from, nst.hour_to, nst.between_stop_dur,
               nst.tr_capacity_id
        from
          (
            select row_number() over(order by rnd.move_direction_id, st2r.order_num) ordernum,
                   rnd.round_id,
                   rnd.move_direction_id,
                   st2r.stop_item_id,
                   st2r.order_num,
                   lead(st2r.stop_item_id) over(partition by rnd.round_id order by order_num) next_stop_item,
                   first_value(st2r.stop_item_id)  over ab first_stop_item,
                   first_value(st2r.stop_item_id)  over ba last_stop_item,
                   last_value(st2r.stop_item_id) over rndw   end_of_route
            from rts.round rnd join rts.stop_item2round st2r on rnd.round_id = st2r.round_id
            where rnd.route_variant_id = p_rv_id
              and not rnd.sign_deleted
              and rnd.code ='00' and not st2r.sign_deleted and not rnd.sign_deleted and ttb.action_type_pkg$is_production_round(rnd.action_type_id)
            window ab as (order by case when rnd.move_direction_id = 1 then st2r.order_num end nulls last),
                   ba as (order by case when rnd.move_direction_id = 2 then st2r.order_num end nulls last),
                   rndw as (partition by rnd.round_id order by order_num rows between unbounded preceding and unbounded following )
          ) q join ttb.norm_between_stop nst on nst.stop_item_1_id = q.stop_item_id and nst.stop_item_2_id = q.next_stop_item
        where nst.norm_id = p_norm_id
          and ((nst.tr_capacity_id = p_tr_capacity) or (p_tr_capacity is null))
      )
  select first_stop_item, last_stop_item, core.interval2minutes(avg(round_duration))::numeric round_duration
  from
    (
      select first_stop_item, last_stop_item, tr_capacity_id, max(next_time) - p_start_time round_duration
      from (
           with recursive q2 as
            (
             select rnd.next_stop_item = rnd.end_of_route is_end_of_route,
                    hour_from stop_time,
                    hour_from + make_interval(secs => between_stop_dur) next_time, rnd.*
             from rnd where ordernum = 1 and hour_from = p_start_time
                 union all
             select rnd.next_stop_item = rnd.end_of_route is_end_of_route,
                    q2.next_time stop_time,
                    q2.next_time + make_interval(secs => rnd.between_stop_dur)
                      + case when rnd.next_stop_item = rnd.end_of_route then
                          ( select make_interval(secs => nr.min_inter_round_stop_dur + nr.reg_inter_round_stop_dur)
                            from ttb.norm_round nr
                            where nr.round_id = rnd.round_id
                              and q2.next_time >= nr.hour_from and q2.next_time <nr.hour_to
                              and nr.tr_capacity_id = q2.tr_capacity_id
                              and nr.norm_id = p_norm_id
                          )
                        else make_interval(secs => 0)
                        end
                    as next_time,
                    rnd.*
             from rnd, q2
             where rnd.ordernum = q2.ordernum + 1
               and rnd.tr_capacity_id = q2.tr_capacity_id
--               and q2.next_time >= rnd.hour_from and q2.next_time < rnd.hour_to
               and q2.stop_time >= rnd.hour_from and q2.stop_time < rnd.hour_to
            )
        select q2.*
        from q2
       ) q3
      group by tr_capacity_id, first_stop_item, last_stop_item
    ) q4
group by first_stop_item, last_stop_item
$$
