--drop view ttb.v_tt_schedule_item;

create or replace view ttb.v_tt_schedule_item as
  select sch_it.tt_schedule_id
    ,sch_it.tt_schedule_item_id
    ,sch_it.order_num
    ,sch_it.repeat_time
    ,sch_it.tt_action_group_id
    ,act_gr.tt_variant_id
    ,act_gr.tt_action_group_name
    ,act_gr.ref_group_kind_id
  from  ttb.tt_schedule_item sch_it
    join ttb.tt_action_group act_gr on act_gr.tt_action_group_id = sch_it.tt_action_group_id
  order by sch_it.order_num
;


comment on view ttb.v_tt_schedule_item IS 'Последовательности действий планового расписания';
comment on column ttb.v_tt_schedule_item.tt_variant_id  is 'Вариант расписания';
comment on column ttb.v_tt_schedule_item.tt_schedule_id  is 'Последовательность действий';
comment on column ttb.v_tt_schedule_item.tt_schedule_item_id  is 'Элемент последовательности';
comment on column ttb.v_tt_schedule_item.order_num  is 'Номер в группе';
comment on column ttb.v_tt_schedule_item.ref_group_kind_id  is 'Тип группы  действий';

