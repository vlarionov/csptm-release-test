--drop view ttb.v_tt_out_oper_unplanned;
--drop view ttb.v_tt_out_oper;

create or replace view ttb.v_tt_out_oper as
  select
    tt_out.tt_out_id,
    tt_out.tt_variant_id,
    tr_capacity_id,
    mode_id,
    tt_out_num,
    tt_out.sign_deleted,
    tt_out.sys_period,
    parent_tt_out_id,
    tr_id,
    order_date,
    norm_id,
    route_variant_id
from ttb.tt_out
join ttb.v_tt_variant_oper tva on tt_out.tt_variant_id = tva.tt_variant_id;

COMMENT ON VIEW ttb.v_tt_out_oper IS 'Перечень выходов расписания оперативного';

COMMENT ON COLUMN ttb.v_tt_out_oper.tt_out_id IS 'Выход';
COMMENT ON COLUMN ttb.v_tt_out_oper.tt_variant_id IS 'Вариант расписания';
COMMENT ON COLUMN ttb.v_tt_out_oper.tr_capacity_id IS 'Вместимость ТС';
COMMENT ON COLUMN ttb.v_tt_out_oper.mode_id IS 'Режим труда и отдыха';
COMMENT ON COLUMN ttb.v_tt_out_oper.tt_out_num IS 'Номер выхода';
COMMENT ON COLUMN ttb.v_tt_out_oper.sign_deleted IS 'Удален';
COMMENT ON COLUMN ttb.v_tt_out_oper.sys_period IS 'Текущий период';
COMMENT ON COLUMN ttb.v_tt_out_oper.parent_tt_out_id IS 'ссылка на родительский выход';
COMMENT ON COLUMN ttb.v_tt_out_oper.tr_id IS 'ТС поставленное на выход';