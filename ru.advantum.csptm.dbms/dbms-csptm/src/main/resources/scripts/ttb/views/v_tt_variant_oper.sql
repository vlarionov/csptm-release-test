--drop view ttb.v_tt_out_oper_unplanned;
--drop view ttb.v_tt_out_oper;
--drop view ttb.v_tt_variant_oper;

create or replace view ttb.v_tt_variant_oper as
select
  tt_variant_id,
  norm_id,
  route_variant_id,
  is_all_tr_online,
  sign_deleted,
  sys_period,
  tt_status_id,
  action_period,
  ttv_name,
  ttv_comment,
  is_calc_indicator,
  parent_tt_variant_id,
  order_date
from ttb.tt_variant
where parent_tt_variant_id is not null;

COMMENT ON VIEW ttb.v_tt_variant_oper IS 'Перечень вариантов расписания оперативного';