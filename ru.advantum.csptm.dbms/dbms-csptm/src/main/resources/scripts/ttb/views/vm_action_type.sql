do $$
begin
  if not exists(select * from pg_matviews where matviewname = 'vm_action_type' and schemaname = 'ttb') THEN

        create materialized view ttb.vm_action_type
        tablespace ttb_data
        as
        select act.action_type_id
                ,act.action_type_code
                ,act.action_type_name
                ,ttb.action_type_pkg$is_depo(act.action_type_id) as is_depo
                ,ttb.action_type_pkg$is_round(act.action_type_id) as is_round
                ,ttb.action_type_pkg$is_production_round(act.action_type_id) as is_production_round
                ,ttb.action_type_pkg$is_technical_round(act.action_type_id) as is_technical_round
                ,ttb.action_type_pkg$is_dinner(act.action_type_id) as is_dinner
                ,ttb.action_type_pkg$is_all_pause(act.action_type_id) as is_all_pause
                ,ttb.action_type_pkg$is_bn_round_break(act.action_type_id) as is_bn_round_break
                ,ttb.action_type_pkg$is_break(act.action_type_id) as is_break
        from ttb.action_type act
        with data;

        COMMENT ON materialized VIEW ttb.vm_action_type IS 'Типы действий (материализованное представление с признаками)';
        COMMENT ON COLUMN ttb.vm_action_type.action_type_id IS 'ID';
        COMMENT ON COLUMN ttb.vm_action_type.action_type_name IS 'Наименование';
        COMMENT ON COLUMN ttb.vm_action_type.action_type_code IS 'код';
        COMMENT ON COLUMN ttb.vm_action_type.is_depo IS 'относится к типу рейс парк или из парка';
        COMMENT ON COLUMN ttb.vm_action_type.is_round IS 'относится к типу  рейсы';
        COMMENT ON COLUMN ttb.vm_action_type.is_production_round IS 'относится к типу производственные рейсы';
        COMMENT ON COLUMN ttb.vm_action_type.is_technical_round IS 'относится к типу технологические рейсы';
        COMMENT ON COLUMN ttb.vm_action_type.is_dinner IS 'относится к типу обед';
        COMMENT ON COLUMN ttb.vm_action_type.is_all_pause IS 'относится к типу обед либо отстой';
        COMMENT ON COLUMN ttb.vm_action_type.is_bn_round_break IS 'относится к типу межрейсовая стоянка';
        COMMENT ON COLUMN ttb.vm_action_type.is_break IS 'относится к типу перерыв';

  end if;
end$$;
