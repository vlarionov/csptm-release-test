--drop view ttb.v_tt_schedule_item_det;

create or replace view ttb.v_tt_schedule_item_det as
  select
    tact.action_type_id
    ,tact.tt_action_type_id
    ,tact.tt_variant_id
    ,act2gr.tt_action_type2group_id
    ,sch_it.tt_schedule_id
    ,sch_it.tt_schedule_item_id
    ,r.tr_type_id
    ,at_rnd.round_id
    ,at_rnd.tt_at_round_id
    ,act2gr.order_num+ sch_it.order_num*1000 as order_num_action
    ,act2gr.order_num
    ,sir_b.stop_item_id   as sp_b_id
    ,sir_b.stop_item2round_id  as sir_b_id
    ,sir_e.stop_item_id  as sp_e_id
    ,sir_e.stop_item2round_id  as sir_e_id
    ,act_gr.ref_group_kind_id
    ,tv.norm_id
    ,sch.parent_schedule_id
  from ttb.tt_schedule_item sch_it
    join ttb.tt_schedule sch on sch.tt_schedule_id = sch_it.tt_schedule_id
    join ttb.tt_action_group act_gr on act_gr.tt_action_group_id = sch_it.tt_action_group_id-- and not act_gr.sign_deleted
    join ttb.tt_variant tv on tv.tt_variant_id = act_gr.tt_variant_id
    join rts.route_variant rv on rv.route_variant_id =tv.route_variant_id
    join rts.route r on r.route_id = rv.route_id
    join ttb.tt_action_type2group act2gr on act2gr.tt_action_group_id = act_gr.tt_action_group_id
    join ttb.tt_action_type tact on tact.tt_action_type_id = act2gr.tt_action_type_id
    left join ttb.tt_at_round at_rnd on at_rnd.tt_action_type_id = tact.tt_action_type_id
    left join rts.stop_item2round sir_b on sir_b.round_id =  at_rnd.round_id
                                           and sir_b.order_num = 1
                                           and not sir_b.sign_deleted
    left join rts.stop_item2round sir_e on sir_e.round_id =  at_rnd.round_id
                                           and sir_e.order_num =  (select count(1) from  rts.stop_item2round t where  t.round_id = at_rnd.round_id and not t.sign_deleted)
                                           and not sir_e.sign_deleted
  ;


comment on view ttb.v_tt_schedule_item_det IS 'Детализация настройки последовательности действий планового расписания';
comment on column ttb.v_tt_schedule_item_det.action_type_id  is 'Тип действия';
comment on column ttb.v_tt_schedule_item_det.tt_action_type_id  is 'Тип действия для планового расписания';
comment on column ttb.v_tt_schedule_item_det.tt_variant_id  is 'Вариант расписания';
comment on column ttb.v_tt_schedule_item_det.tt_action_type2group_id  is 'id  в группе';
comment on column ttb.v_tt_schedule_item_det.tt_schedule_id  is 'Последовательность действий';
comment on column ttb.v_tt_schedule_item_det.tt_schedule_item_id  is 'Элемент последовательности';
comment on column ttb.v_tt_schedule_item_det.tr_type_id  is 'Вид ТС';
comment on column ttb.v_tt_schedule_item_det.round_id  is 'Рейс';
comment on column ttb.v_tt_schedule_item_det.tt_at_round_id  is 'Рейс для планового расписания';
comment on column ttb.v_tt_schedule_item_det.order_num_action  is 'Признак для упорядочивания действий';
comment on column ttb.v_tt_schedule_item_det.order_num  is 'Номер в группе';
comment on column ttb.v_tt_schedule_item_det.sp_b_id  is 'ОП, с которой начинается действие последовательности';
comment on column ttb.v_tt_schedule_item_det.sir_b_id  is 'ОП в рейсе, с которой начинается действие последовательности';
comment on column ttb.v_tt_schedule_item_det.sp_e_id  is 'ОП, с которой заканчивается последнее дейстие последовательности';
comment on column ttb.v_tt_schedule_item_det.sir_e_id  is 'ОП врейсе, с которой заканчивается последнее дейстие последовательности';
comment on column ttb.v_tt_schedule_item_det.ref_group_kind_id  is 'Тип группы  действий';
comment on column ttb.v_tt_schedule_item_det.norm_id  is 'Норматив';
