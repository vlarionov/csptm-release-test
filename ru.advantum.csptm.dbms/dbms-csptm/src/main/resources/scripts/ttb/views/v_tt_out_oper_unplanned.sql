--drop view ttb.v_tt_out_oper_unplanned;

create or replace view ttb.v_tt_out_oper_unplanned as
  select
    tt_out.tt_out_id,
    tt_out.tt_variant_id,
    tr_capacity_id,
    mode_id,
    tt_out_num,
    tt_out.sign_deleted,
    tt_out.sys_period,
    parent_tt_out_id,
    tr_id,
    order_date
      norm_id,
    route_variant_id
  from ttb.tt_out
    join ttb.v_tt_variant_oper tva on tt_out.tt_variant_id = tva.tt_variant_id
  where parent_tt_out_id is null;

COMMENT ON VIEW ttb.v_tt_out_oper_unplanned IS 'Перечень выходов расписания оперативного незаплпнированных';

COMMENT ON COLUMN ttb.v_tt_out_oper_unplanned.tt_out_id IS 'Выход';
COMMENT ON COLUMN ttb.v_tt_out_oper_unplanned.tt_variant_id IS 'Вариант расписания';
COMMENT ON COLUMN ttb.v_tt_out_oper_unplanned.tr_capacity_id IS 'Вместимость ТС';
COMMENT ON COLUMN ttb.v_tt_out_oper_unplanned.mode_id IS 'Режим труда и отдыха';
COMMENT ON COLUMN ttb.v_tt_out_oper_unplanned.tt_out_num IS 'Номер выхода';
COMMENT ON COLUMN ttb.v_tt_out_oper_unplanned.sign_deleted IS 'Удален';
COMMENT ON COLUMN ttb.v_tt_out_oper_unplanned.sys_period IS 'Текущий период';
COMMENT ON COLUMN ttb.v_tt_out_oper_unplanned.parent_tt_out_id IS 'ссылка на родительский выход';
COMMENT ON COLUMN ttb.v_tt_out_oper_unplanned.tr_id IS 'ТС поставленное на выход';