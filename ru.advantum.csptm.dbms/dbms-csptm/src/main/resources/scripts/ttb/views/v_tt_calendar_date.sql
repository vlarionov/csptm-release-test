--drop  view  ttb.v_tt_calendar_date;

create or replace view  ttb.v_tt_calendar_date
as
select tt_variant_id, dt
from ttb.tt_calendar
join ttb.calendar_tag on tt_calendar.calendar_tag_id = calendar_tag.calendar_tag_id
join ttb.calendar_item on calendar_tag.calendar_tag_id = calendar_item.calendar_tag_id
join ttb.calendar on calendar_item.calendar_id = calendar.calendar_id;


COMMENT ON VIEW ttb.v_tt_calendar_date IS 'Даты действия для варианта расписания';
COMMENT ON COLUMN ttb.v_tt_calendar_date.tt_variant_id IS 'ID варианта расписания';
COMMENT ON COLUMN ttb.v_tt_calendar_date.dt IS 'Дата';