--drop view ttb.v_tt_variant_current_info;

CREATE OR REPLACE VIEW ttb.v_tt_variant_current_info AS
  WITH a AS (
      SELECT tt_out.tr_id
        ,tt_action_item.driver_id
        ,tt_variant.tt_variant_id
        ,route.route_id
        ,tt_action_item.dr_shift_id
        ,tt_out.tt_out_num
      FROM rts.route
        JOIN rts.route_variant ON route.route_id = route_variant.route_id
        JOIN ttb.tt_variant ON tt_variant.route_variant_id = route_variant.route_variant_id
        JOIN ttb.tt_out ON tt_variant.tt_variant_id = tt_out.tt_variant_id
        JOIN ttb.tt_action ON tt_out.tt_out_id = tt_action.tt_out_id
        JOIN ttb.tt_action_item ON tt_action.tt_action_id = tt_action_item.tt_action_id
      WHERE ((tt_action.action_range @> (now())::timestamp without time zone)
             AND (tt_variant.parent_tt_variant_id IS NOT NULL)
             AND (NOT tt_variant.sign_deleted)
             AND (NOT tt_out.sign_deleted)
             AND (NOT tt_action.sign_deleted)
             AND (NOT tt_action_item.sign_deleted))
      GROUP BY tt_out.tr_id
        , tt_action_item.driver_id
        , tt_variant.tt_variant_id
        , route.route_id
        , tt_action_item.dr_shift_id
        , tt_out.tt_out_num
  )
  SELECT a.tr_id
    ,a.driver_id
    ,a.tt_variant_id
    ,ttb."jf_tt_variant_pkg$get_text"(a.tt_variant_id) AS tt_variant_text
    ,core."jf_driver_pkg$get_text"(a.driver_id) AS driver_text
    ,a.route_id
    ,dr_shift.dr_shift_num::text as dr_shift_num
    ,a.tt_out_num::text as tt_out_num
  FROM a
    JOIN core.driver ON ((a.driver_id = driver.driver_id))
    join ttb.dr_shift on dr_shift.dr_shift_id = a.dr_shift_id;