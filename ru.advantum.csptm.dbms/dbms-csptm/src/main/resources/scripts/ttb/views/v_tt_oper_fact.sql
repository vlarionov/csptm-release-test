--drop view ttb.v_tt_oper_fact;

create or replace view ttb.v_tt_oper_fact as
  select
    tt_out.tt_out_id,
    tt_out.tt_variant_id,
    tr_capacity_id,
    mode_id,
    tt_out_num,
    tt_out.sign_deleted,
    tt_out.sys_period,
    parent_tt_out_id,
    tr_id,
    order_date,
    norm_id,
    route_variant_id,
    order_num,
    time_begin as time_plan_begin,
    time_end as time_plan_end,
    time_fact_begin,
    time_fact_end,
    parent_tt_variant_id,
    tt_action.action_type_id,
    stop_item2round.stop_item_id,
    action_type.parent_type_id as parent_action_type_id
  from ttb.tt_out
    join ttb.v_tt_variant_oper tva on tt_out.tt_variant_id = tva.tt_variant_id
    join ttb.tt_action on tt_out.tt_out_id = tt_action.tt_out_id
    join ttb.tt_action_item on tt_action.tt_action_id = tt_action_item.tt_action_id
    join rts.stop_item2round on stop_item2round.stop_item2round_id = tt_action_item.stop_item2round_id
    join ttb.tt_action_fact on tt_action.tt_action_id = tt_action_fact.tt_action_id
                               and stop_item2round.stop_item2round_id = tt_action_fact.stop_item2round_id
    join ttb.action_type on action_type.action_type_id = tt_action.action_type_id
  where not tt_out.sign_deleted
    and not tt_action.sign_deleted
    and not tt_action_item.sign_deleted
    and not stop_item2round.sign_deleted;

COMMENT ON VIEW ttb.v_tt_out_oper IS 'Факты расписания оперативного';

COMMENT ON COLUMN ttb.v_tt_out_oper.tt_out_id IS 'Выход';
COMMENT ON COLUMN ttb.v_tt_out_oper.tt_variant_id IS 'Вариант расписания';
COMMENT ON COLUMN ttb.v_tt_out_oper.tr_capacity_id IS 'Вместимость ТС';
COMMENT ON COLUMN ttb.v_tt_out_oper.mode_id IS 'Режим труда и отдыха';
COMMENT ON COLUMN ttb.v_tt_out_oper.tt_out_num IS 'Номер выхода';
COMMENT ON COLUMN ttb.v_tt_out_oper.sign_deleted IS 'Удален';
COMMENT ON COLUMN ttb.v_tt_out_oper.sys_period IS 'Текущий период';
COMMENT ON COLUMN ttb.v_tt_out_oper.parent_tt_out_id IS 'ссылка на родительский выход';
COMMENT ON COLUMN ttb.v_tt_out_oper.tr_id IS 'ТС поставленное на выход';
