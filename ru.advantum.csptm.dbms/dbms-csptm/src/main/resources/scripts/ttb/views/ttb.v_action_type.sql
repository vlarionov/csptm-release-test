﻿--drop  view  ttb.v_action_type;

create or replace view  ttb.v_action_type as
with recursive root_type (action_type_id, parent_type_id, action_type_name, action_type_desc, action_type_code, level, path) as (
  select action_type_id, parent_type_id, action_type_name, action_type_desc,action_type_code, 1, array[action_type_name]
  from  ttb.action_type
  where parent_type_id is   null
  union
  select atp.action_type_id, atp.parent_type_id, atp.action_type_name, atp.action_type_desc, atp.action_type_code, rt.level+1, atp.action_type_name||rt.path
  from  ttb.action_type atp
    join root_type rt on  rt.action_type_id = atp.parent_type_id
)
select root_type.action_type_id,
  root_type.parent_type_id,
  root_type.action_type_name,
  root_type.action_type_desc,
  root_type.action_type_code,
  root_type.level,
  array_to_string(root_type.path,' - ') as path,
  (select action_type_name from ttb.action_type atp_parent where  atp_parent.action_type_id = root_type.parent_type_id ) as parent_name
from root_type;


COMMENT ON VIEW ttb.v_action_type IS 'Типы действий (объединенная структура типов рейсов и типов стоянок)';
COMMENT ON COLUMN ttb.v_action_type.action_type_id IS 'ID';
COMMENT ON COLUMN ttb.v_action_type.action_type_name IS 'Наименование';
COMMENT ON COLUMN ttb.v_action_type.action_type_code IS 'код';
COMMENT ON COLUMN ttb.v_action_type.parent_type_id IS 'Тип-родитель';
COMMENT ON COLUMN ttb.v_action_type.action_type_desc IS 'Описание';
COMMENT ON COLUMN ttb.v_action_type.level IS 'Уровень вложенности';
COMMENT ON COLUMN ttb.v_action_type.path IS 'Полная строка вложенности';
COMMENT ON COLUMN ttb.v_action_type.parent_name IS 'Наменование родительского типа';

