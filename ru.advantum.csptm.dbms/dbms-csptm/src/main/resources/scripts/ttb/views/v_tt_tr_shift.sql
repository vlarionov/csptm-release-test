--drop view ttb.v_tt_tr_shift;

create or replace view ttb.v_tt_tr_shift as
  with
      sch_time as
    (
        select min(case atg.fix_arrival_type_id when ttb.fix_arrival_type_pkg$start_time() then atg.start_time end) as  time_begin
          , max(case atg.fix_arrival_type_id when ttb.fix_arrival_type_pkg$finish_time() then atg.start_time end) as time_end
          , schi.tt_schedule_id
        from
          ttb.tt_schedule_item schi
          join ttb.tt_action_group agr on agr.tt_action_group_id = schi.tt_action_group_id
          join ttb.tt_action_type2group act2gr on act2gr.tt_action_group_id = agr.tt_action_group_id
          join ttb.tt_action_type tact on  tact.tt_action_type_id = act2gr.tt_action_type_id
          join ttb.tt_at_round ar on ar.tt_action_type_id = tact.tt_action_type_id
          join ttb.tt_at_round_stop ars on  ars.tt_at_round_id = ar.tt_at_round_id
          join ttb.atgrs_time atg on  atg.tt_at_round_stop_id= ars.tt_at_round_stop_id
        where atg.fix_arrival_type_id in( ttb.fix_arrival_type_pkg$start_time(),  ttb.fix_arrival_type_pkg$finish_time())
        group by schi.tt_schedule_id

    )
    ,adata as (
      select sw.switch_time
             - make_interval( secs:= ttb.norm_pkg$get_schedule_tm ( p_tt_sсhedule_id=> sch_it.tt_schedule_id
      , p_ref_group_kind_id => ttb.ref_group_kind_pkg$switch_action()
      , p_tm_b =>  (date_trunc ('hour', sw.switch_time) - make_interval(hours:=1))::time
      , p_tm_e => date_trunc ('hour', sw.switch_time)::time
      , p_tr_capacity_id=>  ttr.tr_capacity_id ) ) as switch_time


        , sw.tt_schedule_item_id_from, tp.tt_tr_order_num num_fr, tp_to.tt_tr_order_num num_to, tp.tt_tr_id
        , agr.ref_group_kind_id  = ttb.ref_group_kind_pkg$switch_from_park_action() as is_switch_from_park
        ,sw.tt_schedule_id_to
        ,sch.tt_variant_id as tt_variant_id_to
        ,sch_fr.tt_schedule_id as tt_schedule_id_fr
        ,sch_fr.tt_variant_id as tt_variant_id_fr
        ,sw.tt_tr_period_id_from
        ,sw.tt_tr_period_id_to
      from ttb.tt_switch sw
        join ttb.tt_schedule_item sch_it on sch_it.tt_schedule_item_id = sw.tt_schedule_item_id_from
        join ttb.tt_schedule sch_fr on sch_fr.tt_schedule_id = sch_it.tt_schedule_id
        join ttb.tt_action_group agr on agr.tt_action_group_id = sch_it.tt_action_group_id
        join ttb.tt_tr_period tp on tp.tt_tr_period_id = sw.tt_tr_period_id_from
        join ttb.tt_tr_period tp_to on tp_to.tt_tr_period_id = sw.tt_tr_period_id_to
        join ttb.tt_schedule sch on sch.tt_schedule_id = sw.tt_schedule_id_to
        join ttb.tt_tr ttr on ttr.tt_tr_id = tp.tt_tr_id

  )
    , d as (
    select
        lag(switch_time) over (partition by adata.tt_tr_id order by switch_time) as lag_swtm
      ,adata.switch_time
      ,adata.tt_schedule_id_to as tt_schedule_id_fr
      ,lag(adata.tt_variant_id_fr ) over (partition by adata.tt_tr_id order by switch_time) as tt_variant_id_fr
      , adata.tt_tr_id
      , adata.is_switch_from_park
      , lag(adata.tt_schedule_id_fr ) over (partition by adata.tt_tr_id order by switch_time)   as tt_schedule_id_to
      ,adata.tt_tr_period_id_from tt_tr_period_id_to
      ,lag(adata.tt_schedule_item_id_from ) over (partition by adata.tt_tr_id order by switch_time) as tt_schedule_item_id_fr
      ,adata.tt_schedule_item_id_from as  tt_schedule_item_id_to
    from adata
    union
    select
      adata.switch_time
      ,lead(switch_time) over (partition by adata.tt_tr_id order by switch_time) as lag_swtm
      ,adata.tt_schedule_id_fr
      ,adata.tt_variant_id_fr  ,     adata.tt_tr_id     , false is_switch_from_park  ,   adata.tt_schedule_id_to
      ,adata.tt_tr_period_id_to
      ,adata.tt_schedule_item_id_from as tt_schedule_item_id_fr
      ,lead(adata.tt_schedule_item_id_from ) over (partition by adata.tt_tr_id order by switch_time) as tt_schedule_item_id_to
    from adata
  )
    ,
      sw as (
        select distinct
           lag_swtm   as tm_b
          ,switch_time as tm_e
          ,tt_schedule_id_fr
          ,tt_variant_id_fr
          ,tt_tr_id
          ,case  when lag_swtm is null then is_switch_from_park  else false end as is_switch_from_park
          ,tt_schedule_id_to
          ,tt_tr_period_id_to
          ,tt_schedule_item_id_fr
          ,tt_schedule_item_id_to
        from d
        order by is_switch_from_park ,tm_b
    )
  --select *  from sw

  select
    sch.tt_schedule_id
    ,sch.tt_variant_id
    , case when sw.tm_b is not null then sw.tm_b else coalesce(greatest(sch_time.time_begin, p.time_begin)) end as time_begin
    , case when sw.tm_e is not null then sw.tm_e else least( sch_time.time_end, (select max(pp.time_end)  from ttb.tt_period pp where  pp.tt_schedule_id = sch.tt_schedule_id)) end as time_end
    , row_number() over(partition by sch.tt_schedule_id,  p.tt_period_id order by ttp.tt_tr_order_num) -1 as rn
    , p.tt_period_id
    , p.movement_type_id
    , p.interval_time
    , tt_tr.tt_tr_id
    , tt_tr.tr_capacity_id
    , tt_tr.depo2territory_id
    , ttp.tt_tr_order_num as order_num
    , ttp.is_own
    , m.mode_id
    , m.mode_name
    , m.count_shift
    , m.wrk_min
    , m.wrk_max
    , sw.tt_variant_id_fr
    , sw.tm_b as switch_time_to
    , sw.tm_e as switch_time_fr
    , sw.tt_schedule_id_fr
    , sw.is_switch_from_park  as is_switch_from_park_fr
    ,sw.tt_schedule_item_id_fr
    ,sw.tt_schedule_item_id_to
    , m.is_to
  from
    sch_time
    join ttb.tt_schedule sch on sch_time.tt_schedule_id = sch.tt_schedule_id
    join ttb.tt_period p on p.tt_schedule_id = sch.tt_schedule_id
    left join ttb.tt_tr_period ttp on ttp.tt_period_id = p.tt_period_id
    left join ttb.tt_tr tt_tr on tt_tr.tt_tr_id = ttp.tt_tr_id
    left join (select
                 m.mode_id
                 , m.mode_name
                 , count(drs.dr_shift_id) as count_shift
                 , sum(drs.work_duration_min) as wrk_min
                 , sum(drs.work_duration_max) as wrk_max
                 , tm.break_count= 1 as is_to
               from ttb.mode m
                 join ttb.dr_shift drs on drs.mode_id = m.mode_id
                 join ttb.tr_mode tm on tm.tr_mode_id = m.mode_id
               group by  m.mode_id , m.mode_name, tm.break_count
              ) m on   m.mode_id = tt_tr.tr_mode_id

    left join sw on sw.tt_tr_id=tt_tr.tt_tr_id and ttp.tt_tr_period_id = sw.tt_tr_period_id_to

  group by
    sch.tt_schedule_id
    ,sch.tt_variant_id
    , sch_time.time_begin
    , sch_time.time_end
    , p.tt_period_id
    , p.movement_type_id
    , p.interval_time
    , tt_tr.tt_tr_id
    , tt_tr.tr_capacity_id
    , tt_tr.depo2territory_id
    , ttp.tt_tr_order_num
    , ttp.is_own
    , m.mode_id
    , m.mode_name
    , m.count_shift
    , m.wrk_min
    , m.wrk_max

    , sw.tt_variant_id_fr
    , sw.tm_b
    , sw.tm_e
    , sw. tt_schedule_id_fr
    , sw.is_switch_from_park
    , sw.tt_schedule_item_id_fr
    ,sw.tt_schedule_item_id_to
    , m.is_to

;