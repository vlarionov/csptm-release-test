
select *
from ttb.tt_variant
--where tt_variant_id = 2683233;
where order_date = now()::date;

--oper
select time_begin
from ttb.tt_out
  join ttb.tt_action on tt_out.tt_out_id = tt_action.tt_out_id
  join ttb.tt_action_item on tt_action.tt_action_id = tt_action_item.tt_action_id
where tt_variant_id = 2683233 and order_date = '2018-04-27' :: date
order by time_begin;

--plan
select time_begin, ttb.get_local_timestamp('2018-04-27'::date  + (time_begin - ttb.jf_incident_pkg$cn_plan_tt_date()))
from ttb.tt_out
  join ttb.tt_action on tt_out.tt_out_id = tt_action.tt_out_id
  join ttb.tt_action_item on tt_action.tt_action_id = tt_action_item.tt_action_id
where tt_variant_id = 2682772
order by time_begin;

select *
from ttb.tt_variant
  join ttb.tt_out on tt_variant.tt_variant_id = tt_out.tt_variant_id
  join ttb.tt_action on tt_out.tt_out_id = tt_action.tt_out_id
  join ttb.tt_action_item on tt_action.tt_action_id = tt_action_item.tt_action_id
where tt_variant.tt_variant_id = 2683244;


select *
from ttb.tt_variant
where order_date = '2018-04-27'::date;

update ttb.order_list
set tt_out_created = false
where order_date = '2018-04-27'::date;


delete from ttb.tt_out
where tt_variant_id in (
  2683359,
  2683360,
  2683361,
  2683362,
  2683363,
  2683364,
  2683365,
  2683366,
  2683367

);

delete from ttb.tt_variant
where tt_variant_id in (
  2683359,
  2683360,
  2683361,
  2683362,
  2683363,
  2683364,
  2683365,
  2683366,
  2683367
);


select order_list_id
from ttb.order_list
where not tt_out_created
      and order_date = now()::date;

select ttb.tt_oper_pkg$create_tt_oper('2018-04-27' :: date);

update ttb.order_list
set tt_out_created = false
where order_date = '2018-04-27'::date;


select *
from ttb.order_list
where order_date = '2018-04-27'::date;

delete from ttb.tt_variant
where tt_variant_id = 2683232;

SELECT tc.table_schema, tc.constraint_name, tc.table_name, kcu.column_name, ccu.table_schema AS foreign_table_schema,
                                                                        ccu.table_name AS foreign_table_name, ccu.column_name AS foreign_column_name FROM information_schema.table_constraints AS tc JOIN information_schema.key_column_usage AS kcu ON tc.constraint_name = kcu.constraint_name JOIN information_schema.constraint_column_usage AS ccu ON ccu.constraint_name = tc.constraint_name WHERE constraint_type = 'FOREIGN KEY' AND ccu.table_name='tt_action';


SELECT ccu.table_schema AS foreign_table_schema FROM information_schema.table_constraints AS tc JOIN information_schema.key_column_usage AS kcu ON tc.constraint_name = kcu.constraint_name JOIN information_schema.constraint_column_usage AS ccu ON ccu.constraint_name = tc.constraint_name WHERE constraint_type = 'FOREIGN KEY' AND ccu.table_name='tt_action';

