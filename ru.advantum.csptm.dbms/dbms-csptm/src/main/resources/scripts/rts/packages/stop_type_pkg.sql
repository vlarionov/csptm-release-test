﻿-- Пункт посадки высадки
create or replace function rts.stop_type_pkg$stop()
    returns smallint as 'select 1 :: smallint' language sql immutable;
-- Парк
create or replace function rts.stop_type_pkg$park()
    returns smallint as 'select 2 :: smallint' language sql immutable;
-- Пункт ремонта
create or replace function rts.stop_type_pkg$repair()
    returns smallint as 'select 3 :: smallint' language sql immutable;
-- АЗС
create or replace function rts.stop_type_pkg$gs()
    returns smallint as 'select 4 :: smallint' language sql immutable;
-- Отстойная площадка
create or replace function rts.stop_type_pkg$parking()
    returns smallint as 'select 5 :: smallint' language sql immutable;
-- Пункта обеда
create or replace function rts.stop_type_pkg$lunch()
    returns smallint as 'select 6 :: smallint' language sql immutable;
-- Конечный пункт
create or replace function rts.stop_type_pkg$finish()
    returns smallint as 'select 7 :: smallint' language sql immutable;
-- Контрольный пункт ОУД
create or replace function rts.stop_type_pkg$checkpoint()
    returns smallint as 'select 8 :: smallint' language sql immutable;
-- Техническая
create or replace function rts.stop_type_pkg$technical()
    returns smallint as 'select 9 :: smallint' language sql immutable;
-- Конечный пункт А
create or replace function rts.stop_type_pkg$finish_a()
    returns smallint as 'select 10 :: smallint' language sql immutable;
-- Конечный пункт Б
create or replace function rts.stop_type_pkg$finish_b()
    returns smallint as 'select 11 :: smallint' language sql immutable;
