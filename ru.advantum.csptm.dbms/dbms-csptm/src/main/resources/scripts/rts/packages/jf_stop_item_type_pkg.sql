/*
create function rts.jf_stop_item_type_pkg$attr_to_rowtype(p_attr text) returns rts.stop_item_type
LANGUAGE plpgsql
AS $$
declare 
   l_r rts.stop_item_type%rowtype; 
begin 
   l_r.stop_item_type_id := jofl.jofl_pkg$extract_number(p_attr, 'stop_item_type_id', true); 
   l_r.stop_item_id := jofl.jofl_pkg$extract_number(p_attr, 'stop_item_id', true); 
   l_r.stop_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'stop_type_id', true); 
   l_r.sys_period := jofl.jofl_pkg$extract_varchar(p_attr, 'sys_period', true); 
   l_r.sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'sign_deleted', true); 

   return l_r;
end;
$$;
*/

create or replace function rts.jf_stop_item_type_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare 
 l_signdel_default       boolean := false;
 l_sign_deleted          boolean;
 l_stop_item_id rts.stop_item.stop_item_id%type;
begin 
 /* считываем значения фильтров */
 l_sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'f_sign_deleted', true);
 l_stop_item_id  := jofl.jofl_pkg$extract_number(p_attr, 'stop_item_id', true);
 
 open p_rows for 
  select sit.stop_item_type_id,
         st.stop_type_name,
         sit.stop_item_id,
         sit.stop_type_id,
         sit.sign_deleted
  from rts.stop_item_type sit
       join rts.stop_type st on st.stop_type_id = sit.stop_type_id
  where (not sit.sign_deleted OR sit.sign_deleted = coalesce(l_sign_deleted, l_signdel_default)) and
	      (l_stop_item_id is null or sit.stop_item_id = l_stop_item_id);
end;
$$;

/*
create function rts.jf_stop_item_type_pkg$of_insert(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r rts.stop_item_type%rowtype;
begin 
   l_r := rts.jf_stop_item_type_pkg$attr_to_rowtype(p_attr);

   insert into rts.stop_item_type select l_r.*;

   return null;
end;
$$;

create function rts.jf_stop_item_type_pkg$of_update(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r rts.stop_item_type%rowtype;
begin 
   l_r := rts.jf_stop_item_type_pkg$attr_to_rowtype(p_attr);

   update rts.stop_item_type set 
          stop_item_id = l_r.stop_item_id, 
          stop_type_id = l_r.stop_type_id, 
          sys_period = l_r.sys_period, 
          sign_deleted = l_r.sign_deleted
   where 
          stop_item_type_id = l_r.stop_item_type_id;

   return null;
end;
$$;

create function rts.jf_stop_item_type_pkg$of_delete(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r rts.stop_item_type%rowtype;
begin 
   l_r := rts.jf_stop_item_type_pkg$attr_to_rowtype(p_attr);
   update rts.stop_item_type set sign_deleted = true where stop_item_type_id = l_r.stop_item_type_id;
   return null;
end;
$$;
*/