create or replace function rts."jf_stop_item_pkg$attr_to_rowtype"(p_attr text) returns rts.stop_item
LANGUAGE plpgsql
AS $$
declare 
   l_r rts.stop_item%rowtype; 
begin 
   l_r.stop_item_id := jofl.jofl_pkg$extract_number(p_attr, 'stop_item_id', true); 
   l_r.tr_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'tr_type_id', true);
   l_r.sys_period := jofl.jofl_pkg$extract_varchar(p_attr, 'sys_period', true); 
   l_r.sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'sign_deleted', true); 
   l_r.stop_location_id := jofl.jofl_pkg$extract_number(p_attr, 'stop_location_id', true); 

   return l_r;
end;
$$;

create or replace function rts."jf_stop_item_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
    lf_sign_deleted         boolean;
    lf_arr_tr_type_id       smallint [];
    lf_arr_stop_location_id bigint [];
    p_attr_stop_id          rts.stop.stop_id%type;
    lf_tr_type_id_str       text;
  -- lf_arr_stop_id bigint[];
  -- lf_stop_id_str text;
    lf_stop_location_id_str text;
  /* по-умолчанию - показываем только действующие записи */
    l_signdel_default       boolean := false;
begin
 /* считываем значения фильтров */
 lf_sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'f_sign_deleted', true);

 lf_arr_tr_type_id := jofl.jofl_pkg$extract_narray(p_attr, 'f_tr_type_id', true);
 lf_tr_type_id_str := null;
 IF array_length(lf_arr_tr_type_id, 1) is not null then
  lf_tr_type_id_str := jofl.jofl_pkg$extract_varchar(p_attr, 'f_tr_type_id', true);
 end if;

 /*
 -- код для фильтра по списку stop_id - пока не используется
 lf_arr_stop_id := jofl.jofl_pkg$extract_narray(p_attr, 'f_stop_id', true);
 lf_stop_id_str := null;
 IF array_length(lf_arr_stop_id, 1) is not null then
  lf_stop_id_str := jofl.jofl_pkg$extract_varchar(p_attr, 'f_stop_id', true);
 end if;
 */

 p_attr_stop_id := jofl.jofl_pkg$extract_number(p_attr, 'stop_id', true);

 lf_arr_stop_location_id := jofl.jofl_pkg$extract_narray(p_attr, 'f_stop_location_id', true);
 lf_stop_location_id_str := null;
 IF array_length(lf_arr_stop_location_id, 1) is not null then
  lf_stop_location_id_str := jofl.jofl_pkg$extract_varchar(p_attr, 'f_stop_location_id', true);
 end if;

 /* показываем данные в соотвтствии с фильтром;
  "подтягиваем" данные из справочников для отображения */
 open p_rows for
 with stop_type as (
     select
         si.stop_item_id,
         st.stop_type_id
     from rts.stop_type st
         join rts.stop_item_type sit2 on st.stop_type_id = sit2.stop_type_id
         join rts.stop_item si on si.stop_item_id = sit2.stop_item_id
     where st.stop_type_id=rts.stop_type_pkg$technical()
 )
 select
     sit.stop_item_id as stop_item_id,
         s.stop_id as stop_id,
         s.name as stop_name,
         sit.tr_type_id as tr_type_id,
         tt.name as tr_type_name,
         sit.stop_location_id as stop_location_id,
         sl.building_address as stop_location_build_address,
         sl.building_distance as stop_location_build_distance,
         lower(sit.sys_period) as lower_sysperiod,
         upper(sit.sys_period) as upper_sysperiod,
         sit.sign_deleted,
    '[' || json_build_object('CAPTION', 'На карте',
                             'LINK', json_build_object('URL', '/app.html?m=modules/StopPlaceOnMapCtrl&attr={"wkt":"' || ST_AsText(sl.geom) ||
                                                              '","sp_muid":"' || g.stop_location_muid || '"}'
                             ))::text || ']' AS "JUMP_TO_SP",
         /* возвращаем значения фильтров, которые использовались */
         lf_sign_deleted::text as f_sign_deleted,
 --         lf_stop_id_str as f_stop_id,
         lf_tr_type_id_str as f_tr_type_id,
         lf_stop_location_id_str as f_stop_location_id,
         case when t.stop_type_id isnull then false else true end as is_technical
  from rts.stop_item sit
       join core.tr_type tt on tt.tr_type_id = sit.tr_type_id
       join rts.stop_location sl on sl.stop_location_id = sit.stop_location_id
       join rts.stop s on s.stop_id = sl.stop_id
       join rts.stop_location2gis g on sl.stop_location_id = g.stop_location_id
       left join stop_type t on t.stop_item_id = sit.stop_item_id
  where /* выбираем записи для stop_id из мастер-окна */
        s.stop_id = p_attr_stop_id
        /* фильтр по видимости удаленных записей */
        and (not sit.sign_deleted OR sit.sign_deleted = coalesce(lf_sign_deleted, l_signdel_default))
        /* фильтр по виду ТС */
        and (array_length(lf_arr_tr_type_id, 1) is null OR sit.tr_type_id = any (lf_arr_tr_type_id))
        /* фильтр по stop_location - ближайшее строение */
        and (array_length(lf_arr_stop_location_id, 1) is null OR
             sl.stop_location_id = any (lf_arr_stop_location_id));
end;
$$;

create or replace function rts."jf_stop_item_pkg$of_insert"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r rts.stop_item%rowtype;
begin 
  l_r := rts.jf_stop_item_pkg$attr_to_rowtype(p_attr);

  /* подставляем значение по-умолчанию для sign_deleted */
  l_r.sign_deleted := false;
   insert into rts.stop_item (tr_type_id, sign_deleted, stop_location_id)
     select l_r.tr_type_id, l_r.sign_deleted, l_r.stop_location_id;

  return null;
end;
$$;

create or replace function rts."jf_stop_item_pkg$of_update"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r rts.stop_item%rowtype;
begin 
   l_r := rts.jf_stop_item_pkg$attr_to_rowtype(p_attr);
  /* изменяем только "содержательные" поля */
   update rts.stop_item
    set tr_type_id = l_r.tr_type_id,
        stop_location_id = l_r.stop_location_id
    where stop_item_id = l_r.stop_item_id;

   return null;
end;
$$;

create or replace function rts."jf_stop_item_pkg$of_delete"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r rts.stop_item%rowtype;
begin 
   l_r := rts.jf_stop_item_pkg$attr_to_rowtype(p_attr);
  /* выполняем "логическое" удаление данных */
   update  rts.stop_item set sign_deleted = true where stop_item_id = l_r.stop_item_id;
   return null;
end;
$$;

-- ----------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION rts.jf_stop_item_pkg$of_default_stop (p_id_account numeric, out p_rows refcursor, p_attr text) RETURNS refcursor
LANGUAGE 'plpgsql'
 AS $$
declare
 lf_stop_id rts.stop.stop_id%type := jofl.jofl_pkg$extract_number(p_attr, 'stop_id', true);
begin
 open p_rows for
  select s.stop_id,
         s.name as stop_name
  from rts.stop s
  where s.stop_id = lf_stop_id;
end;
$$;


CREATE OR REPLACE FUNCTION rts.jf_stop_item_pkg$get_text(p_stop_item_id int) RETURNS text
	LANGUAGE sql
AS $$
    select stop.name
    from rts.stop_item
        join rts.stop_location on stop_item.stop_location_id = stop_location.stop_location_id
        join rts.stop on stop_location.stop_id = stop.stop_id
    where stop_item.stop_item_id = p_stop_item_id;
$$