﻿create or replace function rts."jf_route_rounds_pkg$of_rows"(
  in  p_id_account numeric,
  out p_rows       refcursor,
  in  p_attr       text)
  returns refcursor as
$$
declare
  l_route_variant_id rts.round.route_variant_id%type;
  l_route_id         rts.route.route_id%type;
  l_tr_type_id       smallint [];
begin
  l_route_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'route_variant_id', true);
  l_route_id := jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', true);
  l_tr_type_id := jofl.jofl_pkg$extract_narray(p_attr, 'f_tr_type_id', true);
  select rt.p_rows
  into p_rows
  from rts.jf_route_rounds_pkg$rounds_type(l_route_variant_id,
                                           l_route_id,
                                           l_tr_type_id,
                                           ttb.action_type_pkg$production_round(),
                                           p_id_account
       ) rt;
end;
$$
language plpgsql;

create or replace function rts.jf_route_rounds_pkg$rounds_type(
      p_route_variant_id rts.round.route_variant_id%type,
      p_route_id         rts.route.route_id%type,
      p_tr_type_id       smallint [],
      p_round_type       smallint,
      p_account_id       numeric,
  out p_rows             refcursor
)
  returns refcursor as
$$
declare
begin
  open p_rows for
  with
    -- получает id всех подтипов рейсов
      action_type_ids as (
        select
          act.action_type_id,
          act.action_type_name
        from ttb.action_type act
        where act.parent_type_id = any (ttb.action_type_pkg$get_action_type_list(p_round_type, false))
    ),
    --     содержит id первой и последней остановки, а так же их порядковые номер
      a as (
        select
          rnd.round_id,
          action_type_ids.action_type_name,
          min(si2r.order_num) start_point_round,
          max(si2r.order_num) end_point_round
        from rts.round rnd
          join action_type_ids on rnd.action_type_id = action_type_ids.action_type_id
          join rts.stop_item2round si2r on rnd.round_id = si2r.round_id
          join rts.route_variant rrv on rrv.route_variant_id = rnd.route_variant_id
          join rts.route r on r.route_id = rrv.route_id
        where 1 = 1
              and (rnd.route_variant_id = p_route_variant_id or p_route_variant_id is null)
              and not rnd.sign_deleted
              and not si2r.sign_deleted
              and not rrv.sign_deleted
              and not r.sign_deleted
              and r.route_id in (select adm.account_data_realm_pkg$get_routes(p_account_id))
        group by rnd.round_id, action_type_ids.action_type_name
    ),
    --     содержит round_id и название     первой    остановки
      a_start as (
        select
          a.round_id,
          s_start.name as start_point_round
        from a
          join rts.stop_item2round si2r on a.round_id = si2r.round_id
          join rts.stop_item si_start on si2r.stop_item_id = si_start.stop_item_id
                                         and si2r.order_num = a.start_point_round
          join rts.stop_location sl_start on sl_start.stop_location_id = si_start.stop_location_id
          join rts.stop s_start on sl_start.stop_id = s_start.stop_id
        where not si2r.sign_deleted
          and not si_start.sign_deleted
          and not sl_start.sign_deleted
          and not s_start.sign_deleted
    ),
    --     содержит round_id и название     последней     остановки
      a_finish as (
        select
          a.round_id,
          s_finish.name as end_point_round
        from a
          join rts.stop_item2round si2r on a.round_id = si2r.round_id
          join rts.stop_item si_finish on si2r.stop_item_id = si_finish.stop_item_id
                                          and si2r.order_num = a.end_point_round
          join rts.stop_location sl_finish on sl_finish.stop_location_id = si_finish.stop_location_id
          join rts.stop s_finish on sl_finish.stop_id = s_finish.stop_id
        where not si2r.sign_deleted
          and not si_finish.sign_deleted
          and not sl_finish.sign_deleted
          and not s_finish.sign_deleted    
    )
  select
    r.route_id,
    rr.round_id,
    rr.code                                    as round_code,
    a.action_type_name                         as round_type,
    r.route_num,
    trt.name                                   as tr_type,
    a_start.start_point_round,
    a_finish.end_point_round,
    rrv.route_variant_id,
    '[' || json_build_object('CAPTION', 'На карте',
                             'LINK', json_build_object('URL',
                                                       '/app.html?m=modules/RouteOnMapCtrl&attr={"tr_type":"'
                                                       || trt.short_name ||
                                                       '","route_number":"' || r.route_num ||
                                                       '","' || ttb.action_type_pkg$get_url_round_type(p_round_type) ||
                                                       '":"' || rr.code ||
                                                       '","route_direction":"' ||
                                                       rr.move_direction_id ||
                                                       '","rv_muid":"'
                                                       || rrv.route_variant_id || '"}'
                             )) :: text || ']' as "JUMP_TO_MAP"
  from a
    join a_start on a.round_id = a_start.round_id
    join a_finish on a.round_id = a_finish.round_id
    join rts.round rr on rr.round_id in (a_start.round_id, a_finish.round_id)
    join rts.route_variant rrv on rrv.route_variant_id = rr.route_variant_id
    join rts.route r on r.route_id = rrv.route_id
    join core.tr_type trt on trt.tr_type_id = r.tr_type_id
  where r.carrier_id = core.carrier_pkg$mgt()
    and (p_route_id is null or p_route_id = r.route_id)
    and (array_length(p_tr_type_id, 1) is null or r.tr_type_id = any (p_tr_type_id))
    and not rr.sign_deleted
    and not rrv.sign_deleted
    and not r.sign_deleted;
end;
$$
language plpgsql;