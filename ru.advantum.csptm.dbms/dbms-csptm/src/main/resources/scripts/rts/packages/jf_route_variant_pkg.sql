CREATE OR REPLACE FUNCTION rts."jf_route_variant_pkg$of_rows"(
    IN  p_id_account NUMERIC,
    OUT p_rows       REFCURSOR,
    IN  p_attr       TEXT)
    RETURNS REFCURSOR AS
$$
DECLARE
    l_route_id   rts.route_variant.route_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'route_id', TRUE);
    l_tr_type_id rts.route_variant.route_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'tr_type_id', TRUE);
BEGIN
    OPEN p_rows FOR
    SELECT
        rv.route_variant_id,
        r.current_route_variant_id,
        rv.route_id,
        rv.rv_status_id,
        tt.name                         AS tr_type_name,
        tt.tr_type_id,
        rv.rv_comment,
        lower(rv.action_period) :: TEXT AS dt_begin,
        upper(rv.action_period) :: TEXT AS dt_end,
        rvs.rv_status_name,
        CASE rv.route_variant_id
        WHEN r.current_route_variant_id
            THEN TRUE
        ELSE FALSE END                  AS "IS_CURRENT",
        '[' || json_build_object('CAPTION', 'Производственные рейсы',
                                 'JUMP', json_build_object('METHOD', 'rts.route_rounds', 'ATTR', '{}'))
        || ']'                             "JUMP_TO_RR",
        '[' || json_build_object('CAPTION', 'Технологические рейсы',
                                 'JUMP',
                                 json_build_object('METHOD', 'rts.route_null_rounds', 'ATTR', '{}'))
        || ']'                             "JUMP_TO_RNR"
    FROM rts.route_variant rv
        JOIN rts.route r ON r.route_id = l_route_id
        JOIN rts.rv_status rvs ON rvs.rv_status_id = rv.rv_status_id
        JOIN core.tr_type tt ON tt.tr_type_id = l_tr_type_id
    WHERE rv.route_id = l_route_id;
END;
$$ LANGUAGE plpgsql;
--------------------------------
