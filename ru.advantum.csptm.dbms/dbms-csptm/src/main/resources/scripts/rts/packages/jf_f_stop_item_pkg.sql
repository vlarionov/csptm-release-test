create or replace function rts.jf_f_stop_item_pkg$of_rows(p_id_account numeric, out p_rows refcursor, p_attr text) returns refcursor
language plpgsql
as $$
declare
  l_f_db_method  text := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true);
  l_f_route_id   rts.route.route_id%type;
  l_f_tt_out_id  ttb.tt_out.tt_out_id%type;
  l_f_round_code rts.round.code%type;
  l_f_tr_type_id core.tr_type.tr_type_id%type;
  l_f_stop_type_id rts.stop_item_type.stop_type_id%type;
  l_f_route_variant_id rts.route_variant.route_variant_id%type;
begin
  if l_f_db_method = 'askp.visit_tr_stop'
  then
    l_f_route_id := jofl.jofl_pkg$extract_number(p_attr, 'route_id', true);
  elseif l_f_db_method = 'oud.round_interval'
    then
      l_f_route_id := jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', true);
      l_f_round_code := '00';
  elseif l_f_db_method = 'oud.tr_switch'
    then
      l_f_route_id := jofl.jofl_pkg$extract_number(p_attr, 'f_to_route_id', true);
      l_f_tt_out_id := jofl.jofl_pkg$extract_number(p_attr, 'f_to_tt_out_id', true);
  elseif l_f_db_method = 'ttb.stop_item_out_report'
    then
      l_f_tr_type_id := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id', true);
      l_f_round_code := '00';
      l_f_stop_type_id = rts.stop_type_pkg$finish();
  elseif l_f_db_method = 'oud.tt_action_fact'
    then
       l_f_stop_type_id = rts.stop_type_pkg$finish();
       l_f_route_variant_id = jofl.jofl_pkg$extract_number(p_attr, 'route_variant_id', true);
  end if;

  open p_rows for
  with filtered_stop_item as (
      select distinct si.stop_item_id
      from rts.stop_item si
        join rts.stop_item2round si2r on si.stop_item_id = si2r.stop_item_id
        join rts.round rnd on si2r.round_id = rnd.round_id
        join rts.route_variant rv on rnd.route_variant_id = rv.route_variant_id
        left join ttb.tt_action_item ttai on si2r.stop_item2round_id = ttai.stop_item2round_id
        left join ttb.tt_action tta on ttai.tt_action_id = tta.tt_action_id
      where not si.sign_deleted
            and (l_f_tr_type_id is null or si.tr_type_id = l_f_tr_type_id)
            and (l_f_round_code is null or rnd.code = l_f_round_code)
            and (l_f_route_id is null or rv.route_id = l_f_route_id)
            and (l_f_tt_out_id is null or tta.tt_out_id = l_f_tt_out_id)
            and (l_f_route_variant_id is null or rv.route_variant_id = l_f_route_variant_id)
            and (l_f_stop_type_id is null
                        or
                        si.stop_item_id in (
                              select si.stop_item_id
                              from rts.stop_item si
                                join rts.stop_item_type sit on si.stop_item_id = sit.stop_item_id
                              where sit.stop_type_id = l_f_stop_type_id
                              group by si.stop_item_id
                        )
                 )
  )
  select
    si.stop_item_id                            as stop_item_id,
    s.stop_id                                  as stop_id,
    s.name                                     as stop_name,
    trt.tr_type_id                             as tr_type_id,
    trt.name                                   as tr_type_name,
    sl.stop_location_id                        as stop_location_id,
    sl.building_address                        as stop_location_build_address,
    sl.building_distance                       as stop_location_build_distance,
    si.sign_deleted,
    sg.stop_group_short_name,
    s.district,
    s.region,
    s.street,
    '[' || json_build_object('CAPTION', 'На карте',
                             'LINK', json_build_object('URL', '/app.html?m=modules/StopPlaceOnMapCtrl&attr={"wkt":"' || ST_AsText(sl.geom) ||
                                                              '","sp_muid":"' || sl2g.stop_location_muid || '"}'
                             )) :: text || ']' as "JUMP_TO_SP"
  from filtered_stop_item fsi
    join rts.stop_item si on fsi.stop_item_id = si.stop_item_id
    join rts.stop_location sl on si.stop_location_id = sl.stop_location_id
    join rts.stop s on sl.stop_id = s.stop_id
    join rts.stop2stop_group s2sg on s2sg.stop_id=s.stop_id
    join rts.stop_group sg on s2sg.stop_group_id = sg.stop_group_id
    join core.tr_type trt on si.tr_type_id = trt.tr_type_id
    join rts.stop_location2gis sl2g on sl.stop_location_id = sl2g.stop_location_id
  where not sg.sign_deleted;
end;
$$;