CREATE OR REPLACE FUNCTION rts.jf_f_parks_name_pkg$of_rows(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$
begin
   open p_rows for
   select e.name_full,
       e.entity_id,
       e.name_short,
       cd.depo_id
     from core.depo cd
         JOIN core.entity e ON cd.depo_id = e.entity_id;
end;
$BODY$
LANGUAGE plpgsql;