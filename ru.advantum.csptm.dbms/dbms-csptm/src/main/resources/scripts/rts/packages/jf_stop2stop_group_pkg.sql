create or replace function rts."jf_stop2stop_group_pkg$attr_to_rowtype"(p_attr text) returns rts.stop2stop_group
LANGUAGE plpgsql
AS $$
declare 
   l_r rts.stop2stop_group%rowtype; 
begin 
   l_r.stop2stop_group_id := jofl.jofl_pkg$extract_number(p_attr, 'stop2stop_group_id', true); 
   l_r.stop_group_id := jofl.jofl_pkg$extract_varchar(p_attr, 'stop_group_id', true); 
   l_r.stop_id := jofl.jofl_pkg$extract_number(p_attr, 'stop_id', true); 
   l_r.sys_period := jofl.jofl_pkg$extract_varchar(p_attr, 'sys_period', true); 

   return l_r;
end;
$$;

create or replace function rts."jf_stop2stop_group_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
 p_attr_stop_id rts.stop2stop_group.stop_id%type := jofl.jofl_pkg$extract_number(p_attr, 'stop_id', true);
begin 
 open p_rows for 
      select 
        s2sg.stop2stop_group_id as stop2stop_group_id,
        s.name as stop_name,
        s2sg.stop_id as stop_id,
        sg.stop_group_name as stop_group_name,
        s2sg.stop_group_id as stop_group_id,
        lower(s2sg.sys_period) as lower_sysperiod,
        upper(s2sg.sys_period) as upper_sysperiod
      from rts.stop2stop_group s2sg
          join rts.stop s on s.stop_id = s2sg.stop_id
          join rts.stop_group sg on sg.stop_group_id = s2sg.stop_group_id
      where s2sg.stop_id = p_attr_stop_id;
end;
$$;

create or replace function rts."jf_stop2stop_group_pkg$of_insert"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r rts.stop2stop_group%rowtype;
begin 
   l_r := rts.jf_stop2stop_group_pkg$attr_to_rowtype(p_attr);
   insert into rts.stop2stop_group(stop_group_id, stop_id) values (l_r.stop_group_id, l_r.stop_id);
   return null;
end;
$$;

create or replace function rts."jf_stop2stop_group_pkg$of_delete"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r rts.stop2stop_group%rowtype;
begin 
   l_r := rts.jf_stop2stop_group_pkg$attr_to_rowtype(p_attr);
   delete from  rts.stop2stop_group where  stop2stop_group_id = l_r.stop2stop_group_id;
   return null;
end;
$$;

-- ----------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION rts."jf_stop2stop_group_pkg$of_default_stop" (p_id_account numeric, out p_rows refcursor, p_attr text) RETURNS refcursor
LANGUAGE 'plpgsql'
 AS $$
declare
 lf_stop_id rts.stop.stop_id%type := jofl.jofl_pkg$extract_number(p_attr, 'stop_id', true);
begin
 open p_rows for
  select s.stop_id,
         s.name as stop_name
  from rts.stop s
  where s.stop_id = lf_stop_id;
end;
$$;
