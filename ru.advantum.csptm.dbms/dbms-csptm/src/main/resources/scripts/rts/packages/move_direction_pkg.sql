﻿-- Прямое
create or replace function rts.move_direction_pkg$forward()
    returns smallint as 'select 1 :: smallint' language sql immutable;
-- Обратное
create or replace function rts.move_direction_pkg$back()
    returns smallint as 'select 2 :: smallint' language sql immutable;
