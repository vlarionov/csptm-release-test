create or replace function rts."jf_stop_item2round_pkg$attr_to_rowtype"(p_attr text) returns rts.stop_item2round
LANGUAGE plpgsql
AS $$
declare 
   l_r rts.stop_item2round%rowtype; 
begin 
   l_r.stop_item2round_id := jofl.jofl_pkg$extract_number(p_attr, 'stop_item2round_id', true); 
   l_r.round_id := jofl.jofl_pkg$extract_number(p_attr, 'round_id', true); 
   l_r.stop_item_id := jofl.jofl_pkg$extract_number(p_attr, 'stop_item_id', true); 
   l_r.order_num := jofl.jofl_pkg$extract_number(p_attr, 'order_num', true); 
   l_r.length_sector := jofl.jofl_pkg$extract_number(p_attr, 'length_sector', true); 
   l_r.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true); 
   l_r.sys_period := jofl.jofl_pkg$extract_varchar(p_attr, 'sys_period', true); 
   l_r.sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'sign_deleted', true); 

   return l_r;
end;
$$;

create or replace function rts."jf_stop_item2round_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare 
  p_attr_stop_item_id rts.stop_item.stop_item_id%type;
begin 

 /* получаем из параметра значнеие stop_id, для которой выбирать пункты остановки */
 p_attr_stop_item_id := jofl.jofl_pkg$extract_number(p_attr, 'stop_item_id', true);

 open p_rows for 
select si2rd.stop_item2round_id as stop_item2round_id,
       tt.name as tr_type_name,
       rt.route_num as route_num_text,
       '[' || json_build_object('CAPTION', rt.route_num, 'JUMP', json_build_object('METHOD', 'rts.route', 'ATTR', '{"f_route_id":"'||rt.route_id::text||'"}')) || ']' AS route_num,
       initial_stop.name as initial_stop_name,
       final_stop.name as final_stop_name,
       rd.code as round_code,
       md.move_direction_name as move_direction_name,
       si2rd.stop_item_id as stop_item_id,
       rt.route_id as route_id,
       rv.route_variant_id as route_variant_id,
       md.move_direction_id as move_direction_id,
       rd.round_id as round_id
 from rts.route rt
      join rts.route_variant rv on rv.route_id = rt.route_id and rt.current_route_variant_id = rv.route_variant_id
      join core.tr_type tt on tt.tr_type_id = rt.tr_type_id
      join rts.rv_status rvs on rv.rv_status_id = rvs.rv_status_id
      join rts.round rd on rd.route_variant_id = rv.route_variant_id
      /* подзапрос получает пару stop_item_id для round_id - начальную и конечную точки маршрута для отображения в списке маршрутов */
      join (select distinct
                   si2rd.round_id as round_id,
                   first_value(si2rd.stop_item_id) over (partition by si2rd.round_id order by si2rd.order_num ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as initial_stop_item_id,
                   last_value(si2rd.stop_item_id)  over (partition by si2rd.round_id order by si2rd.order_num ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as final_stop_item_id
            from rts.stop_item2round si2rd
                 join rts.stop_item2round_type si2rdt on si2rdt.stop_item2round_id = si2rd.stop_item2round_id
                 join rts.stop_item_type sit on sit.stop_item_type_id = si2rdt.stop_item_type_id
            where sit.stop_type_id = rts.stop_type_pkg$finish()) as round_endpoints on round_endpoints.round_id = rd.round_id
      join rts.move_direction md on md.move_direction_id = rd.move_direction_id
      join rts.stop_item2round si2rd on si2rd.round_id = rd.round_id
      /* получаем название (name) для начальной точки маршрута */
      join rts.stop_item initial_stop_item on initial_stop_item.stop_item_id =  round_endpoints.initial_stop_item_id
      join rts.stop_location initial_stop_location on initial_stop_location.stop_location_id = initial_stop_item.stop_location_id
      join rts.stop initial_stop on initial_stop.stop_id = initial_stop_location.stop_id
      /* получаем название (name) для конечной точки маршрута */
      join rts.stop_item final_stop_item on final_stop_item.stop_item_id =  round_endpoints.final_stop_item_id
      join rts.stop_location final_stop_location on final_stop_location.stop_location_id = final_stop_item.stop_location_id
      join rts.stop final_stop on final_stop.stop_id = final_stop_location.stop_id
 where si2rd.stop_item_id = p_attr_stop_item_id 
 AND rt.carrier_id=core.carrier_pkg$mgt();
end;
$$;
