﻿-- ==================================================
-- Направления движения

-- чётная
create or replace function rts.stop_group_pkg$md_even()
    returns smallint as 'select 8::smallint' language sql immutable;
-- нечётная
create or replace function rts.stop_group_pkg$md_odd()
    returns smallint as 'select 9::smallint' language sql immutable;
-- в обе стороны
create or replace function rts.stop_group_pkg$md_both()
    returns smallint as 'select 10::smallint' language sql immutable;

-- ==================================================
-- ...
-- select * from rts.stop_group order by stop_group_id
