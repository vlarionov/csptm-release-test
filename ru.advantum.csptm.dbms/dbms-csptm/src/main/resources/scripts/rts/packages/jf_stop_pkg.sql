create or replace function rts."jf_stop_pkg$attr_to_rowtype"(p_attr text) returns rts.stop
LANGUAGE plpgsql
AS $$
declare 
   l_r rts.stop%rowtype; 
begin 
   l_r.stop_id := jofl.jofl_pkg$extract_number(p_attr, 'stop_id', true); 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 
   l_r.name_in_english := jofl.jofl_pkg$extract_varchar(p_attr, 'name_in_english', true);
   l_r.sys_period := jofl.jofl_pkg$extract_varchar(p_attr, 'sys_period', true);
   l_r.sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'sign_deleted', true);

   return l_r;
end;
$$;

create or replace function rts."jf_stop_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
  lf_sign_deleted boolean;
  lf_arr_stop_id bigint[];
  lf_arr_tr_type_id smallint[];
  lf_arr_stop_location_id bigint[];

  lf_stop_id_str text;
  lf_tr_type_id_str text;
  lf_stop_location_id_str text;

  /* по-умолчанию - показываем только действующие записи */
  l_signdel_default boolean := false;

begin
 /* считываем значения фильтров */
 lf_sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'F_DELETED_INPLACE_K', true);
 IF lf_sign_deleted is null then
  lf_sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'f_sign_deleted', true);
 end if;

 lf_arr_tr_type_id := jofl.jofl_pkg$extract_narray(p_attr, 'f_tr_type_id', true);
 lf_tr_type_id_str := null;
 IF array_length(lf_arr_tr_type_id, 1) is not null then
  lf_tr_type_id_str := jofl.jofl_pkg$extract_varchar(p_attr, 'f_tr_type_id', true);
 end if;

 lf_arr_stop_id := jofl.jofl_pkg$extract_narray(p_attr, 'f_stop_id', true);
 lf_stop_id_str := null;
 IF array_length(lf_arr_stop_id, 1) is not null then
  lf_stop_id_str := jofl.jofl_pkg$extract_varchar(p_attr, 'f_stop_id', true);
 end if;

 lf_arr_stop_location_id := jofl.jofl_pkg$extract_narray(p_attr, 'f_stop_location_id', true);
 lf_stop_location_id_str := null;
 IF array_length(lf_arr_stop_location_id, 1) is not null then
  lf_stop_location_id_str := jofl.jofl_pkg$extract_varchar(p_attr, 'f_stop_location_id', true);
 end if;

 /* показываем в списке соответствующие фильтрам остановки */
 open p_rows for
  select s.stop_id,
         s.name,
         s.name_in_english,
         lower(s.sys_period) as lower_sysperiod,
         upper(s.sys_period) as upper_sysperiod,
         sd.stop_group_short_name as stop_direction,
         s.sign_deleted,
         /* возвращаем значения фильтров, которые использовались */
         lf_sign_deleted::text as f_sign_deleted,
         lf_stop_id_str as f_stop_id,
         lf_tr_type_id_str as f_tr_type_id,
         lf_stop_location_id_str as f_stop_location_id
       from rts.stop s
            left join (select s2sg.stop_id as stop_id,
                              sg.stop_group_id as stop_group_id,
                              sg.stop_group_short_name as stop_group_short_name
                       from rts.stop2stop_group s2sg
                            join rts.stop_group sg on sg.stop_group_id = s2sg.stop_group_id
                                                      and sg.stop_group_id = any(array[8,9])) sd on sd.stop_id = s.stop_id
       where
        /* фильтр по видимости удаленных записей */
	      (not s.sign_deleted or s.sign_deleted = coalesce(lf_sign_deleted, l_signdel_default))
        /* фильтр по id ОП */
        and (array_length(lf_arr_stop_id, 1) is null or s.stop_id = any(lf_arr_stop_id))
        /* фильтр по виду ТС - выбираем пунсты остановки для выбранного вида ТС (с учетом фильтра по видимоти удаленных записей) */
        and (array_length(lf_arr_tr_type_id, 1) is null OR
             exists (select 1 
			         from rts.stop_item si
                          join rts.stop_location sl on si.stop_location_id = sl.stop_location_id
                      where sl.stop_id = s.stop_id AND
                            si.tr_type_id = any(lf_arr_tr_type_id) and
                            (not si.sign_deleted or si.sign_deleted = coalesce(lf_sign_deleted, l_signdel_default))))
        /* фильтр по stop_location - ближайшее строение */
        and (array_length(lf_arr_stop_location_id, 1) is null OR
             exists(select 1
                    from rts.stop_item sil
                         join rts.stop_location sl on sil.stop_location_id = sl.stop_location_id
                    where sl.stop_id = s.stop_id AND
                          sl.stop_location_id = any (lf_arr_stop_location_id))) 
 order by s.sign_deleted, s.name;
end;
$$;

create or replace function rts."jf_stop_pkg$of_insert"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
   l_r rts.stop%rowtype;
begin
   l_r := rts.jf_stop_pkg$attr_to_rowtype(p_attr);
   
   /* назначаем значения по-умолчнию для полей */
   l_r.sign_deleted := false;

   insert into rts.stop (name, name_in_english, sign_deleted)  values (l_r.name, l_r.name_in_english, l_r.sign_deleted);

   return null;
end;
$$;

create or replace function rts."jf_stop_pkg$of_update"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
   l_r rts.stop%rowtype;
begin
   l_r := rts.jf_stop_pkg$attr_to_rowtype(p_attr);

   /* обновляем только поля с именем остановки */
   update rts.stop set
          name = l_r.name,
          name_in_english = l_r.name_in_english
   where stop_id = l_r.stop_id;

   return null;
end;
$$;

create or replace function rts."jf_stop_pkg$of_delete"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r rts.stop%rowtype;
begin 
   l_r := rts.jf_stop_pkg$attr_to_rowtype(p_attr);

   /* "логическое" удаление данных - ставим признак удаения*/
   update rts.stop set sign_deleted = true where stop_id = l_r.stop_id;

   return null;
end;
$$;
