CREATE OR REPLACE FUNCTION rts.jf_parks2routes_pkg$of_rows(
    IN  p_id_account NUMERIC,
    OUT p_rows       REFCURSOR,
    IN  p_attr       TEXT)
    RETURNS REFCURSOR AS
$$
DECLARE
    l_route_id rts.depo2route.route_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'route_id', TRUE);
BEGIN
    OPEN p_rows FOR
    SELECT
        cd.tn_instance           number,
        ct.name                  tr_type,
        entity4depo.name_full AS name_full
    FROM rts.depo2route rd
        JOIN core.depo cd ON cd.depo_id = rd.depo_id
        JOIN core.entity entity4depo ON entity4depo.entity_id = cd.depo_id
        JOIN core.tr_type ct ON ct.tr_type_id = cd.tr_type_id
    WHERE rd.route_id = l_route_id;
END;
$$
LANGUAGE plpgsql;