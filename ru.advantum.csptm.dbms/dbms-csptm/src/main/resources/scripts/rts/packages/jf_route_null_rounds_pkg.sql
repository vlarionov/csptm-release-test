﻿create or replace function rts."jf_route_null_rounds_pkg$of_rows"(
  in  p_id_account numeric,
  out p_rows       refcursor,
  in  p_attr       text)
  returns refcursor as
$$
declare
  l_route_variant_id rts.round.route_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'route_variant_id', true);
  l_route_id         rts.route.route_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', true);
  l_tr_type_id       smallint [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_tr_type_id', true);
begin
  select rt.p_rows
  into p_rows
  from rts.jf_route_rounds_pkg$rounds_type(l_route_variant_id,
                                           l_route_id,
                                           l_tr_type_id,
                                           ttb.action_type_pkg$technical_round(),
                                           p_id_account) rt;
end;
$$
language plpgsql;