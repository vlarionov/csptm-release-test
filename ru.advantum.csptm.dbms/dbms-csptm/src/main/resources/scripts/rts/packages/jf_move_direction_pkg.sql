﻿CREATE OR REPLACE FUNCTION rts.jf_move_direction_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
 lt_rf_db_method 		text 	:= jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true);
 ln_calculation_task_id integer := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true);
begin 
 open p_rows for 
      select 
        rmd.move_direction_name, 
        rmd.move_direction_id
      from rts.move_direction rmd
      where (lt_rf_db_method is null or lt_rf_db_method <> 'asd.ct_normative_result')
         or (lt_rf_db_method = 'asd.ct_normative_result' 
         	 and exists (select null
                          from asd.ct_round_stop_result res
                          join asd.ct_normative norm on norm.calculation_task_id = res.calculation_task_id
                          join asd.calculation_task ct on ct.calculation_task_id = res.calculation_task_id
                          join core.tr_type tt on tt.tr_type_id= res.tr_type_id
                          join rts.round rnd on rnd.round_id =res.round_id and not rnd.sign_deleted
                          join ttb.action_type act on act.action_type_id = rnd.action_type_id
                          join rts.route_variant rv on rv.route_variant_id = rnd.route_variant_id and not rv.sign_deleted
                          --join rts.route r on r.current_route_variant_id = rv.route_variant_id and not r.sign_deleted
                          join rts.route r on r.route_id = rv.route_id and not r.sign_deleted    
                          join core.tr_capacity cap on cap.tr_capacity_id = res.tr_capacity_id
                          join rts.move_direction md on md.move_direction_id = rnd.move_direction_id 
                          							and md.move_direction_id = rmd.move_direction_id
                        where res.calculation_task_id = ln_calculation_task_id
                         and (select sum(vr.norm_duration) 
                        	 from asd.ct_stop_visit_result vr 
                             where vr.ct_round_stop_result_id = res.ct_round_stop_result_id) > 0
                             )
             )
      order by rmd.move_direction_id; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;