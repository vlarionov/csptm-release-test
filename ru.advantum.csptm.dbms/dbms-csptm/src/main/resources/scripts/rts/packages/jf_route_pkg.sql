﻿create or replace function rts.jf_route_pkg$of_rows(
  in  p_id_account numeric,
  out p_rows       refcursor,
  in  p_attr       text
)
  returns refcursor
language plpgsql as $$
declare
  l_rf_db_method          text := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true);

  l_available_route_ids   integer [];

  l_f_depo_id             core.depo.depo_id%type;
  l_f_route_id            rts.route.route_id%type;
  l_f_tr_type_id          core.tr_type.tr_type_id%type;
  l_f_open_dt             timestamp;
  l_f_close_dt            timestamp;
  l_f_stop_item_id        rts.stop_item.stop_item_id%type;
  l_f_calculation_task_id asd.calculation_task.calculation_task_id%type;
  l_f_is_not_closed       boolean;
  l_f_depo_ids NUMERIC[];
  l_f_route_ids BIGINT[];
begin
  if l_rf_db_method is not null and l_rf_db_method not in (
    'adm.data_realm',
    'adm.data_realm_routes',
    'ASD.RPT_SPEED_ON_ROUTE'
  )
  then
    l_available_route_ids := array(select adm.account_data_realm_pkg$get_routes(p_id_account));
  end if;

  if l_rf_db_method is null
  then
    l_f_depo_id := jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', true);   
    l_f_route_id := jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', true);
    l_f_tr_type_id := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id', true);
    l_f_open_dt := jofl.jofl_pkg$extract_date(p_attr, 'f_open_DT', true);
    l_f_close_dt := jofl.jofl_pkg$extract_date(p_attr, 'f_close_DT', true);
  elseif l_rf_db_method = 'askp.visual_inspection'
    then
      l_f_calculation_task_id := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true);
      l_f_stop_item_id := jofl.jofl_pkg$extract_number(p_attr, 'stop_item_id', true);
    if l_f_stop_item_id isnull
      then
        raise exception '<<Не выбрана остановка!>>';
    end if;
  elseif l_rf_db_method = 'askp.route_inspection'
    then
      l_f_is_not_closed := true;
  elseif l_rf_db_method = 'core.trs_on_map'
    then
      l_f_tr_type_id := jofl.jofl_pkg$extract_number(p_attr, 'F_tr_type_id', true);
  elseif l_rf_db_method = 'ASD.RPT_SPEED_ON_ROUTE'
    then
      l_f_depo_id := jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', true);
  end if;

  IF l_rf_db_method = 'askp.conf_pass_on_sp_detail' THEN
    l_f_route_ids = ARRAY(
        with a AS(
        select distinct round_id
        from rts.stop_item2round
        where stop_item_id = ANY (array(select stop_item_id from rts.stop_item2gis
                              where stop_place_muid = 2993277465250441985))
        )
        select DISTINCT route_id from a
        JOIN rts.round USING(round_id)
        JOIN rts.route_variant USING(route_variant_id)
        JOIN rts.route USING(route_id));
  END IF;

  l_f_depo_ids := jofl.jofl_pkg$extract_narray(p_attr, 'ff_depo_id', true);
  open p_rows for
  select
    trt.tr_type_id,
    trt.name       as tr_type_name,
    trt.short_name as tr_type_short_name,
    r.route_num,
    r.route_id,
    r.current_route_variant_id,
    gis_r.open_date,
    gis_r.close_date
  from rts.route r
    join core.tr_type trt on trt.tr_type_id = r.tr_type_id
    /*связывыемся со старыми структурами специально чтобы не тянуть к себе непонятную инфу*/
    left join rts.route2gis r2g on r2g.route_id = r.route_id
    left join gis.routes gis_r on gis_r.muid = r2g.route_muid
  where
    not r.sign_deleted
    and (r.carrier_id = core.carrier_pkg$mgt())
    and (l_f_route_id is null or r.route_id = l_f_route_id)
    and (l_f_tr_type_id is null or trt.tr_type_id = l_f_tr_type_id)
    and (l_f_open_dt is null or l_f_open_dt = gis_r.open_date)
    and (l_f_close_dt is null or l_f_close_dt = gis_r.close_date)
    and (l_f_depo_id is null or exists(select 1
                                       from rts.depo2route d2r
                                       where d2r.route_id = r.route_id and d2r.depo_id = l_f_depo_id))
    AND (array_length(l_f_depo_ids, 1) IS NULL OR r.route_id IN (select d2r.route_id from rts.depo2route d2r WHERE depo_id = ANY(l_f_depo_ids) ) )
    and (l_f_calculation_task_id is null or not exists(select 1
                                                       from asd.route2ct r2ct
                                                       where r2ct.route_id = r.route_id
                                                             and r2ct.calculation_task_id = l_f_calculation_task_id))
    and (l_f_stop_item_id is null or r.current_route_variant_id in (select rv.route_variant_id
                                                                    from rts.stop_item2round si2r
                                                                      join rts.round rnd on si2r.round_id = rnd.round_id
                                                                      join rts.route_variant rv on rnd.route_variant_id = rv.route_variant_id
                                                                    where si2r.stop_item_id = l_f_stop_item_id
                                                                          and rv.rv_status_id = rts.rv_status_pkg$active()
                                                                          and not si2r.sign_deleted
                                                                          and not rnd.sign_deleted
                                                                          and not rv.sign_deleted))
    and (l_f_is_not_closed is null or gis_r.close_date isnull or gis_r.close_date > now())
    and (l_available_route_ids is null or r.route_id = any (l_available_route_ids))
    AND (r.route_id = ANY(l_f_route_ids) OR array_length(l_f_route_ids,1) IS NULL);
end;
$$;
