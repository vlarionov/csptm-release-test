create or replace function rts.jf_stops_order_pkg$of_rows
    (p_id_account in  numeric,
     p_attr       in  text,
     p_rows       out refcursor)
    returns refcursor
as $$ declare
    l_round_id         integer := jofl.jofl_pkg$extract_number(p_attr, 'round_id', true) :: integer;
    l_route_variant_id integer := jofl.jofl_pkg$extract_number(p_attr, 'route_variant_id', true) :: integer;
    l_round_code       text := jofl.jofl_pkg$extract_varchar(p_attr, 'round_code', true);
    l_f_route_id       numeric := -1;
    l_F_DIR_INPLACE_K  numeric := -1;
    l_f_b_order_num    numeric := -1;
begin
    l_f_route_id  := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', true), -1);
    l_F_DIR_INPLACE_K := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'F_DIR_INPLACE_K', true), -1);
    l_f_b_order_num := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_b_order_num', true), -1);
    if l_f_route_id = -1 or l_F_DIR_INPLACE_K = -1
    then
        open p_rows for
        select
            stop_item2round_id,
            si2r.order_num,
            si2r.stop_item_id,
            st.stop_id,
            st.name,
            md.move_direction_name,
            si2r.comment,
            '[' || json_build_object('CAPTION', st.name, 'JUMP', json_build_object('METHOD', 'rts.stop', 'ATTR', '{}')) || ']' as link_to_map_stops
        from rts.stop_item2round si2r
            join rts.stop_item si on si.stop_item_id = si2r.stop_item_id
            join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
            join rts.stop st on st.stop_id = sl.stop_id
            join rts.round r on si2r.round_id = r.round_id
            join rts.move_direction md on r.move_direction_id = md.move_direction_id
        where si2r.round_id = l_round_id
              and not si2r.sign_deleted
              and not si.sign_deleted
              and not st.sign_deleted
        order by si2r.round_id, si2r.order_num;
    else
        open p_rows for
        select distinct
            stop_item2round_id,
            si2r.order_num,
            si2r.stop_item_id,
            st.stop_id,
            st.name,
            md.move_direction_name,
            si2r.comment,
            '[' || json_build_object('CAPTION', st.name, 'JUMP', json_build_object('METHOD', 'rts.stop', 'ATTR', '{}')) || ']' as link_to_map_stops
        from rts.stop_item2round si2r
            join rts.stop_item si on si.stop_item_id = si2r.stop_item_id
            join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
            join rts.stop st on st.stop_id = sl.stop_id
            join rts.round r on si2r.round_id = r.round_id
            join rts.route_variant rv on r.route_variant_id = rv.route_variant_id
            join rts.route route on rv.route_variant_id = route.current_route_variant_id
            join rts.move_direction md on r.move_direction_id = md.move_direction_id
        where not si2r.sign_deleted
              and not si.sign_deleted
              and not st.sign_deleted
              and route.route_id :: numeric = l_f_route_id
              and md.move_direction_id :: numeric = l_F_DIR_INPLACE_K
              and (si2r.order_num :: numeric > l_f_b_order_num or l_f_b_order_num = -1)
              and r.action_type_id in(1,37,38)
        order by si2r.order_num;
    end if;
end;
$$ language plpgsql;