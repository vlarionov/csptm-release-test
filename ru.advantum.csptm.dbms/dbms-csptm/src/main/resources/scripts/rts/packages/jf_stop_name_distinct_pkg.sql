create or replace function rts."jf_stop_name_distinct_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
begin 
 open p_rows for 
 select  translate(array(select stop_id from rts.stop sa1 where sa1.name = s1.name)::text,'{}', '') as stop_id,
         s1.name as name,
         sum(case s1.sign_deleted
              when true then null::smallint
              when false then 1
             end) as quantity_actual,
         sum(case s1.sign_deleted
              when true then 1
              when false then null::smallint
             end) as quantity_deleted
  from rts.stop s1
  where s1.name is not null
  group by s1.name;
end;
$$;