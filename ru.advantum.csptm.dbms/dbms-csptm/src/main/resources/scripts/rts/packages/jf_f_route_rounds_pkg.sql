create or replace function rts.jf_f_route_rounds_pkg$of_rows
  (p_id_account   in  numeric,
   p_attr         in  text,
   p_rows         out refcursor)
returns refcursor
-- ========================================================================
-- Производственные рейсы текущего варианта расписания
-- для окна "Мониторинг -> График"
-- ========================================================================
as $$ declare
 l_prod_rounds_actype smallint[];
begin
l_prod_rounds_actype := ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$production_round(), false);
open p_rows for
with sp as (select s.stop_id,
                   sl.stop_location_id,
                   si.stop_item_id,
                   s.name
            from rts.stop s
                 left join rts.stop_location sl on sl.stop_id = s.stop_id
                 left join rts.stop_item si on si.stop_location_id = sl.stop_location_id
            where     not s.sign_deleted
                  and not sl.sign_deleted
                  and not si.sign_deleted)
select distinct on (rt.tr_type_id, rt.route_id, rv.route_variant_id, rd.round_num)
       rt.tr_type_id,
       trt.name as tr_type_name,
       rt.route_id,
       rt.route_num,
       rd.round_id,
       rd.code,
       sp_s.name as start_stop_item_name,
       sp_f.name as finish_stop_item_name,
       rv.route_variant_id,
       rd.round_num,
       rd.move_direction_id
from (select * from rts.route r where r.route_id in (select adm.account_data_realm_pkg$get_routes(p_id_account))) rt
     join rts.route_variant rv on rv.route_id = rt.route_id and rv.route_variant_id = rt.current_route_variant_id
     join rts.round rd on rd.route_variant_id = rv.route_variant_id
     join rts.stop_item2round si2r on si2r.round_id = rd.round_id
     join sp as sp_s on sp_s.stop_item_id = si2r.stop_item_id
     join sp as sp_f on sp_f.stop_item_id = rts.round_pkg$get_last_stop_item_id(rd.round_id)
     join core.tr_type trt on trt.tr_type_id = rt.tr_type_id
where not rt.sign_deleted
      and not rv.sign_deleted
      and not rd.sign_deleted
      and rd.action_type_id = any(l_prod_rounds_actype)
order by rt.tr_type_id, rt.route_id, rv.route_variant_id, rd.round_num;
end;
$$ language plpgsql;
