create or replace function rts."jf_parking_pkg$attr_to_rowtype"(p_attr text) returns rts.parking
LANGUAGE plpgsql
AS $$
declare
   l_r rts.parking%rowtype;
begin
   l_r.parking_id := jofl.jofl_pkg$extract_number(p_attr, 'parking_id', true);
   l_r.square := jofl.jofl_pkg$extract_number(p_attr, 'square', true);
   l_r.tr_count := jofl.jofl_pkg$extract_number(p_attr, 'tr_count', true);
  -- l_r.sys_period := jofl.jofl_pkg$extract_varchar(p_attr, 'sys_period', true);
   l_r.sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'sign_deleted', true);
   --l_r.sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'sign_deleted', true);
   return l_r;
end;
$$;

create or replace function rts."jf_parking_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
begin
 open p_rows for
   select
      p.parking_id,
      p.square,
      p.tr_count,
      p.sign_deleted,
      e.name_full,
      e.name_short,
      e.comment,
      zl.zone_location_id,
      zl.sign_deleted loc_del,
     '[' || json_build_object('CAPTION', 'На карте',
                                   'LINK', json_build_object('URL', '/app.html?m=modules/ParkingOnMapCtrl&attr={"p_zone_location_id":"'||zl.zone_location_id ||
				'"}'))::text || ']' AS maplink
  from           rts.parking p
       join      core.entity e        on p.parking_id = e.entity_id
       join      rts.parking2zone pz  on pz.parking_id = p.parking_id
       left join rts.zone_location zl on zl.zone_location_id = pz.zone_location_id and not zl.sign_deleted
  where not p.sign_deleted;
end;
$$;

create or replace function rts."jf_parking_pkg$of_insert"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
   l_r rts.parking%rowtype;
   l_e core.entity%ROWTYPE;
begin
   l_r := rts.jf_parking_pkg$attr_to_rowtype(p_attr);
   l_e.name_short := jofl.jofl_pkg$extract_varchar(p_attr, 'name_short', true);
   l_e.name_full := jofl.jofl_pkg$extract_varchar(p_attr, 'name_full', true);
   l_e.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true);
   l_r.sign_deleted := false;
   l_r.sys_period :=  tstzrange(now(), null);

   insert into core.entity(name_full, name_short, comment)
     values(l_e.name_short, l_e.name_short, l_e.name_short)
     returning entity_id into l_r.parking_id;
   insert into rts.parking select l_r.*;

   return null;
end;
$$;

create or replace function rts."jf_parking_pkg$of_update"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
   l_r rts.parking%rowtype;
   l_e core.entity%ROWTYPE;
begin
  -- l_r := rts.jf_parking_pkg$attr_to_rowtype(p_attr);
   l_e.name_short := jofl.jofl_pkg$extract_varchar(p_attr, 'name_short', true);
   l_e.name_full := jofl.jofl_pkg$extract_varchar(p_attr, 'name_full', true);
   l_e.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true);

   update core.entity
     set name_full = l_e.name_full,
         name_short = l_e.name_short,
         comment = l_e.comment
      where entity_id = l_r.parking_id;
   update rts.parking set
          square = l_r.square,
          tr_count = l_r.tr_count
   where
          parking_id = l_r.parking_id;

   return null;
end;
$$;

create or replace function rts."jf_parking_pkg$of_delete"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
   l_r rts.parking%rowtype;
begin
   l_r := rts.jf_parking_pkg$attr_to_rowtype(p_attr);

   update rts.parking set sign_deleted = true where parking_id = l_r.parking_id;

   return null;
end;
$$;

