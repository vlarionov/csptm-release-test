CREATE OR REPLACE FUNCTION rts.jf_stop2route4select_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  f_route_id INTEGER;
  f_list_route_id INTEGER[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_route_id', false);
  f_list_action_type_id SMALLINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_action_type_id', true);
  --f_list_route_id:"$array[3966]
-- {"dt_b_DT":"2017-12-17 21:00:00.0","dt_e_DT":"2017-12-18 20:59:59.0"
-- ,"F_tr_type_id":1,"f_list_depo_id":"$array[]","f_list_route_id":"$array[4892]"
-- ,"F_<поле>":"","f_list_dr_shift_id":"$array[]","f_list_tr_id":"$array[]","f_list_driver_id":"$array[]"
-- ,"f_list_action_type_id":"$array[]",
--   "f_list_stop_item2round_id":"$array[]","RF_DB_METHOD":"ttb.jf_plan_fact_time_stop_place_rpt"}
BEGIN
  OPEN p_rows FOR
  SELECT DISTINCT
    sir.stop_item2round_id,
    s.stop_id,
    s.name stop_name,
    act.action_type_code,
    sir.order_num,
    rt.route_num,
    r.move_direction_id,
    act.action_type_id
  FROM rts.route rt
    JOIN rts.route_variant rv ON rt.current_route_variant_id = rv.route_variant_id
    JOIN rts.round r ON rv.route_variant_id = r.route_variant_id AND NOT r.sign_deleted
    JOIN rts.move_direction md ON r.move_direction_id = md.move_direction_id
    JOIN ttb.action_type act
      ON r.action_type_id = act.action_type_id AND ttb.action_type_pkg$is_production_round(act.action_type_id)
    JOIN rts.stop_item2round sir ON r.round_id = sir.round_id
    JOIN rts.stop_item si ON sir.stop_item_id = si.stop_item_id
    JOIN rts.stop_location sl ON sl.stop_location_id = si.stop_location_id
    JOIN rts.stop s ON sl.stop_id = s.stop_id
  WHERE 1=1
  and (rt.route_id = any(f_list_route_id) or array_length(f_list_route_id, 1) is null)
  and  (ttb.action_type_pkg$is_parent_type(p_type_id => r.action_type_id , p_parent_type_id => f_list_action_type_id) or array_length(f_list_action_type_id, 1) is null)

  ORDER BY act.action_type_id, r.move_direction_id, sir.order_num;
END;
$$;

