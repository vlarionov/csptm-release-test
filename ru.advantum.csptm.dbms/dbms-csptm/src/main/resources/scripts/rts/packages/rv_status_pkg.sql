create or replace function rts.rv_status_pkg$active()
  returns rts.rv_status.rv_status_id%type
immutable
language plpgsql
as $$
begin
  return 1;
end;
$$;

create or replace function rts.rv_status_pkg$inactive()
  returns rts.rv_status.rv_status_id%type
immutable
language plpgsql
as $$
begin
  return 2;
end;
$$;

create or replace function rts.rv_status_pkg$in_work()
  returns rts.rv_status.rv_status_id%type
immutable
language plpgsql
as $$
begin
  return 3;
end;
$$;