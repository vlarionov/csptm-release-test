create or replace function rts.round_pkg$get_last_stop_item_id(
      p_round_id rts.round.round_id%type,
  out p_result   rts.stop_item.stop_item_id%type
)
stable
language plpgsql as $$
begin
  select si.stop_item_id
  from rts.stop_item2round si2r
    join rts.stop_item si on si2r.stop_item_id = si.stop_item_id
  where si2r.round_id = p_round_id
        and not si2r.sign_deleted
        and not si.sign_deleted
  order by order_num desc
  limit 1
  into p_result;
end;
$$;


create or replace function rts.round_pkg$get_depo2terr_id(in p_round_id numeric) returns numeric as
  $body$
  declare
    l_res numeric;
  begin

    select d2t.depo2territory_id into l_res
    from rts.stop_item2round sir
      join rts.stop_item si on si.stop_item_id = sir.stop_item_id
      join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
      join rts.depo2zone d2z on d2z.zone_location_id = sl.depo_zone_location_id
      join rts.territory2zone t2z on t2z.zone_location_id = sl.depo_zone_location_id
      join core.depo2territory d2t on d2t.territory_id = t2z.territory_id  and d2z.depo_id = d2t.depo_id
    where sir.round_id= p_round_id
          and now()::date between coalesce(d2t.dt_begin, now()::date) and coalesce(d2t.dt_end, now()::date)
    limit 1;


  return l_res;
end;
$body$
language 'plpgsql'
stable