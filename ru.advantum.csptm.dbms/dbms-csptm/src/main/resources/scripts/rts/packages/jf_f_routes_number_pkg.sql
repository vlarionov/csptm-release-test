create or replace function rts.jf_f_routes_number_pkg$of_rows(
  in  p_id_account numeric,
  out p_rows       refcursor,
  in  p_attr       text
)
  returns refcursor
language plpgsql as $$
declare
  l_rf_db_method        text := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true);
  l_carrier_id          bigint := jofl.jofl_pkg$extract_number(p_attr, 'f_carrier_id', true);
  l_depo_list           bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', TRUE);
  l_available_route_ids integer [];
  l_f_tr_type_id        integer;
  l_period_start        timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_START_DT', true);
  l_period_end          timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_END_DT', true); 
  l_date                timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'F_REPORT_DT', true); 
begin
  if l_rf_db_method in ('oud.route_rounds', 'ttb.order_list', 'oud.tr_out_driver', 'oud.round_interval_item','oud.round_interval','rts.route')
  then
    l_available_route_ids := array(select adm.account_data_realm_pkg$get_routes(p_id_account));
  else
    l_f_tr_type_id := coalesce(
        jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id', true),
        jofl.jofl_pkg$extract_number(p_attr, 'F_tr_type_id', true)
    );
  end if;

  open p_rows for
   select distinct r.route_num        as route_num,
                   r.route_id :: text as route_id,
                   trt.short_name     as tr_type_short_name,
	                 r2g.route_muid::text  as route_muid
   from rts.route r
        join core.tr_type trt on r.tr_type_id = trt.tr_type_id
        left join rts.route_variant rv on rv.route_id = r.route_id
        left join rts.route2gis r2g on r2g.route_id = r.route_id
   where not rv.sign_deleted
         and (l_carrier_id is null or r.carrier_id = l_carrier_id)
         and (l_available_route_ids is null or r.route_id = any (l_available_route_ids))
         and (l_f_tr_type_id is null or r.tr_type_id = (l_f_tr_type_id))
         and ((l_date isnull and l_period_start isnull and l_period_end isnull) or((l_date is not null and rv.action_period @> l_date)
              or (l_period_start is not null and
                  l_period_end is not null and
                  not isempty(rv.action_period * tsrange(l_period_start, l_period_end, '[]')))))
         --and (cardinality(l_depo_list) = 0 or exists (select 1
         and (array_length(l_depo_list, 1) is null or exists (select 1
                                                      from rts.depo2route d2r
                                                      where d2r.route_id = r.route_id and
                                                            d2r.depo_id = any(l_depo_list) and
                                                            not d2r.sign_deleted))
order by r.route_num;
end;
$$;
