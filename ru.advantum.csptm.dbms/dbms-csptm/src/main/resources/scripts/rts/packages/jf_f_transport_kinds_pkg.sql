CREATE OR REPLACE FUNCTION rts.jf_f_transport_kinds_pkg$of_rows(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$
begin
   open p_rows for
   select tt.tr_type_id,
       tt.name,
       tt.short_name
     from core.tr_type tt
    order by 1;
end;
$BODY$
LANGUAGE plpgsql;