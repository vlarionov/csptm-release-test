create or replace function rts."jf_gas_station_pkg$attr_to_rowtype"(p_attr text) returns rts.gas_station
LANGUAGE plpgsql
AS $$
declare 
   l_r rts.gas_station%rowtype; 
begin 
   l_r.gas_station_id := jofl.jofl_pkg$extract_number(p_attr, 'gas_station_id', true);
   l_r.time_fueling := jofl.jofl_pkg$extract_varchar(p_attr, 'time_fueling', true);
   l_r.sys_period := jofl.jofl_pkg$extract_varchar(p_attr, 'sys_period', true);
   return l_r;
end;
$$;

create or replace function rts."jf_gas_station_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare 
begin 
 open p_rows for
   select gs.gas_station_id,
          ce.name_full,
          ce.name_short,
          gs.time_fueling,
--          ce.sys_period as ce_sys_period,
--          gs.sys_period as gs_sys_period,
          gs2z.zone_location_id,
          gs2z.gas_station2zone_id
  /* , столбец со ссылкой на карту */
   from rts.gas_station gs
        join core.entity ce on ce.entity_id = gs.gas_station_id
        left join rts.gas_station2zone gs2z on gs2z.gas_station_id = gs.gas_station_id;

end;
$$;

create or replace function rts."jf_gas_station_pkg$of_insert"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r rts.gas_station%rowtype;
   l_core_entity core.entity%rowtype;
begin 
   -- l_r := rts.jf_gas_station_pkg$attr_to_rowtype(p_attr);  
  l_r.time_fueling := jofl.jofl_pkg$extract_varchar(p_attr, 'time_fueling', true);

  l_core_entity.name_full := jofl.jofl_pkg$extract_varchar(p_attr, 'name_full', true);
  l_core_entity.name_short := jofl.jofl_pkg$extract_varchar(p_attr, 'name_short', true);
  l_core_entity.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true);
  l_core_entity.sys_period :=  tstzrange(now(), null);
  l_r.sys_period := l_core_entity.sys_period;

  insert into core.entity(name_full, name_short, comment, sys_period)
   values (l_core_entity.name_full, l_core_entity.name_short, l_core_entity.comment, l_core_entity.sys_period)
   returning entity_id into l_r.gas_station_id;
  
  insert into rts.gas_station(gas_station_id, time_fueling, sys_period)
   values(l_r.gas_station_id, l_r.time_fueling, l_r.sys_period);

   return null;
end;
$$;

create or replace function rts."jf_gas_station_pkg$of_update"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
   l_r rts.gas_station%rowtype;
   l_core_entity core.entity%rowtype;
begin
   -- l_r := rts.jf_gas_station_pkg$attr_to_rowtype(p_attr);

  l_r.gas_station_id := jofl.jofl_pkg$extract_number(p_attr, 'gas_station_id', true);

  l_core_entity.name_full := jofl.jofl_pkg$extract_varchar(p_attr, 'name_full', true);
  l_core_entity.name_short := jofl.jofl_pkg$extract_varchar(p_attr, 'name_short', true);
  l_core_entity.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true);
  l_r.time_fueling := jofl.jofl_pkg$extract_varchar(p_attr, 'time_fueling', true);

  update core.entity ce
   set ce.name_short = l_core_entity.name_short,
       ce.comment = l_core_entity.comment,
       ce.name_full = l_core_entity.name_full
   where ce.entity_id = l_r.gas_station_id;

  update rts.gas_station set
         time_fueling = l_r.time_fueling
  where gas_station_id = l_r.gas_station_id;

  return null;
end;
$$;

create or replace function rts."jf_gas_station_pkg$of_delete"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r rts.gas_station%rowtype;
begin
  l_r.gas_station_id := jofl.jofl_pkg$extract_number(p_attr, 'gas_station_id', true);

   -- удаляем АЗС из связанных таблиц
   delete from rts.gas_station2zone where gas_station_id = l_r.gas_station_id;
   delete from  rts.gas_station where  gas_station_id = l_r.gas_station_id;
   delete from  core.entity where entity.entity_id = l_r.gas_station_id;

   return null;
end;
$$;
