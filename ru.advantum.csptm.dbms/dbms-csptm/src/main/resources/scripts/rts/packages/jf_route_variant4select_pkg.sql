CREATE OR REPLACE FUNCTION rts.jf_route_variant4select_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
 l_ref_db_method text := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true);
 ln_norm_id 	 ttb.norm.norm_id%type := jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true);	
begin
 open p_rows for
     select
       r.route_num,
       rv.route_variant_id,
       rv.route_id,
       rv.rv_status_id,
       rv.rv_comment,
       lower(rv.action_period) as dt_begin,
       upper(rv.action_period) as dt_end,
       tt.name as tr_type_name,
       rvs.rv_status_name,
       r.route_num ||' ('|| to_char(lower(rv.action_period),'dd.mm.yyyy') || '-'||coalesce(to_char(upper(rv.action_period),'dd.mm.yyyy'),'нвр')||')'   as rv_name
     from rts.route r
       join rts.route_variant rv on rv.route_variant_id = r.current_route_variant_id and not rv.sign_deleted
       join rts.rv_status rvs on rvs.rv_status_id = rv.rv_status_id
       join core.tr_type tt on tt.tr_type_id = r.tr_type_id
     where  rv.action_period @> now()::timestamp
       and (exists (select null
                     from asd.route_variant2ct rvct
                     join ttb.norm norm on norm.calculation_task_id = rvct.calculation_task_id
                     and rvct.route_variant_id = rv.route_variant_id
                     where norm.norm_id = ln_norm_id )
            or  ln_norm_id is null);


end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;