﻿CREATE OR REPLACE FUNCTION rts.jf_round_stops_pkg$of_rows (
  p_id_account numeric,
  p_attr text,
  out p_rows refcursor
)
RETURNS refcursor AS
-- ========================================================================
-- Остановки рейса
-- ========================================================================
$body$
 declare
   l_round_id integer := jofl.jofl_pkg$extract_number(p_attr, 'f_round_id', true)::integer; 
begin
   open p_rows for
   select stop_item2round_id, si2r.order_num, si2r.stop_item_id, s.stop_id, s.name, si2r.comment
     from rts.stop_item2round si2r
   	 join rts.stop_item si on si.stop_item_id = si2r.stop_item_id
   	 join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
   	 join rts.stop s on s.stop_id = sl.stop_id
    where si2r.round_id = l_round_id
      and not si2r.sign_deleted
      and not si.sign_deleted
      and not s.sign_deleted
    order by si2r.order_num;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;