CREATE OR REPLACE FUNCTION rts.jf_route_reference_rpt_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                                  p_attr       TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  --attr:{"rpt_dt_DT":"2017-12-18 21:00:00.0","f_tr_type_id":"1","f_route_id":"3957","f_depo_id":""}
  f_tr_type_id SMALLINT := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id' , true);
  f_route_id INTEGER := jofl.jofl_pkg$extract_number(p_attr, 'f_route_id' , true);
  f_depo_id INTEGER := jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id' , true);
BEGIN

  OPEN p_rows FOR
  SELECT
null as depo_name,
null as tr_type_id,
null as route_number,
null as movement_type_id,
null as round_count_plan,
null as round_count_plan_00,
null as round_count_fact,
null as round_count_fact_00,
null as round_finish_prc,
null as round_finish_00_prc,
null as mileage_plan,
null as mileage_fact,
null as mileage_prc,
null as mileage_line_plan,
null as mileage_line_fact,
null as mileage_tech_plan,
null as mileage_tech_fact,
null as mileage_tech_quota_plan,
null as mileage_tech_quota_fact,
null as order_time_plan,
null as order_time_fact,
null as order_time_prc,
null as line_time_plan,
null as line_time_fact,
null as nonline_tima_plan,
null as nonline_time_fact,
null as nonline_time_quota_plan,
null as nonline_time_quota_fact
;


/*
  if f_tr_type_id is null then
  OPEN p_rows FOR
  SELECT
    NULL AS tr_type_id,
    NULL AS tr_capacity_id,
    NULL AS depo_name,
    NULL AS round_type,
    NULL AS round_length,
    NULL AS round_duration,
    NULL AS stop_ab,
    NULL AS stop_places,
    NULL AS movement_type_id,
    NULL AS interval_plan,
    NULL AS interval_fact,
    NULL AS interval_calc,
    NULL AS route_switching,
    NULL AS stop_places_length,
    NULL AS stop_places_duration,
    NULL AS round_characters,
    NULL AS pass_traffic_stop_place,
    NULL AS pass_traffic_time,
    NULL AS traffic_condition,
    NULL AS round_heading
  --    from luchininov.rep_22254
  ;
    RETURN ;
end if;
  OPEN p_rows FOR
  WITH de AS (SELECT
                string_agg(trim(de.name_short), ', ') de_name_short,
                string_agg(trim(de.name_full), ', ')  de_name_full,
                d2r.route_id
              FROM rts.depo2route d2r
                JOIN core.entity de ON d2r.depo_id = de.entity_id
          where (f_depo_id is null or d2r.depo_id=f_depo_id)
              GROUP BY d2r.route_id
  )
    , ttr AS (
      SELECT
        ttr.tt_variant_id,
        string_agg(DISTINCT trc.short_name, ', ') trc_short_name
--         string_agg(DISTINCT te.name_short, ', ')  terr_short_name,
--         string_agg(DISTINCT de.name_short, ', ')  depo_short_name
      FROM ttb.tt_tr ttr
        JOIN core.tr_capacity trc ON ttr.tr_capacity_id = trc.tr_capacity_id
--         JOIN core.depo2territory d2t ON ttr.depo2territory_id = d2t.depo2territory_id
--         JOIN core.entity te ON d2t.territory_id = te.entity_id
--         JOIN core.entity de ON d2t.depo_id = de.entity_id
      GROUP BY ttr.tt_variant_id
  )
  SELECT
    tt.short_name                                                                                   AS tr_type_id,
    ttr.trc_short_name                                                                              AS tr_capacity_id,
    de.de_name_short                                                                                AS depo_name,
    act.action_type_code                                                                            AS round_type,
    rn.length                                                                                       AS round_length,
    NULL                                                                                            AS round_duration,
    concat_ws(' - ', rts.jf_route_reference_rpt_pkg$getEndingStation(p_round_id => rn.round_id, a_what => 'FIRST')
    , rts.jf_route_reference_rpt_pkg$getEndingStation(p_round_id => rn.round_id, a_what => 'LAST')) AS stop_ab,
    NULL                                                                                            AS stop_places,
    NULL                                                                                            AS movement_type_id,
    NULL                                                                                            AS interval_plan,
    NULL                                                                                            AS interval_fact,
    NULL                                                                                            AS interval_calc,
    NULL                                                                                            AS route_switching,
    NULL                                                                                            AS stop_places_length,
    NULL                                                                                            AS stop_places_duration,
    NULL                                                                                            AS round_characters,
    NULL                                                                                            AS pass_traffic_stop_place,
    NULL                                                                                            AS pass_traffic_time,
    NULL                                                                                            AS traffic_condition,
    md.move_direction_name                                                                          AS round_heading

  FROM ttb.tt_variant ttv
    JOIN ttb.tt_action_type tat ON ttv.tt_variant_id = tat.tt_variant_id
    JOIN ttb.tt_at_round tar ON tat.tt_action_type_id = tar.tt_action_type_id
    JOIN ttb.action_type act ON tat.action_type_id = act.action_type_id
    JOIN rts.round rn ON tar.round_id = rn.round_id
    JOIN rts.move_direction md ON rn.move_direction_id = md.move_direction_id
    JOIN rts.route_variant rv
      ON rv.route_variant_id = rn.route_variant_id AND ttv.route_variant_id = rv.route_variant_id
    JOIN de ON de.route_id = rv.route_id
    JOIN ttr ON ttr.tt_variant_id = ttv.tt_variant_id
    join rts.route r on rv.route_variant_id = r.current_route_variant_id
    join core.tr_type tt on tt.tr_type_id=r.tr_type_id
  WHERE 1 = 1
        AND ttb.action_type_pkg$is_production_round(tat.action_type_id)
    and r.tr_type_id = f_tr_type_id
    and r.route_id = f_route_id
  --      AND ttv.tt_variant_id = 48
  ;
*/
END;
$$;

CREATE OR REPLACE FUNCTION rts.jf_route_reference_rpt_pkg$getEndingStation(p_round_id INTEGER,
                                                                           a_what     TEXT DEFAULT 'BOTH')
  RETURNS TEXT
AS
$$
DECLARE
  rc TEXT;
BEGIN

  IF a_what <> 'FIRST' and a_what <> 'LAST'  THEN
    rc := 'Неопределено';
  ELSE
    WITH sir AS (
      SELECT
        si2r.round_id,
        min(si2r.order_num) mynum,
        'FIRST'             what
      FROM rts.stop_item2round si2r
      WHERE round_id = p_round_id
      GROUP BY si2r.round_id
      UNION ALL
      SELECT
        si2r.round_id,
        max(si2r.order_num) mynum,
        'LAST'              what
      FROM rts.stop_item2round si2r
      WHERE round_id = p_round_id
      GROUP BY si2r.round_id

    )
    SELECT s.name
    INTO rc
    FROM sir
      JOIN rts.stop_item2round si2r ON si2r.round_id = sir.round_id AND si2r.order_num = mynum
      JOIN rts.stop_item si ON si2r.stop_item_id = si.stop_item_id
      JOIN rts.stop_location sl on sl.stop_location_id = si.stop_location_id
      JOIN rts.stop s ON sl.stop_id = s.stop_id
    WHERE sir.what = a_what;
  END IF;
  RETURN rc;
END;
$$
LANGUAGE plpgsql
