create or replace function rts.jf_current_route_variant_rounds_pkg$of_rows
  (p_id_account   in  numeric,
   p_attr         in  text,
   p_rows         out refcursor)
returns refcursor
-- ========================================================================
-- ����� �������� �������� ��������
-- ========================================================================
as $$ declare
   l_route_id            integer := coalesce(jofl.jofl_pkg$extract_number(p_attr, 's_route_id', true)::integer, 
                                             jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', true)::integer);
   l_move_direction_id   integer := jofl.jofl_pkg$extract_number(p_attr, 'f_move_direction_id', true)::integer;
begin
   open p_rows for
   select round_id, rnd.round_num, rnd.code, mvd.move_direction_id, mvd.move_direction_name
     from rts.route rut
     join rts.route_variant rtv on rtv.route_variant_id = rut.current_route_variant_id
     join rts.round rnd on rnd.route_variant_id = rtv.route_variant_id
     join rts.move_direction mvd on mvd.move_direction_id = rnd.move_direction_id
    where rut.route_id = l_route_id
      and (l_move_direction_id is null or rnd.move_direction_id = l_move_direction_id)
      and not rnd.sign_deleted
    order by rnd.code, mvd.move_direction_id;
end;
$$ language plpgsql;
