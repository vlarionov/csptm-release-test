create or replace function rts."jf_auto_station_pkg$attr_to_rowtype"(p_attr text) returns rts.parking
LANGUAGE plpgsql
AS $$
declare
   l_r rts.auto_station%rowtype;
begin
   l_r.auto_station_id := jofl.jofl_pkg$extract_number(p_attr, 'auto_station_id', true);
--   l_r.sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'sign_deleted', true);*/
   return l_r;
end;
$$;


create or replace function rts."jf_auto_station_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
begin
 open p_rows for
   select
      p.auto_station_id,
      e.name_full,
      e.name_short,
      e.comment,
      '[' || json_build_object('CAPTION', 'На карте',
                               'LINK', json_build_object('URL', '/app.html?m=modules/AutoStationOnMapCtrl&attr={"AutoStation_location_id":"'||zl.zone_location_id ||
       '"}'))::text || ']' AS maplink

  from           rts.auto_station p
       join      core.entity e        on p.auto_station_id = e.entity_id
       left join rts.auto_station2zone pz  on pz.auto_station_id = p.auto_station_id
       left join rts.zone_location zl on zl.zone_location_id = pz.zone_location_id and not zl.sign_deleted;
end;
$$;

create or replace function rts."jf_auto_station_pkg$of_insert"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
   l_r rts.auto_station%rowtype;
   l_e core.entity%ROWTYPE;
begin
   l_r.auto_station_id := jofl.jofl_pkg$extract_number(p_attr, 'auto_station_id', true);
   l_e.name_short := jofl.jofl_pkg$extract_varchar(p_attr, 'name_short', true);
   l_e.name_full := jofl.jofl_pkg$extract_varchar(p_attr, 'name_full', true);
   l_e.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true);
   l_r.sys_period :=  tstzrange(now(), null);

   insert into core.entity(name_full, name_short, comment)
     values(l_e.name_short, l_e.name_short, l_e.name_short)
     returning entity_id into l_r.auto_station_id;
   insert into rts.auto_station select l_r.*;

   return null;
end;
$$;

create or replace function rts."jf_auto_station_pkg$of_update"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
   l_r rts.auto_station%rowtype;
   l_e core.entity%ROWTYPE;
begin
   l_r.auto_station_id := jofl.jofl_pkg$extract_number(p_attr, 'auto_station_id', true);
   l_e.name_short := jofl.jofl_pkg$extract_varchar(p_attr, 'name_short', true);
   l_e.name_full := jofl.jofl_pkg$extract_varchar(p_attr, 'name_full', true);
   l_e.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true);

   update core.entity
     set name_full = l_e.name_full,
         name_short = l_e.name_short,
         comment = l_e.comment
      where entity_id = l_r.auto_station_id;
    update rts.auto_station a -- чтобы срабатывали триггеры
     set a.auto_station_id = a.auto_station_id
     where a.auto_station_id = l_r.auto_station_id;
  return null;
end;
$$;

create or replace function rts."jf_auto_station_pkg$of_delete"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
   l_r rts.auto_station%rowtype;
begin
   l_r.auto_station_id := jofl.jofl_pkg$extract_number(p_attr, 'auto_station_id', true);
  -- update rts.parking set sign_deleted = true where parking_id = l_r.parking_id;
   delete from rts.auto_station t where t.auto_station_id = l_r.auto_station_id;
   delete from core.entity t where t.entity_id = l_r.auto_station_id;
   return null;
end;
$$;

