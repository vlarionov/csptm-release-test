
create or replace function rts."jf_stop_group_pkg$attr_to_rowtype"(p_attr text) returns rts.stop_group
LANGUAGE plpgsql
AS $$
declare 
   l_r rts.stop_group%rowtype; 
begin 
   l_r.stop_group_id := jofl.jofl_pkg$extract_varchar(p_attr, 'stop_group_id', true); 
   l_r.stop_group_name := jofl.jofl_pkg$extract_varchar(p_attr, 'stop_group_name', true); 
   l_r.stop_group_short_name := jofl.jofl_pkg$extract_varchar(p_attr, 'stop_group_short_name', true);
   l_r.stop_group_desc := jofl.jofl_pkg$extract_varchar(p_attr, 'stop_group_desc', true);
   l_r.sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'sign_deleted', true); 
   l_r.sys_period := jofl.jofl_pkg$extract_varchar(p_attr, 'sys_period', true); 

   return l_r;
end;
$$;

create or replace function rts."jf_stop_group_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
  lf_sign_deleted boolean;
  lf_rf_db_method text;
begin
 lf_sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'f_sign_deleted', true);
 lf_rf_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true);

 /* выбираем группы в соответствии с фильтром */
 open p_rows for
      select sg.stop_group_id, 
             sg.stop_group_name,
             sg.stop_group_short_name,
             sg.stop_group_desc, 
             sg.sign_deleted, 
             lower(sg.sys_period) as lower_sysperiod,
             upper(sg.sys_period) as upper_sysperiod
      from rts.stop_group sg
      where /* фильтр по sign_deleted */
            (lf_rf_db_method is not null and not sg.sign_deleted) or
            lf_rf_db_method is null
	  order by sg.sign_deleted, sg.stop_group_id desc;
end;
$$;

create or replace function rts."jf_stop_group_pkg$of_insert"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r rts.stop_group%rowtype;
begin 
   l_r := rts.jf_stop_group_pkg$attr_to_rowtype(p_attr);

   /* назначаем значения по-умолчнию для полей */
   l_r.sign_deleted := false;
   
   insert into rts.stop_group (stop_group_name, stop_group_short_name, stop_group_desc, sign_deleted) 
    values (l_r.stop_group_name, l_r.stop_group_short_name, l_r.stop_group_desc, l_r.sign_deleted);

   return null;
end;
$$;

create or replace function rts."jf_stop_group_pkg$of_update"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r rts.stop_group%rowtype;
begin 
   l_r := rts.jf_stop_group_pkg$attr_to_rowtype(p_attr);

   /* обновляем только содержательные поля */
   update rts.stop_group set 
          stop_group_name = l_r.stop_group_name,
          stop_group_short_name = l_r.stop_group_short_name,
          stop_group_desc = l_r.stop_group_desc
   where 
          stop_group_id = l_r.stop_group_id;

   return null;
end;
$$;

create or replace function rts."jf_stop_group_pkg$of_delete"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r rts.stop_group%rowtype;
begin 
   l_r := rts.jf_stop_group_pkg$attr_to_rowtype(p_attr);

   /* логическое удаление данных */
   update rts.stop_group
    set sign_deleted = true	    
   where  stop_group_id = l_r.stop_group_id;

   return null;
end;
$$;
