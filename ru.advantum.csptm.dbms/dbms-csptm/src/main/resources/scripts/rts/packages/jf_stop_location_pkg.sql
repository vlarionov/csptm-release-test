create or replace function rts."jf_stop_location_pkg$attr_to_rowtype"(p_attr text) returns rts.stop_location
LANGUAGE plpgsql
AS $$
declare 
   l_r rts.stop_location%rowtype; 
begin 
   l_r.stop_location_id := jofl.jofl_pkg$extract_number(p_attr, 'stop_location_id', true); 
   l_r.building_address := jofl.jofl_pkg$extract_varchar(p_attr, 'building_address', true); 
   l_r.building_distance := jofl.jofl_pkg$extract_varchar(p_attr, 'building_distance', true); 
   l_r.sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'sign_deleted', true); 
   l_r.sys_period := jofl.jofl_pkg$extract_varchar(p_attr, 'sys_period', true); 

   return l_r;
end;
$$;

create or replace function rts."jf_stop_location_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
  lf_sign_deleted boolean;
  l_stop_id rts.stop.stop_id%type;
begin
  l_stop_id := jofl.jofl_pkg$extract_number(p_attr, 'stop_id', true);
  lf_sign_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'f_sign_deleted', true);
  /* выбираем поля, представляющие таблицу, как справочник мест (адресов) остановки ТС */
 open p_rows for select sl.stop_location_id,
                        sl.building_address, 
                        sl.building_distance, 
                        sl.sign_deleted,
                		    lower(sl.sys_period) as lower_sysperiod,
                        upper(sl.sys_period) as upper_sysperiod,
                        '[' || json_build_object('CAPTION', 'На карте',
                                                 'LINK', json_build_object('URL', '/app.html?m=modules/StopPlaceOnMapCtrl&attr={"wkt":"' ||
                                                                           ST_AsText(sl.geom) ||'","sp_muid":"' || g.stop_location_muid || '"}'
                             ))::text || ']' AS "JUMP_TO_SP"
                 from rts.stop_location sl
                      join rts.stop_location2gis g on sl.stop_location_id = g.stop_location_id
                 where /* фильтры по stop_id и sign_deleted */
                       sl.stop_id = coalesce(l_stop_id, sl.stop_id)
                       and (not sl.sign_deleted or sl.sign_deleted = coalesce(lf_sign_deleted, false));
end;
$$;

