-- Table: asupbk.trip

-- DROP TABLE asupbk.trip;

CREATE TABLE asupbk.trip
(
  trip_id text NOT NULL, -- Уникальный идентификатор, присвоенный фактическому рейсу
  day_date timestamp without time zone NOT NULL,
  route_code text NOT NULL, -- Код маршрута из ГИС, используется код ЭРМ
  out_code text NOT NULL, -- 3-значнй код выхода на маршрут
  garage_number text NOT NULL,
  table_number text NOT NULL,
  start_date timestamp without time zone NOT NULL, -- Время начала рейса
  end_date timestamp without time zone NOT NULL, -- Время окончания рейса
  type text NOT NULL, -- Возможные значения: «нулевой», «производственный», «маневровый», «обеденный», «технологический»
  fixation text NOT NULL, -- Возможные значения: «автоматически», «вручную».
  conformation text NOT NULL, -- Возможные значения: «расписание», «указание диспетчера».
  CONSTRAINT pk_trip_id PRIMARY KEY (trip_id)
  USING INDEX TABLESPACE asupbk_idx
)
WITH (
OIDS=FALSE
)
TABLESPACE asupbk_data;

COMMENT ON COLUMN asupbk.trip.trip_id IS 'Уникальный идентификатор, присвоенный фактическому рейсу';
COMMENT ON COLUMN asupbk.trip.route_code IS 'Код маршрута из ГИС, используется код ЭРМ ';
COMMENT ON COLUMN asupbk.trip.out_code IS '3-значнй код выхода на маршрут';
COMMENT ON COLUMN asupbk.trip.start_date IS 'Время начала рейса';
COMMENT ON COLUMN asupbk.trip.end_date IS 'Время окончания рейса';
COMMENT ON COLUMN asupbk.trip.type IS 'Возможные значения: «нулевой», «производственный», «маневровый», «обеденный», «технологический»';
COMMENT ON COLUMN asupbk.trip.fixation IS 'Возможные значения: «автоматически», «вручную».';
COMMENT ON COLUMN asupbk.trip.conformation IS 'Возможные значения: «расписание», «указание диспетчера».';
COMMENT ON COLUMN asupbk.trip.garage_number IS 'Гаражный номер';
COMMENT ON COLUMN asupbk.trip.table_number IS 'Табельный номер';