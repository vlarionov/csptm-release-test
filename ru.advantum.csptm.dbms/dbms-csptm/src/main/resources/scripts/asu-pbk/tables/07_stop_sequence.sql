-- Table: asupbk.stop_sequence

-- DROP TABLE asupbk.stop_sequence;

CREATE TABLE asupbk.stop_sequence
(
  transmission_date date NOT NULL,
  mr_code bigint NOT NULL, -- Номер маршрута
  depot_number bigint NOT NULL, -- гаражный номер
  shift_number smallint NOT NULL, -- номер смены
  route_number smallint NOT NULL, -- порядковый номер рейса
  trip_id text NOT NULL, -- Уникальный идентификатор присвоенный фактическому рейсу
  order_number smallint NOT NULL, -- Порядковый номер остановки в рейсе
  stop_id bigint NOT NULL, -- Уникальный идентификатор, присвоенный остановочному пункту
  erm_id bigint NOT NULL, -- Поле CP_RegNum таблицы CntrlPoints базы данных Transnavi
  arrival_time timestamp without time zone, -- Время фактического прохождения остановочного пункта
  graphic_id bigint NOT NULL, -- Номер режима работы транспортного средства
  stop_check_id serial NOT NULL,
  CONSTRAINT pk_stop_check PRIMARY KEY (stop_check_id)
  USING INDEX TABLESPACE asupbk_idx
)
WITH (
OIDS=FALSE
)
TABLESPACE asupbk_data;

COMMENT ON COLUMN asupbk.stop_sequence.mr_code IS 'Номер маршрута';
COMMENT ON COLUMN asupbk.stop_sequence.depot_number IS 'гаражный номер';
COMMENT ON COLUMN asupbk.stop_sequence.shift_number IS 'номер смены';
COMMENT ON COLUMN asupbk.stop_sequence.route_number IS 'порядковый номер рейса';
COMMENT ON COLUMN asupbk.stop_sequence.trip_id IS 'Уникальный идентификатор присвоенный фактическому рейсу';
COMMENT ON COLUMN asupbk.stop_sequence.order_number IS 'Порядковый номер остановки в рейсе';
COMMENT ON COLUMN asupbk.stop_sequence.stop_id IS 'Уникальный идентификатор, присвоенный остановочному пункту';
COMMENT ON COLUMN asupbk.stop_sequence.erm_id IS 'Поле CP_RegNum таблицы CntrlPoints базы данных Transnavi';
COMMENT ON COLUMN asupbk.stop_sequence.arrival_time IS 'Время фактического прохождения остановочного пункта';
COMMENT ON COLUMN asupbk.stop_sequence.graphic_id IS 'Номер режима работы транспортного средства';
COMMENT ON COLUMN asupbk.stop_sequence.transmission_date IS 'Дата передачи данных о времени прохождения ОП';


-- Index: asupbk.fk_stop_check_graphic

-- DROP INDEX asupbk.fk_stop_check_graphic;

CREATE INDEX fk_stop_check_graphic
  ON asupbk.stop_sequence
  USING btree
  (graphic_id)
TABLESPACE asupbk_idx;

-- Index: asupbk.fk_stop_check_trip

-- DROP INDEX asupbk.fk_stop_check_trip;

CREATE INDEX fk_stop_check_trip
  ON asupbk.stop_sequence
  USING btree
  (trip_id COLLATE pg_catalog."default")
TABLESPACE asupbk_idx;