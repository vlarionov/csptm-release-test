-- Table: asupbk.equipment

-- DROP TABLE asupbk.equipment;

CREATE TABLE asupbk.equipment
(
  equipment_id bigint NOT NULL,
  gos_num text NOT NULL,
  vehicle_id bigint NOT NULL, -- Гаражный номер
  type text NOT NULL,
  CONSTRAINT pk_equipment_id PRIMARY KEY (equipment_id)
  USING INDEX TABLESPACE asupbk_idx
)
WITH (
OIDS=FALSE
)
TABLESPACE asupbk_data;

COMMENT ON COLUMN asupbk.equipment.vehicle_id IS 'Гаражный номер';
COMMENT ON COLUMN asupbk.equipment.gos_num IS 'Гос. номер';
COMMENT ON COLUMN asupbk.equipment.equipment_id IS 'Идентификатор оборудования';
COMMENT ON COLUMN asupbk.equipment.type IS 'Тип ТС, возможные значения: Автобус, Трамвай, Троллейбус';