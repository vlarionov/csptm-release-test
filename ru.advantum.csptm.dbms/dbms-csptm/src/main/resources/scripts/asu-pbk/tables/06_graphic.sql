-- Table: asupbk.graphic

-- DROP TABLE asupbk.graphic;

CREATE TABLE asupbk.graphic
(
  graphic_id bigint NOT NULL,
  route_code text NOT NULL, -- Код маршрута из ГИС, используется код ЭРМ
  out_number text NOT NULL, -- 4х-значный код выхода
  capacity text NOT NULL, -- требуемая вместимость (например: ОБВ, БВ, МВ)
  days_of_week bytea NOT NULL, -- представленные в виде последовательности 7 битов (bit0 - понедельник, bit1 - вторник и т.д., значение бита: 1 = действует; 0 = не действует)
  start_date timestamp without time zone NOT NULL, -- дата ввода в действия варианта расписания
  end_date timestamp without time zone NOT NULL, -- дата окончания действия варианта расписания
  CONSTRAINT pk_graphic_id PRIMARY KEY (graphic_id)
  USING INDEX TABLESPACE asupbk_idx
)
WITH (
OIDS=FALSE
)
TABLESPACE asupbk_data;

COMMENT ON COLUMN asupbk.graphic.route_code IS 'Код маршрута из ГИС, используется код ЭРМ ';
COMMENT ON COLUMN asupbk.graphic.out_number IS '4х-значный код выхода';
COMMENT ON COLUMN asupbk.graphic.capacity IS 'требуемая вместимость (например: ОБВ, БВ, МВ)';
COMMENT ON COLUMN asupbk.graphic.days_of_week IS 'представленные в виде последовательности 7 битов (bit0 - понедельник, bit1 - вторник и т.д., значение бита: 1 = действует; 0 = не действует)';
COMMENT ON COLUMN asupbk.graphic.start_date IS 'дата ввода в действия варианта расписания';
COMMENT ON COLUMN asupbk.graphic.end_date IS 'дата окончания действия варианта расписания';