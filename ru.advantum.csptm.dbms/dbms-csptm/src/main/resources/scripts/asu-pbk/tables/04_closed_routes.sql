-- Table: asupbk.closed_routes

-- DROP TABLE asupbk.closed_routes;

CREATE TABLE asupbk.closed_routes
(
  closed_route_id serial NOT NULL,
  route_id text NOT NULL,
  date_end date NOT NULL DEFAULT ('now'::text)::date,
  date_begin date NOT NULL DEFAULT ('now'::text)::date,
  route_number text NOT NULL,
  CONSTRAINT pk_closed_routes PRIMARY KEY (closed_route_id)
  USING INDEX TABLESPACE asupbk_idx
)
WITH (
OIDS=FALSE
)
TABLESPACE asupbk_data;

COMMENT ON COLUMN asupbk.closed_routes.closed_route_id IS 'Идентификатор записи закрытого маршрута';
COMMENT ON COLUMN asupbk.closed_routes.route_id IS 'Идентификатор закрытого маршрута';
COMMENT ON COLUMN asupbk.closed_routes.date_end IS 'Дата закрытия маршрута';
COMMENT ON COLUMN asupbk.closed_routes.date_begin IS 'Дата начала маршрута';
COMMENT ON COLUMN asupbk.closed_routes.route_number IS 'Номер маршрута';