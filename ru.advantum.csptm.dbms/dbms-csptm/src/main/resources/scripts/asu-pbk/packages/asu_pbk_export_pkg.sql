-- ========================================================================
-- Пакет выгрузки данных из АСУ ПБК
-- ========================================================================

-- ========================================================================
-- Экспорт данных об оборудовании
-- ========================================================================
CREATE OR REPLACE FUNCTION asupbk.asu_pbk_export_pkg$export_equipment()
  RETURNS TABLE(
    equipment_id TEXT,
    vehicle_id   BIGINT,
    type         TEXT,
    gos_num      TEXT
  )
AS $$
DECLARE
  l_now TIMESTAMP WITH TIME ZONE := now();
BEGIN
  RETURN QUERY
  SELECT
    u.unit_num               equipment_id,
    ctr.garage_num :: BIGINT vehicle_id,
    licence                  gos_num,
    tt.name                  "type"
  FROM core.unit2tr u2t
    JOIN core.unit u ON u2t.unit_id = u.unit_id
    JOIN core.tr ctr ON u2t.tr_id = ctr.tr_id
    JOIN core.tr_type tt ON ctr.tr_type_id = tt.tr_type_id
  WHERE u2t.sys_period @> l_now;
END;
$$ LANGUAGE plpgsql;

-- ========================================================================
-- Экспорт данных о транспортной работе
-- ========================================================================
CREATE OR REPLACE FUNCTION asupbk.asu_pbk_export_pkg$export_transport_work(v_date_begin IN DATE, v_route_number IN TEXT)
  RETURNS SETOF asupbk.TRANSPORT_WORK
AS $$
BEGIN
  RETURN QUERY
  SELECT *
  FROM asupbk.transport_work
  WHERE date_begin = v_date_begin
        AND route_number = v_route_number;
END;
$$ LANGUAGE plpgsql;

-- ========================================================================
-- Экспорт данных о телематике за день
-- ========================================================================
CREATE OR REPLACE FUNCTION asupbk.asu_pbk_export_pkg$export_telematic(p_day DATE, OUT p_result REFCURSOR)
  RETURNS REFCURSOR
AS $$
DECLARE
  l_next_day DATE := p_day + '1 DAY' :: INTERVAL;
BEGIN
  OPEN p_result FOR
  SELECT
    t.event_time datetime,
    u.unit_num   equipment_id,
    t.lat,
    t.lon
  FROM core.traffic t
    JOIN core.unit u ON t.unit_id = u.unit_id
  WHERE t.event_time >= p_day AND t.event_time < l_next_day
        AND t.lat IS NOT NULL AND t.lon IS NOT NULL
  ;
END;
$$ LANGUAGE plpgsql;

-- ========================================================================
-- Экспорт данных об оборудовании
-- ========================================================================
CREATE OR REPLACE FUNCTION asupbk.asu_pbk_export_pkg$export_stop_sequence(v_date IN DATE)
  RETURNS SETOF asupbk.STOP_SEQUENCE
AS $$
BEGIN
  RETURN QUERY
  SELECT *
  FROM asupbk.stop_sequence
  WHERE transmission_date = v_date;
END;
$$ LANGUAGE plpgsql;

-- ========================================================================
-- Экспорт данных о закрытом оборудовании
-- ========================================================================
CREATE OR REPLACE FUNCTION asupbk.asu_pbk_export_pkg$export_closed_routes(v_date_end IN DATE)
  RETURNS SETOF asupbk.CLOSED_ROUTES
AS $$
BEGIN
  RETURN QUERY
  SELECT *
  FROM asupbk.closed_routes
  WHERE date_end = v_date_end;
END;
$$ LANGUAGE plpgsql;