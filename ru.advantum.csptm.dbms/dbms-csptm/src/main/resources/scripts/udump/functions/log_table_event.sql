
--DROP FUNCTION udump.log_table_event();

create or replace function udump.log_table_event()
	returns integer
as $id$ declare
	v_id integer;
	begin
		insert into udump.events(event_name, table_name)
		values (lower(TG_OP), TG_TABLE_SCHEMA || '.' || TG_TABLE_NAME)
		returning id into v_id;

		return v_id;
	end;
$id$ language plpgsql;