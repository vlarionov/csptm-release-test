-- ========================================================================
-- Пакет функций универсальной выгрузки Udump
-- ========================================================================

create or replace function udump.udump_pkg$export_tasks()
  returns setof udump.tasks
  -- ========================================================================
  -- Экспорт данных о задачах на выгрузку по периоду
  -- ========================================================================
as $$
begin
  return query select *
               from udump.tasks
               where period is not null and period != 0;
end;
$$ language plpgsql;

create or replace function udump.udump_pkg$export_event_tasks()
returns table(sql text, url text, period smallint, id int)
  -- ========================================================================
  -- Экспорт данных о задачах на выгрузку по событиям
  -- ========================================================================
as $$
begin
  return query
  select ts.sql, ts.url, ts.period, es.id
    from udump.events as es
   inner join udump.event_task as te on es.table_name = te.table_name
                                    and es.event_name = te.event_name
   inner join udump.tasks as ts on te.task_id = ts.id
   where es.is_done = false;
end;
$$ language plpgsql;

create or replace function udump.udump_pkg$set_event_done
  ( v_id in integer )
  returns void
  -- ========================================================================
  -- Маркировка задачи по событию как выполненной
  -- ========================================================================
as $$
begin
  update udump.events set is_done=true where id = v_id;
end;
$$ language plpgsql;