-- Table: udump.event_task

-- DROP TABLE udump.event_task;

CREATE TABLE udump.event_task
(
  task_id integer NOT NULL,
  event_task_id serial NOT NULL,
  event_name text NOT NULL,
  table_name text NOT NULL,
  CONSTRAINT pk_event_task PRIMARY KEY (event_task_id)
)
WITH (
OIDS=FALSE
);

-- Index: udump.ix_task_id

-- DROP INDEX udump.ix_task_id;

CREATE INDEX ix_task_id
  ON udump.event_task
  USING btree
  (task_id);

COMMENT ON COLUMN udump.event_task.task_id IS 'идентификатор задачи';
COMMENT ON COLUMN udump.event_task.event_task_id IS 'идентификатор связи задача - событие';
COMMENT ON COLUMN udump.event_task.event_name IS 'имя события, update, delete, insert etc';
COMMENT ON COLUMN udump.event_task.table_name IS 'таблица в которой произошло событие';