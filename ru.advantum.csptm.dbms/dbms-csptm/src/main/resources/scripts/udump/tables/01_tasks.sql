-- Table: udump.tasks

-- DROP TABLE udump.tasks;

CREATE TABLE udump.tasks
(
  sql text NOT NULL,
  url text NOT NULL,
  period smallint,
  id serial NOT NULL,
  CONSTRAINT pk_tasks PRIMARY KEY (id)
)
WITH (
OIDS=FALSE
);

COMMENT ON COLUMN udump.tasks.sql IS 'текст sql запроса на выгрузку';
COMMENT ON COLUMN udump.tasks.url IS 'url для приемки результата выгрузки';
COMMENT ON COLUMN udump.tasks.period IS 'период выполнения задачи';
COMMENT ON COLUMN udump.tasks.id IS 'идентификатор задачи';