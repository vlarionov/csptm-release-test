-- Table: udump.events

-- DROP TABLE udump.events;

CREATE TABLE udump.events
(
  id serial NOT NULL,
  event_name text NOT NULL,
  table_name text NOT NULL,
  create_date timestamp without time zone NOT NULL DEFAULT now(),
  is_done boolean NOT NULL DEFAULT false,
  CONSTRAINT pk_events PRIMARY KEY (id)
)
WITH (
OIDS=FALSE
);

COMMENT ON COLUMN udump.events.id IS 'идентификатор события';
COMMENT ON COLUMN udump.events.event_name IS 'имя события, update, insert, delete etc';
COMMENT ON COLUMN udump.events.table_name IS 'имя таблицы в которой произошло событие';
COMMENT ON COLUMN udump.events.create_date IS 'дата создания события';
COMMENT ON COLUMN udump.events.is_done IS 'метка выполнения, true если событие успешно обработано, false если произошел сбой';