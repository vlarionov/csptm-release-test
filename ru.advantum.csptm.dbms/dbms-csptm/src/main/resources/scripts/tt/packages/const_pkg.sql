﻿-- ошибка
create or replace function tt.const_pkg$ord_status_error()
    returns smallint as 'select 0 :: smallint' language sql immutable;
-- пройден
create or replace function tt.const_pkg$ord_status_passed()
    returns smallint as 'select 1 :: smallint' language sql immutable;
-- запланирован
create or replace function tt.const_pkg$ord_status_planned()
    returns smallint as 'select 2 :: smallint' language sql immutable;
-- выполняется
create or replace function tt.const_pkg$ord_status_performed()
    returns smallint as 'select 3 :: smallint' language sql immutable;
-- пройден частично
create or replace function tt.const_pkg$ord_status_passed_part()
    returns smallint as 'select 4 :: smallint' language sql immutable;
-- отменен
create or replace function tt.const_pkg$ord_status_canceled()
    returns smallint as 'select 5 :: smallint' language sql immutable;
