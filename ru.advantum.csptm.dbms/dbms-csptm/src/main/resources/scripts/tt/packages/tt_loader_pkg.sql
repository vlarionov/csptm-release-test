CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$select_full_import_dates()
  RETURNS TABLE(import_date DATE)
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT fi.import_date
  FROM tt.full_import fi;
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$insert_park(p_tn_id        BIGINT, p_tn_instance TEXT, p_s_name TEXT,
                                                        p_sign_deleted SMALLINT, p_pk_regnum BIGINT,
                                                        p_dt_begin     TIMESTAMP WITHOUT TIME ZONE,
                                                        p_dt_end       TIMESTAMP WITHOUT TIME ZONE)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  INSERT INTO tt.Park (park_id, tn_id, tn_instance, s_name, sign_deleted, pk_regnum, dt_begin, dt_end)
    SELECT
      nextval('tt.park_id_seq' :: REGCLASS),
      p_tn_id,
      p_tn_instance,
      p_s_name,
      p_sign_deleted,
      p_pk_regnum,
      p_dt_begin,
      p_dt_end
    WHERE NOT EXISTS(SELECT 1
                     FROM tt.park p
                     WHERE p.tn_id = p_tn_id AND p.tn_instance = p_tn_instance);
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$insert_driver(p_driver_name        TEXT, p_driver_last_name TEXT,
                                                          p_driver_middle_name TEXT, p_tab_num TEXT,
                                                          p_tn_id              BIGINT, p_tn_instance TEXT,
                                                          p_dt_begin           TIMESTAMP WITHOUT TIME ZONE,
                                                          p_dt_end             TIMESTAMP WITHOUT TIME ZONE,
                                                          p_sign_delete        SMALLINT,
                                                          p_update_date        TIMESTAMP WITHOUT TIME ZONE)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  INSERT INTO tt.driver (
    driver_id,
    driver_name,
    driver_last_name,
    driver_middle_name,
    tab_num,
    tn_id,
    tn_instance,
    dt_begin,
    dt_end,
    sign_delete,
    update_date
  )
    SELECT
      nextval('tt.driver_id_seq' :: REGCLASS),
      p_driver_name,
      p_driver_last_name,
      p_driver_middle_name,
      p_tab_num,
      p_tn_id,
      p_tn_instance,
      p_dt_begin,
      p_dt_end,
      p_sign_delete,
      p_update_date
    WHERE NOT EXISTS(SELECT 1
                     FROM tt.driver d
                     WHERE d.tn_id = p_tn_id AND d.tn_instance = p_tn_instance);
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$delete_driver(p_tn_id BIGINT, p_tn_instance TEXT)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  UPDATE tt.driver
  SET sign_delete = 1
  WHERE tn_id = p_tn_id AND tn_instance = p_tn_instance;
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$insert_tr(p_tn_id       BIGINT, p_tn_instance TEXT, p_licence TEXT,
                                                      p_garage_num  TEXT, p_dt_begin TIMESTAMP WITHOUT TIME ZONE,
                                                      p_dt_end      TIMESTAMP WITHOUT TIME ZONE,
                                                      p_sign_delete SMALLINT,
                                                      p_dt_update   TIMESTAMP WITHOUT TIME ZONE,
                                                      p_park_tn_id  BIGINT)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  INSERT INTO tt.tr (
    tr_id,
    park_id,
    tn_id,
    tn_instance,
    licence,
    garage_num,
    dt_begin,
    dt_end,
    sign_delete,
    dt_update
  )
    SELECT
      nextval('tt.tr_id_seq' :: REGCLASS),
      p.park_id,
      p_tn_id,
      p_tn_instance,
      p_licence,
      p_garage_num,
      p_dt_begin,
      p_dt_end,
      p_sign_delete,
      p_dt_update
    FROM tt.park p
    WHERE p.tn_id = p_park_tn_id AND p.tn_instance = p_tn_instance
          AND NOT EXISTS(SELECT 1
                         FROM tt.tr t
                         WHERE t.tn_id = p_tn_id AND t.tn_instance = p_tn_instance);
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$delete_tr(p_tn_id BIGINT, p_tn_instance TEXT)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  UPDATE tt.tr
  SET sign_delete = 1
  WHERE tn_id = p_tn_id AND tn_instance = p_tn_instance;
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$insert_timetable(
  p_rmt_id               BIGINT,
  p_rmt_tt_id            BIGINT,
  p_timetable_num        TEXT,
  p_timetable_varr       TEXT,
  p_begin_date           TIMESTAMP WITHOUT TIME ZONE,
  p_end_date             TIMESTAMP WITHOUT TIME ZONE,
  p_sign_deleted         SMALLINT,
  p_dt_update            TIMESTAMP WITHOUT TIME ZONE,
  p_timetable_setting_id BIGINT
)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  INSERT INTO tt.timetable (
    timetable_id,
    rmt_id,
    rmt_tt_id,
    timetable_num,
    timetable_varr,
    begin_date,
    end_date,
    sign_deleted,
    dt_update,
    route_muid,
    timetable_setting_id
  )
    SELECT
      nextval('tt.timetable_id_seq' :: REGCLASS),
      p_rmt_id,
      p_rmt_tt_id,
      p_timetable_num,
      p_timetable_varr,
      p_begin_date,
      p_end_date,
      p_sign_deleted,
      p_dt_update,
      (SELECT r.muid
       FROM gis.routes r
       WHERE btrim(upper(r.number)) = btrim(upper(p_timetable_num))
             AND r.transport_kind_muid = p_rmt_tt_id
             AND (r.close_date IS NULL OR r.close_date :: DATE >= now() :: DATE)
             AND r.open_date :: DATE <= now() :: DATE
             AND r.sign_deleted = 0),
      p_timetable_setting_id
    WHERE NOT EXISTS(SELECT 1
                     FROM tt.timetable t
                     WHERE t.rmt_id = p_rmt_id);
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$insert_timetable_entry(p_rmt_id              BIGINT, p_dr_shift_id BIGINT,
                                                                   p_timetable_entry_num SMALLINT,
                                                                   p_sign_deleted        SMALLINT,
                                                                   p_dt_update           TIMESTAMP WITHOUT TIME ZONE)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN

  /*если изменился текущий вариант маршрута в ГИС, помечаем выход как удаленный*/
  UPDATE tt.timetable_entry tte
  SET sign_deleted = 1,
    dt_update      = now()
  WHERE
    EXISTS(SELECT 1
           FROM tt.timetable tt
             JOIN gis.routes r ON r.muid = tt.route_muid
             JOIN gis.route_variants rr ON rr.muid = r.current_route_variant_muid
           WHERE tt.timetable_id = tte.timetable_id AND tt.rmt_id = p_rmt_id
                 AND tt.sign_deleted = 0
                 AND rr.muid != tte.route_variant_muid
    )
    AND tte.timetable_entry_num = p_timetable_entry_num;


  INSERT INTO tt.timetable_entry (
    timetable_entry_id,
    timetable_id,
    dr_shift_id,
    timetable_entry_num,
    sign_deleted,
    dt_update,
    route_variant_muid
  )
    SELECT
      nextval('tt.timetable_entry_id_seq' :: REGCLASS),
      tt.timetable_id AS tt_id,
      p_dr_shift_id,
      p_timetable_entry_num,
      p_sign_deleted,
      p_dt_update,
      rr.muid
    FROM tt.timetable tt
      LEFT JOIN gis.routes r ON r.muid = tt.route_muid
      LEFT JOIN gis.route_variants rr ON rr.muid = r.current_route_variant_muid
    WHERE tt.rmt_id = p_rmt_id
          AND tt.sign_deleted = 0
          AND NOT EXISTS(SELECT 1
                         FROM tt.timetable_entry tte
                         WHERE tte.timetable_id = tt.timetable_id
                               AND tte.timetable_entry_num = p_timetable_entry_num
                               AND tte.sign_deleted = 0);
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$insert_order_list(p_tn_instance         TEXT, p_driver_tn_id BIGINT,
                                                              p_garage_num          TEXT, p_order_num BIGINT,
                                                              p_order_date          TIMESTAMP WITHOUT TIME ZONE,
                                                              p_shift               BIGINT, p_sign_deleted BIGINT,
                                                              p_dt_update           TIMESTAMP WITHOUT TIME ZONE,
                                                              p_is_active           BIGINT, p_rmt_id BIGINT,
                                                              p_timetable_entry_num SMALLINT)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  INSERT INTO tt.order_list (
    order_list_id,
    timetable_entry_id,
    driver_id,
    tr_id,
    order_num,
    order_date,
    shift,
    sign_deleted,
    dt_update,
    is_active,
    tn_instance,
    tr_garage_num
  )
    SELECT
      nextval('tt.order_list_id_seq' :: REGCLASS),
      tte.timetable_entry_id,
      d.driver_id,
      ctr.tr_id,
      p_order_num,
      p_order_date,
      p_shift,
      p_sign_deleted,
      p_dt_update,
      p_is_active,
      p_tn_instance,
      p_garage_num
    FROM tt.timetable tt
      JOIN tt.timetable_entry tte
        ON tte.timetable_id = tt.timetable_id AND tte.timetable_entry_num = p_timetable_entry_num
      LEFT JOIN tt.driver d ON d.tn_instance = p_tn_instance AND d.tn_id = p_driver_tn_id
      LEFT JOIN core.depo dep ON dep.tn_instance = p_tn_instance
      LEFT JOIN core.tr ctr ON p_garage_num :: INTEGER = ctr.garage_num AND dep.depo_id = ctr.depo_id
    WHERE tt.rmt_id = p_rmt_id AND tt.sign_deleted = 0 AND tte.sign_deleted = 0;
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$insert_round(p_tn_instance  TEXT, p_order_num BIGINT,
                                                         p_order_date   TIMESTAMP WITHOUT TIME ZONE,
                                                         p_direction_id BIGINT,
                                                         p_dt_update    TIMESTAMP WITHOUT TIME ZONE,
                                                         p_round_num    BIGINT, p_round_type TEXT, p_round_code TEXT,
                                                         p_sign_deleted SMALLINT)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  /*если изменилась текущая траектория в ГИС, помечаем рейс как удаленный*/
  UPDATE tt.round
  SET sign_deleted = 1,
    dt_update      = now()
  WHERE
    round_id IN (
      SELECT rd.round_id
      FROM tt.order_list ol
        JOIN tt.timetable_entry tte ON tte.timetable_entry_id = ol.timetable_entry_id
        JOIN gis.route_rounds rr ON rr.route_variant_muid = tte.route_variant_muid
        JOIN gis.route_trajectories rt ON rt.route_round_muid = rr.muid
        JOIN tt.round rd ON rd.timetable_entry_id = ol.timetable_entry_id
                            AND rd.tn_instance = p_tn_instance
                            AND rd.round_num = p_round_num
                            AND rd.round_type = p_round_type
      WHERE ol.tn_instance = p_tn_instance
            AND upper(btrim(rr.code)) = p_round_code
            /*AND ol.sign_deleted = 0
            AND rr.sign_deleted = 0
            AND rt.sign_deleted = 0
            AND tte.sign_deleted = 0*/
            AND (CASE WHEN (rt.trajectory_type_muid % 2 :: BIGINT) > 0
        THEN 1
                 ELSE 2 END) = p_direction_id
            AND ol.order_num = p_order_num
            AND ol.order_date = p_order_date
            AND rd.route_trajectory_muid != rt.muid
    );

  INSERT INTO tt.round (
    round_id,
    route_trajectory_muid,
    timetable_entry_id,
    direction_id,
    dt_update,
    round_num,
    round_type,
    round_code,
    tn_instance,
    sign_deleted
  )
    SELECT
      nextval('tt.round_id_seq' :: REGCLASS),
      rt.muid,
      ol.timetable_entry_id,
      p_direction_id,
      p_dt_update,
      p_round_num,
      p_round_type,
      p_round_code,
      p_tn_instance,
      p_sign_deleted
    FROM tt.order_list ol
      LEFT JOIN tt.timetable_entry tte ON tte.timetable_entry_id = ol.timetable_entry_id
      LEFT JOIN gis.route_rounds rr ON rr.route_variant_muid = tte.route_variant_muid
      LEFT JOIN gis.route_trajectories rt ON rt.route_round_muid = rr.muid
    WHERE ol.tn_instance = p_tn_instance
          AND ol.sign_deleted = 0
          AND upper(btrim(rr.code)) = p_round_code
          AND rr.sign_deleted = 0
          AND rt.sign_deleted = 0
          AND tte.sign_deleted = 0
          AND (CASE WHEN (rt.trajectory_type_muid % 2 :: BIGINT) > 0
      THEN 1
               ELSE 2 END) = p_direction_id
          AND ol.order_num = p_order_num
          AND ol.order_date = p_order_date
          AND NOT EXISTS(SELECT 1
                         FROM tt.round rd
                         WHERE rd.timetable_entry_id = ol.timetable_entry_id
                               AND rd.tn_instance = p_tn_instance
                               AND rd.round_num = p_round_num
                               AND rd.round_type = p_round_type
                               AND rd.sign_deleted = 0);

END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$insert_timetable_plan(
  p_stop_erm_id         BIGINT,
  p_time_begin_epoch    DOUBLE PRECISION,
  p_time_end_epoch      DOUBLE PRECISION,
  p_shift               BIGINT,
  p_dt_update           TIMESTAMP,
  p_rmt_id              BIGINT,
  p_round_code          TEXT,
  p_direction_id        BIGINT,
  p_timetable_entry_num SMALLINT
)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  INSERT INTO tt.timetable_plan (
    timetable_plan_id,
    stop_place_muid,
    round_id,
    stop_erm_id,
    time_begin,
    time_end,
    sign_deleted,
    shift,
    dt_update
  )
    SELECT
      nextval('tt.timetable_plan_id_seq' :: REGCLASS),
      NULL,
      rd.round_id,
      p_stop_erm_id,
      p_time_begin_epoch * INTERVAL '1 second',
      p_time_end_epoch * INTERVAL '1 second',
      0,
      p_shift,
      p_dt_update
    FROM tt.timetable tt
      JOIN tt.timetable_entry tte ON tte.timetable_id = tt.timetable_id
      JOIN tt.round rd ON rd.timetable_entry_id = tte.timetable_entry_id
    WHERE tt.rmt_id = p_rmt_id
          AND rtrim(rd.round_code) = p_round_code
          AND rd.direction_id = p_direction_id
          AND tte.timetable_entry_num = p_timetable_entry_num
          AND tte.sign_deleted = 0
          AND rd.sign_deleted = 0
          AND NOT exists(SELECT 1
                         FROM tt.timetable_plan ttp
                         WHERE ttp.round_id = rd.round_id AND ttp.stop_erm_id = p_stop_erm_id);
END;
$$;


CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$insert_order_round(p_tn_instance TEXT, p_order_num BIGINT,
                                                               p_round_num   BIGINT, p_order_round_num BIGINT,
                                                               p_is_active   BIGINT, p_timetable_entry_num SMALLINT)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  INSERT INTO tt.order_round (
    order_round_id,
    order_list_id,
    round_id,
    order_round_num,
    tn_instance,
    is_active
  ) SELECT
      nextval('tt.order_round_id_seq' :: REGCLASS) AS order_round_id,
      ol.order_list_id                             AS order_list_id,
      rd.round_id                                  AS round_id,
      p_order_round_num                            AS order_round_num,
      p_tn_instance                                AS tn_instance,
      p_is_active                                  AS is_active
    FROM tt.order_list ol
      JOIN tt.timetable_entry tte ON tte.timetable_entry_id = ol.timetable_entry_id
      JOIN tt.round rd ON rd.timetable_entry_id = tte.timetable_entry_id
    WHERE ol.tn_instance = p_tn_instance
          AND ol.order_num = p_order_num
          AND ol.sign_deleted = 0
          AND tte.timetable_entry_num = p_timetable_entry_num
          AND tte.sign_deleted = 0
          AND rd.tn_instance = p_tn_instance
          AND rd.round_num = p_round_num
          AND rd.sign_deleted = 0;
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$insert_asdu_order_oper(p_timetable_plan_id BIGINT, p_tn_instance TEXT,
                                                                   p_order_round_num   BIGINT, p_round_num BIGINT,
                                                                   p_round_type        TEXT, p_order_num BIGINT,
                                                                   p_time_plan         TIMESTAMP WITHOUT TIME ZONE,
                                                                   p_time_oper         TIMESTAMP WITHOUT TIME ZONE,
                                                                   p_sign_deleted      BIGINT,
                                                                   p_dt_update         TIMESTAMP WITHOUT TIME ZONE,
                                                                   p_is_active         BIGINT, p_stop_erm_id BIGINT,
                                                                   p_is_kp             SMALLINT,
                                                                   p_cnrd_orderby      BIGINT)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  INSERT INTO tt.order_oper (
    order_oper_id,
    order_round_id,
    timetable_plan_id,
    time_plan,
    time_oper,
    sign_deleted,
    dt_update,
    is_active,
    stop_erm_id,
    is_kp,
    stop_order_num,
    stop_place_muid,
    cnrd_orderby
  )
    SELECT
      nextval('tt.order_oper_id_seq' :: REGCLASS),
      ord.order_round_id,
      p_timetable_plan_id,
      p_time_plan,
      p_time_oper,
      p_sign_deleted,
      p_dt_update,
      p_is_active,
      p_stop_erm_id,
      p_is_kp,
      NULL,
      sp.muid,
      p_cnrd_orderby
    FROM tt.order_list ol
      JOIN tt.order_round ord ON ol.order_list_id = ord.order_list_id
      JOIN tt.round rd ON ord.round_id = rd.round_id
      LEFT JOIN gis.stop_places sp ON sp.erm_id = p_stop_erm_id
    WHERE ol.tn_instance = p_tn_instance
          AND ol.order_num = p_order_num
          AND ol.sign_deleted = 0
          AND ord.order_round_num = p_order_round_num
          AND rd.round_num = p_round_num
          AND rd.round_type = p_round_type
          AND rd.sign_deleted = 0;
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$select_order_round_ids_by_order_date(p_order_date TIMESTAMP)
  RETURNS TABLE(order_round_id BIGINT)
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT ord.order_round_id
  FROM tt.order_round ord
    JOIN tt.order_list ol ON ol.order_list_id = ord.order_list_id
  WHERE ol.order_date = p_order_date
        AND ol.sign_deleted = 0;
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$insert_full_import(p_import_date DATE)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  INSERT INTO tt.full_import (full_import_id, import_date)
  VALUES (nextval('tt.full_import_id_seq' :: REGCLASS), p_import_date);
END;
$$;


CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$insert_impact(p_order_date         TIMESTAMP WITHOUT TIME ZONE,
                                                          p_order_num          BIGINT, p_order_num_old BIGINT,
                                                          p_entry_num          SMALLINT,
                                                          p_begin_time_plan    TIMESTAMP WITHOUT TIME ZONE,
                                                          p_end_time_plan      TIMESTAMP WITHOUT TIME ZONE,
                                                          p_begin_cnrd_orderby BIGINT, p_end_cnrd_orderby BIGINT,
                                                          p_impact_id          BIGINT, p_tn_instance TEXT,
                                                          p_impact_import_id   INTEGER
)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  WITH changed_order_opers AS (
      SELECT
        oo.order_oper_id AS order_oper_id,
        ol.order_list_id AS order_list_id
      FROM tt.order_list ol
        JOIN tt.timetable_entry tte ON tte.timetable_entry_id = ol.timetable_entry_id
        JOIN tt.order_round ord ON ord.order_list_id = ol.order_list_id
        JOIN tt.order_oper oo ON oo.order_round_id = ord.order_round_id
      WHERE
        ol.tn_instance = p_tn_instance
        AND tte.timetable_entry_num = p_entry_num
        AND ol.order_date = p_order_date
        AND ol.order_num = p_order_num
        AND ol.sign_deleted = 0
        AND oo.sign_deleted = 0
        AND tte.sign_deleted = 0
        AND (
          (oo.cnrd_orderby >= p_begin_cnrd_orderby AND
           (p_end_cnrd_orderby IS NULL OR oo.cnrd_orderby < p_end_cnrd_orderby))
          OR ((p_begin_time_plan IS NULL OR oo.time_plan >= p_begin_time_plan) AND
              (p_end_time_plan IS NULL OR oo.time_plan < p_end_time_plan))
        )
  ),
      inserted_impact_oo AS (
      INSERT INTO tt.impact_import2order_oper (impact_import_id, order_oper_id)
        SELECT
          p_impact_import_id,
          order_oper_id
        FROM changed_order_opers
      RETURNING order_oper_id
    )
  UPDATE tt.order_oper oo
  SET sign_deleted = 1
  FROM inserted_impact_oo iioo
  WHERE oo.order_oper_id = iioo.order_oper_id;
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$insert_impacted_timetable_entry(p_rmt_id              BIGINT,
                                                                            p_timetable_entry_num SMALLINT,
                                                                            p_dt_update           TIMESTAMP WITHOUT TIME ZONE)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  INSERT INTO tt.timetable_entry (
    timetable_entry_id,
    timetable_id,
    dr_shift_id,
    timetable_entry_num,
    sign_deleted,
    dt_update,
    route_variant_muid
  )
    SELECT
      nextval('tt.timetable_entry_id_seq' :: REGCLASS),
      tt.timetable_id AS tt_id,
      NULL,
      p_timetable_entry_num,
      0,
      p_dt_update,
      rr.muid
    FROM tt.timetable tt
      LEFT JOIN gis.routes r ON r.muid = tt.route_muid
      LEFT JOIN gis.route_variants rr ON rr.muid = r.current_route_variant_muid
    WHERE tt.rmt_id = p_rmt_id AND tt.sign_deleted = 0
          AND NOT exists(
        SELECT 1
        FROM tt.timetable_entry x
        WHERE x.timetable_id = tt.timetable_id
              AND x.timetable_entry_num = p_timetable_entry_num
              AND x.sign_deleted = 0
    );
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$insert_impacted_order_list(p_tn_instance         TEXT,
                                                                       p_driver_tn_id        BIGINT,
                                                                       p_garage_num          TEXT, p_order_num BIGINT,
                                                                       p_order_date          TIMESTAMP WITHOUT TIME ZONE,
                                                                       p_shift               BIGINT,
                                                                       p_sign_deleted        BIGINT,
                                                                       p_dt_update           TIMESTAMP WITHOUT TIME ZONE,
                                                                       p_is_active           BIGINT, p_rmt_id BIGINT,
                                                                       p_timetable_entry_num SMALLINT)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  INSERT INTO tt.order_list (
    order_list_id,
    timetable_entry_id,
    driver_id,
    tr_id,
    order_num,
    order_date,
    shift,
    sign_deleted,
    dt_update,
    is_active,
    tn_instance,
    tr_garage_num
  )
    SELECT
      nextval('tt.order_list_id_seq' :: REGCLASS),
      tte.timetable_entry_id,
      d.driver_id,
      ctr.tr_id,
      p_order_num,
      p_order_date,
      p_shift,
      p_sign_deleted,
      p_dt_update,
      p_is_active,
      p_tn_instance,
      p_garage_num
    FROM tt.timetable tt
      JOIN tt.timetable_entry tte
        ON tte.timetable_id = tt.timetable_id AND tte.timetable_entry_num = p_timetable_entry_num
      LEFT JOIN tt.driver d ON d.tn_instance = p_tn_instance AND d.tn_id = p_driver_tn_id
      LEFT JOIN core.depo dep ON dep.tn_instance = p_tn_instance
      LEFT JOIN core.tr ctr ON p_garage_num :: INTEGER = ctr.garage_num AND dep.depo_id = ctr.depo_id
    WHERE tt.rmt_id = p_rmt_id
          AND tt.sign_deleted = 0
          AND tte.sign_deleted = 0
          AND NOT exists(SELECT 1
                         FROM tt.order_list ol
                         WHERE ol.order_date :: DATE = p_order_date :: DATE
                               AND ol.order_num = p_order_num
                               AND ol.tn_instance = p_tn_instance
                               AND ol.sign_deleted = 0);
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$insert_impacted_round(p_direction_id        BIGINT,
                                                                  p_dt_update           TIMESTAMP WITHOUT TIME ZONE,
                                                                  p_round_num           BIGINT, p_round_type TEXT,
                                                                  p_round_code          TEXT, p_tn_instance TEXT,
                                                                  p_rmt_id              BIGINT,
                                                                  p_timetable_entry_num SMALLINT)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN

  /*если изменилась текущая траектория в ГИС, помечаем рейс как удаленный*/
  UPDATE tt.round
  SET sign_deleted = 1,
    dt_update      = now()
  WHERE
    round_id IN (
      SELECT rd.round_id
      FROM tt.timetable tt
        JOIN tt.timetable_entry tte ON tte.timetable_id = tt.timetable_id
        JOIN gis.route_rounds rr ON rr.route_variant_muid = tte.route_variant_muid
        JOIN gis.route_trajectories rt ON rt.route_round_muid = rr.muid
        JOIN tt.round rd ON rd.round_num = p_round_num
                            AND rd.timetable_entry_id = tte.timetable_entry_id
                            AND rd.direction_id = p_direction_id
                            AND rd.tn_instance = p_tn_instance
      WHERE tte.timetable_entry_num = p_timetable_entry_num
            AND tt.rmt_id = p_rmt_id
            AND upper(btrim(rr.code)) = p_round_code
           /* AND rr.sign_deleted = 0
            AND rt.sign_deleted = 0
            AND tt.sign_deleted = 0
            AND tte.sign_deleted = 0*/
            AND rd.round_num = p_round_num
            AND rd.timetable_entry_id = tte.timetable_entry_id
            AND rd.direction_id = p_direction_id
            AND rd.tn_instance = p_tn_instance
            AND (CASE WHEN (rt.trajectory_type_muid % 2 :: BIGINT) > 0
        THEN 1
                 ELSE 2 END) = p_direction_id
            AND rd.route_trajectory_muid != rt.muid
    );

  INSERT INTO tt.round (round_id, route_trajectory_muid, timetable_entry_id, direction_id, dt_update, round_num, round_type, round_code, tn_instance, sign_deleted)
    SELECT
      nextval('tt.round_id_seq' :: REGCLASS),
      NULL,
      tte.timetable_entry_id,
      p_direction_id,
      p_dt_update,
      p_round_num,
      p_round_type,
      p_round_code,
      p_tn_instance,
      0
    FROM tt.timetable tt
      JOIN tt.timetable_entry tte ON tte.timetable_id = tt.timetable_id
      LEFT JOIN gis.route_rounds rr ON rr.route_variant_muid = tte.route_variant_muid
      LEFT JOIN gis.route_trajectories rt ON rt.route_round_muid = rr.muid
    WHERE tte.timetable_entry_num = p_timetable_entry_num
          AND tt.rmt_id = p_rmt_id
          AND tt.sign_deleted = 0
          AND upper(btrim(rr.code)) = p_round_code
          AND rr.sign_deleted = 0
          AND rt.sign_deleted = 0
          AND tte.sign_deleted = 0
          AND (CASE WHEN (rt.trajectory_type_muid % 2 :: BIGINT) > 0
      THEN 1
               ELSE 2 END) = p_direction_id
          AND NOT exists(SELECT 1
                         FROM tt.round rd
                         WHERE
                           rd.round_num = p_round_num
                           AND rd.timetable_entry_id = tte.timetable_entry_id
                           AND rd.direction_id = p_direction_id
                           AND rd.tn_instance = p_tn_instance
                           AND rd.sign_deleted = 0
    );
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$insert_impacted_order_round(
  p_order_round_num     BIGINT,
  p_tn_instance         TEXT,
  p_is_active           BIGINT,
  p_order_num           BIGINT,
  p_timetable_entry_num SMALLINT,
  p_round_num           BIGINT)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  INSERT INTO tt.order_round (order_round_id, order_list_id, round_id, order_round_num, tn_instance, is_active)
    SELECT
      nextval('tt.order_round_id_seq' :: REGCLASS),
      ol.order_list_id,
      rd.round_id,
      p_order_round_num,
      p_tn_instance,
      p_is_active
    FROM
      tt.order_list ol
      JOIN tt.timetable_entry tte ON tte.timetable_entry_id = ol.timetable_entry_id
      JOIN tt.round rd ON rd.timetable_entry_id = tte.timetable_entry_id
    WHERE ol.tn_instance = p_tn_instance
          AND ol.order_num = p_order_num
          AND ol.sign_deleted = 0
          AND rd.tn_instance = p_tn_instance
          AND rd.round_num = p_round_num
          AND rd.sign_deleted = 0
          AND tte.timetable_entry_num = p_timetable_entry_num
          AND NOT exists(SELECT 1
                         FROM tt.order_round ord
                         WHERE ord.order_list_id = ol.order_list_id
                               AND ord.order_round_num = p_order_round_num
                               AND ord.tn_instance = p_tn_instance
                               AND ord.is_active = 1
    );
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$insert_impacted_order_oper(
  p_time_plan        TIMESTAMP WITHOUT TIME ZONE,
  p_time_oper        TIMESTAMP WITHOUT TIME ZONE,
  p_dt_update        TIMESTAMP WITHOUT TIME ZONE,
  p_is_active        BIGINT,
  p_stop_erm_id      BIGINT,
  p_cnrd_orderby     BIGINT, p_tn_instance TEXT,
  p_order_num        BIGINT,
  p_order_round_num  BIGINT, p_round_num BIGINT,
  p_round_type       TEXT,
  p_impact_import_id INTEGER
)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE
  l_order_oper RECORD;
BEGIN
  FOR l_order_oper
  IN SELECT
       nextval('tt.order_oper_id_seq' :: REGCLASS) AS order_oper_id,
       ord.order_round_id                          AS order_round_id,
       (SELECT sp.muid
        FROM gis.stop_places sp
        WHERE sp.erm_id = p_stop_erm_id)           AS stop_place_muid
     FROM
       tt.order_list ol
       JOIN tt.order_round ord ON ol.order_list_id = ord.order_list_id
       JOIN tt.round rd ON ord.round_id = rd.round_id
     WHERE ol.tn_instance = p_tn_instance
           AND ol.order_num = p_order_num
           AND ol.sign_deleted = 0
           AND rd.round_type = p_round_type
           AND rd.round_num = p_round_num
           AND rd.tn_instance = p_tn_instance
           AND rd.sign_deleted = 0
           AND ord.order_round_num = p_order_round_num
           AND ord.is_active = 1
           AND NOT exists(SELECT 1
                          FROM tt.order_oper oo
                          WHERE oo.order_round_id = ord.order_round_id
                                AND oo.cnrd_orderby = p_cnrd_orderby
                                AND oo.sign_deleted = 0)
  LOOP
    INSERT INTO tt.order_oper (
      order_oper_id,
      order_round_id,
      timetable_plan_id,
      time_plan,
      time_oper,
      sign_deleted,
      dt_update,
      is_active,
      stop_erm_id,
      is_kp,
      stop_order_num,
      stop_place_muid,
      cnrd_orderby
    ) VALUES (
      l_order_oper.order_oper_id,
      l_order_oper.order_round_id,
      NULL,
      p_time_plan,
      p_time_oper,
      0,
      p_dt_update,
      p_is_active,
      p_stop_erm_id,
      1,
      NULL,
      l_order_oper.stop_place_muid,
      p_cnrd_orderby
    );
    INSERT INTO tt.impact_import2order_oper (impact_import_id, order_oper_id)
    VALUES (p_impact_import_id, l_order_oper.order_oper_id);
  END LOOP;
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$delete_duplicate_gis_order_oper(p_order_round_ids BIGINT [])
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  UPDATE tt.order_oper
  SET sign_deleted = 1
  WHERE order_oper_id IN (
    SELECT oo2.order_oper_id
    FROM tt.order_oper oo1
      JOIN tt.order_oper oo2 ON oo1.order_round_id = oo2.order_round_id
                                AND oo1.stop_place_muid = oo2.stop_place_muid
                                AND oo1.stop_order_num = oo2.stop_order_num
    WHERE oo1.order_round_id = ANY (p_order_round_ids)
          AND oo1.sign_deleted = 0 AND oo1.is_active = 1 AND oo1.is_kp = 1
          AND oo2.sign_deleted = 0 AND oo2.is_active = 1 AND oo2.is_kp = 0
  );
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$update_inactive_order_rounds(p_order_round_ids BIGINT [])
  RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE l_order_round RECORD;
BEGIN
  LOCK TABLE asd.oper_round_visit IN SHARE MODE;

  FOR l_order_round
  IN SELECT oo.order_round_id
     FROM tt.order_round ord
       JOIN tt.order_oper oo ON ord.order_round_id = oo.order_round_id
     WHERE ord.order_round_id = ANY (p_order_round_ids)
           AND ord.is_active = 1
           AND oo.is_kp = 1
     GROUP BY oo.order_round_id
     HAVING bool_and(oo.is_active = 0 OR oo.sign_deleted = 1)
  LOOP
    UPDATE tt.order_round
    SET is_active = 0,
        dt_update = now()
    WHERE order_round_id = l_order_round.order_round_id;

    UPDATE asd.oper_round_visit
    SET order_round_status_id = tt.const_pkg$ord_status_canceled()
    WHERE order_round_id = l_order_round.order_round_id;
  END LOOP;

END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$update_inactive_order_lists(p_order_round_ids BIGINT [])
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  UPDATE tt.order_list
  SET sign_deleted = 1
  WHERE
    sign_deleted = 0
    AND order_list_id IN (
      SELECT ord.order_list_id
      FROM tt.order_round ord
      WHERE ord.order_list_id IN (
        SELECT DISTINCT updated_ord.order_list_id
        FROM tt.order_round updated_ord
        WHERE updated_ord.order_round_id = ANY (p_order_round_ids)
      )
      GROUP BY ord.order_list_id
      HAVING bool_and(ord.is_active = 0)
    );
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$select_order_round_ids_by_impact_import_id(p_impact_import_id INTEGER)
  RETURNS TABLE(order_round_id BIGINT)
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT DISTINCT oo.order_round_id
  FROM tt.impact_import2order_oper io
    JOIN tt.order_oper oo ON oo.order_oper_id = io.order_oper_id
  WHERE io.impact_import_id = p_impact_import_id;
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$select_last_impact_import_end_date()
  RETURNS TIMESTAMP WITHOUT TIME ZONE
LANGUAGE plpgsql
AS $$
DECLARE
  result TIMESTAMP;
  rec    RECORD;
BEGIN
  FOR rec IN SELECT ii.end_date
             FROM tt.impact_import ii
             ORDER BY ii.end_date DESC
             LIMIT 1 LOOP
    result:=rec.end_date;
  END LOOP;
  RETURN result;
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$insert_impact_import(p_begin_date TIMESTAMP WITHOUT TIME ZONE,
                                                                 p_end_date   TIMESTAMP WITHOUT TIME ZONE)
  RETURNS TABLE(r_impact_import_id INTEGER)
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  INSERT INTO tt.impact_import (impact_import_id, begin_date, end_date)
  VALUES (nextval('tt.impact_import_id_seq' :: REGCLASS), p_begin_date, p_end_date)
  RETURNING impact_import_id AS r_impact_import_id;
END;
$$;


CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$update_oper_round_visit(p_order_round_ids BIGINT [])
  RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE
  rec RECORD;
BEGIN
  LOCK TABLE asd.oper_round_visit IN SHARE MODE;

  FOR rec
  IN SELECT
       ot.order_round_id,
       MIN(ot.time) AS time_plan_begin,
       MAX(ot.time) AS time_plan_end
     FROM (SELECT
             oo.order_round_id,
             coalesce(oo.time_oper, oo.time_plan) AS time
           FROM tt.order_oper oo
           WHERE oo.order_round_id = ANY (p_order_round_ids) AND oo.is_active = 1 AND oo.sign_deleted = 0
          ) ot
     GROUP BY ot.order_round_id
  LOOP
    IF rec.time_plan_begin IS NULL OR rec.time_plan_end IS NULL
    THEN CONTINUE;
    END IF;

    IF NOT EXISTS(
        SELECT 1
        FROM asd.oper_round_visit orv
        WHERE orv.order_round_id = rec.order_round_id
    )
    THEN
      INSERT
      INTO asd.oper_round_visit (order_round_id, time_plan_begin, time_plan_end)
      VALUES (rec.order_round_id, rec.time_plan_begin, rec.time_plan_end);
    ELSE
      UPDATE asd.oper_round_visit
      SET time_plan_begin = rec.time_plan_begin, time_plan_end = rec.time_plan_end
      WHERE order_round_id = rec.order_round_id;
    END IF;
  END LOOP;
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$insert_gis_order_oper(p_order_round_ids BIGINT [])
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  INSERT INTO tt.order_oper (order_oper_id, order_round_id, timetable_plan_id, time_plan, time_oper, sign_deleted, dt_update, is_active, stop_erm_id, is_kp, stop_order_num, stop_place_muid)
    SELECT
      nextval('tt.order_oper_id_seq' :: REGCLASS),
      ord.order_round_id,
      NULL,
      NULL,
      NULL,
      0,
      now(),
      1,
      sp.erm_id,
      0 AS is_kp,
      spt.order_num,
      spt.stop_place_muid
    FROM tt.order_round ord
      JOIN tt.round rd ON ord.round_id = rd.round_id
      JOIN tt.order_list ol ON ol.order_list_id = ord.order_list_id
      JOIN gis.stop_place2route_traj spt ON spt.route_trajectory_muid = rd.route_trajectory_muid
      JOIN gis.stop_places sp ON sp.muid = spt.stop_place_muid
    WHERE ord.order_round_id = ANY (p_order_round_ids)
          AND rd.sign_deleted = 0
          AND
          NOT exists(SELECT 1
                     FROM tt.order_oper oo
                     WHERE oo.order_round_id = ord.order_round_id
                           AND sp.erm_id = oo.stop_erm_id AND oo.sign_deleted = 0);
END;
$$;


CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$update_order_oper_stop_order_num(p_order_round_ids BIGINT [])
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  UPDATE tt.order_oper
  SET stop_order_num = t.order_num
  FROM (
         SELECT
           oo.order_oper_id,
           oo.time_plan,
           oo.time_oper,
           spt.order_num,
           dense_rank()
           OVER (
             PARTITION BY oo.order_round_id, oo.stop_erm_id
             ORDER BY oo.time_oper )  order_oper_rn,
           row_number()
           OVER (
             PARTITION BY oo.order_oper_id
             ORDER BY spt.order_num ) stop_traj_rn
         FROM tt.order_round ord
           JOIN tt.order_oper oo ON ord.order_round_id = oo.order_round_id
           JOIN tt.round rd ON rd.round_id = ord.round_id
           JOIN gis.stop_place2route_traj spt
             ON spt.route_trajectory_muid = rd.route_trajectory_muid AND spt.stop_place_muid = oo.stop_place_muid
         WHERE
           ord.order_round_id = ANY (p_order_round_ids)
           AND oo.is_kp = 1
           AND oo.sign_deleted = 0
           AND oo.is_active = 1
           AND rd.sign_deleted = 0
       ) t
  WHERE order_oper.order_oper_id = t.order_oper_id AND t.order_oper_rn = t.stop_traj_rn;
END;
$$;

CREATE OR REPLACE FUNCTION tt.tt_loader_pkg$update_order_oper_time_plan(p_order_round_ids BIGINT [])
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  UPDATE tt.order_oper
  SET time_plan = t.time_plan_int,
    time_oper   = t.time_oper_int
  FROM (SELECT
          order_oper_id,
          CASE
          WHEN t.next_plan_time != t.prev_plan_time AND
               next_dist != prev_dist -- дублируются иногда остановки, разбираемся
            THEN t.prev_plan_time + (dist - prev_dist) / (next_dist - prev_dist) * (t.next_plan_time - t.prev_plan_time)
          ELSE NULL
          END time_plan_int,
          CASE
          WHEN t.next_oper_time != t.prev_oper_time AND next_dist != prev_dist
            THEN t.prev_oper_time + (dist - prev_dist) / (next_dist - prev_dist) * (t.next_oper_time - t.prev_oper_time)
          ELSE NULL
          END time_oper_int
        FROM (SELECT
                t.*,
                max(CASE WHEN t.is_kp = 0
                  THEN NULL
                    ELSE dist END)
                OVER (
                  PARTITION BY t.order_round_id
                  ORDER BY t.stop_order_num )      prev_dist,
                min(CASE WHEN t.is_kp = 0
                  THEN NULL
                    ELSE dist END)
                OVER (
                  PARTITION BY t.order_round_id
                  ORDER BY t.stop_order_num DESC ) next_dist
              FROM (SELECT
                      oo.order_oper_id,
                      oo.order_round_id,
                      oo.is_kp,
                      oo.stop_order_num,
                      max(CASE WHEN oo.is_kp = 0
                        THEN NULL
                          ELSE oo.time_plan END)
                      OVER w_stop_order               prev_plan_time,
                      min(CASE WHEN oo.is_kp = 0
                        THEN NULL
                          ELSE oo.time_plan END)
                      OVER w_stop_order_d             next_plan_time,
                      max(CASE WHEN oo.is_kp = 0
                        THEN NULL
                          ELSE oo.time_oper END)
                      OVER w_stop_order               prev_oper_time,
                      min(CASE WHEN oo.is_kp = 0
                        THEN NULL
                          ELSE oo.time_oper END)
                      OVER w_stop_order_d             next_oper_time,
                      round(sum(sprt.length_sector)
                            OVER w_stop_order * 1000) dist
                    FROM tt.order_round ord
                      JOIN tt.round rnd ON ord.round_id = rnd.round_id
                      JOIN tt.order_oper oo ON ord.order_round_id = oo.order_round_id
                      JOIN gis.stop_place2route_traj sprt
                        ON sprt.route_trajectory_muid = rnd.route_trajectory_muid AND
                           oo.stop_order_num = sprt.order_num
                    --                       where ord.order_round_id = any(p_order_round_ids)
                    WHERE ord.order_round_id IN (SELECT unnest(p_order_round_ids))
                          AND oo.sign_deleted = 0
                          AND oo.is_active = 1
                          AND ord.is_active = 1
                          AND rnd.sign_deleted = 0
                    WINDOW w_stop_order AS (
                      PARTITION BY oo.order_round_id
                      ORDER BY oo.stop_order_num ),
                        w_stop_order_d AS (
                        PARTITION BY oo.order_round_id
                        ORDER BY oo.stop_order_num DESC )
                   ) t
             ) t
        WHERE t.is_kp = 0
       ) t
  WHERE order_oper.order_oper_id = t.order_oper_id;
END;
$$;
