﻿drop function tt.update_round();

create or replace function tt.update_round() returns text as
$body$
declare
    x_c cursor for
    select rd.round_id, t.muid
    from tt.round rd
        join tt.timetable_entry tte on tte.timetable_entry_id =rd.timetable_entry_id
        join gis.route_rounds rr on rr.route_variant_muid = tte.route_variant_muid and upper(btrim(rr.code))=rd.round_code
        join gis.route_trajectories t on t.route_round_muid = rr.muid and t.trajectory_type_muid = rd.direction_id
    where rr.sign_deleted = 0
      and t.sign_deleted = 0
      and rd.route_trajectory_muid is null;
begin
    for rec  in x_c loop
        update tt.round
        set route_trajectory_muid = rec.muid
        where round_id = rec.round_id;
    end loop;

    return 'ok';
end;
$body$
    language plpgsql volatile
    cost 100;

alter function tt.update_round() owner to adv;