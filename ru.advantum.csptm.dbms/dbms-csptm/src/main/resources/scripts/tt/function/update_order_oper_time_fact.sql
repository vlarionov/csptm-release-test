create or replace function tt.update_order_oper_time_fact
    (p_json in jsonb)
returns void as
$body$
    declare
        l_json_item jsonb;

        l_pass_time timestamp without time zone;
        l_stop_place_muid bigint;
        l_order_round_id bigint;
        l_stop_order_num bigint;
    begin
        for l_json_item in select jsonb_array_elements(p_json) loop
                l_pass_time := (l_json_item->>'passTime')::timestamp without time zone;
                l_stop_place_muid := (l_json_item->>'stopPlaceMuid')::bigint;
                l_order_round_id := (l_json_item->>'orderRoundId')::bigint;
                l_stop_order_num := (l_json_item->>'stopPlaceOrderNum')::bigint;


              insert into tt.order_fact(stop_place_muid, order_round_id, stop_order_num , time_fact, sign_deleted)
              values (l_stop_place_muid, l_order_round_id, l_stop_order_num, l_pass_time, 0)
              on conflict  ON CONSTRAINT  uniq_order_fact do update set
                time_fact = EXCLUDED.time_fact;

              if  l_stop_order_num = 1 and  l_pass_time is not null then
                  update asd.oper_round_visit
                  set order_round_status_id = tt.const_pkg$ord_status_performed()
                  where order_round_id = l_order_round_id
                        -- "начать" можно только запланированный рейс.
                        -- это обезопасит от случаев, когда из-за баги в graph-linker один и тот же рейс два раза прогоняли
                        and order_round_status_id = tt.const_pkg$ord_status_planned();
              end if;
        end loop;
    end;
$body$ language plpgsql;