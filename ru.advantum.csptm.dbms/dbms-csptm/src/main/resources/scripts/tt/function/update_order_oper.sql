﻿drop function tt.update_order_oper();

create or replace function tt.update_order_oper() returns text as
$body$
declare
    x_c cursor for
        select stop_place_muid, order_num, order_oper_id
        from (
            select spt.stop_place_muid, spt.order_num, oo.order_oper_id,
                   dense_rank() over (partition by oo.order_round_id, oo.stop_erm_id order by oo.time_oper) order_oper_rn,
                   row_number() over (partition by oo.order_oper_id order by spt.order_num) stop_traj_rn
            from tt.order_oper oo
                join tt.order_round ord on ord.order_round_id = oo.order_round_id
                join tt.round rd on rd.round_id = ord.round_id
                join gis.stop_place2route_traj spt on spt.route_trajectory_muid =rd.route_trajectory_muid
                join gis.stop_places sp on sp.muid = spt.stop_place_muid  and  sp.erm_id = oo.stop_erm_id
            where oo.stop_place_muid is null
        ) t
        where t.order_oper_rn = t.stop_traj_rn;
begin
    for rec in x_c loop
        update tt.order_oper
        set stop_order_num = rec.order_num,
            stop_place_muid = rec.stop_place_muid
        where order_oper_id= rec.order_oper_id;
    end loop;

    return 'ok';
end;
$body$
    language plpgsql volatile
    cost 100;

alter function tt.update_order_oper() owner to adv;