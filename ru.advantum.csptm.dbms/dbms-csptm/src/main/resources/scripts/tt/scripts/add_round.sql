insert into tt.round(round_id,  route_trajectory_muid, timetable_entry_id, direction_id, dt_update , round_num, round_type, round_code , tn_instance)
select nextval('tt.round_id_seq'::regclass), null, 
ol.timetable_entry_id, (ascii(cn.dstn_type) + 1) % 2 + 1 , now(), cn.dstn_identificator, cn.dstn_type, upper(btrim(race.title)),  cn.tn_instance

FROM tf.tbcardnariad cn
JOIN tf.route r ON r.tn_instance = cn.tn_instance AND r.tn_id = cn.mr_id AND cn.tn_instance = r.tn_instance AND r.is_deleted = false
     JOIN sym.tbmarsh m ON m.marshid = r.mr_code
     JOIN sym.tbmarshvariant mv ON mv.marshid = m.marshid AND mv.deleted = 0 AND mv.isworking
     JOIN sym.tbrace race ON race.mvid = mv.mvid
     JOIN sym.tbracebydir rr ON rr.raceid = race.raceid AND rr.racedir = cn.dstn_type
     JOIN tf.tn_order_date ON tn_order_date.tn_id = cn.nd_identificator AND tn_order_date.tn_instance = cn.tn_instance
     JOIN tf.order_date ON order_date.id_order_date = tn_order_date.id_order_date

    join tt.order_list ol on ol.tn_instance = cn.tn_instance and ol.order_num = cn.nrd_identificator and order_date.dt = ol.order_date 
    
where cn.nd_identificator = 6255 
--and cn.nrd_identificator =  62550245 and cn.tn_instance = '0003' 
and cn.dstn_identificator>0
group by ol.timetable_entry_id, cn.dstn_type, cn.dstn_identificator, cn.dstn_type, upper(btrim(race.title)),  cn.tn_instance;


select tt.update_round() ;

--select * from tt.round where tn_instance = '0003'
/*
-- DROP FUNCTION tt.update_round();

CREATE OR REPLACE FUNCTION tt.update_round()
  RETURNS text AS
$BODY$
declare
  x_c cursor for

select rd.round_id, t.muid
from tt.round rd
join tt.timetable_entry tte on tte.timetable_entry_id =rd.timetable_entry_id
join ptt.route_rounds rr ON rr.route_variant_muid = tte.route_variant_muid and upper(btrim(rr.code))=rd.round_code 
JOIN ptt.route_trajectories t ON t.route_round_muid = rr.muid
WHERE rr.sign_deleted = 0 AND t.sign_deleted = 0
and  CASE  WHEN (t.trajectory_type_muid % 2::bigint) > 0 THEN 0  ELSE 1  END =     rd.direction_id
;

begin 
for rec  in x_c
loop
  update tt.round  
  set route_trajectory_muid = rec.muid
  where round_id= rec.round_id;
end loop;

end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION tt.update_round()
  OWNER TO adv;


select tt.update_round() 
*/