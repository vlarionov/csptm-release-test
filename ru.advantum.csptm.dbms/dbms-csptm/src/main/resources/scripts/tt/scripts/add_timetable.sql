insert into tt.timetable( timetable_id, rmt_id, timetable_num, timetable_varr, begin_date , end_date , sign_deleted , dt_update, route_muid)
select  nextval('tt.timetable_id_seq'::regclass) , mv.mv_identificator, mv.mv_marsh, mv.varr, mv.mv_includedate, mv.mv_excludedate,0,now(),r.muid
from
 sym.marshvariants mv 
     jOIN ptt.routes r ON btrim(upper(r.number)) = btrim(upper(mv.mv_marsh)) AND r.transport_kind_muid = mv.tt_identificator 
             AND (r.close_date IS NULL OR r.close_date::date >= now()::date) AND r.open_date::date <= now()::date AND r.sign_deleted = 0
where (mv.mv_excludedate IS NULL OR mv.mv_excludedate::date >= now()::date) 
                              AND mv.mv_includedate::date <= now()::date 
                              AND mv.varr IS NOT NULL AND mv.pr_deystv = 1


