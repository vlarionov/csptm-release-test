insert into  tt.driver(driver_id, driver_name, driver_last_name, driver_middle_name, tab_num, tn_id, tn_instance , dt_begin, dt_end, sign_delete, update_date )
select nextval('tt.driver_id_seq'), d.first_name, d.surname, d.second_name, d.tab_num, d.tn_id, d.tn_instance, d.dt_begin, d.dt_end, 0 , now() 
from tf.driver d
where d.is_deleted = 0