﻿--1. Выходы**********************************
insert into tt.timetable_entry(timetable_entry_id, timetable_id, dr_shift_id, timetable_entry_num, sign_deleted, dt_update, route_variant_muid )
with bm as(
select
       bm.nd_identificator
     , bm.nrd_identificator as order_num
     , bm.nrd_identificator_old as order_num_old
     , bm.nrd_grafic as entry_num
     , bm.mngm_begintime, bm.mngm_endtime, bm.mngm_beginorder, bm.mngm_endorder
     , bm.mngm_identificator, bm.tn_instance, bm.nrd_smena
from  tf.tbbasemanagment bm
 WHERE bm.mngm_includedate >= '2017-04-03 12:05:00' and bm.mngm_includedate < '2017-04-03 12:10:00'
and nd_identificator =6292
group by   bm.nd_identificator
     , bm.nrd_identificator 
     , bm.nrd_identificator_old 
     , bm.nrd_grafic 
     , bm.mngm_begintime, bm.mngm_endtime, bm.mngm_beginorder, bm.mngm_endorder
     , bm.mngm_identificator, bm.tn_instance, bm.nrd_smena)

 select nextval('tt.timetable_entry_id_seq'::regclass),
 tt.timetable_id,null,  bm.nrd_grafic, 0, now(), null
     FROM  bm
     join (select  max(cn.mrsh_identificator) as mrsh_identificator, cn.tn_instance,cn.nd_identificator ,cn.nrd_identificator,cn.CNRD_Smena
           from  tf.tbcardnariad cn
           where cn.nd_identificator =6292
           group by cn.tn_instance,cn.nd_identificator ,cn.nrd_identificator,cn.CNRD_Smena) cn on cn.tn_instance= bm.tn_instance and cn.nd_identificator =bm.nd_identificator
                                                                                              and cn.nrd_identificator = bm.order_num   and cn.CNRD_Smena = bm.NRD_Smena
     JOIN tf.route_var_graph vg ON vg.tn_id = cn.mrsh_identificator AND vg.tn_instance = bm.tn_instance
     JOIN tf.route_var rv ON rv.id_route_var = vg.id_route_var AND rv.tn_instance = bm.tn_instance AND NOT rv.is_deleted
     join tt.timetable tt on tt.rmt_id= rv.n_num and tt.sign_deleted =0

where
 not exists (select 1
from  tt.timetable_entry x
where  x.timetable_id = tt.timetable_id
and x.timetable_entry_num = bm.nrd_grafic
and x.sign_deleted = 0)
group by  tt.timetable_id, bm.nrd_grafic
order by tt.timetable_id;


--!!!!!!!!!!!!!!!!!!!!!!! Наряды !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--добавляем если появились
insert into  tt.order_list( order_list_id , timetable_entry_id, driver_id, tr_id , order_num , order_date , shift , sign_deleted , dt_update , is_active, tn_instance,  tr_garage_num )

with bm as(
select
       bm.nd_identificator
     , bm.nrd_identificator 
     , bm.nrd_identificator_old as order_num_old
     , bm.nrd_grafic as entry_num
     , bm.mngm_begintime, bm.mngm_endtime, bm.mngm_beginorder, bm.mngm_endorder
     , bm.mngm_identificator, bm.tn_instance, bm.nrd_smena
from  tf.tbbasemanagment bm
 WHERE bm.mngm_includedate >= '2017-04-28 12:05:00' and bm.mngm_includedate < '2017-04-28 14:20:00'
and nd_identificator =6327
and mngm_identificator in (5,6,7,8, 107,128)
/*and trim(bm.mngm_title) in (
'Изм. наряда',
'Новый борт',
'Новый водитель',
'Доп. График',
'Снять с графика',
'Восстан.выход')*/
group by   bm.nd_identificator
     , bm.nrd_identificator 
     , bm.nrd_identificator_old 
     , bm.nrd_grafic 
     , bm.mngm_begintime, bm.mngm_endtime, bm.mngm_beginorder, bm.mngm_endorder
     , bm.mngm_identificator, bm.tn_instance, bm.nrd_smena)

 select nextval('tt.order_list_id_seq'::regclass),  
       tte.timetable_entry_id,  
       (select driver_id from tt.driver dr where dr.tn_instance = bn.tn_instance and dr.tn_id = bn.nsid_uniqueid) as driver_id,
      
       (select tr_id from tt.tr tr where tr.tn_instance = bn.tn_instance and tr.tn_id = bn.nsit_uniqueid) as tr_id, 
        bn.nrd_identificator, order_date.dt, bn.nrd_smena::bigint, 0 , now(), 1 , bn.tn_instance,
       (select tr.garage_num from tt.tr tr where tr.tn_instance = bn.tn_instance and tr.tn_id = bn.nsit_uniqueid) as  tr_garage_num

from bm
join tf.tbbasenariad bn on bn.nd_identificator = bm.nd_identificator and bn.nrd_identificator  = bm.nrd_identificator and bn.tn_instance = bm.tn_instance
JOIN tf.route_var_graph vg ON vg.tn_id = bn.mrsh_identificator AND vg.tn_instance = bn.tn_instance
JOIN tf.route_var rv ON rv.id_route_var = vg.id_route_var AND rv.tn_instance = bn.tn_instance AND NOT rv.is_deleted

join tt.timetable tt on tt.rmt_id= rv.n_num and tt.sign_deleted =0 
     join tt.timetable_entry tte on tte.timetable_id = tt.timetable_id and tte.timetable_entry_num= bn.nrd_grafic
     JOIN tf.tn_order_date ON tn_order_date.tn_id = bn.nd_identificator AND tn_order_date.tn_instance = bn.tn_instance
     JOIN tf.order_date ON order_date.id_order_date = tn_order_date.id_order_date
where bn.nd_identificator = 6256
and not exists (select 1 from tt.order_list ol where ol.order_date  = order_date.dt   and ol.order_num = bn.nrd_identificator and ol.tn_instance = bn.tn_instance and ol.sign_deleted=0);





--2.*****Рейсы*************************************
insert into tt.round(round_id,  route_trajectory_muid, timetable_entry_id, direction_id, dt_update , round_num, round_type, round_code , tn_instance,sign_deleted)
with bm as(
select
       bm.nd_identificator
     , bm.nrd_identificator as order_num
     , bm.nrd_identificator_old as order_num_old
     , bm.nrd_grafic as entry_num
     , bm.mngm_begintime, bm.mngm_endtime, bm.mngm_beginorder, bm.mngm_endorder
     , bm.mngm_identificator, bm.tn_instance, bm.nrd_smena
from  tf.tbbasemanagment bm
 WHERE bm.mngm_includedate >= '2017-04-03 12:05:00' and bm.mngm_includedate < '2017-04-03 12:10:00'
and nd_identificator =6292
group by   bm.nd_identificator
     , bm.nrd_identificator 
     , bm.nrd_identificator_old 
     , bm.nrd_grafic 
     , bm.mngm_begintime, bm.mngm_endtime, bm.mngm_beginorder, bm.mngm_endorder
     , bm.mngm_identificator, bm.tn_instance, bm.nrd_smena)

select nextval('tt.round_id_seq'::regclass), null,
     tte.timetable_entry_id, (ascii(cn.dstn_type) + 1) % 2 + 1 , now(), cn.dstn_identificator, cn.dstn_type, null::text as round_code,  bm.tn_instance , 0
     FROM bm
     join (select  max(cn.mrsh_identificator) as mrsh_identificator, max(cn.dstn_type) as dstn_type, max(cn.dstn_identificator) as dstn_identificator, cn.tn_instance,cn.nd_identificator ,cn.nrd_identificator,cn.CNRD_Smena
           from  tf.tbcardnariad cn
           group by cn.tn_instance,cn.nd_identificator ,cn.nrd_identificator,cn.CNRD_Smena) cn on cn.tn_instance= bm.tn_instance and cn.nd_identificator =bm.nd_identificator and cn.nrd_identificator = bm.nrd_identificator   and cn.CNRD_Smena = bm.NRD_Smena
     JOIN tf.route_var_graph vg ON vg.tn_id = cn.mrsh_identificator AND vg.tn_instance = bm.tn_instance
     JOIN tf.route_var rv ON rv.id_route_var = vg.id_route_var AND rv.tn_instance = bm.tn_instance AND NOT rv.is_deleted
     join tt.timetable tt on tt.rmt_id= rv.n_num and tt.sign_deleted =0
     join tt.timetable_entry tte on tte.timetable_id = tt.timetable_id and bm.nrd_grafic = tte.timetable_entry_num
where not exists (select 1
                from  tt.round rd
                where rd.round_num= cn.dstn_identificator and rd.timetable_entry_id = tte.timetable_entry_id and rd.direction_id = (ascii(cn.dstn_type) + 1) % 2 + 1 and rd.tn_instance =bm.tn_instance)
;

--3.*********************** Рейсы наряда*******************************************
insert into  tt.order_round(order_round_id, order_list_id, round_id, order_round_num, tn_instance, Is_active)
with bm as(
select
       bm.nd_identificator
     , bm.nrd_identificator as order_num
     , bm.nrd_identificator_old as order_num_old
     , bm.nrd_grafic as entry_num
     , bm.mngm_begintime, bm.mngm_endtime, bm.mngm_beginorder, bm.mngm_endorder
     , bm.mngm_identificator, bm.tn_instance, bm.nrd_smena
from  tf.tbbasemanagment bm
 WHERE bm.mngm_includedate >= '2017-04-03 12:05:00' and bm.mngm_includedate < '2017-04-03 12:10:00'
and nd_identificator =6292
group by   bm.nd_identificator
     , bm.nrd_identificator 
     , bm.nrd_identificator_old 
     , bm.nrd_grafic 
     , bm.mngm_begintime, bm.mngm_endtime, bm.mngm_beginorder, bm.mngm_endorder
     , bm.mngm_identificator, bm.tn_instance, bm.nrd_smena)

select nextval('tt.order_round_id_seq'::regclass) ,
       ol.order_list_id, rd.round_id, cn.cnrd_routenumber, cn.tn_instance, case avg(cn.cnrd_sysmark) when 8 then 0 else 1 end as is_active
from
bm
join tf.tbcardnariad cn on cn.tn_instance= bm.tn_instance and cn.nd_identificator =bm.nd_identificator and cn.nrd_identificator = bm.nrd_identificator   and cn.CNRD_Smena = bm.NRD_Smena
join tt.order_list ol on ol.tn_instance = cn.tn_instance and ol.order_num = cn.nrd_identificator
join tt.timetable_entry tte on tte.timetable_entry_id =ol.timetable_entry_id and tte.timetable_entry_num = cn.cnrd_graph
join tt.round rd on rd.timetable_entry_id =tte.timetable_entry_id and cn.tn_instance = rd.tn_instance and rd.round_num = cn.dstn_identificator
where cn.cnrd_routenumber <> 0
  ol.sing_deleted = 0
and not exists (select 1
 from tt.order_round ord
 where ord.order_list_id = ol.order_list_id
   and ord.order_round_num = cn.cnrd_routenumber
   and ord.tn_instance = cn.tn_instance
   and ord.is_active = 1)
group by ol.order_list_id,rd.round_id, cn.cnrd_routenumber, cn.tn_instance;

--************************4.Выполнение наряда******************************************************

insert into tt.order_oper(order_oper_id, order_round_id, timetable_plan_id, time_plan, time_oper , sign_deleted, dt_update , is_active, stop_erm_id, is_kp, stop_order_num, stop_place_muid, cnrd_orderby)
with bm as(
select
       bm.nd_identificator
     , bm.nrd_identificator as order_num
     , bm.nrd_identificator_old as order_num_old
     , bm.nrd_grafic as entry_num
     , bm.mngm_begintime, bm.mngm_endtime, bm.mngm_beginorder, bm.mngm_endorder
     , bm.mngm_identificator, bm.tn_instance, bm.nrd_smena
from  tf.tbbasemanagment bm
 WHERE bm.mngm_includedate >= '2017-04-03 12:05:00' and bm.mngm_includedate < '2017-04-03 12:10:00'
and nd_identificator =6292
group by   bm.nd_identificator
     , bm.nrd_identificator 
     , bm.nrd_identificator_old 
     , bm.nrd_grafic 
     , bm.mngm_begintime, bm.mngm_endtime, bm.mngm_beginorder, bm.mngm_endorder
     , bm.mngm_identificator, bm.tn_instance, bm.nrd_smena)

select  nextval('tt.order_oper_id_seq'::regclass) ,
ord.order_round_id, null,
order_date.dt::date+ (cn.cnrd_timeplan)::double precision * '00:01:00'::interval  AS time_plane,
order_date.dt::date+ (cn.cnrd_timeplan + cn.cnrd_diflextion)::double precision * '00:01:00'::interval  AS time_fact,
0,now() ,
case cn.cnrd_sysmark when 8 then 0 else 1 end as is_active,
stop.erma_id, 1 as is_kp,null, null, cn.cnrd_orderby
from
 bm
join tf.tbcardnariad cn on cn.tn_instance= bm.tn_instance and cn.nd_identificator =bm.nd_identificator and cn.nrd_identificator = bm.nrd_identificator   and cn.CNRD_Smena = bm.NRD_Smena
join tt.order_list ol on cn.tn_instance = bm.tn_instance and ol.order_num = cn.nrd_identificator
join tt.order_round ord on ol.order_list_id = ord.order_list_id and cn.cnrd_routenumber = ord.order_round_num
join tt.round rd on ord.round_id = rd.round_id  and rd.round_num = cn.dstn_identificator and rd.round_type =cn.dstn_type
join tf.tn_order_date ON tn_order_date.tn_id = cn.nd_identificator AND tn_order_date.tn_instance = cn.tn_instance
JOIN tf.order_date ON order_date.id_order_date = tn_order_date.id_order_date
JOIN tf.tn_stop ON tn_stop.tn_id = cn.cp_identificator  and tn_stop.tn_instance = rd.tn_instance
JOIN tf.stop ON stop.id_stop = tn_stop.id_stop
where cn.cnrd_routenumber <> 0
  and ol.sign_deleted = 0
  and ord.is_active = 1
and not exists (select 1
                from tt.order_oper oo
                where oo.order_round_id =  ord.order_round_id
                and oo.cnrd_orderby = cn.cnrd_orderby
                and oo.sign_deleted = 0)
;

