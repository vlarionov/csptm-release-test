insert into  tt.order_round(order_round_id, order_list_id, round_id, order_round_num, tn_instance, Is_active)

select nextval('tt.order_round_id_seq'::regclass) ,
ol.order_list_id, rd.round_id, cn.cnrd_routenumber, cn.tn_instance, case avg(cn.cnrd_sysmark) when 8 then 0 else 1 end as is_active

from tf.tbcardnariad cn 
join tt.order_list ol on ol.tn_instance = cn.tn_instance and ol.order_num = cn.nrd_identificator
join tt.timetable_entry tte on tte.timetable_entry_id =ol.timetable_entry_id and tte.timetable_entry_num = cn.cnrd_graph
join tt.round rd on rd.timetable_entry_id =tte.timetable_entry_id and cn.tn_instance = rd.tn_instance and rd.round_num = cn.dstn_identificator
where cn.nd_identificator = 6255 
--and cn.nrd_identificator =  62550245 and cn.tn_instance = '0003' 

group by ol.order_list_id,rd.round_id, cn.cnrd_routenumber, cn.tn_instance
