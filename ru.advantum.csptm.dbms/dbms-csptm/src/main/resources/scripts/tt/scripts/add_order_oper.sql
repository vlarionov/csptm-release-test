insert into tt.order_oper(oper_order, stop_place_muid, order_round_id, timetable_plan_id, time_plan, time_oper , sign_deleted, dt_update , is_active, stop_order_num)
select nextval('tt.order_oper_id_seq'::regclass) , 
spt.stop_place_muid, ord.order_round_id, null, 
order_date.dt::date+ (cn.cnrd_timeplan)::double precision * '00:01:00'::interval  AS time_plane,
order_date.dt::date+ (cn.cnrd_timeplan + cn.cnrd_diflextion)::double precision * '00:01:00'::interval  AS time_fact,
0,now() , 
case cn.cnrd_sysmark when 8 then 0 else 1 end as is_active
,spt.order_num
from 
tt.round rd
join tt.order_round ord on ord.round_id = rd.round_id
join tt.order_list ol on ol.order_list_id = ord.order_list_id
join ptt.route_trajectories t on t.muid = rd.route_trajectory_muid and t.trajectory_type_muid = rd.direction_id  --and t.sign_deleted =0
join ptt.stop_place2route_traj spt on spt.route_trajectory_muid =t.muid --and spt.sign_deleted = 0
join ptt.stop_places sp on sp.muid = spt.stop_place_muid --and sp.sign_deleted = 0
join ptt.stops st on st.muid= sp.stop_muid and st.direction_muid = rd.direction_id
left JOIN tf.stop ON  stop.erma_id = sp.erm_id 
left JOIN tf.tn_stop ON  tn_stop.tn_instance = rd.tn_instance and stop.id_stop = tn_stop.id_stop 
/*��������!!! ��� ���� ����������� ������ �� ���� ���� �� tf.tbcardnariad  � tt.tbcardnariad */
left join tt.tbcardnariad cn on cn.tn_instance = rd.tn_instance and cn.cnrd_routenumber = ord.order_round_num and rd.round_num = cn.dstn_identificator and rd.round_type =cn.dstn_type
                               and ol.order_num = cn.nrd_identificator and cn.nd_identificator  = 6255 and tn_stop.tn_id = cn.cp_identificator 
left join tf.tn_order_date ON tn_order_date.tn_id = cn.nd_identificator AND tn_order_date.tn_instance = cn.tn_instance
left JOIN tf.order_date ON order_date.id_order_date = tn_order_date.id_order_date;



