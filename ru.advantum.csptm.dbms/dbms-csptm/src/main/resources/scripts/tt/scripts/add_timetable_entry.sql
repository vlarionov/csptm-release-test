insert into tt.timetable_entry(timetable_entry_id,route_variant_muid, timetable_id, dr_shift_id, timetable_entry_num, sign_deleted, dt_update )
 select nextval('tt.timetable_entry_id_seq'::regclass), 
rr.muid, tt.timetable_id,null,  bn.nrd_grafic, 0, now()
     FROM tf.tbbasenariad bn
     JOIN tf.route_var_graph vg ON vg.tn_id = bn.mrsh_identificator AND vg.tn_instance = bn.tn_instance
     JOIN tf.route_var rv ON rv.id_route_var = vg.id_route_var AND rv.tn_instance = bn.tn_instance AND NOT rv.is_deleted
     join tt.timetable tt on tt.rmt_id= rv.n_num and tt.sign_deleted =0 
 jOIN ptt.routes r ON r.muid= tt.route_muid
     JOIN ptt.route_variants rr ON rr.route_muid = r.muid AND rr.muid = r.current_route_variant_muid
where bn.nd_identificator = 6255
and not exists (select 1
from  tt.timetable_entry x
where x.route_variant_muid=rr.muid
and x.timetable_id = tt.timetable_id
and x.timetable_entry_num = bn.nrd_grafic
and x.sign_deleted = 0)
group by  rr.muid, tt.timetable_id, bn.nrd_grafic
order by tt.timetable_id


/*

insert into  tt.order_list( order_list_id , timetable_entry_id, driver_id, tr_id , order_num , order_date , shift , sign_deleted , dt_update , is_active, tn_instance)
 select tt.order_list_id_seq.nextval, tte.timetable_entry_id,  
   (select driver_id from tt.driver dr where dr.tn_instance = bn.tn_instance and dr.tn_id = bn.nsid_uniqueid) as driver_id,
   (select tr_id from tt.tr tr where tr.tn_instance = bn.tn_instance and tr.tn_id = bn.nsit_uniqueid) as tr_id, 
   bn.nd_identificator, order_date.dt, bn.nrd_smena, 0 , now(), 1? , tn_instance
     FROM tf.tbbasenariad bn
     JOIN tf.route_var_graph vg ON vg.tn_id = bn.mrsh_identificator AND vg.tn_instance = bn.tn_instance
     JOIN tf.route_var rv ON rv.id_route_var = vg.id_route_var AND rv.tn_instance = bn.tn_instance AND NOT rv.is_deleted
    -- join tt.timetable tt on tt.rmt_id= rv.n_num and tt.sign_deleted =0 
    join tt.timetable_entry tte on tte.timetable_id = tt.timetable_id and tte.timetable_entry_num= bn.nrd_grafic
JOIN tf.tn_order_date ON tn_order_date.tn_id = bn.nd_identificator AND tn_order_date.tn_instance = bn.tn_instance
     JOIN tf.order_date ON order_date.id_order_date = tn_order_date.id_order_date
     


select * FROM tf.tbbasenariad bn
where bn.nd_identificator = 6253

select * from tf.order_date*/