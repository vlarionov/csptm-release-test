insert into tt.timetable_plan(timetable_plan_id, stop_place_muid, round_id, stop_erm_id, time_begin, time_end, sign_deleted, shift, dt_update)
select nextval('tt.timetable_plan_id_seq'::regclass) , 
      null,
      rd.round_id,
      rsp.stop_id,
      rsp.time_b::interval,
      rsp.time_e::interval,
      0,
      rsp.sm,
      now()
from tt.timetable tt
join tt.timetable_entry tte on tte.timetable_id = tt.timetable_id
join tt.round rd on rd.timetable_entry_id = tte.timetable_entry_id
join sym.tmp_rsp_ost rsp on rsp.mv_identificator =  tt.rmt_id 
                        and rtrim(rsp.tipr)=rtrim(rd.round_code)
                        and rsp.vector+1 = rd.direction_id
                       and rsp.nvx = tte.timetable_entry_num  
                      
where not exists (select 1 from tt.timetable_plan ttp where ttp.round_id = rd.round_id and ttp.stop_erm_id = rsp.stop_id)
;
