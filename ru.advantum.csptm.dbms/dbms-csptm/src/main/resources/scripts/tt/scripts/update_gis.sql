﻿/****************************************************  ИНТЕГРАЦИЯ С ГИС   ***************************************************************************************/
/*на сервере csptm-dev*/

ALTER TABLE tt.order_list ADD CONSTRAINT fk_ordr_list_tr FOREIGN KEY (tr_id) REFERENCES core.tr (tr_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;
ALTER TABLE tt.timetable_plan ADD CONSTRAINT fk_timetable_plan_stop_places FOREIGN KEY (stop_place_muid) REFERENCES gis.stop_places (muid) ON DELETE NO ACTION ON UPDATE NO ACTION
;
ALTER TABLE tt.order_oper ADD CONSTRAINT fk_order_oper_stop_places FOREIGN KEY (stop_place_muid) REFERENCES gis.stop_places (muid) ON DELETE NO ACTION ON UPDATE NO ACTION
;

DROP INDEX idx_round_timetable_round_num ;

DROP INDEX idx_round_timetable_round_type ;

--ищем машины
/*select count(1)
from tt.order_list ol
join core.tr tr on tr.garage_num::text = ol.tr_garage_num::text and ol.tn_instance = tr.park_instance_id

select count(1)
from tt.order_list ol
where ol.tr_id is not null
join core.tr tr on tr.garage_num::text = ol.tr_garage_num::text and ol.tn_instance = tr.park_instance_id

update tt.order_list 
set tt_tr_id = tr_id;

update tt.order_list 
set tr_id = null;

update tt.order_list
set tr_id = (select tr_id from core.tr tr where tr.garage_num::text = tt.order_list.tr_garage_num::text and tt.order_list.tn_instance = tr.park_instance_id);
*/

--Варианты расписания
update  tt.timetable
set route_muid = (select r.muid 
                  from gis.routes r 
                  where btrim(upper(r.number)) = btrim(upper(timetable_num)) 
                    and r.transport_kind_muid = rmt_tt_id
                    and (r.close_date IS NULL OR r.close_date::date >= now()::date) 
                    and r.open_date::date <= now()::date AND r.sign_deleted = 0)
where route_muid  is null;


--Выходы
update tt.timetable_entry
set route_variant_muid = (select rr.muid 
                          from tt.timetable tt 
                          join gis.routes r ON r.muid= tt.route_muid
                          join gis.route_variants rr ON rr.route_muid = r.muid AND rr.muid = r.current_route_variant_muid
                          where tt.timetable_id= tt.timetable_entry.timetable_id)
where route_variant_muid is null;


--Рейсы
select tt.update_round() ;
--c индексами не работает

--Выполнение
  select tt.update_order_oper();

--добавляем остановки, которых нет в нарядах
insert into tt.order_oper(order_oper_id, order_round_id, timetable_plan_id, time_plan, time_oper , sign_deleted, dt_update , is_active, stop_erm_id, is_kp, stop_order_num, stop_place_muid)
select nextval('tt.order_oper_id_seq'::regclass) , 
      ord.order_round_id ,null, null, null, 0, now(), 1, sp.erm_id,0 as is_kp, spt.order_num, spt.stop_place_muid
from tt.round rd 
join tt.order_round ord on ord.round_id = rd.round_id
join tt.order_list ol on ol.order_list_id =ord.order_list_id
join gis.stop_place2route_traj spt on spt.route_trajectory_muid =rd.route_trajectory_muid
join gis.stop_places sp on sp.muid = spt.stop_place_muid 
join gis.stops st on st.muid= sp.stop_muid 
where
--ol.order_date = date '2017-02-13' and
not exists (select 1 from tt.order_oper oo
            where oo.order_round_id = ord.order_round_id 
             and  sp.erm_id = oo.stop_erm_id);







