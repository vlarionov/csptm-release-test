/*���� ����������� ����������� �� 10.68.1.57*/
select order_date.dt::date as order_date
     , bm.nrd_identificator as order_num
     , bm.nrd_identificator_old as order_num_old
     , bm.nrd_grafic as entry_num
     , bm.mngm_begintime, bm.mngm_endtime, bm.mngm_beginorder, bm.mngm_endorder
     , bm.mngm_identificator, bm.tn_instance
from tf.tbbasemanagment bm 
join tf.tn_order_date ON bm.nd_identificator = tn_order_date.tn_id AND bm.tn_instance = tn_order_date.tn_instance
JOIN tf.order_date ON order_date.id_order_date = tn_order_date.id_order_date
     

 where  bm.mngm_includedate >= '2017-03-27 10:30:00' and bm.mngm_includedate < '2017-03-28'
--and bm.nrd_identificator =62928308
--and bm.tn_instance = '0015';

if p_mngm_beginorder >0 and p_mngm_endorder =0 then p_mngm_endorder:=100000000; end if;
if p_mngm_begintime >0 and p_mngm_endtime =0 then 
    l_begtime:= p_order_date::date+ (p_mngm_begintime)::double precision * '00:01:00'::interval;
    l_endtime:= p_order_date::date+ 2000::double precision * '00:01:00'::interval;
 else 
   l_begtime:= p_order_date::date+ (p_mngm_begintime)::double precision * '00:01:00'::interval;
   l_endtime:= p_order_date::date+ (p_mngm_endtime)::double precision * '00:01:00'::interval;
 end if;

/*�������� �� �������� ������ �� �������  �� ebnd*/ 
for c_x in  (
select oo.*
from tt.order_list ol
join tt.timetable_entry tte on tte.timetable_entry_id = ol.timetable_entry_id
join tt.order_round ord on ord.order_list_id = ol.order_list_id
join tt.order_oper oo on oo.order_round_id = ord.order_round_id
where 
 ol.tn_instance = p_tn_instance --'0001'
and tte.timetable_entry_num = p_entry_num --'704'
and ol.order_date = p_order_date --'2017-02-16'
and ol.order_num = p_order_num --62560418 /*nrd_identificator*/
and ol.sign_deleted = 0
and (oo.cnrd_orderby between p_mngm_beginorder and p_mngm_endorder
     or oo.time_plan between l_begtime  and l_endtime )
--and ((oo.time_plan >= '2017-02-16 06:40:00' and  oo.time_plan <= '2017-02-16 07:00:00')
) loop

/*������� ������*/
update tt.order_oper  set sign_deleted =1 where order_oper_id = c_x.order_oper_id;
/*��������� �����������*/
insert into tt.order_oper2impact(order_oper_id,impact_id) values(c_x.order_oper_id, p_impact_id );
end loop;

/*�������� ����� ������, ���� ��� ��������� ��������*/

update tt.order_round
set is_active = 0
where is_active=1
and  order_round_id in (
        select oo.order_round_id 
        from tt.impact_import ii
        join tt.impact_import2order_oper ii2oo on ii2oo.impact_import_id = ii.impact_import_id
        join tt.order_oper oo on oo.order_oper_id = ii2oo.order_oper_id
        where ii.impact_import_id= 185
        group by oo.order_round_id
        having avg(oo.is_active)=0
        )


/*�������� ������ ���� ��� ����� �������� */
update tt.order_list 
set is_active = 0 
where is_active =1
and order_list_id in (
select rr.order_list_id
        from tt.impact_import ii
        join tt.impact_import2order_oper ii2oo on ii2oo.impact_import_id = ii.impact_import_id
        join tt.order_oper oo on oo.order_oper_id = ii2oo.order_oper_id
        join tt.order_round rr on rr.order_round_id = oo.order_round_id
        join tt.order_list ol on ol.order_list_id = rr.order_list_id
        where ii.impact_import_id= 185
        and ol.order_date = '2017-05-02'
        group by rr.order_list_id
        having avg(rr.is_active)=0
)
