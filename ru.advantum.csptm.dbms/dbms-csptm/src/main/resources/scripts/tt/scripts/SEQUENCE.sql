﻿
CREATE SEQUENCE tt.park_id_seq;
CREATE SEQUENCE tt.tr_id_seq;
CREATE SEQUENCE tt.order_list_id_seq;
CREATE SEQUENCE tt.timetable_entry_id_seq;
CREATE SEQUENCE tt.round_id_seq;
CREATE SEQUENCE tt.order_round_id_seq;
CREATE SEQUENCE tt.driver_id_seq;
CREATE SEQUENCE tt.order_oper_id_seq;
CREATE SEQUENCE tt.timetable_plan_id_seq;
CREATE SEQUENCE tt.impact_id_seq;
CREATE SEQUENCE tt.round_type_id_seq;
CREATE SEQUENCE tt.timetable_id_seq;
