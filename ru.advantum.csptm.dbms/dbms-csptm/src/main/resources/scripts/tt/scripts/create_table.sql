/*
DROP TABLE tt.order_oper;
DROP table TT.order_round;
DROP TABLE tt.timetable_plan;
DROP TABLE TT.round;
DROP table  TT.order_list;
*/

-- Table tt.order_list

CREATE TABLE tt.order_list(
 order_list_id Bigint NOT NULL,
 timetable_entry_id Bigint,
 driver_id Bigint,
 tr_id Bigint ,
 order_num Bigint NOT NULL,
 order_date Timestamp NOT NULL,
 shift Bigint NOT NULL,
 sign_deleted Bigint NOT NULL,
 dt_update Timestamp NOT NULL,
 is_active Bigint DEFAULT 1 NOT NULL,
 tn_instance Text NOT NULL,
 tr_park_id Bigint,
 tr_garage_num Text
)
TABLESPACE tt_data
;

COMMENT ON TABLE tt.order_list IS '�����'
;
COMMENT ON COLUMN tt.order_list.order_list_id IS 'ID ������'
;
COMMENT ON COLUMN tt.order_list.timetable_entry_id IS 'ID ��������� ������'
;
COMMENT ON COLUMN tt.order_list.driver_id IS '��������'
;
COMMENT ON COLUMN tt.order_list.tr_id IS '��'
;
COMMENT ON COLUMN tt.order_list.order_num IS '����� ������'
;
COMMENT ON COLUMN tt.order_list.order_date IS '���� ������'
;
COMMENT ON COLUMN tt.order_list.shift IS '����� �����'
;
COMMENT ON COLUMN tt.order_list.sign_deleted IS '���� �������'
;
COMMENT ON COLUMN tt.order_list.dt_update IS '���� ���������'
;
COMMENT ON COLUMN tt.order_list.is_active IS '����������'
;
COMMENT ON COLUMN tt.order_list.tr_park_id IS '����
'
;
COMMENT ON COLUMN tt.order_list.tr_garage_num IS '�������� �����'
;

-- Create indexes for table tt.order_list

CREATE INDEX idx_order_list_tr_entry_id ON tt.order_list (timetable_entry_id)
TABLESPACE tt_idx
;

CREATE INDEX idx_order_list_driver_id ON tt.order_list (driver_id)
TABLESPACE tt_idx
;

CREATE INDEX idx_order_list_tr_id ON tt.order_list (tr_id)
;

-- Add keys for table tt.order_list

ALTER TABLE tt.order_list ADD CONSTRAINT pk_order_list PRIMARY KEY (order_list_id)
;


ALTER TABLE tt.order_list ADD CONSTRAINT fk_order_list_entry_id FOREIGN KEY (timetable_entry_id) REFERENCES tt.timetable_entry (timetable_entry_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;



-- Table tt.round

CREATE TABLE tt.round(
 round_id BigSerial NOT NULL,
 route_trajectory_muid Bigint,
 timetable_entry_id Bigint NOT NULL,
 direction_id Bigint NOT NULL,
 round_num Bigint NOT NULL,
 round_type Text NOT NULL,
 round_code Text NOT NULL,
 tn_instance Text NOT NULL,
 sign_deleted Bigint NOT NULL,
 dt_update Timestamp NOT NULL
)
TABLESPACE tt_data
;

COMMENT ON TABLE tt.round IS '�����'
;
COMMENT ON COLUMN tt.round.round_id IS '����'
;
COMMENT ON COLUMN tt.round.route_trajectory_muid IS '����������'
;
COMMENT ON COLUMN tt.round.timetable_entry_id IS '�����'
;
COMMENT ON COLUMN tt.round.direction_id IS '����������� ����� (1-������,2-��������)'
;
COMMENT ON COLUMN tt.round.round_num IS '����� �����'
;
COMMENT ON COLUMN tt.round.round_type IS '��� �����'
;
COMMENT ON COLUMN tt.round.round_code IS '��� �����'
;
COMMENT ON COLUMN tt.round.sign_deleted IS '������'
;
COMMENT ON COLUMN tt.round.dt_update IS '���� ��������������'
;

-- Create indexes for table tt.round

CREATE INDEX idx_round_route_trajectory_muid ON tt.round (route_trajectory_muid)
TABLESPACE tt_idx
;

CREATE INDEX idx_round_timetable_entry_id ON tt.round (timetable_entry_id)
TABLESPACE tt_idx
;
/*������� ������ ��� ����*/
CREATE INDEX idx_round_timetable_round_num ON tt.round (round_num)
TABLESPACE tt_idx
;

CREATE INDEX idx_round_timetable_round_type ON tt.round (round_type)
TABLESPACE tt_idx
;
/*********/
-- Add keys for table tt.round

ALTER TABLE tt.round ADD CONSTRAINT pk_round PRIMARY KEY (round_id)
;


ALTER TABLE tt.round ADD CONSTRAINT fk_timetable_entry_round FOREIGN KEY (timetable_entry_id) REFERENCES tt.timetable_entry (timetable_entry_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;







-- Table tt.timetable_plan

CREATE TABLE tt.timetable_plan(
 timetable_plan_id Bigint NOT NULL,
 stop_place_muid Bigint,
 time_begin Timestamp NOT NULL,
 time_end Timestamp,
 sign_deleted Bigint NOT NULL,
 dt_update Timestamp NOT NULL DEFAULT NOW(),
 shift Bigint NOT NULL,
 round_id Bigint NOT NULL
)
TABLESPACE tt_data
;


COMMENT ON TABLE tt.timetable_plan IS '�������� ����������'
;
COMMENT ON COLUMN tt.timetable_plan.timetable_plan_id IS '�������� ����������'
;
COMMENT ON COLUMN tt.timetable_plan.stop_place_muid IS '���������'
;
COMMENT ON COLUMN tt.timetable_plan.time_begin IS '����� �������� �� ���������'
;
COMMENT ON COLUMN tt.timetable_plan.time_end IS '����� ������ � ���������'
;
COMMENT ON COLUMN tt.timetable_plan.sign_deleted IS '������'
;
COMMENT ON COLUMN tt.timetable_plan.dt_update IS '���� ��������������'
;
COMMENT ON COLUMN tt.timetable_plan.shift IS '����� (�������� ������ �����)'
;
COMMENT ON COLUMN tt.timetable_plan.round_id IS '����'
;

ALTER TABLE tt.timetable_plan ADD plan_round_num bigint not null; 
COMMENT ON COLUMN tt.timetable_plan.plan_round_num IS '����� ����� � �������� ����������';

ALTER TABLE tt.timetable_plan ADD stop_erm_id bigint not null; 
COMMENT ON COLUMN tt.timetable_plan.plan_round_num IS 'ID ��������� �� ���';


-- Create indexes for table tt.timetable_plan

CREATE INDEX idx_timetable_plan_stop_id ON tt.timetable_plan (stop_place_muid)
TABLESPACE tt_idx
;

CREATE INDEX idx_timetable_plan_round_id ON tt.timetable_plan (round_id)
TABLESPACE tt_idx
;

-- Add keys for table tt.timetable_plan

ALTER TABLE tt.timetable_plan ADD CONSTRAINT pk_timetable_plan PRIMARY KEY (timetable_plan_id)
;

/*ALTER TABLE tt.timetable_plan ADD CONSTRAINT �������� ���������� UNIQUE (timetable_plan_id)
;
*/

ALTER TABLE tt.timetable_plan ADD CONSTRAINT fk_round_timetable_plan FOREIGN KEY (round_id) REFERENCES tt.round (round_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;




-- Table tt.order_round

CREATE TABLE tt.order_round(
 order_round_id Bigint NOT NULL,
 order_list_id Bigint NOT NULL,
 round_id Bigint NOT NULL,
 order_round_num Bigint NOT NULL,
 tn_instance Text NOT NULL,
 Is_active Bigint DEFAULT 1 NOT NULL,
 dt_update Timestamp DEFAULT now() NOT NULL
)
TABLESPACE tt_data
;
COMMENT ON COLUMN tt.order_round.order_round_id IS '����� ������'
;
COMMENT ON COLUMN tt.order_round.order_list_id IS '�����'
;
COMMENT ON COLUMN tt.order_round.round_id IS '�����'
;
COMMENT ON COLUMN tt.order_round.order_round_num IS '����� ����� � ������'
;
COMMENT ON COLUMN tt.order_round.Is_active IS '����������'
;
COMMENT ON COLUMN tt.order_round.dt_update IS '���� ��������������'
;

-- Create indexes for table tt.order_round

CREATE INDEX idx_order_round_order_list_id ON tt.order_round (order_list_id)
TABLESPACE tt_idx
;

CREATE INDEX idx_order_round_round_id ON tt.order_round (round_id)
TABLESPACE tt_idx
;

-- Add keys for table tt.order_round

ALTER TABLE tt.order_round ADD CONSTRAINT pk_order_round PRIMARY KEY (order_round_id)
 USING INDEX TABLESPACE tt_idx
;


ALTER TABLE tt.order_round ADD CONSTRAINT fk_order_round_round FOREIGN KEY (round_id) REFERENCES tt.round (round_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;


ALTER TABLE tt.order_round ADD CONSTRAINT fk_order_round_order_list FOREIGN KEY (order_list_id) REFERENCES tt.order_list (order_list_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;


-- Table tt.order_oper

CREATE TABLE tt.order_oper(
 order_oper_id Bigint NOT NULL,
 stop_place_muid Bigint,
 order_round_id Bigint NOT NULL,
 timetable_plan_id Bigint,
 time_plan Timestamp,
 time_oper Timestamp,
 time_fact Timestamp,
 sign_deleted Bigint NOT NULL,
 dt_update Timestamp DEFAULT now() NOT NULL,
 is_active Bigint DEFAULT 1 NOT NULL,
 stop_order_num Bigint ,
 is_kp Smallint NOT NULL,
 stop_erm_id Bigint
)
TABLESPACE tt_data
;

COMMENT ON TABLE tt.order_oper IS '���������� ������'
;
COMMENT ON COLUMN tt.order_oper.stop_place_muid IS '���������
'
;
COMMENT ON COLUMN tt.order_oper.timetable_plan_id IS '�������� ����������
'
;
COMMENT ON COLUMN tt.order_oper.time_plan IS '�������� ����� (�� ����������)
'
;
COMMENT ON COLUMN tt.order_oper.time_oper IS '����������� ����� (������ ����������)'
;
COMMENT ON COLUMN tt.order_oper.time_fact IS '����������� �����'
;
COMMENT ON COLUMN tt.order_oper.dt_update IS '���� ��������������'
;
COMMENT ON COLUMN tt.order_oper.stop_order_num IS '���������� ����� ���������'
;
COMMENT ON COLUMN tt.order_oper.is_kp IS '����������� ����� (������ �� ������)'
;
COMMENT ON COLUMN tt.order_oper.stop_erm_id IS '��� ��������� �� ���'
;

-- Create indexes for table tt.order_oper

CREATE INDEX idx_order_oper_stop_muid ON tt.order_oper (stop_place_muid)
TABLESPACE tt_idx
;

CREATE INDEX idx_order_oper_order_round_id ON tt.order_oper (order_round_id)
TABLESPACE tt_idx
;

CREATE INDEX idx_order_oper_timetable_plan_id ON tt.order_oper (timetable_plan_id)
TABLESPACE tt_idx
;

-- Add keys for table tt.order_oper

ALTER TABLE tt.order_oper ADD CONSTRAINT pk_order_oper PRIMARY KEY (oper_order)
 USING INDEX TABLESPACE tt_idx
;


