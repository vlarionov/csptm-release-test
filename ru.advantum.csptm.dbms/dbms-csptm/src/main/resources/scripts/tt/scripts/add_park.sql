insert into tt.Park(Park_id, tn_id, tn_instance, s_name, sign_deleted, pk_regnum, dt_begin, dt_end )
select nextval('tt.park_id_seq'::regclass), d.tn_id, d.tn_instance, d.s_name, 0, d.pk_regnum, d.dt_begin, d.dt_end
from tf.depo d
where d.is_deleted = 0 
