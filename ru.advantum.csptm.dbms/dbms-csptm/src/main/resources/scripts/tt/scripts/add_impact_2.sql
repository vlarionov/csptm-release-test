﻿/*ищем управляющие воздействия на 10.68.1.57
ЕСЛИ НАРЯД ИЗМЕНЕН НА ДРУГОЙ или разделен , 
то удаляем из старого наряда строки
*/
select order_date.dt::date as order_date
     , bm.nrd_identificator as order_num
     , bm.nrd_identificator_old as order_num_old
     , bm.nrd_grafic as entry_num
     , bm.mngm_begintime, bm.mngm_endtime, bm.mngm_beginorder, bm.mngm_endorder
     , bm.mngm_identificator, bm.tn_instance
from tf.tbbasemanagment bm 
join tf.tn_order_date ON bm.nd_identificator = tn_order_date.tn_id AND bm.tn_instance = tn_order_date.tn_instance
JOIN tf.order_date ON order_date.id_order_date = tn_order_date.id_order_date    

 where   bm.mngm_includedate >= '2017-04-28 12:05:00' and bm.mngm_includedate < '2017-04-28 12:10:00'
--and nd_identificator =6327
and bm.nrd_identificator!= bm.nrd_identificator_old 
and mngm_identificator in (5,6,7,8, 107,128)
/*and trim(mngm_title) in (
'Изм. наряда',
'Новый борт',
'Новый водитель',
'Доп. График',
'Снять с графика',
'Восстан.выход')*/
group by order_date.dt::date, bm.nd_identificator
     , bm.nrd_identificator
     , bm.nrd_identificator_old
     , bm.nrd_grafic
     , bm.mngm_begintime, bm.mngm_endtime, bm.mngm_beginorder, bm.mngm_endorder
     , bm.mngm_identificator, bm.tn_instance

if p_mngm_beginorder >0 and p_mngm_endorder =0 then p_mngm_endorder:=100000000; end if;
if p_mngm_begintime >0 and p_mngm_endtime =0 then 
    l_begtime:= p_order_date::date+ (p_mngm_begintime)::double precision * '00:01:00'::interval;
    l_endtime:= p_order_date::date+ 2000::double precision * '00:01:00'::interval;
 else 
   l_begtime:= p_order_date::date+ (p_mngm_begintime)::double precision * '00:01:00'::interval;
   l_endtime:= p_order_date::date+ (p_mngm_endtime)::double precision * '00:01:00'::interval;
 end if;

/*помечаем на удаление данные из нарядов  на ebnd*/ 
for c_x in  (
select oo.*
from tt.order_list ol
join tt.timetable_entry tte on tte.timetable_entry_id = ol.timetable_entry_id
join tt.order_round ord on ord.order_list_id = ol.order_list_id
join tt.order_oper oo on oo.order_round_id = ord.order_round_id
where 
 ol.tn_instance = p_tn_instance --'0001'
and tte.timetable_entry_num = p_entry_num --'704'
and ol.order_date = p_order_date --'2017-02-16'
and ol.order_num = p_order_num_OLD --62560418 /*nrd_identificator*/
and ol.sign_deleted = 0
and (oo.cnrd_orderby between p_mngm_beginorder and p_mngm_endorder
     or oo.time_plan between l_begtime  and l_endtime )
--and ((oo.time_plan >= '2017-02-16 06:40:00' and  oo.time_plan <= '2017-02-16 07:00:00')
) loop

/*удаляем строки*/
update tt.order_oper  set sign_deleted =1 where order_oper_id = c_x.order_oper_id;
/*добавляем воздействие*/
insert into tt.order_oper2impact(order_oper_id,impact_id) values(c_x.order_oper_id, p_impact_id );


/*если изменен наряд, то записываем*/
   insert into tt.order_list_tree(order_list_id, order_list_parent_id, impact_id) 
   select c_x.order_oper_id,  
          tt.orders_pkg$get_order_list(c_x.tn_instance,p_entry_num ,p_order_date,p_nrd_identificator_old),
          p_impact
   where not exists (select 1 from tt.order_list_tree where (order_list_id, order_list_parent_id) =(c_x.order_oper_id,  tt.orders_pkg$get_order_list(c_x.tn_instance,p_entry_num ,p_order_date,p_nrd_identificator_old)) );
end loop;

