﻿--*****************************ИНТЕГРАЦИЯ С АСДУ **********************************************************************************************

--1.Парки********************************

insert into tt.Park(Park_id, tn_id, tn_instance, s_name, sign_deleted, pk_regnum, dt_begin, dt_end )
select nextval('tt.park_id_seq'::regclass), d.tn_id, d.tn_instance, d.s_name, 0, d.pk_regnum, d.dt_begin, d.dt_end
from tf.depo d
where d.is_deleted = 0 
and not exists (select 1 from tt.park p where p.tn_id =d.tn_id and p.tn_instance = d.tn_instance);

--2.водители*****************************
insert into  tt.driver(driver_id, driver_name, driver_last_name, driver_middle_name, tab_num, tn_id, tn_instance , dt_begin, dt_end, sign_delete, update_date )
select nextval('tt.driver_id_seq'), d.first_name, d.surname, d.second_name, d.tab_num, d.tn_id, d.tn_instance, d.dt_begin, d.dt_end, 0 , now() 
from tf.driver d
where d.is_deleted = 0 
and not exists (select 1 from tt.driver p where p.tn_id =d.tn_id and p.tn_instance = d.tn_instance);

update tt.driver 
set sign_delete = 0
where exists (select 1 from tf.driver dr where dr.is_deleted=1 and dr.tn_id =tt.driver.tn_id and dr.tn_instance = tt.driver.tn_instance);

--4.ТС***********************************
insert into tt.tr(tr_id, park_id, tn_id, tn_instance, licence, garage_num, dt_begin, dt_end, sign_delete, dt_update)
select nextval('tt.tr_id_seq'::regclass),
(select park_id from tt.park p where p.tn_instance= tr.tn_instance and p.tn_id= depo.tn_id),
tr.tn_id,
tr.tn_instance, tr.gosnum, tr.garagenum, tr.dt_insert, null, 0,now()
from tf.tr tr
join tf.depo depo on depo.id_depo=tr.id_depo
where tr.is_deleted = 0
  and not exists (select 1 from tt.tr t where t.tn_id =tr.tn_id and t.tn_instance = tr.tn_instance);
;

update tt.tr
set sign_delete = 1
where exists (select 1 from tf.tr t where t.is_deleted=1 and t.tn_id =tt.tr.tn_id and t.tn_instance = tt.tr.tn_instance);

--6.Варианты расписания********************
insert into tt.timetable( timetable_id, rmt_id, rmt_tt_id, timetable_num, timetable_varr, begin_date , end_date , sign_deleted , dt_update, route_muid, timetable_setting_id)
select  nextval('tt.timetable_id_seq'::regclass) , mv.mv_identificator,mv.tt_identificator, mv.mv_marsh, mv.varr, mv.mv_includedate, mv.mv_excludedate,0,now(), null /*r.muid*/
,mv.mv_dnd
from
 sym.marshvariants mv 
where (mv.mv_excludedate IS NULL OR mv.mv_excludedate::date >= now()::date) 
                              AND mv.mv_includedate::date <= now()::date 
                              AND mv.varr IS NOT NULL AND mv.pr_deystv = 1
 and not exists (select 1 from tt.timetable tt  where tt.rmt_id =mv.mv_identificator);

--  обновлять, при изменениях???
update tt.timetable
set  begin_date , end_date , sign_deleted , dt_update, route_muid, timetable_setting_id
where  
exists (select 1
from sym.marshvariants mv 
where
rmt_id = mv.mv_identificator, 
and begin_date!= mv.mv_includedate
and timetable_setting_id!= mv.mv_dnd 
and coalesce(end_date,'01.01.1970') != coalesce(mv.mv_excludedate,'01.01.1970')
and mv.pr_deystv = 0
)



--7. Выходы**********************************
insert into tt.timetable_entry(timetable_entry_id, timetable_id, dr_shift_id, timetable_entry_num, sign_deleted, dt_update, route_variant_muid )
 select nextval('tt.timetable_entry_id_seq'::regclass), 
 tt.timetable_id,null,  bn.nrd_grafic, 0, now(), null
     FROM tf.tbbasenariad bn
     JOIN tf.route_var_graph vg ON vg.tn_id = bn.mrsh_identificator AND vg.tn_instance = bn.tn_instance
     JOIN tf.route_var rv ON rv.id_route_var = vg.id_route_var AND rv.tn_instance = bn.tn_instance AND NOT rv.is_deleted
     join tt.timetable tt on tt.rmt_id= rv.n_num and tt.sign_deleted =0 
     
where bn.nd_identificator = 6256
and not exists (select 1
from  tt.timetable_entry x
where  x.timetable_id = tt.timetable_id
and x.timetable_entry_num = bn.nrd_grafic
and x.sign_deleted = 0)
group by  tt.timetable_id, bn.nrd_grafic
order by tt.timetable_id;


--8. Наряды**************************************
insert into  tt.order_list( order_list_id , timetable_entry_id, driver_id, tr_id , order_num , order_date , shift , sign_deleted , dt_update , is_active, tn_instance,  tr_garage_num )
 select nextval('tt.order_list_id_seq'::regclass),  
       tte.timetable_entry_id,  
       (select driver_id from tt.driver dr where dr.tn_instance = bn.tn_instance and dr.tn_id = bn.nsid_uniqueid) as driver_id,
      
       (select tr_id from tt.tr tr where tr.tn_instance = bn.tn_instance and tr.tn_id = bn.nsit_uniqueid) as tr_id, 
        bn.nrd_identificator, order_date.dt, bn.nrd_smena::bigint, 0 , now(), 1 , bn.tn_instance,
       (select tr.garage_num from tt.tr tr where tr.tn_instance = bn.tn_instance and tr.tn_id = bn.nsit_uniqueid) as  tr_garage_num
     FROM tf.tbbasenariad bn
     JOIN tf.route_var_graph vg ON vg.tn_id = bn.mrsh_identificator AND vg.tn_instance = bn.tn_instance
     JOIN tf.route_var rv ON rv.id_route_var = vg.id_route_var AND rv.tn_instance = bn.tn_instance AND NOT rv.is_deleted
     join tt.timetable tt on tt.rmt_id= rv.n_num and tt.sign_deleted =0 
     join tt.timetable_entry tte on tte.timetable_id = tt.timetable_id and tte.timetable_entry_num= bn.nrd_grafic
     JOIN tf.tn_order_date ON tn_order_date.tn_id = bn.nd_identificator AND tn_order_date.tn_instance = bn.tn_instance
     JOIN tf.order_date ON order_date.id_order_date = tn_order_date.id_order_date
where bn.nd_identificator = 6256;
--[Выполнение: 1м 19с] 

--9. Рейсы*************************************
insert into tt.round(round_id,  route_trajectory_muid, timetable_entry_id, direction_id, dt_update , round_num, round_type, round_code , tn_instance,sign_deleted)
select nextval('tt.round_id_seq'::regclass), null, 
ol.timetable_entry_id, (ascii(cn.dstn_type) + 1) % 2 + 1 , now(), cn.dstn_identificator, cn.dstn_type, upper(btrim(race.title)),  cn.tn_instance , 0
FROM tf.tbcardnariad cn
JOIN tf.route r ON r.tn_instance = cn.tn_instance AND r.tn_id = cn.mr_id AND cn.tn_instance = r.tn_instance AND r.is_deleted = false
     JOIN sym.tbmarsh m ON m.marshid = r.mr_code
     JOIN sym.tbmarshvariant mv ON mv.marshid = m.marshid AND mv.deleted = 0 AND mv.isworking
     JOIN sym.tbrace race ON race.mvid = mv.mvid
     JOIN sym.tbracebydir rr ON rr.raceid = race.raceid AND rr.racedir = cn.dstn_type
     JOIN tf.tn_order_date ON tn_order_date.tn_id = cn.nd_identificator AND tn_order_date.tn_instance = cn.tn_instance
     JOIN tf.order_date ON order_date.id_order_date = tn_order_date.id_order_date
     join tt.order_list ol on ol.tn_instance = cn.tn_instance and ol.order_num = cn.nrd_identificator and order_date.dt = ol.order_date 
where cn.nd_identificator = 6256 
  and cn.dstn_identificator>0
  and cn.cnrd_routenumber <> 0
  and not exists (select 1 from tt.round rd where rd.timetable_entry_id = ol.timetable_entry_id  
                                              and rd.tn_instance = cn.tn_instance 
                                              and rd.round_num = cn.dstn_identificator 
                                              and rd.round_type = cn.dstn_type)
group by ol.timetable_entry_id, cn.dstn_type, cn.dstn_identificator, cn.dstn_type, upper(btrim(race.title)),  cn.tn_instance;
--[Выполнение: 3с - ] 

-- 10. Рейсы наряда
insert into  tt.order_round(order_round_id, order_list_id, round_id, order_round_num, tn_instance, Is_active)
select nextval('tt.order_round_id_seq'::regclass) ,
       ol.order_list_id, rd.round_id, cn.cnrd_routenumber, cn.tn_instance, case avg(cn.cnrd_sysmark) when 8 then 0 else 1 end as is_active
from tf.tbcardnariad cn 
join tt.order_list ol on ol.tn_instance = cn.tn_instance and ol.order_num = cn.nrd_identificator
join tt.timetable_entry tte on tte.timetable_entry_id =ol.timetable_entry_id and tte.timetable_entry_num = cn.cnrd_graph
join tt.round rd on rd.timetable_entry_id =tte.timetable_entry_id and cn.tn_instance = rd.tn_instance and rd.round_num = cn.dstn_identificator
where cn.nd_identificator = 6256 
  and cn.cnrd_routenumber <> 0
group by ol.order_list_id,rd.round_id, cn.cnrd_routenumber, cn.tn_instance;
--1мин 21сек -2мин

--11.Выполнение наряда
insert into tt.order_oper(order_oper_id, order_round_id, timetable_plan_id, time_plan, time_oper , sign_deleted, dt_update , is_active, stop_erm_id, is_kp, stop_order_num, stop_place_muid, cnrd_orderby)
select nextval('tt.order_oper_id_seq'::regclass) , 
ord.order_round_id, null, 
order_date.dt::date+ (cn.cnrd_timeplan)::double precision * '00:01:00'::interval  AS time_plane,
order_date.dt::date+ (cn.cnrd_timeplan + cn.cnrd_diflextion)::double precision * '00:01:00'::interval  AS time_fact,
0,now() , 
case cn.cnrd_sysmark when 8 then 0 else 1 end as is_active,
stop.erma_id, 1 as is_kp,null, null, cn.cnrd_orderby
from 
tt.round rd
join tt.order_round ord on ord.round_id = rd.round_id
join tt.order_list ol on ol.order_list_id = ord.order_list_id
join tf.tbcardnariad cn on cn.tn_instance = rd.tn_instance and cn.cnrd_routenumber = ord.order_round_num and rd.round_num = cn.dstn_identificator and rd.round_type =cn.dstn_type
                               and ol.order_num = cn.nrd_identificator and cn.nd_identificator  = 6256 
join tf.tn_order_date ON tn_order_date.tn_id = cn.nd_identificator AND tn_order_date.tn_instance = cn.tn_instance
JOIN tf.order_date ON order_date.id_order_date = tn_order_date.id_order_date
JOIN tf.tn_stop ON tn_stop.tn_id = cn.cp_identificator  and tn_stop.tn_instance = rd.tn_instance
JOIN tf.stop ON stop.id_stop = tn_stop.id_stop  
;

--1мин




/****************************************************  ИНТЕГРАЦИЯ С ГИС   ***************************************************************************************/
/*

ALTER TABLE tt.order_list ADD CONSTRAINT fk_ordr_list_tr FOREIGN KEY (tr_id) REFERENCES core.tr (tr_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;
ALTER TABLE tt.timetable_plan ADD CONSTRAINT fk_timetable_plan_stop_places FOREIGN KEY (stop_place_muid) REFERENCES gis.stop_places (muid) ON DELETE NO ACTION ON UPDATE NO ACTION
;
ALTER TABLE tt.order_oper ADD CONSTRAINT fk_order_oper_stop_places FOREIGN KEY (stop_place_muid) REFERENCES gis.stop_places (muid) ON DELETE NO ACTION ON UPDATE NO ACTION
;

DROP INDEX idx_round_timetable_round_num ;

DROP INDEX idx_round_timetable_round_type ;


--Варианты расписания
update  tt.timetable
set route_muid = (select r.muid 
                  from ptt.routes r 
                  where btrim(upper(r.number)) = btrim(upper(timetable_num)) 
                    and r.transport_kind_muid = rmt_tt_id
                    and (r.close_date IS NULL OR r.close_date::date >= now()::date) 
                    and r.open_date::date <= now()::date AND r.sign_deleted = 0)
where route_muid  is null;


--Выходы
update tt.timetable_entry
set route_variant_muid = (select rr.muid 
                          from tt.timetable tt 
                          join ptt.routes r ON r.muid= tt.route_muid
                          join ptt.route_variants rr ON rr.route_muid = r.muid AND rr.muid = r.current_route_variant_muid
                          where tt.timetable_id= tt.timetable_entry.timetable_id)
where route_variant_muid is null;


--Рейсы
select tt.update_round() ;
--c индексами не работает
 


--Рейсы
  select tt.update_order_oper();

--добавляем остановки, которых нет в нарядах
insert into tt.order_oper(order_oper_id, order_round_id, timetable_plan_id, time_plan, time_oper , sign_deleted, dt_update , is_active, stop_erm_id, is_kp, stop_order_num, stop_place_muid)
select nextval('tt.order_oper_id_seq'::regclass) , 
      ord.order_round_id ,null, null, null, 0, now(), 1, sp.erm_id,0 as is_kp, spt.order_num, spt.stop_place_muid
from tt.round rd 
join tt.order_round ord on ord.round_id = rd.round_id
join tt.order_list ol on ol.order_list_id =ord.order_list_id
join gis.stop_place2route_traj spt on spt.route_trajectory_muid =rd.route_trajectory_muid
join gis.stop_places sp on sp.muid = spt.stop_place_muid
left join tt.order_oper oo on ord.order_round_id = oo.order_round_id and sp.erm_id = oo.stop_erm_id
where ol.order_date = date '2017-02-13';
*/

-- плановое расписание
--update tt.timetable_plan tp
--set stop_place_muid = sp.muid
--from gis.stop_places sp
--where stop_erm_id = sp.erm_id;

