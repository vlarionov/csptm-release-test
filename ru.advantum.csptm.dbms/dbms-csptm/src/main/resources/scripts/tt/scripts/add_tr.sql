insert into tt.tr(tr_id, park_id, tn_id, tn_instance, licence, garage_num, dt_begin, dt_end, sign_delete, dt_update)
select nextval('tt.tr_id_seq'::regclass),
(select park_id from tt.park p where p.tn_instance= tr.tn_instance and p.tn_id= depo.tn_id),
tr.tn_id,
tr.tn_instance, tr.gosnum, tr.garagenum, tr.dt_insert, null, 0,now()
from tf.tr tr
join tf.depo depo on depo.id_depo=tr.id_depo
where tr.is_deleted = 0
