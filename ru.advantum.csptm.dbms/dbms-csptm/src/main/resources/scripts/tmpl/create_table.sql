DROP SCHEMA IF EXISTS tmpl cascade;
create schema tmpl;

/*
Created: 7/12/2016
Modified: 9/21/2017
Project: csptm
Model: Integration
Database: PostgreSQL 9.4
*/

-- Create tables section -------------------------------------------------

-- Table tmpl.equipment_status

CREATE TABLE tmpl.equipment_status(
 equipment_status_id Smallserial NOT NULL,
 name Text NOT NULL,
 description Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.equipment_status IS 'Статус БО'
;
COMMENT ON COLUMN tmpl.equipment_status.equipment_status_id IS NULL
;
COMMENT ON COLUMN tmpl.equipment_status.name IS 'Статус'
;
COMMENT ON COLUMN tmpl.equipment_status.description IS 'Комментарий'
;

-- Add keys for table tmpl.equipment_status

ALTER TABLE tmpl.equipment_status ADD CONSTRAINT pk_equipment_status PRIMARY KEY (equipment_status_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_equipment_status ON tmpl.equipment_status IS NULL
;

-- Table tmpl.equipment_type

CREATE TABLE tmpl.equipment_type(
 equipment_type_id Smallserial NOT NULL,
 name_full Text NOT NULL,
 name_short Text NOT NULL,
 equipment_class_id Smallint NOT NULL,
 has_diagnostic Boolean DEFAULT true NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.equipment_type IS 'Тип БО'
;
COMMENT ON COLUMN tmpl.equipment_type.equipment_type_id IS 'Тип БО'
;
COMMENT ON COLUMN tmpl.equipment_type.name_full IS 'Наименование типа полное'
;
COMMENT ON COLUMN tmpl.equipment_type.name_short IS 'Наименование сокращенное'
;
COMMENT ON COLUMN tmpl.equipment_type.equipment_class_id IS 'Класс
0- Бортовой блок
1 - Датчик'
;
COMMENT ON COLUMN tmpl.equipment_type.has_diagnostic IS 'Диагностика включена'
;

-- Create indexes for table tmpl.equipment_type

CREATE INDEX ix_equipment_type_class_id ON tmpl.equipment_type (equipment_class_id)
TABLESPACE core_idx
;
COMMENT ON INDEX tmpl.ix_equipment_type_class_id IS NULL
;

-- Add keys for table tmpl.equipment_type

ALTER TABLE tmpl.equipment_type ADD CONSTRAINT pk_equipment_type_equipment_type_id PRIMARY KEY (equipment_type_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_equipment_type_equipment_type_id ON tmpl.equipment_type IS NULL
;

-- Table tmpl.mobile_tariff

CREATE TABLE tmpl.mobile_tariff(
 mobile_tariff_id BigSerial NOT NULL,
 name Text NOT NULL,
 is_active Boolean DEFAULT true NOT NULL,
 comment Text,
 facility_id Bigint
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.mobile_tariff IS 'Тариф'
;
COMMENT ON COLUMN tmpl.mobile_tariff.mobile_tariff_id IS 'Идентификатор'
;
COMMENT ON COLUMN tmpl.mobile_tariff.name IS 'Тарифный план'
;
COMMENT ON COLUMN tmpl.mobile_tariff.is_active IS 'Используется'
;
COMMENT ON COLUMN tmpl.mobile_tariff.comment IS 'Комментарий'
;
COMMENT ON COLUMN tmpl.mobile_tariff.facility_id IS 'Сотовый оператор (Организация)'
;

-- Create indexes for table tmpl.mobile_tariff

CREATE INDEX ix_mobile_tariff_facility_id ON tmpl.mobile_tariff (facility_id)
TABLESPACE core_idx
;
COMMENT ON INDEX tmpl.ix_mobile_tariff_facility_id IS NULL
;

-- Add keys for table tmpl.mobile_tariff

ALTER TABLE tmpl.mobile_tariff ADD CONSTRAINT pk_mobile_tariff_mobile_tariff_id PRIMARY KEY (mobile_tariff_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_mobile_tariff_mobile_tariff_id ON tmpl.mobile_tariff IS NULL
;

-- Table tmpl.sim_card_group

CREATE TABLE tmpl.sim_card_group(
 sim_card_group_id BigSerial NOT NULL,
 name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.sim_card_group IS 'Группа sim-карт'
;
COMMENT ON COLUMN tmpl.sim_card_group.sim_card_group_id IS 'Идентификатор'
;
COMMENT ON COLUMN tmpl.sim_card_group.name IS 'Наименование'
;

-- Add keys for table tmpl.sim_card_group

ALTER TABLE tmpl.sim_card_group ADD CONSTRAINT pk_sim_card_group_sim_card_group_id PRIMARY KEY (sim_card_group_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_sim_card_group_sim_card_group_id ON tmpl.sim_card_group IS NULL
;

-- Create tables section -------------------------------------------------

-- Table tmpl.firmware

CREATE TABLE tmpl.firmware(
 firmware_id BigSerial NOT NULL,
 facility_id Bigint NOT NULL,
 equipment_model_id Integer NOT NULL,
 version Text NOT NULL,
 dt Timestamp NOT NULL,
 port Text,
 ip_address Text,
 comment Text,
 url_ftp Text,
 path_ftp Text,
 ftp_user Text,
 ftp_user_password Text,
 name Character varying(29) NOT NULL,
 file_name Character varying(29)
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.firmware IS NULL
;
COMMENT ON COLUMN tmpl.firmware.firmware_id IS NULL
;
COMMENT ON COLUMN tmpl.firmware.facility_id IS 'Производитель прошивки'
;
COMMENT ON COLUMN tmpl.firmware.equipment_model_id IS 'Модель оборудования'
;
COMMENT ON COLUMN tmpl.firmware.version IS 'Версия прошивки'
;
COMMENT ON COLUMN tmpl.firmware.dt IS 'Дата прошивки'
;
COMMENT ON COLUMN tmpl.firmware.port IS 'Порт FTP'
;
COMMENT ON COLUMN tmpl.firmware.ip_address IS 'IP-адрес  FTP'
;
COMMENT ON COLUMN tmpl.firmware.comment IS 'Комментарий'
;
COMMENT ON COLUMN tmpl.firmware.url_ftp IS 'URL адрес FTP'
;
COMMENT ON COLUMN tmpl.firmware.path_ftp IS 'Путь к папке прошивки '
;
COMMENT ON COLUMN tmpl.firmware.ftp_user IS 'Имя пользователя FTP'
;
COMMENT ON COLUMN tmpl.firmware.ftp_user_password IS 'Пароль пользователя FTP'
;
COMMENT ON COLUMN tmpl.firmware.name IS 'Наименование прошивки'
;
COMMENT ON COLUMN tmpl.firmware.file_name IS 'Ссылка на файл'
;

-- Create indexes for table tmpl.firmware

CREATE INDEX ix_firmware_facility_id ON tmpl.firmware (facility_id)
TABLESPACE core_idx
;

CREATE INDEX ix_firmware_equipment_model_id ON tmpl.firmware (equipment_model_id)
TABLESPACE core_idx
;
COMMENT ON INDEX tmpl.ix_firmware_facility_id IS NULL
;
COMMENT ON INDEX tmpl.ix_firmware_equipment_model_id IS NULL
;

-- Add keys for table tmpl.firmware

ALTER TABLE tmpl.firmware ADD CONSTRAINT pk_firmware PRIMARY KEY (firmware_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_firmware ON tmpl.firmware IS NULL
;

-- Create tables section -------------------------------------------------

-- Table tmpl.facility

CREATE TABLE tmpl.facility(
 facility_id Bigint NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.facility IS 'Организации'
;
COMMENT ON COLUMN tmpl.facility.facility_id IS 'Идентификатор организации'
;

-- Add keys for table tmpl.facility

ALTER TABLE tmpl.facility ADD CONSTRAINT pk_facility PRIMARY KEY (facility_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_facility ON tmpl.facility IS NULL
;

-- Table tmpl.unit_bnsr

CREATE TABLE IF NOT EXISTS tmpl.unit_bnsr(
 unit_bnsr_id Bigint NOT NULL,
 channel_num Integer NOT NULL,
 has_manipulator Integer DEFAULT 0,
 usw_num Text,
 last_call Timestamp
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.unit_bnsr IS 'Бортовой блок:БНСР'
;
COMMENT ON COLUMN tmpl.unit_bnsr.unit_bnsr_id IS 'ID бортового блока'
;
COMMENT ON COLUMN tmpl.unit_bnsr.channel_num IS 'Номер канала'
;
COMMENT ON COLUMN tmpl.unit_bnsr.has_manipulator IS 'Манипулятор к комплекте'
;
COMMENT ON COLUMN tmpl.unit_bnsr.usw_num IS 'Номер радиостанции'
;
COMMENT ON COLUMN tmpl.unit_bnsr.last_call IS NULL
;

-- Add keys for table tmpl.unit_bnsr

ALTER TABLE tmpl.unit_bnsr ADD CONSTRAINT pk_unit_bnsr_id PRIMARY KEY (unit_bnsr_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_unit_bnsr_id ON tmpl.unit_bnsr IS NULL
;

-- Create tables section -------------------------------------------------

-- Table tmpl.sim_card

CREATE TABLE tmpl.sim_card(
 sim_card_id BigSerial NOT NULL,
 dt_begin Timestamp NOT NULL,
 iccid Text NOT NULL,
 phone_num Text NOT NULL,
 imsi Text,
 mobile_tariff_id Bigint NOT NULL,
 sim_card_group_id Bigint NOT NULL,
 equipment_status_id Smallint NOT NULL,
 depo_id Integer NOT NULL,
 territory_id Bigint
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.sim_card IS NULL
;
COMMENT ON COLUMN tmpl.sim_card.sim_card_id IS 'Sim-карта'
;
COMMENT ON COLUMN tmpl.sim_card.dt_begin IS 'Дата создания'
;
COMMENT ON COLUMN tmpl.sim_card.iccid IS 'Номер sim-карты (ICCID)'
;
COMMENT ON COLUMN tmpl.sim_card.phone_num IS 'Номер телефона'
;
COMMENT ON COLUMN tmpl.sim_card.imsi IS 'IMSI'
;
COMMENT ON COLUMN tmpl.sim_card.mobile_tariff_id IS 'Тариф'
;
COMMENT ON COLUMN tmpl.sim_card.sim_card_group_id IS 'Группа sim-карт'
;
COMMENT ON COLUMN tmpl.sim_card.equipment_status_id IS 'Статус'
;
COMMENT ON COLUMN tmpl.sim_card.depo_id IS 'ТП'
;
COMMENT ON COLUMN tmpl.sim_card.territory_id IS 'Территория ТП'
;

-- Create indexes for table tmpl.sim_card

CREATE INDEX ix_sim_card_sim_card_group_id ON tmpl.sim_card (sim_card_group_id)
TABLESPACE core_idx
;

CREATE INDEX ix_sim_card_mobile_tariff_id ON tmpl.sim_card (mobile_tariff_id)
TABLESPACE core_idx
;

CREATE INDEX ix_sim_card_equipment_status_id ON tmpl.sim_card (equipment_status_id)
TABLESPACE core_idx
;

CREATE INDEX ix_sim_card_equipment_depo_id ON tmpl.sim_card (depo_id)
TABLESPACE core_idx
;
COMMENT ON INDEX tmpl.ix_sim_card_sim_card_group_id IS NULL
;
COMMENT ON INDEX tmpl.ix_sim_card_mobile_tariff_id IS NULL
;
COMMENT ON INDEX tmpl.ix_sim_card_equipment_status_id IS NULL
;
COMMENT ON INDEX tmpl.ix_sim_card_equipment_depo_id IS NULL
;

-- Add keys for table tmpl.sim_card

ALTER TABLE tmpl.sim_card ADD CONSTRAINT pk_sim_card_sim_card_id PRIMARY KEY (sim_card_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_sim_card_sim_card_id ON tmpl.sim_card IS NULL
;

-- Create tables section -------------------------------------------------

-- Table tmpl.sim_card2unit

CREATE TABLE tmpl.sim_card2unit(
 sim_card_id Bigint NOT NULL,
 equipment_id Bigint NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.sim_card2unit IS 'Sim карта в блоке'
;
COMMENT ON COLUMN tmpl.sim_card2unit.sim_card_id IS 'Sim-карта'
;
COMMENT ON COLUMN tmpl.sim_card2unit.equipment_id IS 'ID бортового блока'
;

-- Add keys for table tmpl.sim_card2unit

ALTER TABLE tmpl.sim_card2unit ADD CONSTRAINT pk_sim_card2unit PRIMARY KEY (sim_card_id,equipment_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_sim_card2unit ON tmpl.sim_card2unit IS NULL
;

-- Create tables section -------------------------------------------------

-- Table tmpl.unit

CREATE TABLE tmpl.unit(
 unit_id Bigint NOT NULL,
 unit_num Text NOT NULL,
 hub_id Integer
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.unit IS 'Бортовой блок'
;
COMMENT ON COLUMN tmpl.unit.unit_id IS NULL
;
COMMENT ON COLUMN tmpl.unit.unit_num IS 'Уникальный номер'
;
COMMENT ON COLUMN tmpl.unit.hub_id IS 'Сервис приема-передачи данных'
;

-- Create indexes for table tmpl.unit

CREATE INDEX ix_unit_hub_id ON tmpl.unit (hub_id)
TABLESPACE core_idx
;

CREATE UNIQUE INDEX uq_unit_unit_num_hub_id ON tmpl.unit (unit_num,hub_id)
TABLESPACE core_idx
;
COMMENT ON INDEX tmpl.ix_unit_hub_id IS NULL
;
COMMENT ON INDEX tmpl.uq_unit_unit_num_hub_id IS NULL
;

-- Add keys for table tmpl.unit

ALTER TABLE tmpl.unit ADD CONSTRAINT pk_unit PRIMARY KEY (unit_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_unit ON tmpl.unit IS NULL
;

-- Table tmpl.unit_service_type

CREATE TABLE tmpl.unit_service_type(
 unit_service_type_id Smallserial NOT NULL,
 name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.unit_service_type IS 'Вид тех. обслуживания_const'
;
COMMENT ON COLUMN tmpl.unit_service_type.unit_service_type_id IS 'Вид тех. обслуживания'
;
COMMENT ON COLUMN tmpl.unit_service_type.name IS 'Вид тех. обслуживания'
;

-- Add keys for table tmpl.unit_service_type

ALTER TABLE tmpl.unit_service_type ADD CONSTRAINT pk_unit_service_type PRIMARY KEY (unit_service_type_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_unit_service_type ON tmpl.unit_service_type IS NULL
;

-- Table tmpl.territory

CREATE TABLE tmpl.territory(
 territory_id Bigint NOT NULL,
 code Integer NOT NULL,
 dt_begin Timestamp NOT NULL,
 dt_end Timestamp,
 wkt_geom Text,
 geom Bigint,
 sign_deleted Smallint DEFAULT 0,
 park_zone_muid Bigint
)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.territory IS 'Территория'
;
COMMENT ON COLUMN tmpl.territory.territory_id IS 'Идентификатор территории'
;
COMMENT ON COLUMN tmpl.territory.code IS 'Номер территории'
;
COMMENT ON COLUMN tmpl.territory.dt_begin IS 'Дата ввода в действие'
;
COMMENT ON COLUMN tmpl.territory.dt_end IS 'Дата ликвидации'
;
COMMENT ON COLUMN tmpl.territory.wkt_geom IS 'Координаты'
;
COMMENT ON COLUMN tmpl.territory.geom IS 'Геометрия'
;
COMMENT ON COLUMN tmpl.territory.sign_deleted IS 'удален'
;
COMMENT ON COLUMN tmpl.territory.park_zone_muid IS 'территория парка из ГИС'
;

-- Add keys for table tmpl.territory

ALTER TABLE tmpl.territory ADD CONSTRAINT pk_terrirtory_id PRIMARY KEY (territory_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_terrirtory_id ON tmpl.territory IS NULL
;

-- Table tmpl.sensor

CREATE TABLE tmpl.sensor(
 sensor_id Bigint NOT NULL,
 sensor_num Bigint
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.sensor IS 'Датчик'
;
COMMENT ON COLUMN tmpl.sensor.sensor_id IS NULL
;
COMMENT ON COLUMN tmpl.sensor.sensor_num IS NULL
;

-- Add keys for table tmpl.sensor

ALTER TABLE tmpl.sensor ADD CONSTRAINT pk_sensor_sensor_id PRIMARY KEY (sensor_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_sensor_sensor_id ON tmpl.sensor IS NULL
;

-- Table tmpl.tr

CREATE TABLE tmpl.tr(
 tr_id BigSerial NOT NULL,
 seat_qty Integer DEFAULT 0 NOT NULL,
 has_low_floor Boolean DEFAULT false NOT NULL,
 has_conditioner Boolean DEFAULT false NOT NULL,
 garage_num Integer NOT NULL,
 licence Text,
 serial_num Text,
 dt_begin Timestamp NOT NULL,
 dt_end Timestamp,
 tr_type_id Smallint NOT NULL,
 tr_status_id Bigint DEFAULT 0 NOT NULL,
 tr_model_id Bigint NOT NULL,
 territory_id Bigint,
 park_instance_id Text DEFAULT '-'::text NOT NULL,
 has_wheelchair_space Boolean,
 has_bicicle_space Boolean,
 seat_qty_disabled_people Integer,
 seat_qty_total Integer NOT NULL,
 depo_id Bigint,
 year_of_issue Integer NOT NULL,
 garage_num_add Integer,
 equipment_afixed Text,
 tr_schema_install_id Integer
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.tr IS 'Транспортное средство'
;
COMMENT ON COLUMN tmpl.tr.tr_id IS NULL
;
COMMENT ON COLUMN tmpl.tr.seat_qty IS 'Кол-во сидячих мест'
;
COMMENT ON COLUMN tmpl.tr.has_low_floor IS 'Признак низкопольности'
;
COMMENT ON COLUMN tmpl.tr.has_conditioner IS 'Наличие кондиционера'
;
COMMENT ON COLUMN tmpl.tr.garage_num IS 'Гаражный номер'
;
COMMENT ON COLUMN tmpl.tr.licence IS 'Государственный номер'
;
COMMENT ON COLUMN tmpl.tr.serial_num IS 'Заводской (серийный) номер'
;
COMMENT ON COLUMN tmpl.tr.dt_begin IS 'Дата ввода в эксплуатацию'
;
COMMENT ON COLUMN tmpl.tr.dt_end IS 'Дата списания'
;
COMMENT ON COLUMN tmpl.tr.tr_type_id IS 'Вид ТС'
;
COMMENT ON COLUMN tmpl.tr.tr_status_id IS 'Статус ТС'
;
COMMENT ON COLUMN tmpl.tr.tr_model_id IS 'Модель ТС'
;
COMMENT ON COLUMN tmpl.tr.territory_id IS 'Территории'
;
COMMENT ON COLUMN tmpl.tr.park_instance_id IS 'ID парка в АСДУ'
;
COMMENT ON COLUMN tmpl.tr.has_wheelchair_space IS 'признак наличия площадки (и/или специальных креплений) для инвалидной коляски'
;
COMMENT ON COLUMN tmpl.tr.has_bicicle_space IS 'признак наличия площадки для велосипеда'
;
COMMENT ON COLUMN tmpl.tr.seat_qty_disabled_people IS 'количество сидячих мест для инвалидов'
;
COMMENT ON COLUMN tmpl.tr.seat_qty_total IS 'общее число место мест в ТС'
;
COMMENT ON COLUMN tmpl.tr.depo_id IS 'Транспортное предприятие'
;
COMMENT ON COLUMN tmpl.tr.year_of_issue IS 'Год выпуска'
;
COMMENT ON COLUMN tmpl.tr.garage_num_add IS 'Дополнительный гаражный номер'
;
COMMENT ON COLUMN tmpl.tr.equipment_afixed IS 'Закрепленное за ТС оборудование'
;
COMMENT ON COLUMN tmpl.tr.tr_schema_install_id IS 'Схема подключения БО'
;

-- Create indexes for table tmpl.tr

CREATE INDEX ix_tr_territory_id ON tmpl.tr (territory_id)
TABLESPACE core_idx
;

CREATE INDEX ix_tr_tr_type ON tmpl.tr (tr_type_id)
TABLESPACE core_idx
;

CREATE INDEX ix_tr_status_id ON tmpl.tr (tr_status_id)
TABLESPACE core_idx
;

CREATE INDEX ix_tr_model_id ON tmpl.tr (tr_model_id)
TABLESPACE core_idx
;

CREATE INDEX ix_tr_depo_id ON tmpl.tr (depo_id)
TABLESPACE core_idx
;

CREATE INDEX ix_tr_schema_install ON tmpl.tr (tr_schema_install_id)
TABLESPACE core_idx
;

CREATE UNIQUE INDEX ixunq_tr_garage_num_depo ON tmpl.tr (garage_num,depo_id)
TABLESPACE core_idx
WHERE (garage_num <> '-1'::integer)
;
COMMENT ON INDEX tmpl.ix_tr_territory_id IS NULL
;
COMMENT ON INDEX tmpl.ix_tr_tr_type IS NULL
;
COMMENT ON INDEX tmpl.ix_tr_status_id IS NULL
;
COMMENT ON INDEX tmpl.ix_tr_model_id IS NULL
;
COMMENT ON INDEX tmpl.ix_tr_depo_id IS NULL
;
COMMENT ON INDEX tmpl.ix_tr_schema_install IS NULL
;
COMMENT ON INDEX tmpl.ixunq_tr_garage_num_depo IS NULL
;

-- Add keys for table tmpl.tr

ALTER TABLE tmpl.tr ADD CONSTRAINT pk_tr_id PRIMARY KEY (tr_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_tr_id ON tmpl.tr IS NULL
;

-- Table tmpl.tr_capacity

CREATE TABLE tmpl.tr_capacity(
 tr_capacity_id Smallserial NOT NULL,
 qty Integer NOT NULL,
 full_name Text NOT NULL,
 short_name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.tr_capacity IS 'Вместимость ТС'
;
COMMENT ON COLUMN tmpl.tr_capacity.tr_capacity_id IS 'Вместимость ТС'
;
COMMENT ON COLUMN tmpl.tr_capacity.qty IS 'Число мест'
;
COMMENT ON COLUMN tmpl.tr_capacity.full_name IS 'Полное наименование'
;
COMMENT ON COLUMN tmpl.tr_capacity.short_name IS 'Краткое наименование'
;

-- Add keys for table tmpl.tr_capacity

ALTER TABLE tmpl.tr_capacity ADD CONSTRAINT pk_tr_capacity PRIMARY KEY (tr_capacity_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_tr_capacity ON tmpl.tr_capacity IS NULL
;

-- Table tmpl.tr_mark

CREATE TABLE tmpl.tr_mark(
 tr_mark_id BigSerial NOT NULL,
 name Text NOT NULL,
 facility_id Bigint
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.tr_mark IS 'Марка ТС'
;
COMMENT ON COLUMN tmpl.tr_mark.tr_mark_id IS 'Марка ТС'
;
COMMENT ON COLUMN tmpl.tr_mark.name IS NULL
;
COMMENT ON COLUMN tmpl.tr_mark.facility_id IS 'Производитель'
;

-- Create indexes for table tmpl.tr_mark

CREATE INDEX ix_tr_mark_facility ON tmpl.tr_mark (facility_id)
TABLESPACE core_idx
;
COMMENT ON INDEX tmpl.ix_tr_mark_facility IS NULL
;

-- Add keys for table tmpl.tr_mark

ALTER TABLE tmpl.tr_mark ADD CONSTRAINT fk_tr_mark_tr_mark_id PRIMARY KEY (tr_mark_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT fk_tr_mark_tr_mark_id ON tmpl.tr_mark IS NULL
;

-- Table tmpl.depo

CREATE TABLE tmpl.depo(
 depo_id Bigint NOT NULL,
 tn_instance Text NOT NULL,
 carrier_id Bigint NOT NULL,
 park_muid Bigint,
 tr_type_id Smallint NOT NULL,
 wkt_geom Text,
 geom Bigint,
 sign_deleted Smallint DEFAULT 0 NOT NULL,
 sys_period tstzrange DEFAULT tstzrange(now(), NULL::timestamp with time zone) NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.depo IS 'Транспортное предприятие'
;
COMMENT ON COLUMN tmpl.depo.depo_id IS 'Идентификатор Транспортное предприятие'
;
COMMENT ON COLUMN tmpl.depo.tn_instance IS 'Код предприятия'
;
COMMENT ON COLUMN tmpl.depo.carrier_id IS 'Идентификатор Транспортная организация'
;
COMMENT ON COLUMN tmpl.depo.park_muid IS 'id парка из ГИС'
;
COMMENT ON COLUMN tmpl.depo.tr_type_id IS 'Вид транспорта'
;
COMMENT ON COLUMN tmpl.depo.wkt_geom IS 'Координаты'
;
COMMENT ON COLUMN tmpl.depo.geom IS 'геометрия'
;
COMMENT ON COLUMN tmpl.depo.sign_deleted IS 'удален'
;
COMMENT ON COLUMN tmpl.depo.sys_period IS NULL
;

-- Create indexes for table tmpl.depo

CREATE INDEX ix_depo_carrier_id ON tmpl.depo (carrier_id)
TABLESPACE core_idx
;

CREATE UNIQUE INDEX uq_depo_tn_instance ON tmpl.depo (tn_instance)
TABLESPACE core_idx
;

CREATE INDEX ix_depo_park_muid ON tmpl.depo (park_muid)
TABLESPACE core_idx
;
COMMENT ON INDEX tmpl.ix_depo_carrier_id IS NULL
;
COMMENT ON INDEX tmpl.uq_depo_tn_instance IS NULL
;
COMMENT ON INDEX tmpl.ix_depo_park_muid IS NULL
;

-- Add keys for table tmpl.depo

ALTER TABLE tmpl.depo ADD CONSTRAINT pk_depo PRIMARY KEY (depo_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_depo ON tmpl.depo IS NULL
;

-- Table tmpl.tr_model

CREATE TABLE tmpl.tr_model(
 tr_model_id BigSerial NOT NULL,
 name Text NOT NULL,
 tr_mark_id Bigint NOT NULL,
 tr_capacity_id Smallint NOT NULL,
 has_low_floor Boolean,
 has_conditioner Boolean,
 has_wheelchair_space Boolean,
 has_bicicle_space Boolean,
 seat_qty Integer,
 seat_qty_disabled_people Integer,
 seat_qty_total Integer NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.tr_model IS 'Модель ТС'
;
COMMENT ON COLUMN tmpl.tr_model.tr_model_id IS NULL
;
COMMENT ON COLUMN tmpl.tr_model.name IS 'Наименование модели ТС'
;
COMMENT ON COLUMN tmpl.tr_model.tr_mark_id IS 'Марка ТС'
;
COMMENT ON COLUMN tmpl.tr_model.tr_capacity_id IS 'вместимость ТС'
;
COMMENT ON COLUMN tmpl.tr_model.has_low_floor IS 'признак низкопольности'
;
COMMENT ON COLUMN tmpl.tr_model.has_conditioner IS 'признак наличия кондиционера'
;
COMMENT ON COLUMN tmpl.tr_model.has_wheelchair_space IS 'признак наличия площадки (и/или специальных креплений) для инвалидной коляски'
;
COMMENT ON COLUMN tmpl.tr_model.has_bicicle_space IS 'признак наличия площадки для велосипеда;'
;
COMMENT ON COLUMN tmpl.tr_model.seat_qty IS 'количество сидячих мест'
;
COMMENT ON COLUMN tmpl.tr_model.seat_qty_disabled_people IS 'количество сидячих мест для инвалидов'
;
COMMENT ON COLUMN tmpl.tr_model.seat_qty_total IS 'общее число место мест в ТС'
;

-- Create indexes for table tmpl.tr_model

CREATE INDEX ix_tr_model_tr_mark_id ON tmpl.tr_model (tr_mark_id)
TABLESPACE core_idx
;

CREATE INDEX ix_tr_model_tr_capacity_id ON tmpl.tr_model (tr_capacity_id)
TABLESPACE core_idx
;
COMMENT ON INDEX tmpl.ix_tr_model_tr_mark_id IS NULL
;
COMMENT ON INDEX tmpl.ix_tr_model_tr_capacity_id IS NULL
;

-- Add keys for table tmpl.tr_model

ALTER TABLE tmpl.tr_model ADD CONSTRAINT tr_model_tr_model_id PRIMARY KEY (tr_model_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT tr_model_tr_model_id ON tmpl.tr_model IS NULL
;

-- Table tmpl.tr_status

CREATE TABLE tmpl.tr_status(
 tr_status_id BigSerial NOT NULL,
 name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.tr_status IS 'Статус ТС'
;
COMMENT ON COLUMN tmpl.tr_status.tr_status_id IS 'Идентификатор Статус ТС'
;
COMMENT ON COLUMN tmpl.tr_status.name IS 'Статус ТС'
;

-- Add keys for table tmpl.tr_status

ALTER TABLE tmpl.tr_status ADD CONSTRAINT pk_tr_status_tr_status_id PRIMARY KEY (tr_status_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_tr_status_tr_status_id ON tmpl.tr_status IS NULL
;

-- Table tmpl.equipment_model

CREATE TABLE tmpl.equipment_model(
 equipment_model_id Serial NOT NULL,
 name Text NOT NULL,
 equipment_type_id Smallint NOT NULL,
 facility_id Bigint,
 has_sim Boolean DEFAULT false,
 has_diagnostic Boolean DEFAULT true NOT NULL,
 input_digital Smallint,
 input_analog Smallint
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.equipment_model IS 'МодельБО'
;
COMMENT ON COLUMN tmpl.equipment_model.equipment_model_id IS 'Модель БО'
;
COMMENT ON COLUMN tmpl.equipment_model.name IS 'Наименование модели'
;
COMMENT ON COLUMN tmpl.equipment_model.equipment_type_id IS 'Тип БО'
;
COMMENT ON COLUMN tmpl.equipment_model.facility_id IS 'Производитель'
;
COMMENT ON COLUMN tmpl.equipment_model.has_sim IS 'Наличие sim-карты'
;
COMMENT ON COLUMN tmpl.equipment_model.has_diagnostic IS 'Диагностика включена'
;
COMMENT ON COLUMN tmpl.equipment_model.input_digital IS 'Количество цифровых входов'
;
COMMENT ON COLUMN tmpl.equipment_model.input_analog IS 'Количество аналоговых входов'
;

-- Create indexes for table tmpl.equipment_model

CREATE INDEX ix_equipment_type_id ON tmpl.equipment_model (equipment_type_id)
TABLESPACE core_idx
;

CREATE INDEX ix_facility_id ON tmpl.equipment_model (facility_id)
TABLESPACE core_idx
;
COMMENT ON INDEX tmpl.ix_equipment_type_id IS NULL
;
COMMENT ON INDEX tmpl.ix_facility_id IS NULL
;

-- Add keys for table tmpl.equipment_model

ALTER TABLE tmpl.equipment_model ADD CONSTRAINT pk_equipment_model PRIMARY KEY (equipment_model_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_equipment_model ON tmpl.equipment_model IS NULL
;

-- Table tmpl.facility_type

CREATE TABLE tmpl.facility_type(
 facility_type_id BigSerial NOT NULL,
 name Text
)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.facility_type IS 'Типы организаций'
;
COMMENT ON COLUMN tmpl.facility_type.facility_type_id IS 'Идентификатор'
;
COMMENT ON COLUMN tmpl.facility_type.name IS NULL
;

-- Add keys for table tmpl.facility_type

ALTER TABLE tmpl.facility_type ADD CONSTRAINT pk_facility_type PRIMARY KEY (facility_type_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_facility_type ON tmpl.facility_type IS NULL
;

-- Table tmpl.equipment

CREATE TABLE tmpl.equipment(
 equipment_id BigSerial NOT NULL,
 firmware_id Bigint,
 unit_service_type_id Smallint NOT NULL,
 facility_id Bigint NOT NULL,
 equipment_status_id Smallint NOT NULL,
 dt_begin Timestamp NOT NULL,
 serial_num Text,
 equipment_model_id Integer,
 depo_id Bigint NOT NULL,
 territory_id Bigint,
 unit_service_facility_id Bigint,
 has_diagnostic Boolean DEFAULT true NOT NULL,
 comment Text,
 external_id Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.equipment IS 'equipment'
;
COMMENT ON COLUMN tmpl.equipment.equipment_id IS 'Оборудование'
;
COMMENT ON COLUMN tmpl.equipment.firmware_id IS 'Прошивка БО'
;
COMMENT ON COLUMN tmpl.equipment.unit_service_type_id IS 'Вид тех. обслуживания'
;
COMMENT ON COLUMN tmpl.equipment.facility_id IS 'Производитель'
;
COMMENT ON COLUMN tmpl.equipment.equipment_status_id IS 'Статус'
;
COMMENT ON COLUMN tmpl.equipment.dt_begin IS 'Дата создания'
;
COMMENT ON COLUMN tmpl.equipment.serial_num IS 'Серийный номер'
;
COMMENT ON COLUMN tmpl.equipment.equipment_model_id IS 'Модель БО'
;
COMMENT ON COLUMN tmpl.equipment.depo_id IS 'Транспортное предприятие'
;
COMMENT ON COLUMN tmpl.equipment.territory_id IS 'Территория транспортного предприятия'
;
COMMENT ON COLUMN tmpl.equipment.unit_service_facility_id IS 'Обслуживающая организация
'
;
COMMENT ON COLUMN tmpl.equipment.has_diagnostic IS 'Диагностика включена'
;
COMMENT ON COLUMN tmpl.equipment.comment IS 'Комментарий'
;
COMMENT ON COLUMN tmpl.equipment.external_id IS 'Внешний идентификатор'
;

-- Create indexes for table tmpl.equipment

CREATE INDEX ix_equipment_firmware_id ON tmpl.equipment (firmware_id)
TABLESPACE core_idx
;

CREATE INDEX ix_equipment_unit_service_type_id ON tmpl.equipment (unit_service_type_id)
TABLESPACE core_idx
;

CREATE INDEX ix_equipment_facility_id ON tmpl.equipment (facility_id)
TABLESPACE core_idx
;

CREATE INDEX ix_equipment_equipment_status_id ON tmpl.equipment (equipment_status_id)
TABLESPACE core_idx
;

CREATE INDEX ix_equipment_equipment_model_id ON tmpl.equipment (equipment_model_id)
TABLESPACE core_idx
;

CREATE INDEX ix_equipment_depo_id ON tmpl.equipment (depo_id)
TABLESPACE core_idx
;

CREATE INDEX ix_equipment_territory_id ON tmpl.equipment (territory_id)
TABLESPACE core_idx
;

CREATE INDEX ix_equipment_unit_service_facility_id ON tmpl.equipment (unit_service_facility_id)
TABLESPACE core_idx
;
COMMENT ON INDEX tmpl.ix_equipment_firmware_id IS NULL
;
COMMENT ON INDEX tmpl.ix_equipment_unit_service_type_id IS NULL
;
COMMENT ON INDEX tmpl.ix_equipment_facility_id IS NULL
;
COMMENT ON INDEX tmpl.ix_equipment_equipment_status_id IS NULL
;
COMMENT ON INDEX tmpl.ix_equipment_equipment_model_id IS NULL
;
COMMENT ON INDEX tmpl.ix_equipment_depo_id IS NULL
;
COMMENT ON INDEX tmpl.ix_equipment_territory_id IS NULL
;
COMMENT ON INDEX tmpl.ix_equipment_unit_service_facility_id IS NULL
;

-- Add keys for table tmpl.equipment

ALTER TABLE tmpl.equipment ADD CONSTRAINT pk_equipment PRIMARY KEY (equipment_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_equipment ON tmpl.equipment IS NULL
;

-- Table tmpl.sensor2unit

CREATE TABLE tmpl.sensor2unit(
 sensor_id Bigint NOT NULL,
 unit_id Bigint NOT NULL,
 sensor_num Bigint NOT NULL,
 sys_period tstzrange DEFAULT tstzrange(now(), NULL::timestamp with time zone) NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.sensor2unit IS NULL
;
COMMENT ON COLUMN tmpl.sensor2unit.sensor_id IS NULL
;
COMMENT ON COLUMN tmpl.sensor2unit.unit_id IS 'Бортовой блок'
;
COMMENT ON COLUMN tmpl.sensor2unit.sensor_num IS 'Номер датчика'
;
COMMENT ON COLUMN tmpl.sensor2unit.sys_period IS NULL
;

-- Create indexes for table tmpl.sensor2unit

CREATE UNIQUE INDEX ux_unit_id_sensor_num ON tmpl.sensor2unit (unit_id,sensor_num)
TABLESPACE core_idx
;

CREATE INDEX ix_sensor2unit_sys_period ON tmpl.sensor2unit USING gist (sys_period)
TABLESPACE core_idx
;
COMMENT ON INDEX tmpl.ux_unit_id_sensor_num IS NULL
;
COMMENT ON INDEX tmpl.ix_sensor2unit_sys_period IS NULL
;

-- Add keys for table tmpl.sensor2unit

ALTER TABLE tmpl.sensor2unit ADD CONSTRAINT pk_sensor2unit_sensor_id PRIMARY KEY (sensor_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_sensor2unit_sensor_id ON tmpl.sensor2unit IS NULL
;

-- Table tmpl.tr_type

CREATE TABLE tmpl.tr_type(
 tr_type_id Smallserial NOT NULL,
 name Text NOT NULL,
 transport_kind_muid Bigint,
 short_name Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.tr_type IS 'Вид ТС'
;
COMMENT ON COLUMN tmpl.tr_type.tr_type_id IS 'Вид ТС'
;
COMMENT ON COLUMN tmpl.tr_type.name IS 'Наименование Вид ТС'
;
COMMENT ON COLUMN tmpl.tr_type.transport_kind_muid IS 'Вид ТС  из ГИС'
;
COMMENT ON COLUMN tmpl.tr_type.short_name IS 'Краткое наименование'
;

-- Create indexes for table tmpl.tr_type

CREATE UNIQUE INDEX ix_transport_kind_muid ON tmpl.tr_type (transport_kind_muid)
TABLESPACE core_idx
;
COMMENT ON INDEX tmpl.ix_transport_kind_muid IS NULL
;

-- Add keys for table tmpl.tr_type

ALTER TABLE tmpl.tr_type ADD CONSTRAINT pk_tr_type PRIMARY KEY (tr_type_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_tr_type ON tmpl.tr_type IS NULL
;

-- Table tmpl.equipment_class

CREATE TABLE tmpl.equipment_class(
 equipment_class_id Smallserial NOT NULL,
 name Text NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.equipment_class IS 'Класс типа БО'
;
COMMENT ON COLUMN tmpl.equipment_class.equipment_class_id IS 'Класс типа БО'
;
COMMENT ON COLUMN tmpl.equipment_class.name IS 'Наименование'
;

-- Add keys for table tmpl.equipment_class

ALTER TABLE tmpl.equipment_class ADD CONSTRAINT pk_equipment_class_id PRIMARY KEY (equipment_class_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_equipment_class_id ON tmpl.equipment_class IS NULL
;

-- Table tmpl.unit2tr

CREATE TABLE tmpl.unit2tr(
 tr_id Bigint NOT NULL,
 unit_id Bigint NOT NULL,
 sys_period tstzrange DEFAULT tstzrange(now(), NULL::timestamp with time zone) NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.unit2tr IS 'Блоки в транспортном средстве'
;
COMMENT ON COLUMN tmpl.unit2tr.tr_id IS 'ТС'
;
COMMENT ON COLUMN tmpl.unit2tr.unit_id IS NULL
;
COMMENT ON COLUMN tmpl.unit2tr.sys_period IS NULL
;

-- Create indexes for table tmpl.unit2tr

CREATE INDEX ix_unit2tr_tr_id ON tmpl.unit2tr (tr_id)
TABLESPACE core_idx
;

CREATE INDEX ix_unit2tr_sys_period ON tmpl.unit2tr USING gist (sys_period)
TABLESPACE core_idx
;
COMMENT ON INDEX tmpl.ix_unit2tr_tr_id IS NULL
;
COMMENT ON INDEX tmpl.ix_unit2tr_sys_period IS NULL
;

-- Add keys for table tmpl.unit2tr

ALTER TABLE tmpl.unit2tr ADD CONSTRAINT unit2tr_unit_id_pk PRIMARY KEY (unit_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT unit2tr_unit_id_pk ON tmpl.unit2tr IS NULL
;

-- Table tmpl.entity

CREATE TABLE tmpl.entity(
 entity_id BigSerial NOT NULL,
 name_full Text,
 name_short Text NOT NULL,
 comment Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.entity IS 'Идентификатор реквизитов'
;
COMMENT ON COLUMN tmpl.entity.entity_id IS 'Идентификатор сущности'
;
COMMENT ON COLUMN tmpl.entity.name_full IS 'Наименование полное'
;
COMMENT ON COLUMN tmpl.entity.name_short IS 'Наименование сокращенное'
;
COMMENT ON COLUMN tmpl.entity.comment IS 'Дополнительно'
;

-- Add keys for table tmpl.entity

ALTER TABLE tmpl.entity ADD CONSTRAINT pk_entity PRIMARY KEY (entity_id)
 USING INDEX TABLESPACE core_idx
;

CREATE UNIQUE INDEX ux_entity_name_short ON core.entity (name_short)
TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_entity ON tmpl.entity IS NULL
;

-- Table tmpl.carrier

CREATE TABLE tmpl.carrier(
 carrier_id Bigint NOT NULL,
 sign_deleted Bigint NOT NULL,
 update_date Timestamp NOT NULL,
 agencies_muid Bigint,
 org_legal_form_id Smallint
)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.carrier IS 'Транспортная организация'
;
COMMENT ON COLUMN tmpl.carrier.carrier_id IS NULL
;
COMMENT ON COLUMN tmpl.carrier.sign_deleted IS NULL
;
COMMENT ON COLUMN tmpl.carrier.update_date IS NULL
;
COMMENT ON COLUMN tmpl.carrier.agencies_muid IS 'связь с ГИС'
;
COMMENT ON COLUMN tmpl.carrier.org_legal_form_id IS 'Организационно-правовая форма'
;

-- Create indexes for table tmpl.carrier

CREATE INDEX ix_carrier_org_legal_form_id ON tmpl.carrier (org_legal_form_id)
TABLESPACE core_idx
;

CREATE INDEX ix_carrier_agencies_muid ON tmpl.carrier (agencies_muid)
TABLESPACE core_idx
;
COMMENT ON INDEX tmpl.ix_carrier_org_legal_form_id IS NULL
;
COMMENT ON INDEX tmpl.ix_carrier_agencies_muid IS NULL
;

-- Add keys for table tmpl.carrier

ALTER TABLE tmpl.carrier ADD CONSTRAINT pk_carrier PRIMARY KEY (carrier_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_carrier ON tmpl.carrier IS NULL
;

-- Table tmpl.depo2territory

CREATE TABLE tmpl.depo2territory(
 territory_id Bigint NOT NULL,
 depo_id Bigint NOT NULL,
 dt_begin Timestamp,
 dt_end Timestamp
)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.depo2territory IS 'Связь м/у парками и территориями'
;
COMMENT ON COLUMN tmpl.depo2territory.territory_id IS 'Территория'
;
COMMENT ON COLUMN tmpl.depo2territory.depo_id IS 'Парк'
;
COMMENT ON COLUMN tmpl.depo2territory.dt_begin IS 'Дата начала'
;
COMMENT ON COLUMN tmpl.depo2territory.dt_end IS 'Дата окончания'
;

-- Add keys for table tmpl.depo2territory

ALTER TABLE tmpl.depo2territory ADD CONSTRAINT pk_depo2territory PRIMARY KEY (territory_id,depo_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_depo2territory ON tmpl.depo2territory IS NULL
;

-- Table tmpl.facility2type

CREATE TABLE tmpl.facility2type(
 facility_id Bigint NOT NULL,
 facility_type_id Bigint NOT NULL
)
TABLESPACE core_data
;

COMMENT ON TABLE tmpl.facility2type IS 'Связь контрагентов с типами организаций'
;
COMMENT ON COLUMN tmpl.facility2type.facility_id IS 'Контрагент
'
;
COMMENT ON COLUMN tmpl.facility2type.facility_type_id IS 'Тип организации
'
;

-- Add keys for table tmpl.facility2type

ALTER TABLE tmpl.facility2type ADD CONSTRAINT pk_facility2type PRIMARY KEY (facility_id,facility_type_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_facility2type ON tmpl.facility2type IS NULL
;

-- Table tmpl.modem

CREATE TABLE tmpl.modem(
 modem_id Bigint NOT NULL,
 imei Text NOT NULL,
 speed Text NOT NULL,
 sim_card_id Bigint
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE tmpl.modem ALTER COLUMN modem_id SET STORAGE PLAIN
;
ALTER TABLE tmpl.modem ALTER COLUMN imei SET STORAGE EXTENDED
;
ALTER TABLE tmpl.modem ALTER COLUMN speed SET STORAGE EXTENDED
;
ALTER TABLE tmpl.modem ALTER COLUMN sim_card_id SET STORAGE PLAIN
;

COMMENT ON TABLE tmpl.modem IS NULL
;
COMMENT ON COLUMN tmpl.modem.modem_id IS NULL
;
COMMENT ON COLUMN tmpl.modem.imei IS NULL
;
COMMENT ON COLUMN tmpl.modem.speed IS NULL
;
COMMENT ON COLUMN tmpl.modem.sim_card_id IS NULL
;

-- Create indexes for table tmpl.modem

CREATE INDEX ix_modem_sim_card_id ON tmpl.modem (sim_card_id)
TABLESPACE core_idx
;
COMMENT ON INDEX tmpl.ix_modem_sim_card_id IS NULL
;

-- Add keys for table tmpl.modem

ALTER TABLE tmpl.modem ADD CONSTRAINT pk_modem_modem_id PRIMARY KEY (modem_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_modem_modem_id ON tmpl.modem IS NULL
;

-- Table tmpl.modem2unit

CREATE TABLE tmpl.modem2unit(
 unit_id Bigint NOT NULL,
 modem_id Bigint NOT NULL,
 sys_period tstzrange DEFAULT tstzrange(now(), NULL::timestamp with time zone) NOT NULL
)
TABLESPACE core_data
;
ALTER TABLE tmpl.modem2unit ALTER COLUMN unit_id SET STORAGE PLAIN
;
ALTER TABLE tmpl.modem2unit ALTER COLUMN modem_id SET STORAGE PLAIN
;

COMMENT ON TABLE tmpl.modem2unit IS NULL
;
COMMENT ON COLUMN tmpl.modem2unit.unit_id IS NULL
;
COMMENT ON COLUMN tmpl.modem2unit.modem_id IS NULL
;
COMMENT ON COLUMN tmpl.modem2unit.sys_period IS NULL
;

-- Create indexes for table tmpl.modem2unit

CREATE INDEX ix_modem2unit_sys_period ON tmpl.modem2unit USING gist (sys_period)
TABLESPACE core_idx
;
COMMENT ON INDEX tmpl.ix_modem2unit_sys_period IS NULL
;

-- Add keys for table tmpl.modem2unit

ALTER TABLE tmpl.modem2unit ADD CONSTRAINT pk_modem2unit_unit_id_modem_id PRIMARY KEY (unit_id,modem_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_modem2unit_unit_id_modem_id ON tmpl.modem2unit IS NULL
;

-- Table tmpl.monitor

CREATE TABLE tmpl.monitor(
 monitor_id Bigint NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE tmpl.monitor ALTER COLUMN monitor_id SET STORAGE PLAIN
;

COMMENT ON TABLE tmpl.monitor IS NULL
;
COMMENT ON COLUMN tmpl.monitor.monitor_id IS NULL
;

-- Add keys for table tmpl.monitor

ALTER TABLE tmpl.monitor ADD CONSTRAINT pk_monitor_monitor_id PRIMARY KEY (monitor_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_monitor_monitor_id ON tmpl.monitor IS NULL
;

-- Table tmpl.monitor2unit

CREATE TABLE tmpl.monitor2unit(
 unit_id Bigint NOT NULL,
 monitor_id Bigint NOT NULL,
 sys_period tstzrange DEFAULT tstzrange(now(), NULL::timestamp with time zone) NOT NULL
)
TABLESPACE core_data
;
ALTER TABLE tmpl.monitor2unit ALTER COLUMN unit_id SET STORAGE PLAIN
;
ALTER TABLE tmpl.monitor2unit ALTER COLUMN monitor_id SET STORAGE PLAIN
;

COMMENT ON TABLE tmpl.monitor2unit IS NULL
;
COMMENT ON COLUMN tmpl.monitor2unit.unit_id IS NULL
;
COMMENT ON COLUMN tmpl.monitor2unit.monitor_id IS NULL
;
COMMENT ON COLUMN tmpl.monitor2unit.sys_period IS NULL
;

-- Create indexes for table tmpl.monitor2unit

CREATE INDEX ix_monitor2unit_sys_period ON tmpl.monitor2unit USING gist (sys_period)
TABLESPACE core_idx
;
COMMENT ON INDEX tmpl.ix_monitor2unit_sys_period IS NULL
;

-- Add keys for table tmpl.monitor2unit

ALTER TABLE tmpl.monitor2unit ADD CONSTRAINT pk_monitor2unit_unit_id_monitor_id PRIMARY KEY (unit_id,monitor_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_monitor2unit_unit_id_monitor_id ON tmpl.monitor2unit IS NULL
;

-- Table tmpl.hdd

CREATE TABLE tmpl.hdd(
 type Integer,
 capacity Double precision,
 size Double precision,
 hdd_id Bigint NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE tmpl.hdd ALTER COLUMN type SET STORAGE PLAIN
;
ALTER TABLE tmpl.hdd ALTER COLUMN capacity SET STORAGE PLAIN
;
ALTER TABLE tmpl.hdd ALTER COLUMN size SET STORAGE PLAIN
;
ALTER TABLE tmpl.hdd ALTER COLUMN hdd_id SET STORAGE PLAIN
;

COMMENT ON TABLE tmpl.hdd IS NULL
;
COMMENT ON COLUMN tmpl.hdd.type IS 'Тип диска: 1 - НМЖД или 2 -твердотельный;'
;
COMMENT ON COLUMN tmpl.hdd.capacity IS 'Объем диска, Гб'
;
COMMENT ON COLUMN tmpl.hdd.size IS 'Типоразмер диска, дюйм'
;
COMMENT ON COLUMN tmpl.hdd.hdd_id IS NULL
;

-- Add keys for table tmpl.hdd

ALTER TABLE tmpl.hdd ADD CONSTRAINT pk_hdd_hdd_id PRIMARY KEY (hdd_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_hdd_hdd_id ON tmpl.hdd IS NULL
;

-- Table tmpl.hdd2unit

CREATE TABLE tmpl.hdd2unit(
 unit_id Bigint NOT NULL,
 hdd_id Bigint NOT NULL,
 sys_period tstzrange DEFAULT tstzrange(now(), NULL::timestamp with time zone) NOT NULL
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE tmpl.hdd2unit ALTER COLUMN unit_id SET STORAGE PLAIN
;
ALTER TABLE tmpl.hdd2unit ALTER COLUMN hdd_id SET STORAGE PLAIN
;

COMMENT ON TABLE tmpl.hdd2unit IS NULL
;
COMMENT ON COLUMN tmpl.hdd2unit.unit_id IS NULL
;
COMMENT ON COLUMN tmpl.hdd2unit.hdd_id IS NULL
;
COMMENT ON COLUMN tmpl.hdd2unit.sys_period IS NULL
;

-- Create indexes for table tmpl.hdd2unit

CREATE INDEX ix_hdd2unit_sys_period ON tmpl.hdd2unit USING gist (sys_period)
TABLESPACE core_idx
;
COMMENT ON INDEX tmpl.ix_hdd2unit_sys_period IS NULL
;

-- Add keys for table tmpl.hdd2unit

ALTER TABLE tmpl.hdd2unit ADD CONSTRAINT pk_unit_id_hd_id PRIMARY KEY (unit_id,hdd_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_unit_id_hd_id ON tmpl.hdd2unit IS NULL
;

-- Table tmpl.sensor2tr

CREATE TABLE tmpl.sensor2tr(
 sensor_id Bigint NOT NULL,
 tr_id Bigint NOT NULL,
 installation_site_id Bigint
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE tmpl.sensor2tr ALTER COLUMN sensor_id SET STORAGE PLAIN
;
ALTER TABLE tmpl.sensor2tr ALTER COLUMN tr_id SET STORAGE PLAIN
;
ALTER TABLE tmpl.sensor2tr ALTER COLUMN installation_site_id SET STORAGE PLAIN
;

COMMENT ON TABLE tmpl.sensor2tr IS NULL
;
COMMENT ON COLUMN tmpl.sensor2tr.sensor_id IS NULL
;
COMMENT ON COLUMN tmpl.sensor2tr.tr_id IS NULL
;
COMMENT ON COLUMN tmpl.sensor2tr.installation_site_id IS NULL
;

-- Create indexes for table tmpl.sensor2tr

CREATE INDEX ix_sensor2tr_installation_site_id ON tmpl.sensor2tr (installation_site_id)
TABLESPACE core_idx
;

CREATE INDEX ix_sensor2tr_installation_tr_id ON tmpl.sensor2tr (tr_id)
TABLESPACE core_idx
;
COMMENT ON INDEX tmpl.ix_sensor2tr_installation_site_id IS NULL
;
COMMENT ON INDEX tmpl.ix_sensor2tr_installation_tr_id IS NULL
;

-- Add keys for table tmpl.sensor2tr

ALTER TABLE tmpl.sensor2tr ADD CONSTRAINT pk_sensor2tr_sensor_id PRIMARY KEY (sensor_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_sensor2tr_sensor_id ON tmpl.sensor2tr IS NULL
;

-- Table tmpl.tr_schema_install

CREATE TABLE tmpl.tr_schema_install(
 tr_schema_install_id Serial NOT NULL,
 tr_type_id Smallint NOT NULL,
 tr_capacity_id Smallint NOT NULL,
 install_name Text NOT NULL,
 install_desc Text,
 dt_begin Timestamp DEFAULT now() NOT NULL,
 is_active Boolean DEFAULT true NOT NULL
)
TABLESPACE core_data
;
ALTER TABLE tmpl.tr_schema_install ALTER COLUMN tr_schema_install_id SET STORAGE PLAIN
;
ALTER TABLE tmpl.tr_schema_install ALTER COLUMN tr_type_id SET STORAGE PLAIN
;
ALTER TABLE tmpl.tr_schema_install ALTER COLUMN tr_capacity_id SET STORAGE PLAIN
;
ALTER TABLE tmpl.tr_schema_install ALTER COLUMN install_name SET STORAGE EXTENDED
;
ALTER TABLE tmpl.tr_schema_install ALTER COLUMN install_desc SET STORAGE EXTENDED
;
ALTER TABLE tmpl.tr_schema_install ALTER COLUMN dt_begin SET STORAGE PLAIN
;
ALTER TABLE tmpl.tr_schema_install ALTER COLUMN is_active SET STORAGE PLAIN
;

COMMENT ON TABLE tmpl.tr_schema_install IS 'Схема подключения БО к видам ТС'
;
COMMENT ON COLUMN tmpl.tr_schema_install.tr_schema_install_id IS NULL
;
COMMENT ON COLUMN tmpl.tr_schema_install.tr_type_id IS 'Вид транспорта
'
;
COMMENT ON COLUMN tmpl.tr_schema_install.tr_capacity_id IS 'Вместимость
'
;
COMMENT ON COLUMN tmpl.tr_schema_install.install_name IS 'Наименование схемы подключения
'
;
COMMENT ON COLUMN tmpl.tr_schema_install.install_desc IS 'Комментарий'
;
COMMENT ON COLUMN tmpl.tr_schema_install.dt_begin IS 'Дата ввода в действие'
;
COMMENT ON COLUMN tmpl.tr_schema_install.is_active IS 'Активная'
;

-- Add keys for table tmpl.tr_schema_install

ALTER TABLE tmpl.tr_schema_install ADD CONSTRAINT pk_tr_schema_install PRIMARY KEY (tr_schema_install_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_tr_schema_install ON tmpl.tr_schema_install IS NULL
;

-- Table tmpl.tr_schema_install_detail

CREATE TABLE tmpl.tr_schema_install_detail(
 tr_schema_install_detail_id Serial NOT NULL,
 sensor2unit_model_id Integer NOT NULL,
 is_required Boolean DEFAULT true NOT NULL,
 install_desc Text,
 tr_schema_install_unit_id Integer
)
TABLESPACE core_data
;
ALTER TABLE tmpl.tr_schema_install_detail ALTER COLUMN tr_schema_install_detail_id SET STORAGE PLAIN
;
ALTER TABLE tmpl.tr_schema_install_detail ALTER COLUMN sensor2unit_model_id SET STORAGE PLAIN
;
ALTER TABLE tmpl.tr_schema_install_detail ALTER COLUMN is_required SET STORAGE PLAIN
;
ALTER TABLE tmpl.tr_schema_install_detail ALTER COLUMN install_desc SET STORAGE EXTENDED
;

COMMENT ON TABLE tmpl.tr_schema_install_detail IS 'Детали подключения оборудования в схеме подключения к видам ТС'
;
COMMENT ON COLUMN tmpl.tr_schema_install_detail.tr_schema_install_detail_id IS NULL
;
COMMENT ON COLUMN tmpl.tr_schema_install_detail.sensor2unit_model_id IS 'Схема подключения БО к блокам'
;
COMMENT ON COLUMN tmpl.tr_schema_install_detail.is_required IS 'обязательно для подключения'
;
COMMENT ON COLUMN tmpl.tr_schema_install_detail.install_desc IS 'Комментарий'
;
COMMENT ON COLUMN tmpl.tr_schema_install_detail.tr_schema_install_unit_id IS NULL
;

-- Create indexes for table tmpl.tr_schema_install_detail

CREATE INDEX ix_tr_schema_install_detail_install_unit ON tmpl.tr_schema_install_detail (tr_schema_install_unit_id)
TABLESPACE core_idx
;
COMMENT ON INDEX tmpl.ix_tr_schema_install_detail_install_unit IS NULL
;

-- Add keys for table tmpl.tr_schema_install_detail

ALTER TABLE tmpl.tr_schema_install_detail ADD CONSTRAINT pk_tr_schema_install_detail PRIMARY KEY (tr_schema_install_detail_id)
;
COMMENT ON CONSTRAINT pk_tr_schema_install_detail ON tmpl.tr_schema_install_detail IS NULL
;

-- Table tmpl.tr_schema_install_unit

CREATE TABLE tmpl.tr_schema_install_unit(
 tr_schema_install_unit_id Serial NOT NULL,
 equipment_type_id Smallint NOT NULL,
 tr_schema_install_id Integer NOT NULL,
 is_main Boolean NOT NULL
)
TABLESPACE core_idx
;

COMMENT ON TABLE tmpl.tr_schema_install_unit IS 'ББ в схеме подключения'
;
COMMENT ON COLUMN tmpl.tr_schema_install_unit.tr_schema_install_unit_id IS NULL
;
COMMENT ON COLUMN tmpl.tr_schema_install_unit.equipment_type_id IS 'ББ'
;
COMMENT ON COLUMN tmpl.tr_schema_install_unit.tr_schema_install_id IS 'Схема подключения'
;
COMMENT ON COLUMN tmpl.tr_schema_install_unit.is_main IS 'Признак основного блока'
;

-- Create indexes for table tmpl.tr_schema_install_unit

CREATE INDEX ix_tr_schema_install_unit_eqtype ON tmpl.tr_schema_install_unit (equipment_type_id)
TABLESPACE core_idx
;

CREATE INDEX ix_tr_schema_install_unit_shema_install ON tmpl.tr_schema_install_unit (tr_schema_install_id)
TABLESPACE core_idx
;
COMMENT ON INDEX tmpl.ix_tr_schema_install_unit_eqtype IS NULL
;
COMMENT ON INDEX tmpl.ix_tr_schema_install_unit_shema_install IS NULL
;

-- Add keys for table tmpl.tr_schema_install_unit

ALTER TABLE tmpl.tr_schema_install_unit ADD CONSTRAINT pk_tr_schema_install_unit PRIMARY KEY (tr_schema_install_unit_id)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_tr_schema_install_unit ON tmpl.tr_schema_install_unit IS NULL
;

-- Table tmpl.sensor2unit_model

CREATE TABLE tmpl.sensor2unit_model(
 sensor2unit_model_id Serial NOT NULL,
 num Smallint NOT NULL,
 equipment_type_sensor_id Smallint NOT NULL,
 equipment_type_unit_id Smallint NOT NULL,
 is_required Boolean DEFAULT true NOT NULL,
 name_short Text
)
WITH (OIDS=TRUE)
TABLESPACE core_data
;
ALTER TABLE tmpl.sensor2unit_model ALTER COLUMN sensor2unit_model_id SET STORAGE PLAIN
;
ALTER TABLE tmpl.sensor2unit_model ALTER COLUMN num SET STORAGE PLAIN
;
ALTER TABLE tmpl.sensor2unit_model ALTER COLUMN equipment_type_sensor_id SET STORAGE PLAIN
;
ALTER TABLE tmpl.sensor2unit_model ALTER COLUMN equipment_type_unit_id SET STORAGE PLAIN
;
ALTER TABLE tmpl.sensor2unit_model ALTER COLUMN is_required SET STORAGE PLAIN
;

COMMENT ON TABLE tmpl.sensor2unit_model IS 'Схема подключения датчиков'
;
COMMENT ON COLUMN tmpl.sensor2unit_model.sensor2unit_model_id IS NULL
;
COMMENT ON COLUMN tmpl.sensor2unit_model.num IS NULL
;
COMMENT ON COLUMN tmpl.sensor2unit_model.equipment_type_sensor_id IS NULL
;
COMMENT ON COLUMN tmpl.sensor2unit_model.equipment_type_unit_id IS NULL
;
COMMENT ON COLUMN tmpl.sensor2unit_model.is_required IS NULL
;
COMMENT ON COLUMN tmpl.sensor2unit_model.name_short IS NULL
;

-- Create indexes for table tmpl.sensor2unit_model

CREATE INDEX ix_sensor2equipment_type_id ON tmpl.sensor2unit_model (equipment_type_sensor_id)
TABLESPACE core_idx
;

CREATE UNIQUE INDEX ux_equipment_type_unit_id_num ON tmpl.sensor2unit_model (equipment_type_unit_id,num)
TABLESPACE core_idx
;

CREATE INDEX IX_Relationship213 ON tmpl.sensor2unit_model (name_short)
;

CREATE INDEX IX_Relationship214 ON tmpl.sensor2unit_model (name_short)
;
COMMENT ON INDEX tmpl.ix_sensor2equipment_type_id IS NULL
;
COMMENT ON INDEX tmpl.ux_equipment_type_unit_id_num IS NULL
;
COMMENT ON INDEX tmpl.IX_Relationship213 IS NULL
;
COMMENT ON INDEX tmpl.IX_Relationship214 IS NULL
;

-- Add keys for table tmpl.sensor2unit_model

ALTER TABLE tmpl.sensor2unit_model ADD CONSTRAINT pk_sensor2unit_model_sensor2unit_model_id PRIMARY KEY (sensor2unit_model_id)
 USING INDEX TABLESPACE core_idx
;

ALTER TABLE tmpl.sensor2unit_model ADD CONSTRAINT uk_sensor2unit_model UNIQUE (equipment_type_unit_id,equipment_type_sensor_id,num)
 USING INDEX TABLESPACE core_idx
;
COMMENT ON CONSTRAINT pk_sensor2unit_model_sensor2unit_model_id ON tmpl.sensor2unit_model IS NULL
;
COMMENT ON CONSTRAINT uk_sensor2unit_model ON tmpl.sensor2unit_model IS NULL
;

-- Create relationships section -------------------------------------------------

ALTER TABLE tmpl.tr ADD CONSTRAINT fk_tr_tr_status FOREIGN KEY (tr_status_id) REFERENCES tmpl.tr_status (tr_status_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_tr_tr_status ON tmpl.tr IS NULL
;

ALTER TABLE tmpl.tr ADD CONSTRAINT fk_tr_tr_model FOREIGN KEY (tr_model_id) REFERENCES tmpl.tr_model (tr_model_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_tr_tr_model ON tmpl.tr IS NULL
;

ALTER TABLE tmpl.tr ADD CONSTRAINT fk_tr_territory FOREIGN KEY (territory_id) REFERENCES tmpl.territory (territory_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_tr_territory ON tmpl.tr IS NULL
;

ALTER TABLE tmpl.tr ADD CONSTRAINT fk_tr_tr_type FOREIGN KEY (tr_type_id) REFERENCES tmpl.tr_type (tr_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_tr_tr_type ON tmpl.tr IS NULL
;

ALTER TABLE tmpl.tr_mark ADD CONSTRAINT fk_tr_mark_facility FOREIGN KEY (facility_id) REFERENCES tmpl.facility (facility_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_tr_mark_facility ON tmpl.tr_mark IS NULL
;

ALTER TABLE tmpl.tr_model ADD CONSTRAINT fk_tr_model_tr_mark_id FOREIGN KEY (tr_mark_id) REFERENCES tmpl.tr_mark (tr_mark_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_tr_model_tr_mark_id ON tmpl.tr_model IS NULL
;

ALTER TABLE tmpl.unit2tr ADD CONSTRAINT fk_unit2tr_unit FOREIGN KEY (unit_id) REFERENCES tmpl.unit (unit_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_unit2tr_unit ON tmpl.unit2tr IS NULL
;

ALTER TABLE tmpl.unit2tr ADD CONSTRAINT fk_unit2tr_tr FOREIGN KEY (tr_id) REFERENCES tmpl.tr (tr_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_unit2tr_tr ON tmpl.unit2tr IS NULL
;

ALTER TABLE tmpl.tr_model ADD CONSTRAINT fk_tr_model_capacity FOREIGN KEY (tr_capacity_id) REFERENCES tmpl.tr_capacity (tr_capacity_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_tr_model_capacity ON tmpl.tr_model IS NULL
;

ALTER TABLE tmpl.depo ADD CONSTRAINT fk_depo_carrier FOREIGN KEY (carrier_id) REFERENCES tmpl.carrier (carrier_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_depo_carrier ON tmpl.depo IS NULL
;

ALTER TABLE tmpl.carrier ADD CONSTRAINT fk_carrier_entity FOREIGN KEY (carrier_id) REFERENCES tmpl.entity (entity_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_carrier_entity ON tmpl.carrier IS NULL
;

ALTER TABLE tmpl.depo ADD CONSTRAINT fk_depo_entity FOREIGN KEY (depo_id) REFERENCES tmpl.entity (entity_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_depo_entity ON tmpl.depo IS NULL
;

ALTER TABLE tmpl.territory ADD CONSTRAINT fk_territory_entity FOREIGN KEY (territory_id) REFERENCES tmpl.entity (entity_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_territory_entity ON tmpl.territory IS NULL
;

ALTER TABLE tmpl.tr ADD CONSTRAINT fk_tr_depo FOREIGN KEY (depo_id) REFERENCES tmpl.depo (depo_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_tr_depo ON tmpl.tr IS NULL
;

ALTER TABLE tmpl.depo2territory ADD CONSTRAINT fk_depo2teritory_territory FOREIGN KEY (territory_id) REFERENCES tmpl.territory (territory_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_depo2teritory_territory ON tmpl.depo2territory IS NULL
;

ALTER TABLE tmpl.equipment_model ADD CONSTRAINT fk_equipment_model_equipment_type FOREIGN KEY (equipment_type_id) REFERENCES tmpl.equipment_type (equipment_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_equipment_model_equipment_type ON tmpl.equipment_model IS NULL
;

ALTER TABLE tmpl.equipment_model ADD CONSTRAINT fk_equipment_model_facility FOREIGN KEY (facility_id) REFERENCES tmpl.facility (facility_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_equipment_model_facility ON tmpl.equipment_model IS NULL
;

ALTER TABLE tmpl.equipment_type ADD CONSTRAINT fk_equipment_type2equipment_class_id_id FOREIGN KEY (equipment_class_id) REFERENCES tmpl.equipment_class (equipment_class_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_equipment_type2equipment_class_id_id ON tmpl.equipment_type IS NULL
;

ALTER TABLE tmpl.firmware ADD CONSTRAINT fk_firmware_facility FOREIGN KEY (facility_id) REFERENCES tmpl.facility (facility_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_firmware_facility ON tmpl.firmware IS NULL
;

ALTER TABLE tmpl.firmware ADD CONSTRAINT fk_equipment_model_to_firmware FOREIGN KEY (equipment_model_id) REFERENCES tmpl.equipment_model (equipment_model_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_equipment_model_to_firmware ON tmpl.firmware IS NULL
;

ALTER TABLE tmpl.equipment ADD CONSTRAINT fk_equipment_unit_service_type FOREIGN KEY (unit_service_type_id) REFERENCES tmpl.unit_service_type (unit_service_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_equipment_unit_service_type ON tmpl.equipment IS NULL
;

ALTER TABLE tmpl.equipment ADD CONSTRAINT fk_equipment_equipment_status FOREIGN KEY (equipment_status_id) REFERENCES tmpl.equipment_status (equipment_status_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_equipment_equipment_status ON tmpl.equipment IS NULL
;

ALTER TABLE tmpl.equipment ADD CONSTRAINT fk_equipment_firmware FOREIGN KEY (firmware_id) REFERENCES tmpl.firmware (firmware_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_equipment_firmware ON tmpl.equipment IS NULL
;

ALTER TABLE tmpl.equipment ADD CONSTRAINT fk_equipment_facility FOREIGN KEY (facility_id) REFERENCES tmpl.facility (facility_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_equipment_facility ON tmpl.equipment IS NULL
;

ALTER TABLE tmpl.equipment ADD CONSTRAINT fk_equipment_equipment_model_id FOREIGN KEY (equipment_model_id) REFERENCES tmpl.equipment_model (equipment_model_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_equipment_equipment_model_id ON tmpl.equipment IS NULL
;

ALTER TABLE tmpl.equipment ADD CONSTRAINT fk_equipment_depo FOREIGN KEY (depo_id) REFERENCES tmpl.depo (depo_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_equipment_depo ON tmpl.equipment IS NULL
;

ALTER TABLE tmpl.sensor ADD CONSTRAINT fk_sensor_equipment FOREIGN KEY (sensor_id) REFERENCES tmpl.equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_sensor_equipment ON tmpl.sensor IS NULL
;

ALTER TABLE tmpl.sensor2unit ADD CONSTRAINT fk_sensor2unit_unit FOREIGN KEY (unit_id) REFERENCES tmpl.unit (unit_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_sensor2unit_unit ON tmpl.sensor2unit IS NULL
;

ALTER TABLE tmpl.sensor2unit ADD CONSTRAINT fk_sensor2unit_sensor FOREIGN KEY (sensor_id) REFERENCES tmpl.sensor (sensor_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_sensor2unit_sensor ON tmpl.sensor2unit IS NULL
;

ALTER TABLE tmpl.facility ADD CONSTRAINT fk_facility_entity FOREIGN KEY (facility_id) REFERENCES tmpl.entity (entity_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_facility_entity ON tmpl.facility IS NULL
;

ALTER TABLE tmpl.unit ADD CONSTRAINT fk_unit_equipment FOREIGN KEY (unit_id) REFERENCES tmpl.equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_unit_equipment ON tmpl.unit IS NULL
;

ALTER TABLE tmpl.sim_card2unit ADD CONSTRAINT fk_sim_card2unit_sim_card FOREIGN KEY (sim_card_id) REFERENCES tmpl.sim_card (sim_card_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_sim_card2unit_sim_card ON tmpl.sim_card2unit IS NULL
;

ALTER TABLE tmpl.sim_card2unit ADD CONSTRAINT fk_sim_card2unit_unit FOREIGN KEY (equipment_id) REFERENCES tmpl.unit (unit_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_sim_card2unit_unit ON tmpl.sim_card2unit IS NULL
;

ALTER TABLE tmpl.unit_bnsr ADD CONSTRAINT fk_unit_bnsr_unit FOREIGN KEY (unit_bnsr_id) REFERENCES tmpl.unit (unit_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_unit_bnsr_unit ON tmpl.unit_bnsr IS NULL
;

ALTER TABLE tmpl.mobile_tariff ADD CONSTRAINT fk_mobile_tariff_facility FOREIGN KEY (facility_id) REFERENCES tmpl.facility (facility_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_mobile_tariff_facility ON tmpl.mobile_tariff IS NULL
;

ALTER TABLE tmpl.sim_card ADD CONSTRAINT fk_sim_card_sim_card_group FOREIGN KEY (sim_card_group_id) REFERENCES tmpl.sim_card_group (sim_card_group_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_sim_card_sim_card_group ON tmpl.sim_card IS NULL
;

ALTER TABLE tmpl.sim_card ADD CONSTRAINT fk_sim_card_mobile_tariff FOREIGN KEY (mobile_tariff_id) REFERENCES tmpl.mobile_tariff (mobile_tariff_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_sim_card_mobile_tariff ON tmpl.sim_card IS NULL
;

ALTER TABLE tmpl.sim_card ADD CONSTRAINT fk_sim_card_equipment_status FOREIGN KEY (equipment_status_id) REFERENCES tmpl.equipment_status (equipment_status_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_sim_card_equipment_status ON tmpl.sim_card IS NULL
;

ALTER TABLE tmpl.depo ADD CONSTRAINT fk_depo_tr_type FOREIGN KEY (tr_type_id) REFERENCES tmpl.tr_type (tr_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_depo_tr_type ON tmpl.depo IS NULL
;

ALTER TABLE tmpl.facility2type ADD CONSTRAINT fk_facility2type_facility_id FOREIGN KEY (facility_id) REFERENCES tmpl.facility (facility_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_facility2type_facility_id ON tmpl.facility2type IS NULL
;

ALTER TABLE tmpl.facility2type ADD CONSTRAINT fk_facility2type_facility_type_id FOREIGN KEY (facility_type_id) REFERENCES tmpl.facility_type (facility_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_facility2type_facility_type_id ON tmpl.facility2type IS NULL
;

ALTER TABLE tmpl.modem ADD CONSTRAINT fk_sim_card_modem FOREIGN KEY (sim_card_id) REFERENCES tmpl.sim_card (sim_card_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_sim_card_modem ON tmpl.modem IS NULL
;

ALTER TABLE tmpl.modem ADD CONSTRAINT fk_modem_equipment FOREIGN KEY (modem_id) REFERENCES tmpl.equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_modem_equipment ON tmpl.modem IS NULL
;

ALTER TABLE tmpl.modem2unit ADD CONSTRAINT fk_modem2unit_unit FOREIGN KEY (unit_id) REFERENCES tmpl.unit (unit_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_modem2unit_unit ON tmpl.modem2unit IS NULL
;

ALTER TABLE tmpl.modem2unit ADD CONSTRAINT fk_modem2unit_modem FOREIGN KEY (modem_id) REFERENCES tmpl.modem (modem_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_modem2unit_modem ON tmpl.modem2unit IS NULL
;

ALTER TABLE tmpl.monitor ADD CONSTRAINT fk_monitor_equipment FOREIGN KEY (monitor_id) REFERENCES tmpl.equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_monitor_equipment ON tmpl.monitor IS NULL
;

ALTER TABLE tmpl.monitor2unit ADD CONSTRAINT fk_monitor2unit_unit FOREIGN KEY (unit_id) REFERENCES tmpl.unit (unit_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_monitor2unit_unit ON tmpl.monitor2unit IS NULL
;

ALTER TABLE tmpl.monitor2unit ADD CONSTRAINT fk_monitor2unit_monitor FOREIGN KEY (monitor_id) REFERENCES tmpl.monitor (monitor_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_monitor2unit_monitor ON tmpl.monitor2unit IS NULL
;

ALTER TABLE tmpl.hdd ADD CONSTRAINT fk_equipment_hdd FOREIGN KEY (hdd_id) REFERENCES tmpl.equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_equipment_hdd ON tmpl.hdd IS NULL
;

ALTER TABLE tmpl.hdd2unit ADD CONSTRAINT fk_hdd2unit_unit FOREIGN KEY (unit_id) REFERENCES tmpl.unit (unit_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_hdd2unit_unit ON tmpl.hdd2unit IS NULL
;

ALTER TABLE tmpl.hdd2unit ADD CONSTRAINT fk_hdd2unit_hdd FOREIGN KEY (hdd_id) REFERENCES tmpl.hdd (hdd_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_hdd2unit_hdd ON tmpl.hdd2unit IS NULL
;

ALTER TABLE tmpl.equipment ADD CONSTRAINT fk_equipment_territory FOREIGN KEY (territory_id) REFERENCES tmpl.territory (territory_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_equipment_territory ON tmpl.equipment IS NULL
;

ALTER TABLE tmpl.equipment ADD CONSTRAINT fk_equipment_unit_service_facility FOREIGN KEY (unit_service_facility_id) REFERENCES tmpl.facility (facility_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_equipment_unit_service_facility ON tmpl.equipment IS NULL
;

ALTER TABLE tmpl.sensor2tr ADD CONSTRAINT fk_sensor2tr_equipment FOREIGN KEY (sensor_id) REFERENCES tmpl.equipment (equipment_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_sensor2tr_equipment ON tmpl.sensor2tr IS NULL
;

ALTER TABLE tmpl.sensor2tr ADD CONSTRAINT fk_sensor2tr_tr FOREIGN KEY (tr_id) REFERENCES tmpl.tr (tr_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_sensor2tr_tr ON tmpl.sensor2tr IS NULL
;

ALTER TABLE tmpl.sim_card ADD CONSTRAINT fk_sim_card_depo FOREIGN KEY (depo_id) REFERENCES tmpl.depo (depo_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_sim_card_depo ON tmpl.sim_card IS NULL
;

ALTER TABLE tmpl.sim_card ADD CONSTRAINT fk_sim_card_territory FOREIGN KEY (territory_id) REFERENCES tmpl.territory (territory_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_sim_card_territory ON tmpl.sim_card IS NULL
;

ALTER TABLE tmpl.tr_schema_install ADD CONSTRAINT fk_tr_schema_install_tr_capacity FOREIGN KEY (tr_capacity_id) REFERENCES tmpl.tr_capacity (tr_capacity_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_tr_schema_install_tr_capacity ON tmpl.tr_schema_install IS NULL
;

ALTER TABLE tmpl.tr_schema_install ADD CONSTRAINT tr_schema_install_tr_type FOREIGN KEY (tr_type_id) REFERENCES tmpl.tr_type (tr_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT tr_schema_install_tr_type ON tmpl.tr_schema_install IS NULL
;

ALTER TABLE tmpl.tr_schema_install_unit ADD CONSTRAINT fk_tr_schema_install_unit_eqtype FOREIGN KEY (equipment_type_id) REFERENCES tmpl.equipment_type (equipment_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_tr_schema_install_unit_eqtype ON tmpl.tr_schema_install_unit IS NULL
;

ALTER TABLE tmpl.tr_schema_install_detail ADD CONSTRAINT fk_tr_schema_install_detail_install_unit FOREIGN KEY (tr_schema_install_unit_id) REFERENCES tmpl.tr_schema_install_unit (tr_schema_install_unit_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_tr_schema_install_detail_install_unit ON tmpl.tr_schema_install_detail IS NULL
;

ALTER TABLE tmpl.tr_schema_install_unit ADD CONSTRAINT fk_tr_schema_install_unit_schema_install FOREIGN KEY (tr_schema_install_id) REFERENCES tmpl.tr_schema_install (tr_schema_install_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_tr_schema_install_unit_schema_install ON tmpl.tr_schema_install_unit IS NULL
;

ALTER TABLE tmpl.tr ADD CONSTRAINT fk_tr_tr_schema_install FOREIGN KEY (tr_schema_install_id) REFERENCES tmpl.tr_schema_install (tr_schema_install_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_tr_tr_schema_install ON tmpl.tr IS NULL
;

ALTER TABLE tmpl.sensor2unit_model ADD CONSTRAINT fk_sensor2unit_model_equipment_type_sensor FOREIGN KEY (equipment_type_unit_id) REFERENCES tmpl.equipment_type (equipment_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_sensor2unit_model_equipment_type_sensor ON tmpl.sensor2unit_model IS NULL
;

ALTER TABLE tmpl.sensor2unit_model ADD CONSTRAINT fk_sensor2unit_model_equipment_type_unit FOREIGN KEY (equipment_type_sensor_id) REFERENCES tmpl.equipment_type (equipment_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_sensor2unit_model_equipment_type_unit ON tmpl.sensor2unit_model IS NULL
;

ALTER TABLE tmpl.tr_schema_install_detail ADD CONSTRAINT fk_tr_schema_install_detail_sensor2unit_model FOREIGN KEY (sensor2unit_model_id) REFERENCES tmpl.sensor2unit_model (sensor2unit_model_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_tr_schema_install_detail_sensor2unit_model ON tmpl.tr_schema_install_detail IS NULL
;

ALTER TABLE tmpl.depo ADD CONSTRAINT Relationship107 FOREIGN KEY (depo_id) REFERENCES tmpl.entity (entity_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship107 ON tmpl.depo IS NULL
;

ALTER TABLE tmpl.equipment ADD CONSTRAINT Relationship111 FOREIGN KEY (depo_id) REFERENCES tmpl.depo (depo_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship111 ON tmpl.equipment IS NULL
;

ALTER TABLE tmpl.tr ADD CONSTRAINT Relationship112 FOREIGN KEY (depo_id) REFERENCES tmpl.depo (depo_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT Relationship112 ON tmpl.tr IS NULL
;

ALTER TABLE tmpl.depo2territory ADD CONSTRAINT fk_depo2teritory_depo FOREIGN KEY (depo_id) REFERENCES tmpl.depo (depo_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

COMMENT ON CONSTRAINT fk_depo2teritory_depo ON tmpl.depo2territory IS NULL
;

