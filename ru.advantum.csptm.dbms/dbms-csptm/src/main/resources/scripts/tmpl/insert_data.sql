insert into tmpl.equipment_status select * from core.equipment_status;

insert into tmpl.entity select * from core.entity;

insert into tmpl.carrier(carrier_id, sign_deleted, update_date) select carrier_id, sign_deleted, update_date  from core.carrier;

insert into tmpl.tr_type select * from core.tr_type;

insert into tmpl.unit_service_type select * from core.unit_service_type;

insert into tmpl.facility_type select * from core.facility_type;

insert into tmpl.facility select * from core.facility;

insert into tmpl.facility2type select * from core.facility2type;

insert into tmpl.territory(territory_id,
                           code,
                           dt_begin,
                           dt_end,
                           wkt_geom) select
territory_id,
code,
dt_begin,
dt_end,
wkt_geom
from core.territory;

insert into tmpl.depo
select
  depo_id,
tn_instance,
  carrier_id,
  park_muid,
  tr_type_id,
  wkt_geom,
  null,
  sign_deleted,
  sys_period
from core.depo;

insert into tmpl.tr_status select * from core.tr_status;

insert into tmpl.tr_capacity select * from core.tr_capacity;

insert into tmpl.equipment_class select * from core.equipment_class;

insert into tmpl.equipment_type select * from core.equipment_type;

insert into tmpl.equipment_model select * from core.equipment_model;

insert into tmpl.firmware select * from core.firmware;

insert into tmpl.equipment select * from core.equipment;

insert into tmpl.unit select * from core.unit;

insert into tmpl.tr_schema_install select * from core.tr_schema_install;

insert into tmpl.tr_mark select * from core.tr_mark;

insert into tmpl.tr_model select * from core.tr_model;

insert into tmpl.tr select * from core.tr;

insert into tmpl.unit2tr select * from core.unit2tr;

insert into tmpl.sensor select * from core.sensor;

insert into tmpl.sensor2unit select * from core.sensor2unit;

insert into tmpl.mobile_tariff select * from core.mobile_tariff;

insert into tmpl.sim_card_group select * from core.sim_card_group;

insert into tmpl.sim_card select * from core.sim_card;

insert into tmpl.sim_card2unit select * from core.sim_card2unit;
