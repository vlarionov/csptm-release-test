/*delete from tmpl.unit_bnsr;
delete from tmpl.sim_card2unit;
delete from tmpl.sim_card;
delete from tmpl.sim_card_group;
delete from tmpl.mobile_tariff;
delete from tmpl.sensor2unit;
delete from tmpl.sensor;
delete from tmpl.unit2tr;
delete from tmpl.tr;
delete from tmpl.tr_model;
delete from tmpl.tr_mark;
delete from tmpl.tr_schema_install;
delete from tmpl.unit;
delete from tmpl.equipment;
delete from tmpl.firmware;
delete from tmpl.equipment_model;
delete from tmpl.equipment_type;
delete from tmpl.equipment_class;
delete from tmpl.tr_capacity;
delete from tmpl.tr_status;
delete from tmpl.depo;
delete from tmpl.territory;
delete from tmpl.facility2type;
delete from tmpl.facility;
delete from tmpl.facility_type;
delete from tmpl.unit_service_type;
delete from tmpl.tr_type;
delete from tmpl.carrier;
delete from tmpl.entity;
delete from tmpl.equipment_status;
*/
TRUNCATE TABLE core.unit_bnsr CASCADE;
TRUNCATE TABLE core.sim_card2unit CASCADE;
TRUNCATE TABLE core.sim_card CASCADE;
TRUNCATE TABLE core.sim_card_group CASCADE;
TRUNCATE TABLE core.mobile_tariff CASCADE;
TRUNCATE TABLE core.sensor2unit CASCADE;
TRUNCATE TABLE core.sensor CASCADE;
TRUNCATE TABLE core.unit2tr CASCADE;
TRUNCATE TABLE core.tr CASCADE;
TRUNCATE TABLE core.tr_model CASCADE;
TRUNCATE TABLE core.tr_mark CASCADE;
TRUNCATE TABLE core.tr_schema_install CASCADE;
TRUNCATE TABLE core.unit CASCADE;
TRUNCATE TABLE core.equipment CASCADE;
TRUNCATE TABLE core.firmware CASCADE;
TRUNCATE TABLE core.equipment_model CASCADE;
TRUNCATE TABLE core.equipment_type CASCADE;
TRUNCATE TABLE core.equipment_class CASCADE;
TRUNCATE TABLE core.tr_capacity CASCADE;
TRUNCATE TABLE core.tr_status CASCADE;
TRUNCATE TABLE core.depo CASCADE;
TRUNCATE TABLE core.territory CASCADE;
TRUNCATE TABLE core.facility2type CASCADE;
TRUNCATE TABLE core.facility CASCADE;
TRUNCATE TABLE core.facility_type CASCADE;
TRUNCATE TABLE core.unit_service_type CASCADE;
TRUNCATE TABLE core.tr_type CASCADE;
TRUNCATE TABLE core.carrier CASCADE;
TRUNCATE TABLE core.entity CASCADE;
TRUNCATE TABLE core.equipment_status CASCADE;


ALTER SEQUENCE core.sim_card_sim_card_id_seq RESTART  WITH 1;
ALTER SEQUENCE core.sim_card_group_sim_card_group_id_seq RESTART  WITH 1;
ALTER SEQUENCE core.mobile_tariff_mobile_tariff_id_seq RESTART  WITH 1;
ALTER SEQUENCE core.tr_model_tr_model_id_seq RESTART  WITH 1;
ALTER SEQUENCE core.tr_mark_tr_mark_id_seq RESTART  WITH 1;
ALTER SEQUENCE core.equipment_model_equipment_model_id_seq RESTART  WITH 1;
ALTER SEQUENCE core.equipment_type_equipment_type_id_seq RESTART  WITH 1;
ALTER SEQUENCE core.equipment_class_equipment_class_id_seq RESTART  WITH 1;
ALTER SEQUENCE core.tr_status_tr_status_id_seq RESTART  WITH 1;
ALTER SEQUENCE core.facility_type_facility_type_id_seq RESTART  WITH 1;
ALTER SEQUENCE core.unit_service_type_unit_service_type_id_seq RESTART  WITH 1;
ALTER SEQUENCE core.tr_type_tr_type_id_seq RESTART  WITH 1;
ALTER SEQUENCE core.entity_entity_id_seq RESTART  WITH 1;
ALTER SEQUENCE core.equipment_status_equipment_status_id_seq RESTART  WITH 1;
