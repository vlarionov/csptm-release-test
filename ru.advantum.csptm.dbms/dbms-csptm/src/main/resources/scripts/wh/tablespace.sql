create schema wh;

create tablespace wh_data location '/u00/postgres/pg_data/wh/wh_data';

create tablespace wh_idx location  '/u00/postgres/pg_data/wh/wh_idx';