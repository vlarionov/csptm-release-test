CREATE OR REPLACE FUNCTION wh.wh_core_process_pkg$of_trs_by_eq_type (
)
RETURNS void AS
$body$
declare 
 l_wh_type_id wh.wh_type.wh_type_id%type;
 l_dt 		  date;
 l_trolley_type core.tr_type.tr_type_id%type;
begin 
 l_wh_type_id := wh.wh_core_process_pkg$cn_skah_cnt();
 l_dt := current_date;
 l_trolley_type := core.tr_type_pkg$tb();
 
 delete from wh.wh_core whc
       where whc.dt = l_dt
         and whc.wh_type_id = l_wh_type_id;
 
 insert into wh.wh_core
      (wh_type_id,
       dt,
       tr_id,
       unit_id,
       tr_type_id,
       depo_id,
       territory_id,
       route_variant_id,
       equipment_type_id,
       sensor_id,
       val_int_01)
 (with cur_rounds as (select *
                        from (
                               select
                                 ol.tr_id,
                                 ol.order_num,
                                 tte.timetable_entry_num,
                                 r.muid route_muid,
                                 r.number route_number,
                                 row_number()   over (partition by ol.tr_id    order by orv.time_plan_begin) rn,
                                 dr.tab_num,
                                 rv.muid rv_muid
                               from tt.order_list ol
                                 join tt.driver d on ol.driver_id = d.driver_id
                                 join tt.order_round ord on ol.order_list_id = ord.order_list_id
                                 join asd.oper_round_visit orv on ord.order_round_id = orv.order_round_id
                                 join tt.round rnd on ord.round_id = rnd.round_id
                                 join tt.timetable_entry tte on tte.timetable_entry_id = rnd.timetable_entry_id
                                 join gis.route_trajectories rt on rnd.route_trajectory_muid = rt.muid
                                 join gis.route_rounds rr on rt.route_round_muid = rr.muid
                                 join gis.route_variants rv on rr.route_variant_muid = rv.muid
                                 join gis.routes r on rv.route_muid = r.muid
                                 left join tt.driver dr on dr.driver_id = ol.driver_id
                               where ol.order_date = now() :: date :: timestamp
                                     and ord.is_active = 1
                                     and ol.tr_id is not null
                                     ) t
                        where rn = 1) 
   select distinct
      l_wh_type_id,
      l_dt,
      tr.tr_id,
      u2t.unit_id,
      tr.tr_type_id,
      tr.depo_id,
      tr.territory_id,
      cr.rv_muid,
      em.equipment_type_id,
      s2u.sensor_id,
      snsr.check_if_unit_has_sensor_type(u2t.unit_id, core.jf_equipment_type_pkg$cn_type_skah())::int as has_sah
    from core.tr tr     
      join core.unit2tr u2t on u2t.tr_id = tr.tr_id      
      left join cur_rounds cr on tr.tr_id = cr.tr_id
      left join core.sensor2unit s2u on s2u.unit_id = u2t.unit_id
      left join core.equipment eq on eq.equipment_id = s2u.sensor_id
      left join core.equipment_model em on em.equipment_model_id = eq.equipment_model_id
  );
 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION wh.wh_core_process_pkg$of_trs_by_eq_type (a_dt date
)
RETURNS void AS
$body$
declare 
 l_wh_type_id wh.wh_type.wh_type_id%type;
 l_dt 		  date;
 l_trolley_type core.tr_type.tr_type_id%type;
begin 
 l_wh_type_id := wh.wh_core_process_pkg$cn_skah_cnt();
 l_dt := coalesce(a_dt, current_date);
 l_trolley_type := core.tr_type_pkg$tb();
 
 delete from wh.wh_core whc
       where whc.dt = l_dt
         and whc.wh_type_id = l_wh_type_id;
 
 insert into wh.wh_core
      (wh_type_id,
       dt,
       tr_id,
       unit_id,
       tr_type_id,
       depo_id,
       territory_id,
       route_variant_id,
       equipment_type_id,
       sensor_id,
       val_int_01)
 (with cur_rounds as (select *
                        from (
                               select
                                 ol.tr_id,
                                 ol.order_num,
                                 tte.timetable_entry_num,
                                 r.muid route_muid,
                                 r.number route_number,
                                 row_number()   over (partition by ol.tr_id    order by orv.time_plan_begin) rn,
                                 dr.tab_num,
                                 rv.muid rv_muid
                               from tt.order_list ol
                                 join tt.driver d on ol.driver_id = d.driver_id
                                 join tt.order_round ord on ol.order_list_id = ord.order_list_id
                                 join asd.oper_round_visit orv on ord.order_round_id = orv.order_round_id
                                 join tt.round rnd on ord.round_id = rnd.round_id
                                 join tt.timetable_entry tte on tte.timetable_entry_id = rnd.timetable_entry_id
                                 join gis.route_trajectories rt on rnd.route_trajectory_muid = rt.muid
                                 join gis.route_rounds rr on rt.route_round_muid = rr.muid
                                 join gis.route_variants rv on rr.route_variant_muid = rv.muid
                                 join gis.routes r on rv.route_muid = r.muid
                                 left join tt.driver dr on dr.driver_id = ol.driver_id
                               where ol.order_date = now() :: date :: timestamp
                                     and ord.is_active = 1
                                     and ol.tr_id is not null
                                     ) t
                        where rn = 1) 
   select distinct
      l_wh_type_id,
      l_dt,
      tr.tr_id,
      u2t.unit_id,
      tr.tr_type_id,
      tr.depo_id,
      tr.territory_id,
      cr.rv_muid,
      em.equipment_type_id,
      s2u.sensor_id,
      snsr.check_if_unit_has_sensor_type(u2t.unit_id, core.jf_equipment_type_pkg$cn_type_skah())::int as has_sah
    from core.tr tr     
      join core.unit2tr u2t on u2t.tr_id = tr.tr_id      
      left join cur_rounds cr on tr.tr_id = cr.tr_id
      left join core.sensor2unit s2u on s2u.unit_id = u2t.unit_id
      left join core.equipment eq on eq.equipment_id = s2u.sensor_id
      left join core.equipment_model em on em.equipment_model_id = eq.equipment_model_id
  );
 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION wh.wh_core_process_pkg$of_trs_by_eq_type_gr (
)
RETURNS void AS
$body$
declare 
 l_wh_type_id wh.wh_type.wh_type_id%type;
 l_dt 		  date;
 l_trolley_type core.tr_type.tr_type_id%type;
begin 
 l_wh_type_id := wh.wh_core_process_pkg$cn_skah_cnt();
 l_dt := current_date;
 l_trolley_type := core.tr_type_pkg$tb();
 
 delete from wh.wh_core whc
       where whc.dt = l_dt
         and whc.wh_type_id = l_wh_type_id;
 
 insert into wh.wh_core
      (wh_type_id,
       dt,
       depo_id,
       territory_id,
       equipment_type_id,
       val_int_01,
       val_int_02)
 (with cur_rounds as (select *
                        from (
                               select
                                 ol.tr_id,
                                 ol.order_num,
                                 tte.timetable_entry_num,
                                 r.muid route_muid,
                                 r.number route_number,
                                 row_number()   over (partition by ol.tr_id    order by orv.time_plan_begin) rn,
                                 dr.tab_num
                               from tt.order_list ol
                                 join tt.driver d on ol.driver_id = d.driver_id
                                 join tt.order_round ord on ol.order_list_id = ord.order_list_id
                                 join asd.oper_round_visit orv on ord.order_round_id = orv.order_round_id
                                 join tt.round rnd on ord.round_id = rnd.round_id
                                 join tt.timetable_entry tte on tte.timetable_entry_id = rnd.timetable_entry_id
                                 join gis.route_trajectories rt on rnd.route_trajectory_muid = rt.muid
                                 join gis.route_rounds rr on rt.route_round_muid = rr.muid
                                 join gis.route_variants rv on rr.route_variant_muid = rv.muid
                                 join gis.routes r on rv.route_muid = r.muid
                                 left join tt.driver dr on dr.driver_id = ol.driver_id
                               where ol.order_date = now() :: date :: timestamp
                                     and ord.is_active = 1
                                     and ol.tr_id is not null
                                     ) t
                        where rn = 1) 
   select distinct
      l_wh_type_id,
      l_dt,
      depo.depo_id,
      terr.territory_id,
      em.equipment_type_id,
      count(tr.tr_id) over (partition by depo.depo_id,  terr.territory_id, em.equipment_type_id) trs_by_type,
      count(tr.tr_id) over (partition by depo.depo_id,  terr.territory_id) trs_total
    from core.tr tr
      join core.tr_model trm on trm.tr_model_id = tr.tr_model_id
      join core.tr_status trs on trs.tr_status_id = tr.tr_status_id
      join core.tr_type trt on trt.tr_type_id = tr.tr_type_id
      join core.unit2tr u2t on u2t.tr_id = tr.tr_id
      left join core.territory terr on terr.territory_id = tr.territory_id
      left join core.entity en on en.entity_id = terr.territory_id
      join core.tr_capacity trc on trc.tr_capacity_id = trm.tr_capacity_id
      join core.depo depo on depo.depo_id =tr.depo_id
      join core.entity ent on ent.entity_id = depo.depo_id
      left join cur_rounds cr on tr.tr_id = cr.tr_id
      left join core.sensor2unit s2u on s2u.unit_id = u2t.unit_id
      left join core.equipment eq on eq.equipment_id = s2u.sensor_id
      left join core.equipment_model em on em.equipment_model_id = eq.equipment_model_id
    /*where tr.tr_type_id = l_trolley_type*/
  );
 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION wh.wh_core_process_pkg$cn_skah_period (
)
RETURNS integer AS
$body$
select 1;
$body$
LANGUAGE 'sql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION wh.wh_core_process_pkg$cn_skah_cnt (
)
RETURNS integer AS
$body$
select 2;
$body$
LANGUAGE 'sql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION wh.wh_core_process_pkg$wf_skah_agg(p_dt_start TIMESTAMP, p_dt_end TIMESTAMP)
  RETURNS void
LANGUAGE plpgsql
AS $function$
begin

  delete
  from wh.wh_core
  where dt in (SELECT date_trunc('day', dd):: date
               FROM generate_series
                    ( p_dt_start
                   , p_dt_end
                   , '1 day'::interval) dd)
        and wh_type_id = 1
  ;

  insert into wh.wh_core(
    wh_type_id,
    dt,
    vals_period,
    tr_id,
    unit_id,
    tr_type_id,
    depo_id,
    territory_id,
    route_variant_id,
    equipment_type_id,
    sensor_id,
    val_int_01,
    val_int_02
  )
    WITH  a  AS
    (
        SELECT
          tr_id
          ,val
          ,sensor_id
          ,event_time
          ,row_number() OVER (ORDER BY tr_id, sensor_id, event_time, packet_id) rn
        FROM snsr.binary_sensor_data
        where equipment_type_id = core.jf_equipment_type_pkg$cn_type_skah()
              and event_time between p_dt_start and p_dt_end
    )
      ,b AS (
        SELECT
          tr_id
          ,val
          ,sensor_id
          ,event_time
          ,row_number() OVER (ORDER BY tr_id, sensor_id, event_time, rn) - row_number() OVER (PARTITION BY tr_id, sensor_id, val ORDER BY tr_id, sensor_id, event_time,rn) gr_id
        FROM a
    )
      ,c as (
        select
          tr_id,
          val,
          sensor_id,
          min(event_time) event_time_min,
          max(event_time) event_time_max
        from b
        where val
        GROUP BY tr_id,
          sensor_id,
          gr_id,
          val
       -- having max(event_time) > min(event_time)
    )
      ,d as (
        select c.tr_id, event_time_min, event_time_max, max(round_offset) - min(round_offset) dist
        from asd.tf_graph_link join c on tf_graph_link.tr_id = c.tr_id
        where event_time BETWEEN event_time_min and event_time_max
        group by c.tr_id, event_time_min, event_time_max
    )
select
  1 as wh_type_id,
  c.event_time_min::date as dt,
  --tsrange(event_time_min,event_time_max) as vals_period,
  case c.event_time_min when c.event_time_max
    then  tsrange('['||c.event_time_min||','||c.event_time_max||']')
  else  tsrange(c.event_time_min,c.event_time_max) end as vals_period,
  sa.tr_id as tr_id,
  sa.unit_id as unit_id,
  sa.tr_type_id as tr_type_id,
  sa.depo_id as depo_id,
  sa.territory_id as territory_id,
  ( select
      tte.route_variant_muid
    from asd.oper_round_visit orv
      join tt.order_round ord on ord.order_round_id = orv.order_round_id
      join tt.order_list ol on ol.order_list_id= ord.order_list_id and ol.sign_deleted = 0
      join tt.timetable_entry tte on tte.timetable_entry_id = ol.timetable_entry_id
    where
      c.event_time_min between orv.time_fact_begin and orv.time_fact_end
      and ol.tr_id = sa.tr_id
      and orv.order_round_status_id in (1,3,4) --пройден, выполняется, пройден частично
    group by tte.route_variant_muid) as route_variant_id,
  --asd.utils_pkg$get_route_var_id(event_time_min, sa.tr_id) as route_variant_id,
  sa.equipment_type_id as equipment_type_id,
  sa.sensor_id,
  case c.event_time_min when c.event_time_max then 1 else ceil(extract (EPOCH FROM  (c.event_time_max - c.event_time_min))::float/60) end as  duration,
  d.dist
FROM snsr.v_sensor_attrib sa join c on sa.tr_id = c.tr_id and sa.sensor_id = c.sensor_id
     LEFT JOIN d on (c.tr_id = d.tr_id and c.event_time_min = d.event_time_min and c.event_time_max = d.event_time_max)
WHERE sa.unit2tr_sys_period @> c.event_time_min::TIMESTAMPTZ
      and sa.sensor2unit_hist_sys_period @> c.event_time_min::TIMESTAMPTZ;


end;
$function$
;

CREATE OR REPLACE FUNCTION wh.wh_core_process_pkg$wf_skah_agg_daly()
  RETURNS void
LANGUAGE plpgsql
AS $function$
begin
  PERFORM wh.wh_core_process_pkg$wf_skah_agg((current_date-1)::timestamp, current_date- interval '1 sec');
end;
$function$
;
