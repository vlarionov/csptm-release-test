/*tables*/
WITH aData AS (SELECT
                 n.nspname                   AS schemaname,
                 c.relname                   AS tablename,
                 pg_get_userbyid(c.relowner) AS tableowner,
                 obj_description(c.OID)      AS tablecomment,
                 c.oid,
                 np.nspname pk_schema,
                 x.conname                   AS primarykeyname,
                 pg_get_constraintdef(x.oid) AS pkdescription
               FROM pg_class c
                 LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
                 LEFT JOIN pg_tablespace t ON t.oid = c.reltablespace
                 left join pg_constraint x on x.conrelid = c.oid and x.contype='p'
                 left JOIN pg_class pk ON x.conrelid != 0 AND x.conrelid = pk.oid
                 LEFT JOIN pg_namespace np ON np.oid = pk.relnamespace
               WHERE c.relkind = 'r'
                     AND NOT exists(SELECT 1
                                    FROM pg_inherits
                                    WHERE (inhrelid = c.oid))
                     AND n.nspname IN (SELECT nspname
                                       FROM pg_catalog.pg_namespace
                                       WHERE obj_description(oid) LIKE 'PCUIP%'
               )
)
SELECT *
FROM aData
ORDER BY 1, 2;

/*columns*/
WITH aData AS (
    SELECT
      n.nspname                                       AS schemaname,
      t.relname                                       AS tablename,
      pg_get_userbyid(t.relowner)                     AS tableowner,
      a.attname                                       AS columnname,
      pg_catalog.format_type(a.atttypid, a.atttypmod) AS datatype,
      a.attnotnull,
      a.attnum,
      CASE a.attlen
      WHEN -1
        THEN 'Unlim'
      ELSE to_char(a.attlen, '999') END                  datasize,
      d.description                                   AS columncomment,
      t.oid,
      CASE
        WHEN exists(SELECT 1
                       FROM pg_constraint x
                       WHERE x.conrelid = t.oid AND a.attnum = ANY (x.conkey) AND x.contype = 'p')
        THEN TRUE
      ELSE FALSE END                                         inprimarykey,
      CASE
        WHEN exists(SELECT 1
                       FROM pg_constraint x
                       WHERE x.conrelid = t.oid AND a.attnum = ANY (x.conkey) AND x.contype = 'u')
        THEN TRUE
      ELSE FALSE END                                         inuniqueconstraint,
      CASE
        WHEN exists(SELECT 1
                       FROM pg_constraint x
                       WHERE x.conrelid = t.oid AND a.attnum = ANY (x.conkey) AND x.contype = 'f')
        THEN TRUE
      ELSE FALSE END                                         inforeignconstraint,
      CASE
        WHEN exists(select ai.attnum
from
    pg_class ti
    join pg_attribute ai on ai.attrelid = ti.oid
    JOIN pg_namespace ni ON ni.oid = ti.relnamespace
    join pg_index ix on ti.oid = ix.indrelid and ai.attnum = ANY(ix.indkey)
    join pg_class i on i.oid = ix.indexrelid
where 1=1
  and ai.attnum = a.attnum  and ni.nspname = n.nspname and ti.relname= t.relname
)
        THEN TRUE
      ELSE FALSE END                                         inindex
, (SELECT string_agg(i.relname, '; ' :: TEXT)
   FROM
     pg_class ti
     JOIN pg_attribute ai ON ai.attrelid = ti.oid
     JOIN pg_namespace ni ON ni.oid = ti.relnamespace
     JOIN pg_index ix ON ti.oid = ix.indrelid AND ai.attnum = ANY (ix.indkey)
     JOIN pg_class i ON i.oid = ix.indexrelid
   WHERE ai.attnum = a.attnum AND ni.nspname = n.nspname AND ti.relname = t.relname
    ) column_indexes
, (SELECT string_agg(i.relname, '; ' :: TEXT)
   FROM
     pg_class ti
     JOIN pg_attribute ai ON ai.attrelid = ti.oid
     JOIN pg_namespace ni ON ni.oid = ti.relnamespace
     JOIN pg_index ix ON ti.oid = ix.indrelid AND ai.attnum = ANY (ix.indkey) and ix.indisunique and not ix.indisprimary
     JOIN pg_class i ON i.oid = ix.indexrelid
   WHERE ai.attnum = a.attnum AND ni.nspname = n.nspname AND ti.relname = t.relname
    ) column_unique_indexes

    FROM pg_class t
      JOIN pg_attribute a ON a.attrelid = t.oid
      LEFT JOIN pg_namespace n ON n.oid = t.relnamespace
      LEFT JOIN pg_description AS d ON (d.objoid = t.oid AND d.objsubid = a.attnum)
    WHERE t.relkind = 'r'
          AND a.attisdropped = FALSE AND a.attnum > 0 AND a.atttypid > 0
          AND t.relname NOT IN ('tmp', 'test', 'test_table') AND t.relname NOT LIKE 'dbg%'
          AND NOT exists(SELECT 1
                         FROM pg_inherits
                         WHERE (inhrelid = t.oid))
          AND n.nspname IN (SELECT nspname
                            FROM pg_catalog.pg_namespace
                            WHERE obj_description(oid) LIKE 'PCUIP%'
    )
)
SELECT *
FROM aData
ORDER BY 1, 2, 7;

/*foreign keys*/
WITH aData AS (
    SELECT
      np.nspname                  AS schemaname,
      pk.relname                  AS tablename,
      x.conname                   AS constraintname,
      pg_get_constraintdef(x.oid) AS description,
      nf.nspname                  AS parentschemaname,
      fk.relname                  AS parenttablename,
      x.oid
    FROM pg_catalog.pg_constraint x
      INNER JOIN pg_catalog.pg_class pk ON x.conrelid != 0 AND x.conrelid = pk.oid
      LEFT JOIN pg_namespace np ON np.oid = pk.relnamespace
      INNER JOIN pg_catalog.pg_class fk ON x.confrelid != 0 AND x.confrelid = fk.oid
      LEFT JOIN pg_namespace nf ON nf.oid = fk.relnamespace
    WHERE x.contype = 'f'
          AND nf.nspname IN (SELECT nspname
                             FROM pg_catalog.pg_namespace
                             WHERE obj_description(oid) LIKE 'PCUIP%'
    )
)
SELECT *
FROM aData
ORDER BY 1, 2, 3;


/*primary keys, unique constraints*/
SELECT
  np.nspname                  AS schemaname,
  pk.relname                  AS tablename,
  x.conname                   AS constraintname,
  pg_get_constraintdef(x.oid) AS description,
  x.oid
FROM pg_catalog.pg_constraint x
  INNER JOIN pg_catalog.pg_class pk ON x.conrelid != 0 AND x.conrelid = pk.oid
  LEFT JOIN pg_namespace np ON np.oid = pk.relnamespace
WHERE 1 = 1
      AND x.contype IN ('p', 'u')
      AND x.contype IN ('p')
      AND np.nspname IN (SELECT nspname
                         FROM pg_catalog.pg_namespace
                         WHERE obj_description(oid) LIKE 'PCUIP%');


/*indexes*/
SELECT
  n.nspname              AS schemaname,
  c.relname              AS tablename,
  i.relname              AS indexname,
  t.spcname              AS tablespace,
  x.indisunique          AS unique,
  x.indisprimary         AS primary,
  pg_get_indexdef(i.oid) AS indexdef,
  x.indislive,
  x.indisready,
  x.indisvalid,
  i.oid
FROM pg_index x
  JOIN pg_class c ON c.oid = x.indrelid
  JOIN pg_class i ON i.oid = x.indexrelid
  LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
  LEFT JOIN pg_tablespace t ON t.oid = i.reltablespace
WHERE ((c.relkind = ANY (ARRAY ['r', 'm'])) AND (i.relkind = 'i'))
      AND n.nspname IN (SELECT nspname
                        FROM pg_catalog.pg_namespace
                        WHERE obj_description(oid) LIKE 'PCUIP%')
      AND x.indisunique AND NOT x.indisprimary
ORDER BY 1, 2, 3;

