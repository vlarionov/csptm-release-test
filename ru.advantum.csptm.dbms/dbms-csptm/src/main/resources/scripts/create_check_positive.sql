/*columns*/
WITH aData AS (
    SELECT
      n.nspname                                       AS schemaname,
      t.relname                                       AS tablename,
      pg_get_userbyid(t.relowner)                     AS tableowner,
      a.attname                                       AS columnname,
      pg_catalog.format_type(a.atttypid, a.atttypmod) AS datatype,
      a.attnotnull,
      a.attnum,
      CASE a.attlen
      WHEN -1
        THEN 'Unlim'
      ELSE to_char(a.attlen, '999') END                  datasize,
      d.description                                   AS columncomment,
      t.oid
    FROM pg_class t
      JOIN pg_attribute a ON a.attrelid = t.oid
      LEFT JOIN pg_namespace n ON n.oid = t.relnamespace
      LEFT JOIN pg_description AS d ON (d.objoid = t.oid AND d.objsubid = a.attnum)
    WHERE t.relkind = 'r'
          AND a.attisdropped = FALSE AND a.attnum > 0 AND a.atttypid > 0
          AND t.relname NOT IN ('tmp', 'test', 'test_table') AND t.relname NOT LIKE 'dbg%'
          AND NOT exists(SELECT 1
                         FROM pg_inherits
                         WHERE (inhrelid = t.oid))
          AND n.nspname IN (SELECT nspname
                            FROM pg_catalog.pg_namespace
                            WHERE obj_description(oid) LIKE 'PCUIP CSPTM%'
    )
)
SELECT concat_ws(' ',
                 'ALTER TABLE', concat_ws('.', aData.schemaname, aData.tablename), 'ADD CONSTRAINT',
                 concat_ws('_', aData.tablename, aData.columnname, 'check_positive'), 'CHECK (', aData.columnname, ' >= 0);'
)
FROM aData
WHERE position('id' IN columnname) = 0
      AND position('time' IN datatype) = 0
      AND position('character' IN datatype) = 0
      AND datatype NOT IN ('text', 'boolean', 'json', 'jsonb', 'tstzrange', 'geometry', 'tsrange', 'date')
ORDER BY 1
;



ALTER TABLE asd.agg_gs_speed ADD CONSTRAINT agg_gs_speed_visit_count_check_positive CHECK ( visit_count  >= 0);
ALTER TABLE asd.agg_gs_speed ADD CONSTRAINT agg_gs_speed_visit_duration_check_positive CHECK ( visit_duration  >= 0);
ALTER TABLE asd.calculation_task ADD CONSTRAINT calculation_task_hour_period_check_positive CHECK ( hour_period  >= 0);
ALTER TABLE asd.constants ADD CONSTRAINT constants_value_check_positive CHECK ( value  >= 0);
ALTER TABLE asd.ct_gs_speed_result ADD CONSTRAINT ct_gs_speed_result_avg_speed_check_positive CHECK ( avg_speed  >= 0);
ALTER TABLE asd.ct_normative ADD CONSTRAINT ct_normative_usr_base_inter_race_check_positive CHECK ( usr_base_inter_race  >= 0);
ALTER TABLE asd.ct_normative ADD CONSTRAINT ct_normative_usr_min_inter_race_check_positive CHECK ( usr_min_inter_race  >= 0);
ALTER TABLE asd.ct_normative ADD CONSTRAINT ct_normative_usr_stop_parking_time_check_positive CHECK ( usr_stop_parking_time  >= 0);
ALTER TABLE asd.ct_normative_sched ADD CONSTRAINT ct_normative_sched_calc_period_check_positive CHECK ( calc_period  >= 0);
ALTER TABLE asd.ct_round_stop_result ADD CONSTRAINT ct_round_stop_result_min_inter_round_stop_dur_check_positive CHECK ( min_inter_round_stop_dur  >= 0);
ALTER TABLE asd.ct_round_stop_result ADD CONSTRAINT ct_round_stop_result_reg_inter_round_stop_dur_check_positive CHECK ( reg_inter_round_stop_dur  >= 0);
ALTER TABLE asd.ct_round_stop_result ADD CONSTRAINT ct_round_stop_result_round_duration_check_positive CHECK ( round_duration  >= 0);
ALTER TABLE asd.ct_speed_bs_result ADD CONSTRAINT ct_speed_bs_result_avg_speed_check_positive CHECK ( avg_speed  >= 0);
ALTER TABLE asd.ct_stop_dur_result ADD CONSTRAINT ct_stop_dur_result_stop_duration_check_positive CHECK ( stop_duration  >= 0);
ALTER TABLE asd.ct_stop_visit_result ADD CONSTRAINT ct_stop_visit_result_avg_speed_check_positive CHECK ( avg_speed  >= 0);
ALTER TABLE asd.ct_stop_visit_result ADD CONSTRAINT ct_stop_visit_result_norm_duration_check_positive CHECK ( norm_duration  >= 0);
ALTER TABLE asd.graph_section_speed ADD CONSTRAINT graph_section_speed_order_num_check_positive CHECK ( order_num  >= 0);
ALTER TABLE asd.order_round_status ADD CONSTRAINT order_round_status_code_check_positive CHECK ( code  >= 0);
ALTER TABLE asd.params2ct ADD CONSTRAINT params2ct_value_check_positive CHECK ( value  >= 0);
ALTER TABLE asd.tf_graph_link ADD CONSTRAINT tf_graph_link_graph_section_offset_check_positive CHECK ( graph_section_offset  >= 0);
ALTER TABLE asd.tf_graph_link ADD CONSTRAINT tf_graph_link_order_num_check_positive CHECK ( order_num  >= 0);
ALTER TABLE asd.tf_graph_link ADD CONSTRAINT tf_graph_link_round_offset_check_positive CHECK ( round_offset  >= 0);
ALTER TABLE asd.visual_review_detail ADD CONSTRAINT visual_review_detail_delay_duration_check_positive CHECK ( delay_duration  >= 0);
ALTER TABLE asd.visual_review_detail ADD CONSTRAINT visual_review_detail_stop_duration_check_positive CHECK ( stop_duration  >= 0);
ALTER TABLE asd.visual_review_detail ADD CONSTRAINT visual_review_detail_stop_sequence_check_positive CHECK ( stop_sequence  >= 0);
ALTER TABLE asd.visual_review_detail ADD CONSTRAINT visual_review_detail_time_arrival_fact_check_positive CHECK ( time_arrival_fact  >= 0);
ALTER TABLE asd.visual_review_item ADD CONSTRAINT visual_review_item_circle_check_positive CHECK ( circle  >= 0);
ALTER TABLE asd.visual_review_report ADD CONSTRAINT visual_review_report_route_round_type_check_positive CHECK ( route_round_type  >= 0);
ALTER TABLE asd.visual_review_report ADD CONSTRAINT visual_review_report_status_check_positive CHECK ( status  >= 0);
ALTER TABLE askp.asmpp_agg_item ADD CONSTRAINT asmpp_agg_item_in_cnt_check_positive CHECK ( in_cnt  >= 0);
ALTER TABLE askp.asmpp_agg_item ADD CONSTRAINT asmpp_agg_item_in_corrected_check_positive CHECK ( in_corrected  >= 0);
ALTER TABLE askp.asmpp_agg_item ADD CONSTRAINT asmpp_agg_item_out_cnt_check_positive CHECK ( out_cnt  >= 0);
ALTER TABLE askp.asmpp_agg_item ADD CONSTRAINT asmpp_agg_item_out_corrected_check_positive CHECK ( out_corrected  >= 0);
ALTER TABLE askp.asmpp_agg_item ADD CONSTRAINT asmpp_agg_item_total_pass_after_check_positive CHECK ( total_pass_after  >= 0);
ALTER TABLE askp.asmpp_agg_item ADD CONSTRAINT asmpp_agg_item_total_pass_before_check_positive CHECK ( total_pass_before  >= 0);
ALTER TABLE askp.passenger_traffic_matrix_item ADD CONSTRAINT passenger_traffic_matrix_item_order_num_from_check_positive CHECK ( order_num_from  >= 0);
ALTER TABLE askp.passenger_traffic_matrix_item ADD CONSTRAINT passenger_traffic_matrix_item_order_num_to_check_positive CHECK ( order_num_to  >= 0);
ALTER TABLE askp.passenger_traffic_matrix_item ADD CONSTRAINT passenger_traffic_matrix_item_passenger_traffic_check_positive CHECK ( passenger_traffic  >= 0);
ALTER TABLE askp.route_inspection ADD CONSTRAINT route_inspection_inspection_count_check_positive CHECK ( inspection_count  >= 0);
ALTER TABLE askp.route_inspection ADD CONSTRAINT route_inspection_round_count_check_positive CHECK ( round_count  >= 0);
ALTER TABLE askp.route_inspection ADD CONSTRAINT route_inspection_status_check_positive CHECK ( status  >= 0);
ALTER TABLE askp.ticket_oper ADD CONSTRAINT ticket_oper_move_num_check_positive CHECK ( move_num  >= 0);
ALTER TABLE askp.trip_pairs_agg ADD CONSTRAINT trip_pairs_agg_time_from_check_positive CHECK ( time_from  >= 0);
ALTER TABLE askp.trip_pairs_agg ADD CONSTRAINT trip_pairs_agg_time_to_check_positive CHECK ( time_to  >= 0);
ALTER TABLE askp.trip_pairs_agg ADD CONSTRAINT trip_pairs_agg_trip_number_check_positive CHECK ( trip_number  >= 0);
ALTER TABLE askp.validation2stops_binding ADD CONSTRAINT validation2stops_binding_move_num_check_positive CHECK ( move_num  >= 0);
ALTER TABLE askp.validation2stops_binding ADD CONSTRAINT validation2stops_binding_order_num_check_positive CHECK ( order_num  >= 0);
ALTER TABLE askp.validation2stops_binding ADD CONSTRAINT validation2stops_binding_order_round_num_check_positive CHECK ( order_round_num  >= 0);
ALTER TABLE askp.validation2stops_binding ADD CONSTRAINT validation2stops_binding_seat_qty_total_check_positive CHECK ( seat_qty_total  >= 0);
ALTER TABLE askp.validation4reports_agg ADD CONSTRAINT validation4reports_agg_check_all_check_positive CHECK ( check_all  >= 0);
ALTER TABLE askp.validation4reports_agg ADD CONSTRAINT validation4reports_agg_check_ordinary_check_positive CHECK ( check_ordinary  >= 0);
ALTER TABLE askp.validation4reports_agg ADD CONSTRAINT validation4reports_agg_check_privilege_check_positive CHECK ( check_privilege  >= 0);
ALTER TABLE askp.validation4reports_agg ADD CONSTRAINT validation4reports_agg_move_num_check_positive CHECK ( move_num  >= 0);
ALTER TABLE askp.visit_tr_stop ADD CONSTRAINT visit_ts_stop_arrival_time_ts_fact_check_positive CHECK ( arrival_time_tr_fact  >= 0);
ALTER TABLE askp.visit_tr_stop ADD CONSTRAINT visit_ts_stop_entered_count_check_positive CHECK ( entered_count  >= 0);
ALTER TABLE askp.visit_tr_stop ADD CONSTRAINT visit_ts_stop_landing_refusal_count_check_positive CHECK ( landing_refusal_count  >= 0);
ALTER TABLE askp.visit_tr_stop ADD CONSTRAINT visit_ts_stop_released_count_check_positive CHECK ( released_count  >= 0);
ALTER TABLE askp.visit_tr_stop ADD CONSTRAINT visit_ts_stop_stay_at_stop_count_check_positive CHECK ( stay_at_stop_count  >= 0);
ALTER TABLE askp.visit_tr_stop ADD CONSTRAINT visit_ts_stop_tr_filling_count_check_positive CHECK ( tr_filling_count  >= 0);
ALTER TABLE askp.visual_inspection ADD CONSTRAINT visual_inspection_status_check_positive CHECK ( status  >= 0);
ALTER TABLE askp.visual_inspection ADD CONSTRAINT visual_inspection_time_begin_inspection_check_positive CHECK ( time_begin_inspection  >= 0);
ALTER TABLE askp.visual_inspection ADD CONSTRAINT visual_inspection_time_end_inspection_check_positive CHECK ( time_end_inspection  >= 0);
ALTER TABLE cmnd.cmnd_ref ADD CONSTRAINT cmnd_ref_low_level_protocol_check_positive CHECK ( low_level_protocol  >= 0);
ALTER TABLE cmnd.cmnd_ref_param ADD CONSTRAINT cmnd_ref_param_data_type_check_positive CHECK ( data_type  >= 0);
ALTER TABLE cmnd.response_ref ADD CONSTRAINT response_ref_order_num_check_positive CHECK ( order_num  >= 0);
ALTER TABLE core.asmpp ADD CONSTRAINT asmpp_doors_count_check_positive CHECK ( doors_count  >= 0);
ALTER TABLE core.breakdown ADD CONSTRAINT breakdown_num_check_positive CHECK ( num  >= 0);
ALTER TABLE core.carrier ADD CONSTRAINT carrier_sign_deleted_check_positive CHECK ( sign_deleted  >= 0);
ALTER TABLE core.depo ADD CONSTRAINT depo_sign_deleted_check_positive CHECK ( sign_deleted  >= 0);
ALTER TABLE core.driver ADD CONSTRAINT driver_sign_deleted_check_positive CHECK ( sign_deleted  >= 0);
ALTER TABLE core.equipment_model ADD CONSTRAINT equipment_model_input_analog_check_positive CHECK ( input_analog  >= 0);
ALTER TABLE core.equipment_model ADD CONSTRAINT equipment_model_input_digital_check_positive CHECK ( input_digital  >= 0);
ALTER TABLE core.hdd ADD CONSTRAINT hdd_capacity_check_positive CHECK ( capacity  >= 0);
ALTER TABLE core.hdd ADD CONSTRAINT hdd_size_check_positive CHECK ( size  >= 0);
ALTER TABLE core.hdd ADD CONSTRAINT hdd_type_check_positive CHECK ( type  >= 0);
ALTER TABLE core.repair_request ADD CONSTRAINT repair_request_num_check_positive CHECK ( num  >= 0);
ALTER TABLE core.sensor2unit ADD CONSTRAINT sensor2unit_sensor_num_check_positive CHECK ( sensor_num  >= 0);
ALTER TABLE core.sensor2unit_model ADD CONSTRAINT sensor2unit_model_num_check_positive CHECK ( num  >= 0);
ALTER TABLE core.sensor ADD CONSTRAINT sensor_sensor_num_check_positive CHECK ( sensor_num  >= 0);
ALTER TABLE core.sim_card2unit_h ADD CONSTRAINT sim_card2unit_h_unit_remove_reason_check_positive CHECK ( unit_remove_reason  >= 0);
ALTER TABLE core.territory ADD CONSTRAINT territory_code_check_positive CHECK ( code  >= 0);
ALTER TABLE core.territory ADD CONSTRAINT territory_sign_deleted_check_positive CHECK ( sign_deleted  >= 0);
ALTER TABLE core.tr ADD CONSTRAINT tr_garage_num_add_check_positive CHECK ( garage_num_add  >= 0);
ALTER TABLE core.tr ADD CONSTRAINT tr_garage_num_check_positive CHECK ( garage_num  >= 0);
ALTER TABLE core.tr ADD CONSTRAINT tr_seat_qty_check_positive CHECK ( seat_qty  >= 0);
ALTER TABLE core.tr ADD CONSTRAINT tr_seat_qty_disabled_people_check_positive CHECK ( seat_qty_disabled_people  >= 0);
ALTER TABLE core.tr ADD CONSTRAINT tr_seat_qty_total_check_positive CHECK ( seat_qty_total  >= 0);
ALTER TABLE core.tr ADD CONSTRAINT tr_year_of_issue_check_positive CHECK ( year_of_issue  >= 0);
ALTER TABLE core.traffic ADD CONSTRAINT traffic_alt_check_positive CHECK ( alt  >= 0);
ALTER TABLE core.traffic ADD CONSTRAINT traffic_heading_check_positive CHECK ( heading  >= 0);
ALTER TABLE core.traffic ADD CONSTRAINT traffic_lat_check_positive CHECK ( lat  >= 0);
ALTER TABLE core.traffic ADD CONSTRAINT traffic_lon_check_positive CHECK ( lon  >= 0);
ALTER TABLE core.traffic ADD CONSTRAINT traffic_speed_check_positive CHECK ( speed  >= 0);
ALTER TABLE core.tr_capacity ADD CONSTRAINT tr_capacity_qty_check_positive CHECK ( qty  >= 0);
ALTER TABLE core.tr_last_state ADD CONSTRAINT tr_last_state_alt_check_positive CHECK ( alt  >= 0);
ALTER TABLE core.tr_last_state ADD CONSTRAINT tr_last_state_heading_check_positive CHECK ( heading  >= 0);
ALTER TABLE core.tr_last_state ADD CONSTRAINT tr_last_state_lat_check_positive CHECK ( lat  >= 0);
ALTER TABLE core.tr_last_state ADD CONSTRAINT tr_last_state_lon_check_positive CHECK ( lon  >= 0);
ALTER TABLE core.tr_last_state ADD CONSTRAINT tr_last_state_speed_check_positive CHECK ( speed  >= 0);
ALTER TABLE core.tr_model ADD CONSTRAINT tr_model_door_count_check_positive CHECK ( door_count  >= 0);
ALTER TABLE core.tr_model ADD CONSTRAINT tr_model_seat_qty_check_positive CHECK ( seat_qty  >= 0);
ALTER TABLE core.tr_model ADD CONSTRAINT tr_model_seat_qty_disabled_people_check_positive CHECK ( seat_qty_disabled_people  >= 0);
ALTER TABLE core.tr_model ADD CONSTRAINT tr_model_seat_qty_total_check_positive CHECK ( seat_qty_total  >= 0);
ALTER TABLE core.unit_bnsr ADD CONSTRAINT unit_bnsr_channel_num_check_positive CHECK ( channel_num  >= 0);
ALTER TABLE core.unit_bnsr ADD CONSTRAINT unit_bnsr_has_manipulator_check_positive CHECK ( has_manipulator  >= 0);
ALTER TABLE core.unit_bnst ADD CONSTRAINT unit_bnst_has_flash_card_check_positive CHECK ( has_flash_card  >= 0);
ALTER TABLE core.unit_bnst ADD CONSTRAINT unit_bnst_has_manipulator_check_positive CHECK ( has_manipulator  >= 0);
ALTER TABLE core.unit_kbtob ADD CONSTRAINT unit_kbtob_channel_qty_check_positive CHECK ( channel_qty  >= 0);
ALTER TABLE core.unit_kbtob ADD CONSTRAINT unit_kbtob_port_qty_check_positive CHECK ( port_qty  >= 0);
ALTER TABLE ibrd.brightness_level ADD CONSTRAINT brightness_level_hour_check_positive CHECK ( hour  >= 0);
ALTER TABLE ibrd.brightness_level ADD CONSTRAINT brightness_level_value_check_positive CHECK ( value  >= 0);
ALTER TABLE ibrd.config ADD CONSTRAINT config_data_waiting_time_check_positive CHECK ( data_waiting_time  >= 0);
ALTER TABLE ibrd.config ADD CONSTRAINT config_max_forecast_interval_check_positive CHECK ( max_forecast_interval  >= 0);
ALTER TABLE ibrd.config ADD CONSTRAINT config_running_row_speed_check_positive CHECK ( running_row_speed  >= 0);
ALTER TABLE ibrd.error_type ADD CONSTRAINT error_type_code_check_positive CHECK ( code  >= 0);
ALTER TABLE ibrd.event ADD CONSTRAINT event_lat_check_positive CHECK ( lat  >= 0);
ALTER TABLE ibrd.event ADD CONSTRAINT event_lon_check_positive CHECK ( lon  >= 0);
ALTER TABLE ibrd.event_csv ADD CONSTRAINT event_csv_lat_check_positive CHECK ( lat  >= 0);
ALTER TABLE ibrd.event_csv ADD CONSTRAINT event_csv_lon_check_positive CHECK ( lon  >= 0);
ALTER TABLE ibrd.forecast ADD CONSTRAINT forecast_from_stop_place_order_num_check_positive CHECK ( from_stop_place_order_num  >= 0);
ALTER TABLE ibrd.forecast ADD CONSTRAINT forecast_stop_place_order_num_check_positive CHECK ( stop_place_order_num  >= 0);
ALTER TABLE ibrd.info_board ADD CONSTRAINT info_board_useful_duration_check_positive CHECK ( useful_duration  >= 0);
ALTER TABLE ibrd.message ADD CONSTRAINT message_priority_check_positive CHECK ( priority  >= 0);
ALTER TABLE ibrd.message_schedule ADD CONSTRAINT message_day_schedule_week_day_check_positive CHECK ( week_day  >= 0);
ALTER TABLE ibrd.mono_rgb_config ADD CONSTRAINT mono_rgb_config_forecast_delay_check_positive CHECK ( forecast_delay  >= 0);
ALTER TABLE ibrd.mono_rgb_config ADD CONSTRAINT mono_rgb_config_rows_number_check_positive CHECK ( rows_number  >= 0);
ALTER TABLE ibrd.round_stat ADD CONSTRAINT round_stat_item_count_check_positive CHECK ( item_count  >= 0);
ALTER TABLE ibrd.round_stat ADD CONSTRAINT round_stat_stop_order_num_check_positive CHECK ( stop_order_num  >= 0);
ALTER TABLE ibrd.round_stat ADD CONSTRAINT round_stat_sum_date_diff_check_positive CHECK ( sum_date_diff  >= 0);
ALTER TABLE ibrd.row_config ADD CONSTRAINT row_config_block_rate_check_positive CHECK ( block_rate  >= 0);
ALTER TABLE ibrd.row_config ADD CONSTRAINT row_config_reboot_time_check_positive CHECK ( reboot_time  >= 0);
ALTER TABLE ibrd.row_config ADD CONSTRAINT row_config_scroll_speed_check_positive CHECK ( scroll_speed  >= 0);
ALTER TABLE ibrd.row_config ADD CONSTRAINT row_config_server_waiting_time_check_positive CHECK ( server_waiting_time  >= 0);
ALTER TABLE ibrd.telematics_response ADD CONSTRAINT telematics_response_lat_check_positive CHECK ( lat  >= 0);
ALTER TABLE ibrd.telematics_response ADD CONSTRAINT telematics_response_lon_check_positive CHECK ( lon  >= 0);
ALTER TABLE kdbo.diagnostic_ref ADD CONSTRAINT diagnostic_ref_val_check_positive CHECK ( val  >= 0);
ALTER TABLE kdbo.unit_session_log ADD CONSTRAINT unit_session_log_hist_packet_qty_check_positive CHECK ( hist_packet_qty  >= 0);
ALTER TABLE kdbo.unit_session_log ADD CONSTRAINT unit_session_log_total_packet_qty_check_positive CHECK ( total_packet_qty  >= 0);
ALTER TABLE rts.gas_station ADD CONSTRAINT gas_station_time_fueling_check_positive CHECK ( time_fueling  >= 0);
ALTER TABLE rts.graph_sec2round ADD CONSTRAINT graph_sec2round_order_num_check_positive CHECK ( order_num  >= 0);
ALTER TABLE rts.graph_section ADD CONSTRAINT graph_section_length_check_positive CHECK ( length  >= 0);
ALTER TABLE rts.parking ADD CONSTRAINT parking_square_check_positive CHECK ( square  >= 0);
ALTER TABLE rts.parking ADD CONSTRAINT parking_tr_count_check_positive CHECK ( tr_count  >= 0);
ALTER TABLE rts.round ADD CONSTRAINT round_graph_section_end_offset_check_positive CHECK ( graph_section_end_offset  >= 0);
ALTER TABLE rts.round ADD CONSTRAINT round_graph_section_start_offset_check_positive CHECK ( graph_section_start_offset  >= 0);
ALTER TABLE rts.round ADD CONSTRAINT round_length_check_positive CHECK ( length  >= 0);
ALTER TABLE rts.round ADD CONSTRAINT round_length_fixed_check_positive CHECK ( length_fixed  >= 0);
ALTER TABLE rts.round ADD CONSTRAINT round_round_num_check_positive CHECK ( round_num  >= 0);
ALTER TABLE rts.stop_item2round ADD CONSTRAINT stop_item2round_length_sector_check_positive CHECK ( length_sector  >= 0);
ALTER TABLE rts.stop_item2round ADD CONSTRAINT stop_item2round_order_num_check_positive CHECK ( order_num  >= 0);
ALTER TABLE rts.stop_location ADD CONSTRAINT stop_location_building_distance_check_positive CHECK ( building_distance  >= 0);
ALTER TABLE rts.stop_location ADD CONSTRAINT stop_location_graph_section_offset_check_positive CHECK ( graph_section_offset  >= 0);
ALTER TABLE snsr.extended_data ADD CONSTRAINT extended_data_accel_energy_check_positive CHECK ( accel_energy  >= 0);
ALTER TABLE snsr.extended_data ADD CONSTRAINT extended_data_alt_check_positive CHECK ( alt  >= 0);
ALTER TABLE snsr.extended_data ADD CONSTRAINT extended_data_battery_voltage_check_positive CHECK ( battery_voltage  >= 0);
ALTER TABLE snsr.extended_data ADD CONSTRAINT extended_data_gprs_state_check_positive CHECK ( gprs_state  >= 0);
ALTER TABLE snsr.extended_data ADD CONSTRAINT extended_data_gsm_level_check_positive CHECK ( gsm_level  >= 0);
ALTER TABLE snsr.extended_data ADD CONSTRAINT extended_data_max_period_speed_check_positive CHECK ( max_period_speed  >= 0);
ALTER TABLE snsr.extended_data ADD CONSTRAINT extended_data_odometer_check_positive CHECK ( odometer  >= 0);
ALTER TABLE snsr.extended_data ADD CONSTRAINT extended_data_satellites_count_check_positive CHECK ( satellites_count  >= 0);
ALTER TABLE snsr.extended_data ADD CONSTRAINT extended_data_track_length_check_positive CHECK ( track_length  >= 0);
ALTER TABLE snsr.fuel_sensor_data ADD CONSTRAINT fuel_sensor_data_level_l_check_positive CHECK ( level_l  >= 0);
ALTER TABLE snsr.fuel_sensor_data ADD CONSTRAINT fuel_sensor_data_level_mm_check_positive CHECK ( level_mm  >= 0);
ALTER TABLE snsr.fuel_sensor_data ADD CONSTRAINT fuel_sensor_data_temperature_check_positive CHECK ( temperature  >= 0);
ALTER TABLE snsr.integer_sensor_data ADD CONSTRAINT integer_sensor_data_val_check_positive CHECK ( val  >= 0);
ALTER TABLE snsr.irma_sensor_data ADD CONSTRAINT irma_sensor_data_in1_check_positive CHECK ( in1  >= 0);
ALTER TABLE snsr.irma_sensor_data ADD CONSTRAINT irma_sensor_data_in2_check_positive CHECK ( in2  >= 0);
ALTER TABLE snsr.irma_sensor_data ADD CONSTRAINT irma_sensor_data_in3_check_positive CHECK ( in3  >= 0);
ALTER TABLE snsr.irma_sensor_data ADD CONSTRAINT irma_sensor_data_in4_check_positive CHECK ( in4  >= 0);
ALTER TABLE snsr.irma_sensor_data ADD CONSTRAINT irma_sensor_data_odometer_check_positive CHECK ( odometer  >= 0);
ALTER TABLE snsr.irma_sensor_data ADD CONSTRAINT irma_sensor_data_out1_check_positive CHECK ( out1  >= 0);
ALTER TABLE snsr.irma_sensor_data ADD CONSTRAINT irma_sensor_data_out2_check_positive CHECK ( out2  >= 0);
ALTER TABLE snsr.irma_sensor_data ADD CONSTRAINT irma_sensor_data_out3_check_positive CHECK ( out3  >= 0);
ALTER TABLE snsr.irma_sensor_data ADD CONSTRAINT irma_sensor_data_out4_check_positive CHECK ( out4  >= 0);
ALTER TABLE snsr.irma_sensor_data ADD CONSTRAINT irma_sensor_data_zone_check_positive CHECK ( zone  >= 0);
ALTER TABLE snsr.termo_data ADD CONSTRAINT termo_data_val_check_positive CHECK ( val  >= 0);
ALTER TABLE ttb.agreement_group ADD CONSTRAINT agreement_group_agreement_period_check_positive CHECK ( agreement_period  >= 0);
ALTER TABLE ttb.agreement_group ADD CONSTRAINT agreement_group_order_num_check_positive CHECK ( order_num  >= 0);
ALTER TABLE ttb.dr_mode ADD CONSTRAINT dr_mode_time_max_check_positive CHECK ( time_max  >= 0);
ALTER TABLE ttb.dr_mode ADD CONSTRAINT dr_mode_time_min_check_positive CHECK ( time_min  >= 0);
ALTER TABLE ttb.dr_mode_bk ADD CONSTRAINT dr_mode_bk_after_time_max_check_positive CHECK ( after_time_max  >= 0);
ALTER TABLE ttb.dr_mode_bk ADD CONSTRAINT dr_mode_bk_before_time_max_check_positive CHECK ( before_time_max  >= 0);
ALTER TABLE ttb.dr_mode_bk ADD CONSTRAINT dr_mode_bk_before_time_min_check_positive CHECK ( before_time_min  >= 0);
ALTER TABLE ttb.dr_mode_bk ADD CONSTRAINT dr_mode_bk_before_time_short_break_max_check_positive CHECK ( before_time_short_break_max  >= 0);
ALTER TABLE ttb.dr_mode_bk ADD CONSTRAINT dr_mode_bk_break_count_max_check_positive CHECK ( break_count_max  >= 0);
ALTER TABLE ttb.dr_mode_bk ADD CONSTRAINT dr_mode_bk_break_count_min_check_positive CHECK ( break_count_min  >= 0);
ALTER TABLE ttb.dr_mode_bk ADD CONSTRAINT dr_mode_bk_break_duration_max_check_positive CHECK ( break_duration_max  >= 0);
ALTER TABLE ttb.dr_mode_bk ADD CONSTRAINT dr_mode_bk_break_duration_min_check_positive CHECK ( break_duration_min  >= 0);
ALTER TABLE ttb.dr_mode_bk ADD CONSTRAINT dr_mode_bk_stop_time_check_positive CHECK ( stop_time  >= 0);
ALTER TABLE ttb.dr_mode_bk ADD CONSTRAINT dr_mode_bk_time_max_check_positive CHECK ( time_max  >= 0);
ALTER TABLE ttb.dr_mode_bk ADD CONSTRAINT dr_mode_bk_time_min_check_positive CHECK ( time_min  >= 0);
ALTER TABLE ttb.dr_mode_bk ADD CONSTRAINT dr_mode_bk_work_break_between_max_check_positive CHECK ( work_break_between_max  >= 0);
ALTER TABLE ttb.dr_mode_bk ADD CONSTRAINT dr_mode_bk_work_break_between_min_check_positive CHECK ( work_break_between_min  >= 0);
ALTER TABLE ttb.dr_mode_bk ADD CONSTRAINT dr_mode_bk_work_duration_max_check_positive CHECK ( work_duration_max  >= 0);
ALTER TABLE ttb.dr_mode_bk ADD CONSTRAINT dr_mode_bk_work_duration_min_check_positive CHECK ( work_duration_min  >= 0);
ALTER TABLE ttb.dr_shift ADD CONSTRAINT dr_shift_after_time_max_check_positive CHECK ( after_time_max  >= 0);
ALTER TABLE ttb.dr_shift ADD CONSTRAINT dr_shift_before_time_max_check_positive CHECK ( before_time_max  >= 0);
ALTER TABLE ttb.dr_shift ADD CONSTRAINT dr_shift_before_time_min_check_positive CHECK ( before_time_min  >= 0);
ALTER TABLE ttb.dr_shift ADD CONSTRAINT dr_shift_before_time_short_break_max_check_positive CHECK ( before_time_short_break_max  >= 0);
ALTER TABLE ttb.dr_shift ADD CONSTRAINT dr_shift_break_count_max_check_positive CHECK ( break_count_max  >= 0);
ALTER TABLE ttb.dr_shift ADD CONSTRAINT dr_shift_break_count_min_check_positive CHECK ( break_count_min  >= 0);
ALTER TABLE ttb.dr_shift ADD CONSTRAINT dr_shift_break_duration_max_check_positive CHECK ( break_duration_max  >= 0);
ALTER TABLE ttb.dr_shift ADD CONSTRAINT dr_shift_dr_shift_num_check_positive CHECK ( dr_shift_num  >= 0);
ALTER TABLE ttb.dr_shift ADD CONSTRAINT dr_shift_stop_time_check_positive CHECK ( stop_time  >= 0);
ALTER TABLE ttb.dr_shift ADD CONSTRAINT dr_shift_work_break_between_max_check_positive CHECK ( work_break_between_max  >= 0);
ALTER TABLE ttb.dr_shift ADD CONSTRAINT dr_shift_work_break_between_min_check_positive CHECK ( work_break_between_min  >= 0);
ALTER TABLE ttb.dr_shift ADD CONSTRAINT dr_shift_work_duration_max_check_positive CHECK ( work_duration_max  >= 0);
ALTER TABLE ttb.dr_shift ADD CONSTRAINT dr_shift_work_duration_min_check_positive CHECK ( work_duration_min  >= 0);
ALTER TABLE ttb.dr_shift_bk ADD CONSTRAINT dr_shift_bk_after_time_max_check_positive CHECK ( after_time_max  >= 0);
ALTER TABLE ttb.dr_shift_bk ADD CONSTRAINT dr_shift_bk_before_time_max_check_positive CHECK ( before_time_max  >= 0);
ALTER TABLE ttb.dr_shift_bk ADD CONSTRAINT dr_shift_bk_before_time_min_check_positive CHECK ( before_time_min  >= 0);
ALTER TABLE ttb.dr_shift_bk ADD CONSTRAINT dr_shift_bk_before_time_short_break_max_check_positive CHECK ( before_time_short_break_max  >= 0);
ALTER TABLE ttb.dr_shift_bk ADD CONSTRAINT dr_shift_bk_break_count_max_check_positive CHECK ( break_count_max  >= 0);
ALTER TABLE ttb.dr_shift_bk ADD CONSTRAINT dr_shift_bk_break_count_min_check_positive CHECK ( break_count_min  >= 0);
ALTER TABLE ttb.dr_shift_bk ADD CONSTRAINT dr_shift_bk_break_duration_max_check_positive CHECK ( break_duration_max  >= 0);
ALTER TABLE ttb.dr_shift_bk ADD CONSTRAINT dr_shift_bk_dr_shift_num_check_positive CHECK ( dr_shift_num  >= 0);
ALTER TABLE ttb.dr_shift_bk ADD CONSTRAINT dr_shift_bk_stop_time_check_positive CHECK ( stop_time  >= 0);
ALTER TABLE ttb.dr_shift_bk ADD CONSTRAINT dr_shift_bk_work_break_between_max_check_positive CHECK ( work_break_between_max  >= 0);
ALTER TABLE ttb.dr_shift_bk ADD CONSTRAINT dr_shift_bk_work_break_between_min_check_positive CHECK ( work_break_between_min  >= 0);
ALTER TABLE ttb.dr_shift_bk ADD CONSTRAINT dr_shift_bk_work_duration_max_check_positive CHECK ( work_duration_max  >= 0);
ALTER TABLE ttb.dr_shift_bk ADD CONSTRAINT dr_shift_bk_work_duration_min_check_positive CHECK ( work_duration_min  >= 0);
ALTER TABLE ttb.incident_action ADD CONSTRAINT incident_action_order_num_check_positive CHECK ( order_num  >= 0);
ALTER TABLE ttb.job_time ADD CONSTRAINT job_time_time_begin_check_positive CHECK ( time_begin  >= 0);
ALTER TABLE ttb.job_time ADD CONSTRAINT job_time_time_end_check_positive CHECK ( time_end  >= 0);
ALTER TABLE ttb.norm ADD CONSTRAINT norm_hour_period_check_positive CHECK ( hour_period  >= 0);
ALTER TABLE ttb.norm_between_stop ADD CONSTRAINT norm_between_stop_between_stop_dur_check_positive CHECK ( between_stop_dur  >= 0);
ALTER TABLE ttb.norm_round ADD CONSTRAINT norm_round_min_inter_round_stop_dur_check_positive CHECK ( min_inter_round_stop_dur  >= 0);
ALTER TABLE ttb.norm_round ADD CONSTRAINT norm_round_reg_inter_round_stop_dur_check_positive CHECK ( reg_inter_round_stop_dur  >= 0);
ALTER TABLE ttb.norm_round ADD CONSTRAINT norm_round_round_duration_check_positive CHECK ( round_duration  >= 0);
ALTER TABLE ttb.norm_stop ADD CONSTRAINT norm_stop_stop_dur_check_positive CHECK ( stop_dur  >= 0);
ALTER TABLE ttb.order_list ADD CONSTRAINT order_list_out_num_check_positive CHECK ( out_num  >= 0);
ALTER TABLE ttb.ref_action_type ADD CONSTRAINT ref_action_type_order_num_check_positive CHECK ( order_num  >= 0);
ALTER TABLE ttb.rv_change_time ADD CONSTRAINT rv_change_time_change_time_check_positive CHECK ( change_time  >= 0);
ALTER TABLE ttb.rv_peak_hour ADD CONSTRAINT rv_peak_hour_time_begin_check_positive CHECK ( time_begin  >= 0);
ALTER TABLE ttb.rv_peak_hour ADD CONSTRAINT rv_peak_hour_time_end_check_positive CHECK ( time_end  >= 0);
ALTER TABLE ttb.rv_tune ADD CONSTRAINT rv_tune_break_dinner_time_check_positive CHECK ( break_dinner_time  >= 0);
ALTER TABLE ttb.rv_tune ADD CONSTRAINT rv_tune_break_round_time_check_positive CHECK ( break_round_time  >= 0);
ALTER TABLE ttb.rv_tune ADD CONSTRAINT rv_tune_break_tr_time_check_positive CHECK ( break_tr_time  >= 0);
ALTER TABLE ttb.spr_change_time ADD CONSTRAINT spr_change_time_change_time_check_positive CHECK ( change_time  >= 0);
ALTER TABLE ttb.tr_mode ADD CONSTRAINT tr_mode_break_count_check_positive CHECK ( break_count  >= 0);
ALTER TABLE ttb.tr_mode ADD CONSTRAINT tr_mode_to_duration_max_check_positive CHECK ( to_duration_max  >= 0);
ALTER TABLE ttb.tr_mode ADD CONSTRAINT tr_mode_to_duration_min_check_positive CHECK ( to_duration_min  >= 0);
ALTER TABLE ttb.tt_action ADD CONSTRAINT tt_action_action_dur_check_positive CHECK ( action_dur  >= 0);
ALTER TABLE ttb.tt_action ADD CONSTRAINT tt_action_action_min_dur_check_positive CHECK ( action_min_dur  >= 0);
ALTER TABLE ttb.tt_action_type2group ADD CONSTRAINT tt_action_type2group_order_num_check_positive CHECK ( order_num  >= 0);
ALTER TABLE ttb.tt_action_type2group ADD CONSTRAINT tt_action_type2group_plan_time_check_positive CHECK ( plan_time  >= 0);
ALTER TABLE ttb.tt_action_type ADD CONSTRAINT tt_action_type_plan_time_check_positive CHECK ( plan_time  >= 0);
ALTER TABLE ttb.tt_change_time ADD CONSTRAINT tt_change_time_change_time_check_positive CHECK ( change_time  >= 0);
ALTER TABLE ttb.tt_check_condition ADD CONSTRAINT tt_check_condition_check_num_check_positive CHECK ( check_num  >= 0);
ALTER TABLE ttb.tt_check_parm ADD CONSTRAINT tt_check_parm_order_num_check_positive CHECK ( order_num  >= 0);
ALTER TABLE ttb.tt_check_parm ADD CONSTRAINT tt_check_parm_parm_value_check_positive CHECK ( parm_value  >= 0);
ALTER TABLE ttb.tt_out ADD CONSTRAINT tt_out_tt_out_num_check_positive CHECK ( tt_out_num  >= 0);
ALTER TABLE ttb.tt_period ADD CONSTRAINT tt_period_interval_time_check_positive CHECK ( interval_time  >= 0);
ALTER TABLE ttb.tt_period ADD CONSTRAINT tt_period_limit_time_check_positive CHECK ( limit_time  >= 0);
ALTER TABLE ttb.tt_schedule_item ADD CONSTRAINT tt_schedule_item_item_type_check_positive CHECK ( item_type  >= 0);
ALTER TABLE ttb.tt_schedule_item ADD CONSTRAINT tt_schedule_item_order_num_check_positive CHECK ( order_num  >= 0);
ALTER TABLE ttb.tt_schedule_item ADD CONSTRAINT tt_schedule_item_repeat_time_check_positive CHECK ( repeat_time  >= 0);
ALTER TABLE ttb.tt_tr ADD CONSTRAINT tt_tr_order_num_check_positive CHECK ( order_num  >= 0);
ALTER TABLE ttb.tt_variant_agreement ADD CONSTRAINT tt_variant_agreement_iteration_num_check_positive CHECK ( iteration_num  >= 0);
ALTER TABLE ttb.ttv_check_condition ADD CONSTRAINT ttv_check_condition_check_result_check_positive CHECK ( check_result  >= 0);
ALTER TABLE ttb.ttv_check_parm ADD CONSTRAINT ttv_check_parm_parm_value_check_positive CHECK ( parm_value  >= 0);
ALTER TABLE ttb.tune_peak_hour ADD CONSTRAINT tune_peak_hour_time_begin_check_positive CHECK ( time_begin  >= 0);
ALTER TABLE ttb.tune_peak_hour ADD CONSTRAINT tune_peak_hour_time_end_check_positive CHECK ( time_end  >= 0);
ALTER TABLE udump.tasks ADD CONSTRAINT tasks_period_check_positive CHECK ( period  >= 0);
ALTER TABLE voip.voice_call_request ADD CONSTRAINT voice_call_request_call_request_status_check_positive CHECK ( call_request_status  >= 0);
ALTER TABLE wh.wh_core ADD CONSTRAINT wh_core_val_int_01_check_positive CHECK ( val_int_01  >= 0);
ALTER TABLE wh.wh_core ADD CONSTRAINT wh_core_val_int_02_check_positive CHECK ( val_int_02  >= 0);

/*client data*/
INSERT INTO jofl.win_field_ref
(db_method, field_name, ref_db_method, ref_field, dst_field, is_custom_factory, ref_factory)
SELECT wf.db_method
  , wf.field_name
  ,wf.field_name ref_db_method
  ,null ref_field
  ,wf.field_name dst_field
  ,1 is_custom_factory
  ,'ru.advantum.agilebeans.service.editor.RegExpEditor|{"ERRMSG":"������������� �����.", "REGEXP":"^[0-9]\d*$"}' ref_factory
FROM jofl.win_field wf
WHERE field_type IN ('N', 'F') AND field_cons_type = 2 AND n_visibility > 0
and not exists (select 1 from jofl.win_field_ref wfr where wfr.db_method=wf.db_method and wfr.field_name=wf.field_name)
;