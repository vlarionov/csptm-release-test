
-- Table oud.tt_variant

--drop TABLE oud.tt_variant;

CREATE TABLE oud.tt_variant(
  tt_variant_id Integer NOT NULL,
  order_date Date NOT NULL,
  route_variant_id Integer NOT NULL,
  action_period tsrange NOT NULL,
  ttv_name Text NOT NULL,
  sign_deleted Boolean DEFAULT false NOT NULL
)
PARTITION BY RANGE (order_date) TABLESPACE oud_data;


CREATE TABLE oud.tt_variant_1712 PARTITION OF oud.tt_variant FOR VALUES FROM ('2017-12-01') TO ('2018-01-01');

CREATE TABLE oud.tt_variant_1801 PARTITION OF oud.tt_variant FOR VALUES FROM ('2018-01-01') TO ('2018-02-01');

CREATE TABLE oud.tt_variant_1802 PARTITION OF oud.tt_variant FOR VALUES FROM ('2018-02-01') TO ('2018-03-01');

CREATE TABLE oud.tt_variant_1803 PARTITION OF oud.tt_variant FOR VALUES FROM ('2018-03-01') TO ('2018-04-01');

CREATE TABLE oud.tt_variant_1804 PARTITION OF oud.tt_variant FOR VALUES FROM ('2018-04-01') TO ('2018-05-01');

CREATE TABLE oud.tt_variant_1805 PARTITION OF oud.tt_variant FOR VALUES FROM ('2018-05-01') TO ('2018-06-01');



COMMENT ON TABLE oud.tt_variant IS 'Варианты расписания'
;
COMMENT ON COLUMN oud.tt_variant.tt_variant_id IS 'Идентификатор варианта расписания'
;
COMMENT ON COLUMN oud.tt_variant.order_date IS 'Дата оперативного расписания(наряда)'
;
COMMENT ON COLUMN oud.tt_variant.route_variant_id IS 'Вариант маршрута'
;
COMMENT ON COLUMN oud.tt_variant.action_period IS 'Период действия'
;
COMMENT ON COLUMN oud.tt_variant.ttv_name IS 'Номер варианта расписания для пользователей'
;
COMMENT ON COLUMN oud.tt_variant.sign_deleted IS 'Удален'
;

/*
-- Create indexes for table oud.tt_variant

CREATE UNIQUE INDEX ux_tt_variant_dt_oper_tt_variant_id ON oud.tt_variant (order_date,tt_variant_id)
TABLESPACE oud_idx
;

CREATE INDEX ix_tt_variant_order_date ON oud.tt_variant (order_date)
TABLESPACE oud_idx
;

CREATE INDEX ix_tt_variant_actperiod_plan ON oud.tt_variant USING gist (action_period)
TABLESPACE oud_idx
  WHERE NOT sign_deleted and parent_tt_variant_id is null
;

CREATE INDEX ix_tt_variant_tt_variant_id ON oud.tt_variant (tt_variant_id)
TABLESPACE oud_idx
;

CREATE INDEX ix_tt_variant_route_variant_id ON oud.tt_variant (route_variant_id)
TABLESPACE oud_idx
;

CREATE INDEX ix_tt_variant_norm_id ON oud.tt_variant (norm_id)
TABLESPACE oud_idx
;
*/










-- Table oud.tt_out

CREATE TABLE oud.tt_out(
  tt_variant_id Integer NOT NULL,
  order_date Date NOT NULL,
  tt_out_num Smallint NOT NULL,
  tr_id Bigint,
  tr_capacity_id Smallint,
  mode_id Smallint,
  action_range tsrange,
  sign_deleted Boolean DEFAULT false NOT NULL
) PARTITION BY RANGE (order_date) TABLESPACE oud_data;
;

COMMENT ON TABLE oud.tt_out IS 'Выходы'
;
COMMENT ON COLUMN oud.tt_out.tt_variant_id IS 'Вариант расписания'
;
COMMENT ON COLUMN oud.tt_out.order_date IS 'Дата наряда'
;
COMMENT ON COLUMN oud.tt_out.tt_out_num IS 'Номер выхода'
;
COMMENT ON COLUMN oud.tt_out.tr_id IS 'ТС поставленное на выход'
;
COMMENT ON COLUMN oud.tt_out.tr_capacity_id IS 'Вместимость ТС'
;
COMMENT ON COLUMN oud.tt_out.mode_id IS 'Режим труда и отдыха'
;
COMMENT ON COLUMN oud.tt_out.action_range IS 'Период действия. Заполняется автоматом, от минимального до макс. периода действия action`ов'
;
COMMENT ON COLUMN oud.tt_out.sign_deleted IS 'Удален'
;

-- Create indexes for table oud.tt_out

CREATE UNIQUE INDEX uq_tt_out ON oud.tt_out (tt_out_num,order_date,tt_variant_id)
TABLESPACE oud_idx
  WHERE not sign_deleted
;

CREATE INDEX ix_tt_out_actrange ON oud.tt_out USING gist (action_range)
TABLESPACE oud_idx
;

CREATE INDEX ix_tt_out_tt_variant ON oud.tt_out (tt_variant_id)
TABLESPACE oud_idx
;

CREATE INDEX ix_tt_out_tr_id ON oud.tt_out (tr_id)
TABLESPACE oud_idx
;

CREATE INDEX ix_tt_out_tr_capacity_id ON oud.tt_out (tr_capacity_id)
TABLESPACE oud_idx
;

CREATE INDEX ix_tt_out_mode_id ON oud.tt_out (mode_id)
TABLESPACE oud_idx
;


CREATE TABLE oud.tt_out_1712 PARTITION OF oud.tt_out FOR VALUES FROM ('2017-12-01') TO ('2018-01-01');

CREATE TABLE oud.tt_out_1801 PARTITION OF oud.tt_out FOR VALUES FROM ('2018-01-01') TO ('2018-02-01');

CREATE TABLE oud.tt_out_1802 PARTITION OF oud.tt_out FOR VALUES FROM ('2018-02-01') TO ('2018-03-01');

CREATE TABLE oud.tt_out_1803 PARTITION OF oud.tt_out FOR VALUES FROM ('2018-03-01') TO ('2018-04-01');

CREATE TABLE oud.tt_out_1804 PARTITION OF oud.tt_out FOR VALUES FROM ('2018-04-01') TO ('2018-05-01');

CREATE TABLE oud.tt_out_1805 PARTITION OF oud.tt_out FOR VALUES FROM ('2018-05-01') TO ('2018-06-01');



select order_date
from ttb.tt_variant
group by order_date;

insert into oud.tt_variant (
  tt_variant_id,
  order_date,
  route_variant_id,
  action_period,
  ttv_name,
  sign_deleted
)
select
  parent_tt_variant_id,
  order_date,
  route_variant_id,
  action_period,
  ttv_name,
  sign_deleted
from ttb.tt_variant
where order_date is not null;




insert into oud.tt_out(
  tt_variant_id,
  order_date,
  tt_out_num,
  tr_id,
  tr_capacity_id,
  mode_id,
  action_range,
  sign_deleted
)
select
  tt_variant.tt_variant_id,
  tt_variant.order_date,
  tt_out.tt_out_num,
  tt_out.tr_id,
  tt_out.tr_capacity_id,
  mode_id,
  tt_out.action_range,
  tt_out.sign_deleted
from ttb.tt_out
  join oud.tt_variant on tt_variant.tt_variant_id = tt_out.tt_variant_id;



EXPLAIN select *
from oud.tt_variant
where order_date = '2018-04-12';

EXPLAIN select *
        from oud.tt_out
        where order_date = '2018-04-12';

ALTER TABLE oud.tt_variant_1805 ADD CONSTRAINT fk_tt_variant_to_oud_tt_variant FOREIGN KEY (tt_variant_id) REFERENCES ttb.tt_variant (tt_variant_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE oud.tt_variant_1805 ADD CONSTRAINT fk_route_variant_to_tt_variant FOREIGN KEY (route_variant_id) REFERENCES rts.route_variant (route_variant_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;
