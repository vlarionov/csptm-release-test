insert into oud.traffic_control_mode(traffic_control_mode_name, traffic_control_mode_comment)
values('Уличное расписание', 'Движение на маршруте по уличному расписанию');

insert into oud.traffic_control_mode(traffic_control_mode_name, traffic_control_mode_comment)
values('Интервальный', 'Движение ТС по интервалу');

insert into oud.traffic_control_mode(traffic_control_mode_name, traffic_control_mode_comment)
values('Снижение', 'Движение ТС на маршруте со снижением скорости на 5-35% (относительно планового расписания)');

insert into oud.traffic_control_mode(traffic_control_mode_name, traffic_control_mode_comment)
values('Без расписания', 'Движение ТС на маршруте без расписания');