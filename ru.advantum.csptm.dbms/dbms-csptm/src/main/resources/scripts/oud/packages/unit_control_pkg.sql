create or replace function oud.unit_control_pkg$crone() returns int as
$$
begin

  perform oud.unit_control_pkg$check_unit();
  return 1;
end;
$$
language 'plpgsql';


create or replace function oud.unit_control_pkg$check_unit()
  returns void as
$$
declare
  l_result json;
begin

    with tt_oper as
    (SELECT
       tr_id,
       min(lower(action_period)) time_start,
       max(upper(action_period)) time_finish
     FROM ttb.tt_variant
       JOIN ttb.tt_out ON tt_variant.tt_variant_id = tt_out.tt_variant_id
     WHERE action_period @> now() :: TIMESTAMP
           AND tt_variant.parent_tt_variant_id IS NOT NULL
           AND NOT tt_variant.sign_deleted
           AND NOT tt_out.sign_deleted
           AND tt_out.tr_id IS NOT NULL
     GROUP BY tr_id
    )
    ,last_call_trs as
      (select tr_id, unit_id
      FROM core.traffic
      WHERE traffic.event_time BETWEEN now():: TIMESTAMP - INTERVAL '1 minutes' * asd.get_constant('MIN_TIME_FOR_TR_DEVIATION') AND now():: TIMESTAMP
        and exists (select 1 from tt_oper where traffic.tr_id = tt_oper.tr_id)
      GROUP BY tr_id, unit_id
    )
    ,last_incidents as (
      select incident.tr_id
      from ttb.incident
        join tt_oper on tt_oper.tr_id = incident.tr_id
      where incident.time_start BETWEEN tt_oper.time_start and tt_oper.time_finish
            and incident.incident_type_id = ttb.jf_incident_pkg$cn_comming_off()
            and incident.time_start BETWEEN now():: TIMESTAMP - INTERVAL '2 days' AND now():: TIMESTAMP
      GROUP BY incident.tr_id
    )
      select
        ttb.jf_incident_pkg$send_incident(array_agg(
                                              ttb.jf_incident_pkg$new_incident(
                                                  p_timeStart := now() :: TIMESTAMP
                                                  , p_incident_initiator_id := ttb.jf_incident_pkg$cn_initiator_system()
                                                  , p_incident_status_id := ttb.jf_incident_pkg$cn_status_new()
                                                  , p_incident_type_id := ttb.jf_incident_pkg$cn_comming_off()
                                                  , p_parent_incident_id := null
                                                  , p_account_id := NULL
                                                  , p_incident_payload :=
                                                  ttb.jf_incident_pkg$new_payload(
                                                      p_trList := ARRAY [tt_oper.tr_id::int]
                                                  )
                                              )
                                          )
        )
      into l_result
    from tt_oper
    where
      not exists (select 1 from last_call_trs where last_call_trs.tr_id = tt_oper.tr_id)
      and
      not exists (select 1 from last_incidents where last_incidents.tr_id = tt_oper.tr_id)
    GROUP BY tr_id;
end;
$$
language 'plpgsql';
