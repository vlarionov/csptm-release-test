﻿
create or replace function oud.jf_driver_calls_drivers4select_pkg$of_rows
  (p_account_id               in numeric,
   p_attr                     in text,
   p_rows                     out refcursor)
returns refcursor
as $$ 
begin
   open p_rows for
   select orl.tr_id,
          orl.driver_id,
          coalesce(dr.driver_last_name, '') || ' ' ||
          coalesce(dr.driver_name, '') || ' ' ||
          coalesce(dr.driver_middle_name, '') as driver_name,
          tr.garage_num,
          rt.route_num || ' ' || tt.short_name as route_num,
          dr.tab_num,
          orl.out_num || '/' || sh.dr_shift_num as out_num,
          json_build_object('tr', orl.tr_id, 'dr', orl.driver_id)::text as tr_driver
          --orl.tr_id::text || ' ' || orl.driver_id::text as tr_driver
          --orl.tr_id as tr_driver_id
     from ttb.order_list orl
     join core.tr tr on tr.tr_id = orl.tr_id
     join core.driver dr on dr.driver_id = orl.driver_id
     join ttb.dr_shift sh on sh.dr_shift_id = orl.dr_shift_id
     join rts.route_variant rv on rv.route_variant_id = orl.route_variant_id
     join rts.route rt on rt.route_id = rv.route_id
     join core.tr_type tt on tt.tr_type_id = rt.tr_type_id
    where orl.order_date = localtimestamp::date::timestamp
      and localtimestamp between orl.time_from and orl.time_to
	  and tr.tr_id in (select adm.account_data_realm_pkg$get_trs_by_all(p_account_id))
    order by dr.driver_last_name;
end;
$$ language plpgsql volatile security invoker;
