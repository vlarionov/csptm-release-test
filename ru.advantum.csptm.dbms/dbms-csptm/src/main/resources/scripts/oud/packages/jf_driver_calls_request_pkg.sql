﻿
create or replace function oud.jf_driver_calls_request_pkg$of_rows
  (p_account_id               in numeric,
   p_attr                     in text,
   p_rows                     out refcursor)
returns refcursor
as $$ declare 
   l_query_date       timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'order_date', true), now());
   l_order_date       timestamp := date_trunc('day', l_query_date);
   l_tr_id            bigint    := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_id', true);
begin
   open p_rows for
   select vcr.call_request_id,
          vcr.tr_id,
          vcr.unit_id,
          vcr.call_request_date,
          tr.garage_num,
          tr.licence,
          eqt.name_short as unit_type_short_name,
          equ.serial_num as unit_num
     from voip.voice_call_request vcr
     join core.tr on tr.tr_id = vcr.tr_id
     join core.equipment       equ on equ.equipment_id = vcr.unit_id
     join core.equipment_model eqm on eqm.equipment_model_id = equ.equipment_model_id
     join core.equipment_type  eqt on eqt.equipment_type_id = eqm.equipment_type_id
    where vcr.call_request_date between l_order_date and l_order_date + interval '1 day'
      and vcr.voice_call_id is null
      and vcr.tr_id = l_tr_id
    order by vcr.call_request_date;
end;
$$ language plpgsql volatile security invoker;
