create or replace function oud.jf_tr_out_driver_pkg$of_rows(
    p_account_id in  numeric,
    p_attr       in  text,
    p_rows       out refcursor
)
    returns refcursor
language plpgsql
as $$
declare
    l_f_route_id      integer [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_route_id', true);
    l_f_tr_id         integer [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_tr_id', true);
    l_f_order_date_dt date := jofl.jofl_pkg$extract_date(p_attr, 'f_order_date_DT', true);
        l_available_route_ids integer []:= array(select adm.account_data_realm_pkg$get_routes(p_account_id));
begin
    open p_rows for
    select
        distinct
        ttv.order_date,
        rv.route_variant_id,
        r.route_id,
        tr.tr_id,
        r.route_num,
        trt.name,
        tr.garage_num,
        dr.driver_last_name || ' ' || dr.driver_name || ' ' ||dr.driver_middle_name  as full_name,
        dr.tab_num,
        tto.tt_out_num,
        tto.tt_out_id,
        ds.dr_shift_num
    from ttb.tt_out tto
        join ttb.tt_action tta on tto.tt_out_id = tta.tt_out_id
        join ttb.tt_action_item tai on tta.tt_action_id = tai.tt_action_id
        join ttb.dr_shift ds on tai.dr_shift_id = ds.dr_shift_id
        join ttb.tt_variant ttv on tto.tt_variant_id = ttv.tt_variant_id
        join core.tr tr on tto.tr_id = tr.tr_id
        join core.tr_type trt on tr.tr_type_id = trt.tr_type_id
        join core.driver dr on tai.driver_id = dr.driver_id
        join rts.route_variant rv on ttv.route_variant_id = rv.route_variant_id
        left join ttb.tt_action_round ttar on tta.tt_action_id = ttar.tt_action_id
        left join rts.round rnd on ttar.round_id = rnd.round_id
        join rts.route r on rv.route_id = r.route_id
    where (ttv.order_date :: date = l_f_order_date_dt) and
          (r.route_id = any (l_f_route_id) or (cardinality(l_f_route_id) = 0)) and
          (tr.tr_id = any (l_f_tr_id) or (cardinality(l_f_tr_id) = 0)) and
          not tta.sign_deleted and
        (r.route_id = any (l_available_route_ids) or l_available_route_ids isnull)
    ;
end;
$$;