create or replace function oud.jf_tt_out_fact_pkg$of_rows(
  p_account_id in  numeric,
  p_attr       in  text,
  p_rows       out refcursor
)
  returns refcursor
language plpgsql
as $$
declare
  l_f_order_date       date := jofl.jofl_pkg$extract_date(p_attr, 'f_order_date_DT', true);
  l_f_route_tr_type_id core.tr_type.tr_type_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_route_tr_type_id', true);
  l_f_route_id         rts.route.route_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', true);
  l_f_tt_out_num       ttb.tt_out.tt_out_num%type := jofl.jofl_pkg$extract_number(p_attr, 'f_tt_out_num_NUMBER', true);
  l_f_dr_shift_num     ttb.dr_shift.dr_shift_num%type := jofl.jofl_pkg$extract_number(p_attr, 'f_dr_shift_num_NUMBER', true);
  l_f_tr_garage_num    core.tr.garage_num%type := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_garage_num_NUMBER', true);

  l_duration_format    text := 'HH24:MI:SS';
begin
  open p_rows for
  with action_item as (
      select
        tto.tt_out_id,
        drs.dr_shift_id,
        tr.tr_id,
        dr.driver_id,
        tta.tt_action_id,
        ttaf.tt_action_fact_id,
        vm_action_type.is_production_round                                is_production_round,
        vm_action_type.is_technical_round                                 is_technical_round,
        (ttar.round_status_id = ttb.round_status_pkg$passed())                                     is_round_passed,
        (ttai.time_begin - lag(ttai.time_begin, 1, ttai.time_begin) over tt_action)                item_plan_duration,
        (ttaf.time_fact_begin - lag(ttaf.time_fact_begin, 1, ttaf.time_fact_begin) over tt_action) item_fact_duration,
        si2r.length_sector
      from ttb.tt_variant ttv
        join ttb.tt_out tto on tto.tt_variant_id = ttv.tt_variant_id
        join rts.route_variant rv on ttv.route_variant_id = rv.route_variant_id
        join rts.route r on rv.route_id = r.route_id
        join core.tr_type rtrt on r.tr_type_id = rtrt.tr_type_id
        join ttb.tt_action tta on tto.tt_out_id = tta.tt_out_id
        join ttb.tt_action_round ttar on tta.tt_action_id = ttar.tt_action_id
        join ttb.tt_action_item ttai on tta.tt_action_id = ttai.tt_action_id
        join ttb.dr_shift drs on ttai.dr_shift_id = drs.dr_shift_id
        join core.tr tr on tto.tr_id = tr.tr_id
        join core.driver dr on ttai.driver_id = dr.driver_id
        join rts.stop_item2round si2r on ttai.stop_item2round_id = si2r.stop_item2round_id
        left join ttb.tt_action_fact ttaf on tta.tt_action_id = ttaf.tt_action_id and ttaf.stop_item2round_id = si2r.stop_item2round_id
        join ttb.vm_action_type on tta.action_type_id = vm_action_type.action_type_id
      where
        ttv.order_date = l_f_order_date
        and not tto.sign_deleted
        and not tta.sign_deleted
        and not ttai.sign_deleted
        and r.route_id in (select adm.account_data_realm_pkg$get_routes(p_account_id))
        and (l_f_route_tr_type_id is null or r.tr_type_id = l_f_route_tr_type_id)
        and (l_f_route_id is null or r.route_id = l_f_route_id)
        and (l_f_tt_out_num is null or tto.tt_out_num = l_f_tt_out_num)
        and (l_f_dr_shift_num is null or drs.dr_shift_num = l_f_dr_shift_num)
        and (l_f_tr_garage_num is null or tr.garage_num = l_f_tr_garage_num)
      window tt_action as ( partition by tta.tt_action_id order by ttai.time_begin )
  ), out_shift_tr_dr as (
      select
        tt_out_id,
        dr_shift_id,
        tr_id,
        driver_id,
        count(distinct tt_action_id) filter (where is_production_round)                         plan_round_count,
        count(distinct tt_action_id) filter (where is_round_passed and is_production_round)     fact_round_count,
        sum(item_fact_duration)                                                                 fact_duration,
        sum(item_plan_duration)                                                                 plan_duration,
        sum(item_fact_duration) filter (where is_production_round)                              production_fact_duration,
        sum(length_sector) filter (where tt_action_fact_id is not null and is_production_round and is_round_passed) production_fact_dist,
        sum(item_fact_duration) filter (where is_technical_round)                               technical_fact_duration,
        sum(length_sector) filter (where tt_action_fact_id is not null and is_technical_round and is_round_passed)  technical_fact_dist
      from action_item
      where true
      group by
        tt_out_id,
        dr_shift_id,
        tr_id,
        driver_id
  )
  select
    ostd.tt_out_id,
    ostd.dr_shift_id,
    ostd.tr_id,
    ostd.driver_id,
    l_f_order_date                                                               order_date,
    rtrt.name                                                                    route_tr_type,
    r.route_num,
    tto.tt_out_num,
    drs.dr_shift_num,
    tr.garage_num                                                                tr_garage_num,
    tr.licence                                                                   tr_serial_num,
    trm.name                                                                     tr_model,
    trc.short_name                                                               tr_capacity,
    dr.tab_num                                                                   dr_tab_num,
    concat_ws(' ', dr.driver_last_name, dr.driver_name, dr.driver_middle_name) dr_full_name,
    plan_round_count,
    fact_round_count,
    to_char(plan_duration, l_duration_format)                                    plan_duration,
    to_char(fact_duration, l_duration_format)                                    fact_duration,
    to_char(production_fact_duration, l_duration_format)                         production_fact_duration,
    to_char(technical_fact_duration, l_duration_format)                          technical_fact_duration,
    round(production_fact_dist ::numeric/ 1000,2)::text as production_fact_dist,
    round(technical_fact_dist ::numeric/ 1000,2) ::text as technical_fact_dist
  from out_shift_tr_dr ostd
    join ttb.tt_out tto on ostd.tt_out_id = tto.tt_out_id
    join ttb.tt_variant ttv on tto.tt_variant_id = ttv.tt_variant_id
    join rts.route_variant rv on ttv.route_variant_id = rv.route_variant_id
    join rts.route r on rv.route_id = r.route_id
    join core.tr_type rtrt on r.tr_type_id = rtrt.tr_type_id
    join ttb.dr_shift drs on ostd.dr_shift_id = drs.dr_shift_id
    join core.tr tr on ostd.tr_id = tr.tr_id
    join core.tr_model trm on tr.tr_model_id = trm.tr_model_id
    join core.tr_capacity trc on trm.tr_capacity_id = trc.tr_capacity_id
    join core.driver dr on ostd.driver_id = dr.driver_id
  order by tto.tt_out_num, drs.dr_shift_num % 100;
end;
$$;

create or replace function oud.jf_tt_out_fact_pkg$get_link(
  p_order_date date,
  p_tt_out_num ttb.tt_out.tt_out_num%type,
  p_route_id rts.route.route_id%type
)
  returns text
language plpgsql as $$
begin
  return '[' || json_build_object(
      'CAPTION', p_tt_out_num,
      'JUMP', json_build_object(
          'METHOD', 'oud.tt_out_fact',
          'ATTR', '{' ||
                  '"f_tt_out_num_NUMBER" : "' || p_tt_out_num || '", ' ||
                  '"f_order_date_DT" : "' || to_char(p_order_date, 'yyyy-MM-dd') || '", ' ||
                  '"f_route_id" : "' || p_route_id || '" ' ||
                  '}'
      )
  ) || ']';
end;
$$;

create or replace function oud.jf_tt_out_fact_pkg$of_rebuild_tt_variant(
  p_account_id in numeric,
  p_attr       in text
)
  returns text
language plpgsql
as $$
declare
  l_tt_out_id     ttb.tt_out.tt_out_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_out_id', false);

  l_tt_variant_id ttb.tt_variant.tt_variant_id%type;
begin
  select tto.tt_variant_id
  from ttb.tt_out tto
  where tto.tt_out_id = l_tt_out_id
  into l_tt_variant_id;

  perform oud.tt_oper_rebuilder_pkg$send_task(l_tt_variant_id);

  return null ;
end;
$$;