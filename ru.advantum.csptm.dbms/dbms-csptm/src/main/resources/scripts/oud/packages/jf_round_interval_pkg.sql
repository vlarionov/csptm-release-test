create or replace function oud.jf_round_interval_pkg$of_rows(
  p_account_id in  numeric,
  p_attr       in  text,
  p_rows       out refcursor
)
  returns refcursor
language plpgsql
as $$
declare
  l_f_order_date       date := jofl.jofl_pkg$extract_date(p_attr, 'f_order_date_DT', true);
  l_f_route_tr_type_id core.tr_type.tr_type_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_route_tr_type_id', true);
  l_f_route_id         rts.route.route_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', true);
  l_f_stop_item_ids    integer [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_stop_item_id', true);
  l_f_is_last_stop     boolean := jofl.jofl_pkg$extract_boolean(p_attr, 'f_is_last_stop_INPLACE_K', true);
  l_f_is_checkpoint    boolean := jofl.jofl_pkg$extract_boolean(p_attr, 'f_is_checkpoint_INPLACE_K', true);
begin
  open p_rows for
  with route_stop_item2round as (
      select
        si2r.stop_item2round_id,
        si2r.stop_item_id,
        r.tr_type_id,
        r.route_num,
        rnd.code,
        rnd.move_direction_id,
        si2r.order_num,
        si2r.stop_item_id in (select sit.stop_item_id
                                from rts.stop_item2round_type si2rt
                                    join rts.stop_item_type sit on si2rt.stop_item_type_id = sit.stop_item_type_id
                                where (sit.stop_type_id = rts.stop_type_pkg$finish_a() or sit.stop_type_id = rts.stop_type_pkg$finish_b()) and si2rt.stop_item2round_id=si2r.stop_item2round_id)                      is_last_stop,
        si2r.stop_item_id in (select sit.stop_item_id
                              from rts.stop_item_type sit
                              where sit.stop_type_id = rts.stop_type_pkg$checkpoint())                    is_checkpoint
      from ttb.tt_variant ttv
        join rts.route_variant rv on ttv.route_variant_id = rv.route_variant_id
        join rts.route r on rv.route_id = r.route_id
        join rts.round rnd on rv.route_variant_id = rnd.route_variant_id
        join rts.stop_item2round si2r on rnd.round_id = si2r.round_id
      where ttv.order_date = l_f_order_date
            and not ttv.sign_deleted
            and not rv.sign_deleted
            and not r.sign_deleted
            and r.route_id in (select adm.account_data_realm_pkg$get_routes(p_account_id))
            and not rnd.sign_deleted
            and not si2r.sign_deleted
            and rnd.code = '00'
            and (l_f_route_id is null or r.route_id = l_f_route_id)
            and (l_f_route_tr_type_id is null or r.tr_type_id = l_f_route_tr_type_id)
  )
  select
    rsi2r.stop_item2round_id,
    l_f_order_date  order_date,
    rsi2r.route_num,
    trt.name        tr_type_name,
    mv.move_direction_name,
    s.name          stop_name,
    rsi2r.order_num stop_order_num,
    rsi2r.is_checkpoint,
    rsi2r.is_last_stop
  from route_stop_item2round rsi2r
    join rts.stop_item2round si2r on rsi2r.stop_item2round_id = si2r.stop_item2round_id
    join core.tr_type trt on rsi2r.tr_type_id = trt.tr_type_id
    join rts.move_direction mv on rsi2r.move_direction_id = mv.move_direction_id
    join rts.stop_item si on rsi2r.stop_item_id = si.stop_item_id
    join rts.stop_location sl on si.stop_location_id = sl.stop_location_id
    join rts.stop s on sl.stop_id = s.stop_id
  where
    (array_length(l_f_stop_item_ids, 1) is null
     or array_length(l_f_stop_item_ids, 1) < 1
     or rsi2r.stop_item_id = any (l_f_stop_item_ids))
    and rsi2r.is_last_stop
    --     and (l_f_is_last_stop is null or not l_f_is_last_stop or rsi2r.is_last_stop)
    and (l_f_is_checkpoint is null or not l_f_is_checkpoint or rsi2r.is_checkpoint)
  order by rsi2r.route_num, mv.move_direction_id, rsi2r.order_num;
end;
$$;