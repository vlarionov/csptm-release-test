create or replace function oud.tt_oper_rebuilder_pkg$send_task(p_tt_variant_id ttb.tt_variant.tt_variant_id%type
                                                            )
  returns void
language plpgsql
as $$
declare
  l_tm_e timestamp;
begin
  l_tm_e:= ttb.timetable_pkg$get_finish_time(
             (select min(arnd.tt_at_round_id)
              from rts.round rnd
                join ttb.tt_at_round arnd on arnd.round_id = rnd.round_id
                join ttb.tt_action_type tact on tact.tt_action_type_id = arnd.tt_action_type_id
              where rnd.move_direction_id = 1
                    and tact.tt_variant_id = p_tt_variant_id
                    and rnd.code ='00')
         ) ;
  perform paa.paa_api_pkg$basic_publish(
      cmnd.jf_cmnd_ref4unit_pkg$cn_command_rmq_broker(),
      'tt-oper-rebuild-tasks',
      '',
      json_build_object('ttVariantId', p_tt_variant_id, 'ttFinishTime', l_tm_e::text) :: text,
      'TTOperRebuildTask'
  );
end;
$$;