create or replace function oud.jf_round_interval_item_pkg$of_rows(
  p_account_id in  numeric,
  p_attr       in  text,
  p_rows       out refcursor
)
  returns refcursor
language plpgsql
as $$
declare
  l_order_date         date := jofl.jofl_pkg$extract_date(p_attr, 'order_date', false);
  l_stop_item2round_id rts.stop.stop_id%type := jofl.jofl_pkg$extract_number(p_attr, 'stop_item2round_id', false);

  l_round_childs       smallint [] := array(select ttb.action_type_pkg$get_child_action_type_list(ttb.action_type_pkg$round()));
  l_main_round_childs  smallint [] := array(select ttb.action_type_pkg$get_child_action_type_list(ttb.action_type_pkg$main_round()));
begin
  open p_rows for
  with recursive stop_item as (
      select distinct stop_item_id
      from rts.stop_item2round
      where stop_item2round_id = l_stop_item2round_id
  ), action_item as (
      select
        tt_action_item.tt_action_item_id,
        tt_action_item.time_begin,
        tt_action_item.time_end,
        tt_action_item.stop_item2round_id,
        stop_item2round.order_num,
        tt_action.tt_action_id,
        tt_action.action_type_id,
        tt_action.parent_tt_action_id,
        tt_out.tt_out_id
      from rts.route_variant
        join ttb.tt_variant on route_variant.route_variant_id = tt_variant.route_variant_id
        join ttb.tt_out on tt_variant.tt_variant_id = tt_out.tt_variant_id
        join ttb.tt_action on tt_out.tt_out_id = tt_action.tt_out_id
        join ttb.tt_action_item on tt_action.tt_action_id = tt_action_item.tt_action_id
        join rts.stop_item2round on tt_action_item.stop_item2round_id = stop_item2round.stop_item2round_id
      where tt_variant.order_date = l_order_date
            and stop_item2round.stop_item_id in (select *
                                                 from stop_item)
            and not route_variant.sign_deleted
            and not tt_variant.sign_deleted
            and not tt_out.sign_deleted
            and not tt_action.sign_deleted
            and not tt_action_item.sign_deleted
  ), action_item_sequence(tt_action_item_id, next_tt_action_item_id, level) as (
    select
      action_item.tt_action_item_id,
      action_item.tt_action_item_id,
      1
    from action_item
    where (action_item.time_begin <> action_item.time_end or action_item.action_type_id = any (l_round_childs))
    union select
            action_item.tt_action_item_id,
            new_next_action_item.tt_action_item_id,
            action_item_sequence.level + 1
          from action_item_sequence
            join action_item on action_item_sequence.tt_action_item_id = action_item.tt_action_item_id
            join action_item next_action_item
              on action_item_sequence.next_tt_action_item_id = next_action_item.tt_action_item_id
            join action_item new_next_action_item on next_action_item.tt_out_id = new_next_action_item.tt_out_id
                                                     and next_action_item.time_end = new_next_action_item.time_begin
                                                     and (next_action_item.time_begin < new_next_action_item.time_end
                                                          or (action_item.tt_action_item_id =
                                                              next_action_item.tt_action_item_id and
                                                              next_action_item.tt_action_item_id <>
                                                              new_next_action_item.tt_action_item_id))
                                                     and (new_next_action_item.time_begin <> new_next_action_item.time_end or
                                                          new_next_action_item.action_type_id = any (l_round_childs))
  ), action_item_sequence_with_max_level as (
      select
        *,
        max(level)
        over (
          partition by tt_action_item_id )      as maxLevelById,
        max(level)
        over (
          partition by next_tt_action_item_id ) as maxLevelByNextId
      from action_item_sequence
  ), oper_stop_item_passing as (
      select
        action_item.tt_action_item_id,
        action_item.action_type_id,
        action_item.tt_action_id,
        action_item.parent_tt_action_id,
        action_item.stop_item2round_id,
        action_item.time_end                 arrival_time,
        next_action_item.tt_action_item_id   next_tt_action_item_id,
        next_action_item.action_type_id      next_action_type_id,
        next_action_item.tt_action_id        next_tt_action_id,
        next_action_item.parent_tt_action_id next_parent_tt_action_id,
        next_action_item.stop_item2round_id  next_stop_item2round_id,
        next_action_item.time_begin          departure_time
      from action_item_sequence_with_max_level ais
        join action_item action_item on ais.tt_action_item_id = action_item.tt_action_item_id
        join action_item next_action_item on ais.next_tt_action_item_id = next_action_item.tt_action_item_id
      where true
            and ais.level = ais.maxLevelById
            and ais.level = ais.maxLevelByNextId
            and next_action_item.stop_item2round_id = l_stop_item2round_id
            and next_action_item.action_type_id = any (l_main_round_childs)
  ), fact_stop_item_passing as (
      select
        oper_passing.tt_action_item_id,
        tt_action_fact.time_fact_end     arrival_time,
        next_action_fact.time_fact_begin departure_time
      from oper_stop_item_passing oper_passing
        left join ttb.tt_action_fact
          on oper_passing.tt_action_id = tt_action_fact.tt_action_id and
             oper_passing.stop_item2round_id = tt_action_fact.stop_item2round_id
        left join ttb.tt_action_fact next_action_fact
          on oper_passing.next_tt_action_id = next_action_fact.tt_action_id and
             oper_passing.next_stop_item2round_id = next_action_fact.stop_item2round_id
  ), oper_stop_item_passing_interval as (
      select
        oper_passing.tt_action_item_id,
        (select oper_passing.departure_time - max(prev_oper_passing.departure_time)
         from oper_stop_item_passing prev_oper_passing
         where oper_passing.departure_time > prev_oper_passing.departure_time
        ) passing_interval
      from oper_stop_item_passing oper_passing
  ), fact_stop_item_passing_interval as (
      select
        fact_passing.tt_action_item_id,
        (select fact_passing.departure_time - max(prev_fact_passing.departure_time)
         from fact_stop_item_passing prev_fact_passing
         where fact_passing.departure_time > prev_fact_passing.departure_time
        ) passing_interval
      from fact_stop_item_passing fact_passing
  )
  select
    distinct
    l_stop_item2round_id                                                                            stop_item2round_id,
    l_order_date                                                                                    order_date,
    oper_passing.tt_action_id,
    oper_passing.next_tt_action_id                                                                  departure_tt_action_id,
    tt_out.tt_out_num,
    oud.jf_tt_out_fact_pkg$get_link(l_order_date, tt_out.tt_out_num, route_variant.route_id)        tt_out_num_link,
    dr_shift.dr_shift_num,
    tr.garage_num                                                                                   tr_garage_num,
    oud.jf_round_interval_item_pkg$get_round_sequential_num(tt_action.tt_action_id)                 round_sequential_num,
    oper_passing.arrival_time                                                                       oper_arrival_time,
    oper_passing.departure_time                                                                     oper_departure_time,
    fact_passing.arrival_time                                                                       fact_arrival_time,
    fact_passing.departure_time                                                                     fact_departure_time,
    oud.jf_round_interval_item_pkg$convert_interval2minutes(oper_passing_interval.passing_interval) oper_interval,
    oud.jf_round_interval_item_pkg$convert_interval2minutes(fact_passing_interval.passing_interval) fact_interval,
    oud.jf_round_interval_item_pkg$format_interval2minutes(
        date_trunc('minute', (oper_passing.arrival_time) :: time) -
        date_trunc('minute', fact_passing.arrival_time :: time))                                    delay,
    oud.jf_round_interval_item_pkg$get_relative_diff(fact_passing_interval.passing_interval,
                                                     oper_passing_interval.passing_interval)        relative_delay,
    oud.jf_round_interval_item_pkg$get_previous_action_type(oper_passing.tt_action_id)              previous_action_type,
    null                                                                                            current_action_type,
    oud.jf_round_interval_item_pkg$get_next_action_type(oper_passing.tt_action_id)                  next_action_type,
    oud.jf_round_interval_item_pkg$get_incident(oper_passing.tt_action_id, 1)                       previous_incident,
    oud.jf_round_interval_item_pkg$get_incident(oper_passing.tt_action_id, 0)                       current_incident,
    case when move_direction.move_direction_id = 1 and stop_item2round.order_num = 1
      then 'P{A},D{OF_UPDATE}' end as                                                               "ROW$POLICY"
  from oper_stop_item_passing oper_passing
    join fact_stop_item_passing fact_passing on oper_passing.tt_action_item_id = fact_passing.tt_action_item_id
    join oper_stop_item_passing_interval oper_passing_interval on oper_passing.tt_action_item_id = oper_passing_interval.tt_action_item_id
    join fact_stop_item_passing_interval fact_passing_interval on oper_passing.tt_action_item_id = fact_passing_interval.tt_action_item_id
    join ttb.tt_action_item on oper_passing.next_tt_action_item_id = tt_action_item.tt_action_item_id
    join ttb.tt_action on tt_action_item.tt_action_id = tt_action.tt_action_id
    join ttb.tt_out on tt_action.tt_out_id = tt_out.tt_out_id
    join ttb.tt_variant on tt_variant.tt_variant_id = tt_out.tt_variant_id
    join rts.route_variant on route_variant.route_variant_id = tt_variant.route_variant_id
    join rts.stop_item2round on tt_action_item.stop_item2round_id = stop_item2round.stop_item2round_id
    join rts.round on stop_item2round.round_id = round.round_id
    join ttb.dr_shift on tt_action_item.dr_shift_id = dr_shift.dr_shift_id
    join core.tr on tt_out.tr_id = tr.tr_id
    join rts.move_direction on round.move_direction_id = move_direction.move_direction_id
  order by oper_passing.departure_time;
end;
$$;

create or replace function oud.jf_round_interval_item_pkg$get_round_sequential_num(
      p_tt_action_id ttb.tt_action.tt_action_id%type,
  out p_result       integer
)
stable
language plpgsql
as $$
begin
  with action_sequence as (
      select
        out_tta.tt_action_id,
        row_number()
        over (
          order by out_tta.dt_action ) sequential_num
      from ttb.tt_action tta
        join ttb.tt_action out_tta on tta.tt_out_id = out_tta.tt_out_id
        join ttb.tt_action_round ttar on out_tta.tt_action_id = ttar.tt_action_id
      where tta.tt_action_id = p_tt_action_id
            and not out_tta.sign_deleted
            and not tta.sign_deleted
  )
  select sequential_num
  from action_sequence
  where tt_action_id = p_tt_action_id

  into p_result;
end;
$$;

create or replace function oud.jf_round_interval_item_pkg$convert_interval2minutes(p_iterval interval)
  returns integer
language plpgsql
as $$
begin
  return (extract(epoch from (p_iterval)) / 60);
end;
$$;

create or replace function oud.jf_round_interval_item_pkg$format_interval2minutes(p_iterval interval)
  returns text
language plpgsql
as $$
declare
  l_minutes integer := oud.jf_round_interval_item_pkg$convert_interval2minutes(p_iterval);
begin
  return to_char(sign(l_minutes), 'SG') || abs(l_minutes);
end;
$$;

create or replace function oud.jf_round_interval_item_pkg$get_relative_diff(p_iterval_1 interval, p_inteval_2 interval)
  returns integer
language plpgsql
as $$
declare
  l_minutes_1 integer := abs(oud.jf_round_interval_item_pkg$convert_interval2minutes(p_iterval_1));
  l_minutes_2 integer := abs(oud.jf_round_interval_item_pkg$convert_interval2minutes(p_inteval_2));
begin
  if l_minutes_2 <> 0
  then return abs(100 * (l_minutes_1 - l_minutes_2) / l_minutes_2);
  else return 0;
    end if ;
end;
$$;

create or replace function oud.jf_round_interval_item_pkg$get_previous_action_type(
      p_tt_action_id ttb.tt_action.tt_action_id%type,
  out p_result       text
)
language plpgsql
as $$
declare
begin
  select action_type.action_type_code
  --     oud.jf_round_interval_item_pkg$get_action_type_name(next_tt_action.action_type_id)
  from ttb.tt_action
    join ttb.tt_action previous_tt_action on tt_action.tt_out_id = previous_tt_action.tt_out_id and tt_action.tt_action_id <> previous_tt_action.tt_action_id
    join ttb.action_type on previous_tt_action.action_type_id = action_type.action_type_id
  where tt_action.tt_action_id = p_tt_action_id
        and not previous_tt_action.sign_deleted
        and (
          previous_tt_action.dt_action < tt_action.dt_action or
          (previous_tt_action.dt_action = tt_action.dt_action and previous_tt_action.action_dur < tt_action.action_dur)
        )
  order by previous_tt_action.dt_action desc, previous_tt_action.action_dur desc
  limit 1
  into p_result;
end;
$$;
create or replace function oud.jf_round_interval_item_pkg$get_next_action_type(
      p_tt_action_id ttb.tt_action.tt_action_id%type,
  out p_result       text
)
language plpgsql
as $$
declare
begin
  select action_type.action_type_code
  --     oud.jf_round_interval_item_pkg$get_action_type_name(next_tt_action.action_type_id)
  from ttb.tt_action
    join ttb.tt_action next_tt_action on tt_action.tt_out_id = next_tt_action.tt_out_id and tt_action.tt_action_id <> next_tt_action.tt_action_id
    join ttb.action_type on next_tt_action.action_type_id = action_type.action_type_id
  where tt_action.tt_action_id = p_tt_action_id
        and not next_tt_action.sign_deleted
        and (
          next_tt_action.dt_action > tt_action.dt_action or
          (next_tt_action.dt_action = tt_action.dt_action and next_tt_action.action_dur > tt_action.action_dur)
        )
  order by next_tt_action.dt_action, next_tt_action.action_dur
  limit 1
  into p_result;
end;
$$;

create or replace function oud.jf_round_interval_item_pkg$get_incident(
      p_tt_action_id ttb.tt_action.tt_action_id%type,
      p_offset       integer,
  out p_result       text
)
language plpgsql
as $$
declare
  l_action_range ttb.tt_action.action_range%type;
begin
  select action_range
  from ttb.tt_action
  where tt_action_id = p_tt_action_id
  into l_action_range;

  select ttb.jf_incident_reason_pkg$get_name_start_from_level((incident.incident_params ->> 'incidentReason') :: int, 3)
  from ttb.incident
  where
    incident.time_start between
    (lower(l_action_range) :: date - interval '1 day')
    and
    (lower(l_action_range) :: date + interval '1 day')
    --     and ttai.time_begin between incident.time_start and incident.time_finish
    and incident.incident_type_id = ttb.jf_incident_pkg$cn_manual()
    and (incident_params -> 'ttActionList' @> jsonb_build_array(p_tt_action_id))
  order by incident.time_start desc, incident.time_finish desc
  offset p_offset
  limit 1
  into p_result;
end;
$$;



create or replace function oud.jf_round_interval_item_pkg$of_update(
  p_id_account numeric,
  p_attr       text
)
  returns text as
$body$
declare
  l_tt_action_id int := jofl.jofl_pkg$extract_number(p_attr, 'departure_tt_action_id', true);
  l_time_begin   timestamp := jofl.jofl_pkg$extract_date(p_attr, 'oper_departure_time', true);
begin
  perform oud.jf_tt_action_fact_pkg$change_tt_action_time_begin(l_tt_action_id, l_time_begin);
  return null;
end;
$body$
language 'plpgsql'
volatile
cost 100;
