create or replace function oud.jf_break_item_fact_pkg$of_rows(
  p_account_id in  numeric,
  p_attr       in  text,
  p_rows       out refcursor
)
  returns refcursor
language plpgsql
as $$
declare
  l_time_format constant text := 'HH:MI';

  l_tt_out_id            ttb.tt_out.tt_out_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_out_id', false);
  l_dr_shift_id          ttb.dr_shift.dr_shift_id%type := jofl.jofl_pkg$extract_number(p_attr, 'dr_shift_id', false);
  l_tr_id                core.tr.tr_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', false);
  l_driver_id            core.driver.driver_id%type := jofl.jofl_pkg$extract_number(p_attr, 'driver_id', false);
  l_tt_action_id         ttb.tt_action.tt_action_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_action_id', false);
begin
  open p_rows for
  select
    s.name                                                                            stop_name,
    ttai.time_begin                                                                   time_plan_begin,
    ttai.time_end                                                                     time_plan_end,
    (extract(epoch from (ttai.time_end - ttai.time_begin)) / 60) :: integer           duration_plan,
    ttaf.time_fact_begin                                                              time_fact_begin,
    ttaf.time_fact_end                                                                time_fact_end,
    (extract(epoch from (ttaf.time_fact_end - ttaf.time_fact_begin)) / 60) :: integer duration_fact,
    at.action_type_name
  from ttb.tt_out tto
    join ttb.tt_action tta on tto.tt_out_id = tta.tt_out_id
    join ttb.tt_action_item ttai on tta.tt_action_id = ttai.tt_action_id
    join core.tr tr on oud.jf_tt_out_fact_pkg$get_tr(tto.tt_out_id, ttai.time_begin) = tr.tr_id
    join core.driver dr on oud.jf_tt_out_fact_pkg$get_driver(tto.tt_out_id, ttai.time_begin) = dr.driver_id
    join rts.stop_item2round si2r on ttai.stop_item2round_id = si2r.stop_item2round_id
    join rts.stop_item si on si2r.stop_item_id = si.stop_item_id
    join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
    join rts.stop s on sl.stop_id = s.stop_id
    join ttb.action_type at on ttai.action_type_id = at.action_type_id
    left join ttb.tt_action_fact ttaf on tta.tt_action_id = ttaf.tt_action_id and ttaf.stop_item2round_id = si2r.stop_item2round_id
  where true
        and not tto.sign_deleted
        and not tta.sign_deleted
        and not ttai.sign_deleted
        and tto.tt_out_id = l_tt_out_id
        and tta.tt_action_id = l_tt_action_id
        and ttai.dr_shift_id = l_dr_shift_id
        and tr.tr_id = l_tr_id
        and dr.driver_id = l_driver_id
  order by ttai.time_begin, ttai.time_end;
end;
$$;