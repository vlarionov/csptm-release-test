create or replace function oud.jf_order_list_violation_pkg$of_rows(
    p_account_id in  numeric,
    p_attr       in  text,
    p_rows       out refcursor
)
    returns refcursor
language plpgsql
as $$
declare
  l_dt_begin timestamp := jofl.jofl_pkg$extract_date(p_attr,'BEGIN_DT', true);
  l_dt_end timestamp := jofl.jofl_pkg$extract_date(p_attr,'END_DT', true);
begin

    open p_rows for
with a AS
(
    SELECT array_agg(b::text::jsonb) c
    from ttb.jf_incident_reason_pkg$get_root_tt_oper_problems_list() b
)
select
  incident_id
  ,time_start
  ,incident.incident_params -> 'incidentReason' as incident_reason_id
  ,ttb.jf_incident_reason_pkg$get_name((incident.incident_params ->> 'incidentReason')::int) incident_reason_name
from ttb.incident, a
where
  incident.time_start BETWEEN l_dt_begin and l_dt_end
  and incident.incident_type_id = ttb.jf_incident_pkg$cn_timetable_update()
and incident.incident_params -> 'incidentReason' @> any (a.c);

end;
$$;


CREATE OR REPLACE FUNCTION oud.jf_order_list_violation_pkg$get_order_list_ids(p_incident_id bigint, p_time_start timestamp)
  RETURNS setof bigint AS
  $$
    SELECT value::text::bigint
    FROM ttb.incident, jsonb_array_elements(incident_params -> 'orderList')
    WHERE incident_id = p_incident_id
          AND time_start = p_time_start;
$$          
LANGUAGE SQL;


CREATE OR REPLACE FUNCTION oud.jf_order_list_violation_pkg$get_tt_variant_ids(p_incident_id bigint, p_time_start timestamp)
  RETURNS setof int AS
  $$
    SELECT value::text::int
    FROM ttb.incident, jsonb_array_elements(incident_params -> 'ttVariantList')
    WHERE incident_id = p_incident_id
          AND time_start = p_time_start;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION oud.jf_order_list_violation_pkg$get_route_variant_list_ids(p_incident_id bigint, p_time_start timestamp)
  RETURNS setof int AS
  $$
    SELECT value::text::int
    FROM ttb.incident, jsonb_array_elements(incident_params -> 'routeVarList')
    WHERE incident_id = p_incident_id
          AND time_start = p_time_start;
$$
LANGUAGE SQL;