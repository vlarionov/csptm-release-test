create or replace function oud.tt_oper_control$crone_check_interval() returns int as
$$
begin

perform oud.tt_oper_control$check_interval();
return 1;
end;
$$
language 'plpgsql';


create or replace function oud.tt_oper_control$crone_check_stops_interval() returns int as
$$
begin

perform oud.tt_oper_control$check_stops_interval();
return 1;
end;
$$
language 'plpgsql';

create or replace function oud.tt_oper_control$crone_check_late_from_depo() returns int as
$$
begin

perform oud.tt_oper_control$check_late_from_depo();
return 1;
end;
$$
language 'plpgsql';




--Контроль интервальности
create or replace function oud.tt_oper_control$check_interval()
returns void as
$$
declare
    l_tt_variant_id integer[];
    l_stop_item_id integer[];
    l_payload json[];
begin

with a as (
    SELECT
      parent_tt_variant_id,
      stop_item_id,
      time_plan_begin,
      lead(time_plan_begin) OVER (PARTITION BY tt_variant_id, stop_item_id, tt_out_id ORDER BY time_plan_begin) time_plan_begin_next,
      lead(time_fact_begin) OVER (PARTITION BY tt_variant_id, stop_item_id, tt_out_id ORDER BY time_fact_begin) time_fact_begin_next,
      time_fact_begin
    FROM ttb.v_tt_oper_fact
    WHERE order_date = now() :: DATE
          AND order_num = 1
          AND parent_action_type_id = 1
)
  ,fact_interval_all as (
    SELECT
      stop_item_id,
      parent_tt_variant_id,
      time_fact_begin,
      time_fact_begin_next,
      time_fact_begin_next - time_fact_begin fact_interval,
      ROW_NUMBER()
      OVER (PARTITION BY parent_tt_variant_id
        ORDER BY time_fact_begin DESC) nn
    FROM a
    WHERE time_fact_begin_next IS NOT NULL
)
  ,fact_interval_last as (
    select
      parent_tt_variant_id
      ,stop_item_id
      ,fact_interval
    FROM fact_interval_all
    WHERE nn = 1
)
  ,check_interval as (
    select ttv_oper_check_param.tt_variant_id, cp_val, cp_val_plan
    from
      ttb.ttv_oper_check_param
      join ttb.ttv_oper_check_movement_type on ttv_oper_check_movement_type.tt_variant_id = ttv_oper_check_param.tt_variant_id
      join ttb.tt_variant on ttv_oper_check_param.tt_variant_id = tt_variant.tt_variant_id
    where ttv_oper_cp_type_id = ttb.jf_ttv_oper_cp_type_pkg$cn_interval_tolerance()
          and tt_variant.parent_tt_variant_id is NULL
          and not tt_variant.sign_deleted
          and movement_type_id = ttb.jf_movement_type_pkg$cn_type_interval()
          and (extract(epoch from now()) - extract(epoch from now()::date::timestamp))::int <@ time_range
)
select
  array_agg(
      ttb.jf_incident_pkg$new_incident(
          p_timeStart := now()::timestamp
          ,p_incident_initiator_id := ttb.jf_incident_pkg$cn_initiator_system()
          ,p_incident_status_id := ttb.jf_incident_pkg$cn_status_new()
          ,p_incident_type_id := ttb.jf_incident_pkg$cn_interval_violate()
          ,p_parent_incident_id := null
          ,p_account_id := null
          ,p_incident_payload :=
          ttb.jf_incident_pkg$new_payload (
              p_ttVariantList := ARRAY[tt_variant.tt_variant_id]
              ,p_stopItemList := ARRAY[stop_item_id]
              ,p_factVal :=  date_part('minutes', fact_interval)::int
          )
      )
  )
into l_payload
from check_interval
  JOIN ttb.tt_variant on tt_variant.parent_tt_variant_id = check_interval.tt_variant_id
  left join fact_interval_last on fact_interval_last.parent_tt_variant_id = check_interval.tt_variant_id
where tt_variant.order_date = now() :: DATE
      and (
        (fact_interval > (cp_val + cp_val_plan) * INTERVAL '1 minutes'
         OR fact_interval < (cp_val - cp_val_plan) * INTERVAL '1 minutes'
        )
        OR fact_interval IS NULL
      );


    if array_length(l_payload, 1) > 0 then
      declare
        l_send_result json;
      begin
        l_send_result :=  ttb.jf_incident_pkg$send_incident(
                    p_incidents_array := l_payload
            );
      end;
    end if;
end;
$$
language 'plpgsql';



--Контроль Пачкообразования
--http://redmine.advantum.ru/issues/23442
create or replace function oud.tt_oper_control$check_stops_interval()
returns void as
$$
declare
    CHECK_INTERVAL interval := interval '2 minutes';
    l_payload json[];
begin

    with a as (
        SELECT
          stop_item2round_id
          ,max(time_fact_begin) time_fact_begin_max
        FROM ttb.tt_action_fact
        WHERE time_fact_begin BETWEEN now() :: TIMESTAMP - INTERVAL '1 minutes' AND now() :: TIMESTAMP
        GROUP BY stop_item2round_id
    )
    , time_diff as (
        SELECT
          a.stop_item2round_id
          ,tt_variant.tt_variant_id as tt_variant_id
          ,time_fact_begin_max - max(time_fact_begin) as diff
          ,tt_variant.route_variant_id as route_variant_id
          ,tt_out.tt_out_id
        FROM ttb.tt_action_fact
          JOIN a ON tt_action_fact.stop_item2round_id = a.stop_item2round_id
          join ttb.tt_action on tt_action_fact.tt_action_id = tt_action.tt_action_id
          join ttb.tt_out on tt_action.tt_out_id = tt_out.tt_out_id
          join ttb.tt_variant on tt_out.tt_variant_id = tt_variant.tt_variant_id
        WHERE time_fact_begin BETWEEN now() :: TIMESTAMP - INTERVAL '3 day' AND now() :: TIMESTAMP
              AND time_fact_begin < a.time_fact_begin_max
        GROUP BY a.stop_item2round_id
                 ,tt_variant.tt_variant_id
                ,time_fact_begin_max
                ,tt_variant.route_variant_id
                ,tt_out.tt_out_id
    )

    select
          array_agg(
              ttb.jf_incident_pkg$new_incident(
                  p_timeStart := now()::timestamp
                  ,p_incident_initiator_id := ttb.jf_incident_pkg$cn_initiator_system()
                  ,p_incident_status_id := ttb.jf_incident_pkg$cn_status_new()
                  ,p_incident_type_id := ttb.jf_incident_pkg$cn_congestion()
                  ,p_parent_incident_id := null
                  ,p_account_id := null
                  ,p_incident_payload :=
                  ttb.jf_incident_pkg$new_payload (
                      p_ttVariantList := ARRAY[time_diff.tt_variant_id]
                      ,p_stopItemList := ARRAY[stop_item.stop_item_id]
                      ,p_factVal :=  date_part('minutes', CHECK_INTERVAL)::int
                      ,p_deviationVal := date_part('minutes', time_diff.diff)::int
                      ,p_routeVarList := ARRAY[route_variant_id]
                      ,p_ttOutList := ARRAY[tt_out_id]
                      --,p_incidentReason
                  )
              )
          )
    into l_payload
    from time_diff
          join rts.stop_item2round on stop_item2round.stop_item2round_id= time_diff.stop_item2round_id
          join rts.stop_item on stop_item2round.stop_item_id = stop_item.stop_item_id
    where time_diff.diff < CHECK_INTERVAL;

    if array_length(l_payload, 1) > 0 then
      declare
        l_send_result json;
      begin
        l_send_result :=  ttb.jf_incident_pkg$send_incident(
                    p_incidents_array := l_payload
            );
      end;
    end if;

end;
$$
language 'plpgsql';

--Контроль прогнозируемого опоздания появления на первой остановке маршрута
--http://redmine.advantum.ru/issues/23441
create or replace function oud.tt_oper_control$check_late_from_depo()
returns void as
$$
declare
    CHECK_INTERVAL interval := interval '2 minutes';
    l_payload json[];
begin
    with tt_oper as (
        SELECT
          tt_action.tt_action_id
          ,stop_item_id
          ,order_num
          ,tt_action_item.time_begin
          ,row_number() over (PARTITION BY tt_out.tt_out_id ORDER BY tt_action_item.time_begin) nn
          ,tt_action.action_type_id
          ,tt_out.tt_variant_id
          ,tt_out.tt_out_id
          ,tt_out.tr_id
        FROM ttb.tt_variant
          JOIN ttb.tt_out ON tt_variant.tt_variant_id = tt_out.tt_variant_id
          JOIN ttb.tt_action ON tt_out.tt_out_id = tt_action.tt_out_id
          JOIN ttb.tt_action_item ON tt_action.tt_action_id = tt_action_item.tt_action_id
          JOIN rts.stop_item2round ON stop_item2round.stop_item2round_id = tt_action_item.stop_item2round_id
          join ttb.vm_action_type on tt_action.action_type_id = vm_action_type.action_type_id
        WHERE action_period @> now() :: TIMESTAMP
              AND tt_variant.parent_tt_variant_id IS NOT NULL
              AND NOT tt_variant.sign_deleted
              AND NOT tt_out.sign_deleted
              AND NOT tt_action_item.sign_deleted
              AND vm_action_type.is_production_round
    )
    select
          array_agg(
              ttb.jf_incident_pkg$new_incident(
                  p_timeStart := now()::timestamp
                  ,p_incident_initiator_id := ttb.jf_incident_pkg$cn_initiator_system()
                  ,p_incident_status_id := ttb.jf_incident_pkg$cn_status_new()
                  ,p_incident_type_id := ttb.jf_incident_pkg$cn_late_from_depo()
                  ,p_parent_incident_id := null
                  ,p_account_id := null
                  ,p_incident_payload :=
                  ttb.jf_incident_pkg$new_payload (
                      p_ttVariantList := ARRAY[tt_oper.tt_variant_id]
                      ,p_stopItemList := ARRAY[tt_oper.stop_item_id]
                      ,p_ttOutList := ARRAY[tt_out_id]
                      ,p_trList := ARRAY[tr_id]::int[]
                  )
              )
          )
    into l_payload
    from ibrd.forecast
      join tt_oper on tt_oper.tt_action_id = forecast.tt_action_id
                      and tt_oper.stop_item_id  = forecast.stop_item_id
                      and tt_oper.order_num  = forecast.stop_order_num
    where not pass_time_forecast BETWEEN tt_oper.time_begin and tt_oper.time_begin + CHECK_INTERVAL
          and nn = 1
          and
          (
            pass_time_forecast < now()::timestamp
            OR
            time_begin < now()::timestamp
          );

    if array_length(l_payload, 1) > 0 then
      declare
        l_send_result json;
      begin
        l_send_result :=  ttb.jf_incident_pkg$send_incident(
                    p_incidents_array := l_payload
            );
      end;
    end if;
end;
$$
language 'plpgsql';



create or replace function oud.tt_oper_control$check_action_late()
returns void as
$$
declare
    l_payload json[];
begin
--http://redmine.advantum.ru/issues/23434
    with a as (SELECT
                 tt_out.tt_variant_id,
                 tt_variant.route_variant_id,
                 stop_item_id,
                 tt_out.tt_out_id,
                 tt_out.tr_id
               FROM ttb.tt_action_round
                 JOIN ttb.tt_action ON tt_action_round.tt_action_id = tt_action.tt_action_id
                 JOIN ttb.tt_out ON tt_action.tt_out_id = tt_out.tt_out_id
                 JOIN ttb.tt_variant ON tt_out.tt_variant_id = tt_variant.tt_variant_id
                 JOIN rts.stop_item2round ON stop_item2round.round_id = tt_action_round.round_id
               WHERE dt_action BETWEEN now() - INTERVAL '4 minutes' AND now() - INTERVAL '2 minutes'
                     AND round_status_id <> ttb.round_status_pkg$in_progress()
                     AND tt_out.tr_id is not null
                     AND not tt_variant.sign_deleted
                     AND not tt_action.sign_deleted
                     AND not tt_out.sign_deleted
                     AND not stop_item2round.sign_deleted
                     AND order_num = 1
               GROUP BY tt_out.tt_variant_id
                        ,tt_variant.route_variant_id
                        ,stop_item_id
                        ,tt_out.tt_out_id
                        ,tt_out.tr_id
    )
    select
      array_agg(
          ttb.jf_incident_pkg$new_incident(
              p_timeStart := now()::timestamp
              ,p_incident_initiator_id := ttb.jf_incident_pkg$cn_initiator_system()
              ,p_incident_status_id := ttb.jf_incident_pkg$cn_status_new()
              ,p_incident_type_id := ttb.jf_incident_pkg$cn_action_late()
              ,p_parent_incident_id := null
              ,p_account_id := null
              ,p_incident_payload :=
              ttb.jf_incident_pkg$new_payload (
                  p_ttVariantList := ARRAY[a.tt_variant_id]
                  ,p_stopItemList := ARRAY[a.stop_item_id]
                  ,p_routeVarList := ARRAY[a.route_variant_id]
                  ,p_ttOutList := ARRAY[a.tt_out_id]
                  ,p_trList := ARRAY[a.tr_id]::int[]
              --,p_incidentReason
              )
          )
      )
    into l_payload
    from a;

    if array_length(l_payload, 1) > 0 then
      declare
        l_send_result json;
      begin
        l_send_result :=  ttb.jf_incident_pkg$send_incident(
                    p_incidents_array := l_payload
            );
      end;
    end if;
end;
$$
language 'plpgsql';