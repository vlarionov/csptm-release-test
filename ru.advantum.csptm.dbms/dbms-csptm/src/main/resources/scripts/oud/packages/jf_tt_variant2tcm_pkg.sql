CREATE OR REPLACE FUNCTION oud.jf_tt_variant2tcm_pkg$attr_to_rowtype(p_attr text)
  RETURNS oud.tt_variant2tcm AS
$BODY$
declare
  l_r oud.tt_variant2tcm%rowtype;
begin
  l_r.tt_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', false);
  l_r.traffic_control_mode_id := jofl.jofl_pkg$extract_number(p_attr, 'traffic_control_mode_id', false);
  l_r.val_min := jofl.jofl_pkg$extract_number(p_attr, 'val_min', false);
  l_r.val_max := jofl.jofl_pkg$extract_number(p_attr, 'val_max', false);
  return l_r;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;

create or replace function  oud.jf_tt_variant2tcm_pkg$of_rows(p_id_account in numeric, out p_rows refcursor, p_attr text)
  returns refcursor  as $$
declare
  l_tt_variant_id int := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', false);
begin
    open p_rows for
    select
      tt_variant2tcm.tt_variant_id
      ,traffic_control_mode.traffic_control_mode_name as name
      ,traffic_control_mode.traffic_control_mode_comment as comment
      ,tt_variant2tcm.val_min
      ,tt_variant2tcm.val_max
      ,tt_variant2tcm.traffic_control_mode_id as traffic_control_mode_id
    from oud.tt_variant2tcm
            join oud.traffic_control_mode on tt_variant2tcm.traffic_control_mode_id = traffic_control_mode.traffic_control_mode_id
    where tt_variant_id = l_tt_variant_id;
end;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION oud.jf_tt_variant2tcm_pkg$of_insert(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r oud.tt_variant2tcm%rowtype;
begin
  l_r := oud.jf_tt_variant2tcm_pkg$attr_to_rowtype(p_attr);

  perform oud.jf_tt_variant2tcm_pkg$of_check_vals(l_r.val_min, l_r.val_max);

  insert into oud.tt_variant2tcm select l_r.*;

  return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION oud.jf_tt_variant2tcm_pkg$of_update(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r oud.tt_variant2tcm%rowtype;
begin
  l_r := oud.jf_tt_variant2tcm_pkg$attr_to_rowtype(p_attr);

  perform oud.jf_tt_variant2tcm_pkg$of_check_vals(l_r.val_min, l_r.val_max);

  delete from oud.tt_variant2tcm
  where tt_variant_id = l_r.tt_variant_id;

  insert into oud.tt_variant2tcm select l_r.*;

  return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION oud.jf_tt_variant2tcm_pkg$of_delete(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r oud.tt_variant2tcm%rowtype;
begin
  l_r := oud.jf_tt_variant2tcm_pkg$attr_to_rowtype(p_attr);
  delete from oud.tt_variant2tcm
  where tt_variant_id = l_r.tt_variant_id;

  return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION oud.jf_tt_variant2tcm_pkg$of_check_vals(
  p_min_val int,
  p_max_val int)
  RETURNS void AS
$BODY$
begin
   if p_min_val > p_max_val then
    raise exception 'Минимальное значение не должно превышать максимальное!';
   end if;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;


