CREATE OR REPLACE FUNCTION oud.jf_tt_variant_incident_check_param_pkg$attr_to_rowtype (p_attr text) RETURNS oud.tt_variant_incident_check_param
	LANGUAGE plpgsql
AS $$

declare
   l_r oud.tt_variant_incident_check_param%rowtype;
begin
   l_r.tt_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
   l_r.pct_tr_qty := jofl.jofl_pkg$extract_varchar(p_attr, 'pct_tr_qty', true);
   l_r.pct_interval := jofl.jofl_pkg$extract_varchar(p_attr, 'pct_interval', true);
   l_r.pct_route := jofl.jofl_pkg$extract_varchar(p_attr, 'pct_route', true);

   return l_r;
end;
$$
;


CREATE OR REPLACE FUNCTION oud.jf_tt_variant_incident_check_param_pkg$of_delete (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r oud.tt_variant_incident_check_param%rowtype;
begin
   l_r := oud.jf_tt_variant_incident_check_param_pkg$attr_to_rowtype(p_attr);

   delete from  oud.tt_variant_incident_check_param where  tt_variant_id = l_r.tt_variant_id;

   return null;
end;
$$
;


CREATE OR REPLACE FUNCTION oud.jf_tt_variant_incident_check_param_pkg$of_insert (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r oud.tt_variant_incident_check_param%rowtype;
begin
   l_r := oud.jf_tt_variant_incident_check_param_pkg$attr_to_rowtype(p_attr);

   insert into oud.tt_variant_incident_check_param select l_r.*;

   return null;
end;
$$
;


CREATE OR REPLACE FUNCTION oud.jf_tt_variant_incident_check_param_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$
declare
    l_tt_variant_id int := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
begin
 open p_rows for
      select
        tt_variant_id,
        pct_tr_qty,
        pct_interval,
        pct_route
      from oud.tt_variant_incident_check_param
      where tt_variant_id = l_tt_variant_id;
end;
$$
;


CREATE OR REPLACE FUNCTION oud.jf_tt_variant_incident_check_param_pkg$of_update (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r oud.tt_variant_incident_check_param%rowtype;
begin
   l_r := oud.jf_tt_variant_incident_check_param_pkg$attr_to_rowtype(p_attr);

   update oud.tt_variant_incident_check_param set
          pct_tr_qty = l_r.pct_tr_qty,
          pct_interval = l_r.pct_interval,
          pct_route = l_r.pct_route
   where
          tt_variant_id = l_r.tt_variant_id;

   return null;
end;
$$
;
