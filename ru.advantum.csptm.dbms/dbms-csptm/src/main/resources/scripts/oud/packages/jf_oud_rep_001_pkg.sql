create or replace function oud.jf_tt_out_fact_pkg$of_rows(
  p_account_id in  numeric,
  p_attr       in  text,
  p_rows       out refcursor
)
  returns refcursor
language plpgsql
as $$
declare
  l_f_order_date       date := jofl.jofl_pkg$extract_date(p_attr, 'f_order_date_DT', true);
  l_f_route_tr_type_id core.tr_type.tr_type_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_route_tr_type_id', true);
  l_f_route_id         rts.route.route_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', true);
  l_f_tt_out_num       ttb.tt_out.tt_out_num%type := jofl.jofl_pkg$extract_number(p_attr, 'f_tt_out_num_NUMBER', true);
  l_f_dr_shift_num     ttb.dr_shift.dr_shift_num%type := jofl.jofl_pkg$extract_number(p_attr, 'f_dr_shift_num_NUMBER', true);
  l_f_tr_garage_num    core.tr.garage_num%type := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_garage_num_NUMBER', true);

  l_duration_format    text := 'HH24:MI:SS';
begin
  open p_rows for

    with all_rounds as (
        select
          tt_out.tt_out_id
          ,tt_out.tt_out_num
          ,tt_variant.ttv_name
          ,tt_action.tt_action_id
          ,is_production_round
          ,is_technical_round
          ,dt_action
          ,upper(tt_action.action_range) action_end
          ,tt_action_round.time_fact_end
          ,row_number() over (partition by tt_out.tt_out_id, is_production_round order by upper(tt_action.action_range) desc) row_number_production_round
          ,row_number() over (partition by tt_out.tt_out_id, is_technical_round order by upper(tt_action.action_range) desc) row_number_technical_round
          ,tt_action.action_type_id
        from ttb.tt_variant
          join ttb.tt_out on tt_variant.tt_variant_id = tt_out.tt_variant_id
          join ttb.tt_action on tt_out.tt_out_id = tt_action.tt_out_id
          join ttb.tt_action_round on tt_action.tt_action_id = tt_action_round.tt_action_id
          join ttb.vm_action_type on vm_action_type.action_type_id = tt_action.action_type_id
        where order_date = '2018-05-02' :: date
                and not tt_variant.sign_deleted
                and not tt_out.sign_deleted
                and not tt_variant.sign_deleted
                and r.route_id in (select adm.account_data_realm_pkg$get_routes(p_account_id))
                and (l_f_route_tr_type_id is null or r.tr_type_id = l_f_route_tr_type_id)
                and (l_f_route_id is null or r.route_id = l_f_route_id)
                and (l_f_tt_out_num is null or tto.tt_out_num = l_f_tt_out_num)
                and (l_f_dr_shift_num is null or drs.dr_shift_num = l_f_dr_shift_num)
                and (l_f_tr_garage_num is null or tr.garage_num = l_f_tr_garage_num)
    )
    ,technical_rounds as (
    SELECT
       max(action_end)
         FILTER (WHERE is_technical_round)  plan_time_technical_round,
       max(time_fact_end)
         FILTER (WHERE is_technical_round)  fact_time_technical_round,
       tt_out_id
     FROM all_rounds
     WHERE row_number_technical_round = 1
     GROUP BY tt_out_id
    )
      ,production_rounds as
    (SELECT
       max(action_end)
         FILTER (WHERE is_production_round) plan_time_production_round,
       max(time_fact_end)
         FILTER (WHERE is_production_round) fact_time_production_round,
       tt_out_id
     FROM all_rounds
     WHERE row_number_production_round = 1
     GROUP BY tt_out_id
    )
    select *
    from technical_rounds join production_rounds
        on technical_rounds.tt_out_id = production_rounds.tt_out_id
    ;

end;
$$;