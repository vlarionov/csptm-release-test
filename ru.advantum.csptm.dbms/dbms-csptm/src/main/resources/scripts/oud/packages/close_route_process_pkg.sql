create or replace function oud.close_route_process_pkg$crone() returns int as
$$
begin

  perform oud.unit_control_pkg$check_unit();
  return 1;
end;
$$
language 'plpgsql';

create or replace function oud.close_route_process_pkg$run() returns int as
$$
declare
    l_return int;
begin
    perform
    (with tt_oper as
    (
        SELECT
          tt_variant.tt_variant_id
          ,row_number()
          OVER (PARTITION BY tt_variant.tt_variant_id
            ORDER BY tt_action_item.time_begin desc) nn
          ,tt_action_item.time_begin
          ,tt_action_round.round_status_id
          ,tt_action.tt_action_id
        FROM ttb.tt_variant
          JOIN ttb.tt_out ON tt_variant.tt_variant_id = tt_out.tt_variant_id
          JOIN ttb.tt_action ON tt_out.tt_out_id = tt_action.tt_out_id
          JOIN ttb.tt_action_item on tt_action.tt_action_id = tt_action_item.tt_action_id
          JOIN ttb.tt_action_round ON tt_action.tt_action_id = tt_action_round.tt_action_id
        WHERE tt_variant.action_period @> now() :: TIMESTAMP
              AND NOT tt_variant.sign_deleted
              AND NOT tt_out.sign_deleted
              AND NOT tt_action.sign_deleted
              AND tt_variant.parent_tt_variant_id is not null
              AND tt_variant.tt_status_id != ttb.jf_tt_status_pkg$cn_tt_oper_close_status_id()
    )
    select oud.close_route_process_pkg$close(tt_variant_id, tt_action_id)
    from tt_oper
    where nn = 1
          and round_status_id != ttb.round_status_pkg$in_progress()
          and time_begin < now()::timestamp
          );

    return 1;
end;
$$
language 'plpgsql';


create or replace function oud.close_route_process_pkg$close(p_tt_variant_id int, p_tt_action_id int) returns int as
$$
declare
    l_return int;
begin

    update ttb.tt_variant
    set round_status_id = ttb.jf_tt_status_pkg$cn_tt_oper_close_status_id()
    where tt_variant_id = p_tt_variant_id;

    return 1;
end;
$$
language 'plpgsql';


create or replace function oud.close_route_process_pkg$close_incidents(p_tt_variant_id int, p_tt_action_id int) returns int as
$$
declare
    l_return int;
begin

--    select
--    from

    update ttb.tt_variant
    set round_status_id = ttb.jf_tt_status_pkg$cn_tt_oper_close_status_id()
    where tt_variant_id = p_tt_variant_id;

    return 1;
end;
$$
language 'plpgsql';