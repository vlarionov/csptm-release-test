create or replace function oud.jf_rounds_pkg$of_rows(
    p_account_id in  numeric,
    p_attr       in  text,
    p_rows       out refcursor
)
    returns refcursor
language plpgsql
as $$
declare
    l_f_round_code      text [] := jofl.jofl_pkg$extract_tarray(p_attr, 'f_round_code', true);
    l_f_round_status_id integer [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_round_status_id', true);
    l_tr_id             core.tr.tr_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
    l_route_variant_id  rts.route_variant.route_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'route_variant_id', true);
    l_tt_out_id         ttb.tt_out.tt_out_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_out_id', true);
begin
    open p_rows for
    select
        r.code,
        tag.tt_action_group_name,
        tat.action_type_name,
        rs.round_status_name                                                                                as status,
        tap.dt_action                                                                                       as plan_begin,
        tap.dt_action + tap.action_dur * '1 sec' :: interval                                                as plan_end,
        max(taf.time_fact_begin)                                                                            as fact_end,
        min(taf.time_fact_begin)                                                                            as fact_begin,
        case when min(taf.time_fact_begin) is not null
            then case when (extract(min from tap.dt_action) - extract(min from min(taf.time_fact_begin))) < 0
                then (extract(min from tap.dt_action) - extract(min from min(taf.time_fact_begin))) :: text
                 else '+' || (extract(min from tap.dt_action) - extract(min from min(taf.time_fact_begin))) :: text end
        end                                                                                                 as deflection
    from ttb.tt_out o
        join ttb.tt_action tap on o.tt_out_id = tap.tt_out_id
        join ttb.action_type tat on tap.action_type_id = tat.action_type_id
        left join ttb.tt_action_fact taf on tap.tt_action_id = taf.tt_action_id
        join ttb.tt_action_group tag on tap.tt_action_group_id = tag.tt_action_group_id
        left join ttb.tt_action_round tar on tap.tt_action_id = tar.tt_action_id
        left join rts.round r on tar.round_id = r.round_id
        left join ttb.tt_action_round ttar on tap.tt_action_id = ttar.tt_action_id
        left join ttb.round_status rs on ttar.round_status_id = rs.round_status_id
    where o.tt_out_id = l_tt_out_id and
          (cardinality(l_f_round_status_id) = 0 or l_f_round_status_id isnull or rs.round_status_id = any (l_f_round_status_id))
    group by r.code, tat.action_type_name, tag.tt_action_group_name, tap.dt_action, tap.action_dur, rs.round_status_name
    order by tap.dt_action;
end;
$$;