create or replace function oud.jf_tt_action_fact_pkg$of_rows(
  p_account_id in  numeric,
  p_attr       in  text,
  p_rows       out refcursor
)
  returns refcursor
language plpgsql
as $$
declare
  l_tt_out_id                ttb.tt_out.tt_out_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_out_id', false);
  l_dr_shift_id              ttb.dr_shift.dr_shift_id%type := jofl.jofl_pkg$extract_number(p_attr, 'dr_shift_id', false);
  l_driver_id                core.driver.driver_id%type := jofl.jofl_pkg$extract_number(p_attr, 'driver_id', false);

  l_f_is_checkpoint          boolean := coalesce(jofl.jofl_pkg$extract_boolean(p_attr, 'f_is_checkpoint_INPLACE_K', true), 1);
  l_f_round_order_num        integer := jofl.jofl_pkg$extract_number(p_attr, 'f_round_order_num_NUMBER', true);
  l_f_parent_action_type_key integer := jofl.jofl_pkg$extract_number(p_attr, 'f_parent_action_type_INPLACE_K', true);
  l_f_parent_action_type_id  ttb.action_type.action_type_id%type;
  l_f_round_code             rts.round.code%type := jofl.jofl_pkg$extract_varchar(p_attr, 'f_round_code_TEXT', true);

  l_production_round_type_list smallint[];
  l_technical_round_type_list smallint[];
  l_break_type_list smallint[];

  l_duration_format    text := 'HH:MI:SS';
begin

  if l_f_parent_action_type_key = 0
  then l_f_parent_action_type_id := ttb.action_type_pkg$production_round();
  elseif l_f_parent_action_type_key = 1
    then l_f_parent_action_type_id := ttb.action_type_pkg$technical_round();
  elseif l_f_parent_action_type_key = 2
    then l_f_parent_action_type_id := ttb.action_type_pkg$break();
  end if;


  select
    ttb.action_type_pkg$get_child_action_type_list(ttb.action_type_pkg$production_round())
    ,ttb.action_type_pkg$get_child_action_type_list(ttb.action_type_pkg$technical_round())
    ,ttb.action_type_pkg$get_child_action_type_list(ttb.action_type_pkg$break())
  into l_production_round_type_list
       ,l_technical_round_type_list
       ,l_break_type_list
  ;

open p_rows for

  with not_filtered as (
      select
        ttai.dr_shift_id,
        ttai.driver_id,
        tto.tt_out_id,
        tta.tt_action_id,
        oud.jf_tt_action_fact_pkg$get_round_sequential_num(tta.tt_action_id)                     round_order_num,
        at.action_type_name                                                                         action_type,
        parent_at.action_type_id                                                                    parent_action_type_id,
        parent_at.action_type_name                                                                  parent_action_type,
        rnd.code                                                                                    round_code,
        (
        select
          string_agg(
            ttb.jf_incident_reason_pkg$get_name_start_from_level((incident.incident_params ->> 'incidentReason')::int, 3), ';'
          )
        from ttb.incident
        where
          incident.time_start between
                (lower(tta.action_range)::date - interval '1 day')
                    and
                (lower(tta.action_range)::date + interval '1 day')
          and ttai.time_begin between incident.time_start and incident.time_finish
          and incident.incident_type_id = ttb.jf_incident_pkg$cn_manual()
          and (incident_params -> 'ttActionList' @> jsonb_build_array(tta.tt_action_id))
        )                                                                                       incident,
        rs.round_status_name,
        s.name                                                                                      stop_name,
        si2r.order_num                                                                              stop_order_num,
        ttai.time_begin                                                                             stop_plan_time,
        ttaf.time_fact_begin                                                                        stop_fact_time,
        (extract(epoch from (ttai.time_begin - ttaf.time_fact_begin)) / 60) :: integer              stop_deviation_int,
        si.stop_item_id in (select sit.stop_item_id
                            from rts.stop_item_type sit
                            where sit.stop_type_id = rts.stop_type_pkg$checkpoint())                is_checkpoint,
        row_number() over ( partition by tta.tt_action_id order by ttai.time_begin, ttai.time_end ) item_order_num,
        si.stop_item_id,
        case when
                parent_at.action_type_id = ttb.action_type_pkg$break()
            then
                (upper(tta.action_range) - lower(tta.action_range))::text
        end break_duration,
        '[' || json_build_object('CAPTION', 'Интервалы', 'JUMP',
                    json_build_object('METHOD', 'oud.round_interval','ATTR',
                        json_build_object('f_order_date_DT', to_char(tt_variant.order_date::timestamp,'yyyy-mm-dd hh24:mi:ss'), 'f_route_id', route_variant.route_id::text)::text
                             )) || ']' as jump_oud_round_interval,
        tt_variant.tt_variant_id as tt_variant_id,
        route_variant.route_variant_id as route_variant_id,
        route_variant.route_id as route_id,
        tt_action_fact_af.is_manual_fill
      from
        ttb.tt_out tto
        join ttb.tt_variant on tt_variant.tt_variant_id = tto.tt_variant_id
        join rts.route_variant on route_variant.route_variant_id = tt_variant.route_variant_id
        join ttb.tt_action tta on tto.tt_out_id = tta.tt_out_id
        join ttb.tt_action_item ttai on tta.tt_action_id = ttai.tt_action_id
        join ttb.tt_variant ttv on tto.tt_variant_id = ttv.tt_variant_id
        join ttb.dr_shift drs on ttai.dr_shift_id = drs.dr_shift_id
        join core.tr tr on tto.tr_id = tr.tr_id
        join core.tr_model trm on tr.tr_model_id = trm.tr_model_id
        join core.tr_capacity trc on trm.tr_capacity_id = trc.tr_capacity_id
        join core.driver dr on ttai.driver_id = dr.driver_id
        join ttb.action_type at on tta.action_type_id = at.action_type_id
        join ttb.action_type parent_at on (
        (tta.action_type_id = ANY(l_production_round_type_list) and
         parent_at.action_type_id = ttb.action_type_pkg$production_round()
         ) or
        (tta.action_type_id = ANY (l_technical_round_type_list) and
         parent_at.action_type_id = ttb.action_type_pkg$technical_round()
         ) or
        (tta.action_type_id = ANY (l_break_type_list) and
        parent_at.action_type_id = ttb.action_type_pkg$break())
        )
        join rts.stop_item2round si2r on ttai.stop_item2round_id = si2r.stop_item2round_id
        join rts.stop_item si on si2r.stop_item_id = si.stop_item_id
        join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
        join rts.stop s on sl.stop_id = s.stop_id
        left join ttb.tt_action_round ttar on tta.tt_action_id = ttar.tt_action_id
        left join ttb.tt_action_fact ttaf
          on ttai.tt_action_id = ttaf.tt_action_id and ttai.stop_item2round_id = ttaf.stop_item2round_id
        left join af.tt_action_fact_af
          on tt_action_fact_af.tt_action_fact_id = ttaf.tt_action_fact_id
        left join ttb.round_status rs on ttar.round_status_id = rs.round_status_id
        left join rts.round rnd on ttar.round_id = rnd.round_id
        left join rts.route_variant rv on rnd.route_variant_id = rv.route_variant_id
        left join rts.route r on rv.route_id = r.route_id
        left join core.tr_type trt on r.tr_type_id = trt.tr_type_id
      where tto.tt_out_id = l_tt_out_id
            and ttai.dr_shift_id = l_dr_shift_id
            and ttai.driver_id = l_driver_id
            and
            (
                (not tta.sign_deleted and not ttai.sign_deleted)
                or
                exists (select 1
                        from ttb.tt_action_item_excluded
                        where ttai.tt_action_item_id = tt_action_item_excluded.tt_action_item_id
                        )
            )
      order by lower(tta.action_range), ttai.time_begin, ttai.time_end
  )
  select
    *,
    to_char(sign(stop_deviation_int), 'SG') || abs(stop_deviation_int) stop_deviation,
    case parent_action_type_id when ttb.action_type_pkg$technical_round() then 'P{A},D{OF_TECHNICAL_ROUND_CHANGE}'
    end AS "ROW$POLICY"

  from not_filtered
  where
    (parent_action_type_id <>  ttb.action_type_pkg$break() or item_order_num = 1)
    and (l_f_is_checkpoint is null or not l_f_is_checkpoint or is_checkpoint)
    and (l_f_round_order_num is null or round_order_num = l_f_round_order_num)
    and (l_f_parent_action_type_id is null or parent_action_type_id = l_f_parent_action_type_id)
    and ((l_f_round_code = '') is not false or round_code = l_f_round_code);
end;
$$;

create or replace function oud.jf_tt_action_fact_pkg$get_round_sequential_num(
      p_tt_action_id ttb.tt_action.tt_action_id%type,
  out p_result       integer
)
stable
language plpgsql
as $$
begin
  with action_sequence as (
      select
        out_tta.tt_action_id,
        row_number() over (order by out_tta.dt_action ) sequential_num
      from ttb.tt_action tta
        join ttb.tt_action out_tta on tta.tt_out_id = out_tta.tt_out_id
        join ttb.tt_action_round ttar on out_tta.tt_action_id = ttar.tt_action_id
        join ttb.vm_action_type on out_tta.action_type_id = vm_action_type.action_type_id
      where tta.tt_action_id = p_tt_action_id
            and not out_tta.sign_deleted
            and vm_action_type.is_production_round
  )
  select sequential_num
  from action_sequence
  where tt_action_id = p_tt_action_id

  into p_result;
end;
$$;

create or replace function oud.jf_tt_action_fact_pkg$is_round_status_done(
      p_tt_action_id ttb.tt_action.tt_action_id%type,
  out p_result       text
)
stable
language plpgsql
as $$
begin
    with result_round as(
        select
        count(*) as all_round,
        count(*) filter
        (where ttaf.time_fact_begin between ttai.time_begin-'5 min'::interval and ttai.time_begin+'5 min'::interval) as done
    from ttb.tt_action tta
        join ttb.tt_action_item ttai on tta.tt_action_id = ttai.tt_action_id
        left join ttb.tt_action_fact ttaf on ttai.tt_action_id = ttaf.tt_action_id and ttai.stop_item2round_id = ttaf.stop_item2round_id
        where tta.tt_action_id=p_tt_action_id and ttb.action_type_pkg$is_round(tta.action_type_id)
            )
    select
        case when rr.all_round=rr.done then 'Выполнен' else 'Не выполнен' end
    from result_round rr
    into p_result;
end;
$$;

create or replace function oud.jf_tt_action_fact_pkg$get_row_policy(p_action_type_id ttb.action_type.action_type_id%type)
  returns text
language plpgsql
as $$
begin
  if ttb.action_type_pkg$is_round(p_action_type_id)
  then return 'P{D},D{oud.round_item_fact}';
  elseif ttb.action_type_pkg$is_break(p_action_type_id)
    then return 'P{D},D{oud.break_item_fact}';
  end if;

  return null;
end;
$$;

create or replace function oud.jf_tt_action_fact_pkg$of_add_tr_incident(
  p_account_id in  numeric,
  p_attr       in  text,
  p_rows       out refcursor
)
  returns refcursor
language plpgsql
as $$
declare
  l_tr_id              core.tr.tr_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', false);
  l_time_begin         timestamp;
  l_time_end         timestamp;
  l_result json;
  l_tt_action_id_array int[];
  l_incident_reason_id int := jofl.jofl_pkg$extract_number(p_attr, 'F_incident_reason_id', false);
  l_tt_variant_id int;
  l_dr_shift_id smallint;
  l_tt_out_id int := jofl.jofl_pkg$extract_number(p_attr, 'tt_out_id', false);
  l_tt_action_id_array_in    int [] := jofl.jofl_pkg$extract_tarray(p_attr, 'arr_tt_action_id_TEXT', true);
  l_p_stop_item2round_id int;
  l_driver_id int;
  l_tt_action_incident_downtime int;
begin

  if l_time_begin > l_time_end then
    raise exception 'Время начала д.б. меньше времени окончания';
  end if;

/*
Если выбрана одна строка, то
время начала события - время в колонке "Время посещения ОП план" выбранной строки,
Время окончания события - это время окончания смены по данному выходу.
              ,l_p_stop_item2round_id
*/
    if array_length(l_tt_action_id_array_in, 1) = 1 then
          select time_begin, stop_item2round_id, driver_id
          into l_time_begin, l_p_stop_item2round_id, l_driver_id
          from ttb.tt_action_item
          where tt_action_id = l_tt_action_id_array_in[1]
                and not tt_action_item.sign_deleted
          order by time_begin limit 1;


          select array_agg(distinct tt_action.tt_action_id)
                , min(tt_out.tt_variant_id)
                , min(time_begin)
                , max(time_end)
                , min(dr_shift_id)
          into l_tt_action_id_array
              ,l_tt_variant_id
              ,l_time_begin
              ,l_time_end
              ,l_dr_shift_id
          from ttb.tt_out
            join ttb.tt_action on tt_out.tt_out_id = tt_action.tt_out_id
            join ttb.tt_action_item on tt_action.tt_action_id = tt_action_item.tt_action_id
          where  l_tr_id is not null and tt_out.tr_id = l_tr_id
                 and tt_out.tt_out_id = l_tt_out_id
                 and not tt_out.sign_deleted
                 and not tt_action.sign_deleted
                 and not tt_action_item.sign_deleted
                 and ttb.tt_action_item.time_begin >= l_time_begin
                 and tt_action_item.dr_shift_id in (
                                    select dr_shift_id
                                    from ttb.tt_action
                                         join ttb.tt_action_item  on tt_action.tt_action_id = tt_action_item.tt_action_id
                                    where tt_action.tt_action_id = l_tt_action_id_array_in[1]
                                            and not tt_action.sign_deleted
                                            and not tt_action_item.sign_deleted
                 );
    else
/*
При добавлении события по ТС:
если выделено несколько строк, то
время начала события - самое раннее время в колонке "Время посещения ОП план" выделенных строк,
Время окончания события - самое позднее время в колонке "Время посещения ОП план" выделенных строк.
*/

          select stop_item2round_id, driver_id
          into l_p_stop_item2round_id, l_driver_id
          from ttb.tt_action_item
          where tt_action_id = l_tt_action_id_array_in[1]
                and not tt_action_item.sign_deleted
          order by time_begin limit 1;


          select array_agg(distinct tt_action.tt_action_id)
                    , min(tt_out.tt_variant_id)
                    , min(time_begin)
                    , max(time_end)
                    , min(dr_shift_id)
          into l_tt_action_id_array
                , l_tt_variant_id
                , l_time_begin
                , l_time_end
                , l_dr_shift_id
          from ttb.tt_out
            join ttb.tt_action on tt_out.tt_out_id = tt_action.tt_out_id
            join ttb.tt_action_item on tt_action.tt_action_id = tt_action_item.tt_action_id
          where  l_tr_id is not null and tt_out.tr_id = l_tr_id
                 and tt_out.tt_out_id = l_tt_out_id
                 and not tt_out.sign_deleted
                 and not tt_action.sign_deleted
                 and not tt_action_item.sign_deleted
                 and tt_action.tt_action_id = any(l_tt_action_id_array_in);
    end if;


  if  array_length(l_tt_action_id_array, 1) > 0 then

    update ttb.tt_action
    set sign_deleted = true
    where tt_action_id = any (l_tt_action_id_array);

    select oud.jf_tt_action_fact_pkg$of_add_incident_downtime(
        p_tt_out_id := l_tt_out_id
        ,p_driver_id := l_driver_id
        ,p_time_start := l_time_begin
        ,p_time_finish := l_time_end
        ,p_stop_item2round_id := l_p_stop_item2round_id
        ,p_dr_shift_id := l_dr_shift_id
    ) into l_tt_action_incident_downtime;

    --пометим преобразование, если случится восстонавливать
    update ttb.tt_action
    set parent_tt_action_id = l_tt_action_incident_downtime
    where tt_action_id = any (l_tt_action_id_array);


    perform ttb.tt_oper_pkg$update_dt_action(l_tt_variant_id);

    select
      ttb.jf_incident_pkg$send_incident(array_agg(
                                            ttb.jf_incident_pkg$new_incident(
                                                p_timeStart := l_time_begin
                                                , p_incident_initiator_id := ttb.jf_incident_pkg$cn_initiator_system()
                                                , p_incident_status_id := ttb.jf_incident_pkg$cn_status_new()
                                                , p_incident_type_id := ttb.jf_incident_pkg$cn_manual()
                                                , p_parent_incident_id := null
                                                , p_account_id := NULL
                                                , p_time_finish := l_time_end
                                                , p_incident_payload :=
                                                ttb.jf_incident_pkg$new_payload(
                                                    p_trList := ARRAY [l_tr_id]::int[]
                                                    ,p_ttActionList := ARRAY [l_tt_action_incident_downtime]::int[]
                                                    ,p_incidentReason := l_incident_reason_id
                                                    ,p_ttVariantList := ARRAY[l_tt_variant_id]
                                                )
                                            )
                                        )
      )
    into l_result;

/*
    --Зафикируем "удаленные" tt_action_item
    insert into ttb.tt_action_item_excluded (tt_action_item_id)
    select tt_action_item_id
    from ttb.tt_action_item
    where tt_action_id = any (l_tt_action_id_array)
    on conflict (tt_action_item_id) do nothing;
  */


  end if;

end;
$$;


create or replace function oud.jf_tt_action_fact_pkg$of_change_tr(
  p_account_id in  numeric,
  p_attr       in  text,
  p_rows       out refcursor
)
  returns refcursor
language plpgsql
as $$
declare
  --   l_f_round_order_num        integer := jofl.jofl_pkg$extract_number(p_attr, 'f_round_order_num_NUMBER', true);
  --   l_f_parent_action_type_key integer := jofl.jofl_pkg$extract_number(p_attr, 'f_parent_action_type_INPLACE_K', true);
  -- выход
  l_tt_out_id BIGINT = jofl.jofl_pkg$extract_number(p_attr, 'tt_out_id', true);
  l_time_from TIMESTAMP = jofl.jofl_pkg$extract_date(p_attr, 'time_from', true);
  l_incident_reason_id INTEGER = jofl.jofl_pkg$extract_number(p_attr, 'incident_reason_id', true);
  l_garage_num_new BIGINT = jofl.jofl_pkg$extract_number(p_attr, 'new_TS', true);
  -- id заменяемого ТС
  l_tr_id BIGINT = jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
  new_tr_id BIGINT;
  -- id нового выхода
  new_tt_out_id INTEGER = nextval('ttb.tt_out_tt_out_id_seq'::regclass);
  new_act_id INTEGER;
  deleted_action_ids INTEGER[];
  deleted_act_id INTEGER;
  next_action_time TIMESTAMP;
  new_action_range TSRANGE;
  p_ttvariantlist integer[];
begin
  p_ttvariantlist := array(select tt_variant_id from ttb.tt_out where tt_out_id = l_tt_out_id);
  select tr_id INTO new_tr_id from core.tr WHERE garage_num = l_garage_num_new AND depo_id =
         (SELECT depo_id from core.tr where tr_id = l_tr_id);
  -- новый выход
  insert into ttb.tt_out(tt_out_id, tt_variant_id, tr_capacity_id, mode_id, tt_out_num, sign_deleted, sys_period, parent_tt_out_id, tr_id, action_range)
    select new_tt_out_id as tt_out_id, tt_variant_id, tr_capacity_id, mode_id, tt_out_num, sign_deleted,
      sys_period, parent_tt_out_id, new_tr_id as tr_id, action_range
    from ttb.tt_out
    WHERE tt_out_id = l_tt_out_id;

  -- время, когда начнется следующий action
  SELECT dt_action INTO next_action_time from ttb.tt_action
  WHERE dt_action > l_time_from and tt_out_id = l_tt_out_id ORDER BY dt_action LIMIT 1;

  new_action_range = TSRANGE((select lower(action_range) from ttb.tt_out where tt_out_id = l_tt_out_id), next_action_time);
  UPDATE ttb.tt_out SET action_range = new_action_range WHERE tt_out_id = l_tt_out_id;
  UPDATE ttb.tt_out SET action_range = TSRANGE(next_action_time, null) WHERE tt_out_id = new_tt_out_id;
  -- будущие actiom помечаются удаленными
  UPDATE ttb.tt_action SET sign_deleted = TRUE WHERE dt_action > l_time_from and tt_out_id = l_tt_out_id;
  deleted_action_ids = array(select tt_action_id from ttb.tt_action where sign_deleted = TRUE AND tt_out_id = l_tt_out_id);

  FOREACH deleted_act_id IN ARRAY deleted_action_ids
  LOOP
    new_act_id := nextval('ttb.tt_action_tt_action_id_seq'::regclass);
    INSERT INTO ttb.tt_action(tt_action_id, tt_out_id, action_dur, dt_action, sys_period, action_type_id, sign_deleted,
                              tt_action_group_id, action_gr_id, parent_tt_action_id, driver_id, action_range)
      SELECT new_act_id as tt_action_id, new_tt_out_id as tt_out_id, action_dur, dt_action, sys_period,
        action_type_id, sign_deleted, tt_action_group_id, action_gr_id, parent_tt_action_id, driver_id, action_range
      FROM ttb.tt_action WHERE tt_action_id = deleted_act_id;

    INSERT INTO ttb.tt_action_item(tt_action_id, time_begin, time_end, stop_item2round_id, action_type_id, dr_shift_id, driver_id)
      SELECT new_act_id as tt_action_id, time_begin, time_end, stop_item2round_id, action_type_id, dr_shift_id, driver_id
      FROM ttb.tt_action_item
      WHERE tt_action_id = deleted_act_id;

    UPDATE ttb.tt_action_item SET sign_deleted = true WHERE tt_action_id = deleted_act_id;

    INSERT INTO ttb.tt_action_round(tt_action_id, round_id, round_status_id)
    -- 2 - запланирован
      SELECT new_act_id as tt_action_id, round_id, 2 as round_status_id FROM ttb.tt_action_round
      where tt_action_id = deleted_act_id;
    -- 5 - отменен
    UPDATE ttb.tt_action_round SET round_status_id = 5 WHERE tt_action_id = deleted_act_id;
  END LOOP;
  -- генерация инцидента
  PERFORM ttb.tt_oper_pkg$send_incident(p_incidentReason := l_incident_reason_id, p_ttVariantList := p_ttvariantlist);
end;
$$;



-------------------
CREATE OR REPLACE FUNCTION oud.jf_tt_action_fact_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
  RETURNS text AS
$body$
declare
  l_r ttb.tt_out%rowtype;
  l_tt_action_id ttb.tt_action.tt_action_id%type;
  l_time_begin timestamp;
  l_tt_out_id int;
begin
  l_r := ttb.jf_tt_out_pkg$attr_to_rowtype(p_attr);
  l_tt_action_id	:= jofl.jofl_pkg$extract_number(p_attr, 'tt_action_id', true);
  l_time_begin		:= jofl.jofl_pkg$extract_date(p_attr, 'stop_plan_time', true);
  --l_r.tt_variant_id	:= jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);


    select tt_out.tt_variant_id, tt_out.tt_out_id
    into strict l_r.tt_variant_id, l_tt_out_id
    from ttb.tt_action
      join ttb.tt_out on tt_action.tt_out_id = tt_out.tt_out_id
    where tt_action_id = l_tt_action_id;
/*
  if l_time_begin < now() then
    raise exception 'Изменять можно только будущие рейсы ...';
  end if;
*/

    perform oud.jf_tt_action_fact_pkg$change_tt_action_time_begin(l_tt_action_id, l_time_begin);

    return null;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
COST 100;

create or replace function oud.jf_tt_action_fact_pkg$of_add_route_incident(
  p_account_id in  numeric,
  p_attr       in  text,
  p_rows       out refcursor
)
  returns refcursor
language plpgsql
as $$
declare
  l_time_begin         timestamp := date_trunc('minute', jofl.jofl_pkg$extract_date(p_attr, 'BEGIN_DT', false));
  l_time_end           timestamp := date_trunc('minute', jofl.jofl_pkg$extract_date(p_attr, 'END_DT', false));
  l_tt_out_id          int := jofl.jofl_pkg$extract_number(p_attr, 'tt_out_id', false);
  l_number numeric;
  l_result json;
  l_tt_action_id_array int[];
  l_tt_variant_id int;
  l_tr_id_array bigint[];
  l_incident_reason_id int := jofl.jofl_pkg$extract_number(p_attr, 'F_incident_reason_id', false);
  l_tr_id              core.tr.tr_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', false);
  l_stop_item_id int := jofl.jofl_pkg$extract_number(p_attr, 'stop_item_id', false);

begin

    if l_time_begin > l_time_end then
        raise exception 'Время начала д.б. меньше времени окончания';
    end if;




    --найдем оперативное расписание
    select tt_variant.tt_variant_id, upper(tt_variant.action_period)
    into strict l_tt_variant_id, l_time_end
    from ttb.tt_out
        join ttb.tt_variant on tt_out.tt_variant_id = tt_variant.tt_variant_id
    where tt_out.tt_out_id = l_tt_out_id
          and not tt_out.sign_deleted;

    select array_agg(tt_action.tt_action_id), array_agg(tt_out.tr_id)
    into l_tt_action_id_array, l_tr_id_array
    from ttb.tt_out
    join ttb.tt_action on tt_out.tt_out_id = tt_action.tt_out_id
    where  l_tt_variant_id is not null and tt_out.tt_variant_id = l_tt_variant_id
            and tt_action.action_range && tsrange (l_time_begin, l_time_end)
            and tt_out.tr_id is not null
            and not tt_out.sign_deleted
            and not tt_action.sign_deleted;

    if  array_length(l_tt_action_id_array, 1) > 0 then
        select
            ttb.jf_incident_pkg$send_incident(array_agg(
                                                  ttb.jf_incident_pkg$new_incident(
                                                      p_timeStart := l_time_begin
                                                      , p_incident_initiator_id := ttb.jf_incident_pkg$cn_initiator_system()
                                                      , p_incident_status_id := ttb.jf_incident_pkg$cn_status_new()
                                                      , p_incident_type_id := ttb.jf_incident_pkg$cn_manual()
                                                      , p_parent_incident_id := null
                                                      , p_account_id := NULL
                                                      , p_time_finish := l_time_end
                                                      , p_incident_payload :=
                                                      ttb.jf_incident_pkg$new_payload(
                                                          p_ttActionList := l_tt_action_id_array
                                                          ,p_incidentReason := l_incident_reason_id
                                                          ,p_trList := l_tr_id_array::int[]
                                                          ,p_ttVariantList := ARRAY[l_tt_variant_id]
                                                          ,p_stopItemList := ARRAY[l_stop_item_id]
                                                      )
                                                  )
                                              )
            )
        into l_result;
    end if;

end;
$$;


create or replace function oud.jf_tt_action_fact_pkg$of_technical_round_change(
  p_account_id in  numeric,
  p_attr       in  text,
  p_rows       out refcursor
)
  returns refcursor
language plpgsql
as $$
declare
  l_round_id int := jofl.jofl_pkg$extract_number(p_attr, 'F_round_id', false);
  l_time_begin         timestamp := jofl.jofl_pkg$extract_date(p_attr, 'BEGIN_DT', false);
  l_tt_variant_id int := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', false);
  l_tt_action_id int := jofl.jofl_pkg$extract_number(p_attr, 'tt_action_id', false);
begin
  perform oud.jf_tt_action_fact_pkg$of_round_change(
      p_tt_action_id := l_tt_action_id
      ,p_round_id := l_round_id
      ,p_tt_variant_id := l_tt_variant_id
      ,p_time_begin := l_time_begin
      ,p_action_type := ttb.action_type_pkg$technical_round()
  );

end;
$$;

create or replace function oud.jf_tt_action_fact_pkg$of_delete_tt_action_id(
  p_tt_action_id_array in  int[]
)
  returns void
language plpgsql
as $$
declare
begin

    update ttb.tt_action_item
    set sign_deleted = true
    where tt_action_item.tt_action_id = any (p_tt_action_id_array);

    update ttb.tt_action
    set sign_deleted = true
    where tt_action.tt_action_id  = any (p_tt_action_id_array);

end;
$$;



create or replace function oud.jf_tt_action_fact_pkg$of_rebuild_tt_variant(
  p_account_id in numeric,
  p_attr       in text
)
  returns text
language plpgsql
as $$
declare
  l_tt_out_id     ttb.tt_out.tt_out_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_out_id', false);

  l_tt_variant_id ttb.tt_variant.tt_variant_id%type;
begin
  select tto.tt_variant_id
  from ttb.tt_out tto
  where tto.tt_out_id = l_tt_out_id
  into l_tt_variant_id;

  perform oud.tt_oper_rebuilder_pkg$send_task(l_tt_variant_id);

  return null ;
end;
$$;

create or replace function oud.jf_tt_action_fact_pkg$of_add_tt_out(
  p_account_id in numeric,
  p_attr       in text
)
  returns text
language plpgsql
as $$
declare
  l_tt_out  ttb.tt_out%rowtype;
  l_tt_variant  ttb.tt_variant%rowtype;
  l_time_begin         timestamp := jofl.jofl_pkg$extract_date(p_attr, 'BEGIN_DT', false);
  l_stop_item_id rts.stop_item.stop_item_id%type := jofl.jofl_pkg$extract_number(p_attr, 'F_stop_item_id', false);
  l_group_id int:= jofl.jofl_pkg$extract_number(p_attr, 'f_group_id', false);
  l_driver_id int;
begin

  l_tt_out.tt_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', false);
  l_tt_out.tt_out_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_out_id', false);

  select driver.driver_id, tr.tr_id
  into strict l_driver_id, l_tt_out.tr_id
  from easu.groups
    join core.driver on groups.person_number :: text = driver.tab_num
    join core.tr on tr.garage_num =   groups.garage_number::bigint
  where groups.group_id = l_group_id limit 1;

  begin
    select *
    into strict l_tt_variant
    from ttb.tt_variant
    where tt_variant.tt_variant_id =  l_tt_out.tt_variant_id
          and tt_variant.action_period @> l_time_begin
          and not tt_variant.sign_deleted;
    exception
    when no_data_found THEN
      raise exception 'Время начала выхода д.б. внутри периода действия расписания!';
  end;

  --Вариант номер 1
  --Восстонавливаем ТС на маршрут с изменением времени начала рейса
  --Если пользователь выбирает выбывшие ТС, то нужно "вернуть" ТС на тот же выход, с которого он выбыл,
  --Время начала рейса и конечный пункт, с которого начнется рейса должен ввести пользователь.
  declare
    l_tt_action_restore  ttb.tt_action%rowtype;
  begin

    with a as (
        select
          tt_action.tt_action_id
          ,row_number() over (order by abs (extract(epoch from l_time_begin - tt_action_item.time_begin))) nn
        from ttb.tt_action_round
          join ttb.tt_action on tt_action_round.tt_action_id = tt_action.tt_action_id
          join ttb.tt_out on tt_action.tt_out_id = tt_out.tt_out_id
          join ttb.tt_variant on tt_out.tt_variant_id = tt_variant.tt_variant_id
          join rts.stop_item2round on stop_item2round.round_id = tt_action_round.round_id
          join ttb.tt_action_item on tt_action_item.tt_action_id = tt_action.tt_action_id
                                     and tt_action_item.stop_item2round_id = stop_item2round.stop_item2round_id
        where tt_variant.tt_variant_id = l_tt_variant.tt_variant_id
              and stop_item2round.order_num = 1
              and stop_item2round.stop_item_id = l_stop_item_id
              and tt_action_item.time_begin between now() and l_time_begin
              and tt_out.tr_id = l_tt_out.tr_id
    )
    select tt_action.*
    into strict l_tt_action_restore
    from ttb.tt_action
      join a on tt_action.tt_action_id = a.tt_action_id
    where nn = 1;


    raise log 'oud.jf_tt_action_fact_pkg$of_add_tt_out found suitable tt_action (%) ', l_tt_action_restore.tt_action_id;
      --восстановим удаленные рейсы
      with tt_action_updated as (
        update ttb.tt_action
        set sign_deleted = false
        where sign_deleted = true
              and tt_action.tt_out_id = l_tt_action_restore.tt_out_id
        returning tt_action_id
      )
        ,tt_action_item_updated as (
        update ttb.tt_action_item
        set sign_deleted = false
        from tt_action_updated
        where tt_action_item.sign_deleted = true
              and tt_action_item.tt_action_id =  tt_action_updated.tt_action_id
              and time_begin >= l_time_begin
        returning tt_action_item_id
      )
      --Зафикируем "удаленные" tt_action_item
      delete
      from ttb.tt_action_item_excluded
      where tt_action_item_excluded.tt_action_item_id in (select tt_action_item_id from tt_action_item_updated);

    raise log 'oud.jf_tt_action_fact_pkg$of_add_tt_out ttb.timetable_edit_pkg$update_out tt_action_id(%) l_time_begin(%)', l_tt_action_restore.tt_action_id, l_time_begin;

    perform oud.jf_tt_action_fact_pkg$change_tt_action_time_begin(l_tt_action_restore.tt_action_id, l_time_begin);
    --закончили
    --perform oud.tt_oper_rebuilder_pkg$send_task(l_tt_out.tt_variant_id);
    return 'Восстановлены отмененные рейсы. Идет выравнивание интервалов.';
    exception
    when no_data_found then
    --Данный метод не подходит
  end;




  --Часть 2 - можно реализовать позже.
  --Если выбывших ТС нет, то нужно предложить ввести номер выхода вручную (2 знака)
  --и Время начала рейса и конечный пункт (предложить пользователю конечный пункты маршрута), с которого начнется рейс.
  --Для добавленного ТС необходимо скопировать рейсы из выхода, у которого время начала ближайшее ко времени начала рейса
  --введенного пользователем и конечный пункт начала рейса совпадает с введенным пользователем.

  declare
    l_tt_out_id_new   ttb.tt_out.tt_out_id%type;
    l_tt_out_num_new smallint;
    l_tt_out_id4copy int;
    l_tt_action_id_first ttb.tt_action.tt_action_id%type;
    l_tt_action_id4copy ttb.tt_action.tt_action_id%type;
    l_dt_action_start timestamp;
  begin

    begin
      l_tt_out_num_new := jofl.jofl_pkg$extract_number(p_attr, 'F_NUMBER', false)::SMALLINT;
      exception
      when others then
        raise exception 'Не указан номер выхода!';
    end;

    begin
      l_tt_out_id4copy := jofl.jofl_pkg$extract_number(p_attr, 'f_tt_out_numtt_out_id', false);
      exception
      when others then
        raise exception 'Не указан номер выхода для копирования!';
    end;

    begin

        with a as (
            select tt_action.tt_action_id, tt_action.dt_action
            from ttb.tt_action_round
              join ttb.tt_action on tt_action_round.tt_action_id = tt_action.tt_action_id
              join ttb.tt_out on tt_action.tt_out_id = tt_out.tt_out_id
              join ttb.tt_variant on tt_out.tt_variant_id = tt_variant.tt_variant_id
              join rts.stop_item2round on stop_item2round.round_id = tt_action_round.round_id
              join ttb.tt_action_item on tt_action_item.tt_action_id = tt_action.tt_action_id
                                         and tt_action_item.stop_item2round_id = stop_item2round.stop_item2round_id
              join ttb.vm_action_type on vm_action_type.action_type_id = tt_action.action_type_id
            where tt_variant.tt_variant_id = l_tt_variant.tt_variant_id
                  and not tt_action.sign_deleted
                  and not tt_action_item.sign_deleted
                  and not tt_out.sign_deleted
                  and stop_item2round.order_num = 1
                  and stop_item2round.stop_item_id = l_stop_item_id
                  and tt_out.tt_out_id = l_tt_out_id4copy
                  and vm_action_type.is_production_round
        )
        select tt_action_id, dt_action
        into strict l_tt_action_id4copy, l_dt_action_start
        from a
        order by dt_action limit 1;

      exception
      when no_data_found then
        raise exception 'Не найден подходящий рейс для копирования!';
    end;

    begin
         select
              oud.jf_tt_action_fact_pkg$copy_tt_out(
              p_tt_action_id_source := l_tt_action_id4copy
              ,p_tt_out_num_new := l_tt_out_num_new
              ,p_time_start_from_source := l_dt_action_start
          )
        into strict l_tt_out_id_new;
    end;


    update ttb.tt_out
    set tr_id = l_tt_out.tr_id
    where tt_out_id = l_tt_out_id_new;

    with a as (
        select distinct
            groups_shfis.shift_number as shift_number,
            first_value(groups_shfis.person_number)
            over (partition by groups_shfis.shift_number
                order by groups.create_time desc) as person_number
        from easu.groups
            join easu.groups as groups_shfis on groups.garage_number = groups_shfis.garage_number
        where groups.group_id = l_group_id
              and groups.out_date = l_tt_variant.order_date
              and groups_shfis.out_date = l_tt_variant.order_date
    )
    ,b as (SELECT
               tt_action_item.tt_action_item_id,
               driver.driver_id,
               tt_action_item.tt_action_id
           FROM a
               JOIN core.driver ON a.person_number :: TEXT = driver.tab_num
               JOIN ttb.dr_shift ON a.shift_number = dr_shift.dr_shift_num
               JOIN ttb.tt_action_item ON tt_action_item.dr_shift_id = dr_shift.dr_shift_id
               JOIN ttb.tt_action ON tt_action_item.tt_action_id = tt_action.tt_action_id
           WHERE tt_action.tt_out_id = l_tt_out_id_new
    )
    ,c as (UPDATE ttb.tt_action
    SET driver_id = b.driver_id
    FROM b
    WHERE tt_action.tt_action_id = b.tt_action_id
    )
    UPDATE ttb.tt_action_item
    SET driver_id = b.driver_id
    FROM b
    WHERE tt_action_item.tt_action_item_id = b.tt_action_item_id;



    select tt_action_id
    into l_tt_action_id_first
    from ttb.tt_action
    where tt_action.tt_out_id = l_tt_out_id_new
    order by dt_action limit 1;

    if l_tt_out_id_new is null then
        raise exception 'Не обнаружено подходящего алгоритма для реализации данной операции';
    else
        perform oud.jf_tt_action_fact_pkg$change_tt_action_time_begin(l_tt_action_id_first, l_time_begin);
        --perform oud.tt_oper_rebuilder_pkg$send_task(l_tt_out.tt_variant_id);
        return 'Добавлен новый выход. Идет выравнивание интервалов.'|| l_tt_out_id_new;
    end if;

    exception
    when no_data_found then
    --Данный метод не подходит
    --return null;
  end;




  raise exception 'Не обнаружено подходящего алгоритма для реализации данной операции';
end;
$$;

--DROP FUNCTION oud."jf_tt_action_fact_pkg$copy_tt_out"(integer,smallint,timestamp without time zone);

CREATE OR REPLACE FUNCTION oud.jf_tt_action_fact_pkg$copy_tt_out(
  p_tt_action_id_source ttb.tt_action.tt_action_id%type
  ,p_tt_out_num_new  ttb.tt_out.tt_out_num%type
  ,p_time_start_from_source timestamp
)
  RETURNS INTEGER
LANGUAGE plpgsql
AS $$
DECLARE
  l_tt_variant_id ttb.tt_variant.tt_variant_id%type;
  l_tt_out_id ttb.tt_out.tt_out_id%type;
BEGIN

  with inserted_tt_out as
  (INSERT INTO ttb.tt_out
    (tt_variant_id
      , tr_capacity_id
      , mode_id
      , tt_out_num
      , sign_deleted
      , parent_tt_out_id)
      SELECT
        tt_out.tt_variant_id
        ,tt_out.tr_capacity_id
        ,tt_out.mode_id
        ,p_tt_out_num_new
        ,tt_out.sign_deleted
        ,tt_out.tt_out_id
      FROM ttb.tt_out
            join ttb.tt_action on tt_action.tt_out_id =  tt_out.tt_out_id
      WHERE tt_action.tt_action_id = p_tt_action_id_source
            and tt_action.dt_action >= p_time_start_from_source
    RETURNING tt_out_id, parent_tt_out_id, tt_variant_id
  ),
      inserted_tt_action as (
      insert into ttb.tt_action
      (tt_out_id
        ,action_dur
        ,dt_action
        ,action_type_id
        ,sign_deleted
        ,tt_action_group_id
        ,action_gr_id
        ,parent_tt_action_id)
        select
          inserted_tt_out.tt_out_id
          ,action_dur
          ,dt_action
          ,action_type_id
          ,sign_deleted
          ,tt_action_group_id
          ,action_gr_id
          ,tt_action_id
        from ttb.tt_action
          join inserted_tt_out on tt_action.tt_out_id = inserted_tt_out.parent_tt_out_id
        where not sign_deleted
              and dt_action >= p_time_start_from_source
      RETURNING parent_tt_action_id, tt_action_id
    ),
      inserted_tt_action_item as (
      insert into ttb.tt_action_item
      (tt_action_id
        ,time_begin
        ,time_end
        ,stop_item2round_id
        ,action_type_id
        ,sign_deleted
        ,dr_shift_id)
        select inserted_tt_action.tt_action_id
          ,time_begin
          ,time_end
          ,stop_item2round_id
          ,action_type_id
          ,sign_deleted
          ,dr_shift_id
        from inserted_tt_action
          join ttb.tt_action_item on inserted_tt_action.parent_tt_action_id = tt_action_item.tt_action_id
        where not sign_deleted
    ),
      inserted_tt_action_round as (
      insert into ttb.tt_action_round
      (tt_action_id
        ,round_id
        ,round_status_id
      )
        select inserted_tt_action.tt_action_id
          ,round_id
          ,ttb.round_status_pkg$planned()
        from inserted_tt_action join ttb.tt_action_round on inserted_tt_action.parent_tt_action_id =  tt_action_round.tt_action_id
    )
  select tt_variant_id, tt_out_id
  into l_tt_variant_id, l_tt_out_id
  from inserted_tt_out;

  --Сделаем парент плановым
  update ttb.tt_out
  set parent_tt_out_id = (select plan.parent_tt_out_id from ttb.tt_out plan where plan.tt_out_id = tt_out.parent_tt_out_id)
  where  tt_out.tt_out_id = l_tt_out_id;


  update ttb.tt_action
  set parent_tt_action_id = (select plan.parent_tt_action_id from ttb.tt_action plan where plan.tt_action_id = tt_action.parent_tt_action_id)
  where  tt_action.tt_out_id = l_tt_out_id;


  perform ttb.tt_oper_pkg$update_dt_action(l_tt_variant_id);

  return l_tt_out_id;
END;
$$;



CREATE OR REPLACE FUNCTION oud.jf_tt_action_fact_pkg$of_change_tt_action_type(
  p_id_account numeric,
  p_attr text
)
  RETURNS text
  LANGUAGE plpgsql
  AS
  $$
  declare
    l_time_begin timestamp;
    l_tt_action_id_array_in    int [];
    l_tr_id_array    int [];
    l_incident_reason_id int := jofl.jofl_pkg$extract_number(p_attr, 'F_incident_reason_id', true);
    l_round_id int := jofl.jofl_pkg$extract_number(p_attr, 'F_round_id', false);
    l_tt_variant_id int := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', false);
    l_tt_action_id_array_new    int [];
    l_result json;
  begin

   SELECT array_agg(distinct a)
   into l_tt_action_id_array_in
   from unnest(jofl.jofl_pkg$extract_tarray(p_attr, 'arr_tt_action_id_TEXT', true)) a;

   select
          array_agg(
            oud.jf_tt_action_fact_pkg$of_round_change(
                  p_tt_action_id := tt_action_id
                  ,p_round_id := l_round_id
                  ,p_tt_variant_id := l_tt_variant_id
                  ,p_time_begin := dt_action
                  ,p_action_type := ttb.action_type_pkg$production_round()
                    )
                )
   into l_tt_action_id_array_new
   from ttb.tt_action
   where tt_action_id = ANY(l_tt_action_id_array_in)
         and not tt_action.sign_deleted;


  if  array_length(l_tt_action_id_array_new, 1) > 0 then

    select array_agg(tr_id)
    into l_tr_id_array
    from ttb.tt_out join
         ttb.tt_action on tt_action.tt_out_id = tt_out.tt_out_id
    where tt_action.tt_action_id = ANY(l_tt_action_id_array_new)
    and not tt_out.sign_deleted
    and not tt_action.sign_deleted;
    
      if l_incident_reason_id is not null then
          select
            ttb.jf_incident_pkg$send_incident(array_agg(
                                                  ttb.jf_incident_pkg$new_incident(
                                                      p_timeStart := now()::timestamp
                                                      , p_incident_initiator_id := ttb.jf_incident_pkg$cn_initiator_system()
                                                      , p_incident_status_id := ttb.jf_incident_pkg$cn_status_new()
                                                      , p_incident_type_id := ttb.jf_incident_pkg$cn_manual()
                                                      , p_parent_incident_id := null
                                                      , p_account_id := NULL
                                                      , p_time_finish := null
                                                      , p_incident_payload :=
                                                      ttb.jf_incident_pkg$new_payload(
                                                          p_trList := l_tr_id_array
                                                          ,p_ttActionList := l_tt_action_id_array_new
                                                          ,p_incidentReason := l_incident_reason_id
                                                          ,p_ttVariantList := ARRAY[l_tt_variant_id]
                                                      )
                                                  )
                                              )
            )
            into l_result;
      end if;
    end if;
    return null;
  END;
$$;


create or replace function oud.jf_tt_action_fact_pkg$of_round_change(
   p_tt_action_id int
  ,p_round_id int
  ,p_tt_variant_id int
  ,p_time_begin timestamp
  ,p_action_type smallint
)
  RETURNS int
language plpgsql
as $$
declare
  l_tt_action_id_4_delete_array int[];
  l_tr_capacity_id int;
  l_tt_at_round_id int;
  l_tt_action ttb.tt_action%rowtype;
  l_tt_action_item ttb.tt_action_item%rowtype;
  l_norm_round ttb.norm_round%rowtype;
  l_time_tmp timestamp;
  l_stop_item2round_id int;
begin
  select *
  into strict l_tt_action
  from ttb.tt_action
  where tt_action_id = p_tt_action_id;

  select *
  into l_tt_action_item
  from ttb.tt_action_item
  where tt_action_id = p_tt_action_id limit 1;

  select array_agg(tt_action_id)
  into l_tt_action_id_4_delete_array
  from ttb.tt_action
  where tt_action_id = p_tt_action_id
        and not tt_action.sign_deleted;


  select tt_at_round_id, tt_action_type.action_type_id
  into l_tt_at_round_id, l_tt_action_item.action_type_id
  from ttb.tt_action_type
    join ttb.tt_at_round on tt_action_type.tt_action_type_id = tt_at_round.tt_action_type_id
    join ttb.vm_action_type on vm_action_type.action_type_id = tt_action_type.action_type_id
  where round_id = p_round_id
        and
        (
          (
            p_action_type = ttb.action_type_pkg$technical_round() and vm_action_type.is_technical_round
          )
          or
          (
            p_action_type = ttb.action_type_pkg$production_round() and vm_action_type.is_production_round
          )
        )
        and  tt_variant_id in (
    select parent_tt_variant_id
    from ttb.tt_variant
    where tt_variant_id = p_tt_variant_id
  ) limit 1;


  select tr_capacity_id
  into l_tr_capacity_id
  from ttb.tt_out
  where tt_out_id = l_tt_action.tt_out_id;


  with plan_tt_action_item as
  (select
     ftime_begin
     ,fstop_item2round_id
     ,row_number() over (order by ftime_begin asc) nn_asc
     ,row_number() over (order by ftime_begin desc) nn_desc
   from ttb.timetable_pkg$get_round_tm(
       p_tt_at_round_id := l_tt_at_round_id,
       p_tr_capacity_id := l_tr_capacity_id,
       p_tm_start := p_time_begin
   )                                                                                 )
    ,inserted_tt_action as
  (
    insert into ttb.tt_action
    (tt_out_id
      ,action_dur
      ,dt_action
      ,action_type_id
      ,sign_deleted
      ,tt_action_group_id
      ,action_gr_id
      ,parent_tt_action_id)
      select
         l_tt_action.tt_out_id as tt_out_id
        ,0 as action_dur
        ,ftime_begin as dt_action
        ,l_tt_action_item.action_type_id as action_type_id
        ,false
        ,l_tt_action.tt_action_group_id
        ,l_tt_action.action_gr_id
        ,null
      from plan_tt_action_item
      where nn_asc = 1
    RETURNING tt_action_id
  )
    ,inserted_tt_action_item as
  (
    insert into ttb.tt_action_item
    (tt_action_id
      ,time_begin
      ,time_end
      ,stop_item2round_id
      ,action_type_id
      ,sign_deleted
      ,driver_id
      ,dr_shift_id)
      select inserted_tt_action.tt_action_id
        ,plan_tt_action_item.ftime_begin
        ,plan_tt_action_item.ftime_begin
        ,plan_tt_action_item.fstop_item2round_id
        ,l_tt_action_item.action_type_id
        ,false
        ,l_tt_action_item.driver_id
        ,l_tt_action_item.dr_shift_id
      from plan_tt_action_item
        ,inserted_tt_action
  )
    ,inserted_tt_action_round as (
    insert into ttb.tt_action_round
    (tt_action_id
      ,round_id
    )
      select inserted_tt_action.tt_action_id
        ,p_round_id
      from inserted_tt_action
  )
  select tt_action_id
    ,(select ftime_begin from plan_tt_action_item where nn_desc = 1)
    ,(select plan_tt_action_item.fstop_item2round_id from plan_tt_action_item where nn_desc = 1)
  into l_tt_action.tt_action_id
    ,l_time_tmp
    ,l_stop_item2round_id
  from inserted_tt_action;


  RAISE LOG 'oud.jf_tt_action_fact_pkg$of_round_change creatd new tt_action round with id %', l_tt_action.tt_action_id;
  --Добавляем межрейс
  begin
    select norm_round.*
    into strict l_norm_round
    from ttb.tt_out
      join ttb.tt_variant on tt_out.tt_variant_id = tt_variant.tt_variant_id
      join ttb.norm_round on tt_variant.norm_id = norm_round.norm_id
                             and tt_out.tr_capacity_id = norm_round.tr_capacity_id
    where tt_out_id = l_tt_action.tt_out_id
          and norm_round.round_id = p_round_id
          and ttb.get_local_timestamp_from_utc(l_time_tmp)::time between hour_from and hour_to
    limit 1;
    exception
    when no_data_found then
      raise exception 'Не удалось найти норму для межрейса! Время (%)', ttb.get_local_timestamp_from_utc(l_time_tmp)::time;
  end;


  with inserted_tt_action as
  (
    insert into ttb.tt_action
    (tt_out_id
      ,action_dur
      ,dt_action
      ,action_type_id
      ,sign_deleted
      ,tt_action_group_id
      ,action_gr_id
      ,parent_tt_action_id)
      select
         l_tt_action.tt_out_id as tt_out_id
        ,0 as action_dur
        ,l_time_tmp + l_norm_round.min_inter_round_stop_dur * interval '1 seconds' as dt_action
        ,ttb.action_type_pkg$bn_round_break_base()
        ,false
        ,l_tt_action.tt_action_group_id
        ,l_tt_action.action_gr_id
        ,null
    RETURNING tt_action_id
  )
    ,inserted_tt_action_item as
  (
    insert into ttb.tt_action_item
    (tt_action_id
      ,time_begin
      ,time_end
      ,stop_item2round_id
      ,action_type_id
      ,sign_deleted
      ,driver_id
      ,dr_shift_id)
      select inserted_tt_action.tt_action_id
        ,l_time_tmp
        ,l_time_tmp + l_norm_round.min_inter_round_stop_dur * interval '1 seconds' as dt_action
        ,l_stop_item2round_id
        ,ttb.action_type_pkg$bn_round_break_base()
        ,false
        ,l_tt_action_item.driver_id
        ,l_tt_action_item.dr_shift_id
      from inserted_tt_action
  )
    ,inserted_tt_action_round as (
    insert into ttb.tt_action_round
    (tt_action_id
      ,round_id
    )
      select inserted_tt_action.tt_action_id
        ,p_round_id
      from inserted_tt_action
  )
  select tt_action_id
  into l_tt_action.tt_action_id
  from inserted_tt_action;

  --ttb.action_type_pkg$bn_round_break_base()

  begin

    RAISE LOG 'oud.jf_tt_action_fact_pkg$of_round_change creatd new tt_action round_break with id %', l_tt_action.tt_action_id;
    perform ttb.tt_oper_pkg$update_dt_action(p_tt_variant_id);

    perform oud.jf_tt_action_fact_pkg$of_delete_tt_action_id(l_tt_action_id_4_delete_array);

    --perform oud.tt_oper_rebuilder_pkg$send_task(p_tt_variant_id);
  end;

  return l_tt_action.tt_action_id;

end ;
$$;

--http://redmine.advantum.ru/issues/23979
create or replace function oud.jf_tt_action_fact_pkg$of_fill_time_fact(
  p_account_id in  numeric,
  p_attr       in  text
)
  returns refcursor
language plpgsql
as $$
declare
    l_tt_action_id_array_in    int [] := jofl.jofl_pkg$extract_tarray(p_attr, 'arr_tt_action_id_TEXT', true);
begin
    if array_length(l_tt_action_id_array_in, 1) > 0 then
        perform oud.jf_tt_action_fact_pkg$fill_time_fact(l_tt_action_id_array_in);
    end if;
    return null;
end ;
$$;

--http://redmine.advantum.ru/issues/23567
--http://redmine.advantum.ru/issues/23567
create or replace function oud.jf_tt_action_fact_pkg$of_add_technical_round(
  p_account_id in  numeric,
  p_attr       in  text
)
  returns refcursor
language plpgsql
as $$
declare
  l_round_id int := jofl.jofl_pkg$extract_number(p_attr, 'F_round_id', false);
  l_time_begin         timestamp := jofl.jofl_pkg$extract_date(p_attr, 'BEGIN_DT', false);
  l_tt_out_id int := jofl.jofl_pkg$extract_number(p_attr, 'tt_out_id', false);
  l_tt_out ttb.tt_out%rowtype;
  l_tt_variant ttb.tt_variant%rowtype;
  l_tt_action_item ttb.tt_action_item%rowtype;
  l_tt_action_group_id int;
  l_round rts.round%rowtype;
  l_tt_action_id int;
  l_tt_at_round_id int;
  l_action_type_id smallint;
begin


  select *
  into strict l_round
  from rts.round
  where round_id = l_round_id;

  select *
  into strict l_tt_out
  from ttb.tt_out
  where tt_out_id = l_tt_out_id;

  select *
  into strict l_tt_variant
  from ttb.tt_variant
  where tt_variant_id = l_tt_out.tt_variant_id;


  select *
  into strict l_tt_action_item
  from ttb.tt_action_item
  where tt_action_id in (
    select tt_action_id
    from ttb.tt_action
    where tt_out_id = l_tt_out.tt_out_id
          and not tt_action.sign_deleted
  )
        and tt_action_item.driver_id is not null
        and tt_action_item.dr_shift_id is not null
        and not tt_action_item.sign_deleted
  order by time_begin limit 1;

  if l_time_begin::date != l_tt_variant.order_date THEN
    raise exception 'Дата начала д.б. совпадать с датой расписания';
  END IF;

  select tt_action_group_id
  into l_tt_action_group_id
  from ttb.tt_action_group
  where tt_variant_id = l_tt_variant.parent_tt_variant_id
        and ref_group_kind_id = ttb.ref_group_kind_pkg$first_action() limit 1;

  begin
    select tt_at_round_id, action_type_id
    into strict l_tt_at_round_id, l_action_type_id
    from ttb.tt_action_type
      join ttb.tt_at_round on tt_action_type.tt_action_type_id = tt_at_round.tt_action_type_id
    where tt_variant_id = l_tt_variant.parent_tt_variant_id
          --and action_type_id = ttb.action_type_pkg$park_to_stop()
          and tt_at_round.round_id = l_round.round_id;
    EXCEPTION
    when no_data_found THEN
      raise exception 'Не найден рейс указанного типа!';
  end;

  declare
    ldt_last_time timestamp;
    l_time_diff interval;
    l_stop_item2round_id_last int;
  begin
    select ftime_begin, fstop_item2round_id
    into ldt_last_time, l_stop_item2round_id_last
    from ttb.timetable_pkg$get_round_tm(
        p_tt_at_round_id := l_tt_at_round_id,
        p_tr_capacity_id := l_tt_out.tr_capacity_id,
        p_tm_start := l_time_begin
    )
    order by ftime_begin desc limit 1;

    if ldt_last_time > l_tt_action_item.time_begin
       and l_action_type_id = ttb.action_type_pkg$park_to_stop() then
      raise exception 'Время начала первого рейса меньше прибытия из парка в конечный пункт';
    end if;

    with a as (
        select *
        from ttb.timetable_pkg$get_round_tm(
            p_tt_at_round_id := l_tt_at_round_id,
            p_tr_capacity_id := l_tt_out.tr_capacity_id,
            p_tm_start := l_time_begin
        )
    )
      ,inserted_tt_action as
    (
      insert into ttb.tt_action
      (tt_out_id
        ,action_dur
        ,dt_action
        ,action_type_id
        ,sign_deleted
        ,tt_action_group_id
        ,action_gr_id
        ,parent_tt_action_id
        ,driver_id)
        select
           l_tt_out.tt_out_id as tt_out_id
          ,0 as action_dur
          ,l_time_begin as dt_action
          ,l_round.action_type_id
          ,false
          ,l_tt_action_group_id
          ,1
          ,null
          ,l_tt_action_item.driver_id
      RETURNING tt_action_id
    )
      ,inserted_tt_action_item as
    (
      insert into ttb.tt_action_item
      (tt_action_id
        ,time_begin
        ,time_end
        ,stop_item2round_id
        ,action_type_id
        ,sign_deleted
        ,driver_id
        ,dr_shift_id)
        select inserted_tt_action.tt_action_id
          ,a.ftime_begin
          ,a.ftime_begin
          ,a.fstop_item2round_id
          ,a.faction_type_id
          ,false
          ,l_tt_action_item.driver_id
          ,l_tt_action_item.dr_shift_id
        from inserted_tt_action, a
    )
      ,inserted_tt_action_round as (
      insert into ttb.tt_action_round
      (tt_action_id
        ,round_id
        ,round_status_id
      )
        select inserted_tt_action.tt_action_id
          ,l_round_id
          ,case when now() > ldt_last_time
            then ttb.round_status_pkg$passed()
            else ttb.round_status_pkg$planned()
           end
        from inserted_tt_action
      returning tt_action_id, round_id, round_status_id
    )
    select tt_action_id
    into l_tt_action_id
    from inserted_tt_action;

    --при необходимости, пометим как пройденый
    perform oud.jf_tt_action_fact_pkg$fill_time_fact(array_agg(tt_action_id))
    from ttb.tt_action_round
    where round_status_id = ttb.round_status_pkg$passed()
          and tt_action_id = l_tt_action_id
          and round_id = l_round_id;

    l_time_diff := l_tt_action_item.time_begin - ldt_last_time;

    --если  необходимо добавим межрейсовую стоянку
    if l_time_diff > interval '1 minutes' then
      with inserted_tt_action as
      (
        insert into ttb.tt_action
        (tt_out_id
          ,action_dur
          ,dt_action
          ,action_type_id
          ,sign_deleted
          ,tt_action_group_id
          ,action_gr_id
          ,parent_tt_action_id
          ,driver_id)
          select
             l_tt_out.tt_out_id as tt_out_id
            ,0 as action_dur
            ,ldt_last_time as dt_action
            ,ttb.action_type_pkg$bn_round_break()
            ,false
            ,l_tt_action_group_id
            ,1
            ,null
            ,l_tt_action_item.driver_id
        RETURNING tt_action_id
      )
      insert into ttb.tt_action_item
      (tt_action_id
        ,time_begin
        ,time_end
        ,stop_item2round_id
        ,action_type_id
        ,sign_deleted
        ,driver_id
        ,dr_shift_id)
        select inserted_tt_action.tt_action_id
          ,ldt_last_time
          ,ldt_last_time + l_time_diff
          ,l_stop_item2round_id_last
          ,ttb.action_type_pkg$bn_round_break_reg()
          ,false
          ,l_tt_action_item.driver_id
          ,l_tt_action_item.dr_shift_id
        from inserted_tt_action;
    end if;

  end;
  perform ttb.tt_oper_pkg$update_dt_action(l_tt_out.tt_variant_id);

  return l_tt_action_id;
end ;
$$;

create or replace function oud.jf_tt_action_fact_pkg$fill_time_fact(
  p_tt_action_id_array int []
)
  returns int
language plpgsql
as $$
begin
  if array_length(p_tt_action_id_array, 1) > 0 then

    with a as  (insert into ttb.tt_action_fact
    (
      tt_action_id
      ,stop_item2round_id
      ,time_fact_begin
      ,time_fact_end
    )
      select tt_action_id
        ,stop_item2round_id
        ,time_begin
        ,time_end
      from ttb.tt_action_item
      where not tt_action_item.sign_deleted
            and tt_action_id = any (p_tt_action_id_array)
    ON CONFLICT (tt_action_id, stop_item2round_id) DO NOTHING
    returning tt_action_fact_id
    )
    insert into af.tt_action_fact_af(
      tt_action_fact_id
      ,is_manual_fill
    )
      select tt_action_fact_id
        ,true
      from a;

    with actions  as
    (
        select tt_action_item.tt_action_id
          ,stop_item2round.round_id
          ,min(time_begin) time_begin
          ,max(time_end) time_end
        from ttb.tt_action_item
          join rts.stop_item2round on stop_item2round.stop_item2round_id = tt_action_item.stop_item2round_id
        where not tt_action_item.sign_deleted
              and not rts.stop_item2round.sign_deleted
              and tt_action_id = any (p_tt_action_id_array)
        group by
          tt_action_item.tt_action_id
          ,stop_item2round.round_id
    )
    update ttb.tt_action_round
    set time_fact_begin = actions.time_begin
      ,time_fact_end = actions.time_end
      ,round_status_id = ttb.round_status_pkg$passed()
    from actions
    where tt_action_round.round_id = actions.round_id
          and tt_action_round.tt_action_id = actions.tt_action_id;
  end if;
  return 1;
end ;
$$;


create or replace function oud.jf_tt_action_fact_pkg$change_tt_action_time_begin(
   p_tt_action_id int
  ,p_time_begin timestamp
)
  returns void
language plpgsql
as $$
declare
  l_tt_out_id int;
  l_tt_variant_id int;
begin

  select tt_out.tt_out_id
         ,tt_out.tt_variant_id
  into strict l_tt_out_id, l_tt_variant_id
  from ttb.tt_action
    join ttb.tt_out on tt_action.tt_out_id = tt_out.tt_out_id
  where  tt_action.tt_action_id = p_tt_action_id
         and not tt_action.sign_deleted
         and not tt_out.sign_deleted;

  perform ttb.tt_out_pkg$make_tmp_tt(l_tt_out_id);

  select ttb.timetable_edit_pkg$update_out(p_tt_action_id, p_time_begin) into p_time_begin;

  perform ttb.tt_out_pkg$fill_db_table(l_tt_out_id);

  perform ttb.jf_ttv_check_condition_pkg$of_close_prev_check_cond(l_tt_variant_id, null::integer);
end;
$$;

create or replace function oud.jf_tt_action_fact_pkg$of_add_action(
  p_id_account numeric,
  p_attr text
)
  returns text
language plpgsql
as $$
declare
  l_tt_variant_id numeric:= jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
  l_tt_action_id ttb.tt_action.tt_action_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'tt_action_id', true);
  l_return text;
begin
  select  ttb.jf_tt_out_pkg$of_make_main_action(
    p_id_account,
    p_attr
  ) into l_return;

  return l_return;
end;
$$;

create or replace function oud.jf_tt_action_fact_pkg$of_delete_action(
  p_id_account numeric,
  p_attr text
)
  returns void
language plpgsql
as $$
declare
  l_tt_variant_id numeric:= jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
  l_tt_action_id ttb.tt_action.tt_action_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'tt_action_id', true);
begin
  perform ttb.jf_tt_out_pkg$of_delete_action (
    p_id_account,
    p_attr
  );
end;
$$;

create or replace function oud.jf_tt_action_fact_pkg$of_add_incident_downtime(
   p_tt_out_id int
  ,p_driver_id int
  ,p_time_start timestamp
  ,p_time_finish timestamp
  ,p_stop_item2round_id int
  ,p_dr_shift_id int
)
  returns int
language plpgsql
as $$
declare
    l_tt_action_id int;
begin
  with tt_action_inserted as  (
    insert into ttb.tt_action
    (tt_out_id
      ,dt_action
      ,action_dur
      ,action_type_id
      ,sign_deleted
      ,driver_id
    )
    values (
      p_tt_out_id
      ,p_time_start
      ,0
      ,ttb.action_type_pkg$incident_downtime()
      ,false
      ,p_driver_id
    )
   returning tt_action_id
  )
  ,inserted_tt_action_item as (
  insert into ttb.tt_action_item
    (tt_action_id
    ,time_begin
    ,time_end
    ,stop_item2round_id
    ,action_type_id
    ,sign_deleted
    ,dr_shift_id
    ,driver_id
    )
    select
    tt_action_inserted.tt_action_id
    ,p_time_start
    ,p_time_finish
    ,p_stop_item2round_id
    ,ttb.action_type_pkg$incident_downtime()
    ,false
    ,p_dr_shift_id
    ,p_driver_id
    from tt_action_inserted
    )
    select tt_action_id
    into l_tt_action_id
    from tt_action_inserted;

    return l_tt_action_id;
end;
$$;

