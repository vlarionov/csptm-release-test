﻿
create or replace function oud.jf_driver_calls_pkg$of_rows
  (p_account_id          in numeric,
   p_attr                in text,
   p_rows                out refcursor)
returns refcursor
-- ========================================================================
-- Форма "Связь с водителем"
-- ========================================================================
as $$ declare 
   l_query_date          timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'F_DT', true), now());
   l_order_date          timestamp := date_trunc('day', l_query_date);
   l_order_period        tsrange;
   l_driver_tab_num      text      := jofl.jofl_pkg$extract_varchar(p_attr, 'f_driver_tab_num_TEXT', true);
   l_garage_num          integer   := jofl.jofl_pkg$extract_number(p_attr, 'f_garage_num_TEXT', true);
   l_route_id            integer   := jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', true);
   l_shift_num           integer   := jofl.jofl_pkg$extract_number(p_attr, 'f_dr_shift_num_NUMBER', true);
begin
   -- указаную дату превращаем в период от 00:00:00 до 23:59:59 указанной даты
   l_order_period := tsrange(l_order_date, l_order_date + interval '86399 seconds');
   
   open p_rows for
   select distinct
          lower(tta.action_range)::date         as order_date,
          rt.route_num || ' ' ||trtp.short_name as route_num,
          tr.garage_num                         as garage_num,
          rtrim(coalesce(dr.driver_last_name, '') || ' ' ||  coalesce(dr.driver_name, '') || ' ' ||  coalesce(dr.driver_middle_name, '')) as driver_name,
          dr.tab_num                            as tab_num,
          tto.tt_out_num || '/' || drsh.dr_shift_num::text as out_num_shift,
          rt.route_id                           as route_id,
          ttai.driver_id                        as f_driver_id,
          tr.tr_id                              as f_tr_id,
          tto.tt_out_id                         as out_id,
          tto.tt_out_num                        as out_num,
          drsh.dr_shift_num::text               as dr_shift,
          rt.route_num                          as route_num_str,
          rt.route_num ||' '|| tto.tt_out_num ||' '|| drsh.dr_shift_num::text as msg_part
     from ttb.tt_out tto
     join core.v_tr_ref tr on tr.tr_id = tto.tr_id
     join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
     join ttb.tt_variant ttv on ttv.tt_variant_id = tto.tt_variant_id
     join rts.route_variant rtv on rtv.route_variant_id = ttv.route_variant_id
     join rts.route rt on rt.route_id = rtv.route_id
     join core.tr_type trtp on trtp.tr_type_id = rt.tr_type_id
     join ttb.tt_action_item ttai on ttai.tt_action_id = tta.tt_action_id
     join core.driver dr on dr.driver_id = ttai.driver_id
     join ttb.dr_shift drsh on drsh.dr_shift_id = ttai.dr_shift_id
    where not tto.sign_deleted
      and tto.parent_tt_out_id is not null
      and not tta.sign_deleted
      and not isempty(tta.action_range)
      and not ttv.sign_deleted
      and not rtv.sign_deleted
      and not rt.sign_deleted
      and not ttai.sign_deleted
      -- применям фильтры
--    and (l_order_date is null or tta.action_range @> l_order_period)
      and not isempty(tta.action_range * l_order_period)
      and (l_driver_tab_num is null or l_driver_tab_num = '' or dr.tab_num like '%' || l_driver_tab_num || '%')
      and (l_garage_num is null or tr.garage_num = l_garage_num)
      and (l_route_id is null or rtv.route_id = l_route_id)
      and tr.tr_id in (select adm.account_data_realm_pkg$get_trs_by_all(p_account_id))
      and (l_shift_num is null or drsh.dr_shift_num = l_shift_num::smallint)
    order by 4;
end;
$$ language plpgsql volatile security invoker;


create or replace function oud.jf_driver_calls_pkg$of_dial
  (p_account_id          in numeric,
   p_attr                in text)
returns text
-- ========================================================================
-- Голосовой вызов водителя - одиночный или групповой, без автодозвона
-- ========================================================================
as $$
declare
  l_result      text;
  l_out_json    text;
  l_out_list    bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'arr_out_id_TEXT', true);
  l_driver_list bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'arr_driver_id_TEXT', true);
  l_tr_list     bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'arr_tr_id_TEXT', true);
  l_unit_list   bigint[];
  l_call_request_id     bigint := null;
  l_call_count  smallint;
  crow          record;
  l_garage      text;
  l_fio         text;
  l_out         text;
  l_route       text;
begin
  l_call_count := cardinality(l_driver_list);
  -- ищем блоки на выбранных ТС
  for crow in select * from oud.jf_driver_calls_pkg$get_unit_list(l_out_list, l_driver_list, l_tr_list, array[]::text[]) loop
   -- собираем блок[и] в массив
   l_garage := null;
   l_fio := null;
   l_out := null;
   if crow.unit_id is not null and not crow.order_err then
    l_unit_list := crow.unit_id || l_unit_list;
	  -- формируем строку-результат
	  select tr.garage_num into l_garage from core.tr tr where tr.tr_id = crow.tr_id;
	  select rtrim(dr.driver_last_name||' '||dr.driver_name||' '||dr.driver_middle_name) into l_fio from core.driver dr where dr.driver_id = crow.driver_id;
    select o.tt_out_num,
           r.route_num
      into l_out, l_route
      from ttb.tt_out o
      join ttb.tt_variant ttv on ttv.tt_variant_id = o.tt_variant_id
      join rts.route_variant rv on rv.route_variant_id = ttv.route_variant_id
      join rts.route r on r.route_id = rv.route_id
     where o.tt_out_id = crow.out_id;
     l_out_json := (jsonb_build_object('dr_fio', l_fio,
                                       'tr_garage_num', l_garage,
                                       'route', l_route::text,
                                       'out_num', l_out))::text || coalesce(','||l_out_json, '');
   elsif l_call_count = 1 and crow.order_err then
     raise exception '<<Водитель в данный момент не работает на данном ТС. Нет активного наряда>>';
   elsif  l_call_count = 1 and crow.unit_id is null then
     raise exception '<<Не найден блок для связи с ТС>>';
   end if;
  end loop;

  if cardinality(l_unit_list) > 0 then
   if  l_call_count = 1 then
    -- вызов одного водителя
    l_result := voip.voice_call_pkg$call_to_unit(p_account_id::bigint, l_tr_list[1]::bigint, l_unit_list[1]::integer, l_call_request_id::bigint, l_driver_list[1]::integer);
   else
    -- вызов группы
    l_result := voip.voice_call_pkg$call_to_units(p_account_id::bigint, l_call_request_id::bigint, l_unit_list::integer[], l_driver_list::integer[], false);
   END IF;
  else
   raise exception '<<Не найдены блоки для связи с ТС>>';
  end if;
  return '['||l_out_json||']';
end;
$$ language plpgsql volatile security invoker;


create or replace function oud.jf_driver_calls_pkg$inner_of_dial
  (p_account_id          in numeric,
   p_attr                in text,
   p_redial              in boolean)
returns text
-- ========================================================================
-- Голосовой вызов водителя
-- ========================================================================
as $$ declare
   l_query_date          timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'F_DT', true), now());
   l_order_date          timestamp := date_trunc('day', l_query_date);
   l_tr_id               bigint := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_id', true);
   l_driver_id           integer:= jofl.jofl_pkg$extract_number(p_attr, 'f_driver_id', true);
   l_unit_id             integer;
   l_call_request_id     bigint := jofl.jofl_pkg$extract_number(p_attr, 'f_call_request_id', true);
   l_result              text;
begin
   
-- raise notice 'l_order_date = %', l_order_date;
-- raise notice 'l_driver_id = %', l_driver_id;
-- raise notice 'l_tr_id = %', l_tr_id;
   
   select min(unit_id) into l_unit_id
     from core.unit2tr utr
     join core.unit_bnst unt on unt.unit_bnst_id = utr.unit_id
    where tr_id = l_tr_id;
   
-- raise notice 'l_unit_id = %', l_unit_id;
   
   if l_unit_id is null then
      raise exception '<<Не найден блок для связи с ТС>>';
   end if;
   
   if not exists(select 1
                   from ttb.tt_out tto
                   join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
                  where not tto.sign_deleted
                    and not tta.sign_deleted
                    and tto.tr_id = l_tr_id
                    and tta.driver_id = l_driver_id
                    and tta.action_range @> localtimestamp)
   then
      raise exception '<<Водитель в данный момент не работает на данном ТС. Нет активного наряда>>';
   end if;
   
   l_result := voip.voice_call_pkg$call_to_unit(p_account_id::bigint, l_tr_id, l_unit_id, l_call_request_id, l_driver_id, p_redial);
   return l_result;
end;
$$ language plpgsql volatile security invoker;


create or replace function oud.jf_driver_calls_pkg$of_redial
  (p_account_id          in numeric,
   p_attr                in text)
returns text
-- ========================================================================
-- Голосовой вызов водителя с автодозвоном
-- ========================================================================
as $$ declare
   v_voice_call_redial_status   text;
   v_try_count                  int := asd.get_constant('VOIP_REDIAL_TRY_COUNT')::int;          -- число попыток
   v_call_timeout               int := asd.get_constant('VOIP_REDIAL_CALL_TIMEOUT_SEC')::int;   -- seconds = 5 min среднее время соединения
   v_begin_date                 timestamp := localtimestamp - (v_call_timeout * v_try_count || ' sec')::interval;
 --v_begin_date                 timestamp := '2018-02-21 06:05:07'::timestamp - (v_call_timeout * v_try_count || ' sec')::interval;
begin
   
 --raise notice 'v_begin_date = %', v_begin_date;
   
   with voice_call_redial as (
      select voc.voice_call_id, voc.voice_call_date, vor.voice_call_redial_time, vor.voice_call_redial_status
        from voip.voice_call voc
        left join voip.voice_call_redial vor on vor.voice_call_redial_id = voc.voice_call_redial_id
       where voc.account_id = p_account_id
         and voc.voice_call_date > v_begin_date
   )
   select voice_call_redial_status
     into v_voice_call_redial_status
     from voice_call_redial
    where voice_call_id = (select max(voice_call_id) from voice_call_redial);
   
 --raise notice 'v_voice_call_redial_status = %', v_voice_call_redial_status;
   
   if v_voice_call_redial_status is not null and 'TRY' = substring(v_voice_call_redial_status from 1 for 3) then
      raise exception '<<<Выполняется вызов с автодозвоном. Отмените операцию или дождитесь ответа>>';
   end if;
   
   return oud.jf_driver_calls_pkg$inner_of_dial(p_account_id, p_attr, true);
end;
$$ language plpgsql volatile security invoker;


create or replace function oud.jf_driver_calls_pkg$of_cancel_redial
  (p_account_id          in numeric,
   p_attr                in text)
returns text
-- ========================================================================
-- Отмена голосового вызова водителя с автодозвоном
-- ========================================================================
as $$ declare
   v_voice_call_id              bigint;
   v_voice_call_redial_id       integer;
   v_voice_call_date            timestamp;
   v_voice_call_redial_status   text;
   v_try_count                  int := asd.get_constant('VOIP_REDIAL_TRY_COUNT')::int;          -- число попыток
   v_call_timeout               int := asd.get_constant('VOIP_REDIAL_CALL_TIMEOUT_SEC')::int;   -- seconds = 5 min среднее время соединения
   v_begin_date                 timestamp := localtimestamp - (v_call_timeout * v_try_count || ' sec')::interval;
 --v_begin_date                 timestamp := '2018-02-21 06:05:07'::timestamp - (v_call_timeout * v_try_count || ' sec')::interval;
begin
   
-- raise notice 'v_begin_date = %', v_begin_date;
   
   with voice_call_redial as (
      select voc.voice_call_id, voc.voice_call_redial_id, voc.voice_call_date, vor.voice_call_redial_status
        from voip.voice_call voc
        left join voip.voice_call_redial vor on vor.voice_call_redial_id = voc.voice_call_redial_id
       where voc.account_id = p_account_id
         and voc.voice_call_date > v_begin_date
   )
   select voice_call_id, voice_call_redial_id, voice_call_date, voice_call_redial_status
     into v_voice_call_id, v_voice_call_redial_id, v_voice_call_date, v_voice_call_redial_status
     from voice_call_redial
    where voice_call_id = (select max(voice_call_id) from voice_call_redial);
   
-- raise notice 'voice_call_date = %', v_voice_call_date;
-- raise notice 'v_voice_call_redial_status = %', v_voice_call_redial_status;
   
   if v_voice_call_redial_status is not null and 'TRY' = substring(v_voice_call_redial_status from 1 for 3) then
--    raise notice 'v_voice_call_id = %', v_voice_call_id;
--    raise notice 'v_voice_call_redial_id = %', v_voice_call_redial_id;
      return voip.voice_call_pkg$cancel_call_to_units(v_voice_call_redial_id);
   end if;
   
   return 'OK';
end;
$$ language plpgsql volatile security invoker;


create or replace function oud.jf_driver_calls_pkg$get_unit_list
  (p_out_id              bigint[],
   p_driver_id           bigint[],
   p_tr_id               bigint[],
   p_msg_part            text[])
  returns table (number_out_shift text,
                 out_id    bigint,
                 driver_id bigint,
                 tr_id     bigint,
                 unit_id   bigint,
                 order_err boolean)
  -- ========================================================================
  -- Функция возвращает идентификаторы блоков и признак "отсутствия наряда" для пары Водитель-ТС
  -- ========================================================================
as $$
begin

  return query
  select replace(sr.number_out_shift, '"', '') as number_out_shift,
         sr.out_id::bigint as out_id,
         sr.driver_id::bigint as driver_id,
         sr.tr_id::bigint as tr_id,
         (select min(utr.unit_id)::bigint as unit_id
          from core.unit2tr utr
            join core.unit_bnst unt on unt.unit_bnst_id = utr.unit_id
          where utr.tr_id = sr.tr_id::int) as unit_id,
        /* not exists (select 1
                     from ttb.tt_out tto
                       join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
--    lead(time_begin) over (partition by ai.tt_action_id order by ai.time_begin) as lead_tb,
--    lag(time_begin) over (partition by ai.tt_action_id order by ai.time_begin) as lag_tb,
                     where not tto.sign_deleted and
                           not tta.sign_deleted and
                           tto.tr_id = sr.tr_id and
                           tta.driver_id = sr.driver_id and
                           tta.action_range @> localtimestamp)*/ FALSE as order_err
  from unnest(p_msg_part, p_out_id, p_driver_id, p_tr_id) as sr (number_out_shift, out_id, driver_id, tr_id);
end;
$$ language plpgsql volatile security invoker;

create or replace function oud.jf_driver_calls_pkg$fill_message_attr
  (p_account_id          in numeric,
   p_attr                in text)
returns text
-- ========================================================================
-- Функция обогощает параметры данными сообщения
-- ========================================================================
as $$
declare
   l_type_text           text     := jofl.jofl_pkg$extract_varchar(p_attr, 'PARAM_MESSAGE_TEXT', true);
   l_message_template    smallint := jofl.jofl_pkg$extract_number(p_attr, 'f_message_template_id', true);
   l_msg_type            text     := jofl.jofl_pkg$extract_varchar(p_attr, 'F_MSG_TYPE_INPLACE_K', true);
   l_attr                text;
   l_background          text := 'background';
   l_dialog              text := 'normal';
   l_answer_text         text := 'Да,Нет';
   l_empty_json          text := '{}';
begin
   -- если нет сообщения - не выбран шаблон и не введен текст
   if coalesce(l_type_text, '') = '' and l_message_template is null  then
      raise exception '<<Отсутствует сообщение для водителя>>';
   end if;
   
   -- обработка введенных/выбранных данных сообщения
   if coalesce(l_type_text, '') <> '' then -- сообщение, введенное вручную
      if coalesce(l_msg_type, '') = '' then -- по-умолчанию - фоновое сообщение
         l_attr := jsonb_build_object('F_MSG_TYPE_INPLACE_K', l_background)::text;
      elsif l_msg_type = l_dialog then -- для диалогового - по умолчанию ответ "Да,Нет"
         l_attr := jsonb_build_object('STR_FIELD_ANSWERS_TEXT', l_answer_text)::text;
      end if;
   else -- формализованное сообщение
      select (jsonb_build_object('PARAM_MESSAGE_TEXT', mt.contents,
                                 'F_SHOW_TIME_NUMBER', mt.display_duration::text,
                                 'F_MSG_TYPE_INPLACE_K', mt.message_type_eng) ||
             -- если тип сообщения "Диалоговый" - добавляем возможные ответы
             case
                when mt.message_type_eng = l_dialog then jsonb_build_object('STR_FIELD_ANSWERS_TEXT', mt.answer_list)
                else l_empty_json::jsonb
             end ||
             -- если нужен звуковой сигнал - добавляем
             case
                when mt.is_sound then jsonb_build_object ('F_IS_SOUND_INPLACE_K', 'yes')
                else l_empty_json::jsonb
             end)::text into l_attr
        from cmnd.jf_message_template_pkg$get_rows(p_account_id, jsonb_build_object('message_template_id', l_message_template::text)::text) mt;
   end if;
   
   -- собираем и возвращаем результат
   l_attr := (p_attr::jsonb || (coalesce(l_attr, l_empty_json))::jsonb)::text;
   return l_attr;
end;
$$ language plpgsql volatile security invoker;


create or replace function oud.jf_driver_calls_pkg$of_text
  (p_account_id          in numeric,
   p_attr                in text)
returns text
-- ========================================================================
-- Отправка сообщения водителю - одному или группе
-- ========================================================================
as $$ declare
  NPH_SED_DEVICE_TITLE_DATA_REQUEST integer := 19;
  l_out_list    bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'arr_out_id_TEXT', true);
  l_driver_list bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'arr_driver_id_TEXT', true);
  l_tr_list     bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'arr_tr_id_TEXT', true);
  l_attr                text;
  l_result              text;
  l_call_count  smallint;
  l_send_count  smallint := 0;
  crow          record;
begin
  /* добавляем необходимые параметры для сообщения */
  l_attr := oud.jf_driver_calls_pkg$fill_message_attr(p_account_id, p_attr);
  l_attr := (l_attr::jsonb || jsonb_build_object('cmnd_ref_id', NPH_SED_DEVICE_TITLE_DATA_REQUEST))::text;

  l_call_count := cardinality(l_driver_list);
  for crow in select * from oud.jf_driver_calls_pkg$get_unit_list(l_out_list, l_driver_list, l_tr_list, array[]::text[]) loop
   -- если сообщение одному водителю - обрабатываем ошибку
   if crow.unit_id is not null and not crow.order_err then
    /* добавляем параметры для команды блоку и отправляем сообщение */
    l_attr := (l_attr::jsonb || jsonb_build_object('unit_id', crow.unit_id))::text;
    l_result := cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_sed_device_title_data(p_account_id, l_attr);
    l_send_count := l_send_count + 1;
   elsif l_call_count = 1 and crow.order_err then
     raise exception '<<Водитель в данный момент не работает на данном ТС. Нет активного наряда>>';
   elsif  l_call_count = 1 and crow.unit_id is null then
     raise exception '<<Не найден блок для связи с ТС>>';
   end if;
  end loop;

  if l_send_count = 0 then
     raise exception '<<Не найдены блоки для связи с ТС>>';
  end if;

  return null;
end;
$$ language plpgsql volatile security invoker;

-- ================================================================================
-- Методы для отправки фиксированных сообщений (расписание, интервал, начало рейса)
-- drop function oud.jf_driver_calls_pkg$checkpoint2round(bigint);
create or replace function oud.jf_driver_calls_pkg$checkpoint2round
  (p_out_id              in bigint)
returns table (tt_out_id bigint,
               tt_action_id bigint,
               tt_action_item_id bigint,
               stop_item_id bigint,
               stop_name text,
               arrive_time timestamp,
               row_num smallint)
-- ================================================================================
-- Функция возвращает время прохода контрольных точек для tt_out_id или всех актуальных tt_out_id
-- ================================================================================
as $$ declare
 l_prod_round smallint[];
 l_dinner smallint[];
begin
   l_prod_round := ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$production_round(), false);
   l_dinner := ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$lunch(), FALSE)||ttb.action_type_pkg$dinner();

   return query
   with
   tt_action_list as (
      select tout.tt_variant_id,
             tout.tt_out_id,
             tact.tt_action_id,
             lower(tact.action_range)::timestamp as action_start,
             upper(tact.action_range)::timestamp as action_end,
             tact.action_type_id,
             trd.round_status_id,
             max(trd.time_fact_begin) over (partition by tout.tt_out_id order by tout.tt_out_id ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) as max_fact_start,
             last_value(trd.tt_action_id) over (partition by tout.tt_out_id order by trd.time_fact_begin nulls first ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) as last_start_round
        from ttb.tt_out tout
        join ttb.tt_action tact on tact.tt_out_id = tout.tt_out_id
        left join ttb.tt_action_round trd on trd.tt_action_id = tact.tt_action_id
       where not tout.sign_deleted and
             not tact.sign_deleted and
             tout.parent_tt_out_id is not null and
             tout.tt_out_id = coalesce(p_out_id, tout.tt_out_id) and
             (tact.action_type_id = ANY (l_prod_round) or tact.action_type_id = ANY (l_dinner))
     ),
   tail_action_item_list as (
      select alist.*,
             ttai.tt_action_item_id,
             ttai.stop_item2round_id,
             ttai.action_type_id as item_type_id,
             ttai.time_begin as item_start,
             ttai.time_end as item_end
        from tt_action_list alist
        join ttb.tt_action_item ttai on ttai.tt_action_id = alist.tt_action_id
        -- ищем пройденные КП (чтобы их исключить)
        left join ttb.tt_action_fact ttaf on ttaf.tt_action_id = ttai.tt_action_id and
                                             ttaf.stop_item2round_id = ttai.stop_item2round_id
       where (((alist.last_start_round is not null and
                alist.action_start >= (select alistmax.action_start
                                         from tt_action_list alistmax
                                        where alistmax.tt_action_id = alist.last_start_round)) or
               (alist.last_start_round is null and
                alist.action_start >= current_timestamp)) and
              (alist.round_status_id in (ttb.round_status_pkg$in_progress(), ttb.round_status_pkg$planned()) or alist.round_status_id is null)) and
             (alist.action_type_id = ttb.action_type_pkg$dinner() or
              (alist.action_type_id != ttb.action_type_pkg$dinner() and ttaf.tt_action_fact_id is null))
     ),
   checkpoint_list as (
      select tai.*,
             si.stop_item_id,
             case
              when tai.action_type_id = ttb.action_type_pkg$dinner()
                then 'Обед' -- (select action_type_name from ttb.action_type where action_type_id = tai.action_type_id)
              else s.name
             end as display_name,
             (row_number() over (partition by tai.tt_out_id order by tai.item_start, tai.item_end ROWS BETWEEN UNBOUNDED PRECEDING AND current row))::smallint as row_num
        from tail_action_item_list tai
             /* получаем название остановки */
       left join rts.stop_item2round si2rd on si2rd.stop_item2round_id = tai.stop_item2round_id
       left join rts.stop_item si on si.stop_item_id = si2rd.stop_item_id
       left join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
       left join rts.stop s on s.stop_id = sl.stop_id
      where tai.action_type_id = ttb.action_type_pkg$dinner() or
            (tai.action_type_id != ttb.action_type_pkg$dinner() and
             /* отбираем только КП и конечные пункты рейса */
             exists (select 1
                       from rts.stop_item2round_type si2rdt
                       join rts.stop_item_type sit on sit.stop_item_type_id = si2rdt.stop_item_type_id
                      where si2rdt.stop_item2round_id = tai.stop_item2round_id and
                            sit.stop_type_id in (rts.stop_type_pkg$checkpoint(), rts.stop_type_pkg$finish(), rts.stop_type_pkg$finish_a(), rts.stop_type_pkg$finish_b())))
     )
   /* для совместимости с имеющимся "окружением" - формируем те же поля,которые уже используются */
   select chp.tt_out_id::bigint,
          chp.tt_action_id::bigint,
          chp.tt_action_item_id::bigint,
          chp.stop_item_id::bigint,
          case
           when chp.action_type_id = ttb.action_type_pkg$dinner()
             then to_char(chp.item_start + interval '3 hours', 'HH24:MI')||'   '||chp.display_name
           else chp.display_name
          end as stop_name,
          case
           when chp.action_type_id = ttb.action_type_pkg$dinner() then chp.item_end
           else chp.item_start
          end as arrive_time,
          chp.row_num::smallint
     from checkpoint_list chp;
end;
$$ language plpgsql volatile security invoker;


create or replace function oud.jf_driver_calls_pkg$interval2round (p_out_id in bigint) returns text
-- ================================================================================
-- Функция возвращает строку с интервалами выхода для отпарвки в сообщении
-- ================================================================================
as $$
declare
   l_result              text;
   l_action_round        smallint[];
   l_forward_dt          timestamp;
   l_fact_dt             timestamp;
   l_action_start        timestamp;
   l_forward_start       timestamp;
   l_backward            smallint;
   l_forward             smallint;
   l_tt_interval         smallint;
begin
   l_action_round := ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$round(), false);
  -- возвращает данные для расчета интервалов
   with last_stop_item as (
      select distinct
             tto.tt_variant_id,
             tta.tt_out_id,
             last_value(ttai.tt_action_id) over (partition by tto.tt_out_id order by ttaf.time_fact_begin ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as tt_action_id,
             last_value(ttaf.stop_item2round_id) over (partition by tto.tt_out_id order by ttaf.time_fact_begin ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as stop_item2round_id,
             (last_value(ttaf.time_fact_begin) over (partition by tto.tt_out_id order by ttaf.time_fact_begin ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)) as fact_time
        from ttb.tt_out tto
        join ttb.tt_action tta on tta.tt_out_id  = tto.tt_out_id
        join ttb.tt_action_item ttai on ttai.tt_action_id = tta.tt_action_id
        join ttb.tt_action_fact ttaf on ttaf.tt_action_id = tta.tt_action_id
                                    and ttaf.stop_item2round_id = ttai.stop_item2round_id
       where not tto.sign_deleted
         and not tta.sign_deleted
         and not ttai.sign_deleted
         and tto.tt_out_id = p_out_id
         and tta.action_type_id = any (l_action_round)
   )
   select lsi.fact_time,
          (select lower(a4.action_range) as action_start
             from ttb.tt_action a4
            where a4.tt_action_id = lsi.tt_action_id) action_start,
          (select max(f1.time_fact_begin) as ft
             from ttb.tt_variant v1
             join ttb.tt_out o1 on o1.tt_variant_id = v1.tt_variant_id
             join ttb.tt_action a1 on a1.tt_out_id = o1.tt_out_id
             join ttb.tt_action_item i1 on i1.tt_action_id = a1.tt_action_id
             left join ttb.tt_action_fact f1 on f1.tt_action_id = a1.tt_action_id
                                            and f1.stop_item2round_id = i1.stop_item2round_id
            where v1.tt_variant_id = lsi.tt_variant_id
              and o1.tt_out_id != lsi.tt_out_id
              and i1.stop_item2round_id = lsi.stop_item2round_id
              and f1.time_fact_begin < lsi.fact_time
              and a1.action_type_id = any (l_action_round)
              and not v1.sign_deleted
              and not o1.sign_deleted
              and not a1.sign_deleted
              and not i1.sign_deleted
          ) as forward_time,
          (select max(i3.time_begin) as pt
             from ttb.tt_variant v3
             join ttb.tt_out o3 on o3.tt_variant_id = v3.tt_variant_id
             join ttb.tt_action a3 on a3.tt_out_id = o3.tt_out_id
             join ttb.tt_action_item i3 on i3.tt_action_id = a3.tt_action_id
             join rts.stop_item2round s3 on s3.stop_item2round_id = i3.stop_item2round_id
                                        and s3.order_num = 1
            where v3.tt_variant_id = lsi.tt_variant_id
              and o3.tt_out_id != lsi.tt_out_id
              and a3.action_type_id = any (l_action_round)
              and i3.time_begin < (select ai.time_begin
                                     from ttb.tt_action_item ai
                                     join rts.stop_item2round si on si.stop_item2round_id = ai.stop_item2round_id
                                                                and si.order_num = 1
                                    where ai.tt_action_id = lsi.tt_action_id)
              and not v3.sign_deleted
              and not o3.sign_deleted
              and not a3.sign_deleted
              and not i3.sign_deleted) as forward_start,
         (select distinct first_value(extract ('hours' from after_stops.time_fact_begin-passed_stops.time_fact_begin) * 60 +
                                      extract ('minutes' from after_stops.time_fact_begin-passed_stops.time_fact_begin))
                          over (partition by passed_stops.tt_out_id order by passed_stops.order_num desc, after_stops.time_fact_begin asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as bs
          from
           (SELECT
              a2.tt_out_id,
              a2.tt_action_id,
              i2.stop_item2round_id,
              sr2.order_num,
              f2.time_fact_begin
            FROM ttb.tt_action a2
              JOIN ttb.tt_action_item i2 ON i2.tt_action_id = a2.tt_action_id
              JOIN ttb.tt_action_fact f2 ON f2.tt_action_id = a2.tt_action_id AND
                                            f2.stop_item2round_id = i2.stop_item2round_id
              join rts.stop_item2round sr2 on sr2.stop_item2round_id=i2.stop_item2round_id
              join ttb.tt_action_round r2 on r2.tt_action_id = a2.tt_action_id
            WHERE a2.tt_action_id = lsi.tt_action_id AND
                  i2.stop_item2round_id != lsi.stop_item2round_id AND
                  r2.round_status_id = ttb.round_status_pkg$in_progress()) passed_stops
           join (select o3.tt_out_id,
                   a3.tt_action_id,
                   i3.stop_item2round_id,
                   f3.time_fact_begin
            from ttb.tt_out o3
                 join ttb.tt_action a3 on a3.tt_out_id = o3.tt_out_id
                 join ttb.tt_action_item i3 on i3.tt_action_id = a3.tt_action_id
                 join ttb.tt_action_fact f3 on f3.tt_action_id = a3.tt_action_id and
                                               f3.stop_item2round_id = i3.stop_item2round_id
                 join ttb.tt_action_round r3 on r3.tt_action_id = a3.tt_action_id
            where o3.tt_variant_id = lsi.tt_variant_id and
                  o3.tt_out_id != lsi.tt_out_id and
                  r3.round_status_id = ttb.round_status_pkg$in_progress() and
                  not o3.sign_deleted and
                  not a3.sign_deleted and
                  not i3.sign_deleted)  after_stops on after_stops.stop_item2round_id = passed_stops.stop_item2round_id and
                                                       after_stops.time_fact_begin > passed_stops.time_fact_begin) as backward_time
     into l_fact_dt, l_action_start, l_forward_dt, l_forward_start, l_backward
     from last_stop_item lsi;

   -- расчет собственно интервалов
   select extract ('hours' from dt.forward_interval) * 60 + extract('minutes' from dt.forward_interval) as forvard_min,
          extract ('hours' from dt.tt_forward_interval) * 60 + extract('minutes' from dt.tt_forward_interval) as tt_interval_min
     from (select (l_fact_dt - l_forward_dt) as forward_interval,
                  (l_action_start - l_forward_start) as tt_forward_interval) dt
     into l_forward, l_tt_interval;

   -- формируем финальную строку
   l_result := coalesce(l_backward ||' мин' || '->', '') || coalesce('[' || l_tt_interval || ']' || '->' ,'')  || coalesce( l_forward ||' мин', '');
   return l_result;
end;
$$ language plpgsql volatile security invoker;


create or replace function oud.jf_driver_calls_pkg$send_ttb_data
  (p_account_id          in numeric,
   p_attr                in text)
returns text
-- ========================================================================
-- Отправка сообщения с данными расписания (начало рейса, контольные пункты, интервалы)
-- ========================================================================
as $$ declare
  NPH_SED_DEVICE_TITLE_DATA_REQUEST integer := 19;
  l_out_list     bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'arr_out_id_TEXT', true);
  l_driver_list  bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'arr_driver_id_TEXT', true);
  l_tr_list      bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'arr_tr_id_TEXT', true);
  l_num_out_sh     text[] := jofl.jofl_pkg$extract_tarray(p_attr, 'arr_msg_part_TEXT', true);
  l_attr         text;
  l_result       text;
  l_call_count   smallint := 0;
  l_send_count   smallint := 0;
  l_error_count  smallint := 0;
  crow           record;
  l_data_type    text := jofl.jofl_pkg$extract_varchar(p_attr, 'DATA_TYPE', true);
  l_action_prod  smallint[];
  l_action_round smallint[];
  l_message_text text;
  l_msg_body     text;
  l_msg_head     text;
  l_msg_foot     text;
begin
  l_action_prod := ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$production_round(), false);
  l_action_round := ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$round(), false);

  /* добавляем общие параметры для всех сообщений */
  l_attr := (p_attr::jsonb ||
             jsonb_build_object('cmnd_ref_id', NPH_SED_DEVICE_TITLE_DATA_REQUEST,
                                'PARAM_MESSAGE_TEXT', 'NO_TEXT',
                                'F_MSG_TYPE_INPLACE_K','background',
                                'F_SHOW_TIME_NUMBER', 300)
                                )::text;
  l_attr := oud.jf_driver_calls_pkg$fill_message_attr(p_account_id, l_attr);

  l_call_count := cardinality(l_driver_list);
  for crow in select * from oud.jf_driver_calls_pkg$get_unit_list(l_out_list, l_driver_list, l_tr_list, l_num_out_sh) loop
   l_msg_head := null;
   l_msg_body := null;
   l_msg_foot := null;
   -- если сообщение одному водителю - обрабатываем ошибку
   if crow.unit_id is not null and not crow.order_err then
begin -- обработка ошибки
    /* в зависимости от данных, которые необходимо отправить - формируем текст сообщения */
    case l_data_type
     when 'NEXT_ROUND' then
      -- начало следующего рейса
      select 'Время начала рейса: '|| chr(10)  ||to_char(min(tta.start_action_msk) , 'DD.MM.YYYY hh24:MI:SS') as str
        into l_msg_body
        from ttb.tt_out tto
        left join (select a.*, lower(a.action_range) + interval '3 hours' as start_action_msk from ttb.tt_action a) tta on tta.tt_out_id = tto.tt_out_id
       where not tto.sign_deleted
         and not tta.sign_deleted
         and tto.parent_tt_out_id is not null
         and tto.tt_out_id = crow.out_id
         and tta.action_type_id = any (l_action_prod)
         and lower(tta.action_range) >= current_timestamp;
     when 'CHECKPOINT' then
      -- контрольные точки рейса
      with cp as (SELECT *
                  FROM oud.jf_driver_calls_pkg$checkpoint2round(crow.out_id) t
                  WHERE t.row_num <= 3)
      select lcp.*,
             (extract ('hours' from fcp.arrive_time - current_timestamp)*60 + extract ('minutes' from fcp.arrive_time - current_timestamp))::text as cp_interval
      from (SELECT array_to_string(array(SELECT rpad(substring(translate(t.stop_name, '"', '') from 1 for 14), 15, ' ') || to_char(t.arrive_time + interval '3 hours', 'HH24:MI')
                                         FROM cp t
                                         ORDER BY t.tt_out_id, t.row_num asc), chr(10))) lcp
           join cp fcp on 1 = 1 and
                          fcp.row_num = 1
      INTO l_msg_body, l_msg_head;
      l_msg_head := crow.number_out_shift || rpad('', 20-length(crow.number_out_shift || l_msg_head), ' ') || l_msg_head;
     when 'INTERVAL' then
      -- контрольные точки и интервал движения
      with cp as (SELECT *
                    FROM oud.jf_driver_calls_pkg$checkpoint2round(crow.out_id) t
                   WHERE t.row_num <= 2)
      select lcp.*,
             (extract ('hours' from fcp.arrive_time - current_timestamp)*60 + extract ('minutes' from fcp.arrive_time - current_timestamp))::text as cp_interval
      from (SELECT array_to_string(array(SELECT rpad(substring(translate(t.stop_name, '"', '') from 1 for 14), 15, ' ') || to_char(t.arrive_time + interval '3 hours', 'HH24:MI')
                                           FROM cp t
                                       ORDER BY t.tt_out_id, t.row_num asc), chr(10))) lcp
           join cp fcp on 1 = 1 and
                          fcp.row_num = 1
      INTO l_msg_body, l_msg_head;
      l_msg_head := crow.number_out_shift || rpad('', 20-length(crow.number_out_shift || l_msg_head), ' ') || l_msg_head;
      l_msg_foot := oud.jf_driver_calls_pkg$interval2round(crow.out_id);
     else
      raise exception '<<Неизвестный тип сообщения>>';
    end case;

    -- собираем полный текст сообшщения
    l_message_text := coalesce(l_msg_head || chr(10), '') || coalesce(l_msg_body || chr(10), '') || coalesce(l_msg_foot || chr(10), '');
    if coalesce(l_message_text, '') = '' then
     raise exception '<<Нет данных по оперативному расписанию для отправки на ТС>>';
    end if;

    /* добавляем параметры для команды блоку и отправляем сообщение */
    l_attr := (l_attr::jsonb ||
               jsonb_build_object('PARAM_MESSAGE_TEXT', l_message_text,
                                  'unit_id', crow.unit_id))::text;
    l_result := cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_sed_device_title_data(p_account_id, l_attr);
    l_send_count := l_send_count + 1;
exception
 when others then
   if l_call_count = 1
    then raise;
    else l_error_count := l_error_count + 1;
   end if;
end; -- обработка ошибки
   elsif l_call_count = 1 and crow.order_err then
     raise exception '<<Водитель в данный момент не работает на данном ТС. Нет активного наряда>>';
   elsif  l_call_count = 1 and crow.unit_id is null then
     raise exception '<<Не найден блок для связи с ТС>>';
   end if;
  end loop;

  if l_error_count > 0 then
     raise exception '<<Сообщения не были отправлены на % ТС>>', l_error_count;
  elsif l_send_count = 0 then
     raise exception '<<Сообщения не были отправлены>>';
  end if;

  return null;
end;
$$ language plpgsql volatile security invoker;


create or replace function oud.jf_driver_calls_pkg$of_next_round
  (p_account_id          in numeric,
   p_attr                in text)
returns text
-- ========================================================================
-- Отправка начала следующего рейса по оперативному расписанию
-- ========================================================================
as $$ declare
  l_attr text;
  l_result text;
begin
  l_attr := (p_attr::jsonb || jsonb_build_object('DATA_TYPE', 'NEXT_ROUND'))::text;
  l_result := oud.jf_driver_calls_pkg$send_ttb_data(p_account_id, l_attr);
  return l_result;
end;
$$ language plpgsql volatile security invoker;


create or replace function oud.jf_driver_calls_pkg$of_next_checkpoint
  (p_account_id          in numeric,
   p_attr                in text)
returns text
-- ========================================================================
-- Отправка следующих контрольных пунктов
-- ========================================================================
as $$ declare
  l_attr text;
  l_result text;
begin
  l_attr := (p_attr::jsonb || jsonb_build_object('DATA_TYPE', 'CHECKPOINT'))::text;
  l_result := oud.jf_driver_calls_pkg$send_ttb_data(p_account_id, l_attr);
  return l_result;
end;
$$ language plpgsql volatile security invoker;


create or replace function oud.jf_driver_calls_pkg$of_interval
  (p_account_id          in numeric,
   p_attr                in text)
returns text
-- ========================================================================
-- Отправка интервалов
-- ========================================================================
as $$ declare
  l_attr text;
  l_result text;
begin
  l_attr := (p_attr::jsonb || jsonb_build_object('DATA_TYPE', 'INTERVAL'))::text;
  l_result := oud.jf_driver_calls_pkg$send_ttb_data(p_account_id, l_attr);
  return l_result;
end;
$$ language plpgsql volatile security invoker;
