create or replace function  oud.jf_traffic_control_mode_pkg$of_rows(p_id_account in numeric, out p_rows refcursor, p_attr text)
  returns refcursor  as $$  declare
begin   open p_rows for
  select
  traffic_control_mode_id,
    traffic_control_mode_name,
    traffic_control_mode_comment
  from oud.traffic_control_mode;
end;
$$ LANGUAGE plpgsql;