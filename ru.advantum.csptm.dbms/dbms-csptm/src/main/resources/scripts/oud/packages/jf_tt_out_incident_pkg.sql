create or replace function oud.jf_tt_out_incident_pkg$of_rows(
  p_account_id in  numeric,
  p_attr       in  text,
  p_rows       out refcursor
)
  returns refcursor
language plpgsql
as $$
declare
  l_tt_out_id ttb.tt_out.tt_out_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tt_out_id', true);
  l_route_num text;
begin
  l_route_num := ttb.get_route_num_via_tt_out_id(l_tt_out_id);
  open p_rows for
    select incident.incident_id as incident_id
         ,incident_type.incident_type_name as incident_type_name
         ,tr.garage_num as garage_num
         ,incident.time_start as time_start
         ,incident.time_finish as time_finish
         ,ttb.jf_incident_reason_pkg$get_name((incident.incident_params ->> 'incidentReason')::int) incident_reason_name
         ,l_route_num route_num
  from ttb.incident
       join ttb.tt_out on incident.tr_id = tt_out.tr_id and
                             incident.time_start between
                             (lower(tt_out.action_range))
                             and
                             (upper(tt_out.action_range))
                             and incident.incident_type_id <> ttb.jf_incident_pkg$cn_red_button()
        join ttb.incident_type on incident_type.incident_type_id = incident.incident_type_id
        join core.tr on tr.tr_id = incident.tr_id
  where tt_out.tt_out_id = l_tt_out_id;
end;
$$;

CREATE OR REPLACE FUNCTION oud.jf_tt_out_incident_pkg$of_update(
  p_id_account numeric,
  p_attr text)
  RETURNS text
language plpgsql
AS
$$
declare
  l_incident_id ttb.incident.incident_id%type := jofl.jofl_pkg$extract_number(p_attr, 'incident_id', true);
  l_time_start ttb.incident.time_start%type := jofl.jofl_pkg$extract_date(p_attr, 'time_start', true);
  l_time_finish ttb.incident.time_finish%type := jofl.jofl_pkg$extract_date(p_attr, 'time_finish', true);

  l_tr_id              core.tr.tr_id%type;
  l_tt_action_id_array int[];
  l_incident_reason_id int ;
  l_stop_item_id int;
  l_first_action_time_begin timestamp;
  l_first_action_id int;
  l_tt_variant_id int;
begin

  if l_time_start > l_time_finish then
    raise exception 'Время начала д.б. меньше времени окончания';
  end if ;

  select
    i.tr_id,
    i.incident_params ->> 'incidentReason',
    oud.jf_tt_out_incident_pkg$jsonb_array_castint(i.incident_params -> 'ttActionList')
  from ttb.incident i
  where
    time_start between l_time_start::date and l_time_start::date + interval '1 day'
    and i.incident_id = l_incident_id
  into l_tr_id, l_incident_reason_id, l_tt_action_id_array;

  --Найдем остановку для рейса по инциденту
    select stop_item2round.stop_item_id
    into l_stop_item_id
    from ttb.tt_action
      join ttb.tt_action_item on tt_action.tt_action_id = ttb.tt_action_item.tt_action_id
      join rts.stop_item2round on stop_item2round.stop_item2round_id = tt_action_item.stop_item2round_id
    where parent_tt_action_id = any(l_tt_action_id_array)
        and tt_action.sign_deleted
        and order_num = 1
    order by time_begin limit 1;

   --Найдем время начала рейса с началом в той-же остановке, но после окончания инцидента
   select tt_action.dt_action, tt_action.tt_action_id
   into l_first_action_time_begin
   from ttb.tt_action
      join ttb.tt_action_item on tt_action.tt_action_id = ttb.tt_action_item.tt_action_id
      join rts.stop_item2round on stop_item2round.stop_item2round_id = tt_action_item.stop_item2round_id
    where parent_tt_action_id = any(l_tt_action_id_array)
        and tt_action.sign_deleted
        and stop_item2round.stop_item_id = l_stop_item_id
        and order_num = 1
        and tt_action.dt_action > l_time_finish
    order by time_begin limit 1;


  update ttb.tt_action
  set dt_action = l_time_start
  where tt_action_id = any(l_tt_action_id_array);

  update ttb.tt_action_item
  set time_begin = l_time_start
        ,time_end = l_time_finish
  where tt_action_id = any(l_tt_action_id_array);

  insert into ttb.tt_action_fact
  (
  tt_action_id
  ,stop_item2round_id
  ,time_fact_begin
  ,time_fact_end
  )
  select tt_action_id
        ,stop_item2round_id
        ,l_time_start
        ,l_time_finish
  from ttb.tt_action_item
  where tt_action_id = any(l_tt_action_id_array);

  select tt_variant_id
  into l_tt_variant_id
  from ttb.tt_out join ttb.tt_action on tt_out.tt_out_id = tt_action.tt_out_id
  where tt_action.tt_action_id = any(l_tt_action_id_array);

  perform ttb.tt_oper_pkg$update_dt_action(l_tt_variant_id);

  update ttb.tt_action
  set sign_deleted = false
  where parent_tt_action_id = any(l_tt_action_id_array)
        and tt_action.sign_deleted
        and dt_action > l_first_action_time_begin;


  perform ttb.tt_oper_pkg$update_dt_action(l_tt_variant_id);

--  perform oud.jf_tt_out_fact_pkg$add_tr_incident(l_tr_id, l_time_start, l_time_finish, l_incident_reason_id);



     update ttb.incident
      set time_finish = l_time_finish
      where  incident_id = l_incident_id
             and time_start between date_trunc('minute', l_time_start) and date_trunc('minute', l_time_start) + interval '1 minute';
  return null;
end;
$$;

CREATE OR REPLACE FUNCTION oud.jf_tt_out_incident_pkg$jsonb_array_castint(jsonb) RETURNS int[] AS $f$
    SELECT array_agg(x)::int[] || ARRAY[]::int[] FROM jsonb_array_elements_text($1) t(x);
$f$ LANGUAGE sql IMMUTABLE;