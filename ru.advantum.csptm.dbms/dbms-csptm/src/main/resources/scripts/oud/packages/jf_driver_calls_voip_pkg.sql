﻿
create or replace function oud.jf_driver_calls_voip_pkg$of_rows
  (p_account_id               in numeric,
   p_attr                     in text,
   p_rows                     out refcursor)
returns refcursor
as $$ declare 
   l_query_date       timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'order_date', true), now());
   l_order_date       timestamp := date_trunc('day', l_query_date);
   l_driver_id        integer   := jofl.jofl_pkg$extract_number(p_attr, 'f_driver_id', true);
begin
   open p_rows for
   select voc.voice_call_begin  as call_begin,
          voc.voice_call_end    as call_end,
          to_char(voc.voice_call_end - voc.voice_call_begin, 'MI:SS')::text  as call_duration,
          voc.voice_call_file   as audio_file,
          case when voc.voice_call_file is not null then jofl.jofl_link_util$build(voc.voice_call_file, 'cdr/' || voc.voice_call_file, voc.voice_call_file) else '' end as audio_file_link
     from voip.voice_call voc
     join voip.voice_call2unit vcu on vcu.voice_call_id = voc.voice_call_id
    where vcu.driver_id = l_driver_id
      and voc.voice_call_date between l_order_date and l_order_date + interval '1 day'
    order by voc.voice_call_begin;
end;
$$ language plpgsql volatile security invoker;
