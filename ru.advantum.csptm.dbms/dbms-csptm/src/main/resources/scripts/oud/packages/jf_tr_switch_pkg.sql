create or replace function oud.jf_tr_switch_pkg$of_rows(
  p_account_id numeric,
  p_attr       text,
  p_rows out   refcursor
)
  returns refcursor
language plpgsql
as $$
declare
  l_tr_selection_all constant          integer := 0;
  l_tr_selection_depos_routes constant integer := 1;
  l_tr_selection_routes constant       integer := 2;

  l_f_from_tr_selection                integer := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_from_tr_selection_INPLACE_K', true), l_tr_selection_routes);
  l_f_from_depo_id                     core.depo.depo_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_from_depo_id', true);
  l_f_from_route_id                    rts.route.route_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_from_route_id', true);

  l_f_to_route_id                      rts.route.route_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_to_route_id', true);
  l_f_to_tt_out_id                     ttb.tt_out.tt_out_id%type :=  jofl.jofl_pkg$extract_number(p_attr, 'f_to_tt_out_id', true);
  l_f_to_stop_item_id                  rts.stop_item.stop_item_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_to_stop_item_id', true);
begin
  open p_rows for
  select
    r.route_num,
    de.name_short depo_name,
    tr.garage_num tr_garage_num,
    trt.name      tr_type_name,
    tt_out.tt_out_num
  from ttb.tt_variant
    join ttb.tt_out on tt_variant.tt_variant_id = tt_out.tt_variant_id
    join rts.route_variant rv on tt_variant.route_variant_id = rv.route_variant_id
    join rts.route r on rv.route_id = r.route_id
    join core.tr tr on tt_out.tr_id = tr.tr_id
    join core.entity de on tr.depo_id = de.entity_id
    join core.tr_type trt on tr.tr_type_id = trt.tr_type_id
  where tt_variant.order_date = current_date
        and exists(select 1
                   from ttb.tt_action
                   where tt_out.tt_out_id = tt_action.tt_out_id
                         and current_timestamp <@ tt_action.sys_period
                         and (l_f_from_depo_id is null or tr.depo_id = l_f_from_depo_id))
        and (l_f_from_route_id is null or rv.route_id = l_f_from_route_id)
        and (
          (l_f_from_tr_selection = l_tr_selection_all)
          or (
            l_f_from_tr_selection = l_tr_selection_routes
            and rv.route_id in (select adm.account_data_realm_pkg$get_routes(p_account_id))
          )
          or (
            l_f_from_tr_selection = l_tr_selection_depos_routes
            and (
              rv.route_id in (select adm.account_data_realm_pkg$get_routes(p_account_id))
              or tr.depo_id in (select adm.account_data_realm_pkg$get_depos(p_account_id))
            )
          )
        )
        and (l_f_to_route_id is null or r.route_id <> l_f_to_route_id)
                         and (l_f_to_tt_out_id is null or tt_out.mode_id in (select tto.mode_id
                                                                             from ttb.tt_out tto
                                                         where tto.tt_out_id = l_f_to_tt_out_id));
end;
$$;