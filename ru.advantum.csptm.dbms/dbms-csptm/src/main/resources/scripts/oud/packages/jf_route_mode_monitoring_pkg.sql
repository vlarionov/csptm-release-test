create or replace function oud.jf_route_mode_monitoring_pkg$attr_to_rowtype (p_attr text) RETURNS oud.route_mode_monitoring
	LANGUAGE plpgsql
AS $$

declare
   l_r oud.route_mode_monitoring%rowtype;
begin
   l_r.dt_fact := jofl.jofl_pkg$extract_varchar(p_attr, 'dt_fact', true);
   l_r.tt_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
   l_r.cnt_plan_tr := jofl.jofl_pkg$extract_number(p_attr, 'cnt_plan_tr', true);
   l_r.cnt_fact_tr := jofl.jofl_pkg$extract_number(p_attr, 'cnt_fact_tr', true);
   l_r.interval_plan := jofl.jofl_pkg$extract_number(p_attr, 'interval_plan', true);
   l_r.interval_fact := jofl.jofl_pkg$extract_number(p_attr, 'interval_fact', true);
   l_r.time_round_plan := jofl.jofl_pkg$extract_number(p_attr, 'time_round_plan', true);
   l_r.time_round_fact := jofl.jofl_pkg$extract_number(p_attr, 'time_round_fact', true);
   l_r.pct_tr_qty := jofl.jofl_pkg$extract_varchar(p_attr, 'pct_tr_qty', true);
   l_r.pct_interval := jofl.jofl_pkg$extract_varchar(p_attr, 'pct_interval', true);
   l_r.pct_route := jofl.jofl_pkg$extract_varchar(p_attr, 'pct_route', true);
   l_r.time_fact := jofl.jofl_pkg$extract_date(p_attr, 'time_fact', true);



   return l_r;
end;
$$;

create or replace function oud.jf_route_mode_monitoring_pkg$of_delete (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r oud.route_mode_monitoring%rowtype;
begin
   l_r := oud.jf_route_mode_monitoring_pkg$attr_to_rowtype(p_attr);

   return null;
end;
$$;


create or replace function oud.jf_route_mode_monitoring_pkg$of_insert (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r oud.route_mode_monitoring%rowtype;
begin
   l_r := oud.jf_route_mode_monitoring_pkg$attr_to_rowtype(p_attr);

   insert into oud.route_mode_monitoring select l_r.*;

   return null;
end;
$$;

create or replace function oud.jf_route_mode_monitoring_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$

declare
    l_date timestamp := jofl.jofl_pkg$extract_date(p_attr, 'f_order_date_DT', true);
    l_route_id int := jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', true);
begin
 open p_rows for
    with a as (select
      route_mode_monitoring.dt_fact,
      route_mode_monitoring.tt_variant_id,
      route_mode_monitoring.cnt_plan_tr,
      route_mode_monitoring.cnt_fact_tr,
      route_mode_monitoring.interval_plan,
      route_mode_monitoring.interval_fact,
      route_mode_monitoring.time_round_plan,
      route_mode_monitoring.time_round_fact,
      route_mode_monitoring.pct_tr_qty,
      route_mode_monitoring.pct_interval,
      route_mode_monitoring.pct_route,
      route_mode_monitoring.time_fact,
      route.route_num,
      oud.jf_route_mode_monitoring_pkg$get_rel(p_plan := route_mode_monitoring.interval_plan
                                                , p_fact := route_mode_monitoring.interval_fact) rel_interval,

      oud.jf_route_mode_monitoring_pkg$get_rel(p_plan := route_mode_monitoring.time_round_plan
                                                , p_fact := route_mode_monitoring.time_round_fact) rel_route,

      oud.jf_route_mode_monitoring_pkg$get_rel(p_plan := route_mode_monitoring.cnt_plan_tr
                                                , p_fact := route_mode_monitoring.cnt_fact_tr) rel_tr_cnt

    from oud.route_mode_monitoring
        join ttb.tt_variant on route_mode_monitoring.tt_variant_id = tt_variant.tt_variant_id
        join rts.route_variant on route_variant.route_variant_id = tt_variant.route_variant_id
        join rts.route on route.route_id = route_variant.route_id
    where route.route_id  = l_route_id
          and route_mode_monitoring.dt_fact = l_date::date
    )
    select a.*
    , (rel_interval * pct_interval
        + rel_route * pct_route
        + rel_tr_cnt * pct_tr_qty) total_rating
    from a;
end;
$$;

create or replace function oud.jf_route_mode_monitoring_pkg$of_update (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r oud.route_mode_monitoring%rowtype;
begin
   l_r := oud.jf_route_mode_monitoring_pkg$attr_to_rowtype(p_attr);

   return null;
end;
$$;

create or replace function oud.jf_route_mode_monitoring_pkg$crone () RETURNS void
	LANGUAGE plpgsql
AS $$
begin
    insert into oud.route_mode_monitoring
    (dt_fact
      , tt_variant_id
      , cnt_plan_tr
      , cnt_fact_tr
      , interval_plan
      , interval_fact
      , time_round_plan
      , time_round_fact
      ,pct_tr_qty
      ,pct_interval
      ,pct_route
      ,time_fact
    )
    select now()::DATE
    ,a.tt_variant_id
    ,tr_plan_online
    ,tr_fact_online
    ,plan_diff_a
    ,fact_diff_a
    ,oud.jf_route_mode_monitoring_pkg$get_duration_abba_plan(tt_variant.parent_tt_variant_id)
    ,oud.jf_route_mode_monitoring_pkg$get_duration_abba_fact(tt_variant.tt_variant_id)
    ,pct_tr_qty
    ,pct_interval
    ,pct_route
    ,now()
    from mnt.jf_disp_main_window_route_pkg$get_rows(
        p_time := now()::TIMESTAMP
      ,p_interval := interval '61 min'
    ) a join ttb.tt_variant on tt_variant.tt_variant_id = a.tt_variant_id
      join oud.tt_variant_incident_check_param
        on tt_variant.parent_tt_variant_id = tt_variant_incident_check_param.tt_variant_id;
end;
$$;

create or replace function oud.jf_route_mode_monitoring_pkg$get_rel(p_plan int, p_fact int) RETURNS float
	LANGUAGE plpgsql
AS $$
begin
    if p_plan = 0 then
        return null;
    else
        return round((p_fact::float/p_plan::float)::numeric, 2);
    end if;
end;
$$ IMMUTABLE;





create or replace function oud.jf_route_mode_monitoring_pkg$get_duration_abba_plan (p_tt_variant_id int) RETURNS int
	LANGUAGE plpgsql
AS $$
declare
    l_duration int;
begin

/*
    средняя длительность последних ав+ва
*/

with abs as (
select tt_action.action_dur
  ,row_number() over (partition by tt_out.tt_out_id order by dt_action desc) nn
  ,tt_out.tt_out_id
from ttb.tt_out
join ttb.tt_action on tt_out.tt_out_id = tt_action.tt_out_id
  and not tt_out.sign_deleted
  and not tt_action.sign_deleted
  where tt_out.tt_variant_id = p_tt_variant_id
  and action_type_id in (ttb.action_type_pkg$main_round_ab(), ttb.action_type_pkg$main_round_ba())
  and dt_action < now()::timestamp
)
,last_abs as (
  select sum(action_dur) out_duration, tt_out_id
  from abs
  where nn < 3
  group by tt_out_id
)
select
  round(avg(out_duration)/60)
into l_duration
from last_abs a;

return l_duration;
end;
$$;

create or replace function oud.jf_route_mode_monitoring_pkg$get_duration_abba_fact (p_tt_variant_id int) RETURNS int
	LANGUAGE plpgsql
AS $$
declare
    l_duration int;
begin
with abs as (
    select time_packet_end - time_packet_begin duration
      , time_packet_begin
      , action_type_id
      ,row_number() over (partition by tt_out.tt_out_id order by dt_action desc) nn
      ,tt_out.tt_out_id
    FROM ttb.tt_out
      JOIN ttb.tt_action ON tt_out.tt_out_id = tt_action.tt_out_id
                            AND NOT tt_out.sign_deleted
                            AND NOT tt_action.sign_deleted
      JOIN ttb.tt_action_round ON tt_action_round.tt_action_id = tt_action.tt_action_id
    WHERE tt_out.tt_variant_id = p_tt_variant_id
          AND time_packet_end < now()::TIMESTAMP
          AND action_type_id IN (ttb.action_type_pkg$main_round_ab(), ttb.action_type_pkg$main_round_ba())
)
,last_abs as (
select sum(  EXTRACT (EPOCH FROM duration))::int/60 out_duration, tt_out_id
from abs
where nn < 3
group by tt_out_id
)
select
  round(avg(out_duration))
into l_duration
from last_abs a;
return l_duration;
end;
$$;