﻿
create or replace function oud.jf_driver_calls_msg_pkg$of_rows
  (p_account_id               in numeric,
   p_attr                     in text,
   p_rows                     out refcursor)
returns refcursor
as $$ declare 
   l_query_date       timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'order_date', true), now());
   l_order_date       timestamp := date_trunc('day', l_query_date);
   l_tr_id            bigint    := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_id', true);
   NPH_SED_DEVICE_TITLE_DATA_REQUEST integer := 19;
begin
   open p_rows for
   select request_time, cmnd_status_name, message_text, time_display,
          case when type_display = 'background' then 'фоновое' else 'диалоговое' end as type_display
     from (select req.request_time,
                  sta.cmnd_status_name,
                  substring(req.message from 'type="(\w+)"')  as type_display,
                  substring(req.message from '<h2>(.*)</h2>') as message_text,
                  substring(req.message from 'time=(\d+)')    as time_display
             from (select r.cmnd_status_id, r.request_time,
                          r.request_param::json ->> 'MESSAGE' as message
                     from cmnd.cmnd_request r
                     join core.unit2tr u on u.unit_id = r.unit_id
                    where r.cmnd_ref_id = NPH_SED_DEVICE_TITLE_DATA_REQUEST
                      and u.tr_id = l_tr_id
                      and r.request_time between l_order_date and l_order_date + interval '1 day'
                  ) req
             join cmnd.cmnd_status sta on sta.cmnd_status_id = req.cmnd_status_id
          ) q
    order by request_time desc;
end;
$$ language plpgsql volatile security invoker;
