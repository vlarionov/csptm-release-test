create or replace function oud.stat_collector_pkg$get_tr_properties()
    returns table(
        tr_id          core.tr.tr_id%type,
        tr_type_id     core.tr_type.tr_type_id%type,
        tr_capacity_id core.tr_capacity.tr_capacity_id%type
    ) as
$$
begin
    return query
    select
        t.tr_id,
        t.tr_type_id :: smallint,
        tm.tr_capacity_id :: smallint
    from core.tr t
        join core.tr_model tm on t.tr_model_id = tm.tr_model_id;
end;
$$
language 'plpgsql'
volatile
called on null input
security invoker;

create or replace function oud.stat_collector_pkg$update_norm(
    p_stop_place_1_muid gis.stop_places.muid%type,
    p_stop_place_2_muid gis.stop_places.muid%type,
    p_tr_capacity_id    core.tr_capacity.tr_capacity_id%type,
    p_tr_type_id        core.tr_type.tr_type_id%type,
    p_between_stop_dur  oud.oper_between_stop.between_stop_dur%type
)
    returns void as
$$
begin
    insert into oud.oper_between_stop (stop_place_1_muid, stop_place_2_muid, tr_capacity_id, tr_type_id, between_stop_dur, update_date)
    values (p_stop_place_1_muid, p_stop_place_2_muid, p_tr_capacity_id, p_tr_type_id, p_between_stop_dur, now())
    on conflict on constraint pk_oper_between_stop
        do update set
            between_stop_dur = excluded.between_stop_dur,
            update_date      = excluded.update_date;
end;
$$
language 'plpgsql'
volatile
called on null input
security invoker;

create or replace function oud.stat_collector_pkg$update_norm_from_json(
    p_norms json
)
    returns void as
$$
begin
    perform oud.stat_collector_pkg$update_norm((e ->> 'stopPlace1Muid') :: bigint,
                                               (e ->> 'stopPlace2Muid') :: bigint,
                                               (e ->> 'trCapacityId') :: smallint,
                                               (e ->> 'trTypeId') :: smallint,
                                               (e ->> 'betweenStopDuration') :: smallint)
    from json_array_elements(p_norms) e;

end;
$$
language 'plpgsql'
volatile
called on null input
security invoker;

