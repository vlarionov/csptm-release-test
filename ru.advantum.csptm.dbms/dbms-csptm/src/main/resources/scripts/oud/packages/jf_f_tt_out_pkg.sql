create or replace function oud.jf_f_tt_out_pkg$of_rows(p_id_account numeric, out p_rows refcursor, p_attr text)
    returns refcursor
language plpgsql
as $BODY$
declare
    l_tt_variant_id int := jofl.jofl_pkg$extract_number(p_attr, 'tt_variant_id', true);
begin
    open p_rows for
        select tt_out_id, tt_out_num
        from ttb.tt_out
        where not sign_deleted
        and tt_variant_id = l_tt_variant_id;
end;
$BODY$;