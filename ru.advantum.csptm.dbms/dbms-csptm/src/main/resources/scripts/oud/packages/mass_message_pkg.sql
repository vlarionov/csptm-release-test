﻿--
-- Отправка интервалов на все ТС в производственных рейсах
--
create or replace function oud.mass_message_pkg$send_interval
(p_account_id          in numeric,
 p_attr                in text) returns text as
$$
declare
	l_showtime            smallint := 300;
	l_attr                text := null;
	l_action_prod         smallint[];
	l_current_out         record;
	l_param_dt            timestamp;
  l_out_arr             text;
  l_driver_arr          text;
  l_tr_arr              text;
  l_msg_arr             text;
  l_stream_cnt          smallint :=  jofl.jofl_pkg$extract_number(p_attr, 'stream_cnt', true);
  l_stream_num          smallint :=  jofl.jofl_pkg$extract_number(p_attr, 'stream_num', true);
begin
 l_stream_cnt := coalesce(l_stream_cnt, 1);
 l_stream_num := coalesce(l_stream_num, 1);
 l_param_dt := current_timestamp::date;
 l_action_prod := ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$production_round(), false);
 for l_current_out in select distinct
                             '"'||r.route_num || ' ' || tto.tt_out_num::text || ' ' || dsh.dr_shift_num::text||'"' as num_out_sh,
                             tto.tt_out_id,
                             tta.driver_id,
                             tto.tr_id
                      from ttb.tt_variant ttv
                      join ttb.tt_out tto on tto.tt_variant_id = ttv.tt_variant_id
                      join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
                      join ttb.tt_action_item ttai on ttai.tt_action_id = tta.tt_action_id
                      join ttb.tt_action_round ttar on ttar.tt_action_id = tta.tt_action_id and
                                                       ttar.round_status_id = ttb.round_status_pkg$in_progress()
                      join core.tr tr on tr.tr_id = tto.tr_id
                      join rts.route_variant rv on rv.route_variant_id = ttv.route_variant_id
                      join rts.route r on r.route_id = rv.route_id
                      join ttb.dr_shift dsh on dsh.dr_shift_id = ttai.dr_shift_id
                      where not ttv.sign_deleted and
                            not tto.sign_deleted and
                            not tta.sign_deleted and
                            (tto.tt_out_id % l_stream_cnt) + 1 = l_stream_num and
                            ttv.parent_tt_variant_id is not null and
                            ttv.order_date in (l_param_dt, l_param_dt - interval '1 days') and
                            tta.action_type_id = any (l_action_prod)
 loop
  l_out_arr    := coalesce(l_out_arr || ',', '') || l_current_out.tt_out_id;
  l_driver_arr := coalesce(l_driver_arr || ',', '') || l_current_out.driver_id;
  l_tr_arr     := coalesce(l_tr_arr || ',', '') || l_current_out.tr_id;
  l_msg_arr    := coalesce(l_msg_arr || ',', '') || l_current_out.num_out_sh;
 end loop;

 l_out_arr    := '$array['||l_out_arr||']';
 l_driver_arr := '$array['||l_driver_arr||']';
 l_tr_arr     := '$array['||l_tr_arr||']';
 l_msg_arr    := '$array['||l_msg_arr||']';

 l_attr := jsonb_build_object('F_SHOW_TIME_NUMBER', l_showtime,
                              'arr_out_id_TEXT', l_out_arr,
                              'arr_driver_id_TEXT', l_driver_arr,
                              'arr_tr_id_TEXT', l_tr_arr,
                              'arr_msg_part_TEXT', l_msg_arr)::text;
  perform oud.jf_driver_calls_pkg$of_interval(p_account_id, l_attr);
 return null;
end;
$$
language 'plpgsql';

/* один джоб */
--insert into cron.job(jobid, schedule, command, nodename, nodeport, database, username)
--  values (nextval('cron.jobid_seq'),  '*/2 * * * *', 'select oud.mass_message_pkg$send_interval(null::numeric, ''{}'')', 'localhost', 5432, 'csptm-dev', 'adv');

/* 10 джобов */
/*
параметры:
stream_cnt - количество джобов
stream_num - номер текущего джоба от 1 до stream_cnt
*/
--insert into cron.job(jobid, schedule, command, nodename, nodeport, database, username)
--  values (nextval('cron.jobid_seq'),  '*/2 * * * *', 'select oud.mass_message_pkg$send_interval(null::numeric, ''{"stream_cnt" : 10, "stream_num" : 1}'')', 'localhost', 5432, 'csptm-dev', 'adv');
--insert into cron.job(jobid, schedule, command, nodename, nodeport, database, username)
--  values (nextval('cron.jobid_seq'),  '*/2 * * * *', 'select oud.mass_message_pkg$send_interval(null::numeric, ''{"stream_cnt" : 10, "stream_num" : 2}'')', 'localhost', 5432, 'csptm-dev', 'adv');
--insert into cron.job(jobid, schedule, command, nodename, nodeport, database, username)
--  values (nextval('cron.jobid_seq'),  '*/2 * * * *', 'select oud.mass_message_pkg$send_interval(null::numeric, ''{"stream_cnt" : 10, "stream_num" : 3}'')', 'localhost', 5432, 'csptm-dev', 'adv');
--insert into cron.job(jobid, schedule, command, nodename, nodeport, database, username)
--  values (nextval('cron.jobid_seq'),  '*/2 * * * *', 'select oud.mass_message_pkg$send_interval(null::numeric, ''{"stream_cnt" : 10, "stream_num" : 4}'')', 'localhost', 5432, 'csptm-dev', 'adv');
--insert into cron.job(jobid, schedule, command, nodename, nodeport, database, username)
--  values (nextval('cron.jobid_seq'),  '*/2 * * * *', 'select oud.mass_message_pkg$send_interval(null::numeric, ''{"stream_cnt" : 10, "stream_num" : 5}'')', 'localhost', 5432, 'csptm-dev', 'adv');
--insert into cron.job(jobid, schedule, command, nodename, nodeport, database, username)
--  values (nextval('cron.jobid_seq'),  '*/2 * * * *', 'select oud.mass_message_pkg$send_interval(null::numeric, ''{"stream_cnt" : 10, "stream_num" : 6}'')', 'localhost', 5432, 'csptm-dev', 'adv');
--insert into cron.job(jobid, schedule, command, nodename, nodeport, database, username)
--  values (nextval('cron.jobid_seq'),  '*/2 * * * *', 'select oud.mass_message_pkg$send_interval(null::numeric, ''{"stream_cnt" : 10, "stream_num" : 7}'')', 'localhost', 5432, 'csptm-dev', 'adv');
--insert into cron.job(jobid, schedule, command, nodename, nodeport, database, username)
--  values (nextval('cron.jobid_seq'),  '*/2 * * * *', 'select oud.mass_message_pkg$send_interval(null::numeric, ''{"stream_cnt" : 10, "stream_num" : 8}'')', 'localhost', 5432, 'csptm-dev', 'adv');
--insert into cron.job(jobid, schedule, command, nodename, nodeport, database, username)
--  values (nextval('cron.jobid_seq'),  '*/2 * * * *', 'select oud.mass_message_pkg$send_interval(null::numeric, ''{"stream_cnt" : 10, "stream_num" : 9}'')', 'localhost', 5432, 'csptm-dev', 'adv');
--insert into cron.job(jobid, schedule, command, nodename, nodeport, database, username)
--  values (nextval('cron.jobid_seq'),  '*/2 * * * *', 'select oud.mass_message_pkg$send_interval(null::numeric, ''{"stream_cnt" : 10, "stream_num" : 10}'')', 'localhost', 5432, 'csptm-dev', 'adv');
