create or replace function oud.jf_tr_switch_f_tt_out_pkg$of_rows(
  p_account_id numeric,
  p_attr       text,
  p_rows out   refcursor
)
  returns refcursor
language plpgsql
as $$
declare
  l_f_to_route_id rts.route.route_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_to_route_id', true);
begin
  open p_rows for
  select
    tto.tt_out_id,
    tto.tt_out_num,
    trc.short_name tr_capacity,
    tr.garage_num tr_garage_num,
    de.name_short depo_name
  from ttb.order_list ol
    join rts.route_variant rv on ol.route_variant_id = rv.route_variant_id
    join ttb.order_list2out ol2o on ol.order_list_id = ol2o.order_list_id
    join ttb.tt_out tto on ol2o.tt_out_id = tto.tt_out_id
    join core.tr_capacity trc on tto.tr_capacity_id = trc.tr_capacity_id
    join core.tr tr on ol.tr_id = tr.tr_id
    join core.depo d on ol.depo_id = d.depo_id
    join core.entity de on d.depo_id = de.entity_id
  where ol.order_date = current_date
        and rv.route_id = l_f_to_route_id
        and not tto.sign_deleted
        and tto.sys_period @> current_timestamp
  order by tto.tt_out_num;
end;
$$;


