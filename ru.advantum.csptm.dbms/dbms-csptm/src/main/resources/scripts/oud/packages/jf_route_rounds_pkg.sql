create or replace function oud.jf_route_rounds_pkg$of_rows(
  in  p_id_account numeric,
  out p_rows       refcursor,
  in  p_attr       text)
  returns refcursor as
$$
declare
  l_route_variant_id rts.round.route_variant_id%type;
  l_route_id         rts.route.route_id%type;
  l_tr_type_id       smallint [];
  l_arr_round_prod   smallint [];
begin
  l_arr_round_prod := ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$production_round(), false);

  l_route_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'route_variant_id', true);
  l_route_id := jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', true);
  l_tr_type_id := jofl.jofl_pkg$extract_narray(p_attr, 'f_tr_type_id', true);

 open p_rows for
  with w_stop_list as (select s.stop_id,
                              sl.stop_location_id,
                              si.stop_item_id,
                              s.name
                         from rts.stop s
                         join rts.stop_location sl on sl.stop_id = s.stop_id
                         join rts.stop_item si on si.stop_location_id = sl.stop_location_id)
  select distinct
         rt.route_num ||' ('|| to_char(lower(rv.action_period),'dd.mm.yyyy') || '-'||coalesce(to_char(upper(rv.action_period),'dd.mm.yyyy'),'нвр')||')' as rv_name,
         to_char(lower(ttv.action_period), 'DD.MM.YYYY') as ttv_start,
         to_char(upper(ttv.action_period), 'DD.MM.YYYY') as ttv_end,
         rt.route_num as route_num,
         rd.round_id as round_id,
         rd.code as round_code,
         rv.route_variant_id as route_variant_id,
         act.action_type_name as round_type,
         first_value(sl.name) over (partition by si2r.round_id order by si2r.order_num rows between UNBOUNDED PRECEDING and UNBOUNDED FOLLOWING) as start_point_round,
         last_value(sl.name) over (partition by si2r.round_id order by si2r.order_num rows between UNBOUNDED PRECEDING and UNBOUNDED FOLLOWING) as end_point_round,
         '[' || json_build_object('CAPTION', 'На карте',
                                  'LINK', json_build_object('URL', '/app.html?m=modules/RouteOnMapCtrl&attr={"tr_type":"' || trt.short_name ||
                                                                   '","route_number":"' || rt.route_num || '","' ||
                                                                   ttb.action_type_pkg$get_url_round_type(ttb.action_type_pkg$production_round()) || '":"' || rd.code ||
                                                                   '","route_direction":"'|| rd.move_direction_id||
                                                                   '","rv_muid":"' || rv.route_variant_id || '"}' )) :: text || ']' as "JUMP_TO_MAP",
       trt.name as tr_type
from ttb.tt_variant ttv
     join rts.route_variant rv on rv.route_variant_id = ttv.route_variant_id
     join rts.route rt on rt.route_id = rv.route_id
     join core.tr_type trt on trt.tr_type_id = rt.tr_type_id
     join rts.round rd on rd.route_variant_id = rv.route_variant_id
     join ttb.action_type act on act.action_type_id = rd.action_type_id
     join rts.stop_item2round si2r on si2r.round_id = rd.round_id
     join w_stop_list sl on sl.stop_item_id = si2r.stop_item_id
where not ttv.sign_deleted and
      (ttv.action_period @> current_timestamp::timestamp or current_timestamp::timestamp < lower(ttv.action_period)) and
      ttv.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id() and
      not rd.sign_deleted and
      not si2r.sign_deleted
      -- rd.action_type_id = any(l_arr_round_prod)
      /* фильтры */
      and (array_length(l_tr_type_id, 1) is null or rt.tr_type_id = any (l_tr_type_id))
      and (l_route_id is null or rt.route_id = l_route_id)
order by rt.route_num, rd.round_id, act.action_type_name;

end;
$$
language plpgsql;
