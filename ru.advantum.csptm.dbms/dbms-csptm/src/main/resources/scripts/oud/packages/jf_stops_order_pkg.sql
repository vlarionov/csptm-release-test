create or replace function oud.jf_stops_order_pkg$of_rows
  (p_id_account in  numeric,
   p_attr       in  text,
   p_rows       out refcursor)
  returns refcursor
as $$ declare
  l_round_id         integer := jofl.jofl_pkg$extract_number(p_attr, 'round_id', true) :: integer;
begin
  open p_rows for
  select
    si2r.stop_item2round_id,
    si2r.order_num,
    si2r.stop_item_id,
    st.stop_id,
    st.name,
    md.move_direction_name,
    si2r.comment,
    '[' || json_build_object('CAPTION', st.name, 'JUMP', json_build_object('METHOD', 'rts.stop', 'ATTR', '{}')) || ']' as link_to_map_stops,
    case
     when si2rdt.stop_item2round_type_id is not null and not si2rdt.sign_deleted then true
     else false
    end as is_checkpoint,
    si2rdt.stop_item2round_type_id as cp_stop_item2round_type_id,
    sitr.stop_item_type_id as cp_stop_item_type_id
  from rts.stop_item2round si2r
    join rts.stop_item si on si.stop_item_id = si2r.stop_item_id
    join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
    join rts.stop st on st.stop_id = sl.stop_id
    join rts.round r on si2r.round_id = r.round_id
    join rts.move_direction md on r.move_direction_id = md.move_direction_id
    left join (select si2rt.stop_item2round_type_id,
                      si2rt.stop_item_type_id,
                      si2rt.stop_item2round_id,
					            si2rt.sign_deleted
               from rts.stop_item2round_type si2rt
               join rts.stop_item_type sit on sit.stop_item_type_id = si2rt.stop_item_type_id and
                                              sit.stop_type_id = rts.stop_type_pkg$checkpoint()) si2rdt on si2rdt.stop_item2round_id = si2r.stop_item2round_id
    left join rts.stop_item_type sitr on sitr.stop_item_id = si.stop_item_id and sitr.stop_type_id = rts.stop_type_pkg$checkpoint()
  where si2r.round_id = l_round_id
        -- and
        -- not si2r.sign_deleted and
        -- not sl.sign_deleted and
        -- not si.sign_deleted and
        -- not st.sign_deleted
  order by si2r.round_id, si2r.order_num;
end;
$$ language plpgsql;

create or replace function oud.jf_stops_order_pkg$of_update(p_id_account numeric, p_attr text)
  returns text
language plpgsql
as $$
declare
 l_stop_item_id  integer;
 l_stop_item2round_id bigint;
 l_stop_item2round_type_id bigint;
 l_stop_item_type_id bigint;
 l_is_checkpoint boolean;
begin
 l_stop_item_id:= jofl.jofl_pkg$extract_number(p_attr, 'stop_item_id', true);
 l_stop_item2round_id := jofl.jofl_pkg$extract_number(p_attr, 'stop_item2round_id', true);
 l_stop_item2round_type_id := jofl.jofl_pkg$extract_number(p_attr, 'cp_stop_item2round_type_id', true);
 l_stop_item_type_id := jofl.jofl_pkg$extract_number(p_attr, 'cp_stop_item_type_id', true);
 l_is_checkpoint := jofl.jofl_pkg$extract_boolean(p_attr, 'is_checkpoint', true);

 if l_is_checkpoint then
  if l_stop_item_type_id is null then
   insert into rts.stop_item_type (stop_item_id, stop_type_id) values (l_stop_item_id, rts.stop_type_pkg$checkpoint()) RETURNING stop_item_type_id into l_stop_item_type_id;
  end if;
  if l_stop_item2round_type_id is null then 
   insert into rts.stop_item2round_type (stop_item_type_id, stop_item2round_id) values (l_stop_item_type_id, l_stop_item2round_id);
  else
   update rts.stop_item2round_type si2rdt set sign_deleted = false where si2rdt.stop_item2round_type_id = l_stop_item2round_type_id;  
  end if;  
 else
  -- delete from rts.stop_item2round_type si2rdt where si2rdt.stop_item2round_type_id = l_stop_item2round_type_id;
  update rts.stop_item2round_type si2rdt set sign_deleted = true where si2rdt.stop_item2round_type_id = l_stop_item2round_type_id;  
 end if;

 return null;
end;
$$;
