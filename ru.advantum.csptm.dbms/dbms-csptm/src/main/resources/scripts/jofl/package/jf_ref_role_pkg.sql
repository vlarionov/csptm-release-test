
create or replace function jofl.jf_ref_role_pkg$attr_to_rowtype(attr text)
		returns jofl.ref_role as $$
	declare
		lr jofl.ref_role%rowtype;
	begin
		lr.id_role   := jofl.jofl_pkg$extract_varchar(attr,'ID_ROLE',false);
		lr.role_name := jofl.jofl_pkg$extract_varchar(attr,'ROLE_NAME', false);
		lr.is_active := jofl.jofl_pkg$extract_boolean(attr,'IS_ACTIVE');
		return lr;
	end;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------

create or replace function jofl.jf_ref_role_pkg$of_insert(aid_account numeric,
							  attr text) returns text as $$
	declare
		lr jofl.ref_role%rowtype;
	begin
		lr := jofl.jf_ref_role_pkg$attr_to_rowtype(attr);
		insert into jofl.ref_role select lr.*;
	return null;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_ref_role_pkg$of_update(aid_account numeric,
							  attr text) returns text as $$
	declare
		lr jofl.ref_role%rowtype;
	begin
		lr := jofl.jf_ref_role_pkg$attr_to_rowtype(attr);
		update jofl.ref_role f set
        			id_role = lr.id_role,
        			role_name = lr.role_name,
        			is_active = lr.is_active
        		where f.id_role = lr.id_role;
	return null;
	end;
$$ LANGUAGE plpgsql;		

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_ref_role_pkg$of_delete(aid_account numeric,
							  attr text) returns text as $$
	declare
		lr jofl.ref_role%rowtype;
	begin
		lr := jofl.jf_ref_role_pkg$attr_to_rowtype(attr);
		delete from jofl.ref_role where id_role = lr.id_role;
	return null;
	end;
$$ LANGUAGE plpgsql;	

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_ref_role_pkg$of_rows(aid_account in numeric
														,out arows refcursor
														,attr text) returns refcursor as $$
	begin
		open arows for
			select id_role AS "ID_ROLE"
   			      ,role_name AS "ROLE_NAME"
			      ,is_active AS "IS_ACTIVE"
			from jofl.ref_role src
		order by id_role;
	end;
$$ LANGUAGE plpgsql;	

