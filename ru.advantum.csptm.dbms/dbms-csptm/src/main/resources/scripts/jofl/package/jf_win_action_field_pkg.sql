create or replace function jofl.jf_win_action_field_pkg$attr_to_rowtype(attr text) 
		returns table (scn numeric,
				id_afield	jofl.win_action_field.	id_afield	%type,
				db_method	jofl.win_action_field.	db_method	%type,
				call_action	jofl.win_action_field.	call_action	%type,
				filter_class	jofl.win_action_field.	filter_class	%type,
				initial_param	jofl.win_action_field.	initial_param	%type,
				n_sort_order	jofl.win_action_field.	n_sort_order	%type,
				field_title	jofl.win_action_field.	field_title	%type,
				rowid	jofl.win_action_field.	rowid	%type) as $$
	begin
		return query 
			select null::numeric as	scn,
			       jofl.jofl_pkg$extract_varchar(attr, 'ID_AFIELD', true) as id_afield,
			       jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', false) as db_method,
			       jofl.jofl_pkg$extract_varchar(attr, 'CALL_ACTION', false) as call_action,
			       jofl.jofl_pkg$extract_varchar(attr, 'FILTER_CLASS', false) as filter_class,
			       jofl.jofl_pkg$extract_varchar(attr, 'INITIAL_PARAM', true) as initial_param,
			       jofl.jofl_pkg$extract_number(attr, 'N_SORT_ORDER', true) as n_sort_order,
			       jofl.jofl_pkg$extract_varchar(attr, 'FIELD_TITLE', true) as field_title,
			       jofl.jofl_pkg$extract_varchar(attr, 'ROWID', true) as rowid;
	end;
$$ LANGUAGE plpgsql;  
	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_win_action_field_pkg$of_insert(aid_account numeric
								 ,attr text) returns text as $$
	begin
		insert into jofl.win_action_field(id_afield	,
						db_method	,
						call_action	,
						filter_class	,
						initial_param	,
						n_sort_order	,
						field_title) 
				select case when (lr.id_afield is null or substr(lr.id_afield, 1, 1) = '-') then
					 upper(replace(public.uuid_generate_v4()::text,'-','')) else lr.id_afield end
					id_afield	,
					db_method	,
					call_action	,
					filter_class	,
					initial_param	,
					n_sort_order	,
					field_title	
				from jofl.jf_win_action_field_pkg$attr_to_rowtype(attr) lr;
		return null;
	end;
$$ LANGUAGE plpgsql;
	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_win_action_field_pkg$of_update(aid_account numeric
								 ,attr text) returns text as $$
	declare
		lr record;
	begin
		lr := jofl.jf_win_action_field_pkg$attr_to_rowtype(attr => attr);

		update jofl.win_action_field set
					id_afield	=	lr.	id_afield,
					db_method	=	lr.	db_method,
					call_action	=	lr.	call_action,
					filter_class	=	lr.	filter_class,
					initial_param	=	lr.	initial_param,
					n_sort_order	=	lr.	n_sort_order,
					field_title	=	lr.	field_title
		where id_afield = lr.id_afield;
		return null;		
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_win_action_field_pkg$of_delete(aid_account numeric
								 ,attr text) returns text as $$
	declare
		lr record;
	begin
		lr := jofl.jf_win_action_field_pkg$attr_to_rowtype(attr => attr);

		delete from jofl.win_action_field where id_afield = lr.id_afield;
		return null;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_win_action_field_pkg$of_rows(aid_account numeric
								 ,out arows refcursor
								 ,attr text default null) returns refcursor as $$
	declare
		lv_method jofl.window.db_method%type := jofl.jofl_pkg$extract_varchar(attr,'DB_METHOD',false);
		lv_call   jofl.win_action_field.call_action%type := jofl.jofl_pkg$extract_varchar(attr,'CALL_ACTION',false);
	begin
		open arows for
			select f.rowid	AS 	ROWID
                   			,f.db_method	AS 	"DB_METHOD"
                   			,f.id_afield	AS 	"ID_AFIELD"
                   			,f.call_action	AS 	"CALL_ACTION"
                   			,f.filter_class	AS 	"FILTER_CLASS"
                   			,f.initial_param AS 	"INITIAL_PARAM"
                   			,f.n_sort_order	AS 	"N_SORT_ORDER"
                   			,f.field_title	AS 	"FIELD_TITLE"
			from jofl.win_action_field f
			where f.db_method = lv_method and f.call_action = lv_call
			order by f.n_sort_order nulls last;
	end;
$$ LANGUAGE plpgsql;