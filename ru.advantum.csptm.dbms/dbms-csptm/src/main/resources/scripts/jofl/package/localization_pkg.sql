create or replace function  jofl.localization_pkg$translate(adb_method jofl.window.db_method%type
							,alocale    jofl.ref_locale.lc_locale%type
							,atag       jofl.ref_locale_tag.lc_tag%type
							,akey       jofl.ref_vocabulary.lc_key%type default '-') 
		returns jofl.ref_vocabulary.lc_value%type as $$
	with t as (select lc_value
		from jofl.ref_vocabulary
		where db_method = adb_method and lc_locale = alocale and lc_tag = atag and
					lc_key = akey)	
	select lc_value from t
	union all 
	select null 
	where not exists (select 1 from t);	
$$ LANGUAGE SQL STRICT IMMUTABLE;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function  jofl.localization_pkg$bound(adb_method jofl.window.db_method%type
							,alocale   jofl.ref_locale.lc_locale%type
							,akey      jofl.ref_vocabulary.lc_key%type default '-')
		returns jofl.ref_vocabulary.lc_value%type as $$
	select 
		jofl.localization_pkg$translate(adb_method,
						alocale,
						'#bound',
						akey);
$$ LANGUAGE SQL STRICT IMMUTABLE;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function  jofl.localization_pkg$bound(adb_method jofl.window.db_method%type
								,attr  text[]
								,akey jofl.ref_vocabulary.lc_key%type default '-')
		returns jofl.ref_vocabulary.lc_value%type as $$
	declare
		lv_lang jofl.ref_locale.lc_locale%type := jofl.jofl_pkg$extract_varchar(attr,
										   attr_locale,
										   true);
	begin
		return jofl.localization_pkg$translate(adb_method,
						       lv_lang,
						       '#bound',
						       akey);
	END;
$$ LANGUAGE plpgsql; 