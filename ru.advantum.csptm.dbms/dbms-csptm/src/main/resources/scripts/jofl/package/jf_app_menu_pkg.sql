create or replace function jofl.jf_app_menu_pkg$of_rows(aid_jofl_account in jofl.account.id_jofl_account%type,
														out arows refcursor,
														attr text default null )  RETURNS refcursor AS $$
	BEGIN
    OPEN arows FOR
	select src.id_menu	as	"ID_MENU"
		  ,src.menu_title	as	"MENU_TITLE"
		  ,src.id_menu_parent	as	"ID_MENU_PARENT"
		  ,src.db_method	as	"DB_METHOD"
		  ,parent.menu_title as	"PARENT_TITLE"
		  ,src.icon	as	"ICON"
		  ,src.n_order	as	"N_ORDER"
			,src.schema_name as "SCHEMA_NAME"
        from jofl.app_menu src
	    left join jofl.app_menu parent
        on src.id_menu_parent = parent.id_menu;
	END;
$$ LANGUAGE plpgsql;

create or replace function jofl.jf_app_menu_pkg$get_app_menu(aid_jofl_account in  jofl.account.id_jofl_account%type,
                                                             arows            out refcursor,
                                                             alocale          in  jofl.ref_locale.lc_locale%type default null,
                                                             aproj            in  jofl.app_menu.schema_name%type default null)
    returns refcursor as $$
begin
    open arows for
        with recursive r as (
            select
                am.id_menu,
                am.id_menu_parent,
                am.menu_title,
                am.icon,
                am.n_order,
                w.db_method,
                1 as level
            from jofl.app_menu am
                join wins w on am.db_method = w.db_method
            union all
            select
                am.id_menu,
                am.id_menu_parent,
                am.menu_title,
                am.icon,
                am.n_order,
                am.db_method,
                r.level + 1 as level
            from jofl.app_menu am
                join r on am.id_menu = r.id_menu_parent
        ), wins as (
            select distinct db_method
            from jofl.ref_role rr
                join jofl.role2call r2c on rr.id_role = r2c.id_role
            where rr.id_role = any
                  (jofl.jofl_pkg$get_account_roles(aid_jofl_account))
                  and r2c.call_action = 'OF_ROWS'
        )
        select distinct
            w.id_menu as "ID_MENU",
            w.menu_title as "MENU_TITLE",
            w.db_method as "DB_METHOD",
            w.id_menu_parent as "ID_MENU_PARENT",
            w.icon as "ICON",
            w.n_order as "N_ORDER"
        from r w
        order by w.n_order nulls last;
end;
$$ language plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_app_menu_pkg$attr_to_rowtype(attr text)
		returns jofl.app_menu as $$
	declare
		lr jofl.app_menu%rowtype;
	begin
		lr.id_menu := jofl.jofl_pkg$extract_number(attr,'ID_MENU',true);
		lr.menu_title := jofl.jofl_pkg$extract_varchar(attr, 'MENU_TITLE', true);
		lr.id_menu_parent := jofl.jofl_pkg$extract_varchar(attr,
							      'ID_MENU_PARENT',
							      true);

		lr.n_order := jofl.jofl_pkg$extract_number(attr,
						      'N_ORDER',
						      true);
		lr.icon := jofl.jofl_pkg$extract_varchar(attr,
						    'ICON',
						    true);

		lr.db_method := jofl.jofl_pkg$extract_varchar(attr,
							 'DB_METHOD',
							 true);

		lr.schema_name := jofl.jofl_pkg$extract_varchar(attr,
							   'SCHEMA_NAME',
							   true);
		return lr;
	end;
$$ LANGUAGE plpgsql;


------------------------------------------------------------------------------------------------------------------------------------------------

create or replace function jofl.jf_app_menu_pkg$of_insert(aid_account numeric
							  ,attr text) returns text as $$
	declare
		lr    jofl.app_menu%rowtype;
	begin
		lr := jofl.jf_app_menu_pkg$attr_to_rowtype(attr);
		insert into jofl.app_menu
			(id_menu,
			menu_title,
			id_menu_parent,
			db_method,
			icon,
			n_order,
			schema_name)
		values (nextval('jofl.sq_menu'),
			lr.menu_title,
			lr.id_menu_parent,
			lr.db_method,
			lr.icon,
			lr.n_order,
			lr.schema_name);
		return null;
	end;
$$ LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------------------------------------------------										 
create or replace function jofl.jf_app_menu_pkg$of_update(aid_account numeric
							 ,attr  text) returns text as $$
	declare
		lr    jofl.app_menu%rowtype;
	begin
		lr := jofl.jf_app_menu_pkg$attr_to_rowtype(attr);
		update jofl.app_menu f set
			id_menu	= lr.id_menu,
			menu_title = lr.menu_title,
			id_menu_parent	= lr.id_menu_parent,
			db_method = lr.db_method,
			icon = lr.icon,
			n_order	= lr.n_order,
			schema_name = lr.schema_name
		where f.id_menu = lr.id_menu;
		return null;
	end;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------										 
create or replace function jofl.jf_app_menu_pkg$of_delete(aid_account numeric
							 ,attr text) returns text as $$
	declare
		lr    jofl.app_menu%rowtype;
	begin
		lr := jofl.jf_app_menu_pkg$attr_to_rowtype(attr);
		delete from jofl.app_menu f where f.id_menu = lr.id_menu;
		return null;
	end;
$$ LANGUAGE plpgsql;
