CREATE OR REPLACE FUNCTION jofl.const_pkg$usr_var_setting()
  RETURNS TEXT
IMMUTABLE
LANGUAGE plpgsql
AS
$$
BEGIN
  RETURN 'usrvar';
END;
$$;

CREATE OR REPLACE FUNCTION jofl.const_pkg$id_jofl_account()
  RETURNS TEXT
IMMUTABLE
LANGUAGE plpgsql
AS
$$
BEGIN
  RETURN 'id_jofl_account';
END;
$$;

CREATE OR REPLACE FUNCTION jofl.const_pkg$jofl_method()
  RETURNS TEXT
IMMUTABLE
LANGUAGE plpgsql
AS
$$
BEGIN
  RETURN 'jofl_method';
END;
$$;

CREATE OR REPLACE FUNCTION jofl.const_pkg$jofl_action()
  RETURNS TEXT
IMMUTABLE
LANGUAGE plpgsql
AS
$$
BEGIN
  RETURN 'jofl_action';
END;
$$;