create or replace function   jofl.ref_field_visibility_pkg$of_rows(aid_account numeric
								,out arows refcursor
								 , attr text default '{}' ) returns refcursor as $$
	begin
		open arows for
        			select n_visibility AS "N_VISIBILITY"
        				,visibility_name AS "VISIBILITY_NAME"
        			from jofl.ref_field_visibility
        			order by n_visibility;
		
	end ;
$$ LANGUAGE plpgsql;