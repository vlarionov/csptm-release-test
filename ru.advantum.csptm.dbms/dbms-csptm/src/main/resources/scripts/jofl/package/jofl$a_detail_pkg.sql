﻿create or replace function  jofl.jofl$a_detail_pkg$attr_to_rowtype(attr text)
		returns jofl.win_action_detail as $$
	declare
		lr jofl.win_action_detail%rowtype;
	begin
		lr.db_method := jofl.jofl_pkg$extract_varchar(attr,'DB_METHOD', false);
		lr.action_title := jofl.jofl_pkg$extract_varchar(attr,'ACTION_TITLE',false);
		lr.detail_db_method := jofl.jofl_pkg$extract_varchar(attr,'DETAIL_DB_METHOD', false);
		lr.is_row_depend := jofl.jofl_pkg$extract_boolean(attr,'IS_ROW_DEPEND', true);
		lr.n_sort_order := jofl.jofl_pkg$extract_number(attr, 'N_SORT_ORDER', true);
		return lr;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jofl$a_detail_pkg$of_rows(aid_account numeric
														  ,out arows refcursor
							  							  ,attr  text) returns refcursor as $$
	declare
		lv_dbmethod jofl.win_field.db_method%type := jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', false);
	begin
		open arows for
			select db_method	AS	"DB_METHOD"
                   				,action_title	AS	"ACTION_TITLE"
                   				,detail_db_method	AS	"DETAIL_DB_METHOD"
                   				,is_row_depend	AS	"IS_ROW_DEPEND"
                   				,n_sort_order	AS	"N_SORT_ORDER"
			from jofl.win_action_detail w
			where w.db_method = lv_dbmethod;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jofl$a_detail_pkg$of_insert(aid_account numeric,
							     attr text) returns text as $$		
	begin
		insert into jofl.win_action_detail( db_method
				,action_title
				,detail_db_method
				,is_row_depend
				,n_sort_order) 
		select  db_method
				,action_title
				,detail_db_method
				,is_row_depend
				,n_sort_order from jofl.jofl$a_detail_pkg$attr_to_rowtype(attr) lr;
		return null;
	end;
$$ LANGUAGE plpgsql;
	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jofl$a_detail_pkg$of_update(aid_account numeric,
							     attr text) returns text as $$	
	declare
		lr jofl.win_action_detail%rowtype;		 
	begin
		lr := jofl.jofl$a_detail_pkg$attr_to_rowtype(attr);
		update jofl.win_action_detail f
		set     action_title		=	lr.	action_title,
			is_row_depend		=	lr.	is_row_depend,
			n_sort_order            =       lr.n_sort_order	
		where f.db_method = lr.db_method and
					f.detail_db_method = lr.detail_db_method;
		return null;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jofl$a_detail_pkg$of_delete(aid_account numeric,
							     attr text) returns text as $$	
	declare
		lr jofl.win_action_detail%rowtype;		 
	begin
		lr := jofl.jofl$a_detail_pkg$attr_to_rowtype(attr);
		delete from jofl.win_action_detail f
		where f.db_method = lr.db_method and
					f.detail_db_method = lr.detail_db_method;
		return null;
	end;
$$ LANGUAGE plpgsql;