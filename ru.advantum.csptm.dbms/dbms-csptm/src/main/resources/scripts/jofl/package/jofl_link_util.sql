create or replace function jofl.jofl_link_util$build(link text [])
    returns text as $$
begin
    return json_build_array(json_build_object('CAPTION', link [1],
                                              'FILE', json_build_object('FILEID', link [2],
                                                                        'FILENAME', link [3])));
end;
$$ language plpgsql;

create or replace function jofl.jofl_link_util$build(p_caption         text,
                                                     p_file_storage_id text,
                                                     p_filename        text)
    returns text as $$
begin
    return jofl.jofl_link_util$build(array [p_caption, p_file_storage_id, p_filename]);
end;
$$ language plpgsql;
