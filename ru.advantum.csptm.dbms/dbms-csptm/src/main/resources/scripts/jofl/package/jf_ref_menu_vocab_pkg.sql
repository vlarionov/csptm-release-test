
create or replace function jofl.jf_ref_menu_vocab_pkg$attr_to_rowtype(attr text) returns table(scn numeric,
											        lc_locale jofl.ref_menu_vocabulary.lc_locale%type,
												id_menu jofl.ref_menu_vocabulary.id_menu%type,
												rowid jofl.ref_menu_vocabulary.rowid%type,
												menu_title jofl.ref_menu_vocabulary.menu_title%type)
			as $$
	begin
		return query 
			select null::numeric as scn,			       
			       jofl.jofl_pkg$extract_varchar(attr,'LC_LOCALE', false)::jofl.ref_menu_vocabulary.lc_locale%type as lc_locale,
			       jofl.jofl_pkg$extract_number(attr,'ID_MENU', false)::jofl.ref_menu_vocabulary.id_menu%type as id_menu,
			       jofl.jofl_pkg$extract_varchar(attr,'ROWID', false, false) as rowid,
			       jofl.jofl_pkg$extract_varchar(attr,'MENU_TITLE', false)::jofl.ref_menu_vocabulary.menu_title%type as menu_title;
	end;
$$ LANGUAGE plpgsql;
	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_ref_menu_vocab_pkg$of_insert(aid_account numeric,
								attr text) returns text as $$
	declare
		lr record;		
	begin
		lr:= jofl.jf_ref_menu_vocab_pkg$attr_to_rowtype(attr);
		insert into jofl.ref_menu_vocabulary (lc_locale,
						      id_menu,
						      rowid,
						      menu_title)
			values (lr.lc_locale,
				lr.id_menu,
				lr.rowid,
				lr.menu_title);
		return null;
	end;
$$ LANGUAGE plpgsql;


------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_ref_menu_vocab_pkg$of_update(aid_account numeric,
								attr text) returns text as $$
	declare
		lr record;		
	begin
		lr:= jofl.jf_ref_menu_vocab_pkg$attr_to_rowtype(attr);
		update jofl.ref_menu_vocabulary set
			lc_locale = lr.lc_locale,
			id_menu	= lr.id_menu,
			rowid = lr.rowid,
			menu_title = lr.menu_title;
		return null;
	end;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_ref_menu_vocab_pkg$of_delete(aid_account numeric,
								attr text) returns text as $$
	declare
		lr record;		
	begin
		lr:= jofl.jf_ref_menu_vocab_pkg$attr_to_rowtype(attr);
		delete from jofl.ref_menu_vocabulary where rowid = lr.rowid;
		return null;
	end;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_ref_menu_vocab_pkg$of_rows(aid_account numeric
							     ,out arows refcursor
							     ,attr text default null) returns refcursor as $$
	declare
		ln_id_menu jofl.app_menu.id_menu%type := jofl.jofl_pkg$extract_number(attr,'ID_MENU', false);
	begin
		open arows for
			select v.rowid AS "ROWID"
				,lc_locale AS "LC_LOCALE"
				,id_menu AS "ID_MENU"
				,menu_title AS "MENU_TITLE"
			from jofl.ref_menu_vocabulary v
			where v.id_menu = ln_id_menu;
	end;
$$ LANGUAGE plpgsql;