create or replace function jofl.jf_ref_vocabulary_pkg$attr_to_rowtype(attr text) 
		returns table(scn numeric,
			      db_method jofl.ref_vocabulary.db_method%type,
			      lc_locale jofl.ref_vocabulary.lc_locale%type,
			      lc_tag jofl.ref_vocabulary.lc_tag%type,
			      lc_key jofl.ref_vocabulary.lc_key%type,
			      lc_value jofl.ref_vocabulary.lc_value%type,
			      rowid jofl.ref_vocabulary.rowid%type)  as $$
	begin
		return query 
			select 	null::numeric scn,
				jofl.jofl_pkg$extract_varchar(attr,'DB_METHOD',false) as db_method,
				jofl.jofl_pkg$extract_varchar(attr, 'LC_LOCALE',false)::char(5) as lc_locale,
				jofl.jofl_pkg$extract_varchar(attr, 'LC_TAG',false) as lc_tag,
				jofl.jofl_pkg$extract_varchar(attr, 'LC_KEY', true) as lc_key,
				jofl.jofl_pkg$extract_varchar(attr, 'LC_VALUE',false) as lc_value,
				jofl.jofl_pkg$extract_varchar(attr, 'ROWID', true) as rowid;
	end;
$$ LANGUAGE plpgsql;	

------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_ref_vocabulary_pkg$of_insert(aid_account numeric
								,attr text) returns text as $$
	begin	
		insert into jofl.ref_vocabulary(db_method,
				lc_locale,
				lc_tag,
				lc_key,
				lc_value)
			select db_method,
				lc_locale,
				lc_tag,
				lc_key,
				lc_value 
			from jofl.jf_ref_vocabulary_pkg$attr_to_rowtype(attr); 
		return null;
	end;
$$ LANGUAGE plpgsql;
	--------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_ref_vocabulary_pkg$of_update(aid_account numeric
								,attr text) returns text as $$
	declare
		lr record;
	begin
		lr := jofl.jf_ref_vocabulary_pkg$attr_to_rowtype(attr);
		update jofl.ref_vocabulary  set
			db_method = lr.db_method,
			lc_locale = lr.lc_locale,
			lc_tag	= lr.lc_tag,
			lc_key	= lr.lc_key,
			lc_value = lr.lc_value
		where rowid = lr.rowid;
		return null;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_ref_vocabulary_pkg$of_delete(aid_account numeric
								,attr text) returns text as $$
	declare
		lr record;
	begin
		lr := jofl.jf_ref_vocabulary_pkg$attr_to_rowtype(attr);
		delete from jofl.ref_vocabulary where rowid = lr.rowid;
		return null;
	end;
$$ LANGUAGE plpgsql;
	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_ref_vocabulary_pkg$of_rows(aid_account numeric
								,out arows refcursor
								,attr text default null) returns refcursor as $$
	declare
		lv_method jofl.window.db_method%type := jofl.jofl_pkg$extract_varchar(attr,'DB_METHOD', true);
	begin
		open arows for
			select rowid AS "ROWID"
				,db_method AS "DB_METHOD"
				,lc_locale AS "LC_LOCALE"
				,lc_tag AS "LC_TAG"
				,lc_key AS "LC_KEY"
				,lc_value AS "LC_VALUE"
			from jofl.ref_vocabulary v
			where (v.db_method = lv_method or lv_method is null );
	end;
$$ LANGUAGE plpgsql;


	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_ref_vocabulary_pkg$of_sync(aid_account numeric
							     ,attr text) returns text as $$
	declare
		lr record;
		f record;
		c record;
		d record;
		lv_lang   jofl.ref_locale.lc_locale%type := jofl.jofl_pkg$extract_varchar(attr,'F_LC_LOCALE',false);
		lv_method jofl.window.db_method%type := jofl.jofl_pkg$extract_varchar(attr,'DB_METHOD', false);
	
		lb_filter_exists boolean := false;	
		ln_qty numeric := 0;
	begin
		lr := jofl.jf_ref_vocabulary_pkg$attr_to_rowtype(attr);
		-- 1)  #title
		begin
			insert into jofl.ref_vocabulary
				(db_method
				,lc_locale
				,lc_tag
				,lc_key
				,lc_value)
			values
				(lv_method
				,lv_lang
				,'#title'
				,'-'
				,attr('WIN_TITLE'));
		
			ln_qty := 1;
		exception
			when  unique_violation then
		end;
	
		-- 2)  #field
		for f in (select field_name
				,display_title
			from jofl.win_field f
			where f.db_method = lv_method and n_visibility != 0)
		loop
			begin
				insert into jofl.ref_vocabulary
					(db_method
					,lc_locale
					,lc_tag
					,lc_key
					,lc_value)
				values
					(lv_method
					,lv_lang
					,'#field'
					,f.field_name
					,f.display_title);
			
				ln_qty := ln_qty + 1;
			exception
				when unique_violation then
			end;
		end loop;
	
	
		-- 2.1)  #field.description
		for f in (select field_name,
				display_description
			from jofl.win_field f
			where f.db_method = lv_method and n_visibility != 0 and
						display_description is not null)
		loop
			begin
				insert into jofl.ref_vocabulary
					(db_method
					,lc_locale
					,lc_tag
					,lc_key
					,lc_value)
				values
					(lv_method
					,lv_lang
					,'#field.description'
					,f.field_name
					,f.display_description);
			
				ln_qty := ln_qty + 1;
			exception
				when unique_violation then
			end;
		end loop;
	
		-- 3) #action
		for c in (select c.call_action,
				c.action_title
			from jofl.win_action_call c
			where c.db_method = lv_method and c.is_internal = 0)
		loop
		
			begin
				insert into jofl.ref_vocabulary
					(db_method
					,lc_locale
					,lc_tag
					,lc_key
					,lc_value)
				values
					(lv_method
					,lv_lang
					,'#action'
					,c.call_action
					,c.action_title);
				ln_qty := ln_qty + 1;
			exception
				when unique_violation  then
			end;
		end loop;
	
	
		-- 4) #action.field
		for f in (select f.id_afield,
				coalesce(f.field_title, '<<EMPTY>>') field_title
			from jofl.win_action_field f
			where f.db_method = lv_method)
		loop
			begin
				insert into jofl.ref_vocabulary
					(db_method
					,lc_locale
					,lc_tag
					,lc_key
					,lc_value)
				values
					(lv_method
					,lv_lang
					,'#action.field'
					,f.id_afield
					,f.field_title);
				ln_qty := ln_qty + 1;
			exception
				when unique_violation then
					null;
			end;
		end loop;
	
		-- 5) #detail 
		for d in (select d.action_title,
				d.detail_db_method
			from jofl.win_action_detail d
			where d.db_method = lv_method)
		loop
			begin
				insert into jofl.ref_vocabulary
					(db_method
					,lc_locale
					,lc_tag
					,lc_key
					,lc_value)
				values
					(lv_method
					,lv_lang
					,'#detail'
					,d.detail_db_method
					,d.action_title);
			
				ln_qty := ln_qty + 1;
			exception
				when unique_violation  then
			end;
		end loop;
	
		-- 6) #filter
		for f in (select f.id_filter,
				coalesce(f.filter_title, '<<EMPTY>>') as filter_title
			from jofl.win_filter f
			where f.db_method = lv_method)
		loop
			lb_filter_exists := true;
			begin
				insert into ref_vocabulary
					(db_method
					,lc_locale
					,lc_tag
					,lc_key
					,lc_value)
				values
					(lv_method
					,lv_lang
					,'#filter'
					,f.id_filter
					,f.filter_title);
			
				ln_qty := ln_qty + 1;
			exception
				when unique_violation  then
			end;
		end loop;
	
		if (lb_filter_exists)
		then
			for f in (select f.but_apply_caption,
					f.but_reset_caption
				from jofl.win_filter_opts f
				where f.db_method = lv_method)
			loop
				begin
					insert into jfl.ref_vocabulary
						(db_method
						,lc_locale
						,lc_tag
						,lc_key
						,lc_value)
					values
						(lv_method
						,lv_lang
						,'#filter.apply'
						,'-'
						,f.but_apply_caption);
				
					ln_qty := ln_qty + 1;
				exception
					when unique_violation then
				end;
			
				begin
					insert into jofl.ref_vocabulary
						(db_method
						,lc_locale
						,lc_tag
						,lc_key
						,lc_value)
					values
						(lv_method
						,lv_lang
						,'#filter.reset'
						,'-'
						,f.but_reset_caption);
				
					ln_qty := ln_qty + 1;
				exception
					when unique_violation then
				end;
			end loop;
		
		end if;
	
		return 'Для метода: ' || lv_method || ' найдено ' || ln_qty ||
						' новых записей! ';
	end;
$$ LANGUAGE plpgsql;