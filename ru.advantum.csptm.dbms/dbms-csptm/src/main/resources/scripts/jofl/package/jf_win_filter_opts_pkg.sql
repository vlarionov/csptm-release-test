create or replace function jofl.jf_win_filter_opts_pkg$attr_to_rowtype(attr text) 
	returns jofl.win_filter_opts as $$
		declare
		lr jofl.win_filter_opts%rowtype;
	begin
		lr.db_method := jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', false);
		lr.but_apply_caption :=  jofl.jofl_pkg$extract_varchar(attr, 'BUT_APPLY_CAPTION', false);
		lr.but_reset_caption := jofl.jofl_pkg$extract_varchar(attr, 'BUT_RESET_CAPTION', false);
		lr.is_reset_present := jofl.jofl_pkg$extract_boolean(attr, 'IS_RESET_PRESENT');
		return lr;
	end;
$$ LANGUAGE plpgsql; 
	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_win_filter_opts_pkg$of_insert(aid_account numeric,
								attr text) returns text as $$
	begin
		insert into jofl.win_filter_opts(db_method	,
						but_apply_caption	,
						but_reset_caption	,
						is_reset_present) 
		select db_method	,
			but_apply_caption	,
			but_reset_caption	,
			is_reset_present
		from jofl.jf_win_filter_opts_pkg$attr_to_rowtype(attr);
		return null;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_win_filter_opts_pkg$of_update(aid_account numeric,
								attr text) returns text as $$
	declare
		lr record;
	begin
		lr := jofl.jf_win_filter_opts_pkg$attr_to_rowtype(attr);
		
		update jofl.win_filter_opts
		set     but_apply_caption	=	lr.	but_apply_caption,
			but_reset_caption	=	lr.	but_reset_caption,
			is_reset_present	=	lr.	is_reset_present
		where db_method = lr.db_method;
		return null;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_win_filter_opts_pkg$of_delete(aid_account numeric,
								attr text) returns text as $$
	declare
		lr record;
	begin
		lr := jofl.jf_win_filter_opts_pkg$attr_to_rowtype(attr);
		delete from jofl.win_filter_opts where db_method = lr.db_method;
		return null;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_win_filter_opts_pkg$of_rows(aid_account numeric
								  ,out arows refcursor
							      ,attr text default null) returns refcursor as $$
	declare
			lv_method jofl.window.db_method%type := jofl.jofl_pkg$extract_varchar(attr,'DB_METHOD', false);
	begin
		open arows for
			select db_method	AS 	"DB_METHOD"
					,but_apply_caption	AS "BUT_APPLY_CAPTION"
					,but_reset_caption	AS 	"BUT_RESET_CAPTION"
					,is_reset_present	AS 	"IS_RESET_PRESENT"
			from jofl.win_filter_opts fo
			where fo.db_method = lv_method;
	end;
$$ LANGUAGE plpgsql;