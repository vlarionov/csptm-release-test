﻿create or replace function jofl.jofl_storage_pkg$get_custom_storage_dir(adb_method  in jofl.window.db_method%type,
                                                                        afield_name in jofl.win_field.field_name%type)
    returns jofl.custom_file_storage.storage_dir%type as $$
select storage_dir
from jofl.custom_file_storage
where (db_method, field_name) = ($1, $2);
$$ language sql;