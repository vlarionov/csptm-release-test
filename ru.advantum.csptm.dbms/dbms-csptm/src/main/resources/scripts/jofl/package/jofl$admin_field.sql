create or replace function jofl.jofl$admin_field$attr_to_rowtype(attr text) returns jofl.win_field as $$
	declare
		lr jofl.win_field%rowtype;
	begin
		lr.rowid := jofl.jofl_pkg$extract_varchar(attr, 'ROWID', true);
		lr.db_method :=jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', false);
		lr.field_name := jofl.jofl_pkg$extract_varchar(attr, 'FIELD_NAME', false);
		lr.field_cons_type := jofl.jofl_pkg$extract_number(attr, 'FIELD_CONS_TYPE', false);
		lr.field_type := jofl.jofl_pkg$extract_varchar(attr, 'FIELD_TYPE', false);
		lr.display_title := jofl.jofl_pkg$extract_varchar(attr, 'DISPLAY_TITLE', false);
		lr.is_display_name := jofl.jofl_pkg$extract_boolean(attr, 'IS_DISPLAY_NAME');
		lr.is_mandatory := jofl.jofl_pkg$extract_boolean(attr, 'IS_MANDATORY');
		lr.display_description := jofl.jofl_pkg$extract_varchar(attr, 'DISPLAY_DESCRIPTION', true);
		lr.pattern := jofl.jofl_pkg$extract_varchar(attr,  'PATTERN', true);
		lr.default_value := jofl.jofl_pkg$extract_varchar(attr, 'DEFAULT_VALUE', true);
		lr.n_order :=jofl.jofl_pkg$extract_number(attr, 'N_ORDER', false);
		lr.n_visibility := jofl.jofl_pkg$extract_number(attr, 'N_VISIBILITY', false);
		lr.max_len := jofl.jofl_pkg$extract_number(attr,  'MAX_LEN', true);
		lr.weight := jofl.jofl_pkg$extract_number(attr, 'WEIGHT', false);
		return lr;
		end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
	-- jofl.api functions
	------------------------------------------------------------------------------------------------------------------------------------------------

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jofl$admin_field$of_rows(aid_account numeric
														,out arows refcursor
														,attr  text) returns refcursor as $$
	declare
		lv_dbmethod jofl.win_field.db_method%type := jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', false);
	begin
		open arows for
			select f.rowid	AS	"ROWID"
                   				,f.db_method	AS	"DB_METHOD"
                   				,f.field_name	AS	"FIELD_NAME"
                   				,f.field_cons_type	AS	"FIELD_CONS_TYPE"
                   				,fct.field_const_name	AS	"FIELD_CONST_NAME"
                   				,f.field_type	AS	"FIELD_TYPE"
                   				,ft.field_type_name	AS	"FIELD_TYPE_NAME"
                   				,f.display_title	AS	"DISPLAY_TITLE"
                   				,f.display_description	AS	"DISPLAY_DESCRIPTION"
                   				,f.default_value	AS	"DEFAULT_VALUE"
                   				,f.n_order	AS	"N_ORDER"
                   				,f.n_visibility	AS	"N_VISIBILITY"
                   				,fv.visibility_name	AS	"VISIBILITY_NAME"
                   				,f.is_display_name	AS	"IS_DISPLAY_NAME"
                   				,f.is_mandatory	AS	"IS_MANDATORY"
                   				,f.pattern AS	"PATTERN"
                   				,f.max_len	AS	"MAX_LEN"
                   				,f.weight	AS	"WEIGHT"
			from jofl.win_field            f
				,jofl.ref_field_type       ft
				,jofl.ref_field_visibility fv
				,jofl.ref_const_type       fct
			where f.db_method = lv_dbmethod and f.field_type = ft.field_type and
						f.n_visibility = fv.n_visibility and
						f.field_cons_type = fct.field_cons_type
			order by n_order;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jofl$admin_field$of_insert(aid_account numeric,
							   attr  text) returns text as $$
	declare
		lv_packet jofl.window.interface_pkg%type;
	begin
		with sel as (select db_method,
					field_name,
					field_cons_type,
					field_type,
					display_title,
					is_display_name,
					is_mandatory,
					display_description,
					pattern,
					default_value,
					n_order,
					n_visibility,
					max_len,
					weight,
					nextval('jofl.sq_jofl') rowid
		from jofl.jofl$admin_field$attr_to_rowtype(attr) lr),
		ins as (insert into jofl.win_field (db_method,
					field_name,
					field_cons_type,
					field_type,
					display_title,
					is_display_name,
					is_mandatory,
					display_description,
					pattern,
					default_value,
					n_order,
					n_visibility,
					max_len,
					weight,
					rowid)
				select db_method,
					field_name,
					field_cons_type,
					field_type,
					display_title,
					is_display_name,
					is_mandatory,
					display_description,
					pattern,
					default_value,
					n_order,
					n_visibility,
					max_len,
					weight,
					 rowid::text
					 from sel returning *)
		select w.interface_pkg into lv_packet
			from sel s
			join jofl.window w on  w.db_method = s.db_method;

		return 'Не забудьте внести изменения в методы: OF_ROWS и ATTR_TO_ROWTYPE пакета - ' ||
						lv_packet || ' , это важно - иначе ничего не будет работать!';
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jofl$admin_field$of_update(aid_account numeric
							   ,attr text) returns text as $$
	declare
		lr  jofl.win_field%rowtype;
	begin
		lr := jofl.jofl$admin_field$attr_to_rowtype(attr);
		update jofl.win_field f set
				db_method	=	lr.	db_method	,
				field_name	=	lr.	field_name	,
				field_cons_type	=	lr.	field_cons_type	,
				field_type	=	lr.	field_type	,
				display_title	=	lr.	display_title	,
				is_display_name	=	lr.	is_display_name	,
				is_mandatory	=	lr.	is_mandatory	,
				display_description	=	lr.	display_description	,
				pattern	=	lr.	pattern	,
				default_value	=	lr.	default_value	,
				n_order	=	lr.	n_order	,
				n_visibility	=	lr.	n_visibility	,
				max_len	=	lr.	max_len	,
				weight	=	lr.	weight
		where f.rowid = lr.rowid
			or (f.db_method = lr.db_method and f.field_name=lr.field_name);

		return null;
	end;
$$ LANGUAGE plpgsql;

	------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jofl$admin_field$of_delete(aid_account numeric
							   ,attr text) returns text as $$
	declare
		lv_packet jofl.window.interface_pkg%type;
	begin
		with sel as (select lr.rowid,
							lr.db_method
					 from jofl.jofl$admin_field$attr_to_rowtype(attr) lr),
		     del as (delete from jofl.win_field  where rowid in (select rowid from sel) returning *)
		 select w.interface_pkg into lv_packet
			from sel s
			join  jofl.window w
				on w.db_method = s.db_method;

		return 'Не забудьте внести изменения в методы: OF_ROWS и ATTR_TO_ROWTYPE пакета - ' ||
						lv_packet || ' , это важно - иначе ничего не будет работать!';
		return null;
	end;
$$ LANGUAGE plpgsql;