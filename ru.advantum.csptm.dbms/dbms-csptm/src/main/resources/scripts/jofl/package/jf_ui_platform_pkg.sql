create or replace function jofl.jf_ui_platform_pkg$attr_to_rowtype(attr text) returns table(rowid jofl.ref_ui_platform.rowid%type
											    ,scn numeric
											    ,id_platform jofl.ref_ui_platform.id_platform%type
											    ,plat_name jofl.ref_ui_platform.plat_name%type) as $$
	begin
		return query
			select null::text as rowid,
				null::numeric as scn,
				jofl.jofl_pkg$extract_varchar(attr,'ID_PLATFORM', false) aS id_platform,
				jofl.jofl_pkg$extract_varchar(attr,'PLAT_NAME', false) as plat_name;
	end;
$$ LANGUAGE plpgsql;	


------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function jofl.jf_ui_platform_pkg$of_rows(aid_account numeric
							   ,out arows refcursor
							   ,attr  text default null) returns refcursor as $$
	begin
		open arows for
			select id_platform AS "ID_PLATFORM"
				,plat_name AS "PLAT_NAME"
			from jofl.ref_ui_platform src;
end;
$$ LANGUAGE plpgsql;	