CREATE OR REPLACE FUNCTION jofl.jf_win_filter_opts_pkg$attr_to_rowtype(attr TEXT)
  RETURNS jofl.WIN_FILTER_OPTS AS $$
DECLARE
  lr jofl.win_filter_opts%ROWTYPE;
BEGIN
  lr.db_method := jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', FALSE);
  lr.but_apply_caption :=  jofl.jofl_pkg$extract_varchar(attr, 'BUT_APPLY_CAPTION', FALSE);
  lr.but_reset_caption := jofl.jofl_pkg$extract_varchar(attr, 'BUT_RESET_CAPTION', FALSE);
  lr.is_reset_present := jofl.jofl_pkg$extract_boolean(attr, 'IS_RESET_PRESENT');
  RETURN lr;
END;
$$ LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_win_filter_opts_pkg$of_insert(aid_account NUMERIC,
                                                                 attr        TEXT)
  RETURNS TEXT AS $$
BEGIN
  INSERT INTO jofl.win_filter_opts (db_method,
                                    but_apply_caption,
                                    but_reset_caption,
                                    is_reset_present)
    SELECT
      db_method,
      but_apply_caption,
      but_reset_caption,
      is_reset_present
    FROM jofl.jf_win_filter_opts_pkg$attr_to_rowtype(attr);
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_win_filter_opts_pkg$of_update(aid_account NUMERIC,
                                                                 attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr RECORD;
BEGIN
  lr := jofl.jf_win_filter_opts_pkg$attr_to_rowtype(attr);

  UPDATE jofl.win_filter_opts
  SET but_apply_caption = lr.but_apply_caption,
    but_reset_caption   = lr.but_reset_caption,
    is_reset_present    = lr.is_reset_present
  WHERE db_method = lr.db_method;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_win_filter_opts_pkg$of_delete(aid_account NUMERIC,
                                                                 attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr RECORD;
BEGIN
  lr := jofl.jf_win_filter_opts_pkg$attr_to_rowtype(attr);
  DELETE FROM jofl.win_filter_opts
  WHERE db_method = lr.db_method;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_win_filter_opts_pkg$of_rows(aid_account NUMERIC
  , OUT                                                        arows       REFCURSOR
  ,                                                            attr        TEXT DEFAULT NULL)
  RETURNS REFCURSOR AS $$
DECLARE
  lv_method jofl.window.db_method%TYPE := jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', FALSE);
BEGIN
  OPEN arows FOR
  SELECT
    db_method         AS "DB_METHOD",
    but_apply_caption AS "BUT_APPLY_CAPTION",
    but_reset_caption AS "BUT_RESET_CAPTION",
    is_reset_present  AS "IS_RESET_PRESENT"
  FROM jofl.win_filter_opts fo
  WHERE fo.db_method = lv_method;
END;
$$ LANGUAGE plpgsql;