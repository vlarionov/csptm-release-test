﻿CREATE OR REPLACE FUNCTION jofl.jf_role2call_pkg$attr_to_rowtype(attr TEXT)
  RETURNS jofl.ROLE2CALL AS $$
DECLARE
  lr jofl.role2call%ROWTYPE;
BEGIN
  lr.id_role     := jofl.jofl_pkg$extract_varchar(attr, 'ID_ROLE', FALSE);
  lr.db_method   := jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', FALSE);
  lr.call_action :=jofl.jofl_pkg$extract_varchar(attr, 'CALL_ACTION', FALSE);
  RETURN lr;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_role2call_pkg$of_insert(aid_account NUMERIC, attr TEXT)
  RETURNS TEXT AS $$
BEGIN
  INSERT INTO jofl.role2call
    SELECT *
    FROM jofl.jf_role2call_pkg$attr_to_rowtype(attr);
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_role2call_pkg$of_update(aid_account NUMERIC, attr TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.role2call%ROWTYPE;
BEGIN
  lr := jofl.jf_role2call_pkg$attr_to_rowtype(attr);
  UPDATE jofl.role2call
  SET id_role   = lr.id_role,
    db_method   = lr.db_method,
    call_action = lr.call_action
  WHERE id_role = lr.id_role
        AND db_method = lr.db_method
        AND call_action = lr.call_action;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_role2call_pkg$of_delete(aid_account NUMERIC, attr TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.role2call%ROWTYPE;
BEGIN
  lr := jofl.jf_role2call_pkg$attr_to_rowtype(attr);
  DELETE FROM jofl.role2call
  WHERE id_role = lr.id_role
        AND db_method = lr.db_method
        AND call_action = lr.call_action;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION jofl.jf_role2call_pkg$of_add_all(aid_account NUMERIC, attr TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr       jofl.role2call%ROWTYPE;
  rowcount NUMERIC;
BEGIN
  lr := jofl.jf_role2call_pkg$attr_to_rowtype(attr);
  WITH rs AS (SELECT
                lr.id_role,
                wa.db_method,
                wa.call_action
              FROM jofl.win_action_call wa
              WHERE wa.db_method = lr.db_method
                    AND wa.call_action NOT IN (lr.call_action)),
      cnt AS (SELECT count(1) AS cn
              FROM jofl.win_action_call wa
              WHERE wa.db_method = lr.db_method
                    AND wa.call_action NOT IN (lr.call_action)),
      ins AS (INSERT INTO jofl.role2call (id_role
      , db_method
      , call_action)
      SELECT
        id_role,
        db_method,
        call_action
      FROM rs
    RETURNING *)
  SELECT cnt.cn
  INTO rowcount
  FROM inst
    FULL OUTER JOIN cnt
      ON 1 = 1;
  RETURN 'Добавлено ' || rowcount || ' методов!';
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;


------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_role2call_pkg$of_rows(aid_account NUMERIC
  , OUT                                                  arows       REFCURSOR
  ,                                                      attr        TEXT)
  RETURNS REFCURSOR AS $$
DECLARE
  lv_role jofl.ref_role.id_role%TYPE := jofl.jofl_pkg$extract_varchar(attr, 'ID_ROLE', FALSE);
  lrows   REFCURSOR;
BEGIN
  OPEN arows FOR
  SELECT
    id_role     AS "ID_ROLE",
    db_method   AS "DB_METHOD",
    call_action AS "CALL_ACTION"
  FROM jofl.role2call src
  WHERE src.id_role = lv_role
  ORDER BY db_method
    , call_action;
END;
$$ LANGUAGE plpgsql;
