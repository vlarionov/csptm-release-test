CREATE OR REPLACE FUNCTION jofl.localization_pkg$translate(adb_method jofl.window.db_method%TYPE
  ,                                                        alocale    jofl.ref_locale.lc_locale%TYPE
  ,                                                        atag       jofl.ref_locale_tag.lc_tag%TYPE
  ,                                                        akey       jofl.ref_vocabulary.lc_key%TYPE DEFAULT '-')
  RETURNS jofl.ref_vocabulary.lc_value%TYPE AS $$
WITH t AS (SELECT lc_value
           FROM jofl.ref_vocabulary
           WHERE db_method = adb_method AND lc_locale = alocale AND lc_tag = atag AND
                 lc_key = akey)
SELECT lc_value
FROM t
UNION ALL
SELECT NULL
WHERE NOT exists(SELECT 1
                 FROM t);
$$ LANGUAGE SQL STRICT IMMUTABLE;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.localization_pkg$bound(adb_method jofl.window.db_method%TYPE
  ,                                                    alocale    jofl.ref_locale.lc_locale%TYPE
  ,                                                    akey       jofl.ref_vocabulary.lc_key%TYPE DEFAULT '-')
  RETURNS jofl.ref_vocabulary.lc_value%TYPE AS $$
SELECT jofl.localization_pkg$translate(adb_method,
                                       alocale,
                                       '#bound',
                                       akey);
$$ LANGUAGE SQL STRICT IMMUTABLE;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.localization_pkg$bound(adb_method jofl.window.db_method%TYPE
  ,                                                    attr       TEXT []
  ,                                                    akey       jofl.ref_vocabulary.lc_key%TYPE DEFAULT '-')
  RETURNS jofl.ref_vocabulary.lc_value%TYPE AS $$
DECLARE
  lv_lang jofl.ref_locale.lc_locale%TYPE := jofl.jofl_pkg$extract_varchar(attr,
                                                                          attr_locale,
                                                                          TRUE);
BEGIN
  RETURN jofl.localization_pkg$translate(adb_method,
                                         lv_lang,
                                         '#bound',
                                         akey);
END;
$$ LANGUAGE plpgsql; 