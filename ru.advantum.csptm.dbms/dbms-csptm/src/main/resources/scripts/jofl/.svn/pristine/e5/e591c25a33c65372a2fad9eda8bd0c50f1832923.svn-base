CREATE OR REPLACE FUNCTION jofl.jf_win_filter_pkg$attr_to_rowtype(attr TEXT)
  RETURNS TABLE(scn           NUMERIC,
                db_method     jofl.win_filter.db_method%TYPE,
                filter_class  jofl.win_filter.filter_class%TYPE,
                initial_param jofl.win_filter.initial_param%TYPE,
                grid_x        jofl.win_filter.grid_x%TYPE,
                grid_y        jofl.win_filter.grid_y%TYPE,
                filter_title  jofl.win_filter.filter_title%TYPE,
                id_filter     jofl.win_filter.id_filter%TYPE,
                rowid         jofl.win_filter.rowid%TYPE) AS $$
BEGIN
  RETURN QUERY
  SELECT
    NULL :: NUMERIC                                            AS scn,
    jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', FALSE)    AS db_method,
    jofl.jofl_pkg$extract_varchar(attr, 'FILTER_CLASS', FALSE) AS filter_class,
    jofl.jofl_pkg$extract_varchar(attr, 'INITIAL_PARAM', TRUE) AS initial_param,
    jofl.jofl_pkg$extract_number(attr, 'GRID_X', FALSE)        AS grid_x,
    jofl.jofl_pkg$extract_number(attr, 'GRID_Y', FALSE)        AS grid_y,
    jofl.jofl_pkg$extract_varchar(attr, 'FILTER_TITLE', TRUE)  AS filter_title,
    jofl.jofl_pkg$extract_varchar(attr, 'ID_FILTER', FALSE)    AS id_filter,
    jofl.jofl_pkg$extract_varchar(attr, 'ROWID', TRUE)         AS rowid;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_win_filter_pkg$of_insert(aid_account NUMERIC,
                                                            attr        TEXT)
  RETURNS TEXT AS $$
BEGIN
  INSERT INTO jofl.win_filter (db_method,
                               filter_class,
                               initial_param,
                               grid_x,
                               grid_y,
                               filter_title,
                               id_filter)
    SELECT
      db_method,
      filter_class,
      initial_param,
      grid_x,
      grid_y,
      filter_title,
      CASE WHEN (lr.id_filter = '' IS TRUE OR substring(lr.id_filter FROM 1 FOR 1) = '-')
        THEN
          upper(replace(public.uuid_generate_v4() :: TEXT, '-', ''))
      ELSE lr.id_filter
      END id_filter
    FROM jofl.jf_win_filter_pkg$attr_to_rowtype(attr) lr;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_win_filter_pkg$of_update(aid_account NUMERIC,
                                                            attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr RECORD;
BEGIN
  lr := jofl.jf_win_filter_pkg$attr_to_rowtype(attr);
  UPDATE jofl.win_filter
  SET
    filter_class  = lr.filter_class,
    initial_param = lr.initial_param,
    grid_x        = lr.grid_x,
    grid_y        = lr.grid_y,
    filter_title  = lr.filter_title
  WHERE db_method = lr.db_method
        AND id_filter = lr.id_filter;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_win_filter_pkg$of_delete(aid_account NUMERIC,
                                                            attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr RECORD;
BEGIN
  lr := jofl.jf_win_filter_pkg$attr_to_rowtype(attr);
  DELETE FROM jofl.win_filter
  WHERE db_method = lr.db_method
        AND id_filter = lr.id_filter;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_win_filter_pkg$of_rows(aid_account NUMERIC
  , OUT                                                   arows       REFCURSOR
  ,                                                       attr        TEXT)
  RETURNS REFCURSOR AS $$
DECLARE
  lv_dbmethod jofl.win_field.db_method%TYPE := jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', FALSE);
BEGIN
  OPEN arows FOR
  SELECT
    rowid         AS "ROWID",
    id_filter     AS "ID_FILTER",
    db_method     AS "DB_METHOD",
    filter_class  AS "FILTER_CLASS",
    initial_param AS "INITIAL_PARAM",
    grid_x        AS "GRID_X",
    grid_y        AS "GRID_Y",
    filter_title  AS "FILTER_TITLE"
  FROM jofl.win_filter src
  WHERE src.db_method = lv_dbmethod;
END;
$$ LANGUAGE plpgsql;