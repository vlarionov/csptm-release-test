﻿CREATE OR REPLACE FUNCTION jofl.jofl_util$tr(asource TEXT)
  RETURNS TEXT AS $$
BEGIN
  RETURN replace(asource, '"', '\"');
END;
$$ LANGUAGE plpgsql;

-----------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_util$cover_result(amsg TEXT)
  RETURNS TEXT AS $$
BEGIN
  RETURN '{"message": "' || jofl.jofl_util$tr(amsg) || '"}';
END;
$$ LANGUAGE plpgsql;

-----------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_util$cover_result(amsg  TEXT
  ,                                                    k1 IN TEXT
  ,                                                    v1 IN TEXT)
  RETURNS TEXT AS $$
BEGIN
  RETURN '{"message": "' || jofl.jofl_util$tr(amsg) || '", "' || k1 || '": "' || jofl.jofl_util$tr(v1) || '" }';
END;
$$ LANGUAGE plpgsql;

-----------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_util$cover_result(amsg TEXT
  ,                                                    k1   TEXT
  ,                                                    v1   TEXT
  ,                                                    k2   TEXT
  ,                                                    v2   TEXT)
  RETURNS TEXT AS $$
BEGIN
  RETURN
  '{"message": "' || jofl.jofl_util$tr(amsg) || '", "' || k1 || '": "' || jofl.jofl_util$tr(v1) || '", "' || k2 || '":"'
  || jofl.jofl_util$tr(v2) || '" }';
END;
$$ LANGUAGE plpgsql;

-----------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_util$cover_result(k1 TEXT
  ,                                                    v1 TEXT)
  RETURNS TEXT AS $$
BEGIN
  RETURN '{"' || k1 || '": "' || jofl.jofl_util$tr(v1) || '" }';
END;
$$ LANGUAGE plpgsql;

-----------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_util$cover_result(k1 TEXT
  ,                                                    v1 TEXT
  ,                                                    k2 TEXT
  ,                                                    v2 TEXT)
  RETURNS TEXT AS $$
BEGIN
  RETURN '{"' || k1 || '": "' || jofl.jofl_util$tr(v1) || '", "' || k2 || '":"' || jofl.jofl_util$tr(v2) || '" }';
END;
$$ LANGUAGE plpgsql;

-----------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_util$cover_result(k1 TEXT
  ,                                                    v1 TEXT
  ,                                                    k2 TEXT
  ,                                                    v2 TEXT
  ,                                                    k3 TEXT
  ,                                                    v3 TEXT)
  RETURNS TEXT AS $$
BEGIN
  RETURN
  '{"' || k1 || '": "' || jofl.jofl_util$tr(v1) || '", "' || k2 || '":"' || jofl.jofl_util$tr(v2) || '", "' || k3 ||
  '":"' || jofl.jofl_util$tr(v3) || '"}';
END;
$$ LANGUAGE plpgsql;

-----------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION jofl.jofl_util$cover_address(addr TEXT
  ,                                                     lon  NUMERIC
  ,                                                     lat  NUMERIC)
  RETURNS VARCHAR AS $$
BEGIN
  RETURN jofl.jofl_util$cover_result('name', addr, 'pointStr', lon || ' ' || lat);
END;
$$ LANGUAGE plpgsql;

-----------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_util$extract_array(attr     TEXT,
                                                        attrname TEXT)
  RETURNS SETOF TEXT AS $$
DECLARE
  rec   RECORD;
  inrec RECORD;
BEGIN
  FOR rec IN (SELECT value
              FROM json_each_text('{"f1":["A1","A2","A3"]}' :: JSON)
              WHERE key = attrname)
  LOOP
    FOR inrec IN (SELECT value
                  FROM json_array_elements_text(rec.value :: JSON))
    LOOP
      RETURN NEXT inrec.value;
    END LOOP;
  END LOOP;

  RETURN;
END;
$$ LANGUAGE plpgsql;

-----------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_util$from_unixtime(unixtime IN NUMERIC)
  RETURNS TIMESTAMP AS $$
BEGIN
  RETURN to_timestamp(unixtime) AT TIME ZONE 'utc';
END;
$$ LANGUAGE plpgsql;