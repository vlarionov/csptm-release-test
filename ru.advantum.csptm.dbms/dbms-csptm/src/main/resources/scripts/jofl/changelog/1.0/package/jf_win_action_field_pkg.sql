CREATE OR REPLACE FUNCTION jofl.jf_win_action_field_pkg$attr_to_rowtype(attr TEXT)
  RETURNS TABLE(scn           NUMERIC,
                id_afield     jofl.win_action_field.id_afield%TYPE,
                db_method     jofl.win_action_field.db_method%TYPE,
                call_action   jofl.win_action_field.call_action%TYPE,
                filter_class  jofl.win_action_field.filter_class%TYPE,
                initial_param jofl.win_action_field.initial_param%TYPE,
                n_sort_order  jofl.win_action_field.n_sort_order%TYPE,
                field_title   jofl.win_action_field.field_title%TYPE,
                rowid         jofl.win_action_field.rowid%TYPE) AS $$
BEGIN
  RETURN QUERY
  SELECT
    NULL :: NUMERIC                                            AS scn,
    jofl.jofl_pkg$extract_varchar(attr, 'ID_AFIELD', TRUE)     AS id_afield,
    jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', FALSE)    AS db_method,
    jofl.jofl_pkg$extract_varchar(attr, 'CALL_ACTION', FALSE)  AS call_action,
    jofl.jofl_pkg$extract_varchar(attr, 'FILTER_CLASS', FALSE) AS filter_class,
    jofl.jofl_pkg$extract_varchar(attr, 'INITIAL_PARAM', TRUE) AS initial_param,
    jofl.jofl_pkg$extract_number(attr, 'N_SORT_ORDER', TRUE)   AS n_sort_order,
    jofl.jofl_pkg$extract_varchar(attr, 'FIELD_TITLE', TRUE)   AS field_title,
    jofl.jofl_pkg$extract_varchar(attr, 'ROWID', TRUE)         AS rowid;
END;
$$ LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_win_action_field_pkg$of_insert(aid_account NUMERIC
  ,                                                               attr        TEXT)
  RETURNS TEXT AS $$
BEGIN
  INSERT INTO jofl.win_action_field (id_afield,
                                     db_method,
                                     call_action,
                                     filter_class,
                                     initial_param,
                                     n_sort_order,
                                     field_title)
    SELECT
      CASE WHEN (lr.id_afield IS NULL OR substr(lr.id_afield, 1, 1) = '-')
        THEN
          upper(replace(public.uuid_generate_v4() :: TEXT, '-', ''))
      ELSE lr.id_afield END
        id_afield,
      db_method,
      call_action,
      filter_class,
      initial_param,
      n_sort_order,
      field_title
    FROM jofl.jf_win_action_field_pkg$attr_to_rowtype(attr) lr;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_win_action_field_pkg$of_update(aid_account NUMERIC
  ,                                                               attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr RECORD;
BEGIN
  lr := jofl.jf_win_action_field_pkg$attr_to_rowtype(attr => attr);

  UPDATE jofl.win_action_field
  SET
    id_afield     = lr.id_afield,
    db_method     = lr.db_method,
    call_action   = lr.call_action,
    filter_class  = lr.filter_class,
    initial_param = lr.initial_param,
    n_sort_order  = lr.n_sort_order,
    field_title   = lr.field_title
  WHERE id_afield = lr.id_afield;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_win_action_field_pkg$of_delete(aid_account NUMERIC
  ,                                                               attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr RECORD;
BEGIN
  lr := jofl.jf_win_action_field_pkg$attr_to_rowtype(attr => attr);

  DELETE FROM jofl.win_action_field
  WHERE id_afield = lr.id_afield;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_win_action_field_pkg$of_rows(aid_account NUMERIC
  , OUT                                                         arows       REFCURSOR
  ,                                                             attr        TEXT DEFAULT NULL)
  RETURNS REFCURSOR AS $$
DECLARE
  lv_method jofl.window.db_method%TYPE := jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', FALSE);
  lv_call   jofl.win_action_field.call_action%TYPE := jofl.jofl_pkg$extract_varchar(attr, 'CALL_ACTION', FALSE);
BEGIN
  OPEN arows FOR
  SELECT
    f.rowid         AS ROWID,
    f.db_method     AS "DB_METHOD",
    f.id_afield     AS "ID_AFIELD",
    f.call_action   AS "CALL_ACTION",
    f.filter_class  AS "FILTER_CLASS",
    f.initial_param AS "INITIAL_PARAM",
    f.n_sort_order  AS "N_SORT_ORDER",
    f.field_title   AS "FIELD_TITLE"
  FROM jofl.win_action_field f
  WHERE f.db_method = lv_method AND f.call_action = lv_call
  ORDER BY f.n_sort_order NULLS LAST;
END;
$$ LANGUAGE plpgsql;