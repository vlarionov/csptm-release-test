create or replace function jofl.jf_app_menu_pkg$get_app_menu (aid_jofl_account in jofl.account.id_jofl_account%type,
								out arows refcursor,
								alocale jofl.ref_locale.lc_locale%type default null,
								aproj jofl.app_menu.schema_name%type default null)
								RETURNS refcursor AS $$
	begin
		open arows for
		WITH RECURSIVE r  as
        	(select id_menu, id_menu_parent, menu_title,icon, n_order	,db_method, 1 AS level
                    from wins where wins.db_method is not null
                union all
                    select wins.id_menu, wins.id_menu_parent, wins.menu_title, wins.icon, wins.n_order	,wins.db_method, r.level+1 AS level
                    from wins
                    join r
                    on wins.id_menu = r.id_menu_parent),
        wins as (select distinct id_menu, coalesce (id_menu_parent,0) id_menu_parent, menu_title,icon, n_order,m.db_method
        					from jofl.role2call r2c
        						left join jofl.app_menu     m
        						on (m.db_method = r2c.db_method or  m.db_method  is null)
        					where r2c.id_role = ANY (jofl.jofl_pkg$get_account_roles(aid_jofl_account))
        						and r2c.call_action = 'OF_ROWS'
        					union
        					select null id_menu, null id_menu_parent,  null menu_title,  null icon, null n_order, db_method
        					from jofl.window
        					where jofl.jofl_pkg$isrootrole(aid_jofl_account)::integer = 1)
        select distinct w.id_menu AS "ID_MENU"
        			 ,w.menu_title AS "MENU_TITLE"
        			 ,w.db_method AS "DB_METHOD"
        			 ,w.id_menu_parent AS "ID_MENU_PARENT"
        			 ,w.icon AS "ICON"
        			 ,w.n_order AS "N_ORDER"
        			 from r w
		order by w.n_order nulls last;
 END;
$$ LANGUAGE plpgsql;