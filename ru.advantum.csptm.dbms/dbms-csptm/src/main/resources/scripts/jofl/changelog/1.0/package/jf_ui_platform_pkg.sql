CREATE OR REPLACE FUNCTION jofl.jf_ui_platform_pkg$attr_to_rowtype(attr TEXT)
  RETURNS TABLE(rowid       jofl.ref_ui_platform.rowid%TYPE
  ,             scn         NUMERIC
  ,             id_platform jofl.ref_ui_platform.id_platform%TYPE
  ,             plat_name   jofl.ref_ui_platform.plat_name%TYPE) AS $$
BEGIN
  RETURN QUERY
  SELECT
    NULL :: TEXT                                              AS rowid,
    NULL :: NUMERIC                                           AS scn,
    jofl.jofl_pkg$extract_varchar(attr, 'ID_PLATFORM', FALSE) AS id_platform,
    jofl.jofl_pkg$extract_varchar(attr, 'PLAT_NAME', FALSE)   AS plat_name;
END;
$$ LANGUAGE plpgsql;


------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_ui_platform_pkg$of_rows(aid_account NUMERIC
  , OUT                                                    arows       REFCURSOR
  ,                                                        attr        TEXT DEFAULT NULL)
  RETURNS REFCURSOR AS $$
BEGIN
  OPEN arows FOR
  SELECT
    id_platform AS "ID_PLATFORM",
    plat_name   AS "PLAT_NAME"
  FROM jofl.ref_ui_platform src;
END;
$$ LANGUAGE plpgsql;	