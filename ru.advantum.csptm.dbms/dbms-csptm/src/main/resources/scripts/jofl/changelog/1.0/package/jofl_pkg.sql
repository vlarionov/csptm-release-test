﻿CREATE OR REPLACE FUNCTION jofl.jofl_pkg$register_account(aid_account_local jofl.account.id_account_local%TYPE
  ,                                                       aproj             jofl.account.schema_name%TYPE)
  RETURNS jofl.account.id_jofl_account%TYPE AS $$
DECLARE
  ln_jofl_account jofl.account.id_jofl_account%TYPE;
BEGIN
  SELECT a.id_jofl_account
  INTO STRICT ln_jofl_account
  FROM jofl.account a
  WHERE a.id_account_local = aid_account_local /*and a.schema_name = aproj*/;

  RETURN ln_jofl_account;
  EXCEPTION
  WHEN no_data_found
    THEN
      INSERT INTO jofl.account
      (id_jofl_account
        , schema_name
        , id_account_local)
      VALUES
        (aid_account_local /*nextval('jofl.sq_jofl')*/
          , aproj
          , aid_account_local)
      RETURNING id_jofl_account
        INTO ln_jofl_account;
      RETURN ln_jofl_account;
END;
$$ LANGUAGE plpgsql;

-----------------------------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION jofl."jofl_pkg$grant_role"(
  aid_account_local NUMERIC,
  aproj             TEXT,
  arole             TEXT,
  single_role       BOOLEAN DEFAULT FALSE)
  RETURNS VOID AS
$BODY$

DECLARE
  ln_jofl_account jofl.account.id_jofl_account%TYPE;
  lv_role         jofl.ref_role.id_role%TYPE;
  ldupl           NUMERIC;
BEGIN
  SELECT jofl.jofl_pkg$register_account(aid_account_local,
                                        aproj)
  INTO ln_jofl_account;

  IF (single_role)
  THEN

    WITH
        nrs AS (SELECT
                  arole           AS id_role,
                  ln_jofl_account AS id_jofl_account
      ),
        dl AS (SELECT count(1) cnt
               FROM jofl.role2account ra, nrs
               WHERE ra.id_jofl_account = nrs.id_jofl_account

      ),
        upd AS (UPDATE jofl.role2account ra
      SET id_role = nrs.id_role
      FROM nrs, dl
      WHERE ra.id_jofl_account = nrs.id_jofl_account AND dl.cnt < 2
      RETURNING *  ),
        inst AS (
        INSERT INTO jofl.role2account
        (id_role
          , id_jofl_account)
          SELECT
            id_role,
            id_jofl_account
          FROM nrs, dl
          WHERE dl.cnt = 0
        RETURNING *
      )
    SELECT dl.cnt
    INTO ln_jofl_account
    FROM inst
      FULL OUTER JOIN dl
        ON 1 = 1
      FULL OUTER JOIN upd
        ON 1 = 1;

    IF (ldupl > 1)
    THEN
      RAISE EXCEPTION USING ERRCODE = 20001, MESSAGE = '<<Пользователю уже назначена более чем 1 роль!>>';
    END IF;
  ELSE
    SELECT id_role
    INTO lv_role
    FROM jofl.role2account ra
    WHERE ra.id_jofl_account = ln_jofl_account AND ra.id_role = arole;


    IF lv_role IS NULL
    THEN
      INSERT INTO jofl.role2account
      (id_role
        , id_jofl_account)
      VALUES
        (arole
          , ln_jofl_account);
    END IF;
  END IF;

END;
$BODY$
LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_pkg$revoke_role(aid_account_local NUMERIC,
                                                     aproj             TEXT,
                                                     arole             TEXT)
  RETURNS VOID AS $$

DECLARE
  ln_jofl_account jofl.account.id_jofl_account%TYPE;
  r               RECORD;
BEGIN
  ln_jofl_account := jofl.jofl_pkg$register_account(aid_account_local,
                                                    aproj);

  WITH sel AS (SELECT id_jofl_account AS jacc
               FROM jofl.account
               WHERE id_account_local = ln_jofl_account
    /*and schema_name=aproj*/),
      del AS (DELETE FROM jofl.role2account ra
    WHERE ra.id_role = arole AND ra.id_jofl_account IN (SELECT jacc
                                                        FROM sel)
    RETURNING *)
  SELECT *
  FROM sel, del
  INTO r;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_pkg$unregister_account(aid_account_local jofl.account.id_account_local%TYPE,
                                                            aproj             jofl.account.schema_name%TYPE)
  RETURNS VOID AS $$
DECLARE
  r RECORD;
BEGIN
  WITH
      rs AS (SELECT a.id_jofl_account
             FROM jofl.account a
             WHERE a.schema_name = aproj
                   AND a.id_account_local = aid_account_local
    ),
      dl_acc AS (DELETE FROM jofl.account a
    WHERE a.id_jofl_account IN (SELECT id_jofl_account
                                FROM rs)
    RETURNING * ),
      dl_r2a AS (DELETE FROM jofl.role2account ra
    WHERE ra.id_jofl_account IN (SELECT id_jofl_account
                                 FROM rs)
    RETURNING * )
  SELECT *
  INTO r
  FROM dl_acc
    FULL OUTER JOIN dl_r2a ON 1 = 1;
END;
$$ LANGUAGE plpgsql;


------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_pkg$translate(aid_jofl_account jofl.account.id_jofl_account%TYPE)
  RETURNS jofl.account.id_account_local%TYPE AS $$
SELECT id_account_local
FROM jofl.account
WHERE id_jofl_account = aid_jofl_account;
$$ LANGUAGE SQL STRICT IMMUTABLE;
------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_pkg$translate(aid_account_local jofl.account.id_account_local%TYPE
  ,                                                aproj             jofl.jf_project.schema_name%TYPE)
  RETURNS jofl.account.id_jofl_account%TYPE AS $$
SELECT a.id_jofl_account
FROM jofl.account a
WHERE a.id_account_local = aid_account_local AND a.schema_name = aproj
UNION ALL
SELECT -1 AS id_jofl_account
WHERE NOT exists(SELECT 1
                 FROM jofl.account a
                 WHERE a.id_account_local = aid_account_local
                       AND a.schema_name = aproj);
$$ LANGUAGE SQL STRICT IMMUTABLE;

------------------------------------------------------------------------------------------------------------------------------------------------
--select * from jofl.jofl_pkg$validate_row('','2;',1)
CREATE OR REPLACE FUNCTION jofl.jofl_pkg$validate_row(atable_name TEXT
  , OUT                                               amsg        TEXT
  , OUT                                               avalid      BOOLEAN
  ,                                                   arowid      TEXT
  ,                                                   ascn        NUMERIC DEFAULT NULL) AS $$
BEGIN
  avalid := TRUE;
  amsg   := NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_pkg$extract_date(a         TEXT,
                                                      allownull BOOLEAN DEFAULT FALSE)
  RETURNS TIMESTAMP WITHOUT TIME ZONE AS $$
DECLARE
  ldt TIMESTAMP WITHOUT TIME ZONE;
BEGIN
  a := replace(a, '+', ' ');
  ldt := a :: TIMESTAMP WITHOUT TIME ZONE;
  IF allownull OR ldt IS NOT NULL
  THEN
    RETURN ldt;
  ELSE
    RAISE data_exception;
  END IF;
  EXCEPTION WHEN invalid_datetime_format
  THEN
    IF allownull
    THEN
      RETURN NULL;
    ELSE
      RAISE data_exception;
    END IF;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION jofl.jofl_pkg$extract_date(attr      TEXT,
                                                      attrname  TEXT,
                                                      allownull BOOLEAN DEFAULT FALSE)
  RETURNS TIMESTAMP WITHOUT TIME ZONE AS $$
DECLARE
  latrval TEXT;
  ldt     TIMESTAMP WITHOUT TIME ZONE;
BEGIN
  SELECT value :: TEXT
  INTO latrval
  FROM json_each_text(attr :: JSON)
  WHERE key = attrname;
  latrval := replace(latrval, '+', ' ');
  ldt := latrval :: TIMESTAMP WITHOUT TIME ZONE;
  IF allownull OR ldt IS NOT NULL
  THEN
    RETURN ldt;
  ELSE
    RAISE data_exception;
  END IF;
  EXCEPTION WHEN invalid_datetime_format
  THEN
    IF allownull
    THEN
      RETURN NULL;
    ELSE
      RAISE data_exception;
    END IF;
END;
$$ LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_pkg$extract_number(attr      TEXT,
                                                        attrname  TEXT,
                                                        allownull BOOLEAN DEFAULT FALSE)
  RETURNS NUMERIC AS $$
DECLARE
  latrval NUMERIC;
BEGIN
  SELECT nullif(value, '') :: NUMERIC
  INTO latrval
  FROM json_each_text(attr :: JSON)
  WHERE key = attrname;

  IF allownull OR latrval IS NOT NULL
  THEN
    RETURN latrval;
  ELSE
    RAISE data_exception;
  END IF;
  EXCEPTION WHEN invalid_datetime_format
  THEN
    IF allownull
    THEN
      RETURN NULL;
    ELSE
      RAISE data_exception;
    END IF;
END;
$$ LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_pkg$extract_varchar(attr      TEXT,
                                                         attrname  TEXT,
                                                         allownull BOOLEAN DEFAULT FALSE)
  RETURNS TEXT AS $$
DECLARE
  latrval TEXT;
BEGIN
  SELECT value :: TEXT
  INTO latrval
  FROM json_each_text(attr :: JSON)
  WHERE key = attrname;

  IF allownull OR latrval IS NOT NULL
  THEN
    RETURN latrval;
  ELSE
    RAISE data_exception;
  END IF;
  EXCEPTION WHEN invalid_datetime_format
  THEN
    IF allownull
    THEN
      RETURN NULL;
    ELSE
      RAISE data_exception;
    END IF;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION jofl.jofl_pkg$get_modify_opts(aid_jofl_account jofl.account.id_jofl_account%TYPE
  ,                                                      adb_method       jofl.window.db_method%TYPE)
  RETURNS NUMERIC AS $$
DECLARE
  lres NUMERIC;
BEGIN
  IF (jofl.jofl_pkg$isrootrole(aid_jofl_account))
  THEN
    RETURN 15;
  ELSE
    WITH t AS (SELECT ac.call_action
               FROM jofl.role2account r2a
                 , jofl.role2call r2c
                 , jofl.win_action_call ac
               WHERE r2a.id_jofl_account = aid_jofl_account AND
                     r2a.id_role = r2c.id_role AND r2c.db_method = adb_method AND
                     r2c.db_method = ac.db_method AND
                     r2c.call_action = ac.call_action AND ac.is_internal = 1)
    SELECT CASE WHEN (exists(SELECT 1
                             FROM t
                             WHERE call_action = 'OF_ROWS'))
      THEN
        ((exists(SELECT 1
                 FROM t
                 WHERE call_action = 'OF_DELETE')) :: INTEGER :: TEXT ||
         (exists(SELECT 1
                 FROM t
                 WHERE call_action = 'OF_INSERT')) :: INTEGER :: TEXT ||
         (exists(SELECT 1
                 FROM t
                 WHERE call_action = 'OF_UPDATE')) :: INTEGER :: TEXT ||
         (exists(SELECT 1
                 FROM t
                 WHERE call_action = 'OF_ROWS')) :: INTEGER :: TEXT) :: BIT(4) :: INTEGER
           ELSE 0 END
    INTO lres;
    RETURN lres;
  END IF;
  EXCEPTION
  WHEN no_data_found
    THEN
      RETURN 0;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
-- jofl.api functions

------------------------------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION jofl.jofl_pkg$of_rows(aid_account NUMERIC,
  OUT                                            arows       REFCURSOR)
  RETURNS REFCURSOR AS $$
BEGIN
  arows := jofl.jofl_pkg$of_rows(aid_account, NULL :: TEXT);
END;
$$ LANGUAGE plpgsql;
-----------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_pkg$of_rows(aid_account NUMERIC
  ,                                              attr        TEXT
  , OUT                                          arows       REFCURSOR)
  RETURNS REFCURSOR AS $$
DECLARE
  lv_proj jofl.window.schema_name%TYPE := jofl.jofl_pkg$extract_varchar(attr,
                                                                        'F_SCHEMA_NAME',
                                                                        TRUE);
BEGIN
  OPEN arows FOR
  SELECT
    db_method            AS "DB_METHOD",
    win_title            AS "WIN_TITLE",
    custom_topcomponent  AS "CUSTOM_TOPCOMPONENT",
    action_factory       AS "ACTION_FACTORY",
    interface_pkg        AS "INTERFACE_PKG",
    n_auth_based         AS "N_AUTH_BASED",
    cache_min            AS "CACHE_MIN",
    icon                 AS "ICON",
    refresh_interval_sec AS "REFRESH_INTERVAL_SEC",
    w.schema_name        AS "SCHEMA_NAME",
    p.row$color          AS "ROW$COLOR",
    w.editor_factory     AS "EDITOR_FACTORY",
    w.custom_jscomponent AS "CUSTOM_JSCOMPONENT"
  FROM jofl.window w
    LEFT JOIN jofl.jf_project p
      ON w.schema_name = p.schema_name
  WHERE ((lv_proj IS NULL) OR
         (lv_proj IS NOT NULL AND w.schema_name = lv_proj))
  ORDER BY w.schema_name
    , w.db_method
    , w.win_title;
END;
$$ LANGUAGE plpgsql;

-----------------------------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION jofl.jofl_pkg$attr_to_rowtype(attr TEXT)
  RETURNS jofl.WINDOW AS $$
DECLARE
  lr jofl.window%ROWTYPE;
BEGIN
  lr.db_method := trim(jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', FALSE));
  lr.win_title := jofl.jofl_pkg$extract_varchar(attr, 'WIN_TITLE', FALSE);
  lr.cache_min := jofl.jofl_pkg$extract_number(attr, 'CACHE_MIN', FALSE);
  lr.refresh_interval_sec := jofl.jofl_pkg$extract_number(attr, 'REFRESH_INTERVAL_SEC', FALSE);

  lr.n_auth_based := jofl.jofl_pkg$extract_boolean(attr, 'N_AUTH_BASED', FALSE);

  -- NULLABLE параметры
  BEGIN
    lr.custom_topcomponent := jofl.jofl_pkg$extract_varchar(attr, 'CUSTOM_TOPCOMPONENT', TRUE);
    EXCEPTION WHEN OTHERS
    THEN
      lr.custom_topcomponent := NULL;
  END;

  lr.custom_jscomponent := jofl.jofl_pkg$extract_varchar(attr, 'CUSTOM_JSCOMPONENT', TRUE);

  BEGIN
    lr.action_factory := jofl.jofl_pkg$extract_varchar(attr, 'ACTION_FACTORY', TRUE);
    EXCEPTION
    WHEN OTHERS
      THEN
        lr.action_factory := NULL;
  END;

  lr.editor_factory := jofl.jofl_pkg$extract_varchar(attr, 'EDITOR_FACTORY', TRUE);

  BEGIN
    lr.interface_pkg := trim(BOTH ' ' FROM jofl.jofl_pkg$extract_varchar(attr, 'INTERFACE_PKG', TRUE));
    EXCEPTION
    WHEN OTHERS
      THEN
        lr.interface_pkg := NULL;
  END;

  BEGIN
    lr.icon := attr('ICON');
    EXCEPTION
    WHEN OTHERS
      THEN
        lr.icon := NULL;
  END;

  BEGIN
    lr.schema_name := jofl.jofl_pkg$extract_varchar(attr, 'SCHEMA_NAME', TRUE);
    EXCEPTION
    WHEN OTHERS
      THEN
        lr.schema_name := NULL;
  END;

  RETURN lr;
END;
$$ LANGUAGE plpgsql;

-----------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_pkg$of_fill_field(aid_account NUMERIC
  ,                                                    attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr              jofl.window%ROWTYPE;
  lv_source_table TEXT := jofl.jofl_pkg$extract_varchar(attr, 'TARGET_TABLE_TEXT');
BEGIN
  lr := jofl.jofl_pkg$attr_to_rowtype(attr);


  IF NOT exists(SELECT 1
                FROM jofl.win_field
                WHERE db_method = lr.db_method)
  THEN
    PERFORM jofl.jofl_gen_pkg$fill_fields(lr.db_method, lv_source_table);
    RETURN jofl.jofl_util$cover_result(
        'Поля успешно заполнены, нажмите "Обновить", далее можно воспользоваться меню "Сгенерировать пакет".');
  ELSE
    RAISE EXCEPTION USING ERRCODE = 20001, MESSAGE = '<<Поля для выбранного окна уже заполнены!>>';
  END IF;

END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_pkg$gson2hashmap(gson TEXT)
  RETURNS TABLE(attr TEXT, attrname TEXT) AS $$
BEGIN
  RETURN QUERY SELECT
                 value AS attr,
                 key   AS attrname
               FROM json_each_text(gson :: JSON);
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION jofl.jofl_pkg$extract_boolean(attr      TEXT,
                                                         attrname  TEXT,
                                                         allownull BOOLEAN DEFAULT FALSE)
  RETURNS INTEGER AS $$
DECLARE
  latrval TEXT;
BEGIN
  SELECT value :: TEXT
  INTO latrval
  FROM json_each_text(attr :: JSON)
  WHERE key = attrname;

  IF NOT allownull AND latrval IS NULL
  THEN
    RAISE data_exception;
  END IF;
  RETURN latrval :: BOOLEAN :: INTEGER;
END;
$$
LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_pkg$get_agile_configuration(aid_jofl_account jofl.account.id_jofl_account%TYPE
  ,                                                              adb_method       jofl.window.db_method%TYPE
  , OUT                                                          atop             REFCURSOR
  , OUT                                                          afield           REFCURSOR
  , OUT                                                          adetail          REFCURSOR
  , OUT                                                          aref             REFCURSOR
  , OUT                                                          acall            REFCURSOR
  , OUT                                                          afilter          REFCURSOR
  , OUT                                                          afilter_opts     REFCURSOR
  , OUT                                                          acall_form       REFCURSOR
  ,                                                              target           TEXT DEFAULT jofl.constant_text(
      'JOFL', 'jofl_pkg$target_platform_openide')
  ,
                                                                 alocale          jofl.ref_locale.lc_locale%TYPE DEFAULT jofl.constant_text(
                                                                     'JOFL', 'jofl_pkg$default_locale'))
  RETURNS RECORD AS $$
DECLARE
  ln_modify_opts NUMERIC;

  ln_auth        jofl.window.n_auth_based%TYPE;
BEGIN
  ln_modify_opts := jofl.jofl_pkg$get_modify_opts(aid_jofl_account, adb_method);

  --- если ничего недоступно (должно быть > 0) - то надо просто выпасть
  IF ln_modify_opts = 0
  THEN
    -- Если не требует авторизации, то можно дать конфигурацию
    SELECT n_auth_based
    INTO ln_auth
    FROM jofl.window
    WHERE db_method = adb_method;

    IF ln_auth = 1
    THEN
      RETURN;
    ELSE
      -- Для целей выдачи данных без авторизации
      -- Дадим права только на select
      --ln_modify_opts := bin_to_num(0, 0, 0, 1);
      ln_modify_opts := 1;
    END IF;
  END IF;

  OPEN atop FOR
  SELECT
    coalesce(nullif(v.lc_value, ''), win_title) AS "WIN_TITLE",
    custom_topcomponent                         AS "CUSTOM_TOPCOMPONENT",
    custom_jscomponent                          AS "CUSTOM_JSCOMPONENT",
    action_factory                              AS "ACTION_FACTORY",
    editor_factory                              AS "EDITOR_FACTORY",
    w.cache_min                                 AS "CACHE_MIN",
    icon                                        AS "ICON",
    ln_modify_opts                              AS "MODIFY_OPTS"
  FROM jofl.window w
    LEFT JOIN (SELECT
                 v.lc_value,
                 v.db_method
               FROM jofl.ref_vocabulary v
               WHERE v.lc_tag = '#title' AND v.lc_locale = alocale) v
      ON w.db_method = v.db_method
  WHERE w.db_method = adb_method;

  OPEN afield FOR
  SELECT
    db_method                                              AS "DB_METHOD",
    field_name                                             AS "FIELD_NAME",
    field_cons_type                                        AS "FIELD_CONS_TYPE",
    field_type                                             AS "FIELD_TYPE",
    coalesce(nullif(v.lc_value, ''), display_title)        AS "DISPLAY_TITLE",
    coalesce(nullif(vd.lc_value, ''), display_description) AS "DISPLAY_DESCRIPTION",
    default_value                                          AS "DEFAULT_VALUE",
    n_order                                                AS "N_ORDER",
    n_visibility                                           AS "N_VISIBILITY",
    is_display_name                                        AS "IS_DISPLAY_NAME",
    f.is_mandatory                                         AS "IS_MANDATORY",
    f.pattern                                              AS "PATTERN",
    f.max_len                                              AS "MAX_LEN",
    f.weight                                               AS "WEIGHT"
  FROM jofl.win_field f
    LEFT JOIN (SELECT
                 v.lc_value,
                 v.lc_key
               FROM jofl.ref_vocabulary v
               WHERE v.lc_tag = '#field' AND v.lc_locale = alocale AND
                     v.db_method = adb_method) v
      ON f.field_name = v.lc_key
    LEFT JOIN (SELECT
                 v.lc_value,
                 v.lc_key
               FROM jofl.ref_vocabulary v
               WHERE v.lc_tag = '#field.description' AND v.lc_locale = alocale AND
                     v.db_method = adb_method) vd
      ON f.field_name = vd.lc_key
  WHERE f.db_method = adb_method
  ORDER BY f.n_order;

  -- #detail +
  OPEN adetail FOR
  SELECT
    coalesce(nullif(v.lc_value, ''), d.action_title) AS "ACTION_TITLE",
    d.detail_db_method                               AS "DETAIL_DB_METHOD",
    w.icon                                           AS "ICON",
    d.is_row_depend                                  AS "IS_ROW_DEPEND"
  FROM jofl.win_action_detail d
    JOIN jofl.window w
      ON d.detail_db_method = w.db_method
    LEFT JOIN (SELECT
                 v.lc_value,
                 v.lc_key
               FROM jofl.ref_vocabulary v
               WHERE v.lc_tag = '#detail' AND v.lc_locale = alocale AND
                     v.db_method = adb_method) v
      ON d.detail_db_method = v.lc_key
  WHERE d.db_method = adb_method AND jofl.jofl_pkg$get_modify_opts(aid_jofl_account, d.detail_db_method) > 0
  ORDER BY d.n_sort_order NULLS LAST;

  OPEN aref FOR
  SELECT
    field_name          AS "FIELD_NAME",
    ref_db_method       AS "REF_DB_METHOD",
    r.is_custom_factory AS "IS_CUSTOM_FACTORY",
    r.ref_factory       AS "REF_FACTORY",
    ref_field           AS "REF_FIELD",
    dst_field           AS "DST_FIELD"
  FROM jofl.win_field_ref r
  WHERE r.db_method = adb_method
  ORDER BY field_name;

  -- #action +
  IF jofl.jofl_pkg$isrootrole(aid_jofl_account)
  THEN
    OPEN acall FOR
    SELECT
      call_action   AS                               "CALL_ACTION",
      coalesce(nullif(v.lc_value, ''), action_title) "ACTION_TITLE",
      is_refresh    AS                               "IS_REFRESH",
      is_win_action AS                               "IS_WIN_ACTION",
      is_row_depend AS                               "IS_ROW_DEPEND",
      call_default  AS                               "CALL_DEFAULT",
      CASE
      WHEN call_default IS NULL
        THEN
          0
      ELSE
        1
      END                                            "IS_CALL_DEFAULT",
      is_internal   AS                               "IS_INTERNAL"
    FROM jofl.win_action_call c
      LEFT JOIN (SELECT
                   v.lc_value,
                   v.lc_key
                 FROM jofl.ref_vocabulary v
                 WHERE v.lc_tag = '#action' AND v.lc_locale = alocale AND
                       v.db_method = adb_method) v
        ON c.call_action = v.lc_key
    WHERE db_method = adb_method AND
          (target = 'JAVASCRIPT' OR is_internal = 0)
    ORDER BY c.n_sort_order NULLS FIRST;
  ELSE
    OPEN acall FOR
    SELECT
      c.call_action                                  AS "CALL_ACTION",
      coalesce(nullif(v.lc_value, ''), action_title) AS "ACTION_TITLE",
      c.is_refresh                                   AS "IS_REFRESH",
      c.is_win_action                                AS "IS_WIN_ACTION",
      c.is_row_depend                                AS "IS_ROW_DEPEND",
      CASE
      WHEN c.call_default IS NULL
        THEN
          0
      ELSE
        1
      END                                               "IS_CALL_DEFAULT",
      c.is_internal                                  AS "IS_INTERNAL"
    FROM jofl.win_action_call c
      JOIN (SELECT DISTINCT r2c.call_action
            FROM jofl.role2call r2c
              , jofl.role2account r2a
            WHERE r2c.db_method = adb_method AND
                  r2c.id_role = r2a.id_role AND
                  r2a.id_jofl_account = aid_jofl_account) g
        ON c.call_action = g.call_action
      LEFT JOIN (SELECT
                   v.lc_value,
                   v.lc_key
                 FROM jofl.ref_vocabulary v
                 WHERE v.lc_tag = '#action' AND v.lc_locale = alocale AND
                       v.db_method = adb_method) v
        ON c.call_action = v.lc_key
    WHERE (target = 'JAVASCRIPT' OR is_internal = 0)
          AND c.db_method = adb_method
    ORDER BY c.n_sort_order NULLS FIRST;
  END IF;

  -- #filter +
  OPEN afilter FOR
  SELECT
    fs.source_class                                AS "FILTER_CLASS",
    initial_param                                  AS "INITIAL_PARAM",
    grid_x                                         AS "GRID_X",
    grid_y                                         AS "GRID_Y",
    coalesce(nullif(v.lc_value, ''), filter_title) AS "FILTER_TITLE"
  FROM jofl.win_filter f
    JOIN jofl.ref_filter r
      ON f.filter_class = r.filter_class
    JOIN jofl.ref_filter_source fs
      ON fs.filter_class = r.filter_class
    LEFT JOIN (SELECT
                 v.lc_value,
                 v.lc_key
               FROM jofl.ref_vocabulary v
               WHERE v.lc_tag = '#filter' AND v.lc_locale = alocale AND
                     v.db_method = adb_method) v
      ON f.id_filter = v.lc_key
  WHERE f.db_method = adb_method
        AND fs.id_platform = target;

  -- #filter.apply +
  -- #filter.reset +
  OPEN afilter_opts FOR
  SELECT
    coalesce(nullif(jofl.localization_pkg$translate(adb_method,
                                                    alocale,
                                                    '#filter.apply'), ''),
             o.but_apply_caption) AS "BUT_APPLY_CAPTION",
    coalesce(nullif(jofl.localization_pkg$translate(adb_method,
                                                    alocale,
                                                    '#filter.reset'), ''),
             o.but_reset_caption) AS "BUT_RESET_CAPTION",
    o.is_reset_present            AS "IS_RESET_PRESENT"
  FROM jofl.win_filter_opts o
  WHERE o.db_method = adb_method;

  -- #action.field +
  OPEN acall_form FOR
  SELECT
    f.call_action                                 AS "CALL_ACTION",
    fs.source_class                               AS "FILTER_CLASS",
    f.initial_param                               AS "INITIAL_PARAM",
    coalesce(nullif(v.lc_key, ''), f.field_title) AS "FIELD_TITLE"
  FROM jofl.win_action_field f
    JOIN jofl.ref_filter_source fs
      ON fs.filter_class = f.filter_class
    JOIN jofl.ref_filter fl
      ON f.filter_class = fl.filter_class

    LEFT JOIN (SELECT
                 v.lc_value,
                 v.lc_key
               FROM jofl.ref_vocabulary v
               WHERE v.lc_tag = '#action.field' AND v.lc_locale = alocale AND
                     v.db_method = adb_method) v
      ON f.id_afield = v.lc_key
  WHERE f.db_method = adb_method
        AND fs.id_platform = target
  ORDER BY f.n_sort_order NULLS LAST
    , f.call_action;

END;
$$ LANGUAGE plpgsql;


------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_pkg$isrootrole(aid_jofl_account jofl.account.id_jofl_account%TYPE)
  RETURNS BOOLEAN AS $$
WITH t AS (SELECT TRUE res
           FROM jofl.role2account r2a
           WHERE r2a.id_role = 'ROOT' AND r2a.id_jofl_account = aid_jofl_account)
SELECT res
FROM t
UNION ALL
SELECT FALSE
WHERE NOT exists(SELECT 1
                 FROM t);
$$ LANGUAGE SQL STRICT IMMUTABLE;

-----------------------------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION jofl.jofl_pkg$attr_to_rowtype(attr TEXT)
  RETURNS jofl.WINDOW AS $$
DECLARE
  lr jofl.window%ROWTYPE;
BEGIN
  lr.db_method := trim(jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', FALSE));
  lr.win_title := jofl.jofl_pkg$extract_varchar(attr, 'WIN_TITLE', TRUE);
  lr.cache_min := jofl.jofl_pkg$extract_number(attr, 'CACHE_MIN', TRUE);
  lr.refresh_interval_sec := jofl.jofl_pkg$extract_number(attr, 'REFRESH_INTERVAL_SEC', TRUE);

  lr.n_auth_based := jofl.jofl_pkg$extract_boolean(attr, 'N_AUTH_BASED', TRUE);

  -- NULLABLE параметры
  BEGIN
    lr.custom_topcomponent := jofl.jofl_pkg$extract_varchar(attr, 'CUSTOM_TOPCOMPONENT', TRUE);
    EXCEPTION WHEN OTHERS
    THEN
      lr.custom_topcomponent := NULL;
  END;

  lr.custom_jscomponent := jofl.jofl_pkg$extract_varchar(attr, 'CUSTOM_JSCOMPONENT', TRUE);

  BEGIN
    lr.action_factory := jofl.jofl_pkg$extract_varchar(attr, 'ACTION_FACTORY', TRUE);
    EXCEPTION
    WHEN OTHERS
      THEN
        lr.action_factory := NULL;
  END;

  lr.editor_factory := jofl.jofl_pkg$extract_varchar(attr, 'EDITOR_FACTORY', TRUE);

  BEGIN
    lr.interface_pkg := trim(BOTH ' ' FROM jofl.jofl_pkg$extract_varchar(attr, 'INTERFACE_PKG', TRUE));
    EXCEPTION
    WHEN OTHERS
      THEN
        lr.interface_pkg := NULL;
  END;

  BEGIN
    lr.icon := attr('ICON');
    EXCEPTION
    WHEN OTHERS
      THEN
        lr.icon := NULL;
  END;

  BEGIN
    lr.schema_name := jofl.jofl_pkg$extract_varchar(attr, 'SCHEMA_NAME', TRUE);
    EXCEPTION
    WHEN OTHERS
      THEN
        lr.schema_name := NULL;
  END;

  RETURN lr;
END;
$$ LANGUAGE plpgsql;

-----------------------------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION jofl.jofl_pkg$of_insert(aid_account NUMERIC
  ,                                                attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.window%ROWTYPE;
BEGIN


  lr := jofl.jofl_pkg$attr_to_rowtype(attr);
  INSERT INTO jofl.window (db_method,
                           win_title,
                           custom_topcomponent,
                           custom_jscomponent,
                           action_factory,
                           editor_factory,
                           interface_pkg,
                           n_auth_based,
                           cache_min,
                           icon,
                           refresh_interval_sec,
                           schema_name)
  VALUES (lr.db_method,
    lr.win_title,
    lr.custom_topcomponent,
    lr.custom_jscomponent,
    lr.action_factory,
    lr.editor_factory,
    lr.interface_pkg,
    lr.n_auth_based,
    lr.cache_min,
    lr.icon,
    lr.refresh_interval_sec,
          lr.schema_name);

  INSERT INTO jofl.win_action_call
  (db_method
    , call_action
    , action_title
    , is_refresh
    , is_internal)
  VALUES
    (lr.db_method
      , 'OF_INSERT'
      , '<<INSERT>>'
      , 0
      , 1);

  INSERT INTO jofl.win_action_call
  (db_method
    , call_action
    , action_title
    , is_refresh
    , is_internal)
  VALUES
    (lr.db_method
      , 'OF_UPDATE'
      , '<<UPDATE>>'
      , 0
      , 1);

  INSERT INTO jofl.win_action_call
  (db_method
    , call_action
    , action_title
    , is_refresh
    , is_internal)
  VALUES
    (lr.db_method
      , 'OF_DELETE'
      , '<<DELETE>>'
      , 0
      , 1);

  INSERT INTO jofl.win_action_call
  (db_method
    , call_action
    , action_title
    , is_refresh
    , is_internal)
  VALUES
    (lr.db_method
      , 'OF_ROWS'
      , '<<SELECT>>'
      , 0
      , 1);

  RETURN jofl.jofl_util$cover_result('Теперь можно воспользоваться меню "Заполнить поля" (Это удобно)');
END;
$$ LANGUAGE plpgsql;
-----------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_pkg$of_update(aid_account NUMERIC
  ,                                                attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.window%ROWTYPE;
BEGIN
  lr := jofl.jofl_pkg$attr_to_rowtype(attr);
  UPDATE jofl.window w
  SET (db_method,
       win_title,
       custom_topcomponent,
       custom_jscomponent,
       action_factory,
       editor_factory,
       interface_pkg,
       n_auth_based,
       cache_min,
       icon,
       refresh_interval_sec,
       schema_name) = (SELECT lr.*)
  WHERE w.db_method = lr.db_method;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;
-----------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_pkg$of_delete(aid_account NUMERIC
  ,                                                attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.window%ROWTYPE;
BEGIN
  lr := jofl.jofl_pkg$attr_to_rowtype(attr);

  DELETE FROM jofl.win_field_ref
  WHERE db_method = lr.db_method;
  DELETE FROM jofl.win_field
  WHERE db_method = lr.db_method;
  DELETE FROM jofl.app_menu
  WHERE db_method = lr.db_method;
  DELETE FROM jofl.win_action_call
  WHERE db_method = lr.db_method;
  DELETE FROM jofl.role2call
  WHERE db_method = lr.db_method;
  DELETE FROM jofl.win_filter
  WHERE db_method = lr.db_method;
  DELETE FROM jofl.WIN_FILTER_OPTS
  WHERE db_method = lr.db_method;
  DELETE FROM jofl.win_action_detail
  WHERE db_method = lr.db_method;
  DELETE FROM jofl.win_action_field
  WHERE db_method = lr.db_method;
  DELETE FROM jofl.window
  WHERE db_method = lr.db_method;

  DELETE FROM jofl.window w
  WHERE w.db_method = lr.db_method;
  RETURN NULL :: TEXT;
END;
$$ LANGUAGE plpgsql;
-----------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_pkg$of_generate_package(aid_account NUMERIC
  ,                                                          attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr      jofl.window%ROWTYPE;
  lv_pack TEXT := upper(jofl.jofl_pkg$extract_varchar(attr, 'TARGET_PACK_TEXT', FALSE));
  lv_obj  TEXT := upper(jofl.jofl_pkg$extract_varchar(attr, 'TARGET_OBJ_TEXT', FALSE));
BEGIN
  lr := jofl.jofl_pkg$attr_to_rowtype(attr);
  lr.interface_pkg := jofl.jofl_gen_pkg$generate_package(lr.db_method,
                                                         lv_pack,
                                                         lv_obj);
  UPDATE jofl.window
  SET interface_pkg = lr.interface_pkg
  WHERE db_method = lr.db_method;
  RETURN jofl.jofl_util$cover_result('Создан пакет: ' || lr.interface_pkg);
END;
$$ LANGUAGE plpgsql;


------------------------------------------------------------------------------------------------------------------------
--select  jofl.jofl_pkg$extract_tarray('{"a":"$array[a,s,s]"}', 'a')
CREATE OR REPLACE FUNCTION jofl.jofl_pkg$extract_tarray(attr      TEXT,
                                                        attrname  TEXT,
                                                        allownull BOOLEAN DEFAULT FALSE)
  RETURNS TEXT [] AS $$
DECLARE
  latrval TEXT [];
BEGIN
  IF attr IS NULL AND NOT allownull
  THEN RAISE data_exception; END IF;
  IF attr IS NULL AND allownull
  THEN RETURN NULL; END IF;

  SELECT string_to_array(replace(replace(value, '$array[', ''), ']', ''), ',')
  INTO latrval:: TEXT []
  FROM json_each_text(attr:: JSON )
  WHERE KEY = attrname;
  RETURN latrval;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------
--select  jofl.jofl_pkg$extract_narray('{"a":"$array[1,2,3]"}', 'a')

CREATE OR REPLACE FUNCTION jofl.jofl_pkg$extract_narray(attr      TEXT,
                                                        attrname  TEXT,
                                                        allownull BOOLEAN DEFAULT FALSE)
  RETURNS NUMERIC [] AS $$
DECLARE
  latrval NUMERIC [];
BEGIN
  IF attr IS NULL AND NOT allownull
  THEN RAISE data_exception; END IF;
  IF attr IS NULL AND allownull
  THEN RETURN NULL; END IF;

  SELECT string_to_array(replace(replace(value, '$array[', ''), ']', ''), ',')
  INTO latrval:: NUMERIC []
  FROM json_each_text(attr:: JSON )
  WHERE KEY = attrname;
  RETURN latrval;
END;
$$ LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------------------------
-- select  jofl.jofl_pkg$extract_darray('{"a":"$array[2016.01.18 08:49,2016.11.18 08:49,2016.09.18 08:49]"}', 'a')

CREATE OR REPLACE FUNCTION jofl.jofl_pkg$extract_darray(attr      TEXT,
                                                        attrname  TEXT,
                                                        allownull BOOLEAN DEFAULT FALSE)
  RETURNS TIMESTAMP WITHOUT TIME ZONE [] AS $$
DECLARE
  latrval TIMESTAMP WITHOUT TIME ZONE [];
BEGIN
  IF attr IS NULL AND NOT allownull
  THEN RAISE data_exception; END IF;
  IF attr IS NULL AND allownull
  THEN RETURN NULL; END IF;

  SELECT string_to_array(replace(replace(value, '$array[', ''), ']', ''), ',')
  INTO latrval:: TIMESTAMP WITHOUT TIME ZONE []
  FROM json_each_text(attr:: JSON )
  WHERE KEY = attrname;
  RETURN latrval;
END;
$$ LANGUAGE plpgsql;