CREATE OR REPLACE FUNCTION jofl.jofl_link_util$build(link TEXT [])
  RETURNS TEXT AS $$
DECLARE
  lv_out TEXT;
BEGIN
  WITH b AS (SELECT
               link [1] "FILEID",
               link [2] "FILENAME"),
      d AS (SELECT row_to_json(b.*)
            FROM b),
      e AS (SELECT
              link [0]         "CAPTION",
              row_to_json(d.*) "FILE"
            FROM d)
  SELECT row_to_json(e.*)
  FROM e
  INTO lv_out;
  RETURN lv_out;
END;
$$ LANGUAGE plpgsql;