﻿create or replace function jofl.jofl_proxy_pkg$set_statement_timeout()
	returns void as $$
declare
begin
		set local statement_timeout to 180000;
end;
$$ language plpgsql;

create or replace function  jofl.jofl_proxy_pkg$do_action(aid_jofl_account jofl.account.id_jofl_account%type
										 ,adb_method    jofl.window.db_method%type
										 ,aaction       text
										 ,av_gson       text
										 ,av_binary     text default null) returns text as $$
	begin
		perform jofl.jofl_proxy_pkg$set_statement_timeout();

		PERFORM jofl.jofl_util$set_usr_var(jofl.const_pkg$id_jofl_account(), aid_jofl_account::TEXT);
		PERFORM jofl.jofl_util$set_usr_var(jofl.const_pkg$jofl_method(), adb_method);
		PERFORM jofl.jofl_util$set_usr_var(jofl.const_pkg$jofl_action(), aaction);

		if jofl.jofl_proxy_pkg$validate_permission(aid_jofl_account, adb_method,aaction)
		then
			return jofl.jofl_proxy_pkg$do_action_(jofl.jofl_pkg$translate(aid_jofl_account),
						      adb_method,
						      aaction,
						      av_gson,
						      av_binary);
		else
			RAISE EXCEPTION USING ERRCODE = jofl.jofl_proxy_pkg$get_error_code(), MESSAGE = '<<Метод недоступен!>>';
		end if;
		return null;
	end ;
$$ LANGUAGE plpgsql;

create or replace function  jofl.jofl_proxy_pkg$get_rows(aid_jofl_account jofl.account.id_jofl_account%type
							,adb_method  jofl.window.db_method%type
							,out arows refcursor
							,av_gson         text
							,apack_method   text default 'OF_ROWS') returns refcursor as $$
	begin
		perform jofl.jofl_proxy_pkg$set_statement_timeout();
		if (jofl.jofl_proxy_pkg$validate_permission(aid_jofl_account,
							    adb_method,
							    apack_method))
		then
			begin
			arows = jofl.jofl_proxy_pkg$get_rows_(jofl.jofl_pkg$translate(aid_jofl_account)::numeric,
							 adb_method::text,
							 av_gson::text,
							 apack_method::text
							 );
			RETURN;
            -- не надо душить исключения!
			-- EXCEPTION WHEN others THEN

			end;
		end if;
		RAISE EXCEPTION USING ERRCODE = jofl.jofl_proxy_pkg$get_error_code(), MESSAGE ='<<Нет доступа! (id: ' || aid_jofl_account ||')>>';
	end ;
$$ LANGUAGE plpgsql;