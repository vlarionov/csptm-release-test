CREATE OR REPLACE FUNCTION jofl.jofl_bin_util$extract_file_id(abinary TEXT, afield_name TEXT)
  RETURNS TEXT AS $$
BEGIN
  RETURN jofl.jofl_pkg$extract_varchar(abinary, afield_name, FALSE);
  EXCEPTION
  WHEN data_exception
    THEN
      RETURN NULL;
END;
$$ LANGUAGE plpgsql;