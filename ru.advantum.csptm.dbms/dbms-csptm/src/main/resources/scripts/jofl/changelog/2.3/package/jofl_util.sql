﻿create or replace function  jofl.jofl_util$tr(asource text) returns text as $$
	begin
		return replace(asource, '"', '\"');
	end ;
$$ LANGUAGE plpgsql;

	-----------------------------------------------------------------------------------------------------------------------------
create or replace function  jofl.jofl_util$cover_result(amsg text) returns text as $$
	begin
		return '{"message": "' || jofl.jofl_util$tr(amsg) || '"}';
	end ;
$$ LANGUAGE plpgsql;

	-----------------------------------------------------------------------------------------------------------------------------
create or replace function  jofl.jofl_util$cover_result(amsg text
							,k1   in text
							,v1   in text) returns text as $$
	begin
		return '{"message": "' || jofl.jofl_util$tr(amsg) || '", "' || k1 || '": "' || jofl.jofl_util$tr(v1) || '" }';
	end ;
$$ LANGUAGE plpgsql;

	-----------------------------------------------------------------------------------------------------------------------------
create or replace function  jofl.jofl_util$cover_result(amsg text
							 ,k1   text
							 ,v1   text
							 ,k2   text
							 ,v2   text) returns text as $$
	begin
		return '{"message": "' || jofl.jofl_util$tr(amsg) || '", "' || k1 || '": "' || jofl.jofl_util$tr(v1) || '", "' || k2 || '":"' || jofl.jofl_util$tr(v2) || '" }';
	end ;
$$ LANGUAGE plpgsql;

	-----------------------------------------------------------------------------------------------------------------------------
create or replace function  jofl.jofl_util$cover_result(k1 text
							,v1 text) returns text as $$
	begin
		return '{"' || k1 || '": "' || jofl.jofl_util$tr(v1) || '" }';
	end ;
$$ LANGUAGE plpgsql;

    -----------------------------------------------------------------------------------------------------------------------------
create or replace function  jofl.jofl_util$cover_result(k1 text
						     ,v1 text
						     ,k2 text
						     ,v2 text) returns text as $$
    begin
        return '{"' || k1 || '": "' || jofl.jofl_util$tr(v1) || '", "' || k2 || '":"' || jofl.jofl_util$tr(v2) || '" }';
    end ;
$$ LANGUAGE plpgsql;

    -----------------------------------------------------------------------------------------------------------------------------
create or replace function  jofl.jofl_util$cover_result(k1 text
						     ,v1 text
						     ,k2 text
						     ,v2 text
						     ,k3 text
						     ,v3 text) returns text as $$
    begin
        return '{"' || k1 || '": "' || jofl.jofl_util$tr(v1) || '", "' || k2 || '":"' || jofl.jofl_util$tr(v2) || '", "' || k3 || '":"' || jofl.jofl_util$tr(v3) || '"}';
	end ;
$$ LANGUAGE plpgsql;


    -----------------------------------------------------------------------------------------------------------------------------

create or replace function  jofl.jofl_util$cover_address(addr text
							,lon  numeric
							,lat  numeric) returns varchar as $$
	begin
		return jofl.jofl_util$cover_result('name', addr, 'pointStr', lon || ' ' || lat);
	end ;
$$ LANGUAGE plpgsql;

	-----------------------------------------------------------------------------------------------------------------------------
create or replace function  jofl.jofl_util$extract_array(attr text,
							attrname text) returns setof text as $$
	DECLARE
		rec record;
		inrec record;
	begin
		for rec in (select value from  json_each_text('{"f1":["A1","A2","A3"]}'::json) where key=attrname )
			loop
				for inrec in (select value from json_array_elements_text(rec.value::json))
					loop
						return next inrec.value;
					end loop;
			end loop;

		return;
	end ;
$$ LANGUAGE plpgsql;

	-----------------------------------------------------------------------------------------------------------------------------
create or replace function  jofl.jofl_util$from_unixtime(unixtime in numeric) returns  timestamp  as $$
	begin
		return to_timestamp(unixtime)  at time zone 'utc';
	end ;
$$ LANGUAGE plpgsql;

  -----------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_util$set_usr_var(p_var_name TEXT, p_var_value TEXT) RETURNS void
LANGUAGE plpgsql
AS $$
BEGIN
  PERFORM set_config(jofl.const_pkg$usr_var_setting() || '.' ||p_var_name, p_var_value, TRUE);
END;
$$;
  -----------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_util$get_usr_var(p_var_name TEXT) RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_var_value TEXT;
BEGIN
  SELECT INTO l_var_value current_setting(jofl.const_pkg$usr_var_setting() || '.' || p_var_name);

  RETURN l_var_value;

	EXCEPTION WHEN undefined_object THEN RETURN NULL;
END;
$$;