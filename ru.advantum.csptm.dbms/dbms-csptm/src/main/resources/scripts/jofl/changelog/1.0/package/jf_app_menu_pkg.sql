CREATE OR REPLACE FUNCTION jofl.jf_app_menu_pkg$of_rows(aid_jofl_account IN        jofl.account.id_jofl_account%TYPE,
                                                                         OUT arows REFCURSOR,
  attr                                                                             TEXT DEFAULT NULL)
  RETURNS REFCURSOR AS $$
BEGIN
  OPEN arows FOR
  SELECT
    src.id_menu        AS "ID_MENU",
    src.menu_title     AS "MENU_TITLE",
    src.id_menu_parent AS "ID_MENU_PARENT",
    src.db_method      AS "DB_METHOD",
    parent.menu_title  AS "PARENT_TITLE",
    src.icon           AS "ICON",
    src.n_order        AS "N_ORDER",
    src.schema_name    AS "SCHEMA_NAME"
  FROM jofl.app_menu src
    LEFT JOIN jofl.app_menu parent
      ON src.id_menu_parent = parent.id_menu;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jofl.jf_app_menu_pkg$get_app_menu(aid_jofl_account IN        jofl.account.id_jofl_account%TYPE,
                                                                              OUT arows REFCURSOR,
  alocale                                                                               jofl.ref_locale.lc_locale%TYPE DEFAULT NULL,
  aproj                                                                                 jofl.app_menu.schema_name%TYPE DEFAULT NULL)
  RETURNS REFCURSOR AS $$
BEGIN
  OPEN arows FOR
  WITH RECURSIVE r AS
  (SELECT
     id_menu,
     id_menu_parent,
     menu_title,
     icon,
     n_order,
     db_method,
     1 AS level
   FROM wins
   WHERE wins.db_method IS NOT NULL
   UNION ALL
   SELECT
     wins.id_menu,
     wins.id_menu_parent,
     wins.menu_title,
     wins.icon,
     wins.n_order,
     wins.db_method,
     r.level + 1 AS level
   FROM wins
     JOIN r
       ON wins.id_menu = r.id_menu_parent),
      wins AS (SELECT DISTINCT
                 id_menu,
                 coalesce(id_menu_parent, 0) id_menu_parent,
                 menu_title,
                 icon,
                 n_order,
                 m.db_method
               FROM jofl.role2account r2a
                 JOIN jofl.role2call r2c
                   ON r2c.id_role = r2a.id_role
                 LEFT JOIN jofl.app_menu m
                   ON (m.db_method = r2c.db_method OR m.db_method IS NULL)
               WHERE r2a.id_jofl_account = aid_jofl_account
                     AND r2c.call_action = 'OF_ROWS'
               UNION
               SELECT
                 NULL id_menu,
                 NULL id_menu_parent,
                 NULL menu_title,
                 NULL icon,
                 NULL n_order,
                 db_method
               FROM jofl.window
               WHERE jofl.jofl_pkg$isrootrole(aid_jofl_account) :: INTEGER = 1)
  SELECT DISTINCT
    w.id_menu        AS "ID_MENU",
    w.menu_title     AS "MENU_TITLE",
    w.db_method      AS "DB_METHOD",
    w.id_menu_parent AS "ID_MENU_PARENT",
    w.icon           AS "ICON",
    w.n_order        AS "N_ORDER"
  FROM r w
  ORDER BY w.n_order NULLS LAST;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_app_menu_pkg$attr_to_rowtype(attr TEXT)
  RETURNS jofl.APP_MENU AS $$
DECLARE
  lr jofl.app_menu%ROWTYPE;
BEGIN
  lr.id_menu := jofl.jofl_pkg$extract_number(attr, 'ID_MENU', TRUE);
  lr.menu_title := jofl.jofl_pkg$extract_varchar(attr, 'MENU_TITLE', TRUE);
  lr.id_menu_parent := jofl.jofl_pkg$extract_varchar(attr,
                                                     'ID_MENU_PARENT',
                                                     TRUE);

  lr.n_order := jofl.jofl_pkg$extract_number(attr,
                                             'N_ORDER',
                                             TRUE);
  lr.icon := jofl.jofl_pkg$extract_varchar(attr,
                                           'ICON',
                                           TRUE);

  lr.db_method := jofl.jofl_pkg$extract_varchar(attr,
                                                'DB_METHOD',
                                                TRUE);

  lr.schema_name := jofl.jofl_pkg$extract_varchar(attr,
                                                  'SCHEMA_NAME',
                                                  TRUE);
  RETURN lr;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION jofl.jf_app_menu_pkg$of_insert(aid_account NUMERIC
  ,                                                       attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.app_menu%ROWTYPE;
BEGIN
  lr := jofl.jf_app_menu_pkg$attr_to_rowtype(attr);
  INSERT INTO jofl.app_menu
  (id_menu,
   menu_title,
   id_menu_parent,
   db_method,
   icon,
   n_order,
   schema_name)
  VALUES (nextval('jofl.sq_menu'),
          lr.menu_title,
          lr.id_menu_parent,
          lr.db_method,
          lr.icon,
          lr.n_order,
          lr.schema_name);
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------------------------------------------------										 
CREATE OR REPLACE FUNCTION jofl.jf_app_menu_pkg$of_update(aid_account NUMERIC
  ,                                                       attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.app_menu%ROWTYPE;
BEGIN
  lr := jofl.jf_app_menu_pkg$attr_to_rowtype(attr);
  UPDATE jofl.app_menu f
  SET
    id_menu        = lr.id_menu,
    menu_title     = lr.menu_title,
    id_menu_parent = lr.id_menu_parent,
    db_method      = lr.db_method,
    icon           = lr.icon,
    n_order        = lr.n_order,
    schema_name    = lr.schema_name
  WHERE f.id_menu = lr.id_menu;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------										 
CREATE OR REPLACE FUNCTION jofl.jf_app_menu_pkg$of_delete(aid_account NUMERIC
  ,                                                       attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.app_menu%ROWTYPE;
BEGIN
  lr := jofl.jf_app_menu_pkg$attr_to_rowtype(attr);
  DELETE FROM jofl.app_menu f
  WHERE f.id_menu = lr.id_menu;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;			
