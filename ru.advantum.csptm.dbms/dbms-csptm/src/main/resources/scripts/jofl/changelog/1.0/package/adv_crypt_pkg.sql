CREATE OR REPLACE FUNCTION jofl.adv_crypt_pkg$md5(TEXT)
  RETURNS TEXT AS $$
SELECT crypt($1, gen_salt('md5'));
$$ LANGUAGE SQL STRICT IMMUTABLE;
-----------------------------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION jofl.adv_crypt_pkg$md5_wtih_seed(md5passwd TEXT,
                                                            seed      TEXT)
  RETURNS TEXT AS $$
SELECT jofl.adv_crypt_pkg$md5(seed || '|' || md5passwd);
$$ LANGUAGE SQL STRICT IMMUTABLE;