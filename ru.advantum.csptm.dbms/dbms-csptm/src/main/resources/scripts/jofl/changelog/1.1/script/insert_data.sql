INSERT INTO jofl.jf_project (schema_name, project_title, row$color) VALUES ('JREP', 'Отчеты', null);
INSERT INTO jofl.jf_project (schema_name, project_title, row$color) VALUES ('JOFL', 'Проект "JOFL"', 15191244);
INSERT INTO jofl.jf_project (schema_name, project_title, row$color) VALUES ('BB', 'Мониторинг', 8764285);
INSERT INTO jofl.jf_project (schema_name, project_title, row$color) VALUES ('TIS', 'Кастомизированные объекты ТИС', null);

INSERT INTO jofl.ref_const_type (field_cons_type, field_const_name) VALUES (0, '(PK) Первичный ключ');
INSERT INTO jofl.ref_const_type (field_cons_type, field_const_name) VALUES (1, '(PK) Редактируемый Первичный ключ');
INSERT INTO jofl.ref_const_type (field_cons_type, field_const_name) VALUES (2, '(READWRITE) Чтение/Запись');
INSERT INTO jofl.ref_const_type (field_cons_type, field_const_name) VALUES (3, '(READONLY) Только для чтения');

INSERT INTO jofl.ref_field_type (field_type, field_type_name) VALUES ('L', 'Линк (hyperlink)');
INSERT INTO jofl.ref_field_type (field_type, field_type_name) VALUES ('R', 'Файл');
INSERT INTO jofl.ref_field_type (field_type, field_type_name) VALUES ('N', 'Целое');
INSERT INTO jofl.ref_field_type (field_type, field_type_name) VALUES ('V', 'Строка');
INSERT INTO jofl.ref_field_type (field_type, field_type_name) VALUES ('F', 'Действительное');
INSERT INTO jofl.ref_field_type (field_type, field_type_name) VALUES ('B', 'Boolean');
INSERT INTO jofl.ref_field_type (field_type, field_type_name) VALUES ('D', 'Дата');
INSERT INTO jofl.ref_field_type (field_type, field_type_name) VALUES ('T', 'Время');
INSERT INTO jofl.ref_field_type (field_type, field_type_name) VALUES ('I', 'Интервал');

INSERT INTO jofl.ref_field_visibility (n_visibility, visibility_name) VALUES (0, 'Нет');
INSERT INTO jofl.ref_field_visibility (n_visibility, visibility_name) VALUES (1, 'Только в Properties');
INSERT INTO jofl.ref_field_visibility (n_visibility, visibility_name) VALUES (2, 'Только в Outline');
INSERT INTO jofl.ref_field_visibility (n_visibility, visibility_name) VALUES (3, 'Везде');

INSERT INTO jofl.ref_filter (filter_class, init_template) VALUES ('NODE', '{"TITLE":"<название>", "PREFIX":"F_", "FACTORY":"<метод>", "FIELD":"<поле>", "WIDTH":"300"}');
INSERT INTO jofl.ref_filter (filter_class, init_template) VALUES ('TEXT', '{"TITLE":"<название>", "PREFIX": "STR_FIELD_", "MANDATORY": "true", "WIDTH": "400" }');
INSERT INTO jofl.ref_filter (filter_class, init_template) VALUES ('NUMBER', '{"TITLE":"<название>", "PREFIX": "F_", "DEFAULT_VALUE": "1", "MANDATORY":"true", "MIN":"0", "MAX":"100", "STEP":"1", "FRACTION":"1"}');
INSERT INTO jofl.ref_filter (filter_class, init_template) VALUES ('DATE', '{"TITLE":"<название>", "PREFIX": "END_", "MANDATORY": "true",  "DEFAULT_DATE": "END_OF_MONTH"}');
INSERT INTO jofl.ref_filter (filter_class, init_template) VALUES ('TIME', '{"TITLE":"<название>", "PREFIX": "END_", "MANDATORY": "true",  "DEFAULT_DATE": "END_OF_MONTH"}');
INSERT INTO jofl.ref_filter (filter_class, init_template) VALUES ('TREE', '{"TITLE":"<название>", "PREFIX":"F_", "FACTORY":"<метод>", "FIELD":"<поле>", "ID_FIELD":"<поле>", "ID_PARENT_FIELD":"<поле>", "WIDTH":"300"}');
INSERT INTO jofl.ref_filter (filter_class, init_template) VALUES ('TIME_ONLY', '{"TITLE":"<название>", "PREFIX": "END_", "MANDATORY": "true",  "DEFAULT_TIME": "END_OF_DATE"}');
INSERT INTO jofl.ref_filter (filter_class, init_template) VALUES ('INPLACE', '{"TITLE":"<название>", "PREFIX": "F_", "MANDATORY": "true",  "INPLACE": [ {"K": "KEY", "V":"VALUE"}  ]}');


INSERT INTO jofl.ref_filter_source (id_platform, filter_class, source_class) VALUES ('OPENIDE', 'NODE', 'ru.advantum.agilebeans.service.filter.NodeFilter');
INSERT INTO jofl.ref_filter_source (id_platform, filter_class, source_class) VALUES ('OPENIDE', 'TEXT', 'ru.advantum.agilebeans.service.filter.TextFilter');
INSERT INTO jofl.ref_filter_source (id_platform, filter_class, source_class) VALUES ('OPENIDE', 'NUMBER', 'ru.advantum.agilebeans.service.filter.NumberFilter');
INSERT INTO jofl.ref_filter_source (id_platform, filter_class, source_class) VALUES ('OPENIDE', 'DATE', 'ru.advantum.agilebeans.service.filter.DateFilter');
INSERT INTO jofl.ref_filter_source (id_platform, filter_class, source_class) VALUES ('OPENIDE', 'TIME', 'ru.advantum.agilebeans.service.filter.TimeFilter');
INSERT INTO jofl.ref_filter_source (id_platform, filter_class, source_class) VALUES ('JAVASCRIPT', 'TREE', 'jsTree');
INSERT INTO jofl.ref_filter_source (id_platform, filter_class, source_class) VALUES ('JAVASCRIPT', 'TIME', 'jsTime');
INSERT INTO jofl.ref_filter_source (id_platform, filter_class, source_class) VALUES ('JAVASCRIPT', 'DATE', 'jsDate');
INSERT INTO jofl.ref_filter_source (id_platform, filter_class, source_class) VALUES ('JAVASCRIPT', 'NUMBER', 'jsNumber');
INSERT INTO jofl.ref_filter_source (id_platform, filter_class, source_class) VALUES ('JAVASCRIPT', 'TEXT', 'jsText');
INSERT INTO jofl.ref_filter_source (id_platform, filter_class, source_class) VALUES ('JAVASCRIPT', 'NODE', 'jsNode');
INSERT INTO jofl.ref_filter_source (id_platform, filter_class, source_class) VALUES ('JAVASCRIPT', 'TIME_ONLY', 'jsTimeOnly');
INSERT INTO jofl.ref_filter_source (id_platform, filter_class, source_class) VALUES ('JAVASCRIPT', 'INPLACE', 'jsInplace');
INSERT INTO jofl.ref_filter_source (id_platform, filter_class, source_class) VALUES ('OPENIDE', 'INPLACE', 'ru.advantum.agilebeans.service.filter.InplaceKVFilter');

INSERT INTO jofl.ref_locale (lc_locale, rowid) VALUES ('en_EN', '1188');
INSERT INTO jofl.ref_locale (lc_locale, rowid) VALUES ('RUR  ', '3094');


INSERT INTO jofl.ref_locale_tag (lc_tag, rowid, tag_name) VALUES ('#bound', '1284', 'Текст');
INSERT INTO jofl.ref_locale_tag (lc_tag, rowid, tag_name) VALUES ('#title', '1285', 'Заголовок окна');
INSERT INTO jofl.ref_locale_tag (lc_tag, rowid, tag_name) VALUES ('#field', '1286', 'Поле');
INSERT INTO jofl.ref_locale_tag (lc_tag, rowid, tag_name) VALUES ('#detail', '1287', 'Деталь');
INSERT INTO jofl.ref_locale_tag (lc_tag, rowid, tag_name) VALUES ('#action', '1288', 'Метод');
INSERT INTO jofl.ref_locale_tag (lc_tag, rowid, tag_name) VALUES ('#action.field', '1289', 'Поле формы метода');
INSERT INTO jofl.ref_locale_tag (lc_tag, rowid, tag_name) VALUES ('#filter.apply', '1290', 'Фильтр: применить');
INSERT INTO jofl.ref_locale_tag (lc_tag, rowid, tag_name) VALUES ('#filter.reset', '1291', 'Фильтр: сбросить');
INSERT INTO jofl.ref_locale_tag (lc_tag, rowid, tag_name) VALUES ('#filter', '1292', 'Фильтр');
INSERT INTO jofl.ref_locale_tag (lc_tag, rowid, tag_name) VALUES ('#field.description', '1293', 'Описание поля');

INSERT INTO jofl.ref_ui_platform (id_platform, plat_name, rowid) VALUES ('OPENIDE', 'Приложение Netbeans Platform', '3088');
INSERT INTO jofl.ref_ui_platform (id_platform, plat_name, rowid) VALUES ('JAVASCRIPT', 'Веб приложение', '3089');

