create or replace function jofl.jf_ref_vocabulary_pkg$attr_to_rowtype(attr text) 
		returns table(scn numeric,
			      db_method jofl.ref_vocabulary.db_method%type,
			      lc_locale jofl.ref_vocabulary.lc_locale%type,
			      lc_tag jofl.ref_vocabulary.lc_tag%type,
			      lc_key jofl.ref_vocabulary.lc_key%type,
			      lc_value jofl.ref_vocabulary.lc_value%type,
			      rowid jofl.ref_vocabulary.rowid%type)  as $$
	begin
		return query 
			select 	null::numeric scn,
				jofl.jofl_pkg$extract_varchar(attr,'DB_METHOD',false) as db_method,
				jofl.jofl_pkg$extract_varchar(attr, 'LC_LOCALE',false)::char(5) as lc_locale,
				jofl.jofl_pkg$extract_varchar(attr, 'LC_TAG',false) as lc_tag,
				jofl.jofl_pkg$extract_varchar(attr, 'LC_KEY', true) as lc_key,
				jofl.jofl_pkg$extract_varchar(attr, 'LC_VALUE',false) as lc_value,
				jofl.jofl_pkg$extract_varchar(attr, 'ROWID', true) as rowid;
	end;
$$ LANGUAGE plpgsql;