CREATE OR REPLACE FUNCTION jofl.date_format(adate TIMESTAMP WITHOUT TIME ZONE)
  RETURNS TEXT AS $$
SELECT to_char(adate, 'YYYY-MM-DD HH24:MI:SS') || '.' || substring(to_char(adate, 'MS') FROM 1 FOR 1);
$$ LANGUAGE SQL STRICT IMMUTABLE;