﻿CREATE TABLE jofl.constants_text
(
  schema_name TEXT NOT NULL,
  c_name      TEXT NOT NULL,
  c_val       TEXT,
  ref_type    TEXT NOT NULL,
  CONSTRAINT pk_constants_text PRIMARY KEY (c_name) USING INDEX TABLESPACE jofl_idx
)
TABLESPACE jofl_data;

COMMENT ON COLUMN jofl.constants_text.schema_name
IS 'наименование схемы';
COMMENT ON COLUMN jofl.constants_text.c_name
IS 'название константы';
COMMENT ON COLUMN jofl.constants_text.c_val
IS 'значение константы';
COMMENT ON COLUMN jofl.constants_text.ref_type
IS 'ссылка на поле типа константы';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.constants_num
(
  schema_name TEXT NOT NULL,
  c_name      TEXT NOT NULL,
  c_val       NUMERIC,
  ref_type    TEXT NOT NULL,
  CONSTRAINT pk_constants_num PRIMARY KEY (c_name) USING INDEX TABLESPACE jofl_idx
)
TABLESPACE jofl_data;
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.jf_project
(
  schema_name   TEXT NOT NULL,
  project_title TEXT NOT NULL,
  row$color     NUMERIC(18),
  CONSTRAINT pk_jf_project PRIMARY KEY (schema_name) USING INDEX TABLESPACE jofl_idx
)
TABLESPACE jofl_data;

COMMENT ON COLUMN jofl.jf_project.schema_name
IS 'наименование схемы в которую будут генерироваться объекты';
COMMENT ON COLUMN jofl.jf_project.project_title
IS 'наименование прокта';
COMMENT ON COLUMN jofl.jf_project.row$color
IS 'подсветка проекта';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.account
(
  id_jofl_account  NUMERIC(18) NOT NULL,
  schema_name      TEXT        NOT NULL,
  id_account_local NUMERIC(18) NOT NULL,
  CONSTRAINT pk_account PRIMARY KEY (id_jofl_account) USING INDEX TABLESPACE jofl_idx,
  FOREIGN KEY (schema_name) REFERENCES jofl.jf_project (schema_name) DEFERRABLE INITIALLY DEFERRED
)
TABLESPACE jofl_data;

COMMENT ON COLUMN jofl.account.id_jofl_account
IS 'ИД Учетной записи JOFL';
COMMENT ON COLUMN jofl.account.schema_name
IS 'Наименование схемы в которую будут генерироваться объекты';
COMMENT ON COLUMN jofl.account.id_account_local
IS 'ИД Учетной записи внешней системы';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.window
(
  db_method            TEXT                  NULL,
  win_title            TEXT                  NULL,
  custom_topcomponent  TEXT,
  custom_jscomponent   TEXT,
  action_factory       TEXT,
  editor_factory       TEXT,
  interface_pkg        TEXT,
  n_auth_based         NUMERIC(1)            NOT NULL,
  cache_min            NUMERIC(18) DEFAULT 0 NOT NULL,
  icon                 TEXT,
  refresh_interval_sec NUMERIC(18) DEFAULT 0 NOT NULL,
  schema_name          TEXT,
  CONSTRAINT pk_window PRIMARY KEY (db_method) USING INDEX TABLESPACE jofl_idx,
  FOREIGN KEY (schema_name) REFERENCES jofl.jf_project (schema_name) DEFERRABLE INITIALLY DEFERRED
)
TABLESPACE jofl_data;

COMMENT ON COLUMN jofl.window.db_method
IS 'Наименование метода';
COMMENT ON COLUMN jofl.window.custom_jscomponent
IS 'JS-Класс';
COMMENT ON COLUMN jofl.window.action_factory
IS 'Класс фабрики Action';
COMMENT ON COLUMN jofl.window.editor_factory
IS 'Фабрика редактора';
COMMENT ON COLUMN jofl.window.interface_pkg
IS 'Пакет отвечающий за работу объекта';
COMMENT ON COLUMN jofl.window.n_auth_based
IS '0 - без использования параметра ID_ACCOUNT, 1 - с использованием параметра';
COMMENT ON COLUMN jofl.window.cache_min
IS 'Кол-во минут которое будет работать кэш';
COMMENT ON COLUMN jofl.window.icon
IS 'имя иконки';
COMMENT ON COLUMN jofl.window.refresh_interval_sec
IS 'Автоматически обновлять данные (сек)';
COMMENT ON COLUMN jofl.window.schema_name
IS 'Наименование схемы в которую будут генерироваться объекты';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.ref_const_type
(
  field_cons_type  NUMERIC(1) NOT NULL,
  field_const_name TEXT       NOT NULL,
  CONSTRAINT pk_ref_const_type PRIMARY KEY (field_cons_type) USING INDEX TABLESPACE jofl_idx
)
TABLESPACE jofl_data;

COMMENT ON COLUMN jofl.ref_const_type.field_cons_type
IS 'Тип поля: 0 - PK, 1 - PK_WRITEABLE, 1 - RW, 2 - RO';
COMMENT ON COLUMN jofl.ref_const_type.field_const_name
IS 'Наименование Constraint';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.ref_field_type
(
  field_type      CHAR(1) NOT NULL,
  field_type_name TEXT    NOT NULL,
  CONSTRAINT pk_ref_field_type PRIMARY KEY (field_type) USING INDEX TABLESPACE jofl_idx
)
TABLESPACE jofl_data;
COMMENT ON COLUMN jofl.ref_field_type.field_type
IS 'Тип данных: N, V, T, D';
COMMENT ON COLUMN jofl.ref_field_type.field_type_name
IS 'Наименование типа данных';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.ref_field_visibility
(
  n_visibility    NUMERIC(1) NOT NULL,
  visibility_name TEXT       NOT NULL,
  CONSTRAINT pk_ref_field_visibility PRIMARY KEY (n_visibility) USING INDEX TABLESPACE jofl_idx
)
TABLESPACE jofl_data;
COMMENT ON COLUMN jofl.ref_field_visibility.n_visibility
IS 'Видимость поля: 0 - NONE, 1 - PROP, 2 - OUTLINE, 3 - BOTH';
COMMENT ON COLUMN jofl.ref_field_visibility.visibility_name
IS 'Наименование видимости';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.win_field
(
  db_method           TEXT                  NOT NULL,
  field_name          TEXT                  NOT NULL,
  field_cons_type     NUMERIC(1)            NOT NULL,
  field_type          CHAR(1)               NOT NULL,
  display_title       TEXT                  NOT NULL,
  display_description TEXT,
  default_value       TEXT,
  n_order             NUMERIC(18)           NOT NULL,
  n_visibility        NUMERIC(1)            NOT NULL,
  is_display_name     NUMERIC(1) DEFAULT 0  NOT NULL,
  is_mandatory        NUMERIC(1) DEFAULT 0  NOT NULL,
  pattern             TEXT,
  max_len             NUMERIC(18),
  weight              NUMERIC(18) DEFAULT 1 NOT NULL,
  rowid               TEXT DEFAULT nextval('jofl.sq_jofl') :: TEXT,
  CONSTRAINT pk_win_field PRIMARY KEY (field_name, db_method)
  USING INDEX
  TABLESPACE jofl_idx,
  FOREIGN KEY (field_cons_type)
  REFERENCES jofl.ref_const_type (field_cons_type)
  DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY (field_type)
  REFERENCES jofl.ref_field_type (field_type)
  DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY (n_visibility)
  REFERENCES jofl.ref_field_visibility (n_visibility)
  DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY (db_method)
  REFERENCES jofl.window (db_method)
  DEFERRABLE INITIALLY DEFERRED
)
TABLESPACE jofl_data;

COMMENT ON COLUMN jofl.win_field.db_method
IS 'Наименование метода';
COMMENT ON COLUMN jofl.win_field.field_name
IS 'Имя поля в БД ';
COMMENT ON COLUMN jofl.win_field.field_cons_type
IS 'Тип поля: 0 - PK, 1 - PK_WRITEABLE, 1 - RW, 2 - RO';
COMMENT ON COLUMN jofl.win_field.field_type
IS 'Тип данных: N, V, T, D';
COMMENT ON COLUMN jofl.win_field.display_title
IS 'Наименование объекта';
COMMENT ON COLUMN jofl.win_field.display_description
IS 'Описание объекта';
COMMENT ON COLUMN jofl.win_field.default_value
IS 'Значение по умолчанию';
COMMENT ON COLUMN jofl.win_field.n_order
IS 'Порядок полей';
COMMENT ON COLUMN jofl.win_field.n_visibility
IS 'Видимость поля: 0 - NONE, 1 - PROP, 2 - OUTLINE, 3 - BOTH';
COMMENT ON COLUMN jofl.win_field.is_display_name
IS 'Является DisplayName';
COMMENT ON COLUMN jofl.win_field.is_mandatory
IS 'Признак обязательности поля';
COMMENT ON COLUMN jofl.win_field.pattern
IS 'Формат поля';
COMMENT ON COLUMN jofl.win_field.max_len
IS 'Длина значения в символах';
COMMENT ON COLUMN jofl.win_field.weight
IS 'Вес поля';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.win_field_ref
(
  db_method         TEXT                    NOT NULL,
  field_name        TEXT                    NOT NULL,
  ref_db_method     TEXT,
  ref_field         TEXT                    NOT NULL,
  dst_field         TEXT                    NOT NULL,
  is_custom_factory NUMERIC(1, 0) DEFAULT 0 NOT NULL,
  ref_factory       TEXT,
  rowid             TEXT DEFAULT nextval('jofl.sq_jofl') :: TEXT,
  CONSTRAINT pk_win_field_ref PRIMARY KEY (db_method, field_name, dst_field)
  USING INDEX
  TABLESPACE jofl_idx,
  FOREIGN KEY (ref_db_method)
  REFERENCES jofl.window (db_method)
  DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY (field_name, db_method)
  REFERENCES jofl.win_field (field_name, db_method)
)
TABLESPACE jofl_data;

-- add comments to the columns
COMMENT ON COLUMN jofl.win_field_ref.db_method
IS 'Наименование метода';
COMMENT ON COLUMN jofl.win_field_ref.field_name
IS 'Наименование поля';
COMMENT ON COLUMN jofl.win_field_ref.ref_db_method
IS 'Наименование справочника';
COMMENT ON COLUMN jofl.win_field_ref.ref_field
IS 'Имя поля справочника';
COMMENT ON COLUMN jofl.win_field_ref.dst_field
IS 'Поле объекта';
COMMENT ON COLUMN jofl.win_field_ref.is_custom_factory
IS 'Признак того, что будет использована Custom фабрика';
COMMENT ON COLUMN jofl.win_field_ref.ref_factory
IS 'Класс (сервис AgileCustomPropertyEditorProvider)';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE jofl.app_menu
(
  id_menu        NUMERIC(18) NOT NULL,
  menu_title     TEXT        NOT NULL,
  id_menu_parent NUMERIC(18),
  db_method      TEXT,
  icon           TEXT,
  n_order        NUMERIC(18),
  schema_name    TEXT,
  CONSTRAINT pk_app_menu PRIMARY KEY (id_menu) USING INDEX TABLESPACE jofl_idx,
  FOREIGN KEY (schema_name) REFERENCES jofl.jf_project (schema_name) DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY (id_menu_parent) REFERENCES jofl.app_menu (id_menu) DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY (db_method) REFERENCES jofl.window (db_method) DEFERRABLE INITIALLY DEFERRED
)
TABLESPACE jofl_data;

COMMENT ON COLUMN jofl.app_menu.id_menu
IS 'ИД Меню';
COMMENT ON COLUMN jofl.app_menu.menu_title
IS 'Наименование меню';
COMMENT ON COLUMN jofl.app_menu.id_menu_parent
IS 'Меню родитель';
COMMENT ON COLUMN jofl.app_menu.db_method
IS 'Наименование метода';
COMMENT ON COLUMN jofl.app_menu.icon
IS 'имя иконки';
COMMENT ON COLUMN jofl.app_menu.n_order
IS 'Порядок сортировки';
COMMENT ON COLUMN jofl.app_menu.schema_name
IS 'Наименование схемы в которую будут генерироваться объекты';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.create$java$lob$table
(
  name     TEXT,
  lob      BYTEA,
  loadtime TIMESTAMP WITHOUT TIME ZONE,
  CONSTRAINT pku_createjavalobtable UNIQUE (name) USING INDEX TABLESPACE jofl_data
)
TABLESPACE jofl_data;

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.java$options
(
  what  TEXT,
  opt   TEXT,
  value TEXT
)
TABLESPACE jofl_data;

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.ref_editor
(
  ref_factory TEXT
)
TABLESPACE jofl_data;
COMMENT ON COLUMN jofl.ref_editor.ref_factory
IS 'Класс (сервис AgileCustomPropertyEditorProvider)';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.ref_filter
(
  filter_class  TEXT NOT NULL,
  init_template TEXT,
  CONSTRAINT pk_ref_filter PRIMARY KEY (filter_class) USING INDEX TABLESPACE jofl_idx
)
TABLESPACE jofl_data;

COMMENT ON COLUMN jofl.ref_filter.filter_class
IS 'Класс фабрики фильтров';
COMMENT ON COLUMN jofl.ref_filter.init_template
IS 'Шаблон параметров инициализации';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.ref_ui_platform
(
  id_platform TEXT NOT NULL,
  plat_name   TEXT NOT NULL,
  rowid       TEXT DEFAULT nextval('jofl.sq_jofl') :: TEXT,
  CONSTRAINT pk_ref_ui_platform PRIMARY KEY (id_platform) USING INDEX TABLESPACE jofl_idx
)
TABLESPACE jofl_data;

COMMENT ON COLUMN jofl.ref_ui_platform.id_platform
IS 'Платформа';
COMMENT ON COLUMN jofl.ref_ui_platform.plat_name
IS 'Наименование';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.ref_filter_source
(
  id_platform  TEXT NOT NULL,
  filter_class TEXT NOT NULL,
  source_class TEXT NOT NULL,
  CONSTRAINT pk_ref_filter_source PRIMARY KEY (id_platform, filter_class) USING INDEX TABLESPACE jofl_idx,
  FOREIGN KEY (id_platform) REFERENCES jofl.ref_ui_platform (id_platform) DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY (filter_class) REFERENCES jofl.ref_filter (filter_class) DEFERRABLE INITIALLY DEFERRED
)
TABLESPACE jofl_data;

COMMENT ON COLUMN jofl.ref_filter_source.id_platform
IS 'Платформа';
COMMENT ON COLUMN jofl.ref_filter_source.filter_class
IS 'Класс фабрики фильтров';
COMMENT ON COLUMN jofl.ref_filter_source.source_class
IS 'Ссылка на java/js класс фильтра';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.ref_locale
(
  lc_locale CHAR(5) NOT NULL,
  rowid     TEXT DEFAULT nextval('jofl.sq_jofl') :: TEXT,
  CONSTRAINT pk_ref_locale PRIMARY KEY (lc_locale) USING INDEX TABLESPACE jofl_idx
)
TABLESPACE jofl_data;

COMMENT ON TABLE jofl.ref_locale
IS 'Поддерживаемые языки';
COMMENT ON COLUMN jofl.ref_locale.lc_locale
IS 'Language and Country';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.ref_locale_tag
(
  lc_tag   TEXT NOT NULL,
  rowid    TEXT DEFAULT nextval('jofl.sq_jofl') :: TEXT,
  tag_name TEXT NOT NULL,
  CONSTRAINT pk_ref_locale_tag PRIMARY KEY (lc_tag)
  USING INDEX
  TABLESPACE jofl_idx
)
TABLESPACE jofl_data;

COMMENT ON TABLE jofl.ref_locale_tag
IS 'Типы ключей для локализации';
COMMENT ON COLUMN jofl.ref_locale_tag.lc_tag
IS 'Тип';
COMMENT ON COLUMN jofl.ref_locale_tag.tag_name
IS 'Наименование';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.ref_role
(
  id_role   TEXT       NOT NULL,
  role_name TEXT       NOT NULL,
  is_active NUMERIC(1) NOT NULL,
  CONSTRAINT pk_jofl_role PRIMARY KEY (id_role)
  USING INDEX
  TABLESPACE jofl_idx
)
TABLESPACE jofl_data;

COMMENT ON COLUMN jofl.ref_role.id_role
IS 'ИД роли';
COMMENT ON COLUMN jofl.ref_role.role_name
IS 'Наименование роли';
COMMENT ON COLUMN jofl.ref_role.is_active
IS 'Активна';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------


CREATE TABLE jofl.ref_vocabulary
(
  db_method TEXT    NOT NULL,
  lc_locale CHAR(5) NOT NULL,
  lc_tag    TEXT    NOT NULL,
  lc_key    TEXT,
  lc_value  TEXT    NOT NULL,
  rowid     TEXT DEFAULT nextval('jofl.sq_jofl') :: TEXT,
  CONSTRAINT uq_ref_vocabulary_translate UNIQUE (db_method, lc_locale, lc_tag, lc_key)
  USING INDEX
  TABLESPACE jofl_idx,
  FOREIGN KEY (lc_locale)
  REFERENCES jofl.ref_locale (lc_locale)
  DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY (lc_tag)
  REFERENCES jofl.ref_locale_tag (lc_tag)
  DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY (db_method)
  REFERENCES jofl.window (db_method)
  DEFERRABLE INITIALLY DEFERRED
)
TABLESPACE jofl_data;

COMMENT ON TABLE jofl.ref_vocabulary
IS 'Словарь';
COMMENT ON COLUMN jofl.ref_vocabulary.db_method
IS 'Наименование метода';
COMMENT ON COLUMN jofl.ref_vocabulary.lc_locale
IS 'Language and Country';
COMMENT ON COLUMN jofl.ref_vocabulary.lc_tag
IS 'Тип';
COMMENT ON COLUMN jofl.ref_vocabulary.lc_key
IS 'Ключ';
COMMENT ON COLUMN jofl.ref_vocabulary.lc_value
IS 'Значение';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.role2account
(
  id_role         TEXT        NOT NULL,
  id_jofl_account NUMERIC(18) NOT NULL,
  CONSTRAINT pk_role2account PRIMARY KEY (id_role, id_jofl_account)
  USING INDEX
  TABLESPACE jofl_idx,
  FOREIGN KEY (id_role)
  REFERENCES jofl.ref_role (id_role)
  DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY (id_jofl_account)
  REFERENCES jofl.account (id_jofl_account)
  DEFERRABLE INITIALLY DEFERRED
)
TABLESPACE jofl_data;

COMMENT ON COLUMN jofl.role2account.id_role
IS 'ИД роли';
COMMENT ON COLUMN jofl.role2account.id_jofl_account
IS 'ИД Учетной записи JOFL';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.win_action_call
(
  db_method         TEXT                 NOT NULL,
  call_action       TEXT                 NOT NULL,
  action_title      TEXT                 NOT NULL,
  is_refresh        NUMERIC(1) DEFAULT 0 NOT NULL,
  is_internal       NUMERIC(1) DEFAULT 0 NOT NULL,
  is_win_action     NUMERIC(1) DEFAULT 0 NOT NULL,
  n_sort_order      NUMERIC(18),
  is_row_depend     NUMERIC(1) DEFAULT 0 NOT NULL,
  call_default      TEXT,
  opt_interface_pkg TEXT,
  CONSTRAINT pk_win_action_call PRIMARY KEY (db_method, call_action)
  USING INDEX
  TABLESPACE jofl_idx,
  FOREIGN KEY (db_method)
  REFERENCES jofl.window (db_method)
  DEFERRABLE INITIALLY DEFERRED
)
TABLESPACE jofl_data;

COMMENT ON COLUMN jofl.win_action_call.db_method
IS 'Наименование метода';
COMMENT ON COLUMN jofl.win_action_call.call_action
IS 'Наименование метода (DO_ACTION)';
COMMENT ON COLUMN jofl.win_action_call.action_title
IS 'Отображение ACTION';
COMMENT ON COLUMN jofl.win_action_call.is_refresh
IS 'Требуется обновление';
COMMENT ON COLUMN jofl.win_action_call.is_internal
IS 'Внутренний вызов (INSERT, UPDATE, DELETE)';
COMMENT ON COLUMN jofl.win_action_call.is_win_action
IS 'Метод окна';
COMMENT ON COLUMN jofl.win_action_call.n_sort_order
IS 'Порядок сортировки';
COMMENT ON COLUMN jofl.win_action_call.is_row_depend
IS 'Строкозависимый';
COMMENT ON COLUMN jofl.win_action_call.call_default
IS 'Значения по умолчанию';
COMMENT ON COLUMN jofl.win_action_call.opt_interface_pkg
IS 'Пакет (опционально)';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.role2call
(
  id_role     TEXT NOT NULL,
  db_method   TEXT NOT NULL,
  call_action TEXT NOT NULL,
  CONSTRAINT pk_role2call PRIMARY KEY (id_role, db_method, call_action)
  USING INDEX
  TABLESPACE jofl_idx,
  FOREIGN KEY (id_role)
  REFERENCES jofl.ref_role (id_role)
  DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY (db_method, call_action)
  REFERENCES jofl.win_action_call (db_method, call_action)
  DEFERRABLE INITIALLY DEFERRED
)
TABLESPACE jofl_data;

COMMENT ON COLUMN jofl.role2call.id_role
IS 'ИД роли';
COMMENT ON COLUMN jofl.role2call.db_method
IS 'Наименование метода';
COMMENT ON COLUMN jofl.role2call.call_action
IS 'Наименование метода (DO_ACTION)';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.win_action_detail
(
  db_method        TEXT                  NOT NULL,
  action_title     TEXT                  NOT NULL,
  detail_db_method TEXT                  NOT NULL,
  is_row_depend    NUMERIC(1) DEFAULT 0  NOT NULL,
  n_sort_order     NUMERIC(18) DEFAULT 0 NOT NULL,
  CONSTRAINT pk_win_detail PRIMARY KEY (db_method, detail_db_method)
  USING INDEX
  TABLESPACE jofl_idx,
  FOREIGN KEY (db_method)
  REFERENCES jofl.window (db_method)
  DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY (detail_db_method)
  REFERENCES jofl.window (db_method)
  DEFERRABLE INITIALLY DEFERRED
)
TABLESPACE jofl_data;

COMMENT ON COLUMN jofl.win_action_detail.db_method
IS 'Наименование метода';
COMMENT ON COLUMN jofl.win_action_detail.action_title
IS 'Отображение ACTION';
COMMENT ON COLUMN jofl.win_action_detail.detail_db_method
IS 'Наименование метода';
COMMENT ON COLUMN jofl.win_action_detail.is_row_depend
IS 'Строкозависимая деталь';
COMMENT ON COLUMN jofl.win_action_detail.n_sort_order
IS 'Порядок сортировки';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.role2detail
(
  id_role          TEXT NOT NULL,
  db_method        TEXT NOT NULL,
  detail_db_method TEXT NOT NULL,
  CONSTRAINT pk_role2detail PRIMARY KEY (id_role, db_method, detail_db_method)
  USING INDEX
  TABLESPACE jofl_data,
  FOREIGN KEY (id_role)
  REFERENCES jofl.ref_role (id_role)
  DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY (db_method, detail_db_method)
  REFERENCES jofl.win_action_detail (db_method, detail_db_method)
  DEFERRABLE INITIALLY DEFERRED
)
TABLESPACE jofl_data;

COMMENT ON COLUMN jofl.role2detail.id_role
IS 'ИД роли';
COMMENT ON COLUMN jofl.role2detail.db_method
IS 'Наименование метода';
COMMENT ON COLUMN jofl.role2detail.detail_db_method
IS 'Наименование метода';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.win_action_field
(
  id_afield     TEXT NOT NULL,
  db_method     TEXT NOT NULL,
  call_action   TEXT NOT NULL,
  filter_class  TEXT NOT NULL,
  initial_param TEXT,
  n_sort_order  NUMERIC(18),
  field_title   TEXT,
  rowid         TEXT DEFAULT nextval('jofl.sq_jofl') :: TEXT,
  CONSTRAINT pk_win_action_field PRIMARY KEY (id_afield)
  USING INDEX
  TABLESPACE jofl_idx,
  FOREIGN KEY (db_method, call_action)
  REFERENCES jofl.win_action_call (db_method, call_action)
  DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY (filter_class)
  REFERENCES jofl.ref_filter (filter_class)
  DEFERRABLE INITIALLY DEFERRED
)
TABLESPACE jofl_data;
COMMENT ON COLUMN jofl.win_action_field.db_method
IS 'Наименование метода';
COMMENT ON COLUMN jofl.win_action_field.call_action
IS 'Наименование метода (DO_ACTION)';
COMMENT ON COLUMN jofl.win_action_field.filter_class
IS 'Класс фабрики фильтров';
COMMENT ON COLUMN jofl.win_action_field.initial_param
IS 'Параметр инициализации (JSON-MAP)';
COMMENT ON COLUMN jofl.win_action_field.n_sort_order
IS 'Порядок сортировки';
COMMENT ON COLUMN jofl.win_action_field.field_title
IS 'Наименование фильтра';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.win_filter
(
  db_method     TEXT        NOT NULL,
  filter_class  TEXT        NOT NULL,
  initial_param TEXT,
  grid_x        NUMERIC(18) NOT NULL,
  grid_y        NUMERIC(18) NOT NULL,
  filter_title  TEXT,
  id_filter     TEXT        NOT NULL,
  rowid         TEXT DEFAULT nextval('jofl.sq_jofl') :: TEXT,
  CONSTRAINT pk_win_filter PRIMARY KEY (id_filter, db_method)
  USING INDEX
  TABLESPACE jofl_idx,
  FOREIGN KEY (filter_class)
  REFERENCES jofl.ref_filter (filter_class)
  DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY (db_method)
  REFERENCES jofl.window (db_method)
  DEFERRABLE INITIALLY DEFERRED
)
TABLESPACE jofl_data;

COMMENT ON COLUMN jofl.win_filter.db_method
IS 'Наименование метода';
COMMENT ON COLUMN jofl.win_filter.filter_class
IS 'Класс фабрики фильтров';
COMMENT ON COLUMN jofl.win_filter.initial_param
IS 'Параметр инициализации (JSON-MAP)';
COMMENT ON COLUMN jofl.win_filter.grid_x
IS 'Положение Х';
COMMENT ON COLUMN jofl.win_filter.grid_y
IS 'Положение Y';
COMMENT ON COLUMN jofl.win_filter.filter_title
IS 'Наименование поля';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.win_filter_opts
(
  db_method         TEXT       NOT NULL,
  but_apply_caption TEXT       NOT NULL,
  but_reset_caption TEXT       NOT NULL,
  is_reset_present  NUMERIC(1) NOT NULL,
  rowid             TEXT DEFAULT nextval('jofl.sq_jofl') :: TEXT,
  CONSTRAINT pk_win_filter_opts PRIMARY KEY (db_method)
  USING INDEX
  TABLESPACE jofl_idx,
  FOREIGN KEY (db_method)
  REFERENCES jofl.window (db_method)
  DEFERRABLE INITIALLY DEFERRED
)
TABLESPACE jofl_data;

COMMENT ON COLUMN jofl.win_filter_opts.db_method
IS 'Наименование метода';
COMMENT ON COLUMN jofl.win_filter_opts.but_apply_caption
IS 'Текст "Apply"';
COMMENT ON COLUMN jofl.win_filter_opts.but_reset_caption
IS 'Текст "Reset"';
COMMENT ON COLUMN jofl.win_filter_opts.is_reset_present
IS 'Видно Reset';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE jofl.ref_menu_vocabulary
(
  lc_locale  CHAR(5)        NOT NULL,
  id_menu    NUMERIC(18, 0) NOT NULL,
  rowid      TEXT DEFAULT nextval('jofl.sq_jofl') :: TEXT,
  menu_title TEXT           NOT NULL,
  CONSTRAINT pk_ref_menu_vocabulary PRIMARY KEY (lc_locale, id_menu) USING INDEX TABLESPACE jofl_idx,
  FOREIGN KEY (lc_locale) REFERENCES jofl.ref_locale (lc_locale),
  FOREIGN KEY (id_menu) REFERENCES jofl.app_menu (id_menu)
)
TABLESPACE jofl_data;
COMMENT ON COLUMN jofl.ref_menu_vocabulary.lc_locale
IS
'Language and Country';
COMMENT ON COLUMN jofl.ref_menu_vocabulary.id_menu
IS
'ИД Меню';
COMMENT ON COLUMN jofl.ref_menu_vocabulary.menu_title
IS
'Название';