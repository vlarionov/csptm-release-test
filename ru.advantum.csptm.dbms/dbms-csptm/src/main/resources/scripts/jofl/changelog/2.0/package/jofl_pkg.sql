create or replace function  jofl.jofl_pkg$register_account(aid_account_local jofl.account.id_account_local%type
    				  			   ,aproj jofl.account.schema_name%type) returns  jofl.account.id_jofl_account%type  AS $$
    declare
    	ln_jofl_account jofl.account.id_jofl_account%type;
    begin
    select a.id_jofl_account
    		into strict ln_jofl_account
    		from jofl.account a
    		where a.id_account_local = aid_account_local /*and a.schema_name = aproj*/;

    		return ln_jofl_account;
    	exception
    		when no_data_found then
    			insert into jofl.account
    				(id_jofl_account
    				,schema_name
    				,id_account_local)
    			values
    				( nextval('jofl.sq_jofl')
    				,aproj
    				,aid_account_local)
    			returning id_jofl_account into ln_jofl_account;
    			return ln_jofl_account;
    end;
    $$ LANGUAGE plpgsql;

    -----------------------------------------------------------------------------------------------------------------------------------------------
create or replace function  jofl.jofl_pkg$get_modify_opts(aid_jofl_account jofl.account.id_jofl_account%type
							 ,adb_method  jofl.window.db_method%type) returns numeric as $$
declare
	lres numeric;
begin
	if (jofl.jofl_pkg$isrootrole(aid_jofl_account))
		then
			return 15;
		else
      with t as (select ac.call_action
                 from jofl.role2call       r2c
                   ,jofl.win_action_call ac
                 where r2c.id_role = ANY (jofl.jofl_pkg$get_account_roles(aid_jofl_account))
											 and r2c.db_method =  adb_method and
                       r2c.db_method = ac.db_method and
                       r2c.call_action = ac.call_action and ac.is_internal = 1)
      select case when (exists (select 1 from t where call_action='OF_ROWS')) then
        ((exists (select 1 from t where call_action='OF_DELETE'))::integer::text||
         (exists (select 1 from t where call_action='OF_INSERT'))::integer::text||
         (exists (select 1 from t where call_action='OF_UPDATE'))::integer::text||
         (exists (select 1 from t where call_action='OF_ROWS'))::integer::text)::bit(4)::integer
             else 0 end
				   into lres;
				   return lres;
		end if;
	exception
		when no_data_found then
			return 0;
END;
$$ LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------------------------------------------------
create or replace function  jofl.jofl_pkg$get_agile_configuration(aid_jofl_account jofl.account.id_jofl_account%type
								 ,adb_method       jofl.window.db_method%type
								 ,out atop         refcursor
								 ,out afield       refcursor
								 ,out adetail      refcursor
								 ,out aref         refcursor
								 ,out acall        refcursor
								 ,out afilter      refcursor
								 ,out afilter_opts refcursor
								 ,out acall_form   refcursor
								 ,target           text default jofl.constant_text('JOFL', 'jofl_pkg$target_platform_openide')
								 ,alocale          jofl.ref_locale.lc_locale%type default jofl.constant_text('JOFL', 'jofl_pkg$default_locale'))
									returns record as $$
declare
		ln_modify_opts numeric;

		ln_auth jofl.window.n_auth_based%type;
	begin
	ln_modify_opts := jofl.jofl_pkg$get_modify_opts(aid_jofl_account, adb_method);

		--- если ничего недоступно (должно быть > 0) - то надо просто выпасть
		if ln_modify_opts = 0 then
			-- Если не требует авторизации, то можно дать конфигурацию
			select n_auth_based
			into ln_auth
			from jofl.window
			where db_method = adb_method;

			if ln_auth = 1	then
				return;
			else
				-- Для целей выдачи данных без авторизации
				-- Дадим права только на select
				--ln_modify_opts := bin_to_num(0, 0, 0, 1);
				ln_modify_opts := 1;
			end if;
		end if;

		open atop for
			select coalesce(nullif(v.lc_value,''), win_title) as	"WIN_TITLE"
                   ,custom_topcomponent	as	"CUSTOM_TOPCOMPONENT"
                   ,custom_jscomponent	as	"CUSTOM_JSCOMPONENT"
                   ,action_factory	as	"ACTION_FACTORY"
                   ,editor_factory	as	"EDITOR_FACTORY"
                   ,w.cache_min	as	"CACHE_MIN"
                   ,icon	as	"ICON"
                   ,ln_modify_opts as	"MODIFY_OPTS"
			from jofl.window w
				left join (select v.lc_value
						,v.db_method
					  from jofl.ref_vocabulary v
					  where v.lc_tag = '#title' and v.lc_locale = alocale) v
				on w.db_method = v.db_method
				where w.db_method = adb_method;

		open afield for
			select
				db_method					as 	"DB_METHOD"
				,field_name					as 	"FIELD_NAME"
				,field_cons_type			as 	"FIELD_CONS_TYPE"
				,field_type					as 	"FIELD_TYPE"
				,coalesce(nullif(v.lc_value,''), display_title) as "DISPLAY_TITLE"
				,coalesce(nullif(vd.lc_value,''), display_description)  as 	"DISPLAY_DESCRIPTION"
				,default_value				as 	"DEFAULT_VALUE"
				,n_order					as 	"N_ORDER"
				,n_visibility				as 	"N_VISIBILITY"
				,is_display_name				as 	"IS_DISPLAY_NAME"
				,f.is_mandatory				as 	"IS_MANDATORY"
				,f.pattern					as 	"PATTERN"
				,f.max_len					as 	"MAX_LEN"
				,f.weight					as 	"WEIGHT"
			from jofl.win_field f
				left join (select v.lc_value
						  ,v.lc_key
						from jofl.ref_vocabulary v
						where v.lc_tag = '#field' and v.lc_locale = alocale and
									v.db_method = adb_method) v
					on f.field_name = v.lc_key
				left join (select v.lc_value
						  ,v.lc_key
						from jofl.ref_vocabulary v
						where v.lc_tag = '#field.description' and v.lc_locale = alocale and
									v.db_method = adb_method) vd
					on f.field_name = vd.lc_key
			where f.db_method = adb_method
			order by f.n_order;

		-- #detail +
		open adetail for
			select coalesce(nullif(v.lc_value,''), d.action_title) as "ACTION_TITLE"
				   ,d.detail_db_method	AS	    	"DETAIL_DB_METHOD"
				   ,w.icon	AS	    	"ICON"
				   ,d.is_row_depend	AS	"IS_ROW_DEPEND"
			from jofl.win_action_detail d
				join jofl.window w
					on  d.detail_db_method = w.db_method
				left join (select v.lc_value
						,v.lc_key
					   from jofl.ref_vocabulary v
						where v.lc_tag = '#detail' and v.lc_locale = alocale and
								v.db_method = adb_method) v
					on d.detail_db_method = v.lc_key
			where d.db_method = adb_method and jofl.jofl_pkg$get_modify_opts(aid_jofl_account, d.detail_db_method) > 0
			order by d.n_sort_order nulls last;

		open aref for
			select field_name	AS	"FIELD_NAME"
                   ,ref_db_method	AS	"REF_DB_METHOD"
                   ,r.is_custom_factory	AS	"IS_CUSTOM_FACTORY"
                   ,r.ref_factory	AS	"REF_FACTORY"
                   ,ref_field	AS	"REF_FIELD"
                   ,dst_field	AS	"DST_FIELD"
			from jofl.win_field_ref r
			where r.db_method = adb_method
			order by field_name;


		-- #action +
		if jofl.jofl_pkg$isrootrole(aid_jofl_account)
		then
			open acall for
				select
					call_action	AS	"CALL_ACTION"
					,coalesce(nullif(v.lc_value,''), action_title) "ACTION_TITLE"
					,is_refresh	AS	"IS_REFRESH"
					,is_win_action	AS	"IS_WIN_ACTION"
					,is_row_depend	AS	"IS_ROW_DEPEND"
					,call_default	AS	"CALL_DEFAULT"
					,case
						 when call_default is null then
							0
						 else
							1
					 end  "IS_CALL_DEFAULT"
					,is_internal	AS	"IS_INTERNAL"
				from jofl.win_action_call c
					left join (select v.lc_value
						   	 ,v.lc_key
						    from jofl.ref_vocabulary v
						    where v.lc_tag = '#action' and v.lc_locale = alocale and
										v.db_method = adb_method) v
						on  c.call_action = v.lc_key
				where db_method = adb_method and
							(target = 'JAVASCRIPT' or is_internal = 0)
				order by c.n_sort_order nulls first;
		else
			open acall for
				select c.call_action	AS	"CALL_ACTION"
                       ,coalesce(nullif(v.lc_value,''), action_title) as "ACTION_TITLE"
                       ,c.is_refresh		as "IS_REFRESH"
                       ,c.is_win_action		as "IS_WIN_ACTION"
                       ,c.is_row_depend		as "IS_ROW_DEPEND"
                       ,case
                       	 when c.call_default is null then
                       		0
                       	 else
                       		1
                        end "IS_CALL_DEFAULT"
                       ,c.is_internal	as "IS_INTERNAL"
				from jofl.win_action_call c
					join (select distinct r2c.call_action
						from jofl.role2call    r2c
						where r2c.db_method = adb_method and
							r2c.id_role = ANY (jofl.jofl_pkg$get_account_roles(aid_jofl_account))
							 ) g
						on c.call_action = g.call_action
					left join (select v.lc_value
							,v.lc_key
						from jofl.ref_vocabulary v
						where v.lc_tag = '#action' and v.lc_locale = alocale and
									v.db_method = adb_method) v
						on c.call_action = v.lc_key
				where (target = 'JAVASCRIPT' or is_internal = 0)
				 and c.db_method = adb_method
				order by c.n_sort_order nulls first;
		end if;


		-- #filter +
		open afilter for
			select fs.source_class as "FILTER_CLASS"
                   ,initial_param	as	"INITIAL_PARAM"
                   ,grid_x	as	"GRID_X"
                   ,grid_y	as	"GRID_Y"
                   ,coalesce(nullif(v.lc_value,''), filter_title) as	"FILTER_TITLE"
			from jofl.win_filter f
				join jofl.ref_filter r
					on f.filter_class = r.filter_class
				join jofl.ref_filter_source fs
					on fs.filter_class = r.filter_class
				left join (select v.lc_value
					   	 ,v.lc_key
					   from jofl.ref_vocabulary v
					   where v.lc_tag = '#filter' and v.lc_locale = alocale and
							v.db_method = adb_method) v
					on f.id_filter = v.lc_key
			where f.db_method = adb_method
						 and fs.id_platform = target;


		-- #filter.apply +
		-- #filter.reset +
		open afilter_opts for
			select coalesce(nullif(jofl.localization_pkg$translate(adb_method,
								   alocale,
								   '#filter.apply'),''),
								 o.but_apply_caption) as "BUT_APPLY_CAPTION"
			      ,coalesce(nullif(jofl.localization_pkg$translate(adb_method,
							      alocale,
							      '#filter.reset'),''),
			       o.but_reset_caption) as "BUT_RESET_CAPTION"
			      ,o.is_reset_present as "IS_RESET_PRESENT"
			from jofl.win_filter_opts o
			where o.db_method = adb_method;

		-- #action.field +
		open acall_form for
			select f.call_action as "CALL_ACTION"
				,fs.source_class as "FILTER_CLASS"
				,f.initial_param as "INITIAL_PARAM"
				,coalesce(nullif(v.lc_key,''), f.field_title) as "FIELD_TITLE"
			from jofl.win_action_field f
				join jofl.ref_filter_source fs
					on fs.filter_class = f.filter_class
				join jofl.ref_filter fl
					on f.filter_class = fl.filter_class

				left join (select v.lc_value
					   	 ,v.lc_key
					   from jofl.ref_vocabulary v
						where v.lc_tag = '#action.field' and v.lc_locale = alocale and
									v.db_method = adb_method) v
					on f.id_afield = v.lc_key
			where f.db_method = adb_method
						and fs.id_platform = target
			order by f.n_sort_order nulls last
				,f.call_action;

	end;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_pkg$get_account_roles(aid_jofl_account jofl.account.id_jofl_account%TYPE)
  RETURNS TEXT []
LANGUAGE plpgsql
AS $$
DECLARE
  l_get_roles_func TEXT;
  l_result         TEXT [];
BEGIN
  SELECT grf.function
  INTO l_get_roles_func
  FROM jofl.get_roles_func grf
    JOIN jofl.account a ON grf.schema_name = a.schema_name
  WHERE a.id_jofl_account = aid_jofl_account;

  IF l_get_roles_func IS NULL
  THEN
    SELECT array_agg(r2a.id_role)
    INTO l_result
    FROM jofl.role2account r2a
    WHERE r2a.id_jofl_account = aid_jofl_account;
  ELSE
    EXECUTE 'SELECT ' || l_get_roles_func || '($1);'
    INTO l_result
    USING aid_jofl_account;
  END IF;

  RETURN l_result;
END;
$$;

	------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_pkg$isrootrole(aid_jofl_account jofl.account.id_jofl_account%TYPE)
  RETURNS BOOLEAN
LANGUAGE plpgsql
STRICT IMMUTABLE
AS $$
DECLARE
  l_result BOOLEAN;
BEGIN
  SELECT 'ROOT' = ANY (jofl.jofl_pkg$get_account_roles(aid_jofl_account))
  INTO l_result;

  RETURN l_result;
END;
$$;