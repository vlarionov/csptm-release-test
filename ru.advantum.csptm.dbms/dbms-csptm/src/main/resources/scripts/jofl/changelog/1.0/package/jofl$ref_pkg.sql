CREATE OR REPLACE FUNCTION jofl.jofl$ref_pkg$attr_to_rowtype(attr TEXT)
  RETURNS jofl.WIN_FIELD_REF AS $$
DECLARE
  lr jofl.win_field_ref%ROWTYPE;
BEGIN
  lr.field_name := jofl.jofl_pkg$extract_varchar(attr, 'FIELD_NAME', FALSE);
  lr.db_method  := jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', FALSE);
  lr.ref_field  := jofl.jofl_pkg$extract_varchar(attr, 'REF_FIELD', FALSE);
  lr.rowid := jofl.jofl_pkg$extract_varchar(attr, 'ROWID', TRUE);
  lr.ref_db_method := jofl.jofl_pkg$extract_varchar(attr, 'REF_DB_METHOD', TRUE);
  lr.dst_field := jofl.jofl_pkg$extract_varchar(attr, 'DST_FIELD', FALSE);
  lr.is_custom_factory := jofl.jofl_pkg$extract_boolean(attr, 'IS_CUSTOM_FACTORY') :: INT :: NUMERIC;
  lr.ref_factory := jofl.jofl_pkg$extract_varchar(attr, 'REF_FACTORY', TRUE);
  RETURN lr;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl$ref_pkg$of_rows(aid_account NUMERIC
  , OUT                                              arows       REFCURSOR
  ,                                                  attr        TEXT)
  RETURNS REFCURSOR AS $$
DECLARE
  lv_dbmethod jofl.win_field.db_method%TYPE := jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', FALSE);
  lv_field    jofl.win_field.field_name%TYPE := jofl.jofl_pkg$extract_varchar(attr, 'FIELD_NAME', FALSE);
BEGIN
  OPEN arows FOR
  SELECT
    rowid                               AS "ROWID",
    db_method                           AS "DB_METHOD",
    field_name                          AS "FIELD_NAME",
    ref_db_method                       AS "REF_DB_METHOD",
    ref_field                           AS "REF_FIELD",
    dst_field                           AS "DST_FIELD",
    is_custom_factory :: INT :: BOOLEAN AS "IS_CUSTOM_FACTORY",
    ref_factory                         AS "REF_FACTORY"
  FROM jofl.win_field_ref f
  WHERE f.db_method = lv_dbmethod
        AND f.field_name = lv_field;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl$ref_pkg$of_insert(aid_account NUMERIC, attr TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.win_field_ref%ROWTYPE;
BEGIN
  lr := jofl.jofl$ref_pkg$attr_to_rowtype(attr);
  lr.rowid := nextval('jofl.sq_jofl');
  INSERT INTO jofl.win_field_ref SELECT lr.*;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl$ref_pkg$of_update(aid_account NUMERIC, attr TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.win_field_ref%ROWTYPE;
BEGIN
  lr := jofl.jofl$ref_pkg$attr_to_rowtype(attr);
  UPDATE jofl.win_field_ref
  SET
    db_method         = lr.db_method,
    field_name        = lr.field_name,
    ref_db_method     = lr.ref_db_method,
    ref_field         = lr.ref_field,
    dst_field         = lr.dst_field,
    is_custom_factory = lr.is_custom_factory,
    ref_factory       = lr.ref_factory
  WHERE rowid = lr.rowid;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl$ref_pkg$of_delete(aid_account NUMERIC, attr TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.win_field_ref%ROWTYPE;
BEGIN
  lr := jofl.jofl$ref_pkg$attr_to_rowtype(attr);
  DELETE FROM jofl.win_field_ref
  WHERE rowid = lr.rowid;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;