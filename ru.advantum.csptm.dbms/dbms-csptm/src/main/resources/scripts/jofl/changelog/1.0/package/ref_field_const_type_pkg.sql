CREATE OR REPLACE FUNCTION jofl.ref_field_const_type_pkg$of_rows(aid_account NUMERIC
  , OUT                                                          arows       REFCURSOR
  ,                                                              attr        TEXT DEFAULT '{}')
  RETURNS REFCURSOR AS $$
BEGIN
  OPEN arows FOR
  SELECT
    field_cons_type  AS "FIELD_CONS_TYPE",
    field_const_name AS "FIELD_CONST_NAME"
  FROM jofl.ref_const_type
  ORDER BY field_cons_type;
END;
$$ LANGUAGE plpgsql;