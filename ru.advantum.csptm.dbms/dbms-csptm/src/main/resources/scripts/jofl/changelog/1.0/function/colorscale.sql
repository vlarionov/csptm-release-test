CREATE OR REPLACE FUNCTION jofl.ColorScale(nArg REAL, -- аргумент
                                           nMin REAL DEFAULT 0, -- мин. значение
                                           nMax REAL DEFAULT 7, -- макс. значение
                                           sClr TEXT DEFAULT '01234567', -- 2-8 цветов для перехода (символы '0'..'7')
                                           nBrt INTEGER DEFAULT 180          -- яркость (-255..+255)
)
  RETURNS INTEGER AS $$
DECLARE
  MC INTEGER;
  OC INTEGER;
  R1 INTEGER;
  G1 INTEGER;
  B1 INTEGER;
  R2 INTEGER;
  G2 INTEGER;
  B2 INTEGER;
  X  REAL;
  NX INTEGER;
  C1 INTEGER;
  C2 INTEGER;
  CX INTEGER;
BEGIN
  IF nArg < 0 OR nMin < 0 OR nMax <= nMin OR NOT regexp_like(sClr, '^[0-7]{2,8}$')
     OR nBrt NOT BETWEEN -255 AND 255
  THEN RAISE EXCEPTION USING ERRCODE = 20100, MESSAGE = 'Ошибка в параметрах'; END IF;

  MC := Least(nBrt, 0) + 255; -- яркость главных компонентов
  OC := Greatest(nBrt, 0); -- яркость остальных компонентов
  X  := Least(CASE nMin
              WHEN 0
                THEN nArg / nMax
              ELSE Ln(Greatest(nArg / nMin, 1)) / Ln(nMax / nMin) END, 0.999999) * (Length(sClr) - 1); -- 0..1
  NX := Trunc(X);
  X := X - NX; -- 0..1

  C1 := Substr(sClr, NX + 1, 1); -- цвет "из"
  C2 := Substr(sClr, NX + 2, 1); -- цвет "в"
  R1 := CASE C1 & 4
        WHEN 0
          THEN OC
        ELSE MC END; -- компоненты цвета "из"
  G1 := CASE C1 & 2
        WHEN 0
          THEN OC
        ELSE MC END;
  B1 := CASE C1 & 1
        WHEN 0
          THEN OC
        ELSE MC END;
  R2 := CASE C2 & 4
        WHEN 0
          THEN OC
        ELSE MC END; -- компоненты цвета "в"
  G2 := CASE C2 & 2
        WHEN 0
          THEN OC
        ELSE MC END;
  B2 := CASE C2 & 1
        WHEN 0
          THEN OC
        ELSE MC END;
  CX := Round(R1 * (1.0 - X) + R2 * X) + (Round(G1 * (1.0 - X) + G2 * X) + Round(B1 * (1.0 - X) + B2 * X) * 256) * 256;

  --dbms_output.put_line('MC='||MC||', OC='||OC||', X='||To_Char(X, '0.9999')||', NX='||NX||', C1='||C1||', C2='||C2||', CX='||To_Char(CX, 'XXXXXXXX'));
  RETURN cx;
END;
$$ LANGUAGE plpgsql;