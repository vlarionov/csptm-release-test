CREATE OR REPLACE FUNCTION jofl.jf_ref_role_pkg$attr_to_rowtype(attr TEXT)
  RETURNS jofl.REF_ROLE AS $$
DECLARE
  lr jofl.ref_role%ROWTYPE;
BEGIN
  lr.id_role   := jofl.jofl_pkg$extract_varchar(attr, 'ID_ROLE', FALSE);
  lr.role_name := jofl.jofl_pkg$extract_varchar(attr, 'ROLE_NAME', FALSE);
  lr.is_active := jofl.jofl_pkg$extract_boolean(attr, 'IS_ACTIVE');
  RETURN lr;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION jofl.jf_ref_role_pkg$of_insert(aid_account NUMERIC,
                                                          attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.ref_role%ROWTYPE;
BEGIN
  lr := jofl.jf_ref_role_pkg$attr_to_rowtype(attr);
  INSERT INTO jofl.ref_role SELECT lr.*;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_ref_role_pkg$of_update(aid_account NUMERIC,
                                                          attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.ref_role%ROWTYPE;
BEGIN
  lr := jofl.jf_ref_role_pkg$attr_to_rowtype(attr);
  UPDATE jofl.ref_role f
  SET
    id_role   = lr.id_role,
    role_name = lr.role_name,
    is_active = lr.is_active
  WHERE f.id_role = lr.id_role;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_ref_role_pkg$of_delete(aid_account NUMERIC,
                                                          attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.ref_role%ROWTYPE;
BEGIN
  lr := jofl.jf_ref_role_pkg$attr_to_rowtype(attr);
  DELETE FROM jofl.ref_role
  WHERE id_role = lr.id_role;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_ref_role_pkg$of_rows(aid_account IN        NUMERIC
  ,                                                                 OUT arows REFCURSOR
  , attr                                                                      TEXT)
  RETURNS REFCURSOR AS $$
BEGIN
  OPEN arows FOR
  SELECT
    id_role   AS "ID_ROLE",
    role_name AS "ROLE_NAME",
    is_active AS "IS_ACTIVE"
  FROM jofl.ref_role src
  ORDER BY id_role;
END;
$$ LANGUAGE plpgsql;	

