CREATE OR REPLACE FUNCTION jofl.jofl_metadata_pkg$get_data_description(
  aid_jofl_account jofl.account.id_jofl_account%TYPE,
  adb_method       jofl.window.db_method%TYPE)
  RETURNS TABLE(pkg    jofl.window.interface_pkg%TYPE
  ,             n_auth jofl.window.n_auth_based%TYPE) AS $$
BEGIN
  RETURN QUERY
  SELECT
    CASE WHEN w.n_auth_based = 1 AND
              NOT jofl.jofl_proxy_pkg$validate_permission(aid_jofl_account, adb_method, 'OF_ROWS')
      THEN NULL
    ELSE w.interface_pkg END
                      pkg,
    w.n_auth_based AS n_auth
  FROM jofl.window w
  WHERE w.db_method = adb_method;

END;
$$ LANGUAGE plpgsql;