﻿CREATE OR REPLACE FUNCTION jofl.jofl$call_pkg$attr_to_rowtype(attr TEXT)
  RETURNS jofl.WIN_ACTION_CALL AS $$
DECLARE
  lr jofl.win_action_call%ROWTYPE;
BEGIN
  lr.opt_interface_pkg := jofl.jofl_pkg$extract_varchar(attr, 'OPT_INTERFACE_PKG', TRUE);
  lr.call_action := jofl.jofl_pkg$extract_varchar(attr, 'CALL_ACTION', FALSE);
  lr.db_method := jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', FALSE);
  lr.action_title := jofl.jofl_pkg$extract_varchar(attr, 'ACTION_TITLE', FALSE);
  lr.call_default := jofl.jofl_pkg$extract_varchar(attr, 'CALL_DEFAULT', TRUE);
  lr.is_refresh := jofl.jofl_pkg$extract_boolean(attr, 'IS_REFRESH');
  lr.is_internal := jofl.jofl_pkg$extract_boolean(attr, 'IS_INTERNAL');
  lr.is_win_action := jofl.jofl_pkg$extract_boolean(attr, 'IS_WIN_ACTION');
  lr.n_sort_order := jofl.jofl_pkg$extract_number(attr, 'N_SORT_ORDER', TRUE);
  lr.is_row_depend := jofl.jofl_pkg$extract_boolean(attr, 'IS_ROW_DEPEND');
  RETURN lr;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl$call_pkg$of_rows(aid_account IN NUMERIC, arows REFCURSOR)
  RETURNS REFCURSOR AS $$
DECLARE
  l_dbmethod jofl.window.db_method%TYPE := jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', TRUE);
BEGIN

  OPEN arows FOR
  SELECT
    db_method    AS DB_METHOD,
    call_action  AS "CALL_ACTION",
    action_title AS "ACTION_TITLE",
    is_refresh   AS "IS_REFRESH",
    is_internal  AS "IS_INTERNAL"
  FROM jofl.win_action_call
  WHERE (l_dbmethod IS NULL OR l_dbmethod = db_method);
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl$call_pkg$of_rows(aid_account IN NUMERIC, OUT arows REFCURSOR, attr TEXT)
  RETURNS REFCURSOR AS $$
DECLARE
  lv_dbmethod jofl.win_field.db_method%TYPE := jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', FALSE);
BEGIN
  OPEN arows FOR
  SELECT
    db_method                  AS "DB_METHOD",
    call_action                AS "CALL_ACTION",
    action_title               AS "ACTION_TITLE",
    is_refresh                 AS "IS_REFRESH",
    is_internal                AS "IS_INTERNAL",
    coalesce(is_win_action, 0) AS "IS_WIN_ACTION",
    n_sort_order               AS "N_SORT_ORDER",
    call_default               AS "CALL_DEFAULT",
    is_row_depend              AS "IS_ROW_DEPEND",
    opt_interface_pkg          AS "OPT_INTERFACE_PKG"
  FROM jofl.win_action_call
  WHERE db_method = lv_dbmethod
  ORDER BY n_sort_order NULLS FIRST;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl$call_pkg$of_insert(aid_account NUMERIC
  ,                                                     attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.win_action_call%ROWTYPE;
BEGIN
  lr := jofl.jofl$call_pkg$attr_to_rowtype(attr);
  INSERT INTO jofl.win_action_call SELECT lr.*;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl$call_pkg$of_update(aid_account NUMERIC
  ,                                                     attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.win_action_call%ROWTYPE;
BEGIN
  lr := jofl.jofl$call_pkg$attr_to_rowtype(attr);
  UPDATE jofl.win_action_call
  SET action_title    = lr.action_title,
    is_refresh        = lr.is_refresh,
    is_internal       = lr.is_internal,
    is_win_action     = lr.is_win_action,
    n_sort_order      = lr.n_sort_order,
    is_row_depend     = lr.is_row_depend,
    call_default      = lr.call_default,
    opt_interface_pkg = lr.opt_interface_pkg
  WHERE db_method = lr.db_method AND call_action = lr.call_action;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl$call_pkg$of_delete(aid_account NUMERIC
  ,                                                     attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.win_action_call%ROWTYPE;
BEGIN
  lr := jofl.jofl$call_pkg$attr_to_rowtype(attr);
  DELETE FROM jofl.win_action_call
  WHERE db_method = lr.db_method AND call_action = lr.call_action;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;