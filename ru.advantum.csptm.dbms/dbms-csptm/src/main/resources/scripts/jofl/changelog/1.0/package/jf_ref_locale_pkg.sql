CREATE OR REPLACE FUNCTION jofl.jf_ref_locale_pkg$attr_to_rowtype(attr TEXT)
  RETURNS TABLE(id        TEXT
  ,             scn       NUMERIC
  ,             lc_locale jofl.ref_locale.lc_locale%TYPE) AS $$
BEGIN
  RETURN QUERY
  SELECT
    jofl.jofl_pkg$extract_varchar(attr, 'ROWID', TRUE)                      AS id,
    NULL :: NUMERIC                                                         AS scn,
    jofl.jofl_pkg$extract_varchar(attr, 'LC_LOCALE', FALSE) :: CHARACTER(5) AS lc_locale;
END;
$$ LANGUAGE plpgsql;


------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_ref_locale_pkg$of_insert(aid_account NUMERIC
  ,                                                         attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lc jofl.ref_locale.lc_locale%TYPE;
BEGIN
  SELECT lc_locale
  INTO lc
  FROM jofl.jf_ref_locale_pkg$attr_to_rowtype(attr);
  INSERT INTO jofl.ref_locale (lc_locale) VALUES (lc);
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;


------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_ref_locale_pkg$of_update(aid_account NUMERIC
  ,                                                         attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr RECORD;
BEGIN
  lr := jofl.jf_ref_locale_pkg$attr_to_rowtype(attr);
  UPDATE jofl.ref_locale
  SET lc_locale = lr.lc_locale
  WHERE rowid = lr.rowid;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_ref_locale_pkg$of_delete(aid_account NUMERIC
  ,                                                         attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr RECORD;
BEGIN
  lr := jofl.jf_ref_locale_pkg$attr_to_rowtype(attr);
  DELETE FROM jofl.ref_locale
  WHERE rowid = lr.id;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;


------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_ref_locale_pkg$of_rows(aid_account NUMERIC
  , OUT                                                   arows       REFCURSOR
  ,                                                       attr        TEXT DEFAULT NULL)
  RETURNS REFCURSOR AS $$
BEGIN
  OPEN arows FOR
  SELECT
    lc_locale AS "LC_LOCALE",
    rowid     AS "ROWID"
  FROM jofl.ref_locale src;
END;
$$ LANGUAGE plpgsql;