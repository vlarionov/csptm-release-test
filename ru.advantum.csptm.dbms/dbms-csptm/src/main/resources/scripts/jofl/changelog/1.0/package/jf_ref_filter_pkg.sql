CREATE OR REPLACE FUNCTION jofl.jf_ref_filter_pkg$attr_to_rowtype(attr TEXT)
  RETURNS TABLE(id            TEXT
  ,             scn           NUMERIC
  ,             filter_class  TEXT
  ,             init_template TEXT) AS $$
BEGIN
  RETURN QUERY
  SELECT
    NULL :: TEXT                                               AS id,
    NULL :: NUMERIC                                            AS scn,
    jofl.jofl_pkg$extract_varchar(attr, 'FILTER_CLASS', TRUE)  AS filter_class,
    jofl.jofl_pkg$extract_varchar(attr, 'INIT_TEMPLATE', TRUE) AS init_template;
END;
$$ LANGUAGE plpgsql;


------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_ref_filter_pkg$of_rows(aid_account NUMERIC
  , OUT                                                   arows       REFCURSOR
  ,                                                       attr        TEXT DEFAULT NULL)
  RETURNS REFCURSOR AS $$
BEGIN
  OPEN arows FOR
  SELECT
    filter_class  AS "FILTER_CLASS",
    init_template AS "INIT_TEMPLATE"
  FROM jofl.ref_filter src;
END;
$$ LANGUAGE plpgsql;
