CREATE OR REPLACE FUNCTION jofl.const_pkg$usr_var_setting()
  RETURNS TEXT
LANGUAGE plpgsql
AS
$$
BEGIN
  RETURN 'usrvar';
END;
$$;

CREATE OR REPLACE FUNCTION jofl.const_pkg$id_jofl_account()
  RETURNS TEXT
LANGUAGE plpgsql
AS
$$
BEGIN
  RETURN 'id_jofl_account';
END;
$$;

CREATE OR REPLACE FUNCTION jofl.const_pkg$jofl_action()
  RETURNS TEXT
LANGUAGE plpgsql
AS
$$
BEGIN
  RETURN 'jofl_action';
END;
$$;