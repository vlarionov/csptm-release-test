CREATE OR REPLACE FUNCTION jofl.install_login(ausername TEXT
  ,                                           akey      TEXT
  ,                                           ahash     TEXT
  ,                                           aappcode  NUMERIC
  ,                                           aappver   NUMERIC)
  RETURNS TABLE(ausermesg TEXT,
                anum      NUMERIC) AS $$
BEGIN
  RETURN QUERY
  SELECT
    'Please change this function on install!' :: TEXT AS ausermesg,
    1 :: NUMERIC                                      AS anum; -- root account!
END;
$$ LANGUAGE plpgsql;