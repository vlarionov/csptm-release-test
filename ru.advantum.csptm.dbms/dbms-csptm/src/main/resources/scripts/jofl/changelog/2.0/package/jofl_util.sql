  -----------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_util$set_usr_var(p_var_name TEXT, p_var_value TEXT) RETURNS void
LANGUAGE plpgsql
AS $$
BEGIN
  PERFORM set_config(jofl.const_pkg$usr_var_setting() || '.' ||p_var_name, p_var_value, false);
END;
$$;
  -----------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_util$get_usr_var(p_var_name TEXT) RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_var_value TEXT;
BEGIN
  SELECT INTO l_var_value current_setting(jofl.const_pkg$usr_var_setting() || '.' || p_var_name);

  RETURN l_var_value;

	EXCEPTION WHEN undefined_object THEN RETURN NULL;
END;
$$;