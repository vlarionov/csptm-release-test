CREATE EXTENSION pgcrypto; --криптография

CREATE EXTENSION timetravel; --версионность строк
CREATE EXTENSION moddatetime;

CREATE EXTENSION "uuid-ossp";