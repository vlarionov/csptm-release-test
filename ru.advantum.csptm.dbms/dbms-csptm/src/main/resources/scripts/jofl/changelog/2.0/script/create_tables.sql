CREATE TABLE jofl.get_roles_func (
  schema_name TEXT NOT NULL,
  function    TEXT NOT NULL
)
TABLESPACE jofl_data;

COMMENT ON TABLE jofl.get_roles_func IS 'Хранимые функции для получения массива доступных ролей в зависимости от проекта и jofl аккаунта, которые должны принимать id_jofl_account NUMERIC и возвращать массив id_role TEXT[]';
COMMENT ON COLUMN jofl.get_roles_func.schema_name IS 'JOFL проект';
COMMENT ON COLUMN jofl.get_roles_func.function IS 'Полное имя функции.';

ALTER TABLE jofl.get_roles_func
  ADD CONSTRAINT pk_get_roles_func PRIMARY KEY (schema_name)
USING INDEX TABLESPACE jofl_idx;

ALTER TABLE jofl.get_roles_func
  ADD CONSTRAINT fk_get_roles_func_jf_project FOREIGN KEY (schema_name)
REFERENCES jofl.jf_project (schema_name) ON DELETE NO ACTION ON UPDATE NO ACTION;
