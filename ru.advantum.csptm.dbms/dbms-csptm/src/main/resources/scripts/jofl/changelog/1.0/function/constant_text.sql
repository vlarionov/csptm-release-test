CREATE OR REPLACE FUNCTION jofl.constant_text(TEXT, TEXT)
  RETURNS TEXT AS $$
SELECT c_val
FROM jofl.constants_text
WHERE schema_name = $1 AND c_name = $2;
$$ LANGUAGE SQL STRICT IMMUTABLE;