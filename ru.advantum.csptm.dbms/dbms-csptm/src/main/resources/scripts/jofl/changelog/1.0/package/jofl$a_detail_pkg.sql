﻿CREATE OR REPLACE FUNCTION jofl.jofl$a_detail_pkg$attr_to_rowtype(attr TEXT)
  RETURNS jofl.WIN_ACTION_DETAIL AS $$
DECLARE
  lr jofl.win_action_detail%ROWTYPE;
BEGIN
  lr.db_method := jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', FALSE);
  lr.action_title := jofl.jofl_pkg$extract_varchar(attr, 'ACTION_TITLE', FALSE);
  lr.detail_db_method := jofl.jofl_pkg$extract_varchar(attr, 'DETAIL_DB_METHOD', FALSE);
  lr.is_row_depend := jofl.jofl_pkg$extract_boolean(attr, 'IS_ROW_DEPEND', TRUE);
  lr.n_sort_order := jofl.jofl_pkg$extract_number(attr, 'N_SORT_ORDER', TRUE);
  RETURN lr;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl$a_detail_pkg$of_rows(aid_account NUMERIC
  , OUT                                                   arows       REFCURSOR
  ,                                                       attr        TEXT)
  RETURNS REFCURSOR AS $$
DECLARE
  lv_dbmethod jofl.win_field.db_method%TYPE := jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', FALSE);
BEGIN
  OPEN arows FOR
  SELECT
    db_method        AS "DB_METHOD",
    action_title     AS "ACTION_TITLE",
    detail_db_method AS "DETAIL_DB_METHOD",
    is_row_depend    AS "IS_ROW_DEPEND",
    n_sort_order     AS "N_SORT_ORDER"
  FROM jofl.win_action_detail w
  WHERE w.db_method = lv_dbmethod;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl$a_detail_pkg$of_insert(aid_account NUMERIC,
                                                            attr        TEXT)
  RETURNS TEXT AS $$
BEGIN
  INSERT INTO jofl.win_action_detail (db_method
    , action_title
    , detail_db_method
    , is_row_depend
    , n_sort_order)
    SELECT
      db_method,
      action_title,
      detail_db_method,
      is_row_depend,
      n_sort_order
    FROM jofl.jofl$a_detail_pkg$attr_to_rowtype(attr) lr;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl$a_detail_pkg$of_update(aid_account NUMERIC,
                                                            attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.win_action_detail%ROWTYPE;
BEGIN
  lr := jofl.jofl$a_detail_pkg$attr_to_rowtype(attr);
  UPDATE jofl.win_action_detail f
  SET action_title = lr.action_title,
    is_row_depend  = lr.is_row_depend,
    n_sort_order   = lr.n_sort_order
  WHERE f.db_method = lr.db_method AND
        f.detail_db_method = lr.detail_db_method;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl$a_detail_pkg$of_delete(aid_account NUMERIC,
                                                            attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.win_action_detail%ROWTYPE;
BEGIN
  lr := jofl.jofl$a_detail_pkg$attr_to_rowtype(attr);
  DELETE FROM jofl.win_action_detail f
  WHERE f.db_method = lr.db_method AND
        f.detail_db_method = lr.detail_db_method;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;