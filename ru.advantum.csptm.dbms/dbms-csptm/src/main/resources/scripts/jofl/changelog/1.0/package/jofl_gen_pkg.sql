﻿CREATE OR REPLACE FUNCTION jofl.jofl_gen_pkg$fill_fields(adb_method jofl.window.db_method%TYPE
  ,                                                      aobj_name  TEXT)
  RETURNS VOID AS $$
DECLARE
  lv_own        TEXT;
  lv_name       TEXT;
  lv_type       TEXT;

  lv_ct         jofl.win_field.field_cons_type%TYPE;
  lv_field_type jofl.win_field.field_type%TYPE;
  c             RECORD;
  const         RECORD;
BEGIN
  BEGIN
    SELECT
      table_schema,
      table_name,
      'TABLE' AS object_type
    INTO STRICT lv_own
      , lv_name
      , lv_type
    FROM information_schema.tables
    WHERE table_schema || '.' || table_name = lower(aobj_name);
    EXCEPTION
    WHEN no_data_found
      THEN
        RAISE EXCEPTION USING ERRCODE = 20001, MESSAGE = '<<Объект не найден: ' || aobj_name || '>>';
  END;

  IF (lv_type = 'TABLE')
  THEN
    FOR c IN (WITH seld AS (SELECT
                              pd.objsubid,
                              relname AS tbl,
                              nspname AS sch,
                              description,
                              pn.oid,
                              pd.objoid
                            FROM pg_description pd
                              JOIN pg_class pc ON pd.objoid = pc.oid
                              JOIN pg_namespace pn ON pc.relnamespace = pn.oid),
        consts AS (SELECT
                     kcu.column_name,
                     tc.table_name,
                     tc.table_schema,
                     substr(upper(constraint_type), 1, 1) AS constraint_type
                   FROM
                     information_schema.table_constraints AS tc
                     JOIN information_schema.key_column_usage AS kcu
                       ON tc.constraint_name = kcu.constraint_name
                     JOIN information_schema.constraint_column_usage AS ccu
                       ON ccu.constraint_name = tc.constraint_name
                   WHERE substr(upper(constraint_type), 1, 1) = 'P'
                         AND tc.table_schema = lv_own AND tc.table_name = lv_name)
    SELECT DISTINCT
      ct.constraint_type,
      tc.column_name,
      data_type,
      is_nullable                                     AS NULLABLE,
      numeric_precision                               AS DATA_PRECISION,
      numeric_scale                                   AS DATA_SCALE,
      ordinal_position                                AS COLUMN_ID,
      description                                     AS COMMENTS,
      tc.character_maximum_length                     AS CHAR_LENGTH,
      position('IS NOT NULL' IN cct.check_clause) > 0 AS notnull
    FROM information_schema.columns tc
      LEFT JOIN consts ct ON ct.column_name = tc.column_name
      LEFT JOIN seld seld ON
                            (seld.tbl = tc.table_name
                             AND seld.sch = tc.table_schema
                             AND seld.objsubid = tc.ordinal_position)
      LEFT JOIN information_schema.check_constraints cct
        ON (sch = cct.constraint_schema
            AND cct.constraint_name LIKE seld.oid || '_' || seld.objoid || '_' || seld.objsubid || '%')
    WHERE tc.table_schema = lv_own AND tc.table_name = lv_name
    ORDER BY ordinal_position)

    LOOP
      -- проверим тип constraint
      IF (c.constraint_type = 'P')
      THEN
        lv_ct := 0;
      ELSE lv_ct := 2;
      END IF;


      IF (c.data_type ILIKE '%bool%')
      THEN
        lv_field_type := 'B';
      ELSIF (c.data_type ILIKE '%NUMERIC%' OR c.data_type ILIKE '%DOUBLE%' OR c.data_type ILIKE 'INTEGER'
             OR c.data_type ILIKE '%bigint%')
        THEN
          lv_field_type := 'N';
      ELSIF (c.data_type ILIKE '%timestamp%')
        THEN
          lv_field_type := 'T';
      ELSE
        lv_field_type := 'V';
      END IF;

      INSERT INTO jofl.win_field
      (db_method
        , field_name
        , field_cons_type
        , field_type
        , display_title
        , display_description
        , default_value
        , n_order
        , n_visibility
        , is_display_name
        , is_mandatory
        , max_len
        , weight)
      VALUES
        (adb_method
          , c.column_name
          , lv_ct
          , lv_field_type
          , coalesce(nullif(c.comments, ''), c.column_name)
          , c.comments
          , NULL
          , c.column_id * 10
          , 3
          , 0
          , CASE WHEN c.notnull
          THEN 1
            ELSE 0 END
          , CASE WHEN lv_field_type = 'V' AND coalesce(c.char_length, 0) > 0
          THEN c.char_length
            ELSE NULL END
          , 1);
    END LOOP;
  END IF;

END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION jofl.jofl_pkg$attr_to_rowtype(attr TEXT)
  RETURNS jofl.WINDOW AS $$
DECLARE
  lr jofl.window%ROWTYPE;
BEGIN
  lr.db_method := trim(jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', FALSE));
  lr.win_title := jofl.jofl_pkg$extract_varchar(attr, 'WIN_TITLE', TRUE);
  lr.cache_min := jofl.jofl_pkg$extract_number(attr, 'CACHE_MIN', TRUE);
  lr.refresh_interval_sec := jofl.jofl_pkg$extract_number(attr, 'REFRESH_INTERVAL_SEC', TRUE);

  lr.n_auth_based := jofl.jofl_pkg$extract_boolean(attr, 'N_AUTH_BASED', TRUE);

  -- NULLABLE параметры
  BEGIN
    lr.custom_topcomponent := jofl.jofl_pkg$extract_varchar(attr, 'CUSTOM_TOPCOMPONENT', TRUE);
    EXCEPTION WHEN OTHERS
    THEN
      lr.custom_topcomponent := NULL;
  END;

  lr.custom_jscomponent := jofl.jofl_pkg$extract_varchar(attr, 'CUSTOM_JSCOMPONENT', TRUE);

  BEGIN
    lr.action_factory := jofl.jofl_pkg$extract_varchar(attr, 'ACTION_FACTORY', TRUE);
    EXCEPTION
    WHEN OTHERS
      THEN
        lr.action_factory := NULL;
  END;

  lr.editor_factory := jofl.jofl_pkg$extract_varchar(attr, 'EDITOR_FACTORY', TRUE);

  BEGIN
    lr.interface_pkg := trim(BOTH ' ' FROM jofl.jofl_pkg$extract_varchar(attr, 'INTERFACE_PKG', TRUE));
    EXCEPTION
    WHEN OTHERS
      THEN
        lr.interface_pkg := NULL;
  END;

  BEGIN
    lr.icon := attr('ICON');
    EXCEPTION
    WHEN OTHERS
      THEN
        lr.icon := NULL;
  END;

  BEGIN
    lr.schema_name := jofl.jofl_pkg$extract_varchar(attr, 'SCHEMA_NAME', TRUE);
    EXCEPTION
    WHEN OTHERS
      THEN
        lr.schema_name := NULL;
  END;

  RETURN lr;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION jofl.jofl_gen_pkg$gen_extract_by_field_type(afield_name jofl.win_field.field_name%TYPE
  ,                                                                    afield_type jofl.win_field.field_type%TYPE)
  RETURNS TEXT AS $$
DECLARE
BEGIN
  IF afield_type = ANY (ARRAY ['N', 'F'])
  THEN
    RETURN 'jofl.jofl_pkg$extract_number(p_attr, ''' || afield_name || ''', true); ';
  ELSIF afield_type = ANY (ARRAY ['L', 'R', 'V'])
    THEN
      RETURN 'jofl.jofl_pkg$extract_varchar(p_attr, ''' || afield_name || ''', true); ';
  ELSIF afield_type = 'B'
    THEN
      RETURN 'jofl.jofl_pkg$extract_boolean(p_attr, ''' || afield_name || ''', true); ';
  ELSIF afield_type = ANY (ARRAY ['D', 'T'])
    THEN
      RETURN 'jofl.jofl_pkg$extract_date(p_attr, ''' || afield_name || ''', true); ';
  ELSE
    RETURN '';
  END IF;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION jofl.jofl_gen_pkg$gen_attr_to_rowtype(adb_method jofl.window.db_method%TYPE
  ,                                                              apkg_name  jofl.window.interface_pkg%TYPE
  ,                                                              aobj       TEXT)
  RETURNS TEXT AS $$
DECLARE
  lfields RECORD;
  lfunc   TEXT;

BEGIN
  IF NOT exists(SELECT 1
                FROM jofl.win_field
                WHERE db_method = adb_method)
  THEN
    RETURN NULL;
  END IF;

  lfunc := 'create or replace function  ' || lower(apkg_name) || '$attr_to_rowtype(p_attr text) returns '
           || lower(aobj) || ' as $' || '$ ' || chr(10) || 'declare ' || chr(10) || '   l_r '
           || lower(aobj) || '%rowtype; ' || chr(10) || 'begin ' || chr(10);

  FOR lfields IN (SELECT *
                  FROM jofl.win_field
                  WHERE db_method = adb_method)
  LOOP
    IF position('$' IN lfields.field_name) < 1
    THEN
      lfunc := lfunc || '   l_r.' || lower(lfields.field_name) || ' := ' ||
               jofl.jofl_gen_pkg$gen_extract_by_field_type(lfields.field_name, lfields.field_type) || chr(10);
    END IF;
  END LOOP;
  lfunc := lfunc || chr(10) || '   return l_r;' || chr(10)
           || 'end;' || chr(10) || ' $' || '$ LANGUAGE plpgsql;';
  RETURN lfunc;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION jofl.jofl_gen_pkg$gen_of_rows(apkg_name  jofl.window.interface_pkg%TYPE
  ,                                                      an_auth    jofl.window.n_auth_based%TYPE
  ,                                                      atable     TEXT
  ,                                                      adb_method jofl.window.db_method%TYPE)
  RETURNS TEXT AS $$
DECLARE
  lfields RECORD;
  lfunc   TEXT;
  lauth   TEXT;
BEGIN
  IF an_auth = 1
  THEN
    lauth := 'p_id_account numeric,';
  ELSE
    lauth := '';
  END IF;
  lfunc :='create or replace function  ' || lower(apkg_name) ||
          '$of_rows(p_id_account in numeric, out p_rows refcursor, p_attr text) returns refcursor '
          || ' as $' || '$ ' || chr(10) || 'declare ' || chr(10) || 'begin ' || chr(10)
          || ' open p_rows for ' || chr(10) || '      select *  ' || chr(10);
  FOR lfields IN (SELECT *
                  FROM jofl.win_field
                  WHERE db_method = adb_method)
  LOOP
    lfunc := lfunc || '        ' || lfields.field_name || ', ' || chr(10);
  END LOOP;

  lfunc := substr(lfunc, 1, length(lfunc) - 3) || chr(10) || '      from ' || lower(atable) || '; ' || chr(10)
           || 'end;' || chr(10) || ' $' || '$ LANGUAGE plpgsql;';

  RETURN lfunc;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION jofl.jofl_gen_pkg$gen_of_action(apkg_name  jofl.window.interface_pkg%TYPE
  ,                                                        an_auth    jofl.window.n_auth_based%TYPE
  ,                                                        aaction    TEXT
  ,                                                        atable     TEXT
  ,                                                        adb_method jofl.window.db_method%TYPE)
  RETURNS TEXT AS $$
DECLARE
  lfields RECORD;
  lfunc   TEXT;
  lauth   TEXT;
BEGIN
  IF an_auth = 1
  THEN
    lauth := 'p_id_account numeric,';
  ELSE
    lauth := '';
  END IF;
  lfunc :='create or replace function  ' || lower(apkg_name) || '$' || aaction ||
          '(p_id_account in numeric, p_attr text) returns text '
          || ' as $' || '$ ' || chr(10) || 'declare ' || chr(10)
          || '   l_r ' || lower(atable) || '%rowtype;' || chr(10)
          || 'begin ' || chr(10)
          || '   l_r := ' || lower(apkg_name) || '$attr_to_rowtype(p_attr);' || chr(10);

  IF aaction ILIKE 'of_insert'
  THEN
    lfunc :=  lfunc || chr(10) || '   insert into ' || lower(atable) || ' select l_r.*;' || chr(10);
  ELSIF aaction ILIKE 'of_delete' AND exists(SELECT 1
                                             FROM jofl.win_field
                                             WHERE db_method = adb_method AND field_cons_type = 0
                                             LIMIT 1)
    THEN
      lfunc :=  lfunc || chr(10) || '   delete from  ' || lower(atable) || ' where ';

      FOR lfields IN (SELECT field_name
                      FROM jofl.win_field
                      WHERE db_method = adb_method AND field_cons_type = 0)
      LOOP
        lfunc := lfunc || ' ' || lfields.field_name || ' = l_r.' || lfields.field_name || ' and ' || chr(10);
      END LOOP;
      lfunc := substr(lfunc, 1, length(lfunc) - 6) || ';' || chr(10);

  ELSIF aaction ILIKE 'of_update' AND exists(SELECT 1
                                             FROM jofl.win_field
                                             WHERE db_method = adb_method AND field_cons_type = 0
                                             LIMIT 1)
    THEN
      lfunc :=  lfunc || chr(10) || '   update ' || lower(atable) || ' set ' || chr(10);

      DECLARE
        cnt NUMERIC;
      BEGIN
        SELECT count(*)
        INTO cnt
        FROM jofl.win_field
        WHERE db_method = adb_method AND field_cons_type <> 0;
        IF cnt = 0
        THEN
          RETURN chr(10);
        END IF;
      END;

      FOR lfields IN (SELECT field_name
                      FROM jofl.win_field
                      WHERE db_method = adb_method AND field_cons_type <> 0)
      LOOP
        lfunc := lfunc || '          ' || lfields.field_name || ' = l_r.' || lfields.field_name || ', ' || chr(10);
      END LOOP;

      lfunc := substr(lfunc, 1, length(lfunc) - 3) || chr(10) || '   where ' || chr(10);

      FOR lfields IN (SELECT field_name
                      FROM jofl.win_field
                      WHERE db_method = adb_method AND field_cons_type = 0)
      LOOP
        lfunc := lfunc || '          ' || lfields.field_name || ' = l_r.' || lfields.field_name || ' and ' || chr(10);
      END LOOP;
      lfunc := substr(lfunc, 1, length(lfunc) - 6) || ';' || chr(10);

  END IF;


  lfunc :=  lfunc || chr(10) || '   return null;' || chr(10)
            || 'end;' || chr(10) || ' $' || '$ LANGUAGE plpgsql;';

  RAISE NOTICE ' %', lfunc;
  RETURN lfunc;
END;
$$ LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION jofl.jofl_gen_pkg$generate_package(adb_method    jofl.window.db_method%TYPE
  ,                                                           apackage_name jofl.window.interface_pkg%TYPE
  ,                                                           aobject       TEXT)
  RETURNS jofl.window.interface_pkg%TYPE AS $$
DECLARE
  lfunc     TEXT;
  lschema   jofl.window.schema_name%TYPE;
  ln_auth   jofl.window.n_auth_based%TYPE;
  lpkg_name jofl.window.interface_pkg%TYPE;
  lobject   TEXT;
  laction   TEXT;
BEGIN

  IF NOT exists(SELECT 1
                FROM jofl.window
                WHERE db_method = adb_method)
  THEN
    RAISE EXCEPTION USING ERRCODE = 20001, MESSAGE = '<<Метод не существует!>>';
  END IF;

  SELECT
    interface_pkg,
    schema_name,
    n_auth_based
  INTO
    lpkg_name,
    lschema,
    ln_auth
  FROM jofl.window
  WHERE db_method = adb_method;


  IF (NOT exists(SELECT 1
                 FROM pg_tables
                 WHERE (schemaname = lower(lschema) AND tablename = lower(aobject)) OR
                       schemaname || '.' || tablename = lower(aobject))
      AND NOT exists(SELECT 1
                     FROM pg_views
                     WHERE (schemaname = lower(lschema) AND viewname = lower(aobject)) OR
                           schemaname || '.' || viewname = lower(aobject)))
  THEN
    RAISE EXCEPTION USING ERRCODE = 20001, MESSAGE = '<<Объект не существует!>>';
  END IF;

  IF position('.' IN apackage_name) < 1
  THEN
    lpkg_name := lschema || '.' || apackage_name;
  ELSE
    lpkg_name := apackage_name;
  END IF;

  IF position('.' IN aobject) < 1
  THEN
    lobject := lschema || '.' || aobject;
  ELSE
    lobject := aobject;
  END IF;

  lfunc := jofl.jofl_gen_pkg$gen_attr_to_rowtype(adb_method, lpkg_name, lobject);

  IF lfunc IS NOT NULL
  THEN
    EXECUTE lfunc;
  END IF;

  lfunc := jofl.jofl_gen_pkg$gen_of_rows(lpkg_name, ln_auth, aobject, adb_method);
  EXECUTE lfunc;

  FOR laction IN (SELECT unnest
                  FROM unnest(ARRAY ['of_insert', 'of_delete', 'of_update']))
  LOOP
    lfunc := jofl.jofl_gen_pkg$gen_of_action(lpkg_name, ln_auth, laction, aobject, adb_method);
    EXECUTE lfunc;
  END LOOP;

  RETURN upper(lpkg_name);

END;
$$ LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
