CREATE OR REPLACE FUNCTION jofl.jf_ref_menu_vocab_pkg$attr_to_rowtype(attr TEXT)
  RETURNS TABLE(scn        NUMERIC,
                lc_locale  jofl.ref_menu_vocabulary.lc_locale%TYPE,
                id_menu    jofl.ref_menu_vocabulary.id_menu%TYPE,
                rowid      jofl.ref_menu_vocabulary.rowid%TYPE,
                menu_title jofl.ref_menu_vocabulary.menu_title%TYPE)
AS $$
BEGIN
  RETURN QUERY
  SELECT
    NULL :: NUMERIC                                                                                        AS scn,
    jofl.jofl_pkg$extract_varchar(attr, 'LC_LOCALE', FALSE) :: jofl.ref_menu_vocabulary.LC_LOCALE % type   AS lc_locale,
    jofl.jofl_pkg$extract_number(attr, 'ID_MENU', FALSE) :: jofl.ref_menu_vocabulary.ID_MENU % type        AS id_menu,
    jofl.jofl_pkg$extract_varchar(attr, 'ROWID', FALSE, FALSE)                                             AS rowid,
    jofl.jofl_pkg$extract_varchar(attr, 'MENU_TITLE', FALSE) :: jofl.ref_menu_vocabulary.MENU_TITLE %
    type                                                                                                   AS menu_title;
END;
$$ LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_ref_menu_vocab_pkg$of_insert(aid_account NUMERIC,
                                                                attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr RECORD;
BEGIN
  lr:= jofl.jf_ref_menu_vocab_pkg$attr_to_rowtype(attr);
  INSERT INTO jofl.ref_menu_vocabulary (lc_locale,
                                        id_menu,
                                        rowid,
                                        menu_title)
  VALUES (lr.lc_locale,
          lr.id_menu,
          lr.rowid,
          lr.menu_title);
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;


------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_ref_menu_vocab_pkg$of_update(aid_account NUMERIC,
                                                                attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr RECORD;
BEGIN
  lr:= jofl.jf_ref_menu_vocab_pkg$attr_to_rowtype(attr);
  UPDATE jofl.ref_menu_vocabulary
  SET
    lc_locale  = lr.lc_locale,
    id_menu    = lr.id_menu,
    rowid      = lr.rowid,
    menu_title = lr.menu_title;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_ref_menu_vocab_pkg$of_delete(aid_account NUMERIC,
                                                                attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr RECORD;
BEGIN
  lr:= jofl.jf_ref_menu_vocab_pkg$attr_to_rowtype(attr);
  DELETE FROM jofl.ref_menu_vocabulary
  WHERE rowid = lr.rowid;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_ref_menu_vocab_pkg$of_rows(aid_account NUMERIC
  , OUT                                                       arows       REFCURSOR
  ,                                                           attr        TEXT DEFAULT NULL)
  RETURNS REFCURSOR AS $$
DECLARE
  ln_id_menu jofl.app_menu.id_menu%TYPE := jofl.jofl_pkg$extract_number(attr, 'ID_MENU', FALSE);
BEGIN
  OPEN arows FOR
  SELECT
    v.rowid    AS "ROWID",
    lc_locale  AS "LC_LOCALE",
    id_menu    AS "ID_MENU",
    menu_title AS "MENU_TITLE"
  FROM jofl.ref_menu_vocabulary v
  WHERE v.id_menu = ln_id_menu;
END;
$$ LANGUAGE plpgsql;