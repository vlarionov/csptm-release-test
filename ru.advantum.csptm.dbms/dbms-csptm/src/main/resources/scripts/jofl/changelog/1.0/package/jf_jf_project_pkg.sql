﻿CREATE OR REPLACE FUNCTION jofl.jf_jf_project_pkg$attr_to_rowtype(attr TEXT)
  RETURNS jofl.JF_PROJECT AS $$
DECLARE
  lr jofl.jf_project%ROWTYPE;
BEGIN
  lr.schema_name   := jofl.jofl_pkg$extract_varchar(attr, 'SCHEMA_NAME', FALSE);
  lr.project_title := jofl.jofl_pkg$extract_varchar(attr, 'PROJECT_TITLE', FALSE);
  lr.row$color := jofl.jofl_pkg$extract_number(attr,
                                               'ROW$COLOR',
                                               TRUE);
  RETURN lr;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_jf_project_pkg$of_rows(aid_account NUMERIC
  , OUT                                                   arows       REFCURSOR
  ,                                                       attr        TEXT)
  RETURNS REFCURSOR AS $$
BEGIN
  OPEN arows FOR
  SELECT
    schema_name   AS "SCHEMA_NAME",
    project_title AS "PROJECT_TITLE",
    row$color     AS "ROW$COLOR"
  FROM jofl.jf_project src;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_jf_project_pkg$of_insert(aid_account NUMERIC,
                                                            attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.jf_project%ROWTYPE;
BEGIN
  lr := jofl.jf_jf_project_pkg$attr_to_rowtype(attr);

  INSERT INTO jofl.jf_project (schema_name,
                               project_title,
                               row$color)
  VALUES (lr.schema_name,
          lr.project_title,
          lr.row$color);

  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_jf_project_pkg$of_update(aid_account NUMERIC,
                                                            attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.jf_project%ROWTYPE;
BEGIN
  lr := jofl.jf_jf_project_pkg$attr_to_rowtype(attr);

  UPDATE jofl.jf_project
  SET schema_name = lr.schema_name,
    project_title = lr.project_title,
    row$color     = lr.row$color
  WHERE schema_name = lr.schema_name;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_jf_project_pkg$of_delete(aid_account NUMERIC,
                                                            attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.jf_project%ROWTYPE;
BEGIN
  lr := jofl.jf_jf_project_pkg$attr_to_rowtype(attr);
  DELETE FROM jofl.jf_project
  WHERE schema_name = lr.schema_name;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

