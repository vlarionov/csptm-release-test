CREATE OR REPLACE FUNCTION jofl.constant_num(TEXT, TEXT)
  RETURNS NUMERIC AS $$
SELECT c_val
FROM jofl.constants_num
WHERE schema_name = $1 AND c_name = $2;
$$ LANGUAGE SQL STRICT IMMUTABLE;