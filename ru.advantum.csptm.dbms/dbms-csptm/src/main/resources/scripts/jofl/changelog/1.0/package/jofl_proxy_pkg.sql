﻿CREATE OR REPLACE FUNCTION jofl.jofl_proxy_pkg$validate_permission(aid_jofl_account jofl.account.id_jofl_account%TYPE
  ,                                                                adb_method       jofl.window.db_method%TYPE
  ,                                                                aaction          TEXT)
  RETURNS BOOLEAN AS $$
DECLARE

  lv_method jofl.win_action_call.call_action%TYPE;
BEGIN
  IF (aid_jofl_account = -1 /*AND AACTION = 'OF_ROWS'*/)
  THEN
    RETURN exists(SELECT 1
                  FROM jofl.window w
                  WHERE w.db_method = adb_method AND w.n_auth_based = 0);
  END IF;

  IF (jofl.jofl_pkg$isrootrole(aid_jofl_account))
  THEN
    -- Вам, Root, можно все ;) Даже того чего нет :)
    RETURN TRUE;
  END IF;

  lv_method := CASE
               WHEN aaction = 'i'
                 THEN
                   'OF_INSERT'
               WHEN aaction = 'U'
                 THEN
                   'OF_UPDATE'
               WHEN AACTION = 'D'
                 THEN
                   'OF_DELETE'
               ELSE
                 aaction
               END;

  RETURN exists(SELECT 1
                FROM jofl.role2account r2a
                  , jofl.role2call r2c
                  , jofl.account a
                WHERE a.id_jofl_account = aid_jofl_account AND
                      a.id_jofl_account = r2a.id_jofl_account AND
                      r2a.id_role = r2c.id_role AND r2c.db_method = adb_method AND
                      r2c.call_action = lv_method);
END;
$$ LANGUAGE plpgsql;


----------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_proxy_pkg$do_action(aid_jofl_account jofl.account.id_jofl_account%TYPE
  ,                                                      adb_method       jofl.window.db_method%TYPE
  ,                                                      aaction          TEXT
  ,                                                      av_gson          TEXT
  ,                                                      av_binary        TEXT DEFAULT NULL)
  RETURNS TEXT AS $$
BEGIN
  IF jofl.jofl_proxy_pkg$validate_permission(aid_jofl_account, adb_method, aaction)
  THEN
    RETURN jofl.jofl_proxy_pkg$do_action_(jofl.jofl_pkg$translate(aid_jofl_account),
                                          adb_method,
                                          aaction,
                                          av_gson,
                                          av_binary);
  ELSE
    RAISE EXCEPTION USING ERRCODE = jofl.jofl_proxy_pkg$get_error_code(), MESSAGE = '<<Метод недоступен!>>';
  END IF;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;


----------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_proxy_pkg$do_action_(aid_account NUMERIC
  ,                                                       adb_method  jofl.window.db_method%TYPE
  ,                                                       aaction     TEXT
  ,                                                       av_gson     TEXT
  ,                                                       av_binary   TEXT DEFAULT NULL)
  RETURNS TEXT AS $$
DECLARE
  lv_pkg        jofl.window.interface_pkg%TYPE;
  lv_method     TEXT;
  lmsg          TEXT;
  ln_auth_based jofl.window.n_auth_based%TYPE;

BEGIN

  SELECT
    w.interface_pkg,
    w.n_auth_based
  INTO lv_pkg
    , ln_auth_based
  FROM jofl.window w
  WHERE w.db_method = adb_method;

  IF (lv_pkg IS NULL)
  THEN
    RAISE EXCEPTION USING ERRCODE = 20001, MESSAGE ='<<Незаполнено поле "Интерфейс"!>>';
  END IF;

  IF (aaction IN ('I', 'U', 'D'))
  THEN
    lv_method := CASE
                 WHEN aaction = 'i'
                   THEN
                     'OF_INSERT'
                 ELSE
                   CASE
                   WHEN aaction = 'u'
                     THEN
                       'OF_UPDATE'
                   ELSE
                     CASE
                     WHEN aaction = 'd'
                       THEN
                         'OF_DELETE'
                     ELSE
                       NULL
                     END
                   END
                 END;

  ELSE
    lmsg := 'Операция успешно выполнена!';
    lv_method := aaction;
  END IF;

  -- разрулим пакет...
  BEGIN
    SELECT coalesce(NULLIF(c.opt_interface_pkg, ''), lv_pkg)
    INTO lv_pkg
    FROM jofl.win_action_call c
    WHERE c.db_method = adb_method AND c.call_action = lv_method;
    EXCEPTION
    WHEN no_data_found
      THEN
        RETURN NULL;
  END;

  IF (ln_auth_based = 0)
  THEN
    IF (av_binary IS NULL OR av_binary = '{}')
    THEN
      EXECUTE 'select ' || lower(lv_pkg) || '$' || lower(lv_method) || '($1::text);'
      USING av_gson INTO lmsg;
    ELSE
      EXECUTE 'select ' || lower(lv_pkg) || '$' || lower(lv_method) || '($1::text,
													$2::text
													);'
      USING av_gson, av_binary INTO lmsg;
    END IF;
  ELSE
    IF (av_binary IS NULL OR av_binary = '{}')
    THEN
      EXECUTE 'select ' || lower(lv_pkg) || '$' || lower(lv_method) ||
              '($1::numeric, $2::text);'
      USING aid_account, av_gson INTO lmsg;
    ELSE
      EXECUTE 'select ' || lower(lv_pkg) || '$' || lower(lv_method) || '($1::numeric,
										    $2::text,
										    $3::text);'
      USING aid_account, av_gson, av_binary INTO lmsg;
    END IF;
  END IF;

  RETURN lmsg;
END;
$$ LANGUAGE plpgsql;

----------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_proxy_pkg$get_rows(aid_jofl_account jofl.account.id_jofl_account%TYPE
  ,                                                     adb_method       jofl.window.db_method%TYPE
  , OUT                                                 arows            REFCURSOR
  ,                                                     av_gson          TEXT
  ,                                                     apack_method     TEXT DEFAULT 'OF_ROWS')
  RETURNS REFCURSOR AS $$
BEGIN

  IF (jofl.jofl_proxy_pkg$validate_permission(aid_jofl_account,
                                              adb_method,
                                              apack_method))
  THEN
    BEGIN
      arows = jofl.jofl_proxy_pkg$get_rows_(jofl.jofl_pkg$translate(aid_jofl_account) :: NUMERIC,
                                            adb_method :: TEXT,
                                            av_gson :: TEXT,
                                            apack_method :: TEXT
      );
      RETURN;
      -- не надо душить исключения!
      -- EXCEPTION WHEN others THEN

    END;
  END IF;
  RAISE EXCEPTION USING ERRCODE = jofl.jofl_proxy_pkg$get_error_code(), MESSAGE =
    '<<Нет доступа! (id: ' || aid_jofl_account || ')>>';
END;
$$ LANGUAGE plpgsql;

----------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_proxy_pkg$get_rows_(aid_jofl_account jofl.account.id_jofl_account%TYPE
  ,                                                      adb_method       jofl.window.db_method%TYPE
  , OUT                                                  arows            REFCURSOR
  ,                                                      av_gson          TEXT
  ,                                                      apack_method     TEXT DEFAULT 'OF_ROWS')
  RETURNS REFCURSOR AS $$
DECLARE
  lv_pkg            jofl.window.interface_pkg%TYPE := NULL;
  ln_auth_based     jofl.window.n_auth_based%TYPE;
  lv_call           TEXT := '<no call>';
  lv_packmethod     jofl.win_action_call.call_action%TYPE := NULL;

  p_error_code      TEXT;
  p_column_name     TEXT;
  p_constraint_name TEXT;
  p_data_type       TEXT;
  p_text            TEXT;
  p_table           TEXT;
  p_schema          TEXT;
  p_detail          TEXT;
  p_hint            TEXT;
  p_call_stack      TEXT;
  p_ermsg           TEXT;
BEGIN
  BEGIN

    IF (apack_method != 'OF_ROWS')
    THEN
      SELECT coalesce(NULLIF(ca.call_default, ''), ca.call_action)
      INTO lv_packmethod
      FROM jofl.win_action_call ca
      WHERE ca.db_method = adb_method AND ca.call_action = apack_method;
    ELSE
      lv_packmethod :=  coalesce(nullif(apack_method, ''), 'OF_ROWS');
    END IF;


    BEGIN
      SELECT opt_interface_pkg
      INTO lv_pkg
      FROM jofl.win_action_call
      WHERE db_method = adb_method AND call_action = lv_packmethod;
      EXCEPTION
      WHEN no_data_found
        THEN
          lv_pkg := NULL;
    END;

    SELECT
      coalesce(nullif(lv_pkg, ''), w.interface_pkg),
      w.n_auth_based
    INTO lv_pkg
      , ln_auth_based
    FROM jofl.window w
    WHERE w.db_method = adb_method;

    -- LN_AUTH_BASED
    -- надо учесть этот параметр...
    /*
    LN_MODIFY_OPTS := GET_MODIFY_OPTS(AID_JOFL_ACCOUNT => AID_JOFL_ACCOUNT,
                                  ADB_METHOD       => ADB_METHOD);*/

    BEGIN
      IF (av_gson IS NULL)
      THEN
        IF (ln_auth_based = 0)
        THEN
          lv_call := 'select ' || lower(lv_pkg) || '$' || lower(lv_packmethod) ||
                     '();';

          EXECUTE lv_call
          INTO arows;
          RETURN;
        ELSE
          lv_call := 'select ' || lower(lv_pkg) || '$' || lower(lv_packmethod) ||
                     '($1);';

          EXECUTE lv_call
          USING aid_jofl_account INTO arows;
          RETURN;

        END IF;
      ELSE
        IF (ln_auth_based = 0)
        THEN
          lv_call := 'select ' || lower(lv_pkg) || '$' || lower(lv_packmethod) ||
                     '($1);';
          EXECUTE lv_call
          USING av_gson INTO arows;
          RETURN;
        ELSE
          lv_call := 'select ' || lower(lv_pkg) || '$' || lower(lv_packmethod) ||
                     '($1, $2);';
          EXECUTE lv_call
          USING aid_jofl_account, av_gson INTO arows;
          RETURN;
        END IF;
      END IF;
    END;

    EXCEPTION WHEN OTHERS
    THEN
      GET STACKED DIAGNOSTICS
      p_error_code :=RETURNED_SQLSTATE,
      p_column_name :=COLUMN_NAME,
      p_constraint_name := CONSTRAINT_NAME,
      p_data_type := PG_DATATYPE_NAME,
      p_text := MESSAGE_TEXT,
      p_table := TABLE_NAME,
      p_schema := SCHEMA_NAME,
      p_detail := PG_EXCEPTION_DETAIL,
      p_hint  := PG_EXCEPTION_HINT,
      p_call_stack := PG_EXCEPTION_CONTEXT;
      BEGIN
        WITH exp AS (SELECT
                       p_error_code      AS error_code,
                       p_column_name     AS COLUMN_NAME,
                       p_constraint_name AS CONSTRAINT_NAME,
                       p_data_type       AS DATATYPE_NAME,
                       p_text            AS MESSAGE_TEXT,
                       p_table           AS TABLE_NAME,
                       p_schema          AS SCHEMA_NAME,
                       p_detail          AS EXCEPTION_DETAIL,
                       p_hint            AS EXCEPTION_HINT,
                       p_call_stack      AS stack)
        SELECT to_json(e.*)
        INTO p_ermsg
        FROM exp e;
        RAISE EXCEPTION USING ERRCODE = p_error_code, MESSAGE = '<<JSON FOR ERROR STACK:' || p_ermsg || '>>';
      END;
  END;
END;
$$ LANGUAGE plpgsql;
--------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_proxy_pkg$get_error_code()
  RETURNS INTEGER AS $$
SELECT 20403;
$$ LANGUAGE SQL STRICT IMMUTABLE;