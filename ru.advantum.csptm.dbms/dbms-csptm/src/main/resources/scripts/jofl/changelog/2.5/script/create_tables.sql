CREATE TABLE jofl.get_rows_history_handler
(
    schema_name text NOT NULL,
    function text NOT NULL,
    CONSTRAINT pk_rows_history_handler PRIMARY KEY (schema_name)
        USING INDEX TABLESPACE jofl_idx
)
WITH (
    OIDS = FALSE
)
TABLESPACE jofl_data;

COMMENT ON TABLE jofl.get_rows_history_handler
    IS 'Хранимые функции для логирования действий пользователей при открытии окон';

COMMENT ON COLUMN jofl.get_rows_history_handler.schema_name
    IS 'JOFL проект';

COMMENT ON COLUMN jofl.get_rows_history_handler.function
    IS 'Полное имя функции.';