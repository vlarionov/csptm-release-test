CREATE OR REPLACE FUNCTION jofl.jofl$admin_field$attr_to_rowtype(attr TEXT)
  RETURNS jofl.WIN_FIELD AS $$
DECLARE
  lr jofl.win_field%ROWTYPE;
BEGIN
  lr.rowid := jofl.jofl_pkg$extract_varchar(attr, 'ROWID', TRUE);
  lr.db_method :=jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', FALSE);
  lr.field_name := jofl.jofl_pkg$extract_varchar(attr, 'FIELD_NAME', FALSE);
  lr.field_cons_type := jofl.jofl_pkg$extract_number(attr, 'FIELD_CONS_TYPE', FALSE);
  lr.field_type := jofl.jofl_pkg$extract_varchar(attr, 'FIELD_TYPE', FALSE);
  lr.display_title := jofl.jofl_pkg$extract_varchar(attr, 'DISPLAY_TITLE', FALSE);
  lr.is_display_name := jofl.jofl_pkg$extract_boolean(attr, 'IS_DISPLAY_NAME');
  lr.is_mandatory := jofl.jofl_pkg$extract_boolean(attr, 'IS_MANDATORY');
  lr.display_description := jofl.jofl_pkg$extract_varchar(attr, 'DISPLAY_DESCRIPTION', TRUE);
  lr.pattern := jofl.jofl_pkg$extract_varchar(attr, 'PATTERN', TRUE);
  lr.default_value := jofl.jofl_pkg$extract_varchar(attr, 'DEFAULT_VALUE', TRUE);
  lr.n_order :=jofl.jofl_pkg$extract_number(attr, 'N_ORDER', FALSE);
  lr.n_visibility := jofl.jofl_pkg$extract_number(attr, 'N_VISIBILITY', FALSE);
  lr.max_len := jofl.jofl_pkg$extract_number(attr, 'MAX_LEN', TRUE);
  lr.weight := jofl.jofl_pkg$extract_number(attr, 'WEIGHT', FALSE);
  RETURN lr;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
-- jofl.api functions
------------------------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl$admin_field$of_rows(aid_account NUMERIC
  , OUT                                                  arows       REFCURSOR
  ,                                                      attr        TEXT)
  RETURNS REFCURSOR AS $$
DECLARE
  lv_dbmethod jofl.win_field.db_method%TYPE := jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', FALSE);
BEGIN
  OPEN arows FOR
  SELECT
    f.rowid               AS "ROWID",
    f.db_method           AS "DB_METHOD",
    f.field_name          AS "FIELD_NAME",
    f.field_cons_type     AS "FIELD_CONS_TYPE",
    fct.field_const_name  AS "FIELD_CONST_NAME",
    f.field_type          AS "FIELD_TYPE",
    ft.field_type_name    AS "FIELD_TYPE_NAME",
    f.display_title       AS "DISPLAY_TITLE",
    f.display_description AS "DISPLAY_DESCRIPTION",
    f.default_value       AS "DEFAULT_VALUE",
    f.n_order             AS "N_ORDER",
    f.n_visibility        AS "N_VISIBILITY",
    fv.visibility_name    AS "VISIBILITY_NAME",
    f.is_display_name     AS "IS_DISPLAY_NAME",
    f.is_mandatory        AS "IS_MANDATORY",
    f.pattern             AS "PATTERN",
    f.max_len             AS "MAX_LEN",
    f.weight              AS "WEIGHT"
  FROM jofl.win_field f
    , jofl.ref_field_type ft
    , jofl.ref_field_visibility fv
    , jofl.ref_const_type fct
  WHERE f.db_method = lv_dbmethod AND f.field_type = ft.field_type AND
        f.n_visibility = fv.n_visibility AND
        f.field_cons_type = fct.field_cons_type
  ORDER BY n_order;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl$admin_field$of_insert(aid_account NUMERIC,
                                                           attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lv_packet jofl.window.interface_pkg%TYPE;
BEGIN
  WITH sel AS (SELECT
                 db_method,
                 field_name,
                 field_cons_type,
                 field_type,
                 display_title,
                 is_display_name,
                 is_mandatory,
                 display_description,
                 pattern,
                 default_value,
                 n_order,
                 n_visibility,
                 max_len,
                 weight,
                 nextval('jofl.sq_jofl') rowid
               FROM jofl.jofl$admin_field$attr_to_rowtype(attr) lr),
      ins AS (INSERT INTO jofl.win_field (db_method,
                                          field_name,
                                          field_cons_type,
                                          field_type,
                                          display_title,
                                          is_display_name,
                                          is_mandatory,
                                          display_description,
                                          pattern,
                                          default_value,
                                          n_order,
                                          n_visibility,
                                          max_len,
                                          weight,
                                          rowid)
      SELECT
        db_method,
        field_name,
        field_cons_type,
        field_type,
        display_title,
        is_display_name,
        is_mandatory,
        display_description,
        pattern,
        default_value,
        n_order,
        n_visibility,
        max_len,
        weight,
        rowid :: TEXT
      FROM sel
    RETURNING *)
  SELECT w.interface_pkg
  INTO lv_packet
  FROM sel s
    JOIN jofl.window w ON w.db_method = s.db_method;

  RETURN 'Не забудьте внести изменения в методы: OF_ROWS и ATTR_TO_ROWTYPE пакета - ' ||
         lv_packet || ' , это важно - иначе ничего не будет работать!';
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl$admin_field$of_update(aid_account NUMERIC
  ,                                                        attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr jofl.win_field%ROWTYPE;
BEGIN
  lr := jofl.jofl$admin_field$attr_to_rowtype(attr);
  UPDATE jofl.win_field f
  SET
    db_method           = lr.db_method,
    field_name          = lr.field_name,
    field_cons_type     = lr.field_cons_type,
    field_type          = lr.field_type,
    display_title       = lr.display_title,
    is_display_name     = lr.is_display_name,
    is_mandatory        = lr.is_mandatory,
    display_description = lr.display_description,
    pattern             = lr.pattern,
    default_value       = lr.default_value,
    n_order             = lr.n_order,
    n_visibility        = lr.n_visibility,
    max_len             = lr.max_len,
    weight              = lr.weight
  WHERE f.rowid = lr.rowid
        OR (f.db_method = lr.db_method AND f.field_name = lr.field_name);

  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl$admin_field$of_delete(aid_account NUMERIC
  ,                                                        attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lv_packet jofl.window.interface_pkg%TYPE;
BEGIN
  WITH sel AS (SELECT
                 lr.rowid,
                 lr.db_method
               FROM jofl.jofl$admin_field$attr_to_rowtype(attr) lr),
      del AS (DELETE FROM jofl.win_field
    WHERE rowid IN (SELECT rowid
                    FROM sel)
    RETURNING *)
  SELECT w.interface_pkg
  INTO lv_packet
  FROM sel s
    JOIN jofl.window w
      ON w.db_method = s.db_method;

  RETURN 'Не забудьте внести изменения в методы: OF_ROWS и ATTR_TO_ROWTYPE пакета - ' ||
         lv_packet || ' , это важно - иначе ничего не будет работать!';
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;