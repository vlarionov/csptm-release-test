create or replace function  jofl.jofl_proxy_pkg$do_action(aid_jofl_account jofl.account.id_jofl_account%type
										 ,adb_method    jofl.window.db_method%type
										 ,aaction       text
										 ,av_gson       text
										 ,av_binary     text default null) returns text as $$
	begin
		PERFORM jofl.jofl_util$set_usr_var(jofl.const_pkg$id_jofl_account(), aid_jofl_account::TEXT);
		PERFORM jofl.jofl_util$set_usr_var(jofl.const_pkg$jofl_action(), aaction);

		if jofl.jofl_proxy_pkg$validate_permission(aid_jofl_account, adb_method,aaction)
		then
			return jofl.jofl_proxy_pkg$do_action_(jofl.jofl_pkg$translate(aid_jofl_account),
						      adb_method,
						      aaction,
						      av_gson,
						      av_binary);
		else
			RAISE EXCEPTION USING ERRCODE = jofl.jofl_proxy_pkg$get_error_code(), MESSAGE = '<<Метод недоступен!>>';
		end if;
		return null;
	end ;
$$ LANGUAGE plpgsql;

create or replace function  jofl.jofl_proxy_pkg$validate_permission(aid_jofl_account jofl.account.id_jofl_account%type
						,adb_method jofl.window.db_method%type
						,aaction    text) returns boolean as $$
	declare

		lv_method jofl.win_action_call.call_action%type;
	begin
		if (aid_jofl_account = -1 /*AND AACTION = 'OF_ROWS'*/)
		then
			return exists (select 1 from jofl.window w where w.db_method = adb_method and w.n_auth_based = 0);
		end if;

		if (jofl.jofl_pkg$isrootrole(aid_jofl_account))
		then
			return true;
		end if;

		lv_method := case
				 when aaction = 'i' then
					'OF_INSERT'
				 when aaction = 'U' then
					'OF_UPDATE'
				 when AACTION = 'D' then
					'OF_DELETE'
				 else
					aaction
			 end;

		RETURN exists(
      SELECT 1
      FROM jofl.role2call r2c
      WHERE r2c.db_method = adb_method
            AND r2c.call_action = lv_method
            AND r2c.id_role = ANY (jofl.jofl_pkg$get_account_roles(aid_jofl_account))
  );
	end ;
$$ LANGUAGE plpgsql;