CREATE OR REPLACE FUNCTION jofl.jf_ref_vocabulary_pkg$attr_to_rowtype(attr TEXT)
  RETURNS TABLE(scn       NUMERIC,
                db_method jofl.ref_vocabulary.db_method%TYPE,
                lc_locale jofl.ref_vocabulary.lc_locale%TYPE,
                lc_tag    jofl.ref_vocabulary.lc_tag%TYPE,
                lc_key    jofl.ref_vocabulary.lc_key%TYPE,
                lc_value  jofl.ref_vocabulary.lc_value%TYPE,
                rowid     jofl.ref_vocabulary.rowid%TYPE) AS $$
BEGIN
  RETURN QUERY
  SELECT
    NULL :: NUMERIC                                                                                    scn,
    jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', FALSE)                                         AS db_method,
    jofl.jofl_pkg$extract_varchar(attr, 'LC_LOCALE', FALSE) :: jofl.ref_vocabulary.LC_LOCALE % type AS lc_locale,
    jofl.jofl_pkg$extract_varchar(attr, 'LC_TAG', FALSE)                                            AS lc_tag,
    jofl.jofl_pkg$extract_varchar(attr, 'LC_KEY', TRUE)                                             AS lc_key,
    jofl.jofl_pkg$extract_varchar(attr, 'LC_VALUE', FALSE)                                          AS lc_value,
    jofl.jofl_pkg$extract_varchar(attr, 'ROWID', TRUE)                                              AS rowid;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_ref_vocabulary_pkg$of_insert(aid_account NUMERIC
  ,                                                             attr        TEXT)
  RETURNS TEXT AS $$
BEGIN
  INSERT INTO jofl.ref_vocabulary (db_method,
                                   lc_locale,
                                   lc_tag,
                                   lc_key,
                                   lc_value)
    SELECT
      db_method,
      lc_locale,
      lc_tag,
      lc_key,
      lc_value
    FROM jofl.jf_ref_vocabulary_pkg$attr_to_rowtype(attr);
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;
--------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_ref_vocabulary_pkg$of_update(aid_account NUMERIC
  ,                                                             attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr RECORD;
BEGIN
  lr := jofl.jf_ref_vocabulary_pkg$attr_to_rowtype(attr);
  UPDATE jofl.ref_vocabulary
  SET
    db_method = lr.db_method,
    lc_locale = lr.lc_locale,
    lc_tag    = lr.lc_tag,
    lc_key    = lr.lc_key,
    lc_value  = lr.lc_value
  WHERE rowid = lr.rowid;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_ref_vocabulary_pkg$of_delete(aid_account NUMERIC
  ,                                                             attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr RECORD;
BEGIN
  lr := jofl.jf_ref_vocabulary_pkg$attr_to_rowtype(attr);
  DELETE FROM jofl.ref_vocabulary
  WHERE rowid = lr.rowid;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_ref_vocabulary_pkg$of_rows(aid_account NUMERIC
  , OUT                                                       arows       REFCURSOR
  ,                                                           attr        TEXT DEFAULT NULL)
  RETURNS REFCURSOR AS $$
DECLARE
  lv_method jofl.window.db_method%TYPE := jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', TRUE);
BEGIN
  OPEN arows FOR
  SELECT
    rowid     AS "ROWID",
    db_method AS "DB_METHOD",
    lc_locale AS "LC_LOCALE",
    lc_tag    AS "LC_TAG",
    lc_key    AS "LC_KEY",
    lc_value  AS "LC_VALUE"
  FROM jofl.ref_vocabulary v
  WHERE (v.db_method = lv_method OR lv_method IS NULL);
END;
$$ LANGUAGE plpgsql;


------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_ref_vocabulary_pkg$of_sync(aid_account NUMERIC
  ,                                                           attr        TEXT)
  RETURNS TEXT AS $$
DECLARE
  lr               RECORD;
  f                RECORD;
  c                RECORD;
  d                RECORD;
  lv_lang          jofl.ref_locale.lc_locale%TYPE := jofl.jofl_pkg$extract_varchar(attr, 'F_LC_LOCALE', FALSE);
  lv_method        jofl.window.db_method%TYPE := jofl.jofl_pkg$extract_varchar(attr, 'DB_METHOD', FALSE);

  lb_filter_exists BOOLEAN := FALSE;
  ln_qty           NUMERIC := 0;
BEGIN
  lr := jofl.jf_ref_vocabulary_pkg$attr_to_rowtype(attr);
  -- 1)  #title
  BEGIN
    INSERT INTO jofl.ref_vocabulary
    (db_method
      , lc_locale
      , lc_tag
      , lc_key
      , lc_value)
    VALUES
      (lv_method
        , lv_lang
        , '#title'
        , '-'
        , attr('WIN_TITLE'));

    ln_qty := 1;
    EXCEPTION
    WHEN unique_violation
      THEN
  END;

  -- 2)  #field
  FOR f IN (SELECT
              field_name,
              display_title
            FROM jofl.win_field f
            WHERE f.db_method = lv_method AND n_visibility != 0)
  LOOP
    BEGIN
      INSERT INTO jofl.ref_vocabulary
      (db_method
        , lc_locale
        , lc_tag
        , lc_key
        , lc_value)
      VALUES
        (lv_method
          , lv_lang
          , '#field'
          , f.field_name
          , f.display_title);

      ln_qty := ln_qty + 1;
      EXCEPTION
      WHEN unique_violation
        THEN
    END;
  END LOOP;

  -- 2.1)  #field.description
  FOR f IN (SELECT
              field_name,
              display_description
            FROM jofl.win_field f
            WHERE f.db_method = lv_method AND n_visibility != 0 AND
                  display_description IS NOT NULL)
  LOOP
    BEGIN
      INSERT INTO jofl.ref_vocabulary
      (db_method
        , lc_locale
        , lc_tag
        , lc_key
        , lc_value)
      VALUES
        (lv_method
          , lv_lang
          , '#field.description'
          , f.field_name
          , f.display_description);

      ln_qty := ln_qty + 1;
      EXCEPTION
      WHEN unique_violation
        THEN
    END;
  END LOOP;

  -- 3) #action
  FOR c IN (SELECT
              c.call_action,
              c.action_title
            FROM jofl.win_action_call c
            WHERE c.db_method = lv_method AND c.is_internal = 0)
  LOOP

    BEGIN
      INSERT INTO jofl.ref_vocabulary
      (db_method
        , lc_locale
        , lc_tag
        , lc_key
        , lc_value)
      VALUES
        (lv_method
          , lv_lang
          , '#action'
          , c.call_action
          , c.action_title);
      ln_qty := ln_qty + 1;
      EXCEPTION
      WHEN unique_violation
        THEN
    END;
  END LOOP;

  -- 4) #action.field
  FOR f IN (SELECT
              f.id_afield,
              coalesce(f.field_title, '<<EMPTY>>') field_title
            FROM jofl.win_action_field f
            WHERE f.db_method = lv_method)
  LOOP
    BEGIN
      INSERT INTO jofl.ref_vocabulary
      (db_method
        , lc_locale
        , lc_tag
        , lc_key
        , lc_value)
      VALUES
        (lv_method
          , lv_lang
          , '#action.field'
          , f.id_afield
          , f.field_title);
      ln_qty := ln_qty + 1;
      EXCEPTION
      WHEN unique_violation
        THEN
          NULL;
    END;
  END LOOP;

  -- 5) #detail
  FOR d IN (SELECT
              d.action_title,
              d.detail_db_method
            FROM jofl.win_action_detail d
            WHERE d.db_method = lv_method)
  LOOP
    BEGIN
      INSERT INTO jofl.ref_vocabulary
      (db_method
        , lc_locale
        , lc_tag
        , lc_key
        , lc_value)
      VALUES
        (lv_method
          , lv_lang
          , '#detail'
          , d.detail_db_method
          , d.action_title);

      ln_qty := ln_qty + 1;
      EXCEPTION
      WHEN unique_violation
        THEN
    END;
  END LOOP;

  -- 6) #filter
  FOR f IN (SELECT
              f.id_filter,
              coalesce(f.filter_title, '<<EMPTY>>') AS filter_title
            FROM jofl.win_filter f
            WHERE f.db_method = lv_method)
  LOOP
    lb_filter_exists := TRUE;
    BEGIN
      INSERT INTO ref_vocabulary
      (db_method
        , lc_locale
        , lc_tag
        , lc_key
        , lc_value)
      VALUES
        (lv_method
          , lv_lang
          , '#filter'
          , f.id_filter
          , f.filter_title);

      ln_qty := ln_qty + 1;
      EXCEPTION
      WHEN unique_violation
        THEN
    END;
  END LOOP;

  IF (lb_filter_exists)
  THEN
    FOR f IN (SELECT
                f.but_apply_caption,
                f.but_reset_caption
              FROM jofl.win_filter_opts f
              WHERE f.db_method = lv_method)
    LOOP
      BEGIN
        INSERT INTO jfl.ref_vocabulary
        (db_method
          , lc_locale
          , lc_tag
          , lc_key
          , lc_value)
        VALUES
          (lv_method
            , lv_lang
            , '#filter.apply'
            , '-'
            , f.but_apply_caption);

        ln_qty := ln_qty + 1;
        EXCEPTION
        WHEN unique_violation
          THEN
      END;

      BEGIN
        INSERT INTO jofl.ref_vocabulary
        (db_method
          , lc_locale
          , lc_tag
          , lc_key
          , lc_value)
        VALUES
          (lv_method
            , lv_lang
            , '#filter.reset'
            , '-'
            , f.but_reset_caption);

        ln_qty := ln_qty + 1;
        EXCEPTION
        WHEN unique_violation
          THEN
      END;
    END LOOP;

  END IF;

  RETURN 'Для метода: ' || lv_method || ' найдено ' || ln_qty ||
         ' новых записей! ';
END;
$$ LANGUAGE plpgsql;