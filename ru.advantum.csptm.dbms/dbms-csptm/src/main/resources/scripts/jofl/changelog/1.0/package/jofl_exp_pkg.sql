CREATE OR REPLACE FUNCTION jofl.jofl_exp_pkg$get_metadata(adb_method IN                jofl.window.db_method%TYPE
  ,                                                                  OUT awindow       REFCURSOR
  ,                                                                  OUT afield        REFCURSOR
  ,                                                                  OUT afield_ref    REFCURSOR
  ,                                                                  OUT acall         REFCURSOR
  ,                                                                  OUT acall_form    REFCURSOR
  ,                                                                  OUT afilter       REFCURSOR
  ,                                                                  OUT afilter_opts  REFCURSOR
  ,                                                                  OUT adetail       REFCURSOR
  ,                                                                  OUT alocalization REFCURSOR)
  RETURNS RECORD AS $$
DECLARE
  lv_method jofl.window.db_method%TYPE;
BEGIN
  SELECT db_method
  INTO lv_method
  FROM jofl.window
  WHERE db_method = adb_method;

  OPEN awindow FOR
  SELECT *
  FROM jofl.window
  WHERE db_method = adb_method;

  OPEN afield FOR
  SELECT *
  FROM jofl.win_field f
  WHERE f.db_method = adb_method;

  OPEN afield_ref FOR
  SELECT *
  FROM jofl.win_field_ref f
  WHERE f.db_method = adb_method;

  OPEN acall FOR
  SELECT *
  FROM jofl.win_action_call
  WHERE db_method = adb_method;

  OPEN acall_form FOR
  SELECT *
  FROM jofl.win_action_field
  WHERE db_method = adb_method;

  OPEN afilter FOR
  SELECT *
  FROM jofl.win_filter
  WHERE db_method = adb_method;

  OPEN afilter_opts FOR
  SELECT *
  FROM jofl.win_filter_opts
  WHERE db_method = adb_method;

  OPEN adetail FOR
  SELECT *
  FROM jofl.win_action_detail
  WHERE db_method = adb_method;

  OPEN alocalization FOR
  SELECT *
  FROM jofl.ref_vocabulary
  WHERE db_method = adb_method;
  RETURN;

  EXCEPTION
  WHEN no_data_found
    THEN
      RAISE EXCEPTION USING ERRCODE = 20001, MESSAGE = 'Method <<' || adb_method || '>> not found!';
END;
$$ LANGUAGE plpgsql;

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jofl_exp_pkg$get_role_metadata(aid_role jofl.ref_role.id_role%TYPE)
  RETURNS REFCURSOR AS $$
DECLARE
  rolecall REFCURSOR;
BEGIN

  OPEN rolecall FOR
  SELECT *
  FROM jofl.role2call r2c
  WHERE r2c.id_role = aid_role;
  RETURN rolecall;
  EXCEPTION
  WHEN no_data_found
    THEN
      RAISE EXCEPTION USING ERRCODE = 20001, MESSAGE = 'Role <<' || AID_ROLE || '>> not found!';
END;
$$ LANGUAGE plpgsql;