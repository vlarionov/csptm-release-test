﻿create or replace function  jofl.jofl_proxy_pkg$get_rows_(aid_jofl_account jofl.account.id_jofl_account%type
    ,adb_method  jofl.window.db_method%type
    ,out arows refcursor
    ,av_gson         text
    ,apack_method   text default 'OF_ROWS')  returns refcursor as $$
declare
    lv_pkg        jofl.window.interface_pkg%type := null;
    ln_auth_based jofl.window.n_auth_based%type;
    lv_call text := '<no call>';
    lv_packmethod jofl.win_action_call.call_action%type := null;

    p_error_code text;
    p_column_name text;
    p_constraint_name text;
    p_data_type text;
    p_text text;
    p_table text;
    p_schema text;
    p_detail text;
    p_hint  text;
    p_call_stack text;
    p_ermsg text;

    l_get_rows_handler_func text;
begin
    begin

        if (apack_method != 'OF_ROWS')
        then
            select  coalesce(NULLIF(ca.call_default,''), ca.call_action)
            into lv_packmethod
            from jofl.win_action_call ca
            where ca.db_method = adb_method and ca.call_action = apack_method;
        else
            lv_packmethod :=  coalesce(nullif(apack_method,''), 'OF_ROWS');
        end if;


        begin
            select opt_interface_pkg
            into lv_pkg
            from jofl.win_action_call
            where db_method = adb_method and call_action = lv_packmethod;
            exception
            when no_data_found then
                lv_pkg := null;
        end;

        select  coalesce(nullif(lv_pkg,''), w.interface_pkg)
            ,w.n_auth_based
        into lv_pkg
            ,ln_auth_based
        from jofl.window w
        where w.db_method = adb_method;

        -- LN_AUTH_BASED
        -- надо учесть этот параметр...
        /*
        LN_MODIFY_OPTS := GET_MODIFY_OPTS(AID_JOFL_ACCOUNT => AID_JOFL_ACCOUNT,
                                      ADB_METHOD       => ADB_METHOD);*/
        begin
            if (av_gson is null)
            then
                if (ln_auth_based = 0)
                then
                    lv_call := 'select ' || lower(lv_pkg) || '$' || lower(lv_packmethod) ||
                               '();';

                    execute lv_call
                    into arows;
                    return;
                else
                    lv_call := 'select ' || lower(lv_pkg) || '$' || lower(lv_packmethod) ||
                               '($1);';

                    execute lv_call
                    using aid_jofl_account into arows;
                    RETURN;

                end if;
            else
                if (ln_auth_based = 0)
                then
                    lv_call := 'select ' || lower(lv_pkg) || '$' || lower(lv_packmethod) ||
                               '($1);';
                    execute lv_call
                    using av_gson into arows;
                    RETURN;
                else
                    lv_call := 'select ' || lower(lv_pkg) || '$' || lower(lv_packmethod) ||
                               '($1, $2);';
                    execute  lv_call
                    using aid_jofl_account, av_gson into arows;

                    SELECT grhh.function
                    INTO l_get_rows_handler_func
                    FROM jofl.get_rows_history_handler grhh
                        JOIN jofl.account a ON grhh.schema_name = a.schema_name
                    WHERE a.id_jofl_account = aid_jofl_account;
                    if l_get_rows_handler_func notnull and lv_packmethod = 'OF_ROWS'-- для сохранения действий пользователя открытия или фильтрации
                    then
                        EXECUTE 'SELECT ' || l_get_rows_handler_func || '($1,$2,$3);'
                        USING aid_jofl_account, adb_method, av_gson;
                    end if;

                    RETURN;
                end if;
            end if;
        end;
    end;
end ;
$$ LANGUAGE plpgsql;