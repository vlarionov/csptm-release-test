CREATE OR REPLACE FUNCTION jofl.ref_field_type_pkg$of_rows(aid_account NUMERIC
  , OUT                                                    arows       REFCURSOR
  ,                                                        attr        TEXT)
  RETURNS REFCURSOR AS $$
BEGIN
  OPEN arows FOR
  SELECT
    field_type      AS "FIELD_TYPE",
    field_type_name AS "FIELD_TYPE_NAME"
  FROM jofl.ref_field_type;
END;
$$ LANGUAGE plpgsql;