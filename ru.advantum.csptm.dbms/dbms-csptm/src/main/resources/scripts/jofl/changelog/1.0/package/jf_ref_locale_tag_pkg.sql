CREATE OR REPLACE FUNCTION jofl.jf_ref_locale_tag_pkg$attr_to_rowtype(attr TEXT)
  RETURNS TABLE(id       TEXT
  ,             scn      NUMERIC
  ,             lc_tag   jofl.ref_locale_tag.lc_tag%TYPE
  ,             tag_name jofl.ref_locale_tag.tag_name%TYPE) AS $$
DECLARE
BEGIN
  RETURN QUERY
  SELECT
    NULL :: TEXT                                                                                  AS id,
    NULL :: NUMERIC                                                                               AS scn,
    jofl.jofl_pkg$extract_varchar(attr, 'LC_TAG', FALSE) :: jofl.ref_locale_tag.LC_TAG % type     AS lc_tag,
    jofl.jofl_pkg$extract_varchar(attr, 'TAG_NAME', FALSE) :: jofl.ref_locale_tag.TAG_NAME % type AS tag_name;
END;
$$ LANGUAGE plpgsql;


------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION jofl.jf_ref_locale_tag_pkg$of_rows(aid_account NUMERIC
  , OUT                                                       arows       REFCURSOR
  ,                                                           attr        TEXT DEFAULT NULL)
  RETURNS REFCURSOR AS $$
BEGIN
  OPEN arows FOR
  SELECT
    lc_tag   AS "LC_TAG",
    tag_name AS "TAG_NAME"
  FROM jofl.ref_locale_tag src;
END;
$$ LANGUAGE plpgsql;