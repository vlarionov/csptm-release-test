﻿create or replace function  jofl.jofl_proxy_pkg$validate_permission(aid_jofl_account jofl.account.id_jofl_account%type
						,adb_method jofl.window.db_method%type
						,aaction    text) returns boolean as $$
	declare

		lv_method jofl.win_action_call.call_action%type;
		l_account_roles TEXT[];
	begin
		if (aid_jofl_account = -1 /*AND AACTION = 'OF_ROWS'*/)
		then
			return exists (select 1 from jofl.window w where w.db_method = adb_method and w.n_auth_based = 0);
		end if;

		if (jofl.jofl_pkg$isrootrole(aid_jofl_account))
		then
			/*Вам, Root, можно все  Даже того чего нет */
			return true;
		end if;

		lv_method := case
				 when aaction = 'i' then
					'OF_INSERT'
				 when aaction = 'U' then
					'OF_UPDATE'
				 when AACTION = 'D' then
					'OF_DELETE'
				 else
					aaction
			 end;

		l_account_roles := jofl.jofl_pkg$get_account_roles(aid_jofl_account);
		RETURN exists(
      SELECT 1
      FROM jofl.role2call r2c
      WHERE r2c.db_method = adb_method
            AND r2c.call_action = lv_method
            AND r2c.id_role = ANY (l_account_roles)
  );
	end ;
$$ LANGUAGE plpgsql;


	----------------------------------------------------------------------------------------------------------------
create or replace function  jofl.jofl_proxy_pkg$do_action(aid_jofl_account jofl.account.id_jofl_account%type
										 ,adb_method    jofl.window.db_method%type
										 ,aaction       text
										 ,av_gson       text
										 ,av_binary     text default null) returns text as $$
	begin
		PERFORM jofl.jofl_util$set_usr_var(jofl.const_pkg$id_jofl_account(), aid_jofl_account::TEXT);
		PERFORM jofl.jofl_util$set_usr_var(jofl.const_pkg$jofl_method(), adb_method);
		PERFORM jofl.jofl_util$set_usr_var(jofl.const_pkg$jofl_action(), aaction);

		if jofl.jofl_proxy_pkg$validate_permission(aid_jofl_account, adb_method,aaction)
		then
			return jofl.jofl_proxy_pkg$do_action_(jofl.jofl_pkg$translate(aid_jofl_account),
						      adb_method,
						      aaction,
						      av_gson,
						      av_binary);
		else
			RAISE EXCEPTION USING ERRCODE = jofl.jofl_proxy_pkg$get_error_code(), MESSAGE = '<<Метод недоступен!>>';
		end if;
		return null;
	end ;
$$ LANGUAGE plpgsql;


	----------------------------------------------------------------------------------------------------------------
create or replace function  jofl.jofl_proxy_pkg$do_action_(aid_account numeric
							  ,adb_method    jofl.window.db_method%type
							  ,aaction       text
							  ,av_gson       text
							  ,av_binary     text default null) returns text as $$
	declare
		lv_pkg    jofl.window.interface_pkg%type;
		lv_method text;
		lmsg text;
		ln_auth_based jofl.window.n_auth_based%type;

	begin

		select w.interface_pkg
			,w.n_auth_based
		into lv_pkg
		    ,ln_auth_based
		from jofl.window w
		where w.db_method = adb_method;

		if (lv_pkg is null)
		then
			RAISE EXCEPTION USING ERRCODE = 20001, MESSAGE ='<<Незаполнено поле "Интерфейс"!>>';
		end if;

		if (aaction in ('I', 'U', 'D'))
		then
			lv_method := case
					 when aaction = 'i' then
						'OF_INSERT'
					 else
						case
							when aaction = 'u' then
							 'OF_UPDATE'
							else
							 case
								 when aaction = 'd' then
									'OF_DELETE'
								 else
									null
							 end
						end
				 end;

		else
			lmsg := 'Операция успешно выполнена!';
			lv_method := aaction;
		end if;

		-- разрулим пакет...
		begin
			select coalesce(NULLIF(c.opt_interface_pkg,''), lv_pkg)
			into lv_pkg
			from jofl.win_action_call c
			where c.db_method = adb_method and c.call_action = lv_method;
		exception
			when no_data_found then
				return null;
		end;

		if (ln_auth_based = 0)
		then
			if (av_binary is null or av_binary = '{}')
			then
				execute  'select ' || lower(lv_pkg) || '$' || lower(lv_method) ||'($1::text);'
					using av_gson into lmsg;
			else
				execute 'select ' || lower(lv_pkg) || '$' || lower(lv_method) || '($1::text,
													$2::text
													);'
					Using av_gson, av_binary into lmsg;
			end if;
		else
			if (av_binary is null or av_binary = '{}')
			then
				execute 'select ' || lower(lv_pkg) || '$' || lower(lv_method) ||
					'($1::numeric, $2::text);'
					using aid_account, av_gson into lmsg;
			else
				execute 'select ' || lower(lv_pkg) || '$' || lower(lv_method) || '($1::numeric,
										    $2::text,
										    $3::text);'
					using aid_account, av_gson, av_binary into lmsg;
			end if;
		end if;
		
		return lmsg;
	end ;
$$ LANGUAGE plpgsql;

	----------------------------------------------------------------------------------------------------------------
create or replace function  jofl.jofl_proxy_pkg$get_rows(aid_jofl_account jofl.account.id_jofl_account%type
							,adb_method  jofl.window.db_method%type
							,out arows refcursor
							,av_gson         text
							,apack_method   text default 'OF_ROWS') returns refcursor as $$
	begin

		if (jofl.jofl_proxy_pkg$validate_permission(aid_jofl_account,
							    adb_method,
							    apack_method))
		then
			begin
			arows = jofl.jofl_proxy_pkg$get_rows_(jofl.jofl_pkg$translate(aid_jofl_account)::numeric,
							 adb_method::text,
							 av_gson::text,
							 apack_method::text
							 );
			RETURN;
            -- не надо душить исключения!
			-- EXCEPTION WHEN others THEN

			end;
		end if;
		RAISE EXCEPTION USING ERRCODE = jofl.jofl_proxy_pkg$get_error_code(), MESSAGE ='<<Нет доступа! (id: ' || aid_jofl_account ||')>>';
	end ;
$$ LANGUAGE plpgsql;

	----------------------------------------------------------------------------------------------------------------
create or replace function  jofl.jofl_proxy_pkg$get_rows_(aid_jofl_account jofl.account.id_jofl_account%type
							,adb_method  jofl.window.db_method%type
							,out arows refcursor
							,av_gson         text
							,apack_method   text default 'OF_ROWS')  returns refcursor as $$
	declare
		lv_pkg        jofl.window.interface_pkg%type := null;
		ln_auth_based jofl.window.n_auth_based%type;
		lv_call text := '<no call>';
		lv_packmethod jofl.win_action_call.call_action%type := null;

			p_error_code text;
        	p_column_name text;
        	p_constraint_name text;
        	p_data_type text;
        	p_text text;
        	p_table text;
        	p_schema text;
        	p_detail text;
        	p_hint  text;
        	p_call_stack text;
        	p_ermsg text;
	begin
	begin

		if (apack_method != 'OF_ROWS')
		then
			select  coalesce(NULLIF(ca.call_default,''), ca.call_action)
			into lv_packmethod
			from jofl.win_action_call ca
			where ca.db_method = adb_method and ca.call_action = apack_method;
		else
			lv_packmethod :=  coalesce(nullif(apack_method,''), 'OF_ROWS');
		end if;


		begin
			select opt_interface_pkg
			into lv_pkg
			from jofl.win_action_call
			where db_method = adb_method and call_action = lv_packmethod;
		exception
			when no_data_found then
				lv_pkg := null;
		end;

		select  coalesce(nullif(lv_pkg,''), w.interface_pkg)
					,w.n_auth_based
		into lv_pkg
				,ln_auth_based
		from jofl.window w
		where w.db_method = adb_method;

		-- LN_AUTH_BASED
		-- надо учесть этот параметр...
		/*
    LN_MODIFY_OPTS := GET_MODIFY_OPTS(AID_JOFL_ACCOUNT => AID_JOFL_ACCOUNT,
                                  ADB_METHOD       => ADB_METHOD);*/

		begin
			if (av_gson is null)
			then
				if (ln_auth_based = 0)
				then
					lv_call := 'select ' || lower(lv_pkg) || '$' || lower(lv_packmethod) ||
										 '();';

					execute lv_call
						into arows;
					return;
				else
					lv_call := 'select ' || lower(lv_pkg) || '$' || lower(lv_packmethod) ||
										 '($1);';

					execute lv_call
						using aid_jofl_account into arows;
					RETURN;

				end if;
			else
				if (ln_auth_based = 0)
				then
					lv_call := 'select ' || lower(lv_pkg) || '$' || lower(lv_packmethod) ||
										 '($1);';
					execute lv_call
						using av_gson into arows;
					RETURN;
				else
					lv_call := 'select ' || lower(lv_pkg) || '$' || lower(lv_packmethod) ||
										 '($1, $2);';
					execute  lv_call
						using aid_jofl_account, av_gson into arows;
					RETURN;
				end if;
			end if;
		end;

		EXCEPTION WHEN others THEN
        	GET STACKED DIAGNOSTICS
        		p_error_code :=RETURNED_SQLSTATE ,
        		p_column_name :=COLUMN_NAME,
        		p_constraint_name := CONSTRAINT_NAME,
        		p_data_type := PG_DATATYPE_NAME,
        		p_text := MESSAGE_TEXT,
        		p_table := TABLE_NAME,
        		p_schema := SCHEMA_NAME,
        		p_detail := PG_EXCEPTION_DETAIL,
        		p_hint  := PG_EXCEPTION_HINT,
        		p_call_stack := PG_EXCEPTION_CONTEXT;
        		begin
        		with exp as (select
                		p_error_code as error_code,
                		p_column_name as COLUMN_NAME,
                		p_constraint_name  as CONSTRAINT_NAME,
                		p_data_type as DATATYPE_NAME,
                		p_text as MESSAGE_TEXT,
                		p_table as  TABLE_NAME,
                		p_schema as SCHEMA_NAME,
                		p_detail as EXCEPTION_DETAIL,
                		p_hint  as EXCEPTION_HINT,
                		p_call_stack as stack)
                	select to_json(e.*)
                		into p_ermsg
                		from exp e;
                		RAISE EXCEPTION USING ERRCODE = p_error_code, MESSAGE = '<<JSON FOR ERROR STACK:'||p_ermsg||'>>';
               	end;
	end;
	end ;
$$ LANGUAGE plpgsql;
--------------------------------------------------------------------
create or replace   function  jofl.jofl_proxy_pkg$get_error_code() RETURNS integer AS $$
	select 20403;
$$ LANGUAGE sql strict immutable;