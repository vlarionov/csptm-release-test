CREATE OR REPLACE FUNCTION jofl.ref_field_visibility_pkg$of_rows(aid_account NUMERIC
  , OUT                                                          arows       REFCURSOR
  ,                                                              attr        TEXT DEFAULT '{}')
  RETURNS REFCURSOR AS $$
BEGIN
  OPEN arows FOR
  SELECT
    n_visibility    AS "N_VISIBILITY",
    visibility_name AS "VISIBILITY_NAME"
  FROM jofl.ref_field_visibility
  ORDER BY n_visibility;

END;
$$ LANGUAGE plpgsql;