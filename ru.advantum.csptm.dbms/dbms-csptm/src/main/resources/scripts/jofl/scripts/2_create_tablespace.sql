﻿CREATE TABLESPACE ums_data 
OWNER postgres
LOCATION '/media/zlobina/Data/POSTGRES/db/ums_data';

CREATE TABLESPACE  ums
OWNER postgres
LOCATION '/media/zlobina/Data/POSTGRES/db/ums';

CREATE TABLESPACE  temp
OWNER postgres
LOCATION '/media/zlobina/Data/POSTGRES/db/temp';

CREATE TABLESPACE  ums_idx
OWNER postgres
LOCATION '/media/zlobina/Data/POSTGRES/db/ums_idx';

CREATE TABLESPACE jofl_data
OWNER postgres
LOCATION '/media/zlobina/Data/POSTGRES/db/jofl_data';

CREATE TABLESPACE jofl_ix
OWNER postgres
LOCATION '/media/zlobina/Data/POSTGRES/db/jofl_ix';

CREATE TABLESPACE jofl_idx OWNER postgres
LOCATION '/media/zlobina/Data/POSTGRES/db/jofl_idx';