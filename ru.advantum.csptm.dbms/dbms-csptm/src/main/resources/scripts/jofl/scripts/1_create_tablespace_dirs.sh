#!/bin/sh

dbdir=$1
pguser=$2
projectname=$3

case $1 in 
*)
                echo 'DB dir must be setted'
				;;
esac				
case $2 in 
*)
                echo 'DB user owner must be setted'
				;;
esac

mkdir $dbdir/temp
mkdir $dbdir/jofl
mkdir $dbdir/jofl/jofl_data
mkdir $dbdir/jofl/jofl_idx
chown $pguser -R $dbdir