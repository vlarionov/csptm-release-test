create user jofl with password 'jofl';

-------------------------------------------------------------------------------------------------------------------

grant all privileges on database bb to jofl;

grant ALL PRIVILEGES ON SCHEMA jofl to jofl;

grant all privileges on tablespace jofl_data to jofl;
grant all privileges on tablespace jofl_idx to jofl;


grant ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA jofl to jofl;
grant ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public to jofl;

grant ALL PRIVILEGES ON ALL TABLES IN SCHEMA jofl to jofl;
grant ALL PRIVILEGES ON ALL TABLES IN SCHEMA public to jofl;

grant ALL PRIVILEGES ON ALL sequences IN SCHEMA jofl to jofl;
grant ALL PRIVILEGES ON ALL sequences IN SCHEMA public to jofl;
