

  create table ums.constants_text
(
  schema_name   text not null,
  c_name text not null,
  c_val text,
  ref_type  text not null,
  constraint pk_constants_text  primary key(c_name)   USING INDEX TABLESPACE  ums_idx
)
tablespace ums_data;

comment on column  ums.constants_text.schema_name
  is 'наименование схемы';
comment on column ums.constants_text.c_name
  is 'название константы';
  comment on column ums.constants_text.c_val
  is 'значение константы';
comment on column ums.constants_text.ref_type
  is 'ссылка на поле типа константы';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table ums.constants_num
(
  schema_name   text not null,
  c_name text not null,
  c_val numeric ,
  ref_type  text not null,
  constraint pk_constants_num  primary key(c_name)   USING INDEX TABLESPACE  ums_idx
)
tablespace ums_data;

comment on column  ums.constants_num.schema_name
  is 'наименование схемы';
comment on column ums.constants_num.c_name
  is 'название константы';
  comment on column ums.constants_num.c_val
  is 'значение константы';
comment on column ums.constants_num.ref_type
  is 'ссылка на поле типа константы';