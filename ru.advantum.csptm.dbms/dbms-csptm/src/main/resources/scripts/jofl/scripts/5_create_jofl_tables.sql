﻿create table jofl.constants_text
(
  schema_name   text not null,
  c_name text not null,
  c_val text,
  ref_type  text not null,
  constraint pk_constants_text  primary key(c_name)   USING INDEX TABLESPACE  jofl_idx
)
tablespace jofl_data;

comment on column  jofl.constants_text.schema_name
  is 'наименование схемы';
comment on column jofl.constants_text.c_name
  is 'название константы';
  comment on column jofl.constants_text.c_val
  is 'значение константы';
comment on column jofl.constants_text.ref_type
  is 'ссылка на поле типа константы';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.constants_num
(
  schema_name   text not null,
  c_name text not null,
  c_val numeric ,
  ref_type  text not null,
  constraint pk_constants_num  primary key(c_name)   USING INDEX TABLESPACE  jofl_idx
)
tablespace jofl_data;
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.jf_project
(
  schema_name   text not null,
  project_title text not null,
  row$color     NUMERIC(18),
  constraint pk_jf_project  primary key(schema_name)   USING INDEX TABLESPACE  jofl_idx
)
tablespace jofl_data;

comment on column jofl.jf_project.schema_name
  is 'наименование схемы в которую будут генерироваться объекты';
comment on column jofl.jf_project.project_title
  is 'наименование прокта';
comment on column jofl.jf_project.row$color
  is 'подсветка проекта';
  
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.account
(
  id_jofl_account  numeric(18) not null,
  schema_name      text not null,
  id_account_local numeric(18) not null,
  constraint pk_account primary key(id_jofl_account)   USING INDEX TABLESPACE  jofl_idx ,
 FOREIGN KEY  (schema_name) REFERENCES jofl.jf_project (schema_name) deferrable initially deferred
) 
tablespace jofl_data;

comment on column jofl.account.id_jofl_account
  is 'ИД Учетной записи JOFL';
comment on column jofl.account.schema_name
  is 'Наименование схемы в которую будут генерироваться объекты';
comment on column jofl.account.id_account_local
  is 'ИД Учетной записи внешней системы';


-------------------------------------------------------------------------------------------------------------------------------------------------------------------

  create table jofl.win_field_ref
(
  db_method         text not null,
  field_name        text not null,
  ref_db_method     text,
  ref_field         text not null,
  dst_field         text not null,
  is_custom_factory numeric(1,0) default 0 not null,
  ref_factory       text,
  rowid text DEFAULT nextval('jofl.sq_jofl')::text,
  constraint pk_win_field_ref primary key (db_method, field_name, dst_field)
  using index 
  tablespace jofl_idx,
  foreign key (ref_db_method)
  references jofl.window (db_method)
  deferrable initially deferred,
  foreign key (field_name, db_method)
  references jofl.win_field (field_name, db_method)
)
tablespace jofl_data;


-- add comments to the columns 
comment on column jofl.win_field_ref.db_method
  is 'Наименование метода';
comment on column jofl.win_field_ref.field_name
  is 'Наименование поля';
comment on column jofl.win_field_ref.ref_db_method
  is 'Наименование справочника';
comment on column jofl.win_field_ref.ref_field
  is 'Имя поля справочника';
comment on column jofl.win_field_ref.dst_field
  is 'Поле объекта';
comment on column jofl.win_field_ref.is_custom_factory
  is 'Признак того, что будет использована Custom фабрика';
comment on column jofl.win_field_ref.ref_factory
  is 'Класс (сервис AgileCustomPropertyEditorProvider)';


-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.window
(
  db_method            text null,
  win_title            text null,
  custom_topcomponent  text,
  custom_jscomponent   text,
  action_factory       text,
  editor_factory       text,
  interface_pkg        text,
  n_auth_based         numeric(1) not null,
  cache_min            numeric(18) default 0 not null,
  icon                 text,
  refresh_interval_sec numeric(18) default 0 not null,
  schema_name          text,
  constraint pk_window primary key (db_method) USING INDEX TABLESPACE  jofl_idx ,
 FOREIGN KEY  (schema_name) REFERENCES jofl.jf_project (schema_name) deferrable initially deferred
)
tablespace jofl_data;

comment on column jofl.window.db_method
  is 'Наименование метода';
comment on column jofl.window.custom_jscomponent
is 'JS-Класс';
comment on column jofl.window.action_factory
  is 'Класс фабрики Action';
comment on column jofl.window.editor_factory
  is 'Фабрика редактора';
comment on column jofl.window.interface_pkg
  is 'Пакет отвечающий за работу объекта';
comment on column jofl.window.n_auth_based
  is '0 - без использования параметра ID_ACCOUNT, 1 - с использованием параметра';
comment on column jofl.window.cache_min
  is 'Кол-во минут которое будет работать кэш';
comment on column jofl.window.icon
  is 'имя иконки';
comment on column jofl.window.refresh_interval_sec
  is 'Автоматически обновлять данные (сек)';
comment on column jofl.window.schema_name
  is 'Наименование схемы в которую будут генерироваться объекты';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
create table jofl.app_menu
(
  id_menu        numeric(18) not null,
  menu_title     text not null,
  id_menu_parent numeric(18),
  db_method      text,
  icon           text,
  n_order        numeric(18),
  schema_name    text,
  constraint pk_app_menu primary key (id_menu) USING INDEX TABLESPACE  jofl_idx ,
  foreign key  (schema_name) references jofl.jf_project (schema_name) deferrable initially deferred,
 foreign key  (id_menu_parent) references jofl.app_menu (id_menu) deferrable initially deferred,
 foreign key   (db_method) references jofl.window (db_method) deferrable initially deferred
)
tablespace jofl_data;

comment on column jofl.app_menu.id_menu
  is 'ИД Меню';
comment on column jofl.app_menu.menu_title
  is 'Наименование меню';
comment on column jofl.app_menu.id_menu_parent
  is 'Меню родитель';
comment on column jofl.app_menu.db_method
  is 'Наименование метода';
comment on column jofl.app_menu.icon
  is 'имя иконки';
comment on column jofl.app_menu.n_order
  is 'Порядок сортировки';
comment on column jofl.app_menu.schema_name
  is 'Наименование схемы в которую будут генерироваться объекты';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.create$java$lob$table
(
  name     text,
  lob      bytea,
  loadtime timestamp without time zone,
  constraint pku_createjavalobtable UNIQUE  (name) USING INDEX tablespace jofl_data
)
tablespace jofl_data;

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.java$options
(
  what  text,
  opt   text,
  value text
)
tablespace jofl_data;

-------------------------------------------------------------------------------------------------------------------------------------------------------------------


create table jofl.ref_const_type
(
  field_cons_type  numeric(1) not null,
  field_const_name text not null,
  constraint pk_ref_const_type primary key (field_cons_type) USING INDEX TABLESPACE  jofl_idx 
)
tablespace jofl_data;

comment on column jofl.ref_const_type.field_cons_type
  is 'Тип поля: 0 - PK, 1 - PK_WRITEABLE, 1 - RW, 2 - RO';
comment on column jofl.ref_const_type.field_const_name
  is 'Наименование Constraint';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.ref_editor
(
  ref_factory text
)
tablespace jofl_data;
comment on column jofl.ref_editor.ref_factory
  is 'Класс (сервис AgileCustomPropertyEditorProvider)';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.ref_field_type
(
  field_type      char(1) not null,
  field_type_name TEXT not null,
  constraint  pk_ref_field_type primary key (field_type) USING INDEX TABLESPACE  jofl_idx 
)
tablespace jofl_data;
comment on column jofl.ref_field_type.field_type
  is 'Тип данных: N, V, T, D';
comment on column jofl.ref_field_type.field_type_name
  is 'Наименование типа данных';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.ref_field_visibility
(
  n_visibility    numeric(1) not null,
  visibility_name text not null,
  constraint pk_ref_field_visibility primary key (n_visibility) USING INDEX TABLESPACE  jofl_idx 
)
tablespace jofl_data;
comment on column jofl.ref_field_visibility.n_visibility
  is 'Видимость поля: 0 - NONE, 1 - PROP, 2 - OUTLINE, 3 - BOTH';
comment on column jofl.ref_field_visibility.visibility_name
  is 'Наименование видимости';
	
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.ref_filter
(
  filter_class  text not null,
  init_template text,
  constraint pk_ref_filter primary key (filter_class) USING INDEX TABLESPACE  jofl_idx 
)
tablespace jofl_data;

comment on column jofl.ref_filter.filter_class
  is 'Класс фабрики фильтров';
comment on column jofl.ref_filter.init_template
  is 'Шаблон параметров инициализации';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.ref_ui_platform
(
  id_platform text not null,
  plat_name   TEXT not null,
  rowid text DEFAULT nextval('jofl.sq_jofl')::text,
  constraint pk_ref_ui_platform primary key (id_platform) using index tablespace jofl_idx
)
tablespace jofl_data;

comment on column jofl.ref_ui_platform.id_platform
  is 'Платформа';
comment on column jofl.ref_ui_platform.plat_name
  is 'Наименование';
  
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.ref_filter_source
(
  id_platform  text not null,
  filter_class text not null,
  source_class text not null,
  constraint pk_ref_filter_source primary key (id_platform, filter_class) using index tablespace jofl_idx,
  foreign key (id_platform)  references jofl.ref_ui_platform (id_platform) deferrable initially deferred,
  foreign key (filter_class) references jofl.ref_filter (filter_class) deferrable initially deferred
)
tablespace jofl_data; 

comment on column jofl.ref_filter_source.id_platform
  is 'Платформа';
comment on column jofl.ref_filter_source.filter_class
  is 'Класс фабрики фильтров';
comment on column jofl.ref_filter_source.source_class
  is 'Ссылка на java/js класс фильтра';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.ref_locale
(
  lc_locale char(5) not null,
  rowid text DEFAULT nextval('jofl.sq_jofl')::text,
  constraint pk_ref_locale primary key (lc_locale) using index tablespace jofl_idx  
)
tablespace jofl_data;

comment on table jofl.ref_locale
  is 'Поддерживаемые языки';
comment on column jofl.ref_locale.lc_locale
  is 'Language and Country';
  
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.ref_locale_tag
(
  lc_tag   text not null,
  rowid text DEFAULT nextval('jofl.sq_jofl')::text,
  tag_name text not null,
  constraint pk_ref_locale_tag primary key (lc_tag)
  using index 
  tablespace jofl_idx
)
tablespace jofl_data;

comment on table jofl.ref_locale_tag
  is 'Типы ключей для локализации';
comment on column jofl.ref_locale_tag.lc_tag
  is 'Тип';
comment on column jofl.ref_locale_tag.tag_name
  is 'Наименование';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.ref_role
(
  id_role   text not null,
  role_name text not null,
  is_active numeric(1) not null,
  constraint pk_jofl_role primary key (id_role)
  using index 
  tablespace jofl_idx
)
tablespace jofl_data;

comment on column jofl.ref_role.id_role
  is 'ИД роли';
comment on column jofl.ref_role.role_name
  is 'Наименование роли';
comment on column jofl.ref_role.is_active
  is 'Активна';
  
-------------------------------------------------------------------------------------------------------------------------------------------------------------------


create table jofl.ref_vocabulary
(
  db_method text not null,
  lc_locale char(5) not null,
  lc_tag    text not null,
  lc_key    text,
  lc_value  text not null,
  rowid text DEFAULT nextval('jofl.sq_jofl')::text,
  constraint uq_ref_vocabulary_translate unique (db_method, lc_locale, lc_tag, lc_key)
  using index 
  tablespace jofl_idx,
  foreign key (lc_locale)
  references jofl.ref_locale (lc_locale)
  deferrable initially deferred,
  foreign key (lc_tag)
  references jofl.ref_locale_tag (lc_tag)
  deferrable initially deferred,
  foreign key (db_method)
  references jofl.window (db_method)
  deferrable initially deferred
)
tablespace jofl_data;

comment on table jofl.ref_vocabulary
  is 'Словарь';
comment on column jofl.ref_vocabulary.db_method
  is 'Наименование метода';
comment on column jofl.ref_vocabulary.lc_locale
  is 'Language and Country';
comment on column jofl.ref_vocabulary.lc_tag
  is 'Тип';
comment on column jofl.ref_vocabulary.lc_key
  is 'Ключ';
comment on column jofl.ref_vocabulary.lc_value
  is 'Значение';
  
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.role2account
(
  id_role         text not null,
  id_jofl_account numeric(18) not null,
  constraint pk_role2account primary key (id_role, id_jofl_account)
  using index 
  tablespace jofl_idx,  
  foreign key (id_role)
  references jofl.ref_role (id_role)
  deferrable initially deferred,
 foreign key (id_jofl_account)
  references jofl.account (id_jofl_account)
  deferrable initially deferred
)
tablespace jofl_data;

comment on column jofl.role2account.id_role
  is 'ИД роли';
comment on column jofl.role2account.id_jofl_account
  is 'ИД Учетной записи JOFL';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.win_action_call
(
  db_method         text not null,
  call_action       text not null,
  action_title      text not null,
  is_refresh        numeric(1) default 0 not null,
  is_internal       numeric(1) default 0 not null,
  is_win_action     numeric(1) default 0 not null,
  n_sort_order      numeric(18),
  is_row_depend     numeric(1) default 0 not null,
  call_default      text,
  opt_interface_pkg text,
  constraint pk_win_action_call primary key (db_method, call_action)
  using index 
  tablespace jofl_idx,
  foreign key (db_method)
  references jofl.window (db_method)
  deferrable initially deferred
)
tablespace jofl_data;

comment on column jofl.win_action_call.db_method
  is 'Наименование метода';
comment on column jofl.win_action_call.call_action
  is 'Наименование метода (DO_ACTION)';
comment on column jofl.win_action_call.action_title
  is 'Отображение ACTION';
comment on column jofl.win_action_call.is_refresh
  is 'Требуется обновление';
comment on column jofl.win_action_call.is_internal
  is 'Внутренний вызов (INSERT, UPDATE, DELETE)';
comment on column jofl.win_action_call.is_win_action
  is 'Метод окна';
comment on column jofl.win_action_call.n_sort_order
  is 'Порядок сортировки';
comment on column jofl.win_action_call.is_row_depend
  is 'Строкозависимый';
comment on column jofl.win_action_call.call_default
  is 'Значения по умолчанию';
comment on column jofl.win_action_call.opt_interface_pkg
  is 'Пакет (опционально)';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.role2call
(
  id_role     text not null,
  db_method   text not null,
  call_action text not null,
  constraint pk_role2call primary key (id_role, db_method, call_action)
  using index 
  tablespace jofl_idx,
  foreign key (id_role)
  references jofl.ref_role (id_role)
  deferrable initially deferred,
foreign key (db_method, call_action)
  references jofl.win_action_call (db_method, call_action)
  deferrable initially deferred
)
tablespace jofl_data;

comment on column jofl.role2call.id_role
  is 'ИД роли';
comment on column jofl.role2call.db_method
  is 'Наименование метода';
comment on column jofl.role2call.call_action
  is 'Наименование метода (DO_ACTION)';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.win_action_detail
(
  db_method        text not null,
  action_title     text not null,
  detail_db_method text not null,
  is_row_depend    numeric(1) default 0 not null,
  n_sort_order     numeric(18) default 0 not null,
  constraint pk_win_detail primary key (db_method, detail_db_method)
  using index 
  tablespace jofl_idx,
  foreign key (db_method)
  references jofl.window (db_method)
  deferrable initially deferred,
  foreign key (detail_db_method)
  references jofl.window (db_method)
  deferrable initially deferred
)
tablespace jofl_data;
  
comment on column jofl.win_action_detail.db_method
  is 'Наименование метода';
comment on column jofl.win_action_detail.action_title
  is 'Отображение ACTION';
comment on column jofl.win_action_detail.detail_db_method
  is 'Наименование метода';
comment on column jofl.win_action_detail.is_row_depend
  is 'Строкозависимая деталь';
comment on column jofl.win_action_detail.n_sort_order
  is 'Порядок сортировки';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.role2detail
(
  id_role          text not null,
  db_method        text not null,
  detail_db_method text not null,
  constraint pk_role2detail primary key (id_role, db_method, detail_db_method)
  using index 
  tablespace jofl_data,
  foreign key (id_role)
  references jofl.ref_role (id_role)
  deferrable initially deferred,
  foreign key (db_method, detail_db_method)
  references jofl.win_action_detail (db_method, detail_db_method)
  deferrable initially deferred
)
tablespace jofl_data;

comment on column jofl.role2detail.id_role
  is 'ИД роли';
comment on column jofl.role2detail.db_method
  is 'Наименование метода';
comment on column jofl.role2detail.detail_db_method
  is 'Наименование метода';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.win_action_field
(
  id_afield     text not null,
  db_method     text not null,
  call_action   text not null,
  filter_class  text not null,
  initial_param text,
  n_sort_order  numeric(18),
  field_title   text,
  rowid text DEFAULT nextval('jofl.sq_jofl')::text,
  constraint pk_win_action_field primary key (id_afield)
  using index 
  tablespace jofl_idx,
  foreign key (db_method, call_action)
  references jofl.win_action_call (db_method, call_action)
  deferrable initially deferred,
  foreign key (filter_class)
  references jofl.ref_filter (filter_class)
  deferrable initially deferred
)
tablespace jofl_data;
comment on column jofl.win_action_field.db_method
  is 'Наименование метода';
comment on column jofl.win_action_field.call_action
  is 'Наименование метода (DO_ACTION)';
comment on column jofl.win_action_field.filter_class
  is 'Класс фабрики фильтров';
comment on column jofl.win_action_field.initial_param
  is 'Параметр инициализации (JSON-MAP)';
comment on column jofl.win_action_field.n_sort_order
  is 'Порядок сортировки';
comment on column jofl.win_action_field.field_title
  is 'Наименование фильтра';

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.ref_const_type
(
  field_cons_type  numeric(1) not null,
  field_const_name text not null,
  constraint pk_ref_const_type primary key (field_cons_type)
  using index 
  tablespace jofl_idx
)
tablespace jofl_data;

comment on column jofl.ref_const_type.field_cons_type
  is 'Тип поля: 0 - PK, 1 - PK_WRITEABLE, 1 - RW, 2 - RO';
comment on column jofl.ref_const_type.field_const_name
  is 'Наименование Constraint';
  
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.win_field
(
  db_method           text not null,
  field_name          text not null,
  field_cons_type     numeric(1) not null,
  field_type          char(1) not null,
  display_title       text not null,
  display_description text,
  default_value       text,
  n_order             numeric(18) not null,
  n_visibility        numeric(1) not null,
  is_display_name     numeric(1) default 0 not null,
  is_mandatory        numeric(1) default 0 not null,
  pattern             text,
  max_len             numeric(18),
  weight              numeric(18) default 1 not null,
	rowid text DEFAULT nextval('jofl.sq_jofl')::text,
  constraint pk_win_field primary key (field_name, db_method)
  using index 
  tablespace jofl_idx,
  foreign key (field_cons_type)
  references jofl.ref_const_type (field_cons_type)
  deferrable initially deferred,
  foreign key (field_type)
  references jofl.ref_field_type (field_type)
  deferrable initially deferred,
  foreign key (n_visibility)
  references jofl.ref_field_visibility (n_visibility)
  deferrable initially deferred,
  foreign key (db_method)
  references jofl.window (db_method)
  deferrable initially deferred
)
tablespace jofl_data;
  
comment on column jofl.win_field.db_method
  is 'Наименование метода';
comment on column jofl.win_field.field_name
  is 'Имя поля в БД ';
comment on column jofl.win_field.field_cons_type
  is 'Тип поля: 0 - PK, 1 - PK_WRITEABLE, 1 - RW, 2 - RO';
comment on column jofl.win_field.field_type
  is 'Тип данных: N, V, T, D';
comment on column jofl.win_field.display_title
  is 'Наименование объекта';
comment on column jofl.win_field.display_description
  is 'Описание объекта';
comment on column jofl.win_field.default_value
  is 'Значение по умолчанию';
comment on column jofl.win_field.n_order
  is 'Порядок полей';
comment on column jofl.win_field.n_visibility
  is 'Видимость поля: 0 - NONE, 1 - PROP, 2 - OUTLINE, 3 - BOTH';
comment on column jofl.win_field.is_display_name
  is 'Является DisplayName';
comment on column jofl.win_field.is_mandatory
  is 'Признак обязательности поля';
comment on column jofl.win_field.pattern
  is 'Формат поля';
comment on column jofl.win_field.max_len
  is 'Длина значения в символах';
comment on column jofl.win_field.weight
  is 'Вес поля';
  
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.win_filter
(
  db_method     text not null,
  filter_class  text not null,
  initial_param text,
  grid_x        numeric(18) not null,
  grid_y        numeric(18) not null,
  filter_title  text,
  id_filter     text not null,
  rowid text DEFAULT nextval('jofl.sq_jofl')::text,
  constraint pk_win_filter primary key (id_filter, db_method)
  using index 
  tablespace jofl_idx,
  foreign key (filter_class)
  references jofl.ref_filter (filter_class)
  deferrable initially deferred,
  foreign key (db_method)
  references jofl.window (db_method)
  deferrable initially deferred
)
tablespace jofl_data;

comment on column jofl.win_filter.db_method
  is 'Наименование метода';
comment on column jofl.win_filter.filter_class
  is 'Класс фабрики фильтров';
comment on column jofl.win_filter.initial_param
  is 'Параметр инициализации (JSON-MAP)';
comment on column jofl.win_filter.grid_x
  is 'Положение Х';
comment on column jofl.win_filter.grid_y
  is 'Положение Y';
comment on column jofl.win_filter.filter_title
  is 'Наименование поля';
  
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

create table jofl.win_filter_opts
(
  db_method         text not null,
  but_apply_caption text not null,
  but_reset_caption text not null,
  is_reset_present  numeric(1) not null,
  rowid text DEFAULT nextval('jofl.sq_jofl')::text,
  constraint pk_win_filter_opts primary key (db_method)
  using index 
  tablespace jofl_idx,
  foreign key (db_method)
  references jofl.window (db_method)
  deferrable initially deferred
)
tablespace jofl_data;

comment on column jofl.win_filter_opts.db_method
  is 'Наименование метода';
comment on column jofl.win_filter_opts.but_apply_caption
  is 'Текст "Apply"';
comment on column jofl.win_filter_opts.but_reset_caption
  is 'Текст "Reset"';
comment on column jofl.win_filter_opts.is_reset_present
  is 'Видно Reset';
  
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------

  create table jofl.ref_menu_vocabulary
  (
    lc_locale  char (5) not null ,
    id_menu    numeric (18,0) not null ,
    rowid text DEFAULT nextval('jofl.sq_jofl')::text,
    menu_title text not null,
    constraint pk_ref_menu_vocabulary primary key ( lc_locale, id_menu ) using index tablespace jofl_idx ,
    foreign key ( lc_locale ) references jofl.ref_locale ( lc_locale ) ,
    foreign key ( id_menu ) references jofl.app_menu ( id_menu ) 
  ) 
  tablespace jofl_data;
comment on column jofl.ref_menu_vocabulary.lc_locale
is
  'Language and Country' ;
  comment on column jofl.ref_menu_vocabulary.id_menu
is
  'ИД Меню' ;
  comment on column jofl.ref_menu_vocabulary.menu_title
is
  'Название' ;

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------


CREATE TABLE jofl.get_roles_func(
 schema_name Text NOT NULL,
 function Text NOT NULL
)
TABLESPACE jofl_data
;

COMMENT ON TABLE jofl.get_roles_func IS 'Хранимые функции для получения массива доступных ролей в зависимости от проекта и jofl аккаунта, которые должны принимать id_jofl_account NUMERIC и возвращать массив id_role TEXT[]'
;
COMMENT ON COLUMN jofl.get_roles_func.schema_name IS 'JOFL проект'
;
COMMENT ON COLUMN jofl.get_roles_func.function IS 'Полное имя функции.'
;

ALTER TABLE jofl.get_roles_func ADD CONSTRAINT pk_get_roles_func PRIMARY KEY (schema_name)
 USING INDEX TABLESPACE jofl_idx
;

ALTER TABLE jofl.get_roles_func ADD CONSTRAINT fk_get_roles_func_jf_project FOREIGN KEY (schema_name)
REFERENCES jofl.jf_project (schema_name) ON DELETE NO ACTION ON UPDATE NO ACTION
;