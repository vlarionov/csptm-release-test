create user jfstorage with password 'jfstorage';
revoke all privileges on database bb from jfstorage;
grant connect on  DATABASE bb to  jfstorage;
grant all privileges on all functions in schema jofl to jfstorage;