create or replace function jofl.constant_num(text, text)  returns numeric AS $$
  select c_val from jofl.constants_num where schema_name=$1 and c_name=$2;
$$ LANGUAGE SQL STRICT IMMUTABLE;