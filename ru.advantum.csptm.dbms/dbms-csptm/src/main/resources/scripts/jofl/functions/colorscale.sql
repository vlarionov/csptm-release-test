create or replace function jofl.ColorScale(nArg    real,                      -- аргумент
                    nMin    real   default 0,           -- мин. значение
                    nMax    real   default 7,           -- макс. значение
                    sClr    text   default '01234567',  -- 2-8 цветов для перехода (символы '0'..'7')
                    nBrt   integer default 180          -- яркость (-255..+255)
) returns integer as $$
declare
  MC     integer;
  OC     integer;
  R1     integer;
  G1     integer;
  B1     integer;
  R2     integer;
  G2     integer;
  B2     integer;
  X      real;
  NX     integer;
  C1     integer;
  C2     integer;
  CX     integer;
begin
    if nArg < 0 or nMin < 0 or nMax <= nMin or not regexp_like(sClr, '^[0-7]{2,8}$')
    or nBrt not between -255 and 255
  then RAISE EXCEPTION USING ERRCODE = 20100, MESSAGE = 'Ошибка в параметрах'; end if;

  MC := Least(nBrt,0)+255; -- яркость главных компонентов
  OC := Greatest(nBrt,0);  -- яркость остальных компонентов
  X  := Least(case nMin when 0 then nArg/nMax else Ln(Greatest(nArg/nMin, 1)) / Ln(nMax/nMin) end, 0.999999) * (Length(sClr)-1); -- 0..1
  NX := Trunc(X); X := X-NX; -- 0..1

  C1 := Substr(sClr, NX+1, 1); -- цвет "из"
  C2 := Substr(sClr, NX+2, 1); -- цвет "в"
  R1 := case C1 & 4 when 0 then OC else MC end; -- компоненты цвета "из"
  G1 := case C1 & 2 when 0 then OC else MC end;
  B1 := case C1 & 1 when 0 then OC else MC end;
  R2 := case C2 & 4 when 0 then OC else MC end; -- компоненты цвета "в"
  G2 := case C2 & 2 when 0 then OC else MC end;
  B2 := case C2 & 1 when 0 then OC else MC end;
  CX := Round(R1*(1.0-X) + R2*X) + (Round(G1*(1.0-X) + G2*X) + Round(B1*(1.0-X) + B2*X) * 256) * 256;

  --dbms_output.put_line('MC='||MC||', OC='||OC||', X='||To_Char(X, '0.9999')||', NX='||NX||', C1='||C1||', C2='||C2||', CX='||To_Char(CX, 'XXXXXXXX'));
  return cx;
end;
$$ LANGUAGE plpgsql;