
-- Table mvt.termo_normativ
CREATE TABLE mvt.termo_normativ(
  t_normativ_id BigSerial NOT NULL,
  date_from Timestamp NOT NULL,
  date_to Timestamp NOT NULL,
  check_delay Smallint NOT NULL,
  t_cab_from Smallint NOT NULL,
  t_cab_to Smallint NOT NULL,
  t_saloon_from Smallint NOT NULL,
  t_saloon_to Smallint NOT NULL,
  allowed_sensor_difference Smallint NOT NULL,
  allowed_sensor_difference_in_packet Smallint NOT NULL,
  comment Text,
  norm_name Text NOT NULL,
  is_default Boolean NOT NULL DEFAULT FALSE
)
TABLESPACE mvt_data;

COMMENT ON TABLE mvt.termo_normativ IS 'Нормативы температуры воздуха в ТС';
COMMENT ON COLUMN mvt.termo_normativ.t_normativ_id IS 'id';
COMMENT ON COLUMN mvt.termo_normativ.date_from IS 'Дата с – начала периода действия норматива';
COMMENT ON COLUMN mvt.termo_normativ.date_to IS 'Дата по – окончание периода действия норматива';
COMMENT ON COLUMN mvt.termo_normativ.check_delay IS 'задержка начала проверки температуры, относительно начала рейса';
COMMENT ON COLUMN mvt.termo_normativ.t_cab_from IS 'начальная температура диапазона допустимой температуры в кабине водителя';
COMMENT ON COLUMN mvt.termo_normativ.t_cab_to IS 'конечная температура диапазона допустимой температуры в кабине водителя';
COMMENT ON COLUMN mvt.termo_normativ.t_saloon_from IS 'начальная температура диапазона допустимой температуры в салоне ТС';
COMMENT ON COLUMN mvt.termo_normativ.t_saloon_to IS 'конечная температура диапазона допустимой температуры в салоне ТС';
COMMENT ON COLUMN mvt.termo_normativ.allowed_sensor_difference IS 'допустимый перепад показаний одного датчика (последовательно по времени)';
COMMENT ON COLUMN mvt.termo_normativ.allowed_sensor_difference_in_packet IS 'допустимый перепад показаний любых двух датчиков(кроме кабины)';
COMMENT ON COLUMN mvt.termo_normativ.comment IS 'комментарий';
COMMENT ON COLUMN mvt.termo_normativ.norm_name IS 'Название норматива';
COMMENT ON COLUMN mvt.termo_normativ.is_default IS 'норматив по умолчанию';

-- Add keys for table mvt.termo_normativ
ALTER TABLE mvt.termo_normativ ADD CONSTRAINT pk_t_normativ PRIMARY KEY (t_normativ_id);
ALTER INDEX mvt.pk_t_normativ SET tablespace mvt_idx;

-- нормативов по умолчанию может быть несколько, но у них должны не пересекаться месяцы

--может быть только один норматив по умолчанию
--CREATE UNIQUE INDEX ix_unique_default ON mvt.termo_normativ (is_default) TABLESPACE mvt_idx WHERE (is_default = true);
--======================================================================================================================

-- Table mvt.tr_model2tn
CREATE TABLE mvt.tr_model2tn(
  tr_model2tn_id BigSerial NOT NULL,
  t_normativ_id Bigint NOT NULL,
  tr_model_id Bigint NOT NULL
)
TABLESPACE mvt_data;
COMMENT ON TABLE mvt.tr_model2tn IS 'Модели ТС для норматива';
COMMENT ON COLUMN mvt.tr_model2tn.tr_model2tn_id IS 'id';
COMMENT ON COLUMN mvt.tr_model2tn.t_normativ_id IS 'id норматива';
COMMENT ON COLUMN mvt.tr_model2tn.tr_model_id IS 'tr_id';
-- Create indexes for table mvt.tr_model2tn
CREATE INDEX ix_tr_model2tn_t_normativ_id ON mvt.tr_model2tn (t_normativ_id) TABLESPACE mvt_idx;
CREATE INDEX ix_tr_model2tn_tr_model_id ON mvt.tr_model2tn (tr_model_id) TABLESPACE mvt_idx;
-- Add keys for table mvt.tr_model2tn
ALTER TABLE mvt.tr_model2tn ADD CONSTRAINT pk_tr_model2tn PRIMARY KEY (tr_model2tn_id);
ALTER INDEX mvt.pk_tr_model2tn SET tablespace mvt_idx;

CREATE UNIQUE INDEX ix_model2tn_norm_id_model_id ON mvt.tr_model2tn (t_normativ_id,tr_model_id) TABLESPACE mvt_idx;
--+++++=================================================================================================================

CREATE TABLE mvt.months2tn(
  months2tn_id BigSerial NOT NULL,
  month Smallint,
  t_normativ_id Bigint
)
TABLESPACE mvt_data;

COMMENT ON TABLE mvt.months2tn IS 'Месяцы (1..12), на которые распространяется норматив';
COMMENT ON COLUMN mvt.months2tn.months2tn_id IS 'id';
COMMENT ON COLUMN mvt.months2tn.month IS 'номер месяца (1-12)';
COMMENT ON COLUMN mvt.months2tn.t_normativ_id IS 'id норматива';
-- Add keys for table mvt.months2tn
ALTER TABLE mvt.months2tn ADD CONSTRAINT pk_months2tn_id PRIMARY KEY (months2tn_id);
ALTER INDEX mvt.pk_tr_model2tn SET tablespace mvt_idx;
-- Create indexes for table mvt.months2tn
CREATE INDEX ix_months2tn_t_normativ_id ON mvt.months2tn (t_normativ_id) TABLESPACE mvt_idx;
CREATE UNIQUE INDEX ix_months2tn_norm_id_month_id ON mvt.months2tn (month,t_normativ_id) TABLESPACE mvt_idx;

-- =====================================================================================================================

-- Table mvt.config_param

CREATE TABLE mvt.config_param(
  config_id Smallserial NOT NULL,
  description Text NOT NULL,
  value SMALLINT NOT NULL,
  CONSTRAINT non_negative CHECK (value > -1)
)
TABLESPACE mvt_data;

COMMENT ON TABLE mvt.config_param IS 'Настроечные параметры системы';
COMMENT ON COLUMN mvt.config_param.config_id IS 'id';
COMMENT ON COLUMN mvt.config_param.description IS 'описание';
COMMENT ON COLUMN mvt.config_param.value IS 'значение параметра';


--======================================================================================================================
-- Table mvt.agg_termo
CREATE TABLE mvt.agg_termo(
  agg_temperature_id BigSerial NOT NULL,
  out_of_range_period tsrange NOT NULL,
  t_salon FLOAT NOT NULL,
  tr_id Bigint NOT NULL,
  depo_id Bigint,
  territory_id Bigint,
  tr_model_id Bigint,
  route_id Bigint,
  driver_id Integer,
  tt_action_id Integer,
  t_normativ_id Bigint,
  round_code Text
)TABLESPACE mvt_data;

COMMENT ON TABLE mvt.agg_termo IS 'Обработанные показания датчиков внутрисалонной температуры';
COMMENT ON COLUMN mvt.agg_termo.agg_temperature_id IS 'id';
COMMENT ON COLUMN mvt.agg_termo.out_of_range_period IS 'период отклонения температуры от норматива';
COMMENT ON COLUMN mvt.agg_termo.t_salon IS 'средняя температура в салоне ТС, °C';
COMMENT ON COLUMN mvt.agg_termo.tr_id IS 'ТС, tr_id';
COMMENT ON COLUMN mvt.agg_termo.depo_id IS 'Парк, depo_id';
COMMENT ON COLUMN mvt.agg_termo.territory_id IS 'Территория, territory_id';
COMMENT ON COLUMN mvt.agg_termo.tr_model_id IS 'Модель СТ, tr_model_id';
COMMENT ON COLUMN mvt.agg_termo.route_id IS 'Маршрут, route_id';
COMMENT ON COLUMN mvt.agg_termo.driver_id IS 'Водитель, driver_id';
COMMENT ON COLUMN mvt.agg_termo.tt_action_id IS 'id рейса';

-- Create indexes for table mvt.agg_termo
CREATE INDEX ix_agg_termo_tr_id ON mvt.agg_termo (tr_id) TABLESPACE mvt_idx;
CREATE INDEX ix_agg_termo_depo_id ON mvt.agg_termo (depo_id) TABLESPACE mvt_idx;
CREATE INDEX ix_agg_termo_territory_id ON mvt.agg_termo (territory_id) TABLESPACE mvt_idx;
CREATE INDEX ix_agg_termo_tr_model_id ON mvt.agg_termo (tr_model_id) TABLESPACE mvt_idx;
CREATE INDEX ix_agg_termo_route_id ON mvt.agg_termo (route_id) TABLESPACE mvt_idx;
CREATE INDEX ix_agg_termo_driver_id ON mvt.agg_termo (driver_id) TABLESPACE mvt_idx;
CREATE INDEX ix_agg_termo_tt_action_id ON mvt.agg_termo (tt_action_id) TABLESPACE mvt_idx;
CREATE INDEX ix_agg_termo_t_normativ_id ON mvt.agg_termo (t_normativ_id) TABLESPACE mvt_idx;
CREATE INDEX ix_agg_termo_out_period ON mvt.agg_termo (lower(out_of_range_period)) TABLESPACE mvt_idx;
-- Add keys for table mvt.agg_termo
ALTER TABLE mvt.agg_termo ADD CONSTRAINT agg_temperature_id PRIMARY KEY (agg_temperature_id);

--======================================================================================================================
-- CREATE TABLE mvt.mean_temp(
--   mean_temp_id BigSerial NOT NULL,
--   time_from TIMESTAMP NOT NULL,
--   time_to   TIMESTAMP NOT NULL,
--   depo_id Bigint,
--   tr_id Bigint,
--   avg_t_salon DOUBLE PRECISION NOT NULL
-- )TABLESPACE mvt_data;
--
-- COMMENT ON TABLE mvt.mean_temp IS 'Средняя температура на ТС по периодам';
-- COMMENT ON COLUMN mvt.mean_temp.mean_temp_id IS 'id';
-- COMMENT ON COLUMN mvt.mean_temp.time_from IS 'период c, усреднения температуры';
-- COMMENT ON COLUMN mvt.mean_temp.time_to   IS 'период по, усреднения температуры';
-- COMMENT ON COLUMN mvt.mean_temp.depo_id IS 'Парк, depo_id';
-- COMMENT ON COLUMN mvt.mean_temp.tr_id IS 'ТС, tr_id';
-- COMMENT ON COLUMN mvt.mean_temp.avg_t_salon IS 'средняя температура в салоне ТС, °C';
--
-- -- Create indexes for table mvt.agg_termo
-- CREATE INDEX ix_mean_temp_tr_id ON mvt.mean_temp (tr_id) TABLESPACE mvt_idx;
-- CREATE INDEX ix_mean_temp_depo_id ON mvt.mean_temp (depo_id) TABLESPACE mvt_idx;
-- CREATE INDEX ix_mean_temp_time_from ON mvt.mean_temp (time_from) TABLESPACE mvt_idx;
-- CREATE INDEX ix_mean_temp_time_to   ON mvt.mean_temp (time_to)   TABLESPACE mvt_idx;
-- -- Add keys for table mvt.agg_termo
-- ALTER TABLE mvt.mean_temp ADD CONSTRAINT mean_temp_id PRIMARY KEY (mean_temp_id);

CREATE TABLE mvt.mean_temp(
  mean_temp_id BigSerial NOT NULL,
  time_from Timestamp NOT NULL,
  time_to Timestamp NOT NULL,
  depo_id Bigint,
  tr_id Bigint,
  avg_t_salon Double precision
)
TABLESPACE mvt_data;

COMMENT ON TABLE mvt.mean_temp IS 'Средняя температура на ТС по периодам';
COMMENT ON COLUMN mvt.mean_temp.mean_temp_id IS 'id';
COMMENT ON COLUMN mvt.mean_temp.time_from IS 'период c, усреднения температуры';
COMMENT ON COLUMN mvt.mean_temp.time_to IS 'период по, усреднения температуры';
COMMENT ON COLUMN mvt.mean_temp.depo_id IS 'Парк, depo_id';
COMMENT ON COLUMN mvt.mean_temp.tr_id IS 'ТС, tr_id';
-- Create indexes for table mvt.mean_temp
CREATE INDEX ix_mean_temp_time_from ON mvt.mean_temp (time_from) TABLESPACE mvt_idx;
CREATE INDEX ix_mean_temp_tr_id ON mvt.mean_temp (tr_id) TABLESPACE mvt_idx;
CREATE INDEX ix_mean_temp_depo_id ON mvt.mean_temp (depo_id) TABLESPACE mvt_idx;
CREATE INDEX ix_mean_temp_time_to ON mvt.mean_temp (time_to) TABLESPACE mvt_idx;
-- Add keys for table mvt.mean_temp
ALTER TABLE mvt.mean_temp ADD CONSTRAINT mean_temp_id PRIMARY KEY (mean_temp_id);

-- *********************************************************************************************************************
CREATE TABLE mvt.notification(
  notification_id BigSerial NOT NULL,
  tr_id Bigint,
  dt Timestamp NOT NULL
)
TABLESPACE mvt_data;

COMMENT ON TABLE mvt.notification IS 'Оповещения отклонения температуры от норматива';
COMMENT ON COLUMN mvt.notification.notification_id IS 'id';
COMMENT ON COLUMN mvt.notification.dt IS 'время';
COMMENT ON COLUMN mvt.notification.tr_id IS 'ТС, tr_id';

-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
CREATE TABLE mvt.notification_cabin(
  notification_cab_id BigSerial NOT NULL,
  tr_id Bigint,
  dt Timestamp NOT NULL
)
TABLESPACE mvt_data;

COMMENT ON TABLE mvt.notification_cabin IS 'Оповещения отклонения температуры от норматива в кабине';
COMMENT ON COLUMN mvt.notification_cabin.notification_cab_id IS 'id';
COMMENT ON COLUMN mvt.notification_cabin.dt IS 'время';
COMMENT ON COLUMN mvt.notification_cabin.tr_id IS 'ТС, tr_id';

