create or replace function mvt."jf_tr_temperature_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  returns refcursor
LANGUAGE plpgsql
AS $$
declare
  l_tr_id BIGINT = jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
begin
  open p_rows for

  WITH t1 AS (
      SELECT ((val :: JSONB) -> 'trs') -> l_tr_id::TEXT str
      FROM core.redis_server
      WHERE key = 'termo_sensor'
  ),
      t3 AS (
        SELECT t1.str -> 'vals' AS vals
        FROM t1
    ),
      t2 AS (
        SELECT jsonb_object_keys(vals) AS key
        FROM t3
    ),
      all_data AS (
        SELECT
          t2.key::INTEGER                         AS        zone,
          -- убрали кавычки в конце и начале
          substring((vals -> t2.key)::TEXT from 2 for char_length((vals -> t2.key)::TEXT)-2) AS   temp,
          (t1.str -> 'time') :: TEXT :: TIMESTAMP AS        time,
          (t1.str -> 'mean_c') :: TEXT :: NUMERIC           mean_c,
          round((t1.str -> 'mean_s') :: TEXT :: NUMERIC, 1) mean_s
        FROM t3, t2, t1
    )
  select * from all_data WHERE zone <> 0
  UNION ALL
  select all_data.zone, v as temp, all_data.time, all_data.mean_c, mean_s from all_data,
        regexp_split_to_table(substring(temp::TEXT from 1 for char_length(temp::TEXT)),',') f(v) WHERE zone = 0;
  --select 1 as zone, 22 as temp, '2018-04-12 08:55:55' as time, 22 as mean_c, 25 as mean_s;
end;
$$;