create or REPLACE function mvt."jf_termo_sensor_pkg$of_rows"(p_id_account numeric,
  OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
  l_tr_id BIGINT = jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
  l_from TIMESTAMP  = jofl.jofl_pkg$extract_date(p_attr, 'period_from', true);
  l_to TIMESTAMP  = jofl.jofl_pkg$extract_date(p_attr, 'period_to', true);
begin
  open p_rows for
  WITH t_data AS(
      select td.val , td.status , sensor_num ,event_time from snsr.termo_data td
        JOIN core.sensor2unit USING(unit_id, sensor_id)
      where td.event_time BETWEEN l_from AND l_to
            AND (tr_id = l_tr_id or l_tr_id isnull )
  )
  select ed.event_time, tr.garage_num, t.unit_id, t.lat, t.lon, t.speed, ed.odometer, ed.is_validity,
    (select status from t_data where t_data.event_time = ed.event_time AND sensor_num = 1600 LIMIT 1) st_1600,
    (select val    from t_data where t_data.event_time = ed.event_time AND sensor_num = 1600 LIMIT 1) val_1600,
    (select status from t_data where t_data.event_time = ed.event_time AND sensor_num = 1601 LIMIT 1) st_1601,
    (select val    from t_data where t_data.event_time = ed.event_time AND sensor_num = 1601 LIMIT 1) val_1601,
    (select status from t_data where t_data.event_time = ed.event_time AND sensor_num = 1602 LIMIT 1) st_1602,
    (select val    from t_data where t_data.event_time = ed.event_time AND sensor_num = 1602 LIMIT 1) val_1602,
    (select status from t_data where t_data.event_time = ed.event_time AND sensor_num = 1603 LIMIT 1) st_1603,
    (select val    from t_data where t_data.event_time = ed.event_time AND sensor_num = 1603 LIMIT 1) val_1603,
    (select status from t_data where t_data.event_time = ed.event_time AND sensor_num = 1604 LIMIT 1) st_1604,
    (select val    from t_data where t_data.event_time = ed.event_time AND sensor_num = 1604 LIMIT 1) val_1604,
    (select status from t_data where t_data.event_time = ed.event_time AND sensor_num = 1605 LIMIT 1) st_1605,
    (select val    from t_data where t_data.event_time = ed.event_time AND sensor_num = 1605 LIMIT 1) val_1605,
    (select status from t_data where t_data.event_time = ed.event_time AND sensor_num = 1606 LIMIT 1) st_1606,
    (select val    from t_data where t_data.event_time = ed.event_time AND sensor_num = 1606 LIMIT 1) val_1606,
    (select status from t_data where t_data.event_time = ed.event_time AND sensor_num = 1607 LIMIT 1) st_1607,
    (select val    from t_data where t_data.event_time = ed.event_time AND sensor_num = 1607 LIMIT 1) val_1607
  ---from /*snsr.termo_data td*/ t_data td
  FROM snsr.extended_data ed --ON ed.packet_id = td.packet_id AND ed.tr_id = td.tr_id
    JOIN core.traffic t ON t.event_time = ed.event_time AND t.tr_id = ed.tr_id
    JOIN core.tr tr ON tr.tr_id = ed.tr_id
  WHERE /*td.event_time BETWEEN now() - INTERVAL '5 minutes 30 seconds' AND now() - INTERVAL '5 minutes'
    AND*/t.event_time BETWEEN l_from  AND l_to
         and (t.tr_id = l_tr_id or l_tr_id isnull )
         AND ed.event_time BETWEEN l_from AND l_to
         AND (ed.tr_id = l_tr_id or l_tr_id isnull ) ;
end;
$$;