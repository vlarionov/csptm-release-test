CREATE OR REPLACE FUNCTION mvt.jf_invalid_thermal_values_pkg$of_rows (
  p_id_account numeric,
  p_attr text,
  out p_rows refcursor
)
RETURNS refcursor AS
$body$
declare
  l_date_begin                 timestamp = jofl.jofl_pkg$extract_date(p_attr, 'f_bgn_DT', true);
  l_date_end                   timestamp = jofl.jofl_pkg$extract_date(p_attr, 'f_end_DT', true);
  l_depo_id                    bigint [] = jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', true);
  l_tr_id                      integer = jofl.jofl_pkg$extract_number(p_attr, 'f_tr_id', true);
  l_max_invalid_status_percent integer = jofl.jofl_pkg$extract_varchar(p_attr, 'F_STATUS_NUMBER', true);
  l_max_invalid_temperature_percent  integer = jofl.jofl_pkg$extract_varchar(p_attr, 'F_TEMPERATURE_NUMBER', true);

begin
  open p_rows for
  with s1 as (select distinct
                tr.depo_id,
                tr.garage_num,
                termo_data.packet_id,
                termo_data.sensor_id,
                termo_data.status,
                tr.tr_id,
                tr.tr_model_id,
                termo_data.event_time,
                ent_depo.name_full,
                termo_data.val,
                count(1)
                over (
                  partition by tr.tr_id, termo_data.sensor_id ) packets_qt,
                (count(case when termo_data.val > dr_max.val or termo_data.val < dr_min.val
                  then 1 end)
                over (
                  partition by termo_data.tr_id, termo_data.sensor_id )) invalid_temperature_qt
              from snsr.termo_data                
                join core.tr on termo_data.tr_id = tr.tr_id
                join core.depo2territory d2t on d2t.territory_id = tr.territory_id
                                          	and (d2t.depo_id = any (l_depo_id) or array_length(l_depo_id, 1) is null)
                join core.entity ent_depo on tr.depo_id = ent_depo.entity_id
                join core.equipment e on termo_data.sensor_id = e.equipment_id
                join core.equipment_model em on e.equipment_type_id = em.equipment_type_id
                --                  TODO добавить функции-константы
                join kdbo.diagnostic_ref dr_min on em.equipment_model_id = dr_min.equipment_model_id
                							   and dr_min.diagnostic_ref_param_id = 21
                join kdbo.diagnostic_ref dr_max on em.equipment_model_id = dr_max.equipment_model_id 
                							   and dr_max.diagnostic_ref_param_id = 22
              where (tr.tr_id = l_tr_id or l_tr_id isnull)
                    and termo_data.event_time between l_date_begin and l_date_end

  ),
      s2 as (
        select distinct
          s1.name_full              depo_name,
          s1.packet_id,
          s1.garage_num,
          sensor2unit.sensor_num                out_num,
          s1.packets_qt,
          count(case when s1.status = 0
            then 1 end)
          over (
            partition by s1.tr_id, s1.sensor_id ) invalid_status_qt,
          s1.invalid_temperature_qt
        from s1
          join core.sensor2unit on s1.sensor_id = sensor2unit.sensor_id
        group by s1.name_full, s1.garage_num, s1.sensor_id, s1.packet_id, invalid_temperature_qt, sensor_num, tr_id, status, packets_qt
    )
  select distinct
    s2.depo_name,
    s2.garage_num,
    s2.out_num,
    s2.packets_qt,
    s2.invalid_status_qt,
    round((s2.invalid_status_qt :: decimal / s2.packets_qt :: decimal * 100 :: decimal), 2) invalid_status_percent,
    s2.invalid_temperature_qt,
    round((s2.invalid_temperature_qt :: decimal / s2.packets_qt :: decimal * 100 :: decimal),
          2)                                                                                invalid_temperature_percent
  from s2
  where round((s2.invalid_status_qt :: decimal / s2.packets_qt :: decimal * 100 ) :: decimal , 2 ) > l_max_invalid_status_percent ::decimal
     or round((s2.invalid_temperature_qt :: decimal / s2.packets_qt ):: decimal * 100 :: decimal  , 2) > l_max_invalid_temperature_percent  ::decimal
  ;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

--------------------------------------------------------------------------------------------

create or replace function mvt."jf_invalid_thermal_values_pkg$of_jrep"(
  p_id_account numeric , p_attr text
)
  returns table(
  depo_name text,
  garage_num text,
  out_num text ,
  packets_qt integer,
  invalid_status_qt integer,
  invalid_status_percent decimal ,
  invalid_temperature_qt integer,
  invalid_temperature_percent decimal
)
language plpgsql as $$
declare
  cr refcursor;
begin
  select mvt.jf_invalid_thermal_values_pkg$of_rows(p_id_account, p_attr) into cr;
  loop
    fetch cr into
    depo_name,
    garage_num,
    out_num,
    packets_qt,
    invalid_status_qt,
    invalid_status_percent,
    invalid_temperature_qt,
    invalid_temperature_percent;
    if not found then return; end if;
    return next;
  end loop;
end;
$$;



