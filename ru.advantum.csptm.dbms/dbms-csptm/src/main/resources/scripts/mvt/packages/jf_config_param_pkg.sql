
create or replace function mvt.jf_config_param_pkg$attr_to_rowtype(p_attr text) returns mvt.config_param
LANGUAGE plpgsql
AS $$
declare
  l_r mvt.config_param%rowtype;
begin
  l_r.value := jofl.jofl_pkg$extract_number(p_attr, 'value', true);
  l_r.config_id := jofl.jofl_pkg$extract_varchar(p_attr, 'config_id', true);
  l_r.description := jofl.jofl_pkg$extract_varchar(p_attr, 'description', true);

  return l_r;
end;
$$;


create or replace function mvt."jf_config_param_pkg$of_delete"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r mvt.config_param%rowtype;
begin
  l_r := mvt.jf_config_param_pkg$attr_to_rowtype(p_attr);

  return null;
end;
$$;


create or replace function mvt."jf_config_param_pkg$of_insert"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r mvt.config_param%rowtype;
begin
  l_r := mvt.jf_config_param_pkg$attr_to_rowtype(p_attr);

  insert into mvt.config_param select l_r.*;

  return null;
end;
$$;

create or replace function mvt."jf_config_param_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
begin
  open p_rows for
  select
    value,
    config_id,
    description
  from mvt.config_param;
end;
$$;

create or REPLACE function mvt."jf_config_param_pkg$of_update"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r mvt.config_param%rowtype;
begin
  l_r := mvt.jf_config_param_pkg$attr_to_rowtype(p_attr);
  IF (l_r.config_id = ANY(ARRAY[1,2]) AND l_r.value > 20 ) THEN
    RAISE EXCEPTION '<<Значение параметра не может быть больше 20! %>>', '';
  END IF;
  update mvt.config_param set value = l_r.value WHERE config_id = l_r.config_id;
  return null;
end;
$$;
