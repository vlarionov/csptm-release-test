create or replace function mvt."jf_termo_changing_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  returns refcursor
LANGUAGE plpgsql
AS $$
declare
  -- id моделей норматива
  l_dt_from TIMESTAMP = jofl.jofl_pkg$extract_date(p_attr, 'BGN_DT', true);
  l_dt_to TIMESTAMP = jofl.jofl_pkg$extract_date(p_attr, 'END_DT', true);
  l_depo_ids BIGINT[] = jofl.jofl_pkg$extract_narray(p_attr, 'ff_depo_id', true);
  l_tr_ids BIGINT[] = jofl.jofl_pkg$extract_narray(p_attr, 'F_tr_id', true);
  l_norm_id BIGINT = jofl.jofl_pkg$extract_number(p_attr, 'F_t_normativ_id', true);
  l_termo_from SMALLINT;
  l_termo_to SMALLINT;
begin
  select t_saloon_from, t_saloon_to INTO l_termo_from, l_termo_to  from mvt.termo_normativ where t_normativ_id = l_norm_id;
  open p_rows for
    select tr_id, time_from dt, round(avg_t_salon::NUMERIC,1) t, tr.garage_num, l_termo_from t_norm_min, l_termo_to t_norm_max
    from mvt.mean_temp term
    JOIN core.tr tr USING(tr_id)
    where time_from BETWEEN l_dt_from AND l_dt_to
    AND (tr.depo_id = ANY(l_depo_ids) OR array_length(l_depo_ids, 1) IS NULL)
    AND (tr.tr_id = ANY(l_tr_ids) OR array_length(l_tr_ids, 1) IS NULL );
end;
$$;