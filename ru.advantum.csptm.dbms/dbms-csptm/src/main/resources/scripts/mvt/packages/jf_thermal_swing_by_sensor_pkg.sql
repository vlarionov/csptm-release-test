CREATE OR REPLACE FUNCTION mvt.jf_thermal_swing_by_sensor_pkg$of_rows (
  p_id_account numeric,
  p_attr text,
  out p_rows refcursor
)
RETURNS refcursor AS
$body$
declare
  l_date_begin        timestamp = jofl.jofl_pkg$extract_date(p_attr, 'f_bgn_DT', true);
  l_date_end          timestamp = jofl.jofl_pkg$extract_date(p_attr, 'f_end_DT', true);
  l_depo_id           bigint [] = jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', true);
  l_tr_id             integer = jofl.jofl_pkg$extract_number(p_attr, 'f_tr_id', true);
  l_max_swing_percent integer = jofl.jofl_pkg$extract_varchar(p_attr, 'F_NUMBER', true);
  --l_ignore_invalid    boolean = jofl.jofl_pkg$extract_boolean(p_attr, 'F_INPLACE_K', true);

begin
  open p_rows for
  with s1 as (select distinct
                tr.garage_num,
                termo_data.sensor_id,
                termo_data.packet_id,
                termo_data.unit_id,
                termo_data.status,
                tr.tr_id,
                tr.tr_model_id,
                termo_data.event_time,
                ent_depo.name_full || '(' || coalesce(ent_depo.name_short, ' ') || ')' as name_full,
                termo_data.val,
                lag(termo_data.val, 1, termo_data.val) over (partition by termo_data.sensor_id order by event_time ) as prev_val,
                count(1) over (partition by tr.tr_id, termo_data.sensor_id) comparison_quantity
              from snsr.termo_data
                join core.tr tr on tr.tr_id = termo_data.tr_id
                join core.depo2territory d2t on d2t.territory_id = tr.territory_id
                join core.entity ent_depo on tr.depo_id = ent_depo.entity_id
                join core.equipment e on termo_data.sensor_id = e.equipment_id
                join core.equipment_model em on e.equipment_type_id = em.equipment_type_id
                join kdbo.diagnostic_ref dr_min on em.equipment_model_id = dr_min.equipment_model_id
                							   and dr_min.diagnostic_ref_param_id = 21
                join kdbo.diagnostic_ref dr_max on em.equipment_model_id = dr_max.equipment_model_id 
                							   and dr_max.diagnostic_ref_param_id = 22
              where (tr.tr_id = l_tr_id  or l_tr_id isnull )
                    and termo_data.event_time between l_date_begin and l_date_end
                    and termo_data.status <> 0
                    and termo_data.val < dr_max.val and termo_data.val > dr_min.val
                         --or not l_ignore_invalid
					and (d2t.depo_id = any (l_depo_id) or array_length(l_depo_id, 1) is null)	 
  	   )
      ,s2 as (select distinct
               s1.name_full,
               s1.garage_num,
               s1.tr_id,
               s1.packet_id,
               s1.status,
               s1.sensor_id,
               s1.comparison_quantity,
               s1.val,
               s1.prev_val,
               @(s1.val - s1.prev_val) as swing,
               (@(s1.val - s1.prev_val) <= mvt.termo_normativ.allowed_sensor_difference) as swing_within_limits,
               mvt.termo_normativ.allowed_sensor_difference
             from s1
               left join mvt.tr_model2tn on s1.tr_model_id = tr_model2tn.tr_model_id
               left join mvt.termo_normativ on ((mvt.tr_model2tn.t_normativ_id = mvt.termo_normativ.t_normativ_id or mvt.termo_normativ.is_default  )
                 					  and s1.event_time between  mvt.termo_normativ.date_from and  mvt.termo_normativ.date_to)
			   left join mvt.months2tn m2tn on m2tn.t_normativ_id = mvt.termo_normativ.t_normativ_id
									  and m2tn.month = date_part('month', now()) 
    where  @(s1.val - s1.prev_val) > mvt.termo_normativ.allowed_sensor_difference )
    ,s21 as (
      select s2.tr_id
      		,s2.status 
      		,round(avg(s2.swing), 2) as avg_swing
      from s2
      group by s2.tr_id
      		  ,s2.status
    )
  	,s3 as (
      select distinct
        s2.name_full ::text,
        s2.garage_num ::text,
        s2.status ::text,
        s2.comparison_quantity,
        sum(count(case when s2.swing_within_limits is false
          then 1 end))
        over (
          partition by s2.tr_id, s2.status )           swings_beyound_limits,
        round(sum(count(case when s2.swing_within_limits is false
          then 1 end))
              over (
                partition by s2.tr_id, s2.status ) :: decimal / s2.comparison_quantity :: decimal * 100 ,
              2) ::text                                       sbl_percent,
        s21.avg_swing ::text,
        s2.allowed_sensor_difference ::text
      from s2
      join s21 on s2.status = s21.status
      		  and s2.tr_id = s21.tr_id
     where s2.swing >  s2.allowed_sensor_difference
      group by
        s2.name_full,
        s2.tr_id,
        s2.garage_num,
        s2.status,
        s2.comparison_quantity,
        s2.swing,
        s21.avg_swing,
        s2.swing_within_limits,
        s2.allowed_sensor_difference
     )
  select s3.name_full,
         s3.garage_num,
         s3.status,
         s3.comparison_quantity,
         s3.swings_beyound_limits,
         s3.sbl_percent,
         s3.avg_swing,
         s3.allowed_sensor_difference
  from s3
  where
    (round(s3.swings_beyound_limits::decimal / s3.comparison_quantity::decimal * 100, 2) >= l_max_swing_percent or
     l_max_swing_percent isnull);
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------------------------------------


create or replace function mvt."jf_thermal_swing_by_sensor_pkg$of_jrep"(
  p_id_account numeric , p_attr text
  )
  returns table(
  name_full  text,
  garage_num  text,
  status  text,
  comparison_quantity text,
  swings_beyound_limits text,
  sbl_percent text,
  avg_swing text,
  allowed_sensor_difference text
)
LANGUAGE plpgsql
as $$
declare
  cr refcursor;
begin
  select mvt."jf_thermal_swing_by_sensor_pkg$of_rows"(p_id_account, p_attr) into cr;
  loop
    fetch cr into
    name_full  ,
    garage_num ,
    status  ,
    comparison_quantity ,
    swings_beyound_limits ,
    sbl_percent ,
    avg_swing ,
    allowed_sensor_difference ;
    if not found then return; end if;
    return next;
  end loop;
end;
$$;

