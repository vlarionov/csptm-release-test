CREATE OR REPLACE FUNCTION mvt.jf_thermal_swing_by_sensors_couple_pkg$of_rows (
  p_id_account numeric,
  p_attr text,
  out p_rows refcursor
)
RETURNS refcursor AS
$body$
declare
  l_date_begin        timestamp = jofl.jofl_pkg$extract_date(p_attr, 'f_bgn_DT', true);
  l_date_end          timestamp = jofl.jofl_pkg$extract_date(p_attr, 'f_end_DT', true);
  l_depo_id           bigint [] = jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', true);
  l_tr_id             integer = jofl.jofl_pkg$extract_number(p_attr, 'f_tr_id', true);
  l_max_swing_percent integer = jofl.jofl_pkg$extract_number(p_attr, 'F_NUMBER', true);
  l_ignore_invalid    boolean = jofl.jofl_pkg$extract_boolean(p_attr, 'F_INPLACE_K', true);

begin
  open p_rows for
  with t_termo as (
     select
        tr.depo_id,
        depo.name_full,
        depo.name_short,
        tr.garage_num,
        td.packet_id,
        td.sensor_id,
        td.status as status_a,
        td2.status as status_b,
        tr.tr_id,
        tr.tr_model_id,
        td.event_time as event_time_a,
        td2.event_time as event_time_b,
        td.val as val_a,
        td2.val as val_b,
        tn.allowed_sensor_difference,
        tn.allowed_sensor_difference_in_packet,
        sum(( @(td.val - td2.val) > tn.allowed_sensor_difference_in_packet)::int) over (partition by /*td.packet_id,*/ tr.tr_id, td.status, td2.status ) as swings_beyound_limits,
        count(1) over (partition by /*td.packet_id,*/ tr.tr_id, td.status, td2.status ) as comparison_quantity,
        --avg(@(td.val - td2.val)) over (partition by /*td.packet_id,*/ tr.tr_id, td.status, td2.status, @(td.val - td2.val) > tn.allowed_sensor_difference_in_packet ) as avg_swing,
        ((greatest(td.val, td2.val) > dr_max.val) or (least(td.val, td2.val) < dr_min.val)) as is_invalid,
        @(td.val - td2.val) > tn.allowed_sensor_difference_in_packet as is_not_allowed
            from core.tr tr                
              join snsr.termo_data td on tr.tr_id = td.tr_id
                                  and (td.event_time between l_date_begin 
                                                         and l_date_end 
                                            )
                                  and td.status <> 0 and td.status <> 1
              join core.depo2territory d2t on d2t.territory_id = tr.territory_id
                                          and (d2t.depo_id = any (l_depo_id) or array_length(l_depo_id, 1) is null)
              join core.entity depo on d2t.depo_id = depo.entity_id
              join core.equipment e on td.sensor_id = e.equipment_id
              join core.equipment_model em on e.equipment_type_id = em.equipment_type_id
              join kdbo.diagnostic_ref dr_min on em.equipment_model_id = dr_min.equipment_model_id
                                                 and dr_min.diagnostic_ref_param_id = 21
              join kdbo.diagnostic_ref dr_max
                  on em.equipment_model_id = dr_max.equipment_model_id and dr_max.diagnostic_ref_param_id = 22
              join snsr.termo_data td2 on (td2.event_time = td.event_time
                                       and td2.packet_id = td.packet_id 
                                       and td2.status > td.status
                                       and td2.tr_id = td.tr_id ) 
              left join mvt.tr_model2tn tm2t on tr.tr_model_id = tm2t.tr_model_id  
              left join mvt.termo_normativ tn on td2.event_time <@ tsrange(tn.date_from, tn.date_to)
                                  and (tm2t.t_normativ_id = tn.t_normativ_id or tn.is_default)
			  left join mvt.months2tn m2tn on m2tn.t_normativ_id = tn.t_normativ_id
    							  and m2tn.month = date_part('month', now()) 
              where tr.tr_id = l_tr_id or l_tr_id isnull
    group by tr.depo_id,
        depo.name_full,
        depo.name_short,
        tr.garage_num,
        td.packet_id,
        td.sensor_id,
        td.status,
        td2.status,
        tr.tr_id,
        tr.tr_model_id,
        td.event_time,
        td2.event_time,
        td.val,
        td2.val,
        tn.allowed_sensor_difference,
        tn.allowed_sensor_difference_in_packet,
        dr_max.val,
        dr_min.val
        )
    ,t2 as (
    select distinct
    	   t.name_full || '(' || coalesce(t.name_short, ' ') || ')' as depo_name,
           t.garage_num,
           t.status_a,
           t.status_b,
           t.comparison_quantity,
           t.swings_beyound_limits,
           --round(t.avg_swing, 2) as avg_swing,
           avg(@(t.val_a - t.val_b)) over (partition by t.tr_id, t.status_a, t.status_b, t.is_invalid, t.is_not_allowed) as avg_swing,
           t.allowed_sensor_difference_in_packet as allowed_swing,
           t.is_not_allowed
    from t_termo t
    where not t.is_invalid
    	  and t.is_not_allowed
     and (round((t.swings_beyound_limits::decimal / t.comparison_quantity::decimal) * 100, 2) >= l_max_swing_percent or
          l_max_swing_percent isnull)
     )
    select
    	   t2.depo_name,
           t2.garage_num,
           t2.status_a,
           t2.status_b,
           t2.comparison_quantity,
           t2.swings_beyound_limits,
           round((t2.swings_beyound_limits::decimal / t2.comparison_quantity::decimal) * 100, 2) as sbl_percent,
           round(t2.avg_swing, 2) as avg_swing,
           t2.allowed_swing
    from t2; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
---------------------------------------------------------------------------------------------

create or replace function mvt.jf_thermal_swing_by_sensors_couple_pkg$of_jrep(
  p_id_account numeric , p_attr text
)
  returns table(
    name_full                 text,
    garage_num                text,
    status_a                  integer,
    status_b                  integer,
    comparison_quantity      text,
    swings_beyound_limits    text,
    sbl_percent              text,
    avg_swing                text,
    allowed_sensor_difference text
  )
LANGUAGE plpgsql
AS $$
declare
  cr refcursor;
begin
  select mvt."jf_thermal_swing_by_sensors_couple_pkg$of_rows"(p_id_account, p_attr) into cr;
  loop
    fetch cr into
    name_full,
    garage_num,
    status_a,
    status_b,
    comparison_quantity,
    swings_beyound_limits,
    sbl_percent,
    avg_swing,
    allowed_sensor_difference;
    if not found then return; end if;
    return next;
  end loop;
end;
$$;


