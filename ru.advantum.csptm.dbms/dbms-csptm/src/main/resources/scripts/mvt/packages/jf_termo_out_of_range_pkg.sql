create or REPLACE function mvt."jf_termo_out_of_range_pkg$of_rows"(p_id_account numeric,
  OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
  l_tr_model_ids BIGINT[] = jofl.jofl_pkg$extract_narray(p_attr, 'f_list_tr_model_id', true);
  l_date_from TIMESTAMP = jofl.jofl_pkg$extract_date(p_attr, 'BGN_DT', true);
  l_date_to TIMESTAMP = jofl.jofl_pkg$extract_date(p_attr, 'END_DT', true);
  l_depo_ids BIGINT[] = jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', true);
  l_territory_ids BIGINT[] = jofl.jofl_pkg$extract_narray(p_attr, 'ff_entity_id', true);
  l_routes_ids BIGINT[] = jofl.jofl_pkg$extract_narray(p_attr, 'ROUTES', true);
  l_utc_offset INTERVAL := ttb.get_utc_offset();
begin
  open p_rows for
  select tr_id, lower(out_of_range_period) period_from, upper(out_of_range_period) period_to, name_short depo, tm.name model, tr.garage_num,-- at.*,
         d.tab_num::TEXT||' '||d.driver_last_name||' '||substring(driver_name from 1 for 1)||'.'||substring(driver_middle_name from 1 for 1)||'.' driver,
         round_code round,  (select number from gis.routes where muid = at.route_id) num,
         to_char(lower(out_of_range_period), 'DD.MM.YY HH24:MI')||'-'||to_char(upper(out_of_range_period), 'HH24:MI') period,
         round(t_salon::NUMERIC,1) mean_t,
         round((t_salon - (tn.t_saloon_from+tn.t_saloon_to)/2 )::NUMERIC,1) deviation,
         tn.t_saloon_from||'-'||tn.t_saloon_to norm,
    '[' || json_build_object(
        'CAPTION', 'Подробнее',
        'JUMP', json_build_object
        ('METHOD', 'mvt.termo_sensor','ATTR',
         json_build_object('time_from', lower(out_of_range_period),'time_to',upper(out_of_range_period))::TEXT ) )|| ']' as detail
  from mvt.agg_termo at
    JOIN core.entity e1 ON e1.entity_id = at.depo_id
    JOIN core.tr_model tm USING(tr_model_id)
    JOIN core.tr tr USING (tr_id)
    JOIN tt.driver d ON d.driver_id = at.driver_id
    JOIN mvt.termo_normativ tn USING(t_normativ_id)
  WHERE (at.tr_model_id = ANY(l_tr_model_ids) OR array_length(l_tr_model_ids,1) IS NULL)
  AND lower(at.out_of_range_period) BETWEEN l_date_from AND l_date_to
  AND (at.depo_id = ANY(l_depo_ids) OR array_length(l_depo_ids,1) IS NULL)
  AND (tr.territory_id = ANY(l_territory_ids) OR array_length(l_territory_ids,1) IS NULL)
  AND (at.route_id = ANY(l_routes_ids) OR array_length(l_routes_ids,1) IS NULL)
  ;
end;
$$;

drop function mvt."jf_termo_out_of_range_pkg$of_jreport";
create or REPLACE function mvt."jf_termo_out_of_range_pkg$of_jreport"(p_id_account numeric, p_attr text)
returns table (
  tr_id bigint,
  period_from date,
  period_to date,
  depo text,
  model text,
  garage_num text,
  driver text,
  round text,
  num text,
  period text,
  meat_t numeric,
  deviation numeric,
  norm text
)
LANGUAGE plpgsql
AS $$
declare
  cr refcursor;
begin
  select mvt."jf_termo_out_of_range_pkg$of_rows"(p_id_account, p_attr) into cr;
  loop
    fetch cr into tr_id,
                  period_from,
                  period_to,
                  depo,
                  model,
                  garage_num,
                  driver,
                  round,
                  num,
                  period,
                  meat_t,
                  deviation,
                  norm;
    if not found then return; end if;
    return next;
  end loop;
end;
$$;
