create or replace function mvt."jf_tr_model2tn_pkg$attr_to_rowtype"(p_attr text) returns mvt.tr_model2tn
LANGUAGE plpgsql
AS $$
declare
  l_r mvt.tr_model2tn%rowtype;
begin
  l_r.tr_model2tn_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_model2tn_id', true);
  l_r.t_normativ_id := jofl.jofl_pkg$extract_number(p_attr, 't_normativ_id', true);
  l_r.tr_model_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_model_id', true);

  return l_r;
end;
$$;

create or replace function mvt."jf_tr_model2tn_pkg$of_delete"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r mvt.tr_model2tn%rowtype;
begin
  l_r := mvt.jf_tr_model2tn_pkg$attr_to_rowtype(p_attr);

  delete from  mvt.tr_model2tn where  tr_model2tn_id = l_r.tr_model2tn_id;

  return null;
end;
$$;

create or replace function mvt."jf_tr_model2tn_pkg$of_insert"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r mvt.tr_model2tn%rowtype;
  l_norm_name TEXT;
begin
  l_r := mvt.jf_tr_model2tn_pkg$attr_to_rowtype(p_attr);

  l_norm_name = mvt.is_unique_month_model(l_r.t_normativ_id, l_r.tr_model_id, null);
  IF l_norm_name IS NOT NULL THEN
      RAISE EXCEPTION '<<Такое сочетание модели и месяца уже есть в нормативе: % >>', l_norm_name;
  END IF;
  l_r.tr_model2tn_id = nextval('mvt.tr_model2tn_tr_model2tn_id_seq'::regclass);
  insert into mvt.tr_model2tn select l_r.*;

  return null;
end;
$$;

create or replace function mvt."jf_tr_model2tn_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
  l_t_normativ_id BIGINT = jofl.jofl_pkg$extract_number(p_attr, 't_normativ_id', true);
begin
  open p_rows for
  select distinct on (tr_model_id)
    tr_model2tn_id,
    t_normativ_id,
    tr_model_id,
    tm.name model_name,
    trt.name tr_type
  from mvt.tr_model2tn t1
  JOIN core.tr_model tm USING(tr_model_id)
  JOIN core.tr tr USING(tr_model_id)
  JOIN core.tr_type trt USING(tr_type_id)
  WHERE t1.t_normativ_id = l_t_normativ_id;
end;
$$;

create or replace function mvt."jf_tr_model2tn_pkg$of_update"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r mvt.tr_model2tn%rowtype;
  l_norm_name TEXT;
begin
  l_r := mvt.jf_tr_model2tn_pkg$attr_to_rowtype(p_attr);

  l_norm_name = mvt.is_unique_month_model(l_r.t_normativ_id, null, l_r.month);
  IF l_norm_name IS NOT NULL THEN
    RAISE EXCEPTION '<<Такое сочетание модели и месяца уже есть в нормативе: % >>', l_norm_name;
  END IF;
  update mvt.tr_model2tn set
    t_normativ_id = l_r.t_normativ_id,
    tr_model_id = l_r.tr_model_id
  where
    tr_model2tn_id = l_r.tr_model2tn_id;

  return null;
end;
$$;

-- возвращает название норматива, в котором уже встречаетс такое сочетание месяца и модели ТС
-- нужно, чтобы пользователь мог понять где, дублирование
create or replace function mvt.is_unique_month_model
  (p_id_normativ BIGINT, p_id_model BIGINT, p_month BIGINT) returns text
LANGUAGE plpgsql
AS $$
declare
  -- месяцы в этом нормативе
  l_months SMALLINT[];
  -- месяцы в этом нормативе
  l_models BIGINT[];

  l_norm_name TEXT;
begin
  --TODO учитывать период действия норматива
  l_months = ARRAY(select month       from mvt.months2tn   where t_normativ_id = p_id_normativ);
  l_models = ARRAY(select tr_model_id from mvt.tr_model2tn where t_normativ_id = p_id_normativ);
  -- нельзя вставить такую модель, если на нее уже есть месяц в ургом нормативе
  IF p_id_model IS NULL THEN
    select norm_name INTO l_norm_name from mvt.months2tn
      LEFT JOIN mvt.tr_model2tn USING(t_normativ_id)
      LEFT JOIN mvt.termo_normativ USING(t_normativ_id)
    WHERE month = p_month
          AND tr_model_id = ANY(l_models)
    LIMIT 1;
    return l_norm_name;
  END IF;

  IF p_month IS NULL THEN
    select norm_name INTO l_norm_name from mvt.months2tn
      LEFT JOIN mvt.tr_model2tn USING(t_normativ_id)
      LEFT JOIN mvt.termo_normativ USING(t_normativ_id)
    WHERE tr_model_id = p_id_model
          AND month = ANY(l_months)
    LIMIT 1;
    return l_norm_name;
  END IF;

  return l_norm_name;
end;
$$;

-- возвращает название норматива по умолчанию, в котором уже есть такие месяцы
create or replace function mvt.is_unique_default_month(p_id_normativ BIGINT) returns text
LANGUAGE plpgsql
AS $$
declare
  -- месяцы в этом нормативе
  l_months SMALLINT[];
  l_norm_name TEXT;
begin
  l_months = array(select month from mvt.termo_normativ tn
  JOIN mvt.months2tn USING(t_normativ_id)
  where t_normativ_id = p_id_normativ);

  select norm_name INTO l_norm_name from mvt.termo_normativ
  JOIN mvt.months2tn USING(t_normativ_id)
  where
  is_default
  AND month = ANY(l_months)
  limit 1;

  return l_norm_name;
end;
$$;

create or replace function mvt."jf_tr_model2tn_pkg$of_all_insert"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  -- модели ТС которые добавляются
  l_models_id BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'models_id', true);
  l_norm_name TEXT;
  l_months SMALLINT[];
  l_norm_id BIGINT = jofl.jofl_pkg$extract_number(p_attr, 't_normativ_id', true);
begin
  l_months = array(select month from mvt.termo_normativ tn
    JOIN mvt.months2tn USING(t_normativ_id)
    where t_normativ_id = l_norm_id);

  select norm_name INTO l_norm_name from mvt.months2tn
    LEFT JOIN mvt.tr_model2tn USING(t_normativ_id)
    LEFT JOIN mvt.termo_normativ USING(t_normativ_id)
  WHERE tr_model_id = ANY(l_models_id)
        AND month = ANY(l_months)
  LIMIT 1;
  IF l_norm_name IS NOT NULL THEN
    RAISE EXCEPTION '<<Такое сочетание модели и месяца уже есть в нормативе: % >>', l_norm_name;
  END IF;

  INSERT INTO mvt.tr_model2tn
    select nextval('mvt.tr_model2tn_tr_model2tn_id_seq'::regclass) tr_model2tn_id,
      l_norm_id t_normativ_id,
      v tr_model_id from unnest(l_models_id) f(v) ;

  return null;
end;
$$;