create or replace function mvt."jf_termal_mode_violation_pkg$of_rows"(p_id_account numeric, p_attr text,
  out                                                                 p_rows       refcursor)
  returns refcursor
language plpgsql
as $$
declare
  l_date_begin    timestamp = jofl.jofl_pkg$extract_date(p_attr, 'f_bgn_DT', true);
  l_date_end      timestamp = jofl.jofl_pkg$extract_date(p_attr, 'f_end_DT', true);
  l_depo_id       bigint [] = jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', true);
  l_grouping_type text = coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_GROUPING_TYPE_INPLACE_K', true), '');

begin
  open p_rows for
  with s1 as (select
                ent_depo.name_full :: text   depo_name,
                l_grouping_type ::text     grouping_type,
                (case when
                  l_grouping_type = 'territory_id'
                  then ent_territory.name_short :: text
                 when l_grouping_type = 'tr_model_id'
                   then tr_model.name :: text
                 when l_grouping_type = 'tr_id'
                   then tr.garage_num :: text
                 when l_grouping_type = 'route_id'
                   then rt.number :: text
                 when l_grouping_type = 'depo_id'
                   then ent_depo.name_full :: text
                 else ''
                 end) :: text               aggr_name,
                (
                  case
                  when l_grouping_type = 'territory_id'
                    then count (*) over (partition by at.depo_id , at.territory_id)
                  when l_grouping_type = 'tr_model_id'
                    then count (*) over (partition by at.depo_id, at.tr_model_id)
                  when l_grouping_type = 'tr_id'
                    then count (*) over (partition by at.depo_id, at.tr_id)
                  when l_grouping_type = 'route_id'
                    then count (*) over (partition by at.depo_id, at.route_id)
                  else count (*) over (partition by at.depo_id)
                end) ::integer counter
              from mvt.agg_termo at
                join core.entity ent_depo on at.depo_id = ent_depo.entity_id
                join core.entity ent_territory on at.territory_id = ent_territory.entity_id
                join core.tr_model on at.tr_model_id = tr_model.tr_model_id
                join gis.routes rt on at.route_id = rt.muid
                join core.tr tr on at.tr_id = tr.tr_id
              where
                (at.depo_id = any (l_depo_id) or l_depo_id isnull or array_length(l_depo_id , 1) is null  )
                and ((at.out_of_range_period && tsrange(l_date_begin, l_date_end)))
  )
  select distinct *
  from s1
  group by depo_name, grouping_type, aggr_name, counter;
end;

$$;


create or replace function mvt.jf_termal_mode_violation_pkg$of_jrep( p_id_account numeric , p_attr text )
  returns  table(
    depo_name text,
    grouping_type text,
    aggr_name text,
    counter integer
  )
language plpgsql as $$
declare
  cr refcursor;
begin
  select mvt.jf_termal_mode_violation_pkg$of_rows(p_id_account, p_attr) into cr;
  loop
    fetch cr into depo_name ,
    grouping_type ,
    aggr_name ,
    counter;
    if not found then return; end if;
    return next;
  end loop;
end;
$$ ;
