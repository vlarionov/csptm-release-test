create or REPLACE function mvt."jf_termo_report_pkg$of_rows"(p_id_account numeric,
  OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
  l_time_from TIMESTAMP  = jofl.jofl_pkg$extract_date(p_attr, 'BGN_DT', true);
  l_time_to   TIMESTAMP  = jofl.jofl_pkg$extract_date(p_attr, 'END_DT', true);
  l_depo_ids  BIGINT[] = jofl.jofl_pkg$extract_narray(p_attr, 'ff_depo_id', true);
  l_tr_ids  BIGINT[] = jofl.jofl_pkg$extract_narray(p_attr, 'f_list_tr_id', true);
  l_routes_ids  BIGINT[] = jofl.jofl_pkg$extract_narray(p_attr, 'ROUTES', true);
  l_stops_ids  BIGINT[] = jofl.jofl_pkg$extract_narray(p_attr, 'F_gis_MUID', true);
  l_drivers_ids  BIGINT[] = jofl.jofl_pkg$extract_narray(p_attr, 'drivers', true);

  l_stop_places_ids BIGINT[];
  l_termo_tr_ids BIGINT[];
begin
  -- ТС с термодатчиками
  l_termo_tr_ids = ARRAY(select distinct tr_id
                         from core.equipment e
                           join core.sensor s on e.equipment_id = s.sensor_id
                           join core.sensor2unit s2u on s.sensor_id = s2u.sensor_id
                           join core.unit2tr u2tr on s2u.unit_id = u2tr.unit_id
                           JOIN core.tr tr USING(tr_id)
                           join core.equipment_model em on e.equipment_model_id = em.equipment_model_id where name = 'ДТ');

  l_stop_places_ids = array(select muid from gis.stop_places WHERE stop_muid = ANY(l_stops_ids));
  open p_rows for

  WITH t2 AS(select DISTINCT tr.tr_id, depo_id, tr_model_id, driver_id, number, round_code
  from tt.order_list o_l
    JOIN tt.round r ON r.timetable_entry_id = o_l.timetable_entry_id
        AND r.route_trajectory_muid /*= 3836334460543515907*/ IN
        (select DISTINCT route_trajectory_muid from gis.stop_place2route_traj
         where /*stop_place_muid*/ /*= 2993277694987068785*/
         stop_place_muid IN (select muid from gis.stop_places where stop_muid = ANY(l_stops_ids) ))
         /*IN (3051183564245445447,3051183564245445449,3051183564245445451,3342860944913542578,3051183564245445450,
               3357468988053856735,3758183886970469961,3799795883766083790)) )*/
         AND r.sign_deleted = 0
    JOIN tt.order_round or_ ON or_.order_list_id = o_l.order_list_id
         AND tr_id IN (select DISTINCT tr_id from snsr.termo_data where event_time BETWEEN l_time_from AND l_time_to/*'2018-04-09 10:00:00' AND '2018-04-09 10:20:00'*/)
    JOIN tt.timetable_entry tte ON tte.timetable_entry_id = o_l.timetable_entry_id
         AND tte.sign_deleted = 0
    JOIN gis.route_variants rv ON rv.muid = tte.route_variant_muid
         AND rv.sign_deleted = 0
    JOIN gis.routes rt ON rt.muid = rv.route_muid
         AND rt.sign_deleted = 0
         AND (rt.muid = ANY(l_routes_ids) OR array_length(l_routes_ids,1) IS NULL)
    JOIN core.tr tr ON tr.tr_id = o_l.tr_id
         AND (tr.depo_id = ANY(l_depo_ids) OR array_length(l_depo_ids,1) IS NULL)
         AND (tr.tr_id = ANY(l_tr_ids) OR array_length(l_tr_ids,1) IS NULL)
  where order_date BETWEEN l_time_from::DATE AND l_time_to::DATE
        AND (driver_id = ANY(l_drivers_ids) OR array_length(l_drivers_ids,1) IS NULL)),
  t1 AS(
  select e.equipment_model_id
  from core.equipment e
  join core.sensor s on e.equipment_id = s.sensor_id
  join core.sensor2unit s2u on s.sensor_id = s2u.sensor_id
  join core.unit2tr u2tr on s2u.unit_id = u2tr.unit_id
  JOIN core.tr tr USING(tr_id)
  join core.equipment_model em on e.equipment_model_id = em.equipment_model_id where name = 'ДТ' limit 1
  ),
  minMaxTemp AS(
  select val, 1 as num from kdbo.diagnostic_ref where diagnostic_ref_param_id IN
  (SELECT diagnostic_ref_param_id from kdbo.diagnostic_ref_param, t1
   WHERE dgnt_ref_param_code = 'P_DT_TEMP_BEG' AND equipment_model_id = t1.equipment_model_id)
   UNION ALL
   select val, 2 as num from kdbo.diagnostic_ref where diagnostic_ref_param_id IN
   (SELECT diagnostic_ref_param_id from kdbo.diagnostic_ref_param, t1
    WHERE dgnt_ref_param_code = 'P_DT_TEMP_END' AND equipment_model_id = t1.equipment_model_id)
  ),
  filtered_temp AS(
  select *, CASE WHEN status > 1 AND status < 9 THEN 1
            WHEN status = 1 THEN 0
            ELSE -1 END as salon
  FROM  snsr.termo_data where
  event_time BETWEEN /*'2018-04-20 12:20:00' AND '2018-04-20 12:55:00'*/l_time_from AND l_time_to
  AND val BETWEEN (select val from minMaxTemp where num = 1)
  AND (select val from minMaxTemp where num = 2)
  AND tr_id IN (select tr_id from t2)
  ),
  mean_termo_on_tr As(
  select distinct on (tr_id) tr_id, salon, avg(val) FILTER (WHERE salon = 1) OVER w as salon_termo,
  avg(val) FILTER (WHERE salon = 0) OVER w as cabin_termo
  from filtered_temp
  window w as (partition by tr_id)
  ),
  t5 As(
  select number, round_code, e.name_short ,garage_num , salon_termo, cabin_termo,
         t2.tr_model_id,
    d.tab_num::TEXT||' '||d.driver_last_name||' '||substring(driver_name from 1 for 1)||'.'||substring(driver_middle_name from 1 for 1)||'.' driver
  from mean_termo_on_tr mt
  JOIN core.tr USING(tr_id)
  JOIN t2 USING(tr_id)
  JOIN core.entity e ON e.entity_id = t2.depo_id
  JOIN tt.driver d ON d.driver_id = t2.driver_id
  ),
  t6 As(
        select t_normativ_id normId, date_from dateFrom, date_to dateTo, check_delay checkDelay,
               t_cab_from tCabFrom, t_cab_to tCabTo,t_saloon_from tSaloonFrom, t_saloon_to tSaloonTo,
               allowed_sensor_difference allowedSensorDifference, allowed_sensor_difference_in_packet allowedSensorDiffInPack,
               comment as comment, norm_name normName, is_default isDefault,
               m.month as month, tr.tr_model_id,
               -- для выбора норматива
               rank() over (PARTITION BY t5.tr_model_id order by  tr.tr_model_id), t5.*
        from mvt.termo_normativ tn
          LEFT JOIN mvt.months2tn m USING(t_normativ_id)
          LEFT JOIN mvt.tr_model2tn tr USING(t_normativ_id)
          JOIN t5 ON (t5.tr_model_id = tr.tr_model_id OR tr.tr_model_id IS NULL)
        where month = 4
   )
  select name_short depo, garage_num, driver driver_name, number, round_code round, round(salon_termo::NUMERIC,1) mean_t_salon,
         tSaloonFrom||'-'||tSaloonTo salon_norm, round(cabin_termo::NUMERIC,1) mean_t_cab, tCabFrom||'-'||tCabTo cab_norm
  from t6 where rank = 1;
end;
$$;

drop function mvt."jf_termo_report_pkg$of_jreport"

create or REPLACE function mvt."jf_termo_report_pkg$of_jreport"(p_id_account numeric,  p_attr text)
returns table(
  depo text,
  garage_num text,
  driver_name text,
  number text,
  round text,
  mean_t_salon numeric,
  salon_norm text,
  mean_t_cab numeric,
  cab_norm text
)
AS $$
declare
  cr refcursor;
begin
  select mvt."jf_termo_report_pkg$of_rows"(p_id_account, p_attr) into cr;
  loop
    fetch cr into depo,
                  garage_num,
                  driver_name,
                  number,
                  round,
                  mean_t_salon,
                  salon_norm,
                  mean_t_cab,
                  cab_norm;
    if not found then return; end if;
    return next;
  end loop;
end;
$$
LANGUAGE plpgsql

select * from mvt."jf_termo_report_pkg$of_jreport"(2, '{"BGN_DT":"2018-05-23 11:00:00.0","END_DT":"2018-05-23 11:38:38.0","ff_depo_id":"$array[]","f_list_tr_id":"$array[]","ROUTES":"$array[]","F_gis_MUID":"$array[]","drivers":"$array[]"}')