create or replace function mvt."jf_months2tn_pkg$attr_to_rowtype"(p_attr text) returns mvt.months2tn
LANGUAGE plpgsql
AS $$
declare
  l_r mvt.months2tn%rowtype;
begin
  l_r.month := jofl.jofl_pkg$extract_varchar(p_attr, 'month', true);
  l_r.months2tn_id := jofl.jofl_pkg$extract_number(p_attr, 'months2tn_id', true);
  l_r.t_normativ_id := jofl.jofl_pkg$extract_number(p_attr, 't_normativ_id', true);

  return l_r;
end;
$$;


create or replace function mvt."jf_months2tn_pkg$of_delete"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r mvt.months2tn%rowtype;
begin
  l_r := mvt.jf_months2tn_pkg$attr_to_rowtype(p_attr);

  delete from  mvt.months2tn where  months2tn_id = l_r.months2tn_id;

  return null;
end;
$$;


create or replace function mvt."jf_months2tn_pkg$of_insert"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r mvt.months2tn%rowtype;
  l_norm_name TEXT;
  l_is_default_norm BOOLEAN;
begin
  l_r := mvt.jf_months2tn_pkg$attr_to_rowtype(p_attr);
  l_norm_name = mvt.is_unique_month_model(l_r.t_normativ_id, null, l_r.month);

  -- добавляются ли месяцы к нормативу по умолчанию
  select is_default INTO l_is_default_norm from mvt.termo_normativ where t_normativ_id = l_r.t_normativ_id;
  -- добавляются ли месяцы к нормативу по умолчанию
  select is_default INTO l_is_default_norm from mvt.termo_normativ where t_normativ_id = l_r.t_normativ_id;
  IF l_is_default_norm THEN
    SELECT norm_name INTO l_norm_name FROM mvt.termo_normativ tn
      JOIN mvt.months2tn m2tn USING(t_normativ_id)
    WHERE is_default
          AND month = l_r.month LIMIT 1;
    -- такой месяц есть в другом нормативе по умолчанию
    IF l_norm_name IS NOT NULL THEN
      RAISE EXCEPTION '<<Такой месяц уже есть в нормативе: % >>', l_norm_name;
    END IF;
  END IF;
  IF l_norm_name IS NOT NULL THEN
    RAISE EXCEPTION '<<Такое сочетание модели и месяца уже есть в нормативе: % >>', l_norm_name;
  END IF;

  l_r.months2tn_id = nextval('mvt.months2tn_months2tn_id_seq'::regclass);
  -- нет дубликатов месяцев для одного норматива
  IF exists(SELECT 1 from mvt.months2tn
         where t_normativ_id = l_r.t_normativ_id
         AND month = l_r.month) THEN
  RETURN null;
  END IF;
  insert into mvt.months2tn select l_r.*;
  return null;
end;
$$;

create or replace function mvt."jf_months2tn_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
  l_t_normativ_id BIGINT = jofl.jofl_pkg$extract_number(p_attr, 't_normativ_id', true);
begin
  open p_rows for
  select
    month,
    months2tn_id,
    t_normativ_id
  from mvt.months2tn m2tn
  WHERE m2tn.t_normativ_id = l_t_normativ_id;
end;
$$;

create or replace function mvt."jf_months2tn_pkg$of_update"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r mvt.months2tn%rowtype;
  l_norm_name TEXT;
  l_is_default_norm BOOLEAN;
begin
  l_r := mvt.jf_months2tn_pkg$attr_to_rowtype(p_attr);

  -- добавляются ли месяцы к нормативу по умолчанию
  select is_default INTO l_is_default_norm from mvt.termo_normativ where t_normativ_id = l_r.t_normativ_id;
  IF l_is_default_norm THEN
      SELECT norm_name INTO l_norm_name FROM mvt.termo_normativ tn
      JOIN mvt.months2tn m2tn USING(t_normativ_id)
      WHERE is_default
      AND month = l_r.month LIMIT 1;
      -- такой месяц есть в другом нормативе по умолчанию
      IF l_norm_name IS NOT NULL THEN
          RAISE EXCEPTION '<<Такой месяц уже есть в нормативе: % >>', l_norm_name;
      END IF;
  END IF;
  l_norm_name = mvt.is_unique_month_model(l_r.t_normativ_id, null, l_r.month);
  IF l_norm_name IS NOT NULL THEN
    RAISE EXCEPTION '<<Такое сочетание модели и месяца уже есть в нормативе: % >>', l_norm_name;
  END IF;
  update mvt.months2tn set
    month = l_r.month,
    t_normativ_id = l_r.t_normativ_id
  where
    months2tn_id = l_r.months2tn_id;

  return null;
end;
$$;
