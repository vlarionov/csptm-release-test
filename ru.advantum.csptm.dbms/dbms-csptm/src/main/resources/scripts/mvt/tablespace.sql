create schema mvt;

create tablespace mvt_data location '/u00/postgres/pg_data/mvt/mvt_data';

create tablespace mvt_idx location  '/u00/postgres/pg_data/mvt/mvt_idx';