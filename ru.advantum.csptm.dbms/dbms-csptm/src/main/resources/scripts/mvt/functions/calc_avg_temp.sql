create or REPLACE function mvt."jf_termo_normativ_pkg$calc_avg_temp"(p_ts_from TIMESTAMP, p_ts_to TIMESTAMP) returns BOOLEAN
LANGUAGE plpgsql
AS $$
declare
  l_time_from TIMESTAMP;
  l_time_to TIMESTAMP;
  -- интервал усреднения 15 минут
  l_avg_interval SMALLINT := 15;
begin
  l_time_from = p_ts_from;
  IF p_ts_from IS NULL THEN
    l_time_from = now()::DATE::TIMESTAMP - INTERVAL '1 day';
  END IF;
  l_time_to = p_ts_to;
  IF p_ts_to IS NULL THEN
    l_time_to = l_time_from + make_interval(hours := 23, mins := 59, secs := 59);
  END IF;

  INSERT INTO mvt.mean_temp(time_from, time_to, depo_id,tr_id, avg_t_salon)
    WITH t1 AS(
        select e.equipment_model_id
        from core.equipment e
          join core.sensor s on e.equipment_id = s.sensor_id
          join core.sensor2unit s2u on s.sensor_id = s2u.sensor_id
          join core.unit2tr u2tr on s2u.unit_id = u2tr.unit_id
          JOIN core.tr tr USING(tr_id)
          join core.equipment_model em on e.equipment_model_id = em.equipment_model_id where name = 'ДТ' limit 1
      ),
      minMaxTemp AS(
        select val, 1 as num from kdbo.diagnostic_ref where diagnostic_ref_param_id IN
               (SELECT diagnostic_ref_param_id from kdbo.diagnostic_ref_param, t1
                WHERE dgnt_ref_param_code = 'P_DT_TEMP_BEG' AND equipment_model_id = t1.equipment_model_id)
        UNION ALL
        select val, 2 as num from kdbo.diagnostic_ref where diagnostic_ref_param_id IN
                (SELECT diagnostic_ref_param_id from kdbo.diagnostic_ref_param, t1
                 WHERE dgnt_ref_param_code = 'P_DT_TEMP_END' AND equipment_model_id = t1.equipment_model_id)
      ),
      filtered_temp AS(
          select *, CASE WHEN status > 1 AND status < 9 THEN 1
                    WHEN status = 1 THEN 0
                    ELSE -1 END as salon
          FROM  snsr.termo_data where
            event_time BETWEEN l_time_from AND l_time_to
            AND val BETWEEN (select val from minMaxTemp where num = 1)
            AND (select val from minMaxTemp where num = 2)
      ),
      --select * from filtered_temp;
      mean_temp AS(
          select distinct on (packet_id, salon) ft.*, avg(val) over (PARTITION BY packet_id, salon) mean_, extract(MONTH FROM event_time),
            tr.tr_model_id
          from filtered_temp  ft
            JOIN core.tr tr USING(tr_id)
          where salon = 1
      ),
      interval_num AS (
          select *, floor(EXTRACT(EPOCH from event_time - l_time_from) / (l_avg_interval*60)) interval_nums from mean_temp
      )
    select distinct on (tr_id, interval_nums)
           (l_time_from + make_interval(mins := (l_avg_interval * interval_nums)::INTEGER      )) time_from,
           (l_time_from + make_interval(mins := (l_avg_interval * (interval_nums + 1))::INTEGER)) time_to,
      tr.depo_id, tr_id, avg(mean_) OVER (PARTITION BY tr_id, interval_nums, salon) avg_t_salon
    from interval_num
    JOIN core.tr tr USING(tr_id);

  RETURN true;
end;
$$;
--select mvt."jf_termo_normativ_pkg$calc_avg_temp"('2018-04-01 00:00:00', '2018-04-02 00:00:00');