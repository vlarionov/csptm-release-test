create or REPLACE function mvt."jf_termo_normativ_pkg$calc_deviation"(p_ts_from TIMESTAMP, p_ts_to TIMESTAMP) returns BOOLEAN
LANGUAGE plpgsql
AS $$
declare
  l_r mvt.termo_normativ%rowtype;
  l_time_from TIMESTAMP;
  l_time_to TIMESTAMP;
begin
  l_time_from = p_ts_from;
  IF p_ts_from IS NULL THEN
    l_time_from = now()::DATE::TIMESTAMP - INTERVAL '1 day';
  END IF;
  l_time_to = p_ts_to;
  IF p_ts_to IS NULL THEN
    l_time_to = l_time_from + make_interval(hours := 23, mins := 59, secs := 59);
  END IF;
  INSERT INTO mvt.agg_termo(out_of_range_period, t_salon, tr_id, depo_id, territory_id, tr_model_id, route_id, driver_id, t_normativ_id, round_code)
    WITH t1 AS(
        select e.equipment_model_id
        from core.equipment e
          join core.sensor s on e.equipment_id = s.sensor_id
          join core.sensor2unit s2u on s.sensor_id = s2u.sensor_id
          join core.unit2tr u2tr on s2u.unit_id = u2tr.unit_id
          JOIN core.tr tr USING(tr_id)
          join core.equipment_model em on e.equipment_model_id = em.equipment_model_id where name = 'ДТ' limit 1
    ),
        minMaxTemp AS(
        select val, 1 as num from kdbo.diagnostic_ref where diagnostic_ref_param_id IN
                                                            (SELECT diagnostic_ref_param_id from kdbo.diagnostic_ref_param, t1
                                                            WHERE dgnt_ref_param_code = 'P_DT_TEMP_BEG' AND equipment_model_id = t1.equipment_model_id)
        UNION ALL
        select val, 2 as num from kdbo.diagnostic_ref where diagnostic_ref_param_id IN
                                                            (SELECT diagnostic_ref_param_id from kdbo.diagnostic_ref_param, t1
                                                            WHERE dgnt_ref_param_code = 'P_DT_TEMP_END' AND equipment_model_id = t1.equipment_model_id)
      ),
        filtered_temp AS(
          select *, CASE WHEN status > 1 AND status < 9 THEN 1
                    WHEN status = 1 THEN 0
                    ELSE -1 END as salon
          FROM  snsr.termo_data where /*event_time::DATE = '2018-04-01'*/
            event_time BETWEEN l_time_from AND l_time_to
            --AND tr_id = 121605
            AND val BETWEEN (select val from minMaxTemp where num = 1)
            AND (select val from minMaxTemp where num = 2)
      ),
      --select * from filtered_temp;
        mean_temp AS(
          select distinct on (packet_id, salon) ft.*, avg(val) over (PARTITION BY packet_id, salon) mean_, extract(MONTH FROM event_time),
            tr.tr_model_id
          from filtered_temp  ft
            JOIN core.tr tr USING(tr_id)
          where salon > -1
      ),
      -- справочник нормативов и температур
        t2 AS (
          select t_normativ_id normId, date_from dateFrom, date_to dateTo, check_delay checkDelay,
                 t_cab_from tCabFrom, t_cab_to tCabTo,t_saloon_from tSaloonFrom, t_saloon_to tSaloonTo,
                 allowed_sensor_difference allowedSensorDifference, allowed_sensor_difference_in_packet allowedSensorDiffInPack,
                 comment as comment, norm_name normName, is_default isDefault,
                 m.month as month, tr_model_id
          from mvt.termo_normativ tn
            LEFT JOIN mvt.months2tn m USING(t_normativ_id)
            LEFT JOIN mvt.tr_model2tn tr USING(t_normativ_id)
          where now() BETWEEN date_from AND date_to
                OR is_default),
        all_norms AS(
          select * from t2 where (isDefault = false AND tr_model_id IS NOT NULL AND month IS NOT NULL)
                                 OR (isDefault = true AND month IS NOT NULL )
      ),
        modelsIdmonth AS(
          select DISTINCT tr_model_id, v as month from core.tr, generate_series(1, 12) f(v) /* месяцы*/
      ),
        norm AS(
        -- норматив для конкретной модели
        select all_norms.*, m.tr_model_id model2  from all_norms
          JOIN modelsIdmonth m USING(tr_model_id,month)
        /*where month = 4
        AND trModelId = 419*/
        UNION ALL
        -- норматив по умолчанию для месяца
        select all_norms.*, m.tr_model_id model2 from all_norms
          JOIN modelsIdmonth m ON m.month = all_norms.month
                                  AND all_norms.tr_model_id IS NULL
        /*where month = 4
        AND trModelId IS NULL*/),
        all_norms2 as(
          select *, rank() over (partition by month, model2 order by tr_model_id) from norm/* where month = 4*/
      ),
        out_of_norm AS(
          select *, case WHEN salon = 1 AND (mean_ < tSaloonFrom OR mean_ > tSaloonTo ) THEN 1
                    WHEN salon = 0 AND (mean_ < tCabFrom OR mean_ > tCabTo ) THEN 1
                    ELSE 0
                    END as outOfRange,
                    rank() over (partition by tr_id, salon order by event_time) row_num /*номер измерения*/
          from all_norms2 an
            join mean_temp mt ON mt.tr_model_id = an.model2
                                 AND an.month = date_part
          where rank = 1/*если есть норматив не по умолчанию, то он*/
        /*AND salon = 0*//*для кабины*/),
      --select * from out_of_norm where salon = 1;
        lead_lag AS(
          select *, lead(outOfRange) OVER w, lag(outOfRange) OVER w
          from out_of_norm
          WINDOW w as (partition by tr_id, salon order by event_time)
      ),
      --select * from lead_lag where salon = 1;
        periods AS(
          select *, CASE WHEN lead = 1 AND (lag = 0 OR lag IS NULL) AND outOfRange = 1 THEN 'start'
                    WHEN (lead = 0 OR lead IS NULL) AND lag = 1  AND outOfRange = 1 THEN 'end'
                    ELSE '-'
                    END as start_stop
          from lead_lag WHERE salon = 1),
      --select * from periods;
        start_stop_only AS (
          SELECT * from periods
          WHERE start_stop IN ('start', 'end')
      ),
      --select * from start_stop_only;
        period_length AS(
          select */*, lead(start_stop) OVER w*/, lead(row_num) OVER w lead_row_num/*, lead(event_time) OVER w lead_event_t*/
          from start_stop_only
          WINDOW w AS (partition by tr_id, salon order by event_time)
      ),
      --select * from period_length;
        data AS(
          select tr_id,normId,checkDelay, start_stop, event_time, --lead_event_t, --lag(start_stop) over w lag_period, lead(start_stop) over w lead_period,
            lead_row_num - row_num + 1 as period, salon,
            CASE WHEN (lead_row_num - row_num + 1) > (select value from mvt.config_param where config_id = 1) AND start_stop = 'start' THEN 'START'
            WHEN ((lead_row_num - row_num + 1) > /*(select value from mvt.config_param where config_id = 2)*/5 OR (lead_row_num - row_num + 1) IS NULL)
                 AND start_stop = 'end' THEN 'END'
            --ELSE 'noise'
            END as not_norm
          from period_length
          window w AS (partition by tr_id, salon order by event_time)
      ),
      --select * from data;
        t55 AS(
          select *, lag(not_norm) OVER (PARTITION BY tr_id ORDER BY event_time)--, lead(not_norm) OVER (PARTITION BY tr_id ORDER BY event_time)
          from data where not_norm IS NOT NULL
      ),
        final_intervals AS(
          select tr_id, normId,checkDelay, start_stop, event_time, not_norm, salon,
            CASE WHEN not_norm = 'START' AND (lag IS NULL OR lag = 'END') THEN true
            WHEN not_norm = 'END' AND (lag IS NULL OR lag = 'START') THEN true
            ELSE false
            END nof_false_edge
          from t55),
      --select * from final_intervals;
        f_data As(
          select *, lead(event_time) over (PARTITION BY tr_id order by event_time)
          from final_intervals where nof_false_edge <> false
      ),
        data4merge AS(select fd.normId,fd.checkDelay, tr_id, salon, /*min, max,*/ tsrange(event_time, lead) bad_range
                      /*(select avg(mean_) from out_of_norm ft where
                        (ft.event_time >= fd.event_time AND ft.event_time < fd.lead) AND ft.tr_id = fd.tr_id and ft.salon = fd.salon) mean_*/
                      from f_data fd where not_norm = 'START'),
      --select * from data4merge;
        rounds AS(
          select depo_id, ol.tr_id ,or_.order_round_id, time_fact, r.round_code, r.round_type, ol.driver_id, tr_model_id, route_muid, tr.territory_id
          from tt.order_round or_
            JOIN tt.order_list ol ON or_.order_list_id = ol.order_list_id
                                     AND ol.sign_deleted = 0
            JOIN tt.order_fact of ON of.order_round_id = or_.order_round_id
            JOIN core.tr tr ON ol.tr_id = tr.tr_id
            JOIN tt.round r ON r.round_id = or_.round_id
                               AND ol.order_date BETWEEN l_time_from AND l_time_to --= '2018-04-01' /*BETWEEN '2018-04-01' AND '2018-04-25'*/
            JOIN tt.timetable_entry tte ON tte.timetable_entry_id = ol.timetable_entry_id
                                           AND tte.sign_deleted = 0
            JOIN gis.route_variants rv ON rv.muid = tte.route_variant_muid
                                          AND rv.sign_deleted = 0
      ),
        rounde_range AS(
          select distinct on (order_round_id) *, tsrange(min(time_fact) over (w), max(time_fact) over (w)) round_range--, round_code
          from rounds
          window w AS (PARTITION BY order_round_id)
      ),
        all_data AS(
          select  rr.tr_id, depo_id, territory_id, tr_model_id, rr.route_muid, driver_id, normId, rr.round_code,
            (tsrange(lower( rr.round_range) + make_interval(mins := checkDelay),upper(rr.round_range)) * d4m.bad_range) bad_periodd,
            (select avg(mean_) from out_of_norm ft where
              (ft.event_time >= lower(tsrange(lower( rr.round_range)+ make_interval(mins := checkDelay),upper(rr.round_range)) * d4m.bad_range)
               AND ft.event_time < upper(tsrange(lower( rr.round_range)+ make_interval(mins := checkDelay),upper(rr.round_range)) * d4m.bad_range))
              AND ft.tr_id = rr.tr_id and ft.salon = d4m.salon) mean_
          from rounde_range rr
            JOIN data4merge d4m ON d4m.tr_id = rr.tr_id
                                   AND lower( rr.round_range)+ make_interval(mins := checkDelay) < upper(rr.round_range)
                                   AND NOT isempty(tsrange(lower( rr.round_range)+ make_interval(mins := checkDelay),upper(rr.round_range)) * d4m.bad_range)
      )
    select bad_periodd, mean_, tr_id, depo_id, territory_id, tr_model_id, route_muid, driver_id, normId, round_code  from all_data where mean_ IS NOT NULL;


  RETURN true;
end;
$$;