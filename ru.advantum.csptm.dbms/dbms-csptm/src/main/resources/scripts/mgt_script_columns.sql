--select count(1) from (
  SELECT
    n.nspname                                       AS schemaname,
    t.relname                                       AS tablename,
    pg_get_userbyid(t.relowner)                     AS tableowner,
    obj_description(t.OID)                          AS tablecomment,
    a.attname,
    pg_catalog.format_type(a.atttypid, a.atttypmod) AS Datatype,
--    a.atttypid,
    a.attnotnull,
    a.attnum,
    d.description
--     a.attlen,
--     a.attrelid,
  FROM pg_class t
    JOIN pg_attribute a ON a.attrelid = t.oid
    LEFT JOIN pg_namespace n ON n.oid = t.relnamespace
    LEFT JOIN pg_description AS d ON (d.objoid = t.oid AND d.objsubid = a.attnum)
  WHERE (t.relkind = 'r' :: "char")
    and a.attisdropped=false and a.attnum > 0 and a.atttypid > 0 and t.relname not in ('tmp', 'test') and t.relname not like 'dbg%'
        AND d.description IS NULL
        AND NOT exists(SELECT 1
                       FROM pg_inherits
                       WHERE (inhrelid = t.oid))
        AND n.nspname IN ('adm',
    'aissc',
    'asd',
    'askp',
    'asupbk',
    'cmnd',
    'core',
    'crep',
    'cron',
    'easu',
    'evt',
    'gis',
    'hist',
    'ibrd',
    'jofl',
    'jrep',
    'kdbo',
    'mnt',
    'oud',
    'paa',
    'rnis',
                          'rts',
                          'snsr',
                          'tt',
                          'ttb',
                          'udump',
                          'usw',
                          'voip',
                          'wh',
                          'yandex'
  )
  ORDER BY n.nspname, t.relname
--) t2
;

comment on column hist.ttb_norm.tr_type_id is 	'Логирование изменения атрибутов нормативов';


/*autocomments*/
  SELECT 'comment on column '||
    n.nspname||'.'||t.relname||'.'||a.attname
    ||' is '''||
     case
      when a.attnum = 1 and  position('id' in a.attname) > 0 then obj_description(t.OID) ||' - код'
      when a.attname = 'rowid' then 'код записи (служебное поле)'
      else a.attname
     end
    ||''';'
    column_comment
  FROM pg_class t
    JOIN pg_attribute a ON a.attrelid = t.oid
    LEFT JOIN pg_namespace n ON n.oid = t.relnamespace
    LEFT JOIN pg_description AS d ON (d.objoid = t.oid AND d.objsubid = a.attnum)
  WHERE (t.relkind = 'r' :: "char")
    and a.attnum > 0 and a.atttypid > 0 and t.relname not in ('tmp', 'test') and t.relname not like 'dbg%'
        AND d.description IS NULL
        AND NOT exists(SELECT 1
                       FROM pg_inherits
                       WHERE (inhrelid = t.oid))
        AND n.nspname IN ('adm',
    'aissc',
    'asd',
    'askp',
    'asupbk',
    'cmnd',
    'core',
    'crep',
    'cron',
    'easu',
    'evt',
    'gis',
--     'hist',
    'ibrd',
    'jofl',
    'jrep',
    'kdbo',
    'mnt',
    'oud',
    'paa',
    'rnis',
                          'rts',
                          'snsr',
                          'tt',
                          'ttb',
                          'udump',
                          'usw',
                          'voip',
                          'wh',
                          'yandex'
  )
  ORDER BY n.nspname, t.relname
;


comment on column cron.job.schedule is 'schedule';
comment on column cron.job.username is 'username';
comment on column cron.job.nodename is 'nodename';
comment on column cron.job.database is 'database';
comment on column cron.job.command is 'command';
comment on column cron.job.jobid is 'jobid';
comment on column cron.job.nodeport is 'nodeport';
comment on column jofl.databasechangelog.filename is 'filename';
comment on column jofl.databasechangelog.deployment_id is 'deployment_id';
comment on column jofl.databasechangelog.liquibase is 'liquibase';
comment on column jofl.databasechangelog.comments is 'comments';
comment on column jofl.databasechangelog.description is 'description';
comment on column jofl.databasechangelog.author is 'author';
comment on column jofl.databasechangelog.id is 'Поддержна верий фреймворка Liquibase  - код';
comment on column jofl.databasechangelog.labels is 'labels';
comment on column jofl.databasechangelog.exectype is 'exectype';
comment on column jofl.databasechangelog.md5sum is 'md5sum';
comment on column jofl.databasechangelog.tag is 'tag';
comment on column jofl.databasechangelog.dateexecuted is 'dateexecuted';
comment on column jofl.databasechangelog.orderexecuted is 'orderexecuted';
comment on column jofl.databasechangelog.contexts is 'contexts';
comment on column jofl.databasechangeloglock.locked is 'locked';
comment on column jofl.databasechangeloglock.lockedby is 'lockedby';
comment on column jofl.databasechangeloglock.lockgranted is 'lockgranted';
comment on column jofl.databasechangeloglock.id is 'Поддержна верий фреймворка Liquibase  - код';
comment on column jofl.win_filter.id_filter is 'id_filter';
comment on column jofl.window.custom_topcomponent is 'custom_topcomponent';
comment on column jofl.window.win_title is 'Заголовок';

comment on column usw.rlc."Channel" is 'Channel';
comment on column usw.rlc."UniqueID" is 'UniqueID';
comment on column usw.rlc."StationNum" is 'StationNum';
comment on column usw.rlc."StationType" is 'StationType';
comment on column usw.rlc."Param" is 'Param';
