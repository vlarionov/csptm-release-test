﻿-- view: mnt.v_round_oper_visit

-- drop view mnt.v_round_oper_visit

create or replace view mnt.v_round_oper_visit as
select orl.order_date, orl.tr_id, orl.driver_id, orr.order_round_id, orv.time_fact_end, orv.order_round_status_id, orv.time_plan_end
  from tt.order_list orl
  join tt.order_round orr on orr.order_list_id = orl.order_list_id
  join asd.oper_round_visit orv on orv.order_round_id = orr.order_round_id
 where orl.sign_deleted = 0
   and orl.is_active = 1
   and orr.is_active = 1;

alter table mnt.v_round_oper_visit owner to adv;

comment on view mnt.v_round_oper_visit is 'Плановое и фактическое время посещения рейсов';
comment on column mnt.v_round_oper_visit.order_date is 'Дата наряда';
comment on column mnt.v_round_oper_visit.tr_id is 'ТС';
comment on column mnt.v_round_oper_visit.driver_id is 'Водитель';
comment on column mnt.v_round_oper_visit.order_round_id is 'рейсы наряда';
comment on column mnt.v_round_oper_visit.time_fact_end is 'Время последнего привязанного пакета к рейсу';
comment on column mnt.v_round_oper_visit.order_round_status_id is 'статус рейса';
comment on column mnt.v_round_oper_visit.time_plan_end is 'Время окончания рейса по плану';

-- select * from mnt.v_round_oper_visit limit 100
