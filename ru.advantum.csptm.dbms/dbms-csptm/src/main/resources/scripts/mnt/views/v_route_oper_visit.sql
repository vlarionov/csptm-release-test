﻿-- view: mnt.v_route_oper_visit

-- drop view mnt.v_route_oper_visit

create or replace view mnt.v_route_oper_visit as
select orl.order_date, orl.tr_id, orr.order_round_id, orl.driver_id, rov.route_muid, rnd.round_id,
       orv.time_plan_begin, orv.time_plan_end, orv.time_fact_begin, orv.time_fact_end, rtj.length,
       orv.order_round_status_id, rtj.trajectory_type_muid, rnd.round_code, ror.route_round_type_muid
  from tt.order_list orl
  join tt.order_round orr on orr.order_list_id = orl.order_list_id
  join tt.round rnd on rnd.round_id = orr.round_id
  join gis.route_trajectories rtj on rtj.muid = rnd.route_trajectory_muid
  join gis.route_rounds ror on ror.muid = rtj.route_round_muid
  join gis.route_variants rov on rov.muid = ror.route_variant_muid
  join asd.oper_round_visit orv on orv.order_round_id = orr.order_round_id
 where orl.sign_deleted = 0
   and orl.is_active = 1
   and orr.is_active = 1;

alter table mnt.v_route_oper_visit owner to adv;

comment on view mnt.v_route_oper_visit is 'Плановое и фактическое время посещения рейсов и маршрут';
comment on column mnt.v_route_oper_visit.order_date is 'Дата наряда';
comment on column mnt.v_route_oper_visit.tr_id is 'ТС';
comment on column mnt.v_route_oper_visit.order_round_id is 'рейсы наряда';
comment on column mnt.v_route_oper_visit.driver_id is 'Водитель';
comment on column mnt.v_route_oper_visit.route_muid is 'Маршрут';
comment on column mnt.v_route_oper_visit.round_id is 'рейс';
comment on column mnt.v_route_oper_visit.time_plan_begin is 'Время начала рейса по плану';
comment on column mnt.v_route_oper_visit.time_plan_end is 'Время окончания рейса по плану';
comment on column mnt.v_route_oper_visit.time_fact_begin is 'Время первого привязанного пакета к рейсу';
comment on column mnt.v_route_oper_visit.time_fact_end is 'Время последнего привязанного пакета к рейсу';
comment on column mnt.v_route_oper_visit.length is 'Длина траектории';
comment on column mnt.v_route_oper_visit.order_round_status_id is 'статус рейса';
comment on column mnt.v_route_oper_visit.trajectory_type_muid is 'Тип траектории';
comment on column mnt.v_route_oper_visit.round_code is 'код рейса';
comment on column mnt.v_route_oper_visit.route_round_type_muid is 'Тип производственного рейса';

--select * from mnt.v_route_oper_visit limit 100
