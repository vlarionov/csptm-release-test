--drop view mnt.v_tt_action_round_attrib;

create or replace view mnt.v_tt_action_round_attrib as
        select
            rv.route_id,
            rv.route_variant_id,
            tv.tt_variant_id,
            tv.order_date,
            tout.tt_out_id,
            tout.tr_id,
            tout.tt_out_num,
            ta.tt_action_id,
            ta.driver_id,
            r.round_id,
            r.action_type_id,
            r.round_num,
            tar.round_status_id,
            ta.action_range,
            case when tar.time_fact_begin is not null
                then
                    tsrange(tar.time_fact_begin, tar.time_fact_end)
            else 'empty' :: tsrange
            end action_fact_range,
            r.code,
            r.length,
            si2r.stop_item_id stop_item_a_id,
            rts.round_pkg$get_last_stop_item_id(tar.round_id) stop_item_b_id,
            r.move_direction_id,
            md.move_direction_name
        from ttb.tt_variant tv
            join rts.route_variant rv on tv.route_variant_id = rv.route_variant_id
            join ttb.tt_out tout on tv.tt_variant_id = tout.tt_variant_id
            join ttb.tt_action ta on tout.tt_out_id = ta.tt_out_id
            join ttb.tt_action_round tar on ta.tt_action_id = tar.tt_action_id
            join rts.round r on tar.round_id = r.round_id
            join rts.move_direction md on r.move_direction_id = md.move_direction_id
            join rts.stop_item2round si2r on si2r.round_id = tar.round_id and si2r.order_num = 1
        where not tv.sign_deleted
              and not tout.sign_deleted
              and not ta.sign_deleted
              and not si2r.sign_deleted
              and tv.parent_tt_variant_id is not null
              and tout.tr_id is not null;

comment on view mnt.v_tt_action_round_attrib is 'Атрибуты рейса';
comment on column mnt.v_tt_action_round_attrib.route_id is 'Маршрут';
comment on column mnt.v_tt_action_round_attrib.route_variant_id is 'Вариант маршрута';
comment on column mnt.v_tt_action_round_attrib.tt_variant_id is 'Вариант расписания';
comment on column mnt.v_tt_action_round_attrib.order_date is 'Дата варианта расписания';
comment on column mnt.v_tt_action_round_attrib.tt_out_id is 'Выход';
comment on column mnt.v_tt_action_round_attrib.tr_id is 'ТС';
comment on column mnt.v_tt_action_round_attrib.tt_out_num is 'Номер выхода';
comment on column mnt.v_tt_action_round_attrib.tt_action_id is 'Действие';
comment on column mnt.v_tt_action_round_attrib.driver_id is 'Водитель';
comment on column mnt.v_tt_action_round_attrib.round_id is 'Рейс';
comment on column mnt.v_tt_action_round_attrib.action_type_id is 'Тип рейса';
comment on column mnt.v_tt_action_round_attrib.round_num is 'Номер рейса';
comment on column mnt.v_tt_action_round_attrib.round_status_id is 'Статус прохождения рйеса';
comment on column mnt.v_tt_action_round_attrib.action_range is 'Плановое время рейса';
comment on column mnt.v_tt_action_round_attrib.action_fact_range is 'Фактическое время прохождения рейса';
comment on column mnt.v_tt_action_round_attrib.code is 'Код рейса';
comment on column mnt.v_tt_action_round_attrib.length is 'Длина рейса (м)';
comment on column mnt.v_tt_action_round_attrib.stop_item_a_id is 'Начальная остановка рейса';
comment on column mnt.v_tt_action_round_attrib.stop_item_b_id is 'Конечная остановка рейса';
comment on column mnt.v_tt_action_round_attrib.move_direction_id is 'Направление движения';
comment on column mnt.v_tt_action_round_attrib.move_direction_name is 'Наименование направления движения';
