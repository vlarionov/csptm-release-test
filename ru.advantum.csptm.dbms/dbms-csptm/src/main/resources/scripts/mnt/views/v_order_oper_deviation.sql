--drop view mnt.v_order_oper_deviation;

create or replace view mnt.v_order_oper_deviation as
  with last_oper as (
      select
        order_oper.order_round_id,
        max(time_oper) last_time,
        ol.tr_id
      from
        tt.order_list ol
        join tt.order_round orr
          on ol.order_list_id = orr.order_list_id
        join tt.order_oper
          on  order_oper.order_round_id = orr.order_round_id
      where order_date = core.get_date_current_msk()
            and time_oper >= now() - interval '1 hours'
            and time_oper <= now() /*- interval '3 hours'*/
            and order_oper.is_active = 1
            and order_oper.sign_deleted = 0
            and orr.is_active = 1
            and ol.sign_deleted = 0
            and ol.is_active = 1
      group by order_oper.order_round_id, ol.tr_id
  ),
      plan as (select order_oper.time_oper time_oper,
                 order_oper.order_round_id,
                 order_oper.stop_place_muid,
                 order_oper.stop_order_num,
                 last_oper.tr_id
               from tt.order_oper
                 join last_oper on order_oper.order_round_id = last_oper.order_round_id
               where order_oper.time_oper = last_oper.last_time
                     and order_oper.is_active = 1
                     and order_oper.sign_deleted = 0

    ),
    max_by_tr as ( select tr_id, max(plan.time_oper) time_oper
                   from plan
                     left join tt.order_fact on (plan.order_round_id = order_fact.order_round_id
                                                 and plan.stop_place_muid = order_fact.stop_place_muid
                                                 and plan.stop_order_num = order_fact.stop_order_num )
                   group by tr_id


    )
  select plan.tr_id, plan.time_oper, order_fact.time_fact, plan.time_oper - order_fact.time_fact deviation, stops.name as stops_name
  from plan
    left join tt.order_fact on (plan.order_round_id = order_fact.order_round_id
                                and plan.stop_place_muid = order_fact.stop_place_muid
                                and plan.stop_order_num = order_fact.stop_order_num)
    join max_by_tr on (plan.tr_id = max_by_tr.tr_id and plan.time_oper = max_by_tr.time_oper)
    join gis.stop_places on stop_places.muid  = plan.stop_place_muid
    join gis.stops on stops.muid = stop_places.stop_muid

  where order_fact.sign_deleted = 0
;


comment on view mnt.v_order_oper_deviation is 'Атрибуты наряда АПИ АСДУ';
comment on column mnt.v_order_oper_deviation.tr_id is 'Идентификатор ТС';
comment on column mnt.v_order_oper_deviation.time_oper is 'Время прибытия оперативное';
comment on column mnt.v_order_oper_deviation.time_fact is 'Время прибытия фактическое';
comment on column mnt.v_order_oper_deviation.deviation is 'Разница между оперативным и фактическим временем';
comment on column mnt.v_order_oper_deviation.stops_name is 'Наименование остановки';