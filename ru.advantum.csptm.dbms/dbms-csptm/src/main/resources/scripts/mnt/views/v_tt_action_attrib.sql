--drop function mnt.jf_mnemonic_stop_times_pkg$get_tr_rounds(bigint, int[]);

--drop view mnt.v_tt_action_attrib;

create or replace view mnt.v_tt_action_attrib as
select
  route_variant.route_id,
  route_variant.route_variant_id,
  tt_variant.tt_variant_id,
  tt_variant.order_date,
  tt_out.tt_out_id,
  tt_out.tr_id,
  tt_out.tt_out_num,
  tt_action.tt_action_id,
  tt_action.driver_id,
  tt_action.action_type_id,
  tt_action.action_range,
  round.round_id,
  round.round_num,
  round.code,
  round.length,
  round.move_direction_id,
  tt_action_round.round_status_id,
  si2r.stop_item_id stop_item_a_id,
  /*rts.round_pkg$get_last_stop_item_id(tt_action_round.round_id)*/ null::int stop_item_b_id,
  move_direction.move_direction_name,
  case when tt_action_round.time_fact_begin is not null
  then
    tsrange(tt_action_round.time_fact_begin, tt_action_round.time_fact_end)
  else
    'empty' :: tsrange
  end action_fact_range,
  tt_action_round.time_fact_begin,
  ttai.stop_item_id as pause_stop_item_id
from ttb.tt_variant
  join rts.route_variant on tt_variant.route_variant_id = route_variant.route_variant_id
  join ttb.tt_out on tt_variant.tt_variant_id = tt_out.tt_variant_id
  join ttb.tt_action on tt_out.tt_out_id = tt_action.tt_out_id
  left join ttb.tt_action_round tt_action_round on tt_action.tt_action_id = tt_action_round.tt_action_id
  left join rts.round on tt_action_round.round_id = round.round_id
  left join rts.move_direction on round.move_direction_id = move_direction.move_direction_id
  left join rts.stop_item2round si2r on si2r.round_id = tt_action_round.round_id and si2r.order_num = 1
  left join lateral
            (SELECT stop_item2round.stop_item_id
             FROM ttb.tt_action_item
               JOIN
               rts.stop_item2round ON tt_action_item.stop_item2round_id = stop_item2round.stop_item2round_id
             WHERE tt_action.tt_action_id = tt_action_item.tt_action_id
                  AND tt_action_item.action_type_id in (ttb.action_type_pkg$dinner()
                                                        ,ttb.action_type_pkg$bn_round_break()
                                                        ,ttb.action_type_pkg$bn_round_break_base()
                                                        )
                  and not tt_action_item.sign_deleted
             limit 1
            ) ttai on true
where not tt_variant.sign_deleted
      and not tt_out.sign_deleted
      and not tt_action.sign_deleted
      and tt_variant.parent_tt_variant_id is not null
      and tt_out.tr_id is not null;

comment on view mnt.v_tt_action_round_attrib is 'Атрибуты рейса';
comment on column mnt.v_tt_action_round_attrib.route_id is 'Маршрут';
comment on column mnt.v_tt_action_round_attrib.route_variant_id is 'Вариант маршрута';
comment on column mnt.v_tt_action_round_attrib.tt_variant_id is 'Вариант расписания';
comment on column mnt.v_tt_action_round_attrib.order_date is 'Дата варианта расписания';
comment on column mnt.v_tt_action_round_attrib.tt_out_id is 'Выход';
comment on column mnt.v_tt_action_round_attrib.tr_id is 'ТС';
comment on column mnt.v_tt_action_round_attrib.tt_out_num is 'Номер выхода';
comment on column mnt.v_tt_action_round_attrib.tt_action_id is 'Действие';
comment on column mnt.v_tt_action_round_attrib.driver_id is 'Водитель';
comment on column mnt.v_tt_action_round_attrib.round_id is 'Рейс';
comment on column mnt.v_tt_action_round_attrib.action_type_id is 'Тип рейса';
comment on column mnt.v_tt_action_round_attrib.round_num is 'Номер рейса';
comment on column mnt.v_tt_action_round_attrib.round_status_id is 'Статус прохождения рйеса';
comment on column mnt.v_tt_action_round_attrib.action_range is 'Плановое время рейса';
comment on column mnt.v_tt_action_round_attrib.action_fact_range is 'Фактическое время прохождения рейса';
comment on column mnt.v_tt_action_round_attrib.code is 'Код рейса';
comment on column mnt.v_tt_action_round_attrib.length is 'Длина рейса (м)';
comment on column mnt.v_tt_action_round_attrib.stop_item_a_id is 'Начальная остановка рейса';
comment on column mnt.v_tt_action_round_attrib.stop_item_b_id is 'Конечная остановка рейса';
comment on column mnt.v_tt_action_round_attrib.move_direction_id is 'Направление движения';
comment on column mnt.v_tt_action_round_attrib.move_direction_name is 'Наименование направления движения';
