--drop view mnt.v_rts_routes;

create or replace view mnt.v_rts_routes
  as
select r.route_id,
       r.current_route_variant_id,
       r.route_num,
       dn.name_full,
       d.depo_id
from rts.route r
     join rts.depo2route d2r on d2r.route_id = r.route_id
     join core.depo d on d.depo_id = d2r.depo_id
     join core.entity dn on dn.entity_id = d.depo_id
where     not r.sign_deleted
      and not d2r.sign_deleted
      and d.sign_deleted = 0;

comment on view mnt.v_rts_routes is 'Атрибуты маршрутов (по схеме rts)';
comment on column mnt.v_rts_routes.route_id is 'Идентификатор маршрута';
comment on column mnt.v_rts_routes.current_route_variant_id is 'Идентификатор текущего варианта маршрута';
comment on column mnt.v_rts_routes.route_num is 'Номер маршрута';
comment on column mnt.v_rts_routes.name_full is 'Наименование парка';
comment on column mnt.v_rts_routes.depo_id is 'Идентификатор парка';