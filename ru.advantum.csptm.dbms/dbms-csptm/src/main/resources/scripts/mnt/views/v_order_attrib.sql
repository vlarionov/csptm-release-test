--drop view mnt.v_order_attrib;

create or replace view mnt.v_order_attrib
as
  select
    ol.order_list_id
    ,t.licence
    ,t.garage_num
    ,d.driver_name||' '||d.driver_middle_name||' '||d.driver_last_name fio
  from tt.order_list ol
    join core.tr t on t.tr_id = ol.tr_id
    join tt.driver d on ol.driver_id = d.driver_id;


comment on view mnt.v_order_attrib is 'Атрибуты наряда АПИ АСДУ';
comment on column mnt.v_order_attrib.order_list_id is 'Идентификатор наряда';
comment on column mnt.v_order_attrib.licence is 'Гос. номер';
comment on column mnt.v_order_attrib.garage_num is 'Гаражный номер';
comment on column mnt.v_order_attrib.fio is 'ФИО водителя';