--drop view mnt.v_ref_routes;

create or replace view mnt.v_ref_routes
  as
  select
     routes.muid as routes_muid
    ,routes.current_route_variant_muid
    ,parks.muid as parks_muid
    ,entity.name_full as parks_name
    ,routes.number as routes_number
    ,depo.depo_id as depo_depo_id
  from gis.routes
    join gis.parks2routes on routes.muid = parks2routes.route_muid
    join gis.parks on parks.muid = parks2routes.park_muid
    join core.depo on depo.park_muid = parks.muid
    join core.entity on entity.entity_id = depo.depo_id
  where parks2routes.sign_deleted = 0;


comment on view mnt.v_ref_routes is 'Атрибуты маршрутов';
comment on column mnt.v_ref_routes.routes_muid is 'Идентификатор маршрута';
comment on column mnt.v_ref_routes.current_route_variant_muid is 'Идентификатор ';
comment on column mnt.v_ref_routes.parks_muid is 'Идентификатор ';
comment on column mnt.v_ref_routes.parks_name is '';
comment on column mnt.v_ref_routes.routes_number is '';
comment on column mnt.v_ref_routes.depo_depo_id is 'Идентификатор ';