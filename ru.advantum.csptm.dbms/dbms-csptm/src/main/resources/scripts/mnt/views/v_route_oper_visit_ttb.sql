--drop view mnt.v_route_oper_visit_ttb;

create or replace view mnt.v_route_oper_visit_ttb as
select
  route_variant.route_id as route_id
  ,tt_variant.order_date as order_date
  ,tt_out.tr_id as tr_id
  ,tt_out.tt_out_num
  ,tt_action.driver_id as driver_id
  ,tt_action.dt_action as time_plan_begin
  ,tt_action.dt_action + tt_action.action_dur*interval '1 second' as time_plan_end
  ,tt_action_round.tt_action_id as tt_action_id
  ,tt_action_round.round_status_id as round_status_id
  ,dr_shift.dr_shift_num
  ,round.length as length
  ,round.code as round_code
  ,round.move_direction_id
  ,round.round_id as round_id
  ,round_status.round_status_name
  ,tt_action_fact.time_fact_begin as tt_action_fact
  ,tt_action_fact.time_fact_end as tt_action_end
from ttb.tt_variant
join rts.route_variant on route_variant.route_variant_id = tt_variant.route_variant_id
join ttb.tt_out on tt_variant.tt_variant_id = tt_out.tt_variant_id
join ttb.tt_action on tt_out.tt_out_id = tt_action.tt_out_id
join ttb.tt_action_round on tt_action.tt_action_id = tt_action_round.tt_action_id
join ttb.tt_action_item on tt_action_item.tt_action_id = tt_action.tt_action_id
join ttb.dr_shift on dr_shift.dr_shift_id = tt_action_item.dr_shift_id
left join rts.round on round.round_id = tt_action_round.round_id
left join ttb.round_status on round_status.round_status_id = tt_action_round.round_status_id
left join ttb.tt_action_fact on tt_action.tt_action_id = tt_action_fact.tt_action_id
where  not tt_variant.sign_deleted
  and not tt_out.sign_deleted
  and not tt_action.sign_deleted
group by
  tt_variant.order_date
  ,tt_out.tr_id
  ,tt_action_round.tt_action_id
  ,tt_action.driver_id
  ,route_variant.route_id
  ,round.round_id
  ,tt_action.dt_action
  ,tt_action_fact.time_fact_begin
  ,tt_action_fact.time_fact_end
  ,rts.round.length
  ,tt_action_round.round_status_id
  ,tt_action.action_dur
  ,dr_shift.dr_shift_num
  ,round.code
  ,tt_out_num
  ,round.move_direction_id
  ,round_status.round_status_name;