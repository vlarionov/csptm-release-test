--drop view mnt.v_rounds_attrib;

create or replace view mnt.v_rounds_attrib
as
select
  -- Не надо использовать в новых запросах, используйте mnt.v_tt_action_round_attrib
  orr.order_round_id
  ,ol.order_date
  ,rr.route_variant_muid
  ,rr.muid rounds_muid
  ,ol.tr_id
  ,ol.driver_id
  ,stop_place_a_muid
  ,stop_place_b_muid
  ,r.direction_id
  ,tt.timetable_entry_num
  ,rr.code round_code
  ,r.round_id
  ,ol.order_list_id
  ,rtj.muid route_trajectories_muid
  ,rtjt.name as direction_name
  ,rtj.length route_trajectories_length
  ,route_variants.route_muid
from tt.order_list ol
  join tt.order_round orr on ol.order_list_id = orr.order_list_id and ol.sign_deleted = 0 and ol.is_active = 1 and orr.is_active = 1
  join tt.round r on r.round_id = orr.round_id
  join gis.route_trajectories rtj on rtj.muid = r.route_trajectory_muid
  join gis.route_rounds rr on rr.muid = rtj.route_round_muid
  join tt.timetable_entry tt on tt.timetable_entry_id = ol.timetable_entry_id and tt.sign_deleted = 0
  join gis.ref_route_trajectory_types rtjt on rtjt.muid = rtj.trajectory_type_muid
  join gis.route_variants on rr.route_variant_muid = route_variants.muid
where r.sign_deleted = 0
      and tr_id is not null
      and rtj.sign_deleted = 0
      and rr.sign_deleted = 0
      and route_variants.sign_deleted = 0;

comment on view mnt.v_rounds_attrib is 'Атрибуты рейса';
comment on column mnt.v_rounds_attrib.order_round_id is 'Идентификатор рейса';
comment on column mnt.v_rounds_attrib.order_date is 'Дата наряда';
comment on column mnt.v_rounds_attrib.route_variant_muid is 'Идентификатор варианта маршрутв';
comment on column mnt.v_rounds_attrib.rounds_muid is 'Идентификатор руйса внешний';
comment on column mnt.v_rounds_attrib.tr_id is 'Идентификатор ТС';
comment on column mnt.v_rounds_attrib.driver_id is 'Идентификатор водителя';
comment on column mnt.v_rounds_attrib.stop_place_a_muid is 'Идентификатор остановочного пункта внешний';
comment on column mnt.v_rounds_attrib.stop_place_b_muid is 'Идентификатор остановочного пункта внешний';
comment on column mnt.v_rounds_attrib.direction_id is 'Идентификатор направления';
comment on column mnt.v_rounds_attrib.timetable_entry_num is 'Номер выхода';
comment on column mnt.v_rounds_attrib.round_code is 'Код рейса';
comment on column mnt.v_rounds_attrib.round_id is 'Идентификатор';
comment on column mnt.v_rounds_attrib.order_list_id is 'Идентификатор наряда';
comment on column mnt.v_rounds_attrib.route_trajectories_muid is 'Идентификатор траектории внешний';
comment on column mnt.v_rounds_attrib.direction_name is 'Направление';
comment on column mnt.v_rounds_attrib.route_trajectories_length is 'Длина траектории';
comment on column mnt.v_rounds_attrib.route_muid is 'Идентификатор маршрута внешний';