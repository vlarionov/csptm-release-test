--returns: 0 - success, 1 - unit has activated already
CREATE OR REPLACE FUNCTION mnt.bnsr_request_list_pkg$add(
  p_unit_bnsr_id       cmnd.bnsr_request_list.unit_bnsr_id%TYPE,
  p_time_start         cmnd.bnsr_request_list.time_start%TYPE,
  p_is_include_manualy cmnd.bnsr_request_list.is_include_manualy%TYPE
)
  RETURNS INTEGER
LANGUAGE plpgsql
AS $$
DECLARE
  l_request_param TEXT;
BEGIN
  IF exists(
      SELECT 1
      FROM cmnd.bnsr_request_list
      WHERE unit_bnsr_id = p_unit_bnsr_id
            AND time_finish IS NULL
  )
  THEN
    RETURN 1;
  END IF;

  l_request_param = mnt.jf_bnsr_request_list_pkg$get_param(p_unit_bnsr_id);

  PERFORM cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd_to_rabbit(NULL, p_unit_bnsr_id, 20, l_request_param);

  INSERT INTO cmnd.bnsr_request_list (unit_bnsr_id, time_start, is_include_manualy)
  VALUES (p_unit_bnsr_id, p_time_start, p_is_include_manualy);

  RETURN 0;
END;
$$;

CREATE OR REPLACE FUNCTION mnt.bnsr_request_list_pkg$remove(
  p_bnsr_request_list_id cmnd.bnsr_request_list.bnsr_request_list_id%TYPE,
  p_is_exlude_manualy    cmnd.bnsr_request_list.is_exlude_manualy%TYPE
)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE
  l_unit_bnsr_id  cmnd.bnsr_request_list.unit_bnsr_id%TYPE;
  lt_res          TEXT;
  l_request_param TEXT;
BEGIN
  SELECT unit_bnsr_id
  INTO STRICT l_unit_bnsr_id
  FROM cmnd.bnsr_request_list
  WHERE bnsr_request_list_id = p_bnsr_request_list_id;

  l_request_param = mnt.jf_bnsr_request_list_pkg$get_param(l_unit_bnsr_id);

  SELECT cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd_to_rabbit(NULL, l_unit_bnsr_id, 21, l_request_param)
  INTO lt_res;

  UPDATE cmnd.bnsr_request_list
  SET
    time_finish       = current_timestamp,
    is_exlude_manualy = p_is_exlude_manualy
  WHERE
    bnsr_request_list_id = p_bnsr_request_list_id;
END;
$$;

CREATE OR REPLACE FUNCTION mnt.bnsr_request_list_pkg$auto_remove_by_unit_id(
  p_unit_id core.unit.unit_id%TYPE
)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  perform (
    SELECT mnt.bnsr_request_list_pkg$remove(bnsr_request_list_id, FALSE)
    FROM cmnd.bnsr_request_list
    WHERE unit_bnsr_id = p_unit_id
          AND time_finish IS NULL
          AND NOT is_include_manualy
  );
END;
$$;
