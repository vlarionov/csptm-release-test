﻿
create or replace function mnt.jf_routes_of_stop_pkg$of_rows
  (p_id_account               in numeric,
   p_rows                     out refcursor,
   p_attr                     in text)
returns refcursor
as $$ declare 
   v_stops_muid gis.stops.muid%type := jofl.jofl_pkg$extract_number(p_attr, 'STOP_MUID', true); 
begin 
   open p_rows for
   select distinct
          rtk.name    as "TR_KIND",
          r.number    as "NUM",
          st_a.name   as "START_POINT",
          st_b.name   as "END_POINT"
     from gis.routes r, gis.route_variants rv, gis.route_rounds rr,
          gis.route_trajectories rt, gis.stop_place2route_traj sp2rt,
          gis.stop_places sp, gis.stops st, gis.ref_transport_kinds rtk,
          gis.stop_places sp_a, gis.stop_places sp_b,
          gis.stops st_a, gis.stops st_b
    where r.muid = rv.route_muid
      and rv.muid = rr.route_variant_muid
      and rr.muid = rt.route_round_muid
      and rt.muid = sp2rt.route_trajectory_muid
      and sp2rt.stop_place_muid = sp.muid
      and sp.stop_muid = st.muid
      and rtk.muid = r.transport_kind_muid
      and r.current_route_variant_muid = rr.route_variant_muid
      and rr.stop_place_a_muid = sp_a.muid
      and rr.stop_place_b_muid = sp_b.muid
      and sp_a.stop_muid = st_a.muid
      and sp_b.stop_muid = st_b.muid
      and st.muid = v_stops_muid
    order by rtk.name, r.number;
  /*
  запрос на новых структурах
  with  st as (
select si2r.round_id,  1 as num_a,   (select max(si2rr.order_num) from rts.stop_item2round si2rr where si2rr.round_id= si2r.round_id  and not si2rr.sign_deleted ) num_b, si2r.order_num, st.name, si.tr_type_id, si.stop_id
from rts.stop_item2round si2r
join rts.stop_item si on si.stop_item_id  = si2r.stop_item_id and not si.sign_deleted
join rts.stop st on st.stop_id = si.stop_id
where st.stop_id =2072 and not si2r.sign_deleted
)
select
          tt.name      as "TR_KIND",
          r.route_num  as "NUM",
          st_a.name   as "START_POINT",
          st_b.name   as "END_POINT",
          rr.code,
          md.move_direction_name,
          md.move_direction_id
from
          rts.route r
          join rts.route_variant rv on rv.route_variant_id = r.current_route_variant_id and not rv.sign_deleted
          join rts.round rr on rr.route_variant_id = rv.route_variant_id
          join rts.move_direction md on md.move_direction_id = rr.move_direction_id
          join st on st.round_id = rr.round_id
          join core.tr_type tt on tt.tr_type_id = st.tr_type_id
          join rts.stop_item2round si2r_a on si2r_a.round_id = st.round_id and si2r_a.order_num =st.num_a  and not si2r_a.sign_deleted
          join rts.stop_item si_a on si_a.stop_item_id  = si2r_a.stop_item_id and not si_a.sign_deleted
          join rts.stop st_a on st_a.stop_id = si_a.stop_id
          join rts.stop_item2round si2r_b on si2r_b.round_id = st.round_id and si2r_b.order_num = st.num_b and not si2r_b.sign_deleted
          join rts.stop_item si_b on si_b.stop_item_id  = si2r_b.stop_item_id and not si_b.sign_deleted
          join rts.stop st_b on st_b.stop_id = si_b.stop_id
where not r.sign_deleted
order by r.route_num, rr.code, md.move_direction_id
  ;
  */
end;
$$ language plpgsql volatile security invoker;
