CREATE OR REPLACE FUNCTION mnt.jf_diagram_timeline_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare
  l_depo_id bigint;
  l_dt timestamp;
begin
  l_depo_id := jofl.jofl_pkg$extract_varchar(p_attr, 'STR_FIELD_TEXT', true )::bigint;
  l_dt  := jofl.jofl_pkg$extract_date(p_attr,    'F_DT', true );

    open p_rows for
with rounds as
(select
     coalesce(min(orf.time_fact), min(oo.time_oper)) begin_time
    ,case when max(oo.time_oper) > now()
        then max(oo.time_oper)
        else coalesce(max(orf.time_fact), max(oo.time_oper))
        end end_time
    ,min(oo.time_plan) begin_time_plan
    ,max(oo.time_plan) end_time_plan
    ,min(orf.time_fact) begin_time_fact
    ,max(orf.time_fact) end_time_fact
    ,ra.order_round_id
 from mnt.v_rounds_attrib ra
      join mnt.v_ref_routes r on r.current_route_variant_muid = ra.route_variant_muid
                                 join mnt.v_order_attrib oa on oa.order_list_id = ra.order_list_id
                        join tt.order_oper oo on (ra.order_round_id = oo.order_round_id
                                                  and oo.sign_deleted = 0
                                                         and oo.is_active = 1)
    left join tt.order_fact orf on (ra.order_round_id = orf.order_round_id
                                    and oo.stop_place_muid = orf.stop_place_muid
                                           and oo.stop_order_num = orf.stop_order_num
                                                                   and orf.sign_deleted = 0)
     where order_date = date_trunc('day', l_dt + interval '3 hours')
           and r.parks_muid = l_depo_id
           and (
             r.depo_depo_id in (select adm.account_data_realm_pkg$get_depos(p_id_account))
             or ra.tr_id in (select adm.account_data_realm_pkg$get_trs_by_tr_type(p_id_account))
             or ra.route_muid in (
               select route_muid
               from rts.route2gis
               where route_id in (select adm.account_data_realm_pkg$get_routes(p_id_account))
             )
           )
    group by ra.order_round_id
    ),
ra as (
    select *
    from mnt.v_rounds_attrib ra
      join mnt.v_ref_routes r on r.current_route_variant_muid = ra.route_variant_muid
      join mnt.v_order_attrib oa on oa.order_list_id = ra.order_list_id
    where order_date = date_trunc('day', l_dt + interval '3 hours')
              and r.parks_muid = l_depo_id
),
    round_visit_finish as
  (SELECT ra.route_variant_muid
     ,count(*) as round_cnt_plan
     ,sum((time_fact_end is null)::int) as round_cnt_fact
   FROM asd.oper_round_visit orv
     JOIN ra ON (orv.order_round_id = ra.order_round_id)
   GROUP BY ra.route_variant_muid
  )
select
   ra.parks_muid::text as depoId
  ,ra.parks_name as depoName
  ,ra.route_variant_muid::text as routeId
  ,ra.routes_number as routeName
  ,ra.rounds_muid::text as raceId
  ,ra.round_code as raceCode
  ,0 isTechRace
  ,ra.timetable_entry_num as exitName
  ,ra.tr_id as trId
  ,tr.garage_num as trname
  ,ra.driver_id as driverId
  ,ra.fio as driverName
  ,3 as periodTypeId
  ,rounds.begin_time
  ,rounds.end_time
  ,tr.licence
  ,tr.tr_type_name
  ,tr.tr_mark_name
  ,tr.tr_model_name
  ,tr.tr_capacity_full_name
  ,rounds.begin_time_plan
  ,rounds.end_time_plan
  ,rounds.begin_time_fact
  ,rounds.end_time_fact
  ,min(rounds.begin_time_plan) over (partition by ra.driver_id) as begin_time_plan_drv
  ,max(rounds.end_time_plan) over (partition by ra.driver_id)  as end_time_plan_drv
  ,min(rounds.begin_time_fact) over (partition by ra.driver_id)  as begin_time_fact_drv
  ,max(rounds.end_time_fact) over (partition by ra.driver_id)  as end_time_fact_drv
  ,min(rounds.begin_time_plan) over (partition by ra.tr_id) as begin_time_plan_tr
  ,max(rounds.end_time_plan) over (partition by ra.tr_id)  as end_time_plan_tr
  ,min(rounds.begin_time_fact) over (partition by ra.tr_id)  as begin_time_fact_tr
  ,max(rounds.end_time_fact) over (partition by ra.tr_id)  as end_time_fact_tr
  ,to_char(
       max(rounds.end_time_plan) over (partition by ra.driver_id) -
       min(rounds.end_time_plan) over (partition by ra.driver_id)
       , 'HH24:MI'
   ) as time_wrk_plan
  ,to_char(
       max(rounds.end_time_fact) over (partition by ra.driver_id) -
       min(rounds.end_time_fact) over (partition by ra.driver_id)
       , 'HH24:MI'
   ) as time_wrk_fact
  ,round_visit_finish.round_cnt_plan
  ,round_visit_finish.round_cnt_fact
  ,drv.tab_num
  ,st.name as  order_round_status_name
from ra
  join rounds on ra.order_round_id =  rounds.order_round_id
  join core.v_tr_ref tr on tr.tr_id = ra.tr_id
  join tt.driver drv on drv.driver_id = ra.driver_id
  join round_visit_finish on ra.route_variant_muid = round_visit_finish.route_variant_muid
  join asd.oper_round_visit orv on orv.order_round_id = rounds.order_round_id
  join asd.order_round_status st on st.order_round_status_id = orv.order_round_status_id
where orv.order_round_status_id in (0,1,3) or (orv.order_round_status_id = 2 and rounds.end_time >= now());

end;
$function$
;

/*
-- в разработке - чернвик запроса по новым структурам ...
select   rrt.depo_id  as depoId                             --  ra.parks_muid::text as depoId
       , rrt.name_full as depoName                          --  ,ra.parks_name as depoName
       , rtv.route_id as routeId                            --  ,ra.route_variant_muid::text as routeId
       , rrt.route_num as routeName                         --  ,ra.routes_number as routeName
   --    , tta.tt_action_id as raceId                         --  ,ra.rounds_muid::text as raceId
--       , rd.code                                            --  ,ra.round_code as raceCode
   --    , tta.action_type_id as actionTypeID
       , 0 isTechRace                                       -- ??? поставить флаг "технический"/"промышленный" рейс
       , tto.tt_out_num as exitName                         -- ,ra.timetable_entry_num as exitName
       , tr.tr_id as trId                                   -- ,ra.tr_id as trId
       , tr.garage_num as trname                            -- ,tr.garage_num as trname
-- ,ra.driver_id as driverId
 -- ,ra.fio as driverName
 -- ,3 as periodTypeId
 -- ,rounds.begin_time
 -- ,rounds.end_time
       , tr.licence as licence                              -- ,tr.licence
       , tr.tr_type_name as tr_type_name                    -- ,tr.tr_type_name
       , tr.tr_mark_name as tr_mark_name                    --  ,tr.tr_mark_name
       , tr.tr_model_name as tr_model_name                  --  ,tr.tr_model_name
       , tr.tr_capacity_full_name as tr_capacity_full_name  --  ,tr.tr_capacity_full_name
--  ,rounds.begin_time_plan
--  ,rounds.end_time_plan
--  ,rounds.begin_time_fact
--  ,rounds.end_time_fact
--  ,min(rounds.begin_time_plan) over (partition by ra.driver_id) as begin_time_plan_drv
--  ,max(rounds.end_time_plan) over (partition by ra.driver_id)  as end_time_plan_drv
--  ,min(rounds.begin_time_fact) over (partition by ra.driver_id)  as begin_time_fact_drv
--  ,max(rounds.end_time_fact) over (partition by ra.driver_id)  as end_time_fact_drv
--  ,min(rounds.begin_time_plan) over (partition by ra.tr_id) as begin_time_plan_tr
--  ,max(rounds.end_time_plan) over (partition by ra.tr_id)  as end_time_plan_tr
--  ,min(rounds.begin_time_fact) over (partition by ra.tr_id)  as begin_time_fact_tr
--  ,max(rounds.end_time_fact) over (partition by ra.tr_id)  as end_time_fact_tr
--  ,to_char(max(rounds.end_time_plan) over (partition by ra.driver_id) - min(rounds.end_time_plan) over (partition by ra.driver_id), 'HH24:MI') as time_wrk_plan
--  ,to_char(max(rounds.end_time_fact) over (partition by ra.driver_id) - min(rounds.end_time_fact) over (partition by ra.driver_id), 'HH24:MI') as time_wrk_fact
--  ,round_visit_finish.round_cnt_plan
--  ,round_visit_finish.round_cnt_fact
--  ,drv.tab_num
--  ,st.name as  order_round_status_name
from ttb.tt_variant ttv
     join rts.route_variant rtv on rtv.route_variant_id = ttv.route_variant_id
     join mnt.v_rts_routes rrt on rrt.route_id = rtv.route_id
     join ttb.tt_out tto on tto.tt_variant_id = ttv.tt_variant_id
--     join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
--     join ttb.tt_action_round ttar on ttar.tt_action_id = tta.tt_action_id
--     join rts.round rd on rd.round_id = ttar.round_id
     join core.v_tr_ref tr on tr.tr_id = tto.tr_id
where     not ttv.sign_deleted
      and ttv.parent_tt_variant_id is not null
      and not rtv.sign_deleted
      and not tto.sign_deleted
   --   and not tta.sign_deleted
--      and not rd.sign_deleted
   --   and tta.action_type_id = any (array[25,2,3,4,5,6,7,9,10,1,42,37,38,39,40,41,43,44,45,46,47,48,49,50,53,54,55,56])
  \* select ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$production_round(), false) *\
and ttv.order_date = date_trunc('day', now() \* l_dt *\)
and rrt.depo_id = 494
*/