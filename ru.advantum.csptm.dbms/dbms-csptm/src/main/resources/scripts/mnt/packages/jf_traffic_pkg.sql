CREATE OR REPLACE FUNCTION mnt.jf_traffic_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS core.traffic AS
$body$
declare 
   l_r core.traffic%rowtype; 
begin 
   l_r.packet_id := jofl.jofl_pkg$extract_number(p_attr, 'packet_id', true); 
   l_r.tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true); 
   l_r.unit_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_id', true); 
   l_r.event_time := jofl.jofl_pkg$extract_date(p_attr, 'event_time', true); 
   l_r.device_event_id := jofl.jofl_pkg$extract_number(p_attr, 'device_event_id', true); 
   l_r.location_valid := jofl.jofl_pkg$extract_boolean(p_attr, 'location_valid', true); 
   l_r.gps_time := jofl.jofl_pkg$extract_date(p_attr, 'gps_time', true); 
   l_r.lon := jofl.jofl_pkg$extract_varchar(p_attr, 'lon', true); 
   l_r.lat := jofl.jofl_pkg$extract_varchar(p_attr, 'lat', true); 
   l_r.alt := jofl.jofl_pkg$extract_number(p_attr, 'alt', true); 
   l_r.speed := jofl.jofl_pkg$extract_number(p_attr, 'speed', true); 
   l_r.heading := jofl.jofl_pkg$extract_number(p_attr, 'heading', true); 
   l_r.receive_time := jofl.jofl_pkg$extract_date(p_attr, 'receive_time', true); 
   l_r.is_hist_data := jofl.jofl_pkg$extract_boolean(p_attr, 'is_hist_data', true); 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_traffic_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r core.traffic%rowtype;
begin 
   l_r := mnt.jf_traffic_pkg$attr_to_rowtype(p_attr);

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_traffic_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r core.traffic%rowtype;
begin 
   l_r := mnt.jf_traffic_pkg$attr_to_rowtype(p_attr);

   /*insert into core.traffic select l_r.*;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_traffic_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
  l_rf_tr_id	 core.tr.tr_id%type 		:= jofl.jofl_pkg$extract_number(p_attr, 'f_tr_id', true);
  l_rf_unit_id	 core.unit.unit_id%type		:= jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_id', true);
  f_bgn_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_bgn_DT', true);
  f_end_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_end_DT', true);
  f_valid		smallint					:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_valid_INPLACE_K', true), -1);
  f_history		smallint					:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_history_INPLACE_K', true), -1);
begin 
 open p_rows for 
 select trf.event_time,
 		trf.receive_time,
        trf.lat,
        trf.lon,
        trf.speed,
        trf.heading,
        ex.track_length,
        trf.alt,
        ex.is_validity,
        ex.is_validity as is_valid_diagnostic,
--         true as is_valid_diagnostic,
        trf.is_hist_data,
        ex.is_internal_battery,
        ex.is_first_switch_on,
        ex.battery_voltage,
        ex.max_period_speed,
        ex.odometer,
        ex.gsm_level,
        ex.gprs_state,
        ex.accel_energy,
        0 as accel_accel,
        ex.satellites_count,
        case when ex.is_north_lat then 'N' when not ex.is_north_lat then 'S' else 'N/A' end is_north_lat,
        case when ex.is_east_lon  then 'E' when not ex.is_east_lon  then 'W' else 'N/A' end is_east_lon,
        ex.is_sos,
        ex.is_alarm,
        ex.is_voice_call,
          '[' || json_build_object('CAPTION', 'Входы и выходы',
                                   'JUMP', json_build_object('METHOD', 'mnt.unit_in_out_data',
                                                             'ATTR', json_build_object('f_event_time', to_char(trf.event_time,'yyyy-mm-dd hh24:mi:ss'), 'f_packet_id', trf.packet_id::text)::text,
                                                             'WIN_TITLE', '')
                                  ) || ']' as jump_in_out
	from core.traffic trf
	left join snsr.extended_data ex on trf.packet_id = ex.packet_id and trf.event_time = ex.event_time
    join core.tr tr on trf.tr_id = tr.tr_id
    join core.unit2tr u2tr on tr.tr_id = u2tr.tr_id 
    join core.unit u on u2tr.unit_id = u.unit_id and u.unit_id = trf.unit_id
   where trf.event_time >= f_bgn_dt 
     and trf.event_time <= f_end_dt  
     and (l_rf_tr_id is null or tr.tr_id = l_rf_tr_id)
  	 and u.unit_id = l_rf_unit_id     
     and (f_valid = -1
  	   or (f_valid = 1 and ex.is_validity)
       or (f_valid = 0 and not ex.is_validity))
     and (f_history = -1
  	   or (f_history = 1 and trf.is_hist_data)
       or (f_history = 0 and not trf.is_hist_data)); 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_traffic_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r core.traffic%rowtype;
begin 
   l_r := mnt.jf_traffic_pkg$attr_to_rowtype(p_attr);

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
----------------------------------------------------------------------