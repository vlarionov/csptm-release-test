CREATE OR REPLACE FUNCTION mnt.jf_fuel_sensor_data_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS snsr.fuel_sensor_data AS
$body$
declare 
   l_r snsr.fuel_sensor_data%rowtype; 
begin 
   l_r.packet_id := jofl.jofl_pkg$extract_number(p_attr, 'packet_id', true); 
   l_r.event_time := jofl.jofl_pkg$extract_date(p_attr, 'event_time', true); 
   l_r.sensor_id := jofl.jofl_pkg$extract_number(p_attr, 'sensor_id', true); 
   l_r.tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true); 
   l_r.level_mm := jofl.jofl_pkg$extract_number(p_attr, 'level_mm', true); 
   l_r.level_l := jofl.jofl_pkg$extract_number(p_attr, 'level_l', true); 
   l_r.temperature := jofl.jofl_pkg$extract_varchar(p_attr, 'temperature', true); 
   l_r.switched_on := jofl.jofl_pkg$extract_boolean(p_attr, 'switched_on', true); 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_fuel_sensor_data_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
  l_rf_tr_id	 core.tr.tr_id%type 		:= jofl.jofl_pkg$extract_number(p_attr, 'f_tr_id', true);
  l_rf_unit_id	 core.unit.unit_id%type		:= jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_id', true);
  l_rf_sensor_id core.sensor.sensor_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_sensor_id', true);
  f_bgn_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_bgn_DT', true);
  f_end_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_end_DT', true);
  p_timezone	bigint						:= 3;
begin 
 open p_rows for 
      select 
        fsd.packet_id, 
        fsd.event_time, 
        fsd.sensor_id, 
        fsd.tr_id,  
 		to_char(fsd.event_time + (p_timezone || ' hour')::INTERVAL, 'dd.mm.yyyy hh24:mi:ss') as s_event_time,
       	fsd.level_l,
        fsd.level_mm,
        fsd.temperature,
        fsd.switched_on::int switched_on
      from snsr.fuel_sensor_data fsd
      join core.tr tr on fsd.tr_id = tr.tr_id
      join core.unit2tr u2tr on tr.tr_id = u2tr.tr_id 
      join core.unit u on u.unit_id = u2tr.unit_id
      join core.sensor2unit s2un on u.unit_id = s2un.unit_id and s2un.sensor_id = fsd.sensor_id
where tr.tr_id = l_rf_tr_id
  and u.unit_id = l_rf_unit_id
  and s2un.sensor_id = l_rf_sensor_id
  and fsd.event_time >= f_bgn_dt 
  and fsd.event_time <= f_end_dt  
  ; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_fuel_sensor_data_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r snsr.fuel_sensor_data%rowtype;
begin 
   l_r := mnt.jf_fuel_sensor_data_pkg$attr_to_rowtype(p_attr);

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_fuel_sensor_data_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r snsr.fuel_sensor_data%rowtype;
begin 
   l_r := mnt.jf_fuel_sensor_data_pkg$attr_to_rowtype(p_attr);

   /*insert into snsr.fuel_sensor_data select l_r.*;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_fuel_sensor_data_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r snsr.fuel_sensor_data%rowtype;
begin 
   l_r := mnt.jf_fuel_sensor_data_pkg$attr_to_rowtype(p_attr);

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------------------------