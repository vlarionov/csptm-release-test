CREATE OR REPLACE FUNCTION mnt.jf_unit_session_log_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS kdbo.unit_session_log AS
$body$
declare 
   l_r kdbo.unit_session_log%rowtype; 
begin 
   l_r.tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true); 
   l_r.unit_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_id', true); 
   l_r.time_start := jofl.jofl_pkg$extract_date(p_attr, 'time_start', true); 
   l_r.time_stop := jofl.jofl_pkg$extract_date(p_attr, 'time_stop', true); 
   l_r.total_packet_qty := jofl.jofl_pkg$extract_number(p_attr, 'total_packet_qty', true); 
   l_r.invalid_packet_qty := jofl.jofl_pkg$extract_number(p_attr, 'invalid_packet_qty', true); 
   l_r.hist_packet_qty := jofl.jofl_pkg$extract_number(p_attr, 'hist_packet_qty', true); 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_unit_session_log_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r kdbo.unit_session_log%rowtype;
begin 
   l_r := mnt.jf_unit_session_log_pkg$attr_to_rowtype(p_attr);

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_unit_session_log_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
  l_rf_tr_id	 core.tr.tr_id%type 		:= jofl.jofl_pkg$extract_number(p_attr, 'f_tr_id', true);
  l_rf_unit_id	 core.unit.unit_id%type		:= jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_id', true);
  l_rf_depo_id   core.depo.depo_id%type		:= jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', true);
  larr_rf_depo_id   BIGINT [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', FALSE);
  f_bgn_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_bgn_DT', true);
  f_end_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_end_DT', true);
  l_session_type text:= jofl.jofl_pkg$extract_varchar(p_attr, 'F_SESSION_INPLACE_K', true);

begin 
 open p_rows for 
       select 
        usl.tr_id, 
        usl.unit_id, 
        max(usl.time_start) time_start,
        max(usl.time_stop) time_stop,
        round(extract (epoch from max(usl.time_stop) - max(usl.time_start))) as conn_duration,
        sum(usl.total_packet_qty) total_packet_qty,
        sum(usl.invalid_packet_qty) invalid_packet_qty,
        sum(usl.hist_packet_qty) hist_packet_qty,
        tr.garage_num,
        u.unit_num,
        eq.serial_num,
        eq.depo_id
      from kdbo.unit_session_log usl
      join core.tr tr on usl.tr_id = tr.tr_id
      join core.unit2tr u2tr on tr.tr_id = u2tr.tr_id 
      join core.unit u on u2tr.unit_id = u.unit_id and usl.unit_id = u.unit_id
      join core.equipment eq on eq.equipment_id = u.unit_id
 where eq.depo_id = ANY (larr_rf_depo_id)
   and (usl.time_start between f_bgn_dt and f_end_dt)
   and (usl.tr_id = l_rf_tr_id or l_rf_tr_id is null)
   and (usl.unit_id = l_rf_unit_id or l_rf_unit_id is null)
GROUP BY
        usl.tr_id,
        usl.unit_id,
        tr.garage_num,
        u.unit_num,
        eq.serial_num,
        eq.depo_id ,
        usl.session_id
having
(
         (max(usl.time_stop) is null and l_session_type = 'open')
         or
         (max(usl.time_stop) is not null and l_session_type = 'closed')
         or
         (l_session_type = 'all')
       )
	; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_unit_session_log_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r kdbo.unit_session_log%rowtype;
begin 
   l_r := mnt.jf_unit_session_log_pkg$attr_to_rowtype(p_attr);

   /*insert into kdbo.unit_session_log select l_r.*;*/

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_unit_session_log_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r kdbo.unit_session_log%rowtype;
begin 
   l_r := mnt.jf_unit_session_log_pkg$attr_to_rowtype(p_attr);

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------------------------------
-------------------------------------------------------------------------