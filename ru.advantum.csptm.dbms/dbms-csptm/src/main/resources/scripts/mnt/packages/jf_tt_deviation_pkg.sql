CREATE OR REPLACE FUNCTION mnt.jf_tt_deviation_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare
    l_route_variant bigint := jofl.jofl_pkg$extract_number(p_attr, 'F_CURRENT_ROUTE_VARIANT_MUID', TRUE)::bigint;
    l_dt_begin timestamp := COALESCE(jofl.jofl_pkg$extract_date(p_attr, 'F_BEGIN_DT', true), date_trunc('day', now()));
    l_dt_end timestamp  := COALESCE(jofl.jofl_pkg$extract_date(p_attr, 'F_END_DT', true), (date_trunc('day', now()) + interval '1 day'));
    l_gtg_num int := jofl.jofl_pkg$extract_number(p_attr, 'F_GRG_NOMER_TEXT', true)::INT;
    l_round_code text := jofl.jofl_pkg$extract_varchar(p_attr, 'F_ROUND_CODE_TEXT', true);
    l_stop_name text := jofl.jofl_pkg$extract_varchar(p_attr, 'F_STOP_NAME_TEXT', true);
    l_tr_id bigint := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true)::BIGINT;
begin


  open p_rows for
  with tr_rounds as
  (
      select
        order_round_id,
        route_variant_muid,
        round_code||'-'||direction_name as round_code,
        tr.tr_id,
        tr.garage_num,
        routes.number as routes_number
      from mnt.v_rounds_attrib
           join core.tr on tr.tr_id = v_rounds_attrib.tr_id
           join gis.routes on routes.current_route_variant_muid = route_variant_muid
      where
        order_date between date_trunc('day', l_dt_begin) and date_trunc('day', l_dt_end)
        and (route_variant_muid = l_route_variant or l_route_variant is null)
        and (tr.garage_num = l_gtg_num or l_gtg_num is null)
        and (v_rounds_attrib.round_code = l_round_code or l_round_code is null)
        and (l_dt_end - l_dt_begin <= interval '24 hours')
        and (l_route_variant is not null or l_tr_id is not null)
        and (tr.tr_id = l_tr_id or l_tr_id is null)
        and (
          tr.tr_id in (select adm.account_data_realm_pkg$get_trs_by_tr_type_and_depo(p_id_account))
          or v_rounds_attrib.route_muid in (select route_muid
                                            from rts.route2gis
                                            where route_id in (select adm.account_data_realm_pkg$get_routes(p_id_account)))
        )
  )
  select
    sp.muid :: text           as route_vatiant_muid,
    oo.time_plan         as time_plan,
    orf.time_fact         as time_fact,
    oo.time_oper         as time_oper,
    round(EXTRACT(EPOCH FROM (oo.time_oper -  orf.time_fact))/60) as tt_deviation,
    route_variant_muid :: text as route_vatiant_muid,
    s.name as stops_name,
    tr_rounds.tr_id as tr_id,
    tr_rounds.garage_num as garage_num,
    tr_rounds.round_code as round_code,
    tr_rounds.routes_number
  from tr_rounds
    join tt.order_oper oo on tr_rounds.order_round_id = oo.order_round_id
    join gis.stop_places sp on oo.stop_place_muid = sp.muid
    join gis.stops s on sp.stop_muid = s.muid
    left join tt.order_fact orf on (tr_rounds.order_round_id = orf.order_round_id
                                    and oo.stop_place_muid = orf.stop_place_muid
                                    and oo.stop_order_num = orf.stop_order_num)
  WHERE oo.sign_deleted = 0 and orf.sign_deleted = 0 and (s.name ilike l_stop_name or l_stop_name is null or l_stop_name = '')
  order by orf.time_fact desc
  limit 1000;


end;
$function$
;

