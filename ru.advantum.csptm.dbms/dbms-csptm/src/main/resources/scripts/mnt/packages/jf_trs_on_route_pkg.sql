﻿create or replace function mnt.jf_trs_on_route_pkg$of_rows
  (p_id_account               in numeric,
   p_attr                     in text,
   p_rows                     out refcursor)
returns refcursor
as $$ declare 
   l_query_date   timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'QUERY_DATE', true), now());
   l_order_date   timestamp := date_trunc('day', l_query_date);
   l_tr_kind_muid bigint    := jofl.jofl_pkg$extract_number(p_attr, 'F_TR_KIND_MUID', true);
   l_route_number text      := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_ROUTE_NUMBER', true), '');
begin
   -- TODO: abc - фнкция не готова, рассматривать как заготовку
   open p_rows for
   select t.tr_id, 
          t.garage_num as "GARAGE_NUM", 
          p.name as "TR_TYPE_NAME",
          r.round_id, 
          m.route_number as "ROUTE_NUMBER",
          null::timestamp as "BEGIN_TIME_PLAN", 
          null::timestamp as "BEGIN_TIME_FACT", 
          null::timestamp as "END_TIME_PLAN", 
          null::timestamp as "END_TIME_FACT",
          null::text      as "TR_STATUS",
          null::text      as "TR_EVENT"
     from core.tr t
     join core.tr_type p on p.tr_type_id = t.tr_type_id
     join (select distinct tr_id
             from tt.order_list
            where order_date = l_order_date
          ) o on o.tr_id = t.tr_id
     left join (select ol.tr_id, dr.round_id
                  from asd.oper_round_visit av
                  join tt.order_round dr on dr.order_round_id = av.order_round_id
                  join tt.order_list ol on ol.order_list_id = dr.order_list_id
                 where l_query_date between av.time_plan_begin and av.time_plan_end
                   and ol.tr_id is not null
                   and ol.sign_deleted = 0
                   and dr.is_active = 1
                 order by ol.tr_id
          ) r on r.tr_id = t.tr_id
     left join (select rd.round_id, rt.number as route_number
                  from tt.round rd
                  join gis.route_trajectories tj on tj.muid = rd.route_trajectory_muid
                  join gis.route_rounds u on u.muid = tj.route_round_muid
                  join gis.route_variants rv on rv.muid = u.route_variant_muid
                  join gis.routes rt on rt.muid = rv.route_muid
               ) m on m.round_id = r.round_id
    where (p.tr_type_id = l_tr_kind_muid or l_tr_kind_muid is null)
      and (m.route_number = l_route_number or l_route_number = '')
    order by t.garage_num;
end;
$$ language plpgsql volatile security invoker;
