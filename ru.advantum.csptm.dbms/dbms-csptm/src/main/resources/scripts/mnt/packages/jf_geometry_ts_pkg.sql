CREATE OR REPLACE FUNCTION mnt.jf_geometry_ts_pkg$of_rows(IN p_id_account NUMERIC, OUT p_rows REFCURSOR, IN p_attr TEXT)
  RETURNS REFCURSOR
AS
  $BODY$
declare
  l_geom geometry;
  l_time_begin timestamp without time zone;
  l_time_end timestamp without time zone;
begin
  l_geom := ST_GeomFromText(jofl.jofl_pkg$extract_varchar(p_attr, 'GEOM', true));
  l_time_begin := jofl.jofl_pkg$extract_date(p_attr, 'DT_BEGIN', true);
  l_time_end := jofl.jofl_pkg$extract_date(p_attr, 'DT_END', true);

  open p_rows for
    select trt.name,
        ro.number,
        tr.garage_num,
        tr.licence,
        r.round_code,
        case when r.direction_id = 1 then 'прямой' else 'обратный' end as direction,
        te.timetable_entry_num,
        max(tgl.event_time) + interval '3 hours' as out_time,
        min(tgl.event_time) + interval '3 hours' as in_time
    from gis.route_trajectories as rt
    	join tt.round as r on r.route_trajectory_muid = rt.muid
    	join tt.order_round as orr on r.round_id = orr.round_id
    	join tt.order_list as ol on orr.order_list_id = ol.order_list_id
    	join gis.route_rounds as rr on rt.route_round_muid = rr.muid
    	join gis.route_variants as rv on rr.route_variant_muid = rv.muid
        join gis.routes as ro on ro.muid = rv.route_muid
    	join asd.oper_round_visit as orv on orv.order_round_id = orr.order_round_id
    	join tt.timetable_entry as te on te.timetable_entry_id = r.timetable_entry_id
    	join asd.tf_graph_link as tgl on tgl.tr_id = ol.tr_id and tgl.order_round_id = orr.order_round_id
    	join core.tr as tr on tr.tr_id = ol.tr_id
    	join core.tr_type as trt on trt.tr_type_id = tr.tr_type_id
    where ST_Intersects(rt.geom, l_geom)
        and orv.time_plan_begin >= l_time_begin and orv.time_plan_end <= l_time_end
        and tgl.event_time >= l_time_begin and tgl.event_time <= l_time_end
        and rt.sign_deleted != 1
        and rv.sign_deleted != 1
        and rr.sign_deleted != 1
        and (rv.end_date is null or rv.end_date >= l_time_end)
    group by tr.tr_id, ro.number, trt.name, tr.garage_num, tr.licence, r.round_code, r.direction_id, te.timetable_entry_num;
  end;
  $BODY$
LANGUAGE plpgsql;
