CREATE OR REPLACE FUNCTION mnt.jf_diagram_statistic_cross_stops_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare
  raceId numeric;
  l_dt timestamp;
  l_round_id_list bigint[];  
  l_stops_array numeric[];
  l_route_variant_id rts.route_variant.route_variant_id%type;
  l_round_num rts.round.round_num%type;
  l_movedir_id rts.move_direction.move_direction_id%type;
begin
  l_route_variant_id  := jofl.jofl_pkg$extract_number(p_attr, 'route_variant_id', true);
  l_round_num := jofl.jofl_pkg$extract_number(p_attr, 'round_num', true);
  l_movedir_id := jofl.jofl_pkg$extract_number(p_attr, 'move_direction_id', true);
  l_stops_array := jofl.jofl_pkg$extract_tarray(p_attr, 'stop_item_list', TRUE);
  l_dt  := jofl.jofl_pkg$extract_date(p_attr,    'F_DT', true) + interval '3 hours';

  -- получаем round_id выбранного рейса
  select array_agg(ro.round_id)
  into l_round_id_list
  from rts.round ro
  where ro.route_variant_id = l_route_variant_id and
        ro.round_num = l_round_num and
		    ro.move_direction_id = l_movedir_id;

  open p_rows for
  with cross_stops as (select cross_round.round_id,
                              cross_round.stop_item_id
                     from (select rd_cross.round_id,
                                  si2rd_cross.stop_item_id,
                                  si2rd_cross.order_num,
                                  lead(si2rd_cross.order_num) OVER (PARTITION BY rd_cross.round_id ORDER BY rd_cross.round_id, si2rd_cross.order_num) next_order_num,
                                  lag(si2rd_cross.order_num) OVER (PARTITION BY rd_cross.round_id ORDER BY rd_cross.round_id, si2rd_cross.order_num) prev_order_num
                           from      (select * from rts.round where round_id = any(l_round_id_list)) rd_main
                                join rts.stop_item2round si2rd_main on si2rd_main.round_id = rd_main.round_id and
                                                                       si2rd_main.stop_item_id = any(l_stops_array)
                                join rts.stop_item2round si2rd_cross on si2rd_cross.stop_item_id = si2rd_main.stop_item_id and
                                                                        si2rd_cross.round_id != rd_main.round_id
                                join rts.round rd_cross on rd_cross.round_id = si2rd_cross.round_id
                           where     not rd_main.sign_deleted
                                 and not si2rd_main.sign_deleted
                                 and not si2rd_cross.sign_deleted
                                 and not rd_cross.sign_deleted) cross_round
                     where (cross_round.next_order_num - cross_round.order_num) = 1 or
                           (cross_round.order_num - cross_round.prev_order_num) = 1),
     rd as (select ra.*,
                   dr.fio,
                   tr.garage_num,
                   tr.licence,
                   r.route_num
            from mnt.v_tt_action_round_attrib ra
                 join (select d.driver_id,
                              d.driver_name||' '||d.driver_middle_name||' '||d.driver_last_name fio
                       from core.driver d
                       where d.sign_deleted = 0) dr on dr.driver_id = ra.driver_id
                 join core.tr tr on tr.tr_id = ra.tr_id
                 join rts.route r on r.route_id = ra.route_id and not r.sign_deleted
            where ra.order_date = date_trunc('day', l_dt))
select   rd.route_variant_id::text as routeId -- ra.route_variant_muid::text as routeId
       , rd.route_num as routeName            --  ,ra.routes_number as routeName
       , rd.round_id as raceId                -- ,ra.rounds_muid::text as raceId
       , rd.code as raceCode                  -- ,ra.round_code as raceCode
       , rd.move_direction_id as dir          -- ,ra.direction_id dir
       , rd.tt_out_num as  exitNum            -- ,ra.timetable_entry_num as exitNum
       , ttai.time_begin as plan_time         -- ,oo.time_plan plan_time
       , ttaf.time_fact_begin as real_time    -- ,orf.time_fact real_time
       , si.stop_item_id::text as stopId      -- ,s.muid::text stopId
       , s.name as stopName                   -- ,s.name stopName
       , sum(si2r.length_sector) OVER (PARTITION BY rd.tt_action_id ORDER BY si2r.order_num) as len   --    ,sum(stop_place2route_traj.length_sector) OVER (PARTITION BY ra.order_round_id ORDER BY order_num) len
       , rd.tt_action_id as roundid           -- ,ra.order_round_id as roundid
from rd
     join ttb.tt_action_item ttai on ttai.tt_action_id = rd.tt_action_id
     join rts.stop_item2round si2r on ttai.stop_item2round_id = si2r.stop_item2round_id
     join cross_stops cs on cs.round_id = rd.round_id and
                            cs.stop_item_id = si2r.stop_item_id
     join rts.stop_item si on si.stop_item_id = si2r.stop_item_id and
                              si.stop_item_id = any(l_stops_array)
     join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
     join rts.stop s on s.stop_id = sl.stop_id
     join ttb.tt_action_fact ttaf on ttaf.tt_action_id = ttai.tt_action_id and
                                     ttaf.stop_item2round_id = ttai.stop_item2round_id
where     not ttai.sign_deleted
      and not si2r.sign_deleted
      and not si.sign_deleted
      and not sl.sign_deleted
      and not s.sign_deleted
order by rd.tt_action_id, si2r.order_num;

end;
$function$
;
