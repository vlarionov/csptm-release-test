﻿
create or replace function mnt.jf_routes_of_park_pkg$of_rows
  (p_id_account               in numeric,
   p_rows                     out refcursor,
   p_attr                     in text)
returns refcursor
as $$ declare
   v_park_muid gis.stops.muid%type := jofl.jofl_pkg$extract_number(p_attr, 'PARK_MUID', true);
begin 
   open p_rows for
   select distinct
          rtk.name    as "TR_KIND",
          r.number    as "NUM",
          st_a.name   as "START_POINT",
          st_b.name   as "END_POINT"
     from gis.parks2routes p
     join gis.routes r on r.muid = p.route_muid
     join gis.ref_transport_kinds rtk on rtk.muid = r.transport_kind_muid 
     join gis.route_rounds rr on rr.route_variant_muid = r.current_route_variant_muid
     join gis.stop_places sp_a on sp_a.muid = rr.stop_place_a_muid
     join gis.stop_places sp_b on sp_b.muid = rr.stop_place_b_muid
     join gis.stops st_a on st_a.muid = sp_a.stop_muid
     join gis.stops st_b on st_b.muid = sp_b.stop_muid
    where p.sign_deleted = 0
      and r.sign_deleted = 0
      and rr.sign_deleted = 0
      and rr.sign_deleted = 0
      and p.park_muid = v_park_muid
    order by rtk.name, r.number;
end;
$$ language plpgsql volatile
security invoker;
