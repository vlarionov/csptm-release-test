create or replace function mnt.jf_route_without_order_pkg$of_rows
    (p_id_account in  numeric,
     p_rows       out refcursor,
     p_attr       in  text)
    returns refcursor
as $$
declare
    f_tr_id      bigint [] :=jofl.jofl_pkg$extract_narray(p_attr, 'f_tr_id', true) :: bigint [];
    f_tr_type_id core.tr_type.tr_type_id%type :=jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id', true) :: smallint;
    l_interval   integer :=asd.get_constant('MNT_SEARCH_INTERVAL');
begin
    open p_rows for
    with all_round_tr as
    (
        select tto.tr_id
        from ttb.tt_out tto
            join ttb.tt_action ta on tto.tt_out_id = ta.tt_out_id
        where
            tto.sign_deleted = false
            and
            ta.dt_action between now() - '1 h' :: interval and now()
            and
            ta.sign_deleted = false
        group by tto.tr_id
    ),
            last_traffic as
        (
            select t2.tr_id
            from core.traffic t2
            where t2.event_time between now() - l_interval * '1 sec' :: interval and now()
            group by t2.tr_id
            order by t2.tr_id
        )
    select
        lt.tr_id,
        tls.event_time time_last_geo_point,
        tt.name        tr_type,
        tt.tr_type_id,
        ct.garage_num,
        ct.licence,
        depo.name_full    depo_name,
        e.name_short depo_ter
    from last_traffic lt
        left join all_round_tr art on lt.tr_id = art.tr_id
        join core.tr ct on ct.tr_id = lt.tr_id
        join core.tr_type tt on ct.tr_type_id = tt.tr_type_id
        join core.tr_last_state tls on ct.tr_id = tls.tr_id
        JOIN core.territory cter ON cter.territory_id = core.jf_tr_pkg$find_territory(ct.tr_id , ct.depo_id)
        JOIN core.entity e ON e.entity_id = cter.territory_id
        JOIN core.entity depo ON depo.entity_id = ct.depo_id
    where lt.tr_id is not null and art.tr_id isnull
          and (cardinality(f_tr_id) = 0 or f_tr_id isnull or ct.tr_id = any (f_tr_id))
          and (f_tr_type_id is null or tt.tr_type_id = f_tr_type_id)
    order by lt.tr_id;
end;
$$ language plpgsql;
