﻿CREATE OR REPLACE FUNCTION mnt.jf_rounds_of_route_pkg$of_rows (
  p_account_id numeric,
  p_attr text,
  out p_rows refcursor
)
RETURNS refcursor AS
$body$
 declare
   l_query_date timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'F_DT', true), now());
   l_order_date timestamp := date_trunc('day', l_query_date);
   l_tr_type_id bigint    := jofl.jofl_pkg$extract_number(p_attr, 'F_tr_type_id', true);
   l_route_muid bigint    := jofl.jofl_pkg$extract_number(p_attr, 'f_route_muid', true);
   l_grg_num    text      := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_GRG_NUM_TEXT', true), '');
   l_driver     text      := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_DRIVER_TEXT', true),'');
   l_status     smallint  := jofl.jofl_pkg$extract_number(p_attr, 'F_order_round_status_id', true);
   l_beg_dt     timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'F_BEG_DT', true);
   l_end_dt     timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'F_END_DT', true);
   l_tr_id      bigint    := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
begin
   if l_tr_type_id    is null and
      l_status        is null and
      l_route_muid    is null and
      l_tr_id         is null and
      (l_grg_num = '')        and
      (l_driver = '')         then
      
      open p_rows for select '[]';
   else
   open p_rows for
   with round_fact as (
   select orf.order_round_id,
          min(orf.time_fact) as time_fact_begin,
          max(orf.time_fact) as time_fact_end,
          core.round_minutes(max(orf.time_fact)) - 
          core.round_minutes(min(orf.time_fact)) as fact_duration
     from tt.order_list orl
     join tt.order_round orr on orr.order_list_id = orl.order_list_id
     join tt.order_fact orf on orf.order_round_id = orr.order_round_id
    where orl.sign_deleted = 0
      and orl.is_active = 1
      and orr.is_active = 1
      and orf.sign_deleted = 0
      and orl.order_date = l_order_date
      and (l_tr_id is null or orl.tr_id = l_tr_id)
    group by orf.order_round_id
   )
   select orl.tr_id, rnd.round_id, typ.name as tr_type_name, t.garage_num,
          drv.driver_last_name || ' ' || drv.driver_name || ' ' || drv.driver_middle_name as driver_name,
          drv.tab_num as driver_tab_num,
          orl.shift,
          tme.timetable_entry_num,
          grt.number as route_number,
          rnd.round_type,
          rnd.round_code,
          case rnd.direction_id when 1 then 'Прямое'  when 2 then 'Обратное' end as round_direction_name,
          st.name             as order_round_status_name,
          rnf.time_fact_begin as stop_place_a_time,
          rnf.time_fact_begin as stop_place_a_date,
        --rnf.time_fact_end   as stop_place_b_time,
          case when orv.order_round_status_id in (tt.const_pkg$ord_status_error(), tt.const_pkg$ord_status_passed()) then rnf.time_fact_end else null::timestamp end as stop_place_b_time,
          case when orv.order_round_status_id in (tt.const_pkg$ord_status_error(), tt.const_pkg$ord_status_passed()) then rnf.time_fact_end else null::timestamp end as stop_place_b_date,
          orv.time_plan_begin as round_time_plan_begin,
          orv.time_plan_end   as round_time_plan_end,
          to_char(rnf.fact_duration, 'HH24:MI') as duration,
          case when orv.order_round_status_id in (tt.const_pkg$ord_status_planned(), tt.const_pkg$ord_status_canceled()) then 0
               when orv.order_round_status_id = tt.const_pkg$ord_status_passed() then gtj.length
               when orv.order_round_status_id = tt.const_pkg$ord_status_performed() then
                    coalesce((select round(max(round_offset / 1000)::numeric, 3)
                                from asd.tf_graph_link tgl
                               where tgl.tr_id = orl.tr_id
                                 and tgl.order_round_id = orv.order_round_id
                                 and tgl.event_time between now() - interval '15 minutes' and now()), 0)
               else coalesce((select round(max(round_offset / 1000)::numeric, 3)
                                from asd.tf_graph_link tgl
                               where tgl.tr_id = orl.tr_id
                                 and tgl.order_round_id = orv.order_round_id
                                 and tgl.event_time between coalesce(orv.time_fact_begin, orv.time_fact_end- interval '60 minutes') and orv.time_fact_end), 0)
          end as run_fact,
          gtj.length as run_plan,
          orr.order_round_id,
          orr.order_round_num,
          '[' || json_build_object('CAPTION', 'Прохождение ОП',
                                   'JUMP', json_build_object('METHOD', 'mnt.stops_of_round',
                                                             'ATTR', '{}',
                                                             'WIN_TITLE', 'Прохождение ОП Маршрут ' || grt.number || ' Рейс ' || rnd.round_code || ' ТС ' || t.garage_num)
                                  ) || ']' as jump_stops_of_round,
          (select cap.short_name 
           from core.tr_model m 
                 join core.tr_capacity cap on cap.tr_capacity_id= m.tr_capacity_id
                 where m.tr_model_id = t.tr_model_id) as tr_capacity_name
     from tt.order_list orl
     join core.tr t on t.tr_id = orl.tr_id
     join core.tr_type typ on typ.tr_type_id = t.tr_type_id
     left join tt.driver drv on drv.driver_id = orl.driver_id
     join tt.order_round orr on orr.order_list_id = orl.order_list_id
     join asd.oper_round_visit orv on orv.order_round_id = orr.order_round_id
     join tt.round rnd on rnd.round_id = orr.round_id
     join tt.timetable_entry tme on tme.timetable_entry_id = rnd.timetable_entry_id
     join gis.route_trajectories gtj on gtj.muid = rnd.route_trajectory_muid
     join gis.route_rounds grr on grr.muid = gtj.route_round_muid
     join gis.route_variants grv on grv.muid = grr.route_variant_muid
     join gis.routes grt on grt.muid = grv.route_muid
     -- join asd.tf_graph_link tgl on orv.packet_id_end = tgl.packet_id
     left join asd.order_round_status st on st.order_round_status_id = orv.order_round_status_id
     left join round_fact rnf on rnf.order_round_id = orr.order_round_id
    where orl.order_date = l_order_date
      and orl.tr_id is not null
      and orl.is_active = 1
      and orr.is_active = 1
      and (l_route_muid is null or grt.muid = l_route_muid)
      and (l_beg_dt is null or orv.time_plan_begin >= l_beg_dt)
      and (l_end_dt is null or orv.time_plan_begin <= l_end_dt)
      and (l_tr_type_id is null or typ.tr_type_id= l_tr_type_id)
      and (l_driver  = '' or upper(drv.driver_last_name) like '%' || upper(l_driver) || '%')
      and (l_grg_num = '' or t.garage_num::text = l_grg_num)
      and (l_status is null or st.order_round_status_id = l_status)
      and (l_tr_id  is null or orl.tr_id = l_tr_id)
      and (
           orl.tr_id in (select adm.account_data_realm_pkg$get_trs_by_tr_type_and_depo(p_account_id))
           or grt.muid in (select route_muid
                           from rts.route2gis
                           where route_id in (select adm.account_data_realm_pkg$get_routes(p_account_id)))
      )
    order by orv.time_plan_begin;
    
  end if;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;


create or replace function mnt.jf_rounds_of_route_pkg$of_dial
  (p_account_id          in numeric,
   p_attr                in text)
returns text
-- ========================================================================
-- Голосовой вызов
-- ========================================================================
as $$ declare
   l_tr_id               bigint := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
   l_driver_id           integer := null;
   l_unit_id             integer;
   l_call_request_id     bigint := null;
   l_redial              boolean := false;
   l_result              text;
begin
   
   select min(unit_id) into l_unit_id
     from core.unit2tr utr
     join core.unit_bnst unt on unt.unit_bnst_id = utr.unit_id
    where tr_id = l_tr_id;
   
   if l_unit_id is null then
      raise exception '<<Не найден блок для связи с ТС>>';
   end if;
   
   l_result := voip.voice_call_pkg$call_to_unit(p_account_id::bigint, l_tr_id, l_unit_id, l_call_request_id, l_driver_id, l_redial);
   return l_result;
end;
$$ language plpgsql volatile security invoker;
