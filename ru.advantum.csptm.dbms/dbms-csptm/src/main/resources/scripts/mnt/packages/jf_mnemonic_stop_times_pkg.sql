create or replace function mnt.jf_mnemonic_stop_times_pkg$of_rows(p_id_account numeric,
    out                                                           p_rows       refcursor,
                                                                  p_attr       text)
    returns refcursor
language plpgsql
as $function$
declare
    l_route_variant_ids int [];
begin
    l_route_variant_ids := jofl.jofl_pkg$extract_tarray(p_attr, 'f_current_route_variant_id', true);

    open p_rows for
    with tr_rounds as (
        select * from
        mnt.jf_mnemonic_stop_times_pkg$get_tr_rounds(p_id_account := p_id_account::bigint, p_route_variant_array := l_route_variant_ids)
    ), last_plan as (
        select distinct on (si2r.stop_item_id, tr.route_variant_id)
            tai.time_begin time_oper,
            -- TODO: поправить надо
            tai.time_begin time_plan,
            si2r.stop_item_id,
            tai.tt_action_id,
            tr.route_variant_id,
            case when (select max(sit.stop_item_type_id)
                       from rts.stop_item2round_type si2rdt
                            join rts.stop_item_type sit on sit.stop_item_type_id = si2rdt.stop_item_type_id
                       where si2rdt.stop_item2round_id = si2r.stop_item2round_id and
                             sit.stop_type_id = rts.stop_type_pkg$checkpoint() and
                             not sit.sign_deleted and
                             not si2rdt.sign_deleted) is not null
              then true
              else false
            end as is_check_point
        from tr_rounds tr
            join ttb.tt_action_item tai on tr.tt_action_id = tai.tt_action_id
            join rts.stop_item2round si2r on tai.stop_item2round_id = si2r.stop_item2round_id
        where not tai.sign_deleted
              and tai.time_begin <= now()
        order by si2r.stop_item_id, tr.route_variant_id, tai.time_begin desc
    ), forecast as (
        select distinct on (lp.stop_item_id, lp.route_variant_id)
            lp.time_oper,
            lp.time_plan,
            lp.stop_item_id,
            lp.is_check_point,
            lp.tt_action_id,
            lp.route_variant_id,
            f.pass_time_forecast
        from last_plan lp
            left join ibrd.forecast f on lp.tt_action_id = f.tt_action_id and lp.tt_action_id = f.tt_action_id
        order by lp.stop_item_id, lp.route_variant_id, f.creation_time desc
    )
    ,
    distinct_rv_ids AS(
        SELECT distinct f.route_variant_id
        from forecast f
    ),
    interval_table AS (
          SELECT extract('minute' FROM date_trunc('minute',(ttb.get_avg_interval_plan_for_route_by_oreder_date(
                                                               p_order_date := date_trunc('day', current_timestamp)::DATE
                                                               ,p_route_id := (select distinct route_id from tr_rounds where tr_rounds.route_variant_id = f.route_variant_id limit 1)
                                                               ,p_move_direction_id := 1)::TIME + INTERVAL '30 second'))) as interval_plan_A,
                extract('minute' FROM date_trunc('minute',(ttb.get_avg_interval_plan_for_route_by_oreder_date(
                                                               p_order_date := date_trunc('day', current_timestamp)::DATE
                                                               ,p_route_id := (select distinct route_id from tr_rounds where tr_rounds.route_variant_id = f.route_variant_id limit 1)
                                                               ,p_move_direction_id := 2)::TIME + INTERVAL '30 second'))) as interval_plan_B,
                extract('minute' FROM date_trunc('minute',(ttb.get_avg_interval_fact_for_route(
                                                               p_order_date := date_trunc('day', current_timestamp)::DATE
                                                               ,p_route_id := (select distinct route_id from tr_rounds where tr_rounds.route_variant_id = f.route_variant_id limit 1)
                                                               ,p_period_start := now()::timestamp - interval '61 min'
                                                               ,p_period_finish := now()::timestamp
                                                               ,p_move_direction_id := 1)::TIME + INTERVAL '30 second'))) as interval_fact_A,
                extract('minute' FROM date_trunc('minute',(ttb.get_avg_interval_fact_for_route(
                                                               p_order_date := date_trunc('day', current_timestamp)::DATE
                                                               ,p_route_id := (select distinct route_id from tr_rounds where tr_rounds.route_variant_id = f.route_variant_id limit 1)
                                                               ,p_period_start := now()::timestamp - interval '61 min'
                                                               ,p_period_finish := now()::timestamp
                                                               ,p_move_direction_id := 2)::TIME + INTERVAL '30 second'))) as interval_fact_B,
                f.route_variant_id
          from distinct_rv_ids f
    )
  select
        f.route_variant_id,
        f.stop_item_id,
        f.time_oper,
        f.time_plan,
        f.pass_time_forecast,
        (select max(taf.time_fact_begin)
         from ttb.tt_action_fact taf
             join rts.stop_item2round si2r on taf.stop_item2round_id = si2r.stop_item2round_id
         where taf.tt_action_id in (select tt_action_id
                                    from tr_rounds)
               and si2r.stop_item_id = f.stop_item_id
        ) max_pass_time,
        it.interval_plan_A, it.interval_plan_B, it.interval_fact_A, it.interval_fact_B,
        f.is_check_point
    from forecast f
    JOIN interval_table it USING (route_variant_id);
end;
$function$;

create or replace function mnt.jf_mnemonic_stop_times_pkg$get_tr_rounds(p_id_account bigint, p_route_variant_array int[]) returns
setof mnt.v_tt_action_attrib
language sql
as $$
    with a as
    (
        SELECT
          vtara.*,
          row_number()
          OVER (PARTITION BY tr_id
            ORDER BY tr_id, lower(action_range) desc, time_fact_begin DESC NULLS LAST) nn
        FROM mnt.v_tt_action_attrib vtara
        WHERE vtara.route_variant_id = any (p_route_variant_array)
          and (tr_id in (select adm.account_data_realm_pkg$get_trs_by_tr_type_and_depo(p_id_account))
               or
               route_id in (select adm.account_data_realm_pkg$get_routes(p_id_account))
               )
          and order_date = now()::date
          and lower(action_range) < now() :: timestamp
    )
    SELECT
      route_id,
      route_variant_id,
      tt_variant_id,
      order_date,
      tt_out_id,
      tr_id,
      tt_out_num,
      tt_action_id,
      driver_id,
      action_type_id,
      action_range,
      round_id,
      round_num,
      code,
      length,
      move_direction_id,
      round_status_id,
      stop_item_a_id,
      stop_item_b_id,
      move_direction_name,
      action_fact_range,
      time_fact_begin,
      pause_stop_item_id
    from a
    where
                nn = 1/* or pause_stop_item_id is not null*/;
$$;