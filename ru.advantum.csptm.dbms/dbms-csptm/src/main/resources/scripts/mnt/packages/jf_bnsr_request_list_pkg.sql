CREATE OR REPLACE FUNCTION mnt.jf_bnsr_request_list_pkg$of_rows(
      p_id_account NUMERIC,
  OUT p_rows       REFCURSOR,
      p_attr       TEXT
)
  RETURNS REFCURSOR AS
$body$
DECLARE
  f_bgn_dt          TIMESTAMP WITHOUT TIME ZONE := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'f_bgn_DT', TRUE),
                                                            current_date - INTERVAL '2 year');
  f_end_dt          TIMESTAMP WITHOUT TIME ZONE := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'f_end_DT', TRUE),
                                                            current_date + INTERVAL '2 year');
  larr_rf_depo_id   BIGINT [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', FALSE);
  l_rf_territory_id core.territory.territory_id%TYPE := coalesce(
      jofl.jofl_pkg$extract_number(p_attr, 'f_territory_id', TRUE), -1);
  f_is_active       SMALLINT := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_is_active_INPLACE_K', TRUE), -1);
  l_rf_tr_id        core.tr.tr_id%TYPE := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_tr_id', TRUE), -1);
  l_rf_unit_id      core.unit.unit_id%TYPE := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_id', TRUE),
                                                       -1);

BEGIN
  OPEN p_rows FOR
  SELECT
    rl.bnsr_request_list_id,
    rl.unit_bnsr_id,
    rl.time_start,
    rl.time_finish,
    rl.is_include_manualy,
    rl.is_exlude_manualy,
    eq.serial_num,
    unit.unit_num,
    eq.equipment_model_name,
    bnsr.channel_num,
    eq.depo_name_full,
    t.code,
    tr.tr_id,
    tr.garage_num AS tr_name
  FROM cmnd.bnsr_request_list rl
    JOIN core.unit_bnsr bnsr ON rl.unit_bnsr_id = bnsr.unit_bnsr_id
    JOIN core.unit unit ON unit.unit_id = bnsr.unit_bnsr_id
    JOIN core.v_equipment eq ON eq.equipment_id = unit.unit_id
    JOIN core.depo2territory d2t ON d2t.depo_id = eq.depo_id
    JOIN core.territory t ON t.territory_id = d2t.territory_id
    LEFT JOIN core.unit2tr u2tr ON rl.unit_bnsr_id = u2tr.unit_id
    LEFT JOIN core.tr tr ON tr.tr_id = u2tr.tr_id
  WHERE ((f_is_active = -1 AND rl.time_start BETWEEN f_bgn_dt AND f_end_dt)
         OR (f_is_active = 1 AND rl.time_finish IS NULL)
         OR (f_is_active = 0 AND rl.time_finish IS NOT NULL)
        )
        AND (d2t.depo_id = ANY (larr_rf_depo_id)/* or l_rf_depo_id = -1*/)
        AND (t.territory_id = l_rf_territory_id OR l_rf_territory_id = -1)
        AND (tr.tr_id = l_rf_tr_id OR l_rf_tr_id = -1)
        AND (rl.unit_bnsr_id = l_rf_unit_id OR l_rf_unit_id = -1);
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;


--------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_bnsr_request_list_pkg$attr_to_rowtype(
  p_attr TEXT
)
  RETURNS cmnd.BNSR_REQUEST_LIST AS
$body$
DECLARE
  l_r cmnd.bnsr_request_list%ROWTYPE;
BEGIN
  l_r.bnsr_request_list_id := jofl.jofl_pkg$extract_number(p_attr, 'bnsr_request_list_id', TRUE);
  l_r.unit_bnsr_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_bnsr_id', TRUE);
  l_r.time_start := jofl.jofl_pkg$extract_date(p_attr, 'time_start', TRUE);
  l_r.time_finish := jofl.jofl_pkg$extract_date(p_attr, 'time_finish', TRUE);
  l_r.is_include_manualy := jofl.jofl_pkg$extract_boolean(p_attr, 'is_include_manualy', TRUE);
  l_r.is_exlude_manualy := jofl.jofl_pkg$extract_boolean(p_attr, 'is_exlude_manualy', TRUE);

  RETURN l_r;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION mnt.jf_bnsr_request_list_pkg$of_delete(
  p_id_account NUMERIC,
  p_attr       TEXT
)
  RETURNS TEXT AS
$body$
DECLARE
  l_r cmnd.bnsr_request_list%ROWTYPE;
BEGIN
  l_r := mnt.jf_bnsr_request_list_pkg$attr_to_rowtype(p_attr);

  PERFORM mnt.bnsr_request_list_pkg$remove(l_r.bnsr_request_list_id, TRUE);

  RETURN NULL;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;


CREATE OR REPLACE FUNCTION mnt.jf_bnsr_request_list_pkg$get_param(p_unit_bnsr_id NUMERIC)
  RETURNS TEXT
AS
$body$
DECLARE
  l_request_param TEXT;
  l_unit_num      TEXT;
  l_channel_num   INT;
BEGIN

  SELECT unit_num
  INTO STRICT l_unit_num
  FROM core.unit
  WHERE unit_id = p_unit_bnsr_id;

  SELECT channel_num
  INTO STRICT l_channel_num
  FROM core.unit_bnsr
  WHERE unit_bnsr_id = p_unit_bnsr_id;


  RETURN json_build_object(
      'NUM', l_unit_num,
      'TYPE', 207,
      'CHANNEL', l_channel_num,
      'ID', 1
  ) :: TEXT;

END;
$body$
LANGUAGE 'plpgsql'
VOLATILE;

--------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_bnsr_request_list_pkg$of_insert(
  p_id_account NUMERIC,
  p_attr       TEXT
)
  RETURNS TEXT AS
$body$
DECLARE
  l_r cmnd.bnsr_request_list%ROWTYPE;
BEGIN
  l_r := mnt.jf_bnsr_request_list_pkg$attr_to_rowtype(p_attr);

  IF mnt.bnsr_request_list_pkg$add(l_r.unit_bnsr_id, l_r.time_start, TRUE) = 1
  THEN
    RETURN jofl.jofl_util$cover_result('error', 'Мониторинг данного блока уже активирован!');
  END IF;

  RETURN NULL;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

--------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_bnsr_request_list_pkg$of_update(
  p_id_account NUMERIC,
  p_attr       TEXT
)
  RETURNS TEXT AS
$body$
DECLARE
  l_r cmnd.bnsr_request_list%ROWTYPE;
BEGIN
  l_r := mnt.jf_bnsr_request_list_pkg$attr_to_rowtype(p_attr);

  /*update cmnd.bnsr_request_list set
         unit_bnsr_id = l_r.unit_bnsr_id,
         time_start = l_r.time_start,
         time_finish = l_r.time_finish,
         is_include_manualy = l_r.is_include_manualy,
         is_exlude_manualy = l_r.is_exlude_manualy
  where
         bnsr_request_list_id = l_r.bnsr_request_list_id;*/

  RETURN NULL;
END;
$body$
LANGUAGE 'plpgsql' VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------