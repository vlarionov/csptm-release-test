CREATE OR REPLACE FUNCTION mnt.jf_depo_by_geometry_pkg$of_rows(IN p_id_account NUMERIC, OUT p_rows REFCURSOR, IN p_attr TEXT)
  RETURNS REFCURSOR
AS
  $BODY$
declare
  l_geom geometry;
begin
  l_geom := ST_GeomFromText(jofl.jofl_pkg$extract_varchar(p_attr, 'GEOM', true));
  open p_rows for
    select p.name as park_name, p.address, tt.name as type_name
    from core.depo as d
        join gis.parks as p on d.park_muid = p.muid
        join core.tr_type as tt on d.tr_type_id = tt.tr_type_id
    where ST_Intersects(d.geom, l_geom)
    group by p.name, p.address, tt.name;
end;
  $BODY$
LANGUAGE plpgsql;