CREATE OR REPLACE FUNCTION mnt.jf_order_round_by_geometry_pkg$of_rows(IN p_id_account NUMERIC, OUT p_rows REFCURSOR, IN p_attr TEXT)
  RETURNS REFCURSOR
AS
  $BODY$
declare
  l_geom geometry;
  l_time_begin timestamp without time zone;
  l_time_end timestamp without time zone;
begin
  l_geom := ST_GeomFromText(jofl.jofl_pkg$extract_varchar(p_attr, 'GEOM', true));
  l_time_begin := jofl.jofl_pkg$extract_date(p_attr, 'DT_BEGIN', true);
  l_time_end := jofl.jofl_pkg$extract_date(p_attr, 'DT_END', true);
    open p_rows for
        select trt.name,
            ro.round_code,
            case when ro.direction_id = 1 then 'прямой' else 'обратный' end as direction,
            r.number,
            st.name as st_a_name,
            st2.name as st_b_name,
            rt."length" as len
        from gis.route_trajectories as rt
            join tt.round as ro on ro.route_trajectory_muid = rt.muid
            join tt.order_round as orr on ro.round_id = orr.round_id
            join tt.order_list as ol on orr.order_list_id = ol.order_list_id
            join core.tr as tr on tr.tr_id = ol.tr_id
            join core.tr_type as trt on trt.tr_type_id = tr.tr_type_id
            join gis.route_rounds as rr on rt.route_round_muid = rr.muid
            join gis.route_variants as rv on rr.route_variant_muid = rv.muid
            join gis.routes as r on r.muid = rv.route_muid
            join gis.stop_places as sp on rr.stop_place_a_muid = sp.muid
            join gis.stop_places as sp2 on rr.stop_place_b_muid = sp2.muid
            join gis.stops as st on sp.stop_muid = st.muid
            join gis.stops as st2 on sp2.stop_muid = st2.muid
        where ST_Intersects(rt.geom, l_geom)
            and rt.sign_deleted != 1
            and rv.sign_deleted != 1
            and rr.sign_deleted != 1
            and (rv.end_date is null or rv.end_date >= l_time_end)
        group by trt.name, ro.round_code, ro.direction_id, r.number, st.name, st2.name, rt."length"
        order by r.number, ro.round_code, direction;
    end;
  $BODY$
LANGUAGE plpgsql;
