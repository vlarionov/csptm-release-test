﻿
create or replace function mnt.info_pkg$get_tr_end_time_fact
  (p_order_date     in timestamp,
   p_tr_id          in bigint)
returns timestamp
-- ========================================================================
-- Фактическое время окончания работы ТС
-- ========================================================================
as $$ declare
   l_result                  timestamp := null;
begin
   select case when rov.order_round_status_id in (tt.const_pkg$ord_status_error(),
                                                  tt.const_pkg$ord_status_passed())
               then (coalesce((select max(orf.time_fact) 
                                 from tt.order_fact orf
                                where orf.sign_deleted = 0
                                  and orf.order_round_id = rov.order_round_id
                              ),
                              (select max(orf.time_fact)
                                 from mnt.v_round_oper_visit vov
                                 join tt.order_fact orf on orf.order_round_id = vov.order_round_id
                                where orf.sign_deleted = 0
                                  and vov.order_date = p_order_date
                                  and vov.tr_id = p_tr_id
                                  and vov.order_round_status_id <> tt.const_pkg$ord_status_canceled()
                              ))
                    )
               else null
          end
     into l_result
     from mnt.v_round_oper_visit rov
    where rov.order_date = p_order_date
      and rov.tr_id = p_tr_id
      and rov.order_round_status_id <> tt.const_pkg$ord_status_canceled()
      and rov.time_plan_end = (select max(time_plan_end) 
                                 from mnt.v_round_oper_visit
                                where order_date = p_order_date
                                  and tr_id = p_tr_id
                                  and order_round_status_id <> tt.const_pkg$ord_status_canceled())
    limit 1;
   
   return l_result;
end;
$$ language plpgsql security definer;


create or replace function mnt.info_pkg$get_driver_end_time_fact
  (p_order_date     in timestamp,
   p_driver_id      in bigint)
returns timestamp
-- ========================================================================
-- Фактическое время окончания работы водителя
-- ========================================================================
as $$ declare
   l_result                  timestamp := null;
begin
   select case when rov.order_round_status_id in (tt.const_pkg$ord_status_error(),
                                                  tt.const_pkg$ord_status_passed())
               then (coalesce((select max(orf.time_fact) 
                                 from tt.order_fact orf
                                where orf.sign_deleted = 0
                                  and orf.order_round_id = rov.order_round_id
                              ),
                              (select max(orf.time_fact)
                                 from mnt.v_round_oper_visit vov
                                 join tt.order_fact orf on orf.order_round_id = vov.order_round_id
                                where orf.sign_deleted = 0
                                  and vov.order_date = p_order_date
                                  and vov.driver_id = p_driver_id
                                  and vov.order_round_status_id <> tt.const_pkg$ord_status_canceled()
                              ))
                    )
               else null
          end
     into l_result
     from mnt.v_round_oper_visit rov
    where rov.order_date = p_order_date
      and rov.driver_id = p_driver_id
      and rov.order_round_status_id <> tt.const_pkg$ord_status_canceled()
      and rov.time_plan_end = (select max(time_plan_end) 
                                 from mnt.v_round_oper_visit
                                where order_date = p_order_date
                                  and driver_id = p_driver_id
                                  and order_round_status_id <> tt.const_pkg$ord_status_canceled())
    limit 1;
   
   return l_result;
end;
$$ language plpgsql security definer;


create or replace function mnt.info_pkg$get_route_end_time_fact
  (p_order_date     in timestamp,
   p_route_muid     in bigint)
returns timestamp
-- ========================================================================
-- Фактическое время окончания работы маршрута
-- ========================================================================
as $$ declare
   l_result         timestamp := null;
begin
   select case when rov.order_round_status_id in (tt.const_pkg$ord_status_error(),
                                                  tt.const_pkg$ord_status_passed())
               then (coalesce((select max(orf.time_fact) 
                                 from tt.order_fact orf
                                where orf.sign_deleted = 0
                                  and orf.order_round_id = rov.order_round_id
                              ),
                              (select max(orf.time_fact)
                                 from mnt.v_route_oper_visit vov
                                 join tt.order_fact orf on orf.order_round_id = vov.order_round_id
                                where orf.sign_deleted = 0
                                  and vov.order_date = p_order_date
                                  and vov.route_muid = p_route_muid
                                  and vov.order_round_status_id <> tt.const_pkg$ord_status_canceled()
                              ))
                    )
               else null
          end
     into l_result
     from mnt.v_route_oper_visit rov
    where rov.order_date = p_order_date
      and rov.route_muid = p_route_muid
      and rov.order_round_status_id <> tt.const_pkg$ord_status_canceled()
      and rov.time_plan_end = (select max(time_plan_end) from mnt.v_route_oper_visit
                                where order_date = p_order_date
                                  and route_muid = p_route_muid
                                  and order_round_status_id <> tt.const_pkg$ord_status_canceled())
    limit 1;

   return l_result;
end;
$$ language plpgsql security definer;


create or replace function mnt.info_pkg$get_route_end_time_fact2
    (p_order_date     in timestamp,
     p_route_muid     in bigint)
    returns timestamp
    -- ========================================================================
    -- Фактическое время окончания работы маршрута
    -- ========================================================================
as $$ declare
    l_result         timestamp := null;
begin
    with rounds as (
        select
            rov.order_round_id,
            rov.order_round_status_id,
            rov.time_plan_end,
        rov.time_plan_begin
        from mnt.v_route_oper_visit rov
        where rov.order_date = p_order_date
              and rov.route_muid = p_route_muid
              and rov.order_round_status_id <> tt.const_pkg$ord_status_canceled()
        order by time_plan_end desc
        limit 1
    )
    select case when order_round_status_id in (tt.const_pkg$ord_status_error(),
                                               tt.const_pkg$ord_status_passed())
        then (coalesce(time_plan_begin,
                       time_plan_end)
        )
           else null
           end
    into l_result
    from rounds r
    limit 1;

    return l_result;
end;
$$ language plpgsql security definer;


create or replace function mnt.info_pkg$get_route_end_time_fact3
  (p_order_date     in timestamp,
   p_route_muid     in bigint)
returns timestamp
-- ========================================================================
-- Фактическое время окончания работы маршрута
-- ========================================================================
as $$ declare
   l_result         timestamp := null;
begin
   select case when rov.order_round_status_id in (tt.const_pkg$ord_status_error(),
                                                  tt.const_pkg$ord_status_passed())
               then (coalesce((select max(orf.time_fact) 
                                 from tt.order_fact orf
                                where orf.sign_deleted = 0
                                  and orf.order_round_id = rov.order_round_id
                              ),
                              (select max(orf.time_fact)
                                 from mnt.v_route_oper_visit vov
                                 join tt.order_fact orf on orf.order_round_id = vov.order_round_id
                                where orf.sign_deleted = 0
                                  and vov.order_date = p_order_date
                                  and vov.route_muid = p_route_muid
                                  and vov.order_round_status_id <> tt.const_pkg$ord_status_canceled()
                              ))
                    )
               else null
          end
     into l_result
     from mnt.v_route_oper_visit rov
    where rov.order_date = p_order_date
      and rov.route_muid = p_route_muid
      and rov.order_round_status_id <> tt.const_pkg$ord_status_canceled()
    order by time_plan_end desc
    limit 1;
   
   return l_result;
end;
$$ language plpgsql security definer;
