CREATE OR REPLACE FUNCTION mnt.jf_event_pkg$of_rows(in p_id_account numeric, out p_rows refcursor, in p_attr text)
  RETURNS refcursor
AS
$BODY$
declare
  l_begin_dt     timestamp without time zone         := jofl.jofl_pkg$extract_date(p_attr,    'F_BEGIN_DT', true);
  l_end_dt       timestamp without time zone         := jofl.jofl_pkg$extract_date(p_attr,    'F_END_DT',   true);
  l_gtg_num int := jofl.jofl_pkg$extract_number(p_attr, 'F_GRG_NOMER_TEXT', true)::INT;
  l_event_status SMALLINT := jofl.jofl_pkg$extract_number(p_attr, 'f_event_status_id', true)::SMALLINT;
  l_incident_type_id SMALLINT := jofl.jofl_pkg$extract_number(p_attr, 'f_incident_type_id', true)::SMALLINT;


begin
  open p_rows for
select
  incident.incident_id as event_id,
  incident.time_start as event_time,
  incident.tr_id as tr_id,
  incident.unit_id as unit_id,
  incident.incident_type_id as event_type_id,
  incident_params ->> 'factVal' fact_value,
  incident_params ->> 'deviationVal' as deviation_value,
  incident_params -> 'pointStart' -> 'lon' lon,
  incident_params -> 'pointStart' -> 'lat' lat,
  incident.incident_status_id event_status_id,
  tr.garage_num,
  incident_type.incident_type_name event_type,
  tr_type.name tr_type_name,
  incident_initiator.incident_initiator_name event_source_name,
  incident_status.incident_status_name event_status_name,
  incident.time_finish confirmation_time,
  incident_params::text,
  ttb.jf_incident_reason_pkg$get_name((incident_params ->> 'incidentReason')::integer) as incident_reason,
  (select name
   from rts.stop
     join rts.stop_location on stop.stop_id = stop_location.stop_id
     join rts.stop_item on stop_location.stop_location_id = stop_item.stop_location_id
   where stop_item_id = (incident_params -> 'stopItemList' ->> 0)::int
  ) stop_name,
  (select route_num
   from ttb.tt_variant
        join rts.route_variant on tt_variant.route_variant_id = route_variant.route_variant_id
        join rts.route on route.route_id = route_variant.route_id
   where tt_variant.tt_variant_id = (incident_params -> 'ttVariantList' ->> 0)::int
  ) route_num
from ttb.incident
  join ttb.incident_type on (incident_type.incident_type_id = incident.incident_type_id)
  join ttb.incident_initiator on (incident.incident_initiator_id = incident_initiator.incident_initiator_id)
  join ttb.incident_status on (incident_status.incident_status_id = incident.incident_status_id)
  left join core.tr on (incident.tr_id = tr.tr_id)
  left join core.tr_type on (tr_type.tr_type_id = tr.tr_type_id)
where incident.time_start between l_begin_dt and l_end_dt + INTERVAL '1 day'
      and (tr.garage_num = l_gtg_num or l_gtg_num is null)
      and (incident.incident_status_id = l_event_status or l_event_status is null)
      and (incident.incident_type_id = l_incident_type_id or l_incident_type_id is null)
      and (incident.tr_id is null or exists (select 1
                    from adm.account_data_realm_pkg$get_trs_on_periods_by_all(p_id_account, tsrange(l_begin_dt, l_end_dt)) tr_relaim
                    where (tr_relaim.tr_id = incident.tr_id and (tr_relaim.period @> incident.time_start or tr_relaim.period is null)))
          )
                    limit 1000;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION mnt.jf_monitoring_event_pkg$cn_status_new()
  RETURNS SMALLINT AS
  'select 20::SMALLINT;'
LANGUAGE SQL IMMUTABLE;


CREATE OR REPLACE FUNCTION mnt.jf_monitoring_event_pkg$cn_status_prosessed()
  RETURNS SMALLINT AS
  'select 80::SMALLINT;'
LANGUAGE SQL IMMUTABLE;