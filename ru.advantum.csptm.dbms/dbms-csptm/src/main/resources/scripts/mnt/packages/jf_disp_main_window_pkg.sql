CREATE OR REPLACE FUNCTION mnt.jf_disp_main_window_pkg$of_rows (
      p_id_account numeric,
  out p_rows refcursor,
      p_attr text
)
  RETURNS refcursor AS
$body$
declare
  lts_request_datetime timestamp without time zone := current_timestamp;
  ld_order_date date;
begin
  ld_order_date := date_trunc('day', lts_request_datetime);

  open p_rows for
  with main_query as (
      select
         route_variant.route_id as route_id
        ,tt_out.tr_id as tr_id
        ,tt_out.tt_out_num
        ,tt_out.tt_out_id
        ,tt_action.driver_id as driver_id
        ,tt_action_round.tt_action_id as tt_action_id
        ,tt_action_round.round_status_id as round_status_id
        ,dr_shift.dr_shift_num as shift
        ,coalesce(round.code, action_type.action_type_code) as round_code
        ,round.move_direction_id
        ,round.round_id as round_id
        ,tt_action.action_range
        ,lead(tt_action.action_range) over (PARTITION BY tt_out.tt_out_id order by tt_out.tt_out_id, dt_action asc) action_range_next
        ,tr.garage_num
        ,vm_action_type.is_production_round is_production_round
        ,

         (
           select
             string_agg(
                 distinct ttb.jf_incident_reason_pkg$get_name_only_level((incident.incident_params ->> 'incidentReason')::int, 3), ';'
             )
           from ttb.incident
           where
             incident.time_start between
             (lower(tt_action.action_range)::date - interval '1 day')
             and
             (lower(tt_action.action_range)::date + interval '1 day')
             and now() between incident.time_start and incident.time_finish
             and incident.incident_type_id = ttb.jf_incident_pkg$cn_manual()
             and (
               incident_params -> 'trList' @> jsonb_build_array(tt_out.tr_id)
             )
         )  as incidents
        ,not (
        select tt_out_parent.tt_out_num = tt_out.tt_out_num
        from ttb.tt_out tt_out_parent
        where tt_out_parent.tt_out_id = tt_out.parent_tt_out_id
      ) is_manual_tt_out
      from ttb.tt_variant
        join rts.route_variant on route_variant.route_variant_id = tt_variant.route_variant_id and not tt_variant.sign_deleted and tt_variant.order_date = ld_order_date
        join ttb.tt_out on tt_variant.tt_variant_id = tt_out.tt_variant_id and not tt_out.sign_deleted
        join ttb.tt_action on tt_out.tt_out_id = tt_action.tt_out_id and not tt_action.sign_deleted
        join ttb.tt_action_item on tt_action_item.tt_action_id = tt_action.tt_action_id and not tt_action_item.sign_deleted
        join ttb.dr_shift on dr_shift.dr_shift_id = tt_action_item.dr_shift_id
        left join ttb.tt_action_round on tt_action.tt_action_id = tt_action_round.tt_action_id
        left join rts.round on round.round_id = tt_action_round.round_id
        join core.tr tr on tr.tr_id = tt_out.tr_id
        join ttb.action_type on action_type.action_type_id = tt_action.action_type_id
        join ttb.vm_action_type on tt_action.action_type_id = vm_action_type.action_type_id
      where  route_variant.route_id in (select adm.account_data_realm_pkg$get_routes(p_id_account))
      group by
        tt_out.tr_id
        ,tt_action_round.tt_action_id
        ,tt_action.driver_id
        ,route_variant.route_id
        ,round.round_id
        ,tt_action.dt_action
        ,rts.round.length
        ,tt_action_round.round_status_id
        ,tt_action.action_dur
        ,dr_shift.dr_shift_num
        ,action_type.action_type_code
        ,tt_out_num
        ,round.move_direction_id
        ,tt_out.tt_out_id
        ,tt_action.action_range
        ,tr.garage_num
        ,coalesce(round.code, action_type.action_type_code)
        ,tt_action.action_type_id
        ,action_type.parent_type_id
        ,vm_action_type.is_production_round
        ,tt_action_item.tt_action_id
  )
    ,outs as (
      select mq.tr_id
        ,case when lts_request_datetime
      between
      min(lower(mq.action_range)) over (partition by mq.tr_id)
      and
      max(upper(mq.action_range_next)) over (partition by mq.tr_id)
        then true
         else false
         end is_on_route
        ,mq.garage_num
        ,case
         when lower(mq.action_range_next) > ld_order_date
           then lower(mq.action_range_next)
         else null
         end time_plan_begin_next
        ,mq.route_id
        ,mq.tt_out_num timetable_entry_num
        ,mq.shift
        ,mq.round_code
        ,mq.move_direction_id AS direction
        ,mq.action_range_next
        ,mq.action_range
        ,row_number() over (partition by mq.tt_out_id order by
        mq.action_range @> lts_request_datetime desc
        ,abs(extract(EPOCH FROM lts_request_datetime - upper(mq.action_range))) asc ) nnn
        ,mq.incidents
        ,is_manual_tt_out
      from main_query mq
  )
    ,alarm as (
      select tr_id from ttb.incident ev
        JOIN ttb.incident_type it ON it.incident_type_id = ev.incident_type_id
      where ev.time_start between ld_order_date::timestamp and ld_order_date::timestamp + interval '1 day'
            AND ev.incident_status_id in (mnt.jf_monitoring_event_pkg$cn_status_new() )-- статус новый
            AND is_alarm = TRUE
      GROUP BY tr_id)
    ,diag_info as (
      select
        tr_id
        ,case
         when a.incident_type_id = ttb.jf_incident_pkg$cn_comming_off()  then 2
         when a.incident_type_id = ANY(kdbo.jf_unit_working_period_pkg$get_diag_event_type_array()) then 1
         else 0
         end out_of_service_flag
      from (
             SELECT
               tr_id,
               incident_type_id,
               row_number()
               OVER (PARTITION BY tr_id
                 ORDER BY tr_id, incident.time_start desc) nnn
             FROM ttb.incident
             WHERE incident_type_id = ANY (kdbo.jf_unit_working_period_pkg$get_diag_event_type_array())
                   AND incident_type_id IN (kdbo.jf_unit_working_period_pkg$get_diag_ok(), ttb.jf_incident_pkg$cn_comming_off())
                   AND time_finish IS NULL
                   AND incident.time_start BETWEEN now() - INTERVAL '1 day' AND now()
           )a
      where nnn = 1
  )
    ,a_incidents as
  (
      select
        string_agg(
            ttb.jf_incident_reason_pkg$get_name_only_level((incident.incident_params ->> 'incidentReason')::int, 3), ';'
        )
      from ttb.incident  join outs on incident_params -> 'trList' @> jsonb_build_array(outs.tr_id)
      where
        incident.time_start between
        (now()::date - interval '1 day')
        and
        (now()::date + interval '1 day')
  and now() between incident.time_start and incident.time_finish
  and incident.incident_type_id = ttb.jf_incident_pkg$cn_manual()
  and (
  incident_params -> 'trList' @> jsonb_build_array(outs.tr_id)
  )
  )
  select outs.tr_id
    ,is_on_route
    ,garage_num
    ,(select lower(main_query.action_range)
      from main_query
      where is_production_round
            and lower(main_query.action_range) > lower(outs.action_range)
            and main_query.tr_id = outs.tr_id
      order by main_query.action_range
      limit 1) as time_plan_begin_next
    ,route_id
    ,timetable_entry_num
    ,shift
    ,round_code
    ,(select main_query.round_code
      from main_query
      where lower(main_query.action_range) > lower(outs.action_range)
            and main_query.tr_id = outs.tr_id
      order by main_query.action_range
      limit 1) as round_code_next
    ,direction
    ,(select main_query.move_direction_id
      from main_query
      where is_production_round
            and main_query.action_range > outs.action_range
            and main_query.tr_id = outs.tr_id
      order by main_query.action_range
      limit 1) as direction_next
    ,diag_info.out_of_service_flag
    ,coalesce(al.tr_id, 0)::integer::boolean is_alarm
    ,case when is_manual_tt_out
    then timetable_entry_num
     else substr(timetable_entry_num::text, 2)::int
     end  as tt_tr_order
    ,(select event_time
      from core.traffic t
      where t.tr_id = outs.tr_id
            AND t.event_time > (now() - INTERVAL '24 hour')
            AND NOT t.is_hist_data
            AND t.location_valid
      order by t.event_time desc LIMIT 1) as last_tf_event_time
    ,incidents as incidents_name
    ,ARRAY[15] && ARRAY[1,7,4,2,6]
  from outs
    left join diag_info on outs.tr_id = diag_info.tr_id
    left join alarm al on al.tr_id = outs.tr_id
  where nnn = 1
  ;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;