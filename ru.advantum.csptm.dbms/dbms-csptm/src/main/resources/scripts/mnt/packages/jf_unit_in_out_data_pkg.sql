CREATE OR REPLACE FUNCTION mnt.jf_unit_in_out_data_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$

declare
  l_packet_id	bigint  := jofl.jofl_pkg$extract_number(p_attr, 'f_packet_id', false);
  l_event_time  timestamp := jofl.jofl_pkg$extract_date(p_attr, 'f_event_time', false);
begin
 open p_rows for
      select
        isd.sensor_id,
        val,
        name_full,
        sensor_num
      from snsr.integer_sensor_data isd
           join core.sensor2unit s2u on s2u.sensor_id = isd.sensor_id
           join core.equipment e on isd.sensor_id = e.equipment_id
           join core.equipment_model em on em.equipment_model_id = e.equipment_model_id
           join core.equipment_type et on et.equipment_type_id = em.equipment_type_id
      where   isd.packet_id = l_packet_id
              and isd.event_time = l_event_time
      union all
      select
        bsd.sensor_id,
        val::int,
        name_full,
        sensor_num
      from snsr.binary_sensor_data bsd
           join core.sensor2unit s2u on s2u.sensor_id = bsd.sensor_id
           join core.equipment e on bsd.sensor_id = e.equipment_id
           join core.equipment_model em on em.equipment_model_id = e.equipment_model_id
           join core.equipment_type et on et.equipment_type_id = em.equipment_type_id
      where   bsd.packet_id = l_packet_id
              and bsd.event_time = l_event_time

              ;
end;

$$;