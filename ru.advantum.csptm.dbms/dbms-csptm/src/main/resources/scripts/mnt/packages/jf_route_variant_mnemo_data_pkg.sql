create or replace function mnt.jf_route_variant_mnemo_data_pkg$of_rows(in  p_id_account numeric,
                                                                       out p_rows       refcursor,
                                                                       in  p_attr       text)
    returns refcursor as
$BODY$
declare
    l_route_variant_ids int [];
begin
    l_route_variant_ids := jofl.jofl_pkg$extract_tarray(p_attr, 'f_current_route_variant_id', true);

    open p_rows for
    select
        rv.route_variant_id,
        r.route_num,
        rnd.round_id,
        rnd.action_type_id,
        rnd.round_num,
        rnd.code round_code,
        rnd.move_direction_id,
        si2r.order_num,
        si2r.length_sector,
        si.stop_item_id,
        s.name stop_name,
        ttb.action_type_pkg$is_technical_round(rnd.action_type_id) round_is_technical
    from rts.route_variant rv
        join rts.route r on r.route_id = rv.route_id
        join rts.round rnd on rv.route_variant_id = rnd.route_variant_id
        join rts.stop_item2round si2r on rnd.round_id = si2r.round_id
        join rts.stop_item si on si2r.stop_item_id = si.stop_item_id
        join rts.stop_location sl on si.stop_location_id = sl.stop_location_id
        join rts.stop s on sl.stop_id = s.stop_id
    where not rv.sign_deleted
          and not r.sign_deleted
          and not rnd.sign_deleted
          and not si2r.sign_deleted
          and not si.sign_deleted
          and not sl.sign_deleted
          and not s.sign_deleted
          and r.carrier_id = core.carrier_pkg$mgt()
          and rv.route_variant_id = any (l_route_variant_ids)
          and r.route_id in (select adm.account_data_realm_pkg$get_routes(p_id_account))
    order by rnd.round_id, si2r.order_num;
end;
$BODY$
language plpgsql
volatile
cost 100;