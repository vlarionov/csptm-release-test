CREATE OR REPLACE FUNCTION mnt.jf_incoming_message_detail_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
  f_bgn_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'order_date', true);
  f_end_dt		timestamp without time zone;
  l_attr text;
begin
  /* дополняем недостающими фильтрами ...  */
  l_attr := p_attr;
  f_end_dt := f_bgn_dt + interval '86399 seconds';
  select l_attr::jsonb || ('{"f_bgn_DT": "'||f_bgn_dt::text||'"}')::jsonb into l_attr;
  select l_attr::jsonb || ('{"f_end_DT": "'||f_end_dt::text||'"}')::jsonb into l_attr;
  /* ... вызываем общую фнкцию - из метода of_rows основного окна и из of_rows детали */
  select mnt.jf_incoming_message_pkg$get_rows (p_id_account, l_attr) into p_rows;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;