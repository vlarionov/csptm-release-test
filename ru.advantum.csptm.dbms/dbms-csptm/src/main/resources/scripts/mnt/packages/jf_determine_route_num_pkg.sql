create or replace function mnt.jf_determine_route_num_pkg$of_rows
    (p_id_account in  numeric,
     p_rows       out refcursor,
     p_attr       in  text)
    returns refcursor
as $$
declare
    l_tr_id      core.tr.tr_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
    l_tr_type_id core.tr.tr_id%type := jofl.jofl_pkg$extract_number(p_attr, 'tr_type_id', true);
    l_interval   integer :=asd.get_constant('MNT_SEARCH_INTERVAL');
begin
    open p_rows for
    with tr_on_sec as (
        select
            st_astext(st_union(lasttraf.pointcoords)),
            gs.graph_section_id as graph_section_id,
            lasttraf.tr_id      as tr_id
        from rts.graph_section gs,
            (
                select
                    ST_MakePoint(tr.lon, tr.lat) as pointcoords,
                    tr.tr_id
                from core.traffic as tr
                where tr.tr_id = l_tr_id and tr.event_time between (now() - l_interval * '1 sec' :: interval) and now()
                group by tr.tr_id, pointcoords
            ) as lasttraf
        where gs.sign_deleted = false and ST_DWithin(lasttraf.pointcoords, gs.geom, 0.00045)
        group by graph_section_id, tr_id
    )
    select
        rroute.route_num,
        tt.name,
        r.code,
        r.wkt_geom,
        '[' || json_build_object('CAPTION', 'На карте',
                                 'LINK', json_build_object('URL',
                                                           '/app.html?m=modules/DetermineRouteNumOnMapCtrl&attr={"p_tr_id":"' || l_tr_id ||
                                                           '","rv_muid":"' || r.route_variant_id ||
                                                           '","rr_code":"' || r.code ||
                                                           '","p_interval":"' || l_interval || '"}'
                                 )) :: text || ']' as link_on_map,
        count(rroute.route_num)                        as count_cross,
        case when r.move_direction_id = 1 :: smallint
            then 'Прямое'
        else 'Обратное' end                        as direct
    from tr_on_sec ts
        join rts.graph_sec2round gs2r on gs2r.graph_section_id = ts.graph_section_id
        join rts.round r on gs2r.round_id = r.round_id
        join rts.route_variant rv on r.route_variant_id = rv.route_variant_id
        join rts.route rroute on rv.route_variant_id = rroute.current_route_variant_id
        join core.tr_type tt on rroute.tr_type_id = tt.tr_type_id
    where l_tr_type_id = tt.tr_type_id and ttb.action_type_pkg$is_production_round(r.action_type_id) and
          rroute.carrier_id = core.carrier_pkg$mgt()
    group by rroute.route_num, tt.name, r.wkt_geom, r.sign_deleted, rroute.sign_deleted, r.code, tt.short_name, r.route_variant_id, r.move_direction_id
    order by count_cross desc
    limit 5;
end;
$$ language plpgsql;