create or replace function mnt.jf_mnemonic_trs_pkg$of_rows(p_id_account numeric, out p_rows refcursor, p_attr text)
  returns refcursor
language plpgsql
as $function$
declare
  l_route_variant_ids int [];
  l_interval interval :=  interval '61 min';
begin
  l_route_variant_ids := jofl.jofl_pkg$extract_tarray(p_attr, 'f_current_route_variant_id', true);

  open p_rows for
  with tr_rounds as (
      select
        vtara.*,
        tsrange(least(lower(vtara.action_range), lower(vtara.action_fact_range)),
                greatest(upper(vtara.action_range), upper(vtara.action_fact_range))) lookup_range,
        action_type_name,
        parent_type_id
      from
            mnt.jf_mnemonic_stop_times_pkg$get_tr_rounds(p_id_account := p_id_account::bigint, p_route_variant_array := l_route_variant_ids) vtara
        join ttb.action_type on action_type.action_type_id = vtara.action_type_id
  ),tr_rounds_agg as (
    select
    tr_rounds.route_id,
    tr_rounds.move_direction_id,
    core.interval2minutes(
          ttb.get_avg_interval_fact_for_route(
              p_order_date := now()::date
              ,p_route_id := tr_rounds.route_id
              ,p_period_start := now()::timestamp - l_interval
              ,p_period_finish := now()::timestamp
              ,p_move_direction_id := tr_rounds.move_direction_id)
              ) as route_interval_avg_current
    from tr_rounds
    group by tr_rounds.route_id, tr_rounds.move_direction_id
  ), cur_offset as (
      select
        tr.tr_id,
        tr.tt_action_id,
        (select tgl.round_offset
         from asd.tf_graph_link_ttb tgl
         where tgl.tr_id = tr.tr_id
               and tgl.tt_action_id = tr.tt_action_id
               and tgl.event_time <@ lookup_range
         order by event_time desc
         limit 1) round_offset,
        (exists(select 1
                from asd.tf_graph_link_ttb tgl
                where tgl.tr_id = tr.tr_id
                      and
                      tgl.event_time between now() - interval '5 minutes' and now() +
                                                                              interval '1 minutes')) in_route
      from tr_rounds tr
  )
  ,last_stops as
  (
      select    tt_action_fact.tt_action_id,
                tt_action_fact.time_fact_begin,
                stop_item2round.stop_item_id,
                row_number() over (partition by tt_action_fact.tt_action_id order by tt_action_fact.tt_action_id, tt_action_fact.time_fact_begin desc) nn_order
      from tr_rounds
           join ttb.tt_action_fact on ttb.tt_action_fact.tt_action_id = tr_rounds.tt_action_id
           join rts.stop_item2round on tt_action_fact.stop_item2round_id = stop_item2round.stop_item2round_id
      where tt_action_fact.time_fact_begin is not null
      order by tt_action_fact.time_fact_begin
  )
  ,drivers_and_shifts as (
      select tr_rounds.tt_action_id
             ,dr_shift.dr_shift_num
             ,tt_action_item.driver_id
      from tr_rounds
            join ttb.tt_action_item on tt_action_item.tt_action_id = tr_rounds.tt_action_id
            join ttb.dr_shift on dr_shift.dr_shift_id = tt_action_item.dr_shift_id
      group by tr_rounds.tt_action_id
               ,dr_shift.dr_shift_num
               ,tt_action_item.driver_id
  )
  select
    tr_rounds.move_direction_id,
    core.jf_driver_pkg$get_text(p_driver_id := drivers_and_shifts.driver_id) driver,
    tr_rounds.tt_out_num,
    round(co.round_offset) round_offset,
    tr_rounds.tr_id,
    tr_rounds.tt_action_id,
    tr_rounds.route_id,
    tr_rounds.round_num,
    tr_rounds.route_variant_id,
    coalesce(tr_rounds.code, tr_rounds.action_type_name) round_code,
    r.route_num,
    t.garage_num,
    t.licence,
    false is_switched,
    case
        when co.in_route
            then 'в рейсе'
        else 'нет данных'
        end as status,
    tr_rounds_agg.route_interval_avg_current as route_interval_avg_current,
    round((lead(co.round_offset)
           over (
             partition by tr_rounds.route_id, tr_rounds.move_direction_id
             order by co.round_offset )
           - co.round_offset) / 500) as interval,
    drivers_and_shifts.dr_shift_num as shift,
    parent_type_id as action_type,
    last_stops.time_fact_begin,
    rts.jf_stop_item_pkg$get_text(last_stops.stop_item_id) last_stop_name,
    case when tr_rounds.action_type_id in (22) then '#FF7F50'
        else '#FFFFFF'
    end color,
    pause_stop_item_id
  from tr_rounds
    left join cur_offset co on tr_rounds.tt_action_id = co.tt_action_id
    left join drivers_and_shifts on tr_rounds.tt_action_id = drivers_and_shifts.tt_action_id
    left join core.tr t on tr_rounds.tr_id = t.tr_id
    left join rts.route r on tr_rounds.route_id = r.route_id
    left join last_stops on last_stops.tt_action_id = tr_rounds.tt_action_id and nn_order = 1
    left join tr_rounds_agg on tr_rounds_agg.route_id = tr_rounds.route_id and tr_rounds_agg.move_direction_id = tr_rounds.move_direction_id;
end;
$function$;


