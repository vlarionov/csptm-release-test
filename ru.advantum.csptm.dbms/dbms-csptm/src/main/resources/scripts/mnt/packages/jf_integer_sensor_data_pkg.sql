CREATE OR REPLACE FUNCTION mnt.jf_integer_sensor_data_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$

declare
  f_bgn_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_bgn_DT', true);
  f_end_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_end_DT', true);
  l_rf_tr_id	 core.tr.tr_id%type 		:= jofl.jofl_pkg$extract_number(p_attr, 'f_tr_id', true);
  l_rf_unit_id	 core.unit.unit_id%type		:= jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_id', true);
  l_rf_sensor_id core.sensor.sensor_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_sensor_id', true);
begin
 open p_rows for
      select
        /*packet_id,*/
        event_time,
        /*unit_id,
        tr_id,
        sensor_id,
        equipment_type_id,*/
        val
      from snsr.integer_sensor_data isd
      where   isd.event_time between f_bgn_dt and  f_end_dt
              and tr_id = l_rf_tr_id
              and unit_id = l_rf_unit_id
              and sensor_id = l_rf_sensor_id      ;
end;

$$;