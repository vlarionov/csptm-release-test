﻿create or replace function mnt.jf_trs_stops_interval_pkg$of_rows
  (p_account_id               in numeric,
   p_attr                     in text,
   p_rows                     out refcursor)
returns refcursor
as $$ declare
   l_begin_date   timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'F_BEG_DT', true));
   l_end_date     timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'F_END_DT', true));
   l_order_date   timestamp := date_trunc('day', l_begin_date);
   l_stop_list    bigint[]  := jofl.jofl_pkg$extract_tarray(p_attr, 'F_STOP_PLACE_MUID', true);
begin
   open p_rows for
   with stop_time_plan as (
   select oro.stop_place_muid, grv.route_muid, grt.transport_kind_muid,
          oro.time_oper as time_plan,
          lag(oro.time_oper, 1, oro.time_oper) over (partition by oro.stop_place_muid, grv.route_muid order by oro.time_oper) as prev_plan_route,
          lag(oro.time_oper, 1, oro.time_oper) over (partition by oro.stop_place_muid, grt.transport_kind_muid order by oro.time_oper) as prev_plan_kind,
          lag(oro.time_oper, 1, oro.time_oper) over (partition by oro.stop_place_muid order by oro.time_oper) as prev_plan_any
     from tt.order_list orl
     join tt.order_round orr on orr.order_list_id = orl.order_list_id
     join tt.order_oper oro on oro.order_round_id = orr.order_round_id
     join tt.round rnd on rnd.round_id = orr.round_id
     join gis.route_trajectories gtj on gtj.muid = rnd.route_trajectory_muid
     join gis.route_rounds grr on grr.muid = gtj.route_round_muid
     join gis.route_variants grv on grv.muid = grr.route_variant_muid
     join gis.routes grt on grt.muid = grv.route_muid
    where oro.time_oper between l_begin_date and l_end_date
      and oro.stop_place_muid = any(l_stop_list)
      and orl.sign_deleted = 0
      and orl.is_active = 1
      and orr.is_active = 1
      and oro.sign_deleted = 0
      and oro.is_active = 1
   ),
   stop_time_fact as (
   select orf.stop_place_muid, grv.route_muid, grt.transport_kind_muid,
          orf.time_fact,
          lag(orf.time_fact, 1, orf.time_fact) over (partition by orf.stop_place_muid, grv.route_muid order by orf.time_fact) as prev_fact_route,
          lag(orf.time_fact, 1, orf.time_fact) over (partition by orf.stop_place_muid, grt.transport_kind_muid order by orf.time_fact) as prev_fact_kind,
          lag(orf.time_fact, 1, orf.time_fact) over (partition by orf.stop_place_muid order by orf.time_fact) as prev_fact_any
     from tt.order_list orl
     join tt.order_round orr on orr.order_list_id = orl.order_list_id
     join tt.order_fact orf on orf.order_round_id = orr.order_round_id
     join tt.round rnd on rnd.round_id = orr.round_id
     join gis.route_trajectories gtj on gtj.muid = rnd.route_trajectory_muid
     join gis.route_rounds grr on grr.muid = gtj.route_round_muid
     join gis.route_variants grv on grv.muid = grr.route_variant_muid
     join gis.routes grt on grt.muid = grv.route_muid
    where orf.time_fact between l_begin_date and l_end_date
      and orf.stop_place_muid = any(l_stop_list)
      and orl.sign_deleted = 0
      and orl.is_active = 1
      and orr.is_active = 1
      and orf.sign_deleted = 0   
   )
   select rut.number     as route_number,
          trt.short_name as tr_type_short_name,
          stp.name       as stop_name,
          mvd.name       as move_dir_name,
          stp1.stop_place_muid, stp1.route_muid, stp1.transport_kind_muid,
          core.interval2minutes(stp2.min_plan_any_diff)::text   as min_plan_any_diff,
          core.interval2minutes(stp2.max_plan_any_diff)::text   as max_plan_any_diff,
          core.interval2minutes(stp2.avg_plan_any_diff)::text   as avg_plan_any_diff,
          core.interval2minutes(stp3.min_plan_route_diff)::text as min_plan_route_diff,
          core.interval2minutes(stp3.max_plan_route_diff)::text as max_plan_route_diff,
          core.interval2minutes(stp3.avg_plan_route_diff)::text as avg_plan_route_diff,
          core.interval2minutes(stp4.min_plan_kind_diff)::text  as min_plan_kind_diff,
          core.interval2minutes(stp4.max_plan_kind_diff)::text  as max_plan_kind_diff,
          core.interval2minutes(stp4.avg_plan_kind_diff)::text  as avg_plan_kind_diff,
          core.interval2minutes(stp5.min_fact_any_diff)::text   as min_fact_any_diff,
          core.interval2minutes(stp5.max_fact_any_diff)::text   as max_fact_any_diff,
          core.interval2minutes(stp5.avg_fact_any_diff)::text   as avg_fact_any_diff,
          core.interval2minutes(stp6.min_fact_route_diff)::text as min_fact_route_diff,
          core.interval2minutes(stp6.max_fact_route_diff)::text as max_fact_route_diff,
          core.interval2minutes(stp6.avg_fact_route_diff)::text as avg_fact_route_diff,
          core.interval2minutes(stp7.min_fact_kind_diff)::text  as min_fact_kind_diff,
          core.interval2minutes(stp7.max_fact_kind_diff)::text  as max_fact_kind_diff,
          core.interval2minutes(stp7.avg_fact_kind_diff)::text  as avg_fact_kind_diff
     from (select distinct stop_place_muid, route_muid, transport_kind_muid from stop_time_plan) stp1
     join gis.routes rut on rut.muid = stp1.route_muid
     join core.tr_type trt on trt.transport_kind_muid = stp1.transport_kind_muid
     join gis.stop_places spp on spp.muid = stp1.stop_place_muid
     join gis.stops stp on stp.muid = spp.stop_muid
     left join gis.ref_movement_directions mvd on mvd.muid = stp.direction_muid
     join (select stop_place_muid, 
                  min(time_plan - prev_plan_any) as min_plan_any_diff,
                  max(time_plan - prev_plan_any) as max_plan_any_diff,
                  avg(time_plan - prev_plan_any) as avg_plan_any_diff
             from stop_time_plan
            where (time_plan - prev_plan_any) >= interval '1 min'
            group by stop_place_muid
          ) stp2 on stp2.stop_place_muid = stp1.stop_place_muid
     left join (select stop_place_muid, route_muid,
                  min(time_plan - prev_plan_route) as min_plan_route_diff,
                  max(time_plan - prev_plan_route) as max_plan_route_diff,
                  avg(time_plan - prev_plan_route) as avg_plan_route_diff
             from stop_time_plan
            where (time_plan - prev_plan_route) >= interval '1 min'
            group by stop_place_muid, route_muid
          ) stp3 on stp3.stop_place_muid = stp1.stop_place_muid
                and stp3.route_muid = stp1.route_muid
     left join (select stop_place_muid, transport_kind_muid,
                  min(time_plan - prev_plan_kind) as min_plan_kind_diff,
                  max(time_plan - prev_plan_kind) as max_plan_kind_diff,
                  avg(time_plan - prev_plan_kind) as avg_plan_kind_diff
             from stop_time_plan
            where (time_plan - prev_plan_kind) >= interval '1 min'
            group by stop_place_muid, transport_kind_muid
          ) stp4 on stp4.stop_place_muid = stp1.stop_place_muid
                and stp4.transport_kind_muid = stp1.transport_kind_muid
     join (select stop_place_muid, 
                  min(time_fact - prev_fact_any) as min_fact_any_diff,
                  max(time_fact - prev_fact_any) as max_fact_any_diff,
                  avg(time_fact - prev_fact_any) as avg_fact_any_diff
             from stop_time_fact
            where (time_fact - prev_fact_any) >= interval '1 min'
            group by stop_place_muid
          ) stp5 on stp5.stop_place_muid = stp1.stop_place_muid
     left join (select stop_place_muid, route_muid,
                  min(time_fact - prev_fact_route) as min_fact_route_diff,
                  max(time_fact - prev_fact_route) as max_fact_route_diff,
                  avg(time_fact - prev_fact_route) as avg_fact_route_diff
             from stop_time_fact
            where (time_fact - prev_fact_route) >= interval '1 min'
            group by stop_place_muid, route_muid
          ) stp6 on stp6.stop_place_muid = stp1.stop_place_muid
                and stp6.route_muid = stp1.route_muid
     left join (select stop_place_muid, transport_kind_muid,
                  min(time_fact - prev_fact_kind) as min_fact_kind_diff,
                  max(time_fact - prev_fact_kind) as max_fact_kind_diff,
                  avg(time_fact - prev_fact_kind) as avg_fact_kind_diff
             from stop_time_fact
            where (time_fact - prev_fact_kind) >= interval '1 min'
            group by stop_place_muid, transport_kind_muid
          ) stp7 on stp7.stop_place_muid = stp1.stop_place_muid
                and stp7.transport_kind_muid = stp1.transport_kind_muid
     where (
       rut.muid in (select route_muid
                    from rts.route2gis
                    where route_id in (select adm.account_data_realm_pkg$get_routes(p_account_id)))
       or trt.tr_type_id in (select adm.account_data_realm_pkg$get_tr_types(p_account_id))
     )
    order by stp.name, rut.number, trt.short_name;
end;
$$ language plpgsql volatile security invoker;
