﻿create or replace function mnt.jf_stops_of_tr_pkg$of_rows
  (p_account_id               in numeric,
   p_attr                     in text,
   p_rows                     out refcursor)
returns refcursor
as $$ declare
   l_query_date   timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'F_DT', true), now());
   l_route_muid   bigint    := jofl.jofl_pkg$extract_number(p_attr, 'f_route_muid', true);


   l_tr_id        bigint    := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_id', true);
   
   --l_available_trs_ids bigint[];
   --l_available_rut_ids bigint[];
begin
   --l_available_trs_ids := adm.data_realm_pkg$get_trs_ids_by_depo_and_tr_type(p_account_id);
   --l_available_rut_ids := adm.data_realm_pkg$get_available_routes_muids(p_account_id);
   
   open p_rows for
   select orr.order_round_num round_num,
          stt.name as stop_name,
          oro.stop_order_num,
          oro.time_oper as stop_time_plan,
          orf.time_fact as stop_time_fact,
          round(extract(epoch from (oro.time_oper - orf.time_fact)) / 60) as stop_between_duration
     from tt.order_list orl
     join tt.order_round orr on orr.order_list_id = orl.order_list_id
     join tt.round rnd on rnd.round_id = orr.round_id
     join tt.order_oper oro on oro.order_round_id = orr.order_round_id
     left join tt.order_fact orf on orf.order_round_id = orr.order_round_id
                                and orf.stop_place_muid = oro.stop_place_muid
                                and orf.stop_order_num = oro.stop_order_num
                                and orf.sign_deleted = 0
     join gis.stop_places stp on stp.muid = oro.stop_place_muid
     join gis.stops stt on stt.muid = stp.stop_muid
    where orl.sign_deleted = 0
      and orl.is_active = 1
      and orr.is_active = 1
      and rnd.sign_deleted = 0
      and oro.sign_deleted = 0
      and oro.is_active = 1
      and orl.order_date = l_query_date
      and orl.tr_id = l_tr_id
    order by oro.time_oper;
end;
$$ language plpgsql volatile security invoker;
