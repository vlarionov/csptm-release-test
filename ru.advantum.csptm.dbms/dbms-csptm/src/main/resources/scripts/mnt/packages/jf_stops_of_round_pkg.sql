﻿create or replace function mnt.jf_stops_of_round_pkg$of_rows
  (p_id_account               in numeric,
   p_attr                     in text,
   p_rows                     out refcursor)
returns refcursor
as $$ declare
  l_order_round_id      bigint    := jofl.jofl_pkg$extract_number(p_attr, 'order_round_id', true);
begin
   open p_rows for
   select
     st.name as stop_name,
     oo.stop_order_num,
     oo.time_oper as stop_time_plan,
     orf.time_fact as stop_time_fact,
     round(extract(epoch from (oo.time_oper - orf.time_fact)) / 60) as stop_between_duration,
     oo.is_kp
   from gis.stops st
     join gis.stop_places sp on sp.stop_muid =st.muid
     join gis.stop_place2route_traj sp2rt on sp2rt.stop_place_muid =sp.muid
     join tt.order_oper oo on oo.stop_place_muid =sp.muid
     join tt.order_round ord on ord.order_round_id = oo.order_round_id
     join tt.round rnd on rnd.round_id = ord.round_id and rnd.route_trajectory_muid = sp2rt.route_trajectory_muid
     left join tt.order_fact orf on orf.order_round_id = oo.order_round_id and orf.stop_place_muid = oo.stop_place_muid
   where oo.order_round_id = l_order_round_id
         and oo.is_active=1
         and oo.sign_deleted =0
   order by oo.stop_order_num;
end;
$$ language plpgsql volatile security invoker;
