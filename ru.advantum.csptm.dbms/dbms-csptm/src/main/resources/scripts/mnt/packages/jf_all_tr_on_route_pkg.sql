﻿create or replace function mnt.jf_all_tr_on_route_pkg$of_rows
  (p_id_account               in numeric,
   p_attr                     in text,
   p_rows                     out refcursor)
returns refcursor
as $$ declare
   l_query_date   timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'F_DT', true), now());
   l_route_muid   bigint    := jofl.jofl_pkg$extract_number(p_attr, 'f_route_muid', true);
begin
   open p_rows for
   with tr_round as (
      select distinct orl.tr_id
        from tt.order_list orl
        join tt.order_round orr on orr.order_list_id = orl.order_list_id
        join tt.round rnd on rnd.round_id = orr.round_id
        join gis.route_trajectories rtj on rtj.muid = rnd.route_trajectory_muid
        join gis.route_rounds rtr on rtr.muid = rtj.route_round_muid
        join gis.route_variants rtv on rtv.muid = rtr.route_variant_muid
       where orl.sign_deleted = 0
         and orl.is_active = 1
         and orl.order_date = l_query_date
         and rtv.route_muid = l_route_muid
   )
   select t.tr_id,
          p.name as tr_type_name,
          t.garage_num gar_num,
          t.licence gos_num
     from core.tr t
     join core.tr_type p on p.tr_type_id = t.tr_type_id
     join tr_round r on r.tr_id = t.tr_id
    order by t.garage_num;
end;
$$ language plpgsql volatile security invoker;
