﻿create or replace function mnt.jf_stops_pkg$of_rows
  (p_id_account   in numeric,
   p_attr         in text,
   p_rows         out refcursor)
returns refcursor
as $$ declare
   l_name     gis.stops.name%type 	  := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_NAME', true), ''); 
   l_district gis.stops.district%type := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_DISTRICT', true), ''); 
   l_region   gis.stops.region%type   := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_REGION', true), ''); 
   l_street   gis.stops.street%type   := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_STREET', true), '');
begin
   
   open p_rows for
   select sp.muid::text             as "MUID",
          s.muid::text              as "STOP_MUID",
          s.name                    as "NAME",
          s.direction_muid          as "DIRECTION_MUID",
          mvd.name                  as "DIRECTION_NAME",
          coalesce(s.district, 'Нет данных') as "DISTRICT", 
          coalesce(s.region,   'Нет данных') as "REGION",
          s.street                  as "STREET",
          sp.building_address	      as "BUILDING_ADDRESS",
          substring((case when sp.has_bus       <> 0 then ', Автобус'    else '' end ||
                     case when sp.has_trolley   <> 0 then ', Троллейбус' else '' end ||         
                     case when sp.has_tram      <> 0 then ', Трамвай'    else '' end ||         
                     case when sp.has_speedtram <> 0 then ', Скоростной трамвай' else '' end) from 3) as "TR_KINDS",
          sp.wkt_geom	            as "GEOM",
          -- Внимание! к признакам привязан фильтр на карте, не убирать
          sp.has_bus                as "HAS_BUS" ,
          sp.has_trolley            as "HAS_TROLLEY",
          sp.has_tram               as "HAS_TRAM",
          sp.has_speedtram          as "HAS_SPEEDTRAM",
          sp.is_technical           as "IS_TECHNICAL",
          rl.route_list             as "ROUTES_LIST",
          rl.routes_list_bus        as "ROUTES_LIST_BUS",
          rl.routes_list_trolley    as "ROUTES_LIST_TROLLEY",
          rl.routes_list_tram       as "ROUTES_LIST_TRAM",
          rl.routes_list_speedtram  as "ROUTES_LIST_SPEEDTRAM",
          'получение информации...' as "PASS_POTOK"
     from gis.stops s
     join gis.stop_places sp on sp.stop_muid = s.muid
     left join gis.ref_movement_directions mvd on mvd.muid = s.direction_muid
     left join (select sp2rt.stop_place_muid,
                       array_to_string(array_agg(distinct r.number), ', ') as route_list,
                       trim(leading '_, ' from (array_to_string(array_agg(distinct case when r.transport_kind_muid = 1 then r.number else '_' end), ', '))) as routes_list_bus,
                       trim(leading '_, ' from (array_to_string(array_agg(distinct case when r.transport_kind_muid = 2 then r.number else '_' end), ', '))) as routes_list_trolley,
                       trim(leading '_, ' from (array_to_string(array_agg(distinct case when r.transport_kind_muid = 3 then r.number else '_' end), ', '))) as routes_list_tram,
                       trim(leading '_, ' from (array_to_string(array_agg(distinct case when r.transport_kind_muid = 4 then r.number else '_' end), ', '))) as routes_list_speedtram
                  from gis.stop_place2route_traj sp2rt
                  join gis.route_trajectories rt on rt.muid = sp2rt.route_trajectory_muid
                  join gis.route_rounds rr on rr.muid = rt.route_round_muid
                  join gis.route_variants rv on rv.muid = rr.route_variant_muid
                  join gis.routes r on r.muid = rv.route_muid and r.current_route_variant_muid = rv.muid
                 where rv.sign_deleted = 0
                   and rr.sign_deleted = 0
                   and rt.sign_deleted = 0
                   and sp2rt.sign_deleted = 0
                 group by sp2rt.stop_place_muid
               ) rl on rl.stop_place_muid = sp.muid
    where s.sign_deleted = 0
      and sp.sign_deleted = 0
      and sp.muid in (select si2g.stop_place_muid
                        from rts.stop_item2gis si2g
                       where si2g.stop_item_id in (select adm.account_data_realm_pkg$get_stop_items_by_all(p_id_account)))
      and (l_name = '' or s.name = l_name)
      and (l_district = coalesce(s.district, 'Нет данных') or l_district = '')
      and (l_region = coalesce(s.region, 'Нет данных') or l_region = '')
      and (l_street = s.street or l_street = '');
end;
$$ language 'plpgsql' volatile security invoker;
