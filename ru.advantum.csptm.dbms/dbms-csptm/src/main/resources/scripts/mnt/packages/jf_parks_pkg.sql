﻿create or replace function mnt.jf_parks_pkg$get_park_geom
  (v_park_muid                in bigint)
  returns text
as $$ declare
  v_result                   text;
begin
  select st_astext(st_multi(st_union(z.geom))) as wkt_geom
  into v_result
  from gis.parks2park_zones p
    join gis.park_zones z on z.muid = p.park_zone_muid
  where p.park_muid = v_park_muid
        and p.sign_deleted = 0;

  return v_result;
end;
$$ language plpgsql security definer;
------

create or replace function mnt.jf_parks_pkg$get_depo_geom
  (p_depo_id                in bigint)
  returns text
as $$ declare
  v_result                   text;
begin
  select st_astext(st_multi(st_union(trr.geom))) as wkt_geom
  into v_result
  from core.depo2territory d2t
  join core.territory trr on d2t.territory_id = trr.territory_id
  where d2t.depo_id =p_depo_id ;

  return v_result;
end;
$$ language plpgsql security definer;
---------

create or replace function mnt.jf_parks_pkg$of_rows
  (p_id_account   in numeric,
   p_rows         out refcursor,
   p_attr         in text)
returns refcursor
as $$ declare
   F_NAME     gis.transport_parkings.name%type 	  := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_NAME',    true), '');
   F_ADDRESS  gis.transport_parkings.address%type := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_ADDRESS', true), '');
begin
   open p_rows for
   select
     t.park_zone_muid::text as "MUID",
     depo.tn_instance::int as "NUM",
     et.name_full as "NAME",
     et.name_short as "SHORT_NAME",
     (select string_agg(adr.address_text, ',') from core.address adr where adr.entity_id=et.entity_id) as  "ADDRESS",
     (select string_agg(ct.phone_num, ',') from core.contact ct where ct.entity_id=et.entity_id) as "PHONE",
     st_astext(t.geom) as "GEOM",
     entity4depo.name_full as  depo_name,
     entity4depo.name_short as depo_name_short,
     (select string_agg(adr.address_text, ',') from core.address adr where adr.entity_id=depo.depo_id) as  depo_address,
     (select string_agg(ct.phone_num, ',') from core.contact ct where ct.entity_id=depo.depo_id) as depo_phone,
     tt.name as tr_type_name,
     depo.depo_id
   from core.depo
     join core.entity entity4depo on entity4depo.entity_id = depo.depo_id
     join core.tr_type tt on tt.tr_type_id = depo.tr_type_id
     join core.depo2territory d2t on d2t.depo_id= depo.depo_id
     join core.territory t on t.territory_id = d2t.territory_id
     join core.entity et on et.entity_id = t.territory_id
   order by depo_name, short_name
 ;
end;
$$ language 'plpgsql' volatile security invoker;
