CREATE OR REPLACE FUNCTION mnt.jf_disp_main_window_route_pkg$of_rows (
      p_id_account numeric,
  out p_rows refcursor,
      p_attr text
)
  RETURNS refcursor AS
$body$
declare
  lts_request_datetime timestamp without time zone := current_timestamp;
  l_interval interval :=  interval '61 min';
begin
  open p_rows for
  select
    a_route_id  as route_id
    ,route_variant_id
    ,rtk_name
    ,tr_type
    ,route_number
    ,start_point
    ,end_point
    ,stop_id_b
    ,stop_id_e
    ,tr_oper_online
    ,tr_fact_online
    ,tr_plan_online
    ,rnd_oper_online
    ,rnd_fact_online
    ,rnd_plan_online
    ,fact_diff_a
    ,fact_diff_b
    ,plan_diff_a
    ,plan_diff_b
    ,route_mode
    ,interval_violate_comment
  from mnt.jf_disp_main_window_route_pkg$get_rows(lts_request_datetime, l_interval)
  where  a_route_id in (select adm.account_data_realm_pkg$get_routes(p_id_account)) ;

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;


CREATE OR REPLACE FUNCTION mnt.jf_disp_main_window_route_pkg$get_rows(
    p_time timestamp without time zone
    ,p_interval interval
)
RETURNS TABLE (
    a_route_id integer
    ,route_variant_id  integer
    ,rtk_name text
    ,tr_type text
    ,route_number text
    ,start_point  text
    ,end_point  text
    ,stop_id_b integer
    ,stop_id_e integer
    ,tr_oper_online integer
    ,tr_fact_online integer
    ,tr_plan_online integer
    ,rnd_oper_online integer
    ,rnd_fact_online integer
    ,rnd_plan_online integer
    ,fact_diff_a integer
    ,fact_diff_b integer
    ,plan_diff_a integer
    ,plan_diff_b integer
    ,route_mode text
    ,interval_violate_comment text
    ,tt_variant_id integer
)
as
$body$
declare
  lts_tuned_datetime timestamp without time zone;
  ld_order_date date;
begin
  ld_order_date := date_trunc('day', p_time);

  select (ttb.jf_incident_pkg$cn_plan_tt_date()::date + now()::time) at TIME ZONE 'utc' at time zone tune_value
  into lts_tuned_datetime
  from ttb.tune where tune_id = 1;



  return query
  with route_ends_stop as (
      SELECT distinct
        route.route_id
        ,route.current_route_variant_id
        ,rtk.name as rtk_name
        ,rtk.short_name as tr_type
        ,route.route_num
        ,first_value(s.name)
         OVER (PARTITION BY rv.route_variant_id
           ORDER BY si2r.order_num)      AS stop_b
        ,first_value(s.name)
         OVER (PARTITION BY rv.route_variant_id
           ORDER BY si2r.order_num DESC) AS stop_e
        ,first_value(s.stop_id)
         OVER (PARTITION BY rv.route_variant_id
           ORDER BY si2r.order_num)      AS stop_id_b
        ,first_value(s.stop_id)
         OVER (PARTITION BY rv.route_variant_id
           ORDER BY si2r.order_num DESC) AS stop_id_e
        ,rtk
        ,ttv.tt_variant_id
      FROM ttb.tt_variant ttv
        JOIN rts.route_variant rv ON rv.route_variant_id = ttv.route_variant_id
                                     AND ttv.parent_tt_variant_id IS NOT NULL
                                     AND not rv.sign_deleted
                                     AND NOT ttv.sign_deleted
        JOIN rts.route ON route.route_id = rv.route_id AND NOT route.sign_deleted
        JOIN core.tr_type rtk ON rtk.tr_type_id = route.tr_type_id
        JOIN rts.round r ON rv.route_variant_id = r.route_variant_id AND NOT r.sign_deleted
        JOIN rts.stop_item2round si2r ON r.round_id = si2r.round_id AND NOT si2r.sign_deleted
        JOIN rts.stop_item si ON si.stop_item_id = si2r.stop_item_id AND NOT si.sign_deleted
        JOIN rts.stop_location sl ON si.stop_location_id = sl.stop_location_id AND NOT sl.sign_deleted
        JOIN rts.stop s ON s.stop_id = sl.stop_id and not s.sign_deleted
      WHERE r.code = '00'
            AND r.move_direction_id = 1
            AND ttv.order_date = now() :: DATE
            )
  ,tt_plan_rounds as (
      select route_variant.route_id
        ,count(DISTINCT tt_out.tt_out_id) tr_plan_online
        ,count(distinct tt_action_round.tt_action_id) rnd_plan_online
      from ttb.tt_variant
        join rts.route_variant on route_variant.route_variant_id = tt_variant.route_variant_id and not tt_variant.sign_deleted
        join ttb.tt_out on tt_variant.tt_variant_id = tt_out.tt_variant_id and not tt_out.sign_deleted
        join ttb.tt_action on tt_out.tt_out_id = tt_action.tt_out_id and not tt_action.sign_deleted
        join ttb.tt_action_round on tt_action.tt_action_id = tt_action_round.tt_action_id
        join rts.round on round.round_id = tt_action_round.round_id and not round.sign_deleted
        join ttb.action_type on action_type.action_type_id = tt_action.action_type_id
        join ttb.vm_action_type on tt_action.action_type_id = vm_action_type.action_type_id
      where  vm_action_type.is_production_round
             and tt_variant.tt_variant_id in
                 (select parent_tt_variant_id from ttb.tt_variant where order_date = ld_order_date and not tt_variant.sign_deleted)
      group by route_variant.route_id
  )
    ,tt_oper_rounds as (
      select route_variant.route_id
        ,count(distinct tt_action_round.tt_action_id) rnd_oper_online
      from ttb.tt_variant
        join rts.route_variant on route_variant.route_variant_id = tt_variant.route_variant_id and not tt_variant.sign_deleted
        join ttb.tt_out on tt_variant.tt_variant_id = tt_out.tt_variant_id and not tt_out.sign_deleted
        join ttb.tt_action on tt_out.tt_out_id = tt_action.tt_out_id and not tt_action.sign_deleted
        join ttb.tt_action_round on tt_action.tt_action_id = tt_action_round.tt_action_id
        join rts.round on round.round_id = tt_action_round.round_id and not round.sign_deleted
        join ttb.action_type on action_type.action_type_id = tt_action.action_type_id
        join ttb.vm_action_type on tt_action.action_type_id = vm_action_type.action_type_id
      where  lts_tuned_datetime >= tt_action.dt_action
             and vm_action_type.is_production_round
             and tt_variant.tt_variant_id in
                 (select parent_tt_variant_id from ttb.tt_variant where order_date = ld_order_date and not tt_variant.sign_deleted)
      group by route_variant.route_id

  )
    ,tt_plan_tr_current_cnt as (
      select route_variant.route_id
        ,count(DISTINCT tt_out.tt_out_id) tr_oper_online
      from ttb.tt_variant
        join rts.route_variant on route_variant.route_variant_id = tt_variant.route_variant_id and not tt_variant.sign_deleted
        join ttb.tt_out on tt_variant.tt_variant_id = tt_out.tt_variant_id and not tt_out.sign_deleted
        join ttb.tt_action on tt_out.tt_out_id = tt_action.tt_out_id and not tt_action.sign_deleted
        join ttb.action_type on action_type.action_type_id = tt_action.action_type_id
        join ttb.vm_action_type on tt_action.action_type_id = vm_action_type.action_type_id
      where
             vm_action_type.is_production_round
             and tt_variant.tt_variant_id in
                 (select parent_tt_variant_id from ttb.tt_variant where order_date = ld_order_date and not tt_variant.sign_deleted)
      group by route_variant.route_id
  )
    ,ts_fact_count as (
      select route_variant.route_id
        ,count(DISTINCT tt_out.tt_out_id) as tr_fact_online
        ,count(distinct tt_action_round.tt_action_id) as rnd_fact_online
      from ttb.tt_variant
        join rts.route_variant on route_variant.route_variant_id = tt_variant.route_variant_id
        join ttb.tt_out on tt_variant.tt_variant_id = tt_out.tt_variant_id
        join ttb.tt_action on tt_out.tt_out_id = tt_action.tt_out_id
        join ttb.tt_action_round on tt_action.tt_action_id = tt_action_round.tt_action_id
        join rts.round on round.round_id = tt_action_round.round_id and not round.sign_deleted
        join ttb.action_type on action_type.action_type_id = tt_action.action_type_id
        join ttb.vm_action_type on tt_action.action_type_id = vm_action_type.action_type_id
      where  not tt_variant.sign_deleted
             and not tt_out.sign_deleted
             and not tt_action.sign_deleted
             and p_time >= tt_action.dt_action
             and vm_action_type.is_production_round
             and order_date = ld_order_date
             and tt_action_round.round_status_id = ttb.round_status_pkg$passed()
      group by route_variant.route_id

  )
    ,tt_oper_tr_current_cnt as (
      select route_variant.route_id
        ,count(DISTINCT tt_out.tt_out_id) as tr_fact_online
      from ttb.tt_variant
        join rts.route_variant on route_variant.route_variant_id = tt_variant.route_variant_id
        join ttb.tt_out on tt_variant.tt_variant_id = tt_out.tt_variant_id
        join ttb.tt_action on tt_out.tt_out_id = tt_action.tt_out_id
      where  not tt_variant.sign_deleted
             and not tt_out.sign_deleted
             and not tt_action.sign_deleted
             and p_time between tt_action.dt_action and (tt_action.dt_action +  action_dur * interval '1 seconds')
             and order_date = ld_order_date
      group by route_variant.route_id

  )
    ,fact_interval_a as (
      select route_id,
        ttb.get_avg_interval_fact_for_route(
            p_order_date := ld_order_date
            ,p_route_id := route_id
            ,p_period_start := now()::timestamp - p_interval
            ,p_period_finish := now()::timestamp
            ,p_move_direction_id := 1) as fact_diff_a
      from tt_oper_rounds
  )
    , fact_interval_b as (
      select route_id,
        ttb.get_avg_interval_fact_for_route(
            p_order_date := ld_order_date
            ,p_route_id := route_id
            ,p_period_start := now()::timestamp - p_interval
            ,p_period_finish := now()::timestamp
            ,p_move_direction_id := 2) as fact_diff_b
      from tt_oper_rounds
  )
    ,plan_interval_a as (
      select route_id,
        ttb.get_avg_interval_plan_for_route_by_oreder_date(
            p_order_date := ld_order_date
            ,p_route_id := route_id
            ,p_move_direction_id := 1) plan_diff_a
      from tt_oper_rounds
  ),
      plan_interval_b as (
        select route_id,
          ttb.get_avg_interval_plan_for_route_by_oreder_date(
              p_order_date := ld_order_date
              ,p_route_id := route_id
              ,p_move_direction_id := 2) plan_diff_b
        from tt_oper_rounds
    )
  select
    route_ends_stop.route_id as a_route_id
    ,route_ends_stop.current_route_variant_id  AS route_variant_id
    ,route_ends_stop.rtk_name                    AS rtk_name
    ,route_ends_stop.tr_type                  AS tr_type
    ,route_ends_stop.route_num                 AS route_number
    ,route_ends_stop.stop_b as start_point
    ,route_ends_stop.stop_e as end_point
    ,route_ends_stop.stop_id_b as stop_id_b
    ,route_ends_stop.stop_id_e as stop_id_e
    ,tt_plan_tr_current_cnt.tr_oper_online::int  as tr_oper_online
    ,tt_oper_tr_current_cnt.tr_fact_online::int  as tr_fact_online
    ,tt_plan_rounds.tr_plan_online::int  as tr_plan_online
    ,tt_oper_rounds.rnd_oper_online::int as rnd_oper_online
    ,ts_fact_count.rnd_fact_online::int as rnd_fact_online
    ,tt_plan_rounds.rnd_plan_online::int as rnd_plan_online
    ,core.interval2minutes(fina.fact_diff_a) as fact_diff_a
    ,core.interval2minutes(finb.fact_diff_b) as fact_diff_b
    ,core.interval2minutes(pina.plan_diff_a) as plan_diff_a
    ,core.interval2minutes(pinb.plan_diff_b) as plan_diff_b
    ,'Интервальное' as route_mode
    ,ttb.jf_incident_pkg$get_comment_interval_violate(route_ends_stop.tt_variant_id) interval_violate_comment
    ,route_ends_stop.tt_variant_id
  from route_ends_stop
    left join tt_oper_rounds on tt_oper_rounds.route_id = route_ends_stop.route_id
    left join ts_fact_count  on ts_fact_count.route_id = route_ends_stop.route_id
    left join tt_plan_rounds on tt_plan_rounds.route_id = route_ends_stop.route_id
    left join fact_interval_a  fina on fina.route_id = route_ends_stop.route_id
    left join fact_interval_b  finb on finb.route_id = route_ends_stop.route_id
    left join plan_interval_a  pina on pina.route_id = route_ends_stop.route_id
    left join plan_interval_b  pinb on pinb.route_id = route_ends_stop.route_id
    left join tt_plan_tr_current_cnt on tt_plan_tr_current_cnt.route_id = route_ends_stop.route_id
    left join tt_oper_tr_current_cnt on tt_oper_tr_current_cnt.route_id = route_ends_stop.route_id;

end;
$body$
LANGUAGE 'plpgsql';