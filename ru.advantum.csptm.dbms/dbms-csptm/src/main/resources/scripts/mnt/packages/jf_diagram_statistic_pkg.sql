CREATE OR REPLACE FUNCTION mnt.jf_diagram_statistic_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare
  l_round_id_list bigint[];
  l_dt timestamp;
  l_route_variant_id rts.route_variant.route_variant_id%type;
  l_round_num rts.round.round_num%type;
begin
  l_dt  := jofl.jofl_pkg$extract_date(p_attr,    'F_DT', true) + interval '3 hours';
  l_route_variant_id  := jofl.jofl_pkg$extract_number(p_attr, 'route_variant_id', true);
  l_round_num := jofl.jofl_pkg$extract_number(p_attr, 'round_num', true);

  -- получаем round_id для прямого и обратного рейсов
  select array_agg(ro.round_id)
  into l_round_id_list
  from rts.round ro
  where ro.route_variant_id = l_route_variant_id and
        ro.round_num = l_round_num;

  open p_rows for
with rd as (select ra.*,
                   dr.fio,
                   tr.garage_num,
                   tr.licence,
                   r.route_num
            from mnt.v_tt_action_round_attrib ra
                 join (select d.driver_id,
                              d.driver_name||' '||d.driver_middle_name||' '||d.driver_last_name fio
                       from core.driver d
                       where d.sign_deleted = 0) dr on dr.driver_id = ra.driver_id
                 join core.tr tr on tr.tr_id = ra.tr_id
                 join rts.route r on r.route_id = ra.route_id and not r.sign_deleted
            where ra.order_date = date_trunc('day', l_dt)
                  and ra.round_id = Any(l_round_id_list)
                  /* and ra.tr_id in (select adm.account_data_realm_pkg$get_trs_by_tr_type_and_depo(p_id_account)) */
), round_count as (
    select
      count(1)                                                                   plan,
      count(1) filter (where rd.round_status_id = ttb.round_status_pkg$passed()) fact
    from rd
    group by rd.round_id
    limit 1
), tr_count as (
    select
      count(rd.tr_id)                                                                   plan,
      count(rd.tr_id) filter (where rd.round_status_id = ttb.round_status_pkg$passed()) fact
    from rd
    group by rd.tr_id
    limit 1
)
select   rd.route_variant_id::text as routeId --  ra.route_variant_muid::text as routeId
       , rd.route_num as routeName            -- ,ra.routes_number as routeName
       , rd.round_id as raceId                -- ,ra.rounds_muid::text as raceId
       , rd.code as raceCode                  -- ,ra.round_code as raceCode
       , rd.move_direction_id as dir          -- ,ra.direction_id dir
       , rd.tt_out_num as  exitNum            -- ,ra.timetable_entry_num as exitNum
       , ttai.time_begin as plan_time         -- ,oo.time_plan plan_time
       , ttaf.time_fact_begin as real_time    --  ,orf.time_fact real_time
       , si.stop_item_id::text as stopId      -- ,s.muid::text stopId
       , s.name as stopName                   -- ,s.name stopName
       , sum(si2r.length_sector) OVER (PARTITION BY rd.tt_action_id ORDER BY si2r.order_num) as len
       , rd.tt_action_id as roundid           -- ,ra.order_round_id as roundid
       , case /* прогноз показываем, для будущего и если нет факта */
          when (/* ttaf.time_fact_begin is null and -- пока показываем любой имеющийся прогноз на будущее */
		            frc.pass_time_forecast > now())
           then frc.pass_time_forecast
          else null::timestamp
         end as pass_time_forecast
       , rc.plan plan_round_count
       , rc.fact fact_round_count
       , trc.plan plan_tr_count
       , trc.fact fact_tr_count
from rd
     join ttb.tt_action_item ttai on ttai.tt_action_id = rd.tt_action_id
     join rts.stop_item2round si2r on ttai.stop_item2round_id = si2r.stop_item2round_id
     join rts.stop_item si on si.stop_item_id = si2r.stop_item_id
     join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
     join rts.stop s on s.stop_id = sl.stop_id
     left join ttb.tt_action_fact ttaf on ttaf.tt_action_id = ttai.tt_action_id and
                                     ttaf.stop_item2round_id = ttai.stop_item2round_id
     left join ibrd.forecast frc on frc.tt_action_id = ttai.tt_action_id and
                                    frc.stop_item_id = si2r.stop_item_id and
                                    frc.stop_order_num = si2r.order_num and
									                  frc.pass_time_forecast >= now() and
                                    frc.creation_time = (select max(creation_time)
                                                          from ibrd.forecast f
                                                          where f.tt_action_id = frc.tt_action_id and
                                                                f.stop_item_id = frc.stop_item_id and
                                                                f.stop_order_num = frc.stop_order_num)
     join round_count rc on true
     join tr_count trc on true
where not ttai.sign_deleted
      and not si2r.sign_deleted
      and not si.sign_deleted
      and not sl.sign_deleted
      and not s.sign_deleted
order by rd.tt_action_id, si2r.order_num;
end;
$function$;
