CREATE OR REPLACE FUNCTION mnt.jf_order_oper_deviation_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
begin
/*  open p_rows for
  select d.tr_id
    ,d.time_oper
    ,d.time_fact
    ,round(EXTRACT(EPOCH FROM (d.time_oper - d.time_fact))/60) as deviation
    ,d.stops_name as stops_name
    ,tr.garage_num
  from mnt.v_order_oper_deviation d
        join core.tr on d.tr_id = tr.tr_id
where 1= 2;
*/
open p_rows for
with a as (
    SELECT val
    FROM core.redis_server
    WHERE key = 'mnt.order_oper_deviation'
)
select tr_id, time_oper, time_fact, deviation, stops_name, garage_num from a, json_to_recordset(a.val::json)
  as x(tr_id bigint, time_oper TIMESTAMP, time_fact TIMESTAMP, deviation numeric, stops_name text, garage_num text)
;


end;
$function$
;

CREATE OR REPLACE FUNCTION mnt.jf_order_oper_deviation_pkg$of_cron()
  RETURNS int
LANGUAGE plpgsql
AS $function$
begin
  update core.redis_server
  set val = (select coalesce(array_to_json(array_agg(row_to_json(t))), '[{}]')
                     from (
                       SELECT
                         d.tr_id,
                         d.time_oper,
                         d.time_fact,
                         round(EXTRACT(EPOCH FROM (d.time_oper - d.time_fact)) / 60) AS deviation,
                         d.stops_name                                                AS stops_name,
                         tr.garage_num
                       FROM mnt.v_order_oper_deviation d
                         JOIN core.tr ON d.tr_id = tr.tr_id
                     ) t)
  where key = 'mnt.order_oper_deviation';

  return 1;
end;
$function$
;