﻿
create or replace function mnt.jf_trs_info_pkg$of_rows
  (p_account_id               in numeric,
   p_attr                     in text,
   p_rows                     out refcursor)
returns refcursor
as $$ declare
-- l_query_date   timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'QUERY_DATE', true), now());
   l_query_date   timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'QUERY_DATE', true), '20170414'::timestamp);
   l_order_date   timestamp := date_trunc('day', l_query_date);
   l_tr_kind_muid bigint    := jofl.jofl_pkg$extract_number(p_attr, 'F_TR_KIND_MUID', true);
   l_park_number  text      := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_PARK_NUMBER', true), '');
begin
   open p_rows for
   with tr2depo as ( 
      select t.tr_id, t.tr_type_id, t.garage_num, t.licence, 
             t.dt_end as tr_dt_end, 
             t.tr_status_id, t.tr_model_id,
             dep.depo_id, 
             dep.entity_name_short as depo_entity_name_short
        from core.tr t
        left join core.territory trr on trr.territory_id = t.territory_id
        left join core.v_depo dep on dep.depo_id = trr.depo_id
   ),
   plan_tr_on_ttround as (
      select ol.tr_id, dr.round_id, av.time_plan_begin, av.time_plan_end
        from asd.oper_round_visit av
        join tt.order_round dr on dr.order_round_id = av.order_round_id
        join tt.order_list ol on ol.order_list_id = dr.order_list_id
       where ol.tr_id is not null
         and ol.sign_deleted = 0
         and dr.is_active = 1
   ),
   ttround2route as (
      select rd.round_id, rt.number as route_number
        from tt.round rd
        join gis.route_trajectories tj on tj.muid = rd.route_trajectory_muid
        join gis.route_rounds u on u.muid = tj.route_round_muid
        join gis.route_variants rv on rv.muid = u.route_variant_muid
        join gis.routes rt on rt.muid = rv.route_muid
       where rd.sign_deleted = 0
   )
   select trd.tr_id,
          trd.garage_num             as "GARAGE_NUM",
          trd.licence                as "GOS_NUM",
          typ.name                   as "TR_TYPE_NAME",
          mod.name                   as "MODEL_NAME",
          cap.short_name             as "CAPACITY",
          trd.depo_entity_name_short as "DEPO_NAME",
          rtr.route_number           as "ROUTE_NUMBER",
          'н/д'::text                as "TR_STATUS",
          tls.speed                  as "SPEED",
          ST_AsText(ST_MakePoint(tls.lon, tls.lat)) as "WKT_GEOM",
          tls.event_time             as "LAST_PKT_TIME"
     from tr2depo trd
     join core.tr_type typ on typ.tr_type_id = trd.tr_type_id
     join core.tr_model mod on mod.tr_model_id = trd.tr_model_id
     left join core.tr_capacity cap on cap.tr_capacity_id = mod.tr_capacity_id
     join (select distinct tr_id from tt.order_list where order_date = l_order_date) ord on ord.tr_id = trd.tr_id
     join plan_tr_on_ttround ptr on ptr.tr_id = trd.tr_id
     left join ttround2route rtr on rtr.round_id = ptr.round_id
     left join core.tr_last_state tls on tls.tr_id = trd.tr_id
    where l_query_date between ptr.time_plan_begin and ptr.time_plan_end
      and (l_tr_kind_muid is null or typ.tr_type_id = l_tr_kind_muid)
    order by trd.garage_num;
end;
$$ language plpgsql volatile security invoker;
