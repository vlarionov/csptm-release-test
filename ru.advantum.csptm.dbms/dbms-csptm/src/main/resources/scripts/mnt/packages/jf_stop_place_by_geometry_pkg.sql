CREATE OR REPLACE FUNCTION mnt.jf_stop_place_by_geometry_pkg$of_rows(IN p_id_account NUMERIC, OUT p_rows REFCURSOR, IN p_attr TEXT)
  RETURNS REFCURSOR
AS
  $BODY$
declare
  l_geom geometry;
begin
  l_geom := ST_GeomFromText(jofl.jofl_pkg$extract_varchar(p_attr, 'GEOM', true));
    open p_rows for
        select
            st.name,
            string_agg(distinct(core.ts_type_as_string(sp.has_bus, sp.has_trolley, sp.has_tram, sp.has_speedtram, 1) || '-' || r.number), ', ') as routes,
            gns.street
        from gis.stop_places as sp
            join gis.stops as st on sp.stop_muid = st.muid
            join gis.stop_place2route_traj as sp2rt on sp.muid = sp2rt.stop_place_muid
            join gis.route_trajectories as rt on sp2rt.route_trajectory_muid = rt.muid
            join gis.route_rounds as rr on rt.route_round_muid = rr.muid
            join gis.route_variants as rv on rr.route_variant_muid = rv.muid
            join gis.routes as r on r.muid = rv.route_muid
            join gis.graph_sections as gs on sp.graph_section_muid = gs.muid
            join gis.graph_nodes as gn on gs.start_node_muid = gn.muid
            join gis.graph_nodes_street as gns on gns.muid = gn.muid
        where ST_Intersects(sp.geom, l_geom)
            and rt.sign_deleted != 1
            and rv.sign_deleted != 1
            and rr.sign_deleted != 1
        group by st.name, sp.has_bus, sp.has_trolley, sp.has_tram, sp.has_speedtram, gns.street;
    end;
  $BODY$
LANGUAGE plpgsql;

--TODO узнать у влада куда это перенести
create or replace function core.ts_type_as_string(p_has_bus numeric, p_has_trolley numeric, p_has_tram numeric, p_has_speedtram numeric, p_result_length integer)
    returns text
as $body$
declare
    l_result text;
begin
    return substring((case when p_has_bus       <> 0 then ', Автобус'    else '' end ||
                         case when p_has_trolley   <> 0 then ', Троллейбус' else '' end ||
                         case when p_has_tram      <> 0 then ', Трамвай'    else '' end ||
                         case when p_has_speedtram <> 0 then ', Скоростной трамвай' else '' end) from 3 for p_result_length);
end;
$body$
language plpgsql;
