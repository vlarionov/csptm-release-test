CREATE OR REPLACE FUNCTION mnt.jf_termo_data_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS snsr.termo_data AS
$body$
declare 
   l_r snsr.termo_data%rowtype; 
begin 
   l_r.packet_id := jofl.jofl_pkg$extract_number(p_attr, 'packet_id', true); 
   l_r.event_time := jofl.jofl_pkg$extract_date(p_attr, 'event_time', true); 
   l_r.tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true); 
   l_r.unit_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_id', true); 
   l_r.sensor_id := jofl.jofl_pkg$extract_number(p_attr, 'sensor_id', true); 
   l_r.val := jofl.jofl_pkg$extract_number(p_attr, 'val', true); 
   l_r.status := jofl.jofl_pkg$extract_number(p_attr, 'status', true); 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_termo_data_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r snsr.termo_data%rowtype;
begin 
   l_r := mnt.jf_termo_data_pkg$attr_to_rowtype(p_attr);

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_termo_data_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r snsr.termo_data%rowtype;
begin 
   l_r := mnt.jf_termo_data_pkg$attr_to_rowtype(p_attr);

   --insert into snsr.termo_data select l_r.*;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_termo_data_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
  l_rf_tr_id	 core.tr.tr_id%type 		:= jofl.jofl_pkg$extract_number(p_attr, 'f_tr_id', true);
  l_rf_unit_id	 core.unit.unit_id%type		:= jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_id', true);
  l_rf_sensor_id core.sensor.sensor_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_sensor_id', true);
  f_bgn_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_bgn_DT', true);
  f_end_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_end_DT', true);
begin 
 open p_rows for 
      select 
        td.packet_id, 
        td.event_time, 
        td.tr_id, 
        td.unit_id, 
        td.sensor_id, 
        td.val, 
        td.status::int status
      from snsr.termo_data td
      join core.tr tr on td.tr_id = tr.tr_id
      join core.unit2tr u2tr on tr.tr_id = u2tr.tr_id 
      join core.unit u on u2tr.unit_id = u.unit_id
      join core.sensor2unit s2un on u.unit_id = s2un.unit_id and td.sensor_id = s2un.sensor_id
where tr.tr_id = l_rf_tr_id
  and u.unit_id = l_rf_unit_id
  and s2un.sensor_id = l_rf_sensor_id 
  and td.event_time >= f_bgn_dt   
  and td.event_time <= f_end_dt ;  
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_termo_data_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r snsr.termo_data%rowtype;
begin 
   l_r := mnt.jf_termo_data_pkg$attr_to_rowtype(p_attr);

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
------------------------------------------------------------------
------------------------------------------------------------------