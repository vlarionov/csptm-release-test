CREATE OR REPLACE FUNCTION mnt.jf_monitoring_event_pkg$of_rows(IN p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                               IN p_attr       TEXT)
  RETURNS REFCURSOR
AS
$BODY$
DECLARE
  l_r                   ttb.incident%ROWTYPE;
  l_begin_dt            TIMESTAMP WITHOUT TIME ZONE := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'F_BEGIN_DT', TRUE), now() - interval '1 hours');
  l_end_dt              TIMESTAMP WITHOUT TIME ZONE := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'F_END_DT', TRUE), now());

  l_enabled_event_types INT [] := ARRAY [
  mnt.jf_monitoring_event_pkg$cn_type_red_buttom(),
  mnt.jf_monitoring_event_pkg$cn_type_over_speed(),
  mnt.jf_monitoring_event_pkg$cn_type_deviation(),
  mnt.jf_monitoring_event_pkg$cn_type_autonomic_move(),
  mnt.jf_monitoring_event_pkg$cn_type_video_loss(),
  mnt.jf_monitoring_event_pkg$cn_type_hdd_full(),
  mnt.jf_monitoring_event_pkg$cn_type_no_disk(),
  mnt.jf_monitoring_event_pkg$cn_type_all_video_loss(),
  mnt.jf_monitoring_event_pkg$cn_type_fan_fail(),
  mnt.jf_monitoring_event_pkg$cn_type_over_hdd_temp(),
  mnt.jf_monitoring_event_pkg$cn_type_hdd_fail(),
  mnt.jf_monitoring_event_pkg$cn_type_inv_pkgs_numbers(),
  mnt.jf_monitoring_event_pkg$cn_type_inv_pkgs_rate(),
  mnt.jf_monitoring_event_pkg$cn_type_pkgs_time_rate()
  ];
BEGIN
  l_r.tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', TRUE);
  OPEN p_rows FOR
  -- странный костыль, призванный помочь с ситуацией, когда запрос выполняется быстро, а через функцию -- долго
WITH q AS (
    SELECT
      incident.incident_id as event_id,
      incident.time_start as event_time,
      incident.tr_id as tr_id,
      incident.unit_id as unit_id,
      incident.incident_type_id,
      incident_params ->> 'factVal' fact_value,
      incident_params ->> 'deviationVal' deviation_value,
      incident_params -> 'pointStart' ->> 'lon' lon,
      incident_params -> 'pointStart' ->> 'lat' lat,
      incident_type.incident_type_name AS event_type_name,
      null confirmation_time,
      null confirmation_account_id,
      null AS "ROW$POLICY"
    FROM ttb.incident
      JOIN ttb.incident_type ON incident.incident_type_id = incident_type.incident_type_id
      JOIN adm.jf_monitoring_incident_pkg$get_available_incident_types(p_id_account) avb_event
        ON incident.incident_type_id = avb_event.incident_type_id
    WHERE (incident.tr_id = l_r.tr_id)
          AND incident.time_start BETWEEN l_begin_dt AND l_end_dt
          AND incident.incident_type_id = ANY (l_enabled_event_types)
    ORDER BY incident.time_start DESC
    LIMIT 500
) SELECT *
  FROM q;

END;
$BODY$
LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION mnt.jf_monitoring_event_pkg$cn_type_red_buttom()
  RETURNS INT AS
'SELECT 1 :: INT;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION mnt.jf_monitoring_event_pkg$cn_type_over_speed()
  RETURNS INT AS
'SELECT 2 :: INT;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION mnt.jf_monitoring_event_pkg$cn_type_deviation()
  RETURNS INT AS
'SELECT 3 :: INT;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION mnt.jf_monitoring_event_pkg$cn_type_autonomic_move()
  RETURNS INT AS
'SELECT 4 :: INT;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION mnt.jf_monitoring_event_pkg$cn_type_video_loss()
  RETURNS INT AS
'SELECT 7 :: INT;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION mnt.jf_monitoring_event_pkg$cn_type_hdd_full()
  RETURNS INT AS
'SELECT 8 :: INT;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION mnt.jf_monitoring_event_pkg$cn_type_no_disk()
  RETURNS INT AS
'SELECT 9 :: INT;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION mnt.jf_monitoring_event_pkg$cn_type_all_video_loss()
  RETURNS INT AS
'SELECT 10 :: INT;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION mnt.jf_monitoring_event_pkg$cn_type_fan_fail()
  RETURNS INT AS
'SELECT 11 :: INT;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION mnt.jf_monitoring_event_pkg$cn_type_over_hdd_temp()
  RETURNS INT AS
'SELECT 12 :: INT;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION mnt.jf_monitoring_event_pkg$cn_type_hdd_fail()
  RETURNS INT AS
'SELECT 13 :: INT;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION mnt.jf_monitoring_event_pkg$cn_type_inv_pkgs_numbers()
  RETURNS INT AS
'SELECT 14 :: INT;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION mnt.jf_monitoring_event_pkg$cn_type_inv_pkgs_rate()
  RETURNS INT AS
'SELECT 15 :: INT;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION mnt.jf_monitoring_event_pkg$cn_type_pkgs_time_rate()
  RETURNS INT AS
'SELECT 16 :: INT;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION mnt.jf_monitoring_event_pkg$cn_type_pkgs_diag_ok()
  RETURNS INT AS
'SELECT 17 :: INT;'
LANGUAGE SQL IMMUTABLE;
