﻿CREATE OR REPLACE FUNCTION mnt.jf_trs_work_pkg$of_rows (
  p_account_id numeric,
  p_attr text,
  out p_rows refcursor
)
RETURNS refcursor AS
$body$
 declare 
   l_query_date   timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'F_DT', true), now());
   l_order_date   timestamp := date_trunc('day', l_query_date);
   l_tr_type_id   bigint    := jofl.jofl_pkg$extract_number(p_attr, 'F_tr_type_id', true);
   l_garage_num   bigint    := jofl.jofl_pkg$extract_number(p_attr, 'F_GRG_NUM_TEXT', true);
   l_depo_id      bigint    := jofl.jofl_pkg$extract_number(p_attr, 'F_depo_id', true);
   l_route_muid   bigint    := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'MUID', true),
                                        jofl.jofl_pkg$extract_number(p_attr, 'F_route_muid', true));
   l_tr_id        bigint    := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
   
begin
   if l_tr_type_id is null and
      l_garage_num is null and
      l_depo_id    is null and
      l_tr_id      is null and
      l_route_muid is null
   then
      open p_rows for select '[]';
   else

   open p_rows for
   with tr_and_type as (
      select t.tr_id, 
      		 t.tr_type_id, 
             t.garage_num, 
             p.name as tr_type_name, 
             tc.full_name as tr_capacity
        from core.tr t
        join core.tr_type p on p.tr_type_id = t.tr_type_id
        join core.tr_model tm on tm.tr_model_id = t.tr_model_id
        join core.tr_capacity tc on tc.tr_capacity_id = tm.tr_capacity_id
       where (l_garage_num is null or t.garage_num = l_garage_num)
         and (l_depo_id is null or t.depo_id = l_depo_id)
         and (l_tr_type_id is null or p.tr_type_id= l_tr_type_id)
         and (l_tr_id is null or t.tr_id= l_tr_id)
   ),
   tr_oper_fact as (
   select q.tr_id,
          sum(q.fact_duration) as fact_duration
     from (select orl.tr_id, orr.order_round_id,
                  core.round_minutes(max(orf.time_fact)) - core.round_minutes(min(orf.time_fact)) as fact_duration
             from tt.order_list orl
             join tt.order_round orr on orr.order_list_id = orl.order_list_id
             join tt.order_fact orf on orf.order_round_id = orr.order_round_id
            where orl.sign_deleted = 0
              and orl.is_active = 1
              and orr.is_active = 1
              and orf.sign_deleted = 0
              and orl.driver_id is not null
              and orl.order_date = l_order_date
              and (l_tr_id is null or orl.tr_id = l_tr_id)
            group by orl.tr_id, orr.order_round_id
          ) q
    group by q.tr_id
   )
   select trt.tr_id, trt.garage_num, trt.tr_type_name, trt.tr_capacity,
          o.plan_rounds, o.fact_rounds,
          o.plan_begin, o.plan_end,
          o.fact_begin,
          mnt.info_pkg$get_tr_end_time_fact(l_order_date, trt.tr_id) as fact_end,
          o.plan_run_line,
          o.fact_run_line,
          o.plan_run_line - o.fact_run_line as diff_run_line,
          o.plan_run_line as plan_run_all,
          o.fact_run_line as fact_run_all,
          o.plan_run_line - o.fact_run_line as diff_run_all,
          to_char(o.plan_end - o.plan_begin, 'HH24:MI') as plan_duration,
          to_char(o.plan_end - o.plan_begin, 'HH24:MI') as plan_duration_all,
          to_char(tof.fact_duration, 'HH24:MI') as fact_duration,
          to_char(tof.fact_duration, 'HH24:MI') as fact_duration_all,
          to_char((o.plan_end - o.plan_begin) - (tof.fact_duration), 'HH24:MI') as diff_duration_all,
          l_query_date    as "F_DT"
     from (select orv.tr_id,
                  count(*)                    as plan_rounds,
                  count(orv.time_fact_end)    as fact_rounds,
                  min(orv.time_plan_begin)    as plan_begin,
                  max(orv.time_plan_end)      as plan_end,
                  min(orv.time_fact_begin)    as fact_begin,
                  sum(orv.length)             as plan_run_line,
                  sum(case 
                         when orv.order_round_status_id in (tt.const_pkg$ord_status_planned(), tt.const_pkg$ord_status_canceled()) then 
                              0
                         when orv.order_round_status_id = tt.const_pkg$ord_status_passed() then 
                              orv.length
                         when orv.order_round_status_id = tt.const_pkg$ord_status_performed() then
                              coalesce((select round(max(round_offset / 1000)::numeric, 3)
                                          from asd.tf_graph_link tgl
                                         where tgl.tr_id = orv.tr_id
                                           and tgl.order_round_id = orv.order_round_id
                                           and tgl.event_time between now() - interval '15 minutes' and now())
                                       , 0)
                         else coalesce((select round(max(round_offset / 1000)::numeric, 3)
                                          from asd.tf_graph_link tgl
                                         where tgl.tr_id = orv.tr_id    
                                           and tgl.order_round_id = orv.order_round_id   
                                           and tgl.event_time between coalesce(orv.time_fact_begin, orv.time_fact_end - interval '60 minutes') and orv.time_fact_end)
                                       ,0)
                      end) as fact_run_line
             from mnt.v_route_oper_visit orv
            where orv.order_date = l_order_date
              and orv.tr_id is not null
              and (l_tr_id is null or orv.tr_id = l_tr_id)
              and (l_route_muid is null or orv.route_muid = l_route_muid)
              and (
                orv.tr_id in (select adm.account_data_realm_pkg$get_trs_by_tr_type_and_depo(p_account_id))
                or orv.route_muid in (select route_muid
                                      from rts.route2gis
                                      where route_id in (select adm.account_data_realm_pkg$get_routes(p_account_id)))
              )
            group by orv.tr_id
          ) o
     join tr_and_type as trt on trt.tr_id = o.tr_id
     left join tr_oper_fact as tof on tof.tr_id = o.tr_id
    order by trt.garage_num;
     
     end if;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;