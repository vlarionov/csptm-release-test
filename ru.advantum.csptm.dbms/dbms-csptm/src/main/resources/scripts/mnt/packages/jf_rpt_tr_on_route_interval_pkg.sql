CREATE OR REPLACE FUNCTION mnt.jf_rpt_tr_on_route_interval_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
AS $function$
declare
  l_route_variant bigint := jofl.jofl_pkg$extract_number(p_attr, 'F_CURRENT_ROUTE_VARIANT_MUID', TRUE)::bigint;
  f_bgn_dt		timestamp := jofl.jofl_pkg$extract_date(p_attr, 'F_BGN_DT', true);
  f_end_dt		timestamp := jofl.jofl_pkg$extract_date(p_attr, 'F_END_DT', true);
begin
open p_rows for
    select order_fact.time_fact as time_fact,
      v_rounds_attrib.timetable_entry_num as exit_num,
      v_rounds_attrib.direction_id as direction
    from
      tt.order_fact
      join mnt.v_rounds_attrib on v_rounds_attrib.order_round_id = order_fact.order_round_id
    where order_fact.time_fact between f_bgn_dt and f_end_dt
    and v_rounds_attrib.route_variant_muid = l_route_variant
    and (   order_fact.stop_place_muid = v_rounds_attrib.stop_place_a_muid
            or
            order_fact.stop_place_muid = v_rounds_attrib.stop_place_b_muid)
        ;
end;
$function$
LANGUAGE plpgsql;