CREATE OR REPLACE FUNCTION mnt.jf_orp_by_geometry_pkg$of_rows(IN p_id_account NUMERIC, OUT p_rows REFCURSOR, IN p_attr TEXT)
  RETURNS REFCURSOR
AS
  $BODY$
declare
  l_geom geometry;
begin
  l_geom := ST_GeomFromText(jofl.jofl_pkg$extract_varchar(p_attr, 'GEOM', true));
  open p_rows for
    select tp.name, tp.address, core.ts_type_as_string(tp.has_bus, tp.has_trolley, tp.has_tram, tp.has_speedtram, 10) as type
    from gis.transport_parkings as tp
    where ST_Intersects(tp.geom, l_geom);
end;
  $BODY$
LANGUAGE plpgsql;