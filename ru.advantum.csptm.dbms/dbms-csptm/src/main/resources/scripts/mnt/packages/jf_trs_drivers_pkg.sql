﻿CREATE OR REPLACE FUNCTION mnt.jf_trs_drivers_pkg$of_rows (
  p_account_id numeric,
  p_attr text,
  out p_rows refcursor
)
RETURNS refcursor AS
$body$
 declare
   l_query_date   timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'F_DT', true), now());
   l_order_date   timestamp := date_trunc('day', l_query_date);
   l_tr_id        bigint    := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
begin
   open p_rows for
   with drv_oper as (
   select q.driver_id,
          min(q.begin_plan)  as begin_plan,
          max(q.end_plan)    as end_plan,
          sum(q.time_plan)   as time_plan,
          min(q.begin_oper)  as begin_oper,
          max(q.end_oper)    as end_oper,
          sum(q.time_oper)   as time_oper,
          timetable_entry_num,
          shift      
     from (select orl.driver_id, orr.order_round_id,
                  min(oro.time_oper) as begin_plan,
                  max(oro.time_oper) as end_plan,
                  max(oro.time_plan) - min(oro.time_plan) as time_plan,
                  min(oro.time_oper) as begin_oper,
                  max(oro.time_oper) as end_oper,
                  max(oro.time_oper) - min(oro.time_oper) as time_oper,
                  tte.timetable_entry_num,
                  orl.shift                  
             from tt.order_list orl
             join tt.order_round orr on orr.order_list_id = orl.order_list_id
             join tt.order_oper oro on oro.order_round_id = orr.order_round_id
             join tt.timetable_entry tte on tte.timetable_entry_id = orl.timetable_entry_id
            where orl.sign_deleted = 0
              and orl.is_active = 1
              and orr.is_active = 1
              and oro.sign_deleted = 0
              and oro.is_active = 1
              and orl.driver_id is not null
              and orl.tr_id = l_tr_id
              and orl.order_date = l_order_date
            group by orl.driver_id, orr.order_round_id, tte.timetable_entry_num, orl.shift
          ) q
    group by q.driver_id, timetable_entry_num, shift  
   ),
   drv_fact as (
   select q.driver_id,
          min(q.begin_fact)  as begin_fact,
          sum(q.time_fact)   as time_fact
     from (select orl.driver_id, orr.order_round_id,
                  min(orf.time_fact) as begin_fact,
                  core.round_minutes(max(orf.time_fact)) - core.round_minutes(min(orf.time_fact)) as time_fact
             from tt.order_list orl
             join tt.order_round orr on orr.order_list_id = orl.order_list_id
             join tt.order_fact orf on orf.order_round_id = orr.order_round_id
            where orl.sign_deleted = 0
              and orl.is_active = 1
              and orr.is_active = 1
              and orf.sign_deleted = 0
              and orl.driver_id is not null
              and orl.tr_id = l_tr_id
              and orl.order_date = l_order_date
            group by orl.driver_id, orr.order_round_id
          ) q
    group by q.driver_id
   ),
   drv_rounds as (
   select orv.driver_id,
          count(*) as rounds_oper,
          count(case orv.order_round_status_id  when tt.const_pkg$ord_status_passed()  then 1 else null end) as rounds_fact
     from mnt.v_round_oper_visit orv
    where orv.order_date = l_order_date
      and orv.tr_id = l_tr_id
    group by orv.driver_id
   )
   select drv.driver_id, drv.tab_num,
          drv.driver_last_name || ' ' || drv.driver_name || ' ' || drv.driver_middle_name as driver_name,
          drr.rounds_oper, drr.rounds_fact,
          dro.begin_plan, dro.end_plan, 
          to_char(dro.time_plan, 'HH24:MI') as time_plan,
          dro.begin_oper, dro.end_oper, 
          to_char(dro.time_oper, 'HH24:MI') as time_oper, 
          drf.begin_fact,
          mnt.info_pkg$get_driver_end_time_fact(l_order_date, drv.driver_id) as end_fact,
          to_char(drf.time_fact, 'HH24:MI') as time_fact,
          dro.timetable_entry_num, 
          dro.shift
     from tt.driver drv
     join drv_oper dro on dro.driver_id = drv.driver_id
     left join drv_fact drf on drf.driver_id = drv.driver_id
     left join drv_rounds drr on drr.driver_id = drv.driver_id
    order by drv.driver_last_name;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;


create or replace function mnt.jf_trs_drivers_pkg$of_dial
  (p_account_id          in numeric,
   p_attr                in text)
returns text
-- ========================================================================
-- Голосовой вызов
-- ========================================================================
as $$ declare
   l_tr_id               bigint := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
   l_driver_id           integer := null;
   l_unit_id             integer;
   l_call_request_id     bigint := null;
   l_redial              boolean := false;
   l_result              text;
begin
   
   select min(unit_id) into l_unit_id
     from core.unit2tr utr
     join core.unit_bnst unt on unt.unit_bnst_id = utr.unit_id
    where tr_id = l_tr_id;
   
   if l_unit_id is null then
      raise exception '<<Не найден блок для связи с ТС>>';
   end if;
   
   l_result := voip.voice_call_pkg$call_to_unit(p_account_id::bigint, l_tr_id, l_unit_id, l_call_request_id, l_driver_id, l_redial);
   return l_result;
end;
$$ language plpgsql volatile security invoker;
