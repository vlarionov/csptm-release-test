CREATE OR REPLACE FUNCTION mnt.jf_irma_sensor_data_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
  l_rf_tr_id	 core.tr.tr_id%type 		:= jofl.jofl_pkg$extract_number(p_attr, 'f_tr_id', true);
  l_rf_unit_id	 core.unit.unit_id%type		:= jofl.jofl_pkg$extract_number(p_attr, 'f_equipment_id', true);
  l_rf_sensor_id core.sensor.sensor_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_sensor_id', true);
  f_bgn_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_bgn_DT', true);
  f_end_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_end_DT', true);
begin 
 open p_rows for 
      select 
        isd.packet_id, 
        isd.event_time, 
        isd.sensor_id, 
        isd.tr_id, 
        isd.in1, 
        isd.in2, 
        isd.in3, 
        isd.in4, 
        isd.out1, 
        isd.out2, 
        isd.out3, 
        isd.out4, 
        isd.has_door1::int has_door1,
        isd.has_door2::int has_door2,
        isd.has_door3::int has_door3,
        isd.has_door4::int has_door4,
        isd.is_closed_door1::int is_closed_door1,
        isd.is_closed_door2::int is_closed_door2,
        isd.is_closed_door3::int is_closed_door3,
        isd.is_closed_door4::int is_closed_door4
      from snsr.irma_sensor_data isd
      join core.tr tr on isd.tr_id = tr.tr_id
      join core.unit2tr u2tr on tr.tr_id = u2tr.tr_id 
      join core.unit u on u2tr.unit_id = u.unit_id
      join core.sensor2unit s2un on u.unit_id = s2un.unit_id and isd.sensor_id = s2un.sensor_id
where tr.tr_id = l_rf_tr_id
  and u.unit_id = l_rf_unit_id
  and s2un.sensor_id = l_rf_sensor_id 
  and isd.event_time >= f_bgn_dt   
  and isd.event_time <= f_end_dt ; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_irma_sensor_data_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r snsr.irma_sensor_data%rowtype;
begin 
   l_r := mnt.jf_irma_sensor_data_pkg$attr_to_rowtype(p_attr);

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_irma_sensor_data_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r snsr.irma_sensor_data%rowtype;
begin 
   l_r := mnt.jf_irma_sensor_data_pkg$attr_to_rowtype(p_attr);

   --insert into snsr.irma_sensor_data select l_r.*;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_irma_sensor_data_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r snsr.irma_sensor_data%rowtype;
begin 
   l_r := mnt.jf_irma_sensor_data_pkg$attr_to_rowtype(p_attr);

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_irma_sensor_data_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS snsr.irma_sensor_data AS
$body$
declare 
   l_r snsr.irma_sensor_data%rowtype; 
begin 
   l_r.packet_id := jofl.jofl_pkg$extract_number(p_attr, 'packet_id', true); 
   l_r.event_time := jofl.jofl_pkg$extract_date(p_attr, 'event_time', true); 
   l_r.sensor_id := jofl.jofl_pkg$extract_number(p_attr, 'sensor_id', true); 
   l_r.tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true); 
   l_r.in1 := jofl.jofl_pkg$extract_varchar(p_attr, 'in1', true); 
   l_r.in2 := jofl.jofl_pkg$extract_varchar(p_attr, 'in2', true); 
   l_r.in3 := jofl.jofl_pkg$extract_varchar(p_attr, 'in3', true); 
   l_r.in4 := jofl.jofl_pkg$extract_varchar(p_attr, 'in4', true); 
   l_r.out1 := jofl.jofl_pkg$extract_varchar(p_attr, 'out1', true); 
   l_r.out2 := jofl.jofl_pkg$extract_varchar(p_attr, 'out2', true); 
   l_r.out3 := jofl.jofl_pkg$extract_varchar(p_attr, 'out3', true); 
   l_r.out4 := jofl.jofl_pkg$extract_varchar(p_attr, 'out4', true); 
   l_r.has_door1 := jofl.jofl_pkg$extract_boolean(p_attr, 'has_door1', true); 
   l_r.has_door2 := jofl.jofl_pkg$extract_boolean(p_attr, 'has_door2', true); 
   l_r.has_door3 := jofl.jofl_pkg$extract_boolean(p_attr, 'has_door3', true); 
   l_r.has_door4 := jofl.jofl_pkg$extract_boolean(p_attr, 'has_door4', true); 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------------------------------------------------------------------