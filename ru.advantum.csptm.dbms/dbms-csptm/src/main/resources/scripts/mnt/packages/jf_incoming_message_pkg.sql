CREATE OR REPLACE FUNCTION mnt.jf_incoming_message_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS snsr.incoming_message AS
$body$
declare 
   l_r snsr.incoming_message%rowtype; 
begin 
   l_r.packet_id := jofl.jofl_pkg$extract_number(p_attr, 'packet_id', true); 
   l_r.event_time := jofl.jofl_pkg$extract_date(p_attr, 'event_time', true); 
   l_r.tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true); 
   l_r.message_text := jofl.jofl_pkg$extract_varchar(p_attr, 'message_text', true); 

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-----------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_incoming_message_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r snsr.incoming_message%rowtype;
begin 
   l_r := mnt.jf_incoming_message_pkg$attr_to_rowtype(p_attr);

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-----------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_incoming_message_pkg$of_insert (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r snsr.incoming_message%rowtype;
begin 
   l_r := mnt.jf_incoming_message_pkg$attr_to_rowtype(p_attr);

   --insert into snsr.incoming_message select l_r.*;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-----------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_incoming_message_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
  l_rf_tr_id	 core.tr.tr_id%type 		:= jofl.jofl_pkg$extract_number(p_attr, 'f_tr_id', true);
  f_bgn_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_bgn_DT', true);
  f_end_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_end_DT', true);
  l_available_trs_ids    BIGINT [];
begin
  /* вызов общей фнкции - из метода of_rows основного окна и из of_rows детали */
  select mnt.jf_incoming_message_pkg$get_rows (p_id_account, p_attr) into p_rows;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-----------------------------------------------------------------------
CREATE OR REPLACE FUNCTION mnt.jf_incoming_message_pkg$of_update (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r snsr.incoming_message%rowtype;
begin 
   l_r := mnt.jf_incoming_message_pkg$attr_to_rowtype(p_attr);

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-----------------------------------------------------------------------

CREATE OR REPLACE FUNCTION mnt.jf_incoming_message_pkg$get_rows (
  p_id_account numeric,
  p_attr text
)
/* общая фнкция, возвращающая данные */
RETURNS refcursor AS
$body$
declare
  l_rf_tr_id	 core.tr.tr_id%type 		:= jofl.jofl_pkg$extract_number(p_attr, 'f_tr_id', true);
  f_bgn_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_bgn_DT', true);
  f_end_dt		timestamp without time zone := jofl.jofl_pkg$extract_date(p_attr, 'f_end_DT', true);
  l_refcurs refcursor;
begin
 open l_refcurs for
      select
        im.packet_id,
        im.event_time,
        im.tr_id,
        im.message_text,
        tr.garage_num,
        u.unit_num,
        im.message_id
      from snsr.incoming_message im
      join core.tr tr on tr.tr_id = im.tr_id
      join core.unit2tr u2tr on u2tr.tr_id = im.tr_id
      join core.unit u on u.unit_id = u2tr.unit_id
	  join core.traffic tf on tf.event_time = im.event_time and tf.packet_id = im.packet_id and tf.tr_id = tr.tr_id and tf.unit_id = u.unit_id
 where (im.tr_id = l_rf_tr_id or l_rf_tr_id is null)
   and im.event_time >= f_bgn_dt
   and im.event_time <= f_end_dt
   and tr.tr_id in (select adm.account_data_realm_pkg$get_trs_by_all(p_id_account));
 return l_refcurs;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
