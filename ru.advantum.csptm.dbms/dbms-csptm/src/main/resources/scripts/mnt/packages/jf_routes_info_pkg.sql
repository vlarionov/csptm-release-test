create or replace function mnt."jf_routes_info_pkg$of_rows"(p_account_id numeric, p_attr text, OUT p_rows refcursor) returns refcursor
language plpgsql
as $$
declare 
   l_query_date         timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'F_DT', true), now());
   l_order_date         timestamp := date_trunc('day', l_query_date);
   l_tr_type_id         bigint    := jofl.jofl_pkg$extract_number(p_attr, 'F_tr_type_id', true);
   l_depo_id            bigint    := jofl.jofl_pkg$extract_number(p_attr, 'F_depo_id', true);
   l_route_muid         bigint    := jofl.jofl_pkg$extract_number(p_attr, 'f_route_muid', true);
   l_stop_order_num     bigint    := coalesce(asd.get_constant('RTI_STOP_ORDER_NUM')::bigint, 1);
   l_trajectory_type_muid  bigint := coalesce(asd.get_constant('RTI_TRAJECTORY_TYPE_MUID')::bigint, 1);
   l_route_round_type_muid bigint := coalesce(asd.get_constant('RTI_ROUTE_ROUND_TYPE_MUID')::bigint, 1);
   l_interval_diff      interval  := (coalesce(asd.get_constant('RTI_INTERVAL_DIFF'), 61)::text || ' min')::interval;
begin
/*   open p_rows for
   with ts_oper_count as
   (select orv.route_muid,
           count(distinct orv.tr_id) as tr_oper_online,
           count(distinct orv.round_id) as rnd_oper_online
      from mnt.v_route_oper_visit orv
     where orv.order_date = l_order_date
       and l_query_date between orv.time_plan_begin and orv.time_plan_end
       and (l_route_muid is null or orv.route_muid = l_route_muid)
     group by orv.route_muid
   ),
   ts_fact_count as
   (select orv.route_muid,
           count(distinct orv.tr_id) as tr_fact_online,
           count(distinct orv.round_id) as rnd_fact_online
      from mnt.v_route_oper_visit orv
     where orv.order_date = l_order_date
       and (l_route_muid is null or orv.route_muid = l_route_muid)
       and orv.order_round_status_id = tt.const_pkg$ord_status_performed()
       and exists (select 1 from asd.tf_graph_link tgl
                    where tgl.tr_id = orv.tr_id
                      and tgl.event_time between l_query_date - interval '15 minutes' and l_query_date
                      and tgl.order_round_id = orv.order_round_id
                  )
     group by orv.route_muid
   ),
   ts_plan_count as
   (select tmt.route_muid,
           count(distinct tte.timetable_entry_id) as tr_plan_online,
           count(distinct tte.timetable_entry_id) as rnd_plan_online
      from tt.timetable tmt
      join tt.timetable_entry tte on tte.timetable_id = tmt.timetable_id
      join tt.round rnd on rnd.timetable_entry_id = tte.timetable_entry_id
     where tmt.begin_date <= l_query_date
       and (tmt.end_date is null or tmt.end_date > l_query_date)
       and tmt.sign_deleted = 0
       and tte.sign_deleted = 0
       and rnd.sign_deleted = 0
       and exists(select 1 from tt.timetable_setting2week_day tsw
                   where tsw.week_day_id = extract(dow from l_order_date)
                     and tsw.timetable_setting_id = tmt.timetable_setting_id
                 )
       and exists (select 1 from (select min(time_begin) as time_plan_begin,
                                         max(time_begin) as time_plan_end
                                    from tt.timetable_plan ttp
                                   where ttp.round_id = rnd.round_id
                                     and ttp.sign_deleted = 0
                                 ) q
                           where l_query_date between l_order_date + q.time_plan_begin
                                                  and l_order_date + q.time_plan_end
                  )
     group by tmt.route_muid
   ),
   rnd_oper_count as
   (select route_muid,
           min(time_plan_begin)     as time_plan_begin,
           min(time_fact_begin)     as time_fact_begin,
           max(time_plan_end)       as time_plan_end,
           sum(case when time_plan_end < l_query_date then 1 else 0 end) as rnd_oper,
           sum(case when time_fact_end < l_query_date then 1 else 0 end) as rnd_fact
      from mnt.v_route_oper_visit
     where order_date = l_order_date
       and (l_route_muid is null or route_muid = l_route_muid)
     group by route_muid
   ),
   plan_interval as
   (select route_muid,
           min(time_oper - prev_oper) as min_oper_diff,
           max(time_oper - prev_oper) as max_oper_diff,
           avg(time_oper - prev_oper) as avg_oper_diff
      from (select orv.route_muid,
                   oro.time_oper,
                   lag(oro.time_oper, 1, oro.time_oper) over (partition by orv.route_muid order by oro.time_oper) as prev_oper
              from mnt.v_route_oper_visit orv
              join tt.order_oper oro on oro.order_round_id = orv.order_round_id
             where orv.order_date = l_order_date
               and orv.trajectory_type_muid = l_trajectory_type_muid
               and orv.route_round_type_muid = l_route_round_type_muid
               and oro.stop_order_num = l_stop_order_num
               and (l_route_muid is null or orv.route_muid = l_route_muid)
               and oro.time_oper between now() - l_interval_diff and now()
           ) q
     where (time_oper - prev_oper) >= interval '1 min'
     group by route_muid
   ),
   fact_interval as
   (select q.route_muid,
           min(time_fact - prev_fact) as min_fact_diff,
           max(time_fact - prev_fact) as max_fact_diff,
           avg(time_fact - prev_fact) as avg_fact_diff
      from (select orv.route_muid,
                   orf.time_fact,
                   lag(orf.time_fact, 1, orf.time_fact) over (partition by orv.route_muid order by orf.time_fact) as prev_fact
              from mnt.v_route_oper_visit orv
              join tt.order_fact orf on orf.order_round_id = orv.order_round_id
             where orv.order_date = now()::date::timestamp
               and orv.trajectory_type_muid = l_trajectory_type_muid
               and orv.route_round_type_muid = l_route_round_type_muid
               and orf.stop_order_num = l_stop_order_num
               and (l_route_muid is null or orv.route_muid = l_route_muid)
               and orf.time_fact between now() - l_interval_diff and now()
           ) q
     where (time_fact - prev_fact) >= interval '1 min'
     group by q.route_muid
   )
   select r.muid::text        as "MUID",
          rtk.name            as "RTK_NAME",
          rtk.short_name      as "TR_TYPE",
          r.number            as "ROUTE_NUMBER",
          r.open_date         as "OPEN_DATE",
          r.close_date        as "CLOSE_DATE",
          r.insert_date       as "INSERT_DATE",
          r.update_date       as "UPDATE_DATE",
          toc.tr_oper_online  as "TR_OPER_ONLINE",
          tfc.tr_fact_online  as "TR_FACT_ONLINE",
          tpc.tr_plan_online  as "TR_PLAN_ONLINE",
          toc.tr_oper_online - tfc.tr_fact_online as "TR_DIFF_ONLINE",
          toc.rnd_oper_online as "RND_OPER_ONLINE",
          tfc.rnd_fact_online as "RND_FACT_ONLINE",
          tpc.rnd_plan_online as "RND_PLAN_ONLINE",
          roc.rnd_oper        as "RND_OPER",
          roc.rnd_fact        as "RND_FACT",
          roc.time_plan_begin as "TIME_PLAN_BEGIN",
          roc.time_fact_begin as "TIME_FACT_BEGIN",
          roc.time_plan_end   as "TIME_PLAN_END",
          null::timestamp     as "TIME_FACT_END",
--        mnt.info_pkg$get_route_end_time_fact (l_order_date, r.muid) as "TIME_FACT_END",
          core.interval2minutes(pin.min_oper_diff) as "MIN_OPER_DIFF",
          core.interval2minutes(pin.max_oper_diff) as "MAX_OPER_DIFF",
          core.interval2minutes(pin.avg_oper_diff) as "AVG_OPER_DIFF",
          core.interval2minutes(fin.min_fact_diff) as "MIN_FACT_DIFF",
          core.interval2minutes(fin.max_fact_diff) as "MAX_FACT_DIFF",
          core.interval2minutes(fin.avg_fact_diff) as "AVG_FACT_DIFF",
          '[' || json_build_object('CAPTION', 'Производственные рейсы', 'JUMP',
                 json_build_object('METHOD', 'GIS.ROUTE_ROUNDS', 'ATTR', '{}')) || ']' "JUMP_TO_RR",
          '[' || json_build_object('CAPTION', 'Технологические рейсы', 'JUMP',
                 json_build_object('METHOD', 'GIS.ROUTE_NULL_ROUNDS', 'ATTR', '{}')) || ']' "JUMP_TO_RNR",
          '[' || json_build_object('CAPTION', 'Работа ТС', 'JUMP',
                 json_build_object('METHOD', 'MNT.TRS_WORK', 'ATTR', '{"F_route_muid":"' || r.muid::text || '"}')) || ']' "JUMP_TO_TRW"
     from gis.routes r
     join core.tr_type        rtk on rtk.transport_kind_muid = r.transport_kind_muid
     join gis.route_variants  rv  on rv.muid = r.current_route_variant_muid
     left join ts_oper_count  toc on toc.route_muid = r.muid
     left join ts_fact_count  tfc on tfc.route_muid = r.muid
     left join ts_plan_count  tpc on tpc.route_muid = r.muid
     left join rnd_oper_count roc on roc.route_muid = r.muid
     left join plan_interval  pin on pin.route_muid = r.muid
     left join fact_interval  fin on fin.route_muid = r.muid
    where (l_tr_type_id is null or rtk.tr_type_id = l_tr_type_id)
      and (l_route_muid is null or r.muid = l_route_muid)
      and (l_depo_id is null or
           exists(select 1 from core.depo dep
                    join gis.parks2routes ptr on ptr.park_muid = dep.park_muid
                   where ptr.route_muid = r.muid
                     and dep.depo_id = l_depo_id)
          )
      and (rtk.tr_type_id in(select adm.account_data_realm_pkg$get_tr_types(p_account_id)) or r.muid in (
        select route_muid
          from rts.route2gis
        where route_id in (select  adm.account_data_realm_pkg$get_routes(p_account_id))
      ))
    order by rtk.name, r.number; */
  open p_rows for
    with prm as (select date_trunc('day', now()::timestamp)::date ld_order_date, interval '61 min' l_interval ),
        v as (  select * from ttb.tt_variant v where now()::timestamp <@ v.action_period and v.parent_tt_variant_id is not null and not v.sign_deleted
               /* select distinct v.*
                from ttb.tt_action a join ttb.tt_out t on a.tt_out_id = t.tt_out_id
                     join ttb.tt_variant v on t.tt_variant_id = v.tt_variant_id
                where now()::timestamp <@ a.action_range
                  and v.parent_tt_variant_id is not null*/
              ),
        f as (select  q2.tt_variant_id,
                      count(distinct q2.tr_id) filter(where now()::timestamp <@ q2.action_range) ts_fact,
                      count(distinct q2.tt_action_id) filter(where q2.is_prod and q2.round_status_id = ttb.round_status_pkg$in_progress()) round_in_progress,
                      count(distinct q2.tt_action_id) filter(where q2.is_prod and q2.round_status_id in (ttb.round_status_pkg$passed(), ttb.round_status_pkg$error())) round_fact,
                      max(q2.time_fact_begin) filter(where is_first_round and q2.is_first_stop) open_fact,
                      max(q2.time_fact_begin) filter(where is_last_round and q2.is_first_stop) close_fact

              from
                (
                  select st2r.order_num,
                         af.time_fact_begin,
                         st2r.order_num = first_value(st2r.order_num) over(partition by q.tt_action_id order by st2r.order_num) is_first_stop,
                         first_value(st2r.order_num) over(partition by q.tt_action_id order by st2r.order_num) first_stop,
                         q.*
                  from
                    ( select v.tt_variant_id,
                             v.ttv_name,
                             a.tt_action_id,
                             a.action_range,
                             t.tr_id,
                             ar.round_id,
                             ar.round_status_id,
                             a.is_prod,
                             a.tt_action_id = first_value(a.tt_action_id) over(partition by t.tt_variant_id order by case when a.is_prod then a.action_range end nulls last) is_first_round,
                             a.tt_action_id = first_value(a.tt_action_id) over(partition by t.tt_variant_id order by case when a.is_prod then a.action_range end  desc nulls last ) is_last_round
                      from v join ttb.tt_out t on t.tt_variant_id = v.tt_variant_id
                           join (select ttb.action_type_pkg$is_production_round(a.action_type_id) is_prod, a.* from ttb.tt_action a) a on a.tt_out_id = t.tt_out_id
                           left join ttb.tt_action_round ar on ar.tt_action_id = a.tt_action_id
                    ) q left join ttb.tt_action_fact af on q.tt_action_id = af.tt_action_id
                      left join rts.stop_item2round st2r on af.stop_item2round_id = st2r.stop_item2round_id and q.round_id = st2r.round_id
                ) q2
              group by q2.tt_variant_id
            ),
        p as (select v.parent_tt_variant_id,
                     count(distinct tt_tr.tt_tr_id) ts_plan,
                     count(distinct arp.tt_action_id) round_plan,
                     core.to_utc(min(lower(ap.action_range)) filter(where ttb.action_type_pkg$is_production_round(ap.action_type_id))) open_plan,
                     core.to_utc(max(lower(ap.action_range)) filter(where ttb.action_type_pkg$is_production_round(ap.action_type_id))) close_plan
              from v join ttb.tt_out tp on tp.tt_variant_id = v.parent_tt_variant_id
                   join ttb.tt_action ap on ap.tt_out_id = tp.tt_out_id
                   join ttb.tt_action_round arp on arp.tt_action_id = ap.tt_action_id
                   join ttb.tt_tr tt_tr on v.parent_tt_variant_id = tt_tr.tt_variant_id
              group by v.parent_tt_variant_id
             )
    select  v.tt_variant_id, v.route_variant_id, v.parent_tt_variant_id, v.ttv_name, r.route_num,
            trt.short_name tr_type_name,
            trt.name tr_type_full_name,
            lower(rv.action_period)::date rv_open,
            upper(rv.action_period)::date rv_close,
            p.ts_plan,
            f.ts_fact,
            p.ts_plan - f.ts_fact plan_fact_diff,
            p.round_plan,
            f.round_in_progress,
            f.round_fact,
            p.open_plan,
            f.open_fact,
            p.close_plan,
            f.close_fact,
            core.interval2minutes(ttb.get_avg_interval_fact_for_route(
                                    p_order_date := prm.ld_order_date
                                    ,p_route_id := r.route_id
                                    ,p_period_start := now()::timestamp - prm.l_interval
                                    ,p_period_finish := now()::timestamp
                                    ,p_move_direction_id := 2
                                  )
            ) as fact_diff_avg,
            core.interval2minutes(ttb.get_avg_interval_plan_for_route(
                                    p_order_date := prm.ld_order_date
                                    ,p_route_id := r.route_id
                                    ,p_period_start := now()::timestamp - prm.l_interval
                                    ,p_period_finish := now()::timestamp
                                    ,p_move_direction_id := 2
                                  )
            ) as plan_diff_avg,
            core.interval2minutes(ttb.get_max_interval_fact_for_route(
                                    p_order_date := prm.ld_order_date
                                    ,p_route_id := r.route_id
                                    ,p_period_start := now()::timestamp - prm.l_interval
                                    ,p_period_finish := now()::timestamp
                                    ,p_move_direction_id := 2
                                  )
            ) as fact_diff_max,
            core.interval2minutes(ttb.get_min_interval_fact_for_route(
                                    p_order_date := prm.ld_order_date
                                    ,p_route_id := r.route_id
                                    ,p_period_start := now()::timestamp - prm.l_interval
                                    ,p_period_finish := now()::timestamp
                                    ,p_move_direction_id := 2
                                  )
            ) as fact_diff_min,
          '[' || json_build_object('CAPTION', 'Производственные рейсы', 'JUMP',
                 json_build_object('METHOD', 'GIS.ROUTE_ROUNDS', 'ATTR', '{}')) || ']' "JUMP_TO_RR",
          '[' || json_build_object('CAPTION', 'Технологические рейсы', 'JUMP',
                 json_build_object('METHOD', 'GIS.ROUTE_NULL_ROUNDS', 'ATTR', '{}')) || ']' "JUMP_TO_RNR",
          '[' || json_build_object('CAPTION', 'Работа ТС', 'JUMP',
                 json_build_object('METHOD', 'MNT.TRS_WORK', 'ATTR', '{"F_route_muid":"' || r2g.route_muid::text || '"}')) || ']' "JUMP_TO_TRW"
    from v join f on v.tt_variant_id = f.tt_variant_id
      join p on v.parent_tt_variant_id = p.parent_tt_variant_id
      join rts.route_variant rv on v.route_variant_id = rv.route_variant_id
      join rts.route r on rv.route_id = r.route_id
      join rts.route2gis r2g on r.route_id = r2g.route_id
      join core.tr_type trt on r.tr_type_id = trt.tr_type_id
      ,
      prm
    where r.route_id in (select  adm.account_data_realm_pkg$get_routes(p_account_id))
      and (l_tr_type_id is null or r.tr_type_id = l_tr_type_id)
      and (l_route_muid is null or r2g.route_muid = l_route_muid)
     /*       and (l_depo_id is null or
           exists(select 1 from core.depo dep
                    join gis.parks2routes ptr on ptr.park_muid = dep.park_muid
                   where ptr.route_muid = r.muid
                     and dep.depo_id = l_depo_id)
          )*/
    order by trt.short_name, r.route_num
;
end;
$$;


