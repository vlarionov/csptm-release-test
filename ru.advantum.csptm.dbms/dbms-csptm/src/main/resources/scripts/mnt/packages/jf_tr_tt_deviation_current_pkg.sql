CREATE OR REPLACE FUNCTION mnt.jf_tr_tt_deviation_current_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare
  l_tr_id        bigint    := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
begin
  open p_rows for
  select time_plan,
         time_fact,
    round(EXTRACT(EPOCH FROM deviation)/60) deviation
  from mnt.get_current_timetable_deviation(l_tr_id);

end;
$function$
;

