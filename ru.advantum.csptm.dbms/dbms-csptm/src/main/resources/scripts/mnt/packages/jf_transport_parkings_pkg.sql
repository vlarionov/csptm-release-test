﻿
create or replace function mnt.jf_transport_parkings_pkg$of_rows
  (p_id_account   in numeric,
   p_rows         out refcursor,
   p_attr         in text)
returns refcursor
as $$ declare
   F_NAME     gis.transport_parkings.name%type 	  := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_NAME',    true), '');
   F_ADDRESS  gis.transport_parkings.address%type := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_ADDRESS', true), '');
begin 
   open p_rows for
   select p.muid::text        as "MUID",
          p.name              as "NAME",
          p.address           as "ADDRESS", 
          p.phone             as "PHONE",
          p.capacity          as "CAPACITY",
          substring((case when p.has_bus     <> 0 then ', Автобус'    else '' end ||
                     case when p.has_trolley <> 0 then ', Троллейбус' else '' end ||         
                     case when p.has_trolley <> 0 then ', Трамвай'    else '' end ||         
                     case when p.has_trolley <> 0 then ', Скоростной трамвай' else '' end) from 3) as "TR_KINDS",
          st_astext(p.geom)	as "GEOM"
     from gis.transport_parkings p
    where p.sign_deleted = 0
	and (F_NAME = p.name or F_NAME = '')
  	and (F_ADDRESS = p.address or F_ADDRESS = '');
end;
$$ language 'plpgsql' volatile security invoker;
