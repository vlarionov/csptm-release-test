﻿create or replace function mnt.jf_trs_stops_run_pkg$of_rows
  (p_account_id               in  numeric,
   p_attr                     in  text,
   p_rows                     out refcursor)
returns refcursor
as $$ declare 
   l_query_date        timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr, 'F_QUERY_DT', true), now());
   l_order_date        timestamp := date_trunc('day', l_query_date);
   l_stop_place_muid   bigint    := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'F_STOP_PLACE_MUID', true), jofl.jofl_pkg$extract_number(p_attr, 'MUID', TRUE));
   l_tr_type_id         bigint    := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id', true);
   l_route_muid        bigint    := jofl.jofl_pkg$extract_number(p_attr, 'f_route_muid', true);
   l_garage_num        bigint    := jofl.jofl_pkg$extract_number(p_attr, 'F_GRG_NOMER_TEXT', true);
   
begin
   open p_rows for
   with stop_interval as (
   select oro.order_oper_id, orf.time_fact,
          core.interval2minutes(oro.time_oper - lag(oro.time_oper, 1, oro.time_oper) over (partition by grv.route_muid order by oro.time_oper))::text as interval_route,
          core.interval2minutes(oro.time_oper - lag(oro.time_oper, 1, oro.time_oper) over (order by oro.time_oper))::text as interval_any,
          core.interval2minutes(orf.time_fact - lag(orf.time_fact, 1, orf.time_fact) over (partition by grv.route_muid order by orf.time_fact))::text as fact_interval_route,
          core.interval2minutes(orf.time_fact - lag(orf.time_fact, 1, orf.time_fact) over (order by orf.time_fact))::text as fact_interval_any
     from tt.order_list orl
     join tt.order_round orr on orr.order_list_id = orl.order_list_id
     join tt.order_oper oro on oro.order_round_id = orr.order_round_id
     left join tt.order_fact orf on orf.order_round_id = orr.order_round_id
                                and orf.stop_place_muid = oro.stop_place_muid
                                and orf.stop_order_num = oro.stop_order_num
                                and orf.sign_deleted = 0
     join tt.round rnd on rnd.round_id = orr.round_id
     join gis.route_trajectories gtj on gtj.muid = rnd.route_trajectory_muid
     join gis.route_rounds grr on grr.muid = gtj.route_round_muid
     join gis.route_variants grv on grv.muid = grr.route_variant_muid
    where oro.stop_place_muid = l_stop_place_muid
      and orl.order_date = l_order_date
      and orl.tr_id is not null
      and orl.sign_deleted = 0
      and orl.is_active = 1
      and orr.is_active = 1
      and oro.is_active = 1
      and oro.sign_deleted = 0
   )
   select oro.time_oper   as time_plan,
          sti.time_fact,
          stt.name        as stop_name,
          typ.name        as tr_type_name, 
          t.garage_num,
          tme.timetable_entry_num, 
          grt.number      as route_number,
          rnd.round_type, rnd.round_code,
          sti.interval_route,
          sti.interval_any,
          sti.fact_interval_route,
          sti.fact_interval_any,
          case gtj.trajectory_type_muid when 1 then 'Прямое' else 'Обратное' end as direction_name
     from tt.order_list orl
     join core.tr t on t.tr_id = orl.tr_id
     join core.tr_type typ on typ.tr_type_id = t.tr_type_id
     join tt.order_round orr on orr.order_list_id = orl.order_list_id
     join tt.order_oper oro on oro.order_round_id = orr.order_round_id
     join gis.stop_places stp on stp.muid = oro.stop_place_muid
     join gis.stops stt on stt.muid = stp.stop_muid
     join tt.round rnd on rnd.round_id = orr.round_id
     join tt.timetable_entry tme on tme.timetable_entry_id = rnd.timetable_entry_id
     join gis.route_trajectories gtj on gtj.muid = rnd.route_trajectory_muid
     join gis.route_rounds grr on grr.muid = gtj.route_round_muid
     join gis.route_variants grv on grv.muid = grr.route_variant_muid
     join gis.routes grt on grt.muid = grv.route_muid
     left join stop_interval sti on sti.order_oper_id = oro.order_oper_id
    where oro.stop_place_muid = l_stop_place_muid
      and orl.order_date = l_order_date
      and (l_tr_type_id is null or t.tr_type_id = l_tr_type_id)
      and (l_route_muid is null or grv.route_muid = l_route_muid)
      and (l_garage_num is null or t.garage_num = l_garage_num)
      and orl.tr_id is not null
      and orl.sign_deleted = 0
      and orl.is_active = 1
      and orr.is_active = 1
      and oro.is_active = 1
      and oro.sign_deleted = 0
      and tme.sign_deleted = 0
      and (
        orl.tr_id in (select adm.account_data_realm_pkg$get_trs_by_tr_type_and_depo(p_account_id))
        or grv.route_muid in (select route_muid
                              from rts.route2gis
                              where route_id in (select adm.account_data_realm_pkg$get_routes(p_account_id)))
      )
    order by oro.time_oper;
end;
$$ language plpgsql volatile security invoker;
