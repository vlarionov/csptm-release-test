create or replace function mnt.get_current_timetable_deviation(p_tr_id bigint)
  RETURNS TABLE(time_plan timestamp, time_fact timestamp, deviation interval) AS
$body$
with order_rounds as
(select order_round_id
 from mnt.v_rounds_attrib
 where tr_id = p_tr_id
       and order_date = core.get_date_current_msk()
),
    last_time as
  (
      select max(time_plan) time_plan
      from tt.order_oper
      where order_round_id
            in (select order_round_id
                from order_rounds)
            and time_plan <= now()/* - interval '3 hours'*/
  ),
    plan as (select order_oper.time_plan,
               order_oper.order_round_id,
               order_oper.stop_place_muid,
               order_oper.stop_order_num
             from tt.order_oper
               join last_time
                 on order_oper.time_plan = last_time.time_plan
             where order_round_id in (select order_round_id
                                      from order_rounds)
                   and sign_deleted = 0
  )
select plan.time_plan, order_fact.time_fact, plan.time_plan - order_fact.time_fact deviation
from plan
  left join tt.order_fact on (plan.order_round_id = order_fact.order_round_id
                              and plan.stop_place_muid = order_fact.stop_place_muid
                              and plan.stop_order_num = order_fact.stop_order_num
    )
  where sign_deleted = 0
;
$body$
language sql;