create or replace function mnt.get_avg_interval_stop_item_a(p_route_variant_id rts.route_variant.route_variant_id%type,
                                                            p_action_type_id   ttb.action_type.action_type_id%type,
                                                            p_order_date       ttb.tt_variant.order_date%type,
                                                            p_period_minutes   int)
    returns interval as
$body$
select avg(stop_interval)
from (select lead(lower(action_range))
             over (
                 order by lower(action_range) ) - lower(action_range) stop_interval
      from mnt.v_tt_action_round_attrib vtara
      where vtara.route_variant_id = p_route_variant_id
            and vtara.order_date = p_order_date
            and vtara.action_type_id = p_action_type_id
            and lower(action_range) between now() - make_interval(mins := p_period_minutes) and now()
     ) t;
$body$
language sql
immutable;