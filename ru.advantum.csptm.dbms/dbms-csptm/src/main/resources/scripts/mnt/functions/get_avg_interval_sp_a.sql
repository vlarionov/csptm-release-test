create or replace function mnt.get_avg_interval_sp_a(p_route_variant_muid bigint, p_period_minute int, p_round_code text)
  returns interval as
  $body$
  select avg(a.interval)
  from (
         select lead(time_plan)
                over (
                  order by oo.time_plan) - oo.time_plan as interval
         from mnt.v_rounds_attrib ra join tt.order_oper oo
             on ra.order_round_id = oo.order_round_id
         where order_date = core.get_date_current_msk()
               and route_variant_muid = p_route_variant_muid
               and oo.stop_place_muid = ra.stop_place_a_muid
               and oo.stop_order_num = 1
               and sign_deleted = 0
               and ra.round_code = p_round_code
               and time_plan between now() and now() + p_period_minute*interval '1 minute'
       ) a
  ;
;
$body$
language sql immutable;