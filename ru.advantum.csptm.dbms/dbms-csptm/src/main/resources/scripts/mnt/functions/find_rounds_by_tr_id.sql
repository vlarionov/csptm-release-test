CREATE OR REPLACE FUNCTION mnt.find_rounds_by_tr_id(p_tr_id core.tr.tr_id%TYPE,p_time TIMESTAMP)
RETURNS table(count_cross int,tr_round int)
AS $$
BEGIN
  RETURN QUERY SELECT
    count(traffics.round_ids) AS count_cross,
    traffics.round_ids as tr_round
  FROM
    (
      SELECT rts.round.round_id AS round_ids
      FROM rts.round,
        (
          SELECT ST_MakePoint(tr.lon, tr.lat) AS pointcoords,tr.event_time
          FROM core.traffic as tr
          WHERE tr.tr_id = p_tr_id and tr.event_time BETWEEN (now()- p_time) and now()
          ORDER BY tr.event_time desc
          ) as lasttraf
      WHERE lasttraf.pointcoords && box2d(rts.round.geom) AND
            ST_DWithin(lasttraf.pointcoords, rts.round.geom,0.00045)
      --LIMIT
    ) AS traffics
  GROUP BY traffics.round_ids
  ORDER BY count_cross DESC
  LIMIT 7;
END;
$$ LANGUAGE plpgsql;