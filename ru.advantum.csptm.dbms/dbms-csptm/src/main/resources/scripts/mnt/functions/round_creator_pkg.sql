CREATE OR REPLACE FUNCTION mnt.find_shortest_round(p_node_begin_id_list int [], p_node_end_id_list int [])
  RETURNS SETOF bigint AS $$
DECLARE
  gs_id BIGINT;
BEGIN
  FOR i IN 1..(SELECT cardinality(p_node_begin_id_list)) LOOP
    gs_id=( sELECT gs.graph_section_id
                         FROM rts.graph_section AS gs
                         WHERE gs.node_begin_id = p_node_begin_id_list [i] AND gs.node_end_id = p_node_end_id_list [i]);
    RETURN NEXT gs_id;
  END LOOP;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION mnt.find_node_id_by_stop_item_id(p_stop_item_id BIGINT)
  RETURNS BIGINT AS
$$
DECLARE
  least_point BIGINT;
BEGIN
  WITH gr as (SELECT rts.graph_section.node_begin_id,rts.graph_section.node_end_id,rts.stop_location.graph_section_offset,rts.graph_section.length
  FROM rts.stop_item
    LEFT JOIN rts.stop_location ON rts.stop_location.stop_location_id = rts.stop_item.stop_location_id
    LEFT JOIN rts.graph_section ON rts.graph_section.graph_section_id = rts.stop_location.graph_section_id
  WHERE rts.stop_item.stop_item_id = p_stop_item_id)
 SELECT CASE
         WHEN gr.graph_section_offset<(gr.length-gr.graph_section_offset) THEN gr.node_begin_id
         ELSE gr.node_end_id
         END INTO STRICT least_point FROM gr;
  RETURN least_point;
END;
$$ LANGUAGE plpgsql;