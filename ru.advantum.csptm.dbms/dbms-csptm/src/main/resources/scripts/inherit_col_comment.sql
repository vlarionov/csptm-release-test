select
  concat('comment on column "', schname, '"."', relname, '"."', attname, '" is ''', pcomment, ''';') attr_comm
from
  (
    select q.*,
      replace((select description from pg_description d2 where d2.objOID = q.inhparent and d2.objsubid = q.attnum),  '''', '''''') pcomment
    FROM
      (
        SELECT
          h.*,
          f.attname,
          f.attnum,
          t.relname,
          ns.nspname                   schname,
          p.relname                    parent_name
        FROM pg_attribute f, pg_inherits h, pg_class t,  pg_class p, pg_namespace ns
        WHERE h.inhrelid = t.OID
              and f.attnum > 0
              and f.attrelid = h.inhrelid
              AND h.inhparent = p.OID
              AND ns.oid = t.relnamespace
              and obj_description(ns.oid) LIKE 'PCUIP%'
              and not exists (select 1 from  pg_description d where d.objOID = h.inhrelid and d.objsubid = f.attnum)
      ) q
  ) qq
  where pcomment is not null
order by relname, attname
--select * from pg_class where oid = 24625

--select * from pg_description where objoid = 2211683532