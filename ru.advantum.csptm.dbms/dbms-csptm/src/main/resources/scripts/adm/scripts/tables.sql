/*
Created: 30.08.2016
Modified: 06.09.2016
Model: kdbo-physical
Database: PostgreSQL 9.4
*/

DROP SCHEMA  IF EXISTS adm CASCADE;

/*
Created: 30.08.2016
Modified: 07.09.2016
Model: kdbo-physical
Database: PostgreSQL 9.4
*/


-- Create schemas section -------------------------------------------------

CREATE SCHEMA adm;
/*
Created: 7/12/2016
Modified: 9/27/2017
Project: csptm
Model: Integration
Database: PostgreSQL 9.4
*/

-- Create tables section -------------------------------------------------

-- Table adm.ad_domain

CREATE TABLE adm.ad_domain(
  ad_domain_id Integer NOT NULL,
  name Text NOT NULL,
  ad_instance_id Integer NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.ad_domain IS 'Домен Active Directory'
;
COMMENT ON COLUMN adm.ad_domain.ad_domain_id IS 'ID'
;
COMMENT ON COLUMN adm.ad_domain.name IS 'Имя'
;
COMMENT ON COLUMN adm.ad_domain.ad_instance_id IS 'Инстанс Active Directory, к которому принадлежит данный домен'
;

-- Create indexes for table adm.ad_domain

CREATE INDEX ix_ad_domain_ad_instance_id ON adm.ad_domain (ad_instance_id)
TABLESPACE adm_idx
;

CREATE UNIQUE INDEX uq_ad_domain_name_ad_instance_id ON adm.ad_domain (name,ad_instance_id)
TABLESPACE adm_idx
;

-- Add keys for table adm.ad_domain

ALTER TABLE adm.ad_domain ADD CONSTRAINT pk_ad_domain PRIMARY KEY (ad_domain_id)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.ad_account

CREATE TABLE adm.ad_account(
  account_id Bigint NOT NULL,
  ad_domain_id Integer NOT NULL,
  ad_name Text NOT NULL,
  guid Text NOT NULL,
  ad_login Text NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.ad_account IS 'Привязка аккаунта к Active Directory'
;
COMMENT ON COLUMN adm.ad_account.account_id IS 'Аккаунт'
;
COMMENT ON COLUMN adm.ad_account.ad_domain_id IS 'Домен AD'
;
COMMENT ON COLUMN adm.ad_account.ad_name IS 'Имя в AD'
;
COMMENT ON COLUMN adm.ad_account.guid IS 'ObjectGUID в AD'
;

-- Create indexes for table adm.ad_account

CREATE UNIQUE INDEX uq_ad_account_name_ad_domain_id ON adm.ad_account (ad_name,ad_domain_id)
TABLESPACE adm_idx
;

CREATE INDEX ix_ad_account_ad_domain_id ON adm.ad_account (ad_domain_id)
TABLESPACE adm_idx
;

-- Add keys for table adm.ad_account

ALTER TABLE adm.ad_account ADD CONSTRAINT pk_ad_account PRIMARY KEY (account_id)
USING INDEX TABLESPACE adm_idx
;

ALTER TABLE adm.ad_account ADD CONSTRAINT uq_ad_account_guid UNIQUE (guid)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.group

CREATE TABLE adm.group(
  group_id Integer NOT NULL,
  name Text,
  comment Text,
  is_personal Boolean NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.group IS 'Группа пользователей'
;
COMMENT ON COLUMN adm.group.group_id IS 'ID'
;
COMMENT ON COLUMN adm.group.name IS 'Наименование'
;
COMMENT ON COLUMN adm.group.comment IS 'Комментарий'
;
COMMENT ON COLUMN adm.group.is_personal IS 'Флаг определяющий скрытая это группа или нет, для выделения индивидуальных прав пользователя'
;

-- Add keys for table adm.group

ALTER TABLE adm.group ADD CONSTRAINT pk_group PRIMARY KEY (group_id)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.account2group

CREATE TABLE adm.account2group(
  account_id Bigint NOT NULL,
  group_id Integer NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.account2group IS 'Связь аккаунтов с группами'
;
COMMENT ON COLUMN adm.account2group.account_id IS 'Аккаунт'
;
COMMENT ON COLUMN adm.account2group.group_id IS 'Группа'
;

-- Create indexes for table adm.account2group

CREATE INDEX ix_account2group_account_id ON adm.account2group (account_id)
TABLESPACE adm_idx
;

CREATE INDEX ix_account2group_group_id ON adm.account2group (group_id)
TABLESPACE adm_idx
;

-- Add keys for table adm.account2group

ALTER TABLE adm.account2group ADD CONSTRAINT pk_account2group PRIMARY KEY (account_id,group_id)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.ad_group

CREATE TABLE adm.ad_group(
  group_id Integer NOT NULL,
  ad_domain_id Integer NOT NULL,
  ad_name Text NOT NULL,
  guid Text
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.ad_group IS 'Привязка группы к Active Directory'
;
COMMENT ON COLUMN adm.ad_group.group_id IS 'Группа'
;
COMMENT ON COLUMN adm.ad_group.ad_domain_id IS 'Домен AD'
;
COMMENT ON COLUMN adm.ad_group.ad_name IS 'Имя в AD'
;
COMMENT ON COLUMN adm.ad_group.guid IS 'ObjectGUID в AD'
;

-- Create indexes for table adm.ad_group

CREATE UNIQUE INDEX uq_ad_group_name_ad_domain_id ON adm.ad_group (ad_name,ad_domain_id)
TABLESPACE adm_idx
;

CREATE INDEX ix_ad_group_ad_domain_id ON adm.ad_group (ad_domain_id)
TABLESPACE adm_idx
;

-- Add keys for table adm.ad_group

ALTER TABLE adm.ad_group ADD CONSTRAINT pk_ad_group PRIMARY KEY (group_id)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.data_realm

CREATE TABLE adm.data_realm(
  data_realm_id Integer NOT NULL,
  name Text NOT NULL,
  comment Text
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.data_realm IS 'Область доступных данных'
;
COMMENT ON COLUMN adm.data_realm.data_realm_id IS 'ID'
;
COMMENT ON COLUMN adm.data_realm.name IS 'Наименование'
;
COMMENT ON COLUMN adm.data_realm.comment IS 'Комментарий'
;

-- Add keys for table adm.data_realm

ALTER TABLE adm.data_realm ADD CONSTRAINT pk_data_realm PRIMARY KEY (data_realm_id)
USING INDEX TABLESPACE adm_idx
;

ALTER TABLE adm.data_realm ADD CONSTRAINT uq_data_realm_name UNIQUE (name)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.data_realm2group

CREATE TABLE adm.data_realm2group(
  data_realm_id Integer NOT NULL,
  group_id Integer NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.data_realm2group IS 'Связь областей доступных данных с группами'
;
COMMENT ON COLUMN adm.data_realm2group.data_realm_id IS 'Абласть доступных данных'
;
COMMENT ON COLUMN adm.data_realm2group.group_id IS 'Группа'
;

-- Create indexes for table adm.data_realm2group

CREATE INDEX ix_data_realm2group_data_realm_id ON adm.data_realm2group (data_realm_id)
TABLESPACE adm_idx
;

CREATE INDEX ix_data_realm2group_group_id ON adm.data_realm2group (group_id)
TABLESPACE adm_idx
;

-- Add keys for table adm.data_realm2group

ALTER TABLE adm.data_realm2group ADD CONSTRAINT pk_data_realm2goup PRIMARY KEY (data_realm_id,group_id)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.role2group

CREATE TABLE adm.role2group(
  group_id Integer NOT NULL,
  id_role Text NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.role2group IS 'Роли группы'
;
COMMENT ON COLUMN adm.role2group.group_id IS 'Группа'
;
COMMENT ON COLUMN adm.role2group.id_role IS 'Роль jofl'
;

-- Create indexes for table adm.role2group

CREATE INDEX ix_role2group_group_id ON adm.role2group (group_id)
TABLESPACE adm_idx
;

CREATE INDEX ix_role2group_id_role ON adm.role2group (id_role)
TABLESPACE adm_idx
;

-- Add keys for table adm.role2group

ALTER TABLE adm.role2group ADD CONSTRAINT pk_role2group PRIMARY KEY (group_id,id_role)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.report2group

CREATE TABLE adm.report2group(
  id_report Integer NOT NULL,
  group_id Integer NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.report2group IS 'Отчеты группы'
;
COMMENT ON COLUMN adm.report2group.id_report IS 'Репорт jofl'
;
COMMENT ON COLUMN adm.report2group.group_id IS 'Группа'
;

-- Create indexes for table adm.report2group

CREATE INDEX ix_report2group_id_report ON adm.report2group (id_report)
TABLESPACE adm_idx
;

CREATE INDEX ix_report2group_group_id ON adm.report2group (group_id)
TABLESPACE adm_idx
;

-- Add keys for table adm.report2group

ALTER TABLE adm.report2group ADD CONSTRAINT pk_report2group PRIMARY KEY (id_report,group_id)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.data_realm2routes

CREATE TABLE adm.data_realm2routes(
  muid Bigint NOT NULL,
  data_realm_id Integer NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.data_realm2routes IS 'Связь области доступных данных с маршрутами'
;
COMMENT ON COLUMN adm.data_realm2routes.muid IS 'Маршрут'
;
COMMENT ON COLUMN adm.data_realm2routes.data_realm_id IS 'Область доступных данных'
;

-- Create indexes for table adm.data_realm2routes

CREATE INDEX ix_data_realm2routes_muid ON adm.data_realm2routes (muid)
TABLESPACE adm_idx
;

CREATE INDEX ix_data_realm2routes_data_realm_id ON adm.data_realm2routes (data_realm_id)
TABLESPACE adm_idx
;

-- Add keys for table adm.data_realm2routes

ALTER TABLE adm.data_realm2routes ADD CONSTRAINT pk_data_realm2routes PRIMARY KEY (muid,data_realm_id)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.group_admin

CREATE TABLE adm.group_admin(
  account_id Bigint NOT NULL,
  group_id Integer NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.group_admin IS 'Администраторы группы'
;
COMMENT ON COLUMN adm.group_admin.account_id IS 'Пользователь'
;
COMMENT ON COLUMN adm.group_admin.group_id IS 'Группа'
;

-- Create indexes for table adm.group_admin

CREATE INDEX ix_group_admin_account_id ON adm.group_admin (account_id)
TABLESPACE adm_idx
;

CREATE INDEX ix_group_admin_group_id ON adm.group_admin (group_id)
TABLESPACE adm_idx
;

-- Add keys for table adm.group_admin

ALTER TABLE adm.group_admin ADD CONSTRAINT pk_group_admin PRIMARY KEY (account_id,group_id)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.login_event

CREATE TABLE adm.login_event(
  login_event_id Integer NOT NULL,
  event_date Timestamp NOT NULL,
  id_jofl_account Numeric(18,0),
  entered_login Text,
  result_code Integer NOT NULL,
  ip_address Text NOT NULL,
  is_sso_login Boolean NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.login_event IS 'Попытки входа пользователей в систему'
;
COMMENT ON COLUMN adm.login_event.login_event_id IS 'ID'
;
COMMENT ON COLUMN adm.login_event.event_date IS 'Время попытки входа'
;
COMMENT ON COLUMN adm.login_event.id_jofl_account IS 'JOFL аккаунт пользователя'
;
COMMENT ON COLUMN adm.login_event.entered_login IS 'Введеный логин'
;
COMMENT ON COLUMN adm.login_event.result_code IS 'Код результата попытки входа
0 - Успешно,
1 - Аккаунт не найден,
2 - Неверный пароль
3 - Аккаунт заблокирован.'
;
COMMENT ON COLUMN adm.login_event.ip_address IS 'IP адрес клиента'
;
COMMENT ON COLUMN adm.login_event.is_sso_login IS 'Логин через sso или login/password'
;

-- Create indexes for table adm.login_event

CREATE INDEX ix_login_event_id_jofl_account ON adm.login_event (id_jofl_account)
TABLESPACE adm_idx
;

-- Add keys for table adm.login_event

ALTER TABLE adm.login_event ADD CONSTRAINT p_login_event PRIMARY KEY (login_event_id)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.session

CREATE TABLE adm.session(
  session_id Integer NOT NULL,
  login_event_id Integer NOT NULL,
  j_session_id Text NOT NULL,
  last_request_date Timestamp NOT NULL,
  close_date Timestamp,
  close_code Integer
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.session IS 'Сессии пользователей на веб сервере'
;
COMMENT ON COLUMN adm.session.session_id IS 'ID'
;
COMMENT ON COLUMN adm.session.login_event_id IS 'Событие успешного входа перед данной сессией'
;
COMMENT ON COLUMN adm.session.j_session_id IS 'JSESSIONID на веб сервере'
;
COMMENT ON COLUMN adm.session.last_request_date IS 'Время последнего запроса'
;
COMMENT ON COLUMN adm.session.close_date IS 'Время окончания сессии'
;
COMMENT ON COLUMN adm.session.close_code IS 'Код типа закрытия сессии
0 - Санкционированный выход из Системы
1 - Сессия завершена по таймауту
2 - Разрыв соединения с сервером'
;

-- Create indexes for table adm.session

CREATE INDEX ix_session_login_event_id ON adm.session (login_event_id)
TABLESPACE adm_idx
;

CREATE INDEX ix_session_j_session_id ON adm.session (j_session_id)
TABLESPACE adm_idx
;

-- Add keys for table adm.session

ALTER TABLE adm.session ADD CONSTRAINT p_session PRIMARY KEY (session_id)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.ad_instance

CREATE TABLE adm.ad_instance(
  ad_instance_id Integer NOT NULL,
  ldap_url Text NOT NULL,
  ldap_user_dn Text NOT NULL,
  ldap_password Text NOT NULL,
  krb_realm Text NOT NULL,
  krb_kdc Text NOT NULL,
  krb_principal Text NOT NULL,
  krb_conf_path Text NOT NULL,
  krb_keytab_path Text NOT NULL,
  krb_debug Boolean NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.ad_instance IS 'Конфигурации для подключения к серверам Active Directory'
;
COMMENT ON COLUMN adm.ad_instance.ad_instance_id IS 'ID'
;
COMMENT ON COLUMN adm.ad_instance.ldap_url IS 'Url ldap сервера'
;
COMMENT ON COLUMN adm.ad_instance.ldap_user_dn IS 'UserDn для подключения по ldap '
;
COMMENT ON COLUMN adm.ad_instance.ldap_password IS 'Password для подключения по ldap'
;
COMMENT ON COLUMN adm.ad_instance.krb_realm IS 'Kerberos realm'
;
COMMENT ON COLUMN adm.ad_instance.krb_kdc IS 'Kerberos KDC'
;
COMMENT ON COLUMN adm.ad_instance.krb_principal IS 'Kerberos service principal'
;
COMMENT ON COLUMN adm.ad_instance.krb_conf_path IS 'Полный путь до krb5.conf'
;
COMMENT ON COLUMN adm.ad_instance.krb_keytab_path IS 'Полный путь до kerberos keytab фала'
;
COMMENT ON COLUMN adm.ad_instance.krb_debug IS 'Флаг работы kerberos модуля в debug mode'
;

-- Add keys for table adm.ad_instance

ALTER TABLE adm.ad_instance ADD CONSTRAINT p_ad_instance PRIMARY KEY (ad_instance_id)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.sys_audit

CREATE TABLE adm.sys_audit(
  operation Integer NOT NULL,
  sys_audit_id Integer NOT NULL,
  name Text NOT NULL,
  comment Text,
  table_name Text NOT NULL,
  query Text,
  message_template Text NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.sys_audit IS 'Аудит системных событий'
;
COMMENT ON COLUMN adm.sys_audit.operation IS 'Операция над таблицей, после которой производится аудит
0 - INSERT
1 - UPDATE
2 - DELETE'
;
COMMENT ON COLUMN adm.sys_audit.sys_audit_id IS 'ID'
;
COMMENT ON COLUMN adm.sys_audit.name IS 'Название аудита'
;
COMMENT ON COLUMN adm.sys_audit.comment IS 'Комментарий'
;
COMMENT ON COLUMN adm.sys_audit.table_name IS 'Таблица, при изменении которой будет производиться аудит'
;
COMMENT ON COLUMN adm.sys_audit.query IS 'SQL запрос параметров события для формирования сообщения'
;
COMMENT ON COLUMN adm.sys_audit.message_template IS 'Шаблон сообщения о событии'
;

-- Create indexes for table adm.sys_audit

CREATE INDEX ix_sys_audit_table_name ON adm.sys_audit (table_name)
TABLESPACE adm_idx
;

-- Add keys for table adm.sys_audit

ALTER TABLE adm.sys_audit ADD CONSTRAINT pk_sys_audit PRIMARY KEY (sys_audit_id)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.sys_event

CREATE TABLE adm.sys_event(
  sys_event_id Integer NOT NULL,
  sys_audit_id Integer NOT NULL,
  event_attributes Json NOT NULL,
  event_date Timestamp NOT NULL,
  is_notified Boolean NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.sys_event IS 'Запись о произошедшем системном событии, содержащая данные для сообщения'
;
COMMENT ON COLUMN adm.sys_event.sys_event_id IS 'ID'
;
COMMENT ON COLUMN adm.sys_event.sys_audit_id IS 'Аудит'
;
COMMENT ON COLUMN adm.sys_event.event_attributes IS 'Данные события, используемые для построения сообщения'
;
COMMENT ON COLUMN adm.sys_event.event_date IS 'Дата события'
;
COMMENT ON COLUMN adm.sys_event.is_notified IS 'Производилось ли уведомление пользователей по данному событию'
;

-- Create indexes for table adm.sys_event

CREATE INDEX ix_sys_event_sys_audit_id ON adm.sys_event (sys_audit_id)
TABLESPACE adm_idx
;

-- Add keys for table adm.sys_event

ALTER TABLE adm.sys_event ADD CONSTRAINT pk_sys_event PRIMARY KEY (sys_event_id)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.sys_audit2group

CREATE TABLE adm.sys_audit2group(
  sys_audit_id Integer NOT NULL,
  group_id Integer NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.sys_audit2group IS 'Связь аудита системных событий с группой'
;
COMMENT ON COLUMN adm.sys_audit2group.sys_audit_id IS 'Аудит системных событий'
;
COMMENT ON COLUMN adm.sys_audit2group.group_id IS 'Группа'
;

-- Add keys for table adm.sys_audit2group

ALTER TABLE adm.sys_audit2group ADD CONSTRAINT pk_sys_audit2group PRIMARY KEY (sys_audit_id,group_id)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.history_data

CREATE TABLE adm.history_data(
  history_data_id Integer NOT NULL,
  id_jofl_account Numeric(18,0),
  change_date Timestamp NOT NULL,
  operation Text NOT NULL,
  table_schema Text NOT NULL,
  table_name Text NOT NULL,
  row_id Text NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.history_data IS 'Факты изменений'
;
COMMENT ON COLUMN adm.history_data.history_data_id IS 'ID'
;
COMMENT ON COLUMN adm.history_data.id_jofl_account IS 'Аккаунт'
;
COMMENT ON COLUMN adm.history_data.change_date IS 'Дата изменения'
;
COMMENT ON COLUMN adm.history_data.operation IS 'Действие'
;
COMMENT ON COLUMN adm.history_data.table_schema IS 'Схема таблицы'
;
COMMENT ON COLUMN adm.history_data.table_name IS 'Имя таблицы'
;
COMMENT ON COLUMN adm.history_data.row_id IS 'Идентификатор изменяемой строки'
;

-- Create indexes for table adm.history_data

CREATE INDEX ix_history_data_id_jofl_account ON adm.history_data (id_jofl_account)
TABLESPACE adm_idx
;

CREATE INDEX ix_history_data_table_schema_table_name ON adm.history_data (table_schema,table_name)
TABLESPACE adm_idx
;

-- Add keys for table adm.history_data

ALTER TABLE adm.history_data ADD CONSTRAINT pk_history_data PRIMARY KEY (history_data_id)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.history_data_item

CREATE TABLE adm.history_data_item(
  history_data_item_id Integer NOT NULL,
  history_data_id Integer NOT NULL,
  column_name Text NOT NULL,
  old_value Text,
  new_value Text
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.history_data_item IS 'Измененные поля'
;
COMMENT ON COLUMN adm.history_data_item.history_data_item_id IS 'ID'
;
COMMENT ON COLUMN adm.history_data_item.history_data_id IS 'Изменение'
;
COMMENT ON COLUMN adm.history_data_item.column_name IS 'Имя атрибута'
;
COMMENT ON COLUMN adm.history_data_item.old_value IS 'Старое значение'
;
COMMENT ON COLUMN adm.history_data_item.new_value IS 'Новое значение'
;

-- Create indexes for table adm.history_data_item

CREATE INDEX ix_history_data_item_history_data_id ON adm.history_data_item (history_data_id)
TABLESPACE adm_idx
;

-- Add keys for table adm.history_data_item

ALTER TABLE adm.history_data_item ADD CONSTRAINT pk_history_data_item_id PRIMARY KEY (history_data_item_id)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.win_table

CREATE TABLE adm.win_table(
  db_method Text NOT NULL,
  table_schema Text NOT NULL,
  table_name Text NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.win_table IS 'Таблица связи JOFL метода и таблицы'
;
COMMENT ON COLUMN adm.win_table.db_method IS 'Наименование метода'
;
COMMENT ON COLUMN adm.win_table.table_schema IS 'Схема таблицы'
;
COMMENT ON COLUMN adm.win_table.table_name IS 'Имя таблицы'
;

-- Add keys for table adm.win_table

ALTER TABLE adm.win_table ADD CONSTRAINT pk_win_table PRIMARY KEY (db_method,table_schema,table_name)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.win_table_key

CREATE TABLE adm.win_table_key(
  table_schema Text NOT NULL,
  table_name Text NOT NULL,
  column_name Text NOT NULL,
  key_table_schema Text NOT NULL,
  key_table_name Text NOT NULL,
  key_column_name Text NOT NULL,
  key_value_column Text NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.win_table_key IS 'Связь значений аттрибутов и вторичных ключей'
;
COMMENT ON COLUMN adm.win_table_key.table_schema IS 'Схема таблицы'
;
COMMENT ON COLUMN adm.win_table_key.table_name IS 'Имя таблицы'
;
COMMENT ON COLUMN adm.win_table_key.column_name IS 'Колонка'
;
COMMENT ON COLUMN adm.win_table_key.key_table_schema IS 'Схема справочной таблицы'
;
COMMENT ON COLUMN adm.win_table_key.key_table_name IS 'Имя справочной таблицы'
;
COMMENT ON COLUMN adm.win_table_key.key_column_name IS 'Справочный ключ'
;
COMMENT ON COLUMN adm.win_table_key.key_value_column IS 'Справочное значение'
;

-- Add keys for table adm.win_table_key

ALTER TABLE adm.win_table_key ADD CONSTRAINT pk_win_table_key PRIMARY KEY (table_schema,table_name,column_name)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.data_realm2depo

CREATE TABLE adm.data_realm2depo(
  depo_id Bigint NOT NULL,
  data_realm_id Integer NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.data_realm2depo IS 'Связь области доступных данных с депо'
;
COMMENT ON COLUMN adm.data_realm2depo.depo_id IS 'Транспортное предприятие'
;
COMMENT ON COLUMN adm.data_realm2depo.data_realm_id IS 'Область доступных данных'
;

-- Create indexes for table adm.data_realm2depo

CREATE INDEX ix_data_realm2depo_depo_id ON adm.data_realm2depo (depo_id)
TABLESPACE adm_idx
;

CREATE INDEX ix_data_realm2depo_data_realm_id ON adm.data_realm2depo (data_realm_id)
TABLESPACE adm_idx
;

-- Add keys for table adm.data_realm2depo

ALTER TABLE adm.data_realm2depo ADD CONSTRAINT pk_data_realm2depo PRIMARY KEY (depo_id,data_realm_id)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.data_realm2tr_type

CREATE TABLE adm.data_realm2tr_type(
  tr_type_id Smallint NOT NULL,
  data_realm_id Integer NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.data_realm2tr_type IS 'Связь области доступных данных с типами ТС'
;
COMMENT ON COLUMN adm.data_realm2tr_type.tr_type_id IS 'Вид ТС'
;
COMMENT ON COLUMN adm.data_realm2tr_type.data_realm_id IS 'Область данных'
;

-- Create indexes for table adm.data_realm2tr_type

CREATE INDEX ix_data_realm2tr_type_tr_type_id ON adm.data_realm2tr_type (tr_type_id)
TABLESPACE adm_idx
;

CREATE INDEX ix_data_realm2tr_type_data_realm_id ON adm.data_realm2tr_type (data_realm_id)
TABLESPACE adm_idx
;

-- Add keys for table adm.data_realm2tr_type

ALTER TABLE adm.data_realm2tr_type ADD CONSTRAINT pk_data_realm2tr_type PRIMARY KEY (tr_type_id,data_realm_id)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.role_detail

CREATE TABLE adm.role_detail(
  id_role Text NOT NULL,
  comment Text,
  is_base Boolean NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.role_detail IS 'Дополнительные детали jofl роли'
;
COMMENT ON COLUMN adm.role_detail.id_role IS 'ID jofl роли'
;
COMMENT ON COLUMN adm.role_detail.comment IS 'Комментарий'
;
COMMENT ON COLUMN adm.role_detail.is_base IS 'Признак Базовая'
;

-- Add keys for table adm.role_detail

ALTER TABLE adm.role_detail ADD CONSTRAINT pk_role_detail PRIMARY KEY (id_role)
USING INDEX TABLESPACE adm_idx
;

-- Table adm.event_type2group

CREATE TABLE adm.event_type2group(
  event_type_id Smallint NOT NULL,
  group_id Integer NOT NULL,
  can_confirm Boolean NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.event_type2group IS 'Доступные для группы тревожные сигналы и возможность их подверждения'
;
COMMENT ON COLUMN adm.event_type2group.can_confirm IS 'Могут ли пользователи группы подтвержать события'
;

-- Create indexes for table adm.event_type2group

CREATE INDEX ix_event_type2group_event_type_id ON adm.event_type2group (event_type_id)
TABLESPACE adm_idx
;

CREATE INDEX ix_event_type2group_group_id ON adm.event_type2group (group_id)
TABLESPACE adm_idx
;

-- Add keys for table adm.event_type2group

ALTER TABLE adm.event_type2group ADD CONSTRAINT pk_event_type2group PRIMARY KEY (event_type_id,group_id)
;

-- Table adm.group_owner

CREATE TABLE adm.group_owner(
  account_id Bigint NOT NULL,
  group_id Integer NOT NULL
)
TABLESPACE adm_data
;

COMMENT ON TABLE adm.group_owner IS 'Владельцы группы'
;
COMMENT ON COLUMN adm.group_owner.account_id IS 'Пользователь'
;
COMMENT ON COLUMN adm.group_owner.group_id IS 'Группа'
;

-- Create indexes for table adm.group_owner

CREATE INDEX ix_group_owner_account_id ON adm.group_owner (account_id)
TABLESPACE adm_idx
;

CREATE INDEX ix_group_owner_group_id ON adm.group_owner (group_id)
TABLESPACE adm_idx
;

-- Add keys for table adm.group_owner

ALTER TABLE adm.group_owner ADD CONSTRAINT pk_group_owner PRIMARY KEY (group_id,account_id)
USING INDEX TABLESPACE adm_idx
;

-- Create relationships section -------------------------------------------------

ALTER TABLE adm.account2group ADD CONSTRAINT fk_account2group_group FOREIGN KEY (group_id) REFERENCES adm.group (group_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE adm.ad_account ADD CONSTRAINT fk_ad_account_domain FOREIGN KEY (ad_domain_id) REFERENCES adm.ad_domain (ad_domain_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE adm.ad_group ADD CONSTRAINT fk_ad_group_group FOREIGN KEY (group_id) REFERENCES adm.group (group_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE adm.ad_group ADD CONSTRAINT fk_ad_group_ad_domain FOREIGN KEY (ad_domain_id) REFERENCES adm.ad_domain (ad_domain_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE adm.data_realm2group ADD CONSTRAINT fk_data_realm2group_data_realm FOREIGN KEY (data_realm_id) REFERENCES adm.data_realm (data_realm_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE adm.data_realm2group ADD CONSTRAINT fk_data_realm2group_group FOREIGN KEY (group_id) REFERENCES adm.group (group_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE adm.role2group ADD CONSTRAINT fk_role2group_group FOREIGN KEY (group_id) REFERENCES adm.group (group_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE adm.report2group ADD CONSTRAINT fk_report2group_group FOREIGN KEY (group_id) REFERENCES adm.group (group_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE adm.data_realm2routes ADD CONSTRAINT fk_data_realm2routes_data_realm FOREIGN KEY (data_realm_id) REFERENCES adm.data_realm (data_realm_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE adm.group_admin ADD CONSTRAINT fk_group_admin_group FOREIGN KEY (group_id) REFERENCES adm.group (group_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE adm.session ADD CONSTRAINT fk_session_login_event FOREIGN KEY (login_event_id) REFERENCES adm.login_event (login_event_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE adm.ad_domain ADD CONSTRAINT fk_ad_domain_ad_instance FOREIGN KEY (ad_instance_id) REFERENCES adm.ad_instance (ad_instance_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE adm.sys_event ADD CONSTRAINT fk_sys_event_sys_audit FOREIGN KEY (sys_audit_id) REFERENCES adm.sys_audit (sys_audit_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE adm.sys_audit2group ADD CONSTRAINT fk_sys_audit2group_sys_audit FOREIGN KEY (sys_audit_id) REFERENCES adm.sys_audit (sys_audit_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE adm.sys_audit2group ADD CONSTRAINT fk_sys_audit2group_group FOREIGN KEY (group_id) REFERENCES adm.group (group_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE adm.history_data_item ADD CONSTRAINT fk_history_data_item_history_data FOREIGN KEY (history_data_id) REFERENCES adm.history_data (history_data_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE adm.data_realm2depo ADD CONSTRAINT fk_data_realm2depo_data_realm FOREIGN KEY (data_realm_id) REFERENCES adm.data_realm (data_realm_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE adm.data_realm2tr_type ADD CONSTRAINT fk_data_realm2tr_type_data_realm FOREIGN KEY (data_realm_id) REFERENCES adm.data_realm (data_realm_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE adm.event_type2group ADD CONSTRAINT fk_event_type2group_group FOREIGN KEY (group_id) REFERENCES adm.group (group_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE adm.group_owner ADD CONSTRAINT fk_group_owner_group FOREIGN KEY (group_id) REFERENCES adm.group (group_id) ON DELETE NO ACTION ON UPDATE NO ACTION
;


ALTER SEQUENCE core.sensor2unit_model_sensor2unit_model_id_seq OWNED BY core.sensor2unit_model.sensor2unit_model_id
;
ALTER SEQUENCE core.sim_card2unit_h_sim_card2unit_h_id_seq OWNED BY core.sim_card2unit_h.sim_card2unit_h_id
;
ALTER SEQUENCE core.unit_bnst_has_manipulator_seq OWNED BY core.unit_bnst.has_manipulator
;
ALTER SEQUENCE core.equipment2tr_h_equipment2tr_h_id_seq OWNED BY core.equipment2tr_h.equipment2tr_h_id
;
ALTER SEQUENCE core.unit_remove_reason_unit_remove_reason_id_seq OWNED BY core.equipment_remove_reason.equipment_remove_reason_id
;
ALTER SEQUENCE core.breakdown_breakdown_id_seq OWNED BY core.breakdown.breakdown_id
;
ALTER SEQUENCE core.breakdown_num_new_seq OWNED BY core.breakdown.num
;
ALTER SEQUENCE core.breakdown_status_breakdown_status_id_seq OWNED BY core.breakdown_status.breakdown_status_id
;
ALTER SEQUENCE core.breakdown_type_breakdown_type_seq OWNED BY core.breakdown_type.breakdown_type_id
;
ALTER SEQUENCE core.repair_request_comment_repair_request_comment_id_seq OWNED BY core.repair_request_comment.repair_request_comment_id
;
ALTER SEQUENCE core.repair_request_num_seq OWNED BY core.repair_request.num
;
ALTER SEQUENCE core.repair_request_repair_request_id_seq OWNED BY core.repair_request.repair_request_id
;
ALTER SEQUENCE core.repair_request_status_repair_request_status_id_seq OWNED BY core.repair_request_status.repair_request_status_id
;
ALTER SEQUENCE core.tr_schema_install_detail_tr_schema_install_detail_id_seq OWNED BY core.tr_schema_install_detail.tr_schema_install_detail_id
;
ALTER SEQUENCE core.tr_schema_install_tr_schema_install_id_seq OWNED BY core.tr_schema_install.tr_schema_install_id
;

-- Grant permissions section -------------------------------------------------


