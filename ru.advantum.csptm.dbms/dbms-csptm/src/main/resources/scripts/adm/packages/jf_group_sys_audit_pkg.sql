CREATE OR REPLACE FUNCTION adm.jf_group_sys_audit_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id adm.group.group_id%TYPE;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);

  OPEN p_rows FOR
  SELECT
    l_group_id                                                         group_id,
    sa.sys_audit_id,
    sa.name,
    adm.group_pkg$get_of_delete_owner_policy(p_id_account, l_group_id) "ROW$POLICY"
  FROM adm.group_data_pkg$get_sys_audits(l_group_id) g2sa
    JOIN adm.sys_audit sa ON g2sa.sys_audit_id = sa.sys_audit_id
  ORDER BY sa.sys_audit_id DESC;
END;
$$;


CREATE OR REPLACE FUNCTION adm.jf_group_sys_audit_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id     adm.group.group_id%TYPE;
  l_sys_audit_id adm.sys_audit.sys_audit_id%TYPE;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);
  l_sys_audit_id := jofl.jofl_pkg$extract_number(p_attr, 'sys_audit_id', FALSE);

  PERFORM adm.group_data_pkg$check_is_not_super_group(l_group_id);
  PERFORM adm.group_pkg$check_is_owner(p_id_account, l_group_id);

  INSERT INTO adm.sys_audit2group (group_id, sys_audit_id) VALUES (
    l_group_id,
    l_sys_audit_id
  );

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_group_sys_audit_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id     adm.group.group_id%TYPE;
  l_sys_audit_id adm.sys_audit.sys_audit_id%TYPE;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);
  l_sys_audit_id := jofl.jofl_pkg$extract_number(p_attr, 'sys_audit_id', FALSE);

  PERFORM adm.group_data_pkg$check_is_not_super_group(l_group_id);
  PERFORM adm.group_pkg$check_is_owner(p_id_account, l_group_id);

  DELETE FROM adm.sys_audit2group
  WHERE group_id = l_group_id AND sys_audit_id = l_sys_audit_id;

  RETURN NULL;
END;
$$;