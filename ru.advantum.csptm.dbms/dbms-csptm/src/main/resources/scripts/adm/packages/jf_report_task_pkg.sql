CREATE OR REPLACE FUNCTION adm.jf_report_task_pkg$attr_to_rowtype(p_attr text)
  RETURNS adm.report_task AS
$BODY$ 
declare 
   l_r adm.report_task%rowtype;
begin
   l_r.id := jofl.jofl_pkg$extract_number(p_attr, 'id', true);
   l_r.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true);
   l_r.next_run := jofl.jofl_pkg$extract_date(p_attr, 'next_run', true);
   l_r.last_run := jofl.jofl_pkg$extract_date(p_attr, 'last_run', true);
   l_r.cron_mask := jofl.jofl_pkg$extract_varchar(p_attr, 'cron_mask', true);

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
----------------------------------------------------------------------------------------  
CREATE OR REPLACE FUNCTION adm.jf_report_task_pkg$of_delete(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r adm.report_task%rowtype;
begin 
   l_r := adm.jf_report_task_pkg$attr_to_rowtype(p_attr);

   delete from  adm.report_task where  ID = l_r.ID;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
----------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION adm.jf_report_task_pkg$of_insert(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r adm.report_task%rowtype;
begin 
   l_r := adm.jf_report_task_pkg$attr_to_rowtype(p_attr);
   l_r.id := nextval('adm.report_task_id_seq');
   insert into adm.report_task select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
----------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION adm.jf_report_task_pkg$of_rows(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
begin 
 open p_rows for 
      select 
	ID AS "id",
        '' AS "cron_text",
        COMMENT AS "comment",
        NEXT_RUN  AS "next_run",
        LAST_RUN  AS "last_run",
        CRON_MASK AS "cron_mask"
      from adm.report_task;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
----------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION adm.jf_report_task_pkg$of_update(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r adm.report_task%rowtype;
begin 
   l_r := adm.jf_report_task_pkg$attr_to_rowtype(p_attr);

   update adm.report_task set
          COMMENT = l_r.COMMENT
          ,CRON_MASK = l_r.CRON_MASK
   where 
          ID = l_r.ID;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
