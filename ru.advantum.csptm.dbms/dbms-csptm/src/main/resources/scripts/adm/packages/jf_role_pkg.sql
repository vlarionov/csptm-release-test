CREATE OR REPLACE FUNCTION adm.jf_role_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
BEGIN
  PERFORM adm.jf_role_pkg$of_insert(
      adm.jf_role_pkg$generate_id(),
      jofl.jofl_pkg$extract_varchar(p_attr, 'role_name', FALSE),
      jofl.jofl_pkg$extract_varchar(p_attr, 'comment', TRUE)
  );
  RETURN NULL;
END;
$$;
CREATE OR REPLACE FUNCTION adm.jf_role_pkg$of_insert(p_id_role TEXT, p_role_name TEXT, p_comment TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
BEGIN
  INSERT INTO jofl.ref_role (id_role, role_name, is_active) VALUES (
    p_id_role,
    p_role_name,
    1
  );

  INSERT INTO adm.role_detail (id_role, comment, is_base) VALUES (
    p_id_role,
    p_comment,
    FALSE
  );

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_role_pkg$generate_id()
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN 'CORE.ADM.' || nextval('adm.role_id_seq');
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_role_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_is_group_window BOOLEAN;
  l_administrable_roles_ids TEXT[];

  l_f_db_method     TEXT;
  l_f_call_action   TEXT;
BEGIN
  l_is_group_window := COALESCE(jofl.jofl_pkg$extract_boolean(p_attr, 'is_group_window', TRUE), 0);
  l_administrable_roles_ids := adm.group_pkg$get_administrable_roles_ids(p_id_account);

  l_f_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'F_DB_METHOD', TRUE);
  l_f_call_action := jofl.jofl_pkg$extract_varchar(p_attr, 'f_call_action', TRUE);

  OPEN p_rows FOR
  SELECT
    r.id_role,
    r.role_name,
    rd.comment,
    rd.is_base
  FROM jofl.ref_role r
    JOIN adm.role_detail rd ON r.id_role = rd.id_role
  WHERE ((NOT l_is_group_window) OR r.id_role = ANY (l_administrable_roles_ids))
        AND ((l_f_db_method <> '') IS NOT TRUE OR exists(SELECT 1
                                                         FROM jofl.role2call r2c
                                                         WHERE r2c.id_role = r.id_role AND
                                                               r2c.db_method = l_f_db_method))
        AND ((l_f_call_action <> '') IS NOT TRUE OR exists(SELECT 1
                                                           FROM jofl.role2call r2c
                                                           WHERE r2c.id_role = r.id_role
                                                                 AND r2c.call_action = l_f_call_action))
  ORDER BY r.role_name;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_role_pkg$of_update(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_ref_role    jofl.ref_role%ROWTYPE;
  l_role_detail adm.role_detail%ROWTYPE;
BEGIN
  l_ref_role.id_role = jofl.jofl_pkg$extract_varchar(p_attr, 'id_role', FALSE);
  l_ref_role.role_name := jofl.jofl_pkg$extract_varchar(p_attr, 'role_name', FALSE);

  EXECUTE adm.jf_role_pkg$check_is_not_base(l_ref_role.id_role);

  UPDATE jofl.ref_role
  SET
    role_name = l_ref_role.role_name
  WHERE id_role = l_ref_role.id_role;

  l_role_detail.id_role := l_ref_role.id_role;
  l_role_detail.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', TRUE);

  UPDATE adm.role_detail
  SET
    comment = l_role_detail.comment
  WHERE id_role = l_role_detail.id_role;

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_role_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_id_role jofl.ref_role.id_role%TYPE;
BEGIN
  l_id_role := jofl.jofl_pkg$extract_varchar(p_attr, 'id_role', FALSE);

  EXECUTE adm.jf_role_pkg$check_is_not_base(l_id_role);

  DELETE FROM adm.role_detail
  WHERE id_role = l_id_role;

  DELETE FROM jofl.role2call
  WHERE id_role = l_id_role;

  DELETE FROM adm.role2group
  WHERE id_role = l_id_role;

  DELETE FROM jofl.ref_role
  WHERE id_role = l_id_role;

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_role_pkg$of_copy(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_copied_id_role jofl.ref_role.id_role%TYPE;
  l_new_id_role    jofl.ref_role.id_role%TYPE;
  l_role2call      RECORD;
  l_role2group     RECORD;
BEGIN
  l_copied_id_role := jofl.jofl_pkg$extract_varchar(p_attr, 'id_role', FALSE);
  l_new_id_role := adm.jf_role_pkg$generate_id();

  PERFORM adm.jf_role_pkg$of_insert(
      l_new_id_role,
      'Копия ' || r.role_name,
      rd.comment
  )
  FROM jofl.ref_role r
    JOIN adm.role_detail rd ON r.id_role = rd.id_role
  WHERE r.id_role = l_copied_id_role;

  FOR l_role2call IN SELECT *
                     FROM jofl.role2call r2c
                     WHERE r2c.id_role = l_copied_id_role
  LOOP
    INSERT INTO jofl.role2call (id_role, db_method, call_action) VALUES (
      l_new_id_role,
      l_role2call.db_method,
      l_role2call.call_action
    );
  END LOOP;

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_role_pkg$check_is_not_base(p_id_role jofl.ref_role.id_role%TYPE)
  RETURNS VOID
IMMUTABLE
LANGUAGE plpgsql
AS $$
DECLARE
  l_is_base adm.role_detail.is_base%TYPE;
BEGIN
  SELECT rd.is_base
  INTO STRICT l_is_base
  FROM adm.role_detail rd
  WHERE rd.id_role = p_id_role;
  IF l_is_base
  THEN
    RAISE EXCEPTION 'Роль является базовой. Отредактировать или удалить базовую роль невозможно!';
  END IF;
END;
$$
