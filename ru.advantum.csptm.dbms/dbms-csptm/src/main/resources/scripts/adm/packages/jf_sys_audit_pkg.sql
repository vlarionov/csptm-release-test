CREATE OR REPLACE FUNCTION adm.jf_sys_audit_pkg$attr_to_rowtype(p_attr TEXT)
  RETURNS adm.SYS_AUDIT
LANGUAGE plpgsql
AS $$
DECLARE
  l_r adm.sys_audit%ROWTYPE;
BEGIN
  l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', FALSE);
  l_r.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', TRUE);
  l_r.table_name := jofl.jofl_pkg$extract_varchar(p_attr, 'table_name', FALSE);
  l_r.query := jofl.jofl_pkg$extract_varchar(p_attr, 'query', TRUE);
  l_r.message_template := jofl.jofl_pkg$extract_varchar(p_attr, 'message_template', FALSE);
  l_r.operation := jofl.jofl_pkg$extract_number(p_attr, 'operation', FALSE);
  RETURN l_r;
END;
$$;


CREATE OR REPLACE FUNCTION adm.jf_sys_audit_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_sys_audit adm.sys_audit%ROWTYPE;
BEGIN
  l_sys_audit := adm.jf_sys_audit_pkg$attr_to_rowtype(p_attr);
  l_sys_audit.sys_audit_id:= nextval('adm.sys_audit_id_seq');

  INSERT INTO adm.sys_audit (sys_audit_id, name, comment, table_name, query, message_template, operation) VALUES (
    l_sys_audit.sys_audit_id,
    l_sys_audit.name,
    l_sys_audit.comment,
    l_sys_audit.table_name,
    l_sys_audit.query,
    l_sys_audit.message_template,
    l_sys_audit.operation
  );

  PERFORM adm.sys_audit_pkg$after_insert_audit(l_sys_audit.table_name);

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_sys_audit_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_is_group_window BOOLEAN;
  l_administrable_sys_audit_ids INTEGER[];
BEGIN
  l_is_group_window := COALESCE(jofl.jofl_pkg$extract_boolean(p_attr, 'is_group_window', TRUE), 0);
  l_administrable_sys_audit_ids := adm.group_pkg$get_administrable_sys_audit_ids(p_id_account);

  OPEN p_rows FOR
  SELECT
    sa.sys_audit_id,
    sa.name,
    sa.comment,
    sa.table_name,
    sa.query,
    sa.message_template,
    sa.operation,
    adm.jf_sys_audit_operation_pkg$get_operation_name(sa.operation) operation_name
  FROM adm.sys_audit sa
  WHERE ((NOT l_is_group_window) OR sa.sys_audit_id = ANY (l_administrable_sys_audit_ids));
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_sys_audit_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_sys_audit_id adm.sys_audit.sys_audit_id%TYPE;
BEGIN
  l_sys_audit_id := jofl.jofl_pkg$extract_number(p_attr, 'sys_audit_id', FALSE);

  PERFORM adm.sys_audit_pkg$before_delete_audit(sa.table_name)
  FROM adm.sys_audit sa
  WHERE sa.sys_audit_id = l_sys_audit_id;

  DELETE
  FROM adm.sys_event
  WHERE sys_audit_id = l_sys_audit_id;

  DELETE
  FROM adm.sys_audit
  WHERE sys_audit_id = l_sys_audit_id;

  RETURN NULL;
END;
$$;