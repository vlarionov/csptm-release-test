CREATE OR REPLACE FUNCTION adm.jf_data_realm_tr_type_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                                 p_attr       TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_data_realm_id INTEGER := jofl.jofl_pkg$extract_number(p_attr, 'data_realm_id', FALSE);
BEGIN
  OPEN p_rows FOR
  SELECT
    l_data_realm_id data_realm_id,
    trt.tr_type_id,
    trt.name
  FROM core.tr_type trt
  where trt.tr_type_id in (select adm.data_realm_pkg$get_tr_types(array [l_data_realm_id]))
  ORDER BY trt.name;
END;
$$;


CREATE OR REPLACE FUNCTION adm.jf_data_realm_tr_type_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_data_realm_id INTEGER;
  l_tr_type_id    BIGINT;
BEGIN
  l_data_realm_id := jofl.jofl_pkg$extract_number(p_attr, 'data_realm_id', FALSE);
  l_tr_type_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_type_id', FALSE);

  PERFORM adm.data_realm_pkg$check_is_not_super_data_realm(l_data_realm_id);

  INSERT INTO adm.data_realm2tr_type (tr_type_id, data_realm_id) VALUES (
    l_tr_type_id,
    l_data_realm_id
  );

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_data_realm_tr_type_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_data_realm_id INTEGER;
  l_tr_type_id    BIGINT;
BEGIN
  l_data_realm_id := jofl.jofl_pkg$extract_number(p_attr, 'data_realm_id', FALSE);
  l_tr_type_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_type_id', FALSE);

  PERFORM adm.data_realm_pkg$check_is_not_super_data_realm(l_data_realm_id);

  DELETE FROM adm.data_realm2tr_type
  WHERE data_realm_id = l_data_realm_id AND tr_type_id = l_tr_type_id;

  RETURN NULL;
END;
$$;