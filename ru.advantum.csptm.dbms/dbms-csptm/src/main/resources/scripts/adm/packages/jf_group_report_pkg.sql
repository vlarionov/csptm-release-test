CREATE OR REPLACE FUNCTION adm.jf_group_report_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id adm.group.group_id%TYPE;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);

  OPEN p_rows FOR
  SELECT
    l_group_id                                                         group_id,
    r.id_report,
    r.s_name,
    adm.group_pkg$get_of_delete_owner_policy(p_id_account, l_group_id) "ROW$POLICY"
  FROM adm.group_data_pkg$get_reports(l_group_id) g2r
    JOIN jrep.report r ON g2r.id_report = r.id_report
  ORDER BY r.id_report DESC;
END;
$$;


CREATE OR REPLACE FUNCTION adm.jf_group_report_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id  adm.group.group_id%TYPE;
  l_id_report jrep.report.id_report%TYPE;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);
  l_id_report := jofl.jofl_pkg$extract_number(p_attr, 'id_report', FALSE);

  PERFORM adm.group_data_pkg$check_is_not_super_group(l_group_id);
  PERFORM adm.group_pkg$check_is_owner(p_id_account, l_group_id);

  INSERT INTO adm.report2group (group_id, id_report) VALUES (
    l_group_id,
    l_id_report
  );

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_group_report_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id  adm.group.group_id%TYPE;
  l_id_report jrep.report.id_report%TYPE;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);
  l_id_report := jofl.jofl_pkg$extract_number(p_attr, 'id_report', FALSE);

  PERFORM adm.group_data_pkg$check_is_not_super_group(l_group_id);
  PERFORM adm.group_pkg$check_is_owner(p_id_account, l_group_id);

  DELETE FROM adm.report2group
  WHERE group_id = l_group_id AND id_report = l_id_report;

  RETURN NULL;
END;
$$;