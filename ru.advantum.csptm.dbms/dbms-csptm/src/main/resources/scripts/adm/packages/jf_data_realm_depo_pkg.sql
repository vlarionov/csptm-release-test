CREATE OR REPLACE FUNCTION adm.jf_data_realm_depo_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_data_realm_id INTEGER := jofl.jofl_pkg$extract_number(p_attr, 'data_realm_id', FALSE);
BEGIN
  OPEN p_rows FOR
  SELECT
    l_data_realm_id data_realm_id,
    d.depo_id,
    e.name_full
  FROM core.depo d
    JOIN core.entity e ON e.entity_id = d.depo_id
  WHERE d.depo_id in(select adm.data_realm_pkg$get_depos(array [l_data_realm_id]))
  ORDER BY e.name_full;
END;
$$;


CREATE OR REPLACE FUNCTION adm.jf_data_realm_depo_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_data_realm_id INTEGER;
  l_depo_id       BIGINT;
BEGIN
  l_data_realm_id := jofl.jofl_pkg$extract_number(p_attr, 'data_realm_id', FALSE);
  l_depo_id := jofl.jofl_pkg$extract_number(p_attr, 'depo_id', FALSE);

  PERFORM adm.data_realm_pkg$check_is_not_super_data_realm(l_data_realm_id);

  INSERT INTO adm.data_realm2depo (depo_id, data_realm_id) VALUES (
    l_depo_id,
    l_data_realm_id
  );

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_data_realm_depo_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_data_realm_id INTEGER;
  l_depo_id       BIGINT;
BEGIN
  l_data_realm_id := jofl.jofl_pkg$extract_number(p_attr, 'data_realm_id', FALSE);
  l_depo_id := jofl.jofl_pkg$extract_number(p_attr, 'depo_id', FALSE);

  PERFORM adm.data_realm_pkg$check_is_not_super_data_realm(l_data_realm_id);

  DELETE FROM adm.data_realm2depo
  WHERE data_realm_id = l_data_realm_id AND depo_id = l_depo_id;

  RETURN NULL;
END;
$$;