CREATE OR REPLACE FUNCTION adm.jf_group_data_realm_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id adm.group.group_id%TYPE;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);

  OPEN p_rows FOR
  SELECT
    l_group_id                                                         group_id,
    dr.data_realm_id,
    dr.name,
    adm.group_pkg$get_of_delete_owner_policy(p_id_account, l_group_id) "ROW$POLICY"
  FROM adm.group_data_pkg$get_data_realms(l_group_id) g2dr
    JOIN adm.data_realm dr ON g2dr.data_realm_id = dr.data_realm_id
  ORDER BY dr.data_realm_id DESC;
END;
$$;


CREATE OR REPLACE FUNCTION adm.jf_group_data_realm_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id      adm.group.group_id%TYPE;
  l_data_realm_id INTEGER;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);
  l_data_realm_id := jofl.jofl_pkg$extract_number(p_attr, 'data_realm_id', FALSE);

  PERFORM adm.group_data_pkg$check_is_not_super_group(l_group_id);
  PERFORM adm.group_pkg$check_is_owner(p_id_account, l_group_id);

  INSERT INTO adm.data_realm2group (group_id, data_realm_id)
  VALUES (l_group_id, l_data_realm_id);

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_group_data_realm_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id      adm.group.group_id%TYPE;
  l_data_realm_id adm.data_realm.data_realm_id%TYPE;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);
  l_data_realm_id := jofl.jofl_pkg$extract_number(p_attr, 'data_realm_id', FALSE);

  PERFORM adm.group_data_pkg$check_is_not_super_group(l_group_id);
  PERFORM adm.group_pkg$check_is_owner(p_id_account, l_group_id);

  DELETE FROM adm.data_realm2group
  WHERE group_id = l_group_id AND data_realm_id = l_data_realm_id;

  RETURN NULL;
END;
$$;