CREATE OR REPLACE FUNCTION adm.jf_history_data_item_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_db_method       TEXT :=jofl.jofl_pkg$extract_varchar(p_attr, 'hist_db_method', TRUE);
  l_history_data_id INTEGER := jofl.jofl_pkg$extract_number(p_attr, 'history_data_id', FALSE);

  --l_window_pkg      TEXT;
BEGIN
  IF l_db_method IS NOT NULL AND NOT (l_db_method = ANY (adm.history_pkg$get_available_windows(p_id_account)))
  THEN
    RETURN;
  END IF;

  /*SELECT w.interface_pkg
  INTO l_window_pkg
  FROM jofl.window w
    JOIN adm.win_table wt ON w.db_method = wt.db_method
  WHERE w.db_method = l_db_method;

  EXECUTE 'SELECT ' || l_window_pkg || '$of_history_items($1, $2)'
  INTO p_rows
  USING p_id_account, p_attr;

  EXCEPTION WHEN undefined_function
  THEN
  */
  OPEN p_rows FOR
  SELECT
    hdi.history_data_id,
    hdi.history_data_item_id,
    adm.jf_history_data_item_pkg$get_field_name(hdi.history_data_item_id, l_db_method) column_name,
    adm.jf_history_data_item_pkg$get_field_value(hdi.old_value, l_db_method, ha.table_schema, ha.table_name,
                                                 hdi.column_name)                      old_value,
    adm.jf_history_data_item_pkg$get_field_value(hdi.new_value, l_db_method, ha.table_schema, ha.table_name,
                                                 hdi.column_name)                      new_value
  FROM adm.history_data_item hdi
    JOIN adm.history_data ha ON hdi.history_data_id = ha.history_data_id
  WHERE hdi.history_data_id = l_history_data_id;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_history_data_item_pkg$get_field_name(
  p_history_data_item_id adm.history_data_item.history_data_item_id%TYPE,
  p_db_method            jofl.win_field.db_method%TYPE
)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_result TEXT;
BEGIN
  SELECT coalesce(jwf.display_title, hdi.column_name)
  INTO l_result
  FROM adm.history_data_item hdi
    LEFT JOIN jofl.win_field jwf ON jwf.db_method = p_db_method AND hdi.column_name = jwf.field_name
  WHERE hdi.history_data_item_id = p_history_data_item_id;

  RETURN l_result;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_history_data_item_pkg$get_field_value(
  p_value        TEXT,
  p_db_method    jofl.win_field.db_method%TYPE,
  p_table_schema TEXT,
  p_table_name   TEXT,
  p_column_name  TEXT
)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_result TEXT;
BEGIN
  IF exists(SELECT
            FROM information_schema.columns
            WHERE table_schema = p_table_schema AND table_name = p_table_name AND column_name = p_column_name AND
                  udt_name = 'timestamp'
  )
  THEN
    RETURN p_value :: TIMESTAMP WITH TIME ZONE :: TEXT;
  END IF;

  SELECT adm.jf_history_data_item_pkg$get_referenced_field_value(
      p_value,
      jwfr.ref_db_method,
      wt.table_schema,
      wt.table_name,
      jwfr.ref_field
  )
  INTO l_result
  FROM jofl.win_field jwf
    LEFT JOIN jofl.win_field_ref jwfr ON jwfr.db_method = p_db_method AND jwfr.dst_field = p_column_name
    LEFT JOIN adm.win_table wt ON jwfr.ref_db_method = wt.db_method
  WHERE jwf.db_method = p_db_method AND jwf.field_name = p_column_name AND jwf.n_visibility = 0;

  RETURN coalesce(l_result, p_value);
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_history_data_item_pkg$get_referenced_field_value(
  p_value            TEXT,
  p_ref_db_method    TEXT,
  p_ref_table_schema TEXT,
  p_ref_table_name   TEXT,
  p_ref_field        TEXT
)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_ref_win_field RECORD;
  l_result        TEXT;
BEGIN
  IF p_value IS NOT NULL
     AND p_ref_table_schema IS NOT NULL
     AND p_ref_table_name IS NOT NULL
     AND p_ref_db_method IS NOT NULL
     AND p_ref_field IS NOT NULL
  THEN
    FOR l_ref_win_field
    IN SELECT *
       FROM jofl.win_field wf
       WHERE wf.db_method = p_ref_db_method
             AND wf.n_visibility != 0
       ORDER BY wf.is_display_name DESC, wf.n_order ASC
       LIMIT 1
    LOOP
      EXECUTE ' select ' || l_ref_win_field.field_name ||
              ' from ' || p_ref_table_schema || '.' || p_ref_table_name ||
              ' where ' || p_ref_field || '::TEXT  = $1'
      INTO l_result
      USING p_value;
    END LOOP;
  END IF;

  IF l_result ISNULL
  THEN
    l_result := p_value;
  END IF;

  RETURN l_result;

  EXCEPTION WHEN OTHERS
  THEN RETURN p_value;
END;
$$;