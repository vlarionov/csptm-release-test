CREATE OR REPLACE FUNCTION adm.jf_auth_event_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_f_event_date_from         TIMESTAMP;
  l_f_event_date_to           TIMESTAMP;
  l_f_depo_id                 core.depo.depo_id%TYPE;
  l_f_facility_id             core.facility.facility_id%TYPE;
  l_f_login_or_full_name_TEXT TEXT;
  l_f_result_type_INPLACE_K   NUMERIC;
BEGIN
  l_f_event_date_from := jofl.jofl_pkg$extract_date(p_attr, 'f_event_date_from_DT', TRUE);
  l_f_event_date_to := jofl.jofl_pkg$extract_date(p_attr, 'f_event_date_to_DT', TRUE);
  l_f_depo_id := jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', TRUE);
  l_f_facility_id := jofl.jofl_pkg$extract_number(p_attr, 'f_facility_id', TRUE);
  l_f_login_or_full_name_TEXT := jofl.jofl_pkg$extract_varchar(p_attr, 'f_login_or_full_name_TEXT', TRUE);
  l_f_result_type_INPLACE_K := jofl.jofl_pkg$extract_number(p_attr, 'f_result_type_INPLACE_K', TRUE);

  OPEN p_rows FOR
  SELECT *
  FROM (
         SELECT
           le.event_date,
           le.entered_login,
           adm.jf_account_pkg$get_full_name(a.account_id)                    full_name,
           (SELECT e.name_short
            FROM core.entity e
            WHERE e.entity_id = a.depo_id)                                   depo_name,
           (SELECT e.name_short
            FROM core.entity e
            WHERE e.entity_id = a.facility_id)                               facility_name,
           a.position,
           adm.jf_auth_event_pkg$get_result_code_description(le.result_code) result_type,
           le.ip_address,
           ss.close_date,
           adm.jf_auth_event_pkg$get_close_code_description(ss.close_code)   close_type,
           EXTRACT(EPOCH FROM ss.last_request_date - le.event_date)          duration
         FROM adm.login_event le
           LEFT JOIN adm.session ss ON le.login_event_id = ss.login_event_id
           LEFT JOIN jofl.account ja ON le.id_jofl_account = ja.id_jofl_account
           LEFT JOIN core.account a ON ja.id_account_local = a.account_id
         WHERE
           (l_f_event_date_from IS NULL OR le.event_date >= l_f_event_date_from)
           AND (l_f_event_date_to IS NULL OR le.event_date <= l_f_event_date_to)
           AND (l_f_depo_id IS NULL OR a.depo_id = l_f_depo_id)
           AND (l_f_facility_id IS NULL OR a.facility_id = l_f_facility_id)
           AND (l_f_result_type_INPLACE_K IS NULL OR le.result_code = l_f_result_type_INPLACE_K)
         ORDER BY le.event_date DESC
       ) ae
  WHERE ((l_f_login_or_full_name_TEXT = '') IS NOT FALSE OR
         lower(full_name) LIKE '%' || lower(l_f_login_or_full_name_TEXT) || '%' OR
         ae.entered_login LIKE '%' || lower(l_f_login_or_full_name_TEXT) || '%');
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_auth_event_pkg$get_result_code_description(p_result_code INTEGER)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
BEGIN
  IF p_result_code = 0
  THEN RETURN 'Успешно';
  ELSEIF p_result_code = 1
    THEN RETURN 'Аккаунт не найден';
  ELSEIF p_result_code = 2
    THEN RETURN 'Неверный пароль';
  ELSEIF p_result_code = 3
    THEN RETURN 'Аккаунт заблокирован';
  ELSEIF p_result_code = 4
    THEN RETURN 'Ошибка авторизации SSO';
  ELSEIF p_result_code = 5
    THEN RETURN 'Ошибка авторизации AD';
  ELSE RETURN NULL;
  END IF;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_auth_event_pkg$get_close_code_description(p_close_code INTEGER)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
BEGIN
  IF p_close_code = 0
  THEN RETURN 'Санкционированный выход';
  ELSEIF p_close_code = 1
    THEN RETURN 'Автоматическое завершение';
  ELSEIF p_close_code = 2
    THEN RETURN 'Разрыв соединения с сервером';
  ELSE RETURN NULL;
  END IF;
END;
$$;