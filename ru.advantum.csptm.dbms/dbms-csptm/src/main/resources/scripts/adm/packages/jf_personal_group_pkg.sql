CREATE OR REPLACE FUNCTION adm.jf_personal_group_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_account_id core.account.account_id%TYPE;
BEGIN
  l_account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', FALSE);

  OPEN p_rows FOR
  SELECT
    a.account_id,
    g.group_id,
    'Индивидуальные права' title,
    TRUE                   is_group_window
  FROM core.account a
    JOIN adm.account2group a2g ON a.account_id = a2g.account_id
    JOIN adm.group g ON a2g.group_id = g.group_id
  WHERE a.account_id = l_account_id AND g.is_personal;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_personal_group_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id adm.group.group_id%TYPE;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);

  DELETE FROM adm.data_realm2group
  WHERE group_id = l_group_id;

  DELETE FROM adm.role2group
  WHERE group_id = l_group_id;

  DELETE FROM adm.sys_audit2group
  WHERE group_id = l_group_id;

  DELETE FROM adm.incident_type2group
  WHERE group_id = l_group_id;

  DELETE FROM adm.report2group
  WHERE group_id = l_group_id;

  RETURN NULL;
END;
$$;