CREATE OR REPLACE FUNCTION adm.jf_account_pkg$attr_to_rowtype(p_attr TEXT)
  RETURNS core.ACCOUNT
LANGUAGE plpgsql
AS $$
DECLARE
  l_r core.account%ROWTYPE;
BEGIN
  l_r.first_name := jofl.jofl_pkg$extract_varchar(p_attr, 'first_name', TRUE);
  l_r.middle_name := jofl.jofl_pkg$extract_varchar(p_attr, 'middle_name', TRUE);
  l_r.last_name := jofl.jofl_pkg$extract_varchar(p_attr, 'last_name', TRUE);
  l_r.depo_id := jofl.jofl_pkg$extract_number(p_attr, 'depo_id', TRUE);
  l_r.facility_id := jofl.jofl_pkg$extract_number(p_attr, 'facility_id', TRUE);
  l_r.position := jofl.jofl_pkg$extract_varchar(p_attr, 'position', TRUE);
  l_r.is_locked := jofl.jofl_pkg$extract_boolean(p_attr, 'is_locked', TRUE);
  l_r.login := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'login', TRUE), '');
  l_r.password := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'password', TRUE), '');
  l_r.email :=jofl.jofl_pkg$extract_varchar(p_attr, 'email', TRUE);
  l_r.cell_phone :=jofl.jofl_pkg$extract_varchar(p_attr, 'cell_phone', TRUE);
  l_r.work_phone :=jofl.jofl_pkg$extract_varchar(p_attr, 'work_phone', TRUE);
  l_r.operator_phone := jofl.jofl_pkg$extract_varchar(p_attr, 'operator_phone', TRUE);
  l_r.is_email_subscriber :=jofl.jofl_pkg$extract_boolean(p_attr, 'is_email_subscriber', TRUE);
  l_r.is_phone_subscriber :=jofl.jofl_pkg$extract_boolean(p_attr, 'is_phone_subscriber', TRUE);
  l_r.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', TRUE);

  RETURN l_r;
END;
$$;


CREATE OR REPLACE FUNCTION adm.jf_account_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_r core.account%ROWTYPE;
BEGIN
  l_r := adm.jf_account_pkg$attr_to_rowtype(p_attr);
  l_r.dt_insert:= now();
  l_r.dt_update:= l_r.dt_insert;
  l_r.is_deleted:= FALSE;

  PERFORM adm.jf_account_pkg$validate(l_r.account_id, l_r.login, l_r.password);

  PERFORM adm.account_pkg$create(
      l_r.last_name,
      l_r.first_name,
      l_r.middle_name,
      l_r.login,
      l_r.password,
      l_r.dt_insert,
      l_r.dt_update,
      l_r.dt_lock,
      l_r.is_locked,
      l_r.is_deleted,
      l_r.email,
      l_r.cell_phone,
      l_r.work_phone,
      l_r.is_email_subscriber,
      l_r.is_phone_subscriber,
      l_r.facility_id,
      l_r.position,
      l_r.comment,
      l_r.depo_id,
      l_r.operator_phone
  );

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_account_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_f_account_id          core.account.account_id%TYPE;
  l_f_dt_insert_from      TIMESTAMP;
  l_f_dt_insert_to        TIMESTAMP;
  l_f_depo_id             core.depo.depo_id%TYPE;
  l_f_facility_id         core.facility.facility_id%TYPE;
    l_f_is_ad_account        BOOLEAN;
    l_f_is_locked            BOOLEAN;
    l_f_has_personal_rights  BOOLEAN;
    l_ad_fields              TEXT [] := ARRAY ['first_name', 'middle_name', 'last_name', 'dt_insert', 'is_locked', 'login',
  'password', 'cell_phone', 'work_phone'];
    l_rf_db_method           text := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
    l_adm_group_owner_filter int := case when l_rf_db_method = 'adm.group_admin' then jofl.jofl_pkg$extract_number(p_attr, 'group_id', TRUE) end;
    l_of_invalidate          text;
    l_is_group_filter        BOOLEAN;
BEGIN
  l_of_invalidate = adm.jofl_policy_pkg$build('A', array ['of_invalidate']);
  l_f_account_id := jofl.jofl_pkg$extract_number(p_attr, 'f_account_id', TRUE);
  l_f_dt_insert_from := jofl.jofl_pkg$extract_date(p_attr, 'f_dt_insert_from_DT', TRUE);
  l_f_dt_insert_to := jofl.jofl_pkg$extract_date(p_attr, 'f_dt_insert_to_DT', TRUE);
  l_f_depo_id := jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', TRUE);
  l_f_facility_id := jofl.jofl_pkg$extract_number(p_attr, 'f_facility_id', TRUE);

  IF jofl.jofl_pkg$extract_varchar(p_attr, 'f_is_ad_account_INPLACE_K', TRUE) <> ''
  THEN
    l_f_is_ad_account := jofl.jofl_pkg$extract_boolean(p_attr, 'f_is_ad_account_INPLACE_K', TRUE);
  END IF;
  IF jofl.jofl_pkg$extract_varchar(p_attr, 'f_is_locked_INPLACE_K', TRUE) <> ''
  THEN
    l_f_is_locked := jofl.jofl_pkg$extract_boolean(p_attr, 'f_is_locked_INPLACE_K', TRUE);
  END IF;
  IF jofl.jofl_pkg$extract_varchar(p_attr, 'f_has_personal_rights_INPLACE_K', TRUE) <> ''
  THEN
    l_f_has_personal_rights := jofl.jofl_pkg$extract_boolean(p_attr, 'f_has_personal_rights_INPLACE_K', TRUE);
  END IF;

  l_is_group_filter := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE) = 'adm.group', FALSE);

    open p_rows for
    select
        t.*,
        case when (t.is_ad_account and adm.jf_account_pkg$is_online(t.account_id))
            then adm.jofl_policy_pkg$append(l_of_invalidate, adm.jofl_policy_pkg$build('RO', l_ad_fields))
        when t.is_ad_account
            then adm.jofl_policy_pkg$build('RO', l_ad_fields)
        when (adm.jf_account_pkg$is_online(t.account_id))
            then l_of_invalidate
        else null end
            "ROW$POLICY"
    from (
         SELECT
           a.account_id,
           a.first_name,
           a.middle_name,
           a.last_name,
           adm.jf_account_pkg$get_full_name(a.account_id) full_name,
           a.depo_id,
           (SELECT e.name_short
            FROM core.entity e
            WHERE e.entity_id = a.depo_id)                depo_name,
           a.facility_id,
           (SELECT e.name_short
            FROM core.entity e
            WHERE e.entity_id = a.facility_id)            facility_name,
           a.position,
           a.dt_insert,
           a.is_locked,
           EXISTS(SELECT 1
                  FROM adm.ad_account aa
                  WHERE aa.account_id = a.account_id)     is_ad_account,
           (SELECT aa.ad_login || '@' || aad.name
            FROM
              adm.ad_account aa
              JOIN adm.ad_domain aad ON aa.ad_domain_id = aad.ad_domain_id
            WHERE
              aa.account_id = a.account_id
           )                                              ad_login,
           a.login,
           a.password,
           a.email,
           a.cell_phone,
           a.work_phone,
           a.operator_phone,
           a.is_email_subscriber,
           a.is_phone_subscriber,
           a.comment
         FROM core.account a
         WHERE
           (l_f_account_id IS NULL OR a.account_id = l_f_account_id)
           AND (l_f_dt_insert_from IS NULL OR a.dt_insert >= l_f_dt_insert_from)
           AND (l_f_dt_insert_to IS NULL OR a.dt_insert <= l_f_dt_insert_to)
           AND (l_f_depo_id IS NULL OR a.depo_id = l_f_depo_id)
           AND (l_f_facility_id IS NULL OR a.facility_id = l_f_facility_id)
           AND (l_f_is_ad_account IS NULL OR EXISTS(SELECT 1
                                                    FROM adm.ad_account aa
                                                    WHERE aa.account_id = a.account_id) = l_f_is_ad_account)
           AND (l_f_is_locked IS NULL OR a.is_locked = l_f_is_locked)
           AND (l_f_has_personal_rights IS NULL
                OR l_f_has_personal_rights = adm.account_pkg$has_personal_rights(a.account_id))
           AND (NOT l_is_group_filter
                OR exists(SELECT 1
                          FROM adm.group_owner go
                          WHERE go.account_id = a.account_id)
                OR exists(SELECT 1
                          FROM adm.group_admin ga
                          WHERE ga.account_id = a.account_id)
           )
           and (l_adm_group_owner_filter is null or exists (select from adm.account2group a2g where a2g.account_id = a.account_id and a2g.group_id = l_adm_group_owner_filter))
        ORDER BY a.account_id DESC
       ) t;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_account_pkg$get_link(p_account_id BIGINT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN '[' ||
         json_build_object(
             'CAPTION', adm.jf_account_pkg$get_full_name(p_account_id),
             'JUMP', json_build_object(
                 'METHOD', 'adm.account',
                 'ATTR', json_build_object('f_account_id', p_account_id) :: TEXT
             )
         )
         || ']';
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_account_pkg$get_full_name(p_account_id core.account.account_id%TYPE)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_result TEXT;
BEGIN
  SELECT coalesce(a.last_name || ' ', '') || coalesce(a.first_name || ' ', '') || coalesce(a.middle_name, '')
  INTO l_result
  FROM core.account a
  WHERE a.account_id = p_account_id;

  RETURN l_result;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_account_pkg$get_department(
  p_depo_id     core.depo.depo_id%TYPE,
  p_facility_id core.facility.facility_id%TYPE
)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_depo_name     TEXT;
  l_facility_name TEXT;
BEGIN
  SELECT e.name_short
  INTO l_depo_name
  FROM core.entity e
  WHERE e.entity_id = p_depo_id;

  SELECT e.name_short
  INTO l_facility_name
  FROM core.entity e
  WHERE e.entity_id = p_facility_id;

  RETURN coalesce(l_depo_name, l_facility_name, '');
END;
$$;
END;

CREATE OR REPLACE FUNCTION adm.jf_account_pkg$of_update(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_r core.account%ROWTYPE;
BEGIN
  l_r := adm.jf_account_pkg$attr_to_rowtype(p_attr);
  l_r.account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', FALSE);

  PERFORM adm.jf_account_pkg$check_not_base(l_r.account_id);

  PERFORM adm.jf_account_pkg$validate(l_r.account_id, l_r.login, l_r.password);

  UPDATE core.account
  SET
    first_name          = l_r.first_name,
    middle_name         = l_r.middle_name,
    last_name           = l_r.last_name,
    depo_id             = l_r.depo_id,
    facility_id         = l_r.facility_id,
    position            = l_r.position,
    is_locked           = l_r.is_locked,
    login               = l_r.login,
    password            = l_r.password,
    email               = l_r.email,
    cell_phone          = l_r.cell_phone,
    work_phone          = l_r.work_phone,
    operator_phone      = l_r.operator_phone,
    is_email_subscriber = l_r.is_email_subscriber,
    is_phone_subscriber = l_r.is_phone_subscriber,
    dt_update           = now(),
    comment             = l_r.comment
  WHERE account_id = l_r.account_id;

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_account_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_account_id BIGINT;
BEGIN
  l_account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', FALSE);

  PERFORM adm.jf_account_pkg$check_not_base(l_account_id);

  IF EXISTS(SELECT 1
            FROM adm.ad_account
            WHERE account_id = l_account_id)
  THEN RAISE EXCEPTION 'Невозможно удаление AD аккаунта';
  END IF;

  PERFORM adm.account_pkg$delete(l_account_id);

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_account_pkg$get_last_login_date(p_account_id NUMERIC)
  RETURNS TIMESTAMP
LANGUAGE plpgsql
AS $$
DECLARE
  l_result TIMESTAMP;
BEGIN

  SELECT le.event_date
  INTO l_result
  FROM adm.login_event le
    JOIN jofl.account ja ON le.id_jofl_account = ja.id_account_local
                            AND ja.schema_name = core.const_pkg$core_project_name()
    JOIN core.account a ON ja.id_account_local = a.account_id
  WHERE a.account_id = p_account_id
  ORDER BY le.event_date DESC
  LIMIT 1;

  RETURN l_result;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_account_pkg$check_not_base(p_account_id NUMERIC)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  IF p_account_id = 1 OR p_account_id = 2
  THEN
    RAISE EXCEPTION 'Данный пользователь является базовым. Редактирование и удаление невозможно';
  END IF;
END;
$$;


CREATE OR REPLACE FUNCTION adm.jf_account_pkg$validate(
  p_account_id core.account.account_id%TYPE,
  p_login      core.account.login%TYPE,
  p_password   core.account.password%TYPE
)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  IF NOT exists(SELECT 1
                FROM adm.ad_account aa
                WHERE aa.account_id = p_account_id)
  THEN
    IF p_login IS NULL OR p_login = ''
    THEN
      RAISE EXCEPTION 'Заполните поле "Логин"';
    END IF;

    IF p_password IS NULL OR p_password = ''
    THEN
      RAISE EXCEPTION 'Заполните поле "Пароль"';
    END IF;
  END IF;

  IF exists(
      SELECT a.account_id
      FROM core.account a
      WHERE a.login = p_login
            AND (a.account_id <> p_account_id OR p_account_id IS NULL)
  )
  THEN RAISE EXCEPTION 'Учетная запись % уже существует', p_login;
  END IF;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_account_pkg$of_invalidate(p_id_account NUMERIC, p_attr TEXT)
    RETURNS VOID
LANGUAGE plpgsql
AS $$
declare
    l_account_id integer:= jofl.jofl_pkg$extract_number(p_attr, 'account_id', FALSE);
    l_id_jofl_account integer;
BEGIN
    select
        a.id_jofl_account into l_id_jofl_account
    from jofl.account a
    where a.id_account_local=l_account_id;
    IF l_account_id = 1 OR l_account_id = 2
    THEN
        RAISE EXCEPTION 'Данный пользователь является базовым. Закрытие сессии невозможно';
    END IF;
    perform paa.paa_api_pkg$basic_publish(cmnd.jf_cmnd_ref4unit_pkg$cn_command_rmq_broker(),
                                          '',
                                          'ass-login-service',
                                          json_build_object('idJoflAccount', l_id_jofl_account) :: text,
                                          'RECALC_REQUEST');
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_account_pkg$is_online(p_account_id bigint)
    RETURNS boolean
LANGUAGE plpgsql
AS $$

BEGIN
    return exists(
    select *
    from jofl.account ja
        join adm.login_event le on ja.id_jofl_account = le.id_jofl_account
        join adm.session ss on le.login_event_id = ss.login_event_id
    where ja.id_account_local = p_account_id and close_code isnull);
END;
$$;


