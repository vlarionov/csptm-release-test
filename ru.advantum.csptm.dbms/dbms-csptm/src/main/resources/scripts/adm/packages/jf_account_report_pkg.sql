CREATE OR REPLACE FUNCTION adm.jf_account_report_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                             p_attr       TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_account_id core.account.account_id%TYPE;
BEGIN
  l_account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', FALSE);

  OPEN p_rows FOR
  SELECT
    a2g.account_id,
    r.s_name,
    string_agg(g.name, ', ') group_name
  FROM adm.account2group a2g
    JOIN adm.group g ON a2g.group_id = g.group_id
    , adm.group_data_pkg$get_reports(g.group_id) g2r
    JOIN jrep.report r ON g2r.id_report = r.id_report
  WHERE a2g.account_id = l_account_id
  GROUP BY r.id_report, a2g.account_id, r.s_name;
END;
$$;