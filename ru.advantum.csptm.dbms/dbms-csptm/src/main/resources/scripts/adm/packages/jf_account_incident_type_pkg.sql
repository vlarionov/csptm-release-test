CREATE OR REPLACE FUNCTION adm.jf_account_incident_type_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                                 p_attr       TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_account_id core.account.account_id%TYPE;
BEGIN
  l_account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', FALSE);

  OPEN p_rows FOR
  SELECT
    a2g.account_id,
    it.incident_type_name,
    string_agg(g.name, ', ') group_name
  FROM  adm.account2group a2g
    JOIN adm.group g ON a2g.group_id = g.group_id
    ,adm.group_data_pkg$get_incident_types(g.group_id) g2et
    JOIN ttb.incident_type it ON g2et.incident_type_id = it.incident_type_id
  WHERE a2g.account_id = l_account_id
  GROUP BY it.incident_type_id, a2g.account_id, it.incident_type_name;

END;
$$;