CREATE OR REPLACE FUNCTION adm.group_data_pkg$is_super_group(p_group_id adm.group.group_id%TYPE)
  RETURNS BOOLEAN
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN p_group_id = 411 OR p_group_id = 172;
END;
$$;

CREATE OR REPLACE FUNCTION adm.group_data_pkg$check_is_not_super_group(p_group_id adm.group.group_id%TYPE)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  IF adm.group_data_pkg$is_super_group(p_group_id)
  THEN
    RAISE EXCEPTION 'Нельзя изменять супер группу';
  END IF;
END;
$$;

CREATE OR REPLACE FUNCTION adm.group_data_pkg$get_incident_types(p_group_id adm.group.group_id%TYPE)
  RETURNS TABLE(
    incident_type_id adm.incident_type2group.incident_type_id%TYPE
  )
LANGUAGE plpgsql
AS $$
BEGIN
  IF adm.group_data_pkg$is_super_group(p_group_id)
  THEN RETURN QUERY
  SELECT
    et.incident_type_id
  FROM ttb.incident_type et;
  END IF;

  RETURN QUERY
  SELECT
    et2g.incident_type_id
  FROM adm.incident_type2group et2g
  WHERE et2g.group_id = p_group_id;
END;
$$;

CREATE OR REPLACE FUNCTION adm.group_data_pkg$get_sys_audits(p_group_id adm.group.group_id%TYPE)
  RETURNS TABLE(sys_audit_id adm.sys_audit.sys_audit_id%TYPE)
LANGUAGE plpgsql
AS $$
BEGIN
  IF adm.group_data_pkg$is_super_group(p_group_id)
  THEN RETURN QUERY
  SELECT sa.sys_audit_id
  FROM adm.sys_audit sa;
  END IF;

  RETURN QUERY
  SELECT sa2g.sys_audit_id
  FROM adm.sys_audit2group sa2g
  WHERE sa2g.group_id = p_group_id;
END;
$$;

CREATE OR REPLACE FUNCTION adm.group_data_pkg$get_reports(p_group_id adm.group.group_id%TYPE)
  RETURNS TABLE(id_report jrep.report.id_report%TYPE)
LANGUAGE plpgsql
AS $$
BEGIN
  IF adm.group_data_pkg$is_super_group(p_group_id)
  THEN RETURN QUERY
  SELECT r.id_report
  FROM jrep.report r;
  END IF;

  RETURN QUERY
  SELECT r2g.id_report
  FROM adm.report2group r2g
  WHERE r2g.group_id = p_group_id;
END;
$$;

CREATE OR REPLACE FUNCTION adm.group_data_pkg$get_data_realms(p_group_id adm.group.group_id%TYPE)
  RETURNS TABLE(data_realm_id adm.data_realm.data_realm_id%TYPE)
LANGUAGE plpgsql
AS $$
BEGIN
  IF adm.group_data_pkg$is_super_group(p_group_id)
  THEN RETURN QUERY
  SELECT dr.data_realm_id
  FROM adm.data_realm dr;
  END IF;

  RETURN QUERY
  SELECT dr2g.data_realm_id
  FROM adm.data_realm2group dr2g
  WHERE dr2g.group_id = p_group_id;
END;
$$;

CREATE OR REPLACE FUNCTION adm.group_data_pkg$get_data_realms(p_group_ids INTEGER [])
  RETURNS setof adm.data_realm.data_realm_id%TYPE
LANGUAGE plpgsql
AS $$
BEGIN
  FOR i IN 1..coalesce(array_upper(p_group_ids, 1), 1)
  LOOP
    IF adm.group_data_pkg$is_super_group(p_group_ids[i])
    THEN RETURN QUERY
    SELECT dr.data_realm_id
    FROM adm.data_realm dr;
    END IF;
  END LOOP;

  RETURN QUERY
  SELECT dr2g.data_realm_id
  FROM adm.data_realm2group dr2g
  WHERE dr2g.group_id = ANY (p_group_ids);
END;
$$;

CREATE OR REPLACE FUNCTION adm.group_data_pkg$get_roles(p_group_id adm.group.group_id%TYPE)
  RETURNS TABLE(id_role jofl.ref_role.id_role%TYPE)
LANGUAGE plpgsql
AS $$
BEGIN
  IF adm.group_data_pkg$is_super_group(p_group_id)
  THEN RETURN QUERY
  SELECT rr.id_role
  FROM jofl.ref_role rr
  WHERE rr.is_active = 1;
  END IF;

  RETURN QUERY
  SELECT r2g.id_role
  FROM adm.role2group r2g
  WHERE r2g.group_id = p_group_id;
END;
$$;

CREATE OR REPLACE FUNCTION adm.group_data_pkg$get_containing_sys_audit(p_sys_audit_id adm.sys_audit.sys_audit_id%TYPE)
  RETURNS TABLE(group_id adm.group.group_id%TYPE)
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT g.group_id
  FROM adm.group g
  WHERE
    adm.group_data_pkg$is_super_group(g.group_id) OR
    exists(
        SELECT 1
        FROM adm.sys_audit2group sa2g
        WHERE sa2g.sys_audit_id = p_sys_audit_id AND sa2g.group_id = g.group_id
    );
END;
$$;

CREATE OR REPLACE FUNCTION adm.group_data_pkg$get_containing_data_realm(p_data_realm_id adm.data_realm.data_realm_id%TYPE)
  RETURNS TABLE(group_id adm.group.group_id%TYPE)
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT g.group_id
  FROM adm.group g
  WHERE
    adm.group_data_pkg$is_super_group(g.group_id) OR
    exists(
        SELECT 1
        FROM adm.data_realm2group dr2g
        WHERE dr2g.data_realm_id = p_data_realm_id AND dr2g.group_id = g.group_id
    );
END;
$$;

CREATE OR REPLACE FUNCTION adm.group_data_pkg$get_containing_role(p_id_role jofl.ref_role.id_role%TYPE)
  RETURNS TABLE(group_id adm.group.group_id%TYPE)
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT g.group_id
  FROM adm.group g
  WHERE adm.group_data_pkg$is_super_group(g.group_id) OR exists(
      SELECT 1
      FROM adm.role2group r2g
      WHERE r2g.id_role = p_id_role AND r2g.group_id = g.group_id
  );
END;
$$;