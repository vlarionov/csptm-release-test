CREATE OR REPLACE FUNCTION adm.jf_group_role_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id INTEGER;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);

  OPEN p_rows FOR
  SELECT
    l_group_id                                                         group_id,
    r.id_role,
    r.role_name,
    adm.group_pkg$get_of_delete_owner_policy(p_id_account, l_group_id) "ROW$POLICY"
  FROM adm.group_data_pkg$get_roles(l_group_id) g2r
    JOIN jofl.ref_role r ON g2r.id_role = r.id_role
  ORDER BY r.id_role DESC;
END;
$$;


CREATE OR REPLACE FUNCTION adm.jf_group_role_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id INTEGER;
  l_id_role  TEXT;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);
  l_id_role := jofl.jofl_pkg$extract_varchar(p_attr, 'id_role', FALSE);

  PERFORM adm.group_data_pkg$check_is_not_super_group(l_group_id);
  PERFORM adm.group_pkg$check_is_owner(p_id_account, l_group_id);

  INSERT INTO adm.role2group (group_id, id_role) VALUES (
    l_group_id,
    l_id_role
  );

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_group_role_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id INTEGER;
  l_id_role  TEXT;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);
  l_id_role := jofl.jofl_pkg$extract_varchar(p_attr, 'id_role', FALSE);

  PERFORM adm.group_data_pkg$check_is_not_super_group(l_group_id);
  PERFORM adm.group_pkg$check_is_owner(p_id_account, l_group_id);

  DELETE FROM adm.role2group
  WHERE group_id = l_group_id AND id_role = l_id_role;

  RETURN NULL;
END;
$$;