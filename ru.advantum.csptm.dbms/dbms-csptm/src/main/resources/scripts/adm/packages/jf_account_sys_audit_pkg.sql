CREATE OR REPLACE FUNCTION adm.jf_account_sys_audit_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                                p_attr       TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_account_id core.account.account_id%TYPE;
BEGIN
  l_account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', FALSE);

  OPEN p_rows FOR
  SELECT
    a2g.account_id,
    sa.name,
    string_agg(g.name, ', ') group_name
  FROM adm.account2group a2g
    JOIN adm.group g ON a2g.group_id = g.group_id
    , adm.group_data_pkg$get_sys_audits(g.group_id) g2sa
    JOIN adm.sys_audit sa ON g2sa.sys_audit_id = sa.sys_audit_id
  WHERE a2g.account_id = l_account_id
  GROUP BY sa.sys_audit_id, a2g.account_id, sa.name;
END;
$$;