CREATE OR REPLACE FUNCTION adm.group_pkg$is_owner(p_id_account NUMERIC, p_group_id adm.group.group_id%TYPE)
  RETURNS BOOLEAN
LANGUAGE plpgsql
AS $$
BEGIN
  IF adm.group_pkg$is_super_admin(p_id_account) OR exists(SELECT 1
                                                                     FROM adm.group g
                                                                     WHERE g.group_id = p_group_id AND g.is_personal)
  THEN
    RETURN TRUE;
  END IF;

  RETURN exists(
      SELECT 1
      FROM adm.group_owner go
      WHERE go.account_id = p_id_account AND go.group_id = p_group_id
  );
END;
$$;

CREATE OR REPLACE FUNCTION adm.group_pkg$is_admin(p_id_account NUMERIC, p_group_id adm.group.group_id%TYPE)
  RETURNS BOOLEAN
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN exists(
      SELECT 1
      FROM adm.group_admin go
      WHERE go.account_id = p_id_account AND go.group_id = p_group_id
  );
END;
$$;

CREATE OR REPLACE FUNCTION adm.group_pkg$is_ad_sync(p_group_id adm.group.group_id%TYPE)
  RETURNS BOOLEAN
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN exists(
      SELECT 1
      FROM adm.group g
        INNER JOIN adm.ad_group adg ON adg.group_id=g.group_id WHERE g.group_id = p_group_id
  );
END;
$$;

CREATE OR REPLACE FUNCTION adm.group_pkg$is_owner_or_admin(p_id_account NUMERIC, p_group_id adm.group.group_id%TYPE)
  RETURNS BOOLEAN
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN adm.group_pkg$is_owner(p_id_account, p_group_id) OR adm.group_pkg$is_admin(p_id_account, p_group_id);
END;
$$;

CREATE OR REPLACE FUNCTION adm.group_pkg$check_is_owner(p_account_id NUMERIC, p_group_id adm.group.group_id%TYPE)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  IF NOT adm.group_pkg$is_owner(p_account_id, p_group_id)
  THEN
    RAISE EXCEPTION 'Нет доступа';
  END IF;
END;
$$;

CREATE OR REPLACE FUNCTION adm.group_pkg$check_is_owner_or_admin(p_account_id NUMERIC, p_group_id adm.group.group_id%TYPE)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  IF NOT adm.group_pkg$is_owner_or_admin(p_account_id, p_group_id)
  THEN
    RAISE EXCEPTION 'Нет доступа';
  END IF;
END;
$$;

CREATE OR REPLACE FUNCTION adm.group_pkg$get_of_delete_owner_policy(p_id_account NUMERIC, p_group_id adm.group.group_id%TYPE)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
BEGIN
  IF adm.group_pkg$is_owner(p_id_account, p_group_id)
  THEN RETURN adm.jofl_policy_pkg$build('A', 'OF_DELETE');
  ELSE RETURN NULL;
  END IF;
END;
$$;

CREATE OR REPLACE FUNCTION adm.group_pkg$get_of_delete_admin_policy(p_id_account NUMERIC, p_group_id adm.group.group_id%TYPE)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
BEGIN
  IF adm.group_pkg$is_owner_or_admin(p_id_account, p_group_id)
  THEN RETURN adm.jofl_policy_pkg$build('A', 'OF_DELETE');
  ELSE RETURN NULL;
  END IF;
END;
$$;


CREATE OR REPLACE FUNCTION adm.group_pkg$get_administrable_incident_types_ids(p_account_id NUMERIC)
  RETURNS SMALLINT []
LANGUAGE plpgsql
AS $$
BEGIN
  IF adm.group_pkg$is_super_admin(p_account_id)
  THEN
    RETURN ARRAY(SELECT ttb.incident_type_id
                 FROM ttb.incident_type et);
  END IF;

  RETURN ARRAY(
      SELECT DISTINCT g2et.incident_type_id
      FROM adm.group_admin ga,
            adm.group_data_pkg$get_incident_types(ga.group_id) g2et
      WHERE ga.account_id = p_account_id
  );
END;
$$;


CREATE OR REPLACE FUNCTION adm.group_pkg$get_administrable_sys_audit_ids(p_account_id NUMERIC)
  RETURNS INTEGER []
LANGUAGE plpgsql
AS $$
BEGIN
  IF adm.group_pkg$is_super_admin(p_account_id)
  THEN
    RETURN ARRAY(SELECT sa.sys_audit_id
                 FROM adm.sys_audit sa);
  END IF;

  RETURN ARRAY(
      SELECT DISTINCT adm.group_data_pkg$get_sys_audits(ga.group_id)
      FROM adm.group_admin ga
      WHERE ga.account_id = p_account_id
  );
END;
$$;

CREATE OR REPLACE FUNCTION adm.group_pkg$get_administrable_reports_ids(p_account_id NUMERIC)
  RETURNS NUMERIC []
LANGUAGE plpgsql
AS $$
BEGIN
  IF adm.group_pkg$is_super_admin(p_account_id)
  THEN
    RETURN ARRAY(SELECT r.id_report
                 FROM jrep.report r);
  END IF;

  RETURN ARRAY(
      SELECT DISTINCT adm.group_data_pkg$get_reports(ga.group_id)
      FROM adm.group_admin ga
      WHERE ga.account_id = p_account_id
  );
END;
$$;

CREATE OR REPLACE FUNCTION adm.group_pkg$get_administrable_data_realms_ids(p_account_id NUMERIC)
  RETURNS INTEGER []
LANGUAGE plpgsql
AS $$
BEGIN
  IF adm.group_pkg$is_super_admin(p_account_id)
  THEN
    RETURN ARRAY(SELECT da.data_realm_id
                 FROM adm.data_realm da);
  END IF;

  RETURN ARRAY(
      SELECT DISTINCT adm.group_data_pkg$get_data_realms(ga.group_id)
      FROM adm.group_admin ga
      WHERE ga.account_id = p_account_id
  );
END;
$$;


CREATE OR REPLACE FUNCTION adm.group_pkg$get_administrable_roles_ids(p_account_id NUMERIC)
  RETURNS TEXT []
LANGUAGE plpgsql
AS $$
BEGIN
  IF adm.group_pkg$is_super_admin(p_account_id)
  THEN
    RETURN ARRAY(SELECT r.id_role
                 FROM jofl.ref_role r);
  END IF;

  RETURN ARRAY(
      SELECT DISTINCT adm.group_data_pkg$get_roles(ga.group_id)
      FROM adm.group_admin ga
      WHERE ga.account_id = p_account_id
  );
END;
$$;

CREATE OR REPLACE FUNCTION adm.group_pkg$is_super_admin(p_account_id NUMERIC)
  RETURNS BOOLEAN
IMMUTABLE
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN exists(
    SELECT 1
    FROM adm.account2group a2g
    WHERE a2g.account_id = p_account_id AND adm.group_data_pkg$is_super_group(a2g.group_id)
  );
END;
$$;