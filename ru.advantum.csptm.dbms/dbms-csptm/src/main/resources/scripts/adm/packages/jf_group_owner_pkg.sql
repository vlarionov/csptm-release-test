CREATE OR REPLACE FUNCTION adm.jf_group_owner_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                          p_attr       TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id adm.group.group_id%TYPE;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);
  OPEN p_rows FOR
  SELECT
    go.group_id,
    a.account_id,
    a.login,
    aa.ad_login,
    adm.jf_account_pkg$get_full_name(a.account_id)                     full_name,
    adm.group_pkg$get_of_delete_owner_policy(p_id_account, l_group_id) "ROW$POLICY"
  FROM adm.group_owner go
    JOIN core.account a ON go.account_id = a.account_id
   left JOIN adm.ad_account aa on go.account_id = aa.account_id
  WHERE go.group_id = l_group_id;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_group_owner_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id   adm.group.group_id%TYPE;
  l_account_id core.account.account_id%TYPE;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);
  l_account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', FALSE);

  PERFORM adm.group_pkg$check_is_owner(p_id_account, l_group_id);

  INSERT INTO adm.group_owner (account_id, group_id) VALUES (
    l_account_id,
    l_group_id
  );

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_group_owner_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id   adm.group.group_id%TYPE;
  l_account_id core.account.account_id%TYPE;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);
  l_account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', FALSE);

  PERFORM adm.group_pkg$check_is_owner(p_id_account, l_group_id);

  DELETE FROM adm.group_owner
  WHERE group_id = l_group_id AND account_id = l_account_id;

  RETURN NULL;
END;
$$;