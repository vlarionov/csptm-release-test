CREATE OR REPLACE FUNCTION adm.jf_history_operation_filter_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                                       p_attr       TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
    l_f_db_method jofl.window.db_method%type;
    l_F_INPLACE_K int :=jofl.jofl_pkg$extract_number(p_attr, 'F_INPLACE_K', true);

BEGIN
  l_f_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'f_db_method', TRUE);

    if (l_F_INPLACE_K = 0)
    then
        open p_rows
        for select
                'OF_ROWS' action_title,
                'OF_ROWS' call_action;
    else
        open p_rows
        for select
                adm.jf_history_data_pkg$get_action_title(wac.db_method, wac.call_action) action_title,
                wac.call_action
            from jofl.win_action_call wac
            where wac.db_method = l_f_db_method;
    end if;
END;
$$;
