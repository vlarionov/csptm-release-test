CREATE OR REPLACE FUNCTION adm.jf_monitoring_incident_pkg$get_available_incident_types (p_account_id numeric) RETURNS TABLE(incident_type_id smallint, can_confirm boolean)
	LANGUAGE plpgsql
AS $$

BEGIN
  RETURN QUERY
  SELECT
    g2et.incident_type_id,
    false
  FROM adm.account2group a2g,
        adm.group_data_pkg$get_incident_types(a2g.group_id) g2et
  WHERE a2g.account_id = p_account_id
  GROUP BY g2et.incident_type_id
  ;
END;
$$
