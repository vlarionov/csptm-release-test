CREATE OR REPLACE FUNCTION adm.sys_audit_pkg$sys_audit_handler()
  RETURNS TRIGGER
LANGUAGE plpgsql
AS $$
DECLARE
  l_event_date       TIMESTAMP := now();
  l_operation        INTEGER;
  l_sys_audit        RECORD;
  l_query            TEXT;
  l_event_attributes JSON;
BEGIN
  IF tg_op = 'INSERT'
  THEN l_operation = adm.const_pkg$insert_op();
  ELSEIF tg_op = 'UPDATE'
    THEN l_operation = adm.const_pkg$update_op();
  ELSEIF tg_op = 'DELETE'
    THEN l_operation = adm.const_pkg$delete_op();
  END IF;

  FOR l_sys_audit IN
  SELECT *
  FROM adm.sys_audit sa
  WHERE sa.table_name = TG_TABLE_SCHEMA || '.' || TG_TABLE_NAME AND l_operation = sa.operation
  LOOP
    l_query = 'SELECT coalesce(array_to_json(array_agg(t)), ''[]''::JSON) FROM (' || l_sys_audit.query || ') t';

    IF l_query IS NULL
    THEN
      l_event_attributes := '{}' :: JSON;
    ELSE
      IF l_operation = adm.const_pkg$insert_op()
      THEN
        EXECUTE l_query
        USING NEW
        INTO l_event_attributes;
      ELSEIF l_operation = adm.const_pkg$update_op()
        THEN
          EXECUTE l_query
          USING OLD, NEW
          INTO l_event_attributes;
      ELSEIF l_operation = adm.const_pkg$delete_op()
        THEN
          EXECUTE l_query
          USING OLD
          INTO l_event_attributes;
      END IF;
    END IF;

    INSERT INTO adm.sys_event (sys_event_id, sys_audit_id, event_attributes, event_date, is_notified) VALUES (
      nextval('adm.sys_event_id_seq'),
      l_sys_audit.sys_audit_id,
      l_event_attributes,
      l_event_date,
      FALSE
    );
  END LOOP;

  IF (l_operation = adm.const_pkg$insert_op() OR l_operation = adm.const_pkg$update_op())
  THEN
    RETURN NEW;
  ELSE
    RETURN OLD;
  END IF;

  EXCEPTION WHEN OTHERS
  THEN
    RAISE NOTICE 'SQLSTATE: %', SQLSTATE;

    IF (l_operation = adm.const_pkg$insert_op() OR l_operation = adm.const_pkg$update_op())
    THEN
      RETURN NEW;
    ELSE
      RETURN OLD;
    END IF;

END;
$$;


CREATE OR REPLACE FUNCTION adm.sys_audit_pkg$after_insert_audit(p_table_name TEXT)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE
  l_audit_count BIGINT;
BEGIN
  LOCK TABLE adm.sys_audit IN SHARE MODE;

  SELECT COUNT(1)
  INTO l_audit_count
  FROM adm.sys_audit sa
  WHERE sa.table_name = p_table_name;

  IF l_audit_count = 1
  THEN
    EXECUTE
    format(
        'CREATE TRIGGER sys_audit ' ||
        'AFTER INSERT OR UPDATE OR DELETE ' ||
        'ON %s FOR EACH ROW ' ||
        'EXECUTE PROCEDURE adm.sys_audit_pkg$sys_audit_handler()',
        p_table_name
    );
  END IF;
END;
$$;

CREATE OR REPLACE FUNCTION adm.sys_audit_pkg$before_delete_audit(p_table_name TEXT)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE
  l_audit_count BIGINT;
BEGIN
  LOCK TABLE adm.sys_audit IN SHARE MODE;

  SELECT COUNT(1)
  INTO l_audit_count
  FROM adm.sys_audit sa
  WHERE sa.table_name = p_table_name;

  IF l_audit_count = 1
  THEN
    EXECUTE format('DROP TRIGGER sys_audit ON %s', p_table_name);
  END IF;
END;
$$;

-- CREATE OR REPLACE FUNCTION adm.sys_audit_pkg$after_update_audit(p_old_table_name TEXT, p_new_table_name TEXT)
--   RETURNS VOID
-- LANGUAGE plpgsql
-- AS $$
-- BEGIN
--   LOCK TABLE adm.sys_audit IN SHARE MODE;
--
--   IF p_old_table_name != p_new_table_name
--   THEN
--     EXECUTE sys_audit_pkg$before_delete_audit(p_old_table_name);
--     EXECUTE sys_audit_pkg$after_insert_audit(p_new_table_name);
--   END IF;
-- END;
-- $$;

CREATE OR REPLACE FUNCTION adm.sys_audit_pkg$find_non_notified_events()
  RETURNS TABLE(
    sys_event_id     INTEGER,
    sys_audit_id     INTEGER,
    audit_name       TEXT,
    event_attributes TEXT,
    message_template TEXT
  )
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT
    se.sys_event_id,
    se.sys_audit_id,
    sa.name,
    se.event_attributes :: TEXT,
    sa.message_template
  FROM
    adm.sys_event se
    JOIN adm.sys_audit sa ON se.sys_audit_id = sa.sys_audit_id
  WHERE NOT se.is_notified;
END;
$$;

CREATE OR REPLACE FUNCTION adm.sys_audit_pkg$find_subscribers(p_sys_audit_id adm.sys_event.sys_audit_id%TYPE)
  RETURNS TABLE(
    is_email_subscriber BOOLEAN,
    email               TEXT,
    is_phone_subscriber BOOLEAN,
    phone_number        TEXT
  )
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT
    a.is_email_subscriber,
    a.email,
    a.is_phone_subscriber,
    a.cell_phone
  FROM adm.group_data_pkg$get_containing_sys_audit(p_sys_audit_id) g
    JOIN adm.account2group a2g ON g.group_id = a2g.group_id
    JOIN core.account a ON a2g.account_id = a.account_id
  WHERE a.is_email_subscriber OR a.is_phone_subscriber
  GROUP BY a.account_id;
END;
$$;

CREATE OR REPLACE FUNCTION adm.sys_audit_pkg$set_sys_event_notified(p_sys_event_id INTEGER)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  UPDATE adm.sys_event
  SET is_notified = TRUE
  WHERE sys_event_id = p_sys_event_id;
END;
$$;