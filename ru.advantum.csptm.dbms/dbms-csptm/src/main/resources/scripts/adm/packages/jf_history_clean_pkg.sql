create or replace function adm.jf_history_clean_pkg$of_rows(p_id_account numeric, out p_rows refcursor,
                                                              p_attr       text)
    returns refcursor
language plpgsql
as $$
declare
    l_available_windows text [];
begin
    l_available_windows := adm.history_pkg$get_available_windows(p_id_account);
    open p_rows
    for select
            w.db_method,
            w.win_title,
            case when hc.cleaner_interval=0 then null else hc.cleaner_interval end
        from jofl.window w
            left join adm.history_clean hc on w.db_method = hc.db_method
        where w.db_method = any (l_available_windows);
end;
$$;


create or replace function adm.jf_history_clean_pkg$OF_UPDATE(p_id_account numeric,
                                                                p_attr       text)
    returns text
language plpgsql
as $$
declare
    l_db_method        text;
    l_cleaner_interval integer;
begin
    l_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'db_method', true);
    l_cleaner_interval := jofl.jofl_pkg$extract_number(p_attr, 'cleaner_interval', true);
    if (exists(select *
               from adm.history_clean hc
               where hc.db_method = l_db_method))
    then
        update adm.history_clean
        set
            cleaner_interval = l_cleaner_interval
        where db_method = l_db_method;
    else
        insert into adm.history_clean (db_method, cleaner_interval) values (l_db_method, l_cleaner_interval);
    end if;
    return null;
end;
$$;




