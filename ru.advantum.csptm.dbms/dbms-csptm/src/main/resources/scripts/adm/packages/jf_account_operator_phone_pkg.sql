CREATE OR REPLACE FUNCTION adm.jf_account_operator_phone_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                                     p_attr       TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
BEGIN
  OPEN p_rows FOR
  SELECT a.operator_phone
  FROM core.account a
  WHERE a.account_id = p_id_account AND a.operator_phone IS NOT NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_account_operator_phone_pkg$of_update(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_operator_phone TEXT;
BEGIN
  l_operator_phone := jofl.jofl_pkg$extract_varchar(p_attr, 'operator_phone', FALSE);

  UPDATE core.account
  SET operator_phone = l_operator_phone
  WHERE account_id = p_id_account;

  RETURN NULL;
END;
$$;