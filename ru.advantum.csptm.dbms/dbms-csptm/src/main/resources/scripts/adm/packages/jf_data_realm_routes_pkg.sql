create or replace function adm.jf_data_realm_routes_pkg$of_rows(p_id_account numeric, out p_rows refcursor,
                                                                p_attr       text)
  returns refcursor
language plpgsql
as $$
declare
  l_data_realm_id adm.data_realm.data_realm_id%type := jofl.jofl_pkg$extract_number(p_attr, 'data_realm_id', false);
begin
  open p_rows for
  select
    l_data_realm_id data_realm_id,
    r.route_id,
    r.route_num,
    trt.name        tr_type_name
  from rts.route r
    join core.tr_type trt on r.tr_type_id = trt.tr_type_id
  where r.route_id in (select adm.data_realm_pkg$get_routes(array [l_data_realm_id]))
  order by r.route_num;
end;
$$;

create or replace function adm.jf_data_realm_routes_pkg$of_multi_insert(p_id_account numeric, p_attr text)
  returns text
language plpgsql
as $$
declare
  l_data_realm_id adm.data_realm.data_realm_id%type := jofl.jofl_pkg$extract_number(p_attr, 'data_realm_id', false);
  l_route_ids     integer [] := jofl.jofl_pkg$extract_narray(p_attr, 'f_route_id', false);
begin
  perform adm.data_realm_pkg$check_is_not_super_data_realm(l_data_realm_id);

  insert into adm.data_realm2route (data_realm_id, route_id)
    select
      l_data_realm_id,
      unnest(l_route_ids);

  return null;
end;
$$;

create or replace function adm.jf_data_realm_routes_pkg$of_delete(p_id_account numeric, p_attr text)
  returns text
language plpgsql
as $$
declare
  l_data_realm_id adm.data_realm.data_realm_id%type := jofl.jofl_pkg$extract_number(p_attr, 'data_realm_id', false);
  l_route_id      rts.route.route_id%type := jofl.jofl_pkg$extract_number(p_attr, 'route_id', false);
begin

  perform adm.data_realm_pkg$check_is_not_super_data_realm(l_data_realm_id);

  delete from adm.data_realm2route
  where data_realm_id = l_data_realm_id and route_id = l_route_id;

  return null;
end;
$$;