create or replace function adm.jf_history_data_pkg$of_rows(p_id_account numeric, out p_rows refcursor, p_attr text)
    returns refcursor
language plpgsql
as $$
declare
    l_db_method                 text;
    l_f_change_date_from        timestamp;
    l_f_change_date_to          timestamp;
    l_f_login_or_full_name_TEXT text;
    l_f_call_action             text;
    l_F_INPLACE_K               int;
begin
    l_F_INPLACE_K:=jofl.jofl_pkg$extract_number(p_attr, 'F_INPLACE_K', true);
    l_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'f_db_method', true);
    if l_db_method is not null and not (l_db_method = any (adm.history_pkg$get_available_windows(p_id_account)))
    then
        open p_rows for select 1
                        where false;
    end if;

    l_f_change_date_from := jofl.jofl_pkg$extract_date(p_attr, 'f_change_date_from_DT', true);
    l_f_change_date_to := jofl.jofl_pkg$extract_date(p_attr, 'f_change_date_to_DT', true);
    l_f_login_or_full_name_TEXT := jofl.jofl_pkg$extract_varchar(p_attr, 'f_login_or_full_name_TEXT', true);
    l_f_call_action := jofl.jofl_pkg$extract_varchar(p_attr, 'f_call_action', true);

    /*SELECT w.interface_pkg
    INTO l_window_pkg
    FROM jofl.window w
      JOIN adm.win_table wt ON w.db_method = wt.db_method
    WHERE w.db_method = l_db_method;

    EXECUTE 'SELECT ' || l_window_pkg || '$of_history($1, $2)'
    INTO p_rows
    USING p_id_account, p_attr;*/

    if (l_F_INPLACE_K = 1)
    then
        open p_rows for
        select
            hd.history_data_id,
            hd.account_full_name,
            hd.login,
            hd.department_name,
            hd.position,
            hd.win_title,
            hd.action_title,
            hd.change_date,
            hd.table_schema,
            hd.table_name,
            hd.operation,
            hd.row_id,
            hd.db_method,
            hd.db_action,
            hd."ROW$POLICY"
        from
            (select
                 hd.history_data_id,
                 adm.jf_history_data_pkg$get_account_link(
                     ca.account_id,
                     coalesce(ca.last_name || ' ', '') || coalesce(ca.first_name || ' ', '') || coalesce(ca.middle_name, '')
                 )                                                                           account_full_name,
                 ca.login,
                 ada.ad_login,
                 adm.jf_account_pkg$get_department(ca.depo_id, ca.facility_id)               department_name,
                 ca.position,
                 adm.jf_history_data_pkg$get_win_title(wt.db_method)                         win_title,
                 adm.jf_history_data_pkg$get_action_title(wt.db_method, hd.operation)        action_title,
                 hd.change_date,
                 hd.table_schema,
                 hd.table_name,
                 hd.operation,
                 hd.row_id,
                 hdw.win_title                                                               db_method,
                 trim(trailing '>>' from trim(leading '<<' from hdwac.action_title)) :: text db_action,
                 'P{D},D{adm.history_data_item}' :: text as                                  "ROW$POLICY"
             from adm.win_table wt
                 join jofl.window w on wt.db_method = w.db_method
                 join adm.history_data hd on wt.table_schema = hd.table_schema and wt.table_name = hd.table_name
                 join jofl.account ja on hd.id_jofl_account = ja.id_jofl_account
                 join core.account ca on ja.id_account_local = ca.account_id
                 left join adm.ad_account ada on ca.account_id = ada.account_id
                 join jofl.window hdw on hd.db_method = hdw.db_method
                 join jofl.win_action_call hdwac on hdw.db_method = hdwac.db_method and hd.db_action = hdwac.call_action
             where wt.db_method = l_db_method
                   and (l_f_change_date_from is null or hd.change_date >= l_f_change_date_from)
                   and (l_f_change_date_to is null or hd.change_date <= l_f_change_date_to)
                   and ((l_f_call_action = '') is not false or hd.db_action = l_f_call_action)
             order by hd.change_date desc
            ) hd
        where ((l_f_login_or_full_name_TEXT = '') is not false or
               lower(hd.account_full_name) like '%' || lower(l_f_login_or_full_name_TEXT) || '%' or
               hd.login like '%' || lower(l_f_login_or_full_name_TEXT) || '%');
    else
        open p_rows for
        select
            dmah.db_method_access_history_id                              as history_data_id,
            adm.jf_history_data_pkg$get_account_link(
                ca.account_id,
                coalesce(ca.last_name || ' ', '') || coalesce(ca.first_name || ' ', '') || coalesce(ca.middle_name, '')
            )                                                             as account_full_name,
            ca.login                                                      as login,
            adm.jf_account_pkg$get_department(ca.depo_id, ca.facility_id) as department_name,
            ca.position                                                   as position,
            adm.jf_history_data_pkg$get_win_title(dmah.db_method)         as win_title,
            'OF_ROWS'                                                     as action_title,
            dmah.action_data                                              as change_date,
            ' '                                                           as table_schema,
            ' '                                                           as table_name,
            'OF_ROWS'                                                     as operation,
            ' '                                                           as row_id,
            hdw.win_title                                                 as db_method,
            case when dmah.params = '{}'
                then 'OF_ROWS'
            else 'Filter' end                                             as db_action,
            null                                                          as "ROW$POLICY"
        from adm.db_method_access_history dmah
            join jofl.account ja on dmah.id_jofl_account = ja.id_jofl_account
            join core.account ca on ja.id_account_local = ca.account_id
            join jofl.window hdw on dmah.db_method = hdw.db_method
        where
            dmah.db_method = l_db_method
            and (l_f_change_date_from is null or dmah.action_data >= l_f_change_date_from)
            and (l_f_change_date_to is null or dmah.action_data <= l_f_change_date_to);
    end if;
end;
$$;

CREATE OR REPLACE FUNCTION adm.jf_history_data_pkg$get_account_link(account_id BIGINT, full_name TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN '[' ||
         json_build_object(
             'CAPTION', full_name,
             'JUMP', json_build_object(
                 'METHOD', 'adm.account',
                 'ATTR', json_build_object('f_account_id', account_id) :: TEXT
             )
         )
         || ']';
END;
$$;
CREATE OR REPLACE FUNCTION adm.jf_history_data_pkg$get_win_title(p_db_method TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_result TEXT;
BEGIN
  SELECT w.win_title
  INTO l_result
  FROM jofl.window w
  WHERE w.db_method = p_db_method;
  RETURN coalesce(l_result, p_db_method);
END;
$$;
CREATE OR REPLACE FUNCTION adm.jf_history_data_pkg$get_action_title(p_db_method TEXT, p_call_action TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_result TEXT;
BEGIN
  SELECT wac.action_title
  INTO l_result
  FROM jofl.win_action_call wac
  WHERE wac.db_method = p_db_method AND wac.call_action = p_call_action;

  l_result := trim(BOTH '<>' FROM l_result);

  RETURN coalesce(l_result, p_call_action);
END;
$$;

create or replace function adm.jf_history_data_pkg$of_delete_checked(p_id_account NUMERIC, p_attr TEXT)
    returns text
language plpgsql
as $$
declare
    l_all_id_TEXT integer [] := jofl.jofl_pkg$extract_narray(p_attr, 'all_id_TEXT', true);
    l_F_INPLACE_K int;
begin
    l_F_INPLACE_K:=jofl.jofl_pkg$extract_number(p_attr, 'F_INPLACE_K', true);
    if (l_F_INPLACE_K = 0)
    then
        delete from adm.db_method_access_history
        where db_method_access_history_id = any (l_all_id_TEXT);
        return null;
    elseif (l_F_INPLACE_K = 1)
        then
            delete from adm.history_data_item
            where history_data_id = any (l_all_id_TEXT);
            delete from adm.history_data
            where history_data_id = any (l_all_id_TEXT);
            return null;
    end if;
end;
$$;