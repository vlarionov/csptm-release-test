CREATE OR REPLACE FUNCTION adm.account_roles_pkg$get_account_roles(
  p_id_jofl_account jofl.account.id_jofl_account%TYPE
)
  RETURNS TEXT []
LANGUAGE plpgsql
AS $$
DECLARE
  l_result TEXT [];
BEGIN
  SELECT coalesce(array_agg(DISTINCT g2r.id_role), ARRAY [] :: TEXT [])
  INTO l_result
  FROM jofl.account ja
    JOIN adm.account2group a2g ON ja.id_account_local = a2g.account_id
    , adm.group_data_pkg$get_roles(a2g.group_id) g2r
  WHERE ja.id_jofl_account = p_id_jofl_account AND ja.schema_name = core.const_pkg$core_project_name();

  RETURN l_result;
END;
$$;