CREATE OR REPLACE FUNCTION adm.jf_role_filter_call_action_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_f_db_method     TEXT;
BEGIN
  l_f_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'F_DB_METHOD', TRUE);

  OPEN p_rows FOR
  SELECT
    DISTINCT wac.call_action
  FROM jofl.win_action_call wac
  WHERE (l_f_db_method IS NULL OR l_f_db_method = '' OR wac.db_method = l_f_db_method)
  ORDER BY wac.call_action;
END;
$$;