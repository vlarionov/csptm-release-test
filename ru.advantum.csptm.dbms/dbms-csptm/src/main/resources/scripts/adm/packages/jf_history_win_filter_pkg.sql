CREATE OR REPLACE FUNCTION adm.jf_history_win_filter_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                                 p_attr       TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_available_windows text[];
BEGIN
  l_available_windows := adm.history_pkg$get_available_windows(p_id_account);
  OPEN p_rows
  FOR SELECT
        w.db_method,
        w.win_title
      FROM jofl.window w
      WHERE w.db_method = ANY (l_available_windows);
END;
$$;