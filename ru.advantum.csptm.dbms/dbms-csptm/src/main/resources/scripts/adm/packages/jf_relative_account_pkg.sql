CREATE OR REPLACE FUNCTION adm.jf_relative_account_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_id_role       jofl.ref_role.id_role%TYPE;
  l_data_realm_id adm.data_realm.data_realm_id%TYPE;
  l_sys_audit_id  adm.sys_audit.sys_audit_id%TYPE;
BEGIN
  l_id_role := jofl.jofl_pkg$extract_varchar(p_attr, 'id_role', TRUE);
  l_data_realm_id := jofl.jofl_pkg$extract_number(p_attr, 'data_realm_id', TRUE);
  l_sys_audit_id :=  jofl.jofl_pkg$extract_number(p_attr, 'sys_audit_id', TRUE);

  OPEN p_rows FOR
  SELECT
    DISTINCT
    l_id_role                                 id_role,
    l_data_realm_id                           data_realm_id,
    l_sys_audit_id                            sys_audit_id,
    adm.jf_account_pkg$get_link(a.account_id) full_name,
    a.login,
    adm.jf_account_pkg$get_last_login_date(
        a.account_id)                         last_login_date,
    (SELECT e.name_short
     FROM core.entity e
     WHERE e.entity_id =
           a.depo_id) AS                      depo_name,
    a.position,
    ad.name                                   domain_name
  FROM
    core.account a
    LEFT JOIN adm.ad_account aa ON a.account_id = aa.account_id
    LEFT JOIN adm.ad_domain ad ON aa.ad_domain_id = ad.ad_domain_id
    JOIN adm.account2group a2g ON a.account_id = a2g.account_id
  WHERE a2g.group_id IN (
    SELECT *
    FROM adm.group_data_pkg$get_containing_role(l_id_role)
    UNION
    SELECT *
    FROM adm.group_data_pkg$get_containing_data_realm(l_data_realm_id)
    UNION
    SELECT *
    FROM adm.group_data_pkg$get_containing_sys_audit(l_sys_audit_id)
  )
  ORDER BY full_name;
END;
$$;