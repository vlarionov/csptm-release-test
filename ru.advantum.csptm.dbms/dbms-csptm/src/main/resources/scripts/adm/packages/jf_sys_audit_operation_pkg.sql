CREATE OR REPLACE FUNCTION adm.jf_sys_audit_operation_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_operations INTEGER [] := ARRAY [
  adm.const_pkg$insert_op(),
  adm.const_pkg$update_op(),
  adm.const_pkg$delete_op()
  ];
BEGIN
  OPEN p_rows FOR
  SELECT
    operation,
    adm.jf_sys_audit_operation_pkg$get_operation_name(operation) operation_name
  FROM unnest(l_operations) operation;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_sys_audit_operation_pkg$get_operation_name(p_operation INTEGER)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
BEGIN
  IF p_operation = adm.const_pkg$insert_op()
  THEN RETURN 'INSERT';
  ELSEIF p_operation = adm.const_pkg$update_op()
    THEN RETURN 'UPDATE';
  ELSEIF p_operation = adm.const_pkg$delete_op()
    THEN RETURN 'DELETE';
  END IF;

  RETURN NULL;
END;
$$;