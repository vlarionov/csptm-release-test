CREATE OR REPLACE FUNCTION adm.jf_ad_domain_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
BEGIN
  OPEN p_rows FOR
  SELECT
    ad.ad_domain_id,
    ad.name
  FROM adm.ad_domain ad
  ORDER BY ad.ad_domain_id;
END;
$$;