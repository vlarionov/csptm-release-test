CREATE OR REPLACE FUNCTION adm.jf_group_pkg$attr_to_rowtype(p_attr TEXT)
  RETURNS adm.GROUP
LANGUAGE plpgsql
AS $$
DECLARE
  l_r adm.group%ROWTYPE;
BEGIN
  l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', FALSE);
  l_r.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', TRUE);
  RETURN l_r;
END;
$$;


CREATE OR REPLACE FUNCTION adm.jf_group_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group    adm.group%ROWTYPE;
  l_ad_group adm.ad_group%ROWTYPE;
BEGIN
  l_group := adm.jf_group_pkg$attr_to_rowtype(p_attr);
  l_group.group_id = nextval('adm.group_id_seq');
  l_group.is_personal:= FALSE;
  INSERT INTO adm.group SELECT l_group.*;

  IF jofl.jofl_pkg$extract_boolean(p_attr, 'is_ad_sync', FALSE)
  THEN
    l_ad_group.ad_name := jofl.jofl_pkg$extract_varchar(p_attr, 'ad_name', TRUE);
    l_ad_group.ad_domain_id := jofl.jofl_pkg$extract_number(p_attr, 'ad_domain_id', TRUE);
    IF (l_ad_group.ad_name IS NOT NULL AND l_ad_group.ad_domain_id IS NOT NULL)
    THEN
      l_ad_group.group_id = l_group.group_id;
      INSERT INTO adm.ad_group SELECT l_ad_group.*;
    ELSE
      RAISE EXCEPTION 'Необходимо указать атрибуты группы AD';
    END IF;
  END IF;

  INSERT INTO adm.group_owner (group_id, account_id) VALUES (
    l_group.group_id,
    p_id_account
  );

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_group_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_f_group_id         adm.group.group_id%TYPE;
  l_f_incident_type_id    NUMERIC [];
  l_f_sys_audit_id     NUMERIC [];
  l_f_id_report        NUMERIC [];
  l_f_data_realm_id    NUMERIC [];
  l_f_id_role          TEXT [];
  l_f_owner_account_id NUMERIC [];
  l_f_admin_account_id NUMERIC [];
BEGIN
  l_f_group_id := jofl.jofl_pkg$extract_number(p_attr, 'f_group_id', TRUE);
  l_f_incident_type_id := jofl.jofl_pkg$extract_narray(p_attr, 'f_incident_type_id', TRUE);
  l_f_sys_audit_id := jofl.jofl_pkg$extract_narray(p_attr, 'f_sys_audit_id', TRUE);
  l_f_id_report := jofl.jofl_pkg$extract_narray(p_attr, 'f_id_report', TRUE);
  l_f_data_realm_id :=jofl.jofl_pkg$extract_narray(p_attr, 'f_data_realm_id', TRUE);
  l_f_id_role := jofl.jofl_pkg$extract_tarray(p_attr, 'f_id_role', TRUE);
  l_f_owner_account_id := jofl.jofl_pkg$extract_narray(p_attr, 'f_owner_account_id', TRUE);
  l_f_admin_account_id := jofl.jofl_pkg$extract_narray(p_attr, 'f_admin_account_id', TRUE);


  OPEN p_rows FOR
  SELECT
    g.group_id,
    g.name,
    g.comment,
    (SELECT count(1)
     FROM adm.account2group a2g
       LEFT JOIN core.account a ON a2g.account_id = a.account_id
     WHERE a2g.group_id = g.group_id
    )                                                     members_number,
    adg.group_id IS NOT NULL                              is_ad_sync,
    ad.ad_domain_id,
    ad.name                                               ad_domain_name,
    adg.ad_name,
    adg.guid IS NOT NULL                                  is_ad_synced,
    adm.jf_group_pkg$get_policy(p_id_account, g.group_id) "ROW$POLICY",
    TRUE                                                  is_group_window
  FROM adm.group g
    LEFT JOIN adm.ad_group adg ON g.group_id = adg.group_id
    LEFT JOIN adm.ad_domain ad ON adg.ad_domain_id = ad.ad_domain_id
  WHERE NOT g.is_personal
        AND (l_f_group_id IS NULL OR g.group_id = l_f_group_id)
        AND (array_length(l_f_incident_type_id, 1) IS NULL
             OR array_length(l_f_incident_type_id, 1) < 1
             OR exists(SELECT 1
                       FROM adm.group_data_pkg$get_incident_types(g.group_id) g2et
                       WHERE g2et.incident_type_id = ANY (l_f_incident_type_id)))
        AND (array_length(l_f_sys_audit_id, 1) IS NULL
             OR array_length(l_f_sys_audit_id, 1) < 1
             OR exists(SELECT 1
                       FROM adm.group_data_pkg$get_sys_audits(g.group_id) g2sa
                       WHERE g2sa.sys_audit_id = ANY (l_f_sys_audit_id)))
        AND (array_length(l_f_id_report, 1) IS NULL
             OR array_length(l_f_id_report, 1) < 1
             OR exists(SELECT 1
                       FROM adm.group_data_pkg$get_reports(g.group_id) g2r
                       WHERE g2r.id_report = ANY (l_f_id_report)))
        AND (array_length(l_f_data_realm_id, 1) IS NULL
             OR array_length(l_f_data_realm_id, 1) < 1
             OR exists(SELECT 1
                       FROM adm.group_data_pkg$get_data_realms(g.group_id) g2dr
                       WHERE g2dr.data_realm_id = ANY (l_f_data_realm_id)))
        AND (array_length(l_f_id_role, 1) IS NULL
             OR array_length(l_f_id_role, 1) < 1
             OR exists(SELECT 1
                       FROM adm.group_data_pkg$get_roles(g.group_id) g2r
                       WHERE g2r.id_role = ANY (l_f_id_role)))
        AND (array_length(l_f_owner_account_id, 1) IS NULL
             OR array_length(l_f_owner_account_id, 1) < 1
             OR exists(SELECT 1
                       FROM adm.group_owner go
                       WHERE go.group_id = g.group_id AND go.account_id = ANY (l_f_owner_account_id)))
        AND (array_length(l_f_admin_account_id, 1) IS NULL
             OR array_length(l_f_admin_account_id, 1) < 1
             OR exists(SELECT 1
                       FROM adm.group_admin ga
                       WHERE ga.group_id = g.group_id AND ga.account_id = ANY (l_f_admin_account_id)))
  ORDER BY g.name;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_group_pkg$get_policy(p_id_account NUMERIC, p_group_id adm.group.group_id%TYPE)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_ad_fields         TEXT [] := ARRAY ['is_ad_sync', 'ad_domain_name', 'ad_name'];
  l_available_actions TEXT [];
  l_result            TEXT;
BEGIN
  l_result := adm.jofl_policy_pkg$build('RO', l_ad_fields);

  IF adm.group_pkg$is_owner(p_id_account, p_group_id)
  THEN l_available_actions := ARRAY ['OF_UPDATE', 'OF_DELETE'];
  ELSEIF adm.group_pkg$is_admin(p_id_account, p_group_id)
    THEN l_available_actions := ARRAY ['OF_UPDATE'];
  END IF;

  IF adm.group_pkg$is_ad_sync(p_group_id)
    THEN l_available_actions := array_append(l_available_actions, 'OF_AD_SYNC_GROUP');
  END IF;

  RETURN adm.jofl_policy_pkg$append(l_result, adm.jofl_policy_pkg$build('A', l_available_actions));
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_group_pkg$of_update(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group adm.group%ROWTYPE;
BEGIN
  l_group := adm.jf_group_pkg$attr_to_rowtype(p_attr);

  l_group.group_id = jofl.jofl_pkg$extract_varchar(p_attr, 'group_id', FALSE);
  --   PERFORM adm.jf_group_pkg$check_not_ad(l_group.group_id);

  PERFORM adm.group_data_pkg$check_is_not_super_group(l_group.group_id);

  UPDATE adm.group
  SET
    name    = l_group.name,
    comment = l_group.comment
  WHERE group_id = l_group.group_id;
  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_group_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id INTEGER;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);

  PERFORM adm.group_data_pkg$check_is_not_super_group(l_group_id);
  PERFORM adm.group_pkg$check_is_owner(p_id_account, l_group_id);

  DELETE
  FROM adm.ad_group
  WHERE group_id = l_group_id;

  DELETE
  FROM adm.group_owner go
  WHERE go.account_id = p_id_account AND go.group_id = l_group_id;

  DELETE
  FROM adm.group
  WHERE group_id = l_group_id;
  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_group_pkg$check_not_ad(p_group_id adm.group.group_id%TYPE)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  IF adm.jf_group_pkg$is_ad(p_group_id)
  THEN
    RAISE EXCEPTION 'Нельзя редактировать группу Active Directory';
  END IF;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_group_pkg$is_ad(p_group_id adm.group.group_id%TYPE)
  RETURNS BOOLEAN
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN exists(SELECT 1
                FROM adm.ad_group adg
                WHERE adg.group_id = p_group_id);
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_group_pkg$of_ad_sync_group(p_account_id NUMERIC, p_attr text)
    RETURNS void
LANGUAGE plpgsql
AS $$
declare
    g_id integer:=jofl.jofl_pkg$extract_number(p_attr, 'group_id', true);
BEGIN
    perform paa.paa_api_pkg$basic_publish(cmnd.jf_cmnd_ref4unit_pkg$cn_command_rmq_broker(),
                                          '',
                                          'ad-sync',
                                          json_build_object('group_id', g_id) :: text,
                                          '');
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_group_pkg$of_ad_sync_all(p_account_id NUMERIC, p_attr text)
    RETURNS void
LANGUAGE plpgsql
AS $$

BEGIN
    perform paa.paa_api_pkg$basic_publish(cmnd.jf_cmnd_ref4unit_pkg$cn_command_rmq_broker(),
                                          '',
                                          'ad-sync',
                                          '{}',
                                          '');
END;
$$;