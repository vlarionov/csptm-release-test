CREATE OR REPLACE FUNCTION adm.account_pkg$create(
  p_last_name           core.account.last_name%TYPE,
  p_first_name          core.account.first_name%TYPE,
  p_middle_name         core.account.middle_name%TYPE,
  p_login               core.account.login%TYPE,
  p_password            core.account.password%TYPE,
  p_dt_insert           core.account.dt_insert%TYPE,
  p_dt_update           core.account.dt_update%TYPE,
  p_dt_lock             core.account.dt_lock%TYPE,
  p_is_locked           core.account.is_locked%TYPE,
  p_is_deleted          core.account.is_deleted%TYPE,
  p_email               core.account.email%TYPE,
  p_cell_phone          core.account.cell_phone%TYPE,
  p_work_phone          core.account.work_phone%TYPE,
  p_is_email_subscriber core.account.is_email_subscriber%TYPE,
  p_is_phone_subscriber core.account.is_phone_subscriber%TYPE,
  p_facility_id         core.account.facility_id%TYPE,
  p_position            core.account.position%TYPE,
  p_comment             core.account.comment%TYPE,
  p_depo_id             core.account.depo_id%TYPE,
  p_operator_phone      core.account.operator_phone%TYPE
)
  RETURNS core.account.account_id%TYPE
LANGUAGE plpgsql
AS $$
DECLARE
  l_account_id core.account.account_id%TYPE;
BEGIN
  l_account_id := nextval('core.account_account_id_seq');

  INSERT INTO core.account (account_id, last_name, first_name, middle_name, login, password,
                            dt_insert, dt_update, dt_lock, is_locked, is_deleted, email,
                            cell_phone, work_phone, is_email_subscriber, is_phone_subscriber,
                            facility_id, position, comment, depo_id, operator_phone
  ) VALUES (
    l_account_id,
    p_last_name,
    p_first_name,
    p_middle_name,
    p_login,
    p_password,
    p_dt_insert,
    p_dt_update,
    p_dt_lock,
    p_is_locked,
    p_is_deleted,
    p_email,
    p_cell_phone,
    p_work_phone,
    p_is_email_subscriber,
    p_is_phone_subscriber,
    p_facility_id,
    p_position,
    p_comment,
    p_depo_id,
    p_operator_phone
  );

  PERFORM adm.account_pkg$create_personal_group(l_account_id);

  PERFORM jofl.jofl_pkg$register_account(
      l_account_id,
      core.const_pkg$core_project_name()
  );

  RETURN l_account_id;
END;
$$;


CREATE OR REPLACE FUNCTION adm.account_pkg$delete(
  p_account_id core.account.account_id%TYPE
)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  PERFORM jofl.jofl_pkg$unregister_account(
      p_account_id,
      core.const_pkg$core_project_name()
  );

  PERFORM adm.account_pkg$delete_personal_group(p_account_id);

  DELETE FROM core.account
  WHERE account_id = p_account_id;
END;
$$;


CREATE OR REPLACE FUNCTION adm.account_pkg$create_personal_group(p_account_id NUMERIC)
  RETURNS adm.group.group_id%TYPE
LANGUAGE plpgsql
AS $$
DECLARE
  l_result adm.group.group_id%TYPE;
BEGIN
  INSERT INTO adm.group (group_id, is_personal)
  VALUES (nextval('adm.group_id_seq'), TRUE)
  RETURNING group_id
    INTO l_result;

  INSERT INTO adm.account2group (account_id, group_id) VALUES (p_account_id, l_result);

  RETURN l_result;
END;
$$;

CREATE OR REPLACE FUNCTION adm.account_pkg$delete_personal_group(p_account_id NUMERIC)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id adm.group.group_id%TYPE;
BEGIN
  SELECT a2g.group_id
  INTO l_group_id
  FROM adm.account2group a2g
    JOIN adm.group g ON a2g.group_id = g.group_id
  WHERE a2g.account_id = p_account_id AND g.is_personal;

  DELETE FROM adm.account2group a2g
  WHERE a2g.account_id = p_account_id AND a2g.group_id = l_group_id;

  DELETE FROM adm.group
  WHERE group_id = l_group_id;
END;
$$;

CREATE OR REPLACE FUNCTION adm.account_pkg$has_personal_rights(p_account_id core.account.account_id%TYPE)
  RETURNS BOOLEAN
LANGUAGE plpgsql
AS $$
DECLARE
  l_personal_group_id adm.group.group_id%TYPE;
BEGIN
  SELECT a2g.group_id
  INTO l_personal_group_id
  FROM adm.account2group a2g
    JOIN adm.group g ON a2g.group_id = g.group_id
  WHERE a2g.account_id = p_account_id AND g.is_personal;

  RETURN exists(
      SELECT 1
      FROM adm.group_data_pkg$get_data_realms(l_personal_group_id)
      UNION
      SELECT 1
      FROM adm.group_data_pkg$get_roles(l_personal_group_id)
      UNION
      SELECT 1
      FROM adm.group_data_pkg$get_sys_audits(l_personal_group_id)
      UNION
      SELECT 1
      FROM adm.group_data_pkg$get_incident_types(l_personal_group_id)
      UNION
      SELECT 1
      FROM adm.group_data_pkg$get_reports(l_personal_group_id)
  );
END;
$$;