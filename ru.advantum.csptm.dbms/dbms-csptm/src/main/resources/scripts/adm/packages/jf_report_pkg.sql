CREATE OR REPLACE FUNCTION adm.jf_report_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_is_group_window BOOLEAN;
  l_administrable_reports_ids NUMERIC[];
BEGIN
  l_is_group_window := COALESCE(jofl.jofl_pkg$extract_boolean(p_attr, 'is_group_window', TRUE), 0);
  l_administrable_reports_ids := adm.group_pkg$get_administrable_reports_ids(p_id_account);

  OPEN p_rows FOR
  SELECT
    r.id_report,
    r.s_name
  FROM jrep.report r
    WHERE ((NOT l_is_group_window) OR r.id_report = ANY (l_administrable_reports_ids))
  ORDER BY r.s_name;
END;
$$;