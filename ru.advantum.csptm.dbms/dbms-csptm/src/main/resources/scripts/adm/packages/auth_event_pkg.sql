CREATE OR REPLACE FUNCTION adm.auth_event_pkg$save_login_event(
  p_event_date      TIMESTAMP,
  p_id_jofl_account BIGINT,
  p_entered_login   TEXT,
  p_result_code     INTEGER,
  p_ip_address      TEXT,
  p_is_sso_login    BOOLEAN,
  p_j_session_id    TEXT
)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE
  l_login_event_id                      INTEGER;
BEGIN
  l_login_event_id:= nextval('adm.login_event_id_seq');

  IF (p_result_code != 0 AND asd.get_constant('IS_INCORRECT_LOGIN_LOGGING_ENABLED') != 1)
  THEN
    RETURN;
  END IF;

  INSERT INTO adm.login_event (
    login_event_id,
    event_date,
    id_jofl_account,
    entered_login,
    result_code,
    ip_address,
    is_sso_login
  )
  VALUES (
    l_login_event_id,
    p_event_date,
    p_id_jofl_account,
    p_entered_login,
    p_result_code,
    p_ip_address,
    p_is_sso_login
  );

  IF p_result_code = 0
  THEN INSERT INTO adm.session (session_id, login_event_id, j_session_id, last_request_date, close_date, close_code)
  VALUES (
    nextval('adm.session_id_seq'),
    l_login_event_id,
    p_j_session_id,
    p_event_date,
    NULL,
    NULL
  );
  END IF;
END;
$$;

CREATE OR REPLACE FUNCTION adm.auth_event_pkg$update_last_requests(
  p_j_session_ids      TEXT[]
)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE now TIMESTAMP:= now();
BEGIN
  UPDATE adm.session
  SET last_request_date = now
  WHERE j_session_id = ANY (p_j_session_ids)
        AND close_date IS NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.auth_event_pkg$close_sessions(p_close_code INTEGER)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  UPDATE adm.session
  SET close_date = last_request_date,
    close_code   = p_close_code
  WHERE close_date IS NULL;
END;
$$;


CREATE OR REPLACE FUNCTION adm.auth_event_pkg$close_session(
  p_jsession_id TEXT,
  p_close_date  TIMESTAMP,
  p_close_code  INTEGER
)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  UPDATE adm.session
  SET close_date = p_close_date,
    close_code   = p_close_code
  WHERE close_date IS NULL AND j_session_id = p_jsession_id;
END;
$$;