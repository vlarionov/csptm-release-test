create or replace function adm."jf_account2report_task_pkg$attr_to_rowtype"(p_attr text)
  returns adm.account2report_task as
$BODY$
declare
  l_r adm.account2report_task%rowtype;
begin
  l_r.id := jofl.jofl_pkg$extract_number(p_attr, 'id', true);
  l_r.account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', true);

  return l_r;
end;
$BODY$
language plpgsql
volatile
cost 100;
--------------------------------------------------------------------------------------------  
create or replace function adm."jf_account2report_task_pkg$of_delete"(
  p_id_account numeric,
  p_attr       text)
  returns text as
$BODY$
declare
  l_r adm.account2report_task%rowtype;
begin
  l_r := adm.jf_account2report_task_pkg$attr_to_rowtype(p_attr);

  delete from adm.account2report_task
  where id = l_r.id and
        account_id = l_r.account_id;

  return null;
end;
$BODY$
language plpgsql
volatile
cost 100;
-------------------------------------------------------------------------------------------- 
create or replace function adm."jf_account2report_task_pkg$of_insert"(
  p_id_account numeric,
  p_attr       text)
  returns text as
$BODY$
declare
  l_r adm.account2report_task%rowtype;
begin
  l_r := adm.jf_account2report_task_pkg$attr_to_rowtype(p_attr);

  insert into adm.account2report_task select l_r.*;

  return null;
end;
$BODY$
language plpgsql
volatile
cost 100;
-------------------------------------------------------------------------------------------- 
create or replace function adm."jf_account2report_task_pkg$of_rows"(
  in  p_id_account numeric,
  out p_rows       refcursor,
  in  p_attr       text)
  returns refcursor as
$BODY$
declare
  ln_id adm.report_task.id%type := jofl.jofl_pkg$extract_number(p_attr, 'id', true);
begin
  open p_rows for
  select
    rt.id         as id,
    rt.account_id as account_id,
    ac.login      as login
  from adm.account2report_task rt,
    core.account ac
  where rt.account_id = ac.account_id
        and id = ln_id;
end;
$BODY$
language plpgsql
volatile
cost 100;
--------------------------------------------------------------------------------------------   