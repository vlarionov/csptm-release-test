CREATE OR REPLACE FUNCTION adm."jf_report2report_task_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$
declare
  ln_id adm.report_task.id%type := jofl.jofl_pkg$extract_number(p_attr, 'id', true);
begin
  open p_rows for
  select
    rt.id_rrt                                   id_rrt,
    rt.ID_REPORT_TASK                           id_report_task,
    (select s_name
     from adm.report_filter
     where report_filter.id = id_report_filter) filter_name,
    rt.id_report_filter                         id_report_filter,
    (select filter_str
     from adm.report_filter
     where report_filter.id = id_report_filter) filter_str,
    'P{RO},D{S_NAME}' as                        "ROW$POLICY"
  from adm.report2report_task rt
  where rt.id_report_task = ln_id;
end;

  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
--------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION adm."jf_report2report_task_pkg$attr_to_rowtype"(p_attr TEXT)
  RETURNS adm.report2report_task AS
  $BODY$
  DECLARE
    l_r adm.report2report_task%ROWTYPE;
  BEGIN
    l_r.id_rrt := jofl.jofl_pkg$extract_number(p_attr, 'id', TRUE);
    l_r.id_report_task := jofl.jofl_pkg$extract_number(p_attr, 'id_report_task', TRUE);
    l_r.id_report_filter := jofl.jofl_pkg$extract_number(p_attr, 'id_report_filter', TRUE);

    RETURN l_r;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
--------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION adm."jf_report2report_task_pkg$of_delete"(
  p_id_account NUMERIC,
  p_attr       TEXT)
  RETURNS TEXT AS
  $BODY$
  DECLARE
    l_r adm.report2report_task%ROWTYPE;
  BEGIN
    l_r := adm.jf_report2report_task_pkg$attr_to_rowtype(p_attr);

    DELETE FROM adm.report2report_task
    WHERE ID_RRT = l_r.ID_RRT ;

    RETURN NULL;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
--------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION adm."jf_report2report_task_pkg$of_insert"(
  p_id_account NUMERIC,
  p_attr       TEXT)
  RETURNS TEXT AS
  $BODY$
  DECLARE
    l_r adm.report2report_task%ROWTYPE;
  BEGIN
    l_r := adm.jf_report2report_task_pkg$attr_to_rowtype(p_attr);
    l_r.id_rrt := nextval('core.account_account_id_seq');
    l_r.id_report_task := jofl.jofl_pkg$extract_number(p_attr, 'id_rrt', TRUE);
    INSERT INTO adm.report2report_task (id_rrt, id_report_task, id_report_filter, subject)  values (
      nextval('adm.report2report_task_id_seq') ,
      l_r.id_report_task ,
    l_r.id_report_filter ,
    l_r.subject);

    RETURN NULL;
  END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
--------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION adm."jf_report2report_task_pkg$of_update" (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
  DECLARE
    l_r adm.report2report_task%ROWTYPE;
  BEGIN
  	l_r := adm.jf_report2report_task_pkg$attr_to_rowtype(p_attr);
  
    update adm.report2report_task set id_report_filter = l_r.id_report_filter where id_rrt = l_r.id_rrt;

    RETURN NULL;
  END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
  