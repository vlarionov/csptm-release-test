CREATE OR REPLACE FUNCTION adm.jf_group_admin_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id adm.group.group_id%TYPE;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);

  OPEN p_rows
  FOR
  SELECT
    g.group_id,
    a.account_id,
    aa.ad_login,
    adm.jf_account_pkg$get_full_name(a.account_id)                     full_name,
    (SELECT e.name_short
     FROM core.entity e
     WHERE e.entity_id =
           a.depo_id)                                                  depo_name,
    a.position,
    a.login,
    adm.group_pkg$get_of_delete_owner_policy(p_id_account, l_group_id) "ROW$POLICY"
  FROM core.account a
    JOIN adm.group_admin ga ON a.account_id = ga.account_id
    left JOIN adm.ad_account aa on a.account_id = aa.account_id
    JOIN adm.group g ON ga.group_id = g.group_id
  WHERE g.group_id = l_group_id
  ORDER BY a.account_id DESC;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_group_admin_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id   adm.group.group_id%TYPE;
  l_account_id core.account.account_id%TYPE;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);
  l_account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', FALSE);

  PERFORM adm.group_pkg$check_is_owner(p_id_account, l_group_id);

  INSERT INTO adm.group_admin (account_id, group_id) VALUES (
    l_account_id,
    l_group_id
  );

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_group_admin_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id   adm.group.group_id%TYPE;
  l_account_id core.account.account_id%TYPE;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);
  l_account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', FALSE);

  PERFORM adm.group_pkg$check_is_owner(p_id_account, l_group_id);

  DELETE FROM adm.group_admin
  WHERE group_id = l_group_id AND account_id = l_account_id;

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_group_admin_pkg$of_add_all(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id   adm.group.group_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);
BEGIN
  PERFORM adm.group_pkg$check_is_owner(p_id_account, l_group_id);
  INSERT INTO adm.group_admin (account_id, group_id)
    select a2g.account_id, l_group_id from adm.account2group a2g where a2g.group_id = l_group_id
    on conflict do nothing;
  return null;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_group_admin_pkg$of_users_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id   adm.group.group_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);
  l_account_id bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_account_id', true);
BEGIN
  PERFORM adm.group_pkg$check_is_owner(p_id_account, l_group_id);
  INSERT INTO adm.group_admin (account_id, group_id)
    select acc_id, l_group_id from unnest(l_account_id) acc_id
    on conflict do nothing;
  return null;
END;
$$;
