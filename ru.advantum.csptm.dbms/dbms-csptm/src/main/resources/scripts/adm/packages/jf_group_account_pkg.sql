CREATE OR REPLACE FUNCTION adm.jf_group_account_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id INTEGER;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);

  OPEN p_rows FOR
  SELECT
    g.group_id,
    a.account_id,
    adm.jf_account_pkg$get_full_name(a.account_id)                full_name_title,
    adm.jf_account_pkg$get_link(a.account_id)                     full_name,
    a.login,
    adm.jf_account_pkg$get_last_login_date(a.account_id),
    (SELECT e.name_short
     FROM core.entity e
     WHERE e.entity_id =
           a.depo_id) AS                                               depo_name,
    a.position,
    ad.name                                                            domain_name,
    aa.ad_login,
    adm.group_pkg$get_of_delete_admin_policy(p_id_account, l_group_id) "ROW$POLICY"
  FROM adm.group g
    JOIN adm.account2group a2g ON g.group_id = a2g.group_id
    JOIN core.account a ON a2g.account_id = a.account_id
    LEFT JOIN adm.ad_account aa ON a.account_id = aa.account_id
    LEFT JOIN adm.ad_domain ad ON aa.ad_domain_id = ad.ad_domain_id
  WHERE g.group_id = l_group_id
  ORDER BY a.account_id DESC;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_group_account_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id   INTEGER;
  l_account_id BIGINT;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);
  l_account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', FALSE);

  PERFORM adm.jf_group_pkg$check_not_ad(l_group_id);
  PERFORM adm.group_pkg$check_is_owner_or_admin(p_id_account, l_group_id);

  INSERT INTO adm.account2group (group_id, account_id) VALUES (
    l_group_id,
    l_account_id
  );

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_group_account_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id   INTEGER;
  l_account_id BIGINT;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);
  l_account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', FALSE);

  PERFORM adm.jf_group_pkg$check_not_ad(l_group_id);
  PERFORM adm.group_pkg$check_is_owner_or_admin(p_id_account, l_group_id);

  DELETE FROM adm.account2group
  WHERE group_id = l_group_id AND account_id = l_account_id;

  RETURN NULL;
END;
$$;