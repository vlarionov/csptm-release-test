CREATE OR REPLACE FUNCTION adm.jf_data_realm_pkg$attr_to_rowtype(p_attr TEXT)
  RETURNS adm.DATA_REALM
LANGUAGE plpgsql
AS $$
DECLARE
  l_r adm.data_realm%ROWTYPE;
BEGIN
  l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', FALSE);
  l_r.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', TRUE);
  RETURN l_r;
END;
$$;


CREATE OR REPLACE FUNCTION adm.jf_data_realm_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_data_realm adm.data_realm%ROWTYPE;
BEGIN
  l_data_realm := adm.jf_data_realm_pkg$attr_to_rowtype(p_attr);
  l_data_realm.data_realm_id = nextval('adm.data_realm_id_seq');
  INSERT INTO adm.data_realm SELECT l_data_realm.*;

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_data_realm_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_f_depo_id                    core.depo.depo_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', true);
  l_f_route_id                   rts.route.route_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', true);
  l_f_tr_type_id                 core.tr_type.tr_type_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_tr_type_id', true);

  l_is_group_window              BOOLEAN;
  l_dministrable_data_realms_ids INTEGER [];
BEGIN
  l_is_group_window := COALESCE(jofl.jofl_pkg$extract_boolean(p_attr, 'is_group_window', TRUE), 0);
  l_dministrable_data_realms_ids := adm.group_pkg$get_administrable_data_realms_ids(p_id_account);

  OPEN p_rows FOR
  SELECT
    dr.data_realm_id,
    dr.name,
    dr.comment
  FROM adm.data_realm dr
  WHERE (l_f_depo_id IS NULL OR dr.data_realm_id IN (SELECT * FROM adm.data_realm_pkg$get_containing_depo(l_f_depo_id)))
        AND (l_f_tr_type_id IS NULL OR dr.data_realm_id IN (SELECT * FROM adm.data_realm_pkg$get_containing_tr_type(l_f_tr_type_id)))
        AND (l_f_route_id IS NULL OR dr.data_realm_id IN (SELECT * FROM adm.data_realm_pkg$get_containing_route(l_f_route_id)))
        AND ((NOT l_is_group_window) OR dr.data_realm_id = ANY (l_dministrable_data_realms_ids))
  ORDER BY dr.name;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_data_realm_pkg$of_update(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_data_realm adm.data_realm%ROWTYPE;
BEGIN
  l_data_realm :=adm.jf_data_realm_pkg$attr_to_rowtype(p_attr);
  l_data_realm.data_realm_id := jofl.jofl_pkg$extract_number(p_attr, 'data_realm_id', FALSE);

  PERFORM adm.data_realm_pkg$check_is_not_super_data_realm(l_data_realm.data_realm_id);

  UPDATE adm.data_realm
  SET
    name    = l_data_realm.name,
    comment = l_data_realm.comment
  WHERE data_realm_id = l_data_realm.data_realm_id;

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_data_realm_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_data_realm_id INTEGER;
BEGIN
  l_data_realm_id :=jofl.jofl_pkg$extract_number(p_attr, 'data_realm_id', FALSE);

  PERFORM adm.data_realm_pkg$check_is_not_super_data_realm(l_data_realm_id);

  DELETE FROM adm.data_realm
  WHERE data_realm_id = l_data_realm_id;

  RETURN NULL;
END;
$$;