CREATE OR REPLACE FUNCTION adm.ad_pkg$find_ad_instances()
  RETURNS TABLE(
    adInstanceId  INTEGER,
    ldapUrl       TEXT,
    ldapUserDn    TEXT,
    ldapPassword  TEXT,
    krbRealm      TEXT,
    krbKdc        TEXT,
    krbPrincipal  TEXT,
    krbConfPath   TEXT,
    krbKeytabPath TEXT,
    krbDebug      BOOLEAN
  )
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT
    ad_instance_id,
    ldap_url,
    ldap_user_dn,
    ldap_password,
    krb_realm,
    krb_kdc,
    krb_principal,
    krb_conf_path,
    krb_keytab_path,
    krb_debug
  FROM adm.ad_instance adi;
END;
$$;

CREATE OR REPLACE FUNCTION adm.ad_pkg$find_groups()
  RETURNS TABLE(groupId INTEGER, adInstanceId INTEGER, adDomain TEXT, adName TEXT, adGUID TEXT)
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT
    ag.group_id,
    ad.ad_instance_id,
    ad.name AS ad_domain,
    ag.ad_name,
    ag.guid AS ad_guid
  FROM adm.ad_group ag
    JOIN adm.ad_domain ad ON ad.ad_domain_id = ag.ad_domain_id;
END;
$$;

CREATE OR REPLACE FUNCTION adm.ad_pkg$find_account_by_guid(p_guid TEXT)
  RETURNS TABLE(
    accountId     BIGINT,
    joflAccountId BIGINT,
    adInstanceId  INTEGER,
    adDomain      TEXT,
    adName        TEXT,
    adGuid        TEXT,
    firstName     TEXT,
    lastName      TEXT,
    dtInsert      TIMESTAMP,
    dtUpdate      TIMESTAMP,
    dtLock        TIMESTAMP,
    isLocked      BOOLEAN,
    email         TEXT,
    cellPhone     TEXT,
    workPhone     TEXT
  )
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT
    a.account_id,
    ja.id_jofl_account :: BIGINT,
    ad.ad_instance_id,
    ad.name,
    aa.ad_name,
    aa.guid AS ad_guid,
    a.first_name,
    a.last_name,
    a.dt_insert,
    a.dt_update,
    a.dt_lock,
    a.is_locked,
    a.email,
    a.cell_phone,
    a.work_phone
  FROM adm.ad_account aa
    JOIN core.account a ON a.account_id = aa.account_id
    JOIN adm.ad_domain ad ON ad.ad_domain_id = aa.ad_domain_id
    JOIN jofl.account ja ON ja.id_account_local = a.account_id AND ja.schema_name = core.const_pkg$core_project_name()
  WHERE aa.guid = p_guid AND NOT a.is_deleted;
END;
$$;


CREATE OR REPLACE FUNCTION adm.ad_pkg$update_group(
  p_ad_instance_id INTEGER,
  p_ad_domain      TEXT,
  p_ad_name        TEXT,
  p_ad_guid        TEXT
)
  RETURNS BOOLEAN
LANGUAGE plpgsql
AS $$
DECLARE
  group_rec RECORD;
BEGIN
  FOR group_rec IN SELECT
                     ag.ad_name AS group_name,
                     ag.group_id
                   FROM adm.ad_group ag
                   WHERE ag.guid = p_ad_guid
  LOOP
    UPDATE adm.ad_group
    SET ad_name = p_ad_name
    WHERE group_id = group_rec.group_id;
    RETURN TRUE;
  END LOOP;

  FOR group_rec IN SELECT
                     ag.guid AS group_guid,
                     ag.group_id
                   FROM adm.ad_group ag
                     JOIN adm.ad_domain ad ON ad.ad_domain_id = ag.ad_domain_id
                   WHERE ad.ad_instance_id = p_ad_instance_id
                         AND upper(ad.name) = upper(p_ad_domain)
                         AND ag.ad_name = p_ad_name
  LOOP
    UPDATE adm.ad_group
    SET guid = p_ad_guid
    WHERE group_id = group_rec.group_id;
    RETURN TRUE;
  END LOOP;

  RETURN FALSE;
END;
$$;

CREATE OR REPLACE FUNCTION adm.ad_pkg$update_account(
  p_ad_instance_id INTEGER,
  p_ad_domain      TEXT,
  p_ad_name        TEXT,
  p_ad_guid        TEXT,
  p_first_name     TEXT,
  p_last_name      TEXT,
  p_dt_lock        TIMESTAMP,
  p_is_locked      BOOLEAN,
  p_email          TEXT,
  p_cell_phone     TEXT,
  p_work_phone     TEXT,
  p_ad_login       TEXT
)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE
  new_account_id BIGINT;
  account_rec    RECORD;
BEGIN
  FOR account_rec IN SELECT
                       a.account_id,
                       aa.ad_name,
                       a.first_name,
                       a.last_name,
                       a.dt_insert,
                       a.dt_update,
                       a.dt_lock,
                       a.is_locked,
                       a.email,
                       a.cell_phone,
                       a.work_phone
                     FROM core.account a
                       JOIN adm.ad_account aa ON aa.account_id = a.account_id AND aa.guid = p_ad_guid
                       JOIN adm.ad_domain ad ON ad.ad_domain_id = aa.ad_domain_id
  LOOP
    UPDATE core.account
    SET
      first_name = p_first_name,
      last_name  = p_last_name,
      dt_update  = localtimestamp,
      dt_lock    = p_dt_lock,
      is_locked  = p_is_locked,
      email      = p_email,
      cell_phone = p_cell_phone,
      work_phone = p_work_phone
    WHERE account_id = account_rec.account_id;

    UPDATE adm.ad_account
    SET ad_name = p_ad_name,
      ad_login  = p_ad_login
    WHERE account_id = account_rec.account_id;

    RETURN;
  END LOOP;

  new_account_id := adm.account_pkg$create(
      p_last_name,
      p_first_name,
      NULL,
      '',
      '',
      localtimestamp,
      localtimestamp,
      p_dt_lock,
      p_is_locked,
      FALSE,
      p_email,
      p_cell_phone,
      p_work_phone,
      FALSE,
      FALSE,
      NULL,
      NULL,
      NULL,
      NULL,
      NULL
  );

  INSERT INTO adm.ad_account (account_id, ad_domain_id, ad_name, guid, ad_login)
  VALUES (
    new_account_id,
    (
      SELECT ad_domain.ad_domain_id
      FROM adm.ad_domain
      WHERE ad_instance_id = p_ad_instance_id AND upper(NAME) = upper(p_ad_domain)
    ),
    p_ad_name,
    p_ad_guid,
    p_ad_login
  );
END;
$$;

CREATE OR REPLACE FUNCTION adm.ad_pkg$update_account_groups(
  account_guid_arg TEXT,
  group_guids_arg  TEXT []
)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE
  account_rec RECORD;
BEGIN
  FOR account_rec IN SELECT account_id
                     FROM adm.ad_account
                     WHERE guid = account_guid_arg
  LOOP
    DELETE FROM adm.account2group
    WHERE account_id = account_rec.account_id
          AND group_id IN (SELECT ag.group_id
                               FROM adm.ad_group ag);
    INSERT INTO adm.account2group (account_id, group_id)
      SELECT
        account_rec.account_id,
        ag.group_id
      FROM adm.ad_group ag
      WHERE ag.guid = ANY (group_guids_arg);
  END LOOP;
END;
$$;


CREATE OR REPLACE FUNCTION adm.ad_pkg$update_group_accounts(
  p_group_id      INTEGER,
  p_group_guid    TEXT,
  p_account_guids TEXT []
)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_rec RECORD;
BEGIN
  FOR l_group_rec IN SELECT ad_group.group_id
                     FROM adm.ad_group
                     WHERE guid = p_group_guid OR group_id = p_group_id
  LOOP
    DELETE FROM adm.account2group
    WHERE group_id = l_group_rec.group_id;
    INSERT INTO adm.account2group (account_id, group_id)
      SELECT
        aa.account_id,
        l_group_rec.group_id
      FROM adm.ad_account aa
      WHERE aa.guid = ANY (p_account_guids);
  END LOOP;
END;
$$;
