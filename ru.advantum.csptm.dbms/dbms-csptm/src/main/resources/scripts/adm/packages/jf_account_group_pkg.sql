CREATE OR REPLACE FUNCTION adm.jf_account_group_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_account_id core.account.account_id%TYPE;
BEGIN
  l_account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', FALSE);

  OPEN p_rows FOR
  SELECT
    a.account_id,
    g.group_id,
    g.name,
    aa.account_id IS NOT NULL is_ad_sync
  FROM adm.group g
    JOIN adm.account2group a2g ON g.group_id = a2g.group_id
    JOIN core.account a ON a2g.account_id = a.account_id
    LEFT JOIN adm.ad_account aa ON a.account_id = aa.account_id
  WHERE NOT g.is_personal AND a.account_id = l_account_id;
END;
$$;


CREATE OR REPLACE FUNCTION adm.jf_account_group_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id   adm.group.group_id%TYPE;
  l_account_id core.account.account_id%TYPE;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);
  PERFORM adm.jf_group_pkg$check_not_ad(l_group_id);
  PERFORM adm.group_pkg$check_is_owner_or_admin(p_id_account, l_group_id);

  l_account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', FALSE);

  INSERT INTO adm.account2group (group_id, account_id) VALUES (
    l_group_id,
    l_account_id
  );

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_account_group_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id   adm.group.group_id%TYPE;
  l_account_id core.account.account_id%TYPE;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);
  PERFORM adm.jf_group_pkg$check_not_ad(l_group_id);
  PERFORM adm.group_pkg$check_is_owner_or_admin(p_id_account, l_group_id);

  l_account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', FALSE);

  DELETE FROM adm.account2group
  WHERE group_id = l_group_id AND account_id = l_account_id;

  RETURN NULL;
END;
$$;