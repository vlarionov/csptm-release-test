CREATE OR REPLACE FUNCTION adm.jf_role_call_pkg$attr_to_rowtype(p_attr TEXT)
  RETURNS jofl.ROLE2CALL
LANGUAGE plpgsql
AS $$
DECLARE
  lr jofl.role2call%ROWTYPE;
BEGIN
  lr.id_role     := jofl.jofl_pkg$extract_varchar(p_attr, 'id_role', FALSE);
  lr.db_method   := jofl.jofl_pkg$extract_varchar(p_attr, 'DB_METHOD', FALSE);
  lr.call_action := jofl.jofl_pkg$extract_varchar(p_attr, 'call_action', FALSE);
  RETURN lr;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_role_call_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_r jofl.role2call%ROWTYPE;
BEGIN
  l_r := adm.jf_role_call_pkg$attr_to_rowtype(p_attr);

  EXECUTE adm.jf_role_pkg$check_is_not_base(l_r.id_role);

  INSERT INTO jofl.role2call (id_role, db_method, call_action) VALUES (
    l_r.id_role,
    l_r.db_method,
    l_r.call_action
  );

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_role_call_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_r jofl.role2call%ROWTYPE;
BEGIN
  l_r := adm.jf_role_call_pkg$attr_to_rowtype(p_attr);

  EXECUTE adm.jf_role_pkg$check_is_not_base(l_r.id_role);

  DELETE FROM jofl.role2call
  WHERE id_role = l_r.id_role
        AND db_method = l_r.db_method
        AND call_action = l_r.call_action;
  RETURN NULL;
END;
$$;


CREATE OR REPLACE FUNCTION adm.jf_role_call_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  lv_role jofl.ref_role.id_role%TYPE := jofl.jofl_pkg$extract_varchar(p_attr, 'id_role', FALSE);
BEGIN
  OPEN p_rows FOR
  SELECT
    r2c.id_role,
    w.win_title,
    r2c.db_method "DB_METHOD",
    r2c.call_action
  FROM jofl.role2call r2c
    JOIN jofl.window w ON r2c.db_method = w.db_method
  WHERE r2c.id_role = lv_role
  ORDER BY win_title, call_action;
END;
$$;