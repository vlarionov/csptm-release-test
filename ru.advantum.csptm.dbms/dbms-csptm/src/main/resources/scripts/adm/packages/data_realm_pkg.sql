create or replace function adm.data_realm_pkg$is_super_data_realm(p_data_realm_id adm.data_realm.data_realm_id%type)
  returns boolean
language plpgsql
as $$
begin
  return p_data_realm_id = 30;
end;
$$;

create or replace function adm.data_realm_pkg$check_is_not_super_data_realm(p_data_realm_id adm.data_realm.data_realm_id%type)
  returns void
language plpgsql
as $$
begin
  if adm.data_realm_pkg$is_super_data_realm(p_data_realm_id)
  then
    raise exception 'Нельзя изменять супер область данных';
  end if;
end;
$$;

create or replace function adm.data_realm_pkg$get_tr_types(p_data_realm_ids integer [])
  returns setof core.tr_type.tr_type_id%type
language plpgsql
as $$
begin
  for i in 1..coalesce(array_upper(p_data_realm_ids, 1), 1)
  loop
    if adm.data_realm_pkg$is_super_data_realm(p_data_realm_ids [i])
    then
      return query
      select tr_type_id
      from core.tr_type;
    end if;
  end loop;

  return query
  select distinct tr_type_id
  from adm.data_realm2tr_type
  where data_realm_id = any (p_data_realm_ids);
end;
$$;

create or replace function adm.data_realm_pkg$get_depos(p_data_realm_ids integer [])
  returns setof core.depo.depo_id%type
language plpgsql
as $$
begin
  for i in 1..coalesce(array_upper(p_data_realm_ids, 1), 1)
  loop
    if adm.data_realm_pkg$is_super_data_realm(p_data_realm_ids [i])
    then
      return query
      select depo_id
      from core.depo;
    end if;
  end loop;

  return query
  select distinct dr2d.depo_id
  from adm.data_realm2depo dr2d
  where dr2d.data_realm_id = any (p_data_realm_ids);
end;
$$;

create or replace function adm.data_realm_pkg$get_routes(p_data_realm_ids integer [])
  returns setof rts.route.route_id%type
language plpgsql
as $$
begin
  for i in 1..coalesce(array_upper(p_data_realm_ids, 1), 1)
  loop
    if adm.data_realm_pkg$is_super_data_realm(p_data_realm_ids [i])
    then
      return query
      select route_id
      from rts.route;
    end if;
  end loop;

  return query
  select route_id
  from adm.data_realm2route
  where data_realm_id = any (p_data_realm_ids);
end;
$$;

create or replace function adm.data_realm_pkg$get_containing_tr_type(p_tr_type_id core.tr_type.tr_type_id%type)
  returns setof adm.data_realm.data_realm_id%type
language plpgsql
as $$
begin
  return query
  select dr.data_realm_id
  from adm.data_realm dr
  where adm.data_realm_pkg$is_super_data_realm(dr.data_realm_id) or exists(
      select 1
      from adm.data_realm2tr_type dr2tt
      where dr2tt.tr_type_id = p_tr_type_id and dr2tt.data_realm_id = dr.data_realm_id
  );
end;
$$;

create or replace function adm.data_realm_pkg$get_containing_depo(p_depo_id core.depo.depo_id%type)
  returns setof adm.data_realm.data_realm_id%type
language plpgsql
as $$
begin
  return query
  select dr.data_realm_id
  from adm.data_realm dr
  where adm.data_realm_pkg$is_super_data_realm(dr.data_realm_id) or exists(
      select 1
      from adm.data_realm2depo dr2d
      where dr2d.depo_id = p_depo_id and dr2d.data_realm_id = dr.data_realm_id
  );
end;
$$;

create or replace function adm.data_realm_pkg$get_containing_route(p_route_id rts.route.route_id%type)
  returns setof adm.data_realm.data_realm_id%type
language plpgsql
as $$
begin
  return query
  select dr.data_realm_id
  from adm.data_realm dr
  where adm.data_realm_pkg$is_super_data_realm(dr.data_realm_id) or exists(
      select 1
      from adm.data_realm2route dr2r
      where dr2r.route_id = p_route_id and dr2r.data_realm_id = dr.data_realm_id
  );
end;
$$;