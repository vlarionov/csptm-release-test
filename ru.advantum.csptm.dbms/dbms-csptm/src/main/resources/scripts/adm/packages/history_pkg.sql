CREATE OR REPLACE FUNCTION adm."history_pkg$get_available_windows"(p_id_account NUMERIC)
  RETURNS TEXT []
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN ARRAY(
      SELECT w.db_method
      FROM jofl.window w
        JOIN jofl.role2call r2c ON w.db_method = r2c.db_method
        JOIN jofl.account ja ON r2c.id_role = ANY (jofl.jofl_pkg$get_account_roles(ja.id_jofl_account))
      WHERE r2c.call_action = 'OF_HISTORY'
            AND ja.schema_name = core.const_pkg$core_project_name()
            AND ja.id_account_local = p_id_account
  );
END;
$$;


CREATE OR REPLACE FUNCTION adm."history_pkg$create_trigger"(p_table_schema TEXT, p_table_name TEXT, p_id_column TEXT)
  RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  EXECUTE
  'CREATE TRIGGER history_tg ' ||
  'AFTER INSERT OR UPDATE OR DELETE ' ||
  'ON ' || p_table_schema || '.' || p_table_name || ' FOR EACH ROW ' ||
  'EXECUTE PROCEDURE adm.history_pkg$history_tg_handler(''' || p_id_column || ''');';
END;
$$;


CREATE OR REPLACE FUNCTION adm.history_pkg$history_tg_handler()
  RETURNS TRIGGER
LANGUAGE plpgsql
AS $$
DECLARE
  l_columns         TEXT [];
  l_id_jofl_account NUMERIC(18);
  l_jofl_method     TEXT;
  l_jofl_action     TEXT;
  l_operation       INTEGER;
  l_row_id          TEXT;
  l_column          TEXT;
  l_old_value       TEXT;
  l_new_value       TEXT;
  l_is_row_modified BOOLEAN := FALSE;
  l_history_data_id INTEGER;
BEGIN
  --RAISE NOTICE 'trigger start';

  IF tg_op = 'INSERT'
  THEN l_operation = adm.const_pkg$insert_op();
  ELSEIF tg_op = 'UPDATE'
    THEN l_operation = adm.const_pkg$update_op();
  ELSEIF tg_op = 'DELETE'
    THEN l_operation = adm.const_pkg$delete_op();
  END IF;

  l_id_jofl_account:= jofl.jofl_util$get_usr_var(jofl.const_pkg$id_jofl_account());
  l_jofl_method := jofl.jofl_util$get_usr_var(jofl.const_pkg$jofl_method());
  l_jofl_action:= jofl.jofl_util$get_usr_var(jofl.const_pkg$jofl_action());

  IF (l_id_jofl_account IS NOT NULL AND l_jofl_method IS NOT NULL AND l_jofl_action IS NOT NULL)
  THEN
    --todo pass as trigger argument if possible
    EXECUTE 'SELECT ARRAY(SELECT c.column_name::TEXT FROM information_schema.columns c WHERE c.table_schema=$1 AND c.table_name=$2)'
    USING tg_table_schema, tg_table_name
    INTO l_columns;

    FOREACH l_column IN ARRAY l_columns
    LOOP
      --RAISE NOTICE 'column: %;', l_column;

      IF (l_operation = adm.const_pkg$insert_op())
      THEN
        EXECUTE 'SELECT ($1).' || l_column || '::TEXT'
        INTO l_new_value
        USING NEW;

        EXECUTE 'SELECT ($1).' || tg_argv [0] || '::TEXT'
        INTO l_row_id
        USING NEW;
      ELSEIF (l_operation = adm.const_pkg$update_op())
        THEN
          EXECUTE 'SELECT ($1).' || l_column || '::TEXT'
          INTO l_old_value
          USING OLD;
          EXECUTE 'SELECT ($1).' || l_column || '::TEXT'
          INTO l_new_value
          USING NEW;

          EXECUTE 'SELECT ($1).' || tg_argv [0] || '::TEXT'
          INTO l_row_id
          USING OLD;
      ELSEIF (l_operation = adm.const_pkg$delete_op())
        THEN
          EXECUTE 'SELECT ($1).' || l_column || '::TEXT'
          INTO l_old_value
          USING OLD;

          EXECUTE 'SELECT ($1).' || tg_argv [0] || '::TEXT'
          INTO l_row_id
          USING OLD;
      END IF;

      RAISE NOTICE 'old: %; new: %;', l_old_value, l_new_value;

      IF (l_old_value IS DISTINCT FROM l_new_value)
      THEN
        IF NOT l_is_row_modified
        THEN
          l_is_row_modified:=TRUE;

          l_history_data_id:= nextval('adm.history_data_id_seq');
          INSERT INTO adm.history_data (history_data_id, id_jofl_account, change_date, db_method, db_action, operation, table_schema, table_name, row_id)
          VALUES (
            l_history_data_id,
            l_id_jofl_account,
            now(),
            l_jofl_method,
            l_jofl_action,
            tg_op,
            TG_TABLE_SCHEMA,
            TG_TABLE_NAME,
            l_row_id
          );
        END IF;

        INSERT INTO adm.history_data_item (history_data_item_id, history_data_id, column_name, old_value, new_value)
        VALUES (
          nextval('adm.history_data_item_id_seq'),
          l_history_data_id,
          l_column,
          l_old_value,
          l_new_value
        );
      END IF;
    END LOOP;
  END IF;

  IF (l_operation = adm.const_pkg$insert_op() OR l_operation = adm.const_pkg$update_op())
  THEN
    RETURN NEW;
  ELSE
    RETURN OLD;
  END IF;

  EXCEPTION WHEN OTHERS
  THEN
    RAISE NOTICE 'SQLSTATE: %', SQLSTATE;

    IF (l_operation = adm.const_pkg$insert_op() OR l_operation = adm.const_pkg$update_op())
    THEN
      RETURN NEW;
    ELSE
      RETURN OLD;
    END IF;

END;
$$;


create or replace function adm.history_pkg$cron_clean()
  returns void
language plpgsql
as $$
begin

  delete from adm.history_data_item hdi
  where hdi.history_data_id in (
    select distinct hd.history_data_id t
    from adm.win_table wt
      join jofl.window w on wt.db_method = w.db_method
      join adm.history_data hd on wt.table_schema = hd.table_schema and wt.table_name = hd.table_name
      left join adm.history_clean hc on w.db_method = hc.db_method
    where (hc.cleaner_interval notnull and hc.cleaner_interval <> 0) and
          hd.change_date < (current_date - hc.cleaner_interval * '1 month' :: interval)
  );
  delete from adm.history_data ahd
    where ahd.history_data_id in (
      select distinct hd.history_data_id t
      from adm.win_table wt
        join jofl.window w on wt.db_method = w.db_method
        join adm.history_data hd on wt.table_schema = hd.table_schema and wt.table_name = hd.table_name
        left join adm.history_clean hc on w.db_method = hc.db_method
      where (hc.cleaner_interval notnull and hc.cleaner_interval <> 0) and
            hd.change_date < (current_date - hc.cleaner_interval * '1 month' :: interval)
    );
end;
$$;


create or replace function adm.history_pkg$insert_of_rows_handler(p_id_account NUMERIC,p_method text,p_attr text)
  returns void
language plpgsql
as $$
declare
  l_available_windows text[]:=adm."history_pkg$get_available_windows"(p_id_account);
begin
  if(p_method = any (l_available_windows))
    then
      insert into adm.db_method_access_history (action_data, id_jofl_account, db_method, params)  values (now(),p_id_account,p_method,p_attr);
    end if;
end;
$$;

