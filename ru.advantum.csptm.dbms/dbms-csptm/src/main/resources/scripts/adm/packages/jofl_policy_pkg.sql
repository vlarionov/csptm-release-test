CREATE OR REPLACE FUNCTION adm.jofl_policy_pkg$build(p_name TEXT, p_values TEXT [])
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN format('P{%s},D{%s}', p_name, array_to_string(p_values, ','));
END;
$$;

CREATE OR REPLACE FUNCTION adm.jofl_policy_pkg$build(p_name TEXT, p_value TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN adm.jofl_policy_pkg$build(p_name, ARRAY[p_value]);
END;
$$;

CREATE OR REPLACE FUNCTION adm.jofl_policy_pkg$append(p_policy_seq TEXT, p_policy TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
BEGIN
  IF p_policy IS NULL
  THEN RETURN p_policy_seq;
  END IF;

  IF p_policy_seq IS NULL OR p_policy_seq = ''
  THEN
    RETURN p_policy;
  ELSE
    RETURN p_policy_seq || ',' || p_policy;
  END IF;
END;
$$;

