CREATE OR REPLACE FUNCTION adm.jf_relative_group_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_id_role       jofl.ref_role.id_role%TYPE;
  l_data_realm_id adm.data_realm.data_realm_id%TYPE;
  l_sys_audit_id  adm.sys_audit.sys_audit_id%TYPE;
BEGIN
  l_id_role := jofl.jofl_pkg$extract_varchar(p_attr, 'id_role', TRUE);
  l_data_realm_id := jofl.jofl_pkg$extract_number(p_attr, 'data_realm_id', TRUE);
  l_sys_audit_id :=  jofl.jofl_pkg$extract_number(p_attr, 'sys_audit_id', TRUE);

  OPEN p_rows FOR
  SELECT
    DISTINCT
    l_id_role                                              id_role,
    l_data_realm_id                                        data_realm_id,
    l_sys_audit_id                                         sys_audit_id,
    g.group_id,
    adm.jf_relative_group_pkg$get_link(g.group_id, g.name) "name"
  FROM adm.group g
  WHERE NOT g.is_personal
        AND g.group_id IN (
    SELECT *
    FROM adm.group_data_pkg$get_containing_role(l_id_role)
    UNION
    SELECT *
    FROM adm.group_data_pkg$get_containing_data_realm(l_data_realm_id)
    UNION
    SELECT *
    FROM adm.group_data_pkg$get_containing_sys_audit(l_sys_audit_id)
  )
  ORDER BY name;
END;
$$;


CREATE OR REPLACE FUNCTION adm.jf_relative_group_pkg$get_link(p_group_id INTEGER, p_group_name TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN '[' ||
         json_build_object(
             'CAPTION', p_group_name,
             'JUMP', json_build_object(
                 'METHOD', 'adm.group',
                 'ATTR', json_build_object('f_group_id', p_group_id) :: TEXT
             )
         )
         || ']';
END;
$$;