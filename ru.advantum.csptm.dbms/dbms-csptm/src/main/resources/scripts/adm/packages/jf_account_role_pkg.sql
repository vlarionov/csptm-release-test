CREATE OR REPLACE FUNCTION adm.jf_account_role_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR,
                                                           p_attr       TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_account_id core.account.account_id%TYPE;
BEGIN
  l_account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', FALSE);

  OPEN p_rows FOR
  SELECT
    a2g.account_id,
    r.role_name,
    string_agg(g.name, ', ') group_name
  FROM adm.account2group a2g
    JOIN adm.group g ON a2g.group_id = g.group_id
    ,adm.group_data_pkg$get_roles(g.group_id) g2r
    JOIN jofl.ref_role r ON g2r.id_role = r.id_role
  WHERE a2g.account_id = l_account_id
  GROUP BY g2r.id_role, a2g.account_id, r.role_name;
END;
$$;