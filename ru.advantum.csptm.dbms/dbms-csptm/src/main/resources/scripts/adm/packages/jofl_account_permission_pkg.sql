CREATE OR REPLACE FUNCTION adm.jofl_account_permission_pkg$get_available_incident_types(p_id_jofl_account NUMERIC)
  RETURNS TABLE(incident_type_id SMALLINT)
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  SELECT DISTINCT g2et.incident_type_id
  FROM jofl.account ja
    JOIN adm.account2group a2g ON ja.id_account_local = a2g.account_id
    , adm.group_data_pkg$get_incident_types(a2g.group_id) g2et
  WHERE ja.id_jofl_account = p_id_jofl_account AND ja.schema_name = core.const_pkg$core_project_name();
END;
$$;

create or replace function adm.jofl_account_permission_pkg$get_available_tr_ids(p_id_jofl_account numeric)
  returns setof core.tr.tr_id%type
language plpgsql
as $$
begin
  return query
  select adm.account_data_realm_pkg$get_trs_by_all(
      adm.jofl_account_permission_pkg$get_local_id(p_id_jofl_account)
  );
end;
$$;

CREATE OR REPLACE FUNCTION adm.jofl_account_permission_pkg$get_local_id(p_id_jofl_account NUMERIC)
  RETURNS NUMERIC
LANGUAGE plpgsql
AS $$
DECLARE l_id_account_local NUMERIC;
BEGIN
  SELECT a.id_account_local
  INTO l_id_account_local
  FROM jofl.account a
  WHERE a.id_jofl_account = p_id_jofl_account;
  RETURN l_id_account_local;
END;
$$;