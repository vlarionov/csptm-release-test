CREATE OR REPLACE FUNCTION adm.jf_group_incident_type_pkg$of_rows(p_id_account NUMERIC, OUT p_rows REFCURSOR, p_attr TEXT)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id adm.incident_type2group.group_id%TYPE;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);

  OPEN p_rows FOR
  SELECT
    l_group_id                                                         group_id,
    et.incident_type_id,
    et.incident_type_name,
    adm.group_pkg$get_of_delete_owner_policy(p_id_account, l_group_id) "ROW$POLICY"
  FROM adm.group_data_pkg$get_incident_types(l_group_id) g2et
    JOIN ttb.incident_type et ON g2et.incident_type_id = et.incident_type_id
  ORDER BY et.incident_type_name;
END;
$$;


CREATE OR REPLACE FUNCTION adm.jf_group_incident_type_pkg$of_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id      adm.incident_type2group.group_id%TYPE;
  l_incident_type_id adm.incident_type2group.incident_type_id%TYPE;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);
  l_incident_type_id := jofl.jofl_pkg$extract_number(p_attr, 'incident_type_id', FALSE);

  PERFORM adm.group_data_pkg$check_is_not_super_group(l_group_id);
  PERFORM adm.group_pkg$check_is_owner(p_id_account, l_group_id);

  INSERT INTO adm.incident_type2group (incident_type_id, group_id) VALUES (
    l_incident_type_id,
    l_group_id
  );

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_group_incident_type_pkg$of_delete(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id      adm.incident_type2group.group_id%TYPE;
  l_incident_type_id adm.incident_type2group.incident_type_id%TYPE;
BEGIN
  l_group_id := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);
  l_incident_type_id := jofl.jofl_pkg$extract_number(p_attr, 'incident_type_id', FALSE);

  PERFORM adm.group_data_pkg$check_is_not_super_group(l_group_id);
  PERFORM adm.group_pkg$check_is_owner(p_id_account, l_group_id);

  DELETE FROM adm.incident_type2group
  WHERE group_id = l_group_id AND incident_type_id = l_incident_type_id;

  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION adm.jf_group_incident_type_pkg$of_incident_insert(p_id_account NUMERIC, p_attr TEXT)
  RETURNS TEXT
LANGUAGE plpgsql
AS $$
DECLARE
  l_group_id   adm.group.group_id%TYPE := jofl.jofl_pkg$extract_number(p_attr, 'group_id', FALSE);
  l_incident_type_id bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_incident_type_id', true);
BEGIN
  PERFORM adm.group_pkg$check_is_owner(p_id_account, l_group_id);
  INSERT INTO adm.incident_type2group (group_id, incident_type_id)
    select l_group_id, inc_id  from unnest(l_incident_type_id) inc_id
    on conflict do nothing;
  return null;
END;
$$;

--select * from adm.incident_type2group
