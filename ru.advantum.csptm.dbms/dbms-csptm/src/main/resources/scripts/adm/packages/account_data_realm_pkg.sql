create or replace function adm.account_data_realm_pkg$get_data_realms(p_account_id numeric)
  returns setof adm.data_realm.data_realm_id%type
language plpgsql
as $$
begin
  return query
  select adm.group_data_pkg$get_data_realms(array_agg(distinct a2g.group_id))
  from adm.account2group a2g
  where a2g.account_id = p_account_id;
end;
$$;

create or replace function adm.account_data_realm_pkg$get_tr_types(p_account_id numeric)
  returns setof core.tr_type.tr_type_id%type
language plpgsql
as $$
begin
  return query
  select adm.data_realm_pkg$get_tr_types(
      array(select adm.account_data_realm_pkg$get_data_realms(p_account_id))
  );
end;
$$;
create or replace function adm.account_data_realm_pkg$get_tr_types_by_all(p_account_id numeric)
  returns setof core.tr_type.tr_type_id%type
language plpgsql
as $$
begin
  return query
  select adm.account_data_realm_pkg$get_tr_types(p_account_id)
  union
  select r.tr_type_id
  from rts.route r
    join rts.depo2route d2r on r.route_id = d2r.route_id
  where
    r.route_id in (select adm.account_data_realm_pkg$get_routes(p_account_id))
    or d2r.depo_id in (select adm.account_data_realm_pkg$get_depos(p_account_id));
end;
$$;

create or replace function adm.account_data_realm_pkg$get_depos(p_account_id numeric)
  returns setof core.depo.depo_id%type
language plpgsql
as $$
begin
  return query
  select adm.data_realm_pkg$get_depos(
      array(select adm.account_data_realm_pkg$get_data_realms(p_account_id))
  );
end;
$$;
create or replace function adm.account_data_realm_pkg$get_depos_by_all(p_account_id numeric)
  returns setof core.depo.depo_id%type
language plpgsql
as $$
begin
  return query
  select adm.account_data_realm_pkg$get_depos(p_account_id)
  union
  select d2r.depo_id
  from rts.route r
    join rts.depo2route d2r on r.route_id = d2r.route_id
  where
    r.route_id in (select adm.account_data_realm_pkg$get_routes(p_account_id))
    or r.tr_type_id in (select adm.account_data_realm_pkg$get_tr_types(p_account_id));
end;
$$;

create or replace function adm.account_data_realm_pkg$get_routes(p_account_id numeric)
  returns setof rts.route.route_id%type
language plpgsql
as $$
begin
  return query
  select adm.data_realm_pkg$get_routes(
      array(select adm.account_data_realm_pkg$get_data_realms(p_account_id))
  );
end;
$$;
create or replace function adm.account_data_realm_pkg$get_routes_by_depo(p_account_id numeric)
  returns setof rts.route.route_id%type
language plpgsql
as $$
begin
  return query
  select adm.account_data_realm_pkg$get_routes(p_account_id)
  union
  select route_id
  from rts.depo2route
  where depo_id in (select adm.account_data_realm_pkg$get_depos(p_account_id));
end;
$$;
create or replace function adm.account_data_realm_pkg$get_routes_by_tr_type(p_account_id numeric)
  returns setof rts.route.route_id%type
language plpgsql
as $$
begin
  return query
  select route_id
  from rts.route
  where tr_type_id in (select adm.account_data_realm_pkg$get_tr_types(p_account_id));
end;
$$;
create or replace function adm.account_data_realm_pkg$get_routes_by_all(p_account_id numeric)
  returns setof rts.route.route_id%type
language plpgsql
as $$
begin
  return query
  select adm.account_data_realm_pkg$get_routes(p_account_id)
  union select adm.account_data_realm_pkg$get_routes_by_depo(p_account_id)
  union select adm.account_data_realm_pkg$get_routes_by_tr_type(p_account_id);
end;
$$;

create or replace function adm.account_data_realm_pkg$get_trs_by_tr_type(p_account_id numeric)
  returns setof core.tr.tr_id%type
language plpgsql
as $$
begin
  return query
  select tr_id
  from core.tr
  where tr_type_id in (select adm.account_data_realm_pkg$get_tr_types(p_account_id));
end;
$$;
create or replace function adm.account_data_realm_pkg$get_trs_by_depo(p_account_id numeric)
  returns setof core.tr.tr_id%type
language plpgsql
as $$
begin
  return query
  select tr.tr_id
  from core.tr tr
  where tr.depo_id in (select adm.account_data_realm_pkg$get_depos(p_account_id));
end;
$$;
create or replace function adm.account_data_realm_pkg$get_trs_by_route(p_account_id numeric)
  returns setof core.tr.tr_id%type
language plpgsql
as $$
begin
  return query
  select tto.tr_id
  from rts.route_variant rv
    join ttb.tt_variant ttv on rv.route_variant_id = ttv.route_variant_id
    join ttb.tt_out tto on ttv.tt_variant_id = tto.tt_variant_id
  where
    rv.route_id in (select adm.account_data_realm_pkg$get_routes(p_account_id))
    and ttv.order_date in (current_date, current_date - '1 DAY' :: interval)
    and not rv.sign_deleted
    and not ttv.sign_deleted
    and not tto.sign_deleted;
end;
$$;
create or replace function adm.account_data_realm_pkg$get_trs_by_tr_type_and_depo(p_account_id numeric)
  returns setof core.tr.tr_id%type
language plpgsql
as $$
begin
  return query
  select adm.account_data_realm_pkg$get_trs_by_tr_type(p_account_id)
  union select adm.account_data_realm_pkg$get_trs_by_depo(p_account_id);
end;
$$;
create or replace function adm.account_data_realm_pkg$get_trs_by_all(p_account_id numeric)
  returns setof core.tr.tr_id%type
language plpgsql
as $$
begin
  return query
  select adm.account_data_realm_pkg$get_trs_by_tr_type(p_account_id)
  union select adm.account_data_realm_pkg$get_trs_by_depo(p_account_id)
  union select adm.account_data_realm_pkg$get_trs_by_route(p_account_id);
end;
$$;
create or replace function adm.account_data_realm_pkg$get_trs_on_periods_by_route(p_account_id numeric, p_tsrange tsrange)
  returns table(tr_id core.tr.tr_id%type, period tsrange)
language plpgsql
as $$
begin
  return query
  select
    tto.tr_id,
    tsrange(ttv.order_date, ttv.order_date + '1 DAY' :: interval)
  from rts.route_variant rv
    join ttb.tt_variant ttv on rv.route_variant_id = ttv.route_variant_id
    join ttb.tt_out tto on ttv.tt_variant_id = tto.tt_variant_id
  where
    rv.route_id in (select adm.account_data_realm_pkg$get_routes(p_account_id))
    and p_tsrange @> ttv.order_date :: timestamp
    and not rv.sign_deleted
    and not ttv.sign_deleted
    and not tto.sign_deleted;
end;
$$;
create or replace function adm.account_data_realm_pkg$get_trs_on_periods_by_all(p_account_id numeric, p_tsrange tsrange)
  returns table(tr_id bigint, period tsrange)
language plpgsql
as $$
begin
  return query
  select
    adm.account_data_realm_pkg$get_trs_by_tr_type_and_depo(p_account_id),
    null :: tsrange
  union
  select *
  from adm.account_data_realm_pkg$get_trs_on_periods_by_route(p_account_id, p_tsrange);
end;
$$;

create or replace function adm.account_data_realm_pkg$get_stop_items_by_all(p_account_id numeric)
  returns setof rts.stop_item.stop_item_id%type
language plpgsql
as $$
begin
  return query
  select distinct si.stop_item_id
  from rts.route r
    join rts.route_variant rv on r.current_route_variant_id = rv.route_variant_id
    join rts.round rnd on rv.route_variant_id = rnd.route_variant_id
    join rts.stop_item2round si2r on rnd.round_id = si2r.round_id
    join rts.stop_item si on si2r.stop_item_id = si.stop_item_id
    left join rts.depo2route d2r on r.route_id = d2r.route_id
  where (
          r.route_id in (select adm.account_data_realm_pkg$get_routes(p_account_id))
          or r.tr_type_id in (select adm.account_data_realm_pkg$get_tr_types(p_account_id))
          or d2r.depo_id in (select adm.account_data_realm_pkg$get_depos(p_account_id))
        )
        and not r.sign_deleted
        and not rv.sign_deleted
        and not si2r.sign_deleted
        and not r.sign_deleted
        and not si.sign_deleted;
end;
$$;