﻿-- view: gis.v_rasp_variants

-- drop view gis.v_rasp_variants

create or replace view gis.v_rasp_variants as
select server_id  as srv_id, 
       rasp_variant_id as rv_id, 
       route_id   as mr_id, 
       week_mask  as rv_dow, 
       season     as rv_season, 
       begin_date as rv_startdate, 
       end_date   as rv_enddate, 
       case when end_date is null then 0 else 1 end as rv_enddateexists,
       rasp_num   as rv_num, 
       check_sum  as rv_checksum
  from gis.rasp_variants;

alter table gis.v_rasp_variants
  owner to adv;

comment on view gis.v_rasp_variants is 'Варианты расписаний';
comment on column gis.v_rasp_variants.srv_id is 'ID сервера'; 
comment on column gis.v_rasp_variants.rv_id is 'ID варианта расписания'; 
comment on column gis.v_rasp_variants.mr_id is 'ID мршрута'; 
comment on column gis.v_rasp_variants.rv_dow is 'Дни недели'; 
comment on column gis.v_rasp_variants.rv_season is 'Сезон действия'; 
comment on column gis.v_rasp_variants.rv_startdate is 'Начало действия'; 
comment on column gis.v_rasp_variants.rv_enddate is 'Окончание действия'; 
comment on column gis.v_rasp_variants.rv_enddateexists is 'Наличие Окончания действия';
comment on column gis.v_rasp_variants.rv_num is 'Номер варианта расписания'; 
comment on column gis.v_rasp_variants.rv_checksum is 'Контрольная сумма';

-- select * from gis.v_rasp_variants limit 100
