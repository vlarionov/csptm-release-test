﻿
-- view: gis.v_marsh_psg_avg_value

-- drop view gis.v_marsh_psg_avg_value

create or replace view gis.v_marsh_psg_avg_value as
select sum(marsh_avg_value) as marsh_avg_value
  from gis.marsh_psg_avg_value;

alter table gis.v_marsh_psg_avg_value
  owner to adv;

comment on view gis.v_marsh_psg_avg_value is 'Среднее значение пассажиропотока по маршруту';
comment on column gis.v_marsh_psg_avg_value.marsh_avg_value is 'Среднее значение пассажиропотока по маршруту'; 

-- select * from gis.v_marsh_psg_avg_value