﻿-- view: gis.v_rasp_holidays

-- drop view gis.v_rasp_holidays

create or replace view gis.v_rasp_holidays as
select route_id     as mr_id, 
       holiday_date as rh_date, 
       week_mask    as rh_dow
  from gis.rasp_holidays;

alter table gis.v_rasp_holidays
  owner to adv;

comment on view gis.v_rasp_holidays is 'Праздничные дни';
comment on column gis.v_rasp_holidays.mr_id is 'ID маршрута';
comment on column gis.v_rasp_holidays.rh_date is 'Праздничная дата';
comment on column gis.v_rasp_holidays.rh_dow is 'Маска для недели, по которому работает транспорт';

-- select * from gis.v_rasp_holidays
