﻿
-- view: gis.v_driver_modes

-- drop view gis.v_driver_modes

create or replace view gis.v_driver_modes as
select driver_mode_id    as dm_id, 
       driver_mode_name  as dm_title, 
       transport_type_id as tt_id, 
       driver_mode_digit as dm_digit, 
       driver_mode_smena_num as dm_sm
  from gis.driver_modes;

alter table gis.v_driver_modes
  owner to adv;

comment on view gis.v_driver_modes is 'Режимы работы водителя';
comment on column gis.v_driver_modes.dm_id is 'ID режима работы водителя';
comment on column gis.v_driver_modes.dm_title is 'Наименование режима работы водителя';
comment on column gis.v_driver_modes.tt_id is 'ID типа транспорта';
comment on column gis.v_driver_modes.dm_digit is 'Первая цифра трехзначного номера графика';
comment on column gis.v_driver_modes.dm_sm is 'Количество смен';

-- select * from gis.v_driver_modes