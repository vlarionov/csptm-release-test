﻿-- view: gis.v_race_card

-- drop view gis.v_race_card

create or replace view gis.v_race_card as
select route_variant_id  as mv_id, 
       race_type         as rl_racetype,
       race_type_ext     as rl_racetypeext,
       stop_order        as rc_orderby,
       stop_id           as st_id,
       flag_start_finish as rc_kkp,
       distance          as rc_distance
  from gis.race_card;

alter table gis.v_race_card
  owner to adv;

comment on view gis.v_race_card is 'Последовательность остановок по типам рейсов';
comment on column gis.v_race_card.mv_id is 'ID варианта маршрута';
comment on column gis.v_race_card.rl_racetype is 'Тип рейса';
comment on column gis.v_race_card.rl_racetypeext is 'Расш наименование типа рейса';
comment on column gis.v_race_card.rc_orderby is 'Номер остановки';
comment on column gis.v_race_card.st_id is 'ID остановки';
comment on column gis.v_race_card.rc_kkp is 'Признак конечной остановки';
comment on column gis.v_race_card.rc_distance is 'Расстояние до следующей остановки';

-- select * from gis.v_race_card