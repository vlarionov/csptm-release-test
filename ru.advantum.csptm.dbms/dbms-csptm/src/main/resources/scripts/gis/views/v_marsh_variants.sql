﻿-- view: gis.v_marsh_variants

-- drop view gis.v_marsh_variants

create or replace view gis.v_marsh_variants as
select route_variant_id   as mv_id,
       route_id           as mr_id,
       check_sum          as mv_checksum,
       begin_date         as mv_startdate,
       end_date           as mv_enddate,
       case when end_date is null then 0 else 1 end as mv_enddateexists,
       route_variant_desc as mv_desc
  from gis.marsh_variants;

alter table gis.v_marsh_variants
  owner to adv;

comment on view gis.v_marsh_variants is 'Варианты маршрута';
comment on column gis.v_marsh_variants.mv_id is 'ID варианта маршрута'; 
comment on column gis.v_marsh_variants.mr_id is 'ID маршрута'; 
comment on column gis.v_marsh_variants.mv_checksum is 'Контрольная сумма'; 
comment on column gis.v_marsh_variants.mv_startdate is 'Начало действия'; 
comment on column gis.v_marsh_variants.mv_enddate is 'Окончание действия'; 
comment on column gis.v_marsh_variants.mv_enddateexists is 'Наличие Окончания действия'; 
comment on column gis.v_marsh_variants.mv_desc is 'Описание варианта маршрута'; 

-- select * from gis.v_marsh_variants
