﻿
-- view: gis.v_race_plan_time

-- drop view gis.v_race_plan_time

create or replace view gis.v_race_plan_time as
select route_variant_id as mv_id, 
       race_type        as rl_racetype, 
       race_type_ext    as rl_racetypeext, 
       avg_time         as rpt_avg_time, 
       expl_speed       as rpt_expl_speed
  from gis.race_plan_time;

alter table gis.v_race_plan_time
  owner to adv;

comment on view gis.v_race_plan_time is 'Среднее плановое время оборотного рейса маршрута';
comment on column gis.v_race_plan_time.mv_id is 'ID варианта маршрута'; 
comment on column gis.v_race_plan_time.rl_racetype is 'Тип рейса'; 
comment on column gis.v_race_plan_time.rl_racetypeext is 'Номер варианта расписания'; 
comment on column gis.v_race_plan_time.rpt_avg_time is 'Среднее плановое время оборотного рейса'; 
comment on column gis.v_race_plan_time.rpt_expl_speed is 'Эксплуатационная скорость'; 

-- select * from gis.v_race_plan_time
