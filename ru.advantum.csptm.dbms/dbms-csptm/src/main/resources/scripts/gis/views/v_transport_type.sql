﻿-- view: gis.v_transport_type

-- drop view gis.v_transport_type

create or replace view gis.v_transport_type as
select transport_type_id   as tt_id, 
       transport_type_name as tt_title, 
       transport_type_note as tt_note
  from gis.transport_type;

alter table gis.v_transport_type
  owner to adv;

comment on view gis.v_transport_type is 'Типы транспорта';
comment on column gis.v_transport_type.tt_id is 'ID типа транспорта'; 
comment on column gis.v_transport_type.tt_title is 'Наименование типа транспорта'; 
comment on column gis.v_transport_type.tt_note is 'Описание типа транспорта';

-- select * from gis.v_transport_type
-- select tt_id, tt_title, tt_note from gis.v_transport_type