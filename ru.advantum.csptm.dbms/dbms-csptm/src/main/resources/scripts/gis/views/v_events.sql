﻿-- view: gis.v_events

-- drop view gis.v_events

create or replace view gis.v_events as
select event_id   as ev_id, 
       event_name as ev_title
  from gis.events;

alter table gis.v_events
  owner to adv;

comment on view gis.v_events is 'Типы технологических событий';
comment on column gis.v_events.ev_id is 'ID типа технологического события'; 
comment on column gis.v_events.ev_title is 'Наименование типа технологического события';

-- select * from gis.v_events