﻿-- view: gis.v_race_list

-- drop view gis.v_race_list

create or replace view gis.v_race_list as
select route_variant_id  as mv_id,
       race_type         as rl_racetype,
       race_type_ext     as rl_racetypeext,
       first_station_id  as rl_firststation_id,
       last_station_id   as rl_laststation_id
  from gis.race_list;

alter table gis.v_race_list
  owner to adv;

comment on view gis.v_race_list is 'Рейсы';
comment on column gis.v_race_list.mv_id is 'ID варианта маршрута'; 
comment on column gis.v_race_list.rl_racetype is 'Тип рейса'; 
comment on column gis.v_race_list.rl_racetypeext is 'Расш наименование типа рейса'; 
comment on column gis.v_race_list.rl_firststation_id is 'ID начальной остановки'; 
comment on column gis.v_race_list.rl_laststation_id is 'ID конечной остановки'; 

-- select * from gis.v_race_list
