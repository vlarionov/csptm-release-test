﻿--TODO remove
-- view: gis.v_nav_point

-- drop view gis.v_nav_point

create or replace view gis.v_nav_point as
select p.point_id, p.point_date, p.tr_id, p.log, p.lat, p.heading, p.point_num, p.point_tag,
       p.route_id, p.race_id, p.stop_id, p.route_trajectory_id, t.gr_num, t.park_name
  from gis.nav_point p
  join gis.tr t on t.tr_id = p.tr_id;

alter table gis.v_nav_point
  owner to adv;

comment on view gis.v_nav_point is 'Данные по навигационным отметкам';
comment on column gis.v_nav_point.point_id is 'ID'; 
comment on column gis.v_nav_point.point_date is 'Время навигационной отметки'; 
comment on column gis.v_nav_point.tr_id is 'ID ТС'; 
comment on column gis.v_nav_point.log is 'Широта'; 
comment on column gis.v_nav_point.lat is 'Долгота'; 
comment on column gis.v_nav_point.heading is 'Азимут'; 
comment on column gis.v_nav_point.point_num is 'Порядковый номер навигационной отметки'; 
comment on column gis.v_nav_point.point_tag is 'Информационная метка'; 
comment on column gis.v_nav_point.route_id is 'ID маршрута';
comment on column gis.v_nav_point.race_id is 'ID рейса';
comment on column gis.v_nav_point.stop_id is 'ID остановки';
comment on column gis.v_nav_point.route_trajectory_id is 'ID траектории'; 
comment on column gis.v_nav_point.gr_num is 'Гаражный номер'; 
comment on column gis.v_nav_point.park_name is 'Номер парка'; 

-- select * from gis.v_nav_point limit 100
