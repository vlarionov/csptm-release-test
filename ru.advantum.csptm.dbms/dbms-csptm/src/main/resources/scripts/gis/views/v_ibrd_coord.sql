﻿-- view: gis.v_ibrd_coord

-- drop view gis.v_ibrd_coord

create or replace view gis.v_ibrd_coord as
select ibg.muid            as info_board_muid,
       evt.event_date      as received_date,
       evt.is_coords_valid as is_valid,
       evt.lon,
       evt.lat
  from ibrd.event evt
  join ibrd.info_board2gis ibg on ibg.info_board_id = evt.info_board_id
 where evt.command_event_id = ibrd.event_type_pkg$telematics_response()
   and evt.error_type_id = 0;

comment on view gis.v_ibrd_coord is 'Координаты ИТОП';
comment on column gis.v_ibrd_coord.info_board_muid is 'Уникальный идентификатор ИТОП';
comment on column gis.v_ibrd_coord.received_date is 'Дата и время пакета данных';
comment on column gis.v_ibrd_coord.is_valid is 'Валидность координаты';
comment on column gis.v_ibrd_coord.lon is 'Широта';
comment on column gis.v_ibrd_coord.lat is 'Долгота';

-- select * from gis.v_ibrd_coord limit 100
