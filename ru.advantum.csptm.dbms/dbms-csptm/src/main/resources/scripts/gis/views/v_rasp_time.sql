﻿-- view: gis.v_rasp_time

-- drop view gis.v_rasp_time

create or replace view gis.v_rasp_time as
select server_id       as srv_id,
       rasp_variant_id as rv_id,
       graph_shift_id  as gr_id,
       order_by        as rt_orderby,
       graph           as rt_graph,
       smena           as rt_smena,
       race_type       as rl_racetype,
       race_type_ext   as rl_racetypeext,
       stop_id         as st_id,
       stop_time       as rt_time,
       flag_inv        as rt_inv,
       race_num        as rt_racenum,
       flag_start_finish as rc_kkp
  from gis.rasp_time;

alter table gis.v_rasp_time
  owner to adv;

comment on view gis.v_rasp_time is 'Плановое время отправлений по остановкам';
comment on column gis.v_rasp_time.srv_id is 'ID сервера';
comment on column gis.v_rasp_time.rv_id is 'ID варианта расписания';
comment on column gis.v_rasp_time.gr_id is 'ID смены графика';
comment on column gis.v_rasp_time.rt_orderby is 'Последовательность прохождения остановок';
comment on column gis.v_rasp_time.rt_graph is 'Номер выхода';
comment on column gis.v_rasp_time.rt_smena is 'Смена';
comment on column gis.v_rasp_time.rl_racetype is 'Тип рейса';
comment on column gis.v_rasp_time.rl_racetypeext is 'Расш наим типа рейса';
comment on column gis.v_rasp_time.st_id is 'ID остановки';
comment on column gis.v_rasp_time.rt_time is 'Врема отправления';
comment on column gis.v_rasp_time.rt_inv is 'Флаг спец график';
comment on column gis.v_rasp_time.rt_racenum is 'Номер рейса в графике';
comment on column gis.v_rasp_time.rc_kkp is 'Признак конечной остановки';

-- select * from gis.v_rasp_time
