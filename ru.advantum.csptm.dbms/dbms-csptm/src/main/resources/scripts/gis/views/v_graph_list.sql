﻿
-- view: gis.v_graph_list

-- drop view gis.v_graph_list

create or replace view gis.v_graph_list as
select server_id       as srv_id, 
       rasp_variant_id as rv_id, 
       graph_num       as gl_graph, 
       driver_mode_id  as dm_id, 
       flag_inv        as gl_inv, 
       capacity        as gl_cap
  from gis.graph_list;

alter table gis.v_graph_list
  owner to adv;

comment on view gis.v_graph_list is 'Графики (Выходы)';
comment on column gis.v_graph_list.srv_id is 'ID сервера'; 
comment on column gis.v_graph_list.rv_id is 'ID варианта расписания'; 
comment on column gis.v_graph_list.gl_graph is 'Номер выхода'; 
comment on column gis.v_graph_list.dm_id is 'ID режима работы водителя'; 
comment on column gis.v_graph_list.gl_inv is 'Флаг спец графика';  
comment on column gis.v_graph_list.gl_cap is 'Требуемая вместимость'; 

-- select * from gis.v_graph_list
