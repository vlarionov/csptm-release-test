﻿
-- view: gis.v_rasp_events

-- drop view gis.v_rasp_events

create or replace view gis.v_rasp_events as
select server_id       as srv_id, 
       rasp_variant_id as rv_id, 
       graph_shift_id  as gr_id, 
       order_by        as re_orderby, 
       graph           as re_graph, 
       smena           as re_smena, 
       begin_time      as re_starttime, 
       end_time        as re_endtime, 
       event_id        as ev_id, 
       distance        as re_distance, 
       null_type       as re_nulltype
  from gis.rasp_events;

alter table gis.v_rasp_events
  owner to adv;

comment on view gis.v_rasp_events is 'Плановые технологические события';
comment on column gis.v_rasp_events.srv_id is 'ID сервера';
comment on column gis.v_rasp_events.rv_id is 'ID варианта расписания';
comment on column gis.v_rasp_events.gr_id is 'ID сменыграфика';
comment on column gis.v_rasp_events.re_orderby is 'последовательность событий одной смены';
comment on column gis.v_rasp_events.re_graph is 'Номер выхода';
comment on column gis.v_rasp_events.re_smena is 'Смена';
comment on column gis.v_rasp_events.re_starttime is 'Начало события';
comment on column gis.v_rasp_events.re_endtime is 'Окончание события';
comment on column gis.v_rasp_events.ev_id is 'Тип события';
comment on column gis.v_rasp_events.re_distance is 'Расстояние для нулевого рейса';
comment on column gis.v_rasp_events.re_nulltype is 'Тип нулевого рейса';

-- select * from gis.v_rasp_events
