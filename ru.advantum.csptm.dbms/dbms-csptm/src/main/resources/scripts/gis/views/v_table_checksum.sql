﻿-- view: gis.v_table_checksum

-- drop view gis.v_table_checksum

create or replace view gis.v_table_checksum as
select table_name as cs_table_name, 
       check_sum  as cs_check_sum
  from gis.table_checksum;

alter table gis.v_table_checksum
  owner to adv;

comment on view gis.v_table_checksum is 'Контрольные суммы общих таблиц';
comment on column gis.v_table_checksum.cs_table_name is 'Наименование таблицы'; 
comment on column gis.v_table_checksum.cs_check_sum is 'Контрольная сумма';

-- select * from gis.v_table_checksum