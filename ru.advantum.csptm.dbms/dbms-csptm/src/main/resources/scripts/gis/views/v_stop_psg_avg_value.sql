﻿
-- view: gis.v_stop_psg_avg_value

-- drop view gis.v_stop_psg_avg_value

create or replace view gis.v_stop_psg_avg_value as
select route_id     as mr_id, 
       stop_avg_in, 
       stop_avg_out
  from gis.stop_psg_avg_value;

alter table gis.v_stop_psg_avg_value
  owner to adv;

comment on view gis.v_stop_psg_avg_value is 'Среднее значение пассажиропотока по остановочному пункту за сутки';
comment on column gis.v_stop_psg_avg_value.mr_id is 'ID маршрута';
comment on column gis.v_stop_psg_avg_value.stop_avg_in is 'Среднее значение пассажиропотока по остановке вошедших';  
comment on column gis.v_stop_psg_avg_value.stop_avg_out is 'Среднее значение пассажиропотока по остановке вышедших'; 

-- select * from gis.v_stop_psg_avg_value
