﻿-- Tablespace: gis_data

-- DROP TABLESPACE gis_data

CREATE TABLESPACE gis_data
  OWNER adv
  LOCATION '/u00/pg_data/gis/gis_data';

-- Tablespace: gis_idxll 

-- DROP TABLESPACE gis_idx

CREATE TABLESPACE gis_idx
  OWNER adv
  LOCATION '/u00/pg_data/gis/gis_idx';

-- ==============================================
-- АСИП
-- Tablespace: gis_data

-- DROP TABLESPACE gis_data

CREATE TABLESPACE gis_data
  OWNER postgres
  LOCATION '/u00/postgres/9.5/tbs/GIS/GIS_DATA';

-- Tablespace: gis_idx

-- DROP TABLESPACE gis_idx

CREATE TABLESPACE gis_idx
  OWNER postgres
  LOCATION '/u00/postgres/9.5/tbs/GIS/GIS_IDX';

-- ==============================================

