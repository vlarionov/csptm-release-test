#!/bin/sh

dbdir=$1
pguser=$2
projectname=$3

case $1 in 
*)
                echo 'DB dir must be setted'
				;;
esac				
case $2 in 
*)
                echo 'DB user owner must be setted'
				;;
esac

mkdir $dbdir/temp
mkdir $dbdir/gis
mkdir $dbdir/gis/gis_data
mkdir $dbdir/gis/gis_idx
chown $pguser -R $dbdir