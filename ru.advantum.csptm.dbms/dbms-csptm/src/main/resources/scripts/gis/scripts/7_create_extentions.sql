-- Extension: postgis

-- DROP EXTENSION postgis;

 CREATE EXTENSION postgis
  SCHEMA public
  VERSION "2.2.2";

-- Extension: postgis_topology

-- DROP EXTENSION postgis_topology;

 CREATE EXTENSION postgis_topology
  SCHEMA topology
  VERSION "2.2.2";
