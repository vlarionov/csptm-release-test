CREATE OR REPLACE FUNCTION gis."jf_get_mnemo_data_pkg$of_rows"(
	IN aid_account numeric,
	OUT arows refcursor,
	IN attr text DEFAULT NULL::text)
	RETURNS refcursor AS
	$BODY$
  declare

    f_ln_route_muid bigint[];

  begin

    f_ln_route_muid := jofl.jofl_pkg$extract_tarray(attr,
                                                    'F_CURRENT_ROUTE_VARIANT_MUID',
                                                    TRUE);

    OPEN arows FOR
    with t as (
      select
        current_route_variant_muid/*r.muid*/::text 				as ROUTE_MUID,
        r.number			as ROUTE_NUMBER,
        rr.muid::text				as RR_MUID,
        rr.route_round_type_muid 	as RR_TYPE_MUID,
        rr.code				as RR_CODE,
        rt.muid::text				as TRAJ_MUID,
        rt.trajectory_type_muid		as TRAJ_TYPE,
        sp2rt.order_num			as ST_ORDER_NUM,
        sp2rt.length_sector		as TRAJ_LENGTH,
        s.muid::text				as ST_MUID,
        s.name				as ST_NAME,
        sp.is_technical			as ST_IS_TECH,
        sp.muid::text				as SP_MUID,
        0 as IS_NULL_ROUND
      from gis.routes r,
        gis.route_variants rv,
        gis.route_rounds rr,
        gis.route_trajectories rt,
        gis.stop_place2route_traj sp2rt,
        gis.stop_places sp,
        gis.stops s
      where r.muid = rv.route_muid
            and rv.muid = rr.route_variant_muid
            and rr.muid = rt.route_round_muid
            and rt.muid = sp2rt.route_trajectory_muid
            and sp2rt.stop_place_muid = sp.muid
            and sp.stop_muid = s.muid
            and rv.muid = ANY(f_ln_route_muid)
            /*and rr.muid = ANY(adm.data_realm_pkg$get_available_routes_muids(aid_account))*/
            and r.agency_muid = gis.jf_agencies_pkg$cn_agency_mgt()
      union all

      select
        current_route_variant_muid/*r.muid*/::text 				as ROUTE_MUID,
        r.number			as ROUTE_NUMBER,
        rr.muid::text				as RR_MUID,
        rr.route_null_round_type_muid 	as RR_TYPE_MUID,
        rr.code				as RR_CODE,
        rt.muid::text				as TRAJ_MUID,
        rt.trajectory_type_muid		as TRAJ_TYPE,
        sp2rt.order_num			as ST_ORDER_NUM,
        sp2rt.length_sector		as TRAJ_LENGTH,
        s.muid::text				as ST_MUID,
        s.name				as ST_NAME,
        sp.is_technical			as ST_IS_TECH,
        sp.muid::text				as SP_MUID,
        1 as IS_NULL_ROUND
      from gis.routes r,
        gis.route_variants rv,
        gis.route_null_rounds rr,
        gis.route_trajectories rt,
        gis.stop_place2route_traj sp2rt,
        gis.stop_places sp,
        gis.stops s
      where r.muid = rv.route_muid
            and rv.muid = rr.route_variant_muid
            and rr.muid = rt.route_null_round_muid
            and rt.muid = sp2rt.route_trajectory_muid
            and sp2rt.stop_place_muid = sp.muid
            and sp.stop_muid = s.muid
            and rv.muid = ANY(f_ln_route_muid)
            and rr.muid in (select route_muid
                            from rts.route2gis
                            where route_id in (select adm.account_data_realm_pkg$get_routes(aid_account)))
            and r.agency_muid = gis.jf_agencies_pkg$cn_agency_mgt()
    )
    select
      t.ROUTE_MUID 		as "ROUTE_MUID",
      t.ROUTE_NUMBER		as "ROUTE_NUMBER",
      t.RR_MUID		as "RR_MUID",
      t.RR_TYPE_MUID		as "RR_TYPE_MUID",
      t.RR_CODE		as "RR_CODE",
      t.TRAJ_MUID		as "TRAJ_MUID",
      t.TRAJ_TYPE		as "TRAJ_TYPE",
      t.ST_ORDER_NUM		as "ST_ORDER_NUM",
      t.TRAJ_LENGTH		as "TRAJ_LENGTH",
      t.ST_MUID		as "ST_MUID",
      t.ST_NAME		as "ST_NAME",
      t.ST_IS_TECH		as "ST_IS_TECH",
      t.SP_MUID		as "SP_MUID",
      t.IS_NULL_ROUND as "IS_NULL_ROUND"
    from t
    order by t.TRAJ_MUID, t.ST_ORDER_NUM;

  END;
	$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;