﻿-- ========================================================================
-- Пакет загрузки данных ГИС v2
-- ========================================================================

create or replace function gis.gis_import_pkg_v2$import
   (v_message_code   in text,
    v_data           in bytea)
returns text
-- ========================================================================
-- Загрузка XML файла с данными. 
-- Файл загружается в таблицу, выполняется парсинг объектов и загрузка их таблицы данных
-- ========================================================================
as $$ declare
    v_txt_result text;
    v_result     bigint;
begin
    
    select 'OK' into v_txt_result from gis.gis_xml_message where message_code = v_message_code;
    if not found then
       insert into gis.gis_xml_message(message_code, data, status, insert_date)
       values (v_message_code,
               convert_from(v_data, 'UTF8')::xml,
               'NEW',
               clock_timestamp());
       
       v_txt_result := 'OK';
    end if;
    
    select gis.gis_import_pkg_v2$import_message_data(data) into v_result
      from gis.gis_xml_message
     where message_code = v_message_code;
    
    if found then
       update gis.gis_xml_message set status = 'READY', parse_date = clock_timestamp() where message_code = v_message_code;
    end if;
    
    return v_txt_result;
end;
$$ language plpgsql security definer;


create or replace function gis.gis_import_pkg_v2$reimport_message_data
   (v_message_code   in text)
returns bigint
-- ========================================================================
-- Импорт объектов из загруженного файла
-- Если указан тип v_object_type - выполняется загрузка данных только данного типа, иначе - всех
-- ========================================================================
as $$ declare
    v_result     bigint := 0;
begin
    select gis.gis_import_pkg_v2$import_message_data(data) into v_result
      from gis.gis_xml_message
     where message_code = v_message_code;
    
    return v_result;
end;
$$ language plpgsql security definer;


create or replace function gis.gis_import_pkg_v2$import_message_data
  (v_data         in xml)
returns bigint
-- ========================================================================
-- Импорт объектов из XML типа
-- ========================================================================
as $$ declare
   v_obj_message record;
   v_result      bigint := 0;
   v_tmp         bigint;
begin
   for v_obj_message in
   (select q.operation, q.n,
           case when ord.import_name is null then q.object_type else ord.import_name end as object_type
      from (select (xpath('/*/header/objectType/text()', v_data))[1]::text as object_type,
                   (xpath('/*/header/operation/text()',  v_data))[1]::text as operation,
                   v_data as n
           ) q
      left join gis.gis_import_order ord on ord.object_type = q.object_type
     order by ord.import_order nulls first
   )
   loop
      v_tmp := gis.gis_import_pkg_v2$import_message_data(v_obj_message.operation, v_obj_message.object_type, v_obj_message.n);
      if v_tmp is not null then
         v_result := v_result + v_tmp;
      end if;
   end loop;
   return v_result;
end;	
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_message_data
  (v_operation     in text,
   v_object_type   in text,
   v_object_data   in xml)
returns bigint
-- ========================================================================
-- Импорт объектов из XML типа
-- ========================================================================
as $$ declare
   v_result        bigint := 0;
begin
   case v_object_type
      when 'TRouteKind' then
         v_result := gis.gis_import_pkg_v2$import_route_kinds(v_operation, v_object_data);
      when 'TGraphNode' then
         v_result := gis.gis_import_pkg_v2$import_graph_nodes(v_operation, v_object_data);
      when 'TGraphSection' then
         v_result := gis.gis_import_pkg_v2$import_graph_sections(v_operation, v_object_data);
      when 'TRoute' then
         v_result := gis.gis_import_pkg_v2$import_routes(v_operation, v_object_data);
      when 'TRouteVariant' then
         v_result := gis.gis_import_pkg_v2$import_route_variants(v_operation, v_object_data);
      when 'TRoute2RouteVariant' then
         v_result := gis.gis_import_pkg_v2$import_routes2variants(v_operation, v_object_data);
      when 'TRouteRound' then
         v_result := gis.gis_import_pkg_v2$import_route_rounds(v_operation, v_object_data);
      when 'TRouteNullRound' then
         v_result := gis.gis_import_pkg_v2$import_route_null_rounds(v_operation, v_object_data);
      when 'TStop' then
         v_result := gis.gis_import_pkg_v2$import_stops(v_operation, v_object_data);
      when 'TStopPlace' then
         v_result := gis.gis_import_pkg_v2$import_stop_places(v_operation, v_object_data);
      when 'TRouteTrajectory' then
         v_result := gis.gis_import_pkg_v2$import_route_trajectories(v_operation, v_object_data);
      when 'TAgency' then
         v_result := gis.gis_import_pkg_v2$import_agencies(v_operation, v_object_data);
      when 'TPark' then
         v_result := gis.gis_import_pkg_v2$import_parks(v_operation, v_object_data);
      when 'TStopZone' then
         v_result := gis.gis_import_pkg_v2$import_stop_zones(v_operation, v_object_data);
      when 'TVehicleType' then
         v_result := gis.gis_import_pkg_v2$import_vehicle_types(v_operation, v_object_data);
      when 'TParkZone' then
         v_result := gis.gis_import_pkg_v2$import_park_zones(v_operation, v_object_data);
      when 'TTransportStaticObject' then
         v_result := gis.gis_import_pkg_v2$import_transport_static_objects(v_operation, v_object_data);
      when 'TTransportParking' then
         v_result := gis.gis_import_pkg_v2$import_transport_parkings(v_operation, v_object_data);
      when 'TTerminalPointZone' then
         v_result := gis.gis_import_pkg_v2$import_terminal_point_zones(v_operation, v_object_data);
      when 'TRouteNullRoundType' then
         v_result := gis.gis_import_pkg_v2$import_route_null_round_type(v_operation, v_object_data);
      when 'TRouteTransportKind' then
         v_result := gis.gis_import_pkg_v2$import_route_transport_kind(v_operation, v_object_data);
      when 'TRouteTransportationKind' then
         v_result := gis.gis_import_pkg_v2$import_route_transportation_kinds(v_operation, v_object_data);
      when 'TRouteState2' then
         v_result := gis.gis_import_pkg_v2$import_route_states2(v_operation, v_object_data);
      when 'TRouteVariantType' then
         v_result := gis.gis_import_pkg_v2$import_route_variant_types(v_operation, v_object_data);
      when 'TRouteRoundType' then
         v_result := gis.gis_import_pkg_v2$import_route_round_types(v_operation, v_object_data);
      when 'TStopMode' then
         v_result := gis.gis_import_pkg_v2$import_stop_mode(v_operation, v_object_data);
      when 'TStopState' then
         v_result := gis.gis_import_pkg_v2$import_stop_state(v_operation, v_object_data);
      when 'TAgencyOrganizationForm' then
         v_result := gis.gis_import_pkg_v2$import_agency_org_form(v_operation, v_object_data);
      when 'TMovementDirection' then
         v_result := gis.gis_import_pkg_v2$import_movement_direction(v_operation, v_object_data);
      when 'TTransportStaticObjectType' then
         v_result := gis.gis_import_pkg_v2$import_transport_static_object_type(v_operation, v_object_data);
      else
         null;
   end case;
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_route_transport_kind
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Видов транспорта маршрута из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/object-message/body/*/muid/text()',      v_data))[1]::text::bigint muid,
               (xpath('/object-message/body/*/name/text()',      v_data))[1]::text s_name,
               (xpath('/object-message/body/*/shortName/text()', v_data))[1]::text short_name,
               (xpath('/object-message/body/*/version/text()',   v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.ref_transport_kinds as gn (muid,
                                               name,
                                               short_name,
                                               version,
                                               sign_deleted,
                                               insert_date)
    select * from parsed
     where sign_deleted = 0 or exists (select 1 from gis.ref_transport_kinds sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (name,
                   short_name,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.name,
                   excluded.short_name,
                   excluded.version, 
                   excluded.sign_deleted,
                   excluded.insert_date)
    where gn.name         is distinct from excluded.name or
          gn.short_name   is distinct from excluded.short_name or
          gn.version      is distinct from excluded.version or
          gn.sign_deleted is distinct from excluded.sign_deleted;
   
   GET DIAGNOSTICS v_result = ROW_COUNT;
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_route_kinds
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Видов маршрута из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/object-message/body/*/muid/text()',    v_data))[1]::text::bigint muid,
               (xpath('/object-message/body/*/name/text()',    v_data))[1]::text s_name,
               (xpath('/object-message/body/*/version/text()', v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.ref_route_kinds as rrk (muid,
                                            name,
                                            version,
                                            sign_deleted,
                                            insert_date)
    select * from parsed
     where sign_deleted = 0 or exists (select 1 from gis.ref_route_kinds sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (name,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.name,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where rrk.name         is distinct from excluded.name or
          rrk.version      is distinct from excluded.version or
          rrk.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_route_transportation_kinds
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Видов сообщения из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/object-message/body/*/muid/text()',    v_data))[1]::text::bigint muid,
               (xpath('/object-message/body/*/name/text()',    v_data))[1]::text s_name,
               (xpath('/object-message/body/*/version/text()', v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.ref_route_transportation_kinds as rrk (muid,
                                                           name,
                                                           version,
                                                           sign_deleted,
                                                           insert_date)
    select * from parsed
     where sign_deleted = 0 or exists (select 1 from gis.ref_route_transportation_kinds sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (name,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.name,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where rrk.name         is distinct from excluded.name or
          rrk.version      is distinct from excluded.version or
          rrk.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_route_states2
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Статусов маршрутов ГИС из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/object-message/body/*/muid/text()',    v_data))[1]::text::bigint muid,
               (xpath('/object-message/body/*/name/text()',    v_data))[1]::text s_name,
               (xpath('/object-message/body/*/version/text()', v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.ref_route_states2 as rrk (muid,
                                              name,
                                              version,
                                              sign_deleted,
                                              insert_date)
    select * from parsed
     where sign_deleted = 0 or exists (select 1 from gis.ref_route_states2 sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (name,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.name,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where rrk.name         is distinct from excluded.name or
          rrk.version      is distinct from excluded.version or
          rrk.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_route_variant_types
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Типов вариантов маршрута из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/object-message/body/*/muid/text()',    v_data))[1]::text::bigint muid,
               (xpath('/object-message/body/*/name/text()',    v_data))[1]::text s_name,
               (xpath('/object-message/body/*/version/text()', v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.ref_route_variant_types as rrk (muid,
                                                    name,
                                                    version,
                                                    sign_deleted,
                                                    insert_date)
    select * from parsed
     where sign_deleted = 0 or exists (select 1 from gis.ref_route_variant_types sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (name,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.name,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where rrk.name         is distinct from excluded.name or
          rrk.version      is distinct from excluded.version or
          rrk.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_route_round_types
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Типы производственных рейсов из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/object-message/body/*/muid/text()',    v_data))[1]::text::bigint muid,
               (xpath('/object-message/body/*/code/text()',    v_data))[1]::text code,
               (xpath('/object-message/body/*/name/text()',    v_data))[1]::text s_name,
               (xpath('/object-message/body/*/version/text()', v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.ref_route_round_types as rrk (muid,
                                                  code,
                                                  name,
                                                  version,
                                                  sign_deleted,
                                                  insert_date)
    select * from parsed
     where sign_deleted = 0 or exists (select 1 from gis.ref_route_round_types sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (code,
                   name,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.code,
                   excluded.name,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where rrk.code         is distinct from excluded.code or
          rrk.name         is distinct from excluded.name or
          rrk.version      is distinct from excluded.version or
          rrk.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_route_null_round_type
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Типы нулевых рейсов из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/object-message/body/*/muid/text()',      v_data))[1]::text::bigint muid,
               (xpath('/object-message/body/*/code/text()',      v_data))[1]::text code,
               (xpath('/object-message/body/*/name/text()',      v_data))[1]::text s_name,
               (xpath('/object-message/body/*/shortName/text()', v_data))[1]::text short_name,
               (xpath('/object-message/body/*/version/text()',   v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.ref_route_null_round_types as rrk (muid,
                                                       code,
                                                       name,
                                                       short_name,
                                                       version,
                                                       sign_deleted,
                                                       insert_date)
    select * from parsed
     where sign_deleted = 0 or exists (select 1 from gis.ref_route_null_round_types sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (code,
                   name,
                   short_name,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.code,
                   excluded.name,
                   excluded.short_name,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where rrk.code         is distinct from excluded.code or
          rrk.name         is distinct from excluded.name or
          rrk.short_name   is distinct from excluded.short_name or
          rrk.version      is distinct from excluded.version or
          rrk.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_vehicle_types
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Типов подвижного состава из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()',            v_data))[1]::text::bigint muid,
               (xpath('/*/body/*/name/text()',            v_data))[1]::text s_name,
               (xpath('/*/body/*/shortName/text()',       v_data))[1]::text short_name,
               (xpath('/*/body/*/model/text()',           v_data))[1]::text model,
               (xpath('/*/body/*/routeTransportKind/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', v_data))[1]::text::bigint
                   route_transport_kind_muid,
               (xpath('/*/body/*/seatingCapacity/text()', v_data))[1]::text::bigint seating_capacity,
               (xpath('/*/body/*/maxCapacity/text()',     v_data))[1]::text::bigint max_capacity,
               (xpath('/*/body/*/effectiveArea/text()',   v_data))[1]::text::real effective_area,
               (xpath('/*/body/*/fullLoadWeight/text()',  v_data))[1]::text::real full_load_weight,
               (xpath('/*/body/*/dimension/text()',       v_data))[1]::text dimensions,
               (xpath('/*/body/*/hasFacilitiesForDisabled/text()', v_data))[1]::text::bigint has_facilities_for_disabled,
               (xpath('/*/body/*/version/text()',         v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() insert_date
    )
    insert into gis.ref_vehicle_types as rv (muid, 
                                             name, 
                                             short_name, 
                                             model, 
                                             route_transport_kind_muid, 
                                             seating_capacity, 
                                             max_capacity, 
                                             effective_area, 
                                             full_load_weight, 
                                             dimensions, 
                                             has_facilities_for_disabled, 
                                             version, 
                                             sign_deleted, 
                                             insert_date)
    select muid, 
           s_name, 
           short_name, 
           model, 
           route_transport_kind_muid, 
           seating_capacity, 
           max_capacity, 
           effective_area, 
           full_load_weight, 
           dimensions, 
           has_facilities_for_disabled, 
           n_version, 
           sign_deleted, 
           insert_date 
      from parsed
     where sign_deleted = 0 or exists (select 1 from gis.ref_vehicle_types sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (name, 
                   short_name, 
                   model, 
                   route_transport_kind_muid, 
                   seating_capacity, 
                   max_capacity, 
                   effective_area, 
                   full_load_weight, 
                   dimensions, 
                   has_facilities_for_disabled, 
                   version, 
                   sign_deleted, 
                   update_date) =
                  (excluded.name, 
                   excluded.short_name, 
                   excluded.model, 
                   excluded.route_transport_kind_muid, 
                   excluded.seating_capacity, 
                   excluded.max_capacity, 
                   excluded.effective_area, 
                   excluded.full_load_weight, 
                   excluded.dimensions, 
                   excluded.has_facilities_for_disabled, 
                   excluded.version, 
                   excluded.sign_deleted, 
                   excluded.insert_date)
    where rv.name is distinct from excluded.name or
          rv.short_name is distinct from excluded.short_name or
          rv.model is distinct from excluded.model or
          rv.route_transport_kind_muid is distinct from excluded.route_transport_kind_muid or
          rv.seating_capacity is distinct from excluded.seating_capacity or
          rv.max_capacity is distinct from excluded.max_capacity or
          rv.effective_area is distinct from excluded.effective_area or
          rv.full_load_weight is distinct from excluded.full_load_weight or
          rv.dimensions is distinct from excluded.dimensions or
          rv.has_facilities_for_disabled is distinct from excluded.has_facilities_for_disabled or
          rv.version is distinct from excluded.version or
          rv.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_stop_mode
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Режимов работы остановки для маршрута из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/object-message/body/*/muid/text()',      v_data))[1]::text::bigint muid,
               (xpath('/object-message/body/*/name/text()',      v_data))[1]::text s_name,
               (xpath('/object-message/body/*/shortName/text()', v_data))[1]::text short_name,
               (xpath('/object-message/body/*/version/text()',   v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.ref_stop_modes as rrk (muid,
                                           name,
                                           short_name,
                                           version,
                                           sign_deleted,
                                           insert_date)
    select * from parsed
     where sign_deleted = 0 or exists (select 1 from gis.ref_stop_modes sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (name,
                   short_name,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.name,
                   excluded.short_name,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where rrk.name         is distinct from excluded.name or
          rrk.short_name   is distinct from excluded.short_name or
          rrk.version      is distinct from excluded.version or
          rrk.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_stop_state
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Состояния остановочных пунктов из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/object-message/body/*/muid/text()',      v_data))[1]::text::bigint muid,
               (xpath('/object-message/body/*/name/text()',      v_data))[1]::text s_name,
               (xpath('/object-message/body/*/version/text()',   v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.ref_stop_states as rrk (muid,
                                            name,
                                            version,
                                            sign_deleted,
                                            insert_date)
    select * from parsed
     where sign_deleted = 0 or exists (select 1 from gis.ref_stop_states sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (name,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.name,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where rrk.name         is distinct from excluded.name or
          rrk.version      is distinct from excluded.version or
          rrk.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_agency_org_form
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Формы организации перевозчиков из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/object-message/body/*/muid/text()',    v_data))[1]::text::bigint muid,
               (xpath('/object-message/body/*/name/text()',    v_data))[1]::text s_name,
               (xpath('/object-message/body/*/code/text()',    v_data))[1]::text okpo_code,
               (xpath('/object-message/body/*/version/text()', v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.agency_organization_forms as rrk (muid,
                                                      name,
                                                      okpo_code,
                                                      version,
                                                      sign_deleted,
                                                      insert_date)
    select * from parsed
     where sign_deleted = 0 or exists (select 1 from gis.agency_organization_forms sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (name,
                   okpo_code,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.name,
                   excluded.okpo_code,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where rrk.name         is distinct from excluded.name or
          rrk.okpo_code    is distinct from excluded.okpo_code or
          rrk.version      is distinct from excluded.version or
          rrk.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_movement_direction
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Направления движения из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/object-message/body/*/muid/text()',      v_data))[1]::text::bigint muid,
               (xpath('/object-message/body/*/name/text()',      v_data))[1]::text s_name,
               (xpath('/object-message/body/*/version/text()',   v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.ref_movement_directions as rrk (muid,
                                                    name,
                                                    version,
                                                    sign_deleted,
                                                    insert_date)
    select * from parsed
     where sign_deleted = 0 or exists (select 1 from gis.ref_movement_directions sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (name,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.name,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where rrk.name         is distinct from excluded.name or
          rrk.version      is distinct from excluded.version or
          rrk.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_transport_static_object_type
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Типы стационарных объектов НГПТ из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/object-message/body/*/muid/text()',      v_data))[1]::text::bigint muid,
               (xpath('/object-message/body/*/name/text()',      v_data))[1]::text s_name,
               (xpath('/object-message/body/*/version/text()',   v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.transport_static_object_types as rrk (muid,
                                                              name,
                                                              version,
                                                              sign_deleted,
                                                              insert_date)
    select * from parsed
     where sign_deleted = 0 or exists (select 1 from gis.transport_static_object_types sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (name,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.name,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where rrk.name         is distinct from excluded.name or
          rrk.version      is distinct from excluded.version or
          rrk.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_graph_nodes
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Узлов графа из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
    v_tmp       bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()',                  v_data))[1]::text::bigint muid,
               (xpath('/*/body/*/wkt_geom/text()',              v_data))[1]::text wkt_geom,
               st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text) geom,
               (xpath('/*/body/*/version/text()',               v_data))[1]::text::bigint n_version,
               case when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.graph_nodes as gn 
          (muid, wkt_geom, geom,   version, sign_deleted, insert_date)
    select muid, wkt_geom, geom, n_version, sign_deleted, cur_time
      from parsed
--   where sign_deleted = 0 or exists (select 1 from gis.graph_nodes sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (wkt_geom,
                   geom,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.wkt_geom,
                   excluded.geom,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where gn.wkt_geom     is distinct from excluded.wkt_geom or
          gn.version      is distinct from excluded.version  or
          gn.sign_deleted is distinct from excluded.sign_deleted;
    
    get diagnostics v_result = row_count;    
    
    with parsed as (
        select (xpath('/*/body/*/muid/text()',                  v_data))[1]::text::bigint muid,
               (xpath('/*/body/*/direction/body/muid/text()',   v_data))[1]::text::bigint direction_muid,
               (xpath('/*/body/*/street/body/name/text()',      v_data))[1]::text street,
               case when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.graph_nodes_street as gn
          (muid, direction_muid, street, insert_date)
    select muid, direction_muid, street, cur_time
      from parsed
--   where sign_deleted = 0 or exists (select 1 from gis.graph_nodes sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (direction_muid,
                   street,
                   update_date) =
                  (excluded.direction_muid,
                   excluded.street,
                   excluded.insert_date)
    where gn.direction_muid is distinct from excluded.direction_muid or
          gn.street is distinct from excluded.street;
    
    get diagnostics v_tmp = row_count;
    v_result := v_result + v_tmp;
    
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_graph_sections
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Дуг графа из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()',                     v_data))[1]::text::bigint muid,
               (xpath('/*/body/*/wkt_geom/text()',                 v_data))[1]::text         wkt_geom,
               st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text)        geom,
               (xpath('/*/body/*/length/text()', v_data))[1]::text::double precision         length,
               (xpath('/*/body/*/startNode/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                   v_data))[1]::text::bigint start_node_muid,
               (xpath('/*/body/*/endNode/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                   v_data))[1]::text::bigint end_node_muid,
               (xpath('/*/body/*/hasBus/text()',                   v_data))[1]::text::bigint has_bus,
               (xpath('/*/body/*/hasTrolley/text()',               v_data))[1]::text::bigint has_trolley,
               (xpath('/*/body/*/hasTram/text()',                  v_data))[1]::text::bigint has_tram,
               (xpath('/*/body/*/hasSpeedTram/text()',             v_data))[1]::text::bigint has_speedtram,
               (xpath('/*/body/*/hasRoutes/text()',                v_data))[1]::text::bigint has_routes,
               (xpath('/*/body/*/hasNulls/text()',                 v_data))[1]::text::bigint has_nulls,
               (xpath('/*/body/*/hasBusLane/text()',               v_data))[1]::text::bigint has_bus_lane,
               (xpath('/*/body/*/version/text()',                  v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()',     v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.graph_sections as gs (muid,
                                          wkt_geom,
                                          geom,
                                          length,
                                          start_node_muid,
                                          end_node_muid,
                                          has_bus,
                                          has_trolley,
                                          has_tram,
                                          has_speedtram,
                                          has_routes,
                                          has_nulls,
                                          has_bus_lane,
                                          version,
                                          sign_deleted,
                                          insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.graph_sections sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (wkt_geom,
                   geom,
                   length,
                   start_node_muid,
                   end_node_muid,
                   has_bus,
                   has_trolley,
                   has_tram,
                   has_speedtram,
                   has_routes,
                   has_nulls,
                   has_bus_lane,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.wkt_geom,
                   excluded.geom,
                   excluded.length,
                   excluded.start_node_muid,
                   excluded.end_node_muid,
                   excluded.has_bus,
                   excluded.has_trolley,
                   excluded.has_tram,
                   excluded.has_speedtram,
                   excluded.has_routes,
                   excluded.has_nulls,
                   excluded.has_bus_lane,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where gs.wkt_geom      is distinct from excluded.wkt_geom      or
          gs.length        is distinct from excluded.length        or
          gs.start_node_muid is distinct from excluded.start_node_muid or
          gs.end_node_muid is distinct from excluded.end_node_muid or
          gs.has_bus       is distinct from excluded.has_bus       or
          gs.has_trolley   is distinct from excluded.has_trolley   or
          gs.has_tram      is distinct from excluded.has_tram      or
          gs.has_speedtram is distinct from excluded.has_speedtram or
          gs.has_routes    is distinct from excluded.has_routes    or
          gs.has_nulls     is distinct from excluded.has_nulls     or
          gs.has_bus_lane  is distinct from excluded.has_bus_lane  or
          gs.version       is distinct from excluded.version       or
          gs.sign_deleted  is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_agencies
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Перевозчиков из XML типа
-- ========================================================================
as $$ declare
    v_result     bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()',               v_data))[1]::text::bigint muid,
               (xpath('/*/body/*/organizationForm/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                             v_data))[1]::text::bigint agency_organization_form_muid,
               (xpath('/*/body/*/name/text()',               v_data))[1]::text s_name,
               (xpath('/*/body/*/legalAddress/text()',       v_data))[1]::text legal_address,
               (xpath('/*/body/*/actualAddress/text()',      v_data))[1]::text actual_address,
               (xpath('/*/body/*/contactInformation/text()', v_data))[1]::text contact_information,
               (xpath('/*/body/*/version/text()',            v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.agencies as a (muid,
                                   agency_organization_form_muid,
                                   name,
                                   legal_address,
                                   actual_address,
                                   contact_information,
                                   version,
                                   sign_deleted,
                                   insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.agencies sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (agency_organization_form_muid,
                   name,
                   legal_address,
                   actual_address,
                   contact_information,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.agency_organization_form_muid,
                   excluded.name,
                   excluded.legal_address,
                   excluded.actual_address,
                   excluded.contact_information,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where a.agency_organization_form_muid is distinct from excluded.agency_organization_form_muid or
          a.name                is distinct from excluded.name or
          a.legal_address       is distinct from excluded.legal_address or
          a.actual_address      is distinct from excluded.actual_address or
          a.contact_information is distinct from excluded.contact_information or
          a.version             is distinct from excluded.version or
          a.sign_deleted        is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_parks
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Парков из XML типа
-- ========================================================================
as $$ declare
    v_result bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()',               v_data))[1]::text::bigint muid,
               (xpath('/*/header/identSet/componentId[component[text()="ERM"]]/componentIdent/text()', 
                                                             v_data))[1]::text::bigint erm_id,
               (xpath('/*/body/*/*/agency/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                             v_data))[1]::text::bigint agency_muid, 
               (xpath('/*/body/*/number/text()',             v_data))[1]::text number,
               (xpath('/*/body/*/name/text()',               v_data))[1]::text s_name,
               (xpath('/*/body/*/shortName/text()',          v_data))[1]::text short_name,
               (xpath('/*/body/*/wkt_geom/text()',           v_data))[1]::text wkt_geom,
               st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text) geom,
               (xpath('/*/body/*/graphSection/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                             v_data))[1]::text::bigint graph_section_muid,
               (xpath('/*/body/*/graphSectionOffset/text()', v_data))[1]::text::real graph_section_offset,
               (xpath('/*/body/*/routeTransportKind/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                             v_data))[1]::text::bigint transport_kind_muid,
               (xpath('/*/body/*/addressString/text()',      v_data))[1]::text address,
               (xpath('/*/body/*/phone/text()',              v_data))[1]::text phone,
               (xpath('/*/body/*/version/text()',            v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.parks as p (muid,
                                erm_id,
                                agency_muid,
                                number,
                                name,
                                short_name,
                                wkt_geom,
                                geom,
                                graph_section_muid,
                                graph_section_offset,
                                transport_kind_muid,
                                address,
                                phone,
                                version,
                                sign_deleted,
                                insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.parks sq where sq.muid = parsed.muid)
    order by muid
    on conflict (muid)
    do update set (erm_id,
                   agency_muid,
                   number,
                   name,
                   short_name,
                   wkt_geom,
                   geom,
                   graph_section_muid,
                   graph_section_offset,
                   transport_kind_muid,
                   address,
                   phone,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.erm_id,
                   excluded.agency_muid,
                   excluded.number,
                   excluded.name,
                   excluded.short_name,
                   excluded.wkt_geom,
                   excluded.geom,
                   excluded.graph_section_muid,
                   excluded.graph_section_offset,
                   excluded.transport_kind_muid,
                   excluded.address,
                   excluded.phone,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where p.erm_id               is distinct from excluded.erm_id or
          p.agency_muid          is distinct from excluded.agency_muid or
          p.number               is distinct from excluded.number or
          p.name                 is distinct from excluded.name or
          p.short_name           is distinct from excluded.short_name or
          p.wkt_geom             is distinct from excluded.wkt_geom or
          p.graph_section_muid   is distinct from excluded.graph_section_muid or
          p.graph_section_offset is distinct from excluded.graph_section_offset or
          p.transport_kind_muid  is distinct from excluded.transport_kind_muid or
          p.address              is distinct from excluded.address or
          p.phone                is distinct from excluded.phone or
          p.version              is distinct from excluded.version or
          p.sign_deleted         is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_park_zones
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Зон парков из XML типа
-- ========================================================================
as $$ declare
    v_result           bigint := 0;
    v_tmp              bigint;
    v_park_zone_muid   bigint;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()',     v_data))[1]::text::bigint muid,
               (xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text wkt_geom,
               st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text) geom,
               (xpath('/*/body/*/version/text()',  v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.park_zones as p (muid,
                                     wkt_geom,
                                     geom,
                                     version,
                                     sign_deleted,
                                     insert_date)
    select * from parsed order by muid
    on conflict (muid)
    do update set (wkt_geom,
                   geom,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.wkt_geom,
                   excluded.geom,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where p.wkt_geom is distinct from excluded.wkt_geom or
          p.version is distinct from excluded.version or
          p.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    
    v_park_zone_muid := (select (xpath('/*/body/*/muid/text()', v_data))[1]::text::bigint);
    update gis.parks2park_zones set sign_deleted = 1 where park_zone_muid = v_park_zone_muid;
    
    with parsed as (
        select (xpath('/*/body/*/park/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                  v_data))[1]::text::bigint park_muid,
               (xpath('/*/body/*/muid/text()',    v_data))[1]::text::bigint park_zone_muid,
               (xpath('/*/body/*/version/text()', v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.parks2park_zones as p (park_muid,
                                           park_zone_muid,
                                           version,
                                           sign_deleted,
                                           insert_date)
    select * from parsed
    where sign_deleted = 0 
       or exists (select 1 from gis.parks2park_zones sq
                   where sq.park_muid = parsed.park_muid
                     and sq.park_zone_muid = parsed.park_zone_muid)
    on conflict (park_muid, park_zone_muid)
    do update set (version,
                   sign_deleted,
                   update_date) =
                  (excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where p.version is distinct from excluded.version or
          p.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_tmp = ROW_COUNT;
    v_result := v_result + v_tmp;
    
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_transport_static_objects
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Стационарных объектов из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()',     v_data))[1]::text::bigint muid,
               (xpath('/*/body/*/type/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                   v_data))[1]::text::bigint transport_static_object_type_muid,
               (xpath('/*/body/*/name/text()',     v_data))[1]::text s_name,
               (xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text wkt_geom,
               st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text) geom,
               (xpath('/*/body/*/version/text()',  v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.transport_static_objects as gn (muid, 
                                                    transport_static_object_type_muid, 
                                                    name, 
                                                    wkt_geom, 
                                                    geom, 
                                                    version, 
                                                    sign_deleted, 
                                                    insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.transport_static_objects sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (transport_static_object_type_muid, 
                   name, 
                   wkt_geom, 
                   geom, 
                   version, 
                   sign_deleted,
                   update_date) =
                  (excluded.transport_static_object_type_muid, 
                   excluded.name, 
                   excluded.wkt_geom, 
                   excluded.geom, 
                   excluded.version, 
                   excluded.sign_deleted,
                   excluded.insert_date)
    where gn.transport_static_object_type_muid is distinct from excluded.transport_static_object_type_muid or
          gn.name         is distinct from excluded.name or
          gn.wkt_geom     is distinct from excluded.wkt_geom or
          gn.geom         is distinct from excluded.geom or
          gn.version      is distinct from excluded.version or
          gn.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_transport_parkings
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Отстойных площадок из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()',          v_data))[1]::text::bigint muid,
               (xpath('/*/body/*/park/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                        v_data))[1]::text::bigint park_muid,
               (xpath('/*/body/*/wkt_geom/text()',      v_data))[1]::text wkt_geom,
               st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text) geom,
               (xpath('/*/body/*/name/text()',          v_data))[1]::text s_name,
               (xpath('/*/body/*/addressString/text()', v_data))[1]::text address,
               (xpath('/*/body/*/phone/text()',         v_data))[1]::text phone,
               (xpath('/*/body/*/graphSection/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                        v_data))[1]::text::bigint graph_section_muid,
               (xpath('/*/body/*/graphSectionOffset/text()', v_data))[1]::text::real graph_section_offset,
               (xpath('/*/body/*/square/text()',        v_data))[1]::text::real square,
               (xpath('/*/body/*/capacity/text()',      v_data))[1]::text::bigint capacity,
               (xpath('/*/body/*/hasBus/text()',        v_data))[1]::text::bigint has_bus, 
               (xpath('/*/body/*/hasTrolley/text()',    v_data))[1]::text::bigint has_trolley, 
               (xpath('/*/body/*/hasTram/text()',       v_data))[1]::text::bigint has_tram, 
               (xpath('/*/body/*/hasSpeedTram/text()',  v_data))[1]::text::bigint has_speedtram, 
               (xpath('/*/body/*/version/text()',       v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.transport_parkings as gn (muid, 
                                              park_muid, 
                                              wkt_geom, 
                                              geom, 
                                              name, 
                                              address, 
                                              phone, 
                                              graph_section_muid, 
                                              graph_section_offset, 
                                              square, 
                                              capacity, 
                                              has_bus, 
                                              has_trolley, 
                                              has_tram, 
                                              has_speedtram, 
                                              version, 
                                              sign_deleted, 
                                              insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.transport_parkings sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (park_muid, 
                   wkt_geom, 
                   geom, 
                   name, 
                   address, 
                   phone, 
                   graph_section_muid, 
                   graph_section_offset, 
                   square, 
                   capacity, 
                   has_bus, 
                   has_trolley, 
                   has_tram, 
                   has_speedtram, 
                   version, 
                   sign_deleted,
                   update_date) = 
                  (excluded.park_muid, 
                   excluded.wkt_geom, 
                   excluded.geom, 
                   excluded.name, 
                   excluded.address, 
                   excluded.phone, 
                   excluded.graph_section_muid, 
                   excluded.graph_section_offset, 
                   excluded.square, 
                   excluded.capacity, 
                   excluded.has_bus, 
                   excluded.has_trolley, 
                   excluded.has_tram, 
                   excluded.has_speedtram, 
                   excluded.version, 
                   excluded.sign_deleted,
                   excluded.insert_date)
    where gn.park_muid     is distinct from excluded.park_muid or
          gn.wkt_geom      is distinct from excluded.wkt_geom  or
          gn.geom          is distinct from excluded.geom      or 
          gn.name          is distinct from excluded.name      or 
          gn.address       is distinct from excluded.address   or 
          gn.phone         is distinct from excluded.phone     or 
          gn.graph_section_muid   is distinct from excluded.graph_section_muid   or 
          gn.graph_section_offset is distinct from excluded.graph_section_offset or 
          gn.square        is distinct from excluded.square    or 
          gn.capacity      is distinct from excluded.capacity  or 
          gn.has_bus       is distinct from excluded.has_bus   or 
          gn.has_trolley   is distinct from excluded.has_trolley   or 
          gn.has_tram      is distinct from excluded.has_tram  or 
          gn.has_speedtram is distinct from excluded.has_speedtram or 
          gn.version       is distinct from excluded.version   or
          gn.sign_deleted  is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_stops
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Остановок из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()',                 v_data))[1]::text::bigint muid,
               (xpath('/*/body/*/name/text()',                 v_data))[1]::text s_name,
               (xpath('/*/body/*/nameInEnglish/text()',        v_data))[1]::text name_in_english,
               (xpath('/*/body/*/nameForTermnalPoint/text()',  v_data))[1]::text name_for_terminal_point,
               (xpath('/*/body/*/direction/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint direction_muid,
               (xpath('/*/body/*/district/body/name/text()',   v_data))[1]::text district,
               (xpath('/*/body/*/region/body/name/text()',     v_data))[1]::text region,
               (xpath('/*/body/*/street/body/name/text()',     v_data))[1]::text street,
               (xpath('/*/body/*/version/text()',              v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.stops as s (muid,
                                name,
                                name_in_english,
                                name_for_terminal_point,
                                direction_muid,
                                district,
                                region,
                                street,
                                version,
                                sign_deleted,
                                insert_date)
    select * from parsed
    where (sign_deleted = 0 or exists (select 1 from gis.stops sq where sq.muid = parsed.muid))
--    and direction_muid is not null
    on conflict (muid)
    do update set (name,
                   name_in_english,
                   name_for_terminal_point,
                   direction_muid,
                   district,
                   region,
                   street,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.name,
                   excluded.name_in_english,
                   excluded.name_for_terminal_point,
                   excluded.direction_muid,
                   excluded.district,
                   excluded.region,
                   excluded.street,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where s.name            is distinct from excluded.name or
          s.name_in_english is distinct from excluded.name_in_english or
          s.name_for_terminal_point is distinct from excluded.name_for_terminal_point or
          s.direction_muid  is distinct from excluded.direction_muid or
          s.district        is distinct from excluded.district or
          s.region          is distinct from excluded.region or
          s.street          is distinct from excluded.street or
          s.version         is distinct from excluded.version or
          s.sign_deleted    is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_stop_places
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Остановочных пунктов из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()',                     v_data))[1]::text::bigint muid,
               (xpath('/*/header/identSet/componentId[component[text()="ERM"]]/componentIdent/text()', 
                                                                   v_data))[1]::text::bigint erm_id,
               (xpath('/*/body/*/stop/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                                   v_data))[1]::text::bigint stop_muid,
               (xpath('/*/body/*/state/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                                   v_data))[1]::text::bigint stop_state_muid,
               (xpath('/*/body/*/mode/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                                   v_data))[1]::text::bigint stop_mode_muid,
               (xpath('/*/body/*/wkt_geom/text()',                 v_data))[1]::text wkt_geom,
               st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text) geom,
               (xpath('/*/body/*/suffix/text()',                   v_data))[1]::text suffix,
               (xpath('/*/body/*/graphSection/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                                   v_data))[1]::text::bigint graph_section_muid,
               (xpath('/*/body/*/graphSectionOffset/text()',       v_data))[1]::text::real graph_section_offset,
               (xpath('/*/body/*/graphTramSection/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                                   v_data))[1]::text::bigint graphTramSectionOffset,
               (xpath('/*/body/*/graphTramSectionOffset/text()',   v_data))[1]::text::real graph_tram_section_offset,
               (xpath('/*/body/*/isTechnical/text()',              v_data))[1]::text::bigint is_technical,
               (xpath('/*/body/*/hasFacilitiesForDisabled/text()', v_data))[1]::text::bigint has_facilities_for_disabled,
               (xpath('/*/body/*/hasBus/text()',                   v_data))[1]::text::bigint has_bus,
               (xpath('/*/body/*/hasTrolley/text()',               v_data))[1]::text::bigint has_trolley,
               (xpath('/*/body/*/hasTram/text()',                  v_data))[1]::text::bigint has_tram,
               (xpath('/*/body/*/hasSpeedTram/text()',             v_data))[1]::text::bigint has_speedtram,
               (xpath('/*/body/*/startDate/text()',                v_data))[1]::text::date start_date,
               (xpath('/*/body/*/changeDate/text()',               v_data))[1]::text::date change_date,
               (xpath('/*/body/*/endDate/text()',                  v_data))[1]::text::date end_date,
               (xpath('/*/body/*/buildingDistance/text()',         v_data))[1]::text::real building_distance,
               (xpath('/*/body/*/buildingAddressString/text()',    v_data))[1]::text building_address,
               (xpath('/*/body/*/comment/text()',                  v_data))[1]::text s_comment,
               (xpath('/*/body/*/version/text()',                  v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()',     v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.stop_places as sp (muid,
                                erm_id,
                                stop_muid,
                                stop_state_muid,
                                stop_mode_muid,
                                wkt_geom,
                                geom,
                                suffix,
                                graph_section_muid,
                                graph_section_offset,
                                graph_tram_section_muid,
                                graph_tram_section_offset,
                                is_technical,
                                has_facilities_for_disabled,
                                has_bus,
                                has_trolley,
                                has_tram,
                                has_speedtram,
                                start_date,
                                change_date,
                                end_date,
                                building_distance,
                                building_address,
                                comment,
                                version,
                                sign_deleted,
                                insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.stop_places sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (erm_id,
                   stop_muid,
                   stop_state_muid,
                   stop_mode_muid,
                   wkt_geom,
                   geom,
                   suffix,
                   graph_section_muid,
                   graph_section_offset,
                   graph_tram_section_muid,
                   graph_tram_section_offset,
                   is_technical,
                   has_facilities_for_disabled,
                   has_bus,
                   has_trolley,
                   has_tram,
                   has_speedtram,
                   start_date,
                   change_date,
                   end_date,
                   building_distance,
                   building_address,
                   comment,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.erm_id,
                   excluded.stop_muid,
                   excluded.stop_state_muid,
                   excluded.stop_mode_muid,
                   excluded.wkt_geom,
                   excluded.geom,
                   excluded.suffix,
                   excluded.graph_section_muid,
                   excluded.graph_section_offset,
                   excluded.graph_tram_section_muid,
                   excluded.graph_tram_section_offset,
                   excluded.is_technical,
                   excluded.has_facilities_for_disabled,
                   excluded.has_bus,
                   excluded.has_trolley,
                   excluded.has_tram,
                   excluded.has_speedtram,
                   excluded.start_date,
                   excluded.change_date,
                   excluded.end_date,
                   excluded.building_distance,
                   excluded.building_address,
                   excluded.comment,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where sp.erm_id            is distinct from excluded.erm_id or
          sp.stop_muid         is distinct from excluded.stop_muid or
          sp.stop_state_muid   is distinct from excluded.stop_state_muid or
          sp.stop_mode_muid    is distinct from excluded.stop_mode_muid or
          sp.wkt_geom          is distinct from excluded.wkt_geom or
          sp.suffix            is distinct from excluded.suffix or
          sp.graph_section_muid          is distinct from excluded.graph_section_muid or
          sp.graph_section_offset        is distinct from excluded.graph_section_offset or
          sp.graph_tram_section_muid     is distinct from excluded.graph_tram_section_muid or
          sp.graph_tram_section_offset   is distinct from excluded.graph_tram_section_offset or
          sp.is_technical      is distinct from excluded.is_technical or
          sp.has_facilities_for_disabled is distinct from excluded.has_facilities_for_disabled or
          sp.has_bus           is distinct from excluded.has_bus or
          sp.has_trolley       is distinct from excluded.has_trolley or
          sp.has_tram          is distinct from excluded.has_tram or
          sp.has_speedtram     is distinct from excluded.has_speedtram or
          sp.start_date        is distinct from excluded.start_date or
          sp.change_date       is distinct from excluded.change_date or
          sp.end_date          is distinct from excluded.end_date or
          sp.building_distance is distinct from excluded.building_distance or
          sp.building_address  is distinct from excluded.building_address or
          sp.comment           is distinct from excluded.comment or
          sp.version           is distinct from excluded.version or
          sp.sign_deleted      is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_stop_zones
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Зон посадки-высадки из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()',                 v_data))[1]::text::bigint muid,
               (xpath('/*/header/identSet/componentId[component[text()="ERM"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint erm_id,
               (xpath('/*/body/*/stopPlace/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint stop_place_muid,
               (xpath('/*/body/*/wkt_geom/text()',             v_data))[1]::text wkt_geom,
               st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text) geom,
               (xpath('/*/body/*/version/text()',              v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.stop_zones as sz (muid,
                                      erm_id,
                                      stop_place_muid,
                                      wkt_geom,
                                      geom,
                                      version,
                                      sign_deleted,
                                      insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.stop_zones sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (erm_id,
                   stop_place_muid,
                   wkt_geom,
                   geom,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.erm_id,
                   excluded.stop_place_muid,
                   excluded.wkt_geom,
                   excluded.geom,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where sz.erm_id          is distinct from excluded.erm_id or
          sz.stop_place_muid is distinct from excluded.stop_place_muid or
          sz.wkt_geom        is distinct from excluded.wkt_geom or
          sz.version         is distinct from excluded.version or
          sz.sign_deleted    is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_terminal_point_zones
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Зона конечных пунктов из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()',                 v_data))[1]::text::bigint muid,
               (xpath('/*/body/*/stopPlace/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint stop_place_muid,
               (xpath('/*/body/*/route/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint route_muid,
               (xpath('/*/body/*/name/text()',                 v_data))[1]::text s_name,
               (xpath('/*/body/*/wkt_geom/text()',             v_data))[1]::text wkt_geom,
               st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text) geom,
               (xpath('/*/body/*/version/text()',              v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.terminal_point_zones as gn (muid,
                                                stop_place_muid,
                                                route_muid,
                                                name,
                                                wkt_geom,
                                                geom, 
                                                version, 
                                                sign_deleted, 
                                                insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.terminal_point_zones sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (stop_place_muid,
                   route_muid,
                   name,
                   wkt_geom,
                   geom, 
                   version, 
                   sign_deleted,
                   update_date) = 
                  (excluded.stop_place_muid,
                   excluded.route_muid,
                   excluded.name,
                   excluded.wkt_geom,
                   excluded.geom, 
                   excluded.version, 
                   excluded.sign_deleted,
                   excluded.insert_date)
    where gn.stop_place_muid is distinct from excluded.stop_place_muid or
          gn.route_muid      is distinct from excluded.route_muid or
          gn.name            is distinct from excluded.name       or
          gn.wkt_geom        is distinct from excluded.wkt_geom   or
          gn.geom            is distinct from excluded.geom       or 
          gn.version         is distinct from excluded.version    or
          gn.sign_deleted    is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_routes
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Маршрутов из XML типа
-- ========================================================================
as $$ declare
    v_result       bigint := 0;
    v_tmp          bigint;
    v_route_muid   bigint;
begin
    -- Статусы маршрутов
    with xml_source as (
        select (xpath('/*/body/*/state/body/muid/text()',        v_data))[1]::text::bigint muid,
               (xpath('/*/body/*/state/body/name/text()',        v_data))[1]::text s_name,
               (xpath('/*/body/*/state/body/version/text()',     v_data))[1]::text::bigint n_version,
               (xpath('/*/body/*/state/body/signDeleted/text()', v_data))[1]::text::bigint  sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.ref_route_states as tt (muid,
                                            name,
                                            version,
                                            sign_deleted,
                                            insert_date)
    select * from xml_source
    where sign_deleted = 0
    on conflict (muid)
    do update set (name,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.name,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.update_date)
    where tt.name          is distinct from excluded.name or
          tt.version       is distinct from excluded.version or
          tt.sign_deleted  is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    
    -- Маршруты
    with parsed as (
        select (xpath('/*/body/*/muid/text()',                 v_data))[1]::text::bigint muid,
               (xpath('/*/header/identSet/componentId[component[text()="ERM"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint erm_id,
               (xpath('/*/body/*/agency/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint agency_muid,
               (xpath('/*/body/*/number/text()',               v_data))[1]::text number,
               (xpath('/*/body/*/registrationNumber/text()',   v_data))[1]::text registration_number,
               (xpath('/*/body/*/kind/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint route_kind_muid,
               (xpath('/*/body/*/transportKind/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint transport_kind_muid,
               (xpath('/*/body/*/transportationKind/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint route_transportation_kind_muid,
               (xpath('/*/body/*/state/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint route_state_muid,
               (xpath('/*/body/*/openDate/text()',             v_data))[1]::text::date open_date,
               (xpath('/*/body/*/closeDate/text()',            v_data))[1]::text::date close_date,
               (xpath('/*/body/*/comment/text()',              v_data))[1]::text s_comment,
               (xpath('/*/body/*/version/text()',              v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.routes as r (muid,
                                 erm_id,
                                 agency_muid,
                                 number,
                                 registration_number,
                                 route_kind_muid,
                                 transport_kind_muid,
                                 route_transportation_kind_muid,
                                 route_state_muid,
                                 open_date,
                                 close_date,
                                 comment,
                                 version,
                                 sign_deleted,
                                 insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.routes sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (erm_id,
                   agency_muid,
                   number,
                   registration_number,
                   route_kind_muid,
                   transport_kind_muid,
                   route_transportation_kind_muid,
                   route_state_muid,
                   open_date,
                   close_date,
                   comment,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.erm_id,
                   excluded.agency_muid,
                   excluded.number,
                   excluded.registration_number,
                   excluded.route_kind_muid,
                   excluded.transport_kind_muid,
                   excluded.route_transportation_kind_muid,
                   excluded.route_state_muid,
                   excluded.open_date,
                   excluded.close_date,
                   excluded.comment,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where r.erm_id              is distinct from excluded.erm_id or
          r.agency_muid         is distinct from excluded.agency_muid or
          r.number              is distinct from excluded.number or
          r.registration_number is distinct from excluded.registration_number or
          r.route_kind_muid     is distinct from excluded.route_kind_muid or
          r.transport_kind_muid is distinct from excluded.transport_kind_muid or
          r.route_transportation_kind_muid is distinct from excluded.route_transportation_kind_muid or
          r.route_state_muid    is distinct from excluded.route_state_muid or
          r.open_date           is distinct from excluded.open_date or
          r.close_date          is distinct from excluded.close_date or
          r.comment             is distinct from excluded.comment or
          r.version             is distinct from excluded.version or
          r.sign_deleted        is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_tmp = ROW_COUNT;
    v_result := v_result + v_tmp;
    
    -- Маршруты Парков
    v_route_muid := (select (xpath('/*/body/*/muid/text()', v_data))[1]::text::bigint);
    update gis.parks2routes set sign_deleted = 1 where route_muid = v_route_muid;
    
    with parsed as (
        select (xpath('/park/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                 p_data))[1]::text::bigint park_muid,
               (xpath('/*/body/*/muid/text()',                   v_data))[1]::text::bigint route_muid,
               (xpath('/*/body/*/version/text()',                v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()',   v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
          from (select unnest(xpath('/*/body/*/parks/park', v_data)) as p_data) t
    )
    insert into gis.parks2routes as p  (park_muid,
                                        route_muid,
                                        version,
                                        sign_deleted,
                                        insert_date)
    select * from parsed
    where sign_deleted = 0
       or exists (select 1 from gis.parks2routes sq
                   where sq.park_muid = parsed.park_muid
                     and sq.route_muid = parsed.route_muid)
    on conflict (park_muid, route_muid)
    do update set (version,
                   sign_deleted,
                   update_date) =
                  (excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where p.version is distinct from excluded.version or
          p.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_tmp = ROW_COUNT;
    v_result := v_result + v_tmp;
    
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_route_variants
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Вариантов Маршрута из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()',                 v_data))[1]::text::bigint muid,
               (xpath('/*/header/identSet/componentId[component[text()="ERM"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint erm_id,
               (xpath('/*/body/*/type/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint route_variant_type_muid,
               (xpath('/*/body/*/route/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint route_muid,
               (xpath('/*/body/*/startDate/text()',            v_data))[1]::text::date start_date,
               (xpath('/*/body/*/endDate/text()',              v_data))[1]::text::date end_date,
               (xpath('/*/body/*/comment/text()',              v_data))[1]::text s_comment,
               (xpath('/*/body/*/version/text()',              v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.route_variants as rv (muid,
                                          erm_id,
                                          route_variant_type_muid,
                                          route_muid,
                                          start_date,
                                          end_date,
                                          comment,
                                          version,
                                          sign_deleted,
                                          insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.route_variants sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (erm_id,
                   route_variant_type_muid,
                   route_muid,
                   start_date,
                   end_date,
                   comment,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.erm_id,
                   excluded.route_variant_type_muid,
                   excluded.route_muid,
                   excluded.start_date,
                   excluded.end_date,
                   excluded.comment,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where rv.erm_id       is distinct from excluded.erm_id or
          rv.route_variant_type_muid is distinct from excluded.route_variant_type_muid or
          rv.route_muid   is distinct from excluded.route_muid or
          rv.start_date   is distinct from excluded.start_date or
          rv.end_date     is distinct from excluded.end_date or
          rv.comment      is distinct from excluded.comment or
          rv.version      is distinct from excluded.version or
          rv.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_routes2variants
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт связки Маршрута и текущего Варианта Маршрута из XML типа
-- ========================================================================
as $$ declare
    v_result bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()',                 v_data))[1]::text::bigint muid,
               (xpath('/*/body/*/currentRouteVariant/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint current_route_variant_muid,
               case
                   when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    update gis.routes as r
       set current_route_variant_muid = parsed.current_route_variant_muid,
           update_date = parsed.cur_time
      from parsed
     where parsed.muid = r.muid
       and parsed.current_route_variant_muid is distinct from r.current_route_variant_muid;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_route_rounds
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Производственных рейсов из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()',                 v_data))[1]::text::bigint muid,
               (xpath('/*/header/identSet/componentId[component[text()="ERM"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint erm_id,
               (xpath('/*/body/*/type/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint route_round_type_muid,
               (xpath('/*/body/*/routeVariant/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint route_variant_muid,
               (xpath('/*/body/*/code/text()', v_data))[1]::text code,
               (xpath('/*/body/*/stopPlaceA/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint stop_place_a_muid,
               (xpath('/*/body/*/stopPlaceB/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint stop_place_b_muid,
               (xpath('/*/body/*/terminalPointZoneA/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint terminal_zone_a_muid,
               (xpath('/*/body/*/terminalPointZoneB/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint terminal_zone_b_muid,
               (xpath('/*/body/*/terminalStationA/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint terminal_station_a_muid,
               (xpath('/*/body/*/terminalStationB/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint terminal_station_b_muid,
               (xpath('/*/body/*/terminalStationC/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint terminal_station_c_muid,
               (xpath('/*/body/*/averageLength/text()',        v_data))[1]::text::real average_length,
               (xpath('/*/body/*/averageLengthFixed/text()',   v_data))[1]::text::real average_length_fixed,
               (xpath('/*/body/*/specification1/text()',       v_data))[1]::text specification1,
               (xpath('/*/body/*/specification2/text()',       v_data))[1]::text specification2,
               (xpath('/*/body/*/comment/text()',              v_data))[1]::text s_comment,
               (xpath('/*/body/*/version/text()',              v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.route_rounds as rr (muid,
                                        erm_id,
                                        route_round_type_muid,
                                        route_variant_muid,
                                        code,
                                        stop_place_a_muid,
                                        stop_place_b_muid,
                                        terminal_zone_a_muid,
                                        terminal_zone_b_muid,
                                        terminal_station_a_muid,
                                        terminal_station_b_muid,
                                        terminal_station_c_muid,
                                        average_length,
                                        average_length_fixed,
                                        specification1,
                                        specification2,
                                        comment,
                                        version,
                                        sign_deleted,
                                        insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.route_rounds sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (erm_id,
                   route_round_type_muid,
                   route_variant_muid,
                   code,
                   stop_place_a_muid,
                   stop_place_b_muid,
                   terminal_zone_a_muid,
                   terminal_zone_b_muid,
                   terminal_station_a_muid,
                   terminal_station_b_muid,
                   terminal_station_c_muid,
                   average_length,
                   average_length_fixed,
                   specification1,
                   specification2,
                   comment,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.erm_id,
                   excluded.route_round_type_muid,
                   excluded.route_variant_muid,
                   excluded.code,
                   excluded.stop_place_a_muid,
                   excluded.stop_place_b_muid,
                   excluded.terminal_zone_a_muid,
                   excluded.terminal_zone_b_muid,
                   excluded.terminal_station_a_muid,
                   excluded.terminal_station_b_muid,
                   excluded.terminal_station_c_muid,
                   excluded.average_length,
                   excluded.average_length_fixed,
                   excluded.specification1,
                   excluded.specification2,
                   excluded.comment,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where rr.erm_id             is distinct from excluded.erm_id or
          rr.route_round_type_muid   is distinct from excluded.route_round_type_muid or
          rr.route_variant_muid is distinct from excluded.route_variant_muid or
          rr.code is distinct from excluded.code or
          rr.stop_place_a_muid  is distinct from excluded.stop_place_a_muid or
          rr.stop_place_b_muid  is distinct from excluded.stop_place_b_muid or
          rr.terminal_zone_a_muid    is distinct from excluded.terminal_zone_a_muid or
          rr.terminal_zone_b_muid    is distinct from excluded.terminal_zone_b_muid or
          rr.terminal_station_a_muid is distinct from excluded.terminal_station_a_muid or
          rr.terminal_station_b_muid is distinct from excluded.terminal_station_b_muid or
          rr.terminal_station_c_muid is distinct from excluded.terminal_station_c_muid or
          rr.average_length     is distinct from excluded.average_length or
          rr.average_length_fixed is distinct from excluded.average_length_fixed or
          rr.specification1     is distinct from excluded.specification1 or
          rr.specification2     is distinct from excluded.specification2 or
          rr.comment            is distinct from excluded.comment or
          rr.version            is distinct from excluded.version or
          rr.sign_deleted       is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_route_null_rounds
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Нулевых рейсов из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()',                 v_data))[1]::text::bigint muid,
               (xpath('/*/body/*/type/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint route_null_round_type_muid,
               (xpath('/*/body/*/routeVariant/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint route_variant_muid,
               (xpath('/*/body/*/code/text()',                 v_data))[1]::text code,
               (xpath('/*/body/*/park1/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint park_1_muid,
               (xpath('/*/body/*/stopPlace1/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint stop_place_1_muid,
               (xpath('/*/body/*/park2/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint park_2_muid,
               (xpath('/*/body/*/stopPlace2/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint stop_place_2_muid,
               (xpath('/*/body/*/park3/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint park_3_muid,
               (xpath('/*/body/*/stopPlace3/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint stop_place_3_muid,
               (xpath('/*/body/*/comment/text()',              v_data))[1]::text s_comment,
               (xpath('/*/body/*/version/text()',              v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.route_null_rounds as rnr (muid,
                                              route_null_round_type_muid,
                                              route_variant_muid,
                                              code,
                                              park_1_muid,
                                              stop_place_1_muid,
                                              park_2_muid,
                                              stop_place_2_muid,
                                              park_3_muid,
                                              stop_place_3_muid,
                                              comment,
                                              version,
                                              sign_deleted,
                                              insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.route_null_rounds sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (route_null_round_type_muid,
                   route_variant_muid,
                   code,
                   park_1_muid,
                   stop_place_1_muid,
                   park_2_muid,
                   stop_place_2_muid,
                   park_3_muid,
                   stop_place_3_muid,
                   comment,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.route_null_round_type_muid,
                   excluded.route_variant_muid,
                   excluded.code,
                   excluded.park_1_muid,
                   excluded.stop_place_1_muid,
                   excluded.park_2_muid,
                   excluded.stop_place_2_muid,
                   excluded.park_3_muid,
                   excluded.stop_place_3_muid,
                   excluded.comment,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where rnr.route_null_round_type_muid is distinct from excluded.route_null_round_type_muid or
          rnr.route_variant_muid is distinct from excluded.route_variant_muid or
          rnr.code               is distinct from excluded.code or
          rnr.park_1_muid        is distinct from excluded.park_1_muid or
          rnr.stop_place_1_muid  is distinct from excluded.stop_place_1_muid or
          rnr.park_2_muid        is distinct from excluded.park_2_muid or
          rnr.stop_place_2_muid  is distinct from excluded.stop_place_2_muid or
          rnr.park_3_muid        is distinct from excluded.park_3_muid or
          rnr.stop_place_3_muid  is distinct from excluded.stop_place_3_muid or
          rnr.comment            is distinct from excluded.comment or
          rnr.version            is distinct from excluded.version or
          rnr.sign_deleted       is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_route_trajectories
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Траекторий маршрутов из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
    v_route_trajectory_muid bigint;
begin
    v_route_trajectory_muid := (select (xpath('/*/body/*/muid/text()', v_data))[1]::text::bigint);
    
    with xml_source as (
        select (xpath('/*/body/*/trajectoryType/body/muid/text()',    v_data))[1]::text::bigint muid,
               (xpath('/*/body/*/trajectoryType/body/name/text()',    v_data))[1]::text s_name,
               (xpath('/*/body/*/trajectoryType/body/version/text()', v_data))[1]::text::bigint n_version,
               (xpath('/*/body/*/signDeleted/text()',        v_data))[1]::text::bigint  sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.ref_route_trajectory_types as tt (muid,
                                                      name,
                                                      version,
                                                      sign_deleted,
                                                      insert_date)
    select * from xml_source
    where sign_deleted = 0
    on conflict (muid)
    do update set (name,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.name,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.update_date)
    where tt.name          is distinct from excluded.name or
          tt.version       is distinct from excluded.version or
          tt.sign_deleted  is distinct from excluded.sign_deleted;
    
    
    with parsed as (
        select v_route_trajectory_muid muid,
               (xpath('/*/header/identSet/componentId[component[text()="ERM"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint erm_id,
               (xpath('/*/body/*/trajectoryType/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint trajectory_type_muid,
               (xpath('/*/body/*/routeRound/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint route_round_muid,
               (xpath('/*/body/*/routeNullRound/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint route_null_round_muid,
               (xpath('/*/body/*/code/text()',                 v_data))[1]::text code,
               (xpath('/*/body/*/wkt_geom/text()',             v_data))[1]::text wkt_geom,
               st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text) geom,
               (xpath('/*/body/*/length/text()',               v_data))[1]::text::real length,
               (xpath('/*/body/*/fixedLength/text()',          v_data))[1]::text::real length_fixed,
               (xpath('/*/body/*/specification/text()',        v_data))[1]::text specification,
               (xpath('/*/body/*/version/text()',              v_data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.route_trajectories as rt (muid,
                                              erm_id,
                                              trajectory_type_muid,
                                              route_round_muid,
                                              route_null_round_muid,
                                              code,
                                              wkt_geom,
                                              geom,
                                              length,
                                              length_fixed,
                                              specification,
                                              version,
                                              sign_deleted,
                                              insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.route_trajectories sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (erm_id,
                   trajectory_type_muid,
                   route_round_muid,
                   route_null_round_muid,
                   code,
                   wkt_geom,
                   geom,
                   length,
                   length_fixed,
                   specification,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.erm_id,
                   excluded.trajectory_type_muid,
                   excluded.route_round_muid,
                   excluded.route_null_round_muid,
                   excluded.code,
                   excluded.wkt_geom,
                   excluded.geom,
                   excluded.length,
                   excluded.length_fixed,
                   excluded.specification,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where rt.erm_id        is distinct from excluded.erm_id or
          rt.trajectory_type_muid  is distinct from excluded.trajectory_type_muid or
          rt.route_round_muid      is distinct from excluded.route_round_muid or
          rt.route_null_round_muid is distinct from excluded.route_null_round_muid or
          rt.code          is distinct from excluded.code or
          rt.wkt_geom      is distinct from excluded.wkt_geom or
          rt.length        is distinct from excluded.length or
          rt.length_fixed  is distinct from excluded.length_fixed or
          rt.specification is distinct from excluded.specification or
          rt.version       is distinct from excluded.version or
          rt.sign_deleted  is distinct from excluded.sign_deleted;
    
    get diagnostics v_result = row_count;
    
    if exists(select 1 from gis.route_trajectories where muid = v_route_trajectory_muid) then
       
       delete from gis.graph_sec2route_traj
        where muid in
              (select muid from gis.graph_sec2route_traj
                where route_trajectory_muid = v_route_trajectory_muid
                  and muid not in
                      (select (xpath('/*/body/muid/text()', gs_data))[1]::text::bigint muid
                         from (select unnest(xpath('/*/body/*/graphSections/linkedGraphSection', v_data)) as gs_data) t
                      )
              );
       
       --delete from gis.graph_sec2route_traj  where route_trajectory_muid = v_route_trajectory_muid;
       --update gis.graph_sec2route_traj set sign_deleted = 1 where route_trajectory_muid = v_route_trajectory_muid;
       
       with parsed as (
           select (xpath('/*/body/muid/text()',                    gs_data))[1]::text::bigint muid, v_route_trajectory_muid,
                  (xpath('/*/body/graphSection/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                                   gs_data))[1]::text::bigint graph_section_muid,
                  (xpath('/*/body/index/text()',                   gs_data))[1]::text::bigint order_num,
                  (xpath('/*/body/graphSectionStartOffset/text()', gs_data))[1]::text::real graph_section_start_offset,
                  (xpath('/*/body/graphSectionEndOffset/text()',   gs_data))[1]::text::real graph_section_end_offset,
                  (xpath('/*/body/version/text()',                 gs_data))[1]::text::bigint n_version,
                  case when (xpath('/*/body/signDeleted/text()',   gs_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
                  clock_timestamp() cur_time
             from (select unnest(xpath('/*/body/*/graphSections/linkedGraphSection', v_data)) as gs_data) t
       )
       insert into gis.graph_sec2route_traj as gs2rt (muid,
                                                      route_trajectory_muid,
                                                      graph_section_muid,
                                                      order_num,
                                                      graph_section_start_offset,
                                                      graph_section_end_offset,
                                                      version,
                                                      sign_deleted,
                                                      insert_date)
       select * from parsed 
       where sign_deleted = 0 or exists (select 1 from gis.graph_sec2route_traj sq where sq.muid = parsed.muid)
       order by order_num
       on conflict (muid)
       do update set (route_trajectory_muid,
                      graph_section_muid,
                      order_num,
                      graph_section_start_offset,
                      graph_section_end_offset,
                      version,
                      sign_deleted,
                      update_date) =
                     (excluded.route_trajectory_muid,
                      excluded.graph_section_muid,
                      excluded.order_num,
                      excluded.graph_section_start_offset,
                      excluded.graph_section_end_offset,
                      excluded.version,
                      excluded.sign_deleted,
                      excluded.insert_date)
           where gs2rt.route_trajectory_muid is distinct from excluded.route_trajectory_muid or
                 gs2rt.graph_section_muid    is distinct from excluded.graph_section_muid or
                 gs2rt.order_num             is distinct from excluded.order_num or
                 gs2rt.graph_section_start_offset is distinct from excluded.graph_section_start_offset or
                 gs2rt.graph_section_end_offset   is distinct from excluded.graph_section_end_offset or
                 gs2rt.version               is distinct from excluded.version or
                 gs2rt.sign_deleted          is distinct from excluded.sign_deleted;
       
       -- ==========================================================================
       delete from gis.stop_place2route_traj
        where muid in
              (select muid from gis.stop_place2route_traj
                where route_trajectory_muid = v_route_trajectory_muid
                  and muid not in
                      (select (xpath('/*/body/muid/text()', gs_data))[1]::text::bigint muid
                         from (select unnest(xpath('/*/body/*/stopPlaces/linkedStopPlace', v_data)) as gs_data) t
                      )
              );
       
       --delete from gis.stop_place2route_traj where route_trajectory_muid = v_route_trajectory_muid;
       --update gis.stop_place2route_traj set sign_deleted = 1 where route_trajectory_muid = v_route_trajectory_muid;
       
       with parsed as (
           select (xpath('/*/body/muid/text()',                  sp_data))[1]::text::bigint muid, v_route_trajectory_muid,
                  (xpath('/*/body/stopPlace/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                                 sp_data))[1]::text::bigint stop_place_muid,
                  (xpath('/*/body/index/text()',                 sp_data))[1]::text::bigint order_num,
                  (xpath('/*/body/mode/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                                 sp_data))[1]::text::bigint stop_mode_muid,
                  (xpath('/*/body/type/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                                 sp_data))[1]::text::bigint stop_type_muid,
                  (xpath('/*/body/lengthSector/text()',          sp_data))[1]::text::real length_sector,
                  (xpath('/*/body/comment/text()',               sp_data))[1]::text s_comment,
                  (xpath('/*/body/version/text()',               sp_data))[1]::text::bigint n_version,
                  case when (xpath('/*/body/signDeleted/text()', sp_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
                  clock_timestamp() cur_time
             from (select unnest(xpath('/*/body/*/stopPlaces/linkedStopPlace', v_data)) as sp_data) t
       )
       insert into gis.stop_place2route_traj as sp2rt (muid,
                                                       route_trajectory_muid,
                                                       stop_place_muid,
                                                       order_num,
                                                       stop_mode_muid,
                                                       stop_type_muid,
                                                       length_sector,
                                                       comment,
                                                       version,
                                                       sign_deleted,
                                                       insert_date)
       select * from parsed 
       where sign_deleted = 0 or exists (select 1 from gis.stop_place2route_traj sq where sq.muid = parsed.muid)
       order by order_num
       on conflict (muid)
       do update set (route_trajectory_muid,
                      stop_place_muid,
                      order_num,
                      stop_mode_muid,
                      stop_type_muid,
                      length_sector,
                      comment,
                      version,
                      sign_deleted,
                      update_date) =
                     (excluded.route_trajectory_muid,
                      excluded.stop_place_muid,
                      excluded.order_num,
                      excluded.stop_mode_muid,
                      excluded.stop_type_muid,
                      excluded.length_sector,
                      excluded.comment,
                      excluded.version,
                      excluded.sign_deleted,
                      excluded.insert_date)
           where sp2rt.route_trajectory_muid is distinct from excluded.route_trajectory_muid or
                 sp2rt.stop_place_muid is distinct from excluded.stop_place_muid or
                 sp2rt.order_num       is distinct from excluded.order_num or
                 sp2rt.stop_mode_muid  is distinct from excluded.stop_mode_muid or
                 sp2rt.stop_type_muid  is distinct from excluded.stop_type_muid or
                 sp2rt.length_sector   is distinct from excluded.length_sector or
                 sp2rt.comment         is distinct from excluded.comment or
                 sp2rt.version         is distinct from excluded.version or
                 sp2rt.sign_deleted    is distinct from excluded.sign_deleted;
    end if;
    
    return v_result;
end;
$$ language plpgsql;
