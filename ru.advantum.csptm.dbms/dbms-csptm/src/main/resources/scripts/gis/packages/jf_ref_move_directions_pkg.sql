CREATE OR REPLACE FUNCTION gis.jf_ref_move_directions_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
begin 
 open p_rows for 
      select 
        NAME		  AS "NAME", 
        SIGN_DELETED  AS "SIGN_DELETED", 
        MUID		  AS "MUID", 
        VERSION		  AS "VERSION", 
        INSERT_DATE	  AS "INSERT_DATE", 
        UPDATE_DATE	  AS "UPDATE_DATE"
      from gis.ref_movement_directions; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;