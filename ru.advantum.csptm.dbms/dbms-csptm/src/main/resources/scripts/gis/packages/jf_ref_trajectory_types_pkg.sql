CREATE OR REPLACE FUNCTION gis.jf_ref_trajectory_types_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
begin 
 open p_rows for 
      select 
        NAME		AS "NAME", 
        MUID		AS "MUID", 
        INSERT_DATE AS "INSERT_DATE", 
        UPDATE_DATE AS "UPDATE_DATE", 
        VERSION		AS "VERSION", 
        SIGN_DELETED AS "SIGN_DELETED"
      from gis.ref_route_trajectory_types; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;