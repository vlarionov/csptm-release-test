CREATE OR REPLACE FUNCTION gis."jf_agencies_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
 f_rtk_MUID numeric := -1;
begin 
 f_rtk_MUID := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_rtk_MUID', TRUE), -1);
if f_rtk_MUID = -1 then
 open p_rows for 
      select 
        MUID		AS "MUID", 
        AGENCY_ORGANIZATION_FORM_MUID AS "AGENCY_ORGANIZATION_FORM_MUID", 
        NAME		AS "NAME", 
        LEGAL_ADDRESS	AS "LEGAL_ADDRESS", 
        ACTUAL_ADDRESS	AS "ACTUAL_ADDRESS", 
        CONTACT_INFORMATION	AS "CONTACT_INFORMATION", 
        VERSION		AS "VERSION", 
        SIGN_DELETED	AS "SIGN_DELETED", 
        INSERT_DATE	AS "INSERT_DATE", 
        UPDATE_DATE	AS "UPDATE_DATE"
      from gis.agencies; 
 else
  open p_rows for 
  select distinct
        ag.MUID		AS "MUID", 
        ag.AGENCY_ORGANIZATION_FORM_MUID AS "AGENCY_ORGANIZATION_FORM_MUID", 
        ag.NAME		AS "NAME", 
        ag.LEGAL_ADDRESS	AS "LEGAL_ADDRESS", 
        ag.ACTUAL_ADDRESS	AS "ACTUAL_ADDRESS", 
        ag.CONTACT_INFORMATION	AS "CONTACT_INFORMATION", 
        ag.VERSION		AS "VERSION", 
        ag.SIGN_DELETED	AS "SIGN_DELETED", 
        ag.INSERT_DATE	AS "INSERT_DATE", 
        ag.UPDATE_DATE	AS "UPDATE_DATE"
      from gis.agencies ag join gis.routes r on (r.agency_muid = ag.muid)
      		join gis.ref_transport_kinds rtk on (rtk.muid = r.transport_kind_muid)
      where rtk.muid = f_rtk_MUID
            and r.agency_muid = gis.jf_agencies_pkg$cn_agency_mgt();
 end if;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


CREATE OR REPLACE FUNCTION gis.jf_agencies_pkg$cn_agency_mgt()
  RETURNS int AS
  'select 1::int;'
LANGUAGE SQL IMMUTABLE;