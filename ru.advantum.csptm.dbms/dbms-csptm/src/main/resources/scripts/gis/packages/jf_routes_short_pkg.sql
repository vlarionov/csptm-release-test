﻿-- Function: gis."jf_routes_short_pkg$of_rows"(numeric, text)

-- DROP FUNCTION gis."jf_routes_short_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION gis."jf_routes_short_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
 ln_stops_muid gis.stops.muid%type := jofl.jofl_pkg$extract_number(p_attr, 'STOP_MUID', true);

begin 
 open p_rows for 
      select distinct
	rtk.name	AS "NAME",
	--r.number	AS "NUMBER",
	'[' || json_build_object('CAPTION', r.number, 'JUMP', json_build_object('METHOD', 'GIS.ROUTES', 'ATTR', '{}')) || ']' AS "NUMBER",
	st_a.name	AS "START_POINT",
	st_b.name	AS "END_POINT",
	rr.code		AS "RR_CODE",
	r.muid::text		AS "R_MUID",
  case rt.trajectory_type_muid when 1 then 'Прямое' else 'Обратное' end as "DIRECTION_NAME"
      from gis.routes r
        join gis.route_variants rv on r.muid = rv.route_muid  and rv.muid = r.current_route_variant_muid
        join gis.route_rounds rr on rv.muid = rr.route_variant_muid
        join gis.route_trajectories rt on  rr.muid = rt.route_round_muid
        join gis.stop_place2route_traj sp2rt on rt.muid = sp2rt.route_trajectory_muid
        join gis.stop_places sp on sp2rt.stop_place_muid = sp.muid
        join gis.stops st on  sp.stop_muid = st.muid
        join gis.ref_transport_kinds rtk on rtk.muid = r.transport_kind_muid
        join gis.stop_places sp_a on rr.stop_place_a_muid = sp_a.muid
        join gis.stop_places sp_b on rr.stop_place_b_muid = sp_b.muid
        join gis.stops st_a on sp_a.stop_muid = st_a.muid
        join gis.stops st_b on sp_b.stop_muid = st_b.muid
     where  st.muid = ln_stops_muid
         and sp2rt.stop_place_muid = sp.muid
         and rv.sign_deleted =0
         and rr.sign_deleted =0
         and rt.sign_deleted =0
         and sp2rt.sign_deleted =0
         and r.agency_muid = gis.jf_agencies_pkg$cn_agency_mgt()
     order by  "NUMBER"
  ;

end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION gis."jf_routes_short_pkg$of_rows"(numeric, text)
  OWNER TO adv;
