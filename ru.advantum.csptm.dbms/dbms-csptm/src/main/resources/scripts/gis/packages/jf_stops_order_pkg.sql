﻿-- Function: gis."jf_stops_order_pkg$of_rows"(numeric, text)

-- DROP FUNCTION gis."jf_stops_order_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION gis."jf_stops_order_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$
declare 
  ln_rr_muid numeric := jofl.jofl_pkg$extract_number(p_attr,
                                                        'MUID',
                                                        TRUE);
  f_r_MUID numeric := -1;	
  f_dir_MUID numeric := -1; 
  f_s_ST_ORDER_NUM numeric := -1; 
begin 
  f_r_MUID  := coalesce(jofl.jofl_pkg$extract_number(p_attr,
                                                        'f_r_MUID',
                                                        TRUE), -1);
  f_dir_MUID := coalesce(jofl.jofl_pkg$extract_number(p_attr,
                                                        'f_dir_MUID',
                                                        TRUE), -1);	
  f_s_ST_ORDER_NUM := coalesce(jofl.jofl_pkg$extract_number(p_attr,
                                                        'f_s_ST_ORDER_NUM',
                                                        TRUE), -1);	                                                        
  if f_r_MUID = -1 or f_dir_MUID = -1 then													
	open p_rows for 
	select 		rt.muid::text			as "TRAJ_MUID",
			rt.trajectory_type_muid		as "TRAJ_TYPE",
			sp2rt.order_num			as "ST_ORDER_NUM",
			sp2rt.length_sector		as "TRAJ_LENGTH",
			s.muid::text			as "ST_MUID",
			s.name				as "ST_NAME",
			sp.is_technical			as "ST_IS_TECH",
			/*ST_X(sp.geom)			as "LON",
			ST_Y(sp.geom)			as "LAT",
			'https://yandex.ru/maps/213/moscow/?ll=37.414713%2C55.713797&z=11&l=trf%2Ctrfe' as "LINK",*/
			rrtt.name			as "TRAJ_TYPE_NAME",
			'[' || json_build_object('CAPTION', s.name, 'JUMP', json_build_object('METHOD', 'GIS.STOPS', 'ATTR', '{}')) || ']' as "ST_NAME_LINK"
			
		from 		
				gis.route_trajectories rt,
				gis.stop_place2route_traj sp2rt,
				gis.ref_route_trajectory_types rrtt,
				gis.stop_places sp,
				gis.stops s
		where rt.muid = sp2rt.route_trajectory_muid
		  and rt.trajectory_type_muid = rrtt.muid
		  and sp2rt.stop_place_muid = sp.muid
		  and sp.stop_muid = s.muid
		  and rt.route_round_muid::numeric = ln_rr_muid
	order by rt.muid, sp2rt.order_num; 
  else
    open p_rows for
	select 	distinct
		rt.muid::text			as "TRAJ_MUID",
		rt.trajectory_type_muid		as "TRAJ_TYPE",
		sp2rt.order_num			as "ST_ORDER_NUM",
		sp2rt.length_sector		as "TRAJ_LENGTH",
		s.muid::text			as "ST_MUID",
		s.name				as "ST_NAME",
		sp.is_technical			as "ST_IS_TECH",
		/*ST_X(sp.geom)			as "LON",
		ST_Y(sp.geom)			as "LAT",
		'https://yandex.ru/maps/213/moscow/?ll=37.414713%2C55.713797&z=11&l=trf%2Ctrfe' as "LINK",*/
		rrtt.name			as "TRAJ_TYPE_NAME",
		'[' || json_build_object('CAPTION', s.name, 'JUMP', json_build_object('METHOD', 'GIS.STOPS', 'ATTR', '{}')) || ']' as "ST_NAME_LINK"
		
	from 	gis.route_trajectories rt join gis.stop_place2route_traj sp2rt on rt.muid = sp2rt.route_trajectory_muid
		join gis.ref_route_trajectory_types rrtt on rt.trajectory_type_muid = rrtt.muid
		join gis.stop_places sp on sp2rt.stop_place_muid = sp.muid
		join gis.stops s on sp.stop_muid = s.muid
        join tt.round rnd on rnd.route_trajectory_muid = rt.muid
        join tt.timetable_entry tte on tte.timetable_entry_id = rnd.timetable_entry_id
		join gis.route_variants rv on rv.muid = tte.route_variant_muid
		join gis.routes r on r.current_route_variant_muid = rv.muid and r.muid = rv.route_muid
	where r.muid::numeric = f_r_MUID
      and rnd.direction_id::numeric = f_dir_MUID   
      and (sp2rt.order_num::numeric > f_s_ST_ORDER_NUM or f_s_ST_ORDER_NUM = -1)
order by  sp2rt.order_num;
  end if;  
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION gis."jf_stops_order_pkg$of_rows"(numeric, text)
  OWNER TO adv;
