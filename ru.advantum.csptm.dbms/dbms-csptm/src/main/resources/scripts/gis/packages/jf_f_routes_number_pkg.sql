CREATE OR REPLACE FUNCTION gis."jf_f_routes_number_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
   l_rf_db_method      TEXT := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE), 'none');
   l_arr_rf_db_method_free_right text array;
   l_depo_ids BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_PARKS_depo_id', true);
   l_pars_routes_muids BIGINT[] := array(
       select route_muid from gis.parks2routes p2r
           JOIN core.depo d ON d.park_muid = p2r.park_muid AND d.depo_id = ANY(l_depo_ids)
   );
   l_close_date TIMESTAMP = null;
begin
  l_arr_rf_db_method_free_right:= '{"snsr.skah_rep_01", ' ||
                                   '"snsr.skah_rep_02", ' ||
                                   '"GIS.ROUTE_VARIANTS" }' ;

  -- нужны только действующие маршруты
  IF (l_rf_db_method = 'askp.asmpp_full_data'
      OR l_rf_db_method = 'askp.askp_full_data'
      OR l_rf_db_method = 'askp.asmpp_askp_full') THEN
    l_close_date := now();
  END IF;
   
   open p_rows for
   select r.NUMBER AS "NUMBER",
          r.muid::text as route_muid,
          (select trt.short_name from core.tr_type trt where trt.transport_kind_muid = r.transport_kind_muid) as tr_type_short_name
     from gis.routes r
   where (l_rf_db_method is null
          or (l_rf_db_method != any (l_arr_rf_db_method_free_right)
              and r.muid in (select route_muid
                             from rts.route2gis
                             where route_id in (select adm.account_data_realm_pkg$get_routes_by_all(p_id_account)))))
         and r.agency_muid = gis.jf_agencies_pkg$cn_agency_mgt()
         and (array_length(l_pars_routes_muids, 1) is null or r.muid = any (l_pars_routes_muids))
         and (l_close_date is null or r.close_date is null)
    order by 1;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;