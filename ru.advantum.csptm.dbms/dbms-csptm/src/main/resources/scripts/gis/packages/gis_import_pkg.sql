﻿-- ========================================================================
-- Пакет загрузки данных ГИС
-- ========================================================================

create or replace function gis.gis_import_pkg$import(afilename in text,
                                                     adata     in bytea)
returns text
-- ========================================================================
-- Загрузка XML файла с данными. 
-- Файл загружается в таблицу, выполняется парсинг объектов и загрузка их 
-- таблицы данных
-- ========================================================================
as $$ declare
    v_txt_result text;
    v_result     bigint;
begin
    
    --select 'EXISTS' into v_txt_result from gis.gis_xml_files where filename = afilename;
    select 'OK' into v_txt_result from gis.gis_xml_files where filename = afilename;
    
    if not found then
        
        insert into gis.gis_xml_files(filename, data, status, insert_date)
         values (afilename,
                replace(convert_from(adata, 'UTF8'),
                        '<object-messages xmlns="http://mappl.ru/2015/XMLSchema/GISMGT">',
                        '<object-messages>')::xml,
                'NEW',
                clock_timestamp());
        
        select gis.gis_import_pkg$import_file_data(data, null::text) into v_result
          from gis.gis_xml_files
         where filename = afilename;
        
        update gis.gis_xml_files
           set status = 'READY',
               parse_date = clock_timestamp()
        where filename = afilename;
       
        v_txt_result := 'OK';
       
    end if;
    
    return v_txt_result;
end;
$$ language plpgsql security definer;


create or replace function gis.gis_import_pkg$reimport_file(v_filename    in text,
                                                            v_object_type in text)
returns bigint
-- ========================================================================
-- Импорт объектов из загруженного файла
-- Если указан тип v_object_type - выполняется загрузка данных только данного
-- типа, иначе - всех
-- ========================================================================
as $$ declare
    obj_file     record;
    v_result     bigint := 0;
    v_tmp        bigint;
begin
    
    for obj_file in (select filename from gis.gis_xml_files
                      where (v_filename is null or filename = v_filename)) 
    loop
        
        select gis.gis_import_pkg$import_file_data(data, v_object_type) into v_tmp
          from gis.gis_xml_files
         where filename = obj_file.filename;
        
        if v_tmp is not null then
           v_result := v_result + v_tmp;
        end if;
        
    end loop;
    
    return v_result;
end;
$$ language plpgsql security definer;


create or replace function gis.gis_import_pkg$import_file_data(data          in xml, 
                                                               v_object_type in text)
returns bigint 
-- ========================================================================
-- Импорт объектов из XML типа
-- Если указан тип v_object_type - выполняется загрузка данных только данного
-- типа, иначе - всех
-- ========================================================================
as $$ declare
    obj_message record;
    v_result    bigint := 0;
    v_tmp       bigint;
begin
    for obj_message in (
        select q.operation, q.n,
               case when ord.import_name is null then q.object_type else ord.import_name end as object_type
          from (select (xpath('/*/header/objectType/text()', n))[1]::text object_type,
                       (xpath('/*/header/operation/text()', n))[1]::text operation,
                       n
                      from (select unnest(xpath('/object-messages/object-message', data)) n ) t
               ) q
          left join gis.gis_import_order ord on ord.object_type = q.object_type
         where (v_object_type is null or q.object_type = v_object_type)
         order by ord.import_order nulls first
    ) loop
        case obj_message.object_type
            when 'TRouteKind' then
                v_tmp := gis.gis_import_pkg$import_route_kinds(obj_message.operation, obj_message.n);
            when 'TGraphNode' then
                v_tmp := gis.gis_import_pkg$import_graph_nodes(obj_message.operation, obj_message.n);
            when 'TGraphSection' then
                perform gis.gis_import_pkg$import_graph_sections(obj_message.operation, obj_message.n);
            when 'TRoute' then
                v_tmp := gis.gis_import_pkg$import_routes(obj_message.operation, obj_message.n);
            when 'TRouteVariant' then
                v_tmp := gis.gis_import_pkg$import_route_variants(obj_message.operation, obj_message.n);
            when 'TRoute2RouteVariant' then
                v_tmp := gis.gis_import_pkg$import_routes2variants(obj_message.operation, obj_message.n);
            when 'TRouteRound' then
                v_tmp := gis.gis_import_pkg$import_route_rounds(obj_message.operation, obj_message.n);
            when 'TRouteNullRound' then
                v_tmp := gis.gis_import_pkg$import_route_null_rounds(obj_message.operation, obj_message.n);
            when 'TStop' then
                v_tmp := gis.gis_import_pkg$import_stops(obj_message.operation, obj_message.n);
            when 'TStopPlace' then
                v_tmp := gis.gis_import_pkg$import_stop_places(obj_message.operation, obj_message.n);
            when 'TRouteTrajectory' then
                v_tmp := gis.gis_import_pkg$import_route_trajectories(obj_message.operation, obj_message.n);
            when 'TAgency' then
                v_tmp := gis.gis_import_pkg$import_agencies(obj_message.operation, obj_message.n);
            when 'TPark' then
                v_tmp := gis.gis_import_pkg$import_parks(obj_message.operation, obj_message.n);
            when 'TStopZone' then
                v_tmp := gis.gis_import_pkg$import_stop_zones(obj_message.operation, obj_message.n);
            when 'TVehicleType' then
                v_tmp := gis.gis_import_pkg$import_vehicle_types(obj_message.operation, obj_message.n);
            when 'TParkZone' then
                v_tmp := gis.gis_import_pkg$import_park_zones(obj_message.operation, obj_message.n);
            when 'TTransportStaticObject' then
                v_tmp := gis.gis_import_pkg$import_transport_static_objects(obj_message.operation, obj_message.n);
            when 'TTransportParking' then
                v_tmp := gis.gis_import_pkg$import_transport_parkings(obj_message.operation, obj_message.n);
            when 'TTerminalPointZone' then
                v_tmp := gis.gis_import_pkg$import_terminal_point_zones(obj_message.operation, obj_message.n);
            else
               null;
        end case;
        
        if v_tmp is not null then
           v_result := v_result + v_tmp;
        end if;
        
    end loop;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg$import_graph_nodes(operation in text,
                                                                 data      in xml)
returns bigint
-- ========================================================================
-- Импорт Узлов графа из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
    v_tmp       bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()', data))[1]::text::bigint muid,
               (xpath('/*/body/*/wkt_geom/text()', data))[1]::text wkt_geom,
               st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', data))[1]::text) geom,
               (xpath('/*/body/*/version/text()', data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', data))[1]::text::bigint = 1 or operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.graph_nodes as gn (muid,
                                       wkt_geom,
                                       geom,
                                       version,
                                       sign_deleted,
                                       insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.graph_nodes sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (wkt_geom,
                   geom,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.wkt_geom,
                   excluded.geom,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where gn.wkt_geom is distinct from excluded.wkt_geom or
          gn.version is distinct from excluded.version or
          gn.sign_deleted is distinct from excluded.sign_deleted;
   
    GET DIAGNOSTICS v_result = ROW_COUNT;    
    
    
    with parsed as (
        select (xpath('/*/body/*/muid/text()', data))[1]::text::bigint muid,
               (xpath('/*/body/*/direction/body/muid/text()', data))[1]::text::bigint direction_muid,
               (xpath('/*/body/*/street/body/name/text()', data))[1]::text street,
               clock_timestamp() cur_time
    )
    insert into gis.graph_nodes_street as gn (muid,
                                              direction_muid, 
                                              street,
                                              insert_date)
    select * from parsed
    on conflict (muid)
    do update set (direction_muid, 
                   street,
                   update_date) =
                  (excluded.direction_muid,
                   excluded.street,
                   excluded.insert_date)
    where gn.direction_muid is distinct from excluded.direction_muid or
          gn.street is distinct from excluded.street;
    
    GET DIAGNOSTICS v_tmp = ROW_COUNT;
    v_result := v_result + v_tmp;
    
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg$import_route_kinds(operation in text,
                                                                 data      in xml)
returns bigint
-- ========================================================================
-- Импорт Узлов графа из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()', data))[1]::text::bigint muid,
               (xpath('/*/body/*/name/text()', data))[1]::text wkt_geom,
               (xpath('/*/body/*/version/text()', data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', data))[1]::text::bigint = 1 or operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.ref_route_kinds as rrk (muid,
                                            name,
                                            version,
                                            sign_deleted,
                                            insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.ref_route_kinds sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (name,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.name,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where rrk.name is distinct from excluded.name or
          rrk.version is distinct from excluded.version or
          rrk.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg$import_graph_sections(operation in text,
                                                                    data      in xml)
returns bigint
-- ========================================================================
-- Импорт Дуг графа из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()', data))[1]::text::bigint muid,
               (xpath('/*/body/*/wkt_geom/text()', data))[1]::text wkt_geom,
               st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', data))[1]::text) geom,
               (xpath('/*/body/*/length/text()', data))[1]::text::double precision length,
               (xpath('/*/body/*/startNode/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                 data))[1]::text::bigint start_node_muid,
               (xpath('/*/body/*/endNode/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                 data))[1]::text::bigint end_node_muid,
               (xpath('/*/body/*/hasBus/text()', data))[1]::text::bigint has_bus,
               (xpath('/*/body/*/hasTrolley/text()', data))[1]::text::bigint has_trolley,
               (xpath('/*/body/*/hasTram/text()', data))[1]::text::bigint has_tram,
               (xpath('/*/body/*/hasSpeedTram/text()', data))[1]::text::bigint has_speedtram,
               (xpath('/*/body/*/hasRoutes/text()', data))[1]::text::bigint has_routes,
               (xpath('/*/body/*/hasNulls/text()', data))[1]::text::bigint has_nulls,
               (xpath('/*/body/*/hasBusLane/text()', data))[1]::text::bigint has_bus_lane,
               (xpath('/*/body/*/version/text()', data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', data))[1]::text::bigint = 1 or operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.graph_sections as gs (muid,
                                          wkt_geom,
                                          geom,
                                          length,
                                          start_node_muid,
                                          end_node_muid,
                                          has_bus,
                                          has_trolley,
                                          has_tram,
                                          has_speedtram,
                                          has_routes,
                                          has_nulls,
                                          has_bus_lane,
                                          version,
                                          sign_deleted,
                                          insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.graph_sections sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (wkt_geom,
                   geom,
                   length,
                   start_node_muid,
                   end_node_muid,
                   has_bus,
                   has_trolley,
                   has_tram,
                   has_speedtram,
                   has_routes,
                   has_nulls,
                   has_bus_lane,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.wkt_geom,
                   excluded.geom,
                   excluded.length,
                   excluded.start_node_muid,
                   excluded.end_node_muid,
                   excluded.has_bus,
                   excluded.has_trolley,
                   excluded.has_tram,
                   excluded.has_speedtram,
                   excluded.has_routes,
                   excluded.has_nulls,
                   excluded.has_bus_lane,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where gs.wkt_geom      is distinct from excluded.wkt_geom      or
          gs.length        is distinct from excluded.length        or
          gs.start_node_muid is distinct from excluded.start_node_muid or
          gs.end_node_muid is distinct from excluded.end_node_muid or
          gs.has_bus       is distinct from excluded.has_bus       or
          gs.has_trolley   is distinct from excluded.has_trolley   or
          gs.has_tram      is distinct from excluded.has_tram      or
          gs.has_speedtram is distinct from excluded.has_speedtram or
          gs.has_routes    is distinct from excluded.has_routes    or
          gs.has_nulls     is distinct from excluded.has_nulls     or
          gs.has_bus_lane  is distinct from excluded.has_bus_lane  or
          gs.version       is distinct from excluded.version       or
          gs.sign_deleted  is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg$import_agencies(operation in text,
                                                              data      in xml)
returns bigint
-- ========================================================================
-- Импорт Перевозчиков из XML типа
-- ========================================================================
as $$ declare
    v_result     bigint := 0;
    lagency_muid bigint;
begin
    lagency_muid := (select (xpath('/*/body/*/muid/text()', data))[1]::text::bigint);
    
    with parsed as (
        select lagency_muid muid,
               (xpath('/*/body/*/organizationForm/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   agency_organization_form_muid,
               (xpath('/*/body/*/name/text()', data))[1]::text s_name,
               (xpath('/*/body/*/legalAddress/text()', data))[1]::text legal_address,
               (xpath('/*/body/*/actualAddress/text()', data))[1]::text actual_address,
               (xpath('/*/body/*/contactInformation/text()', data))[1]::text contact_information,
               (xpath('/*/body/*/version/text()', data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', data))[1]::text::bigint = 1 or operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.agencies as a (muid,
                                   agency_organization_form_muid,
                                   name,
                                   legal_address,
                                   actual_address,
                                   contact_information,
                                   version,
                                   sign_deleted,
                                   insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.agencies sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (agency_organization_form_muid,
                   name,
                   legal_address,
                   actual_address,
                   contact_information,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.agency_organization_form_muid,
                   excluded.name,
                   excluded.legal_address,
                   excluded.actual_address,
                   excluded.contact_information,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where a.agency_organization_form_muid is distinct from excluded.agency_organization_form_muid or
          a.name is distinct from excluded.name or
          a.legal_address is distinct from excluded.legal_address or
          a.actual_address is distinct from excluded.actual_address or
          a.contact_information is distinct from excluded.contact_information or
          a.version is distinct from excluded.version or
          a.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg$import_parks(operation in text,
                                                           data      in xml)
returns bigint
-- ========================================================================
-- Импорт Парков из XML типа
-- ========================================================================
as $$ declare
    v_result bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()', data))[1]::text::bigint muid,
               (xpath('/*/header/identSet/componentId[component[text()="ERM"]]/componentIdent/text()', data))[1]::text::bigint
                   erm_id,
               (xpath('/*/body/*/*/agency/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   agency_muid, 
               (xpath('/*/body/*/number/text()', data))[1]::text number,
               (xpath('/*/body/*/name/text()', data))[1]::text s_name,
               (xpath('/*/body/*/shortName/text()', data))[1]::text short_name,
               (xpath('/*/body/*/wkt_geom/text()', data))[1]::text wkt_geom,
               st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', data))[1]::text) geom,
               (xpath('/*/body/*/graphSection/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   graph_section_muid,
               (xpath('/*/body/*/graphSectionOffset/text()', data))[1]::text::real graph_section_offset,
               (xpath('/*/body/*/addressString/text()', data))[1]::text address,
               (xpath('/*/body/*/phone/text()', data))[1]::text phone,
               (xpath('/*/body/*/version/text()', data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', data))[1]::text::bigint = 1 or operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.parks as p (muid,
                                erm_id,
                                agency_muid,
                                number,
                                name,
                                short_name,
                                wkt_geom,
                                geom,
                                graph_section_muid,
                                graph_section_offset,
                                address,
                                phone,
                                version,
                                sign_deleted,
                                insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.parks sq where sq.muid = parsed.muid)
    order by muid
    on conflict (muid)
    do update set (erm_id,
                   agency_muid,
                   number,
                   name,
                   short_name,
                   wkt_geom,
                   geom,
                   graph_section_muid,
                   graph_section_offset,
                   address,
                   phone,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.erm_id,
                   excluded.agency_muid,
                   excluded.number,
                   excluded.name,
                   excluded.short_name,
                   excluded.wkt_geom,
                   excluded.geom,
                   excluded.graph_section_muid,
                   excluded.graph_section_offset,
                   excluded.address,
                   excluded.phone,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where p.erm_id is distinct from excluded.erm_id or
          p.agency_muid is distinct from excluded.agency_muid or
          p.number is distinct from excluded.number or
          p.name is distinct from excluded.name or
          p.short_name is distinct from excluded.short_name or
          p.wkt_geom is distinct from excluded.wkt_geom or
          p.graph_section_muid is distinct from excluded.graph_section_muid or
          p.graph_section_offset is distinct from excluded.graph_section_offset or
          p.address is distinct from excluded.address or
          p.phone is distinct from excluded.phone or
          p.version is distinct from excluded.version or
          p.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg$import_routes(operation in text,
                                                            data      in xml)
returns bigint
-- ========================================================================
-- Импорт Маршрутов из XML типа
-- ========================================================================
as $$ declare
    v_result bigint := 0;
    v_tmp    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()', data))[1]::text::bigint muid,
               (xpath('/*/header/identSet/componentId[component[text()="ERM"]]/componentIdent/text()', data))[1]::text::bigint
                   erm_id,
               (xpath('/*/body/*/agency/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   agency_muid,
               (xpath('/*/body/*/number/text()', data))[1]::text number,
               (xpath('/*/body/*/registrationNumber/text()', data))[1]::text registration_number,
               (xpath('/*/body/*/kind/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   route_kind_muid,
               (xpath('/*/body/*/transportKind/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   transport_kind_muid,
               (xpath('/*/body/*/transportationKind/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   route_transportation_kind_muid,
               (xpath('/*/body/*/state/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   route_state_muid,
               (xpath('/*/body/*/openDate/text()', data))[1]::text::date open_date,
               (xpath('/*/body/*/closeDate/text()', data))[1]::text::date close_date,
               (xpath('/*/body/*/comment/text()', data))[1]::text s_comment,
               (xpath('/*/body/*/version/text()', data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', data))[1]::text::bigint = 1 or operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.routes as r (muid,
                                 erm_id,
                                 agency_muid,
                                 number,
                                 registration_number,
                                 route_kind_muid,
                                 transport_kind_muid,
                                 route_transportation_kind_muid,
                                 route_state_muid,
                                 open_date,
                                 close_date,
                                 comment,
                                 version,
                                 sign_deleted,
                                 insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.routes sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (erm_id,
                   agency_muid,
                   number,
                   registration_number,
                   route_kind_muid,
                   transport_kind_muid,
                   route_transportation_kind_muid,
                   route_state_muid,
                   open_date,
                   close_date,
                   comment,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.erm_id,
                   excluded.agency_muid,
                   excluded.number,
                   excluded.registration_number,
                   excluded.route_kind_muid,
                   excluded.transport_kind_muid,
                   excluded.route_transportation_kind_muid,
                   excluded.route_state_muid,
                   excluded.open_date,
                   excluded.close_date,
                   excluded.comment,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where r.erm_id is distinct from excluded.erm_id or
          r.agency_muid is distinct from excluded.agency_muid or
          r.number is distinct from excluded.number or
          r.registration_number is distinct from excluded.registration_number or
          r.route_kind_muid is distinct from excluded.route_kind_muid or
          r.transport_kind_muid is distinct from excluded.transport_kind_muid or
          r.route_transportation_kind_muid is distinct from excluded.route_transportation_kind_muid or
          r.route_state_muid is distinct from excluded.route_state_muid or
          r.open_date is distinct from excluded.open_date or
          r.close_date is distinct from excluded.close_date or
          r.comment is distinct from excluded.comment or
          r.version is distinct from excluded.version or
          r.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg$import_route_variants(operation in text,
                                                                    data      in xml)
returns bigint
-- ========================================================================
-- Импорт Вариантов Маршрута из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()', data))[1]::text::bigint muid,
               (xpath('/*/header/identSet/componentId[component[text()="ERM"]]/componentIdent/text()', data))[1]::text::bigint
                   erm_id,
               (xpath('/*/body/*/type/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   route_variant_type_muid,
               (xpath('/*/body/*/route/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   route_muid,
               (xpath('/*/body/*/startDate/text()', data))[1]::text::date start_date,
               (xpath('/*/body/*/endDate/text()', data))[1]::text::date end_date,
               (xpath('/*/body/*/comment/text()', data))[1]::text s_comment,
               (xpath('/*/body/*/version/text()', data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', data))[1]::text::bigint = 1 or operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.route_variants as rv (muid,
                                          erm_id,
                                          route_variant_type_muid,
                                          route_muid,
                                          start_date,
                                          end_date,
                                          comment,
                                          version,
                                          sign_deleted,
                                          insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.route_variants sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (erm_id,
                   route_variant_type_muid,
                   route_muid,
                   start_date,
                   end_date,
                   comment,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.erm_id,
                   excluded.route_variant_type_muid,
                   excluded.route_muid,
                   excluded.start_date,
                   excluded.end_date,
                   excluded.comment,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where rv.erm_id is distinct from excluded.erm_id or
          rv.route_variant_type_muid is distinct from excluded.route_variant_type_muid or
          rv.route_muid is distinct from excluded.route_muid or
          rv.start_date is distinct from excluded.start_date or
          rv.end_date is distinct from excluded.end_date or
          rv.comment is distinct from excluded.comment or
          rv.version is distinct from excluded.version or
          rv.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg$import_routes2variants(operation in text,
                                                                     data      in xml)
returns bigint
-- ========================================================================
-- Импорт связки Маршрута и текущего Варианта Маршрута из XML типа
-- ========================================================================
as $$ declare
    v_result bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()', data))[1]::text::bigint muid,
               (xpath('/*/body/*/currentRouteVariant/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   current_route_variant_muid,
               case
                   when (xpath('/*/body/*/signDeleted/text()', data))[1]::text::bigint = 1 or operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    update gis.routes as r
       set current_route_variant_muid = parsed.current_route_variant_muid,
           update_date = parsed.cur_time
      from parsed
     where parsed.muid = r.muid
       and parsed.current_route_variant_muid is distinct from r.current_route_variant_muid;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg$import_route_rounds(operation in text,
                                                                  data      in xml)
returns bigint
-- ========================================================================
-- Импорт Производственных рейсов из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()', data))[1]::text::bigint muid,
               (xpath('/*/header/identSet/componentId[component[text()="ERM"]]/componentIdent/text()', data))[1]::text::bigint
                   erm_id,
               (xpath('/*/body/*/type/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   route_round_type_muid,
               (xpath('/*/body/*/routeVariant/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   route_variant_muid,
               (xpath('/*/body/*/code/text()', data))[1]::text code,
               (xpath('/*/body/*/stopPlaceA/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   stop_place_a_muid,
               (xpath('/*/body/*/stopPlaceB/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   stop_place_b_muid,
               (xpath('/*/body/*/terminalPointZoneA/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   terminal_zone_a_muid,
               (xpath('/*/body/*/terminalPointZoneB/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   terminal_zone_b_muid,
               (xpath('/*/body/*/terminalStationA/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   terminal_station_a_muid,
               (xpath('/*/body/*/terminalStationB/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   terminal_station_b_muid,
               (xpath('/*/body/*/terminalStationC/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   terminal_station_c_muid,
               (xpath('/*/body/*/averageLength/text()', data))[1]::text::real average_length,
               (xpath('/*/body/*/averageLengthFixed/text()', data))[1]::text::real average_length_fixed,
               (xpath('/*/body/*/specification1/text()', data))[1]::text specification1,
               (xpath('/*/body/*/specification2/text()', data))[1]::text specification2,
               (xpath('/*/body/*/comment/text()', data))[1]::text s_comment,
               (xpath('/*/body/*/version/text()', data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', data))[1]::text::bigint = 1 or operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.route_rounds as rr (muid,
                                        erm_id,
                                        route_round_type_muid,
                                        route_variant_muid,
                                        code,
                                        stop_place_a_muid,
                                        stop_place_b_muid,
                                        terminal_zone_a_muid,
                                        terminal_zone_b_muid,
                                        terminal_station_a_muid,
                                        terminal_station_b_muid,
                                        terminal_station_c_muid,
                                        average_length,
                                        average_length_fixed,
                                        specification1,
                                        specification2,
                                        comment,
                                        version,
                                        sign_deleted,
                                        insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.route_rounds sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (erm_id,
                   route_round_type_muid,
                   route_variant_muid,
                   code,
                   stop_place_a_muid,
                   stop_place_b_muid,
                   terminal_zone_a_muid,
                   terminal_zone_b_muid,
                   terminal_station_a_muid,
                   terminal_station_b_muid,
                   terminal_station_c_muid,
                   average_length,
                   average_length_fixed,
                   specification1,
                   specification2,
                   comment,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.erm_id,
                   excluded.route_round_type_muid,
                   excluded.route_variant_muid,
                   excluded.code,
                   excluded.stop_place_a_muid,
                   excluded.stop_place_b_muid,
                   excluded.terminal_zone_a_muid,
                   excluded.terminal_zone_b_muid,
                   excluded.terminal_station_a_muid,
                   excluded.terminal_station_b_muid,
                   excluded.terminal_station_c_muid,
                   excluded.average_length,
                   excluded.average_length_fixed,
                   excluded.specification1,
                   excluded.specification2,
                   excluded.comment,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where rr.erm_id is distinct from excluded.erm_id or
          rr.route_round_type_muid is distinct from excluded.route_round_type_muid or
          rr.route_variant_muid is distinct from excluded.route_variant_muid or
          rr.code is distinct from excluded.code or
          rr.stop_place_a_muid is distinct from excluded.stop_place_a_muid or
          rr.stop_place_b_muid is distinct from excluded.stop_place_b_muid or
          rr.terminal_zone_a_muid is distinct from excluded.terminal_zone_a_muid or
          rr.terminal_zone_b_muid is distinct from excluded.terminal_zone_b_muid or
          rr.terminal_station_a_muid is distinct from excluded.terminal_station_a_muid or
          rr.terminal_station_b_muid is distinct from excluded.terminal_station_b_muid or
          rr.terminal_station_c_muid is distinct from excluded.terminal_station_c_muid or
          rr.average_length is distinct from excluded.average_length or
          rr.average_length_fixed is distinct from excluded.average_length_fixed or
          rr.specification1 is distinct from excluded.specification1 or
          rr.specification2 is distinct from excluded.specification2 or
          rr.comment is distinct from excluded.comment or
          rr.version is distinct from excluded.version or
          rr.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg$import_route_null_rounds(operation in text,
                                                                       data      in xml)
returns bigint
-- ========================================================================
-- Импорт Нулевых рейсов из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()', data))[1]::text::bigint muid,
               (xpath('/*/body/*/type/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   route_null_round_type_muid,
               (xpath('/*/body/*/routeVariant/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   route_variant_muid,
               (xpath('/*/body/*/code/text()', data))[1]::text code,
               (xpath('/*/body/*/park1/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   park_1_muid,
               (xpath('/*/body/*/stopPlace1/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   stop_place_1_muid,
               (xpath('/*/body/*/park2/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   park_2_muid,
               (xpath('/*/body/*/stopPlace2/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   stop_place_2_muid,
               (xpath('/*/body/*/park3/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   park_3_muid,
               (xpath('/*/body/*/stopPlace3/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   stop_place_3_muid,
               (xpath('/*/body/*/comment/text()', data))[1]::text s_comment,
               (xpath('/*/body/*/version/text()', data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', data))[1]::text::bigint = 1 or operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.route_null_rounds as rnr (muid,
                                              route_null_round_type_muid,
                                              route_variant_muid,
                                              code,
                                              park_1_muid,
                                              stop_place_1_muid,
                                              park_2_muid,
                                              stop_place_2_muid,
                                              park_3_muid,
                                              stop_place_3_muid,
                                              comment,
                                              version,
                                              sign_deleted,
                                              insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.route_null_rounds sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (route_null_round_type_muid,
                   route_variant_muid,
                   code,
                   park_1_muid,
                   stop_place_1_muid,
                   park_2_muid,
                   stop_place_2_muid,
                   park_3_muid,
                   stop_place_3_muid,
                   comment,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.route_null_round_type_muid,
                   excluded.route_variant_muid,
                   excluded.code,
                   excluded.park_1_muid,
                   excluded.stop_place_1_muid,
                   excluded.park_2_muid,
                   excluded.stop_place_2_muid,
                   excluded.park_3_muid,
                   excluded.stop_place_3_muid,
                   excluded.comment,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where rnr.route_null_round_type_muid is distinct from excluded.route_null_round_type_muid or
          rnr.route_variant_muid is distinct from excluded.route_variant_muid or
          rnr.code is distinct from excluded.code or
          rnr.park_1_muid is distinct from excluded.park_1_muid or
          rnr.stop_place_1_muid is distinct from excluded.stop_place_1_muid or
          rnr.park_2_muid is distinct from excluded.park_2_muid or
          rnr.stop_place_2_muid is distinct from excluded.stop_place_2_muid or
          rnr.park_3_muid is distinct from excluded.park_3_muid or
          rnr.stop_place_3_muid is distinct from excluded.stop_place_3_muid or
          rnr.comment is distinct from excluded.comment or
          rnr.version is distinct from excluded.version or
          rnr.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg$import_stops(operation in text,
                                                           data      in xml)
returns bigint
-- ========================================================================
-- Импорт Остановок из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()', data))[1]::text::bigint muid,
               (xpath('/*/body/*/name/text()', data))[1]::text s_name,
               (xpath('/*/body/*/nameInEnglish/text()', data))[1]::text name_in_english,
               (xpath('/*/body/*/nameForTermnalPoint/text()', data))[1]::text name_for_terminal_point,
               (xpath('/*/body/*/direction/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   direction_muid,
               (xpath('/*/body/*/district/body/name/text()', data))[1]::text district,
               (xpath('/*/body/*/region/body/name/text()', data))[1]::text region,
               (xpath('/*/body/*/street/body/name/text()', data))[1]::text street,
               (xpath('/*/body/*/version/text()', data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', data))[1]::text::bigint = 1 or operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.stops as s (muid,
                                name,
                                name_in_english,
                                name_for_terminal_point,
                                direction_muid,
                                district,
                                region,
                                street,
                                version,
                                sign_deleted,
                                insert_date)
    select * from parsed
    where (sign_deleted = 0 or exists (select 1 from gis.stops sq where sq.muid = parsed.muid))
      and direction_muid is not null
    on conflict (muid)
    do update set (name,
                   name_in_english,
                   name_for_terminal_point,
                   direction_muid,
                   district,
                   region,
                   street,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.name,
                   excluded.name_in_english,
                   excluded.name_for_terminal_point,
                   excluded.direction_muid,
                   excluded.district,
                   excluded.region,
                   excluded.street,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where s.name is distinct from excluded.name or
          s.name_in_english is distinct from excluded.name_in_english or
          s.name_for_terminal_point is distinct from excluded.name_for_terminal_point or
          s.direction_muid is distinct from excluded.direction_muid or
          s.district is distinct from excluded.district or
          s.region is distinct from excluded.region or
          s.street is distinct from excluded.street or
          s.version is distinct from excluded.version or
          s.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg$import_stop_places(operation in text,
                                                                 data      in xml)
returns bigint
-- ========================================================================
-- Импорт Остановочных пунктов из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()', data))[1]::text::bigint muid,
               (xpath('/*/header/identSet/componentId[component[text()="ERM"]]/componentIdent/text()', data))[1]::text::bigint
                   erm_id,
               (xpath('/*/body/*/stop/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   stop_muid,
               (xpath('/*/body/*/state/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   stop_state_muid,
               (xpath('/*/body/*/mode/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   stop_mode_muid,
               (xpath('/*/body/*/wkt_geom/text()', data))[1]::text wkt_geom,
               st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', data))[1]::text) geom,
               (xpath('/*/body/*/suffix/text()', data))[1]::text suffix,
               (xpath('/*/body/*/graphSection/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   graph_section_muid,
               (xpath('/*/body/*/graphSectionOffset/text()', data))[1]::text::real graph_section_offset,
               (xpath('/*/body/*/graphTramSection/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   graphTramSectionOffset,
               (xpath('/*/body/*/graphTramSectionOffset/text()', data))[1]::text::real graph_tram_section_offset,
               (xpath('/*/body/*/isTechnical/text()', data))[1]::text::bigint is_technical,
               (xpath('/*/body/*/hasFacilitiesForDisabled/text()', data))[1]::text::bigint has_facilities_for_disabled,
               (xpath('/*/body/*/hasBus/text()', data))[1]::text::bigint has_bus,
               (xpath('/*/body/*/hasTrolley/text()', data))[1]::text::bigint has_trolley,
               (xpath('/*/body/*/hasTram/text()', data))[1]::text::bigint has_tram,
               (xpath('/*/body/*/hasSpeedTram/text()', data))[1]::text::bigint has_speedtram,
               (xpath('/*/body/*/startDate/text()', data))[1]::text::date start_date,
               (xpath('/*/body/*/changeDate/text()', data))[1]::text::date change_date,
               (xpath('/*/body/*/endDate/text()', data))[1]::text::date end_date,
               (xpath('/*/body/*/buildingDistance/text()', data))[1]::text::real building_distance,
               (xpath('/*/body/*/buildingAddressString/text()', data))[1]::text building_address,
               (xpath('/*/body/*/comment/text()', data))[1]::text s_comment,
               (xpath('/*/body/*/version/text()', data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', data))[1]::text::bigint = 1 or operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.stop_places as sp (muid,
                                erm_id,
                                stop_muid,
                                stop_state_muid,
                                stop_mode_muid,
                                wkt_geom,
                                geom,
                                suffix,
                                graph_section_muid,
                                graph_section_offset,
                                graph_tram_section_muid,
                                graph_tram_section_offset,
                                is_technical,
                                has_facilities_for_disabled,
                                has_bus,
                                has_trolley,
                                has_tram,
                                has_speedtram,
                                start_date,
                                change_date,
                                end_date,
                                building_distance,
                                building_address,
                                comment,
                                version,
                                sign_deleted,
                                insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.stop_places sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (erm_id,
                   stop_muid,
                   stop_state_muid,
                   stop_mode_muid,
                   wkt_geom,
                   geom,
                   suffix,
                   graph_section_muid,
                   graph_section_offset,
                   graph_tram_section_muid,
                   graph_tram_section_offset,
                   is_technical,
                   has_facilities_for_disabled,
                   has_bus,
                   has_trolley,
                   has_tram,
                   has_speedtram,
                   start_date,
                   change_date,
                   end_date,
                   building_distance,
                   building_address,
                   comment,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.erm_id,
                   excluded.stop_muid,
                   excluded.stop_state_muid,
                   excluded.stop_mode_muid,
                   excluded.wkt_geom,
                   excluded.geom,
                   excluded.suffix,
                   excluded.graph_section_muid,
                   excluded.graph_section_offset,
                   excluded.graph_tram_section_muid,
                   excluded.graph_tram_section_offset,
                   excluded.is_technical,
                   excluded.has_facilities_for_disabled,
                   excluded.has_bus,
                   excluded.has_trolley,
                   excluded.has_tram,
                   excluded.has_speedtram,
                   excluded.start_date,
                   excluded.change_date,
                   excluded.end_date,
                   excluded.building_distance,
                   excluded.building_address,
                   excluded.comment,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where sp.erm_id is distinct from excluded.erm_id or
          sp.stop_muid is distinct from excluded.stop_muid or
          sp.stop_state_muid is distinct from excluded.stop_state_muid or
          sp.stop_mode_muid is distinct from excluded.stop_mode_muid or
          sp.wkt_geom is distinct from excluded.wkt_geom or
          sp.suffix is distinct from excluded.suffix or
          sp.graph_section_muid is distinct from excluded.graph_section_muid or
          sp.graph_section_offset is distinct from excluded.graph_section_offset or
          sp.graph_tram_section_muid is distinct from excluded.graph_tram_section_muid or
          sp.graph_tram_section_offset is distinct from excluded.graph_tram_section_offset or
          sp.is_technical is distinct from excluded.is_technical or
          sp.has_facilities_for_disabled is distinct from excluded.has_facilities_for_disabled or
          sp.has_bus is distinct from excluded.has_bus or
          sp.has_trolley is distinct from excluded.has_trolley or
          sp.has_tram is distinct from excluded.has_tram or
          sp.has_speedtram is distinct from excluded.has_speedtram or
          sp.start_date is distinct from excluded.start_date or
          sp.change_date is distinct from excluded.change_date or
          sp.end_date is distinct from excluded.end_date or
          sp.building_distance is distinct from excluded.building_distance or
          sp.building_address is distinct from excluded.building_address or
          sp.comment is distinct from excluded.comment or
          sp.version is distinct from excluded.version or
          sp.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg$import_stop_zones(operation in text,
                                                                data      in xml)
returns bigint
-- ========================================================================
-- Импорт Зон посадки-высадки из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()', data))[1]::text::bigint muid,
               (xpath('/*/header/identSet/componentId[component[text()="ERM"]]/componentIdent/text()', data))[1]::text::bigint
                   erm_id,
               (xpath('/*/body/*/stopPlace/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   stop_place_muid,
               (xpath('/*/body/*/wkt_geom/text()', data))[1]::text wkt_geom,
               st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', data))[1]::text) geom,
               (xpath('/*/body/*/version/text()', data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', data))[1]::text::bigint = 1 or operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.stop_zones as sz (muid,
                                      erm_id,
                                      stop_place_muid,
                                      wkt_geom,
                                      geom,
                                      version,
                                      sign_deleted,
                                      insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.stop_zones sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (erm_id,
                   stop_place_muid,
                   wkt_geom,
                   geom,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.erm_id,
                   excluded.stop_place_muid,
                   excluded.wkt_geom,
                   excluded.geom,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where sz.erm_id is distinct from excluded.erm_id or
          sz.stop_place_muid is distinct from excluded.stop_place_muid or
          sz.wkt_geom is distinct from excluded.wkt_geom or
          sz.version is distinct from excluded.version or
          sz.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg$import_route_trajectories(operation in text,
                                                                        data      in xml)
returns bigint
-- ========================================================================
-- Импорт Траекторий маршрутов из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
    lroute_trajectory_muid bigint;
begin
    lroute_trajectory_muid := (select (xpath('/*/body/*/muid/text()', data))[1]::text::bigint);

    with parsed as (
        select lroute_trajectory_muid muid,
               (xpath('/*/header/identSet/componentId[component[text()="ERM"]]/componentIdent/text()', data))[1]::text::bigint
                   erm_id,
               (xpath('/*/body/*/trajectoryType/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   trajectory_type_muid,
               (xpath('/*/body/*/routeRound/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   route_round_muid,
               (xpath('/*/body/*/routeNullRound/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   route_null_round_muid,
               (xpath('/*/body/*/code/text()', data))[1]::text code,
               (xpath('/*/body/*/wkt_geom/text()', data))[1]::text wkt_geom,
               st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', data))[1]::text) geom,
               (xpath('/*/body/*/length/text()', data))[1]::text::real length,
               (xpath('/*/body/*/fixedLength/text()', data))[1]::text::real length_fixed,
               (xpath('/*/body/*/specification/text()', data))[1]::text specification,
               (xpath('/*/body/*/version/text()', data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', data))[1]::text::bigint = 1 or operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.route_trajectories as rt (muid,
                                              erm_id,
                                              trajectory_type_muid,
                                              route_round_muid,
                                              route_null_round_muid,
                                              code,
                                              wkt_geom,
                                              geom,
                                              length,
                                              length_fixed,
                                              specification,
                                              version,
                                              sign_deleted,
                                              insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.route_trajectories sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (erm_id,
                   trajectory_type_muid,
                   route_round_muid,
                   route_null_round_muid,
                   code,
                   wkt_geom,
                   geom,
                   length,
                   length_fixed,
                   specification,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.erm_id,
                   excluded.trajectory_type_muid,
                   excluded.route_round_muid,
                   excluded.route_null_round_muid,
                   excluded.code,
                   excluded.wkt_geom,
                   excluded.geom,
                   excluded.length,
                   excluded.length_fixed,
                   excluded.specification,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where rt.erm_id is distinct from excluded.erm_id or
          rt.trajectory_type_muid is distinct from excluded.trajectory_type_muid or
          rt.route_round_muid is distinct from excluded.route_round_muid or
          rt.route_null_round_muid is distinct from excluded.route_null_round_muid or
          rt.code is distinct from excluded.code or
          rt.wkt_geom is distinct from excluded.wkt_geom or
          rt.length is distinct from excluded.length or
          rt.length_fixed is distinct from excluded.length_fixed or
          rt.specification is distinct from excluded.specification or
          rt.version is distinct from excluded.version or
          rt.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    
    delete from gis.graph_sec2route_traj where route_trajectory_muid = lroute_trajectory_muid;
    delete from gis.stop_place2route_traj where route_trajectory_muid = lroute_trajectory_muid;

    with parsed as (
        select (xpath('/*/body/muid/text()', gs_data))[1]::text::bigint muid,
               lroute_trajectory_muid,
               (xpath('/*/body/graphSection/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', gs_data))[1]::text::bigint
                   graph_section_muid,
               (xpath('/*/body/index/text()', gs_data))[1]::text::bigint order_num,
               (xpath('/*/body/graphSectionStartOffset/text()', gs_data))[1]::text::real graph_section_start_offset,
               (xpath('/*/body/graphSectionEndOffset/text()', gs_data))[1]::text::real graph_section_end_offset,
               (xpath('/*/body/version/text()', gs_data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/signDeleted/text()', gs_data))[1]::text::bigint = 1 or operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
        from (
            select unnest(xpath('/*/body/*/graphSections/linkedGraphSection', data)) as gs_data
        ) t
    )
    insert into gis.graph_sec2route_traj as gs2rt (muid,
                                                   route_trajectory_muid,
                                                   graph_section_muid,
                                                   order_num,
                                                   graph_section_start_offset,
                                                   graph_section_end_offset,
                                                   version,
                                                   sign_deleted,
                                                   insert_date)
    select * from parsed 
    where sign_deleted = 0 or exists (select 1 from gis.graph_sec2route_traj sq where sq.muid = parsed.muid)
    order by order_num
    on conflict (muid)
    do update set (route_trajectory_muid,
                   graph_section_muid,
                   order_num,
                   graph_section_start_offset,
                   graph_section_end_offset,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.route_trajectory_muid,
                   excluded.graph_section_muid,
                   excluded.order_num,
                   excluded.graph_section_start_offset,
                   excluded.graph_section_end_offset,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where gs2rt.route_trajectory_muid is distinct from excluded.route_trajectory_muid or
          gs2rt.graph_section_muid is distinct from excluded.graph_section_muid or
          gs2rt.order_num is distinct from excluded.order_num or
          gs2rt.graph_section_start_offset is distinct from excluded.graph_section_start_offset or
          gs2rt.graph_section_end_offset is distinct from excluded.graph_section_end_offset or
          gs2rt.version is distinct from excluded.version or
          gs2rt.sign_deleted is distinct from excluded.sign_deleted;

    with parsed as (
        select (xpath('/*/body/muid/text()', sp_data))[1]::text::bigint muid,
               lroute_trajectory_muid,
               (xpath('/*/body/stopPlace/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', sp_data))[1]::text::bigint
                   stop_place_muid,
               (xpath('/*/body/index/text()', sp_data))[1]::text::bigint order_num,
               (xpath('/*/body/mode/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', sp_data))[1]::text::bigint
                   stop_mode_muid,
               (xpath('/*/body/type/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', sp_data))[1]::text::bigint
                   stop_type_muid,
               (xpath('/*/body/lengthSector/text()', sp_data))[1]::text::real length_sector,
               (xpath('/*/body/comment/text()', sp_data))[1]::text s_comment,
               (xpath('/*/body/version/text()', sp_data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/signDeleted/text()', sp_data))[1]::text::bigint = 1 or operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
        from (
            select unnest(xpath('/*/body/*/stopPlaces/linkedStopPlace', data)) as sp_data
        ) t
    )
    insert into gis.stop_place2route_traj as sp2rt (muid,
                                                    route_trajectory_muid,
                                                    stop_place_muid,
                                                    order_num,
                                                    stop_mode_muid,
                                                    stop_type_muid,
                                                    length_sector,
                                                    comment,
                                                    version,
                                                    sign_deleted,
                                                    insert_date)
    select * from parsed 
    where sign_deleted = 0 or exists (select 1 from gis.stop_place2route_traj sq where sq.muid = parsed.muid)
    order by order_num
    on conflict (muid)
    do update set (route_trajectory_muid,
                   stop_place_muid,
                   order_num,
                   stop_mode_muid,
                   stop_type_muid,
                   length_sector,
                   comment,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.route_trajectory_muid,
                   excluded.stop_place_muid,
                   excluded.order_num,
                   excluded.stop_mode_muid,
                   excluded.stop_type_muid,
                   excluded.length_sector,
                   excluded.comment,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where sp2rt.route_trajectory_muid is distinct from excluded.route_trajectory_muid or
          sp2rt.stop_place_muid is distinct from excluded.stop_place_muid or
          sp2rt.order_num is distinct from excluded.order_num or
          sp2rt.stop_mode_muid is distinct from excluded.stop_mode_muid or
          sp2rt.stop_type_muid is distinct from excluded.stop_type_muid or
          sp2rt.length_sector is distinct from excluded.length_sector or
          sp2rt.comment is distinct from excluded.comment or
          sp2rt.version is distinct from excluded.version or
          sp2rt.sign_deleted is distinct from excluded.sign_deleted;
    
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg$import_refs_init()
returns bigint
-- ========================================================================
-- Импорт (инициализация) основных справочников
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with xml_source as (
        select distinct
               (xpath('/*/body/*/muid/text()', n))[1]::text::bigint muid,
               (xpath('/*/body/*/name/text()', n))[1]::text s_name,
               (xpath('/*/body/*/shortName/text()', n))[1]::text short_name,
               (xpath('/*/body/*/code/text()', n))[1]::text code,
               (xpath('/*/body/*/version/text()', n))[1]::text::bigint n_version,
               (xpath('/*/body/*/signDeleted/text()', n))[1]::text::bigint sign_deleted
        from (
            select unnest(xpath('/*/*[header/objectType/text()="TRouteNullRoundType"]', data)) n
            from gis.gis_xml_files
            where xpath_exists('/*/*/header/objectType[text()="TRouteNullRoundType"]', data)
        ) t
        order by muid
    )
    insert into gis.ref_route_null_round_types(muid,
                                               name,
                                               short_name,
                                               code,
                                               version,
                                               sign_deleted,
                                               insert_date)
    select muid,
           s_name,
           short_name,
           code,
           n_version,
           sign_deleted,
           clock_timestamp()
    from xml_source;
    
    
    with xml_source as (
        select distinct
               (xpath('/*/body/*/muid/text()', n))[1]::text::bigint muid,
               (xpath('/*/body/*/name/text()', n))[1]::text s_name,
               (xpath('/*/body/*/code/text()', n))[1]::text code,
               (xpath('/*/body/*/version/text()', n))[1]::text::bigint n_version,
               (xpath('/*/body/*/signDeleted/text()', n))[1]::text::bigint sign_deleted
        from (
            select unnest(xpath('/*/*[header/objectType/text()="TRouteRoundType"]', data)) n
            from gis.gis_xml_files
            where xpath_exists('/*/*/header/objectType[text()="TRouteRoundType"]', data)
        ) t
        order by muid
    )
    insert into gis.ref_route_round_types(muid,
                                          name,
                                          code,
                                          version,
                                          sign_deleted,
                                          insert_date)
    select muid,
           s_name,
           code,
           n_version,
           sign_deleted,
           clock_timestamp()
    from xml_source;
    
    
    with xml_source as (
        select distinct
               (xpath('/*/body/muid/text()', n))[1]::text::bigint muid,
               (xpath('/*/body/name/text()', n))[1]::text s_name,
               (xpath('/*/body/version/text()', n))[1]::text::bigint n_version,
               (xpath('/*/body/signDeleted/text()', n))[1]::text::bigint sign_deleted
        from (
            select unnest(xpath('//*[header/objectType/text()="TRouteState"]', data)) n
            from gis.gis_xml_files
            where xpath_exists('//header/objectType[text()="TRouteState"]', data)
        ) t
        order by muid
    )
    insert into gis.ref_route_states(muid,
                                     name,
                                     version,
                                     sign_deleted,
                                     insert_date)
    select muid,
           s_name,
           n_version,
           sign_deleted,
           clock_timestamp()
    from xml_source;
    
    
    with xml_source as (
        select distinct
               (xpath('/*/body/*/muid/text()', n))[1]::text::bigint muid,
               (xpath('/*/body/*/name/text()', n))[1]::text s_name,
               (xpath('/*/body/*/version/text()', n))[1]::text::bigint n_version,
               (xpath('/*/body/*/signDeleted/text()', n))[1]::text::bigint sign_deleted
        from (
            select unnest(xpath('/*/*[header/objectType/text()="TRouteState2"]', data)) n
            from gis.gis_xml_files
            where xpath_exists('/*/*/header/objectType[text()="TRouteState2"]', data)
        ) t
        order by muid
    )
    insert into gis.ref_route_states2(muid,
                                      name,
                                      version,
                                      sign_deleted,
                                      insert_date)
    select muid,
           s_name,
           n_version,
           sign_deleted,
           clock_timestamp()
    from xml_source;
    
    
    with xml_source as (
        select distinct
               (xpath('/*/body/muid/text()', n))[1]::text::bigint muid,
               (xpath('/*/body/name/text()', n))[1]::text s_name,
               (xpath('/*/body/version/text()', n))[1]::text::bigint n_version,
               (xpath('/*/body/signDeleted/text()', n))[1]::text::bigint sign_deleted
        from (
            select unnest(xpath('//*[header/objectType/text()="TRouteTrajectoryType"]', data)) n
            from gis.gis_xml_files
            where xpath_exists('//header/objectType[text()="TRouteTrajectoryType"]', data)
        ) t
        order by muid
    )
    insert into gis.ref_route_trajectory_types(muid,
                                               name,
                                               version,
                                               sign_deleted,
                                               insert_date)
    select muid,
           s_name,
           n_version,
           sign_deleted,
           clock_timestamp()
    from xml_source;
    
    
    with xml_source as (
        select distinct
               (xpath('/*/body/*/muid/text()', n))[1]::text::bigint muid,
               (xpath('/*/body/*/name/text()', n))[1]::text s_name,
               (xpath('/*/body/*/version/text()', n))[1]::text::bigint n_version,
               (xpath('/*/body/*/signDeleted/text()', n))[1]::text::bigint sign_deleted
        from (
            select unnest(xpath('/*/*[header/objectType/text()="TRouteTransportationKind"]', data)) n
            from gis.gis_xml_files
            where xpath_exists('/*/*/header/objectType[text()="TRouteTransportationKind"]', data)
        ) t
        order by muid
    )
    insert into gis.ref_route_transportation_kinds(muid,
                                                   name,
                                                   version,
                                                   sign_deleted,
                                                   insert_date)
    select muid,
           s_name,
           n_version,
           sign_deleted,
           clock_timestamp()
    from xml_source;
    
    
    with xml_source as (
        select distinct
               (xpath('/*/body/*/muid/text()', n))[1]::text::bigint muid,
               (xpath('/*/body/*/name/text()', n))[1]::text s_name,
               (xpath('/*/body/*/shortName/text()', n))[1]::text short_name,
               (xpath('/*/body/*/version/text()', n))[1]::text::bigint n_version,
               (xpath('/*/body/*/signDeleted/text()', n))[1]::text::bigint sign_deleted
        from (
            select unnest(xpath('/*/*[header/objectType/text()="TRouteTransportKind"]', data)) n
              from gis.gis_xml_files
             where xpath_exists('/*/*/header/objectType[text()="TRouteTransportKind"]', data)
        ) t
        order by muid
    )
    insert into gis.ref_transport_kinds(muid,
                                        name,
                                        short_name,
                                        version,
                                        sign_deleted,
                                        insert_date)
    select muid,
           s_name,
           short_name,
           n_version,
           sign_deleted,
           clock_timestamp()
    from xml_source;
    
    
    with xml_source as (
        select distinct
               (xpath('/*/body/*/muid/text()', n))[1]::text::bigint muid,
               (xpath('/*/body/*/name/text()', n))[1]::text s_name,
               (xpath('/*/body/*/version/text()', n))[1]::text::bigint n_version,
               (xpath('/*/body/*/signDeleted/text()', n))[1]::text::bigint sign_deleted
        from (
            select unnest(xpath('/*/*[header/objectType/text()="TRouteVariantType"]', data)) n
            from gis.gis_xml_files
            where xpath_exists('/*/*/header/objectType[text()="TRouteVariantType"]', data)
        ) t
        order by muid
    )
    insert into gis.ref_route_variant_types(muid,
                                            name,
                                            version,
                                            sign_deleted,
                                            insert_date)
    select muid,
           s_name,
           n_version,
           sign_deleted,
           clock_timestamp()
    from xml_source;
    
    
    with xml_source as (
        select distinct
               (xpath('/*/body/*/muid/text()', n))[1]::text::bigint muid,
               (xpath('/*/body/*/name/text()', n))[1]::text s_name,
               (xpath('/*/body/*/version/text()', n))[1]::text::bigint n_version,
               (xpath('/*/body/*/signDeleted/text()', n))[1]::text::bigint sign_deleted
        from (
            select unnest(xpath('/*/*[header/objectType/text()="TStopState"]', data)) n
            from gis.gis_xml_files
            where xpath_exists('/*/*/header/objectType[text()="TStopState"]', data)
        ) t
        order by muid
    )
    insert into gis.ref_stop_states(muid,
                                    name,
                                    version,
                                    sign_deleted,
                                    insert_date)
    select muid,
           s_name,
           n_version,
           sign_deleted,
           clock_timestamp()
    from xml_source;
    
-- not found
    
    with xml_source as (
        select distinct
               (xpath('/*/body/muid/text()', n))[1]::text::bigint muid,
               (xpath('/*/body/name/text()', n))[1]::text s_name,
               (xpath('/*/body/version/text()', n))[1]::text::bigint n_version,
               (xpath('/*/body/signDeleted/text()', n))[1]::text::bigint sign_deleted
        from (
            select unnest(xpath('//*[header/objectType/text()="TStopType"]', data)) n
            from gis.gis_xml_files
            where xpath_exists('//header/objectType[text()="TStopType"]', data)
        ) t
        order by muid
    )
    insert into gis.ref_stop_types(muid,
                                   name,
                                   version,
                                   sign_deleted,
                                   insert_date)
    select muid,
           s_name,
           n_version,
           sign_deleted,
           clock_timestamp()
    from xml_source
   where muid is not null;
    
-- not found. insert 1
    with xml_source as (
        select distinct
               (xpath('/*/body/muid/text()', n))[1]::text::bigint muid,
               (xpath('/*/body/name/text()', n))[1]::text s_name,
               (xpath('/*/body/version/text()', n))[1]::text::bigint n_version,
               (xpath('/*/body/signDeleted/text()', n))[1]::text::bigint sign_deleted
        from (
            select unnest(xpath('//*[header/objectType/text()="TStopType"]', data)) n
            from gis.gis_xml_files
            where xpath_exists('//header/objectType[text()="TStopType"]', data)
        ) t
        order by muid
    )
    insert into gis.ref_stop_types(muid,
                                   name,
                                   version,
                                   sign_deleted,
                                   insert_date)
    select muid,
           s_name,
           n_version,
           sign_deleted,
           clock_timestamp()
    from xml_source
    where muid is not null;    
    
    with xml_source as (
        select distinct
               (xpath('/*/body/*/muid/text()', n))[1]::text::bigint muid,
               (xpath('/*/body/*/name/text()', n))[1]::text s_name,
               (xpath('/*/body/*/shortName/text()', n))[1]::text short_name,
               (xpath('/*/body/*/version/text()', n))[1]::text::bigint n_version,
               (xpath('/*/body/*/signDeleted/text()', n))[1]::text::bigint sign_deleted
        from (
            select unnest(xpath('/*/*[header/objectType[text()="TStopMode"]]', data)) n
            from gis.gis_xml_files
            where xpath_exists('/*/*/header/objectType[text()="TStopMode"]', data)
        ) t
        order by muid
    )
    insert into gis.ref_stop_modes(muid,
                                   name,
                                   short_name,
                                   version,
                                   sign_deleted,
                                   insert_date)
    select muid,
           s_name,
           short_name,
           n_version,
           sign_deleted,
           clock_timestamp()
    from xml_source;
    
    
    with xml_source as (
        select distinct
               (xpath('/*/body/*/muid/text()', n))[1]::text::bigint muid,
               (xpath('/*/body/*/name/text()', n))[1]::text s_name,
               (xpath('/*/body/*/code/text()', n))[1]::text okpo_code,
               (xpath('/*/body/*/version/text()', n))[1]::text::bigint n_version,
               (xpath('/*/body/*/signDeleted/text()', n))[1]::text::bigint sign_deleted
        from (
            select unnest(xpath('/*/*[header/objectType/text()="TAgencyOrganizationForm"]', data)) n
            from gis.gis_xml_files
            where xpath_exists('/*/*/header/objectType[text()="TAgencyOrganizationForm"]', data)
        ) t
        order by muid
    )
    insert into gis.agency_organization_forms(muid,
                                              name,
                                              okpo_code,
                                              version,
                                              sign_deleted,
                                              insert_date)
    select muid,
           s_name,
           okpo_code,
           n_version,
           sign_deleted,
           clock_timestamp()
    from xml_source;
    
    
    with xml_source as (
        select distinct
               (xpath('/*/body/*/muid/text()', n))[1]::text::bigint muid,
               (xpath('/*/body/*/name/text()', n))[1]::text s_name,
               (xpath('/*/body/*/version/text()', n))[1]::text::bigint n_version,
               (xpath('/*/body/*/signDeleted/text()', n))[1]::text::bigint sign_deleted
        from (
            select unnest(xpath('/*/*[header/objectType/text()="TTransportStaticObjectType"]', data)) n
            from gis.gis_xml_files
            where xpath_exists('/*/*/header/objectType[text()="TTransportStaticObjectType"]', data)
        ) t
        order by muid
    )
    insert into gis.transport_static_object_types(muid,
                                                  name,
                                                  version,
                                                  sign_deleted,
                                                  insert_date)
    select muid,
           s_name,
           n_version,
           sign_deleted,
           clock_timestamp()
    from xml_source;
    
    
    with xml_source as (
        select distinct
               (xpath('/*/body/*/muid/text()', n))[1]::text::bigint muid,
               (xpath('/*/body/*/name/text()', n))[1]::text s_name,
               (xpath('/*/body/*/version/text()', n))[1]::text::bigint n_version,
               (xpath('/*/body/*/signDeleted/text()', n))[1]::text::bigint sign_deleted
        from (
            select unnest(xpath('/*/*[header/objectType/text()="TMovementDirection"]', data)) n
            from gis.gis_xml_files
            where xpath_exists('/*/*/header/objectType[text()="TMovementDirection"]', data)
        ) t
        order by muid
    )
    insert into gis.ref_movement_directions(muid, 
                                                  name, 
                                                  version, 
                                                  sign_deleted, 
                                                  insert_date)
    select muid,
           s_name,
           n_version,
           sign_deleted,
           clock_timestamp()
    from xml_source;
    
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg$import_vehicle_types(operation in text,
                                                                   data      in xml)
returns bigint
-- ========================================================================
-- Импорт Типов подвижного состава из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()', data))[1]::text::bigint muid,
               (xpath('/*/body/*/name/text()', data))[1]::text s_name,
               (xpath('/*/body/*/shortName/text()', data))[1]::text short_name,
               (xpath('/*/body/*/model/text()', data))[1]::text model,
               (xpath('/*/body/TVehicleType/routeTransportKind/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   route_transport_kind_muid,
               (xpath('/*/body/*/seatingCapacity/text()', data))[1]::text::bigint seating_capacity,
               (xpath('/*/body/*/maxCapacity/text()', data))[1]::text::bigint max_capacity,
               (xpath('/*/body/*/effectiveArea/text()', data))[1]::text::real effective_area,
               (xpath('/*/body/*/fullLoadWeight/text()', data))[1]::text::real full_load_weight,
               (xpath('/*/body/*/dimension/text()', data))[1]::text dimensions,
               (xpath('/*/body/*/hasFacilitiesForDisabled/text()', data))[1]::text::bigint has_facilities_for_disabled,
               (xpath('/*/body/*/version/text()', data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', data))[1]::text::bigint = 1 or operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() insert_date
    )
    insert into gis.ref_vehicle_types as rv (muid, 
                                          name, 
                                          short_name, 
                                          model, 
                                          route_transport_kind_muid, 
                                          seating_capacity, 
                                          max_capacity, 
                                          effective_area, 
                                          full_load_weight, 
                                          dimensions, 
                                          has_facilities_for_disabled, 
                                          version, 
                                          sign_deleted, 
                                          insert_date)
    select muid, 
           s_name, 
           short_name, 
           model, 
           route_transport_kind_muid, 
           seating_capacity, 
           max_capacity, 
           effective_area, 
           full_load_weight, 
           dimensions, 
           has_facilities_for_disabled, 
           n_version, 
           sign_deleted, 
           insert_date 
      from parsed
     where sign_deleted = 0 or exists (select 1 from gis.ref_vehicle_types sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (name, 
                   short_name, 
                   model, 
                   route_transport_kind_muid, 
                   seating_capacity, 
                   max_capacity, 
                   effective_area, 
                   full_load_weight, 
                   dimensions, 
                   has_facilities_for_disabled, 
                   version, 
                   sign_deleted, 
                   update_date) =
                  (excluded.name, 
                   excluded.short_name, 
                   excluded.model, 
                   excluded.route_transport_kind_muid, 
                   excluded.seating_capacity, 
                   excluded.max_capacity, 
                   excluded.effective_area, 
                   excluded.full_load_weight, 
                   excluded.dimensions, 
                   excluded.has_facilities_for_disabled, 
                   excluded.version, 
                   excluded.sign_deleted, 
                   excluded.insert_date)
    where rv.name is distinct from excluded.name or
          rv.short_name is distinct from excluded.short_name or
          rv.model is distinct from excluded.model or
          rv.route_transport_kind_muid is distinct from excluded.route_transport_kind_muid or
          rv.seating_capacity is distinct from excluded.seating_capacity or
          rv.max_capacity is distinct from excluded.max_capacity or
          rv.effective_area is distinct from excluded.effective_area or
          rv.full_load_weight is distinct from excluded.full_load_weight or
          rv.dimensions is distinct from excluded.dimensions or
          rv.has_facilities_for_disabled is distinct from excluded.has_facilities_for_disabled or
          rv.version is distinct from excluded.version or
          rv.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg$import_park_zones(operation in text,
                                                                data      in xml)
returns bigint
-- ========================================================================
-- Импорт Зон парков из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()', data))[1]::text::bigint muid,
               (xpath('/*/body/*/wkt_geom/text()', data))[1]::text wkt_geom,
               st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', data))[1]::text) geom,
               (xpath('/*/body/*/version/text()', data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', data))[1]::text::bigint = 1 or operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.park_zones as p (muid,
                                     wkt_geom,
                                     geom,
                                     version,
                                     sign_deleted,
                                     insert_date)
    select * from parsed order by muid
    on conflict (muid)
    do update set (wkt_geom,
                   geom,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.wkt_geom,
                   excluded.geom,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where p.wkt_geom is distinct from excluded.wkt_geom or
          p.version is distinct from excluded.version or
          p.sign_deleted is distinct from excluded.sign_deleted;
    
    with parsed as (
        select (xpath('/*/body/*/park/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   park_muid,
               (xpath('/*/body/*/muid/text()', data))[1]::text::bigint park_zone_muid,
               (xpath('/*/body/*/version/text()', data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', data))[1]::text::bigint = 1 or operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.parks2park_zones as p (park_muid,
                                           park_zone_muid,
                                           version,
                                           sign_deleted,
                                           insert_date)
    select * from parsed
    where sign_deleted = 0 
       or exists (select 1 from gis.parks2park_zones sq
                   where sq.park_muid = parsed.park_muid
                     and sq.park_zone_muid = parsed.park_zone_muid)
    on conflict (park_muid, park_zone_muid)
    do update set (version,
                   sign_deleted,
                   update_date) =
                  (excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where p.version is distinct from excluded.version or
          p.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg$import_transport_static_objects(operation in text,
                                                                              data      in xml)
returns bigint
-- ========================================================================
-- Импорт Стационарных объектов из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()', data))[1]::text::bigint muid,
               (xpath('/*/body/*/type/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   transport_static_object_type_muid,
               (xpath('/*/body/*/name/text()', data))[1]::text s_name,
               (xpath('/*/body/*/wkt_geom/text()', data))[1]::text wkt_geom,
               st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', data))[1]::text) geom,
               (xpath('/*/body/*/version/text()', data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', data))[1]::text::bigint = 1 or operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.transport_static_objects as gn (muid, 
                                                    transport_static_object_type_muid, 
                                                    name, 
                                                    wkt_geom, 
                                                    geom, 
                                                    version, 
                                                    sign_deleted, 
                                                    insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.transport_static_objects sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (transport_static_object_type_muid, 
                   name, 
                   wkt_geom, 
                   geom, 
                   version, 
                   sign_deleted,
                   update_date) =
                  (excluded.transport_static_object_type_muid, 
                   excluded.name, 
                   excluded.wkt_geom, 
                   excluded.geom, 
                   excluded.version, 
                   excluded.sign_deleted,
                   excluded.insert_date)
    where gn.transport_static_object_type_muid is distinct from excluded.transport_static_object_type_muid or
          gn.name         is distinct from excluded.name or
          gn.wkt_geom     is distinct from excluded.wkt_geom or
          gn.geom         is distinct from excluded.geom or
          gn.version      is distinct from excluded.version or
          gn.sign_deleted is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg$import_transport_parkings(operation in text,
                                                                        data      in xml)
returns bigint
-- ========================================================================
-- Импорт Отстойных площадок из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()', data))[1]::text::bigint muid,
               (xpath('/*/body/*/park/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   park_muid,
               (xpath('/*/body/*/wkt_geom/text()', data))[1]::text wkt_geom,
               st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', data))[1]::text) geom,
               (xpath('/*/body/*/name/text()', data))[1]::text s_name,
               (xpath('/*/body/*/addressString/text()', data))[1]::text address,
               (xpath('/*/body/*/phone/text()', data))[1]::text phone,
               (xpath('/*/body/*/graphSection/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   graph_section_muid,
               (xpath('/*/body/*/graphSectionOffset/text()', data))[1]::text::real graph_section_offset,
               (xpath('/*/body/*/square/text()', data))[1]::text::real square,
               (xpath('/*/body/*/capacity/text()', data))[1]::text::bigint capacity,
               (xpath('/*/body/*/hasBus/text()', data))[1]::text::bigint has_bus, 
               (xpath('/*/body/*/hasTrolley/text()', data))[1]::text::bigint has_trolley, 
               (xpath('/*/body/*/hasTram/text()', data))[1]::text::bigint has_tram, 
               (xpath('/*/body/*/hasSpeedTram/text()', data))[1]::text::bigint has_speedtram, 
               (xpath('/*/body/*/version/text()', data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', data))[1]::text::bigint = 1 or operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.transport_parkings as gn (muid, 
                                              park_muid, 
                                              wkt_geom, 
                                              geom, 
                                              name, 
                                              address, 
                                              phone, 
                                              graph_section_muid, 
                                              graph_section_offset, 
                                              square, 
                                              capacity, 
                                              has_bus, 
                                              has_trolley, 
                                              has_tram, 
                                              has_speedtram, 
                                              version, 
                                              sign_deleted, 
                                              insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.transport_parkings sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (park_muid, 
                   wkt_geom, 
                   geom, 
                   name, 
                   address, 
                   phone, 
                   graph_section_muid, 
                   graph_section_offset, 
                   square, 
                   capacity, 
                   has_bus, 
                   has_trolley, 
                   has_tram, 
                   has_speedtram, 
                   version, 
                   sign_deleted,
                   update_date) = 
                  (excluded.park_muid, 
                   excluded.wkt_geom, 
                   excluded.geom, 
                   excluded.name, 
                   excluded.address, 
                   excluded.phone, 
                   excluded.graph_section_muid, 
                   excluded.graph_section_offset, 
                   excluded.square, 
                   excluded.capacity, 
                   excluded.has_bus, 
                   excluded.has_trolley, 
                   excluded.has_tram, 
                   excluded.has_speedtram, 
                   excluded.version, 
                   excluded.sign_deleted,
                   excluded.insert_date)
    where gn.park_muid     is distinct from excluded.park_muid or
          gn.wkt_geom      is distinct from excluded.wkt_geom  or
          gn.geom          is distinct from excluded.geom      or 
          gn.name          is distinct from excluded.name      or 
          gn.address       is distinct from excluded.address   or 
          gn.phone         is distinct from excluded.phone     or 
          gn.graph_section_muid   is distinct from excluded.graph_section_muid   or 
          gn.graph_section_offset is distinct from excluded.graph_section_offset or 
          gn.square        is distinct from excluded.square    or 
          gn.capacity      is distinct from excluded.capacity  or 
          gn.has_bus       is distinct from excluded.has_bus   or 
          gn.has_trolley   is distinct from excluded.has_trolley   or 
          gn.has_tram      is distinct from excluded.has_tram  or 
          gn.has_speedtram is distinct from excluded.has_speedtram or 
          gn.version       is distinct from excluded.version   or
          gn.sign_deleted  is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg$import_terminal_point_zones(operation in text,
                                                                          data      in xml)
returns bigint
-- ========================================================================
-- Импорт Зона конечных пунктов из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/*/body/*/muid/text()', data))[1]::text::bigint muid,
               (xpath('/*/body/*/stopPlace/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   stop_place_muid,
               (xpath('/*/body/*/route/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', data))[1]::text::bigint
                   route_muid,
               (xpath('/*/body/*/name/text()', data))[1]::text s_name,
               (xpath('/*/body/*/wkt_geom/text()', data))[1]::text wkt_geom,
               st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', data))[1]::text) geom,
               (xpath('/*/body/*/version/text()', data))[1]::text::bigint n_version,
               case
                   when (xpath('/*/body/*/signDeleted/text()', data))[1]::text::bigint = 1 or operation = 'DELETE'
                   then 1
                   else 0
               end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.terminal_point_zones as gn (muid,
                                                stop_place_muid,
                                                route_muid,
                                                name,
                                                wkt_geom,
                                                geom, 
                                                version, 
                                                sign_deleted, 
                                                insert_date)
    select * from parsed
    where sign_deleted = 0 or exists (select 1 from gis.terminal_point_zones sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (stop_place_muid,
                   route_muid,
                   name,
                   wkt_geom,
                   geom, 
                   version, 
                   sign_deleted,
                   update_date) = 
                  (excluded.stop_place_muid,
                   excluded.route_muid,
                   excluded.name,
                   excluded.wkt_geom,
                   excluded.geom, 
                   excluded.version, 
                   excluded.sign_deleted,
                   excluded.insert_date)
    where gn.stop_place_muid is distinct from excluded.stop_place_muid or
          gn.route_muid      is distinct from excluded.route_muid or
          gn.name            is distinct from excluded.name       or
          gn.wkt_geom        is distinct from excluded.wkt_geom   or
          gn.geom            is distinct from excluded.geom       or 
          gn.version         is distinct from excluded.version    or
          gn.sign_deleted    is distinct from excluded.sign_deleted;
    
    GET DIAGNOSTICS v_result = ROW_COUNT;
    return v_result;
end;
$$ language plpgsql;
