CREATE OR REPLACE FUNCTION gis."jf_tr_static_objects_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
begin 
 open p_rows for 
      select 
        tso.MUID		AS "MUID", 
        tso.TRANSPORT_STATIC_OBJECT_TYPE_MUID AS "TRANSPORT_STATIC_OBJECT_TYPE_MUID", 
        tso.NAME		AS "NAME", 
        tso.WKT_GEOM	AS "WKT_GEOM", 
        tso.GEOM		AS "GEOM", 
        tso.VERSION		AS "VERSION", 
        tso.SIGN_DELETED	AS "SIGN_DELETED", 
        tso.INSERT_DATE	AS "INSERT_DATE", 
        tso.UPDATE_DATE	AS "UPDATE_DATE",
        tsot.NAME		AS "TSOT_NAME"
      from gis.transport_static_objects tso, gis.transport_static_object_types tsot
       where tso.transport_static_object_type_muid = tsot.muid; 
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
