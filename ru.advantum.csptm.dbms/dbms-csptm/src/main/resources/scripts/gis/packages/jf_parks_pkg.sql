CREATE OR REPLACE FUNCTION gis."jf_parks_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
 f_number gis.parks.number%type := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_NUMBER', true), '');
 f_name gis.parks.name%type     := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_NAME', true), '');
 f_rtk_MUID numeric := -1;
begin 
 f_rtk_MUID := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_rtk_MUID', TRUE), -1);
 
if f_rtk_MUID = -1 then
 open p_rows for 
      select 
        p.MUID::text			AS "MUID", 
        p.AGENCY_MUID		AS "AGENCY_MUID", 
        p.ERM_ID		AS "ERM_ID", 
        p.FHD_ID		AS "FHD_ID", 
        p.PHONE			AS "PHONE",
        p.VERSION		AS "VERSION", 
        p.ADDRESS		AS "ADDRESS", 
        p.SHORT_NAME		AS "SHORT_NAME", 
        p.NAME			AS "NAME", 
        p.NUMBER		AS "NUMBER", 
        p.SIGN_DELETED		AS "SIGN_DELETED", 
        p.INSERT_DATE		AS "INSERT_DATE", 
        p.UPDATE_DATE		AS "UPDATE_DATE",
        p.TRANSPORT_KIND_MUID	AS "TRANSPORT_KIND_MUID",
        rtk.NAME		AS "RTK_NAME"
      from gis.parks p, gis.ref_transport_kinds rtk
	where p.transport_kind_muid = rtk.muid
	  and (p.NAME = f_name or f_name = '')
	  and (p.NUMBER = f_number or f_number = ''); 
 else 
   open p_rows for 
   select 
        p.MUID::text			AS "MUID", 
        p.AGENCY_MUID		AS "AGENCY_MUID", 
        p.ERM_ID		AS "ERM_ID", 
        p.FHD_ID		AS "FHD_ID", 
        p.PHONE			AS "PHONE",
        p.VERSION		AS "VERSION", 
        p.ADDRESS		AS "ADDRESS", 
        p.SHORT_NAME		AS "SHORT_NAME", 
        p.NAME			AS "NAME", 
        p.NUMBER		AS "NUMBER", 
        p.SIGN_DELETED		AS "SIGN_DELETED", 
        p.INSERT_DATE		AS "INSERT_DATE", 
        p.UPDATE_DATE		AS "UPDATE_DATE",
        p.TRANSPORT_KIND_MUID	AS "TRANSPORT_KIND_MUID",
        rtk.NAME		AS "RTK_NAME"
      from gis.parks p, gis.ref_transport_kinds rtk
	where p.transport_kind_muid = rtk.muid
	  and rtk.muid = f_rtk_MUID
      ;
 end if;       
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;