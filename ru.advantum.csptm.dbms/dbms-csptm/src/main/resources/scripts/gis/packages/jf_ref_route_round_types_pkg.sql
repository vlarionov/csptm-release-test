CREATE OR REPLACE FUNCTION gis."jf_ref_route_round_types_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
begin 
 open p_rows for 
      select 
        MUID		AS "MUID", 
        CODE		AS "CODE", 
        NAME		AS "NAME", 
        VERSION		AS "VERSION", 
        SIGN_DELETED	AS "SIGN_DELETED", 
        INSERT_DATE	AS "INSERT_DATE", 
        UPDATE_DATE	AS "UPDATE_DATE"
      from gis.ref_route_round_types; 
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;