CREATE OR REPLACE FUNCTION gis."jf_route_variants_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
 ln_route_muid gis.route_variants.route_muid%type := jofl.jofl_pkg$extract_number(p_attr, 'MUID', true);
 lt_tr_type 		gis.ref_transport_kinds.short_name%type := jofl.jofl_pkg$extract_varchar(p_attr, 'RTK_SHORT_NAME', true);
 lt_route_number 	gis.routes.number%type := jofl.jofl_pkg$extract_varchar(p_attr, 'NUMBER', true); 
 
begin 
 open p_rows for 
      select 
        rv.MUID::text			AS "MUID", 
        rv.ERM_ID			AS "ERM_ID", 
        rv.ROUTE_VARIANT_TYPE_MUID AS "ROUTE_VARIANT_TYPE_MUID", 
        rv.ROUTE_MUID::text		AS "ROUTE_MUID", 
        rv.START_DATE		AS "START_DATE", 
        rv.END_DATE		AS "END_DATE", 
        rv.COMMENT			AS "COMMENT", 
        rv.INSERT_DATE		AS "INSERT_DATE", 
        rv.UPDATE_DATE		AS "UPDATE_DATE", 
        rv.VERSION			AS "VERSION", 
        rv.SIGN_DELETED		AS "SIGN_DELETED",
	      lt_tr_type			AS "TR_TYPE",
	      lt_route_number		AS "ROUTE_NUMBER",
        case rv.muid when r.current_route_variant_muid then  true ELSE false END AS "IS_CURRENT",
	'[' || json_build_object('CAPTION', 'Производственные рейсы',
                                 'JUMP', json_build_object('METHOD', 'GIS.ROUTE_ROUNDS', 'ATTR', '{}'))
        || ']' "JUMP_TO_RR",
	'[' || json_build_object('CAPTION', 'Технологические рейсы',
                                 'JUMP', json_build_object('METHOD', 'GIS.ROUTE_NULL_ROUNDS', 'ATTR', '{}'))
        || ']' "JUMP_TO_RNR"
      from gis.route_variants rv
      join gis.routes r on r.muid = rv.route_muid
      where /*rv.MUID = ln_curr_muid*/
	  rv.ROUTE_MUID = ln_route_muid
  order by rv.start_date desc;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION gis."jf_route_variants_pkg$of_rows"(numeric, text)
  OWNER TO adv;
