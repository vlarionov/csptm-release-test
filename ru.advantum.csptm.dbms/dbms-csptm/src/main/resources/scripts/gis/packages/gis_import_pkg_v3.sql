﻿-- ========================================================================
-- Пакет загрузки данных ГИС v3
-- ========================================================================

create or replace function gis.gis_import_pkg_v3$import
  (v_message_code   in text,
   v_data           in bytea)
returns text
-- ========================================================================
-- Загрузка XML файла с данными и обработка
-- ========================================================================
as $$ declare
   v_txt_result     text;
   v_result         bigint;
   v_xml_data       xml := convert_from(v_data, 'UTF8')::xml;
begin
   
   select 'OK' into v_txt_result from gis.gis_xml_message where message_code = v_message_code;
   if not found then
      insert into gis.gis_xml_message(message_code, object_type, object_code, data, status, insert_date)
      values (v_message_code,
              (xpath('/*/header/objectType/text()', v_xml_data))[1]::text, -- object_type
              (xpath('/*/body/*/muid/text()',       v_xml_data))[1]::text, -- object_muid
              v_xml_data,
              'NEW',
              clock_timestamp());
      
      v_txt_result := 'OK';
   end if;
   
   select gis.gis_import_pkg_v3$import_message_data(data) into v_result
     from gis.gis_xml_message
    where message_code = v_message_code;
   
   if found then
      update gis.gis_xml_message set status = 'READY', parse_date = clock_timestamp() where message_code = v_message_code;
   end if;
   
   return v_txt_result;
end;
$$ language plpgsql security definer;


create or replace function gis.gis_import_pkg_v3$import_message_file
  (v_message_code   in text,
   v_data           in bytea)
returns text
-- ========================================================================
-- Загрузка XML файла с данными
-- ========================================================================
as $$ declare
   v_txt_result         text := 'OK';
   v_xml_data           xml;
   v_object_type        text;
   v_object_muid        text;
begin
   
   if v_message_code is null or v_data is null then
      return v_txt_result;      
   end if;
   
   v_xml_data := convert_from(v_data, 'UTF8')::xml;
   v_object_type := (xpath('/*/header/objectType/text()', v_xml_data))[1]::text;
   v_object_muid := (xpath('/*/body/*/muid/text()',       v_xml_data))[1]::text;
   if v_object_type is null or v_object_muid is null then
      return v_txt_result;      
   end if;   
   
   delete from gis.gis_xml_message_deffered where message_code = v_message_code;
   delete from gis.gis_xml_message where message_code = v_message_code;
   
   insert into gis.gis_xml_message(message_code, object_type, object_code, data, status, insert_date)
   values (v_message_code, v_object_type, v_object_muid, v_xml_data, 'NEW', clock_timestamp());
   
   return v_txt_result;
end;
$$ language plpgsql security definer;


create or replace function gis.gis_import_pkg_v3$import_step1
  (v_message_code   in text)
returns text
-- ========================================================================
-- Обработка файла с данными
-- ========================================================================
as $$ declare
   v_txt_result     text := 'OK';
   v_result         bigint;
begin
   
   select gis.gis_import_pkg_v3$import_message_data(data) into v_result
     from gis.gis_xml_message
    where message_code = v_message_code;
   
   if found then
      update gis.gis_xml_message set status = 'READY', parse_date = clock_timestamp() where message_code = v_message_code;
   end if;
   
   return v_txt_result;
end;
$$ language plpgsql security definer;


create or replace function gis.gis_import_pkg_v3$reimport_message_data
  (v_message_code   in text)
returns bigint
-- ========================================================================
-- Импорт объектов сообщения
-- ========================================================================
as $$ declare
   v_result     bigint := 0;
begin
   select gis.gis_import_pkg_v3$import_message_data(data) into v_result
     from gis.gis_xml_message
     where message_code = v_message_code;
   
   return v_result;
end;
$$ language plpgsql security definer;


create or replace function gis.gis_import_pkg_v3$import_message_data
  (v_data         in xml)
returns bigint
-- ========================================================================
-- Импорт объектов из XML типа
-- ========================================================================
as $$ declare
   v_obj_message  record;
   v_result       bigint := 0;
   v_tmp          bigint;
begin
   for v_obj_message in
   (select q.operation, q.n,
           case when ord.import_name is null then q.object_type else ord.import_name end as object_type
      from (select (xpath('/*/header/objectType/text()', v_data))[1]::text as object_type,
                   (xpath('/*/header/operation/text()',  v_data))[1]::text as operation,
                   v_data as n
           ) q
      left join gis.gis_import_order ord on ord.object_type = q.object_type
     order by ord.import_order nulls first
   )
   loop
      v_tmp := gis.gis_import_pkg_v3$import_message_data(v_obj_message.operation, v_obj_message.object_type, v_obj_message.n);
      if v_tmp is not null then
         v_result := v_result + v_tmp;
      end if;
   end loop;
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_message_data
  (v_operation     in text,
   v_object_type   in text,
   v_object_data   in xml)
returns bigint
-- ========================================================================
-- Импорт объектов из XML типа
-- ========================================================================
as $$ declare
   v_result        bigint := 0;
begin
   case v_object_type
      when 'TPark' then
         v_result := gis.gis_import_pkg_v3$import_parks(v_operation, v_object_data);
      when 'TParkZone' then
         v_result := gis.gis_import_pkg_v3$import_park_zones(v_operation, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_rts_zone_location2park_zones(v_operation, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_rts_zone_location2park_zones_territory(v_operation, v_object_data);
      when 'TTransportParking' then
         v_result := gis.gis_import_pkg_v3$import_transport_parkings(v_operation, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_rts_zone_location2transport_parkings(v_operation, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_rts_parking(v_operation, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_rts_parking2zone(v_operation, v_object_data);
      when 'TStopPlace' then
         v_result := gis.gis_import_pkg_v3$import_stop_places(v_operation, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_rts_stop_location(v_operation, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_rts_stop_item(v_operation, v_object_data);
      when 'TDisplayPanelManufacturers' then
         v_result := gis.gis_import_pkg_v3$import_display_panel_manufacturers(v_operation, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_ibrd_manufacturer(v_operation, v_object_data);
      when 'TDisplayPanelTypes' then
         v_result := gis.gis_import_pkg_v3$import_display_panel_types(v_operation, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_ibrd_type(v_operation, v_object_data);
      when 'TDisplayPanelInstallationPlaces' then
         v_result := gis.gis_import_pkg_v3$import_display_panel_installation_places(v_operation, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_ibrd_installation_place(v_operation, v_object_data);
      when 'TDisplayPanelStates' then
         v_result := gis.gis_import_pkg_v3$import_display_panel_states(v_operation, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_ibrd_state(v_operation, v_object_data);
      when 'TMobileOperators' then
         v_result := gis.gis_import_pkg_v3$import_mobile_operators(v_operation, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_ibrd_mobile_operator(v_operation, v_object_data);
      when 'TAdressPrograms' then
         v_result := gis.gis_import_pkg_v3$import_address_programs(v_operation, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_ibrd_address_program(v_operation, v_object_data);
      when 'TDisplayPanelServiceOrganizations' then
         v_result := gis.gis_import_pkg_v3$import_display_panel_service_organizations(v_operation, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_ibrd_service_organization(v_operation, v_object_data);
      when 'TDisplayPanelBalanceOrganizations' then
         v_result := gis.gis_import_pkg_v3$import_display_panel_balance_organizations(v_operation, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_ibrd_balance_organization(v_operation, v_object_data);
      when 'TDisplayPanelModels' then
         v_result := gis.gis_import_pkg_v3$import_display_panel_models(v_operation, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_ibrd_model(v_operation, v_object_data);
      when 'TDisplayPanelProtocols' then
         v_result := gis.gis_import_pkg_v3$import_display_panel_protocols(v_operation, v_object_data);
      when 'TDisplayPanelServiceContracts' then
         v_result := gis.gis_import_pkg_v3$import_display_panel_service_contracts(v_operation, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_ibrd_service_contract(v_operation, v_object_data);
      when 'TDisplayPanels' then
         v_result := gis.gis_import_pkg_v3$import_display_panels(v_operation, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_ibrd_info_board(v_operation, v_object_data);
      when 'TGraphNode' then
         v_result := gis.gis_import_pkg_v2$import_message_data(v_operation, v_object_type, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_rts_graph_nodes(v_operation, v_object_data);
      when 'TGraphSection' then
         v_result := gis.gis_import_pkg_v2$import_message_data(v_operation, v_object_type, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_rts_graph_sections(v_operation, v_object_data);
      when 'TRoute' then
         v_result := gis.gis_import_pkg_v2$import_message_data(v_operation, v_object_type, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_rts_routes(v_operation, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_rts_depo2route(v_operation, v_object_data);
      when 'TRouteVariant' then
         v_result := gis.gis_import_pkg_v2$import_message_data(v_operation, v_object_type, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_rts_route_variants(v_operation, v_object_data);
      when 'TRouteTrajectory' then
         v_result := gis.gis_import_pkg_v3$import_rts_round_prepare(v_operation, v_object_data);
         v_result := gis.gis_import_pkg_v2$import_message_data(v_operation, v_object_type, v_object_data);
         if v_result >= 0 then
            v_result := gis.gis_import_pkg_v3$import_rts_round(v_operation, v_object_data);
            if v_result >= 0 then
               v_result := v_result + gis.gis_import_pkg_v3$import_rts_graph_sec2round(v_operation, v_object_data);
               if v_result >= 0 then
                  v_result := v_result + gis.gis_import_pkg_v3$import_rts_stop_item2round(v_operation, v_object_data);
               end if;
            end if;
         end if;
      when 'TStop' then
         v_result := gis.gis_import_pkg_v2$import_message_data(v_operation, v_object_type, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_rts_stop(v_operation, v_object_data);
      when 'TStopZone' then
         v_result := gis.gis_import_pkg_v2$import_message_data(v_operation, v_object_type, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_rts_zone_location2stop_zones(v_operation, v_object_data);
      when 'TTerminalPointZone' then
         v_result := gis.gis_import_pkg_v2$import_message_data(v_operation, v_object_type, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_rts_zone_location2term_point_zones(v_operation, v_object_data);
      when 'TRouteNullRoundType2' then
         v_result := gis.gis_import_pkg_v3$import_route_null_round_type2(v_operation, v_object_data);
      when 'TRouteRound' then
         v_result := gis.gis_import_pkg_v2$import_route_rounds(v_operation, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_rts_route_rounds(v_operation, v_object_data);
      when 'TRouteNullRound' then
         v_result := gis.gis_import_pkg_v3$import_route_null_rounds(v_operation, v_object_data);
         v_result := v_result + gis.gis_import_pkg_v3$import_rts_route_null_rounds(v_operation, v_object_data);
      else
         v_result := gis.gis_import_pkg_v2$import_message_data(v_operation, v_object_type, v_object_data);
      end case;
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_message_data_step2
  (v_operation     in text,
   v_object_type   in text,
   v_object_data   in xml)
returns bigint
-- ========================================================================
-- Импорт объектов из XML типа
-- ========================================================================
as $$ declare
   v_result        bigint := 0;
begin
   case v_object_type
      when 'TRoute' then
         v_result := gis.gis_import_pkg_v2$import_rts_current_route_variants(v_operation, v_object_data);
   end case;
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_step2()
returns bigint
-- ========================================================================
-- Импорт объектов из XML типа
-- ========================================================================
as $$ declare
   v_obj_message  record;
   v_result       bigint := 0;
   v_tmp          bigint;
begin
   for v_obj_message in
   (select gxd.message_code,
           (xpath('/*/header/objectType/text()', gxm.data))[1]::text as object_type,
           (xpath('/*/header/operation/text()',  gxm.data))[1]::text as operation,
           gxm.data as n
      from gis.gis_xml_message_deffered gxd
      join gis.gis_xml_message gxm on gxm.message_code = gxd.message_code
     where gxd.step = 2
     order by gxd.row_id
   )
   loop
      v_tmp := gis.gis_import_pkg_v3$import_message_data_step2(v_obj_message.operation, v_obj_message.object_type, v_obj_message.n);
      if v_tmp is not null then
         v_result := v_result + v_tmp;
      end if;
      
      delete from gis.gis_xml_message_deffered where message_code = v_obj_message.message_code and step = 2;
      
   end loop;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_cleanup()
returns bigint
-- ========================================================================
-- Удаление вспомогательных данных
-- ========================================================================
as $$ declare
   v_result       bigint := 0;
   v_tmp          bigint;
begin
   
   delete from rts.stop_item2gis as g
    where g.stop_item_id in
          (select s.stop_item_id from rts.stop_item s
            where s.sign_deleted
              and not exists (select 1 from rts.stop_item_type where stop_item_type.stop_item_id = s.stop_item_id)
              and not exists (select 1 from rts.stop_item2round where rts.stop_item2round.stop_item_id = s.stop_item_id));
   
   get diagnostics v_result = row_count;
   
   delete from rts.stop_item s
    where s.sign_deleted
      and not exists (select 1 from rts.stop_item_type where stop_item_type.stop_item_id = s.stop_item_id)
      and not exists (select 1 from rts.stop_item2round where rts.stop_item2round.stop_item_id = s.stop_item_id);
   
   get diagnostics v_tmp = row_count;
   v_result := v_result + v_tmp;
   
   return v_result;
end;
$$ language plpgsql;

-- ========================================================================
-- Загрузка сообщений в модель gis

create or replace function gis.gis_import_pkg_v3$import_parks
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Парков из XML типа
-- ========================================================================
as $$ declare
   v_result bigint := 0;
begin
   with parsed as (
      select (xpath('/*/body/*/muid/text()',               v_data))[1]::text::bigint muid,
             (xpath('/*/header/identSet/componentId[component[text()="ERM"]]/componentIdent/text()',
                                                           v_data))[1]::text::bigint erm_id,
             (xpath('/*/body/*/agency/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                           v_data))[1]::text::bigint agency_muid, 
             (xpath('/*/body/*/number/text()',             v_data))[1]::text n_number,
             (xpath('/*/body/*/name/text()',               v_data))[1]::text s_name,
             (xpath('/*/body/*/shortName/text()',          v_data))[1]::text short_name,
             (xpath('/*/body/*/routeTransportKind/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                    v_data))[1]::text::bigint transport_kind_muid,
             (xpath('/*/body/*/addressString/text()',      v_data))[1]::text address,
             (xpath('/*/body/*/phone/text()',              v_data))[1]::text phone,
             (xpath('/*/body/*/version/text()',            v_data))[1]::text::bigint n_version,
             case
             when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE'
             then 1
             else 0
             end sign_deleted,
             clock_timestamp() cur_time
   )
   insert into gis.parks as p (muid,
                               erm_id,
                               agency_muid,
                               number,
                               name,
                               short_name,
                               transport_kind_muid,
                               address,
                               phone,
                               version,
                               sign_deleted,
                               insert_date)
   select muid, 
          erm_id, 
          agency_muid, 
          n_number, 
          s_name, 
          short_name, 
          transport_kind_muid, 
          address, 
          phone, 
          n_version, 
          sign_deleted, 
          cur_time
     from parsed
    where sign_deleted = 0 or exists (select 1 from gis.parks sq where sq.muid = parsed.muid)
   on conflict (muid)
   do update 
         set (erm_id,
              agency_muid,
              number,
              name,
              short_name,
              transport_kind_muid,
              address,
              phone,
              version,
              sign_deleted,
              update_date) =
             (excluded.erm_id,
              excluded.agency_muid,
              excluded.number,
              excluded.name,
              excluded.short_name,
              excluded.transport_kind_muid,
              excluded.address,
              excluded.phone,
              excluded.version,
              excluded.sign_deleted,
              excluded.insert_date)
        where p.erm_id               is distinct from excluded.erm_id or
              p.agency_muid          is distinct from excluded.agency_muid or
              p.number               is distinct from excluded.number or
              p.name                 is distinct from excluded.name or
              p.short_name           is distinct from excluded.short_name or
              p.address              is distinct from excluded.address or
              p.phone                is distinct from excluded.phone or
              p.version              is distinct from excluded.version or
              p.sign_deleted         is distinct from excluded.sign_deleted;
   
   get diagnostics v_result = row_count;
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_park_zones
  (v_operation        in text,
   v_data             in xml)
returns bigint
-- ========================================================================
-- Импорт Зон парков из XML типа
-- ========================================================================
as $$ declare
   v_result           bigint := 0;
   v_tmp              bigint;
   v_park_zone_muid   bigint;
begin
   with parsed as 
   (select (xpath('/*/body/*/muid/text()',        v_data))[1]::text::bigint muid,
           (xpath('/*/body/*/muid/text()',        v_data))[1]::text::bigint fhd_id,
           (xpath('/*/body/*/route_transport_kind/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                  v_data))[1]::text::bigint transport_kind_muid,
           (xpath('/*/body/*/number/text()',      v_data))[1]::text::bigint park_zones_num,
           (xpath('/*/body/*/wkt_geom/text()',    v_data))[1]::text wkt_geom,
           st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text) geom,
           (xpath('/*/body/*/name/text()',        v_data))[1]::text s_name,
           (xpath('/*/body/*/short_name/text()',  v_data))[1]::text short_name,
           (xpath('/*/body/*/street_muid/body/name/text()',
                                                  v_data))[1]::text street,
           (xpath('/*/body/*/address/text()',     v_data))[1]::text address,
           (xpath('/*/body/*/phone/text()',       v_data))[1]::text phone,
           (xpath('/*/body/*/version/text()',  v_data))[1]::text::bigint n_version,
           case when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
           clock_timestamp() cur_time
   )
   insert into gis.park_zones as p (muid,
                                    fhd_id,
                                    transport_kind_muid,
                                    park_zones_num,
                                    wkt_geom,
                                    geom,
                                    name,
                                    short_name,
                                    street,
                                    address,
                                    phone,
                                    version,
                                    sign_deleted,
                                    insert_date)
   select muid, 
          fhd_id,
          transport_kind_muid,
          park_zones_num,
          wkt_geom,
          geom,
          s_name,
          short_name,
          street,
          address,
          phone,
          n_version,
          sign_deleted,
          cur_time
     from parsed 
   on conflict (muid)
   do update set (fhd_id,
                  transport_kind_muid,
                  park_zones_num,
                  wkt_geom,
                  geom,
                  name,
                  short_name,
                  street,
                  address,
                  phone,
                  version,
                  sign_deleted,
                  update_date) =
                 (excluded.fhd_id,
                  excluded.transport_kind_muid,
                  excluded.park_zones_num,
                  excluded.wkt_geom,
                  excluded.geom,
                  excluded.name,
                  excluded.short_name,
                  excluded.street,
                  excluded.address,
                  excluded.phone,
                  excluded.version,
                  excluded.sign_deleted,
                  excluded.insert_date)
       where p.wkt_geom     is distinct from excluded.wkt_geom   or
             p.fhd_id       is distinct from excluded.fhd_id     or
             p.transport_kind_muid is distinct from excluded.transport_kind_muid or
             p.park_zones_num      is distinct from excluded.park_zones_num or
             p.wkt_geom     is distinct from excluded.wkt_geom   or
             p.name         is distinct from excluded.name       or
             p.short_name   is distinct from excluded.short_name or
             p.street       is distinct from excluded.street     or 
             p.address      is distinct from excluded.address    or
             p.phone        is distinct from excluded.phone      or
             p.version      is distinct from excluded.version    or
             p.sign_deleted is distinct from excluded.sign_deleted;
   
   get diagnostics v_result = row_count;
   
   v_park_zone_muid := (select (xpath('/*/body/*/muid/text()', v_data))[1]::text::bigint);
   update gis.parks2park_zones set sign_deleted = 1 where park_zone_muid = v_park_zone_muid;
   
   with parsed as 
   (select (xpath('/park/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                            sp_data))[1]::text::bigint park_muid,
           v_park_zone_muid as park_zone_muid,
           (xpath('/*/body/*/version/text()', v_data))[1]::text::bigint n_version,
           case when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
           clock_timestamp() cur_time
      from (select unnest(xpath('/*/body/*/parks/park', v_data)) as sp_data) t
   )
   insert into gis.parks2park_zones as p (park_muid,
                                          park_zone_muid,
                                          version,
                                          sign_deleted,
                                          insert_date)
   select park_muid,
          park_zone_muid,
          n_version,
          sign_deleted,
          cur_time
     from parsed
    where sign_deleted = 0 
       or exists (select 1 from gis.parks2park_zones sq
                   where sq.park_muid = parsed.park_muid
                     and sq.park_zone_muid = parsed.park_zone_muid)
   on conflict (park_muid, park_zone_muid)
   do update set (version,
                  sign_deleted,
                  update_date) =
                 (excluded.version,
                  excluded.sign_deleted,
                  excluded.insert_date)
       where p.version      is distinct from excluded.version or
             p.sign_deleted is distinct from excluded.sign_deleted;
   
   get diagnostics v_tmp = row_count;
   v_result := v_result + v_tmp;
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_transport_parkings
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Отстойных площадок из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
   with parsed as
   (select (xpath('/*/body/*/muid/text()',          v_data))[1]::text::bigint muid,
           (xpath('/*/body/*/park/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                    v_data))[1]::text::bigint park_muid,
           (xpath('/*/body/*/wkt_geom/text()',      v_data))[1]::text wkt_geom,
           st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text) geom,
           (xpath('/*/body/*/name/text()',          v_data))[1]::text s_name,
           (xpath('/*/body/*/addressString/text()', v_data))[1]::text address,
           (xpath('/*/body/*/phone/text()',         v_data))[1]::text phone,
           (xpath('/*/body/*/square/text()',        v_data))[1]::text::real square,
           (xpath('/*/body/*/capacity/text()',      v_data))[1]::text::bigint capacity,
           (xpath('/*/body/*/hasBus/text()',        v_data))[1]::text::bigint has_bus, 
           (xpath('/*/body/*/hasTrolley/text()',    v_data))[1]::text::bigint has_trolley, 
           (xpath('/*/body/*/hasTram/text()',       v_data))[1]::text::bigint has_tram, 
           (xpath('/*/body/*/hasSpeedTram/text()',  v_data))[1]::text::bigint has_speedtram, 
           (xpath('/*/body/*/version/text()',       v_data))[1]::text::bigint n_version,
           case when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
           clock_timestamp() cur_time
   )
   insert into gis.transport_parkings as gn (muid, 
                                             park_muid,
                                             wkt_geom,
                                             geom,
                                             name,
                                             address,
                                             phone,
                                             square,
                                             capacity,
                                             has_bus,
                                             has_trolley,
                                             has_tram,
                                             has_speedtram,
                                             version,
                                             sign_deleted,
                                             insert_date)
   select muid, 
          park_muid,
          wkt_geom,
          geom,
          s_name,
          address,
          phone,
          square,
          capacity,
          has_bus,
          has_trolley,
          has_tram,
          has_speedtram,
          n_version,
          sign_deleted,
          cur_time
     from parsed
    where sign_deleted = 0 or exists (select 1 from gis.transport_parkings sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (park_muid, 
                   wkt_geom, 
                   geom, 
                   name, 
                   address, 
                   phone, 
                   square, 
                   capacity, 
                   has_bus, 
                   has_trolley, 
                   has_tram, 
                   has_speedtram, 
                   version, 
                   sign_deleted,
                   update_date) = 
                  (excluded.park_muid, 
                   excluded.wkt_geom, 
                   excluded.geom, 
                   excluded.name, 
                   excluded.address, 
                   excluded.phone, 
                   excluded.square, 
                   excluded.capacity, 
                   excluded.has_bus, 
                   excluded.has_trolley, 
                   excluded.has_tram, 
                   excluded.has_speedtram, 
                   excluded.version, 
                   excluded.sign_deleted,
                   excluded.insert_date)
    where gn.park_muid     is distinct from excluded.park_muid or
          gn.wkt_geom      is distinct from excluded.wkt_geom  or
          gn.geom          is distinct from excluded.geom      or 
          gn.name          is distinct from excluded.name      or 
          gn.address       is distinct from excluded.address   or 
          gn.phone         is distinct from excluded.phone     or 
          gn.square        is distinct from excluded.square    or 
          gn.capacity      is distinct from excluded.capacity  or 
          gn.has_bus       is distinct from excluded.has_bus   or 
          gn.has_trolley   is distinct from excluded.has_trolley   or 
          gn.has_tram      is distinct from excluded.has_tram  or 
          gn.has_speedtram is distinct from excluded.has_speedtram or 
          gn.version       is distinct from excluded.version   or
          gn.sign_deleted  is distinct from excluded.sign_deleted;
    
    get diagnostics v_result = row_count;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_stop_places
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Остановочных пунктов из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as 
    (select (xpath('/*/body/*/muid/text()',                     v_data))[1]::text::bigint muid,
            (xpath('/*/header/identSet/componentId[component[text()="ERM"]]/componentIdent/text()', 
                                                                v_data))[1]::text::bigint erm_id,
            (xpath('/*/body/*/stop/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                                v_data))[1]::text::bigint stop_muid,
            (xpath('/*/body/*/state/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                                v_data))[1]::text::bigint stop_state_muid,
            (xpath('/*/body/*/mode/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                                v_data))[1]::text::bigint stop_mode_muid,
            (xpath('/*/body/*/wkt_geom/text()',                 v_data))[1]::text         wkt_geom,
            st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text)        geom,
            (xpath('/*/body/*/suffix/text()',                   v_data))[1]::text         suffix,
            (xpath('/*/body/*/park_zone/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                v_data))[1]::text::bigint park_zone_muid,
            (xpath('/*/body/*/transport_static_object/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                v_data))[1]::text::bigint transport_static_object_muid,
            (xpath('/*/body/*/transport_parking/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                v_data))[1]::text::bigint transport_parking_muid,
            (xpath('/*/body/*/graphSection/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                                v_data))[1]::text::bigint graph_section_muid,
            (xpath('/*/body/*/graphSectionOffset/text()',       v_data))[1]::text::real   graph_section_offset,
            (xpath('/*/body/*/graphTramSection/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                                v_data))[1]::text::bigint graph_tram_section_muid,
            (xpath('/*/body/*/graphTramSectionOffset/text()',   v_data))[1]::text::real   graph_tram_section_offset,
            (xpath('/*/body/*/isTechnical/text()',              v_data))[1]::text::bigint is_technical,
            (xpath('/*/body/*/hasFacilitiesForDisabled/text()', v_data))[1]::text::bigint has_facilities_for_disabled,
            (xpath('/*/body/*/hasBus/text()',                   v_data))[1]::text::bigint has_bus,
            (xpath('/*/body/*/hasTrolley/text()',               v_data))[1]::text::bigint has_trolley,
            (xpath('/*/body/*/hasTram/text()',                  v_data))[1]::text::bigint has_tram,
            (xpath('/*/body/*/hasSpeedTram/text()',             v_data))[1]::text::bigint has_speedtram,
            (xpath('/*/body/*/startDate/text()',                v_data))[1]::text::date   start_date,
            (xpath('/*/body/*/changeDate/text()',               v_data))[1]::text::date   change_date,
            (xpath('/*/body/*/endDate/text()',                  v_data))[1]::text::date   end_date,
            (xpath('/*/body/*/buildingDistance/text()',         v_data))[1]::text::real   building_distance,
            (xpath('/*/body/*/buildingAddressString/text()',    v_data))[1]::text         building_address,
            (xpath('/*/body/*/comment/text()',                  v_data))[1]::text         s_comment,
            (xpath('/*/body/*/version/text()',                  v_data))[1]::text::bigint n_version,
            case when (xpath('/*/body/*/signDeleted/text()',    v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
            clock_timestamp() cur_time
    )
    insert into gis.stop_places as sp (muid,
                                       erm_id,
                                       stop_muid,
                                       stop_state_muid,
                                       stop_mode_muid,
                                       wkt_geom,
                                       geom,
                                       suffix,
                                       park_zone_muid, 
                                       transport_parking_muid,
                                       transport_static_object_muid,
                                       graph_section_muid,
                                       graph_section_offset,
                                       graph_tram_section_muid,
                                       graph_tram_section_offset,
                                       is_technical,
                                       has_facilities_for_disabled,
                                       has_bus,
                                       has_trolley,
                                       has_tram,
                                       has_speedtram,
                                       start_date,
                                       change_date,
                                       end_date,
                                       building_distance,
                                       building_address,
                                       comment,
                                       version,
                                       sign_deleted,
                                       insert_date)
    select muid,
           erm_id,
           stop_muid,
           stop_state_muid,
           stop_mode_muid,
           wkt_geom,
           geom,
           suffix,
           park_zone_muid,
           transport_parking_muid,
           transport_static_object_muid,
           graph_section_muid,
           graph_section_offset,
           graph_tram_section_muid,
           graph_tram_section_offset,
           is_technical,
           has_facilities_for_disabled,
           has_bus,
           has_trolley,
           has_tram,
           has_speedtram,
           start_date,
           change_date,
           end_date,
           building_distance,
           building_address,
           s_comment,
           n_version,
           sign_deleted,
           cur_time
      from parsed
    where sign_deleted = 0 or exists (select 1 from gis.stop_places sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (erm_id,
                   stop_muid,
                   stop_state_muid,
                   stop_mode_muid,
                   wkt_geom,
                   geom,
                   suffix,
                   park_zone_muid,
                   transport_parking_muid,
                   transport_static_object_muid,
                   graph_section_muid,
                   graph_section_offset,
                   graph_tram_section_muid,
                   graph_tram_section_offset,
                   is_technical,
                   has_facilities_for_disabled,
                   has_bus,
                   has_trolley,
                   has_tram,
                   has_speedtram,
                   start_date,
                   change_date,
                   end_date,
                   building_distance,
                   building_address,
                   comment,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.erm_id,
                   excluded.stop_muid,
                   excluded.stop_state_muid,
                   excluded.stop_mode_muid,
                   excluded.wkt_geom,
                   excluded.geom,
                   excluded.suffix,
                   excluded.park_zone_muid,
                   excluded.transport_parking_muid,
                   excluded.transport_static_object_muid,
                   excluded.graph_section_muid,
                   excluded.graph_section_offset,
                   excluded.graph_tram_section_muid,
                   excluded.graph_tram_section_offset,
                   excluded.is_technical,
                   excluded.has_facilities_for_disabled,
                   excluded.has_bus,
                   excluded.has_trolley,
                   excluded.has_tram,
                   excluded.has_speedtram,
                   excluded.start_date,
                   excluded.change_date,
                   excluded.end_date,
                   excluded.building_distance,
                   excluded.building_address,
                   excluded.comment,
                   excluded.version,
                   excluded.sign_deleted,
                   excluded.insert_date)
    where sp.erm_id            is distinct from excluded.erm_id or
          sp.stop_muid         is distinct from excluded.stop_muid or
          sp.stop_state_muid   is distinct from excluded.stop_state_muid or
          sp.stop_mode_muid    is distinct from excluded.stop_mode_muid or
          sp.wkt_geom          is distinct from excluded.wkt_geom or
          sp.suffix            is distinct from excluded.suffix or
          sp.park_zone_muid    is distinct from excluded.park_zone_muid or
          sp.transport_parking_muid        is distinct from excluded.transport_parking_muid or
          sp.transport_static_object_muid  is distinct from excluded.transport_static_object_muid or
          sp.graph_section_muid            is distinct from excluded.graph_section_muid or
          sp.graph_section_offset          is distinct from excluded.graph_section_offset or
          sp.graph_tram_section_muid       is distinct from excluded.graph_tram_section_muid or
          sp.graph_tram_section_offset     is distinct from excluded.graph_tram_section_offset or
          sp.is_technical      is distinct from excluded.is_technical or
          sp.has_facilities_for_disabled   is distinct from excluded.has_facilities_for_disabled or
          sp.has_bus           is distinct from excluded.has_bus or
          sp.has_trolley       is distinct from excluded.has_trolley or
          sp.has_tram          is distinct from excluded.has_tram or
          sp.has_speedtram     is distinct from excluded.has_speedtram or
          sp.start_date        is distinct from excluded.start_date or
          sp.change_date       is distinct from excluded.change_date or
          sp.end_date          is distinct from excluded.end_date or
          sp.building_distance is distinct from excluded.building_distance or
          sp.building_address  is distinct from excluded.building_address or
          sp.comment           is distinct from excluded.comment or
          sp.version           is distinct from excluded.version or
          sp.sign_deleted      is distinct from excluded.sign_deleted;
    
    get diagnostics v_result = row_count;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_route_null_rounds
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Нулевых рейсов из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
   with parsed as 
   (select (xpath('/*/body/*/muid/text()',                  v_data))[1]::text::bigint muid,
           (xpath('/*/body/*/route_null_round_type_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                            v_data))[1]::text::bigint route_null_round_type_muid,
           (xpath('/*/body/*/type2_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                            v_data))[1]::text::bigint route_null_round_type2_muid,
           (xpath('/*/body/*/routeVariant/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                            v_data))[1]::text::bigint route_variant_muid,
           (xpath('/*/body/*/code/text()',                  v_data))[1]::text         s_code,
           (xpath('/*/body/*/stopPlaceA/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                            v_data))[1]::text::bigint stop_place_a_muid,
           (xpath('/*/body/*/stopPlaceB/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                            v_data))[1]::text::bigint stop_place_b_muid,
           (xpath('/*/body/*/comment/text()',               v_data))[1]::text         s_comment,
           (xpath('/*/body/*/version/text()',               v_data))[1]::text::bigint n_version,
           case when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
           clock_timestamp() cur_time
   )
   insert into gis.route_null_rounds as rnr
         (muid, route_null_round_type_muid, route_null_round_type2_muid, route_variant_muid, code,
          stop_place_a_muid, stop_place_b_muid,   comment,   version, sign_deleted, insert_date)
   select muid, route_null_round_type_muid, route_null_round_type2_muid, route_variant_muid, s_code,
          stop_place_a_muid, stop_place_b_muid, s_comment, n_version, sign_deleted, cur_time
     from parsed
    where sign_deleted = 0 or exists (select 1 from gis.route_null_rounds sq where sq.muid = parsed.muid)
   on conflict (muid)
   do update set (route_null_round_type_muid, route_null_round_type2_muid, route_variant_muid, code,
                  stop_place_a_muid, stop_place_b_muid, comment, version, sign_deleted, insert_date) =
                 (excluded.route_null_round_type_muid,
                  excluded.route_null_round_type2_muid,
                  excluded.route_variant_muid,
                  excluded.code,
                  excluded.stop_place_a_muid,
                  excluded.stop_place_b_muid,
                  excluded.comment,
                  excluded.version,
                  excluded.sign_deleted,
                  excluded.insert_date)
            where rnr.route_null_round_type_muid is distinct from excluded.route_null_round_type_muid or
                  rnr.route_null_round_type2_muid is distinct from excluded.route_null_round_type2_muid or
                  rnr.route_variant_muid is distinct from excluded.route_variant_muid or
                  rnr.code               is distinct from excluded.code or
                  rnr.stop_place_a_muid  is distinct from excluded.stop_place_a_muid or
                  rnr.stop_place_b_muid  is distinct from excluded.stop_place_b_muid or
                  rnr.comment            is distinct from excluded.comment or
                  rnr.version            is distinct from excluded.version or
                  rnr.sign_deleted       is distinct from excluded.sign_deleted;
    
    get diagnostics v_result = row_count;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_route_trajectory_types
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Видов транспорта маршрута из XML типа
-- ========================================================================
as $$ declare
    v_result    bigint := 0;
begin
    with parsed as (
        select (xpath('/object-message/body/*/muid/text()',      v_data))[1]::text::bigint muid,
               (xpath('/object-message/body/*/name/text()',      v_data))[1]::text s_name,
               --(xpath('/object-message/body/*/shortName/text()', v_data))[1]::text short_name,
               (xpath('/object-message/body/*/version/text()',   v_data))[1]::text::bigint n_version,
               case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
               clock_timestamp() cur_time
    )
    insert into gis.ref_route_trajectory_types as gn
          (muid,   name, /* short_name, */   version, sign_deleted, insert_date)
    select muid, s_name, /* short_name, */ n_version, sign_deleted, cur_time
      from parsed
     where sign_deleted = 0 or exists (select 1 from gis.ref_transport_kinds sq where sq.muid = parsed.muid)
    on conflict (muid)
    do update set (name,
                   --short_name,
                   version,
                   sign_deleted,
                   update_date) =
                  (excluded.name,
                   --excluded.short_name,
                   excluded.version, 
                   excluded.sign_deleted,
                   excluded.insert_date)
    where gn.name         is distinct from excluded.name or
          --gn.short_name   is distinct from excluded.short_name or
          gn.version      is distinct from excluded.version or
          gn.sign_deleted is distinct from excluded.sign_deleted;
   
   GET DIAGNOSTICS v_result = ROW_COUNT;
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_display_panel_manufacturers
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Производители ИТ
-- ========================================================================
as $$ declare
   v_result    bigint := 0;
begin
   with parsed as 
   (select (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint    muid,
           (xpath('/object-message/body/*/name/text()', v_data))[1]::text            s_name,
           (xpath('/object-message/body/*/version/text()', v_data))[1]::text::bigint n_version,
           case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
           clock_timestamp() cur_time
   )
   insert into gis.display_panel_manufacturers as p (muid, 
                                                     name, 
                                                     version, 
                                                     sign_deleted, 
                                                     insert_date)
   select muid,
          s_name,
          n_version,
          sign_deleted,
          cur_time
     from parsed
    where sign_deleted = 0 or exists (select 1 from gis.display_panel_manufacturers sq where sq.muid = parsed.muid)
   on conflict (muid)
   do update set (name,
                  version,
                  sign_deleted,
                  update_date) =
                 (excluded.name,
                  excluded.version,
                  excluded.sign_deleted,
                  excluded.insert_date)
       where p.name         is distinct from excluded.name or
             p.version      is distinct from excluded.version or
             p.sign_deleted is distinct from excluded.sign_deleted;
    
    get diagnostics v_result = row_count;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_ibrd_manufacturer
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Производитель ИТОП ibrd
-- ========================================================================
as $$ declare
   v_result     bigint := 0;
   v_muid       bigint := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_name       text   := (xpath('/object-message/body/*/name/text()', v_data))[1]::text;
   v_is_deleted boolean := case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end;
   v_manufacturer_id integer;
begin
   
   select manufacturer_id into v_manufacturer_id from ibrd.manufacturer2gis where muid = v_muid;
   
   if not found then
      insert into ibrd.manufacturer(is_deleted, name) values (v_is_deleted, v_name)
      returning manufacturer_id into v_manufacturer_id;
      
      insert into ibrd.manufacturer2gis(manufacturer_id, muid) values (v_manufacturer_id, v_muid);
      v_result := 1;
   else
      update ibrd.manufacturer
         set name = v_name,
             is_deleted = v_is_deleted
       where manufacturer_id = v_manufacturer_id
         and (name is distinct from v_name or is_deleted is distinct from v_is_deleted);
      
      get diagnostics v_result = row_count;
   end if;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_display_panel_types
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Типы ИТ
-- ========================================================================
as $$ declare
   v_result    bigint := 0;
begin
   with parsed as 
   (select (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint    muid,
           (xpath('/object-message/body/*/name/text()', v_data))[1]::text            s_name,
           (xpath('/object-message/body/*/version/text()', v_data))[1]::text::bigint n_version,
           case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
           clock_timestamp() cur_time
   )
   insert into gis.display_panel_types as p (muid, 
                                                     name, 
                                                     version, 
                                                     sign_deleted, 
                                                     insert_date)
   select muid,
          s_name,
          n_version,
          sign_deleted,
          cur_time
     from parsed
    where sign_deleted = 0 or exists (select 1 from gis.display_panel_types sq where sq.muid = parsed.muid)
   on conflict (muid)
   do update set (name,
                  version,
                  sign_deleted,
                  update_date) =
                 (excluded.name,
                  excluded.version,
                  excluded.sign_deleted,
                  excluded.insert_date)
       where p.name         is distinct from excluded.name or
             p.version      is distinct from excluded.version or
             p.sign_deleted is distinct from excluded.sign_deleted;
    
   get diagnostics v_result = row_count;
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_ibrd_type
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Тип ИТОП ibrd
-- ========================================================================
as $$ declare
   v_result     bigint := 0;
   v_muid       bigint := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_name       text   := (xpath('/object-message/body/*/name/text()', v_data))[1]::text;
   v_is_deleted boolean := case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end;
   v_type_id    integer;
begin
   
   select type_id into v_type_id from ibrd.type2gis where muid = v_muid;
   
   if not found then
      
      insert into ibrd.type (is_deleted, name) values (v_is_deleted, v_name)
      returning type_id into v_type_id;
      
      insert into ibrd.type2gis(type_id, muid) values (v_type_id, v_muid);
      v_result := 1;
   else
      update ibrd.type
         set name       = v_name,
             is_deleted = v_is_deleted
       where type_id = v_type_id
         and (name is distinct from v_name or is_deleted is distinct from v_is_deleted);
      
      get diagnostics v_result = row_count;
   end if;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_display_panel_installation_places
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Типы ИТ
-- ========================================================================
as $$ declare
   v_result    bigint := 0;
begin
   with parsed as 
   (select (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint    muid,
           (xpath('/object-message/body/*/name/text()', v_data))[1]::text            s_name,
           (xpath('/object-message/body/*/version/text()', v_data))[1]::text::bigint n_version,
           case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
           clock_timestamp() cur_time
   )
   insert into gis.display_panel_installation_places as p (muid, 
                                                           name, 
                                                           version, 
                                                           sign_deleted, 
                                                           insert_date)
   select muid,
          s_name,
          n_version,
          sign_deleted,
          cur_time
     from parsed
    where sign_deleted = 0 or exists (select 1 from gis.display_panel_installation_places sq where sq.muid = parsed.muid)
   on conflict (muid)
   do update set (name,
                  version,
                  sign_deleted,
                  update_date) =
                 (excluded.name,
                  excluded.version,
                  excluded.sign_deleted,
                  excluded.insert_date)
       where p.name         is distinct from excluded.name or
             p.version      is distinct from excluded.version or
             p.sign_deleted is distinct from excluded.sign_deleted;
    
    get diagnostics v_result = row_count;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_ibrd_installation_place
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Место установки ИТОП ibrd
-- ========================================================================
as $$ declare
   v_result     bigint := 0;
   v_muid       bigint := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_name       text   := (xpath('/object-message/body/*/name/text()', v_data))[1]::text;
   v_is_deleted boolean := case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end;
   v_installation_place_id integer;
begin
   
   select installation_place_id into v_installation_place_id from ibrd.installation_place2gis where muid = v_muid;
   
   if not found then
      
      insert into ibrd.installation_place(is_deleted, name) values (v_is_deleted, v_name)
      returning installation_place_id into v_installation_place_id;
      
      insert into ibrd.installation_place2gis(installation_place_id, muid) values (v_installation_place_id, v_muid);
      v_result := 1;
   else
      update ibrd.installation_place
         set name       = v_name,
             is_deleted = v_is_deleted
       where installation_place_id = v_installation_place_id
         and (name is distinct from v_name or is_deleted is distinct from v_is_deleted);
      
      get diagnostics v_result = row_count;
   end if;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_display_panel_states
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Статусы ИТ
-- ========================================================================
as $$ declare
   v_result    bigint := 0;
begin
   with parsed as 
   (select (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint    muid,
           (xpath('/object-message/body/*/name/text()', v_data))[1]::text            s_name,
           (xpath('/object-message/body/*/version/text()', v_data))[1]::text::bigint n_version,
           case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
           clock_timestamp() cur_time
   )
   insert into gis.display_panel_states as p (muid, 
                                              name, 
                                              version, 
                                              sign_deleted, 
                                              insert_date)
   select muid,
          s_name,
          n_version,
          sign_deleted,
          cur_time
     from parsed
    where sign_deleted = 0 or exists (select 1 from gis.display_panel_states sq where sq.muid = parsed.muid)
   on conflict (muid)
   do update set (name,
                  version,
                  sign_deleted,
                  update_date) =
                 (excluded.name,
                  excluded.version,
                  excluded.sign_deleted,
                  excluded.insert_date)
       where p.name         is distinct from excluded.name or
             p.version      is distinct from excluded.version or
             p.sign_deleted is distinct from excluded.sign_deleted;
    
    get diagnostics v_result = row_count;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_ibrd_state
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Статус ИТОП ibrd
-- ========================================================================
as $$ declare
   v_result     bigint := 0;
   v_muid       bigint := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_name       text   := (xpath('/object-message/body/*/name/text()', v_data))[1]::text;
   v_is_deleted boolean := case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end;
   v_state_id    integer;
begin
   
   select state_id into v_state_id from ibrd.state2gis where muid = v_muid;
   
   if not found then
      
      insert into ibrd.state (is_deleted, name) values (v_is_deleted, v_name)
      returning state_id into v_state_id;
      
      insert into ibrd.state2gis(state_id, muid) values (v_state_id, v_muid);
      v_result := 1;
   else
      update ibrd.state
         set name       = v_name,
             is_deleted = v_is_deleted
       where state_id = v_state_id
         and (name is distinct from v_name or is_deleted is distinct from v_is_deleted);
      
      get diagnostics v_result = row_count;
   end if;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_mobile_operators
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Операторы сотовой связи
-- ========================================================================
as $$ declare
   v_result    bigint := 0;
begin
   with parsed as 
   (select (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint    muid,
           (xpath('/object-message/body/*/name/text()', v_data))[1]::text            s_name,
           (xpath('/object-message/body/*/version/text()', v_data))[1]::text::bigint n_version,
           case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
           clock_timestamp() cur_time
   )
   insert into gis.mobile_operators as p (muid, 
                                              name, 
                                              version, 
                                              sign_deleted, 
                                              insert_date)
   select muid,
          s_name,
          n_version,
          sign_deleted,
          cur_time
     from parsed
    where sign_deleted = 0 or exists (select 1 from gis.mobile_operators sq where sq.muid = parsed.muid)
   on conflict (muid)
   do update set (name,
                  version,
                  sign_deleted,
                  update_date) =
                 (excluded.name,
                  excluded.version,
                  excluded.sign_deleted,
                  excluded.insert_date)
       where p.name         is distinct from excluded.name or
             p.version      is distinct from excluded.version or
             p.sign_deleted is distinct from excluded.sign_deleted;
    
    get diagnostics v_result = row_count;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_ibrd_mobile_operator
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Оператор сотовой связи ibrd
-- ========================================================================
as $$ declare
   v_result     bigint := 0;
   v_muid       bigint := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_name       text   := (xpath('/object-message/body/*/name/text()', v_data))[1]::text;
   v_is_deleted boolean := case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end;
   v_mobile_operator_id integer;
begin
   
   select mobile_operator_id into v_mobile_operator_id from ibrd.mobile_operator2gis where muid = v_muid;
   
   if not found then
      
      insert into ibrd.mobile_operator (is_deleted, name) values (v_is_deleted, v_name)
      returning mobile_operator_id into v_mobile_operator_id;
      
      insert into ibrd.mobile_operator2gis(mobile_operator_id, muid) values (v_mobile_operator_id, v_muid);
      v_result := 1;
   else
      update ibrd.mobile_operator
         set name       = v_name,
             is_deleted = v_is_deleted
       where mobile_operator_id = v_mobile_operator_id
         and (name is distinct from v_name or is_deleted is distinct from v_is_deleted);
      
      get diagnostics v_result = row_count;
   end if;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_address_programs
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Адресные программы
-- ========================================================================
as $$ declare
   v_result    bigint := 0;
begin
   with parsed as 
   (select (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint    muid,
           (xpath('/object-message/body/*/name/text()', v_data))[1]::text            s_name,
           (xpath('/object-message/body/*/version/text()', v_data))[1]::text::bigint n_version,
           case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
           clock_timestamp() cur_time
   )
   insert into gis.address_programs as p (muid, 
                                          name, 
                                          version, 
                                          sign_deleted, 
                                          insert_date)
   select muid,
          s_name,
          n_version,
          sign_deleted,
          cur_time
     from parsed
    where sign_deleted = 0 or exists (select 1 from gis.address_programs sq where sq.muid = parsed.muid)
   on conflict (muid)
   do update set (name,
                  version,
                  sign_deleted,
                  update_date) =
                 (excluded.name,
                  excluded.version,
                  excluded.sign_deleted,
                  excluded.insert_date)
       where p.name         is distinct from excluded.name or
             p.version      is distinct from excluded.version or
             p.sign_deleted is distinct from excluded.sign_deleted;
    
    get diagnostics v_result = row_count;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_ibrd_address_program
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Адресная программа ibrd
-- ========================================================================
as $$ declare
   v_result     bigint := 0;
   v_muid       bigint := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_name       text   := (xpath('/object-message/body/*/name/text()', v_data))[1]::text;
   v_is_deleted boolean := case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end;
   v_address_program_id integer;
begin
   
   select address_program_id into v_address_program_id from ibrd.address_program2gis where muid = v_muid;
   
   if not found then
      
      insert into ibrd.address_program (is_deleted, name) values (v_is_deleted, v_name)
      returning address_program_id into v_address_program_id;
      
      insert into ibrd.address_program2gis(address_program_id, muid) values (v_address_program_id, v_muid);
      v_result := 1;
   else
      update ibrd.address_program
         set name       = v_name,
             is_deleted = v_is_deleted
       where address_program_id = v_address_program_id
         and (name is distinct from v_name or is_deleted is distinct from v_is_deleted);
      
      get diagnostics v_result = row_count;
   end if;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_display_panel_service_organizations
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Обслуживающие организации ИТ
-- ========================================================================
as $$ declare
   v_result    bigint := 0;
begin
   with parsed as 
   (select (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint    muid,
           (xpath('/object-message/body/*/name/text()', v_data))[1]::text            s_name,
           (xpath('/object-message/body/*/version/text()', v_data))[1]::text::bigint n_version,
           case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
           clock_timestamp() cur_time
   )
   insert into gis.display_panel_service_organizations as p (muid, 
                                              name, 
                                              version, 
                                              sign_deleted, 
                                              insert_date)
   select muid,
          s_name,
          n_version,
          sign_deleted,
          cur_time
     from parsed
    where sign_deleted = 0 or exists (select 1 from gis.display_panel_service_organizations sq where sq.muid = parsed.muid)
   on conflict (muid)
   do update set (name,
                  version,
                  sign_deleted,
                  update_date) =
                 (excluded.name,
                  excluded.version,
                  excluded.sign_deleted,
                  excluded.insert_date)
       where p.name         is distinct from excluded.name or
             p.version      is distinct from excluded.version or
             p.sign_deleted is distinct from excluded.sign_deleted;
    
    get diagnostics v_result = row_count;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_ibrd_service_organization
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Обслуживающая ИТОП организация ibrd
-- ========================================================================
as $$ declare
   v_result     bigint := 0;
   v_muid       bigint := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_name       text   := (xpath('/object-message/body/*/name/text()', v_data))[1]::text;
   v_is_deleted boolean := case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end;
   v_service_organization_id integer;
begin
   
   select service_organization_id into v_service_organization_id from ibrd.service_organization2gis where muid = v_muid;
   
   if not found then
      
      insert into ibrd.service_organization (is_deleted, name) values (v_is_deleted, v_name)
      returning service_organization_id into v_service_organization_id;
      
      insert into ibrd.service_organization2gis(service_organization_id, muid) values (v_service_organization_id, v_muid);
      v_result := 1;
   else
      update ibrd.service_organization
         set name       = v_name,
             is_deleted = v_is_deleted
       where service_organization_id = v_service_organization_id
         and (name is distinct from v_name or is_deleted is distinct from v_is_deleted);
      
      get diagnostics v_result = row_count;
   end if;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_display_panel_balance_organizations
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Балансодержатели ИТ
-- ========================================================================
as $$ declare
   v_result    bigint := 0;
begin
   with parsed as 
   (select (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint    muid,
           (xpath('/object-message/body/*/name/text()', v_data))[1]::text            s_name,
           (xpath('/object-message/body/*/version/text()', v_data))[1]::text::bigint n_version,
           case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
           clock_timestamp() cur_time
   )
   insert into gis.display_panel_balance_organizations as p (muid, 
                                              name, 
                                              version, 
                                              sign_deleted, 
                                              insert_date)
   select muid,
          s_name,
          n_version,
          sign_deleted,
          cur_time
     from parsed
    where sign_deleted = 0 or exists (select 1 from gis.display_panel_balance_organizations sq where sq.muid = parsed.muid)
   on conflict (muid)
   do update set (name,
                  version,
                  sign_deleted,
                  update_date) =
                 (excluded.name,
                  excluded.version,
                  excluded.sign_deleted,
                  excluded.insert_date)
       where p.name         is distinct from excluded.name or
             p.version      is distinct from excluded.version or
             p.sign_deleted is distinct from excluded.sign_deleted;
    
    get diagnostics v_result = row_count;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_ibrd_balance_organization
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Балансодержатель ИТОП ibrd
-- ========================================================================
as $$ declare
   v_result     bigint := 0;
   v_muid       bigint := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_name       text   := (xpath('/object-message/body/*/name/text()', v_data))[1]::text;
   v_is_deleted boolean := case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end;
   v_balance_organization_id integer;
begin
   
   select balance_organization_id into v_balance_organization_id from ibrd.balance_organization2gis where muid = v_muid;
   
   if not found then
      
      insert into ibrd.balance_organization (is_deleted, name) values (v_is_deleted, v_name)
      returning balance_organization_id into v_balance_organization_id;
      
      insert into ibrd.balance_organization2gis(balance_organization_id, muid) values (v_balance_organization_id, v_muid);
      v_result := 1;
   else
      update ibrd.balance_organization
         set name       = v_name,
             is_deleted = v_is_deleted
       where balance_organization_id = v_balance_organization_id
         and (name is distinct from v_name or is_deleted is distinct from v_is_deleted);
      
      get diagnostics v_result = row_count;
   end if;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_display_panel_models
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Модели ИТ
-- ========================================================================
as $$ declare
   v_result    bigint := 0;
begin
   with parsed as 
   (select (xpath('/object-message/body/*/muid/text()',    v_data))[1]::text::bigint    muid,
           (xpath('/*/body/*/type_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                           v_data))[1]::text::bigint type_muid,
           (xpath('/*/body/*/manufacturer_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                           v_data))[1]::text::bigint manufacturer_muid,
           (xpath('/object-message/body/*/name/text()',    v_data))[1]::text            s_name,
           (xpath('/object-message/body/*/version/text()', v_data))[1]::text::bigint n_version,
           case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
           clock_timestamp() cur_time
   )
   insert into gis.display_panel_models as p (muid, 
                                              type_muid, 
                                              manufacturer_muid,
                                              name, 
                                              version, 
                                              sign_deleted, 
                                              insert_date)
   select muid,
          type_muid, 
          manufacturer_muid,
          s_name,
          n_version,
          sign_deleted,
          cur_time
     from parsed
    where sign_deleted = 0 or exists (select 1 from gis.display_panel_models sq where sq.muid = parsed.muid)
   on conflict (muid)
   do update set (type_muid, 
                  manufacturer_muid,
                  name,
                  version,
                  sign_deleted,
                  update_date) =
                 (excluded.type_muid,
                  excluded.manufacturer_muid,
                  excluded.name,
                  excluded.version,
                  excluded.sign_deleted,
                  excluded.insert_date)
       where p.type_muid    is distinct from excluded.type_muid or
             p.manufacturer_muid is distinct from excluded.manufacturer_muid or
             p.name         is distinct from excluded.name or
             p.version      is distinct from excluded.version or
             p.sign_deleted is distinct from excluded.sign_deleted;
    
    get diagnostics v_result = row_count;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_ibrd_model
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Модель ИТОП ibrd
-- ========================================================================
as $$ declare
   v_result     bigint := 0;
   v_muid       bigint := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_is_deleted boolean := case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end;
   v_name       text   := (xpath('/object-message/body/*/name/text()', v_data))[1]::text;
   v_type_muid  bigint := (xpath('/*/body/*/type_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                       v_data))[1]::text::bigint;
   v_manufacturer_muid bigint := (xpath('/*/body/*/manufacturer_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                       v_data))[1]::text::bigint;
   v_model_id   integer;
   v_type_id    bigint;
   v_manufacturer_id bigint;
begin
   
   select model_id into v_model_id from ibrd.model2gis where muid = v_muid;
   select type_id into v_type_id from ibrd.type2gis where muid = v_type_muid;
   select manufacturer_id into v_manufacturer_id from ibrd.manufacturer2gis where muid = v_manufacturer_muid;
   
   if v_model_id is null then
      
      insert into ibrd.model (is_deleted, name, manufacturer_id, type_id) values (v_is_deleted, v_name, v_manufacturer_id, v_type_id)
      returning model_id into v_model_id;
      
      insert into ibrd.model2gis(model_id, muid) values (v_model_id, v_muid);
      v_result := 1;
   else
      update ibrd.model
         set name       = v_name,
             is_deleted = v_is_deleted,
             manufacturer_id = v_manufacturer_id,
             type_id    = v_type_id
       where model_id   = v_model_id
         and (name            is distinct from v_name or
              is_deleted      is distinct from v_is_deleted or
              manufacturer_id is distinct from v_manufacturer_id or
              type_id         is distinct from v_type_id);
      
      get diagnostics v_result = row_count;
   end if;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_display_panel_protocols
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Протоколы приема-передачи данных ИТОП
-- ========================================================================
as $$ declare
   v_result    bigint := 0;
begin
   with parsed as 
   (select (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint    muid,
           (xpath('/object-message/body/*/name/text()', v_data))[1]::text            s_name,
           (xpath('/object-message/body/*/version/text()', v_data))[1]::text::bigint n_version,
           case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
           clock_timestamp() cur_time
   )
   insert into gis.display_panel_protocols as p (muid, 
                                              name, 
                                              version, 
                                              sign_deleted, 
                                              insert_date)
   select muid,
          s_name,
          n_version,
          sign_deleted,
          cur_time
     from parsed
    where sign_deleted = 0 or exists (select 1 from gis.display_panel_protocols sq where sq.muid = parsed.muid)
   on conflict (muid)
   do update set (name,
                  version,
                  sign_deleted,
                  update_date) =
                 (excluded.name,
                  excluded.version,
                  excluded.sign_deleted,
                  excluded.insert_date)
       where p.name         is distinct from excluded.name or
             p.version      is distinct from excluded.version or
             p.sign_deleted is distinct from excluded.sign_deleted;
    
    get diagnostics v_result = row_count;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_display_panel_service_contracts
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Договора на обслуживание ИТ
-- ========================================================================
as $$ declare
   v_result    bigint := 0;
begin
   with parsed as 
   (select (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint    muid,
           (xpath('/*/body/*/service_organization_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                      v_data))[1]::text::bigint    service_organization_muid,
           (xpath('/*/body/*/contract_number/text()', v_data))[1]::text            contract_number,
           (xpath('/*/body/*/contract_date/text()',   v_data))[1]::text::timestamp contract_date,
           (xpath('/*/body/*/start_date/text()',      v_data))[1]::text::timestamp start_date,
           (xpath('/*/body/*/end_date/text()',        v_data))[1]::text::timestamp end_date,
           (xpath('/object-message/body/*/version/text()', v_data))[1]::text::bigint n_version,
           case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
           clock_timestamp() cur_time
   )
   insert into gis.display_panel_service_contracts as p (muid,
                                                         service_organization_muid,
                                                         contract_number,
                                                         contract_date,
                                                         start_date,
                                                         end_date,
                                                         version,
                                                         sign_deleted,
                                                         insert_date)
   select muid,
          service_organization_muid,
          contract_number,
          contract_date,
          start_date,
          end_date,
          n_version,
          sign_deleted,
          cur_time
     from parsed
    where sign_deleted = 0 or exists (select 1 from gis.display_panel_service_contracts sq where sq.muid = parsed.muid)
   on conflict (muid)
   do update set (service_organization_muid,
                  contract_number,
                  contract_date,
                  start_date,
                  end_date,
                  version,
                  sign_deleted,
                  update_date) =
                 (excluded.service_organization_muid,
                  excluded.contract_number,
                  excluded.contract_date,
                  excluded.start_date,
                  excluded.end_date,
                  excluded.version,
                  excluded.sign_deleted,
                  excluded.insert_date)
       where p.service_organization_muid is distinct from excluded.service_organization_muid or
             p.contract_number is distinct from excluded.contract_number or
             p.contract_date   is distinct from excluded.contract_date   or
             p.start_date      is distinct from excluded.start_date      or
             p.end_date        is distinct from excluded.end_date        or
             p.version         is distinct from excluded.version         or
             p.sign_deleted    is distinct from excluded.sign_deleted;
    
    get diagnostics v_result = row_count;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_ibrd_service_contract
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Договор о техническом сопровождении ИТОП ibrd
-- ========================================================================
as $$ declare
   v_result        bigint    := 0;
   v_muid          bigint    := (xpath('/object-message/body/*/muid/text()',            v_data))[1]::text::bigint;
   --v_is_deleted    boolean   := case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end;
   v_contract_number text    := (xpath('/object-message/body/*/contract_number/text()', v_data))[1]::text;
   v_contract_date timestamp := (xpath('/object-message/body/*/contract_date/text()',   v_data))[1]::text::timestamp;
   v_start_date    timestamp := (xpath('/object-message/body/*/start_date/text()',      v_data))[1]::text::timestamp;
   v_end_date      timestamp := (xpath('/object-message/body/*/end_date/text()',        v_data))[1]::text::timestamp;
   v_service_organization_muid bigint := (xpath('/object-message/body/*/service_organization_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                                        v_data))[1]::text::bigint;
   v_service_contract_id       integer;
   v_service_organization_id   integer;
begin
   
   select service_contract_id into v_service_contract_id from ibrd.service_contract2gis where muid = v_muid;
   select service_organization_id into v_service_organization_id from ibrd.service_organization2gis where muid = v_service_organization_muid;
   
   if v_service_contract_id is null then
      
      insert into ibrd.service_contract(number, contract_date, service_organization_id, start_date, end_date)
      values (v_contract_number, v_contract_date, v_service_organization_id, v_start_date, v_end_date)
      returning service_contract_id into v_service_contract_id;
      
      insert into ibrd.service_contract2gis(service_contract_id, muid) values (v_service_contract_id, v_muid);
      v_result := 1;
   else
      update ibrd.service_contract
         set number        = v_contract_number,
             contract_date = v_contract_date,
             service_organization_id = v_service_organization_id,
             start_date    = v_start_date,
             end_date      = v_end_date
       where service_contract_id = v_service_contract_id
         and (number        is distinct from v_contract_number or
              contract_date is distinct from v_contract_date or
              service_organization_id is distinct from v_service_organization_id or
              start_date    is distinct from v_start_date or
              end_date      is distinct from v_end_date);
      
      get diagnostics v_result = row_count;
   end if;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_display_panels
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Информационные табло (ИТ)
-- ========================================================================
as $$ declare
   v_result    bigint := 0;
begin
   with parsed as 
   (select (xpath('/*/body/*/muid/text()',                     v_data))[1]::text::bigint    muid,
           (xpath('/*/body/*/wkt_geom/text()',                 v_data))[1]::text            wkt_geom,
           st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text)           geom,
           (xpath('/*/body/*/code/text()',                     v_data))[1]::text            s_code,
           (xpath('/*/body/*/installation_place_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint    installation_place_muid,
           (xpath('/*/body/*/stop_pavilion_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint    stop_pavilion_muid,
           (xpath('/*/body/*/stop_place_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint    stop_place_muid,
           (xpath('/*/body/*/stop_place_2_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint    stop_place_2_muid,
           (xpath('/*/body/*/stop_place_3_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint    stop_place_3_muid,
           (xpath('/*/body/*/stop_place_4_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint    stop_place_4_muid,
           (xpath('/*/body/*/stop_place_5_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint    stop_place_5_muid,
           (xpath('/*/body/*/tpu/text()',                      v_data))[1]::text            tpu,
           (xpath('/*/body/*/address/text()',                  v_data))[1]::text            address,
           (xpath('/*/body/*/state_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint    state_muid,
           (xpath('/*/body/*/state_date_end/text()',           v_data))[1]::text::timestamp state_date_end,
           (xpath('/*/body/*/type_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint    type_muid,
           (xpath('/*/body/*/model_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint    model_muid,
           (xpath('/*/body/*/imei/text()',                     v_data))[1]::text            imei,
           (xpath('/*/body/*/serial_number/text()',            v_data))[1]::text            serial_number,
           (xpath('/*/body/*/inventory_number/text()',         v_data))[1]::text            inventory_number,
           (xpath('/*/body/*/start_date/text()',               v_data))[1]::text::timestamp start_date,
           (xpath('/*/body/*/end_date/text()',                 v_data))[1]::text::timestamp end_date,
           (xpath('/*/body/*/month_count/text()',              v_data))[1]::text::integer   month_count,
           (xpath('/*/body/*/mobile_operator_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint    mobile_operator_muid,
           (xpath('/*/body/*/mobile_number/text()',            v_data))[1]::text            mobile_number,
           (xpath('/*/body/*/balance_organization_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint    balance_organization_muid,
           (xpath('/*/body/*/protocol_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint    protocol_muid,
           (xpath('/*/body/*/address_program_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint    address_program_muid,
           (xpath('/*/body/*/service_contract_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint    service_contract_muid,
           (xpath('/*/body/*/comment/text()',                  v_data))[1]::text            s_comment,
           (xpath('/object-message/body/*/version/text()', v_data))[1]::text::bigint        n_version,
           case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
           clock_timestamp() cur_time
   )
   insert into gis.display_panels as p (muid, 
                                        code, 
                                        imei, 
                                        tpu, 
                                        type_muid, 
                                        model_muid, 
                                        installation_place_muid,
                                        stop_pavilion_muid,
                                        stop_place_muid, 
                                        stop_place_2_muid, 
                                        stop_place_3_muid, 
                                        stop_place_4_muid, 
                                        stop_place_5_muid, 
                                        protocol_muid, 
                                        mobile_operator_muid, 
                                        balance_organization_muid, 
                                        service_contract_muid, 
                                        address_program_muid, 
                                        state_muid, 
                                        state_date_end, 
                                        inventory_number, 
                                        serial_number, 
                                        start_date, 
                                        end_date,
                                        month_count,
                                        mobile_number, 
                                        address, 
                                        comment,
                                        version, 
                                        sign_deleted, 
                                        insert_date)
   select muid, 
          s_code, 
          imei, 
          tpu, 
          type_muid, 
          model_muid, 
          installation_place_muid,
          stop_pavilion_muid,
          stop_place_muid, 
          stop_place_2_muid, 
          stop_place_3_muid, 
          stop_place_4_muid, 
          stop_place_5_muid, 
          protocol_muid, 
          mobile_operator_muid, 
          balance_organization_muid, 
          service_contract_muid, 
          address_program_muid, 
          state_muid, 
          state_date_end, 
          inventory_number, 
          serial_number, 
          start_date, 
          end_date,
          month_count,
          mobile_number, 
          address, 
          s_comment,
          n_version,
          sign_deleted,
          cur_time
     from parsed
    where sign_deleted = 0 or exists (select 1 from gis.display_panels sq where sq.muid = parsed.muid)
   on conflict (muid)
   do update set (code,
                  imei,
                  tpu,
                  type_muid,
                  model_muid,
                  installation_place_muid,
                  stop_pavilion_muid,
                  stop_place_muid,
                  stop_place_2_muid,
                  stop_place_3_muid, 
                  stop_place_4_muid,
                  stop_place_5_muid,
                  protocol_muid,
                  mobile_operator_muid, 
                  balance_organization_muid, 
                  service_contract_muid, 
                  address_program_muid, 
                  state_muid, 
                  state_date_end, 
                  inventory_number, 
                  serial_number, 
                  start_date, 
                  end_date,
                  month_count,
                  mobile_number, 
                  address, 
                  comment,
                  version,
                  sign_deleted,
                  update_date) =
                 (excluded.code, 
                  excluded.imei, 
                  excluded.tpu, 
                  excluded.type_muid, 
                  excluded.model_muid, 
                  excluded.installation_place_muid,
                  excluded.stop_pavilion_muid,
                  excluded.stop_place_muid, 
                  excluded.stop_place_2_muid, 
                  excluded.stop_place_3_muid, 
                  excluded.stop_place_4_muid, 
                  excluded.stop_place_5_muid, 
                  excluded.protocol_muid, 
                  excluded.mobile_operator_muid, 
                  excluded.balance_organization_muid, 
                  excluded.service_contract_muid, 
                  excluded.address_program_muid, 
                  excluded.state_muid, 
                  excluded.state_date_end, 
                  excluded.inventory_number, 
                  excluded.serial_number, 
                  excluded.start_date, 
                  excluded.end_date,
                  excluded.month_count,
                  excluded.mobile_number, 
                  excluded.address, 
                  excluded.comment,
                  excluded.version,
                  excluded.sign_deleted,
                  excluded.insert_date)
       where p.code              is distinct from excluded.code or
             p.imei              is distinct from excluded.imei or
             p.tpu               is distinct from excluded.tpu  or
             p.type_muid         is distinct from excluded.type_muid  or
             p.model_muid        is distinct from excluded.model_muid or
             p.installation_place_muid is distinct from excluded.installation_place_muid or
             p.stop_pavilion_muid      is distinct from excluded.stop_pavilion_muid or
             p.stop_place_muid   is distinct from excluded.stop_place_muid   or
             p.stop_place_2_muid is distinct from excluded.stop_place_2_muid or
             p.stop_place_3_muid is distinct from excluded.stop_place_3_muid or
             p.stop_place_4_muid is distinct from excluded.stop_place_4_muid or
             p.stop_place_5_muid is distinct from excluded.stop_place_5_muid or
             p.protocol_muid     is distinct from excluded.protocol_muid     or
             p.mobile_operator_muid      is distinct from excluded.mobile_operator_muid      or
             p.balance_organization_muid is distinct from excluded.balance_organization_muid or
             p.service_contract_muid     is distinct from excluded.service_contract_muid     or
             p.address_program_muid      is distinct from excluded.address_program_muid      or
             p.state_muid        is distinct from excluded.state_muid        or
             p.state_date_end    is distinct from excluded.state_date_end    or
             p.inventory_number  is distinct from excluded.inventory_number  or
             p.serial_number     is distinct from excluded.serial_number     or
             p.start_date        is distinct from excluded.start_date        or
             p.end_date          is distinct from excluded.end_date          or
             p.month_count       is distinct from excluded.month_count       or
             p.mobile_number     is distinct from excluded.mobile_number     or
             p.address           is distinct from excluded.address           or
             p.comment           is distinct from excluded.comment           or
             p.version           is distinct from excluded.version           or
             p.sign_deleted      is distinct from excluded.sign_deleted;
    
    get diagnostics v_result = row_count;
    return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_ibrd_info_board
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт ИТОП ibrd
-- ========================================================================
as $$ declare
   v_result     bigint  := 0;
   v_muid       bigint  := (xpath('/object-message/body/*/muid/text()',        v_data))[1]::text::bigint;
   --v_name       text    := (xpath('/object-message/body/*/name/text()',        v_data))[1]::text;
   v_is_deleted boolean := case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end;
   v_code       text    := (xpath('/*/body/*/code/text()',                     v_data))[1]::text;
   v_model_muid bigint  := (xpath('/*/body/*/model_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', v_data))[1]::text::bigint;
   v_serial_number text := (xpath('/*/body/*/serial_number/text()',            v_data))[1]::text;
   v_address_program_muid bigint := (xpath('/*/body/*/address_program_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                                               v_data))[1]::text::bigint;
   v_installation_place_muid bigint := (xpath('/*/body/*/installation_place_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                               v_data))[1]::text::bigint;
   v_address    text    := (xpath('/*/body/*/address/text()',                  v_data))[1]::text;
   v_has_stop_pavilion  boolean := (xpath('/*/body/*/stop_pavilion_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                               v_data))[1]::text::bigint is null;
   v_imei       text    := (xpath('/*/body/*/imei/text()',                     v_data))[1]::text;
   v_mobile_operator_muid bigint := (xpath('/*/body/*/mobile_operator_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                               v_data))[1]::text::bigint;
   v_mobile_number       text := (xpath('/*/body/*/mobile_number/text()',      v_data))[1]::text;
   v_balance_organization_muid bigint := (xpath('/*/body/*/balance_organization_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                               v_data))[1]::text::bigint;
   v_inventory_number    text := (xpath('/*/body/*/inventory_number/text()',   v_data))[1]::text;
   v_start_date     timestamp := (xpath('/*/body/*/start_date/text()',         v_data))[1]::text::timestamp;
   v_useful_duration interval := ((xpath('/*/body/*/month_count/text()',       v_data))[1]::text || ' month')::interval;
   v_end_date       timestamp := (xpath('/*/body/*/end_date/text()',           v_data))[1]::text::timestamp;
   v_stop_place_1_muid bigint := (xpath('/*/body/*/stop_place_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                                               v_data))[1]::text::bigint;
   v_stop_place_2_muid bigint := (xpath('/*/body/*/stop_place_2_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                                               v_data))[1]::text::bigint;
   v_stop_place_3_muid bigint := (xpath('/*/body/*/stop_place_3_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                                               v_data))[1]::text::bigint;
   v_stop_place_4_muid bigint := (xpath('/*/body/*/stop_place_4_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                                               v_data))[1]::text::bigint;
   v_stop_place_5_muid bigint := (xpath('/*/body/*/stop_place_5_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                                               v_data))[1]::text::bigint;
   v_state_muid        bigint := (xpath('/*/body/*/state_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                               v_data))[1]::text::bigint;
   v_state_date_end timestamp := (xpath('/*/body/*/state_date_end/text()',     v_data))[1]::text::timestamp;
   v_service_contract_muid bigint := (xpath('/*/body/*/service_contract_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                               v_data))[1]::text::bigint;
   v_info_board_id           integer;
   v_model_id                bigint;
   v_address_program_id      bigint;
   v_installation_place_id   bigint;
   v_mobile_operator_id      bigint;
   v_balance_organization_id bigint;
   v_state_id                integer;
   v_service_contract_id     integer;
begin
   
   if v_code is null then
      raise notice 'error - "code" is null: muid = %', v_muid;
      --return -1;
   end if;
   
   select info_board_id into v_info_board_id from ibrd.info_board2gis where muid = v_muid;
   if v_is_deleted and v_info_board_id is null then
      return 0;
   end if;
   
   select model_id into v_model_id from ibrd.model2gis where muid = v_model_muid;
   select address_program_id into v_address_program_id from ibrd.address_program2gis where muid = v_address_program_muid;
   select installation_place_id into v_installation_place_id from ibrd.installation_place2gis where muid = v_installation_place_muid;
   select mobile_operator_id into v_mobile_operator_id from ibrd.mobile_operator2gis where muid = v_mobile_operator_muid;
   select balance_organization_id into v_balance_organization_id from ibrd.balance_organization2gis where muid = v_balance_organization_muid;
   select state_id into v_state_id from ibrd.state2gis where muid = v_state_muid;
   select service_contract_id into v_service_contract_id from ibrd.service_contract2gis where muid = v_service_contract_muid;
   
   if v_info_board_id is null then
      
      insert into ibrd.info_board(code, model_id, serial_number, address_program_id, installation_place_id, address, has_stop_pavilion,
              imei, mobile_operator_id, mobile_number, balance_organization_id, inventory_number, start_date, useful_duration, end_date,
              state_id, state_end_date, service_contract_id, is_deleted)
      values (v_code, v_model_id, v_serial_number, v_address_program_id, v_installation_place_id, v_address, v_has_stop_pavilion, v_imei,
              v_mobile_operator_id, v_mobile_number, v_balance_organization_id, v_inventory_number, v_start_date, v_useful_duration, v_end_date,
              v_state_id, v_state_date_end, v_service_contract_id, v_is_deleted)
      returning info_board_id into v_info_board_id;
      
      insert into ibrd.info_board2gis(info_board_id, muid) values (v_info_board_id, v_muid);
      v_result := 1;
   else
      update ibrd.info_board
         set code                = v_code,
             model_id            = v_model_id,
             serial_number       = v_serial_number,
             address_program_id  = v_address_program_id,
             installation_place_id = v_installation_place_id,
             address             = v_address,
             has_stop_pavilion   = v_has_stop_pavilion,
             imei                = v_imei,
             mobile_operator_id  = v_mobile_operator_id,
             mobile_number       = v_mobile_number,
             balance_organization_id = v_balance_organization_id,
             inventory_number    = v_inventory_number,
             start_date          = v_start_date,
             useful_duration     = v_useful_duration,
             end_date            = v_end_date,
             state_id            = v_state_id,
             state_end_date      = v_state_date_end,
             service_contract_id = v_service_contract_id,
             is_deleted          = v_is_deleted
       where info_board_id = v_info_board_id
         and (code                    is distinct from v_code or
              model_id                is distinct from v_model_id or
              serial_number           is distinct from v_serial_number or
              address_program_id      is distinct from v_address_program_id or
              installation_place_id   is distinct from v_installation_place_id or
              address                 is distinct from v_address or
              has_stop_pavilion       is distinct from v_has_stop_pavilion  or
              imei                    is distinct from v_imei or
              mobile_operator_id      is distinct from v_mobile_operator_id or
              mobile_number           is distinct from v_mobile_number    or
              balance_organization_id is distinct from v_balance_organization_id or
              inventory_number        is distinct from v_inventory_number or
              useful_duration         is distinct from v_useful_duration  or
              end_date                is distinct from v_end_date or
              state_id                is distinct from v_state_id or
              state_end_date          is distinct from v_state_date_end   or
              service_contract_id     is distinct from v_service_contract_id or
              is_deleted              is distinct from v_is_deleted);
      
      get diagnostics v_result = row_count;
   end if;
   
   delete from ibrd.stop_location2info_board
    where info_board_id = v_info_board_id
      and stop_location_id not in 
          (select sl2g.stop_location_id
             from rts.stop_location2gis sl2g
            where sl2g.stop_location_muid in (v_stop_place_1_muid,
                                              v_stop_place_2_muid,
                                              v_stop_place_3_muid,
                                              v_stop_place_4_muid,
                                              v_stop_place_5_muid)
          );
   
   insert into ibrd.stop_location2info_board as p
         (stop_location_id, info_board_id, is_base)
   select sl2g.stop_location_id, v_info_board_id,
          sl2g.stop_location_muid = coalesce(v_stop_place_1_muid, 
                                             v_stop_place_2_muid, 
                                             v_stop_place_3_muid, 
                                             v_stop_place_4_muid, 
                                             v_stop_place_5_muid)
     from rts.stop_location2gis sl2g
    where sl2g.stop_location_muid in (v_stop_place_1_muid,
                                      v_stop_place_2_muid,
                                      v_stop_place_3_muid,
                                      v_stop_place_4_muid,
                                      v_stop_place_5_muid)
   on conflict (stop_location_id, info_board_id)
   do update set is_base = excluded.is_base
       where p.is_base is distinct from excluded.is_base;
   
   -- Копфигурация по умолчанию
   with proto as (
      select p.protocol_id
        from ibrd.info_board ib
        join ibrd.model m on ib.model_id = m.model_id
        join ibrd.type t on m.type_id = t.type_id
        join ibrd.ibrd_protocol p on t.type_id = p.type_id
       where ib.info_board_id = v_info_board_id
   )
   insert into ibrd.info_board2config (info_board_id, config_id)
   select v_info_board_id, c.config_id
     from ibrd.config c
     join ibrd.row_config rc on c.config_id = rc.config_id
    where c.is_default
      and exists(select 1 from proto where protocol_id = ibrd.protocol_pkg$row_protocol_id())
   union
   select v_info_board_id, c.config_id
     from ibrd.config c
     join ibrd.mono_rgb_config mrc
       on c.config_id = mrc.config_id
    where c.is_default
      and exists(select 1 from proto where protocol_id in (ibrd.protocol_pkg$mono_protocol_id(),
                                                           ibrd.protocol_pkg$rgb_protocol_id())
                )
   on conflict(info_board_id)
   do nothing;

   return v_result;
end;
$$ language plpgsql;

-- ========================================================================
-- Проверка данных (дубликатов и т.п.)
-- 

create or replace function gis.gis_import_pkg_v3$get_check_func_list
  (p_rows           out refcursor)
returns refcursor
-- ========================================================================
-- Список функций проверки
-- Эти функции будут вызваны по окончании импорта
-- ========================================================================
as $$ declare
begin
   open p_rows for
   select r.routine_schema || '.' || r.routine_name as routine_name
     from information_schema.routines r
    where r.routine_schema = 'gis'
      and r.routine_name like 'gis_import_pkg_v3$check_%';
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$check_dup_agency
  (p_rows           out refcursor)
returns refcursor
-- ========================================================================
-- Проверка дубливатов в TAgency
-- ========================================================================
as $$ declare
begin
   open p_rows for
   select muid, 'Duplicate found: ' || name as msg, 'TAgency' as code
     from gis.agencies
    where sign_deleted = 0
      and (agency_organization_form_muid, name, legal_address, contact_information) in
          (select agency_organization_form_muid, name, legal_address, contact_information
             from gis.agencies
            where sign_deleted = 0
            group by agency_organization_form_muid, name, legal_address, contact_information
           having count(*) > 1);
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$check_dup_agency_org_form
  (p_rows           out refcursor)
returns refcursor
-- ========================================================================
-- Проверка дубливатов в TAgencyOrganizationForm
-- ========================================================================
as $$ declare
begin
   open p_rows for
   select muid, 'Duplicate found: ' || name as msg, 'TAgencyOrganizationForm' as code
     from gis.agency_organization_forms
    where sign_deleted = 0
      and (name, okpo_code) in
          (select name, okpo_code
             from gis.agency_organization_forms
            where sign_deleted = 0
            group by name, okpo_code
           having count(*) > 1);
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$check_dup_park
  (p_rows           out refcursor)
returns refcursor
-- ========================================================================
-- Проверка дубливатов в TPark
-- ========================================================================
as $$ declare
begin
   open p_rows for
   select muid, 'Duplicate found: ' || name as msg, 'TPark' as code
     from gis.parks
    where sign_deleted = 0
      and (number, name, transport_kind_muid) in
          (select number, name, transport_kind_muid
             from gis.parks
            where sign_deleted = 0
            group by number, name, transport_kind_muid
           having count(*) > 1);
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$check_dup_park_zone
  (p_rows           out refcursor)
returns refcursor
-- ========================================================================
-- Проверка дубливатов в TParkZone
-- ========================================================================
as $$ declare
begin
   open p_rows for
   select muid, 'Duplicate found: ' || name as msg, 'TParkZone' as code
     from gis.park_zones
    where sign_deleted = 0
      and (park_zones_num, name, wkt_geom, sign_deleted) in
          (select park_zones_num, name, wkt_geom, sign_deleted
             from gis.park_zones
            where sign_deleted = 0
            group by park_zones_num, name, wkt_geom, sign_deleted
           having count(*) > 1);
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$check_dup_route_round_type
  (p_rows           out refcursor)
returns refcursor
-- ========================================================================
-- Проверка дубливатов в TRouteRoundType
-- ========================================================================
as $$ declare
begin
   open p_rows for
   select muid, 'Duplicate found: ' || name as msg, 'TRouteRoundType' as code
     from gis.ref_route_round_types
    where sign_deleted = 0
      and (name) in
          (select name
             from gis.ref_route_round_types
            where sign_deleted = 0
            group by name
           having count(*) > 1);
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$check_dup_route_null_round_type
  (p_rows           out refcursor)
returns refcursor
-- ========================================================================
-- Проверка дубливатов в TRouteNullRoundType
-- ========================================================================
as $$ declare
begin
   open p_rows for
   select muid, 'Duplicate found: ' || name as msg, 'TRouteNullRoundType' as code
     from gis.ref_route_null_round_types
    where sign_deleted = 0
      and (name) in
          (select name
             from gis.ref_route_null_round_types
            where sign_deleted = 0
            group by name
           having count(*) > 1);
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$check_dup_route_kind
  (p_rows           out refcursor)
returns refcursor
-- ========================================================================
-- Проверка дубливатов в TRouteKind
-- ========================================================================
as $$ declare
begin
   open p_rows for
   select muid, 'Duplicate found: ' || name as msg, 'TRouteKind' as code
     from gis.ref_route_kinds
    where sign_deleted = 0
      and (name) in
          (select name
             from gis.ref_route_kinds
            where sign_deleted = 0
            group by name
           having count(*) > 1);
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$check_dup_stop_mode
  (p_rows           out refcursor)
returns refcursor
-- ========================================================================
-- Проверка дубливатов в TStopMode
-- ========================================================================
as $$ declare
begin
   open p_rows for
   select muid, 'Duplicate found: ' || name as msg, 'TStopMode' as code
     from gis.ref_stop_modes
    where sign_deleted = 0
      and (name) in
          (select name
             from gis.ref_stop_modes
            where sign_deleted = 0
            group by name
           having count(*) > 1);
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$check_dup_route
  (p_rows           out refcursor)
returns refcursor
-- ========================================================================
-- Проверка дубливатов в TRoute
-- ========================================================================
as $$ declare
begin
   open p_rows for
   select muid, 'Duplicate found: ' || number as msg, 'TRoute' as code
     from gis.routes
    where sign_deleted = 0
      and (number, agency_muid, transport_kind_muid, route_transportation_kind_muid, route_state_muid, sign_deleted) in
          (select number, agency_muid, transport_kind_muid, route_transportation_kind_muid, route_state_muid, sign_deleted
             from gis.routes
            where sign_deleted = 0
            group by number, agency_muid, transport_kind_muid, route_transportation_kind_muid, route_state_muid, sign_deleted
           having count(*) > 1);
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$check_dup_route_trajectory
  (p_rows           out refcursor)
returns refcursor
-- ========================================================================
-- Проверка дубливатов в TRouteTrajectory
-- ========================================================================
as $$ declare
begin
   open p_rows for
   select muid, 'Duplicate found: ' || specification as msg, 'TRouteTrajectory' as code
     from gis.route_trajectories
    where sign_deleted = 0
      and (route_round_muid, trajectory_type_muid, length, length_fixed, sign_deleted) in
          (select route_round_muid, trajectory_type_muid, length, length_fixed, sign_deleted
             from gis.route_trajectories
            where route_round_muid is not null
              and sign_deleted = 0
            group by route_round_muid, trajectory_type_muid, length, length_fixed, sign_deleted
           having count(*) > 1);
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$check_dup_transport_static_objects
  (p_rows           out refcursor)
returns refcursor
-- ========================================================================
-- Проверка дубливатов в TTransportStaticObject
-- ========================================================================
as $$ declare
begin
   open p_rows for
   select muid, 'Duplicate found: ' || name as msg, 'TTransportStaticObject' as code
     from gis.transport_static_objects
    where sign_deleted = 0
      and (name) in
          (select name
             from gis.transport_static_objects
            where sign_deleted = 0
            group by name
           having count(*) > 1 );
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$check_dup_route_variant1
  (p_rows           out refcursor)
returns refcursor
-- ========================================================================
-- Проверка дубливатов в TRouteVariant 1
-- ========================================================================
as $$ declare
begin
   open p_rows for
   select rtv.muid, 'Duplicate found: ' || ror.code as msg, 'TRouteVariant + TRouteRound' as code
     from gis.route_variants rtv
     join gis.route_rounds ror on ror.route_variant_muid = rtv.muid
    where rtv.sign_deleted = 0
      and ror.sign_deleted = 0
    group by rtv.muid, ror.code
   having count(*) > 1;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$check_dup_route_variant2
  (p_rows           out refcursor)
returns refcursor
-- ========================================================================
-- Проверка дубливатов в TRouteVariant 2
-- ========================================================================
as $$ declare
begin
   open p_rows for
   select rtv.muid, 'Duplicate found: ' || ror.code as msg, 'TRouteVariant + TRouteNullRound' as code
     from gis.route_variants rtv
     join gis.route_null_rounds ror on ror.route_variant_muid = rtv.muid
    where rtv.sign_deleted = 0
      and ror.sign_deleted = 0
    group by rtv.muid, ror.code
   having count(*) > 1;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$check_erm_id
  (p_rows           out refcursor)
returns refcursor
-- ========================================================================
-- Проверка наличия erm_id
-- ========================================================================
as $$ declare
begin
   open p_rows for
   select muid, 'Не задан erm_id ' || name as msg, 'TPark' as code
     from gis.parks
    where sign_deleted = 0
      and agency_muid = 1
      and erm_id is null;
end;
$$ language plpgsql;

-- ========================================================================
-- Загрузка сообщений в модель rts

create or replace function gis.gis_import_pkg_v3$import_rts_graph_nodes
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Узлов графа rts
-- ========================================================================
as $$ declare
   v_result        bigint    := 0;
   v_muid          bigint    := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_graph_node_id integer;
   v_new_graph_node_id integer;
begin
   
   select graph_node_id into v_graph_node_id from rts.graph_node2gis where graph_node_muid = v_muid;
   
   with parsed as (
       select case when v_graph_node_id is null then nextval(pg_get_serial_sequence('rts.graph_node', 'graph_node_id')) else v_graph_node_id end graph_node_id,
              (xpath('/*/body/*/muid/text()',     v_data))[1]::text::bigint          muid,
              (xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text                  wkt_geom,
              st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text) geom,
              case when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false  end sign_deleted
   )
   insert into rts.graph_node as p (graph_node_id,
                                    wkt_geom,
                                    geom,
                                    sign_deleted)
   select graph_node_id,
          wkt_geom,
          geom,
          sign_deleted
     from parsed
--  where not sign_deleted or exists (select 1 from rts.graph_node sq where sq.graph_node_id = parsed.graph_node_id)
   on conflict (graph_node_id)
   do update set (wkt_geom,
                  geom,
                  sign_deleted) =
                 (excluded.wkt_geom,
                  excluded.geom,
                  excluded.sign_deleted)
   where p.wkt_geom     is distinct from excluded.wkt_geom or
         p.sign_deleted is distinct from excluded.sign_deleted
   returning graph_node_id into v_new_graph_node_id;
   
   get diagnostics v_result = row_count;
   
   if v_graph_node_id is null and v_new_graph_node_id is not null then
      insert into rts.graph_node2gis(graph_node_id, graph_node_muid) values (v_new_graph_node_id, v_muid);
   end if;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_graph_sections
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Дуг графа rts
-- ========================================================================
as $$ declare
   v_result               bigint := 0;
   v_muid                 bigint := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_graph_section_id     integer;
   v_new_graph_section_id integer;
begin
   
   select graph_section_id into v_graph_section_id from rts.graph_section2gis where graph_section_muid = v_muid;
   
   with parsed as (
      select case when v_graph_section_id is null then nextval(pg_get_serial_sequence('rts.graph_section', 'graph_section_id')) else v_graph_section_id end graph_section_id,
             (xpath('/*/body/*/wkt_geom/text()',                 v_data))[1]::text          wkt_geom,
             st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text)         geom,
             (xpath('/*/body/*/length/text()', v_data))[1]::text::double precision          length,
             (select graph_node_id from rts.graph_node2gis
               where graph_node_muid = (xpath('/*/body/*/startNode/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                 v_data))[1]::text::bigint) node_begin_id,
             (select graph_node_id from rts.graph_node2gis
               where graph_node_muid = (xpath('/*/body/*/endNode/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                 v_data))[1]::text::bigint) node_end_id,
             case when (xpath('/*/body/*/signDeleted/text()',    v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end sign_deleted
   )
   insert into rts.graph_section as p
         (graph_section_id, wkt_geom, geom, length, node_begin_id, node_end_id, sign_deleted)
   select graph_section_id, wkt_geom, geom, length, node_begin_id, node_end_id, sign_deleted
     from parsed
    where not sign_deleted or exists (select 1 from rts.graph_section sq where sq.graph_section_id = parsed.graph_section_id)
   on conflict (graph_section_id)
   do update set (wkt_geom, geom, length, node_begin_id, node_end_id, sign_deleted) =
                 (excluded.wkt_geom, excluded.geom, excluded.length, excluded.node_begin_id, excluded.node_end_id, excluded.sign_deleted)
       where p.wkt_geom      is distinct from excluded.wkt_geom or
             p.length        is distinct from excluded.length or
             p.node_begin_id is distinct from excluded.node_begin_id or
             p.node_end_id   is distinct from excluded.node_end_id or
             p.sign_deleted  is distinct from excluded.sign_deleted
   returning graph_section_id into v_new_graph_section_id;
   
   get diagnostics v_result = row_count;
   
   if v_graph_section_id is null and v_new_graph_section_id is not null then
      insert into rts.graph_section2gis(graph_section_id, graph_section_muid) values (v_new_graph_section_id, v_muid);
   end if;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_routes
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Маршрутов rts
-- ========================================================================
as $$ declare
   v_result               bigint := 0;
   v_muid                 bigint := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_route_id     integer;
   v_new_route_id integer;
begin
   
   select route_id into v_route_id from rts.route2gis where route_muid = v_muid;
   
   with parsed as (
       select case when v_route_id is null then nextval(pg_get_serial_sequence('rts.route', 'route_id')) else v_route_id end route_id,
              (xpath('/*/body/*/number/text()',                v_data))[1]::text route_num,
              (select tr_type_id from core.tr_type
                where transport_kind_muid = (xpath('/*/body/*/transportKind/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint) tr_type_id,
              (select carrier_id from core.carrier
                where agencies_muid = (xpath('/*/body/*/agency/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                               v_data))[1]::text::bigint) carrier_id,
              (xpath('/*/body/*/comment/text()',               v_data))[1]::text s_comment,
              case when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end sign_deleted
   )
   insert into rts.route as p
         (route_id, route_num, tr_type_id, carrier_id, comment,   sign_deleted)
   select route_id, route_num, tr_type_id, carrier_id, s_comment, sign_deleted
     from parsed
    where not sign_deleted or exists (select 1 from rts.route sq where sq.route_id = parsed.route_id)
   on conflict (route_id)
   do update set (route_num, tr_type_id, carrier_id, comment, sign_deleted) =
                 (excluded.route_num, excluded.tr_type_id, excluded.carrier_id, excluded.comment, excluded.sign_deleted)
       where p.route_num    is distinct from excluded.route_num or
             p.tr_type_id   is distinct from excluded.tr_type_id or
             p.carrier_id   is distinct from excluded.carrier_id or
             p.comment      is distinct from excluded.comment or
             p.sign_deleted is distinct from excluded.sign_deleted
   returning route_id into v_new_route_id;
   
   get diagnostics v_result = row_count;
   
   if v_route_id is null and v_new_route_id is not null then
      insert into rts.route2gis(route_id, route_muid) values (v_new_route_id, v_muid);
   end if;
   
   insert into gis.gis_xml_message_deffered(message_code, step, insert_date)
   select (xpath('/*/header/messageIdent/text()', v_data))[1]::text, 2, localtimestamp
    where not exists(select 1 from gis.gis_xml_message_deffered
                      where message_code = (xpath('/*/header/messageIdent/text()', v_data))[1]::text);
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_depo2route
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Связь парков и маршрутов rts
-- ========================================================================
as $$ declare
   v_result               bigint := 0;
   v_muid                 bigint := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_route_id     integer;
begin
   
   --raise notice 'route_muid = %', v_muid;
   
   select route_id into v_route_id from rts.route2gis where route_muid = v_muid;
   
   delete from rts.depo2route
    where (route_id, depo_id) in 
          (with parsed as 
           (select (select depo_id from core.depo
                      where park_muid = (xpath('/park/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                    sp_data))[1]::text::bigint) depo_id,
                   v_route_id route_id,
                   case when (xpath('/*/body/*/signDeleted/text()', sp_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end sign_deleted
              from (select unnest(xpath('/*/body/*/parks/park', v_data)) as sp_data) t
           )
           select route_id, depo_id from parsed where sign_deleted
          );
   
   with parsed as 
   (select (select depo_id from core.depo
              where park_muid = (xpath('/park/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                            sp_data))[1]::text::bigint) depo_id,
           v_route_id route_id,
           case when (xpath('/*/body/*/signDeleted/text()', sp_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end sign_deleted
      from (select unnest(xpath('/*/body/*/parks/park', v_data)) as sp_data) t
   )
   insert into rts.depo2route as p (route_id, depo_id)
   select route_id, depo_id
     from parsed
    where not sign_deleted
      and depo_id is not null
   on conflict (route_id, depo_id)
   do nothing;
   
   get diagnostics v_result = row_count;
   return v_result;   
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v2$import_rts_current_route_variants
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт связки Маршрута и текущего Варианта Маршрута из XML типа
-- ========================================================================
as $$ declare
   v_result bigint := 0;
begin
   with parsed as
   (select (select route_id from rts.route2gis
             where route_muid = (xpath('/*/body/*/muid/text()', v_data))[1]::text::bigint) route_id,
           (select route_variant_id from rts.route_variant2gis
             where route_variant_muid = (xpath('/*/body/*/currentRouteVariant/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                v_data))[1]::text::bigint) current_route_variant_id
   )
   update rts.route as r
      set current_route_variant_id = parsed.current_route_variant_id
     from parsed
    where parsed.route_id = r.route_id
      and parsed.current_route_variant_id is distinct from r.current_route_variant_id;
   
   get diagnostics v_result = row_count;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_route_variants
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Вариантов Маршрута rts
-- ========================================================================
as $$ declare
   v_result               bigint := 0;
   v_muid                 bigint := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_route_variant_id     integer;
   v_new_route_variant_id integer;
begin
   
   select route_variant_id into v_route_variant_id from rts.route_variant2gis where route_variant_muid = v_muid;
   
   with parsed as (
       select case when v_route_variant_id is null then nextval(pg_get_serial_sequence('rts.route_variant', 'route_variant_id')) else v_route_variant_id end route_variant_id,
              (select route_id from rts.route2gis
                where route_muid = (xpath('/*/body/*/route/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint)    route_id,
              (select case route_state_muid
                         when 3 then 1
                         when 4 then 2
                         when 1 then 3
                         else 2
                      end
                 from gis.routes
                where muid = (xpath('/*/body/*/route/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint)    rv_status_id,
              tsrange((xpath('/*/body/*/startDate/text()',     v_data))[1]::text::timestamp,
                      (xpath('/*/body/*/endDate/text()',       v_data))[1]::text::timestamp) action_period,
              (xpath('/*/body/*/comment/text()',               v_data))[1]::text             rv_comment,
              case when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end sign_deleted
   )
   insert into rts.route_variant as p
         (route_variant_id, route_id, rv_status_id, action_period, sign_deleted, rv_comment)
   select route_variant_id, route_id, rv_status_id, action_period, sign_deleted, rv_comment
     from parsed
    where not sign_deleted or exists (select 1 from rts.route_variant sq where sq.route_variant_id = parsed.route_variant_id)
   on conflict (route_variant_id)
   do update set (route_id, rv_status_id, action_period, sign_deleted, rv_comment) =
                 (excluded.route_id, excluded.rv_status_id, excluded.action_period, excluded.sign_deleted, excluded.rv_comment)
       where p.route_id      is distinct from excluded.route_id or
             p.rv_status_id  is distinct from excluded.rv_status_id or
             p.action_period is distinct from excluded.action_period or
             p.sign_deleted  is distinct from excluded.sign_deleted or
             p.rv_comment    is distinct from excluded.rv_comment
   returning route_variant_id into v_new_route_variant_id;
   
   get diagnostics v_result = row_count;
   
   if v_route_variant_id is null and v_new_route_variant_id is not null then
      insert into rts.route_variant2gis(route_variant_id, route_variant_muid) values (v_new_route_variant_id, v_muid);
   end if;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_round_prepare
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт подготовка к импорту Рейсов rts
-- ========================================================================
as $$ declare
   v_result                bigint  := 0;
   v_tmp                   bigint;
   v_muid                  bigint := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
begin
   
   -- Первый вызов
   --raise notice 'route_trajectory_muid = %', v_muid;
   
   update rts.stop_item2round
      set sign_deleted = true
    where stop_item2round_id in 
          (select stop2round_id from rts.stop2round2gis
            where stop_place2route_traj_muid in
                  (select muid from gis.stop_place2route_traj
                    where route_trajectory_muid = v_muid
                      and muid not in 
                          (select (xpath('/*/body/muid/text()', gs_data))[1]::text::bigint  muid
                             from (select unnest(xpath('/*/body/*/stopPlaces/linkedStopPlace', v_data)) as gs_data) t
                          )
                  )
          );
   
   get diagnostics v_result = row_count;
   
   delete from rts.stop2round2gis
    where stop_place2route_traj_muid in
          (select muid from gis.stop_place2route_traj
            where route_trajectory_muid = v_muid
              and muid not in
                  (select (xpath('/*/body/muid/text()', gs_data))[1]::text::bigint muid
                     from (select unnest(xpath('/*/body/*/stopPlaces/linkedStopPlace', v_data)) as gs_data) t
                  )
          );
   
   get diagnostics v_tmp = row_count;
   v_result := v_result + v_tmp;
   
   update rts.graph_sec2round
      set sign_deleted = true
    where graph_sec2round_id in 
          (select graph_sec2trajectory_id from rts.graph_sec2trajectory2gis
            where graph_sec2route_traj_muid in
                  (select muid from gis.graph_sec2route_traj
                    where route_trajectory_muid = v_muid
                      and muid not in 
                          (select (xpath('/*/body/muid/text()', gs_data))[1]::text::bigint  muid
                             from (select unnest(xpath('/*/body/*/graphSections/linkedGraphSection', v_data)) as gs_data) t
                          )
                  )
          );
   
   get diagnostics v_tmp = row_count;
   v_result := v_result + v_tmp;
   
   delete from rts.graph_sec2trajectory2gis
    where graph_sec2route_traj_muid in
          (select muid from gis.graph_sec2route_traj
            where route_trajectory_muid = v_muid
              and muid not in 
                  (select (xpath('/*/body/muid/text()', gs_data))[1]::text::bigint  muid
                     from (select unnest(xpath('/*/body/*/graphSections/linkedGraphSection', v_data)) as gs_data) t
                  )
          );
   
   get diagnostics v_tmp = row_count;
   v_result := v_result + v_tmp;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_round
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Рейсов rts
-- ========================================================================
as $$ declare
   v_result                bigint  := 0;
   v_muid                  bigint  := (xpath('/*/body/*/muid/text()', v_data))[1]::text::bigint;
   v_route_round_muid      bigint  := (xpath('/*/body/*/routeRound/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', v_data))[1]::text::bigint;
   v_route_null_round_muid bigint  := (xpath('/*/body/*/routeNullRound/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', v_data))[1]::text::bigint;
   v_sign_deleted          boolean := case when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end;
   v_round_id              integer;
   v_new_round_id          integer;
   v_action_type_id        smallint;
   v_route_variant_muid    bigint;
   v_trajectory_type_muid  bigint;
   v_round_num             integer;
   v_code                  text;
   v_graph_section_start_offset real;
   v_graph_section_end_offset   real;
begin
   
   --raise notice 'route_trajectory_muid = %', v_muid;
   
   select trajectory_type_muid into v_trajectory_type_muid from gis.route_trajectories where muid = v_muid;
   if not found then
      raise notice 'error(import_rts_round): no route trajectory, muid = %', v_muid;
      return -1;
   end if;
   
   if v_route_round_muid is not null and
      (not v_sign_deleted or exists(select 1 from gis.route_rounds where muid = v_route_round_muid)) then
      
      insert into gis.route_round_number(route_round_muid)
      values (v_route_round_muid)
      on conflict (route_round_muid)
      do nothing;
      
      select atg.action_type_id, ror.route_variant_muid, ror.code, ron.route_round_num
        into v_action_type_id, v_route_variant_muid, v_code, v_round_num
        from gis.route_rounds ror
        join gis.route_round_number ron on ron.route_round_muid = ror.muid
        left join rts.action_type2route_round_types atg on atg.route_round_type_muid = ror.route_round_type_muid
                                                    and atg.route_trajectory_type_muid = v_trajectory_type_muid
       where ror.muid = v_route_round_muid;
      
   elsif v_route_null_round_muid is not null and
      (not v_sign_deleted or exists(select 1 from gis.route_null_rounds where muid = v_route_null_round_muid)) then
      
      insert into gis.route_round_number(route_null_round_muid)
      values (v_route_null_round_muid)
      on conflict (route_null_round_muid)
      do nothing;
      
      select case 
                when atg2.action_type_id is not null and
                     atg2.action_type_id in (ttb.action_type_pkg$maneuver_ab(),
                                             ttb.action_type_pkg$maneuver_ba())
                then atg2.action_type_id
                else coalesce(atg1.action_type_id, atg2.action_type_id)
             end,
             rnr.route_variant_muid, code, ron.route_round_num
        into v_action_type_id, v_route_variant_muid, v_code, v_round_num
        from gis.route_null_rounds rnr
        join gis.route_round_number ron on ron.route_null_round_muid = rnr.muid
        left join rts.action_type2route_null_round_types  atg1 on atg1.route_null_round_type_muid = rnr.route_null_round_type_muid
        left join rts.action_type2route_null_round_types2 atg2 on atg2.route_null_round_type_muid = rnr.route_null_round_type2_muid
                                                              and atg2.route_trajectory_type_muid = v_trajectory_type_muid
       where rnr.muid = v_route_null_round_muid;
      
	if v_action_type_id is null then
         -- Непроизводственные рейсы которым нет соответствия
         raise notice 'error(import_rts_round): no action type route_null_round_muid = %', v_route_null_round_muid;
         return -1;
      end if;
   else
      return -1;
   end if;
   
   select round_id into v_round_id from rts.round2gis where route_trajectory_muid = v_muid;
   
   with parsed as (
      select (xpath('/*/body/index/text()',                   gs_data))[1]::text::bigint order_num,
             (xpath('/*/body/graphSectionStartOffset/text()', gs_data))[1]::text::real   graph_section_start_offset,
             (xpath('/*/body/graphSectionEndOffset/text()',   gs_data))[1]::text::real   graph_section_end_offset,
             (xpath('/*/body/signDeleted/text()',             gs_data))[1]::text::bigint sign_deleted
        from (select unnest(xpath('/*/body/*/graphSections/linkedGraphSection', v_data)) as gs_data) t
    )
    select (select graph_section_start_offset from parsed where sign_deleted = 0 and order_num = 1 ) as graph_section_start_offset,
           (select graph_section_end_offset from parsed where sign_deleted = 0 and order_num = (select max(order_num) from parsed)) as graph_section_end_offset
      into v_graph_section_start_offset, v_graph_section_end_offset;
   
   with parsed as (
      select case when v_round_id is null then nextval(pg_get_serial_sequence('rts.round', 'round_id')) else v_round_id end round_id,
             v_action_type_id                                                           action_type_id,
             v_round_num                                                                round_num,
             v_code                                                                     code,
             (select route_variant_id from rts.route_variant2gis where route_variant_muid = v_route_variant_muid) as route_variant_id,
             (xpath('/*/body/*/trajectoryType/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                              v_data))[1]::text::bigint move_direction_id,
             (xpath('/*/body/*/wkt_geom/text()',              v_data))[1]::text         wkt_geom,
             st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text)     geom,
             ((xpath('/*/body/*/length/text()',               v_data))[1]::text::real * 1000)::integer length,
             ((xpath('/*/body/*/fixedLength/text()',          v_data))[1]::text::real * 1000)::integer length_fixed,
             (xpath('/*/body/*/specification/text()',         v_data))[1]::text         specification,
             v_graph_section_start_offset                                               graph_section_start_offset,
             v_graph_section_end_offset                                                 graph_section_end_offset,
             case when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end sign_deleted
   )
   insert into rts.round as p
         (round_id, move_direction_id, route_variant_id, code, wkt_geom, geom, length, length_fixed, specification,
          graph_section_start_offset, graph_section_end_offset, round_num, sign_deleted, action_type_id)
   select round_id, move_direction_id, route_variant_id, code, wkt_geom, geom, length, length_fixed, specification,
          graph_section_start_offset, graph_section_end_offset, round_num, sign_deleted, action_type_id
     from parsed
    where not sign_deleted or exists (select 1 from rts.round sq where sq.round_id = parsed.round_id)
   on conflict (round_id)
   do update set (move_direction_id, route_variant_id, code, wkt_geom, geom, length, length_fixed, specification,
                  graph_section_start_offset, graph_section_end_offset, round_num, sign_deleted, action_type_id) =
                 (excluded.move_direction_id, excluded.route_variant_id, excluded.code, excluded.wkt_geom, excluded.geom,
                  excluded.length, excluded.length_fixed, excluded.specification, excluded.graph_section_start_offset,
                  excluded.graph_section_end_offset, excluded.round_num, excluded.sign_deleted, excluded.action_type_id)
       where p.move_direction_id is distinct from excluded.move_direction_id or
             p.route_variant_id  is distinct from excluded.route_variant_id or
             p.code              is distinct from excluded.code or
             p.wkt_geom          is distinct from excluded.wkt_geom or
             p.length            is distinct from excluded.length or
             p.length_fixed      is distinct from excluded.length_fixed or
             p.specification     is distinct from excluded.specification or
             p.graph_section_start_offset is distinct from excluded.graph_section_start_offset or
             p.graph_section_end_offset   is distinct from excluded.graph_section_end_offset or
             p.round_num         is distinct from excluded.round_num or
             p.sign_deleted      is distinct from excluded.sign_deleted or
             p.action_type_id    is distinct from excluded.action_type_id
   returning round_id into v_new_round_id;
   
   get diagnostics v_result = row_count;
   
   if v_round_id is null and v_new_round_id is not null then
      insert into rts.round2gis(round_id, route_trajectory_muid) values (v_new_round_id, v_muid);
   end if;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_graph_sec2round
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Секции рейса rts
-- ========================================================================
as $$ declare
   v_result                 bigint    := 0;
   v_tmp                    bigint    := 0;
   v_muid                   bigint    := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_rec                    record;
   v_round_id               integer;
   v_new_graph_sec2round_id integer;
begin
   
   --raise notice 'trajectory_muid = %', v_muid;
   
   if not exists(select 1 from gis.route_trajectories where muid = v_muid) then
      raise notice 'error(import_rts_graph_sec2round): not found route_trajectory_muid = %', v_muid;
      return -1;
   end if;
   
   select round_id into v_round_id from rts.round2gis where route_trajectory_muid = v_muid;
   if v_round_id is null then
      raise notice 'error(import_rts_graph_sec2round): not found round_id, route_trajectory_muid = %', v_muid;
      return -1;
   end if;
   
   --update rts.graph_sec2round set sign_deleted = true where trajectory_id = v_round_id;
   
   update rts.graph_sec2round
      set sign_deleted = true
    where graph_sec2round_id in
          (select sir.graph_sec2round_id
             from rts.graph_sec2round sir
             join rts.graph_sec2trajectory2gis srg on srg.graph_sec2trajectory_id = sir.graph_sec2round_id
            where sir.round_id = v_round_id
              and srg.graph_sec2route_traj_muid not in
                  (select (xpath('/*/body/muid/text()', gs_data))[1]::text::bigint  muid
                     from (select unnest(xpath('/*/body/*/graphSections/linkedGraphSection', v_data)) as gs_data) t
                  )
          );
   
   for v_rec in (
      select (xpath('/*/body/muid/text()',                    gs_data))[1]::text::bigint  muid,
             (select graph_sec2trajectory_id from rts.graph_sec2trajectory2gis
               where graph_sec2route_traj_muid = (xpath('/*/body/muid/text()',
                                                              gs_data))[1]::text::bigint) graph_sec2trajectory_id,
             (select graph_section_id from rts.graph_section2gis
               where graph_section_muid = (xpath('/*/body/graphSection/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                              gs_data))[1]::text::bigint) graph_section_id,
             (xpath('/*/body/index/text()',                   gs_data))[1]::text::bigint  order_num,
             case when (xpath('/*/body/signDeleted/text()',   gs_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end sign_deleted
        from (select unnest(xpath('/*/body/*/graphSections/linkedGraphSection', v_data)) as gs_data) t
   )
   loop
      
      if not exists(select 1 from gis.graph_sec2route_traj where muid = v_rec.muid) then
         raise notice 'error(import_rts_graph_sec2round): not found graph_sec2route_traj_muid = %', v_rec.muid;
         continue;
      end if;
      
      with parsed as (
         select case when v_rec.graph_sec2trajectory_id is null then nextval(pg_get_serial_sequence('rts.graph_sec2round', 'graph_sec2round_id')) else v_rec.graph_sec2trajectory_id end as graph_sec2round_id,
                v_round_id as round_id,
                v_rec.graph_section_id,
                v_rec.order_num,
                v_rec.sign_deleted
      )
      insert into rts.graph_sec2round as p
            (graph_sec2round_id, round_id, graph_section_id, order_num, sign_deleted)
      select graph_sec2round_id, round_id, graph_section_id, order_num, sign_deleted
        from parsed
       where not sign_deleted or exists (select 1 from rts.graph_sec2round sq where sq.graph_sec2round_id = parsed.graph_sec2round_id)
      on conflict (graph_sec2round_id)
      do update set (round_id, graph_section_id, order_num, sign_deleted) =
                    (excluded.round_id, excluded.graph_section_id, excluded.order_num, excluded.sign_deleted)
          where p.round_id         is distinct from excluded.round_id or
                p.graph_section_id is distinct from excluded.graph_section_id or
                p.order_num        is distinct from excluded.order_num or
                p.sign_deleted     is distinct from excluded.sign_deleted
      returning graph_sec2round_id into v_new_graph_sec2round_id;
      
      get diagnostics v_tmp = row_count;
      v_result := v_result + v_tmp;
      
      --raise notice 'v_new_graph_sec2round_id = %, v_rec.muid = %, v_result = %', v_new_graph_sec2round_id, v_rec.muid, v_result;
      
      if v_rec.graph_sec2trajectory_id is null and v_new_graph_sec2round_id is not null then
         insert into rts.graph_sec2trajectory2gis(graph_sec2trajectory_id, graph_sec2route_traj_muid) values (v_new_graph_sec2round_id, v_rec.muid);
      end if;
      
   end loop;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_get_stop_item_type
  (v_round_id           in  integer,
   p_beg_stop_type_id   out smallint,                                      -- Тип первой остановки
   p_end_stop_type_id   out smallint,                                      -- Тип последней остановки
   p_bge_stop_type_id   out smallint)                                      -- Дополнительный тип первой остановки
-- ========================================================================
-- Получить тип начальной/конечной остновки
-- ========================================================================
as $$ declare
   v_action_type_id      integer;
   v_move_direction_id   smallint;
   v_code                text;
begin
   p_beg_stop_type_id := rts.stop_type_pkg$stop();
   p_end_stop_type_id := rts.stop_type_pkg$stop();
   p_bge_stop_type_id := null;
   
   select action_type_id, move_direction_id, code
     into v_action_type_id, v_move_direction_id, v_code
     from rts.round
    where round_id = v_round_id;
   
   if not found then
      raise notice 'not found round %', v_round_id;
      return;
   end if;
   
   if v_action_type_id = ttb.action_type_pkg$park_to_stop() then -- из парка на ОП
      p_beg_stop_type_id := rts.stop_type_pkg$park();
   elsif v_action_type_id = ttb.action_type_pkg$stop_to_park() then -- с ОП в парк
      p_end_stop_type_id := rts.stop_type_pkg$park();
   elsif v_action_type_id = ttb.action_type_pkg$lunch_to_stop() then -- из пункта обеда на ОП
      p_beg_stop_type_id := rts.stop_type_pkg$lunch();
   elsif v_action_type_id = ttb.action_type_pkg$stop_to_lunch() then -- с ОП на обед
      p_end_stop_type_id := rts.stop_type_pkg$lunch();
   elsif v_action_type_id = ttb.action_type_pkg$repair_to_stop() then -- из пункта ремонта на ОП
      p_beg_stop_type_id := rts.stop_type_pkg$repair();
   elsif v_action_type_id = ttb.action_type_pkg$stop_to_repair() then -- с ОП на пункт ремонта
      p_end_stop_type_id := rts.stop_type_pkg$repair();
   elsif v_action_type_id = ttb.action_type_pkg$gs_to_stop() then -- с АЗС на ОП
      p_beg_stop_type_id := rts.stop_type_pkg$gs();
   elsif v_action_type_id = ttb.action_type_pkg$stop_to_gs() then -- с ОП на АЗС
      p_end_stop_type_id := rts.stop_type_pkg$gs();
   elsif v_action_type_id = ttb.action_type_pkg$parking_to_stop() then -- с отстойной площадки на ОП
      p_beg_stop_type_id := rts.stop_type_pkg$parking();
   elsif v_action_type_id = ttb.action_type_pkg$stop_to_parking() then -- с ОП на отстойную площадку
      p_end_stop_type_id := rts.stop_type_pkg$parking();
   end if;
   
   -- Дополнительный тип первой остановки
   if '00' = v_code and v_move_direction_id = rts.move_direction_pkg$forward() then
      p_bge_stop_type_id := rts.stop_type_pkg$finish_a();
   elsif '00' = v_code and v_move_direction_id = rts.move_direction_pkg$back() then
      p_bge_stop_type_id := rts.stop_type_pkg$finish_b();
   end if;
   
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_add_stop_item2round_type
  (v_stop_item2round_id   in integer,
   v_stop_type_id         in smallint,
   v_stop_item_id         in integer)
returns bigint
-- ========================================================================
-- Добавить остановке тип
-- ========================================================================
as $$ declare
   v_result              bigint := 0;
   v_stop_item_type_id   bigint;
begin
   if v_stop_item2round_id is null then
      return v_result;
   end if;
   
   select stop_item_type_id into v_stop_item_type_id
     from rts.stop_item_type
    where stop_item_id = v_stop_item_id
      and stop_type_id = v_stop_type_id;
   
   if not found then
      insert into rts.stop_item_type as p
             (stop_item_id, stop_type_id)
      values (v_stop_item_id, v_stop_type_id)
      returning stop_item_type_id into v_stop_item_type_id;
   else
      update rts.stop_item_type
         set sign_deleted = false
       where stop_item_type_id = v_stop_item_type_id
         and sign_deleted;
   end if;
   
   insert into rts.stop_item2round_type as p
          (stop_item_type_id, stop_item2round_id)
   values (v_stop_item_type_id, v_stop_item2round_id)
   on conflict (stop_item_type_id, stop_item2round_id)
   do update set sign_deleted = false
       where p.sign_deleted;
   
   get diagnostics v_result = row_count;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_stop_item2round_type
  (v_operation    in text,
   v_data         in xml,
   v_round_id     in integer,
   v_tr_type_id   in smallint)
returns bigint
-- ========================================================================
-- Импорт Типы остановочных пунктов для остановок в рейсе rts
-- ========================================================================
as $$ declare
   v_result                 bigint := 0;
   v_tmp                    bigint := 0;
   v_rec                    record;
   v_max_order_num          bigint;
   v_stop_item2round_id     integer;
   v_beg_stop_type_id       smallint;
   v_end_stop_type_id       smallint;
   v_bge_stop_type_id       smallint;
begin
   
   select p_beg_stop_type_id, p_end_stop_type_id, p_bge_stop_type_id
     into v_beg_stop_type_id, v_end_stop_type_id, v_bge_stop_type_id
     from gis.gis_import_pkg_v3$import_rts_get_stop_item_type(v_round_id);
   
   select max(order_num) into v_max_order_num from rts.stop_item2round where round_id = v_round_id and not sign_deleted;
   
   for v_rec in (
      select (xpath('/*/body/muid/text()',                    gs_data))[1]::text::bigint  stop_place2route_traj_muid,
             (xpath('/*/body/index/text()',                   gs_data))[1]::text::bigint  order_num,
             (select is_technical from gis.stop_places
               where muid = (xpath('/*/body/stopPlace/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                              gs_data))[1]::text::bigint) is_technical,
             (select stop_item_id from rts.stop_item2gis
               where tr_type_id = v_tr_type_id
                 and stop_place_muid = (xpath('/*/body/stopPlace/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                              gs_data))[1]::text::bigint) stop_item_id
        from (select unnest(xpath('/*/body/*/stopPlaces/linkedStopPlace', v_data)) as gs_data) t
   )
   loop
      
      select stop2round_id into v_stop_item2round_id from rts.stop2round2gis where stop_place2route_traj_muid = v_rec.stop_place2route_traj_muid;
      if not found or v_stop_item2round_id is null then
         raise notice 'not found stop_item2round_id for stop_place2route_traj_muid = %', v_rec.stop_place2route_traj_muid;
         continue;
      else
         
         update rts.stop_item2round_type set sign_deleted = true where stop_item2round_id = v_stop_item2round_id;
         
         if v_rec.order_num = 1 then
            v_tmp := gis.gis_import_pkg_v3$import_rts_add_stop_item2round_type(v_stop_item2round_id, rts.stop_type_pkg$finish(), v_rec.stop_item_id);
            v_tmp := gis.gis_import_pkg_v3$import_rts_add_stop_item2round_type(v_stop_item2round_id, v_beg_stop_type_id, v_rec.stop_item_id);
            if v_bge_stop_type_id is not null then
               v_tmp := gis.gis_import_pkg_v3$import_rts_add_stop_item2round_type(v_stop_item2round_id, v_bge_stop_type_id, v_rec.stop_item_id);
            end if;
         elsif v_rec.order_num = v_max_order_num then
            v_tmp := gis.gis_import_pkg_v3$import_rts_add_stop_item2round_type(v_stop_item2round_id, rts.stop_type_pkg$finish(), v_rec.stop_item_id);
            v_tmp := gis.gis_import_pkg_v3$import_rts_add_stop_item2round_type(v_stop_item2round_id, v_end_stop_type_id, v_rec.stop_item_id);
         else
            v_tmp := gis.gis_import_pkg_v3$import_rts_add_stop_item2round_type(v_stop_item2round_id, rts.stop_type_pkg$stop(), v_rec.stop_item_id);
         end if;
         v_result := v_result + v_tmp;
         
         if 1 = v_rec.is_technical then
            v_tmp := gis.gis_import_pkg_v3$import_rts_add_stop_item2round_type(v_stop_item2round_id, rts.stop_type_pkg$technical(), v_rec.stop_item_id);
         end if;
         v_result := v_result + v_tmp;
         
      end if;
      
   end loop;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_stop_item2round
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Остановки в рейсе rts
-- ========================================================================
as $$ declare
   v_result                 bigint := 0;
   v_tmp                    bigint := 0;
   v_muid                   bigint := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_route_round_muid       bigint := (xpath('/*/body/*/routeRound/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', v_data))[1]::text::bigint;
   v_route_null_round_muid  bigint := (xpath('/*/body/*/routeNullRound/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', v_data))[1]::text::bigint;
   v_round_id               integer;
   v_tr_type_id             smallint;
   v_rec                    record;
   v_stop_item2round_id     integer;
   v_new_stop_item2round_id integer;
begin
   
   if not exists(select 1 from gis.route_trajectories where muid = v_muid) then
      raise notice 'not found route_trajectory_muid %', v_muid;
      return -1;
   end if;
   
   select round_id into v_round_id from rts.round2gis where route_trajectory_muid = v_muid;
   if v_round_id is null then
      raise notice 'not found round_id for route_trajectory_muid %', v_route_trajectory_muid;
      return -1;
   end if;
   
   if v_route_round_muid is not null then
      
      select trt.tr_type_id::smallint into v_tr_type_id
        from gis.route_trajectories roj
        join gis.route_rounds ror on ror.muid = roj.route_round_muid
        join gis.route_variants rov on rov.muid = ror.route_variant_muid
        join gis.routes rot on rot.muid = rov.route_muid
        join core.tr_type trt on trt.transport_kind_muid = rot.transport_kind_muid
       where roj.muid = v_muid;

   elsif v_route_null_round_muid is not null then
      
      select trt.tr_type_id::smallint into v_tr_type_id
        from gis.route_trajectories roj
        join gis.route_null_rounds ror on ror.muid = roj.route_null_round_muid
        join gis.route_variants rov on rov.muid = ror.route_variant_muid
        join gis.routes rot on rot.muid = rov.route_muid
        join core.tr_type trt on trt.transport_kind_muid = rot.transport_kind_muid
       where roj.muid = v_muid;
   end if;
   
   update rts.stop_item2round
      set sign_deleted = true
    where stop_item2round_id in
          (select sir.stop_item2round_id 
             from rts.stop_item2round sir
             join rts.stop2round2gis srg on srg.stop2round_id = sir.stop_item2round_id
            where round_id = v_round_id
              and srg.stop_place2route_traj_muid not in
                  (select (xpath('/*/body/muid/text()', gs_data))[1]::text::bigint  muid
                     from (select unnest(xpath('/*/body/*/stopPlaces/linkedStopPlace', v_data)) as gs_data) t
                  )
          );
   
   for v_rec in (
      select (xpath('/*/body/muid/text()',                    gs_data))[1]::text::bigint  muid,
             (xpath('/*/body/index/text()',                   gs_data))[1]::text::bigint  order_num,
             ((xpath('/*/body/lengthSector/text()',           gs_data))[1]::text::real * 1000)::integer length_sector,
             (xpath('/*/body/comment/text()',                 gs_data))[1]::text          s_comment,
             (select stop_item_id from rts.stop_item2gis
               where tr_type_id = v_tr_type_id
                 and stop_place_muid = (xpath('/*/body/stopPlace/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                              gs_data))[1]::text::bigint) stop_item_id,
             case when (xpath('/*/body/signDeleted/text()',   gs_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end sign_deleted
        from (select unnest(xpath('/*/body/*/stopPlaces/linkedStopPlace', v_data)) as gs_data) t
   )
   loop
      
      if not exists(select 1 from gis.stop_place2route_traj where muid = v_rec.muid) then
         raise notice 'not found stop_place2route_traj_muid = %', v_rec.muid;
         continue;
      end if;
      
      select stop2round_id into v_stop_item2round_id from rts.stop2round2gis where stop_place2route_traj_muid = v_rec.muid;
      
      with parsed as (
         select case when v_stop_item2round_id is null then nextval(pg_get_serial_sequence('rts.stop_item2round', 'stop_item2round_id')) else v_stop_item2round_id end as stop_item2round_id,
                v_rec.sign_deleted as sign_deleted
      )
      insert into rts.stop_item2round as p
            (stop_item2round_id,    round_id,      stop_item_id,       order_num,       length_sector,         comment, sign_deleted)
      select stop_item2round_id, v_round_id, v_rec.stop_item_id, v_rec.order_num, v_rec.length_sector, v_rec.s_comment, sign_deleted
        from parsed
       where not sign_deleted or exists (select 1 from rts.stop_item2round sq where sq.stop_item2round_id = parsed.stop_item2round_id)
      on conflict (stop_item2round_id)
      do update set (round_id, stop_item_id, order_num, length_sector, comment, sign_deleted) = 
                    (excluded.round_id, excluded.stop_item_id, excluded.order_num, excluded.length_sector, excluded.comment, excluded.sign_deleted)
          where p.round_id      is distinct from excluded.round_id or
                p.stop_item_id  is distinct from excluded.stop_item_id or
                p.order_num     is distinct from excluded.order_num or
                p.length_sector is distinct from excluded.length_sector or
                p.comment       is distinct from excluded.comment or
                p.sign_deleted  is distinct from excluded.sign_deleted
      returning stop_item2round_id into v_new_stop_item2round_id;
      
      get diagnostics v_tmp = row_count;
      v_result := v_result + v_tmp;
      
      if v_stop_item2round_id is null and v_new_stop_item2round_id is not null then
         insert into rts.stop2round2gis(stop2round_id, stop_place2route_traj_muid) values (v_new_stop_item2round_id, v_rec.muid);
      end if;
      
   end loop;
   
   -- Установить Типы остановочных пунктов для остановок в рейсе
   v_tmp := gis.gis_import_pkg_v3$import_rts_stop_item2round_type (v_operation, v_data, v_round_id, v_tr_type_id);
   v_result := v_result + v_tmp;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_stop
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт ОП rts
-- ========================================================================
as $$ declare
   v_result            bigint := 0;
   v_muid              bigint := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_direction_muid    bigint := (xpath('/*/body/*/direction/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', v_data))[1]::text::bigint;
   v_stop_id           integer;
   v_new_stop_id       integer;
   v_stop_group_id     smallint;
   v_new_stop_group_id smallint := gis.movement_directions_pkg$md_to_rts(v_direction_muid);
begin
   
   select stop_id into v_stop_id from rts.stop2gis where stop_muid = v_muid;
-- raise notice 'stop_id = %', v_stop_id;
   
   with parsed as (
      select case when v_stop_id is null then nextval(pg_get_serial_sequence('rts.stop', 'stop_id')) else v_stop_id end stop_id,
             (xpath('/*/body/*/name/text()',                  v_data))[1]::text s_name,
             (xpath('/*/body/*/nameInEnglish/text()',         v_data))[1]::text name_in_english,
             (xpath('/*/body/*/district/body/name/text()',    v_data))[1]::text district,
             (xpath('/*/body/*/region/body/name/text()',      v_data))[1]::text region,
             (xpath('/*/body/*/street/body/name/text()',      v_data))[1]::text street,
             case when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false  end sign_deleted
   )
   insert into rts.stop as p
         (stop_id, name,   name_in_english, sign_deleted, district, region, street)
   select stop_id, s_name, name_in_english, sign_deleted, district, region, street
     from parsed
    where not sign_deleted or exists (select 1 from rts.stop sq where sq.stop_id = parsed.stop_id)
   on conflict (stop_id)
   do update set (name, name_in_english, sign_deleted, district, region, street) =
                 (excluded.name, excluded.name_in_english, excluded.sign_deleted,
                  excluded.district, excluded.region, excluded.street)
   where p.name            is distinct from excluded.name or
         p.name_in_english is distinct from excluded.name_in_english or
         p.sign_deleted    is distinct from excluded.sign_deleted or
         p.district        is distinct from excluded.district or
         p.region          is distinct from excluded.region or
         p.street          is distinct from excluded.street
   returning stop_id into v_new_stop_id;
   
   get diagnostics v_result = row_count;
   
   if v_stop_id is null and v_new_stop_id is not null then
      insert into rts.stop2gis(stop_id, stop_muid) values (v_new_stop_id, v_muid);
   end if;
   
   if v_stop_id is not null or v_new_stop_id is not null then
      if v_new_stop_group_id is not null then
      
         insert into rts.stop2stop_group
                (stop_group_id, stop_id)
         values (v_new_stop_group_id, coalesce(v_stop_id, v_new_stop_id))
         on conflict (stop_group_id, stop_id)
         do nothing;
      
         if v_stop_id is not null then
            delete from rts.stop2stop_group
             where stop_id = v_stop_id
               and stop_group_id <> v_new_stop_group_id
               and stop_group_id in (rts.stop_group_pkg$md_even(),
                                     rts.stop_group_pkg$md_odd(),
                                     rts.stop_group_pkg$md_both());
         end if;
      elsif v_stop_id is not null then
         delete from rts.stop2stop_group
          where stop_id = v_stop_id
            and stop_group_id in (rts.stop_group_pkg$md_even(),
                                  rts.stop_group_pkg$md_odd(),
                                  rts.stop_group_pkg$md_both());
      end if;
   end if;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_stop_location
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Геометрия объектов rts
-- ========================================================================
as $$ declare
   v_result           bigint    := 0;
   v_muid             bigint    := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_stop_location_id       integer;
   v_new_stop_location_id   integer;
begin
   
   select stop_location_id into v_stop_location_id from rts.stop_location2gis where stop_location_muid = v_muid;
   
   with parsed as (
      select case when v_stop_location_id is null then nextval(pg_get_serial_sequence('rts.stop_location', 'stop_location_id')) else v_stop_location_id end stop_location_id,
             (select graph_section_id from rts.graph_section2gis
               where graph_section_muid = (xpath('/*/body/*/graphSection/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                 v_data))[1]::text::bigint
             ) graph_section_id,
             (select stop_id from rts.stop2gis
               where stop_muid = (xpath('/*/body/*/stop/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                                 v_data))[1]::text::bigint
             ) stop_id,
             (select zone_location_id from rts.zone_location2park_zones
               where park_zones_muid = (xpath('/*/body/*/park_zone/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                 v_data))[1]::text::bigint
             ) depo_zone_location_id,
             (xpath('/*/body/*/graphSection/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                 v_data))[1]::text::bigint graph_section_muid,
             (xpath('/*/body/*/graphSectionOffset/text()',       v_data))[1]::text::real   graph_section_offset,
             (xpath('/*/body/*/wkt_geom/text()',                 v_data))[1]::text         wkt_geom,
             st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text)        geom,
             (xpath('/*/body/*/buildingDistance/text()',         v_data))[1]::text::real   building_distance,
             (xpath('/*/body/*/buildingAddressString/text()',    v_data))[1]::text         building_address,
             case when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false  end sign_deleted
   )
   insert into rts.stop_location as p
         (stop_location_id, graph_section_id, stop_id, geom, wkt_geom, graph_section_offset, building_distance, building_address, sign_deleted, depo_zone_location_id)
   select stop_location_id, graph_section_id, stop_id, geom, wkt_geom, graph_section_offset, building_distance, building_address, sign_deleted, depo_zone_location_id
     from parsed
    where not sign_deleted or exists (select 1 from rts.stop_location sq where sq.stop_location_id = parsed.stop_location_id)
   on conflict (stop_location_id)
   do update set (graph_section_id, stop_id, geom, wkt_geom, graph_section_offset, building_distance, building_address, sign_deleted, depo_zone_location_id) =
                 (excluded.graph_section_id, excluded.stop_id, excluded.geom, excluded.wkt_geom, excluded.graph_section_offset, 
                  excluded.building_distance, excluded.building_address, excluded.sign_deleted, excluded.depo_zone_location_id)
       where p.graph_section_id     is distinct from excluded.graph_section_id or
             p.stop_id              is distinct from excluded.stop_id  or
             p.wkt_geom             is distinct from excluded.wkt_geom or
             p.graph_section_offset is distinct from excluded.graph_section_offset or
             p.building_distance    is distinct from excluded.building_distance or
             p.building_address     is distinct from excluded.building_address  or
             p.sign_deleted         is distinct from excluded.sign_deleted or
             p.depo_zone_location_id is distinct from excluded.depo_zone_location_id
   returning stop_location_id into v_new_stop_location_id;
   
   get diagnostics v_result = row_count;
   
   if v_stop_location_id is null and v_new_stop_location_id is not null then
      insert into rts.stop_location2gis(stop_location_id, stop_location_muid) values (v_new_stop_location_id, v_muid);
   end if;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_stop_item
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Классификация остановок rts
-- ========================================================================
as $$ declare
   v_result           bigint    := 0;
   v_tmp              bigint    := 0;
   v_muid             bigint    := (xpath('/object-message/body/*/muid/text()',     v_data))[1]::text::bigint;
   v_sign_deleted     boolean   := case when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end;
   v_stop_location_id integer;
   v_stop_item_id     integer;
   v_new_stop_item_id integer;
   v_rec              record;
begin
   --raise notice 'stop_place_muid = %', v_muid;
   select stop_location_id into v_stop_location_id from rts.stop_location2gis where stop_location_muid = v_muid;
   
   for v_rec in (
      with parsed_type as (
         select unnest(array[1, 2, 3, 4]) as tr_type_id,
                unnest(array[(xpath('/*/body/*/hasBus/text()',       v_data))[1]::text::bigint,
                             (xpath('/*/body/*/hasTrolley/text()',   v_data))[1]::text::bigint,
                             (xpath('/*/body/*/hasTram/text()',      v_data))[1]::text::bigint,
                             (xpath('/*/body/*/hasSpeedTram/text()', v_data))[1]::text::bigint]) as flag_active
      )
      select tr_type_id,
             case
                when exists(select 1 from parsed_type where parsed_type.flag_active = 1 and parsed_type.tr_type_id = tr_type.tr_type_id) then false
                else true
             end as sign_deleted
        from core.tr_type
   )
   loop
      
      select stop_item_id into v_stop_item_id from rts.stop_item2gis where stop_place_muid = v_muid and tr_type_id = v_rec.tr_type_id;
      
      with parsed as (
         select case when v_stop_item_id is null then nextval(pg_get_serial_sequence('rts.stop_item', 'stop_item_id')) else v_stop_item_id end stop_item_id,
                v_stop_location_id as stop_location_id
      )
      insert into rts.stop_item as p
            (stop_item_id, tr_type_id, sign_deleted, stop_location_id)
      select stop_item_id, v_rec.tr_type_id,
             case when v_sign_deleted then true else v_rec.sign_deleted end,
             stop_location_id
        from parsed
       where not v_sign_deleted or exists (select 1 from rts.stop_item sq where sq.stop_item_id = parsed.stop_item_id)
      on conflict (stop_item_id)
      do update set (tr_type_id, sign_deleted, stop_location_id) =
                    (excluded.tr_type_id, excluded.sign_deleted, excluded.stop_location_id)
          where p.tr_type_id       is distinct from excluded.tr_type_id or
                p.sign_deleted     is distinct from excluded.sign_deleted or
                p.stop_location_id is distinct from excluded.stop_location_id
      returning stop_item_id into v_new_stop_item_id;
      
      get diagnostics v_tmp = row_count;
      v_result := v_result + v_tmp;
      
      if v_stop_item_id is null and v_new_stop_item_id is not null then
         insert into rts.stop_item2gis(stop_item_id, tr_type_id, stop_place_muid) values (v_new_stop_item_id, v_rec.tr_type_id, v_muid);
      end if;
      
   end loop;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_zone_location2park_zones
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Зоны парков rts
-- ========================================================================
as $$ declare
   v_result                 bigint    := 0;
   v_muid                   bigint    := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_zone_location_id       integer;
   v_new_zone_location_id   integer;
begin
   
   select zone_location_id into v_zone_location_id from rts.zone_location2park_zones where park_zones_muid = v_muid;
   
   with parsed as (
      select case when v_zone_location_id is null then nextval(pg_get_serial_sequence('rts.zone_location', 'zone_location_id')) else v_zone_location_id end zone_location_id,
             (xpath('/*/body/*/wkt_geom/text()',                 v_data))[1]::text wkt_geom,
             st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text) geom,
             case when (xpath('/*/body/*/signDeleted/text()',    v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end sign_deleted
   )
   insert into rts.zone_location as p
         (zone_location_id, wkt_geom, geom, sign_deleted)
   select zone_location_id, wkt_geom, geom, sign_deleted
     from parsed
    where not sign_deleted or exists (select 1 from rts.zone_location sq where sq.zone_location_id = parsed.zone_location_id)
   on conflict (zone_location_id)
   do update set (wkt_geom, geom, sign_deleted) =
                 (excluded.wkt_geom, excluded.geom, excluded.sign_deleted)
   where p.wkt_geom        is distinct from excluded.wkt_geom or
         p.geom            is distinct from excluded.geom or
         p.sign_deleted    is distinct from excluded.sign_deleted
   returning zone_location_id into v_new_zone_location_id;
   
   get diagnostics v_result = row_count;
   
   if v_zone_location_id is null and v_new_zone_location_id is not null then
      insert into rts.zone_location2park_zones(zone_location_id, park_zones_muid) values (v_new_zone_location_id, v_muid);
   end if;
   
   delete from rts.depo2zone
    where (zone_location_id, depo_id) in 
          (with parsed as 
           (select ((select depo_id from core.depo
                      where park_muid = (xpath('/park/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                               sp_data))[1]::text::bigint)
                                        )        depo_id,
                                        coalesce(v_zone_location_id, v_new_zone_location_id) zone_location_id,
                                        case when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end sign_deleted
                       from (select unnest(xpath('/*/body/*/parks/park', v_data)) as sp_data) t
           )
           select zone_location_id, depo_id from parsed where sign_deleted
          );
   
   with parsed as (
      select ((select depo_id from core.depo
                where park_muid = (xpath('/park/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                            sp_data))[1]::text::bigint)) depo_id,
             coalesce(v_zone_location_id, v_new_zone_location_id)                        zone_location_id,
             case when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end sign_deleted
        from (select unnest(xpath('/*/body/*/parks/park', v_data)) as sp_data) t
   )
   insert into rts.depo2zone as p (zone_location_id, depo_id)
   select zone_location_id, depo_id
     from parsed
    where not sign_deleted
   on conflict (zone_location_id, depo_id)
   do nothing;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_zone_location2park_zones_territory
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Зоны парков - территория rts
-- ========================================================================
as $$ declare
   v_result                 bigint    := 0;
   v_rec                    record;
   v_park_zone_muid         bigint    := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_zone_location_id       bigint;
   v_territory_id           bigint;
begin
   
   select zone_location_id into v_zone_location_id from rts.zone_location2park_zones where park_zones_muid = v_park_zone_muid;
   select territory_id into v_territory_id from core.territory where park_zone_muid = v_park_zone_muid;
   if v_zone_location_id is null or v_territory_id is null then
      return v_result;
   end if;
   
   insert into rts.territory2zone as p
          (zone_location_id, territory_id)
   values (v_zone_location_id, v_territory_id)
   on conflict (zone_location_id)
   do update set territory_id = excluded.territory_id
   where p.territory_id is distinct from excluded.territory_id;
   
   get diagnostics v_result = row_count;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_zone_location2stop_zones
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Зоны посадки-высадки rts
-- ========================================================================
as $$ declare
   v_result                 bigint    := 0;
   v_muid                   bigint    := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_zone_location_id       integer;
   v_new_zone_location_id   integer;
begin
   
   select zone_location_id into v_zone_location_id from rts.zone_location2stop_zones where stop_zones_muid = v_muid;
   
   with parsed as (
      select case when v_zone_location_id is null then nextval(pg_get_serial_sequence('rts.zone_location', 'zone_location_id')) else v_zone_location_id end zone_location_id,
             (xpath('/*/body/*/wkt_geom/text()',                 v_data))[1]::text  wkt_geom,
             st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text) geom,
             case when (xpath('/*/body/*/signDeleted/text()',    v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end sign_deleted
   )
   insert into rts.zone_location as p
         (zone_location_id, wkt_geom, geom, sign_deleted)
   select zone_location_id, wkt_geom, geom, sign_deleted
     from parsed
    where not sign_deleted or exists (select 1 from rts.zone_location sq where sq.zone_location_id = parsed.zone_location_id)
   on conflict (zone_location_id)
   do update set (wkt_geom, geom, sign_deleted) =
                 (excluded.wkt_geom, excluded.geom, excluded.sign_deleted)
   where p.wkt_geom        is distinct from excluded.wkt_geom or
         p.geom            is distinct from excluded.geom or
         p.sign_deleted    is distinct from excluded.sign_deleted
   returning zone_location_id into v_new_zone_location_id;
   
   get diagnostics v_result = row_count;
   
   if v_zone_location_id is null and v_new_zone_location_id is not null then
      insert into rts.zone_location2stop_zones(zone_location_id, stop_zones_muid) values (v_new_zone_location_id, v_muid);
      v_zone_location_id := v_new_zone_location_id;
   end if;
   
   update rts.stop_location
      set zone_location_id = v_zone_location_id
    where zone_location_id is distinct from v_zone_location_id
      and stop_location_id in
          (select sti.stop_location_id from rts.stop_item sti
             join rts.stop_item2gis stg on stg.stop_item_id = sti.stop_item_id
            where stg.stop_place_muid = (xpath('/*/body/*/stopPlace/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                               v_data))[1]::text::bigint);
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_zone_location2term_point_zones
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Зона конечного пункта rts
-- ========================================================================
as $$ declare
   v_result                 bigint    := 0;
   v_muid                   bigint    := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_zone_location_id       integer;
   v_new_zone_location_id   integer;
begin
   
   select zone_location_id into v_zone_location_id from rts.zone_location2terminal_point_zones where terminal_point_zones_muid = v_muid;
   
   with parsed as (
      select case when v_zone_location_id is null then nextval(pg_get_serial_sequence('rts.zone_location', 'zone_location_id')) else v_zone_location_id end zone_location_id,
             (xpath('/*/body/*/wkt_geom/text()',                 v_data))[1]::text  wkt_geom,
             st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text) geom,
             case when (xpath('/*/body/*/signDeleted/text()',    v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end sign_deleted
   )
   insert into rts.zone_location as p
         (zone_location_id, wkt_geom, geom, sign_deleted)
   select zone_location_id, wkt_geom, geom, sign_deleted
     from parsed
    where (not sign_deleted or exists (select 1 from rts.zone_location sq where sq.zone_location_id = parsed.zone_location_id))
      and wkt_geom is not null
   on conflict (zone_location_id)
   do update set (wkt_geom, geom, sign_deleted) =
                 (excluded.wkt_geom, excluded.geom, excluded.sign_deleted)
   where p.wkt_geom        is distinct from excluded.wkt_geom or
         p.geom            is distinct from excluded.geom or
         p.sign_deleted    is distinct from excluded.sign_deleted
   returning zone_location_id into v_new_zone_location_id;
   
   get diagnostics v_result = row_count;
   
   if v_zone_location_id is null and v_new_zone_location_id is not null then
      insert into rts.zone_location2terminal_point_zones(zone_location_id, terminal_point_zones_muid) values (v_new_zone_location_id, v_muid);
   end if;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_zone_location2transport_parkings
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Зона конечного пункта rts
-- ========================================================================
as $$ declare
   v_result                 bigint    := 0;
   v_muid                   bigint    := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_zone_location_id       integer;
   v_new_zone_location_id   integer;
begin
   
   select zone_location_id into v_zone_location_id from rts.zone_location2transport_parkings where transport_parkings_muid = v_muid;
   
   with parsed as (
      select case when v_zone_location_id is null then nextval(pg_get_serial_sequence('rts.zone_location', 'zone_location_id')) else v_zone_location_id end zone_location_id,
             (xpath('/*/body/*/wkt_geom/text()',                 v_data))[1]::text  wkt_geom,
             st_geomfromtext((xpath('/*/body/*/wkt_geom/text()', v_data))[1]::text) geom,
             case when (xpath('/*/body/*/signDeleted/text()',    v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false  end sign_deleted
   )
   insert into rts.zone_location as p
         (zone_location_id, wkt_geom, geom, sign_deleted)
   select zone_location_id, wkt_geom, geom, sign_deleted
     from parsed
    where not sign_deleted or exists (select 1 from rts.zone_location sq where sq.zone_location_id = parsed.zone_location_id)
   on conflict (zone_location_id)
   do update set (wkt_geom, geom, sign_deleted) =
                 (excluded.wkt_geom, excluded.geom, excluded.sign_deleted)
   where p.wkt_geom        is distinct from excluded.wkt_geom or
         p.geom            is distinct from excluded.geom or
         p.sign_deleted    is distinct from excluded.sign_deleted
   returning zone_location_id into v_new_zone_location_id;
   
   get diagnostics v_result = row_count;
   
   if v_zone_location_id is null and v_new_zone_location_id is not null then
      insert into rts.zone_location2transport_parkings(zone_location_id, transport_parkings_muid) values (v_new_zone_location_id, v_muid);
   end if;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_parking
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Отойно-разворотная площадка rts
-- ========================================================================
as $$ declare
   v_result           bigint := 0;
   v_muid             bigint := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_parking_id       integer;
   v_new_parking_id   integer;
begin
   
   select parking_id into v_parking_id from rts.parking2gis where transport_parkings_muid = v_muid;
   
   with parsed as (
      select case when v_parking_id is null then nextval(pg_get_serial_sequence('core.entity', 'entity_id')) else v_parking_id end entity_id,
             (xpath('/*/body/*/name/text()',                  v_data))[1]::text s_name,
             case when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false  end sign_deleted
   )
   insert into core.entity as p
         (entity_id, name_full, name_short)
   select entity_id, s_name, s_name
     from parsed
    where not sign_deleted or exists (select 1 from rts.parking sq where sq.parking_id = parsed.entity_id)
   on conflict (entity_id)
   do update set (name_full, name_short) =
                 (excluded.name_full, excluded.name_short)
   where p.name_full  is distinct from excluded.name_full or
         p.name_short is distinct from excluded.name_short
   returning entity_id into v_new_parking_id;
   
   with parsed as (
      select coalesce(v_parking_id, v_new_parking_id) parking_id,
             (xpath('/*/body/*/square/text()',                 v_data))[1]::text::integer  square,
             (xpath('/*/body/*/capacity/text()',               v_data))[1]::text::smallint tr_count,
              case when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false  end sign_deleted
   )
   insert into rts.parking as p
         (parking_id, square, tr_count, sign_deleted)
   select parking_id, square, coalesce(tr_count, 0), sign_deleted
     from parsed
    where not sign_deleted or exists (select 1 from rts.parking sq where sq.parking_id = parsed.parking_id)
   on conflict (parking_id)
   do update set (square, tr_count, sign_deleted) =
                 (excluded.square, excluded.tr_count, excluded.sign_deleted)
   where p.square       is distinct from excluded.square or
         p.tr_count     is distinct from excluded.tr_count or
         p.sign_deleted is distinct from excluded.sign_deleted;
   
   get diagnostics v_result = row_count;
   
   if v_parking_id is null and v_new_parking_id is not null then
      insert into rts.parking2gis(parking_id, transport_parkings_muid) values (v_new_parking_id, v_muid);
   end if;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_parking2zone
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Отойно-разворотная площадка rts
-- ========================================================================
as $$ declare
   v_result           bigint    := 0;
   v_muid             bigint    := (xpath('/object-message/body/*/muid/text()',     v_data))[1]::text::bigint;
   v_sign_deleted     boolean   := case when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end;
   v_parking_id       integer;
begin
   
   select parking_id into v_parking_id from rts.parking2gis where transport_parkings_muid = v_muid;
   
-- delete from rts.parking2zone where parking_id = parking_id and v_sign_deleted
   
   with parsed as (
      select (select zone_location_id from rts.zone_location2transport_parkings where transport_parkings_muid = v_muid) as zone_location_id,
             v_parking_id   as parking_id,
             v_sign_deleted as sign_deleted
   )
   insert into rts.parking2zone as p
         (zone_location_id, parking_id)
   select zone_location_id, parking_id
     from parsed
    where not sign_deleted
   on conflict (zone_location_id, parking_id)
   do nothing;
   
   get diagnostics v_result = row_count;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_route_null_round_type2
   (v_operation in text,
    v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Типы нулевых рейсов 2 из XML типа
-- ========================================================================
as $$ declare
   v_result    bigint := 0;
   v_tmp       bigint := 0;
   v_muid      bigint := (xpath('/object-message/body/*/muid/text()', v_data))[1]::text::bigint;
   v_name      text   := (xpath('/object-message/body/*/name/text()', v_data))[1]::text;
begin
   with parsed as (
      select v_muid as muid,
             v_name as s_name,
             (xpath('/object-message/body/*/version/text()',   v_data))[1]::text::bigint n_version,
             case when (xpath('/object-message/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then 1 else 0 end sign_deleted,
             clock_timestamp() cur_time
   )
   insert into gis.ref_route_null_round_types2 as rrk 
         (muid, name,   version,   sign_deleted, insert_date)
   select muid, s_name, n_version, sign_deleted, cur_time
     from parsed
    where sign_deleted = 0 or exists (select 1 from gis.ref_route_null_round_types2 sq where sq.muid = parsed.muid)
   on conflict (muid)
   do update set (name, version, sign_deleted, insert_date) =
                 (excluded.name,
                  excluded.version,
                  excluded.sign_deleted,
                  excluded.insert_date)
    where rrk.name         is distinct from excluded.name or
          rrk.version      is distinct from excluded.version or
          rrk.sign_deleted is distinct from excluded.sign_deleted;
    
    get diagnostics v_result = row_count;
    
    update ttb.action_type as act
       set action_type_gis_name = v_name
      from rts.action_type2route_null_round_types2 atr
     where act.action_type_id = atr.action_type_id
       and atr.route_null_round_type_muid = v_muid
       and act.action_type_gis_name is distinct from v_name;
    
    get diagnostics v_tmp = row_count;
    
    return v_result + v_tmp;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_route_rounds
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Производственных рейсов rts
-- ========================================================================
as $$ declare
   v_result                bigint  := 0;
   v_tmp                   bigint  := 0;
   v_rec                   record;
   
   v_route_round_muid      bigint  := (xpath('/object-message/body/*/muid/text()',     v_data))[1]::text::bigint;
   v_sign_deleted          boolean := case when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end;
   v_route_round_type_muid bigint  := (xpath('/*/body/*/type/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()', 
                                                                                       v_data))[1]::text::bigint;
   v_code                  text    := (xpath('/*/body/*/code/text()',                  v_data))[1]::text;
begin
-- raise notice 'route_round_muid = %', v_route_round_muid;
-- raise notice 'route_round_type_muid = %', v_route_round_type_muid;
   
   for v_rec in (
      select rdg.route_trajectory_muid, rtj.route_round_muid, rtj.trajectory_type_muid, atg.action_type_id, rdg.round_id
        from gis.route_trajectories rtj
        join rts.round2gis rdg on rdg.route_trajectory_muid = rtj.muid
        left join rts.action_type2route_round_types atg on atg.route_round_type_muid = v_route_round_type_muid
                                                       and atg.route_trajectory_type_muid = rtj.trajectory_type_muid
       where rtj.sign_deleted = 0
         and rtj.route_round_muid = v_route_round_muid
   )
   loop
      update rts.round
         set code           = v_code,
             action_type_id = v_rec.action_type_id,
             sign_deleted   = v_sign_deleted
       where round_id = v_rec.round_id
         and (code           is distinct from v_code or
              action_type_id is distinct from v_rec.action_type_id or
              sign_deleted   is distinct from v_sign_deleted);
      
      get diagnostics v_tmp = row_count;
      v_result := v_result + v_tmp;
   end loop;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_import_pkg_v3$import_rts_route_null_rounds
  (v_operation in text,
   v_data      in xml)
returns bigint
-- ========================================================================
-- Импорт Нулевых рейсов rts
-- ========================================================================
as $$ declare
   v_result                bigint  := 0;
   v_tmp                   bigint  := 0;
   v_rec                   record;
   
   v_route_null_round_muid bigint  := (xpath('/*/body/*/muid/text()',                  v_data))[1]::text::bigint;
   v_sign_deleted          boolean := case when (xpath('/*/body/*/signDeleted/text()', v_data))[1]::text::bigint = 1 or v_operation = 'DELETE' then true else false end;
   
   v_route_null_round_type_muid  bigint := (xpath('/*/body/*/route_null_round_type_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                                       v_data))[1]::text::bigint;
   v_route_null_round_type2_muid bigint := (xpath('/*/body/*/type2_muid/header/identSet/componentId[component[text()="GIS_MGT"]]/componentIdent/text()',
                                                                                       v_data))[1]::text::bigint;
   v_code                  text    := (xpath('/*/body/*/code/text()',                  v_data))[1]::text;
begin
-- raise notice 'route_null_round_muid = %', v_route_null_round_muid;
-- raise notice 'sign_deleted = %', v_sign_deleted;
-- raise notice 'route_null_round_type_muid = %', v_route_null_round_type_muid;
-- raise notice 'route_null_round_type2_muid = %', v_route_null_round_type2_muid;
-- raise notice 'code = %', v_code;
-- return 0;
   
   for v_rec in (
      
      select case 
                when atg2.action_type_id is not null and
                     atg2.action_type_id in (ttb.action_type_pkg$maneuver_ab(),
                                             ttb.action_type_pkg$maneuver_ba())
                then atg2.action_type_id
                else coalesce(atg1.action_type_id, atg2.action_type_id)
             end as action_type_id,
             rdg.round_id
        from gis.route_trajectories rtj
        join rts.round2gis rdg on rdg.route_trajectory_muid = rtj.muid
        left join rts.action_type2route_null_round_types  atg1 on atg1.route_null_round_type_muid = v_route_null_round_type_muid
        left join rts.action_type2route_null_round_types2 atg2 on atg2.route_null_round_type_muid = v_route_null_round_type2_muid
                                                              and atg2.route_trajectory_type_muid = rtj.trajectory_type_muid
       where rtj.sign_deleted = 0
         and rtj.route_null_round_muid = v_route_null_round_muid
   )
   loop
--    raise notice 'v_rec.action_type_id = %', v_rec.action_type_id;
--    raise notice 'v_rec.round_id = %', v_rec.round_id;
      
      update rts.round
         set code           = v_code,
             action_type_id = v_rec.action_type_id,
             sign_deleted   = v_sign_deleted
       where round_id = v_rec.round_id
         and (code           is distinct from v_code or
              action_type_id is distinct from v_rec.action_type_id or
              sign_deleted   is distinct from v_sign_deleted);
      
      get diagnostics v_tmp = row_count;
      v_result := v_result + v_tmp;
      
   end loop;
   
   return v_result;
end;
$$ language plpgsql;
