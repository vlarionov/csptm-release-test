CREATE OR REPLACE FUNCTION gis."jf_ref_transport_kinds_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$
declare
  l_rf_db_method TEXT;

begin
  
  l_rf_db_method := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE), '');

  
  open p_rows for
  select MUID::text AS "MUID",
         SHORT_NAME AS "SHORT_NAME",
         NAME AS "NAME",
         VERSION AS "VERSION",
         SIGN_DELETED AS "SIGN_DELETED",
         INSERT_DATE AS "INSERT_DATE",
         UPDATE_DATE AS "UPDATE_DATE"
  from gis.ref_transport_kinds tk
  ;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;