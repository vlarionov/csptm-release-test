-- Function: gis."jf_route_rounds_filter_pkg$of_rows"(numeric, text)

-- DROP FUNCTION gis."jf_route_rounds_filter_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION gis.jf_route_rounds_filter_pkg$of_rows(
  IN p_id_account numeric,
  OUT p_rows refcursor,
  IN p_attr text)
  RETURNS refcursor AS
  $BODY$
declare
  l_route_rounds_code text := jofl.jofl_pkg$extract_varchar(p_attr, 'route_rounds_code', TRUE);
begin
 open p_rows for
    select
      rtk.muid::text as trtype_id,
      rtk.name as trtype_name,
      rv.muid::text as routeid,
      r.number as route_name,
      rr.muid::text as rounds_muid,
      rr.code as "code",
      s_a.name as stop_name_a,
      s_b.name as stop_name_b
    from
      gis.routes r
      join gis.ref_transport_kinds rtk on rtk.muid = r.transport_kind_muid
      join gis.route_variants rv on r.current_route_variant_muid = rv.muid
      join gis.route_rounds rr on rv.muid = rr.route_variant_muid
      join gis.stop_places sp_a on rr.stop_place_a_muid = sp_a.muid
      JOIN gis.stop_places sp_b on rr.stop_place_b_muid = sp_b.muid
      join gis.stops s_a on sp_a.stop_muid = s_a.muid
      join gis.stops s_b on sp_b.stop_muid = s_b.muid
    where rr.sign_deleted = 0
    and rv.sign_deleted = 0
    and sp_a.sign_deleted = 0
    and sp_b.sign_deleted = 0
    and rtk.sign_deleted = 0
    and (rr.code = l_route_rounds_code or l_route_rounds_code is null)
    and r.agency_muid = gis.jf_agencies_pkg$cn_agency_mgt();

end;
 $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION gis."jf_route_rounds_filter_pkg$of_rows"(numeric, text)
OWNER TO adv;
