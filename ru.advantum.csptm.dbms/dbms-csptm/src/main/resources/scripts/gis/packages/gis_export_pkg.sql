﻿CREATE OR REPLACE FUNCTION gis.gis_export_pkg$get_gr_num_list(
  p_route_muid BIGINT,
  p_date_from  TIMESTAMP[],
  p_date_to    TIMESTAMP[],
  p_result OUT REFCURSOR
)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
BEGIN
  OPEN p_result FOR
  SELECT
    ctr.garage_num gr_num,
    e.name_full    park_name,
    ctr.depo_id    park_id,
    count(ctr.garage_num)       nav_point_count

  FROM asd.tf_graph_link tgl
    JOIN core.tr ctr ON tgl.tr_id = ctr.tr_id
    JOIN core.entity e ON ctr.depo_id = e.entity_id
    JOIN tt.order_round ord ON tgl.order_round_id = ord.order_round_id
    JOIN tt.round r ON ord.round_id = r.round_id
    JOIN tt.timetable_entry te ON r.timetable_entry_id = te.timetable_entry_id
    JOIN tt.timetable tt ON te.timetable_id = tt.timetable_id
  WHERE
    exists(SELECT
           FROM (SELECT unnest(p_date_from) as t1, unnest(p_date_to) as t2) as tab
           WHERE tgl.event_time>=t1 and tgl.event_time<t2)
        AND tt.route_muid = p_route_muid
  GROUP BY  ctr.garage_num,e.name_full,ctr.depo_id;
END;
$$;

CREATE OR REPLACE FUNCTION gis.gis_export_pkg$get_nav_point_list(
  p_route_muid BIGINT[],
  p_date_from  TIMESTAMP[],
  p_date_to    TIMESTAMP[],
  p_lonmin FLOAT,
  p_lonmax FLOAT,
  p_latmin FLOAT,
  p_latmax FLOAT,
  p_headingmin INTEGER,
  p_headingmax INTEGER,
  p_speedmin INTEGER,
  p_speedmax INTEGER,
  p_gr_num_list INTEGER[],
  p_result OUT REFCURSOR
)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
BEGIN
    OPEN p_result FOR
  SELECT
    tgl.packet_id           point_id,
    tgl.event_time          point_date,
    tgl.tr_id,
    t.lon                   lon,
    t.lat                   lat,
    t.heading               heading,
    tgl.order_num           point_num,
    NULL                    point_tag,
    tt.route_muid           route_id,
    r.round_id              race_id,
    r.route_trajectory_muid route_trajectory_id,
    NULL                    stop_id,
    ctr.garage_num          gr_num,
    ctr.licence             gos_num,
    t.speed                 speed
  FROM core.tr ctr
    JOIN asd.tf_graph_link tgl ON ctr.tr_id = tgl.tr_id
    JOIN tt.order_round ord ON tgl.order_round_id = ord.order_round_id
    JOIN tt.round r ON ord.round_id = r.round_id
    JOIN tt.timetable_entry te ON r.timetable_entry_id = te.timetable_entry_id
    JOIN tt.timetable tt ON te.timetable_id = tt.timetable_id
    JOIN core.traffic t ON tgl.unit_id = t.unit_id AND tgl.event_time = t.event_time AND tgl.packet_id = t.packet_id
  WHERE (tt.route_muid = ANY(p_route_muid))
        AND (p_lonmax IS NULL OR t.lon<=p_lonmax)
        AND (p_lonmin IS NULL OR t.lon>=p_lonmin)
        AND (p_latmax IS NULL OR t.lat<=p_latmax)
        AND (p_latmin IS NULL OR t.lat>=p_latmin )
        AND (p_speedmax IS NULL OR t.speed<=p_speedmax)
        AND (p_speedmin IS NULL OR t.speed>=p_speedmin)
        AND (p_headingmin IS NULL OR t.heading>=p_headingmin)
        AND (p_headingmax IS NULL OR t.heading<=p_headingmax)
        AND exists(
                  SELECT
                  FROM (SELECT unnest(p_date_from) as t1, unnest(p_date_to) as t2) as tab
                  WHERE tgl.event_time>=t1 and tgl.event_time<=t2
                  )
        AND (cardinality(p_gr_num_list)=0 or ctr.garage_num=ANY(p_gr_num_list))
  ORDER BY gr_num,point_date;
END;
$$;


CREATE OR REPLACE FUNCTION gis.gis_export_pkg$get_ibrd_connection_list(
  p_ibrd_muid ibrd.info_board2gis.muid%type,
  p_result    OUT REFCURSOR
)
  RETURNS REFCURSOR
LANGUAGE plpgsql
AS $$
BEGIN
OPEN p_result FOR
  with ib_events as (
      select
        e.info_board_id,
        e.event_type_id,
        max(event_date) event_date
      from ibrd.event e
      where e.event_type_id in (0, 1)
        and (p_ibrd_muid is null or e.info_board_id in (select info_board2gis.info_board_id from ibrd.info_board2gis where muid = p_ibrd_muid))
      group by e.info_board_id, e.event_type_id
  )
  select
    ib2g.muid ibrd_muid,
    ib.code,
    coalesce((select 1 - e.event_type_id
              from ib_events e
              where e.info_board_id = ib.info_board_id
              order by e.event_date desc
              limit 1),
             0) ibrd_connection,
    (select event_date
     from ib_events e
     where e.info_board_id = ib.info_board_id and e.event_type_id = 0) date_connection
  from ibrd.info_board ib
    join ibrd.info_board2gis ib2g on ib.info_board_id = ib2g.info_board_id
  where p_ibrd_muid is null or ib2g.muid = p_ibrd_muid
  order by ibrd_connection desc,
           date_connection;
END;
$$;


create or replace function gis.gis_export_pkg$convert_calendar_tag_to_bit
  (v_tag_name_short in text)
returns bit(7)
-- ========================================================================
-- Перевод тегов календаря в битовую маску
-- ========================================================================
as $$ declare
   v_result    bit(7);
begin
   select case 
             when v_tag_name_short = 'Л' or v_tag_name_short = 'З' then B'1111111'
             when v_tag_name_short = 'Будни' then B'0011111'
             when v_tag_name_short = 'Выходные'then B'1100000'
             when v_tag_name_short = 'Пн' then B'0000001'
             when v_tag_name_short = 'Вт' then B'0000010'
             when v_tag_name_short = 'Ср' then B'0000100'
             when v_tag_name_short = 'Чт' then B'0001000'
             when v_tag_name_short = 'Пт' then B'0010000'
             when v_tag_name_short = 'Сб' then B'0100000'
             when v_tag_name_short = 'Вс' then B'1000000'
             else B'0000000'
          end
     into v_result;
   return v_result;
end;
$$ language plpgsql immutable;


create or replace function gis.gis_export_pkg$convert_calendar_tag_to_bit
  (v_begin_date     in date,
   v_current_time   in timestamp)
returns integer
-- ========================================================================
-- Вспомогательная функция группировки по времени
-- ========================================================================
as $$ declare
   v_result    integer := 0;
   v_time      time;
begin
   v_time := (v_current_time - v_begin_date)::time;
   if v_time >= '00:00'::time and v_time < '00:30'::time then
      v_result := 1;


   end if;
   return v_result;
end;
$$ language plpgsql immutable;


create or replace function gis.gis_export_pkg$convert_calendar_to_bit
  (v_tt_variant_id   in integer)
returns integer
-- ========================================================================
-- Перевод календаря равианта расписания в битовую маску
-- ========================================================================
as $$ declare
   v_result    integer;
begin
   select bit_and(week_mask)::int into v_result
     from (select bit_or(week_mask) as week_mask
             from (select gis.gis_export_pkg$convert_calendar_tag_to_bit(tag_name_short) as week_mask
                     from ttb.tt_calendar ttc
                     join ttb.calendar_tag ctg on ctg.calendar_tag_id = ttc.calendar_tag_id
                    where ctg.calendar_tag_group_id in (1, 7)
                      and ttc.tt_variant_id = v_tt_variant_id
                      and not ttc.is_include
                  ) days_f
           union all
           select bit_and(week_mask) as week_mask
             from (select gis.gis_export_pkg$convert_calendar_tag_to_bit(tag_name_short) as week_mask
                     from ttb.tt_calendar ttc
                     join ttb.calendar_tag ctg on ctg.calendar_tag_id = ttc.calendar_tag_id
                    where ctg.calendar_tag_group_id in (1, 7)
                      and ttc.tt_variant_id = v_tt_variant_id
                      and ttc.is_include
                  ) days_t
          ) q;
   
   return coalesce(v_result, 0);
end;
$$ language plpgsql;


create or replace function gis.gis_export_pkg$get_schedules
   (p_route_variant_muid   in bigint,
    p_tt_variant_list      in text,
    p_result               out refcursor)
returns refcursor
-- ========================================================================
-- Данные о вариантах расписания
-- ========================================================================
as $$ declare
   v_tt_variant_arr        bigint[];
begin
   if p_tt_variant_list is not null then
      v_tt_variant_arr := jofl.jofl_pkg$extract_narray(p_tt_variant_list, 'tt_variant', false);
   end if;
   
   open p_result for
   with tt_variant_round as (
      select distinct tto.tt_variant_id, rnd.round_id, rvg.route_variant_muid
        from ttb.tt_variant ttv
        join ttb.tt_out tto on tto.tt_variant_id = ttv.tt_variant_id
        join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
        join ttb.tt_action_round ttr on ttr.tt_action_id = tta.tt_action_id
        join rts.round rnd on rnd.round_id = ttr.round_id
        join rts.route_variant2gis rvg on rvg.route_variant_id = ttv.route_variant_id
       where rvg.route_variant_muid = p_route_variant_muid
         and ttv.parent_tt_variant_id is null
         and not ttv.sign_deleted
         and not tto.sign_deleted
         and not tta.sign_deleted
   )
   select q.*,
          md5(q::text) as checksum
     from (select ttv.route_variant_id, tvr.route_variant_muid,
                  rtv.route_id, rtg.route_muid,
                  ttv.tt_variant_id,
                  lower(ttv.action_period) action_begin,
                  upper(ttv.action_period) action_end,
                  ttv.is_calc_indicator as is_main,
                  ttv.tt_status_id,
                  (ttv.tt_status_id = ttb.tt_status_pkg$active()) as is_active,
                  gis.gis_export_pkg$convert_calendar_to_bit(ttv.tt_variant_id) as week_days,
                  rnd.round_id,
                  rnd.code as round_code,
                  case 
                     when ttb.action_type_pkg$is_production_round(rnd.action_type_id) then 'P'
                     when ttb.action_type_pkg$is_technical_round(rnd.action_type_id)  then 'T'
                     else ''
                  end as round_type
             from tt_variant_round tvr
             join rts.round rnd on rnd.round_id = tvr.round_id
             join ttb.tt_variant ttv on ttv.tt_variant_id = tvr.tt_variant_id
             join rts.route_variant rtv on rtv.route_variant_id = ttv.route_variant_id
             join rts.route2gis  rtg on rtg.route_id = rtv.route_id
            where ttv.tt_status_id in (ttb.tt_status_pkg$active(), ttb.tt_status_pkg$arch())
              and (p_tt_variant_list is null or ttv.tt_variant_id = any (v_tt_variant_arr))
          ) q
    order by route_id, route_variant_id, tt_variant_id, round_code;
end;    
$$ language plpgsql;


create or replace function gis.gis_export_pkg$get_outs
   (p_tt_variant_id        in bigint,
    p_result               out refcursor)
returns refcursor
-- ========================================================================
-- Данные о выходах в привязке к варианту расписания
-- ========================================================================
as $$ declare
begin
   open p_result for
   select tot.tt_out_num,
          (select count(*) from ttb.dr_shift sh where sh.mode_id = tot.mode_id) as sm_count,
          cap.short_name as capacity_short_name
     from ttb.tt_out tot
     join core.tr_capacity cap on cap.tr_capacity_id = tot.tr_capacity_id
    where not tot.sign_deleted

      and tot.tt_variant_id = p_tt_variant_id
    order by tot.tt_out_num;
end;
$$ language plpgsql;


create or replace function gis.gis_export_pkg$get_release
   (p_tt_variant_id        in bigint,
    p_result               out refcursor)
returns refcursor
-- ========================================================================
-- Данные о выпуске в привязке к варианту расписания
-- ========================================================================
as $$ declare
begin
   open p_result for
   select cap.short_name as capacity_short_name,
          q.tr_capacity_id, q.tr_capacity_count,
          sum(q.tr_capacity_count) over () as all_tr_capacity_count
     from (select tr_capacity_id, count(*) as tr_capacity_count
             from ttb.tt_out
            where tt_variant_id = p_tt_variant_id
            group by tr_capacity_id
          ) q
     join core.tr_capacity cap on cap.tr_capacity_id = q.tr_capacity_id
    order by 1;
end;
$$ language plpgsql;


create or replace function gis.gis_export_pkg$get_action_type
   (p_result               out refcursor)
returns refcursor
-- ========================================================================
-- Данные о выпуске в привязке к варианту расписания
-- ========================================================================
as $$ declare
begin
   open p_result for
   select action_type_id, parent_type_id, action_type_name
     from ttb.action_type
    order by parent_type_id, action_type_name;
end;
$$ language plpgsql;


create or replace function gis.gis_export_pkg$get_event_type
   (p_tt_variant_id        in bigint,
    p_result               out refcursor)
returns refcursor
-- ========================================================================
-- Данные о событиях в привязке к варианту расписания
-- ========================================================================
as $$ declare
begin
   open p_result for
   select tot.tt_out_id, tot.tt_out_num,
          row_number() over (partition by tot.tt_out_id order by act.dt_action) as event_num,
          act.dt_action,
          coalesce(rtj.route_round_muid, rtj.route_null_round_muid) as round_muid,
          rnd.round_id,
          rnd.code as round_code,
          case 
             when ttb.action_type_pkg$is_production_round(rnd.action_type_id) then 'P'
             when ttb.action_type_pkg$is_technical_round(rnd.action_type_id)  then 'T'
             else ''
          end as round_type,
          rtg.route_trajectory_muid,
          rtj.trajectory_type_muid,
          act.action_type_id,
          (select min(t.time_begin) from ttb.tt_action_item t where not t.sign_deleted and t.tt_action_id = act.tt_action_id) as time_begin,
          (select max(t.time_end)   from ttb.tt_action_item t where not t.sign_deleted and t.tt_action_id = act.tt_action_id) as time_end
     from ttb.tt_out tot
     join ttb.tt_action act on act.tt_out_id = tot.tt_out_id
     left join ttb.tt_action_round tar on tar.tt_action_id = act.tt_action_id
     left join rts.round rnd on rnd.round_id = tar.round_id
     left join rts.round2gis rtg on rtg.round_id = tar.round_id
     left join gis.route_trajectories rtj on rtj.muid = rtg.route_trajectory_muid
    where not tot.sign_deleted
      and not act.sign_deleted
      and tot.tt_variant_id = p_tt_variant_id
    order by 1, 2;
end;
$$ language plpgsql;


create or replace function gis.gis_export_pkg$get_stop_times
   (p_tt_variant_id        in bigint,
    p_stop_place_list      in bigint[],
    p_result               out refcursor)
returns refcursor
-- ========================================================================
-- Данные о временах отправлений (расписание)
-- ========================================================================
as $$ declare
begin
   open p_result for
   with round_num_list as (
      select tta.tt_action_id,
             row_number() over (partition by tta.tt_out_id order by tta.tt_action_id) as round_num
        from ttb.tt_out tot
        join ttb.tt_action tta on tta.tt_out_id = tot.tt_out_id
        join ttb.tt_action_round ttr on ttr.tt_action_id = tta.tt_action_id
       where not tta.sign_deleted
         and tot.tt_variant_id = p_tt_variant_id
   )
   select tot.tt_variant_id, sig.stop_place_muid,
          (select string_agg(qsit.stop_type_id::text, ',')
             from rts.stop_item2round_type qsir
             join rts.stop_item_type qsit on qsit.stop_item_type_id = qsir.stop_item_type_id
            where not qsir.sign_deleted
              and not qsit.sign_deleted
              and qsir.stop_item2round_id = aci.stop_item2round_id) as stop_type,
          coalesce(rtj.route_round_muid, rtj.route_null_round_muid) as route_muid,
          case 
             when ttb.action_type_pkg$is_production_round(rnd.action_type_id) then 'P'
             when ttb.action_type_pkg$is_technical_round(rnd.action_type_id)  then 'T'
            else 'U'
          end as round_type,
          rnd.code as round_code,
          rdg.route_trajectory_muid,
          rtj.trajectory_type_muid,
          tot.tt_out_num,
          shi.dr_shift_num,
          rnl.round_num as out_order_num,
          sir.order_num,
          sir.order_num as op_entry_number,
          aci.time_begin, 
          aci.time_end,
          extract(epoch from aci.time_begin - '1900-01-01 00:00:00'::timestamp) / 60 as time_begin_min,
          extract(epoch from aci.time_end   - '1900-01-01 00:00:00'::timestamp) / 60 as time_end_min
     from ttb.tt_out tot
     join ttb.tt_action act on act.tt_out_id = tot.tt_out_id
     join round_num_list rnl on rnl.tt_action_id = act.tt_action_id
     join ttb.tt_action_item aci on aci.tt_action_id = act.tt_action_id
     join rts.stop_item2round sir on sir.stop_item2round_id = aci.stop_item2round_id
     join rts.stop_item2gis sig on sig.stop_item_id = sir.stop_item_id
     join rts.round rnd on rnd.round_id = sir.round_id
     join rts.round2gis rdg on rdg.round_id = sir.round_id
     join gis.route_trajectories rtj on rtj.muid = rdg.route_trajectory_muid
     left join ttb.dr_shift shi on shi.dr_shift_id = aci.dr_shift_id
    where not tot.sign_deleted
      and not act.sign_deleted
      and not aci.sign_deleted
      and tot.parent_tt_out_id is null
      and tot.tt_variant_id = p_tt_variant_id
      and (p_stop_place_list is null or cardinality(p_stop_place_list) = 0 or sig.stop_place_muid = any(p_stop_place_list))
    order by tot.tt_out_num, rnl.round_num, sir.order_num;
end;
$$ language plpgsql;


create or replace function gis.gis_export_pkg$get_avg_round_time
  (p_tt_variant_id   in integer,
   p_period_begin    in timestamp,
   p_period_end      in timestamp,
   p_period_begin2   in timestamp,
   p_period_end2     in timestamp)
returns table (tt_variant_id int, route_variant_id int, 
               period_begin timestamp, period_end timestamp, 
               period_begin2 timestamp, period_end2 timestamp, 
               avg_duration int)
-- ========================================================================
-- Среднее времени выполнения кругорейса на интервале
-- ========================================================================
as $$ declare
   v_result           integer;
   v_norm_id          integer;
   v_route_variant_id integer;
   v_period_begin     timestamp := p_period_begin;
   v_period_end       timestamp := p_period_end;
   v_period_begin2    timestamp := p_period_begin2;
   v_period_end2      timestamp := p_period_end2;
   v_time_end         time := p_period_end::time;
begin
   
   select q.norm_id, q.route_variant_id
     into v_norm_id, v_route_variant_id
     from ttb.tt_variant q
    where q.tt_variant_id = p_tt_variant_id;
   
--   raise notice 'p_period_begin = %', p_period_begin;
--   raise notice 'p_period_end = %', p_period_end;
--   raise notice 'v_norm_id = %', v_norm_id;
--   raise notice 'v_route_variant_id = %', v_route_variant_id;
   
   -- Случай с периодом - время работы расписания
   if p_period_begin = p_period_end then
      v_period_begin2  := null;
      v_period_end2    := null;
      
      select date_trunc('hour', min(tti.time_begin) + interval '55 minute') as period_begin,
             date_trunc('hour', max(tti.time_end)   + interval '55 minute') as period_end
        into v_period_begin, v_period_end
        from ttb.tt_out tto
        join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
        join ttb.tt_action_round ttr on ttr.tt_action_id = tta.tt_action_id
        join ttb.tt_action_item tti on tti.tt_action_id = tta.tt_action_id
        join rts.round rnd on rnd.round_id = ttr.round_id
       where tto.tt_variant_id = p_tt_variant_id
         and not tto.sign_deleted
         and not tta.sign_deleted
         and not tti.sign_deleted
         and rnd.code = '00'
         and ttb.action_type_pkg$is_production_round(rnd.action_type_id);
      
--      raise notice 'rote case';
--      raise notice 'v_period_begin = %', v_period_begin;
--      raise notice 'v_period_end = %', v_period_end;
   end if;
   
   if date_part('day', v_period_begin) <> date_part('day', v_period_end) then
      v_period_begin2 := '1900-01-02 00:00:00'::timestamp;
      v_period_end2   := v_period_end;
      v_period_end    := '1900-01-02 00:00:00'::timestamp;
      v_time_end      := '24:00'::time;
   end if;
   
--   raise notice 'v_period_begin = %', v_period_begin;
--   raise notice 'v_period_end = %', v_period_end;
--   raise notice 'v_period_begin2 = %', v_period_begin2;
--   raise notice 'v_period_end2 = %', v_period_end2;
   
   return query
   with recursive interval_base as (
      select '00:00'::time as hour_from,
             '01:00'::time as hour_to
      union all
      select hour_from + interval '1 hour' as hour_from,
             case when hour_to = '23:00'::time then '24:00'::time else hour_to + interval '1 hour' end as hour_to
        from interval_base
       where hour_from < '23:00'::time
   ),
   interval_list as (
      select hour_from, hour_to
        from interval_base
   ),
   filtered_interval_list as (
      select hour_from, hour_to
        from interval_list
       where ((hour_from >= v_period_begin::time and hour_from < v_time_end) 
               or
              (v_period_begin2 is not null and v_period_end2 is not null and
               (hour_from >= v_period_begin2::time and hour_from < v_period_end2::time))
             )
   )
   select p_tt_variant_id    as tt_variant_id,
          v_route_variant_id as route_variant_id,
          v_period_begin     as period_begin,
          v_period_end       as period_end,
          v_period_begin2    as period_begin2,
          v_period_end2      as period_end2,
          avg((ttb.over_round_duration(v_norm_id, v_route_variant_id, hour_from, null)).p_duration)::int as avg_duration
     from filtered_interval_list;
end;
$$ language plpgsql;


create or replace function gis.gis_export_pkg$get_max_tr_count
  (p_tt_variant_id   in integer,
   p_period_begin    in timestamp,
   p_period_end      in timestamp,
   p_period_begin2   in timestamp,
   p_period_end2     in timestamp)
returns integer
-- ========================================================================
-- Максимальное количество ТС, работающих на линии
-- ========================================================================
as $$ declare
   v_result           integer;
begin
   select count(distinct tto.tt_out_id) as tr_count
     into v_result
     from ttb.tt_out tto
     join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
     join ttb.tt_action_round ttr on ttr.tt_action_id = tta.tt_action_id
     join ttb.tt_action_item tti on tti.tt_action_id = tta.tt_action_id
     join rts.round rnd on rnd.round_id = ttr.round_id
    where tto.tt_variant_id = p_tt_variant_id
      and not tto.sign_deleted
      and not tta.sign_deleted
      and not tti.sign_deleted
      and rnd.code = '00'
      and ttb.action_type_pkg$is_production_round(rnd.action_type_id)
      and ((tti.time_begin >= p_period_begin and tti.time_begin < p_period_end)
            or
           (p_period_begin2 is not null and p_period_end2 is not null and
           (tti.time_begin >= p_period_begin2 and tti.time_begin < p_period_end2))
          );
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_export_pkg$get_oper_speed
  (p_tt_variant_id   in integer,
   p_period_begin    in timestamp,
   p_period_end      in timestamp,
   p_period_begin2   in timestamp,
   p_period_end2     in timestamp)
returns numeric
-- ========================================================================
-- Эксплуатационная скорость
-- ========================================================================
as $$ declare
   v_result          float8;
begin
   select case when r.route_sec = 0 then 0::numeric else r.route_len::numeric / r.route_sec * 3.6 end
     into v_result
     from (select sum(extract(epoch from s.route_end - s.route_begin)) as route_sec,
                  sum(s.route_len) route_len
             from (select q.tt_action_id, min(q.time_begin) as route_begin, max(q.time_end) as route_end,
                          sum(case when row_num = 1 then 0 else q.length_sector end) route_len
                     from (select tti.tt_action_id, sir.length_sector, tti.time_begin, tti.time_end,
                                  row_number() over (partition by tti.tt_action_id order by tti.time_begin) as row_num
                             from ttb.tt_out tto
                             join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
                             join ttb.tt_action_round ttr on ttr.tt_action_id = tta.tt_action_id
                             join ttb.tt_action_item tti on tti.tt_action_id = tta.tt_action_id
                             join rts.stop_item2round sir on sir.stop_item2round_id = tti.stop_item2round_id
                            where tto.tt_variant_id = p_tt_variant_id
                              and tti.time_begin >= p_period_begin
                              and tti.time_begin <  p_period_end
                           union all 
                           select tti.tt_action_id, sir.length_sector, tti.time_begin, tti.time_end,
                                  row_number() over (partition by tti.tt_action_id order by tti.time_begin) as row_num
                             from ttb.tt_out tto
                             join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
                             join ttb.tt_action_round ttr on ttr.tt_action_id = tta.tt_action_id
                             join ttb.tt_action_item tti on tti.tt_action_id = tta.tt_action_id
                             join rts.stop_item2round sir on sir.stop_item2round_id = tti.stop_item2round_id
                            where tto.tt_variant_id = p_tt_variant_id
                              and tti.time_begin >= p_period_begin
                              and tti.time_begin <  p_period_end
                          ) q
                    group by q.tt_action_id
                  ) s
            where s.route_len > 0
          ) r;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function gis.gis_export_pkg$get_exploitation_by_periods
   (p_tt_variant_id   in  integer,
    p_result          out refcursor)
returns refcursor
-- ========================================================================
-- Данные об эксплуатационных показателях в привязке к варианту расписания 
-- с разбивкой по временным промежуткам
-- ========================================================================
as $$ declare
begin
   open p_result for
   select p_tt_variant_id as tt_variant_id,
          r.export_period_id as time_code,
          r.export_period_name as time_name,
          case when r.avg_duration_min <> 0 then round(60::numeric / r.avg_duration_min, 1) else 0::numeric end as frequency,
          case when r.max_tr_count <> 0 then r.avg_duration_min / r.max_tr_count else 0::integer end as interval,
          r.avg_duration_min as round_time,
          round(r.oper_speed, 1) as speed
     from (select q.export_period_id, q.export_period_name,
                  gis.gis_export_pkg$get_max_tr_count(p_tt_variant_id,
                                                     (q.data).period_begin,
                                                     (q.data).period_end,
                                                     (q.data).period_begin2,
                                                     (q.data).period_end2) as max_tr_count,
                  (q.data).avg_duration as avg_duration_min,
                  gis.gis_export_pkg$get_oper_speed(p_tt_variant_id,
                                                     (q.data).period_begin,
                                                     (q.data).period_end,
                                                     (q.data).period_begin2,
                                                     (q.data).period_end2) as oper_speed
             from (select export_period_id, export_period_name,
                          gis.gis_export_pkg$get_avg_round_time(p_tt_variant_id, 
                                                                export_period_begin, 
                                                                export_period_end, 
                                                                export_period_begin2, 
                                                                export_period_end2) as data
                     from gis.operating_export_period
                  ) q
          ) r
    order by r.export_period_id;
end;
$$ language plpgsql;


create or replace function gis.gis_export_pkg$get_exploitation
   (p_tt_variant_id   in  integer,
    p_period_begin    in  timestamp,
    p_period_end      in  timestamp,
    p_result          out refcursor)
returns refcursor
-- ========================================================================
-- Данные об эксплуатационных показателях в привязке к варианту расписания 
-- ========================================================================
as $$ declare
   p_period_begin2   timestamp := null;
   p_period_end2     timestamp := null;
begin
   open p_result for
   select p_tt_variant_id as tt_variant_id,
          p_period_begin as begin_time,
          p_period_end as end_time,
          case when r.avg_duration_min <> 0 then round(60::numeric / r.avg_duration_min, 1) else 0::numeric end as frequency,
          case when r.max_tr_count <> 0 then r.avg_duration_min / r.max_tr_count else 0::integer end as interval,
          r.avg_duration_min as round_time,
          round(r.oper_speed, 1) as speed
     from (select gis.gis_export_pkg$get_max_tr_count(p_tt_variant_id,
                                                     (q.data).period_begin,
                                                     (q.data).period_end,
                                                     (q.data).period_begin2,
                                                     (q.data).period_end2) as max_tr_count,
                  (q.data).avg_duration as avg_duration_min,
                  gis.gis_export_pkg$get_oper_speed(p_tt_variant_id,
                                                     (q.data).period_begin,
                                                     (q.data).period_end,
                                                     (q.data).period_begin2,
                                                     (q.data).period_end2) as oper_speed
             from (select gis.gis_export_pkg$get_avg_round_time(p_tt_variant_id, 
                                                                p_period_begin, 
                                                                p_period_end, 
                                                                p_period_begin2, 
                                                                p_period_end2) as data
                  ) q
          ) r;
end;
$$ language plpgsql;


create or replace function gis.gis_export_pkg$get_sensor_termo
   (p_period_begin    in  timestamp,
    p_result          out refcursor)
returns refcursor
-- ========================================================================
-- Показания температурных датчиков
-- f========================================================================
as $$ declare
   p_period_begin2   timestamp := null;
   p_period_end2     timestamp := null;
begin
   open p_result for
   select ttr.tn_id as vehicle_id,
          tr.licence as gos_nomer,
          ttp.pk_regnum as depot_id,
          tr.garage_num as depot_number,
          dt.status as sensor_status,
          dt.val as sensor_termo,
          to_char(dt.event_time, 'yyyy-mm-dd hh24:mi:ss') as datetime
     from snsr.termo_data dt
     join core.tr on tr.tr_id = dt.tr_id
     join core.depo dp on dp.depo_id = tr.depo_id
     join tt.park ttp on ttp.tn_instance = dp.tn_instance
     join tt.tr ttr on ttr.park_id = ttp.park_id
                   and ttr.garage_num::int = tr.garage_num
    where event_time between p_period_begin::date::timestamp and p_period_begin::date::timestamp + interval '1 day';
end;
$$ language plpgsql;
