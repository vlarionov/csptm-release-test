﻿create or replace function gis.jf_route_null_rounds_pkg$of_rows
  (p_id_account   in  numeric,
   p_attr         in  text,
   p_rows         out refcursor)
returns refcursor as $body$
declare 
   ln_rv_muid        bigint := jofl.jofl_pkg$extract_number(p_attr, 'MUID', TRUE)::bigint;
   lt_tr_type        gis.ref_transport_kinds.short_name%type := jofl.jofl_pkg$extract_varchar(p_attr, 'TR_TYPE', true); 
   lt_route_number   gis.routes.number%type := jofl.jofl_pkg$extract_varchar(p_attr, 'ROUTE_NUMBER', true); 
begin
   open p_rows for
   with recursive round_type as (
      select action_type_id, action_type_name, action_type_code, parent_type_id, action_type_desc
        from ttb.action_type
       where action_type_code = 'Тх'
      union
      select e.action_type_id, e.action_type_name, e.action_type_code, e.parent_type_id, e.action_type_desc
        from ttb.action_type e
        inner join round_type s on s.action_type_id = e.parent_type_id
   ),
   stopitem2round as (
      select distinct round_id,
             first_value(stop_item_id) over (partition by round_id order by order_num rows between unbounded preceding and unbounded following) as stop_item_a_id,
             last_value (stop_item_id) over (partition by round_id order by order_num rows between unbounded preceding and unbounded following) as stop_item_b_id
        from rts.stop_item2round
       where not sign_deleted
   )
   select rnd.round_id         as "ROUND_ID",
          rnd.code             as "CODE",
          rnt.action_type_name as "TYPE_NAME",
          spa.name             as "POINT1",
          case 
             when substring(rnt.action_type_code from 1 for 1) = 'П' then 'Парк' 
             when substring(rnt.action_type_code from 1 for 1) = 'А' then 'Остановка'
             when substring(rnt.action_type_code from 1 for 1) = 'О' then 'Обед'
             when substring(rnt.action_type_code from 1 for 1) = 'Р' then 'Ремонт'
             when substring(rnt.action_type_code from 1 for 1) = 'З' then 'АЗС'
             when substring(rnt.action_type_code from 1 for 1) = 'Т' then 'Отстой'
             else ''
          end                  as "POINT1_TYPE",
          spb.name             as "POINT2",
          case 
             when substring(rnt.action_type_code from 2 for 1) = 'П' then 'Парк' 
             when substring(rnt.action_type_code from 2 for 1) = 'А' then 'Остановка'
             when substring(rnt.action_type_code from 2 for 1) = 'О' then 'Обед'
             when substring(rnt.action_type_code from 2 for 1) = 'Р' then 'Ремонт'
             when substring(rnt.action_type_code from 2 for 1) = 'З' then 'АЗС'
             when substring(rnt.action_type_code from 2 for 1) = 'Т' then 'Отстой'
             else ''
          end                  as "POINT2_TYPE",
          ''                   as "POINT3",
          ''                   as "POINT3_TYPE",
          ''                   as "COMMENT",
          ''                   as "MUID",
          '[' || json_build_object('CAPTION', 'На карте',
                                   'LINK', json_build_object('URL', '/app.html?m=modules/RouteOnMapCtrl&attr={"tr_type":"' ||typ.short_name|| '","route_number":"' ||rut.route_num|| '","rnr_code":"' ||rnd.code|| '","rv_muid":"'||rtv.route_variant_id||'"}'
                                  ))::text || ']' as "JUMP_TO_RNR"
     from rts.round rnd
     join round_type rnt on rnt.action_type_id = rnd.action_type_id
     join rts.route_variant rtv on rtv.route_variant_id = rnd.route_variant_id
     join rts.route rut on rut.route_id = rtv.route_id
     join core.tr_type typ on typ.tr_type_id = rut.tr_type_id
     join rts.route_variant2gis rv2g on rv2g.route_variant_id = rnd.route_variant_id
     left join stopitem2round sir on sir.round_id = rnd.round_id
     left join rts.stop_item sia on sia.stop_item_id = sir.stop_item_a_id
     left join rts.stop_location sla on sla.stop_location_id = sia.stop_location_id
     left join rts.stop spa on spa.stop_id = sla.stop_id
     left join rts.stop_item sib on sib.stop_item_id = sir.stop_item_b_id
     left join rts.stop_location slb on slb.stop_location_id = sib.stop_location_id
     left join rts.stop spb on spb.stop_id = slb.stop_id
    where not rnd.sign_deleted
      and not rtv.sign_deleted
      and not rut.sign_deleted
      and rv2g.route_variant_muid = ln_rv_muid
    order by rnd.code;
end; 
$body$ language plpgsql volatile;

alter function gis.jf_route_null_rounds_pkg$of_rows(numeric, text)
  owner to adv;
