CREATE OR REPLACE FUNCTION gis.jf_transport_parkings_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
begin 
 open p_rows for 
      select 
        tp.MUID			AS "MUID", 
        tp.PARK_MUID		AS "PARK_MUID", 
        tp.WKT_GEOM		AS "WKT_GEOM", 
        tp.GEOM			AS "GEOM", 
        tp.NAME			AS "NAME", 
        tp.ADDRESS			AS "ADDRESS", 
        tp.PHONE			AS "PHONE", 
        --tp.GRAPH_SECTION_MUID	AS "GRAPH_SECTION_MUID", 
        --tp.GRAPH_SECTION_OFFSET	AS "GRAPH_SECTION_OFFSET", 
        tp.SQUARE			AS "SQUARE", 
        tp.CAPACITY		AS "CAPACITY", 
        tp.HAS_BUS			AS "HAS_BUS", 
        tp.HAS_TROLLEY		AS "HAS_TROLLEY", 
        tp.HAS_TRAM		AS "HAS_TRAM", 
        tp.HAS_SPEEDTRAM		AS "HAS_SPEEDTRAM", 
        tp.VERSION			AS "VERSION", 
        tp.SIGN_DELETED		AS "SIGN_DELETED", 
        tp.INSERT_DATE		AS "INSERT_DATE", 
        tp.UPDATE_DATE		AS "UPDATE_DATE",
        p.name			AS "P_NAME",
        ''			AS "T_NAME"
      from gis.transport_parkings tp, gis.parks p
      where tp.park_muid = p.muid; 
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;