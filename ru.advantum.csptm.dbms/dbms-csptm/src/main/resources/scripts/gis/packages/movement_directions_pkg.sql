﻿-- чётная
create or replace function gis.movement_directions_pkg$even()
  returns bigint as 'select 1::bigint' language sql immutable;
-- нечётная
create or replace function gis.movement_directions_pkg$odd()
  returns bigint as 'select 2::bigint' language sql immutable;
-- в обе стороны
create or replace function gis.movement_directions_pkg$both()
  returns bigint as 'select 3::bigint' language sql immutable;


create or replace function gis.movement_directions_pkg$md_to_rts
   (p_movement_direction_muid   in bigint)
returns smallint
-- ========================================================================
-- Направления движения ГИС -> RTS
-- ========================================================================
as $$ declare
    v_result   smallint;
begin
   if p_movement_direction_muid = gis.movement_directions_pkg$even() then
      v_result := rts.stop_group_pkg$md_even();
   elsif p_movement_direction_muid = gis.movement_directions_pkg$odd() then
      v_result := rts.stop_group_pkg$md_odd();
   elsif p_movement_direction_muid = gis.movement_directions_pkg$both() then
      v_result := rts.stop_group_pkg$md_both();
   end if;
   return v_result;
end;
$$ language plpgsql immutable;
