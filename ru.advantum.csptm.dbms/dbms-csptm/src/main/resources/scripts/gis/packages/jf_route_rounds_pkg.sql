﻿-- Function: gis."jf_route_rounds_pkg$of_rows"(numeric, text)

-- DROP FUNCTION gis."jf_route_rounds_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION gis."jf_route_rounds_pkg$of_rows"
   (IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
RETURNS refcursor
AS $BODY$ declare
    ln_rv_muid numeric := jofl.jofl_pkg$extract_number(p_attr, 'MUID', TRUE);
    lt_tr_type 		gis.ref_transport_kinds.short_name%type := jofl.jofl_pkg$extract_varchar(p_attr, 'TR_TYPE', true);
    lt_route_number 	gis.routes.number%type := jofl.jofl_pkg$extract_varchar(p_attr, 'ROUTE_NUMBER', true);
begin 
    open p_rows for
    select distinct
           rr.muid::text       as "MUID",
           rrt.name            as "RRT_NAME",
           rr.code             as "CODE",
    	     st_a.name           as "START_POINT",
           st_b.name           as "END_POINT",
           case
              when (select count(*) from gis.route_trajectories where route_round_muid = rr.muid ) > 0 then
                 '[' || json_build_object('CAPTION', 'На карте',
                                          'LINK', json_build_object('URL', '/app.html?m=modules/RouteOnMapCtrl&attr={"tr_type":"' || tt.short_name || '","route_number":"' ||
                                                                    r.number || '","rr_code":"' || rr.CODE || '","rv_muid":"' || g.route_variant_id || '"}'
                                          ))::text || ']' ELSE  '' 
           end as "JUMP_TO_RR"
      from gis.route_rounds rr
      join gis.ref_route_round_types rrt on rrt.muid = rr.route_round_type_muid
      join gis.route_variants rv on rv.muid = rr.route_variant_muid
      join rts.route_variant2gis g on rv.muid = g.route_variant_muid
      join gis.routes r on r.muid = rv.route_muid
      join gis.stop_places sp_a on rr.stop_place_a_muid = sp_a.muid
      join gis.stop_places sp_b on rr.stop_place_b_muid = sp_b.muid
      join gis.stops st_a on sp_a.stop_muid = st_a.muid
      join gis.stops st_b on sp_b.stop_muid = st_b.muid
      join core.tr_type tt on tt.transport_kind_muid = r.transport_kind_muid
     where rv.muid::numeric = ln_rv_muid
       and rr.sign_deleted = 0
       and rv.sign_deleted = 0
       and r.sign_deleted = 0;
end;
$BODY$ LANGUAGE plpgsql VOLATILE COST 100;

ALTER FUNCTION gis."jf_route_rounds_pkg$of_rows"(numeric, text) OWNER TO adv;
