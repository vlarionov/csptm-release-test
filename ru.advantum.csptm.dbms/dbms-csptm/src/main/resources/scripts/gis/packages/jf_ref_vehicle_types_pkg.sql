CREATE OR REPLACE FUNCTION gis."jf_ref_vehicle_types_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare
  l_rf_db_method TEXT;
  l_calculation_task_id bigint;
begin

  l_rf_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
  l_calculation_task_id=jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true);

  open p_rows for
      select
        name,
        short_name,
        route_transport_kind_muid,
        model,
        seating_capacity,
        max_capacity,
        effective_area,
        full_load_weight,
        dimensions,
        muid,
        has_facilities_for_disabled,
        version,
        sign_deleted,
        insert_date,
        update_date
      from gis.ref_vehicle_types vt
      where   l_rf_db_method is null or
            (l_rf_db_method = 'asd.vehicle_type2ct' and
             vt.version>1 and
             not exists (select 1 from asd.vehicle_type2ct vh2ct
                         where vh2ct.vehicle_type_muid = vt.muid
                           and vh2ct.calculation_task_id = l_calculation_task_id))
      ;
end;
 $function$
;
