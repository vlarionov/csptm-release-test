﻿-- Function: gis."jf_routes_pkg$of_rows"(numeric, text)

-- DROP FUNCTION gis."jf_routes_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION gis."jf_routes_pkg$of_rows"(
IN p_id_account numeric,
OUT p_rows refcursor,
IN p_attr text)
  RETURNS refcursor AS
$BODY$
declare 

  F_RTK_NAME	gis.ref_transport_kinds.name%type := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_RTK_NAME', true), jofl.jofl_pkg$extract_varchar(p_attr, 'transport_kind_name', true),''); 
  F_PARKS_NAME	gis.parks.name%type 		  := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_PARKS_NAME', true), '');
  OPEN_DT  timestamp				  := jofl.jofl_pkg$extract_date(p_attr, 'OPEN_DT', true);
  CLOSE_DT timestamp				  := jofl.jofl_pkg$extract_date(p_attr, 'CLOSE_DT', true);
  F_NUMBER	gis.routes.number%type 		  := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_NUMBER_NUMBER', true), '');
  l_rf_db_method text := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);
  l_calculation_task_id bigint := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true);
  f_rtk_MUID numeric := -1; 
  f_prk_MUID  numeric := -1;
  f_depo_id BIGINT;
  R_MUID numeric := jofl.jofl_pkg$extract_number(p_attr, 'R_MUID', TRUE);
  l_filter_off boolean:= false;
  l_available_routes_muids BIGINT[];
begin
  f_depo_id :=jofl.jofl_pkg$extract_number(p_attr, 'F_depo_id', TRUE);

  select max(d.park_muid)
  into f_depo_id
  from core.depo d
  where d.depo_id = f_depo_id;

  f_rtk_MUID := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_rtk_MUID',  TRUE), jofl.jofl_pkg$extract_number(p_attr, 'F_tr_type_id',  TRUE), -1);
  f_prk_MUID := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_prk_MUID',  TRUE), f_depo_id,  -1);


  if (upper(l_rf_db_method) in (
    'ASD.RPT_SPEED_ON_ROUTE',
    'ADM.DATA_REALM',
    'ADM.DATA_REALM_ROUTES',
    'MNT.RPT_TR_ON_ROUTE_INTERVAL',
    'core.trs_on_map',
    'ibrd.forecast_full_report'
  )) or l_rf_db_method is null then
    l_filter_off = true;
  end if;
  l_available_routes_muids := array(select route_muid
                                    from rts.route2gis
                                    where route_id in (select adm.account_data_realm_pkg$get_routes(p_id_account)));


if f_rtk_MUID = -1 or f_prk_MUID = -1 then 
 open p_rows for 
      select distinct
        rtk.name		AS "RTK_NAME"
       --,p.name			AS "P_NAME"
       ,r.number		AS "NUMBER"
       ,r.open_date		AS "OPEN_DATE"
       ,r.close_date		AS "CLOSE_DATE"
       ,rtk.short_name		AS "RTK_SHORT_NAME"
	   ,r.MUID::text 			AS "MUID"
	   ,r.CURRENT_ROUTE_VARIANT_MUID::text AS "CURRENT_ROUTE_VARIANT_MUID"      
      from gis.routes r, gis.ref_transport_kinds rtk/*,
           gis.parks2routes p2r, gis.parks p*/
     where r.transport_kind_muid = rtk.muid
       /*and p2r.route_muid = r.muid 
       and p2r.park_muid = p.muid*/
       and (F_RTK_NAME = rtk.name or F_RTK_NAME = '')
       --and (F_PARKS_NAME = p.name or F_PARKS_NAME = '')
       and (exists(select 1 from gis.parks2routes p2r, gis.parks p where p2r.route_muid = r.muid and p2r.park_muid = p.muid and F_PARKS_NAME = p.name) or F_PARKS_NAME = '')
       and (OPEN_DT + interval '3 hour' = r.open_date or OPEN_DT is null)
       and (CLOSE_DT + interval '3 hour' = r.close_date or CLOSE_DT is null)
       and (F_NUMBER = r.number or F_NUMBER = '')
--        and (
--              (l_rf_db_method   in ('adm.data_realm_routes', 'adm.data_realm', 'asd.route2ct','asd.ct_speed_between_stops', 'asd.mnemonic_trs', 'mnt.jf_tt_deviation', 'asd.mnemonic_stop_times') and
--               not exists (select 1 from asd.route2ct r2ct
--               where r2ct.route_muid = r.muid
--                     and r2ct.calculation_task_id = l_calculation_task_id))*/
--            )
       and (r.MUID = R_MUID OR R_MUID IS NULL)
       and r.sign_deleted = 0
       and (l_filter_off or r.MUID = ANY(l_available_routes_muids))
       and r.agency_muid = gis.jf_agencies_pkg$cn_agency_mgt()
       ;
 else 
   open p_rows for       
     select distinct
        rtk.name		AS "RTK_NAME"
       --,p.name			AS "P_NAME"
       ,r.number		AS "NUMBER"
       ,r.open_date		AS "OPEN_DATE"
       ,r.close_date		AS "CLOSE_DATE"
       ,rtk.short_name		AS "RTK_SHORT_NAME"
	   ,r.MUID::text 			AS "MUID"
	   ,r.CURRENT_ROUTE_VARIANT_MUID::text AS "CURRENT_ROUTE_VARIANT_MUID"      
      from gis.routes r, gis.ref_transport_kinds rtk/*,
           gis.parks2routes p2r, gis.parks p*/
     where r.transport_kind_muid = rtk.muid
       /*and p2r.route_muid = r.muid 
       and p2r.park_muid = p.muid*/
       and rtk.muid = f_rtk_MUID 
       --and p.muid = f_prk_MUID 
       and exists(select 1 from gis.parks2routes p2r where p2r.route_muid = r.muid and p2r.park_muid = f_prk_MUID)
       and (r.MUID = R_MUID OR R_MUID IS NULL)
       and r.sign_deleted = 0
       and (l_filter_off or r.MUID = ANY(l_available_routes_muids))
       and r.agency_muid = gis.jf_agencies_pkg$cn_agency_mgt()
       ;
 end if;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION gis."jf_routes_pkg$of_rows"(numeric, text)
  OWNER TO adv;
