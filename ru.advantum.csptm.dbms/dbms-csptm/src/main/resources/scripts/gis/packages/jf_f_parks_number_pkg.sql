CREATE OR REPLACE FUNCTION gis."jf_f_parks_number_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
begin 
 open p_rows for 
      select distinct
        NUMBER AS "NUMBER"
      from gis.parks; 
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;