﻿-- Function: gis."jf_parks2routes_pkg$of_rows"(numeric, text)

-- DROP FUNCTION gis."jf_parks2routes_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION gis."jf_parks2routes_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
 ln_routes_muid gis.routes.muid%type := jofl.jofl_pkg$extract_number(p_attr, 'MUID', true); 

begin 
 open p_rows for 
      select distinct
	p.number	AS "NUMBER",
	p.name		AS "NAME",
	rtk.name	AS "RTK_NAME"
      from gis.routes r, gis.ref_transport_kinds rtk,
           gis.parks2routes p2r, gis.parks p
     where r.transport_kind_muid = rtk.muid
       and p2r.route_muid = r.muid 
       and p2r.park_muid = p.muid
       and r.muid = ln_routes_muid
       and r.agency_muid = gis.jf_agencies_pkg$cn_agency_mgt();

end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION gis."jf_parks2routes_pkg$of_rows"(numeric, text)
  OWNER TO adv;
