﻿-- Function: gis."jf_stops_pkg$of_rows"(numeric, text)

-- DROP FUNCTION gis."jf_stops_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION gis."jf_stops_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$
declare 
  F_NAME	gis.stops.name%type 	:= coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_NAME', true), ''); 
  F_DISTRICT	gis.stops.district%type := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_DISTRICT', true), ''); 
  F_REGION	gis.stops.region%type := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_REGION', true), ''); 
  F_STREET	gis.stops.street%type := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_STREET', true), ''); 
  F_HAS_BUS_INPLACE_V		gis.stop_places.has_bus%type		:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'F_HAS_BUS_INPLACE_V', true), -1); 
  F_HAS_TROLLEY_INPLACE_V	gis.stop_places.has_trolley%type	:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'F_HAS_TROLLEY_INPLACE_V', true), -1); 
  F_HAS_TRAM_INPLACE_V		gis.stop_places.has_tram%type		:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'F_HAS_TRAM_INPLACE_V', true), -1); 
  F_HAS_SPEEDTRAM_INPLACE_V	gis.stop_places.has_speedtram%type	:= coalesce(jofl.jofl_pkg$extract_number(p_attr, 'F_HAS_SPEEDTRAM_INPLACE_V', true), -1); 

  lt_muid gis.stops.muid%type := jofl.jofl_pkg$extract_varchar(p_attr, 'ST_MUID', true); 
begin 
 open p_rows for 
      select
          s.MUID::text			AS "MUID",
          s.MUID::text			AS "STOP_MUID",
          s.NAME			AS "NAME",
          s.NAME_IN_ENGLISH	AS "NAME_IN_ENGLISH",
          s.NAME_FOR_TERMINAL_POINT AS "NAME_FOR_TERMINAL_POINT",
          case when s.DIRECTION_MUID = 1
              then 'От центра'
          when s.DIRECTION_MUID = 2
              then 'К центру'
          when s.DIRECTION_MUID = 1
              then 'в обе стороны' end
              as "DIRECTION_MUID",
          s.VERSION		AS "VERSION",
          s.SIGN_DELETED		AS "SIGN_DELETED",
          s.INSERT_DATE		AS "INSERT_DATE",
          s.UPDATE_DATE		AS "UPDATE_DATE",
          COALESCE(s.DISTRICT, 'Нет данных')		AS "DISTRICT",
          COALESCE(s.REGION, 'Нет данных')		AS "REGION",
          s.STREET		AS "STREET",
          sp.building_address	AS "BUILDING_ADDRESS",
          sp.has_bus		AS "HAS_BUS",
          sp.has_trolley		AS "HAS_TROLLEY",
          sp.has_tram		AS "HAS_TRAM",
          sp.has_speedtram	AS "HAS_SPEEDTRAM",
	ST_AsText(sp.geom)	AS "POINT",
	'[' || json_build_object('CAPTION', 'На карте',
                                   'LINK', json_build_object('URL', '/app.html?m=modules/StopPlaceOnMapCtrl&attr={"wkt":"' || ST_AsText(sp.geom) ||
				'","sp_muid":"' || sp.muid || '"}'
                                   ))::text || ']' AS "JUMP_TO_SP",
        sp.muid::TEXT			AS "SP_MUID"
         
      from gis.stops s, gis.stop_places sp
      where s.muid = sp.stop_muid
	and (F_NAME = s.NAME or F_NAME = '')
	and (F_DISTRICT = COALESCE(s.DISTRICT, 'Нет данных') or F_DISTRICT = '')
	and (F_REGION = COALESCE(s.REGION, 'Нет данных') or F_REGION = '')
  	and (F_STREET = s.STREET or F_STREET = '')
  	and (F_HAS_BUS_INPLACE_V = sp.has_bus or F_HAS_BUS_INPLACE_V = -1)
  	and (F_HAS_TROLLEY_INPLACE_V = sp.has_trolley or F_HAS_TROLLEY_INPLACE_V = -1)
  	and (F_HAS_TRAM_INPLACE_V = sp.has_tram or F_HAS_TRAM_INPLACE_V = -1)	
  	and (F_HAS_SPEEDTRAM_INPLACE_V = sp.has_speedtram or F_HAS_SPEEDTRAM_INPLACE_V = -1)
  	and (s.muid = lt_muid or lt_muid is null) 
	; 
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION gis."jf_stops_pkg$of_rows"(numeric, text)
  OWNER TO adv;
