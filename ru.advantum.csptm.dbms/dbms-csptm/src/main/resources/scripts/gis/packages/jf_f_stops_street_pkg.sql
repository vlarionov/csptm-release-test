CREATE OR REPLACE FUNCTION gis."jf_f_stops_street_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
begin 
 open p_rows for 
      select distinct
        s.street AS "STREET"
      from gis.stops s
	order by 1
	; 
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;