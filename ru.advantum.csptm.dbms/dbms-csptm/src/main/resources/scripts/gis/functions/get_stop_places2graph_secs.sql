﻿create or replace function gis.get_stop_places2graph_secs(p_route_trajectory_muid in gis.route_trajectories.muid%type)
    returns table(stop_place_muid gis.stop_places.muid%type,
                  sp_order_num gis.stop_place2route_traj.order_num%type,
                  gs_order_num gis.graph_sec2route_traj.order_num%type)
as
$$
declare
    r record;
begin
    for r in (select sp.muid,
                     sp2rt.order_num sp_order_num,
                     array_agg(gs2rt.order_num) gs_order_nums
              from gis.route_trajectories rt
                   join gis.stop_place2route_traj sp2rt on rt.muid = sp2rt.route_trajectory_muid
                   join gis.stop_places sp on sp2rt.stop_place_muid = sp.muid
                   join gis.graph_sec2route_traj gs2rt on rt.muid = gs2rt.route_trajectory_muid and gs2rt.graph_section_muid =
                     sp.graph_section_muid
              where rt.muid = p_route_trajectory_muid
              group by sp.muid,
                       sp2rt.order_num,
                       sp2rt.length_sector,
                       sp.graph_section_muid
              order by sp_order_num) loop

        stop_place_muid := r.muid;
        sp_order_num := r.sp_order_num;
        if array_length(r.gs_order_nums, 1) = 1 or gs_order_num is null then
            gs_order_num := r.gs_order_nums[1];
        else
            gs_order_num := (select min(n) from unnest(r.gs_order_nums) n where n >= gs_order_num);
        end if;

        return next;
    end loop;
end;
$$ language plpgsql;