
--drop function adt.csv_loader_pkg$set_comment(text, text, text)

CREATE OR REPLACE FUNCTION adt.csv_loader_pkg$set_comment(
    p_table   TEXT,
    p_column  TEXT,
    p_comment TEXT)
    RETURNS VOID
LANGUAGE 'plpgsql'
AS $BODY$
BEGIN
    IF (p_comment IS NOT NULL)
    THEN
        EXECUTE format('COMMENT ON COLUMN %s.%s is %L', p_table, p_column, p_comment);
    END IF;
END;
$BODY$;


CREATE OR REPLACE FUNCTION adt.csv_loader_pkg$get_comment_column_by_table(
    p_table TEXT)
    RETURNS TABLE(tname TEXT, tcomment TEXT)
LANGUAGE 'plpgsql'
AS $BODY$

BEGIN
    RETURN QUERY
    SELECT
        cols.column_name :: TEXT AS tname,
        (
            SELECT pg_catalog.col_description(c.oid, cols.ordinal_position :: INT)
            FROM pg_catalog.pg_class c
            WHERE
                c.oid = (SELECT cols.table_name :: REGCLASS :: OID) AND
                c.relname = cols.table_name
        ) :: TEXT                AS tcomment

    FROM information_schema.columns cols
    WHERE
        cols.table_schema || '.' || cols.table_name = p_table;
END;
$BODY$;

--drop function adt.csv_loader_pkg$copy_comments(text, text)

CREATE OR REPLACE FUNCTION adt.csv_loader_pkg$copy_comments(
    p_src_table TEXT, p_dst_table TEXT)
    RETURNS VOID
LANGUAGE 'plpgsql'
AS $BODY$
BEGIN

    SELECT
        *,
        adt.csv_loader_pkg$set_comment(p_dst_table, f.tname, fc.tcomment)
    FROM adt.csv_loader_pkg$get_comment_column_by_table(p_src_table) fc
        JOIN adt.csv_loader_pkg$get_comment_column_by_table(p_dst_table) f ON fc.tname = f.tname;
END;
$BODY$;

CREATE OR REPLACE FUNCTION adt.csv_loader_pkg$create_inherits_table(p_table_name       TEXT,
                                                                    p_suffix           TEXT,
                                                                    p_tablespace       TEXT,
                                                                    p_start_date       TIMESTAMP,
                                                                    p_finish_date      TIMESTAMP,
                                                                    p_partition_column TEXT)
    RETURNS VOID
LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
    l_replace TEXT;
BEGIN
    l_replace = replace(p_table_name, '.', '_');
    EXECUTE format(
        'CREATE TABLE IF NOT EXISTS %s_%s (CONSTRAINT %s_%s_%s_check CHECK(%s>=%L::TIMESTAMP AND %s< %L::TIMESTAMP)) INHERITS (%s) WITH (OIDS=TRUE) TABLESPACE  %s;'
        , p_table_name, p_suffix, l_replace, p_suffix, p_partition_column, p_partition_column, p_start_date,
        p_partition_column, p_finish_date, p_table_name, p_tablespace);
END;
$BODY$;


--drop function adt.csv_loader_pkg$create_index(p_index_name TEXT, p_table_name TEXT, p_column_list TEXT,
--                                                           p_tablespace TEXT, p_type TEXT, p_where TEXT,
--                                                           p_is_unique  TEXT)

CREATE OR REPLACE FUNCTION adt.csv_loader_pkg$create_index(p_index_name TEXT, p_table_name TEXT, p_column_list TEXT,
                                                           p_tablespace TEXT, p_type TEXT, p_where TEXT,
                                                           p_is_unique  TEXT)
    RETURNS VOID
LANGUAGE 'plpgsql'
AS $BODY$
BEGIN
    EXECUTE format(
        'CREATE %s INDEX IF NOT EXISTS %s ON %s  USING %s (%s) TABLESPACE %s %s ;', p_is_unique, p_index_name,
        p_table_name, p_type, p_column_list, p_tablespace,
        p_where);
END;
$BODY$;


CREATE OR REPLACE FUNCTION adt.csv_loader_pkg$create_default_inherits_table_and_indexes(p_table_name TEXT)
    RETURNS VOID
LANGUAGE 'plpgsql'
AS $BODY$
BEGIN
    EXECUTE format('CREATE TABLE IF NOT EXISTS %s_default (LIKE %s INCLUDING ALL) INHERITS (%s)', p_table_name,
                   p_table_name, p_table_name);
END;
$BODY$;

CREATE OR REPLACE FUNCTION adt.csv_loader_pkg$is_exist_table(p_table_name TEXT, p_suffix TEXT)
    RETURNS BOOLEAN
LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
    l_is_exist BOOLEAN;
BEGIN
    SELECT (count(1) = 1)
    INTO l_is_exist
    FROM information_schema.tables
    WHERE table_schema || '.' || table_name = p_table_name || '_' || p_suffix;
    RETURN l_is_exist;
END;
$BODY$;
