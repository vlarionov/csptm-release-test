﻿-- Tablespace: yandex_data

-- DROP TABLESPACE yandex_data

CREATE TABLESPACE yandex_data
  OWNER adv
  LOCATION '/u00/postgres/pg_data/yandex/yandex_data';

-- Tablespace: yandex_idx

-- DROP TABLESPACE yandex_idx

CREATE TABLESPACE yandex_idx
  OWNER adv
  LOCATION '/u00/postgres/pg_data/yandex/yandex_idx';

