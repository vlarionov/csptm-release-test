﻿-- view: yandex.v_tr

-- drop view yandex.v_tr

CREATE OR REPLACE VIEW yandex.v_tr AS
  SELECT DISTINCT ON (ctr.tr_id)
    ctr.tr_id                                                       trId,
    ctr.tr_id :: TEXT                                               uuid,
    yandex.yandex_pkg$get_vehicle_type(ctr.tr_type_id)              vehicleType,
    tt.timetable_num                                                route,
    yandex.yandex_pkg$safe_boolean_to_int(ctr.has_wheelchair_space) wheelchairAccessible,
    yandex.yandex_pkg$safe_boolean_to_int(ctr.has_bicicle_space)    bikesAllowed,
    yandex.yandex_pkg$safe_boolean_to_int(ctr.has_conditioner)      airConditioning,
    yandex.yandex_pkg$safe_boolean_to_int(ctr.has_low_floor)        lowFloor,
    0                                                               toDepot
  FROM asd.oper_round_visit orv
    JOIN tt.order_round ord ON orv.order_round_id = ord.order_round_id
    JOIN tt.order_list ol ON ord.order_list_id = ol.order_list_id
    JOIN core.tr ctr ON ctr.tr_id = ol.tr_id
    JOIN tt.round rnd ON ord.round_id = rnd.round_id
    JOIN tt.timetable_entry tte ON tte.timetable_entry_id = rnd.timetable_entry_id
    JOIN tt.timetable tt ON tte.timetable_id = tt.timetable_id
  WHERE now() BETWEEN orv.time_plan_begin AND orv.time_plan_end
        AND ol.is_active = 1 AND ol.sign_deleted = 0
        AND ord.is_active = 1
        AND ol.tr_id IS NOT NULL
  ORDER BY ctr.tr_id, orv.time_plan_begin DESC;

ALTER TABLE yandex.v_tr
  OWNER TO adv;

COMMENT ON VIEW yandex.v_tr IS 'Представление данных о ТС для экспорта в yandex';
COMMENT ON COLUMN yandex.v_tr.trId IS 'ID ТС';
COMMENT ON COLUMN yandex.v_tr.uuid IS 'ID ТС';
COMMENT ON COLUMN yandex.v_tr.vehicleType IS 'Тип транспорта';
COMMENT ON COLUMN yandex.v_tr.route IS 'Номер варианта расписания';
COMMENT ON COLUMN yandex.v_tr.wheelchairAccessible IS 'Признак наличия мест для инвалидов';
COMMENT ON COLUMN yandex.v_tr.bikesAllowed IS 'Признак наличия мест для велосипедов';
COMMENT ON COLUMN yandex.v_tr.airConditioning IS 'Признак наличия кондиционера';
COMMENT ON COLUMN yandex.v_tr.lowFloor IS 'Признак низкопольности';
COMMENT ON COLUMN yandex.v_tr.toDepot IS 'Признак следования в депо';

-- select * from yandex.v_tr
