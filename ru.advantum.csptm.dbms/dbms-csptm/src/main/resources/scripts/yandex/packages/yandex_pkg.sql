CREATE OR REPLACE FUNCTION yandex.yandex_pkg$get_vehicle_type(tr_type_id core.tr_type.tr_type_id%TYPE)
  RETURNS TEXT IMMUTABLE
LANGUAGE plpgsql
AS $$
BEGIN
  IF tr_type_id = 1
  THEN RETURN 'bus';
  ELSEIF tr_type_id = 2
    THEN RETURN 'trolleybus';
  ELSEIF tr_type_id = 3 OR tr_type_id = 4
    THEN RETURN 'tramway';
  END IF;
  RETURN NULL;
END;
$$;

CREATE OR REPLACE FUNCTION yandex.yandex_pkg$safe_boolean_to_int(p_value BOOLEAN)
  RETURNS INTEGER IMMUTABLE
LANGUAGE plpgsql
AS $$
BEGIN
  IF p_value IS NULL
  THEN RETURN 0;
  ELSE
    RETURN p_value :: INTEGER;
  END IF;
END;
$$;

CREATE OR REPLACE FUNCTION yandex.yandex_pkg$get_posted_tr(p_start_date TIMESTAMP, p_end_date TIMESTAMP)
  RETURNS TABLE(tr_id BIGINT)
LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  select trs.tr_id
  from (
         select
           ol.tr_id,
           tsrange(min(orv.time_plan_begin), max(orv.time_plan_end)) * tsrange(p_start_date, p_end_date) r
         from tt.order_list ol
           join tt.order_round ord on ol.order_list_id = ord.order_list_id
           join asd.oper_round_visit orv on orv.order_round_id = ord.order_round_id
         where ol.order_date between p_start_date :: date and p_end_date :: date
               and tsrange(orv.time_plan_begin, orv.time_plan_end) && tsrange(p_start_date, p_end_date)
               and ol.is_active = 1 and ol.sign_deleted = 0 and ord.is_active = 1
         group by ol.tr_id
       ) trs
  where exists(select 1
               from core.traffic t
               where t.tr_id = trs.tr_id and t.event_time between lower(trs.r) and upper(trs.r));
END;
$$;