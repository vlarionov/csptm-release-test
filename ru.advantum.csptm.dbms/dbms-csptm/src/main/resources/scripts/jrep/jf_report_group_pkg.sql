CREATE OR REPLACE FUNCTION jrep."jf_report_group_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$

begin
  open p_rows for
  select distinct r.id_report_group,
         r.id_parent_group,
         r.order_num,
         r.s_name
  from jrep.report_group r;
end;
$function$
;


CREATE OR REPLACE FUNCTION jrep."jf_report_group_pkg$of_update"(p_id_account numeric, p_attr text)
  RETURNS text
LANGUAGE plpgsql
AS $function$

declare
  p jrep.report_group%rowtype;
begin
  p := jrep.jf_report_group_pkg$attr_to_rowtype(p_attr);

  update jrep.report_group set
    s_name = p.s_name,
    order_num = p.order_num,
    id_parent_group= p.id_parent_group
  where
    jrep.report_group.id_parent_group = p.id_parent_group;

  return null;
end;
$function$
;

CREATE OR REPLACE FUNCTION jrep."jf_report_group_pkg$of_delete"(p_id_account numeric, p_attr text)
  RETURNS text
LANGUAGE plpgsql
AS $function$

declare
  r jrep.report_group%rowtype;
begin
  r := jrep.jf_report_group_pkg$attr_to_rowtype(p_attr);

  delete from  jrep.report_group where id_report_group = r.id_report_group;

  return null;
end;
$function$
;

CREATE OR REPLACE FUNCTION jrep."jf_report_group_pkg$of_insert"(p_id_account numeric, p_attr text)
  RETURNS text
LANGUAGE plpgsql
AS $function$

declare
  r jrep.report_group%rowtype;
begin
  r := jrep.jf_report_group_pkg$attr_to_rowtype(p_attr);
  r.id_report_group := nextval( 'jrep.report_group_id_report_group_seq' );

  if r.id_report_group < 0 then
    r.id_report_group := null;
  end if;

  insert into jrep.report_group select r.*;

  return null;
end;
$function$
;

CREATE OR REPLACE FUNCTION jrep."jf_report_group_pkg$attr_to_rowtype"(p_attr text)
  RETURNS jrep.report_group
LANGUAGE plpgsql
AS $function$

declare
  r jrep.report_group%rowtype;
begin
  r.id_report_group := jofl.jofl_pkg$extract_number(p_attr, 'id_report_group', true);
  r.id_parent_group := jofl.jofl_pkg$extract_number(p_attr, 'id_parent_group', true);
  r.order_num := jofl.jofl_pkg$extract_number(p_attr, 'order_num', true);
  r.s_name := jofl.jofl_pkg$extract_varchar(p_attr, 's_name', true);

  return r;
end;
$function$
;
