create or replace function jrep.report_data_pkg$report_list(
        aid_reports integer [],
        aplatform   text,
        alocation   text,
    out areports    refcursor,
    out afilters    refcursor,
    out agroups     refcursor)
    returns record
as $$
declare
begin
    open areports for
    select
        r.id_report as "ID_REPORT",
        r.s_name as "REPORT_NAME",
        r.id_report_group as "ID_REPORT_GROUP",
        (select string_agg(format_type, ',')
         from jrep.report_format
         where id_report = r.id_report) as "FORMATS",
        ui.class_source as "CUSTOM_CLASS",
        r.name_mask as "NAME_MASK",
        r.is_immediate as "IS_IMMEDIATE",
        r.order_num as "ORDER_NUM"
    from jrep.report r
        inner join (select unnest(aid_reports) column_value) rpts on r.id_report = rpts.column_value
        left outer join jrep.report_ui_class ui on r.id_report = ui.id_report and ui.id_platform = aplatform
    where r.id_location = alocation
    order by r.id_report;

    open afilters for
    select
        rf.id_report as "ID_REPORT",
        fs.source_class as "SOURCE_CLASS",
        rf.initial_param as "INITIAL_PARAM",
        rf.grid_x as "GRID_X",
        rf.grid_y as "GRID_Y",
        rf.dept_group as "DEPT_GROUP",
        rf.dept_priority as "DEPT_PRIORITY",
        rf.excl_group as "EXCL_GROUP"
    from jrep.report_filter rf
        inner join jrep.report r on rf.id_report = r.id_report
        inner join jofl.ref_filter_source fs on fs.filter_class = rf.filter_class
        inner join (select unnest(aid_reports) column_value) rpts on rpts.column_value = rf.id_report
    where r.id_location = alocation
          and fs.id_platform = aplatform
    order by rf.id_report;

    open agroups for
    select
        id_report_group as "ID_REPORT_GROUP",
        s_name as "GROUP_NAME",
        order_num as "ORDER_NUM",
        id_parent_group as "ID_PARENT_GROUP"
    from jrep.report_group;

end;
$$ language plpgsql;
