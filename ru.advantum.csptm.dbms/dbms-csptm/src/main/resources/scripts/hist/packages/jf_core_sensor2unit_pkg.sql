﻿CREATE OR REPLACE FUNCTION hist."jf_core_sensor2unit_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_rf_db_method text;
  l_equipment_id core.equipment.equipment_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
begin
  l_rf_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', TRUE);

 open p_rows for

 with hist as(
   select  su.sensor_num, su.sensor_id,    su.unit_id, su.sys_period   from hist.core_sensor2unit su
   union all
   select  null,          hu.hdd_id,       hu.unit_id, hu.sys_period   from hist.core_hdd2unit hu
   union all
   select  null,          mu.modem_id,     mu.unit_id, mu.sys_period   from hist.core_modem2unit mu
   union all
   select  null,          mu.monitor_id,   mu.unit_id, mu.sys_period   from hist.core_monitor2unit mu
 )
 select
       su.sensor_num,
       su.sensor_id,
       su.unit_id,
       lower(su.sys_period) as dt_begin,
       upper(su.sys_period) as dt_end,

       ss.equipment_type_name as sensor_type_name,
       ss.equipment_model_name as sensor_model_name,
       ss.serial_num as sensor_serial_num,

       bb.equipment_type_name as unit_type_name,
       bb.equipment_model_name as unit_model_name,
       bb.serial_num as unit_serial_num

     from hist su
       join core.v_equipment ss on ss.equipment_id = su.sensor_id
       join core.v_equipment bb on bb.equipment_id = su.unit_id

     where
       su.unit_id = l_equipment_id or su.sensor_id =l_equipment_id
;


end;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hist."jf_core_sensor2unit_pkg$of_rows"(numeric, text)
  OWNER TO adv;