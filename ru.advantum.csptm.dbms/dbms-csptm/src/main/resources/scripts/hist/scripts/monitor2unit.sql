ALTER TABLE core.monitor2unit ADD COLUMN sys_period tstzrange NOT NULL DEFAULT tstzrange(current_timestamp, null);

--DROP TABLE hist.monitor2unit;
CREATE TABLE hist.core_monitor2unit (LIKE core.monitor2unit) tablespace hist_data;

update core.monitor2unit
    set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

CREATE TRIGGER versioning_trigger
BEFORE INSERT OR UPDATE OR DELETE ON core.monitor2unit
FOR EACH ROW EXECUTE PROCEDURE public.versioning(
    'sys_period', 'hist.core_monitor2unit', true
);


CREATE VIEW hist.v_monitor2unit_hist AS
    SELECT * FROM core.monitor2unit
  UNION ALL
    SELECT * FROM hist.core_monitor2unit;


CREATE INDEX ix_monitor2unit_sys_period ON core.monitor2unit USING gist (sys_period) tablespace core_idx;
CREATE INDEX ON hist.core_monitor2unit USING gist (sys_period) tablespace hist_idx;
