﻿--alter table rts.depo2route add column sys_period tstzrange not null default tstzrange(current_timestamp, null);

--drop index rts.ix_depo2route_sys_period;

--drop trigger versioning_trigger on rts.depo2route;

--drop table hist.rts_depo2route;

create table hist.rts_depo2route (like rts.depo2route) tablespace hist_data;

--update rts.depo2route set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on rts.depo2route
   for each row execute procedure public.versioning('sys_period', 'hist.rts_depo2route', true);

create index ix_depo2route_sys_period on rts.depo2route using gist (sys_period) tablespace rts_idx;
create index on hist.rts_depo2route using gist (sys_period) tablespace hist_idx;

goto ... create view hist.v_depo2route_hist as