﻿
--alter table ibrd.info_board add column sys_period tstzrange not null default tstzrange(current_timestamp, null);
--comment on column ibrd.info_board.sys_period is 'Текущий период';

--drop index ibrd.ix_info_board_sys_period;

--drop trigger versioning_trigger on ibrd.info_board;

--drop table hist.ibrd_info_board;

create table hist.ibrd_info_board (like ibrd.info_board) tablespace hist_data;

--update ibrd.info_board set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on ibrd.info_board
   for each row execute procedure public.versioning('sys_period', 'hist.ibrd_info_board', true);

create index ix_info_board_sys_period on ibrd.info_board using gist (sys_period) tablespace ibrd_idx;
create index on hist.ibrd_info_board using gist (sys_period) tablespace hist_idx;

goto ... create view hist.v_info_board_hist as