﻿--alter table voip.voice_call add column sys_period tstzrange not null default tstzrange(current_timestamp, null);
--comment on column voip.voice_call.sys_period is 'Период действия';

--create table hist.tmp_voip_voice_call as select * from hist.voip_voice_call

--drop index voip.ix_voice_call_sys_period;

--drop trigger versioning_trigger on voip.voice_call;

--drop table hist.voip_voice_call cascade;

create table hist.voip_voice_call (like voip.voice_call) tablespace hist_data;

--update voip.voice_call set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on voip.voice_call
   for each row execute procedure public.versioning('sys_period', 'hist.voip_voice_call', true);

create index ix_voice_call_sys_period on voip.voice_call using gist (sys_period) tablespace voip_idx;
create index on hist.voip_voice_call using gist (sys_period) tablespace hist_idx;

goto ... create view hist.v_voice_call_hist as

--drop table hist.tmp_voip_voice_call