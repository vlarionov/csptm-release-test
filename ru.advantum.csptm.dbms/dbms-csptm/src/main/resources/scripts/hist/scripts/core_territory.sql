﻿
--alter table core.territory add column sys_period tstzrange not null default tstzrange(current_timestamp, null);

--drop index core.ix_territory_sys_period;

--drop trigger versioning_trigger on core.territory;

--drop table hist.core_territory;

create table hist.core_territory (like core.territory) tablespace hist_data;

--update core.territory set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on core.territory
   for each row execute procedure public.versioning('sys_period', 'hist.core_territory', true);

create index ix_territory_sys_period on core.territory using gist (sys_period) tablespace core_idx;
create index on hist.core_territory using gist (sys_period) tablespace hist_idx;

goto ... create view hist.v_territory_hist as