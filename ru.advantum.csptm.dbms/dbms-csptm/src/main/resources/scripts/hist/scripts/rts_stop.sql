﻿
--alter table rts.stop add column sys_period tstzrange not null default tstzrange(current_timestamp, null);

--drop index rts.ix_stop_sys_period;

--drop trigger versioning_trigger on rts.stop;

--drop table hist.rts_stop cascade;

create table hist.rts_stop (like rts.stop) tablespace hist_data;

--update rts.stop set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on rts.stop
   for each row execute procedure public.versioning('sys_period', 'hist.rts_stop', true);

create index ix_stop_sys_period on rts.stop using gist (sys_period) tablespace rts_idx;
create index on hist.rts_stop using gist (sys_period) tablespace hist_idx;

--goto ... create view hist.v_stop_hist as
