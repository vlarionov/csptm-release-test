
--drop trigger versioning_trigger on ttb.tt_action_round;

--drop table hist.ttb_tt_action_round;

create table hist.ttb_tt_action_round (like ttb.tt_action_round) tablespace hist_data;


create trigger versioning_trigger
   before insert or update or delete on ttb.tt_action_round
   for each row execute procedure public.versioning('sys_period', 'hist.ttb_tt_action_round', true);

create index ix_tt_action_round_sys_period on ttb.tt_action_round using gist (sys_period) tablespace ttb_idx;


create index on hist.ttb_tt_action_round using gist (sys_period) tablespace hist_idx;

