
--drop table hist.ttb_norm cascade;

CREATE TABLE hist.ttb_norm (LIKE ttb.norm)  tablespace hist_data;

--drop TRIGGER versioning_trigger ON ttb.norm;

CREATE TRIGGER versioning_trigger
BEFORE INSERT OR UPDATE OR DELETE ON ttb.norm
FOR EACH ROW EXECUTE PROCEDURE public.versioning(
    'sys_period', 'hist.ttb_norm', true
);


CREATE VIEW hist.v_norm_hist AS
    SELECT * FROM ttb.norm
  UNION ALL
    SELECT * FROM hist.ttb_norm;


CREATE INDEX ix_norm_sys_period ON ttb.norm USING gist (sys_period) tablespace ttb_idx;
CREATE INDEX ON hist.ttb_norm USING gist (sys_period) tablespace hist_idx;
