﻿--alter table ttb.tt_action add column sys_period tstzrange not null default tstzrange(current_timestamp, null);
--comment on column ttb.tt_action.sys_period is 'Период действия';

--drop index ttb.ix_tt_action_sys_period;

--drop trigger versioning_trigger on ttb.tt_action;

--drop table hist.ttb_tt_action;

create table hist.ttb_tt_action (like ttb.tt_action) tablespace hist_data;

--update ttb.tt_action set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on ttb.tt_action
   for each row execute procedure public.versioning('sys_period', 'hist.ttb_tt_action', true);

create index ix_tt_action_sys_period on ttb.tt_action using gist (sys_period) tablespace ttb_idx;
create index on hist.ttb_tt_action using gist (sys_period) tablespace hist_idx;

--goto ... create view hist.v_tt_action_hist as