﻿
--alter table ttb.tt_calendar add column sys_period tstzrange not null default tstzrange(current_timestamp, null);
--comment on column ttb.tt_calendar.sys_period is 'Период действия';

--drop index voip.ix_tt_calendar_sys_period;

--drop trigger versioning_trigger on ttb.tt_calendar;

--drop table hist.ttb_tt_calendar;

create table hist.ttb_tt_calendar (like ttb.tt_calendar) tablespace hist_data;

--update ttb.tt_calendar set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on ttb.tt_calendar
   for each row execute procedure public.versioning('sys_period', 'hist.ttb_tt_calendar', true);

create index ix_tt_calendar_sys_period on ttb.tt_calendar using gist (sys_period) tablespace ttb_idx;
create index on hist.ttb_tt_calendar using gist (sys_period) tablespace hist_idx;

--goto ... create view hist.v_tt_calendar_hist as