﻿--alter table rts.stop_location add column sys_period tstzrange not null default tstzrange(current_timestamp, null);

--drop index rts.ix_stop_location_sys_period;

--drop trigger versioning_trigger on rts.stop_location;

--drop table hist.rts_stop_location;

create table hist.rts_stop_location (like rts.stop_location) tablespace hist_data;

--update rts.stop_location set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on rts.stop_location
   for each row execute procedure public.versioning('sys_period', 'hist.rts_stop_location', true);

create index ix_stop_location_sys_period on rts.stop_location using gist (sys_period) tablespace rts_idx;
create index on hist.rts_stop_location using gist (sys_period) tablespace hist_idx;

goto ... create view hist.v_stop_location_hist as
