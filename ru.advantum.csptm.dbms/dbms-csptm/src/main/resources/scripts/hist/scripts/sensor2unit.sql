ALTER TABLE core.sensor2unit ADD COLUMN sys_period tstzrange NOT NULL DEFAULT tstzrange(current_timestamp, null);

--DROP TABLE hist.sensor2unit;
CREATE TABLE hist.core_sensor2unit (LIKE core.sensor2unit) tablespace hist_data;

update core.sensor2unit
    set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

CREATE TRIGGER versioning_trigger
BEFORE INSERT OR UPDATE OR DELETE ON core.sensor2unit
FOR EACH ROW EXECUTE PROCEDURE public.versioning(
    'sys_period', 'hist.core_sensor2unit', true
);


CREATE VIEW hist.v_sensor2unit_hist AS
    SELECT * FROM core.sensor2unit
  UNION ALL
    SELECT * FROM hist.core_sensor2unit;


CREATE INDEX ON core.sensor2unit USING gist (sys_period) tablespace core_idx;
CREATE INDEX ON hist.core_sensor2unit USING gist (sys_period) tablespace hist_idx;
