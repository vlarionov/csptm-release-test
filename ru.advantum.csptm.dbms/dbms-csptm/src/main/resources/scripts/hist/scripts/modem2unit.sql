ALTER TABLE core.modem2unit ADD COLUMN sys_period tstzrange NOT NULL DEFAULT tstzrange(current_timestamp, null);

CREATE TABLE hist.core_modem2unit (LIKE core.modem2unit) tablespace hist_data;

update core.modem2unit
    set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

CREATE TRIGGER versioning_trigger
BEFORE INSERT OR UPDATE OR DELETE ON core.modem2unit
FOR EACH ROW EXECUTE PROCEDURE public.versioning(
    'sys_period', 'hist.core_modem2unit', true
);


CREATE VIEW hist.v_modem2unit_hist AS
    SELECT * FROM core.modem2unit
  UNION ALL
    SELECT * FROM hist.core_modem2unit;


CREATE INDEX ix_modem2unit_sys_period ON core.modem2unit USING gist (sys_period) tablespace core_idx;
CREATE INDEX ON hist.core_modem2unit USING gist (sys_period) tablespace hist_idx;
