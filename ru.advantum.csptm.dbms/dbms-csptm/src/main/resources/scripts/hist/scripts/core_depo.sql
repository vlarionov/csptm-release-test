﻿--alter table core.depo add column sys_period tstzrange not null default tstzrange(current_timestamp, null);

--drop index core.ix_depo_sys_period;

--drop trigger versioning_trigger on core.depo;

--drop table hist.core_depo;

create table hist.core_depo (like core.depo) tablespace hist_data;

--update core.depo set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on core.depo
   for each row execute procedure public.versioning('sys_period', 'hist.core_depo', true);

create index ix_depo_sys_period on core.depo using gist (sys_period) tablespace core_idx;
create index on hist.core_depo using gist (sys_period) tablespace hist_idx;

goto ... create view hist.v_depo_hist as
