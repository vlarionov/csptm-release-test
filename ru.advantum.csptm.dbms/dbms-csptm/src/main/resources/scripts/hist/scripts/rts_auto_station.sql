﻿--alter table rts.auto_station add column sys_period tstzrange not null default tstzrange(current_timestamp, null);

--drop index rts.ix_auto_station_sys_period;

--drop trigger versioning_trigger on rts.auto_station;

--drop table hist.rts_auto_station;

create table hist.rts_auto_station (like rts.auto_station) tablespace hist_data;

--update rts.auto_station set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on rts.auto_station
   for each row execute procedure public.versioning('sys_period', 'hist.rts_auto_station', true);

create index ix_auto_station_sys_period on rts.auto_station using gist (sys_period) tablespace rts_idx;
create index on hist.rts_auto_station using gist (sys_period) tablespace hist_idx;

goto ... create view hist.v_auto_station_hist as