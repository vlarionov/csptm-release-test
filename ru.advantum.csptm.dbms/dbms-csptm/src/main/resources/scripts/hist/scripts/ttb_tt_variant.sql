﻿--alter table ttb.tt_variant add column sys_period tstzrange not null default tstzrange(current_timestamp, null);
--comment on column ttb.tt_variant.sys_period is 'Период действия';

--drop index ttb.ix_tt_variant_sys_period;

--drop trigger versioning_trigger on ttb.tt_variant;

--drop table hist.ttb_tt_variant cascade;

create table hist.ttb_tt_variant (like ttb.tt_variant) tablespace hist_data;

--update ttb.tt_variant set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on ttb.tt_variant
   for each row execute procedure public.versioning('sys_period', 'hist.ttb_tt_variant', true);

create index ix_tt_variant_sys_period on ttb.tt_variant using gist (sys_period) tablespace ttb_idx;
create index on hist.ttb_tt_variant using gist (sys_period) tablespace hist_idx;

--goto ... create view hist.v_tt_variant_hist as