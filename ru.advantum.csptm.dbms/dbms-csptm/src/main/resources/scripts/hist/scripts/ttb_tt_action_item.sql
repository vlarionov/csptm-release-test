
--drop trigger versioning_trigger on ttb.tt_action_item;

--drop table hist.ttb_tt_action_item;

create table hist.ttb_tt_action_item (like ttb.tt_action_item) tablespace hist_data;


create trigger versioning_trigger
   before insert or update or delete on ttb.tt_action_item
   for each row execute procedure public.versioning('sys_period', 'hist.ttb_tt_action_item', true);

create index ix_tt_action_item_sys_period on ttb.tt_action_item using gist (sys_period) tablespace ttb_idx;


create index on hist.ttb_tt_action_item using gist (sys_period) tablespace hist_idx;

