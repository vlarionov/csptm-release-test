﻿--alter table rts.zone_location add column sys_period tstzrange not null default tstzrange(current_timestamp, null);

--drop index rts.ix_zone_location_sys_period;

--drop trigger versioning_trigger on rts.zone_location;

--drop table hist.rts_zone_location;

create table hist.rts_zone_location (like rts.zone_location) tablespace hist_data;

--update rts.zone_location set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on rts.zone_location
   for each row execute procedure public.versioning('sys_period', 'hist.rts_zone_location', true);

create index ix_zone_location_sys_period on rts.zone_location using gist (sys_period) tablespace rts_idx;
create index on hist.rts_zone_location using gist (sys_period) tablespace hist_idx;

goto ... create view hist.v_zone_location_hist as