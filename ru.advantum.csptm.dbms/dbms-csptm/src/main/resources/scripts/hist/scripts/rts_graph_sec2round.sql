﻿--alter table rts.graph_sec2round add column sys_period tstzrange not null default tstzrange(current_timestamp, null);

--drop index rts.ix_graph_sec2round_sys_period;

--drop trigger versioning_trigger on rts.graph_sec2round;

--drop table hist.rts_graph_sec2round;

create table hist.rts_graph_sec2round (like rts.graph_sec2round) tablespace hist_data;

--update rts.graph_sec2round set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on rts.graph_sec2round
   for each row execute procedure public.versioning('sys_period', 'hist.rts_graph_sec2round', true);

create index ix_graph_sec2round_sys_period on rts.graph_sec2round using gist (sys_period) tablespace rts_idx;
create index on hist.rts_graph_sec2round using gist (sys_period) tablespace hist_idx;

goto ... create view hist.v_graph_sec2round_hist as