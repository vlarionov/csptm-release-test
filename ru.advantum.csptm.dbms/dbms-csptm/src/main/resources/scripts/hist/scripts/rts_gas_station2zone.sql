﻿alter table rts.gas_station2zone add column sys_period tstzrange not null default tstzrange(current_timestamp, null);
comment on column rts.gas_station2zone.sys_period is 'Период действия';

--drop index rts.ix_gas_station2zone_sys_period;

--drop trigger versioning_trigger on rts.gas_station2zone;

--drop table hist.rts_gas_station2zone;

create table hist.rts_gas_station2zone (like rts.gas_station2zone) tablespace hist_data;

--update rts.gas_station2zone set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on rts.gas_station2zone
   for each row execute procedure public.versioning('sys_period', 'hist.rts_gas_station2zone', true);

create index ix_gas_station2zone_sys_period on rts.gas_station2zone using gist (sys_period) tablespace rts_idx;
create index on hist.rts_gas_station2zone using gist (sys_period) tablespace hist_idx;

goto ... create view hist.v_gas_station2zone_hist as