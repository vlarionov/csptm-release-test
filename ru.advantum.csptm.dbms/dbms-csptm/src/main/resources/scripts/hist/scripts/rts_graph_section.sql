﻿--alter table rts.graph_section add column sys_period tstzrange not null default tstzrange(current_timestamp, null);

--drop index rts.ix_graph_section_sys_period;

--drop trigger versioning_trigger on rts.graph_section;

--drop table hist.rts_graph_section;

create table hist.rts_graph_section (like rts.graph_section) tablespace hist_data;

--update rts.graph_section set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on rts.graph_section
   for each row execute procedure public.versioning('sys_period', 'hist.rts_graph_section', true);

create index ix_graph_section_sys_period on rts.graph_section using gist (sys_period) tablespace rts_idx;
create index on hist.rts_graph_section using gist (sys_period) tablespace hist_idx;

goto ... create view hist.v_graph_section_hist as