﻿alter table core.entity add column sys_period tstzrange not null default tstzrange(current_timestamp, null);
comment on column core.entity.sys_period is 'Период действия';

--drop index core.ix_entity_sys_period;

--drop trigger versioning_trigger on core.entity;

--drop table hist.core_entity;

create table hist.core_entity (like core.entity) tablespace hist_data;

--update core.entity set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on core.entity
   for each row execute procedure public.versioning('sys_period', 'hist.core_entity', true);

create index ix_entity_sys_period on core.entity using gist (sys_period) tablespace core_idx;
create index on hist.core_entity using gist (sys_period) tablespace hist_idx;

goto ... create view hist.v_entity_hist as