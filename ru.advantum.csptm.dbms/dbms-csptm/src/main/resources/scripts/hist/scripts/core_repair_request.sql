create table hist.repair_request (like core.repair_request ) tablespace hist_data;

create table hist.core_repair_request

(
	repair_request_id bigserial not null
		constraint pk_repair_request_repair_request_id
			primary key,
	dt timestamp not null,
	description text,
	dt_closed timestamp,
	repair_request_status_id bigint not null,
	breakdown_type_id bigint not null,
	close_result_id bigint,
	territory_id bigint,
	facility_id bigint,
	breakdown_id bigint,
	num serial not null,
	equipment_id bigint,
	replacement_equipment_id bigint
)
;


comment on table hist.core_repair_request is 'Заявка на ремонт БО'
;

comment on column hist.core_repair_request.repair_request_id is 'Заявка на ремонт БО'
;

comment on column hist.core_repair_request.dt is 'Дата создания'
;

comment on column hist.core_repair_request.description is 'Описание поломки'
;

comment on column hist.core_repair_request.dt_closed is 'Дата закрытия'
;

comment on column hist.core_repair_request.repair_request_status_id is 'Статус заявки'
;

comment on column hist.core_repair_request.breakdown_type_id is 'Тип поломки'
;

comment on column hist.core_repair_request.close_result_id is 'Результат закрытия'
;

comment on column hist.core_repair_request.territory_id is 'Территория'
;

comment on column hist.core_repair_request.facility_id is 'Подрядчик'
;

comment on column hist.core_repair_request.breakdown_id is 'Запись о поломке'
;

comment on column hist.core_repair_request.num is 'Номер завки на ремонт'
;

comment on column hist.core_repair_request.equipment_id is 'Оборудование'
;

comment on column hist.core_repair_request.replacement_equipment_id is 'Оборудование на замену'
;


create trigger versioning_trigger
   before insert or update or delete on core.repair_request
   for each row execute procedure public.versioning('sys_period', 'hist.repair_request ', true);

create index ix_tt_variant_sys_period on core.repair_request using gist (sys_period) tablespace voip_idx;
create index on hist.repair_request using gist (sys_period) tablespace hist_idx;

goto ... create view hist.v_repair_request_hist as