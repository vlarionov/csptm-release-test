﻿--alter table rts.route_variant add column sys_period tstzrange not null default tstzrange(current_timestamp, null);

--drop index rts.ix_route_variant_sys_period;

--drop trigger versioning_trigger on rts.route_variant;

--drop table hist.rts_route_variant;

create table hist.rts_route_variant (like rts.route_variant) tablespace hist_data;

--update rts.route_variant set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on rts.route_variant
   for each row execute procedure public.versioning('sys_period', 'hist.rts_route_variant', true);

create index ix_route_variant_sys_period on rts.route_variant using gist (sys_period) tablespace rts_idx;
create index on hist.rts_route_variant using gist (sys_period) tablespace hist_idx;

goto ... create view hist.v_route_variant_hist as