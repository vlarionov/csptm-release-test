﻿alter table rts.stop_group add column sys_period tstzrange not null default tstzrange(current_timestamp, null);
comment on column rts.stop_group.sys_period is 'Период действия';

--drop index rts.ix_stop_group_sys_period;

--drop trigger versioning_trigger on rts.stop_group;

--drop table hist.rts_stop_group cascade;

create table hist.rts_stop_group (like rts.stop_group) tablespace hist_data;

--update rts.stop_group set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on rts.stop_group
   for each row execute procedure public.versioning('sys_period', 'hist.rts_stop_group', true);

create index ix_stop_group_sys_period on rts.stop_group using gist (sys_period) tablespace rts_idx;
create index on hist.rts_stop_group using gist (sys_period) tablespace hist_idx;

-- goto ... create view hist.v_stop_group_hist as