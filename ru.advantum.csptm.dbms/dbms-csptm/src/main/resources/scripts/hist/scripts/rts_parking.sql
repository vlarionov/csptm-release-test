﻿
--alter table rts.parking add column sys_period tstzrange not null default tstzrange(current_timestamp, null);

--drop index rts.ix_parking_sys_period;

--drop trigger versioning_trigger on rts.parking;

--drop table hist.rts_parking;

create table hist.rts_parking (like rts.parking) tablespace hist_data;

--update rts.parking set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on rts.parking
   for each row execute procedure public.versioning('sys_period', 'hist.rts_parking', true);

create index ix_parking_sys_period on rts.parking using gist (sys_period) tablespace rts_idx;
create index on hist.rts_parking using gist (sys_period) tablespace hist_idx;

goto ... create view hist.v_parking_hist as