﻿--alter table rts.stop_item add column sys_period tstzrange not null default tstzrange(current_timestamp, null);

--drop index rts.ix_stop_item_sys_period;

--drop trigger versioning_trigger on rts.stop_item;

--drop table hist.rts_stop_item;

create table hist.rts_stop_item (like rts.stop_item) tablespace hist_data;

--update rts.stop_item set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on rts.stop_item
   for each row execute procedure public.versioning('sys_period', 'hist.rts_stop_item', true);

create index ix_stop_item_sys_period on rts.stop_item using gist (sys_period) tablespace rts_idx;
create index on hist.rts_stop_item using gist (sys_period) tablespace hist_idx;

--goto ... create view hist.v_stop_item_hist as