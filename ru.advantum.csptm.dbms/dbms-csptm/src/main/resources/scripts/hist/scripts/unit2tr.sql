ALTER TABLE core.unit2tr ADD COLUMN sys_period tstzrange NOT NULL DEFAULT tstzrange(current_timestamp, null);

--DROP TABLE hist.unit2tr;
CREATE TABLE hist.core_unit2tr (LIKE core.unit2tr) tablespace hist_data;

update core.unit2tr
    set sys_period = tstzrange('2010-05-18 00:00:00'::TIMESTAMP, null);

CREATE TRIGGER versioning_trigger
BEFORE INSERT OR UPDATE OR DELETE ON core.unit2tr
FOR EACH ROW EXECUTE PROCEDURE public.versioning(
    'sys_period', 'hist.core_unit2tr', true
);


CREATE VIEW hist.v_unit2tr_hist AS
    SELECT * FROM core.unit2tr
  UNION ALL
    SELECT * FROM hist.core_unit2tr;


CREATE INDEX ix_unit2tr_sys_period ON core.unit2tr USING gist (sys_period) tablespace core_idx;
CREATE INDEX ON hist.core_unit2tr USING gist (sys_period) tablespace hist_idx;