﻿alter table rts.depo2zone add column sys_period tstzrange not null default tstzrange(current_timestamp, null);
comment on column rts.depo2zone.sys_period is 'Период действия';

--drop index rts.ix_depo2zone_sys_period;

--drop trigger versioning_trigger on rts.depo2zone;

--drop table hist.rts_depo2zone;

create table hist.rts_depo2zone (like rts.depo2zone) tablespace hist_data;

--update rts.depo2zone set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on rts.depo2zone
   for each row execute procedure public.versioning('sys_period', 'hist.rts_depo2zone', true);

create index ix_depo2zone_sys_period on rts.depo2zone using gist (sys_period) tablespace rts_idx;
create index on hist.rts_depo2zone using gist (sys_period) tablespace hist_idx;

goto ... create view hist.v_depo2zone_hist as