--ALTER TABLE core.tr ADD COLUMN sys_period tstzrange NOT NULL DEFAULT tstzrange(current_timestamp, null);

--drop INDEX core.ix_tr_sys_period ;


DROP TABLE hist.core_tr cascade;

CREATE TABLE hist.core_tr (LIKE core.tr) tablespace hist_data;

--update core.tr set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

DROP TRIGGER versioning_trigger ON core.tr;

CREATE TRIGGER versioning_trigger
BEFORE INSERT OR UPDATE OR DELETE ON core.tr
FOR EACH ROW EXECUTE PROCEDURE public.versioning(
    'sys_period', 'hist.core_tr', true
);


CREATE VIEW hist.v_tr_hist AS
    SELECT * FROM core.tr
  UNION ALL
    SELECT * FROM hist.core_tr;


CREATE INDEX ix_tr_sys_period ON core.tr USING gist (sys_period) tablespace core_idx;
CREATE INDEX ON hist.core_tr USING gist (sys_period) tablespace hist_idx;
