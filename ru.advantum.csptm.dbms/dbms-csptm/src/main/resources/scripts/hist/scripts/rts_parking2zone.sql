﻿alter table rts.parking2zone add column sys_period tstzrange not null default tstzrange(current_timestamp, null);
comment on column rts.parking2zone.sys_period is 'Период действия';

--drop index rts.ix_parking2zone_sys_period;

--drop trigger versioning_trigger on rts.parking2zone;

--drop table hist.rts_parking2zone;

create table hist.rts_parking2zone (like rts.parking2zone) tablespace hist_data;

--update rts.parking2zone set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on rts.parking2zone
   for each row execute procedure public.versioning('sys_period', 'hist.rts_parking2zone', true);

create index ix_parking2zone_sys_period on rts.parking2zone using gist (sys_period) tablespace rts_idx;
create index on hist.rts_parking2zone using gist (sys_period) tablespace hist_idx;

goto ... create view hist.v_parking2zone_hist as
