
--drop trigger versioning_trigger on ttb.tt_out;

--drop table hist.ttb_tt_out;

create table hist.ttb_tt_out (like ttb.tt_out) tablespace hist_data;


create trigger versioning_trigger
   before insert or update or delete on ttb.tt_out
   for each row execute procedure public.versioning('sys_period', 'hist.ttb_tt_out', true);

create index ix_tt_out_sys_period on ttb.tt_out using gist (sys_period) tablespace ttb_idx;
create index on hist.ttb_tt_out using gist (sys_period) tablespace hist_idx;

