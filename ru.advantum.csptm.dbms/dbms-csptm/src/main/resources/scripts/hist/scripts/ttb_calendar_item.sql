﻿
--alter table ttb.calendar_item add column sys_period tstzrange not null default tstzrange(current_timestamp, null);
--comment on column ttb.calendar_item.sys_period is 'Период действия';

--drop index voip.ix_calendar_item_sys_period;

--drop trigger versioning_trigger on ttb.calendar_item;

--drop table hist.ttb_calendar_item;

create table hist.ttb_calendar_item (like ttb.calendar_item) tablespace hist_data;

--update ttb.calendar_item set sys_period = tstzrange('2010-05-18 00:00:00'::timestamp, null);

create trigger versioning_trigger
   before insert or update or delete on ttb.calendar_item
   for each row execute procedure public.versioning('sys_period', 'hist.ttb_calendar_item', true);

create index ix_calendar_item_sys_period on ttb.calendar_item using gist (sys_period) tablespace ttb_idx;
create index on hist.ttb_calendar_item using gist (sys_period) tablespace hist_idx;

--goto ... create view hist.v_calendar_item_hist as