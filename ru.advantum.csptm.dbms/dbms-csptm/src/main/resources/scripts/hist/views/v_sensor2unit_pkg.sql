
CREATE OR REPLACE VIEW hist.v_sensor2unit_hist AS
    SELECT * FROM core.sensor2unit
  UNION ALL
    SELECT * FROM hist.core_sensor2unit;

COMMENT ON VIEW hist.v_sensor2unit_hist IS 'История привязки  датчиков к блоку';
COMMENT ON COLUMN hist.v_sensor2unit_hist.sensor_id IS 'Датчик';
COMMENT ON COLUMN hist.v_sensor2unit_hist.unit_id IS 'Бортовой блок';
COMMENT ON COLUMN hist.v_sensor2unit_hist.sensor_num IS 'Номер датчика';
COMMENT ON COLUMN hist.v_sensor2unit_hist.sys_period IS 'Период действия';
