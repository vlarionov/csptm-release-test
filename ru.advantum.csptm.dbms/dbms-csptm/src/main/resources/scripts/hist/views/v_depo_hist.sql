﻿create or replace view hist.v_depo_hist as
select * from core.depo
union all
select * from hist.core_depo;

COMMENT ON VIEW hist.v_depo_hist IS 'История изменений Транспортное предприятие';
COMMENT ON COLUMN hist.v_depo_hist.depo_id IS 'Идентификатор Транспортное предприятие';
COMMENT ON COLUMN hist.v_depo_hist.tn_instance IS 'Код предприятия';
COMMENT ON COLUMN hist.v_depo_hist.carrier_id IS 'Идентификатор Транспортная организация';
COMMENT ON COLUMN hist.v_depo_hist.park_muid IS 'id парка из ГИС';
COMMENT ON COLUMN hist.v_depo_hist.tr_type_id IS 'Вид транспорта';
COMMENT ON COLUMN hist.v_depo_hist.wkt_geom IS 'Координаты';
COMMENT ON COLUMN hist.v_depo_hist.geom IS 'Геометрия';
COMMENT ON COLUMN hist.v_depo_hist.sign_deleted IS 'Удален';
COMMENT ON COLUMN hist.v_depo_hist.sys_period IS 'Период действия';
