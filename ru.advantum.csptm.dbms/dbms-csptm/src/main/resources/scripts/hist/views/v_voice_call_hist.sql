﻿
--drop view hist.v_voice_call_hist

create or replace view hist.v_voice_call_hist as
select * from voip.voice_call
union all
select * from hist.voip_voice_call;

COMMENT ON VIEW hist.v_voice_call_hist IS 'История изменений Голосовые вызовы';
COMMENT ON COLUMN hist.v_voice_call_hist.voice_call_id IS 'ID голосового вызова';
COMMENT ON COLUMN hist.v_voice_call_hist.account_id IS 'ID инициатора звонка';
COMMENT ON COLUMN hist.v_voice_call_hist.voice_call_date IS 'Дата голосового вызова';
COMMENT ON COLUMN hist.v_voice_call_hist.voice_call_status IS 'Статус голосового вызова';
COMMENT ON COLUMN hist.v_voice_call_hist.voice_call_begin IS 'Начало голосового вызова';
COMMENT ON COLUMN hist.v_voice_call_hist.voice_call_end IS 'Окончание голосового вызова';
COMMENT ON COLUMN hist.v_voice_call_hist.voice_call_file IS 'Аудио файл голосового вызова';
COMMENT ON COLUMN hist.v_voice_call_hist.sys_period IS 'Фиксация изменений';
COMMENT ON COLUMN hist.v_voice_call_hist.voice_call_redial_id IS 'ID статуса автодозвона';

--select * from hist.v_voice_call_hist limit 100
