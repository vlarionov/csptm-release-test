﻿create or replace view hist.v_territory_hist as
select * from core.territory
union all
select * from hist.core_territory;

COMMENT ON VIEW hist.v_territory_hist IS 'История изменений Территория';
COMMENT ON COLUMN hist.v_territory_hist.territory_id IS 'Идентификатор территории';
COMMENT ON COLUMN hist.v_territory_hist.code IS 'Номер территории';
COMMENT ON COLUMN hist.v_territory_hist.dt_begin IS 'Дата ввода в действие';
COMMENT ON COLUMN hist.v_territory_hist.dt_end IS 'Дата ликвидации';
COMMENT ON COLUMN hist.v_territory_hist.wkt_geom IS 'Координаты';
COMMENT ON COLUMN hist.v_territory_hist.geom IS 'Геометрия';
COMMENT ON COLUMN hist.v_territory_hist.sign_deleted IS 'удален';
COMMENT ON COLUMN hist.v_territory_hist.park_zone_muid IS 'территория парка из ГИС';
COMMENT ON COLUMN hist.v_territory_hist.sys_period IS 'Период действия';
