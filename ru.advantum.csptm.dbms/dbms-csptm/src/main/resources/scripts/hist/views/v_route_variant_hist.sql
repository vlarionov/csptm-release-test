﻿create or replace view hist.v_route_variant_hist as
select * from rts.route_variant
union all
select * from hist.rts_route_variant;

COMMENT ON VIEW hist.v_route_variant_hist IS 'История изменений Вариант маршрута';
COMMENT ON COLUMN hist.v_route_variant_hist.route_variant_id IS 'ID';
COMMENT ON COLUMN hist.v_route_variant_hist.route_id IS 'Маршрут';
COMMENT ON COLUMN hist.v_route_variant_hist.rv_status_id IS 'Статус';
COMMENT ON COLUMN hist.v_route_variant_hist.action_period IS 'Период действия варианта маршрута';
COMMENT ON COLUMN hist.v_route_variant_hist.sign_deleted IS 'Удален';
COMMENT ON COLUMN hist.v_route_variant_hist.sys_period IS 'Текущий период';
COMMENT ON COLUMN hist.v_route_variant_hist.rv_comment IS 'Комментарий';