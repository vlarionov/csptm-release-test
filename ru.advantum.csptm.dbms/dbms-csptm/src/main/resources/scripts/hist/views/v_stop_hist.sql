﻿create or replace view hist.v_stop_hist as
select * from rts.stop
union all
select * from hist.rts_stop;

COMMENT ON VIEW hist.v_stop_hist IS 'История изменений Остановочные пункты ОП';
COMMENT ON COLUMN hist.v_stop_hist.stop_id IS 'ОП';
COMMENT ON COLUMN hist.v_stop_hist.name IS 'Наименование';
COMMENT ON COLUMN hist.v_stop_hist.name_in_english IS 'Наименование на английском';
COMMENT ON COLUMN hist.v_stop_hist.sys_period IS 'Период действия';
COMMENT ON COLUMN hist.v_stop_hist.sign_deleted IS 'Удален';
