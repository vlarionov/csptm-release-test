﻿create or replace view hist.v_gas_station2zone_hist as
select * from rts.gas_station2zone
union all
select * from hist.rts_gas_station2zone;

COMMENT ON VIEW hist.v_gas_station2zone_hist IS 'История изменений Зоны заправок';
COMMENT ON COLUMN hist.v_gas_station2zone_hist.gas_station2zone_id IS 'ID';
COMMENT ON COLUMN hist.v_gas_station2zone_hist.zone_location_id IS 'Геометрия объекта';
COMMENT ON COLUMN hist.v_gas_station2zone_hist.gas_station_id IS 'АЗС';
COMMENT ON COLUMN hist.v_gas_station2zone_hist.sys_period IS 'Период действия';