﻿create or replace view hist.v_depo2route_hist as
select * from rts.depo2route
union all
select * from hist.rts_depo2route;

COMMENT ON VIEW hist.v_depo2route_hist IS 'Связь парков и маршрутов';
COMMENT ON COLUMN hist.v_depo2route_hist.depo2route_id IS 'Связь парков и маршрутов';
COMMENT ON COLUMN hist.v_depo2route_hist.route_id IS 'Маршрут';
COMMENT ON COLUMN hist.v_depo2route_hist.depo_id IS 'Парк';
COMMENT ON COLUMN hist.v_depo2route_hist.sign_deleted IS 'Удален';
COMMENT ON COLUMN hist.v_depo2route_hist.sys_period IS 'Период действия';
