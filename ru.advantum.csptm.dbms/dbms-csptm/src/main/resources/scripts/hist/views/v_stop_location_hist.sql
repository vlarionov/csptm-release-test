﻿create or replace view hist.v_stop_location_hist as
select * from rts.stop_location
union all
select * from hist.rts_stop_location;

COMMENT ON VIEW hist.v_stop_location_hist IS 'История изменений Месторасположение ОП';
COMMENT ON COLUMN hist.v_stop_location_hist.stop_location_id IS 'Месторасположение';
COMMENT ON COLUMN hist.v_stop_location_hist.graph_section_id IS 'Дуга графа';
COMMENT ON COLUMN hist.v_stop_location_hist.geom IS 'Геометрия';
COMMENT ON COLUMN hist.v_stop_location_hist.wkt_geom IS 'Координаты';
COMMENT ON COLUMN hist.v_stop_location_hist.graph_section_offset IS 'Длина от начала дуги графа';
COMMENT ON COLUMN hist.v_stop_location_hist.building_distance IS 'Расстояние до ближайшего строения';
COMMENT ON COLUMN hist.v_stop_location_hist.building_address IS 'Адрес ближайшего строения';
COMMENT ON COLUMN hist.v_stop_location_hist.sign_deleted IS 'Удалено';
COMMENT ON COLUMN hist.v_stop_location_hist.zone_location_id IS 'ID';
COMMENT ON COLUMN hist.v_stop_location_hist.sys_period IS 'Период действия';
