﻿
create or replace view hist.v_gas_station_hist as
select * from rts.gas_station
union all
select * from hist.rts_gas_station;

COMMENT ON VIEW hist.v_gas_station_hist IS 'АЗС';
COMMENT ON COLUMN hist.v_gas_station_hist.gas_station_id IS 'ID';
COMMENT ON COLUMN hist.v_gas_station_hist.time_fueling IS 'Время на заправку';
COMMENT ON COLUMN hist.v_gas_station_hist.sys_period IS 'период действия';
