﻿create or replace view hist.v_territory2zone_hist as
select * from rts.territory2zone
union all
select * from hist.rts_territory2zone;

COMMENT ON VIEW hist.v_territory2zone_hist IS 'История изменений Зоны территорий';
COMMENT ON COLUMN hist.v_territory2zone_hist.territory2zone_id IS 'ID';
COMMENT ON COLUMN hist.v_territory2zone_hist.zone_location_id IS 'Геометрия объекта';
COMMENT ON COLUMN hist.v_territory2zone_hist.territory_id IS 'Территория ТП';
COMMENT ON COLUMN hist.v_territory2zone_hist.sys_period IS 'Период действия';
