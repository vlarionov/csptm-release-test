﻿create or replace view hist.v_auto_station2zone_hist as
select * from rts.auto_station2zone
union all
select * from hist.rts_auto_station2zone;

COMMENT ON VIEW hist.v_auto_station2zone_hist IS 'Зоны автостанции';
COMMENT ON COLUMN hist.v_auto_station2zone_hist.auto_station2zone_id IS 'ID';
COMMENT ON COLUMN hist.v_auto_station2zone_hist.zone_location_id IS 'Геометрия объекта';
COMMENT ON COLUMN hist.v_auto_station2zone_hist.auto_station_id IS 'Автостанция';
COMMENT ON COLUMN hist.v_auto_station2zone_hist.sys_period IS 'Период действия';
