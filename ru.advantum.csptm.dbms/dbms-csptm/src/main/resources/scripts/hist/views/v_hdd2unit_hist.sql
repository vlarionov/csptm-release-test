
CREATE OR REPLACE VIEW hist.v_hdd2unit_hist AS
    SELECT * FROM core.hdd2unit
  UNION ALL
    SELECT * FROM hist.core_hdd2unit;

COMMENT ON VIEW hist.v_hdd2unit_hist IS 'История привязки жесткого диска к блоку';
COMMENT ON COLUMN hist.v_hdd2unit_hist.unit_id IS 'Блок';
COMMENT ON COLUMN hist.v_hdd2unit_hist.hdd_id IS 'Жесткий диск';
COMMENT ON COLUMN hist.v_hdd2unit_hist.sys_period IS 'Период действия';
