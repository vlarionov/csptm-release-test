﻿
create or replace view hist.v_tt_action_round_hist as
select * from ttb.tt_action_round
union all
select * from hist.ttb_tt_action_round;

COMMENT ON VIEW hist.v_tt_action_round_hist IS 'История изменений Действия - рейсы';
COMMENT ON COLUMN hist.v_tt_action_round_hist.tt_action_id IS 'Действие';
COMMENT ON COLUMN hist.v_tt_action_round_hist.round_id IS 'рейс';
COMMENT ON COLUMN hist.v_tt_action_round_hist.round_status_id IS 'Статус рейса (2-запланирован)';
COMMENT ON COLUMN hist.v_tt_action_round_hist.time_packet_begin IS 'Время первого привязанного пакета к рейсу';
COMMENT ON COLUMN hist.v_tt_action_round_hist.packet_id_begin IS 'ID первого привязанного пакета к рейсу';
COMMENT ON COLUMN hist.v_tt_action_round_hist.time_packet_end IS 'Время последнего привязанного пакета к рейсу';
COMMENT ON COLUMN hist.v_tt_action_round_hist.packet_id_end IS 'ID последнего привязанного пакета к рейсу';
COMMENT ON COLUMN hist.v_tt_action_round_hist.first_graph_section IS 'Время прохождения первой секции графа';
COMMENT ON COLUMN hist.v_tt_action_round_hist.last_graph_section IS 'Время прохождения последней секции графа';
COMMENT ON COLUMN hist.v_tt_action_round_hist.time_fact_begin IS 'Фактическое время прохождения первого ОП';
COMMENT ON COLUMN hist.v_tt_action_round_hist.time_fact_end IS 'Фактическое время  прохождения последнего ОП';
