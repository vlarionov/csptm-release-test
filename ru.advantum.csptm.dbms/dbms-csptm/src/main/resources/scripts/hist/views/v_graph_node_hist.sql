﻿create or replace view hist.v_graph_node_hist as
select * from rts.graph_node
union all
select * from hist.rts_graph_node;

COMMENT ON VIEW hist.v_graph_node_hist IS 'История изменений Узлы графа';
COMMENT ON COLUMN hist.v_graph_node_hist.graph_node_id IS 'ID';
COMMENT ON COLUMN hist.v_graph_node_hist.wkt_geom IS 'Координаты';
COMMENT ON COLUMN hist.v_graph_node_hist.geom IS 'Геометрия';
COMMENT ON COLUMN hist.v_graph_node_hist.sys_period IS 'период действия';
COMMENT ON COLUMN hist.v_graph_node_hist.sign_deleted IS 'Удален';
