﻿create or replace view hist.v_stop_item2round_type_hist as
select * from rts.stop_item2round_type
union all
select * from hist.rts_stop_item2round_type;

COMMENT ON VIEW hist.v_stop_item2round_type_hist IS 'История изменений Типы остановочных пунктов для остановок в рейсе';
COMMENT ON COLUMN hist.v_stop_item2round_type_hist.stop_item2round_type_id IS 'ID';
COMMENT ON COLUMN hist.v_stop_item2round_type_hist.stop_item_type_id IS 'Тип ОП';
COMMENT ON COLUMN hist.v_stop_item2round_type_hist.stop_item2round_id IS 'ОП на траектории';
COMMENT ON COLUMN hist.v_stop_item2round_type_hist.sys_period IS 'Текущий период';
COMMENT ON COLUMN hist.v_stop_item2round_type_hist.sign_deleted IS 'Удален';
