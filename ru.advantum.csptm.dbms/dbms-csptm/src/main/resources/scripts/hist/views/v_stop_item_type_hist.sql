﻿create or replace view hist.v_stop_item_type_hist as
select * from rts.stop_item_type
union all
select * from hist.rts_stop_item_type;

COMMENT ON VIEW hist.v_stop_item_type_hist IS 'Типы остановочных пунктов';
COMMENT ON COLUMN hist.v_stop_item_type_hist.stop_item_type_id IS 'ID';
COMMENT ON COLUMN hist.v_stop_item_type_hist.stop_item_id IS 'Классификатор ОП';
COMMENT ON COLUMN hist.v_stop_item_type_hist.stop_type_id IS 'Тип ОП';
COMMENT ON COLUMN hist.v_stop_item_type_hist.sys_period IS 'Период действия';
COMMENT ON COLUMN hist.v_stop_item_type_hist.sign_deleted IS 'Удален';
