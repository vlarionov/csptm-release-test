﻿-- drop view hist.v_stop_item_hist

create or replace view hist.v_stop_item_hist as
select * from rts.stop_item
union all
select * from hist.rts_stop_item;

COMMENT ON VIEW hist.v_stop_item_hist IS 'История изменений Классификация остановок';
COMMENT ON COLUMN hist.v_stop_item_hist.stop_item_id IS 'Классификация остановок';
COMMENT ON COLUMN hist.v_stop_item_hist.tr_type_id IS 'Вид ТС';
COMMENT ON COLUMN hist.v_stop_item_hist.sys_period IS 'Период действия';
COMMENT ON COLUMN hist.v_stop_item_hist.sign_deleted IS 'Удален';
COMMENT ON COLUMN hist.v_stop_item_hist.stop_location_id IS 'Месторасположение ОП';
