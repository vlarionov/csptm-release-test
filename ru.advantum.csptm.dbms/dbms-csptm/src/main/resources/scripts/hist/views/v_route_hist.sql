﻿create or replace view hist.v_route_hist as
select * from rts.route
union all
select * from hist.rts_route;

COMMENT ON VIEW hist.v_route_hist IS 'История изменений Маршруты';
COMMENT ON COLUMN hist.v_route_hist.route_id IS 'Маршрут';
COMMENT ON COLUMN hist.v_route_hist.tr_type_id IS 'Вид ТС';
COMMENT ON COLUMN hist.v_route_hist.carrier_id IS 'Перевозчик';
COMMENT ON COLUMN hist.v_route_hist.current_route_variant_id IS 'Текущий вариант маршрута';
COMMENT ON COLUMN hist.v_route_hist.route_num IS 'Номер маршрута';
COMMENT ON COLUMN hist.v_route_hist.comment IS 'Комментарий';
COMMENT ON COLUMN hist.v_route_hist.sign_deleted IS 'Удален';
COMMENT ON COLUMN hist.v_route_hist.sys_period IS 'Текущий период';
