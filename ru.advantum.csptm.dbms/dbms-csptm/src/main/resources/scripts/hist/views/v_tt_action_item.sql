﻿
create or replace view hist.v_tt_action_item_hist as
select * from ttb.tt_action_item
union all
select * from hist.ttb_tt_action_item;

COMMENT ON VIEW hist.v_tt_action_item_hist IS 'История изменений Детали выполнения действий';
COMMENT ON COLUMN hist.v_tt_action_item_hist.tt_action_item_id IS 'ID';
COMMENT ON COLUMN hist.v_tt_action_item_hist.tt_action_id IS 'Выполняемое действие';
COMMENT ON COLUMN hist.v_tt_action_item_hist.time_begin IS 'Время начала действия';
COMMENT ON COLUMN hist.v_tt_action_item_hist.time_end IS 'Время окончания действия';
COMMENT ON COLUMN hist.v_tt_action_item_hist.stop_item2round_id IS 'ОП, на котором происходит действие';
COMMENT ON COLUMN hist.v_tt_action_item_hist.action_type_id IS 'Тип действия';
COMMENT ON COLUMN hist.v_tt_action_item_hist.sign_deleted IS 'Удален';
COMMENT ON COLUMN hist.v_tt_action_item_hist.sys_period IS 'Период действия';
COMMENT ON COLUMN hist.v_tt_action_item_hist.dr_shift_id IS 'Смена';
COMMENT ON COLUMN hist.v_tt_action_item_hist.driver_id IS 'Водитель';
