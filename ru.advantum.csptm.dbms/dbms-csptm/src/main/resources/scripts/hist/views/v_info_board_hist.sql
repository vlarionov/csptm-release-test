﻿create or replace view hist.v_info_board_hist as
select * from ibrd.info_board
union all
select * from hist.ibrd_info_board;

COMMENT ON VIEW hist.v_info_board_hist IS 'История изменений ИТОП';
COMMENT ON COLUMN hist.v_info_board_hist.info_board_id IS 'ID';
COMMENT ON COLUMN hist.v_info_board_hist.code IS 'Код';
COMMENT ON COLUMN hist.v_info_board_hist.model_id IS 'Модель';
COMMENT ON COLUMN hist.v_info_board_hist.serial_number IS 'Серийный номер';
COMMENT ON COLUMN hist.v_info_board_hist.address_program_id IS 'Адресная программа';
COMMENT ON COLUMN hist.v_info_board_hist.installation_place_id IS 'Место установки';
COMMENT ON COLUMN hist.v_info_board_hist.address IS 'Адрес';
COMMENT ON COLUMN hist.v_info_board_hist.has_stop_pavilion IS 'Флаг наличия павилиона ожидания';
COMMENT ON COLUMN hist.v_info_board_hist.imei IS 'IMEI';
COMMENT ON COLUMN hist.v_info_board_hist.mobile_operator_id IS 'Оператор сотовой связи';
COMMENT ON COLUMN hist.v_info_board_hist.mobile_number IS 'Номер телефона';
COMMENT ON COLUMN hist.v_info_board_hist.balance_organization_id IS 'Балансодержатель';
COMMENT ON COLUMN hist.v_info_board_hist.inventory_number IS 'Инвентарный номер';
COMMENT ON COLUMN hist.v_info_board_hist.start_date IS 'Дата ввода в эксплуатацию';
COMMENT ON COLUMN hist.v_info_board_hist.useful_duration IS 'Срок полезного использования';
COMMENT ON COLUMN hist.v_info_board_hist.end_date IS 'Дата окончания срока полезного использования';
COMMENT ON COLUMN hist.v_info_board_hist.state_id IS 'Статус';
COMMENT ON COLUMN hist.v_info_board_hist.state_end_date IS 'Дата окончания статуса';
COMMENT ON COLUMN hist.v_info_board_hist.service_contract_id IS 'Договор технического сопровождения';
COMMENT ON COLUMN hist.v_info_board_hist.is_deleted IS 'Признак удаления';
COMMENT ON COLUMN hist.v_info_board_hist.sys_period IS 'Текущий период';
