CREATE OR REPLACE VIEW hist.v_unit2tr_hist AS
    SELECT * FROM core.unit2tr
  UNION ALL
    SELECT * FROM hist.core_unit2tr;

COMMENT ON VIEW hist.v_unit2tr_hist IS 'История блоков в транспортном средстве';
COMMENT ON COLUMN hist.v_unit2tr_hist.tr_id IS 'ТС';
COMMENT ON COLUMN hist.v_unit2tr_hist.unit_id IS 'Блок';
COMMENT ON COLUMN hist.v_unit2tr_hist.sys_period IS 'Период действия';
