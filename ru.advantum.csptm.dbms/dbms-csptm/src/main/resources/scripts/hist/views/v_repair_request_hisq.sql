
CREATE OR REPLACE VIEW hist.v_repair_request_hist AS
  SELECT * FROM core.repair_request
  UNION ALL
  SELECT * FROM hist.core_repair_request;


comment on view hist.v_repair_request_hist is 'Заявка на ремонт БО'
;

comment on column hist.v_repair_request_hist.repair_request_id is 'Заявка на ремонт БО'
;

comment on column hist.v_repair_request_hist.dt is 'Дата создания'
;

comment on column hist.v_repair_request_hist.description is 'Описание поломки'
;

comment on column hist.v_repair_request_hist.dt_closed is 'Дата закрытия'
;

comment on column hist.v_repair_request_hist.repair_request_status_id is 'Статус заявки'
;

comment on column hist.v_repair_request_hist.breakdown_type_id is 'Тип поломки'
;

comment on column hist.v_repair_request_hist.close_result_id is 'Результат закрытия'
;

comment on column hist.v_repair_request_hist.territory_id is 'Территория'
;

comment on column hist.v_repair_request_hist.facility_id is 'Подрядчик'
;

comment on column hist.v_repair_request_hist.breakdown_id is 'Запись о поломке'
;

comment on column hist.v_repair_request_hist.num is 'Номер завки на ремонт'
;

comment on column hist.v_repair_request_hist.equipment_id is 'Оборудование'
;

comment on column hist.v_repair_request_hist.replacement_equipment_id is 'Оборудование на замену'
;

