CREATE OR REPLACE VIEW hist.v_monitor2unit_hist AS
    SELECT * FROM core.monitor2unit
  UNION ALL
    SELECT * FROM hist.core_monitor2unit;

COMMENT ON VIEW hist.v_monitor2unit_hist IS 'История привязки монитора к блоку';
COMMENT ON COLUMN hist.v_monitor2unit_hist.unit_id IS 'Блок';
COMMENT ON COLUMN hist.v_monitor2unit_hist.monitor_id IS 'Монитор';
COMMENT ON COLUMN hist.v_monitor2unit_hist.sys_period IS 'Период действия';
