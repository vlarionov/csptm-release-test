﻿create or replace view hist.v_auto_station_hist as
select * from rts.auto_station
union all
select * from hist.rts_auto_station;

COMMENT ON VIEW hist.v_auto_station_hist IS 'Автостанции';
COMMENT ON COLUMN hist.v_auto_station_hist.auto_station_id IS 'Автостанции';
COMMENT ON COLUMN hist.v_auto_station_hist.sys_period IS 'Период действия';
