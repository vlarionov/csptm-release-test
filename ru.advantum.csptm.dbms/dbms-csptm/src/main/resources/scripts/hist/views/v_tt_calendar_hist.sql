﻿
create or replace view hist.v_calendar_item_hist as
select * from ttb.calendar_item
union all
select * from hist.ttb_calendar_item;

COMMENT ON VIEW hist.v_calendar_item_hist IS 'Детализированный календарь';
COMMENT ON TABLE ttb.calendar_item IS 'Детализированный календарь';
COMMENT ON COLUMN hist.v_calendar_item_hist.calendar_item_id IS 'id';
COMMENT ON COLUMN hist.v_calendar_item_hist.calendar_id IS 'Дата';
COMMENT ON COLUMN hist.v_calendar_item_hist.calendar_tag_id IS 'Тег';
COMMENT ON COLUMN hist.v_calendar_item_hist.is_fixed IS 'уникальный на дату (нельзя править)';
COMMENT ON COLUMN hist.v_calendar_item_hist.sys_period IS 'Текущий период';
