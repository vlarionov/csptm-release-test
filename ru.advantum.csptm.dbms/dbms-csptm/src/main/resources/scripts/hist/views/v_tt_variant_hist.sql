﻿
create or replace view hist.v_tt_variant_hist as
select * from ttb.tt_variant
union all
select * from hist.ttb_tt_variant;

COMMENT ON VIEW hist.v_tt_variant_hist IS 'Варианты расписания';
COMMENT ON COLUMN hist.v_tt_variant_hist.tt_variant_id IS 'ID';
COMMENT ON COLUMN hist.v_tt_variant_hist.norm_id IS 'Норматив';
COMMENT ON COLUMN hist.v_tt_variant_hist.route_variant_id IS 'Вариант маршрута';
COMMENT ON COLUMN hist.v_tt_variant_hist.is_all_tr_online IS 'Выпустить все ТС на линию в часы пик';
COMMENT ON COLUMN hist.v_tt_variant_hist.sign_deleted IS 'Удален';
COMMENT ON COLUMN hist.v_tt_variant_hist.sys_period IS 'Текущий период';
COMMENT ON COLUMN hist.v_tt_variant_hist.tt_status_id IS 'Статус';
COMMENT ON COLUMN hist.v_tt_variant_hist.action_period IS 'Период действия';
COMMENT ON COLUMN hist.v_tt_variant_hist.ttv_name IS 'Номер варианта расписания для пользователей';
COMMENT ON COLUMN hist.v_tt_variant_hist.ttv_comment IS 'Комментарий';
COMMENT ON COLUMN hist.v_tt_variant_hist.is_calc_indicator IS 'Используется для расчета эксплуатационных показателей';
COMMENT ON COLUMN hist.v_tt_variant_hist.parent_tt_variant_id IS 'Ссылка на плановое расписание для оперативного';
COMMENT ON COLUMN hist.v_tt_variant_hist.order_date IS 'Дата оперативного расписания(наряда)';
comment on column hist.v_tt_variant_hist.sys_period is 'Период действия';
