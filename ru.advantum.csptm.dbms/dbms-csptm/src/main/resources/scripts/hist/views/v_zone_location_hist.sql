﻿create or replace view hist.v_zone_location_hist as
select * from rts.zone_location
union all
select * from hist.rts_zone_location;

COMMENT ON VIEW hist.v_zone_location_hist IS 'Геометрия объектов';
COMMENT ON COLUMN hist.v_zone_location_hist.zone_location_id IS 'ID';
COMMENT ON COLUMN hist.v_zone_location_hist.wkt_geom IS 'Координаты';
COMMENT ON COLUMN hist.v_zone_location_hist.geom IS 'Геометрия';
COMMENT ON COLUMN hist.v_zone_location_hist.sys_period IS 'Период действия';
COMMENT ON COLUMN hist.v_zone_location_hist.sign_deleted IS 'Удален';
