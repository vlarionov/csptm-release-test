
CREATE OR REPLACE VIEW hist.v_norm_hist AS
    SELECT * FROM ttb.norm
  UNION ALL
    SELECT * FROM hist.ttb_norm;

COMMENT ON VIEW hist.v_norm_hist IS 'История изменения нормативов';
COMMENT ON COLUMN hist.v_norm_hist.norm_id IS 'ID';
COMMENT ON COLUMN hist.v_norm_hist.calculation_task_id IS 'Исходный расчет';
COMMENT ON COLUMN hist.v_norm_hist.norm_name IS 'Наименование ';
COMMENT ON COLUMN hist.v_norm_hist.is_current IS 'Норматив может использоваться при расчете расписания';
COMMENT ON COLUMN hist.v_norm_hist.hour_period IS 'Временные периоды для расчета, мин';
COMMENT ON COLUMN hist.v_norm_hist.dt_begin IS 'Дата начала периода';
COMMENT ON COLUMN hist.v_norm_hist.dt_end IS 'Дата окончания периода';
COMMENT ON COLUMN hist.v_norm_hist.sign_deleted IS 'Удален';
COMMENT ON COLUMN hist.v_norm_hist.sys_period IS 'Текущий период';
COMMENT ON COLUMN hist.v_norm_hist.norm_desc IS 'Комментарий к нормативу';
