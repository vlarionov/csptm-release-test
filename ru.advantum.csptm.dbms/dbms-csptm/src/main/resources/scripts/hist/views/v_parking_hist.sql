﻿create or replace view hist.v_parking_hist as
select * from rts.parking
union all
select * from hist.rts_parking;

COMMENT ON VIEW hist.v_parking_hist IS 'История изменений Отстойно-разворотная площадка';
COMMENT ON COLUMN hist.v_parking_hist.parking_id IS 'Id';
COMMENT ON COLUMN hist.v_parking_hist.square IS 'Площадь, м';
COMMENT ON COLUMN hist.v_parking_hist.tr_count IS 'Вместимость площадки';
COMMENT ON COLUMN hist.v_parking_hist.sys_period IS 'Период действия';
COMMENT ON COLUMN hist.v_parking_hist.sign_deleted IS 'Удалено';
