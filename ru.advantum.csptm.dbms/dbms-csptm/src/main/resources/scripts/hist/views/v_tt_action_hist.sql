﻿
create or replace view hist.v_tt_action_hist as
select * from ttb.tt_action
union all
select * from hist.ttb_tt_action;

COMMENT ON VIEW hist.v_tt_action_hist IS 'История изменений Действия (стоянки/рейсы)';
COMMENT ON COLUMN hist.v_tt_action_hist.tt_action_id IS 'Действия';
COMMENT ON COLUMN hist.v_tt_action_hist.tt_out_id IS 'Выход';
COMMENT ON COLUMN hist.v_tt_action_hist.action_dur IS 'Продолжительность, сек';
COMMENT ON COLUMN hist.v_tt_action_hist.dt_action IS 'Дата действия';
COMMENT ON COLUMN hist.v_tt_action_hist.sys_period IS 'Период действия';
COMMENT ON COLUMN hist.v_tt_action_hist.action_type_id IS 'Тип действия';
COMMENT ON COLUMN hist.v_tt_action_hist.sign_deleted IS 'Удален';
COMMENT ON COLUMN hist.v_tt_action_hist.tt_action_group_id IS 'Группа действий';
COMMENT ON COLUMN hist.v_tt_action_hist.action_gr_id IS 'Группирующий признак для действия';
COMMENT ON COLUMN hist.v_tt_action_hist.parent_tt_action_id IS 'ссылка на действие планового расписания';
COMMENT ON COLUMN hist.v_tt_action_hist.driver_id IS 'Водитель, начавший рейс';
COMMENT ON COLUMN hist.v_tt_action_hist.action_range IS 'Период действия ';
