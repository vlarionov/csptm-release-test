﻿create or replace view hist.v_stop_group_hist as
select * from rts.stop_group
union all
select * from hist.rts_stop_group;

COMMENT ON VIEW hist.v_stop_group_hist IS 'История изменений Группа ОП';
COMMENT ON COLUMN hist.v_stop_group_hist.stop_group_id IS 'Группа ОП';
COMMENT ON COLUMN hist.v_stop_group_hist.stop_group_name IS 'Наименование группы';
COMMENT ON COLUMN hist.v_stop_group_hist.stop_group_desc IS 'Описание группы';
COMMENT ON COLUMN hist.v_stop_group_hist.sign_deleted IS 'Удален';
COMMENT ON COLUMN hist.v_stop_group_hist.sys_period IS 'Период действия';
