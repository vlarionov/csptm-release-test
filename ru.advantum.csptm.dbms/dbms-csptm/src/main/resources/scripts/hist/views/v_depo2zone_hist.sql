﻿create or replace view hist.v_depo2zone_hist as
select * from rts.depo2zone
union all
select * from hist.rts_depo2zone;

COMMENT ON VIEW hist.v_depo2zone_hist IS 'Зоны парка';
COMMENT ON COLUMN hist.v_depo2zone_hist.depo2zone_id IS 'Id';
COMMENT ON COLUMN hist.v_depo2zone_hist.zone_location_id IS 'Геометрия объекта';
COMMENT ON COLUMN hist.v_depo2zone_hist.depo_id IS 'ТП';
COMMENT ON COLUMN hist.v_depo2zone_hist.sys_period IS 'Период действия';
