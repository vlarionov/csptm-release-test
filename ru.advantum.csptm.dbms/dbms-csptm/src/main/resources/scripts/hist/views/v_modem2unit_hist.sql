
CREATE OR REPLACE VIEW hist.v_modem2unit_hist AS
    SELECT * FROM core.modem2unit
  UNION ALL
    SELECT * FROM hist.core_modem2unit;

COMMENT ON VIEW hist.v_modem2unit_hist IS 'История привязки модема к блоку';
COMMENT ON COLUMN hist.v_modem2unit_hist.unit_id IS 'Блок';
COMMENT ON COLUMN hist.v_modem2unit_hist.modem_id IS 'Модем';
COMMENT ON COLUMN hist.v_modem2unit_hist.sys_period IS 'Период действия';
