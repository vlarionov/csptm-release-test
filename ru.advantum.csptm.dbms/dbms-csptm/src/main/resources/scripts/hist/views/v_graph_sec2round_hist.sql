﻿create or replace view hist.v_graph_sec2round_hist as
select * from rts.graph_sec2round
union all
select * from hist.rts_graph_sec2round;

COMMENT ON VIEW hist.v_graph_sec2round_hist IS 'История изменений Связь секций графа с рейсом';
COMMENT ON COLUMN hist.v_graph_sec2round_hist.graph_sec2round_id IS 'ID';
COMMENT ON COLUMN hist.v_graph_sec2round_hist.round_id IS 'Рейс';
COMMENT ON COLUMN hist.v_graph_sec2round_hist.graph_section_id IS 'Секция графа';
COMMENT ON COLUMN hist.v_graph_sec2round_hist.order_num IS 'Порядковый номер дуги графа в траектории маршрута';
COMMENT ON COLUMN hist.v_graph_sec2round_hist.sign_deleted IS 'Удален';
COMMENT ON COLUMN hist.v_graph_sec2round_hist.sys_period IS 'Текущий период';
