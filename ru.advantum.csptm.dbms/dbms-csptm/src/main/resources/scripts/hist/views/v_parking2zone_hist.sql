﻿create or replace view hist.v_parking2zone_hist as
select * from rts.parking2zone
union all
select * from hist.rts_parking2zone;

COMMENT ON VIEW hist.v_parking2zone_hist IS 'Зоны парковки';
COMMENT ON COLUMN hist.v_parking2zone_hist.parking2zone_id IS 'Id';
COMMENT ON COLUMN hist.v_parking2zone_hist.zone_location_id IS 'Геометрия объекта';
COMMENT ON COLUMN hist.v_parking2zone_hist.parking_id IS 'Отстойно-разворотная площадка';
COMMENT ON COLUMN hist.v_parking2zone_hist.sys_period IS 'Период действия';
