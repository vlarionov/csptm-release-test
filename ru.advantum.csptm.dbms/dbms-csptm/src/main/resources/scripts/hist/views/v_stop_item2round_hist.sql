﻿create or replace view hist.v_stop_item2round_hist as
select * from rts.stop_item2round
union all
select * from hist.rts_stop_item2round;

COMMENT ON VIEW hist.v_stop_item2round_hist IS 'История изменений Остановки в рейсе';
COMMENT ON COLUMN hist.v_stop_item2round_hist.stop_item2round_id IS 'ID';
COMMENT ON COLUMN hist.v_stop_item2round_hist.round_id IS 'Рейс';
COMMENT ON COLUMN hist.v_stop_item2round_hist.stop_item_id IS 'Классификатор ОП';
COMMENT ON COLUMN hist.v_stop_item2round_hist.length_sector IS 'Длина участка между остановками, м (от предыдущей до данной)';
COMMENT ON COLUMN hist.v_stop_item2round_hist.order_num IS 'Порядковый номер дуги графа в траектории маршрута';
COMMENT ON COLUMN hist.v_stop_item2round_hist.sys_period IS 'Текущий период';
COMMENT ON COLUMN hist.v_stop_item2round_hist.sign_deleted IS 'Удален';
COMMENT ON COLUMN hist.v_stop_item2round_hist.comment IS 'Комментарий';
