CREATE OR REPLACE VIEW hist.v_tr_hist AS
    SELECT * FROM core.tr
  UNION ALL
    SELECT * FROM hist.core_tr;

COMMENT ON VIEW hist.v_tr_hist IS 'История изменений ТС';
COMMENT ON COLUMN hist.v_tr_hist.tr_id IS 'ID';
COMMENT ON COLUMN hist.v_tr_hist.seat_qty IS 'Кол-во сидячих мест';
COMMENT ON COLUMN hist.v_tr_hist.has_low_floor IS 'Признак низкопольности';
COMMENT ON COLUMN hist.v_tr_hist.has_conditioner IS 'Наличие кондиционера';
COMMENT ON COLUMN hist.v_tr_hist.garage_num IS 'Гаражный номер';
COMMENT ON COLUMN hist.v_tr_hist.licence IS 'Государственный номер';
COMMENT ON COLUMN hist.v_tr_hist.serial_num IS 'Заводской (серийный) номер';
COMMENT ON COLUMN hist.v_tr_hist.dt_begin IS 'Дата ввода в эксплуатацию';
COMMENT ON COLUMN hist.v_tr_hist.dt_end IS 'Дата списания';
COMMENT ON COLUMN hist.v_tr_hist.tr_type_id IS 'Вид ТС';
COMMENT ON COLUMN hist.v_tr_hist.tr_status_id IS 'Статус ТС';
COMMENT ON COLUMN hist.v_tr_hist.tr_model_id IS 'Модель ТС';
COMMENT ON COLUMN hist.v_tr_hist.territory_id IS 'Территории';
COMMENT ON COLUMN hist.v_tr_hist.park_instance_id IS 'ID парка в АСДУ';
COMMENT ON COLUMN hist.v_tr_hist.has_wheelchair_space IS 'признак наличия площадки (и/или специальных креплений) для инвалидной коляски';
COMMENT ON COLUMN hist.v_tr_hist.has_bicicle_space IS 'признак наличия площадки для велосипеда';
COMMENT ON COLUMN hist.v_tr_hist.seat_qty_disabled_people IS 'количество сидячих мест для инвалидов';
COMMENT ON COLUMN hist.v_tr_hist.seat_qty_total IS 'общее число место мест в ТС';
COMMENT ON COLUMN hist.v_tr_hist.depo_id IS 'Транспортное предприятие';
COMMENT ON COLUMN hist.v_tr_hist.year_of_issue IS 'Год выпуска';
COMMENT ON COLUMN hist.v_tr_hist.garage_num_add IS 'Дополнительный гаражный номер';
COMMENT ON COLUMN hist.v_tr_hist.equipment_afixed IS 'Закрепленное за ТС оборудование';
COMMENT ON COLUMN hist.v_tr_hist.tr_schema_install_id IS 'Схема подключения БО';
COMMENT ON COLUMN hist.v_tr_hist.sys_period IS 'Фиксация изменений';

