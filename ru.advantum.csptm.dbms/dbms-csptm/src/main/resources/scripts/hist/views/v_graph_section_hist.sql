﻿create or replace view hist.v_graph_section_hist as
select * from rts.graph_section
union all
select * from hist.rts_graph_section;

COMMENT ON VIEW hist.v_graph_section_hist IS 'История изменений Дуга графа';
COMMENT ON COLUMN hist.v_graph_section_hist.graph_section_id IS 'Дуга графа';
COMMENT ON COLUMN hist.v_graph_section_hist.geom IS 'Геометрия';
COMMENT ON COLUMN hist.v_graph_section_hist.wkt_geom IS 'Координаты';
COMMENT ON COLUMN hist.v_graph_section_hist.length IS 'Длина дуги в метрах';
COMMENT ON COLUMN hist.v_graph_section_hist.node_begin_id IS 'Начало дуги';
COMMENT ON COLUMN hist.v_graph_section_hist.node_end_id IS 'Конец дуги';
COMMENT ON COLUMN hist.v_graph_section_hist.sys_period IS 'Период действия';
COMMENT ON COLUMN hist.v_graph_section_hist.sign_deleted IS 'Удален';
