﻿create or replace view hist.v_entity_hist as
select * from core.entity
union all
select * from hist.core_entity;

COMMENT ON VIEW hist.v_entity_hist IS 'Идентификатор реквизитов';
COMMENT ON COLUMN hist.v_entity_hist.entity_id IS 'Идентификатор сущности';
COMMENT ON COLUMN hist.v_entity_hist.name_full IS 'Наименование полное';
COMMENT ON COLUMN hist.v_entity_hist.name_short IS 'Наименование сокращенное';
COMMENT ON COLUMN hist.v_entity_hist.comment IS 'Дополнительно';
COMMENT ON COLUMN hist.v_entity_hist.sys_period IS 'Период действия';
