﻿create or replace view hist.v_stop2stop_group_hist as
select * from rts.stop2stop_group
union all
select * from hist.rts_stop2stop_group;

COMMENT ON VIEW hist.v_stop2stop_group_hist IS 'Группы для остановок';
COMMENT ON COLUMN hist.v_stop2stop_group_hist.stop2stop_group_id IS 'ID';
COMMENT ON COLUMN hist.v_stop2stop_group_hist.stop_group_id IS 'Группа ОП';
COMMENT ON COLUMN hist.v_stop2stop_group_hist.stop_id IS 'ОП';
COMMENT ON COLUMN hist.v_stop2stop_group_hist.sys_period IS 'Период действия';
