﻿create or replace view hist.v_round_hist as
select * from rts.round
union all
select * from hist.rts_round;

COMMENT ON VIEW hist.v_round_hist IS 'История изменений Траектории';
COMMENT ON COLUMN hist.v_round_hist.round_id IS 'Рейс';
COMMENT ON COLUMN hist.v_round_hist.move_direction_id IS 'Направление движения';
COMMENT ON COLUMN hist.v_round_hist.route_variant_id IS 'Вариант маршрута';
COMMENT ON COLUMN hist.v_round_hist.code IS 'Код';
COMMENT ON COLUMN hist.v_round_hist.wkt_geom IS 'Координаты';
COMMENT ON COLUMN hist.v_round_hist.geom IS 'Геометрия';
COMMENT ON COLUMN hist.v_round_hist.length IS 'Длина траектории, м';
COMMENT ON COLUMN hist.v_round_hist.length_fixed IS 'Фиксированная длина траектории, м';
COMMENT ON COLUMN hist.v_round_hist.specification IS 'Особенности траектории';
COMMENT ON COLUMN hist.v_round_hist.graph_section_start_offset IS 'Длина от начала дуги графа до начальной точки';
COMMENT ON COLUMN hist.v_round_hist.graph_section_end_offset IS 'Длина от начала дуги графа до конечной точки';
COMMENT ON COLUMN hist.v_round_hist.round_num IS 'Номер рейса для объединения в кругорейсы';
COMMENT ON COLUMN hist.v_round_hist.sign_deleted IS 'Удален';
COMMENT ON COLUMN hist.v_round_hist.sys_period IS 'Период действия';
COMMENT ON COLUMN hist.v_round_hist.action_type_id IS 'Тип рейса';
