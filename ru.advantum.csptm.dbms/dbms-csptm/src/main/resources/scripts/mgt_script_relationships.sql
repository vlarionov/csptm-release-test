with aData as (
SELECT
  np.nspname                  AS schemaname,
  pk.relname                  AS tablename,
  x.conname                   AS constraintname,
  pg_get_constraintdef(x.oid) AS description,
  nf.nspname                  AS parentschemaname,
  fk.relname                  AS parenttablename
FROM pg_catalog.pg_constraint x
  INNER JOIN pg_catalog.pg_class pk ON x.conrelid != 0 AND x.conrelid = pk.oid
  LEFT JOIN pg_namespace np ON np.oid = pk.relnamespace
  INNER JOIN pg_catalog.pg_class fk ON x.confrelid != 0 AND x.confrelid = fk.oid
  LEFT JOIN pg_namespace nf ON nf.oid = fk.relnamespace
WHERE x.contype = 'f'
      AND nf.nspname IN ('adm',
  'aissc',
  'asd',
  'askp',
  'asupbk',
  'cmnd',
  'core',
  'crep',
  'cron',
  'easu',
  'evt',
  'gis',
  'hist',
  'ibrd',
  'jofl',
  'jrep',
  'kdbo',
  'mnt',
  'oud',
  'paa',
  'rnis',
                         'rts',
                         'snsr',
                         'tt',
                         'ttb',
                         'udump',
                         'usw',
                         'voip',
                         'wh',
                         'yandex'
))
select * from aData ORDER BY 1,2,3;