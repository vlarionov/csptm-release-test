drop SEQUENCE cmnd.possible_answer_id_seq;

CREATE SEQUENCE cmnd.possible_answer_id_seq
INCREMENT 1
START 1
MINVALUE 1
MAXVALUE 65535
CACHE 1
CYCLE;
