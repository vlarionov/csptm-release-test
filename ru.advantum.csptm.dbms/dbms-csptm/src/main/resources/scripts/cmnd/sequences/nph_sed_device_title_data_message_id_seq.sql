drop SEQUENCE cmnd.nph_sed_device_title_data_message_id_seq;

CREATE SEQUENCE cmnd.nph_sed_device_title_data_message_id_seq
INCREMENT 1
START 1
MINVALUE 1
MAXVALUE 65535
CACHE 1
CYCLE;
