
-- Table cmnd.message_template
CREATE TABLE cmnd.message_template(
 message_template_id Smallserial NOT NULL,
 code Smallserial,
 contents Text NOT NULL,
 display_duration Smallserial,
 is_sound Boolean
)
TABLESPACE cmnd_data;

COMMENT ON TABLE cmnd.message_template IS 'Шаблон предопределенного сообщения';
COMMENT ON COLUMN cmnd.message_template.message_template_id IS 'id';
COMMENT ON COLUMN cmnd.message_template.code IS 'Код сообщения';
COMMENT ON COLUMN cmnd.message_template.contents IS 'Текст сообщения';
COMMENT ON COLUMN cmnd.message_template.display_duration IS 'Длительность отображения';

-- Add keys for table cmnd.message_template

ALTER TABLE cmnd.message_template ADD CONSTRAINT message_template_pk PRIMARY KEY (message_template_id) USING INDEX TABLESPACE cmnd_idx;

-- Table cmnd.possible_answer

CREATE TABLE cmnd.possible_answer(
 possible_answer_id Smallserial NOT NULL,
 response Text NOT NULL
)
TABLESPACE cmnd_data;

COMMENT ON TABLE cmnd.possible_answer IS 'Возможный ответ на текстовое сообщение';
COMMENT ON COLUMN cmnd.possible_answer.possible_answer_id IS 'id';
COMMENT ON COLUMN cmnd.possible_answer.response IS 'Текст ответа';

-- Add keys for table cmnd.possible_answer

ALTER TABLE cmnd.possible_answer ADD CONSTRAINT possible_answer_pk PRIMARY KEY (possible_answer_id) USING INDEX TABLESPACE cmnd_idx;
ALTER TABLE cmnd.possible_answer ADD CONSTRAINT response UNIQUE (response);

-- Table cmnd.possible_answer2message_template

CREATE TABLE cmnd.possible_answer2message_template(
 possible_answer2message_template_id Smallserial NOT NULL,
 message_template_id Smallint NOT NULL,
 possible_answer_id Smallint NOT NULL,
 order_num Smallint
)
TABLESPACE cmnd_data;

COMMENT ON TABLE cmnd.possible_answer2message_template IS 'Возможный ответ для шаблона сообщения';
COMMENT ON COLUMN cmnd.possible_answer2message_template.possible_answer2message_template_id IS 'id';
COMMENT ON COLUMN cmnd.possible_answer2message_template.message_template_id IS 'Шаблон сообщения';
COMMENT ON COLUMN cmnd.possible_answer2message_template.possible_answer_id IS 'Возможный ответ';
COMMENT ON COLUMN cmnd.possible_answer2message_template.order_num IS 'Порядковый номер';

-- Create indexes for table cmnd.possible_answer2message_template

CREATE INDEX ix_possible_answer2message_template_message_template_id ON cmnd.possible_answer2message_template (message_template_id) TABLESPACE cmnd_idx;
CREATE INDEX ix_possible_answer2message_template_possible_answer_id ON cmnd.possible_answer2message_template (possible_answer_id);

-- Add keys for table cmnd.possible_answer2message_template
ALTER TABLE cmnd.possible_answer2message_template ADD CONSTRAINT possible_answer2message_template_pk PRIMARY KEY (possible_answer2message_template_id) USING INDEX TABLESPACE cmnd_idx;
ALTER TABLE cmnd.possible_answer2message_template ADD CONSTRAINT fk_possible_answer2msg_template_to_msg_template FOREIGN KEY (message_template_id) REFERENCES cmnd.message_template (message_template_id) ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE cmnd.possible_answer2message_template ADD CONSTRAINT fk_possible_answer2msg_template_to_possible_answer FOREIGN KEY (possible_answer_id) REFERENCES cmnd.possible_answer (possible_answer_id) ON DELETE RESTRICT ON UPDATE NO ACTION;
