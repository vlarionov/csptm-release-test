INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (2, 'Программирование дискретных выходов', 'NPH_SET_PRDO', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (3, 'Запрос дискретных выходов ', 'NPH_GET_PRDO', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (7, 'Запрос параметров навигации (установленных на блоке)', 'NPH_GET_PRNAV', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (8, 'Запрос идентификационных данных от блока (навигации и др.)', 'NPH_GET_INFO', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (9, 'Запрос баланса sim-карты', 'NPH_GET_BALANCE', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (10, 'Установка текущего времени', 'NPH_SET_CURTIME', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (11, 'Установка маршрута и рейса на автоинформаторе', 'NPH_SET_ROUTE_AUTOINFORMER', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (12, 'Запрос маршрута и рейса на автоинформаторе ', 'NPH_GET_ROUTE_AUTOINFORMER', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (13, 'Сброс внутренних состояний блока', 'NPH_RESET_INT_STATE', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (14, 'Запрос IMSI-кода у SIM-карты в блоке', 'NPH_GET_SIM_IMSI', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (15, 'Запрос типов обслуживания ', 'NPH_SGC_SERVICE_REQUEST', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (4, 'Загрузка прошивки по FTP', 'NPH_SET_LOADFIRM', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (6, 'Установка параметров навигации', 'NPH_SET_PRNAV', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (5, 'Запрос параметров GPRS  ', 'NPH_GET_PRIA', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (18, 'Запрос навигации от блока ', 'BB+GETNAVINFO', 3, false, 2);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (17, 'Установка параметров навигации', 'BB+PRNAV', 3, false, 2);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (16, 'Загрузка прошивки по FTP', 'BB+PRLFIRM', 3, false, 2);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (19, 'Передача текстового сообщения', 'NPH_SED_DEVICE_TITLE_DATA', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (20, 'Включить передачу', 'START', 5, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (21, 'Выключить передачу', 'STOP', 5, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (1, 'Запрос описания участника соединения', 'NPH_SGC_PEER_DESC_REQUEST', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (0, 'Установка параметров GPRS ', 'NPH_SET_PRIA', 3, false, 1);



insert into cmnd.cmnd_status(
  cmnd_status_id,
  cmnd_status_name
)
values
(0,
'Новый'
);

insert into cmnd.cmnd_status(
  cmnd_status_id,
  cmnd_status_name
)
values
(1,
'Передано'
);

insert into cmnd.cmnd_status(
  cmnd_status_id,
  cmnd_status_name
)
values
(2,
'Блок недоступен'
);


insert into cmnd.cmnd_status(
  cmnd_status_id,
  cmnd_status_name
)
values
(3,
'Ошибка системы'
);


INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (2, 'Программирование дискретных выходов', 'NPH_SET_PRDO', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (3, 'Запрос дискретных выходов ', 'NPH_GET_PRDO', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (7, 'Запрос параметров навигации (установленных на блоке)', 'NPH_GET_PRNAV', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (8, 'Запрос идентификационных данных от блока (навигации и др.)', 'NPH_GET_INFO', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (9, 'Запрос баланса sim-карты', 'NPH_GET_BALANCE', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (10, 'Установка текущего времени', 'NPH_SET_CURTIME', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (11, 'Установка маршрута и рейса на автоинформаторе', 'NPH_SET_ROUTE_AUTOINFORMER', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (12, 'Запрос маршрута и рейса на автоинформаторе ', 'NPH_GET_ROUTE_AUTOINFORMER', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (13, 'Сброс внутренних состояний блока', 'NPH_RESET_INT_STATE', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (14, 'Запрос IMSI-кода у SIM-карты в блоке', 'NPH_GET_SIM_IMSI', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (1, 'Запрос описания участника соединения. ', 'NPH_SGC_PEER_DESC_REQUEST', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (15, 'Запрос типов обслуживания ', 'NPH_SGC_SERVICE_REQUEST', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (4, 'Загрузка прошивки по FTP', 'NPH_SET_LOADFIRM', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (6, 'Установка параметров навигации', 'NPH_SET_PRNAV', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (0, 'Установка параметров GPRS ', 'NPH_SET_PRIA_EXT', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (5, 'Запрос параметров GPRS  ', 'NPH_GET_PRIA', 3, false, 1);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (18, 'Запрос навигации от блока ', 'BB+GETNAVINFO', 3, false, 2);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (17, 'Установка параметров навигации', 'BB+PRNAV', 3, false, 2);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (16, 'Загрузка прошивки по FTP', 'BB+PRLFIRM', 3, false, 2);
INSERT INTO cmnd.cmnd_ref (cmnd_ref_id, cmnd_ref_name, cmnd_ref_protocol, protocol_id, is_deleted, low_level_protocol) VALUES (19, 'Передача текстового сообщения', 'NPH_SED_DEVICE_TITLE_DATA', 3, false, 1);



INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (2, 3, 2, 'Ответ на запрос дискретных выходов', 'NPH_PRDO');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (3, 3, 1, 'Общий пакет подтверждения', 'NPH_RESULT ');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (1, 2, 1, 'Общий пакет подтверждения', ' NPH_RESULT');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (4, 5, 1, 'Общий пакет подтверждения', 'NPH_RESULT');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (5, 5, 2, 'Ответ на запрос параметров GPRS', 'NPH_PRIA_EXT');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (6, 6, 1, 'Общий пакет подтверждения', 'NPH_RESULT');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (7, 7, 1, 'Общий пакет подтверждения', 'NPH_RESULT ');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (8, 7, 2, 'Ответ на запрос навигационных параметров', 'NPH_PRNAV');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (9, 8, 1, 'Общий пакет подтверждения', 'NPH_RESULT');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (10, 8, 2, 'Ответ на запрос идентификационных данных', 'NPH_INFO');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (11, 9, 1, 'Общий пакет подтверждения', 'NPH_RESULT ');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (12, 9, 2, 'Ответ на запрос баланса sim-карты', 'NPH_BALANCE');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (13, 10, 1, 'Общий пакет подтверждения', 'NPH_RESULT');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (14, 11, 1, 'Общий пакет подтверждения', 'NPH_RESULT');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (15, 11, 2, 'Ответ на команду установки маршрута/рейса на автоинформатор', 'NPH_ROUTE_AUTOINFORMER');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (16, 12, 1, 'Общий пакет подтверждения', 'NPH_RESULT');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (17, 12, 2, 'Ответ на запрос маршрута/рейса на автоинформаторе', 'NPH_ROUTE_AUTOINFORMER');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (18, 13, 1, 'Общий пакет подтверждения', 'NPH_RESULT');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (19, 14, 1, 'Общий пакет подтверждения', 'NPH_RESULT ');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (20, 14, 2, 'Ответ на запрос IMSI кода sim-карты', 'NPH_SIM_IMSI');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (21, 1, 1, 'Ответ на запрос описания участника соединения ', 'NPH_SGC_PEER_DESC ');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (22, 15, 1, 'Ответ на запрос типов обслуживания', 'NPH_SGC_SERVICES');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (23, 0, 1, 'Общий пакет подтверждения', 'NPH_RESULT');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (25, 16, 1, 'Результат загрузки прошивки', '<Неизвестно>');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (24, 17, 1, 'Подтверждение установки параметров навигации', '<Неизвестно>');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (26, 18, 1, 'Навигация от блока ', '<Неизвестно>');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (27, 19, 1, 'Результат передачи текстового сообщения', 'NPH_SED_DEVICE_RESULT');
INSERT INTO cmnd.response_ref (response_ref_id, cmnd_ref_id, order_num, response_ref_name, response_ref_protocol) VALUES (28, 19, 2, 'Ответ на вопрос', 'NPH_SED_DEVICE_TITLE_DATA');


INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (41, 19, 'Порядковый номер данного передающего устройства', 1, 'address_from_num');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (2, 2, 'Номер выхода ( 0-7)', 1, 'output_id');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (3, 2, 'Состояние: 0/1, выключено/включено  ', 1, 'mode');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (4, 3, 'Номер выхода ( 0-7)', 1, 'output_id');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (5, 4, 'IP адрес FTP', 1, 'ip_address');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (6, 4, 'Порт FTP', 1, 'port');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (7, 4, 'URL адрес FTP', 2, 'URL');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (8, 4, 'Путь к папке прошивки ', 2, 'dir_file');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (9, 4, 'Имя файла прошивки ', 2, 'file_name');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (10, 4, 'Имя пользователя ', 2, 'User');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (11, 4, 'Пароль', 2, 'Password');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (13, 6, 'Интервал в секундах для генерации отметки в режиме движения', 1, 'SendDataInterval');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (14, 6, 'Интервал в минутах для генерации отметки в режиме стоянки', 1, 'SendDataInterval_stop');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (15, 6, 'Угол поворота для генерирования отметки', 1, 'Angle');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (16, 6, 'Дистанция, после прохождения которой прибор генерирует отметку', 1, 'Dist');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (17, 9, 'Команда, которая используется для запроса баланса. Если данная строка нулевой длины, то навигатор возьмет данную команду из параметров.', 2, 'cmd_req');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (18, 10, 'Текущее время в формате UTC', 1, 'cur_time');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (19, 11, 'Имя маршрута', 2, 'route');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (20, 11, 'Имя рейса', 2, 'run');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (21, 13, 'Битовая маска сброса состояний: 0бит - сброс голосового вызова, 1 бит - сброс сигнала SOS', 1, 'state');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (22, 15, 'Содержит запрашиваемый тип обслуживания (NPH_SRV_CLIENT_LIST, NPH_SRV_DEBUG, NPH_SRV_SERVER_GENERIC, NPH_SRV_EXTERNAL_DEVICE, NPH_SRV_NAVDATA, NPH_SRV_GENERIC_CONTROLS) или пусто', 1, 'data');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (24, 0, 'Порт сервера', 1, 'port');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (26, 0, 'Имя точки доступа', 2, 'APN');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (27, 0, 'Имя пользователя', 2, 'User');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (28, 0, 'Пароль', 2, 'Password');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (42, 19, 'Порядковый номер  принимающего устройства', 1, 'address_to_num');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (40, 19, 'Тип периферийного устройства передающего устройства', 2, 'address_from_type');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (43, 19, 'Тип периферийного устройства передающего устройства', 2, 'address_to_type');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (44, 19, 'Тип передаваемых данных', 2, 'type_data');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (45, 19, 'Текст', 2, 'message');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (46, 19, 'Номер сообщения', 1, 'message_id');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (23, 0, 'IP адрес сервера', 2, 'ip_address');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (38, 16, 'пароль для подключения к ftp', 2, 'Password');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (37, 16, 'имя пользователя для подключения к ftp сервера ', 2, 'User');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (36, 16, 'имя файла для скачивания ', 2, 'FileName');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (35, 16, 'директория на ftp', 2, 'DirName');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (34, 16, 'порт сервера', 1, 'port');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (33, 16, 'IP адрес сервера или URL адрес сервера ', 2, 'AddServer');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (32, 17, 'Угол поворота', 1, '4.Angle');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (31, 17, 'Пройденное расстояние', 1, '3.Distance');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (29, 17, 'Отметка в режиме движения (сек)', 1, '1.MoveTime');
INSERT INTO cmnd.cmnd_ref_param (cmnd_ref_param_id, cmnd_ref_id, param_name, data_type, param_protocol) VALUES (30, 17, 'Отметка в режиме стоянки (мин.)', 1, '2.ParkTime');



INSERT INTO cmnd.cmnd_ref_param_val (cmnd_ref_param_val_id, cmnd_ref_param_id, param_ref_val_name, param_ref_val) VALUES (1, 40, 'NPH_SED_ADDR_DISPLAY_DEVICE', 'NPH_SED_ADDR_DISPLAY_DEVICE');
INSERT INTO cmnd.cmnd_ref_param_val (cmnd_ref_param_val_id, cmnd_ref_param_id, param_ref_val_name, param_ref_val) VALUES (2, 40, 'NPH_SED_ADDR_VOICE_DRIVER_DEVICE', 'NPH_SED_ADDR_VOICE_DRIVER_DEVICE');
INSERT INTO cmnd.cmnd_ref_param_val (cmnd_ref_param_val_id, cmnd_ref_param_id, param_ref_val_name, param_ref_val) VALUES (3, 40, 'NPH_SED_ADDR_VOICE_DRIVER_PASSANGERS', 'NPH_SED_ADDR_VOICE_DRIVER_PASSANGERS');
INSERT INTO cmnd.cmnd_ref_param_val (cmnd_ref_param_val_id, cmnd_ref_param_id, param_ref_val_name, param_ref_val) VALUES (4, 40, 'NPH_SED_ADDR_INTERNAL_DISPLAY', 'NPH_SED_ADDR_INTERNAL_DISPLAY');
INSERT INTO cmnd.cmnd_ref_param_val (cmnd_ref_param_val_id, cmnd_ref_param_id, param_ref_val_name, param_ref_val) VALUES (5, 40, 'NPH_SED_ADDR_EXTERNAL_DISPLAY', 'NPH_SED_ADDR_EXTERNAL_DISPLAY');
INSERT INTO cmnd.cmnd_ref_param_val (cmnd_ref_param_val_id, cmnd_ref_param_id, param_ref_val_name, param_ref_val) VALUES (6, 40, 'NPH_SED_ADDR_IRMA_DEVICE', 'NPH_SED_ADDR_IRMA_DEVICE');
INSERT INTO cmnd.cmnd_ref_param_val (cmnd_ref_param_val_id, cmnd_ref_param_id, param_ref_val_name, param_ref_val) VALUES (7, 40, 'NPH_SED_ADDR_BPKRD_DEVICE', 'NPH_SED_ADDR_BPKRD_DEVICE');
INSERT INTO cmnd.cmnd_ref_param_val (cmnd_ref_param_val_id, cmnd_ref_param_id, param_ref_val_name, param_ref_val) VALUES (8, 40, 'NPH_SED_ADDR_TEMPERATURE_ON_BOARD', 'NPH_SED_ADDR_TEMPERATURE_ON_BOARD');
INSERT INTO cmnd.cmnd_ref_param_val (cmnd_ref_param_val_id, cmnd_ref_param_id, param_ref_val_name, param_ref_val) VALUES (9, 43, 'NPH_SED_ADDR_DISPLAY_DEVICE', 'NPH_SED_ADDR_DISPLAY_DEVICE');
INSERT INTO cmnd.cmnd_ref_param_val (cmnd_ref_param_val_id, cmnd_ref_param_id, param_ref_val_name, param_ref_val) VALUES (10, 43, 'NPH_SED_ADDR_VOICE_DRIVER_DEVICE', 'NPH_SED_ADDR_VOICE_DRIVER_DEVICE');
INSERT INTO cmnd.cmnd_ref_param_val (cmnd_ref_param_val_id, cmnd_ref_param_id, param_ref_val_name, param_ref_val) VALUES (11, 43, 'NPH_SED_ADDR_VOICE_DRIVER_PASSANGERS', 'NPH_SED_ADDR_VOICE_DRIVER_PASSANGERS');
INSERT INTO cmnd.cmnd_ref_param_val (cmnd_ref_param_val_id, cmnd_ref_param_id, param_ref_val_name, param_ref_val) VALUES (12, 43, 'NPH_SED_ADDR_INTERNAL_DISPLAY', 'NPH_SED_ADDR_INTERNAL_DISPLAY');
INSERT INTO cmnd.cmnd_ref_param_val (cmnd_ref_param_val_id, cmnd_ref_param_id, param_ref_val_name, param_ref_val) VALUES (13, 43, 'NPH_SED_ADDR_EXTERNAL_DISPLAY', 'NPH_SED_ADDR_EXTERNAL_DISPLAY');
INSERT INTO cmnd.cmnd_ref_param_val (cmnd_ref_param_val_id, cmnd_ref_param_id, param_ref_val_name, param_ref_val) VALUES (14, 43, 'NPH_SED_ADDR_IRMA_DEVICE', 'NPH_SED_ADDR_IRMA_DEVICE');
INSERT INTO cmnd.cmnd_ref_param_val (cmnd_ref_param_val_id, cmnd_ref_param_id, param_ref_val_name, param_ref_val) VALUES (15, 43, 'NPH_SED_ADDR_BPKRD_DEVICE', 'NPH_SED_ADDR_BPKRD_DEVICE');
INSERT INTO cmnd.cmnd_ref_param_val (cmnd_ref_param_val_id, cmnd_ref_param_id, param_ref_val_name, param_ref_val) VALUES (16, 43, 'NPH_SED_ADDR_TEMPERATURE_ON_BOARD', 'NPH_SED_ADDR_TEMPERATURE_ON_BOARD');
INSERT INTO cmnd.cmnd_ref_param_val (cmnd_ref_param_val_id, cmnd_ref_param_id, param_ref_val_name, param_ref_val) VALUES (17, 44, 'скрипт сообщения для дисплея прибора', 'NPH_SED_TYPE_MSG_SCRIPT_DISPLAY');
INSERT INTO cmnd.cmnd_ref_param_val (cmnd_ref_param_val_id, cmnd_ref_param_id, param_ref_val_name, param_ref_val) VALUES (18, 44, 'картинка в формате jpg', 'NPH_SED_TYPE_JPG_FOTO ');
INSERT INTO cmnd.cmnd_ref_param_val (cmnd_ref_param_val_id, cmnd_ref_param_id, param_ref_val_name, param_ref_val) VALUES (19, 44, 'векторная карта, в собственном формате', 'NPH_SED_TYPE_VECTOR_MAP');
INSERT INTO cmnd.cmnd_ref_param_val (cmnd_ref_param_val_id, cmnd_ref_param_id, param_ref_val_name, param_ref_val) VALUES (20, 44, 'голосовое сообщение, сжатое по алгоритму LPC10', 'NPH_SED_TYPE_VOICE_LPC10');
INSERT INTO cmnd.cmnd_ref_param_val (cmnd_ref_param_val_id, cmnd_ref_param_id, param_ref_val_name, param_ref_val) VALUES (21, 44, 'голосовое сообщение, сжатое по алгоритму GSM630', 'NPH_SED_TYPE_VOICE_GSM630');
INSERT INTO cmnd.cmnd_ref_param_val (cmnd_ref_param_val_id, cmnd_ref_param_id, param_ref_val_name, param_ref_val) VALUES (22, 44, 'голосовое сообщение, сжатое по алгоритму Speex', 'NPH_SED_TYPE_VOICE_SPEEX');


INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (3, 2, 'Номер выхода ( 0-7)', 'output_id');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (4, 2, 'Состояние: 0– выключено, 1 – включено ', 'mode');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (7, 5, 'IP адрес сервера', 'ip_address');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (8, 5, 'Порт', 'port');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (9, 5, 'URL адрес', 'URL');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (10, 5, 'Имя точки доступа ', 'APN');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (11, 5, 'Имя пользователя', 'User');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (12, 5, 'Пароль', 'Password');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (13, 5, 'Номер сервера ', 'Num_server');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (15, 7, 'Код ошибки', 'error');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (16, 8, 'Интервал в секундах для генерации отметки в режиме движения', 'SendDataInterval');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (17, 8, 'Интервал в минутах для генерации отметки в режиме стоянки', 'SendDataInterval_stop');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (18, 8, 'Угол поворота для генерирования отметки', 'Angle');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (19, 8, 'Дистанция, после прохождения которой прибор генерирует отметку', 'Dist');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (20, 9, 'Код ошибки', 'error');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (21, 10, 'Дата прошивки', 'Data_Ver');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (22, 10, 'Тип навигатора', 'Ver_navig');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (23, 10, 'Широта', 'lat');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (24, 10, 'Долгота', 'lon');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (25, 10, 'Количество видимых спутников', 'Num_sat');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (26, 10, 'Скорость движения', 'speed');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (27, 11, 'Код ошибки', 'error');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (28, 12, 'Сведения о балансе (то, что вернул сотовый оператор)', 'Balance');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (29, 13, 'Код ошибки', 'error');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (30, 14, 'Код ошибки', 'error');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (31, 15, 'Имя маршрута', 'route');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (32, 15, 'Имя рейса', 'run');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (33, 16, 'Код ошибки', 'error');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (34, 17, 'Имя маршрута', 'route');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (35, 17, 'Имя рейса', 'run');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (36, 18, 'Код ошибки', 'error');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (37, 19, 'Код ошибки', 'error');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (38, 20, 'IMSI код', 'IMSI');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (39, 21, 'Строка описания участника соединения', 'data');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (40, 22, 'Типы обслуживания (перечень)', 'data');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (41, 23, 'Код ошибки', 'error');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (14, 6, 'Код ошибки', 'error');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (2, 1, 'Код ошибки', 'error');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (5, 3, 'Код ошибки', 'error');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (6, 4, 'Код ошибки', 'error');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (42, 26, 'Номер блока', '#');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (43, 26, 'Дата', 'D');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (44, 26, 'Широта', 'Lat');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (45, 26, 'Долгота', 'Lon');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (46, 26, 'Количество функций', 'NS');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (47, 26, 'Валидность', 'V');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (48, 26, 'Скорость', 'S');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (49, 26, 'Курс ', 'C');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (50, 27, 'Ошибка', 'ERROR_PACKET');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (51, 28, 'Номер ответа', 'STATUS');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (52, 25, 'Текст ответа', 'TEXT');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (53, 24, 'Текст ответа', 'TEXT');
INSERT INTO cmnd.response_ref_attr (response_ref_attr_id, response_ref_id, response_attr_name, response_attr_protokol) VALUES (54, 26, 'Текст ответа', 'TEXT');
