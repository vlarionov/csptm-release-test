CREATE OR REPLACE FUNCTION cmnd.insert_sms_ask(
p_cmnd_request_id bigint,
p_status int
) RETURNS bigint
	LANGUAGE plpgsql
AS $$
declare
    l_cmnd_response_id bigint;
begin

    insert into cmnd.cmnd_response(cmnd_request_id, response_time, response_name, response_param)
    values (p_cmnd_request_id, now(), 'ask', null) RETURNING cmnd_response_id id INTO l_cmnd_response_id;


    update cmnd.cmnd_request
    set cmnd_status_id = p_status
    where cmnd_request_id = p_cmnd_request_id;

    return l_cmnd_response_id;
end;
$$;