CREATE OR REPLACE FUNCTION cmnd.insert_sms(
p_phone text,
p_provider_id text,
p_text text,
p_delivery_time timestamp
) RETURNS bigint
	LANGUAGE plpgsql
AS $$
declare
    l_cmnd_response_id bigint;
    l_cmnd_request_id bigint;
    l_request_time timestamp;
begin
    --найдем время последней доставленной команды без ответа
    select min(cmnd_request.request_time)
    into l_request_time
    from core.sim_card
    join core.sim_card2unit on sim_card.sim_card_id = sim_card2unit.sim_card_id
    join cmnd.cmnd_request on cmnd_request.unit_id = sim_card2unit.equipment_id
    join cmnd.cmnd_ref on cmnd_ref.cmnd_ref_id = cmnd_request.cmnd_ref_id
    where core.sim_card.phone_num = p_phone
    and not exists(select 1 from cmnd.cmnd_response where cmnd_request.cmnd_request_id = cmnd_response.cmnd_request_id)
    and cmnd_status_id = cmnd.jf_cmnd_status_pkg$cn_status_sended()
    and cmnd_ref.low_level_protocol = cmnd.jf_cmnd_ref_pkg$cn_sms();

    if l_request_time is null then
        --если не нашли найдем последнюю отправленную команду
        select max(cmnd_request.cmnd_request_id)
        into l_cmnd_request_id
        from core.sim_card
            join core.sim_card2unit on sim_card.sim_card_id = sim_card2unit.sim_card_id
            join cmnd.cmnd_request on cmnd_request.unit_id = sim_card2unit.equipment_id
        where core.sim_card.phone_num = p_phone
            and cmnd_status_id = cmnd.jf_cmnd_status_pkg$cn_status_sended();

     else

        select cmnd_request.cmnd_request_id
        into l_cmnd_request_id
        from core.sim_card
            join core.sim_card2unit on sim_card.sim_card_id = sim_card2unit.sim_card_id
            join cmnd.cmnd_request on cmnd_request.unit_id = sim_card2unit.equipment_id
        where core.sim_card.phone_num = p_phone
            and not exists(select 1 from cmnd.cmnd_response where cmnd_request.cmnd_request_id = cmnd_response.cmnd_request_id)
            and cmnd_status_id = cmnd.jf_cmnd_status_pkg$cn_status_sended()
            and cmnd_request.request_time = l_request_time limit 1;

    end if;

    insert into cmnd.cmnd_response(cmnd_request_id, response_time, response_name, response_param)
    values (l_cmnd_request_id, p_delivery_time, coalesce(p_provider_id, 'unnown'), jsonb_build_object('TEXT', p_text))
    RETURNING cmnd_response_id id INTO l_cmnd_response_id;

    return l_cmnd_response_id;
end;
$$;