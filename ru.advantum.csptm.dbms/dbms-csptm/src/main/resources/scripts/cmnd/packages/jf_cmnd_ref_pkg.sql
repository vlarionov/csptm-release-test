CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_pkg$of_delete (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.cmnd_ref%rowtype;
begin
   l_r := cmnd.jf_cmnd_ref_pkg$attr_to_rowtype(p_attr);

   delete from  cmnd.cmnd_ref where  cmnd_ref_id = l_r.cmnd_ref_id;

   return null;
end;

$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_pkg$attr_to_rowtype (p_attr text) RETURNS cmnd.cmnd_ref
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.cmnd_ref%rowtype;
begin
   l_r.cmnd_ref_id := jofl.jofl_pkg$extract_number(p_attr, 'cmnd_ref_id', true);
   l_r.cmnd_ref_name := jofl.jofl_pkg$extract_varchar(p_attr, 'cmnd_ref_name', true);
   l_r.cmnd_ref_protocol := jofl.jofl_pkg$extract_varchar(p_attr, 'cmnd_ref_protocol', true);
   l_r.protocol_id := jofl.jofl_pkg$extract_varchar(p_attr, 'protocol_id', true);
   l_r.is_deleted := jofl.jofl_pkg$extract_boolean(p_attr, 'is_deleted', true);
   l_r.low_level_protocol := jofl.jofl_pkg$extract_number(p_attr, 'low_level_protocol', true);
   return l_r;
end;

$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_pkg$of_insert (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.cmnd_ref%rowtype;
begin
   l_r := cmnd.jf_cmnd_ref_pkg$attr_to_rowtype(p_attr);
   l_r.cmnd_ref_id := nextval( 'cmnd.cmnd_ref_cmnd_ref_id_seq' );


   insert into cmnd.cmnd_ref select l_r.*;

   return null;
end;

$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$

declare
    rec record;
begin
 open p_rows for
      select
        cmnd_ref_id,
        cmnd_ref_name,
        cmnd_ref_protocol,
        cmnd_ref.protocol_id,
        is_deleted,
        low_level_protocol,
        name_full protocol_name
      from cmnd.cmnd_ref
           join core.protocol cp on cmnd_ref.protocol_id = cp.protocol_id;
end;

$$;

CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_pkg$of_update (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.cmnd_ref%rowtype;
begin
   l_r := cmnd.jf_cmnd_ref_pkg$attr_to_rowtype(p_attr);

   update cmnd.cmnd_ref set
          cmnd_ref_name = l_r.cmnd_ref_name,
          cmnd_ref_protocol = l_r.cmnd_ref_protocol,
          protocol_id = l_r.protocol_id,
          is_deleted = l_r.is_deleted,
          low_level_protocol = l_r.low_level_protocol
   where
          cmnd_ref_id = l_r.cmnd_ref_id;

   return null;
end;

$$;

/*
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_pkg$of_create_ui(p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
DECLARE
    rec record;
    lfunc text;

BEGIN

delete from jofl.role2call
where db_method = cmnd.jf_cmnd_ref_pkg$cn_db_method() and call_action like cmnd.jf_cmnd_ref_pkg$cn_call_action_part()||'%';

delete from jofl.win_action_field
where db_method = cmnd.jf_cmnd_ref_pkg$cn_db_method() and call_action like cmnd.jf_cmnd_ref_pkg$cn_call_action_part()||'%';

delete from jofl.win_action_call
where db_method = cmnd.jf_cmnd_ref_pkg$cn_db_method() and call_action like cmnd.jf_cmnd_ref_pkg$cn_call_action_part()||'%';



INSERT
INTO jofl.win_action_call (
  db_method,
  call_action,
  action_title,
  is_refresh,
  is_internal,
  is_win_action,
  n_sort_order,
  is_row_depend,
  call_default,
  opt_interface_pkg)
SELECT
  cmnd.jf_cmnd_ref_pkg$cn_db_method() as db_method,
  cmnd.jf_cmnd_ref_pkg$get_call_action(cmnd_ref.protocol_id||'_'||cmnd_ref.cmnd_ref_protocol) as call_action,
  cmnd_ref_name as action_title,
  0 as is_refresh,
  0 as is_internal,
  0 as is_win_action,
  row_number() over () as n_sort_order,
  1 as is_row_depend,
  null as call_default,
  null as opt_interface_pkg
from cmnd.cmnd_ref;

insert into jofl.win_action_field
(id_afield,
 db_method,
 call_action,
 filter_class,
 initial_param,
 n_sort_order,
 field_title
)
select
  public.uuid_generate_v4() as id_afield,
  cmnd.jf_cmnd_ref_pkg$cn_db_method() as db_method,
  cmnd.jf_cmnd_ref_pkg$get_call_action(cmnd_ref.protocol_id||'_'||cmnd_ref.cmnd_ref_protocol) as call_action,
  case data_type
    when 1 then 'NUMBER'
    when 2 then 'TEXT'
  end as filter_class,
  case data_type
  when 1 then
    json_build_object(
        'TITLE', param_name,
        'PREFIX', cmnd.jf_cmnd_ref_pkg$cn_param()||'_'||UPPER(cmnd_ref_param.param_protocol)||'_',
        'MANDATORY','true'
    )
  when 2 then
    json_build_object(
        'TITLE', param_name,
        'PREFIX', cmnd.jf_cmnd_ref_pkg$cn_param()||'_'||upper(cmnd_ref_param.param_protocol)||'_',
        'MANDATORY','true',
         'WIDTH', '400'
    )
  end initial_param,
  row_number() over (partition by cmnd_ref_param.cmnd_ref_id) as n_sort_order,
  param_name as field_title
from cmnd.cmnd_ref_param join cmnd.cmnd_ref on cmnd_ref.cmnd_ref_id = cmnd_ref_param.cmnd_ref_id;


insert into jofl.role2call
(id_role, db_method, call_action)
SELECT
  'CORE.ADMIN' as id_role,
  cmnd.jf_cmnd_ref_pkg$cn_db_method() as db_method,
  cmnd.jf_cmnd_ref_pkg$get_call_action(cmnd_ref.protocol_id||'_'||cmnd_ref.cmnd_ref_protocol) as call_action
from cmnd.cmnd_ref;

		for rec in select call_action from jofl.win_action_call
                    where db_method = cmnd.jf_cmnd_ref_pkg$cn_db_method() and win_action_call.call_action like cmnd.jf_cmnd_ref_pkg$cn_call_action_part()||'%'

		loop
            lfunc  := 'CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$'||
            rec.call_action||
            ' '||
            '(p_id_account numeric, p_attr text) RETURNS text LANGUAGE plpgsql AS $' ||
            '$ BEGIN '||
            'return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd(p_id_account, p_attr); '||'
            END  $' ||'$; ';
		    execute lfunc;
		end loop;

return null;

END
$$;
*/

CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_pkg$cn_db_method()
  RETURNS text AS
  'select ''cmnd.cmnd_ref4unit''::text;'
LANGUAGE SQL IMMUTABLE;


CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_pkg$cn_call_action_part()
  RETURNS text AS
  'select ''of_autogen''::text;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_pkg$cn_param()
  RETURNS text AS
  'select ''PARAM''::text;'
LANGUAGE SQL IMMUTABLE;



CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_pkg$get_call_action(t text) RETURNS text AS $$
    select lower(cmnd.jf_cmnd_ref_pkg$cn_call_action_part()||'_'||t) as result;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_pkg$get_low_level_protocol_name(low_level_protocol int) RETURNS text AS $$
    select case low_level_protocol
            when 2 then 'SMS'
            when 1 then 'TCP_IP'
            else 'UNNOWS'
            end
    as result;
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_pkg$cn_sms()
  RETURNS int AS
  'select 2::int;'
LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_pkg$cn_tcp_ip()
  RETURNS int AS
  'select 1::int;'
LANGUAGE SQL IMMUTABLE;

