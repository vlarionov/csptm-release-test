CREATE or replace FUNCTION cmnd.jf_bnsr_telematic_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$

declare
    l_equipment_id numeric;
    l_last_call timestamp;
begin
 l_equipment_id := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', false);
 l_last_call := jofl.jofl_pkg$extract_date(p_attr, 'last_call', true);


 open p_rows for
      select
        packet_id,
        tr_id,
        unit_id,
        event_time,
        device_event_id,
        location_valid,
        gps_time,
        lon,
        lat,
        alt,
        speed,
        heading,
        receive_time,
        is_hist_data
      from core.traffic
      where unit_id = l_equipment_id
            and event_time between l_last_call and l_last_call + interval '1 minutes' ;
end;

$$
