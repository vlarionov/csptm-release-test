CREATE OR REPLACE FUNCTION cmnd.parse_bnst_response(p_response_param json, p_attrib text) RETURNS text
	LANGUAGE plpgsql
AS $$
declare
    l_data text;
begin

    with b as (
    select a.key, a.value
    from json_each_text(p_response_param::json) a
    )
    select b.value into l_data
    from b
    where b.key = p_attrib;

    return l_data;
end;
$$;