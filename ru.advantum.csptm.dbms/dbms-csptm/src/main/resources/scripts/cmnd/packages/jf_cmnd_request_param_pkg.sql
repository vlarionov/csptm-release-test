
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_request_param_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.cmnd_request%rowtype;
begin
   l_r.cmnd_request_id := jofl.jofl_pkg$extract_number(p_attr, 'cmnd_request_id', true);

 open p_rows for
    select a.key, a.value, cmnd_request_id
    from cmnd.cmnd_request, jsonb_each_text(request_param) a
    where cmnd_request_id = l_r.cmnd_request_id;
end;

$$;



