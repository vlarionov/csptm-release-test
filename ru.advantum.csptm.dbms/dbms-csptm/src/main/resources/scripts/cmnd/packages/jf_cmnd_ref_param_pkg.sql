CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_param_pkg$attr_to_rowtype (p_attr text) RETURNS cmnd.cmnd_ref_param
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.cmnd_ref_param%rowtype;
begin
   l_r.cmnd_ref_param_id := jofl.jofl_pkg$extract_number(p_attr, 'cmnd_ref_param_id', true);
   l_r.cmnd_ref_id := jofl.jofl_pkg$extract_number(p_attr, 'cmnd_ref_id', true);
   l_r.param_name := jofl.jofl_pkg$extract_varchar(p_attr, 'param_name', true);
   l_r.data_type := jofl.jofl_pkg$extract_number(p_attr, 'data_type', true);
   l_r.param_protocol := jofl.jofl_pkg$extract_varchar(p_attr, 'param_protocol', true);
   return l_r;
end;

$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_param_pkg$of_delete (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.cmnd_ref_param%rowtype;
begin
   l_r := cmnd.jf_cmnd_ref_param_pkg$attr_to_rowtype(p_attr);

   delete from  cmnd.cmnd_ref_param where  cmnd_ref_param_id = l_r.cmnd_ref_param_id;

   return null;
end;

$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_param_pkg$of_insert (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.cmnd_ref_param%rowtype;
begin

   l_r := cmnd.jf_cmnd_ref_param_pkg$attr_to_rowtype(p_attr);
   l_r.cmnd_ref_param_id := nextval( 'cmnd.cmnd_ref_param_cmnd_ref_param_id_seq' );
   insert into cmnd.cmnd_ref_param select l_r.*;

   return null;
end;

$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_param_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$

declare
    ln_cmnd_ref_id integer :=  jofl.jofl_pkg$extract_number(p_attr, 'cmnd_ref_id', false);
begin
 open p_rows for
      select
        cmnd_ref_param_id,
        cmnd_ref_id,
        param_name,
        data_type,
        param_protocol
      from cmnd.cmnd_ref_param
      where cmnd_ref_id = ln_cmnd_ref_id;
end;

$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_param_pkg$of_update (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.cmnd_ref_param%rowtype;
begin
   l_r := cmnd.jf_cmnd_ref_param_pkg$attr_to_rowtype(p_attr);

   update cmnd.cmnd_ref_param set
          cmnd_ref_id = l_r.cmnd_ref_id,
          param_name = l_r.param_name,
          data_type = l_r.data_type,
          param_protocol  = l_r.param_protocol
   where
          cmnd_ref_param_id = l_r.cmnd_ref_param_id;

   return null;
end;

$$;