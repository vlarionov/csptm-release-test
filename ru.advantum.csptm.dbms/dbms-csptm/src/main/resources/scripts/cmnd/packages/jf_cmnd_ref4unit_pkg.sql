CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$attr_to_rowtype (p_attr text) RETURNS cmnd.cmnd_request
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.cmnd_request%rowtype;
begin
   l_r.unit_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_id', true);
   l_r.cmnd_ref_id := jofl.jofl_pkg$extract_number(p_attr, 'cmnd_ref_id', true);

   return l_r;
end;
$$;

CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$

declare
   l_unit_id int:= jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', false);
begin
 open p_rows for
    select
    cmnd_ref.cmnd_ref_id
    ,cmnd_ref.cmnd_ref_name
    ,cmnd_ref.cmnd_ref_protocol
    ,unit.unit_id
    ,cmnd.jf_cmnd_ref_pkg$get_low_level_protocol_name(cmnd_ref.low_level_protocol) low_level_protocol_name
    ,'P{A},D{'||cmnd.jf_cmnd_ref_pkg$get_call_action(cmnd_ref.protocol_id||'_'||replace(cmnd_ref.cmnd_ref_protocol, '+', '_'))||'}' AS "ROW$POLICY"
    from cmnd.cmnd_ref
      join core.protocol ON protocol.protocol_id = cmnd_ref.protocol_id
      join core.hub on protocol.protocol_id = hub.protocol_id
      join core.unit on hub.hub_id = unit.hub_id
    where unit_id = l_unit_id;
end;
$$;


CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd_to_rabbit(p_id_account numeric, p_unit_id NUMERIC, p_cmnd_ref_id numeric, p_request_param text) RETURNS bigint
LANGUAGE plpgsql
AS $$
begin
  return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd_to_rabbit(p_id_account, p_unit_id, p_cmnd_ref_id, p_request_param, false);
end;
$$;

CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd_to_rabbit(p_id_account numeric, p_unit_id NUMERIC, p_cmnd_ref_id numeric, p_request_param text, p_with_delay boolean) RETURNS bigint
LANGUAGE plpgsql
AS $$

declare
  l_r cmnd.cmnd_request%rowtype;
  l_cmnd_ref_protocol text;
  l_cmd text;
  l_queue text;
  sucsses boolean = false;
  l_low_level_protocol int;
  l_phone_num text;
  l_sms_text text;
begin
  l_r.unit_id := p_unit_id;
  l_r.cmnd_ref_id := p_cmnd_ref_id;
  l_r.cmnd_status_id := 0;
  l_r.request_param := p_request_param;
  l_r.cmnd_request_id := nextval( 'cmnd.cmnd_request_cmnd_request_id_seq' );
  l_r.request_time = now();
  l_r.account_id = p_id_account;

  select cmnd_ref_protocol, low_level_protocol
  into strict l_cmnd_ref_protocol, l_low_level_protocol
  from cmnd.cmnd_ref
  where cmnd_ref_id = l_r.cmnd_ref_id;

  select 'hub_'||hub_id
  into strict l_queue
  from core.unit
  where unit_id = l_r.unit_id;


  if l_low_level_protocol = cmnd.jf_cmnd_ref_pkg$cn_sms() then

        select phone_num
        into strict l_phone_num
        from core.sim_card2unit
          JOIN core.sim_card ON sim_card2unit.sim_card_id = sim_card.sim_card_id
        where equipment_id = l_r.unit_id;

        with a as (
                select key, value
                from json_each_text(l_r.request_param:: JSON)
                order by key
        )
        select string_agg(a.value, ',')
        into l_sms_text
        from  a;

        if l_sms_text is null then
            l_sms_text := l_cmnd_ref_protocol;
        else
            l_sms_text := l_cmnd_ref_protocol ||'=' ||l_sms_text;
        end if;

        l_cmd := json_build_object(
            'id', l_r.cmnd_request_id,
            'phone', l_phone_num,
            'text', l_sms_text
        )::text;

        perform paa.paa_api_pkg$basic_publish(cmnd.jf_cmnd_ref4unit_pkg$cn_command_rmq_broker(), 'sms-outgoing', '', l_cmd, 'SmsMessage');


  elsif l_low_level_protocol = cmnd.jf_cmnd_ref_pkg$cn_tcp_ip() then

    l_cmd := array_to_json(array_agg(json_build_object(
                                         'unitId', l_r.unit_id,
                                         'cmdId', l_r.cmnd_request_id,
                                         'timeSent', core.timestamp2json_string(l_r.request_time),
                                         'cmdName', l_cmnd_ref_protocol,
                                         'parameters', l_r.request_param
                                     )))::text;

        if p_with_delay then
            perform paa.paa_api_pkg$basic_publish(cmnd.jf_cmnd_ref4unit_pkg$cn_command_rmq_broker(),
                                                            'x-delayed',
                                                            ''::text,
                                                            l_cmd,
                                                            'COMMAND',
                                                            jsonb_build_object('x-delay', '10001')
                                                           );
        else
            perform paa.paa_api_pkg$basic_publish(cmnd.jf_cmnd_ref4unit_pkg$cn_command_rmq_broker(), ''::text, l_queue, l_cmd, 'COMMAND');

        end if;
  end if;

  insert into cmnd.cmnd_request select l_r.*;

  return l_r.cmnd_request_id;
end;
$$;

--**************************************************************************************************************
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd(p_id_account numeric, p_attr text) RETURNS text
LANGUAGE plpgsql
AS $$

declare
  l_r cmnd.cmnd_request%rowtype;
  l_unit_id_array int[];
  l_unit_id int;
begin
  l_r.unit_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_id', true);
  l_r.cmnd_ref_id := jofl.jofl_pkg$extract_number(p_attr, 'cmnd_ref_id', true);
  l_unit_id_array := jofl.jofl_pkg$extract_tarray(p_attr, 'parent_equipment_id_array', true)::int[];


  with a as(
        select array_agg(
            replace(
                replace(
                    substring(
                        upper(key)
                            from 7),
                     '_NUMBER', ''),
                 '_TEXT', '')
             ) k, array_agg(VALUE) v
        from json_each_text(p_attr::json)
        where replace(
                replace(
                    substring(
                        upper(key)
                            from 7),
                     '_NUMBER', ''),
                 '_TEXT', '') = any(ARRAY(select upper(param_protocol) from cmnd.cmnd_ref_param where cmnd_ref_id = l_r.cmnd_ref_id))
  )
  select json_object(k, v) into l_r.request_param from a;

  if l_unit_id_array is null then
    return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd_to_rabbit(p_id_account, l_r.unit_id, l_r.cmnd_ref_id, l_r.request_param::text)::text;
  else
      FOREACH l_unit_id IN ARRAY l_unit_id_array
      LOOP
        PERFORM  cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd_to_rabbit(p_id_account, l_unit_id, l_r.cmnd_ref_id, l_r.request_param::text);
      END LOOP;
  end if;

  return null;
end;
$$;


CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_set_pria(p_id_account numeric, p_attr text) RETURNS text
   LANGUAGE plpgsql
AS $$
declare
  l_r cmnd.cmnd_request%rowtype;
  l_ip text;
  l_port integer;
  l_unit_id_array int[];
  l_unit_id int;
begin
    return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd(p_id_account, p_attr);
 END
$$;


CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$check_response_sed_device_title_data(p_unit_id bigint, p_seconds_to_show int) RETURNS boolean
LANGUAGE plpgsql
AS $$
declare
    l_return int;
    l_last_res_time timestamp;
    l_last_request_time timestamp;
    NPH_SED_DEVICE_RESULT_RESPONSE text:= 'NPH_SED_DEVICE_RESULT';
    NPH_SED_DEVICE_TITLE_DATA_RESPONSE text := 'NPH_SED_DEVICE_TITLE_DATA';
    MAX_MESSAGE_RESPONSE_TIME_WAIT_SEC text := 'MAX_MESSAGE_RESPONSE_TIME_WAIT_SEC';
    NPH_SED_DEVICE_TITLE_DATA_REQUEST int := 19;
    l_time_sec_wait bigint;
    l_param json;
    BACKGROUND text := 'background';
    l_message_type text;
begin

        select max(request_time) last_time
        into l_last_request_time
        FROM cmnd.cmnd_request
        WHERE unit_id = p_unit_id AND cmnd_ref_id = NPH_SED_DEVICE_TITLE_DATA_REQUEST;

        --xpath использовать не получилось - искуственно испорчен XML
        --Цитата:
        -- орбита почему-то не любит, когда время пишут как time="10", а любит, когда пишут time=10
        -- граниту все равно
        select substring(request_param ->> 'MESSAGE', BACKGROUND)
        into l_message_type
        FROM cmnd.cmnd_request
        WHERE unit_id = p_unit_id AND cmnd_ref_id = NPH_SED_DEVICE_TITLE_DATA_REQUEST and request_time = l_last_request_time;


        SELECT count(*), max(response_time)
        into l_return, l_last_res_time
        from cmnd.cmnd_response res
        join cmnd.cmnd_request req on (res.cmnd_request_id = req.cmnd_request_id)
        where req.unit_id = p_unit_id AND req.cmnd_ref_id = NPH_SED_DEVICE_TITLE_DATA_REQUEST
        and response_name in (NPH_SED_DEVICE_RESULT_RESPONSE, NPH_SED_DEVICE_TITLE_DATA_RESPONSE)
        and req.request_time = l_last_request_time;

    if l_message_type = BACKGROUND then
        return 1;
    end if;

  --0 - блок не получил запрос
  --1 - блок получил запрос, но не выдал ответа
  --2 - блок ответил


  if l_return = 1 then
        return true; -- можно отправлять новое сообщение
  else
    l_time_sec_wait := asd.get_constant (MAX_MESSAGE_RESPONSE_TIME_WAIT_SEC);
    if now() > l_last_request_time + l_time_sec_wait * interval '1 seconds' then
        return true; -- можно отправлять новое сообщение
    else
        return false; -- более сложный вариант (http://redmine.advantum.ru/issues/21062)
    end if;
  end if;

  return l_return > 1;
END
$$;


CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_sed_device_title_data(p_id_account numeric, p_attr text) RETURNS text
LANGUAGE plpgsql
AS $$
DECLARE
  l_unit_id int;
  l_show_time int;
  l_user_choice int;
  FORCED_CALL int = 1;
  l_ret text;
BEGIN
  l_unit_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_id', true);
  l_show_time := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'F_SHOW_TIME_NUMBER', true), 30);
  l_user_choice := jofl.jofl_pkg$extract_number(p_attr, 'user_choice', true);
  -- проверим, что произошло с последним запросом и решим можно ли отправлять
  /*уже не актуально http://redmine.advantum.ru/issues/21062
  if cmnd.jf_cmnd_ref4unit_pkg$check_response_sed_device_title_data(l_unit_id, l_show_time) or l_user_choice = FORCED_CALL then
        if l_user_choice = FORCED_CALL then
            l_ret := cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_sed_device_title_data(p_id_account, p_attr, false);
            return cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_sed_device_title_data(p_id_account, p_attr, true);
        else
            return cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_sed_device_title_data(p_id_account, p_attr, false);
        end if;
  else
      return json_build_object('dialog', json_build_object(
                                 'title', 'Выбор пользовтеля',
                                 'msg', 'Предыдущее диалоговое сообщение, отправленное водителю, будет сброшено? Текст переданного ранее сообщения: “Текст предыдущего диалогового сообщения',
                                 'param_name', 'user_choice',
                                 'btn', array_to_json(ARRAY ['Отмена', 'Сбросить'])
                             ))::text;

  end if;
  */

    -- #23396 временно отключена проверка повторной отправки
--   if cmnd.jf_cmnd_ref4unit_pkg$check_response_sed_device_title_data(l_unit_id, l_show_time) then
            return cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_sed_device_title_data(p_id_account, p_attr, false);
--   else
--     RAISE  EXCEPTION '<<Отправка сообщений блокирована, пока не истекло время отображения предыдущего диалогового сообщения на блоке с поправкой на время получения подтверждения от блока о доставке сообщения, или пока от блока не будет получен ответ.>>';
--   end if;
END
$$;


CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_sed_device_title_data(p_id_account numeric, p_attr text, p_with_delay boolean) RETURNS text
LANGUAGE plpgsql
AS $$
declare
  l_r cmnd.cmnd_request%rowtype;
  l_message_xml xml;
  l_message_xml_text text;
  l_message_xml_a xml;
  l_message text;
  l_message_id integer;
  l_unit_id_array int[];
  l_unit_id int;
  l_msg_type text;
  l_show_time int;
  l_is_sound boolean;
  l_answers text;
  l_answers_array text[];
  l_answers_xml xml;
  i int = 0;
  x text;
  t text;
  l_cmnd_request_id bigint;
  l_last_request_state int;
  AMSG text;
begin
  l_r.unit_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_id', true);
  l_r.cmnd_ref_id := jofl.jofl_pkg$extract_number(p_attr, 'cmnd_ref_id', true);
  l_message := jofl.jofl_pkg$extract_varchar(p_attr, 'PARAM_MESSAGE_TEXT', false);
  l_unit_id_array := jofl.jofl_pkg$extract_tarray(p_attr, 'parent_equipment_id_array', true)::int[];


  l_msg_type := jofl.jofl_pkg$extract_varchar(p_attr, 'F_MSG_TYPE_INPLACE_K', false);
  l_show_time := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'F_SHOW_TIME_NUMBER', true), 30);
  l_is_sound := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_IS_SOUND_INPLACE_K', true) = 'yes', false);
  l_answers := jofl.jofl_pkg$extract_varchar(p_attr, 'STR_FIELD_ANSWERS_TEXT', true);


  l_message_xml := xmlelement(name "h2", l_message);
  if l_msg_type = 'normal' and l_answers is not null and l_answers != '' then
      l_answers_array := regexp_split_to_array(l_answers, ',');

      foreach x in array l_answers_array
      loop
        i = i + 1;
        l_answers_xml = xmlconcat(l_answers_xml, cmnd.dynamic_xml_tag('btn'||i, x::xml));
      end loop;

      l_message_xml = xmlconcat(l_message_xml, l_answers_xml);
  end if;


  if l_unit_id_array is null then
      l_unit_id_array := array[l_r.unit_id];
  end if;

    FOREACH l_unit_id IN ARRAY l_unit_id_array
    LOOP

                  l_message_id := nextval( 'cmnd.nph_sed_device_title_data_message_id_seq' );

                  l_message_xml_a := xmlelement(name "NAVSCR", xmlattributes (1.0 as ver),
                                      xmlelement(name "ID", l_message_id),
                                      xmlelement(name "FROM", 'SERVER'),
                                      xmlelement(name "TO", 'USER'),
                                      xmlelement(name "TYPE", 'QUERY'),
                                      xmlelement(name "MSG", xmlattributes(l_show_time as time, l_msg_type as type, l_is_sound::int as beep  ), l_message_xml));

                  l_message_xml_text := l_message_xml_a::text;
                  -- орбита почему-то не любит, когда время пишут как time="10", а любит, когда пишут time=10
                  -- граниту все равно

                  l_message_xml_text := regexp_replace(l_message_xml_text, 'time="([^"]+)"', 'time=\1');
                  l_message_xml_text := regexp_replace(l_message_xml_text, 'beep="([^"]+)"', 'beep=\1');
                  l_message_xml_text := replace(l_message_xml_text, E'\n', '<br/>');

                  l_r.request_param = json_build_object(
                      'ADDRESS_FROM_NUM', 0,
                      'ADDRESS_TO_NUM', 0,
                      'ADDRESS_FROM_TYPE', 'NPH_SED_ADDR_DISPLAY_DEVICE',
                      'ADDRESS_TO_TYPE', 'NPH_SED_ADDR_INTERNAL_DISPLAY',
                      'TYPE_DATA', 'NPH_SED_TYPE_MSG_SCRIPT_DISPLAY',
                      'MESSAGE', l_message_xml_text,
                      'MESSAGE_ID', l_message_id
                  )::text;

                 l_cmnd_request_id :=  cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd_to_rabbit(p_id_account, l_unit_id, l_r.cmnd_ref_id, l_r.request_param::text, p_with_delay);

                insert into cmnd.message_to_request(
                    cmnd_request_id,
                    message_id,
                    request_time
                ) values (
                    l_cmnd_request_id,
                    l_message_id,
                    now()
                );
    END LOOP;
    return null;
END
$$;


CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_sgc_service_request (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
 declare
    l_r cmnd.cmnd_request%rowtype;
    l_param text;
    l_unit_id_array int[];
    l_unit_id int;
 BEGIN
    l_param := jofl.jofl_pkg$extract_varchar(p_attr, 'F_param_ref_val', false);
    l_r.unit_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_id', true);
    l_r.cmnd_ref_id := jofl.jofl_pkg$extract_number(p_attr, 'cmnd_ref_id', true);
    l_unit_id_array := jofl.jofl_pkg$extract_tarray(p_attr, 'parent_equipment_id_array', true)::int[];

    l_r.request_param = json_build_object(
          'DATA', l_param
          )::text;

  if l_unit_id_array is null then
    return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd_to_rabbit(p_id_account, l_r.unit_id, l_r.cmnd_ref_id, l_r.request_param::text)::text;
  else
      FOREACH l_unit_id IN ARRAY l_unit_id_array
      LOOP
        PERFORM  cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd_to_rabbit(p_id_account, l_unit_id, l_r.cmnd_ref_id, l_r.request_param::text)::text;
      END LOOP;
  end if;
  return null;
 END
$$;


CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_set_loadfirm (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
 declare
    l_r cmnd.cmnd_request%rowtype;
    l_r_firmware core.firmware%rowtype;
    l_firmware_id int;
    l_unit_id_array int[];
    l_unit_id int;
 BEGIN
    l_firmware_id := jofl.jofl_pkg$extract_number(p_attr, 'f_firmware_id', false);
    l_r.unit_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_id', true);
    l_r.cmnd_ref_id := jofl.jofl_pkg$extract_number(p_attr, 'cmnd_ref_id', true);
    l_unit_id_array := jofl.jofl_pkg$extract_tarray(p_attr, 'parent_equipment_id_array', true)::int[];

    select * into strict l_r_firmware from core.firmware where firmware_id = l_firmware_id;

    l_r.request_param = json_build_object(
            'IP_ADDRESS', l_r_firmware.ip_address,
            'PORT', l_r_firmware.port,
            'URL', l_r_firmware.url_ftp,
            'DIR_FILE', l_r_firmware.path_ftp,
            'FILE_NAME', l_r_firmware.file_name,
            'USER', l_r_firmware.ftp_user,
            'PASSWORD', l_r_firmware.ftp_user_password
          )::text;

  if l_unit_id_array is null then
    return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd_to_rabbit(p_id_account, l_r.unit_id, l_r.cmnd_ref_id, l_r.request_param::text);
  else
      FOREACH l_unit_id IN ARRAY l_unit_id_array
      LOOP
        PERFORM  cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd_to_rabbit(p_id_account, l_unit_id, l_r.cmnd_ref_id, l_r.request_param::text);
      END LOOP;
  end if;
  return null;
 END
$$;

CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_bb_getnavinfo (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
 BEGIN return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd(p_id_account, p_attr); 
            END  
$$;


CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_bb_prlfirm(p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
 declare
    l_r cmnd.cmnd_request%rowtype;
    l_r_firmware core.firmware%rowtype;
    l_firmware_id int;
    l_unit_id_array int[];
    l_unit_id int;
 BEGIN
    l_firmware_id := jofl.jofl_pkg$extract_number(p_attr, 'f_firmware_id', false);
    l_r.unit_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_id', true);
    l_r.cmnd_ref_id := jofl.jofl_pkg$extract_number(p_attr, 'cmnd_ref_id', true);
    l_unit_id_array := jofl.jofl_pkg$extract_tarray(p_attr, 'parent_equipment_id_array', true)::int[];

    select * into strict l_r_firmware from core.firmware where firmware_id = l_firmware_id;

    l_r.request_param = json_build_object(
            '1.ADDSERVER', coalesce(l_r_firmware.ip_address, l_r_firmware.url_ftp),
            '2.PORT', l_r_firmware.port,
            '3.DIRNAME', l_r_firmware.path_ftp,
            '4.FILENAME', l_r_firmware.file_name,
            '5.USER', l_r_firmware.ftp_user,
            '6.PASSWORD', l_r_firmware.ftp_user_password
          )::text;

  if l_unit_id_array is null then
    return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd_to_rabbit(p_id_account, l_r.unit_id, l_r.cmnd_ref_id, l_r.request_param::text)::text;
  else
      FOREACH l_unit_id IN ARRAY l_unit_id_array
      LOOP
        PERFORM  cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd_to_rabbit(p_id_account, l_unit_id, l_r.cmnd_ref_id, l_r.request_param::text);
      END LOOP;
  end if;
  return null;
 END
$$;


CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_bb_prnav (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
 BEGIN return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd(p_id_account, p_attr); 
            END  
$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_get_balance (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
 BEGIN return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd(p_id_account, p_attr); 
            END  
$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_get_info (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
 BEGIN return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd(p_id_account, p_attr); 
            END  
$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_get_prdo (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
 BEGIN return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd(p_id_account, p_attr); 
            END  
$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_get_pria (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
 BEGIN return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd(p_id_account, p_attr); 
            END  
$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_get_pria_ext (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
 BEGIN return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd(p_id_account, p_attr); 
            END  
$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_get_prnav (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
 BEGIN return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd(p_id_account, p_attr); 
            END  
$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_get_route_autoinformer (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
 BEGIN return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd(p_id_account, p_attr); 
            END  
$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_get_sim_imsi (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
 BEGIN return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd(p_id_account, p_attr); 
            END  
$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_reset_int_state (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
 BEGIN return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd(p_id_account, p_attr); 
            END  
$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_set_curtime (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
 BEGIN return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd(p_id_account, p_attr); 
            END  
$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_set_prdo (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
 BEGIN return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd(p_id_account, p_attr); 
            END  
$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_set_pria (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
 BEGIN return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd(p_id_account, p_attr); 
            END  
$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_set_prnav (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
 BEGIN return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd(p_id_account, p_attr); 
            END  
$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_set_route_autoinformer (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
 BEGIN return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd(p_id_account, p_attr); 
            END  
$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$of_autogen_3_nph_sgc_peer_desc_request (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$
 BEGIN return cmnd.jf_cmnd_ref4unit_pkg$of_send_cmd(p_id_account, p_attr); 
            END  
$$;

CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref4unit_pkg$cn_command_rmq_broker()
  RETURNS smallint AS
  'select 1::smallint;'
LANGUAGE SQL IMMUTABLE;

--drop FUNCTION cmnd.dynamic_xml_tag(name text, val text);

CREATE OR REPLACE FUNCTION cmnd.dynamic_xml_tag(nam text, val xml)
 RETURNS xml
 LANGUAGE plpgsql
AS $function$
    declare result text;
    begin
        execute format('SELECT xmlelement(name %I, $1)', nam) USING val INTO result;
        return result;
    end;
$function$
