CREATE OR REPLACE FUNCTION cmnd.jf_response_ref_pkg$of_update (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.response_ref%rowtype;
begin
   l_r := cmnd.jf_response_ref_pkg$attr_to_rowtype(p_attr);

   update cmnd.response_ref set
          response_ref_name = l_r.response_ref_name,
          response_ref_protocol = l_r.response_ref_protocol,
          order_num = l_r.order_num
   where
          response_ref_id = l_r.response_ref_id;

   return null;
end;

$$;
CREATE OR REPLACE FUNCTION cmnd.jf_response_ref_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$

declare
    l_r cmnd.response_ref%rowtype;
begin

 l_r.cmnd_ref_id := jofl.jofl_pkg$extract_number(p_attr, 'cmnd_ref_id', true);

 open p_rows for
      select
        response_ref_name,
        response_ref_protocol,
        response_ref_id,
        order_num,
        cmnd_ref_id
      from cmnd.response_ref
      where cmnd_ref_id = l_r.cmnd_ref_id;
end;

$$;
CREATE OR REPLACE FUNCTION cmnd.jf_response_ref_pkg$of_insert (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.response_ref%rowtype;
begin
   l_r := cmnd.jf_response_ref_pkg$attr_to_rowtype(p_attr);


   l_r.response_ref_id := nextval( 'cmnd.response_ref_response_ref_id_seq' );

   insert into cmnd.response_ref select l_r.*;

   return null;
end;

$$;
CREATE OR REPLACE FUNCTION cmnd.jf_response_ref_pkg$of_delete (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.response_ref%rowtype;
begin
   l_r := cmnd.jf_response_ref_pkg$attr_to_rowtype(p_attr);

   delete from  cmnd.response_ref where  response_ref_id = l_r.response_ref_id;

   return null;
end;

$$;
CREATE OR REPLACE FUNCTION cmnd.jf_response_ref_pkg$attr_to_rowtype (p_attr text) RETURNS cmnd.response_ref
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.response_ref%rowtype;
begin
   l_r.response_ref_name := jofl.jofl_pkg$extract_varchar(p_attr, 'response_ref_name', true);
   l_r.response_ref_protocol := jofl.jofl_pkg$extract_varchar(p_attr, 'response_ref_protocol', true);
   l_r.response_ref_id := jofl.jofl_pkg$extract_number(p_attr, 'response_ref_id', true);
   l_r.order_num := jofl.jofl_pkg$extract_number(p_attr, 'order_num', true);
   l_r.cmnd_ref_id := jofl.jofl_pkg$extract_number(p_attr, 'cmnd_ref_id', true);

   return l_r;
end;

$$;