-- DROP FUNCTION cmnd.jf_message_template_pkg$of_rows(NUMERIC, TEXT);
create or replace function cmnd.jf_message_template4select_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare 
begin 
 open p_rows for
  select mt.contents,
         mt.code,
         mt.display_duration || ' сек.' as display_duration,
         mt.message_template_id,
         mt.is_sound,
         mt.answer_type
  from cmnd.jf_message_template_pkg$get_rows(p_id_account, p_attr) mt
  order by mt.code nulls last, mt.message_template_id;
end;
$$;
