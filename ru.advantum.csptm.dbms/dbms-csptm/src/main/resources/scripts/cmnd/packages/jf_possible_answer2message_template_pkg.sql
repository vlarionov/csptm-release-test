-- DROP FUNCTION cmnd.jf_possible_answer2message_template_pkg$attr_to_rowtype(TEXT);
create or replace function cmnd.jf_possible_answer2message_template_pkg$attr_to_rowtype(p_attr text) returns cmnd.possible_answer2message_template
LANGUAGE plpgsql
AS $$
declare 
   l_r cmnd.possible_answer2message_template%rowtype; 
begin 
   l_r.order_num := jofl.jofl_pkg$extract_varchar(p_attr, 'order_num', true); 
   l_r.possible_answer2message_template_id := jofl.jofl_pkg$extract_varchar(p_attr, 'possible_answer2message_template_id', true); 
   l_r.message_template_id := jofl.jofl_pkg$extract_varchar(p_attr, 'message_template_id', true); 
   l_r.possible_answer_id := jofl.jofl_pkg$extract_varchar(p_attr, 'possible_answer_id', true); 

   return l_r;
end;
$$;

-- DROP FUNCTION cmnd.jf_possible_answer2message_template_pkg$of_rows(NUMERIC, TEXT);
create or replace function cmnd.jf_possible_answer2message_template_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
 l_message_template_id smallint :=  jofl.jofl_pkg$extract_number(p_attr, 'message_template_id', true);
begin
 open p_rows for
  select dt.order_num,
         dt.possible_answer2message_template_id,
         dt.message_template_id,
         dt.possible_answer_id,
		     dt.pa_response,
         dt.order_min,
         dt.order_max,
         dt.rec_count,
         replace(
          case
           when dt.order_num > dt.order_min then 'P{A},D{OF_MOVE_UP}'
           else ''
          end||
          case
           when dt.order_num < dt.order_max then 'P{A},D{OF_MOVE_DOWN}'
           else ''
          end, '}P{', '},P{') as "ROW$POLICY"
  from (select pa2mt.order_num,
               pa2mt.possible_answer2message_template_id,
               pa2mt.message_template_id,
               pa2mt.possible_answer_id,
		           pa.response as pa_response,
               min(pa2mt.order_num) over (partition by pa2mt.message_template_id) as order_min,
               max(pa2mt.order_num) over (partition by pa2mt.message_template_id) as order_max,
               count(1) over (partition by pa2mt.message_template_id) as rec_count
        from cmnd.possible_answer2message_template pa2mt
             left join cmnd.possible_answer pa on pa.possible_answer_id = pa2mt.possible_answer_id
        where pa2mt.message_template_id = l_message_template_id) dt
      order by dt.message_template_id, dt.order_num;
end;
$$;

-- DROP FUNCTION cmnd.jf_possible_answer2message_template_pkg$of_insert(NUMERIC, TEXT);
create or replace function cmnd.jf_possible_answer2message_template_pkg$of_insert(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r cmnd.possible_answer2message_template%rowtype;
   l_rec_count smallint;
begin 
   l_r := cmnd.jf_possible_answer2message_template_pkg$attr_to_rowtype(p_attr);
   l_r.possible_answer2message_template_id := nextval( 'cmnd.possible_answer2message_template_id_seq' );
   
   select coalesce(max(pa2mt.order_num), 0)+1, count(1) into l_r.order_num, l_rec_count
    from cmnd.possible_answer2message_template pa2mt
    where   pa2mt.message_template_id = l_r.message_template_id;

   if l_rec_count >= 3 then
    raise exception '<<Достигнуто максимальное количество возможных ответов!>>';
   end if;

   insert into cmnd.possible_answer2message_template select l_r.*;

   return null;
end;
$$;

-- DROP FUNCTION cmnd.jf_possible_answer2message_template_pkg$of_update(NUMERIC, TEXT);
create or replace function cmnd.jf_possible_answer2message_template_pkg$of_update(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r cmnd.possible_answer2message_template%rowtype;
begin 
   l_r := cmnd.jf_possible_answer2message_template_pkg$attr_to_rowtype(p_attr);

   update cmnd.possible_answer2message_template set 
          possible_answer_id = l_r.possible_answer_id
   where 
          possible_answer2message_template_id = l_r.possible_answer2message_template_id;

   return null;
end;
$$;

create or replace function cmnd.jf_possible_answer2message_template_pkg$do_reorder(p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
 l_message_template_id smallint := jofl.jofl_pkg$extract_number(p_attr, 'message_template_id', true);
 l_order_num smallint := jofl.jofl_pkg$extract_number(p_attr, 'order_num', true);
 l_edit_key smallint := sign(jofl.jofl_pkg$extract_number(p_attr, 'edit_key', true));
 l_order_min smallint := jofl.jofl_pkg$extract_number(p_attr, 'order_min', true);
 l_order_max smallint := jofl.jofl_pkg$extract_number(p_attr, 'order_max', true);
 l_check_key smallint;
begin
 /* проверяем наличие редактируемой записи */
 select count(1) into l_check_key
  from cmnd.possible_answer2message_template pa2mt
  where pa2mt.message_template_id = l_message_template_id and
         pa2mt.order_num = l_order_num;

 /* удаление */
 if l_edit_key = 0 and coalesce(l_check_key, 0) = 0 then
  update cmnd.possible_answer2message_template pa2mt
   set order_num = order_num - 1
   where pa2mt.message_template_id = l_message_template_id and
         pa2mt.order_num > l_order_num;
 end if;

 /* перемещение вверх / вниз по списку */
 if coalesce(l_check_key, 0) != 0 and
    ((l_edit_key = -1 and l_order_num > l_order_min) or         -- перемещение вверх, кроме первой строки
     (l_edit_key =  1 and l_order_num < l_order_max))           -- перемещение вниз,  кроме последней строки
 then
  -- одной командой меняем местами две строки - изменяемую и соседнюю (если она есть)
  update cmnd.possible_answer2message_template pa2mt
   set order_num = case
                    when order_num = l_order_num              then order_num + l_edit_key
                    when order_num = l_order_num + l_edit_key then order_num - l_edit_key
                    else order_num
                   end
   where pa2mt.message_template_id = l_message_template_id and
         pa2mt.order_num in (l_order_num, l_order_num + l_edit_key);
end if;

 return null;
end;
$$;

-- DROP FUNCTION cmnd.jf_possible_answer2message_template_pkg$of_delete(NUMERIC, TEXT);
create or replace function cmnd.jf_possible_answer2message_template_pkg$of_delete(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r cmnd.possible_answer2message_template%rowtype;
   l_attr text;
   l_res text;
begin 
   l_r := cmnd.jf_possible_answer2message_template_pkg$attr_to_rowtype(p_attr);

   delete from cmnd.possible_answer2message_template
    where possible_answer2message_template_id = l_r.possible_answer2message_template_id;

   l_attr := (p_attr::jsonb || jsonb_build_object('edit_key', 0))::text;
   l_res := cmnd.jf_possible_answer2message_template_pkg$do_reorder(l_attr);
   return null;
end;
$$;

-- DROP FUNCTION cmnd.jf_possible_answer2message_template_pkg$of_move_up(NUMERIC, TEXT);
create or replace function cmnd.jf_possible_answer2message_template_pkg$of_move_up(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
 l_attr text;
 l_res text;
begin 
  l_attr := (p_attr::jsonb || jsonb_build_object('edit_key', -1))::text;
  l_res := cmnd.jf_possible_answer2message_template_pkg$do_reorder(l_attr);
  return null;
end;
$$;

-- DROP FUNCTION cmnd.jf_possible_answer2message_template_pkg$of_move_down(NUMERIC, TEXT);
create or replace function cmnd.jf_possible_answer2message_template_pkg$of_move_down(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
 l_attr text;
 l_res text;
begin 
  l_attr := (p_attr::jsonb || jsonb_build_object('edit_key', 1))::text;
  l_res := cmnd.jf_possible_answer2message_template_pkg$do_reorder(l_attr);
  return null;
end;
$$;
