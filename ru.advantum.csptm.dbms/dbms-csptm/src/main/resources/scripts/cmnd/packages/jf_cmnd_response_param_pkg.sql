CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_response_param_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.cmnd_request%rowtype;
begin
   l_r.cmnd_request_id := jofl.jofl_pkg$extract_number(p_attr, 'cmnd_request_id', true);

 open p_rows for
select
  key param_protokol,
  response_ref_attr.response_attr_name param_name,
  case
    when key = 'error' then
        case value
            when '0' then 'NPL_ERR_OK'
            when '1' then 'NPL_ERR_DECRYPTION_FAILED'
            when '2' then 'NPL_ERR_CRC_FAILED'
            when '203' then 'NPH_RESULT_PACKET_INVALID_PARAMETER'
        end
     when (key = 'lat' or key = 'lon') then round(((value::numeric)/10000000)::numeric, 6)::text
  else value
  end param_value,
  cmnd_response.cmnd_request_id
from cmnd.cmnd_response
  JOIN cmnd.cmnd_request on (cmnd_response.cmnd_request_id = cmnd_request.cmnd_request_id)
  JOIN cmnd.response_ref on (response_ref.cmnd_ref_id = cmnd_request.cmnd_ref_id)
  JOIN cmnd.response_ref_attr on (response_ref_attr.response_ref_id = response_ref.response_ref_id)
  join jsonb_each_text(cmnd.cmnd_response.response_param) on (lower(response_attr_protokol) = lower(key))
    where cmnd_request.cmnd_request_id = l_r.cmnd_request_id;
end;

$$;




