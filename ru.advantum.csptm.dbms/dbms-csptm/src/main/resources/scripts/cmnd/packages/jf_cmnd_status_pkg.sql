CREATE OR replace FUNCTION cmnd.jf_cmnd_status_pkg$attr_to_rowtype (p_attr text) RETURNS cmnd.cmnd_status
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.cmnd_status%rowtype;
begin
   l_r.cmnd_status_id := jofl.jofl_pkg$extract_varchar(p_attr, 'cmnd_status_id', true);
   l_r.cmnd_status_name := jofl.jofl_pkg$extract_varchar(p_attr, 'cmnd_status_name', true);

   return l_r;
end;

$$;

CREATE OR replace FUNCTION cmnd.jf_cmnd_status_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$

declare
begin
 open p_rows for
      select
        cmnd_status_id,
        cmnd_status_name
      from cmnd.cmnd_status;
end;

$$;

CREATE OR replace FUNCTION cmnd.jf_cmnd_status_pkg$of_update (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.cmnd_status%rowtype;
begin
   l_r := cmnd.jf_cmnd_status_pkg$attr_to_rowtype(p_attr);

   update cmnd.cmnd_status set
          cmnd_status_name = l_r.cmnd_status_name
   where
          cmnd_status_id = l_r.cmnd_status_id;

   return null;
end;

$$;


CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_status_pkg$cn_status_sended()
  RETURNS smallint AS
  'select 1::smallint;'
LANGUAGE SQL IMMUTABLE;


CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_status_pkg$cn_status_sys_error()
  RETURNS smallint AS
  'select 3::smallint;'
LANGUAGE SQL IMMUTABLE;