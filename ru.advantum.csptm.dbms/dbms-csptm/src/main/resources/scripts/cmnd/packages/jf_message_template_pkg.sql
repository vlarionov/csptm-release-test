-- DROP FUNCTION cmnd.jf_message_template_pkg$attr_to_rowtype(TEXT);
create or replace function cmnd.jf_message_template_pkg$attr_to_rowtype(p_attr text) returns cmnd.message_template
LANGUAGE plpgsql
AS $$
declare 
   l_r cmnd.message_template%rowtype; 
begin 
   l_r.contents := jofl.jofl_pkg$extract_varchar(p_attr, 'contents', true); 
   l_r.code := jofl.jofl_pkg$extract_varchar(p_attr, 'code', true); 
   l_r.display_duration := jofl.jofl_pkg$extract_varchar(p_attr, 'display_duration', true); 
   l_r.message_template_id := jofl.jofl_pkg$extract_varchar(p_attr, 'message_template_id', true); 
   l_r.is_sound := jofl.jofl_pkg$extract_boolean(p_attr, 'is_sound', true);
   return l_r;
end;
$$;

drop function cmnd.jf_message_template_pkg$get_rows(numeric, text);
create or replace function cmnd.jf_message_template_pkg$get_rows(p_id_account numeric, p_attr text)
/* функция возвращает данные "Формализованнных сообщений" для всех случаев - справочника, выбора, отправки сообщения */
RETURNS TABLE (contents text,
               code smallint,
               display_duration smallint,
               message_template_id smallint,
               is_sound boolean,
               answer_type text,
               message_type_eng text,
               message_type text,			   
               answer_list text) AS
$body$
declare
 l_message_template_id smallint;
begin
 if coalesce(p_attr, '') != '' then
  l_message_template_id := (jofl.jofl_pkg$extract_number(p_attr, 'message_template_id', true))::smallint;
 end if;
 return query
  select mt.contents,
         mt.code,
         mt.display_duration,
         mt.message_template_id,
         mt.is_sound,
         case
          when coalesce(mta.answer_list, '') = '' then 'Фоновое'
          else 'Диалоговое'||' ('||mta.answer_list||')'
         end as answer_type,
         case
          when coalesce(mta.answer_list, '') = '' then 'background'
          else 'normal'
         end as message_type_eng,
         case
          when coalesce(mta.answer_list, '') = '' then 'Фоновое'
          else 'Диалоговое'
         end as message_type,
         mta.answer_list
  from cmnd.message_template mt
       left join (select distinct
                         pa2mt.message_template_id,
                         array_to_string(array_agg(pa.response) over (partition by pa2mt.message_template_id order by pa2mt.order_num rows BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING), ',') as answer_list
                  from cmnd.possible_answer2message_template pa2mt
                       join cmnd.possible_answer pa on pa.possible_answer_id = pa2mt.possible_answer_id) mta on mta.message_template_id = mt.message_template_id
  where mt.message_template_id = coalesce(l_message_template_id, mt.message_template_id);
end;
$body$
LANGUAGE 'plpgsql';
-- ---------------------------------

-- DROP FUNCTION cmnd.jf_message_template_pkg$of_rows(NUMERIC, TEXT);
create or replace function cmnd.jf_message_template_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare 
begin 
 open p_rows for
  select mt.contents,
         mt.code,
         mt.display_duration,
         mt.message_template_id,
         mt.is_sound,
         message_type as mt_type
  from cmnd.jf_message_template_pkg$get_rows(p_id_account, p_attr) mt
  order by mt.code nulls last, mt.message_template_id;
end;
$$;

-- DROP FUNCTION cmnd.jf_message_template_pkg$of_insert(NUMERIC, TEXT);
create or replace function cmnd.jf_message_template_pkg$of_insert(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r cmnd.message_template%rowtype;
   l_max_code smallint;
begin 
   l_r := cmnd.jf_message_template_pkg$attr_to_rowtype(p_attr);

   select count(mt.code)+1
    into l_max_code
    from cmnd.message_template mt;

   l_r.message_template_id := nextval( 'cmnd.message_template_id_seq' );
   l_r.code := coalesce (l_r.code, l_max_code);

   insert into cmnd.message_template select l_r.*;
   return null;
end;
$$;

-- DROP FUNCTION cmnd.jf_message_template_pkg$of_update(NUMERIC, TEXT);
create or replace function cmnd.jf_message_template_pkg$of_update(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r cmnd.message_template%rowtype;
begin 
   l_r := cmnd.jf_message_template_pkg$attr_to_rowtype(p_attr);

   update cmnd.message_template set 
          contents = l_r.contents, 
          code = l_r.code, 
          display_duration = l_r.display_duration,
          is_sound = l_r.is_sound
   where message_template_id = l_r.message_template_id;

   return null;
end;
$$;

-- DROP FUNCTION cmnd.jf_message_template_pkg$of_delete(NUMERIC, TEXT);
create or replace function cmnd.jf_message_template_pkg$of_delete(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r cmnd.message_template%rowtype;
begin 
   l_r := cmnd.jf_message_template_pkg$attr_to_rowtype(p_attr);

   delete from  cmnd.message_template where  message_template_id = l_r.message_template_id;

   return null;
end;
$$;
