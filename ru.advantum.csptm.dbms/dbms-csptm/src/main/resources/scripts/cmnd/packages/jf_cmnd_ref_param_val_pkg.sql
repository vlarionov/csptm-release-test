CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_param_val_pkg$attr_to_rowtype (p_attr text) RETURNS cmnd.cmnd_ref_param_val
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.cmnd_ref_param_val%rowtype;
begin
   l_r.param_ref_val_name := jofl.jofl_pkg$extract_varchar(p_attr, 'param_ref_val_name', true);
   l_r.param_ref_val := jofl.jofl_pkg$extract_varchar(p_attr, 'param_ref_val', true);
   l_r.cmnd_ref_param_id := jofl.jofl_pkg$extract_number(p_attr, 'cmnd_ref_param_id', true);
   l_r.cmnd_ref_param_val_id := jofl.jofl_pkg$extract_number(p_attr, 'cmnd_ref_param_val_id', true);

   return l_r;
end;

$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_param_val_pkg$of_delete (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.cmnd_ref_param_val%rowtype;
begin
   l_r := cmnd.jf_cmnd_ref_param_val_pkg$attr_to_rowtype(p_attr);

   delete from  cmnd.cmnd_ref_param_val where  cmnd_ref_param_id = l_r.cmnd_ref_param_id and
 cmnd_ref_param_val_id = l_r.cmnd_ref_param_val_id;

   return null;
end;

$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_param_val_pkg$of_insert (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.cmnd_ref_param_val%rowtype;
begin
   l_r := cmnd.jf_cmnd_ref_param_val_pkg$attr_to_rowtype(p_attr);
   l_r.cmnd_ref_param_val_id := nextval( 'cmnd.cmnd_ref_param_val_cmnd_ref_param_val_id_seq' );
   insert into cmnd.cmnd_ref_param_val select l_r.*;

   return null;
end;

$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_param_val_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$
declare
    l_cmnd_ref_param_id int := jofl.jofl_pkg$extract_number(p_attr, 'cmnd_ref_param_id', true);
    l_cmnd_ref_id int := jofl.jofl_pkg$extract_number(p_attr, 'cmnd_ref_id', true);
    --":"NPH_SGC_SERVICE_REQUEST","cmnd_ref_id":"15"
begin
    if l_cmnd_ref_id = 15 then
         open p_rows for
              select
                param_ref_val_name,
                param_ref_val,
                cmnd_ref_param_val.cmnd_ref_param_id,
                cmnd_ref_param_val_id
              from cmnd.cmnd_ref_param_val
                   join cmnd.cmnd_ref_param on (cmnd_ref_param.cmnd_ref_param_id = cmnd_ref_param_val.cmnd_ref_param_id)
              where  cmnd_ref_param.cmnd_ref_id = l_cmnd_ref_id;
    else
         open p_rows for
              select
                param_ref_val_name,
                param_ref_val,
                cmnd_ref_param_id,
                cmnd_ref_param_val_id
              from cmnd.cmnd_ref_param_val
              where cmnd_ref_param_id = l_cmnd_ref_param_id;
    end if;
end;

$$;
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_ref_param_val_pkg$of_update (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.cmnd_ref_param_val%rowtype;
begin
   l_r := cmnd.jf_cmnd_ref_param_val_pkg$attr_to_rowtype(p_attr);

   update cmnd.cmnd_ref_param_val set
          param_ref_val_name = l_r.param_ref_val_name,
          param_ref_val = l_r.param_ref_val
   where
          cmnd_ref_param_id = l_r.cmnd_ref_param_id and
          cmnd_ref_param_val_id = l_r.cmnd_ref_param_val_id;

   return null;
end;

$$;