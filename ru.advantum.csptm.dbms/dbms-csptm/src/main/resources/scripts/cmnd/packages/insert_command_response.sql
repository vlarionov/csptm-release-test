CREATE OR REPLACE FUNCTION cmnd.insert_command_response(
p_cmnd_request_id bigint,
p_response_name text,
p_response_time timestamp,
p_response_param text
) RETURNS bigint
	LANGUAGE plpgsql
AS $$
declare
    l_cmnd_response_id bigint;
    l_message_id int;
    l_cmnd_request_id bigint;
    l_unit_id bigint;
    l_cmnd_ref_id int;
    NPH_GET_INFO int := 8;
    NPH_GET_SIM_IMSI int := 14;
    NPH_RESULT text := 'NPH_RESULT';
    l_ver_data text;
    l_error text;
begin
    select cmnd.parse_bnst_response(p_response_param::json, 'MESSAGE_ID')::int into l_message_id;

    if l_message_id is not null then
        select cmnd_request_id
        into l_cmnd_request_id
        from cmnd.message_to_request
        where message_id = l_message_id and request_time between now() - interval '1 hour' and now() + interval '1 hour';
    else
       l_cmnd_request_id = p_cmnd_request_id;
    end if;

    declare
        l_check bigint;
    begin
        select cmnd_request_id
        into strict l_check
        from cmnd.cmnd_request
        where cmnd_request_id = l_cmnd_request_id;
    exception
        when no_data_found then
            --ответ на незаданный вопрос
            return 0;
    end;


    insert into cmnd.cmnd_response(cmnd_request_id, response_time, response_name, response_param)
    values (l_cmnd_request_id, p_response_time, p_response_name, p_response_param::json) RETURNING cmnd_response_id id INTO l_cmnd_response_id;


    update cmnd.cmnd_request
    set cmnd_status_id = cmnd.jf_cmnd_status_pkg$cn_status_sended()
    where cmnd_request_id = l_cmnd_request_id;

    select unit_id,cmnd_ref_id
    into l_unit_id, l_cmnd_ref_id
    from cmnd.cmnd_request
    where cmnd_request_id = l_cmnd_request_id;

    if l_cmnd_ref_id = NPH_GET_INFO then
        select cmnd.parse_bnst_response(p_response_param::json, 'data_ver') into l_ver_data;

        if l_ver_data is not null then
            update core.unit_bnst
            set data_ver = l_ver_data
            where unit_bnst_id = l_unit_id;
        end if;
    elsif l_cmnd_ref_id = NPH_GET_SIM_IMSI then

        if p_response_name = 'NPH_RESULT' then
            select cmnd.parse_bnst_response(p_response_param::json, 'error') into l_ver_data;

            if l_ver_data != '0' then
                update core.sim_card
                set imsi = 'Ошибка от блока'
                where sim_card_id in (select sim_card_id from core.sim_card2unit where equipment_id = l_unit_id);
            end if;

        else

            select cmnd.parse_bnst_response(p_response_param::json, 'imsi') into l_ver_data;

            if l_ver_data is not null then
                update core.sim_card
                set imsi = l_ver_data
                where sim_card_id in (select sim_card_id from core.sim_card2unit where equipment_id = l_unit_id);
            end if;
        end if;
    end if;

    return l_cmnd_response_id;
end;
$$;