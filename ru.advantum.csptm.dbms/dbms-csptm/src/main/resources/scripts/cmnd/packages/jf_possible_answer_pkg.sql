-- DROP FUNCTION cmnd.jf_possible_answer_pkg$attr_to_rowtype(TEXT);
create or replace function cmnd.jf_possible_answer_pkg$attr_to_rowtype(p_attr text) returns cmnd.possible_answer
LANGUAGE plpgsql
AS $$
declare 
   l_r cmnd.possible_answer%rowtype; 
begin 
   l_r.response := jofl.jofl_pkg$extract_varchar(p_attr, 'response', true); 
   l_r.possible_answer_id := jofl.jofl_pkg$extract_varchar(p_attr, 'possible_answer_id', true); 
   return l_r;
end;
$$;

-- DROP FUNCTION cmnd.jf_possible_answer_pkg$of_rows(NUMERIC, TEXT);
create or replace function cmnd.jf_possible_answer_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
 l_all_recs boolean := true;
 l_rf_db_method text;
 l_message_template_id smallint;
begin

 l_rf_db_method := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true);
 if coalesce(l_rf_db_method, '') = 'cmnd.possible_answer2message_template' then
  l_message_template_id := (jofl.jofl_pkg$extract_varchar(p_attr, 'message_template_id', true))::smallint;
  l_all_recs := false;
 end if;

 open p_rows for 
      select pa.response,
             pa.possible_answer_id
      from cmnd.possible_answer pa
      where l_all_recs or
            (not l_all_recs and
             pa.possible_answer_id not in (select pa2mt.possible_answer_id
                                           from cmnd.possible_answer2message_template pa2mt
                                           where pa2mt.message_template_id = l_message_template_id))
      order by pa.possible_answer_id;
end;
$$;

-- DROP FUNCTION cmnd.jf_possible_answer_pkg$of_insert(NUMERIC, TEXT);
create or replace function cmnd.jf_possible_answer_pkg$of_insert(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r cmnd.possible_answer%rowtype;
begin 
   l_r := cmnd.jf_possible_answer_pkg$attr_to_rowtype(p_attr);
   
   l_r.possible_answer_id := nextval( 'cmnd.possible_answer_id_seq' );
   
   insert into cmnd.possible_answer select l_r.*;

   return null;
end;
$$;

-- DROP FUNCTION cmnd.jf_possible_answer_pkg$of_update(NUMERIC, TEXT);
create or replace function cmnd.jf_possible_answer_pkg$of_update(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r cmnd.possible_answer%rowtype;
begin 
   l_r := cmnd.jf_possible_answer_pkg$attr_to_rowtype(p_attr);

   update cmnd.possible_answer set 
          response = l_r.response
   where 
          possible_answer_id = l_r.possible_answer_id;

   return null;
end;
$$;

-- DROP FUNCTION cmnd.jf_possible_answer_pkg$of_delete(NUMERIC, TEXT);
create or replace function cmnd.jf_possible_answer_pkg$of_delete(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare 
   l_r cmnd.possible_answer%rowtype;
begin 
   l_r := cmnd.jf_possible_answer_pkg$attr_to_rowtype(p_attr);

   delete from  cmnd.possible_answer where  possible_answer_id = l_r.possible_answer_id;

   return null;
end;
$$;
