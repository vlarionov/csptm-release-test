CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_request_pkg$attr_to_rowtype (p_attr text) RETURNS cmnd.cmnd_request
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.cmnd_request%rowtype;
begin
   l_r.cmnd_request_id := jofl.jofl_pkg$extract_number(p_attr, 'cmnd_request_id', true);
   l_r.unit_id := jofl.jofl_pkg$extract_number(p_attr, 'unit_id', true);
   l_r.request_time := jofl.jofl_pkg$extract_date(p_attr, 'request_time', true);
   l_r.cmnd_ref_id := jofl.jofl_pkg$extract_number(p_attr, 'cmnd_ref_id', true);
   l_r.cmnd_status_id := jofl.jofl_pkg$extract_varchar(p_attr, 'cmnd_status_id', true);
   l_r.request_param := jofl.jofl_pkg$extract_varchar(p_attr, 'request_param', true);

   return l_r;
end;

$$;
----------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION cmnd.jf_cmnd_request_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$

declare
    l_dt_begin timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr,'BEGIN_DT', true), date_trunc('day', now()));
    l_dt_end timestamp := coalesce(jofl.jofl_pkg$extract_date(p_attr,'END_DT', true), date_trunc('day', now() + interval '1 day'));
    l_equipment_id_array int[] := jofl.jofl_pkg$extract_narray(p_attr,'f_equipment_id', true);
    l_cmnd_status_id int := jofl.jofl_pkg$extract_number(p_attr, 'f_cmnd_status_id', true);
    l_depo_id_array int[] := jofl.jofl_pkg$extract_tarray(p_attr, 'f_depo_id', true);
    l_territory_id_array int [] := jofl.jofl_pkg$extract_tarray(p_attr, 'f_territory_id', true);
    l_tr_id int [] := jofl.jofl_pkg$extract_tarray(p_attr, 'f_tr_id', true);
    l_equipment_id int := jofl.jofl_pkg$extract_number(p_attr, 'equipment_id', true);
    l_rf_cmnd_ref_id int := jofl.jofl_pkg$extract_number(p_attr, 'f_cmnd_ref_id', true);
begin
 open p_rows for
    select
      cmnd_request.cmnd_request_id,
      cmnd_request.unit_id,
      request_time,
      cmnd_response.response_time,
      cmnd_request.cmnd_ref_id,
      cmnd_request.cmnd_status_id,
      request_param::text,
      case when response_name = 'ask' then 'SMS отправлено'
      else cmnd_response.response_param::text
      end response_param,
      cmnd_ref.cmnd_ref_name,
      cmnd_ref.cmnd_ref_protocol,
      unit.unit_num,
      cmnd_status.cmnd_status_name,
      tr.garage_num,
      territory_entity.name_full territory_name,
      depo_entity.name_short depo_name,
      ets.name as equipment_status_name,
      account.first_name||' '||account.middle_name||' '||account.last_name user_name
    from cmnd.cmnd_request
      join cmnd.cmnd_ref on cmnd_request.cmnd_ref_id = cmnd_ref.cmnd_ref_id
      join core.unit on unit.unit_id = cmnd_request.unit_id
      join core.equipment et on et.equipment_id= unit.unit_id
      join cmnd.cmnd_status on cmnd_status.cmnd_status_id = cmnd.cmnd_request.cmnd_status_id
      join core.equipment_status ets on ets.equipment_status_id = et.equipment_status_id
      left join cmnd.cmnd_response on (cmnd_request.cmnd_request_id =  cmnd_response.cmnd_request_id)
      left join hist.v_unit2tr_hist u2t on u2t.unit_id = cmnd_request.unit_id
      left join core.tr on tr.tr_id = u2t.tr_id
      left join core.territory on tr.territory_id = territory.territory_id
      left join core.depo2territory d2t on territory.territory_id = d2t.territory_id and
                                           cmnd_request.request_time BETWEEN d2t.dt_begin and coalesce(d2t.dt_end, cmnd_request.request_time)
      left join core.entity territory_entity on territory.territory_id = territory_entity.entity_id
      left join core.entity depo_entity on d2t.depo_id = depo_entity.entity_id
      left join core.account on cmnd_request.account_id = account.account_id
    where u2t.sys_period @> cmnd_request.request_time::TIMESTAMPTZ
            and request_time between l_dt_begin and l_dt_end
            and ( unit.unit_id  = l_equipment_id
                or
                l_equipment_id  is null
                )            
            and ( unit.unit_id  = any(l_equipment_id_array)
                or
                array_length(l_equipment_id_array, 1)  is null
                )
            and (cmnd_request.cmnd_status_id = l_cmnd_status_id or l_cmnd_status_id is null)
            and (
                tr.tr_id in (select tr_id from core.tr where depo_id = any(l_depo_id_array))
                or
                    (array_length(l_depo_id_array, 1)  is null)
                )
            and (
                    tr.tr_id in (select tr_id
                            from core.tr
                           where territory_id = any(l_territory_id_array))
                or
                    (array_length(l_territory_id_array, 1)  is null)
                )
            and (
                    tr.tr_id = any(l_tr_id)
                or
                    (array_length(l_tr_id, 1)  is null)
                )
            and (cmnd_request.cmnd_ref_id = l_rf_cmnd_ref_id or l_rf_cmnd_ref_id is null)    
            ;
end;

$$;