CREATE OR REPLACE FUNCTION cmnd.jf_response_ref_attr_pkg$attr_to_rowtype (p_attr text) RETURNS cmnd.response_ref_attr
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.response_ref_attr%rowtype;
begin
   l_r.response_ref_id := jofl.jofl_pkg$extract_number(p_attr, 'response_ref_id', true);
   l_r.response_ref_attr_id := jofl.jofl_pkg$extract_number(p_attr, 'response_ref_attr_id', true);
   l_r.response_attr_name := jofl.jofl_pkg$extract_varchar(p_attr, 'response_attr_name', true);
   l_r.response_attr_protokol := jofl.jofl_pkg$extract_varchar(p_attr, 'response_attr_protokol', true);

   return l_r;
end;

$$;
CREATE OR REPLACE FUNCTION cmnd.jf_response_ref_attr_pkg$of_delete (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.response_ref_attr%rowtype;
begin
   l_r := cmnd.jf_response_ref_attr_pkg$attr_to_rowtype(p_attr);

   delete from  cmnd.response_ref_attr where response_ref_attr_id = l_r.response_ref_attr_id;

   return null;
end;

$$;
CREATE OR REPLACE FUNCTION cmnd.jf_response_ref_attr_pkg$of_insert (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.response_ref_attr%rowtype;
begin
   l_r := cmnd.jf_response_ref_attr_pkg$attr_to_rowtype(p_attr);

   l_r.response_ref_attr_id := nextval( 'cmnd.response_ref_attr_response_ref_attr_id_seq' );

   insert into cmnd.response_ref_attr select l_r.*;

   return null;
end;

$$;
CREATE OR REPLACE FUNCTION cmnd.jf_response_ref_attr_pkg$of_rows (p_id_account numeric, OUT p_rows refcursor, p_attr text) RETURNS refcursor
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.response_ref_attr%rowtype;
begin
   l_r.response_ref_id := jofl.jofl_pkg$extract_number(p_attr, 'response_ref_id', true);
 open p_rows for
      select
        response_ref_id,
        response_ref_attr_id,
        response_attr_name,
        response_attr_protokol
      from cmnd.response_ref_attr
      where response_ref_id = l_r.response_ref_id;
end;

$$;
CREATE OR REPLACE FUNCTION cmnd.jf_response_ref_attr_pkg$of_update (p_id_account numeric, p_attr text) RETURNS text
	LANGUAGE plpgsql
AS $$

declare
   l_r cmnd.response_ref_attr%rowtype;
begin
   l_r := cmnd.jf_response_ref_attr_pkg$attr_to_rowtype(p_attr);

   update cmnd.response_ref_attr set
          response_attr_name = l_r.response_attr_name,
          response_attr_protokol = l_r.response_attr_protokol
   where response_ref_attr_id = l_r.response_ref_attr_id;

   return null;
end;

$$;