﻿-- view: rnis.v_agency_csptm
-- drop view rnis.v_agency_csptm

create or replace view rnis.v_agency_csptm as 
select carrier_id            as agency_id,
       name_full             as agency_name,
       case when carrier_id = core.carrier_pkg$mgt() then 'http://www.mosgortrans.ru/'::text else null::text end as agency_url,
       'Europe/Moscow'::text as agency_timezone
  from core.carrier c
  join core.entity e on e.entity_id = c.carrier_id
 where c.sign_deleted = 0
   and carrier_id = core.carrier_pkg$mgt();

comment on view rnis.v_agency_csptm is 'Справочник транспортных организаций';
comment on column rnis.v_agency_csptm.agency_id is 'Идентификатор транспортной организации';  
comment on column rnis.v_agency_csptm.agency_name is 'Наименование транспортной организации';  
comment on column rnis.v_agency_csptm.agency_url is 'Сайт транспортной организации';  
comment on column rnis.v_agency_csptm.agency_timezone is 'Часовой пояс транспортной организации';  

-- select * from rnis.v_agency_csptm
