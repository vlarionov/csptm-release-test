﻿-- view: rnis.v_trips_csptm

-- drop view rnis.v_trips_csptm

create or replace view rnis.v_trips_csptm as
with tt_action_data as (
           select distinct rnd.round_id, rnd.code as round_code, rnd.move_direction_id, rnd.route_variant_id, tto.tt_variant_id
             from ttb.tt_out tto
             join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id and tta.parent_tt_action_id is null
             join ttb.tt_action_item tti on tti.tt_action_id = tta.tt_action_id
             join rts.stop_item2round sir on sir.stop_item2round_id = tti.stop_item2round_id
             join rts.round rnd on rnd.round_id = sir.round_id
            where not tto.sign_deleted
              and not tta.sign_deleted
              and not tti.sign_deleted
              and not sir.sign_deleted
              and ttb.action_type_pkg$is_production_round(rnd.action_type_id)
              and tto.tt_variant_id in (select ttv.tt_variant_id from ttb.tt_variant ttv
                                         join rts.route_variant rtv on rtv.route_variant_id = ttv.route_variant_id
                                        where not ttv.sign_deleted
                                          and ttv.action_period @> localtimestamp
                                          and ttv.parent_tt_variant_id is null
                                          and ttv.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id())
)
select rtv.route_id,
       tad.tt_variant_id as service_id,
       rnd.round_id as trip_id,
       ttb.tt_export_service_pkg$get_round_last_stop_name(rnd.round_id) as trip_headsign,
       rnd.move_direction_id - 1::smallint as direction_id,
       null::text   as block_id,
       null::text   as volume_id,
       null::text   as trip_type
  from tt_action_data tad
  join rts.round rnd on rnd.round_id = tad.round_id
  join rts.route_variant rtv on rtv.route_variant_id = rnd.route_variant_id;

comment on view rnis.v_trips_csptm is 'Рейсы маршрутов НГПТ';
comment on column rnis.v_trips_csptm.route_id is 'Код маршрута';
comment on column rnis.v_trips_csptm.service_id is 'Код перечня дат';
comment on column rnis.v_trips_csptm.trip_id is 'Код рейса';
comment on column rnis.v_trips_csptm.trip_headsign is 'Пункт назначения рейса, отображающийся на информационном табло';
comment on column rnis.v_trips_csptm.direction_id is 'Направление движения рейса'; 
comment on column rnis.v_trips_csptm.block_id is 'Код связки рейсов'; 
comment on column rnis.v_trips_csptm.volume_id is 'Вместимость транспортного средства'; 
comment on column rnis.v_trips_csptm.trip_type is 'Тип рейса';

-- select * from rnis.v_trips_csptm
