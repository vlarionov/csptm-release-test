﻿-- view: rnis.v_routes_csptm

-- drop view rnis.v_routes_csptm

create or replace view rnis.v_routes_csptm as
select rut.route_id,
       rut.carrier_id as agency_id,
       rut.route_num  as route_short_name,
       coalesce(ttb.tt_export_service_pkg$get_route_first_stop_name(rut.route_id) || ' - ' ||
                ttb.tt_export_service_pkg$get_route_last_stop_name (rut.route_id), '') as route_long_name,
       case when rut.tr_type_id = 1 then 3
            when rut.tr_type_id = 2 then 5
            when rut.tr_type_id = 3 or rut.tr_type_id = 4 then 0
       end as route_type,
       rut.route_num || ' : ' || rut.comment    as route_desc,
       null::text as route_union,
       case when rut.sign_deleted then 'не активна' else 'активна' end as EntryState,
       null::text as EntryAddReason,
       null::text as EntryModifyReason,
       null::text as EntryDeleteReason,
       null::text as ParentEntries,
       null::text as ChildEntries
  from rts.route rut;

comment on view rnis.v_routes_csptm is 'Маршруты НГПТ';
comment on column rnis.v_routes_csptm.route_id is 'Код маршрута';
comment on column rnis.v_routes_csptm.agency_id is 'Код транспортного предприятия';
comment on column rnis.v_routes_csptm.route_short_name is 'Номер маршрута';
comment on column rnis.v_routes_csptm.route_long_name is 'Полное наименование маршрута';
comment on column rnis.v_routes_csptm.route_type is 'Вид транспорта';
comment on column rnis.v_routes_csptm.route_desc is 'Описание маршрута';
comment on column rnis.v_routes_csptm.route_union is 'Совместный маршрут';
comment on column rnis.v_routes_csptm.EntryState is 'Состояние записи';
comment on column rnis.v_routes_csptm.EntryAddReason is 'Причина добавления записи';
comment on column rnis.v_routes_csptm.EntryModifyReason is 'Причина изменения записи';
comment on column rnis.v_routes_csptm.EntryDeleteReason is 'Причина удаления записи';
comment on column rnis.v_routes_csptm.ParentEntries is 'Родительские объекты';
comment on column rnis.v_routes_csptm.ChildEntries is 'Дочерние объекты';

-- select * from rnis.v_routes_csptm
