﻿-- view: rnis.v_plan_ts_time
-- drop view rnis.v_plan_ts_time

create or replace view rnis.v_plan_ts_time as
with all_num as (
   select route_variant_list.route_variant_id, num_list.num, 0 as tr_count
     from (select distinct ttv.route_variant_id as route_variant_id
             from ttb.tt_variant ttv
            where not ttv.sign_deleted
              and ttv.order_date = now()::date
          ) route_variant_list,
          ( select generate_series(0, 57) as num
          ) num_list
),
tr_num as (
   select ttv.route_variant_id,
          ((extract(hour from (tti.time_begin - ttv.order_date)::time)::int * 60 + extract(minute from (tti.time_begin - ttv.order_date)::time)::int) / 30)::int as num,
          count(distinct tto.tr_id) as tr_count
     from ttb.tt_variant ttv
     join ttb.tt_out tto on tto.tt_variant_id = ttv.tt_variant_id 
     join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
     join ttb.tt_action_round ttr on ttr.tt_action_id = tta.tt_action_id
     join ttb.tt_action_item tti on tti.tt_action_id = tta.tt_action_id
     join rts.round rnd on rnd.round_id = ttr.round_id
    where not ttv.sign_deleted
      and not tto.sign_deleted
      and not tta.sign_deleted
      and not tti.sign_deleted
      and tto.tr_id is not null
      and ttv.order_date = now()::date
    group by 1, 2
),
out_list as (
   select rtv.route_id, all_num.num,
          coalesce(tr_num.tr_count, all_num.tr_count) as tr_count
     from all_num
     left join tr_num on tr_num.route_variant_id = all_num.route_variant_id
                     and tr_num.num = all_num.num
     join rts.route_variant rtv on rtv.route_variant_id = all_num.route_variant_id
)
select to_char(now(), 'yyyy-mm-dd') as date,
       route_id,
       sum(tr_count) filter (where num =  0) as plan_ts_0300_0330,
       sum(tr_count) filter (where num =  1) as plan_ts_0330_0400,
       sum(tr_count) filter (where num =  2) as plan_ts_0400_0430,
       sum(tr_count) filter (where num =  3) as plan_ts_0430_0500,
       sum(tr_count) filter (where num =  4) as plan_ts_0500_0530,
       sum(tr_count) filter (where num =  5) as plan_ts_0530_0600,
       sum(tr_count) filter (where num =  6) as plan_ts_0600_0630,
       sum(tr_count) filter (where num =  7) as plan_ts_0630_0700,
       sum(tr_count) filter (where num =  8) as plan_ts_0700_0730,
       sum(tr_count) filter (where num =  9) as plan_ts_0730_0800,
       sum(tr_count) filter (where num = 10) as plan_ts_0800_0830,
       sum(tr_count) filter (where num = 11) as plan_ts_0830_0900,
       sum(tr_count) filter (where num = 12) as plan_ts_0900_0930,
       sum(tr_count) filter (where num = 13) as plan_ts_0930_1000,
       sum(tr_count) filter (where num = 14) as plan_ts_1000_1030,
       sum(tr_count) filter (where num = 15) as plan_ts_1030_1100,
       sum(tr_count) filter (where num = 16) as plan_ts_1100_1130,
       sum(tr_count) filter (where num = 17) as plan_ts_1130_1200,
       sum(tr_count) filter (where num = 18) as plan_ts_1200_1230,
       sum(tr_count) filter (where num = 19) as plan_ts_1230_1300,
       sum(tr_count) filter (where num = 20) as plan_ts_1300_1330,
       sum(tr_count) filter (where num = 21) as plan_ts_1330_1400,
       sum(tr_count) filter (where num = 22) as plan_ts_1400_1430,
       sum(tr_count) filter (where num = 23) as plan_ts_1430_1500,
       sum(tr_count) filter (where num = 24) as plan_ts_1500_1530,
       sum(tr_count) filter (where num = 25) as plan_ts_1530_1600,
       sum(tr_count) filter (where num = 26) as plan_ts_1600_1630,
       sum(tr_count) filter (where num = 27) as plan_ts_1630_1700,
       sum(tr_count) filter (where num = 28) as plan_ts_1700_1730,
       sum(tr_count) filter (where num = 29) as plan_ts_1730_1800,
       sum(tr_count) filter (where num = 30) as plan_ts_1800_1830,
       sum(tr_count) filter (where num = 31) as plan_ts_1830_1900,
       sum(tr_count) filter (where num = 32) as plan_ts_1900_1930,
       sum(tr_count) filter (where num = 33) as plan_ts_1930_2000,
       sum(tr_count) filter (where num = 34) as plan_ts_2000_2030,
       sum(tr_count) filter (where num = 35) as plan_ts_2030_2100,
       sum(tr_count) filter (where num = 36) as plan_ts_2100_2130,
       sum(tr_count) filter (where num = 37) as plan_ts_2130_2200,
       sum(tr_count) filter (where num = 38) as plan_ts_2200_2230,
       sum(tr_count) filter (where num = 39) as plan_ts_2230_2300,
       sum(tr_count) filter (where num = 40) as plan_ts_2300_2330,
       sum(tr_count) filter (where num = 41) as plan_ts_2330_2400,
       sum(tr_count) filter (where num = 42) as plan_ts_2400_2430,
       sum(tr_count) filter (where num = 43) as plan_ts_2430_2500,
       sum(tr_count) filter (where num = 44) as plan_ts_2500_2530,
       sum(tr_count) filter (where num = 45) as plan_ts_2530_2600,
       sum(tr_count) filter (where num = 46) as plan_ts_2600_2630,
       sum(tr_count) filter (where num = 47) as plan_ts_2630_2700,
       sum(tr_count) filter (where num = 48) as plan_ts_2700_2730,
       sum(tr_count) filter (where num = 49) as plan_ts_2730_2800,
       sum(tr_count) filter (where num = 50) as plan_ts_2800_2830,
       sum(tr_count) filter (where num = 51) as plan_ts_2830_2900,
       sum(tr_count) filter (where num = 52) as plan_ts_2900_2930,
       sum(tr_count) filter (where num = 53) as plan_ts_2930_3000,
       sum(tr_count) filter (where num = 54) as plan_ts_3000_3030,
       sum(tr_count) filter (where num = 55) as plan_ts_3030_3100,
       sum(tr_count) filter (where num = 56) as plan_ts_3100_3130,
       sum(tr_count) filter (where num = 57) as plan_ts_3130_3200
  from out_list
 group by route_id
 order by route_id;

comment on view rnis.v_plan_ts_time is 'Справочник значений планового выпуска по маршрутам';
comment on column rnis.v_plan_ts_time.date is 'Дата';
comment on column rnis.v_plan_ts_time.route_id is 'Идентификатор маршрута';
comment on column rnis.v_plan_ts_time.plan_ts_0300_0330 is 'Плановое количество ТС, которые должны осуществлять перевозку на маршруте в период с 03:00 до 03:30 на дату';

-- select * from rnis.v_plan_ts_time
