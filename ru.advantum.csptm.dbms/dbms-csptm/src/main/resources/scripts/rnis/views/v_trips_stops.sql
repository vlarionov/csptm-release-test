﻿-- view: rnis.v_trips_stops

-- drop view rnis.v_trips_stops

create or replace view rnis.v_trips_stops as
select rte.route_id,
       rte.route_num  as route_short_name,
       rte.route_num  as reg_num,
       trt.short_name as route_type,
       rnd.round_id   as trip_id,
       rnd.code       as trip_short_name,
       rnd.move_direction_id - 1 as direction_id,
       to_char(lower(rtv.action_period), 'yyyy-mm-dd') as start_date,
       to_char(upper(rtv.action_period), 'yyyy-mm-dd') as end_date,
       sir.order_num  as stop_sequence,
       stl.stop_id
  from rts.route rte
  join core.tr_type trt on trt.tr_type_id = rte.tr_type_id
  join rts.route_variant rtv on rtv.route_id = rte.route_id
  join rts.round rnd on rnd.route_variant_id = rtv.route_variant_id
  join rts.stop_item2round sir on sir.round_id = rnd.round_id
  join rts.stop_item sti on sti.stop_item_id = sir.stop_item_id
  join rts.stop_location stl on stl.stop_location_id = sti.stop_location_id
 where not rte.sign_deleted
   and not rtv.sign_deleted
   and not rnd.sign_deleted
   and not sir.sign_deleted
   and not sti.sign_deleted
   and not stl.sign_deleted
   and (upper(rtv.action_period) is null or 
        upper(rtv.action_period) > now() - interval '3 mon')
 order by route_short_name, start_date, trip_short_name, direction_id, stop_sequence;


comment on view rnis.v_trips_stops is 'Справочник рейсов с последовательностями остановок';
comment on column rnis.v_trips_stops.route_id is 'Идентификатор маршрута';
comment on column rnis.v_trips_stops.route_short_name is 'Краткое наименование маршрута';
comment on column rnis.v_trips_stops.reg_num is 'Регистрационный номер маршрута';
comment on column rnis.v_trips_stops.route_type is 'Вид ТС на маршруте';
comment on column rnis.v_trips_stops.trip_id is 'Идентификатор рейса';
comment on column rnis.v_trips_stops.trip_short_name is 'Тип рейса';
comment on column rnis.v_trips_stops.direction_id is 'Направление рейса';
comment on column rnis.v_trips_stops.start_date is 'Дата начала действия варианта маршрута';
comment on column rnis.v_trips_stops.end_date is 'Дата окончания действия варианта маршрута';
comment on column rnis.v_trips_stops.stop_sequence is 'Номер остановочного пункта для рейса';
comment on column rnis.v_trips_stops.stop_id is 'Идентификатор остановочного пункта';

-- select * from rnis.v_trips_stops limit 1000
