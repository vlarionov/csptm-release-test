﻿-- view: rnis.v_stop_times

-- drop view rnis.v_stop_times

create or replace view rnis.v_stop_times as
with active_tt_variant as (
   select tt_variant_id, route_variant_id
     from ttb.tt_variant
    where tt_variant_id in 
          (select ttv.tt_variant_id as tt_variant_id
             from ttb.tt_variant ttv
             join rts.route_variant rtv on rtv.route_variant_id = ttv.route_variant_id
            where not ttv.sign_deleted
              and ttv.action_period @> localtimestamp
              and ttv.parent_tt_variant_id is null
              and ttv.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id())
),
stop_length as (
   select sir.stop_item2round_id, rnd.route_variant_id, rnd.code as round_code, sir.round_id, sir.order_num, stl.stop_id, sir.length_sector,
          sum(sir.length_sector) over (partition by sir.round_id order by sir.order_num) as sum_length_sector
     from rts.stop_item2round sir
     join rts.stop_item sti on sti.stop_item_id = sir.stop_item_id
     join rts.stop_location stl on stl.stop_location_id = sti.stop_location_id
     join rts.round rnd on rnd.round_id = sir.round_id
    where rnd.route_variant_id in (select distinct route_variant_id from active_tt_variant)
      and not sir.sign_deleted
      and not rnd.sign_deleted
),
round_num_list as (
   select tot.tt_variant_id, tta.tt_action_id,
          row_number() over (partition by tta.tt_out_id order by tta.tt_action_id) as round_num
     from ttb.tt_out tot
     join ttb.tt_action tta on tta.tt_out_id = tot.tt_out_id
     join ttb.tt_action_round ttr on ttr.tt_action_id = tta.tt_action_id
    where not tta.sign_deleted
      and tot.tt_variant_id in (select distinct tt_variant_id from active_tt_variant)
),
action_times as (
   select ttv.tt_variant_id, ttv.route_variant_id, tti.time_begin, tti.time_end, 
          tti.stop_item2round_id, tto.tt_out_num, drs.dr_shift_num, rnl.round_num
     from ttb.tt_variant ttv
     join ttb.tt_out tto on tto.tt_variant_id = ttv.tt_variant_id
     join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
     join ttb.tt_action_item tti on tti.tt_action_id = tta.tt_action_id
     join ttb.dr_shift drs on drs.dr_shift_id = tti.dr_shift_id
     left join round_num_list rnl on rnl.tt_variant_id = ttv.tt_variant_id
                                 and rnl.tt_action_id = tta.tt_action_id
    where ttv.parent_tt_variant_id is null
      and tta.parent_tt_action_id is null
      and not ttv.sign_deleted
      and not tto.sign_deleted
      and not tta.sign_deleted
      and not tti.sign_deleted
      and ttv.tt_variant_id in (select distinct tt_variant_id from active_tt_variant)
)
select stl.round_id                      as trip_id,
       att.tt_variant_id                 as service_id,
       to_char(time_begin, 'hh24:mi:ss') as arrival_time,
       to_char(time_end, 'hh24:mi:ss')   as departure_time,
       stop_id,
       order_num                         as stop_sequence,
       ttb.tt_export_service_pkg$get_round_last_stop_name(stl.round_id) as stop_headsign,
       null::int                         as pickup_type,
       null::int                         as drop_off_type,
       sum_length_sector::float/1000     as shape_dist_traveled,
       att.round_num                     as trip_num,
       att.dr_shift_num                  as shift_num,
       att.tt_out_num                    as grafic
  from stop_length stl
  left join action_times att on att.route_variant_id = stl.route_variant_id
                            and att.stop_item2round_id = stl.stop_item2round_id
 order by round_id, order_num;

comment on view rnis.v_stop_times is 'Справочник поостановочных расписаний';
comment on column rnis.v_stop_times.trip_id is 'Идентификатор рейса';
comment on column rnis.v_stop_times.service_id is 'Идентификатор варианта расписания';
comment on column rnis.v_stop_times.arrival_time is 'Время прибытия на остановочный пункт';  
comment on column rnis.v_stop_times.departure_time is 'Время отправления с остановочного пункта';  
comment on column rnis.v_stop_times.stop_id is 'Идентификатор остановочного пункта'; 
comment on column rnis.v_stop_times.stop_sequence is 'Номер остановочного пункта для рейса';
comment on column rnis.v_stop_times.stop_headsign is 'Наименование остановочного пункта';
comment on column rnis.v_stop_times.pickup_type is 'Порядок посадки пассажиров'; 
comment on column rnis.v_stop_times.drop_off_type is 'Порядок высадки пассажиров';
comment on column rnis.v_stop_times.shape_dist_traveled is 'Протяженность трассы от конечного пункта маршрута до остановочного пункта';
comment on column rnis.v_stop_times.trip_num is 'Порядковый номер рейса транспортного средства'; 
comment on column rnis.v_stop_times.shift_num is 'Номер смены водителя транспортного средства'; 
comment on column rnis.v_stop_times.grafic is 'Идентификатор выхода ТС'; 

-- select * from rnis.v_stop_times limit 100
