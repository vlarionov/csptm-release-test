﻿-- view: rnis.v_equipment

-- drop view rnis.v_equipment

create or replace view rnis.v_equipment as
select equ.equipment_id,
       unt.unit_num as identificator,
       equ.serial_num as serial,
       ent.name_short as brand,
       eqm.name as model,
       (select string_agg(c.phone_num, ',') as cell_number
          from core.sim_card2unit u
          join core.sim_card c on c.sim_card_id = u.sim_card_id
         where u.equipment_id = equ.equipment_id
       ) as cell_number,
       fir.version,
       etr.tr_id as vehicle_id,
       0 as is_deleted
  from core.equipment equ
  join core.unit unt on unt.unit_id = equ.equipment_id
  left join core.equipment_model eqm on eqm.equipment_model_id = equ.equipment_model_id
  left join core.firmware fir on fir.firmware_id = equ.firmware_id
  left join core.equipment2tr etr on etr.equipment_id = equ.equipment_id
  left join core.entity ent on ent.entity_id = equ.facility_id;

comment on view rnis.v_equipment is 'Справочник бортовых блоков транспортных средств типа БНСТ';
comment on column rnis.v_equipment.equipment_id is 'Уникальный идентификатор абонентского терминала'; 
comment on column rnis.v_equipment.identificator is 'Идентификатор, используемый при передаче телематических данных'; 
comment on column rnis.v_equipment.serial is 'Серийный номер, указанный на приборе'; 
comment on column rnis.v_equipment.brand is 'Марка'; 
comment on column rnis.v_equipment.model is 'Модель'; 
comment on column rnis.v_equipment.cell_number is 'Номер абонента'; 
comment on column rnis.v_equipment.version is 'Версия прошивки'; 
comment on column rnis.v_equipment.is_deleted is 'Признак удаления'; 

--select * from rnis.v_equipment limit 100
