﻿-- view: rnis.v_replace_vehicle

-- drop view rnis.v_replace_vehicle

create or replace view rnis.v_replace_vehicle as
select s.tt_out_num  as grafic,
       s.order_date  as date,
       s.change_time as time,
       s.prev_tr_id  as vehicle_id_1,
       s.tr_id       as vehicle_id_2,
       (ttb.tt_export_service_pkg$get_first_shift_and_driver_of_out(s.tt_out_id)).driver_id,
       (ttb.tt_export_service_pkg$get_first_shift_and_driver_of_out(s.tt_out_id)).dr_shift_num as shift_num,
       rtv.route_id
  from (select ttv.order_date, ttv.route_variant_id, tto.tt_out_id, tto.tt_out_num, tto.tr_id, action_range, lower(action_range) as change_time,
               lag(tto.tr_id) over (partition by ttv.route_variant_id, tto.tt_out_num order by action_range) as prev_tr_id
          from ttb.tt_variant ttv
          join ttb.tt_out tto on tto.tt_variant_id = ttv.tt_variant_id
         where ttv.order_date = now()::date
           and tto.tr_id is not null
           and not ttv.sign_deleted
           and not tto.sign_deleted
           and (ttv.route_variant_id, tto.tt_out_num) in 
               (select route_variant_id, tt_out_num
                  from (select distinct ttv.route_variant_id, tto.tt_out_num, tto.tr_id
                          from ttb.tt_variant ttv
                          join ttb.tt_out tto on tto.tt_variant_id = ttv.tt_variant_id
                         where ttv.order_date = now()::date
                           and tto.tr_id is not null
                           and not ttv.sign_deleted
                           and not tto.sign_deleted
                       ) q
                 group by route_variant_id, tt_out_num
                having count(*) > 1
               )
       ) s
  join rts.route_variant rtv on rtv.route_variant_id = s.route_variant_id
 where prev_tr_id is not null;

comment on view rnis.v_replace_vehicle is 'Сведения о замене ТС на маршруте';
comment on column rnis.v_replace_vehicle.grafic  is 'Идентификатор выхода ТС'; 
comment on column rnis.v_replace_vehicle.date    is 'Дата.'; 
comment on column rnis.v_replace_vehicle.time    is 'Время, с которого начинает выполнять рейсы ТС, заменяющее неисправное ТС'; 
comment on column rnis.v_replace_vehicle.vehicle_id_1 is 'Идентификатор заменяемого транспортного средства'; 
comment on column rnis.v_replace_vehicle.vehicle_id_2 is 'Идентификатор транспортного средства, вышедшего на замену'; 
comment on column rnis.v_replace_vehicle.driver_id is 'Идентификатор водителя на транспортном средстве, вышедшем на замену'; 
comment on column rnis.v_replace_vehicle.shift_num is 'Номер смены водителя транспортного средства'; 

--select * from rnis.v_replace_vehicle limit 100
