﻿-- view: rnis.v_impact_codes

-- drop view rnis.v_impact_codes

create or replace view rnis.v_impact_codes as
select cmnd_ref_id       impact_id, 
       cmnd_ref_protocol impact_code, 
       cmnd_ref_name     impact_name
  from cmnd.cmnd_ref
 where not is_deleted;

comment on view rnis.v_impact_codes is 'Коды управляющих воздействий';
comment on column rnis.v_impact_codes.impact_id is 'ID управляющего воздействия';
comment on column rnis.v_impact_codes.impact_code is 'Код управляющего воздействия';
comment on column rnis.v_impact_codes.impact_name is 'Наименование управляющего воздействия';

--select * from rnis.v_impact_codes limit 100
