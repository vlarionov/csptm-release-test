﻿-- view: rnis.v_calendar_dates_csptm

-- drop view rnis.v_calendar_dates_csptm

create or replace view rnis.v_calendar_dates_csptm as
select ctg.calendar_tag_id         as calendar_dates_id,
       ttv.tt_variant_id           as service_id,
       to_char(cnd.dt, 'YYYYMMDD') as "date",
       1                           as exception_type
  from ttb.tt_calendar ttc
  join ttb.tt_variant ttv on ttv.tt_variant_id = ttc.tt_variant_id
  join ttb.calendar_tag ctg on ctg.calendar_tag_id = ttc.calendar_tag_id
  join ttb.calendar_item cit on cit.calendar_tag_id = ctg.calendar_tag_id
  join ttb.calendar cnd on cnd.calendar_id = cit.calendar_id
 where ctg.calendar_tag_group_id in (2, 8)
   and not ttv.sign_deleted
   and ttv.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id();

comment on view rnis.v_calendar_dates_csptm is 'Календарь маршрутов НГПТ в праздничные дни';
comment on column rnis.v_calendar_dates_csptm.calendar_dates_id is 'Код перечня праздничных дат';
comment on column rnis.v_calendar_dates_csptm.service_id is 'Код перечня дат';
comment on column rnis.v_calendar_dates_csptm.date is 'Дата доступности рейса';
comment on column rnis.v_calendar_dates_csptm.exception_type is 'Тип наличия рейса в праздничные дни';

-- select * from rnis.v_calendar_dates_csptm
