﻿-- view: rnis.v_stops

-- drop view rnis.v_stops

create or replace view rnis.v_stops as
select stp.stop_id      as stop_id,
       stp.name         as stop_code,
       stp.name         as stop_name,
       null::text       as stop_desc,
       st_y(geom)       as stop_lat,
       st_x(geom)       as stop_lon,
       null::int        as zone_id,
       null::text       as stop_url,
       null::int        as location_type,
       null::text       as parent_station,
       null::text       as stop_timezone,
       null::text       as wheelchair_boarding,
       null::text       as stop_polygon,
       case when stp.sign_deleted then 1 else 0 end as is_deleted
  from (select distinct stop_location_id from rts.stop_item where not sign_deleted) sti
  join rts.stop_location stl on stl.stop_location_id = sti.stop_location_id 
  join rts.stop stp on stp.stop_id = stl.stop_id
  join rts.stop2gis stg on stg.stop_id = stl.stop_id
  join gis.stops gst on gst.muid = stg.stop_muid;

comment on view rnis.v_stops is 'Справочник остановочных пунктов';
comment on column rnis.v_stops.stop_id is 'Идентификатор остановочного пункта'; 
comment on column rnis.v_stops.stop_code is 'Краткое наименование остановочного пункта'; 
comment on column rnis.v_stops.stop_name is 'Полное наименование остановочного пункта'; 
comment on column rnis.v_stops.stop_desc is 'Описание остановочного пункта'; 
comment on column rnis.v_stops.stop_lat is 'Широта расположения остановочного пункта'; 
comment on column rnis.v_stops.stop_lon is 'Долгота расположения остановочного пункта'; 
comment on column rnis.v_stops.zone_id is 'Тарифная зона остановочного пункта'; 
comment on column rnis.v_stops.stop_url is 'Сайт остановочного пункта'; 
comment on column rnis.v_stops.location_type is 'Тип остановочного пункта'; 
comment on column rnis.v_stops.parent_station is 'Станция для остановочного пункта'; 
comment on column rnis.v_stops.stop_timezone is 'Часовой пояс остановочного пункта'; 
comment on column rnis.v_stops.wheelchair_boarding is 'Возможность посадки на инвалидном кресле'; 
comment on column rnis.v_stops.stop_polygon is 'Полигон остановочного пункта'; 
comment on column rnis.v_stops.is_deleted is 'Признак удаления'; 

-- select * from rnis.v_stops
