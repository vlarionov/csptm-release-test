﻿-- view: rnis.v_routes

-- drop view rnis.v_routes

create or replace view rnis.v_routes as
select rte.route_id,
       rte.carrier_id as agency_id,
       rte.route_num  as route_short_name,
       coalesce(ttb.tt_export_service_pkg$get_route_first_stop_name(rte.route_id) || ' - ' ||
                ttb.tt_export_service_pkg$get_route_last_stop_name (rte.route_id), '') as route_long_name,
       rte.comment    as route_desc,
       null::text     as route_view, -- ??
       trt.short_name as route_type,
       null::text     as route_url,
       null::text     as route_color,
       null::text     as route_text_color,
       null::text     as route_date_start,
       null::text     as route_date_end
  from rts.route rte
  join core.tr_type trt on trt.tr_type_id = rte.tr_type_id;

comment on view rnis.v_routes is 'Справочник маршрутов';
comment on column rnis.v_routes.route_id is 'Идентификатор маршрута'; 
comment on column rnis.v_routes.agency_id is 'Идентификатор транспортной организации'; 
comment on column rnis.v_routes.route_short_name is 'Краткое наименование маршрута'; 
comment on column rnis.v_routes.route_long_name is 'Полное наименование маршрута'; 
comment on column rnis.v_routes.route_desc is 'Описание маршрута'; 
comment on column rnis.v_routes.route_view is 'Вид маршрута (постоянный, компенсационный)';
comment on column rnis.v_routes.route_type is 'Вид ТС на маршруте'; 
comment on column rnis.v_routes.route_url is 'Сайт маршрута';
comment on column rnis.v_routes.route_color is 'Цвет отображения маршрута';
comment on column rnis.v_routes.route_text_color is 'Цвет отображения текста маршрута';
comment on column rnis.v_routes.route_date_start is 'Дата ввода маршрута в действие';
comment on column rnis.v_routes.route_date_end is 'Дата вывода маршрута из действия'; 

-- select * from rnis.v_routes
