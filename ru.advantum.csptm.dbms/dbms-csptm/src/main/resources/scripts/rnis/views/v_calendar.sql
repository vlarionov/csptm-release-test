﻿-- view: rnis.v_calendar

-- drop view rnis.v_calendar

create or replace view rnis.v_calendar as
select ttv.tt_variant_id as service_id,
       substring(tt_calendar_bit from 1 for 1)::int  as monday,
       substring(tt_calendar_bit from 2 for 1)::int  as tuesday,
       substring(tt_calendar_bit from 3 for 1)::int  as wednesday,
       substring(tt_calendar_bit from 4 for 1)::int  as thursday,
       substring(tt_calendar_bit from 5 for 1)::int  as friday,
       substring(tt_calendar_bit from 6 for 1)::int  as saturday,
       substring(tt_calendar_bit from 7 for 1)::int  as sunday,
       to_char(lower(ttv.action_period), 'YYYYMMDD') as start_date,
       to_char(upper(ttv.action_period), 'YYYYMMDD') as end_date
  from (select tt_variant_id,
               ttb.tt_export_service_pkg$convert_calendar_to_bit(tt_variant_id) as tt_calendar_bit
          from ttb.tt_calendar
       ) q
  join ttb.tt_variant ttv on ttv.tt_variant_id = q.tt_variant_id
 where not ttv.sign_deleted
   and ttv.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id();

comment on view rnis.v_calendar is 'Календарь маршрутов НГПТ';
comment on column rnis.v_calendar.service_id is 'Код перечня дат';  
comment on column rnis.v_calendar.monday is 'Доступность маршрута по понедельникам';  
comment on column rnis.v_calendar.tuesday is 'Доступность маршрута по вторникам';  
comment on column rnis.v_calendar.wednesday is 'Доступность маршрута по средам';  
comment on column rnis.v_calendar.thursday is 'Доступность маршрута по четвергам';  
comment on column rnis.v_calendar.friday is 'Доступность маршрута по пятницам';  
comment on column rnis.v_calendar.saturday is 'Доступность маршрута по субботам';  
comment on column rnis.v_calendar.sunday is 'Доступность маршрута по воскресеньям'; 
comment on column rnis.v_calendar.start_date is 'Дата начала периода действия варианта расписания маршрута'; 
comment on column rnis.v_calendar.end_date is 'Дата окончания периода действия варианта расписания маршрута'; 

-- select * from rnis.v_calendar
-- select * from rnis.v_calendar
-- select to_char(now(), 'YYYYMMDD')
