﻿-- view: rnis.v_vehicles

-- drop view rnis.v_vehicles

create or replace view rnis.v_vehicles as
select tr.tr_id          as vehicle_id,
       tr.licence        as gos_nomer,
       tr.depo_id        as depot_id,
       tr.garage_num     as depot_number,
       tk.name           as brand,
       tm.name           as model,
       tt.short_name     as route_type,
       tc.short_name     as capacity,
       tm.seat_qty_total as seats,
       null::text        as square,
       null::text        as mass,
       null::text        as length,
       null::text        as width,
       null::text        as height,  
       null::text        as speed_max,
       0::int            as is_deleted
  from core.tr
  join core.tr_type tt on tt.tr_type_id = tr.tr_type_id
  join core.tr_model tm on tm.tr_model_id = tr.tr_model_id
  join core.tr_mark tk on tk.tr_mark_id = tm.tr_mark_id
  join core.tr_capacity tc on tc.tr_capacity_id = tm.tr_capacity_id;

comment on view rnis.v_vehicles is 'Справочник транспортных средств';
comment on column rnis.v_vehicles.vehicle_id is 'Уникальный идентификатор транспортного средства';
comment on column rnis.v_vehicles.gos_nomer is 'Серия и номер государственного регистрационного знака';
comment on column rnis.v_vehicles.depot_id is 'Идентификатор транспортного предприятия';
comment on column rnis.v_vehicles.depot_number is 'Гаражный номер';
comment on column rnis.v_vehicles.brand is 'Марка';
comment on column rnis.v_vehicles.model is 'Модель';
comment on column rnis.v_vehicles.route_type is 'Вид ТС на маршруте';
comment on column rnis.v_vehicles.capacity is 'Вместимость';
comment on column rnis.v_vehicles.seats is 'Количество посадочных мест';
comment on column rnis.v_vehicles.square is 'Полезная площадь салона';
comment on column rnis.v_vehicles.mass is 'Снаряженная масса';
comment on column rnis.v_vehicles.length is 'Габаритная длина';
comment on column rnis.v_vehicles.width is 'Габаритная ширина';
comment on column rnis.v_vehicles.height is 'Габаритная высота';
comment on column rnis.v_vehicles.speed_max is 'Максимальная скорость';
comment on column rnis.v_vehicles.is_deleted is 'Признак удаления ТС';

--select * from rnis.v_vehicles limit 100
--drop table rnis.vehicles