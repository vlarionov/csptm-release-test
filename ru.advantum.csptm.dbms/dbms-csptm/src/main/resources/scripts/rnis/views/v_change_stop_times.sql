﻿-- view: rnis.v_change_stop_times

-- drop view rnis.v_change_stop_times

create or replace view rnis.v_change_stop_times as
with action_changed as (
   select distinct tta.tt_out_id, tti.tt_action_item_id
     from ttb.tt_action tta
     join ttb.tt_action_item tti on tti.tt_action_id = tta.tt_action_id
     join ttb.tt_action_round ttr on ttr.tt_action_id = tta.tt_action_id
     join rts.stop_item2round_type sir on sir.stop_item2round_id = tti.stop_item2round_id
     join rts.stop_item_type sit on sit.stop_item_type_id = sir.stop_item_type_id
    where not tti.sign_deleted
      and not sir.sign_deleted
      and not sit.sign_deleted
      and sit.stop_type_id in (rts.stop_type_pkg$finish(), rts.stop_type_pkg$checkpoint())
      and tta.parent_tt_action_id is not null
      and tti.time_begin >= localtimestamp - interval  '5 min'
      and tti.time_begin <= localtimestamp + interval '10 min'
      and not exists (select 1 from ttb.tt_action qtta
                        join ttb.tt_action_item qtti on qtti.tt_action_id = qtta.tt_action_id
                       where not qtti.sign_deleted
                         and qtta.tt_action_id = tta.parent_tt_action_id
                         and qtti.stop_item2round_id = tti.stop_item2round_id
                         and qtti.time_begin::time = ttb.get_local_timestamp_from_utc(tti.time_begin)::time)
),
stop_length as (
   select sir.stop_item2round_id, rnd.route_variant_id, rnd.code as round_code, sir.round_id, sir.order_num, stl.stop_id, sir.length_sector,
          sum(sir.length_sector) over (partition by sir.round_id order by sir.order_num) as sum_length_sector
     from rts.stop_item2round sir
     join rts.stop_item sti on sti.stop_item_id = sir.stop_item_id
     join rts.stop_location stl on stl.stop_location_id = sti.stop_location_id
     join rts.round rnd on rnd.round_id = sir.round_id
    where rnd.route_variant_id in (select ttv.route_variant_id
                                     from action_changed ach
                                     join ttb.tt_out tto on tto.tt_out_id = ach.tt_out_id
                                     join ttb.tt_variant ttv on ttv.tt_variant_id = tto.tt_variant_id
                                    where not tto.sign_deleted
                                      and not ttv.sign_deleted)
      and not sir.sign_deleted
      and not rnd.sign_deleted
),
round_num_list as (
   select tot.tt_variant_id, tta.tt_action_id,
          row_number() over (partition by tta.tt_out_id order by tta.tt_action_id) as round_num
     from ttb.tt_out tot
     join ttb.tt_action tta on tta.tt_out_id = tot.tt_out_id
     join ttb.tt_action_round ttr on ttr.tt_action_id = tta.tt_action_id
    where not tta.sign_deleted
      and tot.tt_out_id in (select tt_out_id from action_changed)
),
action_times as (
   select ttv.tt_variant_id, ttv.route_variant_id, ttv.order_date, tti.time_begin, tti.time_end, 
          tti.stop_item2round_id, tto.tt_out_num, drs.dr_shift_num, rnl.round_num
     from ttb.tt_variant ttv
     join ttb.tt_out tto on tto.tt_variant_id = ttv.tt_variant_id
     join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
     join ttb.tt_action_item tti on tti.tt_action_id = tta.tt_action_id
     join action_changed ach on ach.tt_action_item_id = tti.tt_action_item_id
     join ttb.dr_shift drs on drs.dr_shift_id = tti.dr_shift_id
     left join round_num_list rnl on rnl.tt_variant_id = ttv.tt_variant_id
                                 and rnl.tt_action_id = tta.tt_action_id
    where not ttv.sign_deleted
      and not tto.sign_deleted
      and not tta.sign_deleted
      and not tti.sign_deleted
)
select stl.round_id                          as trip_id,
       att.tt_variant_id                     as service_id,
       to_char(att.order_date, 'yyyy-mm-dd') as trip_date,
       to_char(time_begin, 'hh24:mi')        as arrival_time,
       to_char(time_end, 'hh24:mi')          as departure_time,
       stop_id,
       order_num                             as stop_sequence,
       ttb.tt_export_service_pkg$get_round_last_stop_name(stl.round_id) as stop_headsign,
       null::int                             as pickup_type,
       null::int                             as drop_off_type,
       sum_length_sector::float/1000         as shape_dist_traveled,
       att.round_num                         as trip_num,
       att.dr_shift_num                      as shift_num,
       att.tt_out_num                        as grafic
  from action_times att
  left join stop_length stl on stl.route_variant_id = att.route_variant_id
                           and stl.stop_item2round_id = att.stop_item2round_id
 order by round_id, order_num;


comment on view rnis.v_change_stop_times is 'Оперативные изменения поостановочных расписаний';
comment on column rnis.v_change_stop_times.trip_id is 'Идентификатор рейса';
comment on column rnis.v_change_stop_times.trip_date is 'Дата начала рейса';
comment on column rnis.v_change_stop_times.arrival_time is 'Время прибытия на остановочный пункт';
comment on column rnis.v_change_stop_times.departure_time is 'Время отправления с остановочного пункта'; 
comment on column rnis.v_change_stop_times.stop_id is 'Идентификатор остановочного пункта';
comment on column rnis.v_change_stop_times.stop_sequence is 'Номер остановочного пункта для рейса';  
comment on column rnis.v_change_stop_times.stop_headsign is 'Наименование остановочного пункта';
comment on column rnis.v_change_stop_times.pickup_type is 'Порядок посадки пассажиров';
comment on column rnis.v_change_stop_times.drop_off_type is 'Порядок высадки пассажиров';
comment on column rnis.v_change_stop_times.shape_dist_traveled is 'Протяженность трассы от конечного пункта маршрута до остановочного пункта';
comment on column rnis.v_change_stop_times.trip_num is 'Порядковый номер рейса транспортного средства';
comment on column rnis.v_change_stop_times.shift_num is 'Номер смены водителя транспортного средства';
comment on column rnis.v_change_stop_times.grafic is 'Идентификатор выхода ТС';

--select * from rnis.v_change_stop_times
