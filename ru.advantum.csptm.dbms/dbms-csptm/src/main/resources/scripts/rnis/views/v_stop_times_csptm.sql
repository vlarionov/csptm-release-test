﻿-- view: rnis.v_stop_times_csptm

-- drop view rnis.v_stop_times_csptm

create or replace view rnis.v_stop_times_csptm as
with active_tt_variant as (
   select tt_variant_id, route_variant_id
     from ttb.tt_variant
    where tt_variant_id in 
          (select ttv.tt_variant_id as tt_variant_id
             from ttb.tt_variant ttv
             join rts.route_variant rtv on rtv.route_variant_id = ttv.route_variant_id
            where not ttv.sign_deleted
              and ttv.action_period @> localtimestamp
              and ttv.parent_tt_variant_id is null
              and ttv.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id())
),
stop_length as (
   select sir.stop_item2round_id, rnd.route_variant_id, rnd.code as round_code, sir.round_id, sir.order_num, stl.stop_id, sir.length_sector,
          sum(sir.length_sector) over (partition by sir.round_id order by sir.order_num) as sum_length_sector
     from rts.stop_item2round sir
     join rts.stop_item sti on sti.stop_item_id = sir.stop_item_id
     join rts.stop_location stl on stl.stop_location_id = sti.stop_location_id
     join rts.round rnd on rnd.round_id = sir.round_id
    where rnd.route_variant_id in (select distinct route_variant_id from active_tt_variant)
      and not sir.sign_deleted
      and not rnd.sign_deleted
),
action_times as (
   select ttv.tt_variant_id, ttv.route_variant_id, tti.time_begin, tti.time_end, tti.stop_item2round_id
     from ttb.tt_variant ttv
     join ttb.tt_out tto on tto.tt_variant_id = ttv.tt_variant_id
     join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
     join ttb.tt_action_item tti on tti.tt_action_id = tta.tt_action_id
    where ttv.parent_tt_variant_id is null
      and tta.parent_tt_action_id is null
      and not ttv.sign_deleted
      and not tto.sign_deleted
      and not tta.sign_deleted
      and not tti.sign_deleted
      and ttv.tt_variant_id in (select distinct tt_variant_id from active_tt_variant)
)
select att.tt_variant_id                 as stop_times_id,
       stl.round_id                      as trip_id,
       to_char(time_begin, 'hh24:mi:ss') as arrival_time,
       to_char(time_end, 'hh24:mi:ss')   as departure_time,
       stop_id,
       sum_length_sector::float/1000     as shape_dist_traveled,
       null::int                         as pickup_type,
       null::text                        as stop_headsign,
       order_num                         as stop_sequence,
       round_code                        as trip_type,
       null::int                         as stop_interval
  from stop_length stl
  left join action_times att on att.route_variant_id = stl.route_variant_id
                            and att.stop_item2round_id = stl.stop_item2round_id
 order by round_id, order_num;

comment on view rnis.v_stop_times_csptm is 'Расписание рейсов НГПТ';
comment on column rnis.v_stop_times_csptm.stop_times_id is 'Код расписания'; 
comment on column rnis.v_stop_times_csptm.trip_id is 'Код рейса'; 
comment on column rnis.v_stop_times_csptm.arrival_time is 'Время прибытия на остановочный пункт';  
comment on column rnis.v_stop_times_csptm.departure_time is 'Время отправления с остановочного пункта';  
comment on column rnis.v_stop_times_csptm.stop_id is 'Идентификатор остановочного пункта';
comment on column rnis.v_stop_times_csptm.shape_dist_traveled is 'Указывается расстояние от первой точки пути следования до остановки'; 
comment on column rnis.v_stop_times_csptm.pickup_type is 'Тип посадки';
comment on column rnis.v_stop_times_csptm.stop_headsign is 'Пункт назначения рейса';
comment on column rnis.v_stop_times_csptm.stop_sequence is 'Номер остановки по пути следования';
comment on column rnis.v_stop_times_csptm.trip_type is 'Тип рейса';
comment on column rnis.v_stop_times_csptm.stop_interval is 'Интервал на маршруте';

-- select * from rnis.v_stop_times_csptm