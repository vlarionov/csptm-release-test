﻿-- view: rnis.v_vehicle_task

-- drop view rnis.v_vehicle_task

create or replace view rnis.v_vehicle_task as
with out_list as (
   select tto.tt_out_id, ttv.order_date, tto.tr_id, rtv.route_id, tto.tt_out_num
     from ttb.tt_out tto
     join ttb.tt_variant ttv on ttv.tt_variant_id = tto.tt_variant_id
     join rts.route_variant rtv on rtv.route_variant_id = ttv.route_variant_id
    where ttv.order_date = now()::date
      and ttv.tt_status_id = ttb.jf_tt_status_pkg$cn_active_status_id()
      and tto.tr_id is not null
),
drv_list as (
   select tta.tt_out_id, tti.driver_id, shi.dr_shift_num,
          min(time_begin) as time_begin,
          max(time_end)   as time_end
     from ttb.tt_action tta
     join ttb.tt_action_item tti on tti.tt_action_id = tta.tt_action_id
     join ttb.dr_shift shi on shi.dr_shift_id = tti.dr_shift_id
    where tta.tt_out_id in (select tt_out_id from out_list)
    group by tta.tt_out_id, tti.driver_id, shi.dr_shift_num
)
select to_char(o.order_date, 'yyyy-mm-dd') as date,
       o.tr_id        as vehicle_id,
       o.route_id,
       o.tt_out_num   as grafic,
       d.dr_shift_num as shift_num,
       to_char(d.time_begin, 'yyyy-mm-dd hh:mi') as start_time,
       to_char(d.time_end, 'yyyy-mm-dd hh:mi')   as end_time,
       d.driver_id 
  from out_list o
  left join drv_list d on d.tt_out_id = o.tt_out_id
 order by o.tr_id, o.tt_out_num, d.dr_shift_num;

comment on view rnis.v_vehicle_task is 'Справочник назначений ТС на маршрут';
comment on column rnis.v_vehicle_task.date is 'Сутки';
comment on column rnis.v_vehicle_task.shift_num is 'Номер смены водителя ТС';
comment on column rnis.v_vehicle_task.start_time is 'Время начала смены';
comment on column rnis.v_vehicle_task.end_time is 'Время окончания смены';
comment on column rnis.v_vehicle_task.driver_id is 'Идентификатор водителя';

--select * from rnis.v_vehicle_task
