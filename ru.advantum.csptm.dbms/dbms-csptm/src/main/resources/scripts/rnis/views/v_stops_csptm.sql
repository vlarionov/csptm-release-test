﻿-- view: rnis.v_stops_csptm

-- drop view rnis.v_stops_csptm

create or replace view rnis.v_stops_csptm as
select stp.stop_id      as stop_id,
       stp.name         as stop_name,
       stp.name         as StationName,
       st_y(geom)       as stop_lat,
       st_x(geom)       as stop_lon,
       gst.street       as Street,
       null::text       as Direction,
       'нет'::text      as Pavilion,
       null::text       as OperatingOrgName,
       case when stp.sign_deleted then 'не активна' else 'активна' end as EntryState,
       null::text       as EntryAddReason,
       null::text       as EntryModifyReason,
       null::text       as EntryDeleteReason
 from (select distinct stop_location_id from rts.stop_item where not sign_deleted) sti
 join rts.stop_location stl on stl.stop_location_id = sti.stop_location_id 
 join rts.stop stp on stp.stop_id = stl.stop_id
 join rts.stop2gis stg on stg.stop_id = stl.stop_id
 join gis.stops gst on gst.muid = stg.stop_muid
where not stl.sign_deleted;

comment on view rnis.v_stops_csptm is 'Справочник остановочных пунктов';
comment on column rnis.v_stops_csptm.stop_id is 'Идентификатор остановочного пункта'; 
comment on column rnis.v_stops_csptm.stop_name is 'Полное наименование остановочного пункта'; 
comment on column rnis.v_stops_csptm.StationName is 'Название остановки согласно указателю'; 
comment on column rnis.v_stops_csptm.stop_lat is 'Широта расположения остановочного пункта'; 
comment on column rnis.v_stops_csptm.stop_lon is 'Долгота расположения остановочного пункта'; 
comment on column rnis.v_stops_csptm.Street is 'Наименование улицы, на которой находится остановка'; 
comment on column rnis.v_stops_csptm.Direction is 'Направление'; 
comment on column rnis.v_stops_csptm.Pavilion is 'Наличие павильона'; 
comment on column rnis.v_stops_csptm.OperatingOrgName is 'Балансодержатель остановки с павильоном';
comment on column rnis.v_stops_csptm.EntryState is 'Состояние записи';
comment on column rnis.v_stops_csptm.EntryAddReason is 'Причина добавления записи';
comment on column rnis.v_stops_csptm.EntryModifyReason is 'Причина изменения записи'; 
comment on column rnis.v_stops_csptm.EntryDeleteReason is 'Причина удаления записи';

-- select * from rnis.v_stops_csptm limit 100

-- 12109
-- 11722
