﻿-- view: rnis.v_change_equipment

-- drop view rnis.v_change_equipment

create or replace view rnis.v_change_equipment as
select unt.unit_id           as equipment_id,
       unt.unit_num          as identificator,
       uth.tr_id             as vehicle_id,
       to_char(lower(uth.sys_period), 'yyyymmdd hh24:mi:ss') as time_change
  from core.unit unt
  join hist.v_unit2tr_hist uth on uth.unit_id = unt.unit_id
 where tstzrange(now() - interval '30 day', null) @> uth.sys_period
union all
select null::bigint          as equipment_id,
       unt.unit_num          as identificator,
       uth.tr_id             as vehicle_id,
       to_char(upper(uth.sys_period), 'yyyymmdd hh24:mi:ss') as time_change
  from core.unit unt
  join hist.v_unit2tr_hist uth on uth.unit_id = unt.unit_id
 where tstzrange(now() - interval '30 day', null) && uth.sys_period
   and upper(uth.sys_period) is not null
 order by vehicle_id, time_change;

comment on view rnis.v_change_equipment is 'Сведения о перестановке бортового оборудования';
comment on column rnis.v_change_equipment.vehicle_id is 'Уникальный идентификатор ТС'; 
comment on column rnis.v_change_equipment.equipment_id is 'Идентификатор установленного бортового блока'; 
comment on column rnis.v_change_equipment.time_change is 'Дата замены/установки/снятия блока на ТС'; 

--select * from rnis.v_change_equipment limit 100
