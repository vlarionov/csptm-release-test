﻿drop view rnis.v_com_vehicles;

create or replace view rnis.v_com_vehicles as
select object_id
  from rnis.com_vehicles
 where tmatic_id is not null
   and object_id is not null
   and depot_code = 'TScomp';

comment on view rnis.v_com_vehicles is 'Справочник транспортных средств коммерческих перевозчиков';
comment on column rnis.v_com_vehicles.object_id is 'ID в базе данных РНИС'; 

--select * from rnis.v_com_vehicles limit 100