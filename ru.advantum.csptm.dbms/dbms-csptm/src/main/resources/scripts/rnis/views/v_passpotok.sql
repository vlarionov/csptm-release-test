﻿-- view: rnis.v_passpotok

--drop view rnis.v_passpotok

create or replace view rnis.v_passpotok as
select agg.asmpp_agg_id as survey_id,
       to_char(agg.time_start, 'yyyy-mm-dd') as survey_start_date,
       to_char(agg.time_finish, 'yyyy-mm-dd') as survey_end_date,
       rtv.route_id,
       rdg.round_id as trip_id,
       stl.stop_id,
       to_char(agi.time_start, 'hh:mi') as arrival_time,
       coalesce(in_corrected, in_cnt) as pass_in,
       coalesce(out_corrected, out_cnt) as pass_out,
       'н/д'::text as fact_trip_datetime,
       agi.tr_id as vehicle_id,
       tte.timetable_entry_num as grafic,
       orr.order_round_num as trip_num,
       orl.shift as shift_num,
       'обработано'::text as status,
       agi.stop_order_num as stop_sequence
  from askp.asmpp_agg_item agi
  join askp.asmpp_agg agg on agg.asmpp_agg_id = agi.asmpp_agg_id
  join rts.stop_location2gis lcg on lcg.stop_location_muid = agi.stop_place_muid
  join rts.stop_location stl on stl.stop_location_id = lcg.stop_location_id
  join tt.order_round orr on orr.order_round_id = agi.order_round_id
  join tt.order_list orl on orl.order_list_id = orr.order_list_id
  join tt.timetable_entry tte on tte.timetable_entry_id = orl.timetable_entry_id
  join tt.round rnd on rnd.round_id = orr.round_id
  join rts.round2gis rdg on rdg.route_trajectory_muid = rnd.route_trajectory_muid
  join rts.route_variant2gis rvg on rvg.route_variant_muid = agi.route_variants_muid
  join rts.route_variant rtv on rtv.route_variant_id = rvg.route_variant_id
 where agg.time_start >= localtimestamp::date::timestamp - interval '2 d';

comment on view rnis.v_passpotok is 'Данные о пассажиропотоке на рейсах';
comment on column rnis.v_passpotok.survey_id is 'Идентификатор обследования';
comment on column rnis.v_passpotok.survey_start_date is 'Дата начала обследования';
comment on column rnis.v_passpotok.survey_end_date is 'Дата завершения обследования';
comment on column rnis.v_passpotok.route_id is 'Идентификатор маршрута';
comment on column rnis.v_passpotok.trip_id is 'Идентификатор рейса';
comment on column rnis.v_passpotok.stop_id is 'Идентификатор остановки';
comment on column rnis.v_passpotok.arrival_time is 'Фактическое время прибытия';
comment on column rnis.v_passpotok.pass_in is 'Количество вошедших пассажиров';
comment on column rnis.v_passpotok.pass_out is 'Количество вышедших пассажиров';
comment on column rnis.v_passpotok.fact_trip_datetime is 'Дата и время начала выполненного рейса';
comment on column rnis.v_passpotok.vehicle_id is 'Идентификатор транспортного средства';
comment on column rnis.v_passpotok.grafic is 'Идентификатор выхода ТС'; 
comment on column rnis.v_passpotok.trip_num is 'Порядковый номер рейса ТС'; 
comment on column rnis.v_passpotok.shift_num is 'Номер смены водителя';
comment on column rnis.v_passpotok.status is 'Статус обработки';
comment on column rnis.v_passpotok.stop_sequence is 'Порядковый номер остановочного пункта для рейса';

--select * from rnis.v_passpotok limit 100
