﻿-- view: rnis.v_impact

-- drop view rnis.v_impact

create or replace view rnis.v_impact as
select q.impact_id,
       q.depo_id,
       q.impact_time,
       q.vehicle_id,
       (q.trip_data).trip_date,
       (q.trip_data).trip_id,
       (q.trip_data).grafic,
       (q.trip_data).shift_num,
       (q.trip_data).route_num,
       q.impact_code,
       q.impact_name,
       q.impact_text_value,
       q.impact_event_time
  from (select cmnd_request.cmnd_request_id as impact_id,
               tr.depo_id,
               to_char(cmnd_request.request_time, 'yyyy-mm-dd hh:mi') as impact_time,
               u2t.tr_id as vehicle_id,
               cmnd_ref.cmnd_ref_protocol as impact_code,
               cmnd_ref.cmnd_ref_name as impact_name,
               request_param::text as impact_text_value,
               to_char(cmnd_response.response_time, 'yyyy-mm-dd hh:mi') as impact_event_time,
               ttb.tt_export_service_pkg$get_shift_and_driver_of_tr(u2t.tr_id, cmnd_request.request_time) as trip_data
          from cmnd.cmnd_request
          join cmnd.cmnd_ref on cmnd_request.cmnd_ref_id = cmnd_ref.cmnd_ref_id
          join core.unit on unit.unit_id = cmnd_request.unit_id
          join core.equipment et on et.equipment_id= unit.unit_id
          join cmnd.cmnd_status on cmnd_status.cmnd_status_id = cmnd.cmnd_request.cmnd_status_id
          join core.equipment_status ets on ets.equipment_status_id = et.equipment_status_id
          left join cmnd.cmnd_response on (cmnd_request.cmnd_request_id = cmnd_response.cmnd_request_id)
          left join hist.v_unit2tr_hist u2t on u2t.unit_id = cmnd_request.unit_id
          left join core.tr on tr.tr_id = u2t.tr_id
         where cmnd_request.request_time > localtimestamp - interval '1 h'
       ) q;

comment on view rnis.v_impact is 'Управляющие воздействия';
comment on column rnis.v_impact.impact_id is 'Идентификатор управляющего воздействия';
comment on column rnis.v_impact.depo_id is 'Идентификатор автотранспортного предприятия';
comment on column rnis.v_impact.trip_date is 'Дата рейса';
comment on column rnis.v_impact.impact_time is 'Время управляющего воздействия';
comment on column rnis.v_impact.vehicle_id is 'Идентификатор транспортного средства';
comment on column rnis.v_impact.trip_id is 'Идентификатор рейса';
comment on column rnis.v_impact.grafic is 'Идентификатор выхода ТС';
comment on column rnis.v_impact.route_num is 'Порядковый номер рейса транспортного средства';
comment on column rnis.v_impact.shift_num is 'Номер смены водителя транспортного средства';
comment on column rnis.v_impact.impact_code is 'Код управляющего воздействия';
comment on column rnis.v_impact.impact_name is 'Наименование управляющего воздействия';
comment on column rnis.v_impact.impact_text_value is 'Сообщение диспетчерского управляющего воздействия';
comment on column rnis.v_impact.impact_event_time is 'Время для указания времени исполнения диспетчерского управляющего воздействия';

--select * from rnis.v_impact limit 100
