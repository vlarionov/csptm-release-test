﻿-- view: rnis.v_drivers

-- drop view rnis.v_drivers

create or replace view rnis.v_drivers as
select driver_last_name as last_name, 
       driver_name as first_name,
       driver_middle_name as middle_name, 
       depo_id as depot_id, 
       tab_num as driver_id
  from core.driver;

comment on view rnis.v_drivers is 'Справочник водителей';
comment on column rnis.v_drivers.last_name is 'Фамилия'; 
comment on column rnis.v_drivers.first_name is 'Имя'; 
comment on column rnis.v_drivers.middle_name is 'Отчество';
comment on column rnis.v_drivers.depot_id is 'Идентификатор автотранспортного предприятия'; 
comment on column rnis.v_drivers.driver_id is 'Табельный номер'; 

--select * from rnis.v_drivers limit 100

--drop table rnis.drivers
