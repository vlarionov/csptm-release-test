﻿-- view: rnis.v_depots

-- drop view rnis.v_depots

create or replace view rnis.v_depots as
select d.carrier_id   as agency_id,
       d.depo_id      as depot_id,
       e.name_full    as depot_name,
       d.wkt_geom,
       d.sign_deleted as is_deleted
  from core.depo d
  join core.entity e on e.entity_id = d.depo_id;

comment on view rnis.v_depots is 'Справочник траспортных предприятий';
comment on column rnis.v_depots.depot_id is 'Идентификатор транспортного предприятия';
comment on column rnis.v_depots.agency_id is 'Идентификатор транспортной организации';
comment on column rnis.v_depots.depot_name is 'Наименование транспортного предприятия';
comment on column rnis.v_depots.wkt_geom is 'Координаты автотранспортного предприятия в формате WKT';
comment on column rnis.v_depots.is_deleted is 'Признак удаления предприятия';

-- select * from rnis.v_depots
