﻿-- view: rnis.v_trip_shapes

-- drop view rnis.v_trip_shapes

create or replace view rnis.v_trip_shapes as
select rnd.round_id         as shape_id,
       rnd.round_id         as trip_id,
       gsr.graph_section_id as arc_id,
       gsr.order_num        as arc_sequence,
       grs.wkt_geom         as arc_geometry,
       null::int            as arc_max_speed,
       ttb.tt_export_service_pkg$get_round_section_len_list(rnd.round_id, gsr.graph_section_id) as arc_stops_id,
       ttb.tt_export_service_pkg$get_round_section_stop_id_list(rnd.round_id, gsr.graph_section_id) as arc_stops_dist
  from rts.route rte
  join rts.route_variant rtv on rtv.route_variant_id = rte.current_route_variant_id
  join rts.round rnd on rnd.route_variant_id = rtv.route_variant_id
  join rts.graph_sec2round gsr on gsr.round_id = rnd.round_id
  join rts.graph_section grs on grs.graph_section_id = gsr.graph_section_id
 where not rte.sign_deleted
   and not rtv.sign_deleted
   and not rnd.sign_deleted
   and not gsr.sign_deleted
   and not grs.sign_deleted;

comment on view rnis.v_trip_shapes is 'Справочник трасс рейсов';
comment on column rnis.v_trip_shapes.shape_id is 'Идентификатор трассы рейса';
comment on column rnis.v_trip_shapes.trip_id is 'Идентификатор рейса';
comment on column rnis.v_trip_shapes.arc_id is 'Уникальный идентификатор ребра графа'; 
comment on column rnis.v_trip_shapes.arc_sequence is 'Порядковый номер ребра отсчитываемый относительно начала трассы рейса'; 
comment on column rnis.v_trip_shapes.arc_geometry is 'Геометрия маршрута'; 
comment on column rnis.v_trip_shapes.arc_max_speed is 'Максимально разрешенная скорость'; 
comment on column rnis.v_trip_shapes.arc_stops_id is 'Массив идентификаторов остановочных пунктов маршрута'; 
comment on column rnis.v_trip_shapes.arc_stops_dist is 'Массив положений остановочных пунктов на ребре графа'; 

-- select * from rnis.v_trip_shapes limit 100
