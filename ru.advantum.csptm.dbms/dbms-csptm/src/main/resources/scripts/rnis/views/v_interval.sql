﻿-- view: rnis.v_interval

-- drop view rnis.v_interval

create or replace view rnis.v_interval as
with tt_route_variant as (
   select wttv.tt_variant_id, wttv.route_variant_id
     from ttb.tt_variant wttv
    where wttv.parent_tt_variant_id is null
      and not wttv.sign_deleted
      and wttv.tt_status_id in (ttb.tt_status_pkg$active(), ttb.tt_status_pkg$agreed())
      and wttv.action_period @> localtimestamp
)
select s.route_variant_id as service_id,
       'н/д'::text as depot_id,
       to_char(s.start_time, 'hh:mi') as start_time,
       to_char(s.start_time + interval '30 min', 'hh:mi') as end_time,
       s.interval_min, s.interval_max,
       rtv.route_id
  from (select q.route_variant_id, q.start_time,
               min(stop_interval) as interval_min,
               max(stop_interval) as interval_max
          from (select ttv.route_variant_id,
                       core.floor_minutes(tti.time_begin, 30) as start_time,
                       core.interval2minutes(tti.time_begin - lag(tti.time_begin) over (partition by tti.stop_item2round_id order by tti.time_begin)) as stop_interval
                  from tt_route_variant ttv
                  join ttb.tt_out tto on tto.tt_variant_id = ttv.tt_variant_id 
                  join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
                  join ttb.tt_action_round ttr on ttr.tt_action_id = tta.tt_action_id
                  join ttb.tt_action_item tti on tti.tt_action_id = tta.tt_action_id
                  join rts.round rnd on rnd.round_id = ttr.round_id
                 where not tto.sign_deleted
                   and not tta.sign_deleted
                   and not tti.sign_deleted
                   and ttb.action_type_pkg$is_production_round(tta.action_type_id)
               ) q
         where q.stop_interval > 0
         group by q.route_variant_id, q.start_time
       ) s
  join rts.route_variant rtv on rtv.route_variant_id = s.route_variant_id
 where not rtv.sign_deleted
 order by s.route_variant_id, s.start_time;

comment on view rnis.v_interval is 'Справочник интервалов движения на маршруте';
comment on column rnis.v_interval.service_id is 'Идентификатор периода действия маршрута'; 
comment on column rnis.v_interval.depot_id is 'Идентификатор автотранспортного предприятия'; 
comment on column rnis.v_interval.start_time is 'Час начала действия интервальности'; 
comment on column rnis.v_interval.end_time is 'Час окончания действия интервальности'; 
comment on column rnis.v_interval.interval_min is 'Минимальное значение интервала'; 
comment on column rnis.v_interval.interval_max is 'Максимальное значение интервала'; 
comment on column rnis.v_interval.route_id is 'Идентификатор маршрута';

-- select * from rnis.v_interval limit 100
