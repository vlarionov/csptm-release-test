﻿-- view: rnis.v_agency

-- drop view rnis.v_agency

create or replace view rnis.v_agency as
select carrier_id            as agency_id,
       name_full             as agency_name,
       case when carrier_id = core.carrier_pkg$mgt() then 'http://www.mosgortrans.ru/'::text else null::text end as agency_url,
       'Europe/Moscow'::text as agency_timezone,
       'RU'::text            as agency_lang,
       null::text            as agency_phone,
       null::text            as agency_fare_url
  from core.carrier c
  join core.entity e on e.entity_id = c.carrier_id
 where c.sign_deleted = 0
   and carrier_id = core.carrier_pkg$mgt();

comment on view rnis.v_agency is 'Справочник транспортных организаций';
comment on column rnis.v_agency.agency_id is 'Идентификатор транспортной организации';  
comment on column rnis.v_agency.agency_name is 'Наименование транспортной организации';  
comment on column rnis.v_agency.agency_url is 'Сайт транспортной организации';  
comment on column rnis.v_agency.agency_timezone is 'Часовой пояс транспортной организации';  
comment on column rnis.v_agency.agency_lang is 'Языковой код транспортной организации';  
comment on column rnis.v_agency.agency_phone is 'Телефон транспортной организации';  
comment on column rnis.v_agency.agency_fare_url is 'Сайт покупки билетов на рейсы транспортной организации'; 

-- select * from rnis.v_agency
