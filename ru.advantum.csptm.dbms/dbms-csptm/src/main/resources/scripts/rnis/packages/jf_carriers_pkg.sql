
create or replace function rnis.jf_carriers_pkg$of_rows
  (v_id_account   in numeric,
   v_rows         out refcursor,
   v_attr         in text)
returns refcursor 
as $$ declare
begin
   open v_rows for
   select distinct carrier as "CARRIER"
     from rnis.com_vehicles
    where carrier is not null
    order by carrier;
end; 
$$ language 'plpgsql' volatile security invoker;
