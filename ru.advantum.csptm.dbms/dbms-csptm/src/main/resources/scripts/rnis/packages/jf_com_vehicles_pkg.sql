
create or replace function rnis.jf_com_vehicles_pkg$of_rows
  (v_id_account   in numeric,
   v_rows         out refcursor,
   v_attr         in text)
returns refcursor 
as $$ declare
   v_begin_dt rnis.com_vehicles.change_time%type := jofl.jofl_pkg$extract_date(v_attr, 'F_BEGIN_DT',  true);
   v_end_dt   rnis.com_vehicles.change_time%type := jofl.jofl_pkg$extract_date(v_attr, 'F_END_DT',    true);
   v_carrier  rnis.com_vehicles.carrier%type     := jofl.jofl_pkg$extract_varchar(v_attr,'F_CARRIER', true);
   v_active   integer                            := jofl.jofl_pkg$extract_number(v_attr, 'F_ACTIVE_INPLACE_K',  true);
begin
   
   if date(v_begin_dt) = date(v_end_dt) and date(v_begin_dt) = date(now()) then
      v_begin_dt := null;
      v_end_dt   := null;
   end if;
   
   open v_rows for
   select c.object_id, c.depot_code, 
          coalesce(c.marka,    'н/д') as marka,
          coalesce(c.category, 'н/д') as category,
          coalesce(c.model,    'н/д') as model,
          coalesce(c.carrier,  'н/д') as carrier, 
          c.garage_nomer, c.name, c.change_time, c.id, c.att, 
          coalesce(c.label,    'н/д') as label,
          c.tmatic_id, c.gos_nomer, c.sign_deleted, c.out_date
     from rnis.com_vehicles c
     join rnis.v_com_vehicles v on v.object_id = c.object_id
    where ((v_begin_dt is null or v_end_dt is null) or c.change_time between v_begin_dt and v_end_dt)
      and (v_carrier is null or v_carrier = '' or c.carrier = v_carrier)
      and (v_active is null or (v_active = 1 and out_date is null) or (v_active = 0 and out_date is not null));
end;
$$ language 'plpgsql' volatile security invoker;
