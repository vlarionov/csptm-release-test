﻿-- ========================================================================
-- Пакет загрузки данных РНИС
-- ========================================================================
-- Created:   Date: 20.03.2017               Author: abc
-- Modified:  Date: 20.03.2017               Author: abc
-- ========================================================================

create or replace function rnis.rnis_import_pkg$import_radio_jam_color
   (v_situation_time          in timestamp,
    v_line_number             in bigint,
    v_node_number             in bigint,
    v_situation_code          in bigint)
returns integer
-- ========================================================================
-- Импорт данных о скорости транспортных потоков
-- ========================================================================
as $$ declare
    v_result                  integer;
begin
    
    insert into rnis.radio_jam_color as t (situation_time, line_number, node_number, situation_code)
    values (v_situation_time, v_line_number, v_node_number, v_situation_code)
    on conflict on constraint pk_radio_jam_color do
    update set situation_code = excluded.situation_code;
    -- where t.situation_code is distinct from excluded.situation_code;
    
    get diagnostics v_result = row_count;
    return v_result;
end;
$$ language plpgsql security definer;


create or replace function rnis.rnis_import_pkg$clear_radio_jam_color()
returns integer
-- ========================================================================
-- Очистка данных о скорости транспортных потоков
-- ========================================================================
as $$ declare
   v_result integer;
begin

   delete from rnis.radio_jam_color;

   get diagnostics v_result = row_count;
   return v_result;
end;
$$ language plpgsql security definer;

-- ========================================================================
-- ========================================================================
-- Звгрузка коммерческих ТС

create or replace function rnis.rnis_import_pkg$get_package_num()
returns bigint
-- ========================================================================
-- ID пользователя по имени 
-- ========================================================================
as $$ declare
   v_result                     bigint := 0;
begin
   select nextval('rnis.seq_package_num') into v_result;
   return v_result;
end;
$$ language plpgsql security definer;


create or replace function rnis.rnis_import_pkg$clear_package
   (v_package_num             in bigint)
returns integer
-- ========================================================================
-- Удалить пакет из буфера
-- ========================================================================
as $$ declare
   v_result                     bigint := 0;
begin
   delete from rnis.com_vehicles_tmp_status where package_num = v_package_num;
   delete from rnis.com_vehicles_tmp_status where package_num < v_package_num - 10;
   
   get diagnostics v_result = row_count;
   return v_result;
end;
$$ language plpgsql security definer;


create or replace function rnis.rnis_import_pkg$import_com_vehicles
   (v_depot_code              in text,
    v_marka                   in text,
    v_category                in text,
    v_model                   in text, 
    v_carrier                 in text,
    v_garage_nomer            in text, 
    v_object_id               in bigint,
    v_name                    in text,
    v_change_time             in timestamp, 
    v_id                      in text, 
    v_att                     in text, 
    v_label                   in text, 
    v_tmatic_id               in bigint,
    v_gos_nomer               in text)
returns integer
-- ========================================================================
-- Импорт данных о ТС коммерческих перевозчиков
-- ========================================================================
as $$ declare
    v_result                  integer;
begin
    
    insert into rnis.com_vehicles as t (depot_code, marka, category, model, carrier, garage_nomer, object_id, name, change_time, id, att, label, tmatic_id, gos_nomer)
    values (v_depot_code, v_marka, v_category, v_model, v_carrier, v_garage_nomer, v_object_id, v_name, v_change_time, v_id, v_att, v_label, v_tmatic_id, v_gos_nomer)
    on conflict (object_id)
    do update set (depot_code, marka, category, model, carrier, garage_nomer, name, change_time, id, att, label, tmatic_id, gos_nomer) = 
                  (excluded.depot_code, excluded.marka, excluded.category, excluded.model, excluded.carrier, excluded.garage_nomer, 
                   excluded.name, excluded.change_time, excluded.id, excluded.att, excluded.label, excluded.tmatic_id, excluded.gos_nomer)
    where t.depot_code   is distinct from excluded.depot_code   or
          t.marka        is distinct from excluded.marka        or
          t.category     is distinct from excluded.category     or
          t.model        is distinct from excluded.model        or
          t.carrier      is distinct from excluded.carrier      or
          t.garage_nomer is distinct from excluded.garage_nomer or
          t.name         is distinct from excluded.name         or
          t.change_time  is distinct from excluded.change_time  or
          t.id           is distinct from excluded.id           or
          t.att          is distinct from excluded.att          or
          t.label        is distinct from excluded.label        or
          t.tmatic_id    is distinct from excluded.tmatic_id    or
          t.gos_nomer    is distinct from excluded.gos_nomer;
    
    get diagnostics v_result = row_count;
    return v_result;
end;
$$ language plpgsql security definer;


create or replace function rnis.rnis_import_pkg$import_com_vehicles
   (v_package_num             in bigint,
    v_depot_code              in text,
    v_marka                   in text,
    v_category                in text,
    v_model                   in text, 
    v_carrier                 in text,
    v_garage_nomer            in text, 
    v_object_id               in bigint,
    v_name                    in text,
    v_change_time             in timestamp, 
    v_id                      in text, 
    v_att                     in text, 
    v_label                   in text, 
    v_tmatic_id               in bigint,
    v_gos_nomer               in text)
returns integer
-- ========================================================================
-- Загрузка данных ТС в буфер
-- ========================================================================
as $$ declare
   v_result                   integer := 0;
begin
   
   select rnis.rnis_import_pkg$import_com_vehicles (v_depot_code, v_marka, v_category, v_model, v_carrier,
                                                    v_garage_nomer, v_object_id, v_name, v_change_time, v_id,
                                                    v_att, v_label, v_tmatic_id, v_gos_nomer)
     into v_result;
   
   insert into rnis.com_vehicles_tmp_status as t (package_num, depot_code, object_id)
   values (v_package_num, v_depot_code, v_object_id)
   on conflict (package_num, depot_code, object_id)
   do nothing;
   
   return v_result;
end;
$$ language plpgsql security definer;


create or replace function rnis.rnis_import_pkg$check_deleted
   (v_package_num             in bigint)
returns integer
-- ========================================================================
-- Проверка удаленных
-- ========================================================================
as $$ declare
   v_result                   integer := -1;
   v_tmp                      integer := 0;
   v_depot_code               text;
begin
   
   select depot_code into v_depot_code
     from rnis.com_vehicles_tmp_status
    where package_num = v_package_num
    limit 1;
   
   if not found then
      return v_result;
   end if;
   
   update rnis.com_vehicles c
      set sign_deleted = 1,
          out_date     = now()
    where not exists(select 1 from rnis.com_vehicles_tmp_status t
                      where t.package_num = v_package_num
                        and t.object_id = c.object_id)
      and c.sign_deleted <> 1
      and c.depot_code = v_depot_code;
   
   get diagnostics v_result = row_count;
   
   update rnis.com_vehicles c
      set sign_deleted = 0,
          out_date     = null
     from rnis.com_vehicles_tmp_status t
    where t.object_id = c.object_id
      and t.package_num = v_package_num
      and c.sign_deleted <> 0;
   
   get diagnostics v_tmp = row_count;
   return v_result + v_tmp;
end;
$$ language plpgsql security definer;
