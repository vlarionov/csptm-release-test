﻿
create or replace function rnis.jf_track_pkg$of_rows
  (l_account in numeric, 
   l_rows    out refcursor, 
   l_attr    in text default '{}') 
returns refcursor 
as $$ declare
   l_begin_dt     timestamp without time zone         := jofl.jofl_pkg$extract_date(l_attr,    'F_BEGIN_DT', true);
   l_end_dt       timestamp without time zone         := jofl.jofl_pkg$extract_date(l_attr,    'F_END_DT',   true);
   l_carrier      rnis.com_vehicles.carrier%type      := jofl.jofl_pkg$extract_varchar(l_attr, 'F_CARRIER',  true);
   l_gos_nomer    rnis.com_vehicles.gos_nomer%type    := jofl.jofl_pkg$extract_varchar(l_attr, 'F_GOS_NOMER_TEXT',  true);
   l_garage_nomer rnis.com_vehicles.garage_nomer%type := jofl.jofl_pkg$extract_varchar(l_attr, 'F_GRG_NOMER_TEXT',  true);
begin
   if (l_gos_nomer is null or l_gos_nomer = '') and (l_garage_nomer is null or l_garage_nomer = '') then
      l_gos_nomer := 'n/a';
   end if;
   
   open l_rows for
   select (event_time)::text||'.0' "GMTEVENTTIME",
          tmatic_id::text          "TMATIC_ID",
          gos_nomer                "GOS_NOMER",
          garage_nomer             "GARAGE_NOMER",
          carrier                  "CARRIER",
          lat::text                "LAT",
          lon::text                "LON",
          speed                    "SPEED",
          heading                  "HEADING"
     from rnis.traffic_rnis t
     join rnis.com_vehicles v on v.object_id = t.tr_id
    where event_time between l_begin_dt and l_end_dt
      and (l_carrier is null or l_carrier = '' or v.carrier = l_carrier)
      and (l_gos_nomer is null or l_gos_nomer = '' or gos_nomer = l_gos_nomer)
      and (l_garage_nomer is null or l_garage_nomer = '' or garage_nomer = l_garage_nomer)
      and lat != 0
      and lon != 0
      and location_valid
    order by event_time;
end;
$$ language plpgsql;
