﻿-- Tablespace: gis_data

-- DROP TABLESPACE rnis_data

CREATE TABLESPACE rnis_data
  OWNER adv
  LOCATION '/u00/postgres/pg_data/rnis/rnis_data';

-- Tablespace: rnis_idx

-- DROP TABLESPACE rnis_idx

CREATE TABLESPACE rnis_idx
  OWNER adv
  LOCATION '/u00/postgres/pg_data/rnis/rnis_idx';