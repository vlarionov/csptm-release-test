﻿-- ========================================================================
-- Пакет выгрузки данных из ЕАСУ ФХД
-- ========================================================================

create or replace function easu.easu_export_pkg$start_export()
returns int
-- ========================================================================
-- Фиксирование момента начала выгрузки данных
-- ========================================================================
as $$ declare
   v_result    int := 0;
begin
   insert into easu.export_hist as eh (export_time, status) 
   values (localtimestamp, 'B')
   on conflict (export_time)
   do update set status = excluded.export_time
       where eh.status is distinct from excluded.status
   returning export_id into v_result;
   
   return v_result;
end;
$$ language plpgsql;

create or replace function easu.easu_export_pkg$finish_export_success
  (p_export_id       in bigint)
returns int
-- ========================================================================
-- Фиксирование момента окончания выгрузки данных и и сохранение атрибутов 
-- ========================================================================
as $$ declare
   v_result    int := 0;
begin
   
   insert into easu.route_variant_export_last as tt
         (route_variant_id, route_variant_export_id)
   select route_variant_id, route_variant_export_id
     from easu.route_variant_export
    where export_id = p_export_id
   on conflict (route_variant_id)
   do update set route_variant_export_id = excluded.route_variant_export_id
       where tt.route_variant_export_id is distinct from excluded.route_variant_export_id;
   
   insert into easu.route_variant_export_tt as tt
          (route_variant_export_id, tt_variant_id, route_variant_id, action_period)
   select rve.route_variant_export_id, ttv.tt_variant_id, ttv.route_variant_id, ttv.action_period
     from easu.route_variant_export rve
     join easu.route_variant_export_last rel on rel.route_variant_export_id = rve.route_variant_export_id
     join ttb.tt_variant ttv on ttv.route_variant_id = rel.route_variant_id
    where rve.export_id = p_export_id
      and not ttv.sign_deleted
      and ttv.tt_status_id in (ttb.tt_status_pkg$active(), ttb.tt_status_pkg$agreed())
   on conflict (route_variant_export_id, tt_variant_id) do 
   update set route_variant_id = excluded.route_variant_id,
              action_period = excluded.action_period
    where tt.route_variant_id is distinct from excluded.route_variant_id or
          tt.action_period    is distinct from excluded.action_period;
   
   insert into easu.route_variant_export_calendar as tt
          (route_variant_export_id, tt_variant_id, calendar_tag_id, is_include)
   select rve.route_variant_export_id,  ttv.tt_variant_id, ttc.calendar_tag_id, ttc.is_include
     from easu.route_variant_export rve
     join easu.route_variant_export_last rel on rel.route_variant_export_id = rve.route_variant_export_id
     join ttb.tt_variant ttv on ttv.route_variant_id = rel.route_variant_id
     join ttb.tt_calendar ttc on ttc.tt_variant_id = ttv.tt_variant_id
    where rve.export_id = p_export_id
      and not ttv.sign_deleted
      and ttv.tt_status_id in (ttb.tt_status_pkg$active(), ttb.tt_status_pkg$agreed())
   on conflict (route_variant_export_id, tt_variant_id, calendar_tag_id) do 
   update set is_include = excluded.is_include
    where tt.is_include is distinct from excluded.is_include;
   
   insert into easu.route_variant_export_calendar_item
          (route_variant_export_id, tt_variant_id, calendar_tag_id, calendar_id)
   select rve.route_variant_export_id, ttv.tt_variant_id, ttc.calendar_tag_id, tci.calendar_id
     from easu.route_variant_export rve
     join easu.route_variant_export_last rel on rel.route_variant_export_id = rve.route_variant_export_id
     join ttb.tt_variant ttv on ttv.route_variant_id = rel.route_variant_id
     join ttb.tt_calendar ttc on ttc.tt_variant_id = ttv.tt_variant_id
     join ttb.calendar_tag tct on tct.calendar_tag_id = ttc.calendar_tag_id
     join ttb.calendar_item tci on tci.calendar_tag_id = ttc.calendar_tag_id
     join ttb.calendar cld on cld.calendar_id = tci.calendar_id
    where rve.export_id = p_export_id
      and not ttv.sign_deleted
      and ttv.tt_status_id in (ttb.tt_status_pkg$active(), ttb.tt_status_pkg$agreed())
      and extract(year from cld.dt) = extract(year from now())
      and tct.calendar_tag_group_id not in (ttb.calendar_pkg$get_daysofweek_tag_group_id(),
                                            ttb.calendar_pkg$get_season_tag_group_id())
      and not (extract(dow from cld.dt) in (0, 6) and tct.calendar_tag_id = ttb.calendar_pkg$get_holiday_tag_id())
      and not (extract(dow from cld.dt) in (1, 2, 3, 4, 5) and tct.calendar_tag_id = ttb.calendar_pkg$get_weekday_tag_id())
   on conflict (route_variant_export_id, tt_variant_id, calendar_tag_id, calendar_id)
   do nothing;
   
   update easu.export_hist set status = 'S' where export_id = p_export_id;
   get diagnostics v_result = row_count;
   
   return v_result;
end;
$$ language plpgsql;


create or replace function easu.easu_export_pkg$is_tt_variant_changed
  (p_tt_variant_id      in integer)
returns boolean
-- ========================================================================
-- Проверка на изменения вариант расписания
-- ========================================================================
as $$ declare
   v_result                  boolean;
   v_route_variant_id_last   integer;
   v_action_period_last      tsrange;
   v_route_variant_id_curr   integer;
   v_action_period_curr      tsrange;
begin
   
   select route_variant_id, action_period
     into v_route_variant_id_curr, v_action_period_curr
     from ttb.tt_variant
    where tt_variant_id = p_tt_variant_id;
   
   if found then
      
      select ret.route_variant_id, ret.action_period
        into v_route_variant_id_last, v_action_period_last
        from easu.route_variant_export_last rve
        join easu.route_variant_export_tt ret on ret.route_variant_export_id = rve.route_variant_export_id
                                             and ret.route_variant_id = rve.route_variant_id
       where tt_variant_id = p_tt_variant_id;
      
      --raise notice 'v_route_variant_id_curr = %', v_route_variant_id_curr;
      --raise notice'v_route_variant_id_last = %', v_route_variant_id_last;
      --raise notice 'v_action_period_curr = %', v_action_period_curr;
      --raise notice 'v_action_period_last = %', v_action_period_last;
      
      if found and
         v_action_period_curr = v_action_period_last and
         v_route_variant_id_curr = v_route_variant_id_last then
         
         with t1 as (
            select calendar_tag_id, is_include
              from ttb.tt_calendar
             where tt_variant_id = p_tt_variant_id
         ),
         t3 as (
            select rvc.calendar_tag_id, rvc.is_include
              from easu.route_variant_export_last rve
              join easu.route_variant_export_calendar rvc on rvc.route_variant_export_id = rve.route_variant_export_id
             where rvc.tt_variant_id = p_tt_variant_id
         )
         select false into v_result
          where not exists(select 1 from t3
                            where not exists(select 1 from t1 
                                              where t1.calendar_tag_id = t3.calendar_tag_id
                                                and t1.is_include = t3.is_include))
            and not exists(select 1 from t1
                            where not exists(select 1 from t3 
                                              where t3.calendar_tag_id = t1.calendar_tag_id
                                                and t3.is_include = t1.is_include));
         if found then
            with t1 as (
               select ttc.calendar_tag_id, tci.calendar_id
                 from ttb.tt_calendar ttc
                 join ttb.calendar_tag tct on tct.calendar_tag_id = ttc.calendar_tag_id
                 join ttb.calendar_item tci on tci.calendar_tag_id = ttc.calendar_tag_id
                 join ttb.calendar cld on cld.calendar_id = tci.calendar_id
                where ttc.tt_variant_id = p_tt_variant_id
                  and extract(year from cld.dt) = extract(year from now())
                  and tct.calendar_tag_group_id not in (ttb.calendar_pkg$get_daysofweek_tag_group_id(),
                                                        ttb.calendar_pkg$get_season_tag_group_id())
                  and not (extract(dow from cld.dt) in (0, 6) and tct.calendar_tag_id = ttb.calendar_pkg$get_holiday_tag_id())
                  and not (extract(dow from cld.dt) in (1, 2, 3, 4, 5) and tct.calendar_tag_id = ttb.calendar_pkg$get_weekday_tag_id())
            ),   
            t3 as (
               select rvi.calendar_tag_id, rvi.calendar_id
                 from easu.route_variant_export_last rve
                 join easu.route_variant_export_calendar rvc on rvc.route_variant_export_id = rve.route_variant_export_id
                 join easu.route_variant_export_calendar_item rvi on rvi.route_variant_export_id = rvc.route_variant_export_id
                                                                 and rvi.tt_variant_id = rvc.tt_variant_id
                                                                 and rvi.calendar_tag_id = rvc.calendar_tag_id
                where rvc.tt_variant_id = p_tt_variant_id
            )
            select false into v_result
             where not exists(select 1 from t3
                               where not exists(select 1 from t1 
                                                 where t1.calendar_tag_id = t3.calendar_tag_id
                                                   and t1.calendar_id = t3.calendar_id))
               and not exists(select 1 from t1
                               where not exists(select 1 from t3
                                                 where t3.calendar_tag_id = t1.calendar_tag_id
                                                   and t3.calendar_id = t1.calendar_id));
         end if;
      end if;
   end if;
   return coalesce(v_result, true);
end;
$$ language plpgsql;


create or replace function easu.easu_export_pkg$is_route_variant_changed
  (p_route_variant_id   in integer)
returns boolean
-- ========================================================================
-- Проверка на изменения варианта маршрута
-- ========================================================================
as $$ declare
   v_result   boolean;
begin
   
   select bool_or(easu.easu_export_pkg$is_tt_variant_changed(wttv.tt_variant_id))
     into v_result
     from ttb.tt_variant wttv
    where wttv.parent_tt_variant_id is null
      and not wttv.sign_deleted
      and wttv.tt_status_id in (ttb.tt_status_pkg$active(), ttb.tt_status_pkg$agreed())
      and wttv.route_variant_id = p_route_variant_id
      and wttv.action_period @> localtimestamp;
   
   return coalesce(v_result, true);
end;
$$ language plpgsql;


create or replace function easu.easu_export_pkg$finish_export_error
  (p_export_id       in bigint)
returns int
-- ========================================================================
-- Фиксирование момента окончания выгрузки данных с ошибкой
-- ========================================================================
as $$ declare
   v_result    int := 0;
begin
   update easu.export_hist set status = 'E' where export_id = p_export_id;
   get diagnostics v_result = row_count;
   return v_result;
end;
$$ language plpgsql;


create or replace function easu.easu_export_pkg$get_export_begin_time
  (p_export_id       in bigint)
returns timestamp
-- ========================================================================
-- Время начала периода зкспорта экспорта
-- ========================================================================
as $$ declare
   v_result    timestamp;
begin
   select max(export_time) into v_result
     from easu.export_hist
    where status = 'S'
      and export_time < (select export_time from easu.export_hist where export_id = p_export_id);
   return coalesce(v_result, '1900-01-01'::timestamp);
end;
$$ language plpgsql;


create or replace function easu.easu_export_pkg$get_export_time
  (p_export_id       in bigint)
returns timestamp
-- ========================================================================
-- Время экспорта
-- ========================================================================
as $$ declare
   v_result    timestamp;
begin
   select export_time into v_result from easu.export_hist where export_id = p_export_id;
   return v_result;
end;
$$ language plpgsql;


create or replace function easu.easu_export_pkg$register_route_variant_export
  (p_export_id          in bigint,
   p_route_variant_id   in integer)
returns bigint
-- ========================================================================
-- Регистрация выгрузки варианта маршрута
-- ========================================================================
as $$ declare
   v_result             bigint := 0;
begin
   
   select route_variant_export_id into v_result
     from easu.route_variant_export
    where route_variant_id = p_route_variant_id
      and export_id = p_export_id;
   
   if not found then

      insert into easu.route_variant_export(route_variant_id, export_id)
      values (p_route_variant_id, p_export_id)
      returning route_variant_export_id into v_result;

   end if;
   
   return v_result;
end;
$$ language plpgsql;

create or replace function easu.easu_export_pkg$export_ras_change
  (p_export_id       in bigint)
returns table(change_id int8, change_type text, change_date timestamp, 
              route_num text, route_id int4, route_variant_id int4)
-- ========================================================================
-- Экспорт данных об изменениях в расписании
-- ========================================================================
as $$ declare
   v_export_time         timestamp;
   v_export_begin_time   timestamp;
begin
   v_export_time := easu.easu_export_pkg$get_export_time(p_export_id);
   v_export_begin_time := easu.easu_export_pkg$get_export_begin_time(p_export_id);
   
   return query
   with route_variant_closed as (
      select rvl.route_variant_id, max(rvl.change_date) as change_date
        from (select q.route_variant_id, upper(q.action_period) change_date
                from ttb.tt_variant q
               where q.parent_tt_variant_id is null
                 and not q.sign_deleted
                 and q.tt_status_id in (ttb.tt_status_pkg$active(), ttb.tt_status_pkg$agreed(), ttb.tt_status_pkg$arch())
                 and upper(q.action_period) < v_export_time
                 and lower(q.sys_period) > v_export_begin_time
              union all
              select q.route_variant_id, upper(q.action_period) change_date
                from rts.route_variant q
               where not q.sign_deleted
                 and q.rv_status_id in (rts.rv_status_pkg$active(), rts.rv_status_pkg$inactive())
                 and upper(q.action_period) < v_export_time
                 and lower(q.sys_period) > v_export_begin_time
             ) rvl
       group by rvl.route_variant_id
   )
   select p_export_id    as change_id,
          'closed_route' as change_type,
          rvc.change_date,
          rte.route_num,
          rte.route_id,
          rtv.route_variant_id
     from route_variant_closed rvc
     join rts.route_variant rtv on rtv.route_variant_id = rvc.route_variant_id
     join rts.route rte on rte.route_id = rtv.route_id
   union all
   select p_export_id  as change_id,
          'full'::text as change_type,
          s.change_date,
          rte.route_num,
          rte.route_id,
          s.route_variant_id
     from (select q.route_variant_id, max(q.change_date) as change_date
             from (select rtv.route_variant_id,
                          max(greatest(lower(rtv.sys_period),
                                       lower(rnd.sys_period),
                                       lower(sir.sys_period))
                             )::timestamp as change_date
                     from rts.route_variant rtv
                     join rts.round rnd on rnd.route_variant_id = rtv.route_variant_id
                     join rts.stop_item2round sir on sir.round_id = rnd.round_id
                    where not rtv.sign_deleted
                      and not rnd.sign_deleted   
                      and not sir.sign_deleted
                      and (v_export_begin_time < lower(rtv.sys_period) or
                           v_export_begin_time < lower(rnd.sys_period) or
                           v_export_begin_time < lower(sir.sys_period))
                      and rtv.route_variant_id not in (select s.route_variant_id from route_variant_closed s)
                    group by rtv.route_variant_id
                   union 
                   select ttv.route_variant_id,
                          max(greatest(lower(ttv.sys_period),
                                       lower(tto.sys_period),
                                       lower(tta.sys_period),
                                       lower(tti.sys_period))
                             )::timestamp as change_date
                     from ttb.tt_variant ttv
                     join ttb.tt_out tto on tto.tt_variant_id = ttv.tt_variant_id
                     join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
                     join ttb.tt_action_item tti on tti.tt_action_id = tta.tt_action_id
                    where not ttv.sign_deleted
                      and not tto.sign_deleted   
                      and not tti.sign_deleted
                      and (v_export_begin_time < lower(ttv.sys_period) or
                           v_export_begin_time < lower(tto.sys_period) or
                           v_export_begin_time < lower(tta.sys_period) or
                           v_export_begin_time < lower(tti.sys_period))
                      and ttv.route_variant_id not in (select s.route_variant_id from route_variant_closed s)
                    group by ttv.route_variant_id
                   union 
                   select ttv.route_variant_id,
                          max(greatest(lower(ttc.sys_period),
                                       lower(tci.sys_period))
                             )::timestamp as change_date
                     from ttb.tt_variant ttv
                     join ttb.tt_calendar ttc on ttc.tt_variant_id = ttv.tt_variant_id
                     join ttb.calendar_item tci on tci.calendar_tag_id = ttc.calendar_tag_id
                    where (v_export_begin_time < lower(ttc.sys_period) or
                           v_export_begin_time < lower(tci.sys_period))
                      and ttv.route_variant_id not in (select s.route_variant_id from route_variant_closed s)
                    group by ttv.route_variant_id
                  ) q
            group by q.route_variant_id
          ) s
     join rts.route rte on rte.current_route_variant_id = s.route_variant_id
    where easu.easu_export_pkg$is_route_variant_changed(s.route_variant_id)
    order by 4, 2;
end;
$$ language plpgsql;

-- ========================================================================
-- Справочники ЕАСУ

create or replace function easu.easu_export_pkg$action_type_to_time_interval
  (p_action_type_id   in smallint)
returns smallint
-- ========================================================================
-- Справочник назначений временных интервалов
-- ========================================================================
as $$ declare
   v_result   smallint := 0;
begin
   if p_action_type_id = ttb.action_type_pkg$pause() or 
      ttb.action_type_pkg$is_parent_type(p_action_type_id, ttb.action_type_pkg$pause()) then
      v_result := 4; -- Отстой
   elsif p_action_type_id = ttb.action_type_pkg$repair() or 
         ttb.action_type_pkg$is_parent_type(p_action_type_id, ttb.action_type_pkg$repair()) then
      v_result := 5; -- Ремонт
   elsif p_action_type_id = ttb.action_type_pkg$bn_round_break() or
         ttb.action_type_pkg$is_parent_type(p_action_type_id, ttb.action_type_pkg$bn_round_break()) or
         p_action_type_id = ttb.action_type_pkg$change_driver() then
      v_result := 6; -- Межрейсовая стоянка
   elsif p_action_type_id = ttb.action_type_pkg$dinner() or
         ttb.action_type_pkg$is_parent_type(p_action_type_id, ttb.action_type_pkg$dinner()) then
      v_result := 7; -- Обед
   elsif p_action_type_id = ttb.action_type_pkg$production_round() or 
         ttb.action_type_pkg$is_parent_type(p_action_type_id, ttb.action_type_pkg$production_round()) or
         p_action_type_id = ttb.action_type_pkg$technical_round()  or 
         ttb.action_type_pkg$is_parent_type(p_action_type_id, ttb.action_type_pkg$technical_round())  then
      v_result := 1; -- Технологический рейс, Производственный рейс
   end if;
   return v_result;
end;
$$ language plpgsql immutable;


create or replace function easu.easu_export_pkg$action_type_to_line_type
  (p_action_type_id   in smallint)
returns smallint
-- ========================================================================
-- Справочник типов линий
-- ========================================================================
as $$ declare
   v_result   smallint := 0;
begin
   if p_action_type_id = ttb.action_type_pkg$short_round() or ttb.action_type_pkg$is_parent_type(p_action_type_id, ttb.action_type_pkg$short_round())  then
      v_result := 3; -- Укороченная
   elsif p_action_type_id = ttb.action_type_pkg$long_round() or ttb.action_type_pkg$is_parent_type(p_action_type_id, ttb.action_type_pkg$long_round()) then
      v_result := 2; -- Удлиненная
   else
      v_result := 1; -- Основная
   end if;
   return v_result;
end;
$$ language plpgsql immutable;


create or replace function easu.easu_export_pkg$action_type_to_line_kind
  (p_action_type_id   in smallint)
returns smallint
-- ========================================================================
-- Справочник видов линий
-- ========================================================================
as $$ declare
   v_result   smallint := 0;
begin
   if p_action_type_id = ttb.action_type_pkg$depo() or ttb.action_type_pkg$is_parent_type(p_action_type_id, ttb.action_type_pkg$depo()) or 
      p_action_type_id = ttb.action_type_pkg$technical_round() or ttb.action_type_pkg$is_parent_type(p_action_type_id, ttb.action_type_pkg$technical_round()) then
      v_result := 1; -- Нулевой
   elsif p_action_type_id = ttb.action_type_pkg$maneuver() or ttb.action_type_pkg$is_parent_type(p_action_type_id, ttb.action_type_pkg$maneuver())  then
      v_result := 2; -- Маневровый
   elsif p_action_type_id = ttb.action_type_pkg$production_round() or ttb.action_type_pkg$is_parent_type(p_action_type_id, ttb.action_type_pkg$production_round())  then
      v_result := 5; -- Производственный рейс
-- elsif p_action_type_id = ttb.action_type_pkg$technical_round() or ttb.action_type_pkg$is_parent_type(p_action_type_id, ttb.action_type_pkg$technical_round()) then
--    v_result := 3; -- Технологический рейс
   end if;
   return v_result;
end;
$$ language plpgsql immutable;


create or replace function easu.easu_export_pkg$action_type_to_tripe_type
  (p_action_type_id   in smallint)
returns smallint
-- ========================================================================
-- Справочник типов рейсов (выгрузка РНИС)
-- ========================================================================
as $$ declare
   v_result   smallint := null;
begin
   if p_action_type_id = ttb.action_type_pkg$short_round() or ttb.action_type_pkg$is_parent_type(p_action_type_id, ttb.action_type_pkg$short_round())  then
      v_result := 1; -- Укороченный вариант маршрута
   elseif p_action_type_id = ttb.action_type_pkg$depo() or ttb.action_type_pkg$is_parent_type(p_action_type_id, ttb.action_type_pkg$depo()) or
      p_action_type_id = ttb.action_type_pkg$technical_round() or ttb.action_type_pkg$is_parent_type(p_action_type_id, ttb.action_type_pkg$technical_round()) then
      v_result := 4; -- Нулевой
   -- 2 – Дополнительный вариант маршрута
   -- 3 – Кольцевой вариант маршрута
   end if;
   return v_result;
end;
$$ language plpgsql immutable;


create or replace function easu.easu_export_pkg$action_type_to_move_mode
  (p_action_type_id   in smallint)
returns smallint
-- ========================================================================
-- Справочник режимов движения
-- ========================================================================
as $$ declare
   v_result   smallint := 0;
begin
   if p_action_type_id = ttb.action_type_pkg$express_round() or ttb.action_type_pkg$is_parent_type(p_action_type_id, ttb.action_type_pkg$express_round()) then
      v_result := 2; -- Экспрессный
   else
      v_result := 1; -- Обычный
   end if;
   return v_result;
end;
$$ language plpgsql immutable;


create or replace function easu.easu_export_pkg$get_eps_type
  (p_tr_type_id       in smallint,
   p_tr_capacity_id   in smallint)
returns text
-- ========================================================================
-- Тип ЕПС
-- ========================================================================
as $$ declare
   v_result   text := '';
begin
   if p_tr_capacity_id = core.tr_capacity_pkg$huge() then
      v_result := 'ОБВ';
   elsif p_tr_capacity_id = core.tr_capacity_pkg$big() then
      v_result := 'БВ';
   elsif p_tr_capacity_id = core.tr_capacity_pkg$middle() then
      v_result := 'СВ';
   elsif p_tr_capacity_id = core.tr_capacity_pkg$small() then
      v_result := 'МВ';
   end if;
   
   if p_tr_type_id = core.tr_type_pkg$tb() then
      v_result := 'ТРОЛ' || v_result;
   elsif p_tr_type_id = core.tr_type_pkg$tm() then
      v_result := 'ТРАМ' || v_result;      
   end if;
   return v_result;
end;
$$ language plpgsql immutable;


create or replace function easu.easu_export_pkg$get_stop_type
  (p_stop_item2round_id   in bigint,
   p_is_last_stop         in boolean)
returns smallint
-- ========================================================================
-- Тип конечного пункта
-- ========================================================================
as $$ declare
   v_result   smallint;
begin
   if p_is_last_stop then
      select 5::smallint into v_result
        from rts.stop_item2round_type sir
        join rts.stop_item_type sit on sit.stop_item_type_id = sir.stop_item_type_id
       where not sir.sign_deleted
         and not sit.sign_deleted
         and sit.stop_type_id = rts.stop_type_pkg$park()
         and sir.stop_item2round_id = p_stop_item2round_id
         limit 1;
   end if;
   return coalesce(v_result, 1::smallint);
end;
$$ language plpgsql immutable;

-- ========================================================================

create or replace function easu.easu_export_pkg$transport_work_list (p_date    in timestamp)
returns table(tt_variant_id int, order_date date, route_num text,
              tr_type_id smallint, file_name_prefix text)
-- ========================================================================
-- Список маршрутов для экспорта транспортной работы за определенную дату
-- ========================================================================
as $$
declare
   l_action_type_list smallint[];
begin
 l_action_type_list := ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$round(), false);
   
 return query
  with tt_round as (
      select distinct ttv.tt_variant_id,
                      tto.tt_out_id,
                      ttv.order_date,
                      r.route_num,
                      r.tr_type_id,
                      'F-'||to_char(ttv.order_date, 'YYYYMMDD')||'-'||r.route_num as file_name_prefix,
                      tta.tt_action_id,
                      ttar.round_status_id as round_status_id
      from ttb.tt_variant ttv
      join rts.route_variant rv on rv.route_variant_id = ttv.route_variant_id
      join rts.route r on r.route_id = rv.route_id
      join ttb.tt_out tto on tto.tt_variant_id = ttv.tt_variant_id
      join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
      join ttb.action_type ttat on ttat.action_type_id = tta.action_type_id
      join ttb.tt_action_round ttar on ttar.tt_action_id = tta.tt_action_id
      where ttv.order_date = p_date::date
        and not ttv.sign_deleted
        and ttv.parent_tt_variant_id is not null
        and not tto.sign_deleted
        and not tta.sign_deleted
        and tta.action_type_id = any (ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$round(), false)))
   select distinct
          lr.tt_variant_id,
          lr.order_date::date,
          lr.route_num::text,
          lr.tr_type_id::smallint,
          lr.file_name_prefix::text
     from tt_round lr
     where not exists (select 1
                       from tt_round r1
                       where r1.tt_variant_id = lr.tt_variant_id and
                             r1.round_status_id in (ttb.round_status_pkg$in_progress())) and
           exists (select 1
                   from tt_round r2
                   where r2.tt_variant_id = lr.tt_variant_id and
                         r2.round_status_id in (ttb.round_status_pkg$passed() /*, ttb.round_status_pkg$passed_partial(), ttb.round_status_pkg$error() */ ));
end;
$$ language plpgsql;

-- drop function easu.easu_export_pkg$export_transport_work(bigint);
create or replace function easu.easu_export_pkg$export_transport_work
  (v_tt_variant_id   in bigint)
returns table(park text, group_date timestamp, route_type text, customer_type int2, 
              route_number text, out_number text, garage_number int4, shift_number int2, 
              table_number int8, time_code int2, date_begin text, time_begin text, 
              date_end text, time_end text, stay_type text, mass int8, length numeric,
              equipment_mask text, equipment_time int8, engine_time int8, froute_type text, 
              fcustomer_type int2, froute_number text, fout_number text, acc_begin int8, 
              acc_end int8, interval_count int4, fpark_number text)
-- ========================================================================
-- Экспорт данных о выполненной транспортной работе
-- ========================================================================
as $$
declare
 l_prodround smallint[];
 l_techround smallint[];
 l_dinner smallint[];
 l_round_break smallint[];
begin
 l_prodround := ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$production_round(), false);
 l_techround := ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$technical_round(), false);
 l_dinner    := ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$dinner(), false);
 l_round_break := ttb.action_type_pkg$get_action_type_list(ttb.action_type_pkg$bn_round_break(), false);

   return query
   with tt_var as (select tv.*,
                          r.route_num
                   from ttb.tt_variant tv
                        join rts.route_variant rv on rv.route_variant_id = tv.route_variant_id
                        join rts.route r on r.route_id = rv.route_id
                    where tt_variant_id = v_tt_variant_id
                          and parent_tt_variant_id is not null)
   select edt.park::text as park,                            -- поле 1
          edt.group_date::timestamp as group_date,           -- поле 2
          edt.route_type::text as route_type,                -- поле 3 -- по письму Войнова
          edt.customer_type::int2 as customer_type,          -- поле 4
          edt.route_number as route_number,                  -- поле 5
          edt.out_number::text as out_number,                -- поле 6
          edt.garage_number::int4 as garage_number,          -- поле 7
          edt.shift_number::int2 as shift_number,            -- поле 8
          edt.table_number::int8 as table_number,            -- поле 9
          edt.time_code::smallint as time_code,
          to_char(edt.date_begin, 'yyyymmdd') as date_begin, -- поле 11
          to_char(edt.date_begin, 'hh24miss') as time_begin, -- поле 12
          to_char(edt.date_end, 'yyyymmdd') as date_end,     -- поле 13
          to_char(edt.date_end, 'hh24miss') as time_end,     -- поле 14
          edt.stay_type as stay_type,                        -- поле 15 ??? что есть простой по таблицам ttb ???
          edt.mass::int8 as mass,                            -- поле 16
          edt.route_length as "length",                      -- поле 17
          edt.equipment_mask as equipment_mask,              -- поле 18
          edt.equipment_time::int8 as equipment_time,        -- поле 19
          edt.engine_time::int8 as engine_time,              -- поле 20
          edt.froute_type::text as froute_type,              -- поле 21
          edt.fcustomer_type::int2 as fcustomer_type,        -- поле 22
          edt.froute_number::text as froute_number,          -- поле 23
          edt.fout_number::text as fout_number,              -- поле 24
          edt.acc_begin::int8 as acc_begin,                  -- поле 25
          edt.acc_end::int8 as acc_end,                      -- поле 26
          edt.interval_count::int4 as interval_count,        -- поле 27 -- по письму Войнова
          edt.fpark_number::text as fpark_number             -- поле 28
   from (select distinct
                d2c.easu_depo_code as park,          -- поле 1
                ttv.order_date as group_date,        -- поле 2
                null::text as route_type,            -- поле 3 -- по письму Войнова
                5 as customer_type,                  -- поле 4
                ttv.route_num as route_number,       -- поле 5
                tto.tt_out_num as out_number,        -- поле 6
                tr.garage_num as garage_number,      -- поле 7
                drsh.dr_shift_num as shift_number,   -- поле 8
                dr.tab_num as table_number,          -- поле 9
                (case /* преобразуем action_type_id в "1.3	Справочник назначений временных интервалов" */
                  when tta.action_type_id = any (l_prodround) then 3
                  when tta.action_type_id = any (l_techround) then 1 -- по письму Войнова
                  when tta.action_type_id = any (l_dinner) then 10
                  when tta.action_type_id = any (l_round_break) then 8
                  else null
                 end)::smallint as time_code,
                (first_value(coalesce(ttaf.time_fact_begin, af_pred.time_fact_end)) over (partition by tta.tt_action_id order by ttaf.time_fact_begin ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)) + interval '3 hours' as date_begin, -- поле 11, 12
                (last_value (coalesce(ttaf.time_fact_end, af_next.time_fact_begin)) over (partition by tta.tt_action_id order by ttaf.time_fact_begin ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)) + interval '3 hours' as date_end,   -- поле 13, 14
                null as stay_type,                   -- поле 15 ??? что есть простой по таблицам ttb ???
                null::int8 as mass,                  -- поле 16
                (case /* длина траектории - выгружаем для рейсов, для обедов и стоянок - нет */
                  when tta.action_type_id = any (l_round_break) or tta.action_type_id = any (l_dinner) then 0
                  else round((sum(si2r.length_sector::numeric) over (partition by tta.tt_action_id ))/1000, 2)
                 end)::numeric as route_length,      -- поле 17
                ' ' as equipment_mask,               -- поле 18
                null::int8 as equipment_time,        -- поле 19
                null::int8 as engine_time,           -- поле 20
                1 as froute_type,                    -- поле 21
                5 as fcustomer_type,                 -- поле 22
                ttv.route_num as froute_number,      -- поле 23
                tto.tt_out_num as fout_number,       -- поле 24
                null as acc_begin,                   -- поле 25
                null as acc_end,                     -- поле 26
                (case /* выгружаем количество рейсов */
                  when tta.action_type_id = any (l_round_break) or tta.action_type_id = any (l_dinner) then 0
                  else 1
                 end)::int4 as interval_count,       -- поле 27 -- по письму Войнова
                d2c.easu_depo_code as fpark_number   -- поле 28
           from tt_var ttv
           join ttb.tt_out tto on tto.tt_variant_id = ttv.tt_variant_id
           join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
      left join ttb.tt_action a_pred on a_pred.tt_out_id = tta.tt_out_id and
                                        upper (a_pred.action_range) = lower(tta.action_range)
      left join ttb.tt_action_item ai_pred on ai_pred.tt_action_id = a_pred.tt_action_id and
                                              ai_pred.time_end = upper(a_pred.action_range)
     left join ttb.tt_action_fact af_pred on af_pred.tt_action_id = a_pred.tt_action_id and
                                             af_pred.stop_item2round_id = ai_pred.stop_item2round_id
     left join ttb.tt_action a_next on a_next.tt_out_id = tta.tt_out_id
                                       and lower (a_next.action_range) = upper(tta.action_range)
     left join ttb.tt_action_item ai_next on ai_next.tt_action_id = a_next.tt_action_id and
                                        ai_next.time_end = lower(a_next.action_range)
     left join ttb.tt_action_fact af_next on af_next.tt_action_id = a_next.tt_action_id and
                                             af_next.stop_item2round_id = ai_next.stop_item2round_id
          join ttb.tt_action_item ttai on ttai.tt_action_id = tta.tt_action_id
          join rts.stop_item2round si2r on si2r.stop_item2round_id = ttai.stop_item2round_id
     left join ttb.tt_action_fact ttaf on ttaf.tt_action_id = tta.tt_action_id and
                                          ttaf.stop_item2round_id = ttai.stop_item2round_id
          join core.tr tr on tr.tr_id = tto.tr_id
          join easu.depo2core d2c on d2c.depo_id = tr.depo_id
          join core.driver dr on dr.driver_id = ttai.driver_id
          join ttb.dr_shift drsh on drsh.dr_shift_id = ttai.dr_shift_id
	 left join ttb.tt_action_round r on r.tt_action_id = tta.tt_action_id
         where not ttv.sign_deleted and
               not tto.sign_deleted and
               not tta.sign_deleted and
               not ttai.sign_deleted and
               (/* ограничиваем выгрузку пройденными рейсами (производственными и тенологическими) и обедом */
                (tta.action_type_id = any(l_dinner) or  tta.action_type_id = any(l_round_break)) or
                ((tta.action_type_id = any(l_prodround) or tta.action_type_id = any(l_techround)) and r.round_status_id = ttb.round_status_pkg$passed())
                )
        ) edt
   where edt.date_begin is not null and
         edt.date_end is not null and
         date_trunc('seconds', edt.date_begin) != date_trunc('seconds', edt.date_end)
   order by edt.out_number, edt.shift_number, edt.date_begin, edt.date_end;
end;
$$ language plpgsql;

-- ========================================================================

create or replace function easu.easu_export_pkg$get_route_variant_export_code
   (p_route_variant_export_id   in bigint)
returns text
-- ========================================================================
-- Сформировать route_variant_export_code для route_variant_export_id
-- ========================================================================
as $$
begin
   return 's' || trim(to_char(p_route_variant_export_id, '0000000000'));
end;
$$ language plpgsql;


create or replace function easu.easu_export_pkg$export_closed_route
  (p_export_id          in bigint,
   p_route_variant_id   in integer)
returns table(route_variant_code text, close_date text, close_time text)
-- ========================================================================
-- Экспорт данных о закрытом маршруте по идентификатору маршрута
-- ========================================================================
as $$
begin
   return query
   select easu.easu_export_pkg$get_route_variant_export_code(rve.route_variant_export_id) as route_variant_code,
          to_char(ttv.close_datetime::date, 'yyyymmdd') as close_date,
          to_char(ttv.close_datetime::time, 'hh24miss') as close_time
     from (select stv.route_variant_id, max(upper(stv.action_period)) close_datetime
             from ttb.tt_variant stv
            where stv.parent_tt_variant_id is null
              and not stv.sign_deleted
              and stv.tt_status_id in (ttb.tt_status_pkg$active(), ttb.tt_status_pkg$agreed(), ttb.tt_status_pkg$arch())
              and upper(stv.action_period) < easu.easu_export_pkg$get_export_time(p_export_id)
              and stv.route_variant_id = p_route_variant_id
            group by stv.route_variant_id
          ) ttv
     left join easu.route_variant_export_last rve on rve.route_variant_id = ttv.route_variant_id;
end;
$$ language plpgsql;


create or replace function easu.easu_export_pkg$export_route_holiday
  (p_export_id          in bigint,
   p_route_variant_id   in integer)
returns table(route_variant_code text, holiday_date text, action text)
-- ========================================================================
-- Экспорт данных о праздничных днях для маршрутов
-- ========================================================================
as $$ declare
   v_route_variant_export   bigint;
begin
   v_route_variant_export := easu.easu_export_pkg$register_route_variant_export(p_export_id, p_route_variant_id);
   
   return query
   with tt_route_variant as (
      select wttv.tt_variant_id, wttv.route_variant_id
        from ttb.tt_variant wttv
       where wttv.parent_tt_variant_id is null
         and not wttv.sign_deleted
         and wttv.tt_status_id in (ttb.tt_status_pkg$active(), ttb.tt_status_pkg$agreed())
         and wttv.route_variant_id = p_route_variant_id
         and wttv.action_period @> localtimestamp
   ),
   route_dt as (
      select ttv.tt_variant_id, ttv.route_variant_id, cld.dt, 'A'::text as action
        from tt_route_variant ttv
        join ttb.tt_calendar ttc on ttc.tt_variant_id = ttv.tt_variant_id
        join ttb.calendar_tag clt on clt.calendar_tag_id = ttc.calendar_tag_id
        join ttb.calendar_item cli on cli.calendar_tag_id = clt.calendar_tag_id
        join ttb.calendar cld on cld.calendar_id = cli.calendar_id
       where (clt.calendar_tag_group_id = ttb.calendar_pkg$get_holiday_tag_group_id() or 
              clt.calendar_tag_id = ttb.calendar_pkg$get_holiday_tag_id())
         and extract(dow from cld.dt) in (1, 2, 3, 4, 5)
         and extract(year from cld.dt) = extract(year from now())
      union all
      select ttv.tt_variant_id, ttv.route_variant_id, cld.dt, 'R'::text as action
        from tt_route_variant ttv
        join ttb.tt_calendar ttc on ttc.tt_variant_id = ttv.tt_variant_id
        join ttb.calendar_tag clt on clt.calendar_tag_id = ttc.calendar_tag_id
        join ttb.calendar_item cli on cli.calendar_tag_id = clt.calendar_tag_id
        join ttb.calendar cld on cld.calendar_id = cli.calendar_id
       where clt.calendar_tag_id = ttb.calendar_pkg$get_weekday_tag_id()
         and extract(dow from cld.dt) in (0, 6)
         and extract(year from cld.dt) = extract(year from now())
   )
   select easu.easu_export_pkg$get_route_variant_export_code(rve.route_variant_export_id) as route_variant_code,
          to_char(q.dt::date, 'yyyymmdd') as holiday_date,
          q.action
     from route_dt q
     left join easu.route_variant_export rve on rve.route_variant_id = q.route_variant_id
                                            and rve.export_id = p_export_id
    order by q.tt_variant_id, q.route_variant_id, q.dt;
end;
$$ language plpgsql;

create or replace function easu.easu_export_pkg$export_new_route
  (p_export_id          in bigint,
   p_route_variant_id   in integer)
returns table(route_variant_code text, status int2, date_begin text,
              time_begin text, number text, date_end text, time_end text,
              type_code text, average_speed text, season_id int, 
              season_code text, day_of_week_mask text, view_code text)
-- ========================================================================
-- Экспорт данных об изменениях в маршрутах
-- ========================================================================
as $$ declare
   v_route_variant_export   bigint;
begin
   v_route_variant_export := easu.easu_export_pkg$register_route_variant_export(p_export_id, p_route_variant_id);
   
   return query
   with tt_route_variant as (
      select wttv.tt_variant_id, wttv.route_variant_id, wttv.action_period
        from ttb.tt_variant wttv
       where wttv.parent_tt_variant_id is null
         and not wttv.sign_deleted
         and wttv.tt_status_id in (ttb.tt_status_pkg$active(), ttb.tt_status_pkg$agreed())
         and wttv.action_period @> localtimestamp
         and wttv.route_variant_id = p_route_variant_id
   ),
   tt_route_variant_len as (
      select rtv.route_variant_id, rtv.tt_variant_id, sum(length_sector) as route_len
        from rts.stop_item2round sir
        join ttb.tt_action_round ttr on ttr.round_id = sir.round_id
        join ttb.tt_action tta on tta.tt_action_id = ttr.tt_action_id
        join ttb.tt_out tto on tto.tt_out_id = tta.tt_out_id
        join tt_route_variant rtv on rtv.tt_variant_id = tto.tt_variant_id
       where not sir.sign_deleted
         and not tta.sign_deleted
         and not tto.sign_deleted
       group by rtv.route_variant_id, rtv.tt_variant_id
   ),
   tt_route_variant_time as (
      select q.route_variant_id, q.tt_variant_id,
             sum(extract(epoch from q.route_end - q.route_begin)) as route_sec
        from (select rtv.route_variant_id, rtv.tt_variant_id, tti.tt_action_id,
                     min(tti.time_begin) as route_begin, max(tti.time_end) as route_end
                from ttb.tt_action_item tti
                join ttb.tt_action tta on tta.tt_action_id = tti.tt_action_id
                join ttb.tt_out tto on tto.tt_out_id = tta.tt_out_id
                join tt_route_variant rtv on rtv.tt_variant_id = tto.tt_variant_id
               where not tti.sign_deleted
                 and not tta.sign_deleted
                 and not tto.sign_deleted
               group by rtv.route_variant_id, rtv.tt_variant_id, tti.tt_action_id
             ) q
       group by q.route_variant_id, q.tt_variant_id
   )
   select easu.easu_export_pkg$get_route_variant_export_code(rve.route_variant_export_id) as route_variant_code,
          1::smallint as status,
          to_char(lower(ttv.action_period)::date, 'yyyymmdd') as date_begin,
          '030000'::text as time_begin,
          rte.route_num::text as number,
          coalesce(to_char(upper(ttv.action_period)::date, 'yyyymmdd'), '99991231') as date_end,
          coalesce(to_char(upper(ttv.action_period)::time, 'hh24miss'),   '235959') as time_end,
          '01'::text as type_code,
          replace(to_char((rvl.route_len / route_sec * 3.6)::float8, 'FM999.00'), '.', ',') as average_speed,
          coalesce(ctg4tv.calendar_tag_id, 1) as season_id,
          coalesce(ctg4tv.tag_name, 'ЛЕТО')   as season_code,
          ttb.tt_export_service_pkg$convert_calendar_to_bit(ttv.tt_variant_id) as day_of_week_mask,
          rte.tr_type_id::text as view_code
     from rts.route_variant rtv
     join rts.route rte on rtv.route_id = rte.route_id
     join tt_route_variant ttv on ttv.route_variant_id = rtv.route_variant_id
     join tt_route_variant_len  rvl on rvl.route_variant_id = ttv.route_variant_id
                                   and rvl.tt_variant_id = ttv.tt_variant_id
     join tt_route_variant_time rvt on rvt.route_variant_id = ttv.route_variant_id
                                   and rvt.tt_variant_id = ttv.tt_variant_id
     left join easu.route_variant_export rve on rve.route_variant_id = rtv.route_variant_id
                                            and rve.export_id = p_export_id
     left join (select c4.tt_variant_id,
                       case when upper(ct4.tag_name) = 'ЗИМА' then 1 else 2 end as calendar_tag_id,
                       upper(ct4.tag_name) as tag_name
                  from ttb.tt_calendar c4
                  join ttb.calendar_tag ct4 on ct4.calendar_tag_id = c4.calendar_tag_id
                  join ttb.calendar_tag_group ctg4 on ct4.calendar_tag_group_id = ctg4.calendar_tag_group_id
                 where ctg4.calendar_tag_group_id = 4
               ) ctg4tv on ctg4tv.tt_variant_id = ttv.tt_variant_id
    where rtv.route_variant_id = p_route_variant_id;
   
end;
$$ language plpgsql;


create or replace function easu.easu_export_pkg$export_node
  (p_route_variant_id   in integer)
returns table(stop_id int4, stop_name text)
-- ========================================================================
-- Экспорт данных справочника остановочных пунктов линий маршрутов
-- ========================================================================
as $$
begin
   return query
   select distinct stp.stop_id, stp.name as stop_name
     from rts.stop stp
     join rts.stop_location stl on stl.stop_id = stp.stop_id
     join rts.stop_item sti on sti.stop_location_id = stl.stop_location_id
     join rts.stop_item2round sir on sir.stop_item_id = sti.stop_item_id
     join rts.round rnd on rnd.round_id = sir.round_id
     join rts.route_variant rtv on rtv.route_variant_id = rnd.route_variant_id
    where not rnd.sign_deleted
      and not sir.sign_deleted
      and rtv.route_variant_id = p_route_variant_id
    order by stp.stop_id;
end;
$$ language plpgsql;


create or replace function easu.easu_export_pkg$export_node_type()
returns table(stop_type_id int2, stop_type_name text)
-- ========================================================================
-- Экспорт данных справочника справочника видов (назначений) остановочных пунктов линий маршрутов
-- ========================================================================
as $$
begin
   return query
   select t.stop_type_id, t.stop_type_name
     from rts.stop_type t
    where not t.sign_deleted
    order by t.stop_type_id;
end;
$$ language plpgsql;


create or replace function easu.easu_export_pkg$export_path_node
  (p_export_id          in bigint,
   p_route_variant_id   in integer)
returns table(route_variant_code text, round_id int4, stop_id int4, tr_type_id int2)
-- ========================================================================
-- Экспорт данных об остановочных пунктах линий
-- ========================================================================
as $$ declare
   v_route_variant_export   bigint;
begin
   v_route_variant_export := easu.easu_export_pkg$register_route_variant_export(p_export_id, p_route_variant_id);
   
   return query
   with tt_route_variant as (
      select wttv.tt_variant_id, wttv.route_variant_id
        from ttb.tt_variant wttv
       where wttv.parent_tt_variant_id is null
         and not wttv.sign_deleted
         and wttv.tt_status_id in (ttb.tt_status_pkg$active(), ttb.tt_status_pkg$agreed())
         and wttv.action_period @> localtimestamp
         and wttv.route_variant_id = p_route_variant_id
   ),
   tt_route_round as (
      select distinct ttv.route_variant_id, ttr.round_id
        from ttb.tt_out tto
        join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
        join ttb.tt_action_round ttr on ttr.tt_action_id = tta.tt_action_id
        join tt_route_variant ttv on ttv.tt_variant_id = tto.tt_variant_id
       where not tto.sign_deleted
         and not tta.sign_deleted
   ),
   items_list as (
      select q.route_variant_id, q.round_id, q.stop_item_id, q.order_num,
             easu.easu_export_pkg$get_stop_type(889741, q.stop_item_id = q.last_stop_item_id) as tr_type_id
        from (select rnd.route_variant_id, sir.round_id, sir.order_num, sir.stop_item_id, sir.stop_item2round_id,
                     first_value(sir.stop_item_id) over (partition by sir.round_id order by sir.order_num) as first_stop_item_id,
                     last_value(sir.stop_item_id)  over (partition by sir.round_id order by sir.order_num rows between unbounded preceding and unbounded following) as last_stop_item_id
                from rts.stop_item2round sir
                join rts.round rnd on rnd.round_id = sir.round_id
               where not sir.sign_deleted
                 and not rnd.sign_deleted
                 and rnd.route_variant_id = p_route_variant_id
             ) q
       where q.stop_item_id in (q.first_stop_item_id, q.last_stop_item_id)      
   )
   select easu.easu_export_pkg$get_route_variant_export_code(rve.route_variant_export_id) as route_variant_code,
          itl.round_id, stl.stop_id, itl.tr_type_id
     from tt_route_round rtv
     join items_list itl on itl.route_variant_id = rtv.route_variant_id
                        and itl.round_id = rtv.round_id
     join rts.stop_item sti on sti.stop_item_id = itl.stop_item_id
     join rts.stop_location stl on stl.stop_location_id = sti.stop_location_id
     left join easu.route_variant_export rve on rve.route_variant_id = rtv.route_variant_id
                                            and rve.export_id = p_export_id
    order by itl.round_id, itl.order_num;
end;
$$ language plpgsql;


create or replace function easu.easu_export_pkg$export_path
  (p_export_id          in bigint,
   p_route_variant_id   in integer)
returns table(route_variant_code text, round_id int4, line_type int2, line_view int2, 
              movement_code int2, endpointa int4, endpointb int4, length text)
-- ========================================================================
-- экспорт данных о линиях маршрутов
-- ========================================================================
as $$ declare
   v_route_variant_export   bigint;
begin
   v_route_variant_export := easu.easu_export_pkg$register_route_variant_export(p_export_id, p_route_variant_id);
   
   return query
   with tt_route_variant as (
      select wttv.tt_variant_id, wttv.route_variant_id
        from ttb.tt_variant wttv
       where wttv.parent_tt_variant_id is null
         and not wttv.sign_deleted
         and wttv.tt_status_id in (ttb.tt_status_pkg$active(), ttb.tt_status_pkg$agreed())
         and wttv.action_period @> localtimestamp
         and wttv.route_variant_id = p_route_variant_id
   ),
   tt_route_round as (
      select distinct tto.tt_variant_id, tta.action_type_id, ttr.round_id
        from ttb.tt_out tto
        join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
        join ttb.tt_action_round ttr on ttr.tt_action_id = tta.tt_action_id
       where not tto.sign_deleted
         and not tta.sign_deleted
   )
   select easu.easu_export_pkg$get_route_variant_export_code(rve.route_variant_export_id) as route_variant_code,
          trr.round_id,
          easu.easu_export_pkg$action_type_to_line_type(trr.action_type_id) as line_type, -- тип линии маршрута
          easu.easu_export_pkg$action_type_to_line_kind(trr.action_type_id) as line_view, -- вид линии маршрута
          easu.easu_export_pkg$action_type_to_move_mode(trr.action_type_id) as movement_code, -- режим движения
          stla.stop_id                                                      as endpointa,
          stlb.stop_id                                                      as endpointb,
          replace(to_char(si2r_dt.round_length::float8/1000, 'fm9999.99'), '.', ',') as "length"
     from tt_route_variant rtv
     join tt_route_round trr on trr.tt_variant_id = rtv.tt_variant_id
     left join easu.route_variant_export rve on rve.route_variant_id = rtv.route_variant_id
                                            and rve.export_id = p_export_id
     join (select distinct si2r.round_id,
                  sum(si2r.length_sector)        over (partition by si2r.round_id order by si2r.order_num rows between unbounded preceding and unbounded following) as round_length,
                  first_value(si2r.stop_item_id) over (partition by si2r.round_id order by si2r.order_num rows between unbounded preceding and unbounded following) as first_si,
                  last_value(si2r.stop_item_id)  over (partition by si2r.round_id order by si2r.order_num rows between unbounded preceding and unbounded following) as last_si
             from rts.stop_item2round si2r
          ) si2r_dt on si2r_dt.round_id = trr.round_id
     join rts.stop_item stia on stia.stop_item_id = si2r_dt.first_si
     join rts.stop_location stla on stla.stop_location_id = stia.stop_location_id
     join rts.stop_item stib on stib.stop_item_id = si2r_dt.last_si
     join rts.stop_location stlb on stlb.stop_location_id = stib.stop_location_id;
end;
$$ language plpgsql;


create or replace function easu.easu_export_pkg$export_season()
returns table(season_id int8, name text, date_begin date, date_end date)
-- ========================================================================
-- Экспорт справочника сезонов
-- ========================================================================
as $$
begin
   return query 
   select ct4.calendar_tag_id::int8 as season_id,
          ct4.tag_name::text        as name,
          null::date                as date_begin,
          null::date                as date_end
     from ttb.calendar_tag ct4
    where ct4.calendar_tag_group_id = 4;
end;
$$ language plpgsql;


create or replace function easu.easu_export_pkg$export_schedule
  (p_export_id          in bigint,
   p_route_variant_id   in integer)
returns table(route_variant_code text, status int4, date_begin text, time_begin text, 
              date_end text, time_end text, out_number text, park_number text, 
              shift_number int4, eps_type text, out_type int4, interval_begin text, 
              interval_end text, interval_code text, line_code int4, froute_id int4)
-- ========================================================================
-- Экспорт данных об изменении расписания
-- ========================================================================
as $$ declare
   v_route_variant_export   bigint;
begin
   v_route_variant_export := easu.easu_export_pkg$register_route_variant_export(p_export_id, p_route_variant_id);
   
   return query
   with tt_route_variant as (
      select ttv.tt_variant_id, ttv.route_variant_id, ttv.action_period, rte.tr_type_id
        from ttb.tt_variant ttv
        join rts.route_variant rtv on rtv.route_variant_id = ttv.route_variant_id
        join rts.route rte on rte.route_id = rtv.route_id
       where ttv.parent_tt_variant_id is null
         and not ttv.sign_deleted
         and not rtv.sign_deleted
         and ttv.tt_status_id in (ttb.tt_status_pkg$active(), ttb.tt_status_pkg$agreed())
         and ttv.action_period @> localtimestamp
         and rtv.route_variant_id = p_route_variant_id
   ),
   action_list as (
      select q.tt_action_id, q.dr_shift_id, q.time_begin, q.time_end, tar.round_id
        from (select tta.tt_action_id, tti.dr_shift_id, 
                     min(tti.time_begin) as time_begin,
                     max(tti.time_end)   as time_end
                from tt_route_variant ttr
                join ttb.tt_out tto on tto.tt_variant_id = ttr.tt_variant_id
                join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
                join ttb.tt_action_item tti on tti.tt_action_id = tta.tt_action_id
               where not tto.sign_deleted
                 and not tta.sign_deleted
                 and not tti.sign_deleted
               group by tta.tt_action_id, tti.dr_shift_id
             ) q
        left join ttb.tt_action_round tar on tar.tt_action_id = q.tt_action_id
   ),
   tt_variant_action_list as (
      select q.tt_variant_id, q.tt_out_id, q.tt_action_id, q.dt_action, q.row_num, q.last_tt_action_id
        from (select tto.tt_variant_id, tto.tt_out_id, tta.tt_action_id, tta.dt_action,
                     row_number() over (order by tta.dt_action) as row_num,
                     last_value(tta.tt_action_id) over (order by tta.dt_action rows between unbounded preceding and unbounded following) as last_tt_action_id
                from ttb.tt_out tto
                join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
                join ttb.tt_action_round ttr on ttr.tt_action_id = tta.tt_action_id
                join tt_route_variant rtv on rtv.tt_variant_id = tto.tt_variant_id
               where not tto.sign_deleted
                 and not tta.sign_deleted
          ) q
       where (q.row_num in (1, 2) or q.tt_action_id = q.last_tt_action_id)
   ),
   tt_variant_action as (
      select val.tt_out_id, 1 as out_status from tt_variant_action_list val where row_num = 1
      union
      select val.tt_out_id, 4 as out_status from tt_variant_action_list val where row_num not in (1, 2)
         and not exists (select 1 from tt_variant_action_list sal where sal.tt_out_id = val.tt_out_id and row_num = 1)
      union
      select val.tt_out_id, 2 as out_status from tt_variant_action_list val where row_num = 2
         and not exists (select 1 from tt_variant_action_list sal where sal.tt_out_id = val.tt_out_id and row_num  <> 2)
   ),
   tt_depo as (
      select ttu.tt_out_id, easu_depo_code
        from ttb.tt_out2tt_tr ttu
        join ttb.tt_tr ttt on ttt.tt_tr_id = ttu.tt_tr_id
        join core.depo2territory d2t on d2t.depo2territory_id = ttt.depo2territory_id
        join easu.depo2core d2c on d2c.depo_id = d2t.depo_id
   )
   select easu.easu_export_pkg$get_route_variant_export_code(rve.route_variant_export_id) as route_variant_code,
          1::int                as status,
          to_char(lower(rtv.action_period)::date, 'yyyymmdd') as date_begin,
          '030000'::text as time_begin,
          coalesce(to_char(upper(rtv.action_period)::date, 'yyyymmdd'), '99991231') as date_end,
          coalesce(to_char(upper(rtv.action_period)::time, 'hh24miss'),   '235959') as time_end,
          tto.tt_out_num::text  as out_number,
          ttd.easu_depo_code    as park_number,
          ds.dr_shift_num::int  as shift_number,
          easu.easu_export_pkg$get_eps_type(rtv.tr_type_id, tto.tr_capacity_id) as eps_type,
          coalesce(tva.out_status, 3) as out_type,
          to_char(ttai.time_begin::time, 'hh24miss') as interval_begin,
          to_char(ttai.time_end::time, 'hh24miss')   as interval_end,
          easu.easu_export_pkg$action_type_to_time_interval(tta.action_type_id)::text as interval_code,
          ttai.round_id         as line_code,
          null::int             as froute_id
     from tt_route_variant rtv
     join ttb.tt_out tto on tto.tt_variant_id = rtv.tt_variant_id
     join ttb.tt_action tta on tta.tt_out_id = tto.tt_out_id
     left join action_list ttai on ttai.tt_action_id = tta.tt_action_id
     left join ttb.dr_shift ds on ds.dr_shift_id = ttai.dr_shift_id
     left join easu.route_variant_export rve on rve.route_variant_id = rtv.route_variant_id
                                            and rve.export_id = p_export_id
     left join tt_variant_action tva on tva.tt_out_id = tto.tt_out_id
     left join tt_depo ttd on ttd.tt_out_id = tto.tt_out_id
    where not tto.sign_deleted
      and not tta.sign_deleted
    order by tto.tt_out_num, ttai.time_begin;
end;
$$ language plpgsql;
