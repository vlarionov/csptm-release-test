﻿-- ========================================================================
-- Пакет загрузки данных в ЕАСУ ФХД
-- ========================================================================

create or replace function easu.easu_import_pkg$import_drivers
  (v_park_number              in text,
   v_person_number            in bigint,
   v_person_name              in text)
returns integer
-- ========================================================================
-- Импорт данных о водителях
-- ========================================================================
as $$ declare
   v_id integer;
begin
   insert into easu.drivers as t (park_number, person_number, person_name)
   values (v_park_number, v_person_number, v_person_name)
   on conflict on constraint ck_drivers_unique do
   update set person_name = excluded.person_name
    where t.person_name is distinct from excluded.person_name
   returning driver_id into v_id;
   
   with dr as (
      select (select depo_id from easu.depo2core where depo2core.park_number = v_park_number) as depo_id,
             (regexp_split_to_array(v_person_name, E'\\s+'))[1] as driver_last_name,
             (regexp_split_to_array(v_person_name, E'\\s+'))[2] as driver_name,
             (regexp_split_to_array(v_person_name, E'\\s+'))[3] as driver_middle_name,
             v_person_number as tab_num
   )
   insert into core.driver
         (depo_id, driver_name, driver_last_name, driver_middle_name, tab_num)
   select depo_id, driver_name, driver_last_name, driver_middle_name, tab_num
     from dr
    where depo_id is not null
      and driver_last_name is not null
      and driver_name is not null
      and tab_num is not null
   on conflict (depo_id, tab_num)
   do update set (driver_name, driver_last_name, driver_middle_name) =
                 (excluded.driver_name, excluded.driver_last_name, excluded.driver_middle_name)
       where driver.driver_name        is distinct from excluded.driver_name
          or driver.driver_last_name   is distinct from excluded.driver_last_name
          or driver.driver_middle_name is distinct from excluded.driver_middle_name;
   
   return v_id;
end;
$$ language plpgsql;


create or replace function easu.easu_import_pkg$get_tr_or_create
  (v_depo_id                  in bigint,
   v_garage_number            in bigint)
returns bigint
-- ========================================================================
-- Получить ID TC или создать новый
-- ========================================================================
as $$ declare
   v_tr_id                    bigint;
   v_tr_type_id               smallint;
   v_dt_begin                 timestamp := '19000101'::timestamp;
   v_tr_status_id             bigint := 0;                                 -- Не определен
   v_tr_model_id              bigint := 494;                               -- НЕОПРЕДЕЛЕНО
   v_seat_qty_total           integer := 0;
   v_year_of_issue            integer := 1990;
begin
   
   select tr_id into v_tr_id from core.tr where depo_id = v_depo_id and garage_num = v_garage_number;
   if not found then
      select tr_type_id into v_tr_type_id from core.depo where depo_id = v_depo_id;
      
      insert into core.tr
             (garage_num, dt_begin, tr_type_id, tr_status_id, tr_model_id, seat_qty_total, depo_id, year_of_issue)
      values (v_garage_number, v_dt_begin, v_tr_type_id, v_tr_status_id, v_tr_model_id, v_seat_qty_total, v_depo_id, v_year_of_issue)
      returning tr_id into v_tr_id;
   end if;
   return v_tr_id;
end;
$$ language plpgsql;


create or replace function easu.easu_import_pkg$import_order_item
  (v_group_id                 in bigint,
   v_park_number              in text,
   v_out_date                 in date,
   v_column_number            in text,
   v_route_number             in text,
   v_out_number               in text,
   v_time_from                in time,
   v_time_to                  in time,
   v_route_kind               in smallint,
   v_garage_number            in bigint,
   v_garage_number_additive   in bigint,
   v_shift_number             in smallint,
   v_person_number            in bigint,
   v_route_type               in text)
returns integer
-- ========================================================================
-- Импорт данных о наряде в схему ttb
-- -1  Код парка соответствует несуществующему парку
-- -2  Не найден водитель
-- -3  Не найден маршрут 
-- -4  Не найден вид ТС
-- -5  Недопустимый тип маршрута
-- -6  Не найдено ТС
-- -7  Смена водителя не задана для обычного маршрута
-- -8  Пересечение периодов работы водителя
-- -9  Ошибка выпуска на линию. Отсутствуют активные расписания - пока не реализовано
-- -10 Ошибка выпуска на линию. ТС уже выпущено по маршруту     - пока не реализовано
-- -11 Неверный номер выхода
-- ========================================================================
as $$ declare
   v_depo_id                  bigint;
   v_mode_name                text;
   v_driver_id                integer;
   v_tr_id                    bigint;
   v_route_variant_id         integer;
   v_dr_shift_id              smallint;
   v_ts_from                  timestamp;
   v_ts_to                    timestamp;
   v_order_list_id            bigint;
   v_err_num                  integer := 0;
-- v_out_number_4             text;
begin
   
   if upper(v_route_type) = 'R' then
      return 0;
   end if;
   
   v_ts_from := v_out_date + v_time_from;
   if v_time_to >= v_time_from then
      v_ts_to := v_out_date + v_time_to;
   else
      v_ts_to := (v_out_date + integer '1') + v_time_to;
   end if;
   v_ts_from := v_ts_from - interval '3 hours';
   v_ts_to := v_ts_to - interval '3 hours';
   
   --raise notice 'ts_from = %', v_ts_from;
   --raise notice 'v_ts_to = %', v_ts_to;
   
   if v_route_kind not in (1, 2, 3) then
      return -4;
   end if;
   --raise notice 'v_route_kind = %', v_route_kind;
   
   if v_route_type not in ('1', '2', '7', '16') then
      return -5;
   end if;
   --raise notice 'v_route_type = %', v_route_type;
   
   --raise notice 'v_out_number = %', v_out_number;
   if length(v_out_number) <> 3 or not (v_out_number ~ '[0-9][0-9][0-9]') then
      return -11;
   end if;
   --v_out_number_4 := substring(v_out_number from 1 for 1) || '0' || substring(v_out_number from 2 for 2);
   --raise notice 'v_4out_number = %', v_4out_number;
   v_mode_name := substring(v_out_number from 1 for 1) || '00';
   --raise notice 'v_mode_name = %', v_mode_name;
   
   select depo_id into v_depo_id from easu.depo2core where depo2core.park_number = v_park_number;
   --raise notice 'v_depo_id = %', v_depo_id;
   if not found then
      return -1;
   end if;
   
   select current_route_variant_id into v_route_variant_id from rts.route where route_num = v_route_number and tr_type_id = v_route_kind;
   --raise notice 'v_route_variant_id = %', v_route_variant_id;
   if not found then
      return -3;
   end if;
   
   select driver_id into v_driver_id from core.driver where depo_id = v_depo_id and tab_num = v_person_number::text;
   --raise notice 'v_driver_id = %, v_person_number = %', v_driver_id, v_person_number;
   if v_err_num = 0 and v_person_number is not null and v_driver_id is null then
      v_err_num := -2;
   end if;
   
   v_tr_id := easu.easu_import_pkg$get_tr_or_create (v_depo_id, v_garage_number);
   --raise notice 'v_tr_id = %', v_tr_id;
   if v_err_num = 0 and v_garage_number is not null and v_tr_id is null then
      v_err_num := -6;
   end if;
   
   select dr_shift_id into v_dr_shift_id
     from ttb.dr_shift sh
     join ttb.mode md on md.mode_id = sh.mode_id
    where sh.dr_shift_num = v_shift_number 
      and md.mode_name  = v_mode_name
      and md.tr_type_id = v_route_kind;
   
   --raise notice 'v_dr_shift_id = %', v_dr_shift_id;
   if v_err_num = 0 and v_route_type = '1' and v_shift_number is null then
      v_err_num := -7;
   end if;
   
   insert into ttb.order_list as t
          (group_id, depo_id, route_variant_id, driver_id, dr_shift_id, tr_id, order_date, out_num, time_from, time_to)
   values (v_group_id, v_depo_id, v_route_variant_id, v_driver_id, v_dr_shift_id, v_tr_id, v_out_date, v_out_number::smallint, v_ts_from, v_ts_to)
   on conflict (group_id) do
   update set (depo_id, route_variant_id, driver_id, dr_shift_id, tr_id, order_date, out_num, time_from, time_to) = 
              (excluded.depo_id, excluded.route_variant_id, excluded.driver_id, excluded.dr_shift_id, excluded.tr_id,
               excluded.order_date, excluded.out_num, excluded.time_from, excluded.time_to)
    where t.depo_id     is distinct from excluded.depo_id or
          t.route_variant_id is distinct from excluded.route_variant_id or
          t.driver_id   is distinct from excluded.driver_id or
          t.dr_shift_id is distinct from excluded.dr_shift_id or
          t.tr_id       is distinct from excluded.tr_id or
          t.order_date  is distinct from excluded.order_date or
          t.out_num     is distinct from excluded.out_num or
          t.time_from   is distinct from excluded.time_from or
          t.time_to     is distinct from excluded.time_to
   returning order_list_id into v_order_list_id;
   
   if v_err_num = 0 and v_order_list_id is not null then
      -- Проверка Пересечение периодов работы водителя
      select -8 into v_err_num
        from ttb.order_list
       where order_date = v_out_date
         and driver_id = v_driver_id
         and order_list_id <> v_order_list_id
         and ((time_from >= v_ts_from and time_from < v_ts_to)
               or
              (time_from <  v_ts_from and time_to > v_ts_from))
       limit 1;
   end if;
   
   return coalesce(v_err_num, 0);
end;
$$ language plpgsql;


create or replace function easu.easu_import_pkg$reimport_order_item
  (v_group_id                 in bigint)
returns integer
-- ========================================================================
-- Повторный импорт данных о наряде в схему ttb
-- ========================================================================
as $$ declare
   v_err_num                  integer := 0;
begin
   select easu.easu_import_pkg$import_order_item(group_id,
                                                 park_number,
                                                 out_date,
                                                 column_number,
                                                 route_number,
                                                 out_number,
                                                 time_from,
                                                 time_to,
                                                 route_kind,
                                                 garage_number,
                                                 garage_number_additive,
                                                 shift_number,
                                                 person_number,
                                                 route_type)
     into v_err_num
     from easu.groups
    where group_id = v_group_id;
   
   return v_err_num;
end;
$$ language plpgsql;


create or replace function easu.easu_import_pkg$import_orders
  (v_park_number              in text,
   v_out_date                 in date,
   v_column_number            in text,
   v_route_number             in text,
   v_out_number               in text,
   v_time_from                in time,
   v_time_to                  in time,
   v_route_kind               in smallint,
   v_garage_number            in bigint,
   v_garage_number_additive   in bigint,
   v_shift_number             in smallint,
   v_person_number            in bigint,
   v_route_type               in text)
returns integer
-- ========================================================================
-- Импорт данных о нарядах
-- ========================================================================
as $$ declare
   v_group_id                 integer := null;
   v_err_num                  integer := 0;
begin
   
   -- Инорировать наряды без ТС и водителя
   if v_person_number = 0 and v_garage_number is null
   then
      return v_err_num;
   end if;
   
   if v_route_type <> 'R' then
      insert into easu.groups
             (park_number, out_date, column_number, route_number, out_number, time_from, time_to,
              route_kind, garage_number, garage_number_additive, shift_number, person_number, route_type)
      values (v_park_number, v_out_date, v_column_number, v_route_number, v_out_number, v_time_from, v_time_to,
              v_route_kind, v_garage_number, v_garage_number_additive, v_shift_number, v_person_number, v_route_type)
          on conflict (park_number, out_date, column_number, route_number, out_number, shift_number,
                       time_from, time_to, route_kind, garage_number, person_number, route_type)
       where route_type <> 'R' do
      update set garage_number_additive = excluded.garage_number_additive
      returning group_id into v_group_id;
  else
      if v_garage_number is null then
         insert into easu.groups
                 (park_number, out_date, column_number, route_number, out_number, time_from, time_to,
                  route_kind, garage_number, garage_number_additive, shift_number, person_number, route_type)
          values (v_park_number, v_out_date, v_column_number, v_route_number, v_out_number, v_time_from, v_time_to,
                  v_route_kind, v_garage_number, v_garage_number_additive, v_shift_number, v_person_number, v_route_type)
              on conflict (park_number, out_date, column_number, route_number, out_number, shift_number, person_number)
           where route_type = 'R' and garage_number is null do
          update set time_from              = excluded.time_from,
                     time_to                = excluded.time_to,
                     route_kind             = excluded.route_kind,
                     garage_number          = excluded.garage_number,
                     garage_number_additive = excluded.garage_number_additive,
                     route_type             = excluded.route_type;
      else
         insert into easu.groups
                (park_number, out_date, column_number, route_number, out_number, time_from, time_to,
                 route_kind, garage_number, garage_number_additive, shift_number, person_number, route_type)
         values (v_park_number, v_out_date, v_column_number, v_route_number, v_out_number, v_time_from, v_time_to,
                 v_route_kind, v_garage_number, v_garage_number_additive, v_shift_number, v_person_number, v_route_type)
             on conflict (park_number, out_date, column_number, route_number, out_number, shift_number, person_number, garage_number)
          where route_type = 'R' do
         update set time_from              = excluded.time_from,
                    time_to                = excluded.time_to,
                    route_kind             = excluded.route_kind,
                    garage_number_additive = excluded.garage_number_additive,
                    route_type             = excluded.route_type;
      end if;
   end if;
   
   if v_group_id is not null then
      v_err_num := easu.easu_import_pkg$import_order_item(v_group_id,
                                                          v_park_number,
                                                          v_out_date,
                                                          v_column_number,
                                                          v_route_number,
                                                          v_out_number,
                                                          v_time_from,
                                                          v_time_to,
                                                          v_route_kind,
                                                          v_garage_number,
                                                          v_garage_number_additive,
                                                          v_shift_number,
                                                          v_person_number,
                                                          v_route_type);
   end if;
   
   return v_err_num;
end;
$$ language plpgsql;


create or replace function easu.easu_import_pkg$select_tr_current_route_out
  (v_park_number              in  text,
   v_garage_number            in  bigint,
   v_route_num                out text,
   v_out_num                  out text)
-- ========================================================================
-- Текущие маршрут и выход для ТС
-- ========================================================================
as $$ declare
begin
   v_route_num := 'AAA';
   v_out_num := '111';
   
   -- ... code is comming
end;
$$ language plpgsql;


create or replace function easu.easu_import_pkg$tt_process_request()
returns integer
-- ========================================================================
-- Отпрвить запрос на пересчет оперативного расписания
-- ========================================================================
as $$ declare
   l_rmq_queue        text    := 'event-manager-booked';
   l_rmq_type         text    := 'ORDER_LISTS_AFTER_LOAD';
   l_command          jsonb   := '{}'::jsonb;
   v_result           integer := 0;
begin
   perform paa.paa_api_pkg$basic_publish(cmnd.jf_cmnd_ref4unit_pkg$cn_command_rmq_broker(), '', l_rmq_queue, l_command::text, l_rmq_type);
   return v_result;
end;
$$ language plpgsql;
