create or replace function easu.jf_reserve_groups_pkg$of_rows(
      p_id_account numeric,
      p_attr       text,
  out p_rows       refcursor
)
  returns refcursor
language plpgsql
as $$
declare
  l_f_out_date easu.groups.out_date%type := jofl.jofl_pkg$extract_date(p_attr, 'f_out_date_DT', true);
  l_f_depo_id  core.depo.depo_id%type := jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', true);
  l_f_column_number easu.groups.column_number%type :=  jofl.jofl_pkg$extract_varchar(p_attr, 'f_column_number_TEXT', true);

  l_tr_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
  l_DB_METHOD TEXT := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true);
  l_route_type text[] = ARRAY['R'];
  l_show_all boolean = false;
begin
  IF l_DB_METHOD = 'oud.tt_action_fact' THEN
      l_f_out_date = now()::DATE;
      l_route_type = ARRAY['R', '1', '2'];
      --select depo_id INTO l_f_depo_id from core.tr where tr_id = l_tr_id;
  END IF;
  open p_rows for
  select
    g.out_date,
    to_char(g.time_from, 'HH24:MI')                                              time_from,
    to_char(g.time_to, 'HH24:MI')                                                time_to,
    g.garage_number,
    trt.name                                                                     tr_type,
    e.name_full                                                                  park,
    g.route_number,
    g.out_number,
    g.shift_number,
    g.person_number,
    dr.driver_last_name || ' ' || dr.driver_name || ' ' || dr.driver_middle_name person_name,
    g.column_number,
    g.group_id
  from easu.groups g
    left join core.driver dr on g.person_number :: text = dr.tab_num
    left join easu.depo2core d2c on g.park_number = d2c.park_number
    left join core.depo d on d2c.depo_id = d.depo_id
    left join core.entity e on d.depo_id = e.entity_id
    left join core.tr_type trt on g.route_kind = trt.tr_type_id
  where g.route_type = ANY (l_route_type)
        and g.out_date = l_f_out_date
        and (l_f_depo_id is null or d.depo_id = l_f_depo_id)
        and ((l_f_column_number = '') is not false or g.column_number = l_f_column_number);
end;
$$;