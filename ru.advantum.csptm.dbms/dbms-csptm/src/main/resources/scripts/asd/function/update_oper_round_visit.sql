create or replace function asd.update_oper_round_visit
    ( p_json in jsonb )
returns void as
$body$
    declare
        l_status smallint;
        l_json_item jsonb;

        l_event_time_begin timestamp without time zone;
        l_packet_id_begin bigint;
        l_event_time_end timestamp without time zone;
        l_packet_id_end bigint;
        l_gs_begin_time timestamp without time zone;
        l_gs_end_time timestamp without time zone;
        l_event_type text;
        l_order_round_id bigint;
    begin
        for l_json_item in select jsonb_array_elements(p_json) loop
            begin
                l_event_time_begin := (l_json_item->'eventBegin'->'simpleUnitPacket'->>'eventTime')::timestamp without time zone;
                l_packet_id_begin := (l_json_item->'eventBegin'->'simpleUnitPacket'->>'packetId')::bigint;
                l_event_time_end := (l_json_item->'eventEnd'->'simpleUnitPacket'->>'eventTime')::timestamp without time zone;
                l_packet_id_end := (l_json_item->'eventEnd'->'simpleUnitPacket'->>'packetId')::bigint;
                l_gs_begin_time := (l_json_item->'graphSectionBegin'->>'startTime')::timestamp without time zone;
                l_gs_end_time := (l_json_item->'graphSectionEnd'->>'startTime')::timestamp without time zone;
                l_event_type := l_json_item->'eventEnd'->>'type';
                l_order_round_id := (l_json_item->>'orderRoundId')::bigint;

                if l_event_time_begin is null
                    or l_event_time_end is null
                    or l_event_type = 'TRAJ_END_ERROR'
                    or l_gs_begin_time is null
                    or l_gs_end_time is null
                then
                    l_status := tt.const_pkg$ord_status_error();
                else
                    l_status := tt.const_pkg$ord_status_passed();
                end if;

                update
                    asd.oper_round_visit
                set
                    time_fact_begin       = l_event_time_begin,
                    packet_id_begin       = l_packet_id_begin,
                    time_fact_end         = l_event_time_end,
                    packet_id_end         = l_packet_id_end,
                    order_round_status_id = l_status,
                    first_graph_section   = l_gs_begin_time,
                    last_graph_section    = l_gs_end_time
                where
                    order_round_id = l_order_round_id
                    -- "закончить" можно только еще не законченный рейс.
                    -- это обезопасит от случаев, когда из-за баги в graph-linker один и тот же рейс два раза прогоняли
                    and order_round_status_id != tt.const_pkg$ord_status_passed();
            end;
        end loop;
    end;
$body$ language plpgsql;