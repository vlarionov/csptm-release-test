create or replace function asd.fill_agg_gs_speed
    ( p_date in timestamp without time zone )
returns void as
$body$
    declare
        l_dow smallint;
    begin
        l_dow := extract(isodow from p_date);

        insert into asd.agg_gs_speed(route_trajectory_muid, graph_section_muid, vehicle_type_muid, week_day_id, visit_hour, visit_count, visit_duration)
        	select route_trajectory_muid, graph_section_muid, vehicle_type_muid, week_day_id, visit_time, sum(visit_count), array_agg(visit_duration)
        	from (
        	    select
        	        r.route_trajectory_muid,
                    gss.graph_section_muid,
                    tr.vehicle_type_muid,
                    l_dow as week_day_id,
                    core.floor_minutes(gss.begin_time, 30) as visit_time,
                    1 as visit_count,
                    extract(epoch from (gss.end_time - gss.begin_time)) as visit_duration
                from asd.graph_section_speed as gss
                inner join tt.order_round as oro
                    on oro.order_round_id = gss.order_round_id
                inner join tt.round as r
                    on oro.round_id = r.round_id
                inner join tt.order_list as ol
                    on oro.order_list_id = ol.order_list_id
                inner join core.tr as tr
                    on ol.tr_id = tr.tr_id
                where gss.begin_time < p_date + interval '1d'
                    and gss.begin_time >= p_date) as t
            group by route_trajectory_muid, graph_section_muid, vehicle_type_muid, week_day_id, visit_time;
    end;
$body$ language plpgsql;