﻿CREATE OR REPLACE FUNCTION asd."jf_ct_gs_speed_result_pkg$attr_to_rowtype"(p_attr text)
 RETURNS asd.ct_gs_speed_result
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.ct_gs_speed_result%rowtype; 
begin 
   l_r.ct_gs_speed_result_id := jofl.jofl_pkg$extract_number(p_attr, 'ct_gs_speed_result_id', true); 
   l_r.calculation_task_id := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true); 
   l_r.week_day_id := jofl.jofl_pkg$extract_varchar(p_attr, 'week_day_id', true); 
   l_r.hour := jofl.jofl_pkg$extract_varchar(p_attr, 'hour', true); 
   l_r.tr_type_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_type_id', true);
 --l_r.route_id := jofl.jofl_pkg$extract_number(p_attr, 'route_id', true);
   l_r.tr_capacity_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_capacity_id', true);
   l_r.graph_section_id := jofl.jofl_pkg$extract_number(p_attr, 'graph_section_id', true); 
   l_r.avg_speed := jofl.jofl_pkg$extract_varchar(p_attr, 'avg_speed', true); 

   return l_r;
end;
 $function$
;
-------------------

create or replace function asd.jf_ct_gs_speed_result_pkg$of_rows
  (p_id_account   in  numeric, 
   p_attr         in  text,
   p_rows         out refcursor)
returns refcursor
as $$ declare
   l_r asd.ct_gs_speed_result%rowtype;
begin
   l_r := asd.jf_ct_gs_speed_result_pkg$attr_to_rowtype(p_attr);
   
   open p_rows for 
   select res.ct_gs_speed_result_id, 
          res.calculation_task_id, 
          res.week_day_id, 
          res.hour + date '1970-01-01' "hour",
          res.tr_type_id,
        --res.route_muid,
          res.route_id,
          res.tr_capacity_id,
        --res.graph_section_muid,
          res.graph_section_id,
          round(res.avg_speed) as avg_speed,
          ctt.ct_type_name,
        --ctrt.ct_round_type_name,
          '' ct_round_type_name,
          to_char(gss.calc_period_begin,'dd.mm.yyyy') || ' - ' || to_char(gss.calc_period_end,'dd.mm.yyyy') as dt_period,
          case gss.detail_transp_kind
             when true then tk.name 
             else (select string_agg(ttk.name, ' ,')
                     from core.tr_type ttk
                    where exists (select 1 from asd.tr_type2ct t
                                   where t.calculation_task_id = res.calculation_task_id
                                     and t.tr_type_id= ttk.tr_type_id)
                  )
          end tk_name,
          case gss.detail_route
             when true then r.route_num 
             else case (select count(1) from asd.route2ct r2ct where r2ct.calculation_task_id = res.calculation_task_id)
                     when 1 then (select rts.route_num from asd.route2ct r2ct join rts.route rts on rts.route_id = r2ct.route_id
                                   where r2ct.calculation_task_id = res.calculation_task_id)
                     else '<Список...>' 
                  end
          end route_name,
          case gss.detail_hour when true then res.hour + date '1970-01-01' else h2ct.hour_from end as hour_from,
          case gss.detail_hour when true then res.hour + date '1970-01-01' else h2ct.hour_to end as hour_to,
          (select string_agg(wd.wd_short_name, ' ,')  
             from core.week_day wd 
            where exists (select 1 from asd.week_day2ct t 
                           where t.calculation_task_id = res.calculation_task_id
                             and t.week_day_id= wd.week_day_id)
          ) as week_day_name,
          (select string_agg(cp.short_name, ' ,')
             from core.tr_capacity cp
            where exists (select 1 from  asd.tr_capacity2ct t
                           where t.calculation_task_id = res.calculation_task_id
                            and t.tr_capacity_id= cp.tr_capacity_id)
          ) vht_name
     from asd.ct_gs_speed_result res
     join asd.calculation_task ct on ct.calculation_task_id = res.calculation_task_id
     join asd.ct_graph_section_speed gss on gss.calculation_task_id = res.calculation_task_id
     join asd.ct_type ctt on ctt.ct_type_id = ct.ct_type_id
   --join asd.ct_round_type ctrt on ctrt.ct_round_type_id = ct.ct_round_type_id
     join asd.hours2ct h2ct on h2ct.calculation_task_id = res.calculation_task_id
     left join core.tr_type tk on tk.tr_type_id= res.tr_type_id
     left join rts.route r on r.route_id = res.route_id
     left join core.tr_capacity cp on cp.tr_capacity_id= res.tr_capacity_id
    where res.calculation_task_id = l_r.calculation_task_id;
end;
$$ language plpgsql;

-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_gs_speed_result_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.ct_gs_speed_result%rowtype;
begin 
   l_r := asd.jf_ct_gs_speed_result_pkg$attr_to_rowtype(p_attr);

  /* update asd.ct_gs_speed_result set 
          calculation_task_id = l_r.calculation_task_id, 
          week_day_id = l_r.week_day_id, 
          hour = l_r.hour, 
          transport_kind_muid = l_r.transport_kind_muid,
          route_muid = l_r.route_muid, 
          tr_capacity_id = l_r.tr_capacity_id, 
          graph_section_muid = l_r.graph_section_muid, 
          avg_speed = l_r.avg_speed
   where 
          ct_gs_speed_result_id = l_r.ct_gs_speed_result_id;
*/
   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_gs_speed_result_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.ct_gs_speed_result%rowtype;
begin 
   l_r := asd.jf_ct_gs_speed_result_pkg$attr_to_rowtype(p_attr);

--   delete from  asd.ct_gs_speed_result where  ct_gs_speed_result_id = l_r.ct_gs_speed_result_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_gs_speed_result_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.ct_gs_speed_result%rowtype;
begin 
   l_r := asd.jf_ct_gs_speed_result_pkg$attr_to_rowtype(p_attr);

   --insert into asd.ct_gs_speed_result select l_r.*;

   return null;
end;
 $function$
;