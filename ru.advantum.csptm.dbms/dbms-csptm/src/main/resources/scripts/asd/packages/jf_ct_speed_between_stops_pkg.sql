﻿
--DROP TYPE asd.jf_calc_speed_bs_type

do $$
begin
  if not exists(select * from pg_type where typname = 'jf_calc_speed_bs_type') THEN

    CREATE TYPE asd.jf_calc_speed_bs_type AS
      (r_sbs asd.ct_speed_between_stops,
       r_calc_task asd.calculation_task);

    ALTER TYPE asd.jf_calc_speed_bs_type
      OWNER TO adv;

  end if;
end$$;

create or replace function asd.jf_ct_speed_between_stops_pkg$of_rows
  (p_id_account in  numeric,
   p_attr       in  text,
   p_rows       out refcursor)
returns refcursor
-- ========================================================================
-- Задачи расчета скоростей между остановками
-- ========================================================================
as $$ declare
   l_f_dt_begin timestamp := jofl.jofl_pkg$extract_date(p_attr, 'F_BEGIN_DT', true);
   l_f_dt_end   timestamp := jofl.jofl_pkg$extract_date(p_attr, 'F_END_DT',   true);
begin
   open p_rows for
   select ct.calculation_task_id,
          ct.account_id,
          ct.ct_status_id,
          ct.name,
          ct.time_create,
          ct.hour_period,
          sbs.calc_period_begin,
          sbs.calc_period_end,
        --to_char(sbs.calc_period_begin, 'DD.MM.YYYY') || '-' || to_char(sbs.calc_period_end, 'DD.MM.YYYY') date_interval,
          to_char(sbs.calc_period_begin + interval '3 hour', 'DD.MM.YYYY') || '-' || to_char(sbs.calc_period_end + interval '3 hour', 'DD.MM.YYYY') date_interval,
          ct.time_calc_begin,
          ct.time_calc_end,
          st.name status_name,
          rt.route_num,
          rt.route_id s_route_id,
          tt.tr_type_id f_tr_type_id,
          tt.name transport_kind_name,
          rd.round_id f_round_id,
          rd.round_num,
          rd.code round_code,
          md.move_direction_id f_move_direction_id,
          md.move_direction_name,
          si1.stop_item_id stop_item_id_1,
          sp1.name         stop_item_name_1,
          sir1.stop_item2round_id stop_item2round_id_1,
          sir1.order_num   stop_item_order_1,
          si2.stop_item_id stop_item_id_2,
          sp2.name         stop_item_name_2,
          sir2.stop_item2round_id stop_item2round_id_2,
          sir2.order_num   stop_item_order_2,
          case when ct.ct_status_id = 0 -- новый
             then 'P{A},E{OF_DELETE,OF_UPDATE,OF_CALC,OF_INSERT},P{D},D{asd.week_day2ct,asd.tr_capacity2ct,asd.hours2ct}'
             when ct.ct_status_id = 3 -- "готово", только чтение
               then 'P{A},E{OF_DELETE},P{D},D{asd.week_day2ct2,asd.tr_capacity2ct2,asd.hours2ct2}'
             else ''
          end as "ROW$POLICY",
          sbs.is_capacity_dependent
     from asd.calculation_task ct
     join asd.ct_speed_between_stops sbs on sbs.calculation_task_id = ct.calculation_task_id
     join asd.ct_status st on st.ct_status_id = ct.ct_status_id
     join rts.round rd on rd.round_id = sbs.round_id
     join rts.move_direction md on md.move_direction_id = rd.move_direction_id
     join rts.route_variant rv on rv.route_variant_id = rd.route_variant_id
     join rts.route rt on rt.route_id = rv.route_id
     join core.tr_type tt on tt.tr_type_id = rt.tr_type_id
     join rts.stop_item2round sir1 on sir1.stop_item2round_id = sbs.stop_item2round_1_id
     join rts.stop_item si1 on si1.stop_item_id = sir1.stop_item_id
     join rts.stop_location sl1 on sl1.stop_location_id = si1.stop_location_id
     join rts.stop sp1 on sp1.stop_id = sl1.stop_id
     join rts.stop_item2round sir2 on sir2.stop_item2round_id = sbs.stop_item2round_2_id
     join rts.stop_item si2 on si2.stop_item_id = sir2.stop_item_id
     join rts.stop_location sl2 on sl2.stop_location_id = si2.stop_location_id
     join rts.stop sp2 on sp2.stop_id = sl2.stop_id
    where (l_f_dt_begin is null or l_f_dt_end is null)
       or ct.time_create between l_f_dt_begin and l_f_dt_end
    order by ct.time_create;
end;
$$ language plpgsql;


create or replace function asd.jf_ct_speed_between_stops_pkg$attr_to_rowtype
  (p_attr   in text)
returns asd.jf_calc_speed_bs_type
-- ========================================================================
-- JSON -> Type
-- ========================================================================
as $$ declare
   l_r_return      asd.jf_calc_speed_bs_type;
   l_r_sbs         asd.ct_speed_between_stops%rowtype;
   l_r_calc_task   asd.calculation_task%rowtype;
begin
   l_r_sbs.calculation_task_id       := jofl.jofl_pkg$extract_number (p_attr, 'calculation_task_id', true);
   l_r_sbs.calc_period_begin         := jofl.jofl_pkg$extract_date   (p_attr, 'calc_period_begin', true);
   l_r_sbs.calc_period_end           := jofl.jofl_pkg$extract_date   (p_attr, 'calc_period_end', true);
   l_r_sbs.stop_item2round_1_id      := jofl.jofl_pkg$extract_number (p_attr, 'stop_item2round_id_1', true);
   l_r_sbs.stop_item2round_2_id      := jofl.jofl_pkg$extract_number (p_attr, 'stop_item2round_id_2', true);
   l_r_sbs.round_id                  := jofl.jofl_pkg$extract_number (p_attr, 'f_round_id', true);
   l_r_sbs.is_capacity_dependent     := jofl.jofl_pkg$extract_boolean(p_attr, 'is_capacity_dependent', true);

   l_r_calc_task.calculation_task_id := jofl.jofl_pkg$extract_number (p_attr, 'calculation_task_id', true);
   l_r_calc_task.account_id          := jofl.jofl_pkg$extract_number (p_attr, 'account_id', true);
   l_r_calc_task.ct_status_id        := jofl.jofl_pkg$extract_varchar(p_attr, 'ct_status_id', true);
   l_r_calc_task.name                := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true);
   l_r_calc_task.time_create         := jofl.jofl_pkg$extract_date   (p_attr, 'time_create', true);
   l_r_calc_task.time_calc_begin     := jofl.jofl_pkg$extract_date   (p_attr, 'time_calc_begin', true);
   l_r_calc_task.time_calc_end       := jofl.jofl_pkg$extract_date   (p_attr, 'time_calc_end', true);
   l_r_calc_task.ct_type_id          := jofl.jofl_pkg$extract_varchar(p_attr, 'ct_type_id', true);
   l_r_calc_task.detail_capacity     := jofl.jofl_pkg$extract_boolean(p_attr, 'detail_capacity', true);
   l_r_calc_task.hour_period         := jofl.jofl_pkg$extract_number (p_attr, 'hour_period', true);
   
   l_r_return.r_sbs := l_r_sbs;
   l_r_return.r_calc_task := l_r_calc_task;
   
   return l_r_return;
end;
$$ language plpgsql;


create or replace function asd.jf_ct_speed_between_stops_pkg$of_insert
  (p_id_account   in numeric,
   p_attr         in text)
returns text
-- ========================================================================
-- Создать задачу расчета
-- ========================================================================
as $$ declare
   l_r_return      asd.jf_calc_speed_bs_type;
   l_r_sbs         asd.ct_speed_between_stops%rowtype;
   l_r_calc_task   asd.calculation_task%rowtype;
   l_res           text;
begin
   l_r_return := asd.jf_ct_speed_between_stops_pkg$attr_to_rowtype(p_attr);
   l_r_sbs := l_r_return.r_sbs;
   l_r_calc_task := l_r_return.r_calc_task;
   
   l_r_sbs.calculation_task_id := nextval('asd.calculation_task_calculation_task_id_seq');
   l_r_calc_task.calculation_task_id := l_r_sbs.calculation_task_id;
   l_r_calc_task.account_id := p_id_account;
   l_r_calc_task.time_create := now();
   l_r_calc_task.ct_status_id := 0; -- новый
   l_r_calc_task.ct_type_id := asd.calculation_task_pkg$cn_ct_type_stop_speed();
   l_r_calc_task.detail_capacity := 0;
   
   l_res :=  asd.jf_calculation_task_pkg$of_insert(p_id_account, row_to_json(l_r_calc_task)::text);
   
   insert into asd.ct_speed_between_stops select l_r_sbs.*;
   
   return null;
end;
$$ language plpgsql;


create or replace function asd.jf_ct_speed_between_stops_pkg$of_update
  (p_id_account   in numeric,
   p_attr         in text)
returns text
-- ========================================================================
-- Изменить задачу расчета
-- ========================================================================
as $$ declare
   l_r_return      asd.jf_calc_speed_bs_type;
   l_r_sbs         asd.ct_speed_between_stops%rowtype;
   l_r_calc_task   asd.calculation_task%rowtype;
   l_res           text;
begin
   l_r_return := asd.jf_ct_speed_between_stops_pkg$attr_to_rowtype(p_attr);
   l_r_sbs := l_r_return.r_sbs ;
   l_r_calc_task := l_r_return.r_calc_task ;
   
   if l_r_calc_task.ct_status_id != asd.calculation_task_pkg$cn_ct_status_new() then
      return null;
   end if;
   
   l_res :=  asd.jf_calculation_task_pkg$of_update(p_id_account, row_to_json(l_r_calc_task)::text);
   
   update asd.ct_speed_between_stops
      set calc_period_begin    = l_r_sbs.calc_period_begin,
          calc_period_end      = l_r_sbs.calc_period_end,
          stop_item2round_1_id = l_r_sbs.stop_item2round_1_id,
          stop_item2round_2_id = l_r_sbs.stop_item2round_2_id,
          round_id             = l_r_sbs.round_id,
          is_capacity_dependent= l_r_sbs.is_capacity_dependent
    where calculation_task_id  = l_r_sbs.calculation_task_id;
   
   return null;
end;
$$ language plpgsql;


create or replace function asd.jf_ct_speed_between_stops_pkg$of_delete
  (p_id_account   in numeric,
   p_attr         in text)
returns text
-- ========================================================================
-- Удалить задачу расчета
-- ========================================================================
as $$ declare
   l_r_return     asd.jf_calc_speed_bs_type;
   l_r_sbs        asd.ct_speed_between_stops%rowtype;
   l_r_calc_task  asd.calculation_task%rowtype;
   l_res text;
begin
   l_r_return := asd.jf_ct_speed_between_stops_pkg$attr_to_rowtype(p_attr);
   l_r_sbs := l_r_return.r_sbs ;
   l_r_calc_task := l_r_return.r_calc_task ;
   
   if l_r_calc_task.ct_status_id != asd.calculation_task_pkg$cn_ct_status_new() then
      return null;
   end if;
   
   delete from asd.hours2ct    where calculation_task_id = l_r_sbs.calculation_task_id;
   delete from asd.params2ct   where calculation_task_id = l_r_sbs.calculation_task_id;
   delete from asd.week_day2ct where calculation_task_id = l_r_sbs.calculation_task_id;
   delete from asd.route_variant2ct   where calculation_task_id = l_r_sbs.calculation_task_id;
   delete from asd.ct_speed_bs_result where calculation_task_id = l_r_sbs.calculation_task_id;
   delete from asd.ct_speed_between_stops where calculation_task_id = l_r_sbs.calculation_task_id;
   
   l_res:= asd.jf_calculation_task_pkg$of_delete(p_id_account, row_to_json(l_r_calc_task)::text);
   
   return null;
end;
$$ language plpgsql;


create or replace function asd.jf_ct_speed_between_stops_pkg$of_calc
  (p_id_account   in numeric,
   p_attr         in text)
returns text
-- ========================================================================
-- Выпонить задачу расчета
-- ========================================================================
as $$ declare
   l_r_return      asd.jf_calc_speed_bs_type;
   l_r_sbs         asd.ct_speed_between_stops%rowtype;
   l_r_calc_task   asd.calculation_task%rowtype;
begin
   l_r_return := asd.jf_ct_speed_between_stops_pkg$attr_to_rowtype(p_attr);
   l_r_sbs := l_r_return.r_sbs ;
   l_r_calc_task := l_r_return.r_calc_task ;
   
   if (select count(1) from asd.hours2ct where calculation_task_id = l_r_sbs.calculation_task_id) = 0 then
      raise exception '<<Введите диапазон часов суток>>';
   end if;
   
   if (select count(1) from asd.week_day2ct where calculation_task_id = l_r_sbs.calculation_task_id) = 0 then
      raise exception '<<Выберите хоть один день недели>>';
   end if;
   
   perform asd.calculation_task_pkg$queue_task(l_r_calc_task.calculation_task_id);
   
   return null;
end;
$$ language plpgsql;
------------------------------------------------------------------
CREATE OR REPLACE FUNCTION asd.jf_ct_speed_between_stops_pkg$of_copy (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare
   ln_calculation_task_id       asd.calculation_task.calculation_task_id%type := jofl.jofl_pkg$extract_number (p_attr, 'calculation_task_id', true);
   ln_calculation_task_id_new	asd.calculation_task.calculation_task_id%type := nextval('asd.calculation_task_calculation_task_id_seq');
   lt_new_name					asd.calculation_task.name%type;
begin
   
   with ins_ct_sbs as (
	insert into asd.ct_speed_between_stops
    		 ( calculation_task_id
              ,calc_period_begin
              ,calc_period_end
              ,stop_item2round_1_id
              ,stop_item2round_2_id
              ,round_id
              ,is_capacity_dependent
              )
	select ln_calculation_task_id_new
    	  ,calc_period_begin
    	  ,calc_period_end
          ,stop_item2round_1_id
          ,stop_item2round_2_id
          ,round_id
          ,is_capacity_dependent
	from asd.ct_speed_between_stops where calculation_task_id = ln_calculation_task_id
    returning *
    )
    ,ins_ct as (
    insert into asd.calculation_task
    		( calculation_task_id
             ,account_id
             ,ct_status_id
             ,name
             ,time_create
             ,time_calc_begin
             ,time_calc_end
             ,ct_type_id
             ,detail_capacity
             ,hour_period
             )
    select 	ln_calculation_task_id_new
    	   ,p_id_account
    	   ,0
           ,name || ' (копия)'
           ,now()
           ,null::timestamp
           ,null::timestamp
           ,ct_type_id
           ,detail_capacity
           ,hour_period
	from asd.calculation_task where calculation_task_id = ln_calculation_task_id
    returning *
    )
    ,ins_hours2ct as (
    insert into asd.hours2ct
    	   ( calculation_task_id
            ,hour_from
            ,hour_to
            )
    select ( select ins_ct_sbs.calculation_task_id from ins_ct_sbs )
    		,hour_from
            ,hour_to
    from asd.hours2ct where calculation_task_id = ln_calculation_task_id
    returning *
    )
    ,ins_week_day2ct as (
    insert into asd.week_day2ct
    		(calculation_task_id
			,week_day_id
            )
    select ( select ins_ct_sbs.calculation_task_id from ins_ct_sbs )
    		,week_day_id
    from asd.week_day2ct where calculation_task_id = ln_calculation_task_id
	returning *
    )
    --,ins_tr_capacity2ct
    insert into asd.tr_capacity2ct
    		(calculation_task_id
			,tr_capacity_id
            )
    select ( select ins_ct_sbs.calculation_task_id from ins_ct_sbs )
    		,tr_capacity_id
    from asd.tr_capacity2ct where calculation_task_id = ln_calculation_task_id
	--returning *
    ;
	
    select name
    into lt_new_name
    from asd.calculation_task
    where calculation_task_id = ln_calculation_task_id_new;
    
    return jofl.jofl_util$cover_result('Создан расчет:<b>' || lt_new_name || '</b>');
   
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;