﻿-- ========================================================================
-- Пакет расчета средней скорости в привязке на ребрам графа
-- ========================================================================

create or replace function asd.calc_gs_speed_pkg$calc_task
  (p_ct_id   in integer)
returns void
-- ========================================================================
-- Расчет средней скорости в привязке на ребрам графа
-- ========================================================================
as $$ declare
   l_ct                   record;
   l_ct_hour              record;
   l_hour_from            time without time zone;
   l_hour_to              time without time zone;
   l_cross_day            boolean;
   l_extreme_percent      numeric;
   
   l_tr_types             smallint[];
   l_tr_capacity          bigint[];
   l_routes               bigint[];
   l_route_var            bigint[];
   l_rounds               integer[];
   l_week_days            smallint[];
begin
   
   select * into l_ct
     from asd.calculation_task as ct
     join asd.ct_graph_section_speed as ctgss on ct.calculation_task_id = ctgss.calculation_task_id
    where ct.calculation_task_id = p_ct_id;
   
--   raise notice 'l_ct: %', l_ct;
   
   select * into l_ct_hour from asd.hours2ct as h2ct where h2ct.calculation_task_id = p_ct_id;
   select asd.get_constant('PRC_EXTR') into l_extreme_percent;

--   raise notice 'l_ct_hour: %', l_ct_hour;
--   raise notice 'l_extreme_percent: %', l_extreme_percent;
   
   l_hour_from := l_ct_hour.hour_from::time;
   l_hour_to   := l_ct_hour.hour_to::time;
   l_cross_day := l_hour_from > l_hour_to;

--   raise notice 'l_hour_from: %', l_hour_from;
--   raise notice 'l_hour_to: %', l_hour_to;
--   raise notice 'l_cross_day: %', l_cross_day;
   
   select array_agg(rct.route_variant_id) into l_route_var
     from asd.route_variant2ct as rct
    where rct.calculation_task_id = p_ct_id;
   
--   raise notice 'l_route_var: %', l_route_var;
   
   if l_route_var is null or array_length(l_route_var, 1) = 0 then
      select array_agg(r.current_route_variant_id) into l_route_var
        from asd.tr_type2ct t
        join rts.route r on r.tr_type_id = t.tr_type_id
       where t.calculation_task_id = p_ct_id;
   end if;
   
   select array_agg(rd.round_id) into l_rounds
     from rts.round rd
    where rd.route_variant_id = any(l_route_var);
   
--   raise notice 'l_rounds: %', l_rounds;
   
   select array_agg(tcct.tr_capacity_id) into l_tr_capacity
     from asd.tr_capacity2ct tcct
    where tcct.calculation_task_id = p_ct_id;

--   raise notice 'l_tr_capacity: %', l_tr_capacity;
   
   select array_agg(wdct.week_day_id) into l_week_days
     from asd.week_day2ct wdct
    where wdct.calculation_task_id = p_ct_id;
   
--   raise notice 'l_week_days: %', l_week_days;
/*   
   select array_agg(tct.tr_type_id) into l_tr_types
     from asd.tr_type2ct tct
    where tct.calculation_task_id = p_ct_id;
   
   raise notice 'l_tr_types: %', l_tr_types;
*/   
   insert into asd.ct_gs_speed_result
         (calculation_task_id, week_day_id, tr_type_id, tr_capacity_id, avg_speed,       hour, graph_section_id, route_variant_id)
   select calculation_task_id, week_day_id, tr_type_id, tr_capacity_id, avg_speed, visit_hour, graph_section_id, route_variant_id
     from (select l_ct.calculation_task_id     as calculation_task_id,
                  avs.week_day_id_t            as week_day_id,
                  avs.tr_type_id_t             as tr_type_id,
                  avs.route_variant_id_t       as route_variant_id,
                  avs.visit_hour_t             as visit_hour,
                  avs.graph_section_id_t       as graph_section_id,
                  avs.tr_capacity_id_t         as tr_capacity_id,
                  (grs."length" / (asd.calc_gs_speed_pkg$calc_visit_duration_sum(visit_duration_t, l_extreme_percent))) * 3.6 as avg_speed -- километры в час
             from (select gss.week_day_id       as week_day_id_t,
                          case when l_ct.detail_transp_kind then gss.tr_type_id else null end  as tr_type_id_t,
                          case when l_ct.detail_route then gss.route_variant_id else null end  as route_variant_id_t,
                          gss.visit_hour        as visit_hour_t,
                          gss.graph_section_id  as graph_section_id_t,
                          case when l_ct.detail_capacity then gss.tr_capacity_id else null end as tr_capacity_id_t,
                          core.array_accum(gss.visit_duration) as visit_duration_t,
                          sum(gss.visit_count)  as visit_count_t
                     from (select ags.graph_section_id, ags.tr_capacity_id, ags.visit_duration, ags.visit_count,
                                  case
                                     when not l_ct.detail_hour then null
                                     when l_ct.hour_period = 60 then date_trunc('hour', ags.visit_hour)::time
                                     else ags.visit_hour::time
                                  end           as visit_hour,
                                  case when l_ct.detail_week_day 
                                     then ags.week_day_id 
                                     else null 
                                  end           as week_day_id,
                                  rnd.route_variant_id, rut.tr_type_id
                             from asd.agg_gs_speed as ags
                             join rts.round rnd on rnd.round_id = ags.round_id
                             join rts.route_variant rov on rov.route_variant_id = rnd.route_variant_id
                             join rts.route rut on rut.route_id = rov.route_id
                            where ags.visit_hour between l_ct.calc_period_begin and l_ct.calc_period_end
                              and case when l_cross_day 
                                     then ags.visit_hour::time >= l_hour_from  or ags.visit_hour::time <= l_hour_to
                                     else ags.visit_hour::time >= l_hour_from and ags.visit_hour::time <= l_hour_to
                                  end
                              and (l_week_days is null or ags.week_day_id = any(l_week_days))
                              and (l_tr_capacity is null or ags.tr_capacity_id = any(l_tr_capacity))
                              and rnd.round_id = any(l_rounds)
                          ) as gss
                    group by visit_hour_t, week_day_id_t, tr_type_id_t, route_variant_id_t, graph_section_id_t, tr_capacity_id_t
                  ) as avs
             join rts.graph_section as grs on grs.graph_section_id = avs.graph_section_id_t
          ) as t;
end;
$$ language plpgsql;


create or replace function asd.calc_gs_speed_pkg$calc_visit_duration_sum 
  (p_durations   in double precision[], 
   p_percent     in numeric)
returns double precision
-- ========================================================================
-- 
-- ========================================================================
as $$ declare
   l_array_size integer;
   l_subtract integer;
   l_result_size integer;
   l_result double precision;
begin
   l_array_size = array_length(p_durations, 1);
   l_subtract = floor(l_array_size / p_percent);
   l_result_size := l_array_size - l_subtract * 2;
   
   select sum(vd) / l_result_size into l_result
     from (select vd from unnest(p_durations) as vd
            order by 1
            limit l_result_size
           offset l_subtract
          ) as vd;
  
   return l_result;
end;
$$ language plpgsql;


create or replace function asd.calc_gs_speed_pkg$clean_task
  (p_ct_id   in integer)
returns void
-- ========================================================================
-- Удалить задачу расчета скорости
-- ========================================================================
as $$
begin
   delete from asd.ct_gs_speed_result where calculation_task_id = p_ct_id;
end;
$$ language plpgsql;
