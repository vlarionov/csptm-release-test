CREATE OR REPLACE FUNCTION asd."jf_week_day2ct_pkg$attr_to_rowtype"(p_attr text)
 RETURNS asd.week_day2ct
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.week_day2ct%rowtype; 
begin 
   l_r.calculation_task_id := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true); 
   l_r.week_day_id := jofl.jofl_pkg$extract_varchar(p_attr, 'week_day_id', true); 

   return l_r;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_week_day2ct_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare
  l_r asd.week_day2ct%rowtype;
begin
  l_r := asd.jf_week_day2ct_pkg$attr_to_rowtype(p_attr);

  open p_rows for
  select w.calculation_task_id,
         w.week_day_id,
         wd.wd_full_name,
         wd.wd_short_name,
         case
           when ct.ct_status_id = 0 THEN 'P{A},E{OF_DELETE,OF_INSERT,OF_UPDATE,OF_DELETE,OF_INSERT_ALL}'-- новый
           ELSE ''
         END AS "ROW$POLICY"
  from asd.week_day2ct w
       join asd.calculation_task ct on ct.calculation_task_id = w.calculation_task_id
       join core.week_day wd on wd.week_day_id = w.week_day_id
  where w.calculation_task_id = l_r.calculation_task_id;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_week_day2ct_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
  l_r asd.week_day2ct%rowtype;
 l_res boolean;

begin
  l_r := asd.jf_week_day2ct_pkg$attr_to_rowtype(p_attr);
  l_res:= asd.jf_ct_graph_section_speed_pkg$of_check_calc_err(l_r.calculation_task_id);
  
  delete
  from asd.week_day2ct
  where calculation_task_id = l_r.calculation_task_id and
        week_day_id = l_r.week_day_id;

  return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_week_day2ct_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
  l_r asd.week_day2ct%rowtype;
  l_res boolean;
 
begin

  l_r := asd.jf_week_day2ct_pkg$attr_to_rowtype(p_attr);
  l_res:= asd.jf_ct_graph_section_speed_pkg$of_check_calc_err(l_r.calculation_task_id);
 
  insert into asd.week_day2ct
  select l_r.*;

  return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_week_day2ct_pkg$of_insert_all"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
  l_r asd.week_day2ct%rowtype;
  l_res boolean;

  l_weekdays INTEGER[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_week_day_id', true);
begin
  l_r := asd.jf_week_day2ct_pkg$attr_to_rowtype(p_attr);
  l_res:= asd.jf_ct_graph_section_speed_pkg$of_check_calc_err(l_r.calculation_task_id);

  insert into asd.week_day2ct
  select l_r.calculation_task_id,
         wd.week_day_id
  from core.week_day wd
  where not exists (
                     select 1
                     from asd.week_day2ct wd2ct
                     where wd.week_day_id = wd2ct.week_day_id and
                           wd2ct.calculation_task_id = l_r.calculation_task_id
        )
  AND wd.week_day_id = ANY(l_weekdays);

  return null;
end;
$function$
;