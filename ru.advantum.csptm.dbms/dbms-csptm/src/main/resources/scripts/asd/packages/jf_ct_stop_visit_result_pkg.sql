------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_stop_visit_result_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare
  l_ids TEXT[] :=jofl.jofl_pkg$extract_tarray(p_attr, 'F_ids', true);
  l_avg_speed NUMERIC[] :=jofl.jofl_pkg$extract_narray(p_attr, 'F_avg_speed', true);
begin
  open p_rows for
  WITH t1 AS (select
    c.ct_stop_visit_result_id,
    c.calculation_task_id,
    s_1.name stop_1_name,
    s_2.name stop_2_name,
    sir_1.order_num as  order_num1,
    sir_2.order_num as  order_num2,
    c.norm_duration as norm_duration,
    c.avg_speed,
    si_1.stop_item_id||'-'||si_2.stop_item_id as ids
  from asd.ct_stop_visit_result c
    join rts.stop_item2round sir_1 on sir_1.stop_item2round_id = c.stop_item2round_1_id
    join rts.stop_item2round sir_2 on sir_2.stop_item2round_id = c.stop_item2round_2_id
    join rts.stop_item si_1 on si_1.stop_item_id = sir_1.stop_item_id
    join rts.stop_item si_2 on si_2.stop_item_id = sir_2.stop_item_id
    join rts.stop_location sl_1 on sl_1.stop_location_id = si_1.stop_location_id
    join rts.stop_location sl_2 on sl_2.stop_location_id = si_2.stop_location_id
    join rts.stop s_1 on s_1.stop_id = sl_1.stop_id
    join rts.stop s_2 on s_2.stop_id = sl_2.stop_id
  where c.ct_round_stop_result_id= jofl.jofl_pkg$extract_number(p_attr, 'ct_round_stop_result_id', true)
  AND (c.avg_speed = ANY(l_avg_speed) OR array_length(l_avg_speed,1) IS NULL)
  order by sir_1.order_num)
  SELECT * from t1 where (t1.ids = ANY(l_ids) OR array_length(l_ids,1) IS NULL) ;
end;
$function$
;
