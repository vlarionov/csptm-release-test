CREATE OR REPLACE FUNCTION asd.rep_asd_pkg$visual_review_rpt (
  p_id_account text,
  f_review_id text,
  p_timezone text,
  f_garage_num text = NULL::text,
  f_begin text = NULL::text,
  f_end text = NULL::text
)
RETURNS TABLE (
  lt_rtk_name text,
  lt_ag_name text,
  lt_r_number text,
  ld_rv_date text,
  lt_st_name text,
  ld_time_arvl_fact text,
  li_stop_dur text,
  li_delay_dur text,
  lt_delay_res text,
  lt_fio text,
  lt_rr_type text,
  ln_stop_sequence integer,
  lt_garage_num text,
  lt_run_duration text,
  ld_begin text,
  ld_end text,
  lt_garage_num_list text
) AS
$body$
    declare
    ln_review_id 	numeric   := f_review_id::numeric;
    ldt_begin   	timestamp := f_begin::timestamp;
    ldt_end			timestamp := f_end::timestamp;
    lt_garage_num 	text[]	  := string_to_array(ltrim( rtrim(f_garage_num, ']'), '['), ',');
    ln_garage_num   integer[] := lt_garage_num::integer[];
    begin

	p_timezone := 0;
	
      return query
	  select 
		rtk.name,
		ag.name,
		vrr.route_name,
		to_char(vrr.review_date::timestamp, 'dd.mm.yyyy'),
		vrd.stop_name,
		(vrd.time_arrival_fact + (0/*p_timezone*/ || ' hour')::interval)::text,
		vrd.stop_duration::text,
		vrd.delay_duration::text,
		vrd.delay_reason,
        coalesce(ac.last_name, '')||' '||coalesce(ac.first_name, '')||' '||coalesce(ac.middle_name, ''),
        vrr.route_round_type_name,
        vrd.stop_sequence,
        tr.garage_num::text,
        ((vrd.time_arrival_fact + (0/*p_timezone*/ || ' hour')::interval) -
        lag(vrd.time_arrival_fact + (0/*p_timezone*/ || ' hour')::interval, 1, vrd.time_arrival_fact + (0/*p_timezone*/ || ' hour')::interval)
        over (partition by tr.tr_id order by vrd.stop_sequence))
        ::text,
        coalesce(ldt_begin::time::interval::text, ''),
        coalesce(ldt_end::time::interval::text, ''),
        array_to_string(lt_garage_num, ', ')
	from asd.visual_review_report vrr
    join asd.visual_review_item vri on vri.review_id = vrr.review_id
    join asd.visual_review_detail vrd on vrd.visual_review_item_id = vri.visual_review_item_id
	join gis.routes r on r.muid = vrr.route_id
	join gis.ref_transport_kinds rtk on rtk.muid = r.transport_kind_muid
	join gis.agencies ag on ag.muid = r.agency_muid
    join core.account ac on ac.account_id = P_ID_ACCOUNT::numeric
    join core.tr tr on tr.tr_id = vri.tr_id
       where vrr.review_id = ln_review_id
	 --and (tr.garage_num::text = any(lt_garage_num) or array_length(lt_garage_num, 1) is null) 
     and (tr.garage_num = any(ln_garage_num) or array_length(ln_garage_num, 1) is null)   
     and (vrd.time_arrival_fact >= ldt_begin::time::interval or ldt_begin is null)
     and (vrd.time_arrival_fact <= ldt_end::time::interval or ldt_end is null)
    order by min(vrd.time_arrival_fact) over (partition by vrd.visual_review_item_id)
			,vrd.visual_review_item_id
    		,vrd.stop_sequence;

   end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;
-------------------
CREATE OR REPLACE FUNCTION asd.rep_asd_pkg$visual_review (
  p_id_account text,
  f_review_id text
)
RETURNS TABLE (
  lt_rtk_name text,
  lt_ag_name text,
  lt_r_number text,
  ld_rv_date text,
  lt_st_name text,
  ld_time_arvl_fact text,
  li_stop_dur text,
  li_delay_dur text,
  lt_delay_res text,
  lt_fio text,
  lt_rr_type text,
  lt_stop_sequence bigint
) AS
$body$
    declare
    ln_review_id numeric := f_review_id::numeric;
    begin


      return query
       select rtk.name,
		ag.name,
		vrr.route_name,
		to_char(vrr.review_date::timestamp, 'dd.mm.yyyy'),
        s.name as stop_name,
        null::text, /*vrd.time_arrival_fact::text,*/
        null::text,/*vrd.stop_duration::text,*/
		null::text,/*vrd.delay_duration::text,*/
		null::text, /*vrd.delay_reason*/
        coalesce(ac.last_name, '')||' '||coalesce(ac.first_name, '')||' '||coalesce(ac.middle_name, ''),
        vrr.route_round_type_name,
        spr.order_num as stop_sequence
	from asd.visual_review_report vrr
	join gis.routes r on r.muid = vrr.route_id
	join gis.ref_transport_kinds rtk on rtk.muid = r.transport_kind_muid
	join gis.agencies ag on ag.muid = r.agency_muid
    join core.account ac on ac.account_id = P_ID_ACCOUNT::numeric
    join gis.route_rounds rr on r.current_route_variant_muid = rr.route_variant_muid
    						and rr.route_round_type_muid = vrr.route_round_type
    join gis.route_trajectories rt on rt.route_round_muid = rr.muid
    join gis.stop_place2route_traj spr on spr.route_trajectory_muid = rt.muid
    join gis.stop_places sp on sp.muid = spr.stop_place_muid
    join gis.stops s on s.muid = sp.stop_muid 
       where 1 = 1
	 and vrr.review_id = ln_review_id
    order by spr.order_num;

   end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100 ROWS 1000;
-------------------
create or replace function asd.rep_asd_pkg$route_duration_tr(
    p_id_account text,
    f_rtk_muid   text,
    f_prk_muid   text,
    f_dt         text,
    f_r_muid     text,
    f_garage_num text,
    p_timezone   text
)
    returns table(
        lt_rtk_name       text,
        lt_prk_name       text,
        lt_r_number       text,
        lt_st_name        text,
        lt_tt_entry_num   text,
        lt_r_type         text,
        lt_time_arvl_fact text,
        lt_time_way_fact  text,
        lt_fio            text,
        lt_dt             text
    ) as
$body$
declare

begin

    return query
    select
        rtk.name,
        p.name,
        r.number,
        s.name,
        tte.timetable_entry_num :: text,
        rnd.round_code,
        to_char(orop.time_fact + (p_timezone || ' hour') :: interval, 'dd.mm.yyyy hh24:mi:ss'),
        to_char(orop.time_fact + (p_timezone || ' hour') :: interval -
                case when lag(ornd.order_round_id)
                          over (
                              order by orlst.order_list_id, tte.timetable_entry_id, rnd.round_id, ornd.order_round_id, tr.tr_id, orop.stop_order_num )
                          = ornd.order_round_id
                          and lag(tr.tr_id)
                              over (
                                  order by orlst.order_list_id, tte.timetable_entry_id, rnd.round_id, ornd.order_round_id, orop.stop_order_num ) =
                              tr.tr_id
                    then lag(orop.time_fact + (p_timezone || ' hour') :: interval)
                    over (
                        order by orlst.order_list_id, tte.timetable_entry_id, rnd.round_id, ornd.order_round_id, tr.tr_id, orop.stop_order_num )
                else null
                end
        , 'hh24:mi:ss'),
        coalesce(ac.last_name, '') || ' ' || coalesce(ac.first_name, '') || ' ' || coalesce(ac.middle_name, '') as fio,
        to_char(to_timestamp(f_dt, 'yyyy-mm-dd hh24:mi:ss') + (p_timezone || ' hour') :: interval, 'dd.mm.yyyy')
    from tt.order_fact orop
        join tt.order_round ornd on (orop.order_round_id = ornd.order_round_id)
        join gis.stop_places sp on (sp.muid = orop.stop_place_muid)
        join gis.stops s on (s.muid = sp.stop_muid)
        join tt.order_list orlst on (orlst.order_list_id = ornd.order_list_id)
        join core.tr tr on (tr.tr_id = orlst.tr_id)
        join tt.timetable_entry tte on (tte.timetable_entry_id = orlst.timetable_entry_id)
        join tt.round rnd on (rnd.timetable_entry_id = tte.timetable_entry_id and rnd.round_id = ornd.round_id)
        join gis.route_trajectories rt on (rnd.route_trajectory_muid = rt.muid)
        join gis.route_rounds rr on (rr.muid = rt.route_round_muid)
        join gis.route_variants rv on (rv.muid = rr.route_variant_muid)
        join gis.routes r on (r.muid = rv.route_muid and r.current_route_variant_muid = rv.muid)
        --join gis.agencies ag on (ag.muid = r.agency_muid)
        join gis.ref_transport_kinds rtk on rtk.muid = r.transport_kind_muid
        join core.account ac on ac.account_id = p_id_account :: bigint
        join gis.parks2routes p2r on p2r.route_muid = r.muid
        join gis.parks p on p.muid = p2r.park_muid
    where 1 = 1
          and rtk.muid = f_rtk_muid :: bigint /*'Автобус'*/
          and p.muid = f_prk_muid :: bigint
          /*and ag.muid = f_ag_muid::bigint*/ /*'ГУП "Мосгортранс"'*/
          and date_trunc('day', orlst.order_date) = to_timestamp(f_dt, 'yyyy-mm-dd hh24:mi:ss') + (p_timezone || ' hour') :: interval
          /*'2017-02-13 00:00:00.0000'*/
          and r.muid = f_r_muid :: bigint /*'Н5'*/
          and tr.garage_num :: text = f_garage_num /*'160235'*/
    ;

end;
$body$
language 'plpgsql'
volatile
called on null input
security invoker
cost 100 rows 1000;
-------------------
create or replace function asd.rep_asd_pkg$route_duration(
    p_id_account text,
    f_rtk_muid   text,
    f_prk_muid   text,
    f_dt         text,
    f_r_muid     text,
    p_timezone   text
)
    returns table(
        lt_rtk_name       text,
        lt_prk_name       text,
        lt_r_number       text,
        lt_st_name        text,
        lt_tt_entry_num   text,
        lt_r_type         text,
        lt_time_arvl_fact text,
        lt_time_way_fact  text,
        lt_fio            text,
        lt_dt             text,
        lt_direction      text
    ) as
$body$
declare

begin

    return query
    select
        rtk.name,
        p.name,
        r.number,
        s.name,
        tte.timetable_entry_num :: text,
        rnd.round_code,
        to_char(orop.time_fact + (p_timezone || ' hour') :: interval, 'dd.mm.yyyy hh24:mi:ss'),
        to_char(orop.time_fact + (p_timezone || ' hour') :: interval -
                case when lag(ornd.order_round_id)
                          over (
                              order by orlst.order_list_id, tte.timetable_entry_id, rnd.round_id, ornd.order_round_id, tr.tr_id, orop.stop_order_num )
                          = ornd.order_round_id
                          and lag(tr.tr_id)
                              over (
                                  order by orlst.order_list_id, tte.timetable_entry_id, rnd.round_id, ornd.order_round_id, orop.stop_order_num ) =
                              tr.tr_id
                    then lag(orop.time_fact + (p_timezone || ' hour') :: interval)
                    over (
                        order by orlst.order_list_id, tte.timetable_entry_id, rnd.round_id, ornd.order_round_id, tr.tr_id, orop.stop_order_num )
                else null
                end
        , 'hh24:mi:ss'),
        coalesce(ac.last_name, '') || ' ' || coalesce(ac.first_name, '') || ' ' || coalesce(ac.middle_name, '') as fio,
        to_char(to_timestamp(f_dt, 'yyyy-mm-dd hh24:mi:ss') + (p_timezone || ' hour') :: interval, 'dd.mm.yyyy'),
        rrtt.name
    from tt.order_fact orop
        join tt.order_round ornd on (orop.order_round_id = ornd.order_round_id)
        join gis.stop_places sp on (sp.muid = orop.stop_place_muid)
        join gis.stops s on (s.muid = sp.stop_muid)
        join tt.order_list orlst on (orlst.order_list_id = ornd.order_list_id)
        join core.tr tr on (tr.tr_id = orlst.tr_id)
        join tt.timetable_entry tte on (tte.timetable_entry_id = orlst.timetable_entry_id)
        join tt.round rnd on (rnd.timetable_entry_id = tte.timetable_entry_id and rnd.round_id = ornd.round_id)
        join gis.route_trajectories rt on (rnd.route_trajectory_muid = rt.muid)
        join gis.route_rounds rr on (rr.muid = rt.route_round_muid)
        join gis.route_variants rv on (rv.muid = rr.route_variant_muid)
        join gis.routes r on (r.muid = rv.route_muid and r.current_route_variant_muid = rv.muid)
        --join gis.agencies ag on (ag.muid = r.agency_muid)
        join gis.ref_transport_kinds rtk on rtk.muid = r.transport_kind_muid
        join gis.ref_route_trajectory_types rrtt on rt.trajectory_type_muid = rrtt.muid
        join core.account ac on ac.account_id = p_id_account :: bigint
        join gis.parks2routes p2r on p2r.route_muid = r.muid
        join gis.parks p on p.muid = p2r.park_muid
    where 1 = 1
          and rtk.muid = f_rtk_muid :: bigint /*'Автобус'*/
          and p.muid = f_prk_muid :: bigint
          /*and ag.muid = f_ag_muid::bigint*/ /*'ГУП "Мосгортранс"'*/
          and date_trunc('day', orlst.order_date) = to_timestamp(f_dt, 'yyyy-mm-dd hh24:mi:ss') + (p_timezone || ' hour') :: interval
          /*'2017-02-13 00:00:00.0000'*/
          and r.muid = f_r_muid :: bigint /*'Н5'*/
          and orop.time_fact notnull;

end;
$body$
language 'plpgsql'
volatile
called on null input
security invoker
cost 100 rows 1000;
-------------------------------------
create or replace function asd.rep_asd_pkg$speed_on_route_period(
    p_id_account        text,
    p_depo_id           text,
    p_dt                text,
    p_hh_b              text,
    p_hh_e              text,
    p_route_id          text,
    p_stop_num_b        text,
    p_stop_num_e        text,
    p_move_direction_id text,
    p_timezone          text
)
    returns table(
        lt_rtk_name   text,
        lt_prk_name   text,
        lt_dt         text,
        lt_period     text,
        lt_r_number   text,
        lt_st_segment text,
        lt_direction  text,
        lt_fio        text,
        lt_rr_code    text,
        lt_stord_num  text,
        lt_st_name    text,
        lt_capacity   text,
        lt_speed      text
    ) as
$body$
declare
    l_action_type_id smallint;
    l_ts             timestamp :=to_timestamp(p_dt, 'yyyy-mm-dd hh24:mi:ss') + (p_timezone || ' hour') :: interval;
    l_date           date :=l_ts :: date;
begin
    select case when p_move_direction_id = 1 :: text
        then 37 :: smallint
           else 38 :: smallint end
    into l_action_type_id;
    return query
    with stop_item_num as (
        select
            tv.tt_variant_id,
            si2r.order_num,
            si2r.stop_item_id,
            r.round_num,
            route.route_num,
            r.move_direction_id,
            md.move_direction_name,
            tv.order_date,
            rv.route_variant_id,
            si2r.stop_item2round_id,
            si2r.length_sector,
            r.code,
            rs.name,
            ctt.name                           tr_name,
            ce.name_full,
            first_value(rs.name)
            over (
                partition by route.route_num
                order by si2r.order_num )      start_stop_name,
            first_value(rs.name)
            over (
                partition by route.route_num
                order by si2r.order_num desc ) end_stop_name
        from ttb.tt_variant tv
            join rts.route_variant rv on tv.route_variant_id = rv.route_variant_id
            join rts.round r on rv.route_variant_id = r.route_variant_id
            join rts.move_direction md on r.move_direction_id = md.move_direction_id
            join rts.stop_item2round si2r on r.round_id = si2r.round_id
            join rts.stop_item si on si2r.stop_item_id = si.stop_item_id
            join rts.stop_location sl on si.stop_location_id = sl.stop_location_id
            join rts.stop rs on sl.stop_id = rs.stop_id
            join rts.route route on rv.route_variant_id = route.current_route_variant_id
            join core.tr_type ctt on route.tr_type_id = ctt.tr_type_id
            join rts.depo2route d2r on route.route_id = d2r.route_id
            join core.depo cd on d2r.depo_id = cd.depo_id
            join core.entity ce on cd.depo_id = ce.entity_id

        where 1 = 1
              and tv.parent_tt_variant_id notnull
              and r.move_direction_id = p_move_direction_id :: smallint
              and cd.depo_id = p_depo_id :: bigint
              and route.route_id = p_route_id :: integer
              and tv.order_date = l_date
              and r.code = '00'
        order by order_num
    )
        , time_between_stop as (
        select
            sin.order_num,
            sin.stop_item_id,
            tto.tt_out_id,
            tai.time_begin :: time                                                                                       time_stop_end,
            lag(tai.time_begin)
            over (
                partition by tta.tt_action_id
                order by tto.tt_out_id, tai.time_begin, sin.round_num, sin.move_direction_id, sin.order_num ) :: time as time_stop_begin,
            sin.round_num,
            sin.order_date,
            sin.stop_item2round_id,
            ctc.short_name
        from stop_item_num sin
            join ttb.tt_out tto on sin.tt_variant_id = tto.tt_variant_id
            join core.tr_capacity ctc on tto.tr_capacity_id = ctc.tr_capacity_id

            join ttb.tt_action tta on tto.tt_out_id = tta.tt_out_id
            join ttb.tt_action_item tai on tta.tt_action_id = tai.tt_action_id and sin.stop_item2round_id = tai.stop_item2round_id
        where 1 = 1
              and tta.action_type_id = l_action_type_id
              and tto.parent_tt_out_id notnull
              and tai.time_begin :: time between p_hh_b :: integer * '1 h' :: interval and p_hh_e :: integer * '1 h' :: interval
        order by tto.tt_out_id, tai.time_begin, sin.round_num, sin.move_direction_id, sin.order_num
    )
        , avg_time as (select
                           tbs.order_num,
                           tbs.short_name,
                           tbs.stop_item_id,
                           tbs.stop_item2round_id,
                           avg(extract(epoch from (tbs.time_stop_end - tbs.time_stop_begin))) avg_time
                       from time_between_stop tbs
                       where tbs.order_num between p_stop_num_b :: integer and p_stop_num_e :: integer
                             or tbs.order_num between p_stop_num_e :: integer and p_stop_num_b :: integer
                       group by tbs.order_num, tbs.stop_item_id, tbs.stop_item2round_id, tbs.short_name
                       order by tbs.order_num)
    select
        tr_name                                                                                                    tr_type_id,
        name_full                                                                                                  depo_name,
        to_char(l_ts, 'dd.mm.yyyy')                                                                             as dt,
        'С ' || p_hh_b || ' по ' || p_hh_e                                                                      as period,
        sin.route_num,
        'C ' || sin.start_stop_name || ' по ' || sin.end_stop_name                                              as st_segment,
        sin.move_direction_name,
        coalesce(ca.last_name, '') || ' ' || coalesce(ca.first_name, '') || ' ' || coalesce(ca.middle_name, '') as fio,
        sin.code,
        sin.order_num :: text,
        sin.name,
        a.short_name,
        case when round(sin.length_sector :: numeric * 3.6 :: numeric / a.avg_time :: numeric, 2) > 120
                  or (sin.length_sector :: numeric * 3.6 :: numeric / a.avg_time) isnull
            then 0 :: text
        else round(sin.length_sector :: numeric * 3.6 :: numeric / a.avg_time :: numeric, 2) :: text end
    from stop_item_num sin
        join core.account ca on ca.account_id :: text = p_id_account
        join avg_time a on a.stop_item_id = sin.stop_item_id
    order by sin.order_num;
end;
$body$
language 'plpgsql'
volatile
called on null input
security invoker
cost 100 rows 1000;