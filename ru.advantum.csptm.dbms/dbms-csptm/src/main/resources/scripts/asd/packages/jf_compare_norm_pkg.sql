CREATE OR REPLACE FUNCTION asd."jf_compare_norm_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare
  l_route_variant_id rts.route_variant.route_variant_id%type := jofl.jofl_pkg$extract_number(p_attr, 'route_variant_id', true);
begin
  open p_rows for
  select
    norm.norm_id,
    norm.calculation_task_id,
    norm.norm_name calc,
    norm.norm_desc as name,
    norm.is_current,
    norm.hour_period periods,
    norm.dt_begin date_from,
    norm.dt_end date_to,
    norm.sign_deleted,
    norm.sys_period,
    ct.name as calc_task_name,
    norm.update_date,
    is_active,
    (select string_agg(r.route_num, ', ')
     from asd.route_variant2ct r2ct
       join asd.calculation_task ct on ct.calculation_task_id = r2ct.calculation_task_id
       join rts.route_variant rv on rv.route_variant_id = r2ct.route_variant_id
       join rts.route r on r.route_id = rv.route_id
       join ttb.norm n ON n.calculation_task_id = ct.calculation_task_id
     WHERE norm_id = norm.norm_id) as numbers
  from ttb.norm norm
    left join asd.calculation_task ct on ct.calculation_task_id = norm.calculation_task_id
  where
    l_route_variant_id is null or
    exists (select 1
            from asd.route_variant2ct rvct
            where rvct.route_variant_id= l_route_variant_id
                  and rvct.calculation_task_id= norm.calculation_task_id)
  ;


end;
$function$
;