CREATE OR REPLACE FUNCTION asd."jf_directions_type_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare 
begin 
 open p_rows for 
      select t.muid direction_id,
	  t.name direction_name        
      from gis.ref_route_trajectory_types t; 
end;
$function$
;