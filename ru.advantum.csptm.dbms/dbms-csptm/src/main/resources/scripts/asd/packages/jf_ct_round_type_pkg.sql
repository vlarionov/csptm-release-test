CREATE OR REPLACE FUNCTION asd."jf_ct_round_type_pkg$attr_to_rowtype"(p_attr text)
 RETURNS asd.ct_round_type
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.ct_round_type%rowtype; 
begin 
   l_r.ct_round_type_name := jofl.jofl_pkg$extract_varchar(p_attr, 'ct_round_type_name', true); 
   l_r.ct_round_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'ct_round_type_id', true); 

   return l_r;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_round_type_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$ 
declare 
begin 
 open p_rows for 
      select 
        ct_round_type_name, 
        ct_round_type_id
      from asd.ct_round_type; 
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_round_type_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.ct_round_type%rowtype;
begin 
   l_r := asd.jf_ct_round_type_pkg$attr_to_rowtype(p_attr);

  /* update asd.ct_round_type set 
          ct_round_type_name = l_r.ct_round_type_name
   where 
          ct_round_type_id = l_r.ct_round_type_id;
*/
   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_round_type_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.ct_round_type%rowtype;
begin 
   l_r := asd.jf_ct_round_type_pkg$attr_to_rowtype(p_attr);

   --delete from  asd.ct_round_type where  ct_round_type_id = l_r.ct_round_type_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_round_type_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.ct_round_type%rowtype;
begin 
   l_r := asd.jf_ct_round_type_pkg$attr_to_rowtype(p_attr);

  -- insert into asd.ct_round_type select l_r.*;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_round_type_pkg$cn_round_type_techno"()
  RETURNS smallint AS
'select 0::smallint'
LANGUAGE sql VOLATILE
COST 100;
ALTER FUNCTION asd."jf_ct_round_type_pkg$cn_round_type_techno"()
OWNER TO adv;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_round_type_pkg$cn_round_type_prod"()
  RETURNS smallint AS
'select 1::smallint'
LANGUAGE sql VOLATILE
COST 100;
ALTER FUNCTION asd."jf_ct_round_type_pkg$cn_round_type_techno"()
OWNER TO adv;
