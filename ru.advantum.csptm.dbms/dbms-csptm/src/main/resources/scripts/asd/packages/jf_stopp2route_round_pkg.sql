CREATE OR REPLACE FUNCTION asd."jf_stopp2route_round_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare

begin
  open p_rows for
  select  spr.order_num sequence_num,
  sp.muid::text stopp_id,
         s.name stopp_name
  from gis.routes r
       join gis.route_rounds rr on rr.route_variant_muid = r.current_route_variant_muid
       join gis.route_trajectories rt on rt.route_round_muid = rr.muid
       join gis.stop_place2route_traj spr on spr.route_trajectory_muid = rt.muid
       join gis.stop_places sp on sp.muid = spr.stop_place_muid
       join gis.stops s on s.muid = sp.stop_muid
  where r.muid::text = jofl.jofl_pkg$extract_varchar(p_attr, 'route_muid', true) and
        rt.trajectory_type_muid = jofl.jofl_pkg$extract_number(p_attr, 'trajectory_type_id', true) and
        (coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'stop_place_1_muid', true), '') = '' or
        spr.order_num >
        (
          select sprt.order_num
          from gis.stop_place2route_traj sprt
          where sprt.route_trajectory_muid = rt.muid and
                sprt.stop_place_muid::text = jofl.jofl_pkg$extract_varchar(p_attr, 'stop_place_1_muid', true)
        )) and
		(coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'stop_place_2_muid', true), '') = '' or
        spr.order_num <
        (
          select sprt.order_num
          from gis.stop_place2route_traj sprt
          where sprt.route_trajectory_muid = rt.muid and
                sprt.stop_place_muid::text = jofl.jofl_pkg$extract_varchar(p_attr, 'stop_place_2_muid', true)
        ))
		
		
		and rr.route_round_type_muid=1
		order by spr.order_num;
		

end;
$function$
;