CREATE OR REPLACE FUNCTION asd."jf_visual_review_report_pkg$attr_to_rowtype"(p_attr text)
 RETURNS asd.visual_review_report
 LANGUAGE plpgsql
AS $function$
declare
 l_r asd.visual_review_report%rowtype;
begin
  l_r.review_id := jofl.jofl_pkg$extract_number(p_attr, 'review_id', true);
  l_r.s_name := jofl.jofl_pkg$extract_varchar(p_attr, 's_name', true);
  l_r.insert_date := jofl.jofl_pkg$extract_date(p_attr, 'insert_date', true);
  l_r.review_date := jofl.jofl_pkg$extract_date(p_attr, 'review_date', true) + INTERVAL '3 hours';
  l_r.route_id := (jofl.jofl_pkg$extract_varchar(p_attr, 'route_id', true))::BIGINT;
  l_r.route_round_type := jofl.jofl_pkg$extract_number(p_attr,
    'route_round_type', true);
  -- l_r.shift := jofl.jofl_pkg$extract_number(p_attr, 'shift', true);
  l_r.status := jofl.jofl_pkg$extract_number(p_attr, 'status', true);
  l_r.route_name := jofl.jofl_pkg$extract_varchar(p_attr, 'route_name', true);
  l_r.route_round_type_name := jofl.jofl_pkg$extract_varchar(p_attr,
    'route_round_type_name', true);
 -- l_r.timetable_entry_num  := jofl.jofl_pkg$extract_number(p_attr, 'timetable_entry_num', true); 
    

  return l_r;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_visual_review_report_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare
F_REVIEW_DT date := jofl.jofl_pkg$extract_date(p_attr, 'F_REVIEW_DT', true);
F_NUMBER gis.routes.number%type := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_NUMBER_NUMBER', true), '');
F_STATUS integer := jofl.jofl_pkg$extract_number(p_attr, 'F_STATUS_INPLACE_K', true);
F_NAME text := '%'||upper(coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'F_NAME_TEXT', true), ''))||'%';
begin
  open p_rows for
  select r.review_id,
         r.s_name,
         r.insert_date,
         r.review_date,
         r.route_id::text,
         r.route_name,
         r.route_round_type,
         r.route_round_type_name,
         r.status,
         case
           when r.status = 0 THEN 'P{A},E{OF_DELETE,OF_UPDATE,OF_BEGIN_REVIEW},P{D},D{asd.visual_review_item}'
           when r.status = 1 THEN 'P{A},E{OF_DELETE,OF_FINISH},P{D},D{asd.visual_review_item}'
           when r.status = 2 THEN 'P{D},D{asd.visual_review_item2}' -- завершено - нельзя добавлять
           ELSE ''
         END AS "ROW$POLICY"
  from asd.visual_review_report r
 where (date_trunc('day', r.review_date) = F_REVIEW_DT + 1 OR F_REVIEW_DT IS NULL)
   and (F_NUMBER = r.route_name or F_NUMBER = '')
   and (F_STATUS IS NULL or r.status = F_STATUS)
   and (F_NAME ='%%' OR upper(r.s_name) LIKE F_NAME)
 ;




end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_visual_review_report_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare

  l_r asd.visual_review_report%rowtype;
  l_status asd.visual_review_report.status%type;
  l_flag integer;
begin

  l_r := asd.jf_visual_review_report_pkg$attr_to_rowtype(p_attr);
  select v.status
  into l_status
  from asd.visual_review_report v
  where v.review_id = l_r.review_id;

  if l_status!=0 then
    return null;
  end if;
  select distinct 1
  into strict l_flag
  from gis.routes r
       join gis.route_rounds rr on r.current_route_variant_muid =
         rr.route_variant_muid
       join tt.timetable tt on tt.route_muid = r.muid
       join tt.timetable_entry te on te.timetable_id = tt.timetable_id
       join gis.ref_route_round_types rrrt on rrrt.muid =
         rr.route_round_type_muid
  where r.muid = l_r.route_id and
        l_r.review_date between tt.begin_date and
        coalesce(tt.end_date, now() + interval '100 year') and
        l_r.route_round_type = rrrt.muid;
  update asd.visual_review_report
  set s_name = l_r.s_name,
      review_date = l_r.review_date,
      route_id = l_r.route_id,
      route_name = l_r.route_name,
      route_round_type = l_r.route_round_type,
      route_round_type_name = l_r.route_round_type_name
  where review_id = l_r.review_id;

  delete
  from asd.visual_review_detail
  where visual_review_item_id in (select visual_review_item_id from asd.visual_review_item where review_id = l_r.review_id);

  insert into asd.visual_review_detail(visual_review_item_id, stop_id, stop_name,
    stop_sequence)
  select visual_review_item_id,
	t.muid,
	t.name,
	t.rn
	from(select l_r.review_id,
		 t.muid,
		 t.name,
		 row_number() over() rn
	  from (
		 select sp.muid,
			s.name
		 from gis.routes r
		      join gis.route_rounds rr on r.current_route_variant_muid =
			rr.route_variant_muid
		      join gis.route_trajectories rt on rt.route_round_muid = rr.muid
		      join gis.stop_place2route_traj spr on spr.route_trajectory_muid =
			rt.muid
		      join gis.stop_places sp on sp.muid = spr.stop_place_muid
		      join gis.stops s on s.muid = sp.stop_muid
		 where r.muid = l_r.route_id and
			rr.route_round_type_muid = l_r.route_round_type
		 order by rt.trajectory_type_muid,
			  spr.order_num
	       ) t)t,
	       asd.visual_review_item  ari
	       where ari.review_id = t.review_id
	       order by visual_review_item_id,rn;

  return null;

  exception when no_data_found then
  raise  exception '<<Параметры обследования не соответсвуют друг другу!>>';

end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_visual_review_report_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare

  l_r asd.visual_review_report%rowtype;
  l_status asd.visual_review_report.status%type;
begin

  l_r := asd.jf_visual_review_report_pkg$attr_to_rowtype(p_attr);
  select v.status
  into l_status
  from asd.visual_review_report v
  where v.review_id = l_r.review_id;

  if l_status=2 then
    return null;
  end if;
  delete
  from asd.visual_review_detail
  where visual_review_item_id in (select visual_review_item_id from asd.visual_review_item where review_id = l_r.review_id);
  delete
  from asd.visual_review_item
  where review_id = l_r.review_id;
  delete
  from asd.visual_review_report
  where review_id = l_r.review_id;

  return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_visual_review_report_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare

  l_r asd.visual_review_report%rowtype;
  l_review_id asd.visual_review_report.review_id%type;
begin
  l_r := asd.jf_visual_review_report_pkg$attr_to_rowtype(p_attr);
  insert into asd.visual_review_report(review_id, s_name, insert_date,
    review_date, route_id, route_name, route_round_type, route_round_type_name,
    status)
  values (nextval('asd.visual_review_report_review_id_seq'), l_r.s_name, now(),
    l_r.review_date, l_r.route_id, l_r.route_name, l_r.route_round_type,
    l_r.route_round_type_name, 0) returning review_id
  into l_review_id;
  /*insert into asd.visual_review_detail(review_id, stop_id, stop_name,
    stop_sequence)
  select l_review_id,
         t.muid,
         t.name,
         row_number() over()
  from (
         select sp.muid,
                s.name
         from gis.routes r
              join gis.route_rounds rr on r.current_route_variant_muid =
                rr.route_variant_muid
              join gis.route_trajectories rt on rt.route_round_muid = rr.muid
              join gis.stop_place2route_traj spr on spr.route_trajectory_muid =
                rt.muid
              join gis.stop_places sp on sp.muid = spr.stop_place_muid
              join gis.stops s on s.muid = sp.stop_muid
         where r.muid = l_r.route_id and
               rr.route_round_type_muid = l_r.route_round_type
         order by rt.trajectory_type_muid,
                  spr.order_num
       ) t;*/

  /* insert into asd.visual_review_detail(review_id, stop_id, stop_name,
    time_arrival_plan,stop_sequence)
  select l_review_id,
         t.muid,
         t.name,
         t.time_begin,row_number() over()
  from (
         select sp.muid,
                tp.time_begin,
                s.name,
                rank() over(partition by sp.stop_muid
         order by tp.time_begin) rnk
         from gis.routes r
              join gis.route_rounds rr on r.current_route_variant_muid =
                rr.route_variant_muid
              join tt.timetable tt on tt.route_muid = r.muid
              join tt.timetable_entry te on te.timetable_id = tt.timetable_id
              join tt.round tr on tr.timetable_entry_id = te.timetable_entry_id
              join tt.timetable_plan tp on tp.round_id = tr.round_id
              join gis.stop_places sp on sp.erm_id = tp.stop_erm_id
              join gis.stops s on s.muid = sp.stop_muid
         where r.muid= l_r.route_id
              and te.timetable_entry_num =  l_r.timetable_entry_num and
              tp.shift = l_r.shift
         order by tp.time_begin
       ) t
  where t.rnk = 1;*/

  return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_visual_review_report_pkg$of_finish"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
  l_r asd.visual_review_report%rowtype;
 l_status  asd.visual_review_report.status%type;
begin
 l_r := asd.jf_visual_review_report_pkg$attr_to_rowtype(p_attr);
  
select v.status
into l_status
from asd.visual_review_report v
where v.review_id=l_r.review_id;

if l_status!=1 then 
return null;
end if;
 
  update asd.visual_review_report 
set status=2
where review_id=l_r.review_id;

  return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_visual_review_report_pkg$of_begin_review"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
  l_r asd.visual_review_report%rowtype;
 l_status  asd.visual_review_report.status%type;
begin
 l_r := asd.jf_visual_review_report_pkg$attr_to_rowtype(p_attr);
  
select v.status
into l_status
from asd.visual_review_report v
where v.review_id=l_r.review_id;

if l_status!=0 then 
return null;
end if;
 
  update asd.visual_review_report 
set status=1
where review_id=l_r.review_id;
  return null;
end;
$function$
;