CREATE OR REPLACE FUNCTION asd.get_constant(p_name text)
 RETURNS numeric IMMUTABLE
 LANGUAGE plpgsql
AS $function$
declare  
l_value text;
begin
  select max(c.value) into l_value from asd.constants c where c.name = p_name;
  return l_value;
  
  end;
$function$
;