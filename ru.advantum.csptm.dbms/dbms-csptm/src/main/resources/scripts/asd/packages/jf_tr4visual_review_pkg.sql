CREATE OR REPLACE FUNCTION asd.jf_tr4visual_review_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare 
  ln_review_id 	numeric   := jofl.jofl_pkg$extract_number(p_attr, 'f_review_id', true);
begin
	
 open p_rows for 
    select tr.garage_num::text
        from asd.visual_review_report vrr
        join asd.visual_review_item vri on vri.review_id = vrr.review_id
        join asd.visual_review_detail vrd on vrd.visual_review_item_id = vri.visual_review_item_id
        join gis.routes r on r.muid = vrr.route_id
        join gis.ref_transport_kinds rtk on rtk.muid = r.transport_kind_muid
        join gis.agencies ag on ag.muid = r.agency_muid
        join core.tr tr on tr.tr_id = vri.tr_id
       where vrr.review_id = ln_review_id
    group by tr.garage_num::text
    order by tr.garage_num::text;

   end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
