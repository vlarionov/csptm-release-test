﻿CREATE OR REPLACE FUNCTION asd."jf_ct_graph_section_speed_pkg$attr_to_rowtype"(p_attr text)
 RETURNS asd.jf_calc_speed_type
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r_return 		asd.jf_calc_speed_type;
   l_r_gss 			asd.ct_graph_section_speed%rowtype; 
   l_r_calc_task 	asd.calculation_task%rowtype;
   l_r_hour 		asd.hours2ct%rowtype;
   l_sec_b  		int:= jofl.jofl_pkg$extract_number(p_attr, 'sec_b', true);
   l_sec_e  		int:= jofl.jofl_pkg$extract_number(p_attr, 'sec_e', true);
begin 
   l_r_gss.calculation_task_id := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true); 
   l_r_gss.detail_week_day := jofl.jofl_pkg$extract_boolean(p_attr, 'detail_week_day', true); 
   l_r_gss.detail_hour := jofl.jofl_pkg$extract_boolean(p_attr, 'detail_hour', true); 
   l_r_gss.detail_transp_kind := jofl.jofl_pkg$extract_boolean(p_attr, 'detail_transp_kind', true); 
   l_r_gss.detail_route := jofl.jofl_pkg$extract_boolean(p_attr, 'detail_route', true);
   l_r_gss.calc_period_begin := jofl.jofl_pkg$extract_date(p_attr, 'calc_period_begin', true); 
   l_r_gss.calc_period_end := jofl.jofl_pkg$extract_date(p_attr, 'calc_period_end', true);

   l_r_calc_task.calculation_task_id := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true); 
   l_r_calc_task.account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', true); 
   l_r_calc_task.ct_status_id := jofl.jofl_pkg$extract_varchar(p_attr, 'ct_status_id', true); 
   l_r_calc_task.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 
   l_r_calc_task.time_create := jofl.jofl_pkg$extract_date(p_attr, 'time_create', true); 
   l_r_calc_task.time_calc_begin := jofl.jofl_pkg$extract_date(p_attr, 'time_calc_begin', true);
   l_r_calc_task.time_calc_end := jofl.jofl_pkg$extract_date(p_attr, 'time_calc_end', true); 
   l_r_calc_task.ct_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'ct_type_id', true); 
   l_r_calc_task.detail_capacity := jofl.jofl_pkg$extract_boolean(p_attr, 'detail_capacity', true);
   l_r_calc_task.hour_period := jofl.jofl_pkg$extract_number(p_attr, 'hour_period', true);

   l_r_hour.calculation_task_id := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true);
   --l_r_hour.hour_from := jofl.jofl_pkg$extract_varchar(p_attr, 'hour_from', true);
   --l_r_hour.hour_to := jofl.jofl_pkg$extract_varchar(p_attr, 'hour_to', true);
   l_r_hour.hour_from := (now()::date  + l_sec_b * interval '1 sec')::timestamp;
   l_r_hour.hour_to := (now()::date  + l_sec_e * interval '1 sec')::timestamp;
   
   l_r_return.r_gss := l_r_gss;
   l_r_return.r_calc_task := l_r_calc_task;
   l_r_return.r_hour := l_r_hour;
   
   return l_r_return;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_graph_section_speed_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare
   l_f_dt_begin date;
   l_f_dt_end date;
begin
   l_f_dt_begin:=jofl.jofl_pkg$extract_date(p_attr, 'F_BEGIN_DT', true) ;
   l_f_dt_end:=jofl.jofl_pkg$extract_date(p_attr, 'F_END_DT', true) ;
   open p_rows for
      select
        gss.calculation_task_id, 
        gss.detail_week_day, 
        gss.detail_hour, 
        gss.detail_transp_kind, 
        gss.detail_route,
        gss.calc_period_begin, 
        gss.calc_period_end,
        ctk.account_id,
        acc.last_name as account_name,
        ctk.ct_status_id, 
        st.name as ct_status_name,
        ctk.name, 
        ctk.time_create,
        h2ct.hour_from,
        h2ct.hour_to,
        ctk.hour_period,
        ctk.time_calc_begin, 
        ctk.time_calc_end, 
        ctk.ct_type_id, 
        tp.ct_type_name, 
        ctk.detail_capacity,
		extract(epoch from h2ct.hour_from::time) sec_b,
        extract(epoch from h2ct.hour_to::time) sec_e,
        CASE
        WHEN ctk.time_calc_begin is null THEN
           'P{A},D{OF_UPDATE, OF_DELETE}'
        ELSE
           NULL
        END
        AS "ROW$POLICY"
      from asd.ct_graph_section_speed gss
      join asd.calculation_task ctk on ctk.calculation_task_id = gss.calculation_task_id
      join asd.hours2ct h2ct on h2ct.calculation_task_id = ctk.calculation_task_id
      join asd.ct_type tp on tp.ct_type_id = ctk.ct_type_id 
      join asd.ct_status st on st.ct_status_id = ctk.ct_status_id
      join core.account acc on acc.account_id = ctk.account_id
     where (l_f_dt_begin is  null or l_f_dt_end is null)
        or date_trunc('day', ctk.time_create) between  l_f_dt_begin + integer '1' and l_f_dt_end + integer '1';
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_graph_section_speed_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r_return asd.jf_calc_speed_type;
   l_r_gss asd.ct_graph_section_speed%rowtype; 
   l_r_calc_task asd.calculation_task%rowtype;
   l_r_hour asd.hours2ct%rowtype;
   l_res text;
begin 
   l_r_return := asd.jf_ct_graph_section_speed_pkg$attr_to_rowtype(p_attr);

   l_r_gss:= l_r_return.r_gss ;
   l_r_calc_task:= l_r_return.r_calc_task ;
   l_r_hour:= l_r_return.r_hour ;

   l_res :=  asd.jf_calculation_task_pkg$of_update(p_id_account, row_to_json(l_r_calc_task)::text);

   delete from  asd.hours2ct where   calculation_task_id = l_r_gss.calculation_task_id;
   l_res :=  asd.jf_hours2ct_pkg$of_insert(p_id_account, row_to_json(l_r_hour)::text);

   update asd.ct_graph_section_speed set 
          detail_week_day = l_r_gss.detail_week_day, 
          detail_hour = l_r_gss.detail_hour, 
          detail_transp_kind = l_r_gss.detail_transp_kind, 
          detail_route = l_r_gss.detail_route,
          calc_period_begin = l_r_gss.calc_period_begin, 
          calc_period_end = l_r_gss.calc_period_end          
   where 
          calculation_task_id = l_r_gss.calculation_task_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_graph_section_speed_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r_return asd.jf_calc_speed_type;
   l_r_gss asd.ct_graph_section_speed%rowtype; 
   l_r_calc_task asd.calculation_task%rowtype;
   l_r_hour asd.hours2ct%rowtype;
   l_res text;
begin 
   l_r_return := asd.jf_ct_graph_section_speed_pkg$attr_to_rowtype(p_attr);

   l_r_gss:= l_r_return.r_gss ;
   l_r_calc_task:= l_r_return.r_calc_task ;
   l_r_hour:= l_r_return.r_hour ;

   l_res :=  asd.jf_hours2ct_pkg$of_delete(p_id_account, row_to_json(l_r_hour)::text);

   delete from  asd.ct_graph_section_speed where  calculation_task_id = l_r_gss.calculation_task_id;

   l_res:= asd.jf_calculation_task_pkg$of_delete(p_id_account, row_to_json(l_r_calc_task)::text);
    
   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_graph_section_speed_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r_return asd.jf_calc_speed_type;
   l_r_gss asd.ct_graph_section_speed%rowtype; 
   l_r_calc_task asd.calculation_task%rowtype;
   l_r_hour asd.hours2ct%rowtype;
   l_res text;
begin 
   l_r_return := asd.jf_ct_graph_section_speed_pkg$attr_to_rowtype(p_attr);

   l_r_gss:= l_r_return.r_gss ;
   l_r_calc_task:= l_r_return.r_calc_task ;
   l_r_hour:= l_r_return.r_hour ;

   l_r_gss.calculation_task_id:= nextval( 'asd.calculation_task_calculation_task_id_seq' );
   l_r_calc_task.calculation_task_id:= l_r_gss.calculation_task_id;
   l_r_calc_task.account_id   := p_id_account;
   l_r_calc_task.time_create  := now();
   l_r_calc_task.ct_status_id :=0; --новый
   
   l_r_hour.calculation_task_id:= l_r_gss.calculation_task_id;
   
   l_res :=  asd.jf_calculation_task_pkg$of_insert(p_id_account, row_to_json(l_r_calc_task)::text);
   l_res :=  asd.jf_hours2ct_pkg$of_insert(p_id_account, row_to_json(l_r_hour)::text);
   
   insert into asd.ct_graph_section_speed select l_r_gss.*;
   
   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_graph_section_speed_pkg$of_check_calc_err"(p_id_account numeric, p_attr text)
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$
declare
  l_r_return asd.jf_calc_speed_type;
begin
  l_r_return := asd.jf_ct_graph_section_speed_pkg$attr_to_rowtype(p_attr);

  if asd.jf_ct_graph_section_speed_pkg$of_check_calc(p_id_account, p_attr)  then
    RAISE exception '<<Сделан расчет. Изменение невозможно.>>';
  end if;
  return true;

end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_graph_section_speed_pkg$of_check_calc"(p_id_account numeric, p_attr text)
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$
declare
  l_r_return asd.jf_calc_speed_type;
  l_r_calc_task asd.calculation_task%rowtype;
begin
  l_r_return := asd.jf_ct_graph_section_speed_pkg$attr_to_rowtype(p_attr);
  l_r_calc_task:= l_r_return.r_calc_task ;
  return (l_r_calc_task.time_calc_begin is not null);

end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_graph_section_speed_pkg$of_check_calc"(p_id_calc_task numeric)
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_dt_calc date;
begin 
   select time_calc_begin
   into l_dt_calc
   from asd.calculation_task ct
   where ct.calculation_task_id = p_id_calc_task;
   

   return (l_dt_calc is not null);
   
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_graph_section_speed_pkg$of_check_calc_err"(p_id_calc_task numeric)
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$ 
begin 
  if asd.jf_ct_graph_section_speed_pkg$of_check_calc(p_id_calc_task)   then
     RAISE exception '<<Сделан расчет. Изменение невозможно.>>';
  end if; 
  return true;
   
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_graph_section_speed_pkg$of_calc"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r_return asd.jf_calc_speed_type;
   l_r_gss asd.ct_graph_section_speed%rowtype; 
   l_r_calc_task asd.calculation_task%rowtype;
   l_r_hour asd.hours2ct%rowtype;
   l_res text;
begin 
   l_r_return := asd.jf_ct_graph_section_speed_pkg$attr_to_rowtype(p_attr);

   l_r_gss:= l_r_return.r_gss ;
   l_r_calc_task:= l_r_return.r_calc_task ;
   l_r_hour:= l_r_return.r_hour ;

   perform asd.calculation_task_pkg$queue_task(l_r_calc_task.calculation_task_id);
      
   /*Тут вызов расчета д.б*/
   return null;
end;
 $function$
;