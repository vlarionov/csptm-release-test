CREATE OR REPLACE FUNCTION asd."jf_ct_normative_filter_name_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare
begin
  open p_rows for
  WITH full_result AS(
      select distinct ct.name||'; Формирование:'||to_char(ct.time_calc_end, 'YYYY-MM-DD HH24:MI:SS') as name, ct.calculation_task_id::TEXT
      from asd.calculation_task ct
  )
  select * from full_result where name IS NOT NULL;
end;
$function$;

