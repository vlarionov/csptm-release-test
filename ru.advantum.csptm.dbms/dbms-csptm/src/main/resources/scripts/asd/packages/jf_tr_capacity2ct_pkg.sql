﻿-- Function: asd."jf_tr_capacity2ct_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION asd."jf_tr_capacity2ct_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION asd."jf_tr_capacity2ct_pkg$attr_to_rowtype"(p_attr text)
  RETURNS asd.tr_capacity2ct AS
$BODY$ 
declare 
   l_r asd.tr_capacity2ct%rowtype; 
begin 
   l_r.calculation_task_id := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true); 
   l_r.tr_capacity_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_capacity_id', true); 

   return l_r;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asd."jf_tr_capacity2ct_pkg$attr_to_rowtype"(text)
  OWNER TO adv;
--************************************************************************************
-- Function: asd."jf_tr_capacity2ct_pkg$of_delete"(numeric, text)

-- DROP FUNCTION asd."jf_tr_capacity2ct_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION asd."jf_tr_capacity2ct_pkg$of_delete"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r asd.tr_capacity2ct%rowtype;
   l_res boolean;
begin 
   l_r := asd.jf_tr_capacity2ct_pkg$attr_to_rowtype(p_attr);
   l_res:= asd.jf_ct_graph_section_speed_pkg$of_check_calc_err(l_r.calculation_task_id);
   
   delete from  asd.tr_capacity2ct where  calculation_task_id = l_r.calculation_task_id and 
                                          tr_capacity_id = l_r.tr_capacity_id;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asd."jf_tr_capacity2ct_pkg$of_delete"(numeric, text)
  OWNER TO adv;

--************************************************************************************
-- Function: asd."jf_tr_capacity2ct_pkg$of_insert"(numeric, text)

-- DROP FUNCTION asd."jf_tr_capacity2ct_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION asd."jf_tr_capacity2ct_pkg$of_insert"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r asd.tr_capacity2ct%rowtype;
   l_res boolean;
begin 
   l_r := asd.jf_tr_capacity2ct_pkg$attr_to_rowtype(p_attr);
   l_res:= asd.jf_ct_graph_section_speed_pkg$of_check_calc_err(l_r.calculation_task_id);

   insert into asd.tr_capacity2ct select l_r.*;

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asd."jf_tr_capacity2ct_pkg$of_insert"(numeric, text)
  OWNER TO adv;

--************************************************************************************
-- Function: asd."jf_tr_capacity2ct_pkg$of_insert_all"(numeric, text)

-- DROP FUNCTION asd."jf_tr_capacity2ct_pkg$of_insert_all"(numeric, text);

CREATE OR REPLACE FUNCTION asd."jf_tr_capacity2ct_pkg$of_insert_all"(
    p_id_account numeric,
    p_attr text)
  RETURNS text AS
$BODY$ 
declare 
   l_r asd.tr_capacity2ct%rowtype;
   l_res boolean;
begin 
   l_r := asd.jf_tr_capacity2ct_pkg$attr_to_rowtype(p_attr);
   l_res:= asd.jf_ct_graph_section_speed_pkg$of_check_calc_err(l_r.calculation_task_id);

   insert into asd.tr_capacity2ct 
   select 
      l_r.calculation_task_id,
      vt.muid
   from gis.ref_vehicle_types vt 
   where not exists (select 1 from asd.tr_capacity2ct vt2ct
                     where vt.muid = vt2ct.tr_capacity_id
                       and vt2ct.calculation_task_id = l_r.calculation_task_id);

   return null;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asd."jf_tr_capacity2ct_pkg$of_insert_all"(numeric, text)
  OWNER TO adv;

--************************************************************************************
-- Function: asd."jf_tr_capacity2ct_pkg$of_rows"(numeric, text)

-- DROP FUNCTION asd."jf_tr_capacity2ct_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION asd."jf_tr_capacity2ct_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare
  l_r asd.tr_capacity2ct%rowtype;
begin
  l_r := asd.jf_tr_capacity2ct_pkg$attr_to_rowtype(p_attr);
 open p_rows for 
 select
        cp2ct.calculation_task_id,
        cp2ct.tr_capacity_id,
        cp.short_name as tr_capacity_name,
        CASE
        WHEN ct.time_calc_begin is null THEN
           'P{A},D{OF_UPDATE, OF_DELETE}'
        ELSE
           NULL
        END
        AS "ROW$POLICY"                
      from asd.tr_capacity2ct cp2ct
      join asd.calculation_task ct on ct.calculation_task_id = cp2ct.calculation_task_id
      join core.tr_capacity cp on cp.tr_capacity_id = cp2ct.tr_capacity_id
      where cp2ct.calculation_task_id = l_r.calculation_task_id;

end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asd."jf_tr_capacity2ct_pkg$of_rows"(numeric, text)
  OWNER TO adv;


create or REPLACE function asd."jf_tr_capacity2ct_pkg$of_add_all"
  (p_id_account numeric, p_attr text) returns boolean
LANGUAGE plpgsql
AS $$
DECLARE
  l_tr_capacity_id BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_tr_capacity_id', true);
  l_calculation_task_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true);
BEGIN

  insert into asd.tr_capacity2ct
    select l_calculation_task_id as calculation_task_id, cc.tr_capacity_id
    from core.tr_capacity cc
    WHERE cc.tr_capacity_id = ANY(l_tr_capacity_id)
    AND cc.tr_capacity_id NOT IN
      (select tr_capacity_id from asd.tr_capacity2ct where
      calculation_task_id = l_calculation_task_id);
  return TRUE;
END;
$$;