CREATE OR REPLACE FUNCTION asd."jf_ct_stop_dur_result_filter_stops_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare
  l_calculation_task_id asd.ct_stop_dur_result.calculation_task_id%type := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true);
begin
  open p_rows for
  select distinct on (s.name) s.name stop_name
  from asd.ct_stop_dur_result c
    join rts.stop_item2round sir on sir.stop_item2round_id = c.stop_item2round_id
    join rts.stop_item si on si.stop_item_id = sir.stop_item_id
    join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
    join rts.stop s on s.stop_id = sl.stop_id
  where c.calculation_task_id = l_calculation_task_id;
end;
$function$
;