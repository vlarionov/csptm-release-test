drop function asd.graph_linker_ttb_pkg$get_rounds(p_round_ids int [] );
drop function asd.graph_linker_ttb_pkg$get_graph_sections(p_round_ids int [] );
drop function asd.graph_linker_ttb_pkg$get_stop_items(p_round_ids int [] );
drop function asd.graph_linker_ttb_pkg$get_stop_items2(p_round_ids int [] );
drop function asd.graph_linker_ttb_pkg$get_norms(p_stop1 bigint, p_stop2 bigint);
drop function asd.graph_linker_ttb_pkg$get_timetables(p_order_date ttb.order_list.order_date%type, p_tr_id core.tr.tr_id%type, p_only_planned bool );
drop function asd.graph_linker_ttb_pkg$get_timetables2(p_order_date ttb.order_list.order_date%type, p_tr_id core.tr.tr_id%type, p_only_planned bool );
drop function asd.graph_linker_ttb_pkg$get_updated_timetables(p_tt_variant_ids int [], p_tt_out_ids int [], p_tt_action_ids int [] );
drop function asd.graph_linker_ttb_pkg$get_updated_timetables2(p_tt_variant_ids int [], p_tt_out_ids int [], p_tt_action_ids int [] );
drop function asd.graph_linker_ttb_pkg$clean_links(p_order_date ttb.order_list.order_date%type, p_tr_id core.tr.tr_id%type );

create or replace function asd.graph_linker_ttb_pkg$get_rounds(
    p_round_ids int []
)
    returns table(
        round_id                   rts.round.round_id%type,
        length                     rts.round.length%type,
        graph_section_start_offset rts.round.graph_section_start_offset%type,
        geometry                   rts.round.wkt_geom%type
    ) as
$$
begin
    return query
    select
        r.round_id,
        r.length,
        r.graph_section_start_offset,
        r.wkt_geom
    from rts.round r
    where r.round_id = any (p_round_ids);
end;
$$
language 'plpgsql'
volatile
called on null input
security invoker;

create or replace function asd.graph_linker_ttb_pkg$get_graph_sections(
    p_round_ids int []
)
    returns table(
        graph_section_id rts.graph_section.graph_section_id%type,
        round_id         rts.round.round_id%type,
        order_num        rts.graph_sec2round.order_num%type,
        length           rts.graph_section.length%type,
        round_offset     double precision,
        geometry         rts.graph_section.wkt_geom%type
    ) as
$$
begin
    return query
    select
        gs.graph_section_id,
        gs2r.round_id,
        gs2r.order_num,
        gs.length graph_section_length,
        sum(gs.length)
        over (
            partition by gs2r.round_id
            order by gs2r.order_num ) - gs.length - coalesce(r.graph_section_start_offset, 0) round_offset,
        gs.wkt_geom graph_section_geometry
    from rts.round r
        join rts.graph_sec2round gs2r on r.round_id = gs2r.round_id
        join rts.graph_section gs on gs2r.graph_section_id = gs.graph_section_id
    where r.round_id = any (p_round_ids)
          and not gs2r.sign_deleted
    order by
        gs2r.round_id,
        gs2r.order_num;
end;
$$
language 'plpgsql'
volatile
called on null input
security invoker;

create or replace function asd.graph_linker_ttb_pkg$get_stop_items(
    p_round_ids int []
)
    returns table(
        stop_item_id rts.stop_item.stop_item_id%type,
        round_id     rts.round.round_id%type,
        order_num    rts.stop_item2round.order_num%type,
        round_offset double precision
    ) as
$$
begin
    return query
    select
        si.stop_item_id,
        si2r.round_id,
        si2r.order_num,
        sum(si2r.length_sector)
        over (
            partition by si2r.round_id
            order by si2r.order_num ) :: double precision round_offset
    from rts.stop_item2round si2r
        join rts.stop_item si on si2r.stop_item_id = si.stop_item_id
    where si2r.round_id = any (p_round_ids)
          and not si2r.sign_deleted
    order by
        si2r.round_id,
        si2r.order_num;
end;
$$
language 'plpgsql'
volatile
called on null input
security invoker;

create or replace function asd.graph_linker_ttb_pkg$get_stop_items2(
    p_round_ids int []
)
    returns table(
        stop_item_id rts.stop_item2round.stop_item_id%type,
        round_id     rts.round.round_id%type,
        order_num    rts.stop_item2round.order_num%type,
        round_offset double precision,
        technical    BOOLEAN
    ) as
$$
begin
    return query
    select t.stop_item_id, t.round_id, t.order_num,
          sum(t.length_sector)
          OVER (
              PARTITION BY t.round_id
              ORDER BY t.order_num ) :: DOUBLE PRECISION round_offset
        , bool_or(t.technical) technical
    FROM (
             SELECT
                 si2r.stop_item_id stop_item_id,
                 si2r.round_id round_id,
                 si2r.order_num order_num,
                 si2r.length_sector,
                 sit.stop_type_id = rts.stop_type_pkg$technical() technical
             FROM rts.stop_item2round si2r
                 JOIN rts.stop_item2round_type si2rt ON si2rt.stop_item2round_id = si2r.stop_item2round_id
                 JOIN rts.stop_item_type sit ON sit.stop_item_type_id = si2rt.stop_item_type_id
             WHERE si2r.round_id = any(p_round_ids)
                   AND NOT si2r.sign_deleted

         ) t GROUP BY t.stop_item_id, t.round_id, t.order_num, t.length_sector
    ORDER BY
        t.round_id,
        t.order_num;

end;
$$
language 'plpgsql'
volatile
called on null input
security invoker;

create or replace function asd.graph_linker_ttb_pkg$get_norms(
    p_stop1 bigint,
    p_stop2 bigint
)
    returns table(
        norm_id ttb.norm_between_stop.norm_id%type,
        hour_from    ttb.norm_between_stop.hour_from%type,
        hour_to    ttb.norm_between_stop.hour_to%type,
        capacity ttb.norm_between_stop.tr_capacity_id%type,
        duration ttb.norm_between_stop.between_stop_dur%type,
        tr_type ttb.norm_between_stop.tr_type_id%type
    ) as
$$
begin
    return query
    select nbs.norm_id, nbs.hour_from, nbs.hour_to, nbs.tr_capacity_id, nbs.between_stop_dur, nbs.tr_type_id from ttb.norm_between_stop nbs
    where nbs.stop_item_1_id=p_stop1 and nbs.stop_item_2_id=p_stop2;
end;
$$
language 'plpgsql'
volatile
called on null input
security invoker;

create or replace function asd.graph_linker_ttb_pkg$get_timetables(
    p_order_date   ttb.order_list.order_date%type,
    p_tr_id        core.tr.tr_id%type,
    p_only_planned bool
)
    returns table(
        tr_id           core.tr.tr_id%type,
        round_id        rts.round.round_id%type,
        tt_action_id    ttb.tt_action.tt_action_id%type,
        time_plan_begin ttb.tt_action.dt_action%type,
        time_plan_end   ttb.tt_action.dt_action%type,
        is_active       boolean
    ) as
$$
begin
    return query
    select
        tout.tr_id,
        tar.round_id,
        ta.tt_action_id,
        ta.dt_action,
        ta.dt_action + ta.action_dur * interval '1 second',
        true is_active
    from ttb.tt_variant tv
        join ttb.tt_out tout on tv.tt_variant_id = tout.tt_variant_id
        join ttb.tt_action ta on tout.tt_out_id = ta.tt_out_id
        join ttb.tt_action_round tar on ta.tt_action_id = tar.tt_action_id
    where tv.order_date = p_order_date
          and (p_tr_id is null or tout.tr_id = p_tr_id)
          and tout.tr_id is not null
          and not tv.sign_deleted
          and not tout.sign_deleted
          and not ta.sign_deleted
    order by
        tout.tr_id,
        ta.dt_action;
end;
$$
language 'plpgsql'
volatile
called on null input
security invoker;

create or replace function asd.graph_linker_ttb_pkg$get_timetables2(
    p_order_date   ttb.order_list.order_date%type,
    p_tr_id        core.tr.tr_id%type,
    p_only_planned bool
)
    returns table(
        tr_id           core.tr.tr_id%type,
        tr_type         core.tr.tr_type_id%type,
        norm_id ttb.tt_variant.norm_id%type,
        capacity ttb.tt_out.tr_capacity_id%type,
        round_id        rts.round.round_id%type,
        tt_action_id    ttb.tt_action.tt_action_id%type,
        time_plan_begin ttb.tt_action.dt_action%type,
        time_plan_end   ttb.tt_action.dt_action%type,
        is_active       boolean
    ) as
$$
begin
    return query
    select
        tout.tr_id,
        tr.tr_type_id,
        tv.norm_id,
        tout.tr_capacity_id,
        tar.round_id,
        ta.tt_action_id,
        ta.dt_action,
        ta.dt_action + ta.action_dur * interval '1 second',
        true is_active
    from ttb.tt_variant tv
        join ttb.tt_out tout on tv.tt_variant_id = tout.tt_variant_id
        join ttb.tt_action ta on tout.tt_out_id = ta.tt_out_id
        join ttb.tt_action_round tar on ta.tt_action_id = tar.tt_action_id
        join core.tr on tout.tr_id = tr.tr_id
    where tv.order_date = p_order_date
          and (p_tr_id is null or tout.tr_id = p_tr_id)
          and tout.tr_id is not null
          and not tv.sign_deleted
          and not tout.sign_deleted
          and not ta.sign_deleted
    order by
        tout.tr_id,
        ta.dt_action;
end;
$$
language 'plpgsql'
volatile
called on null input
security invoker;


create or replace function asd.graph_linker_ttb_pkg$get_updated_timetables(
    p_tt_variant_ids int [],
    p_tt_out_ids     int [],
    p_tt_action_ids  int []
)
    returns table(
        tr_id           core.tr.tr_id%type,
        round_id        rts.round.round_id%type,
        tt_action_id    ttb.tt_action.tt_action_id%type,
        time_plan_begin ttb.tt_action.dt_action%type,
        time_plan_end   ttb.tt_action.dt_action%type,
        is_active       boolean
    ) as
$$
begin
    return query
    with effective_actions as (
        select ta.tt_action_id
        from ttb.tt_variant tv
            join ttb.tt_out tout on tv.tt_variant_id = tout.tt_variant_id
            join ttb.tt_action ta on tout.tt_out_id = ta.tt_out_id
        where tv.tt_variant_id = any (p_tt_variant_ids)
        union
        select ta.tt_action_id
        from ttb.tt_out tout
            join ttb.tt_action ta on tout.tt_out_id = ta.tt_out_id
        where tout.tt_out_id = any (p_tt_out_ids)
        union
        select unnest(p_tt_action_ids)
    )
    select
        tout.tr_id,
        tar.round_id,
        ta.tt_action_id,
        ta.dt_action,
        ta.dt_action + ta.action_dur * interval '1 second',
        not tv.sign_deleted and not tout.sign_deleted and not ta.sign_deleted is_active
    from effective_actions ea
        join ttb.tt_action ta on ea.tt_action_id = ta.tt_action_id
        join ttb.tt_action_round tar on ta.tt_action_id = tar.tt_action_id
        join ttb.tt_out tout on tout.tt_out_id = ta.tt_out_id
        join ttb.tt_variant tv on tv.tt_variant_id = tout.tt_variant_id
    where tout.tr_id is not null
    order by
        tout.tr_id,
        ta.dt_action;
end;
$$
language 'plpgsql'
volatile
called on null input
security invoker;

create or replace function asd.graph_linker_ttb_pkg$get_updated_timetables2(
    p_tt_variant_ids int [],
    p_tt_out_ids     int [],
    p_tt_action_ids  int []
)
    returns table(
        tr_id           core.tr.tr_id%type,
        tr_type         core.tr.tr_type_id%type,
        norm_id ttb.tt_variant.norm_id%type,
        capacity ttb.tt_out.tr_capacity_id%type,
        round_id        rts.round.round_id%type,
        tt_action_id    ttb.tt_action.tt_action_id%type,
        time_plan_begin ttb.tt_action.dt_action%type,
        time_plan_end   ttb.tt_action.dt_action%type,
        is_active       boolean
    ) as
$$
begin
    return query
    with effective_actions as (
        select ta.tt_action_id
        from ttb.tt_variant tv
            join ttb.tt_out tout on tv.tt_variant_id = tout.tt_variant_id
            join ttb.tt_action ta on tout.tt_out_id = ta.tt_out_id
        where tv.tt_variant_id = any (p_tt_variant_ids)
        union
        select ta.tt_action_id
        from ttb.tt_out tout
            join ttb.tt_action ta on tout.tt_out_id = ta.tt_out_id
        where tout.tt_out_id = any (p_tt_out_ids)
        union
        select unnest(p_tt_action_ids)
    )
    select
        tout.tr_id,
        tr.tr_type_id,
        tv.norm_id,
        tout.tr_capacity_id,
        tar.round_id,
        ta.tt_action_id,
        ta.dt_action,
        ta.dt_action + ta.action_dur * interval '1 second',
        not tv.sign_deleted and not tout.sign_deleted and not ta.sign_deleted is_active
    from effective_actions ea
        join ttb.tt_action ta on ea.tt_action_id = ta.tt_action_id
        join ttb.tt_action_round tar on ta.tt_action_id = tar.tt_action_id
        join ttb.tt_out tout on tout.tt_out_id = ta.tt_out_id
        join ttb.tt_variant tv on tv.tt_variant_id = tout.tt_variant_id
        join core.tr on tout.tr_id = tr.tr_id
    where tout.tr_id is not null
    order by
        tout.tr_id,
        ta.dt_action;
end;
$$
language 'plpgsql'
volatile
called on null input
security invoker;

create or replace function asd.graph_linker_ttb_pkg$clean_links(
    p_order_date ttb.order_list.order_date%type,
    p_tr_id      core.tr.tr_id%type
)
    returns text as
$$
declare
    l_deleted_links    int;
    l_deleted_sections int;
    l_deleted_stops    int;
    l_nullified_rounds int;
begin
    perform 1
    from ttb.tt_variant tv
        join ttb.tt_out tout on tv.tt_variant_id = tout.tt_variant_id
        join ttb.tt_action ta on tout.tt_out_id = ta.tt_out_id
        join ttb.tt_action_round tar on ta.tt_action_id = tar.tt_action_id
    where tv.order_date = p_order_date
          and tout.tr_id = p_tr_id
    for update nowait;

    delete from asd.tf_graph_link_ttb tgl
    using ttb.tt_variant tv
        join ttb.tt_out tout on tv.tt_variant_id = tout.tt_variant_id
        join ttb.tt_action ta on tout.tt_out_id = ta.tt_out_id
        join ttb.tt_action_round tar on ta.tt_action_id = tar.tt_action_id
    where tv.order_date = p_order_date
          and tout.tr_id = p_tr_id
          and tout.tr_id = tgl.tr_id
          and tgl.event_time between tar.time_packet_begin and tar.time_packet_end
          and tgl.tt_action_id = tar.tt_action_id;

    get diagnostics l_deleted_links = row_count;

    delete from asd.graph_section_speed_ttb gss
    using ttb.tt_variant tv
        join ttb.tt_out tout on tv.tt_variant_id = tout.tt_variant_id
        join ttb.tt_action ta on tout.tt_out_id = ta.tt_out_id
        join ttb.tt_action_round tar on ta.tt_action_id = tar.tt_action_id
    where tv.order_date = p_order_date
          and tout.tr_id = p_tr_id
          and tout.tr_id = gss.tr_id
          and gss.tt_action_id = tar.tt_action_id
          and gss.begin_time between tar.first_graph_section and tar.last_graph_section;

    get diagnostics l_deleted_sections = row_count;

    delete from ttb.tt_action_fact taf
    using ttb.tt_variant tv
        join ttb.tt_out tout on tv.tt_variant_id = tout.tt_variant_id
        join ttb.tt_action ta on tout.tt_out_id = ta.tt_out_id
        join ttb.tt_action_round tar on ta.tt_action_id = tar.tt_action_id
    where tv.order_date = p_order_date
          and tout.tr_id = p_tr_id
          and taf.tt_action_id = tar.tt_action_id;

    get diagnostics l_deleted_stops = row_count;

    update ttb.tt_action_round tar
    set
        round_status_id     = case
                              when not tv.sign_deleted
                                   and not tout.sign_deleted
                                   and not ta.sign_deleted
                                  then ttb.round_status_pkg$planned()
                              else ttb.round_status_pkg$cancelled()
                              end,
        packet_id_begin     = null,
        packet_id_end       = null,
        time_packet_begin   = null,
        time_packet_end     = null,
        first_graph_section = null,
        last_graph_section  = null,
        time_fact_begin     = null,
        time_fact_end       = null
    from ttb.tt_variant tv
        join ttb.tt_out tout on tv.tt_variant_id = tout.tt_variant_id
        join ttb.tt_action ta on tout.tt_out_id = ta.tt_out_id
    where tv.order_date = p_order_date
          and tout.tr_id = p_tr_id
          and tar.tt_action_id = ta.tt_action_id;

    get diagnostics l_nullified_rounds = row_count;

    return json_build_object('deleted_links', l_deleted_links,
                             'deleted_sections', l_deleted_sections,
                             'deleted_stops', l_deleted_stops,
                             'nullified_rounds', l_nullified_rounds) :: text;
end;
$$
language 'plpgsql'
security invoker;

create or replace function asd.graph_linker_ttb_pkg$request_recalc(
    p_order_date ttb.order_list.order_date%type,
    p_tr_id      core.tr.tr_id%type
)
    returns void as
$$
begin
    perform paa.paa_api_pkg$basic_publish(cmnd.jf_cmnd_ref4unit_pkg$cn_command_rmq_broker(),
                                          '',
                                          'graph-linker-offline-ttb',
                                          json_build_object('orderDate', p_order_date :: date,
                                                            'trId', p_tr_id) :: text,
                                          'RECALC_REQUEST');
end;
$$
language 'plpgsql'
security invoker;
