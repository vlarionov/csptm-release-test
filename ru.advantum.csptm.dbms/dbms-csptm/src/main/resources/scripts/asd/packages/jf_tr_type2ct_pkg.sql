﻿CREATE OR REPLACE FUNCTION asd."jf_tr_type2ct_pkg$attr_to_rowtype"(p_attr text)
 RETURNS asd.tr_type2ct
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.tr_type2ct%rowtype;
begin 
   l_r.calculation_task_id := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true); 
   l_r.tr_type_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_type_id', true);

   return l_r;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_tr_type2ct_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare 
 l_r asd.tr_type2ct%rowtype;
begin 
 l_r := asd.jf_tr_type2ct_pkg$attr_to_rowtype(p_attr);
 open p_rows for 
      select 
        tk2ct.calculation_task_id, 
        tk2ct.tr_type_id,
        tt.name as tr_type_name,
        CASE
        WHEN ct.ct_status_id=0 THEN
           'P{A},D{OF_DELETE}'
        ELSE
           NULL
        END
        AS "ROW$POLICY"
      from asd.tr_type2ct tk2ct
      join core.tr_type tt on tt.tr_type_id = tk2ct.tr_type_id  
      join asd.calculation_task ct on ct.calculation_task_id = tk2ct.calculation_task_id
      where tk2ct.calculation_task_id = l_r.calculation_task_id; 
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_tr_type2ct_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
  l_r asd.tr_type2ct%rowtype;
  l_res boolean;
begin

  l_r := asd.jf_tr_type2ct_pkg$attr_to_rowtype(p_attr);
  l_res:= asd.jf_ct_graph_section_speed_pkg$of_check_calc_err(l_r.calculation_task_id);
  

  
  delete
  from asd.tr_type2ct
  where calculation_task_id = l_r.calculation_task_id and
        tr_type_id = l_r.tr_type_id;  return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_tr_type2ct_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
  l_r asd.tr_type2ct%rowtype;
  l_res boolean;
begin

  l_r := asd.jf_tr_type2ct_pkg$attr_to_rowtype(p_attr);
  l_res:= asd.jf_ct_graph_section_speed_pkg$of_check_calc_err(l_r.calculation_task_id);
  
  insert into asd.tr_type2ct
  select l_r.*;  return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_tr_type2ct_pkg$of_insert_all"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare
  l_r asd.tr_type2ct%rowtype;
  l_res boolean;

  l_tr_type_ids INTEGER[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_tr_type_id', TRUE);
begin
  l_r := asd.jf_tr_type2ct_pkg$attr_to_rowtype(p_attr);
  l_res:= asd.jf_ct_graph_section_speed_pkg$of_check_calc_err(l_r.calculation_task_id);

 insert into asd.tr_type2ct(calculation_task_id, tr_type_id)
 select l_r.calculation_task_id,
        tt.tr_type_id
  from core.tr_type tt 
  where not exists (
                     select 1
                     from asd.tr_type2ct cr2tk
                     where cr2tk.tr_type_id = tt.tr_type_id and
                           cr2tk.calculation_task_id = l_r.calculation_task_id
        )
  AND tt.tr_type_id = ANY(l_tr_type_ids);
  return null;
end;
$function$
;