-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_round_stop_result_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare
  l_ct_round_stop_result_id asd.ct_round_stop_result.ct_round_stop_result_id%type := jofl.jofl_pkg$extract_varchar(p_attr, 'ct_round_stop_result_id', true);
begin
  open p_rows for
  select c.ct_round_stop_result_id,
    c.calculation_task_id,
    s.name stop_name,
    c.min_inter_round_stop_dur as min_inter_round_stop_dur,
    c.reg_inter_round_stop_dur as reg_inter_round_stop_dur
  from asd.ct_round_stop_result c
    join rts.round rnd on rnd.round_id = c.round_id and not rnd.sign_deleted
    join rts.stop_item2round sir on sir.round_id = rnd.round_id and sir.order_num = (select max(t.order_num) from rts.stop_item2round t where t.round_id = rnd.round_id and not t.sign_deleted) and not sir.sign_deleted
    join rts.stop_item si on si.stop_item_id = sir.stop_item_id
    join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
    join rts.stop s on s.stop_id = sl.stop_id
  where c.ct_round_stop_result_id = l_ct_round_stop_result_id;
end;
$function$
;
