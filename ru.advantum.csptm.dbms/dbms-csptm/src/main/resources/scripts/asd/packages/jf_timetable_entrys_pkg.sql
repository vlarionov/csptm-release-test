CREATE OR REPLACE FUNCTION asd.jf_timetable_entrys_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare

l_route_id text := jofl.jofl_pkg$extract_varchar(p_attr, 'route_id', true);
l_review_date date :=  jofl.jofl_pkg$extract_date(p_attr, 'review_date', true);
l_route_round_type gis.ref_route_round_types.muid%type := jofl.jofl_pkg$extract_number(p_attr, 'route_round_type', true);
l_ref_db_method text := jofl.jofl_pkg$extract_varchar(p_attr, 'RF_DB_METHOD', true);

begin
 open p_rows for
select distinct te.timetable_entry_num
from gis.routes r
     join gis.route_rounds rr on r.current_route_variant_muid =
       rr.route_variant_muid
     join tt.timetable tt on tt.route_muid = r.muid
     join tt.timetable_entry te on te.timetable_id = tt.timetable_id
     join gis.ref_route_round_types rrrt on rrrt.muid = rr.route_round_type_muid
where (r.muid::text = l_route_id and
       l_review_date between tt.begin_date and
       coalesce(tt.end_date, now() + interval '100 year') and
       rrrt.muid = l_route_round_type)
      or l_ref_db_method in ( 'askp.asmpp_askp_agg', 'askp.asmpp_data');

end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;