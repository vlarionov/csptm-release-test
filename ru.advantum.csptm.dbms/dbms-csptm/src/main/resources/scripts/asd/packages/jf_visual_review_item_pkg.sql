-- код скопирован из базы

create or REPLACE function asd."jf_visual_review_item_pkg$attr_to_rowtype"(p_attr text) returns asd.visual_review_item
LANGUAGE plpgsql
AS $$
declare
  l_r asd.visual_review_item%rowtype;
begin
  l_r.visual_review_item_id := jofl.jofl_pkg$extract_number(p_attr, 'visual_review_item_id', true);
  l_r.review_id := jofl.jofl_pkg$extract_number(p_attr, 'review_id', true);
  l_r.tr_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_id', true);
  l_r.circle := jofl.jofl_pkg$extract_number(p_attr, 'circle', true);


  return l_r;
end;

$$;


create or REPLACE function asd."jf_visual_review_item_pkg$of_delete"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r asd.visual_review_item%rowtype;
begin
  l_r := asd.jf_visual_review_item_pkg$attr_to_rowtype(p_attr);

  delete from  asd.visual_review_detail where  visual_review_item_id = l_r.visual_review_item_id;

  delete from  asd.visual_review_item where  visual_review_item_id = l_r.visual_review_item_id;

  return null;
end;

$$;


create or REPLACE function asd."jf_visual_review_item_pkg$of_insert"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r asd.visual_review_item%rowtype;
  ln_status asd.visual_review_report.status%type;
begin
  l_r := asd.jf_visual_review_item_pkg$attr_to_rowtype(p_attr);

  select status
  into ln_status
  from asd.visual_review_report
  where review_id = l_r.review_id;

  if ln_status = 2 then
    raise  exception '<<Обследование завершено, добавление запрещено!>>';
  end if;

  /*if l_r.tr_id is null then
    raise  exception '<<Укажите ТС!>>';
  end if;*/
  if l_r.circle is null then
    raise  exception '<<Укажите круг обследования!>>';
  end if;

  l_r.visual_review_item_id := nextval('asd.visual_review_item_visual_review_item_id_seq');

  insert into asd.visual_review_item select l_r.*;

  insert into asd.visual_review_detail(visual_review_item_id, stop_id, stop_name, stop_sequence)
    select l_r.visual_review_item_id,
      t.muid,
      t.name,
      row_number() over() rn
    from (
           select sp.muid,
             s.name
           from gis.routes r
             join gis.route_rounds rr on r.current_route_variant_muid = rr.route_variant_muid
             join gis.route_trajectories rt on rt.route_round_muid = rr.muid
             join gis.stop_place2route_traj spr on spr.route_trajectory_muid = rt.muid
             join gis.stop_places sp on sp.muid = spr.stop_place_muid
             join gis.stops s on s.muid = sp.stop_muid
             join asd.visual_review_report vr on r.muid = vr.route_id and rr.route_round_type_muid = vr.route_round_type
           where vr.review_id = l_r.review_id
           order by rt.trajectory_type_muid,
             spr.order_num
         ) t;


  return null;
end;

$$;


create or REPLACE function asd."jf_visual_review_item_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text) returns refcursor
LANGUAGE plpgsql
AS $$
declare
  l_r asd.visual_review_item%rowtype;
begin
  l_r.review_id := jofl.jofl_pkg$extract_number(p_attr, 'review_id', true);
  open p_rows for
  select
    vri.visual_review_item_id,
    vri.review_id,
    vri.circle,
    vri.tr_id,
    tr.garage_num,
    tr_model.name tr_model_name,
    tr_capacity.qty tr_capacity_qty,
    '[' || json_build_object('CAPTION', 'Ведомость', 'JUMP', json_build_object('METHOD', 'asd.visual_review_detail', 'ATTR', '{}')) || ']' AS jump_vrd,
    case
    when vrr.status in (0, 1) THEN 'P{A},E{OF_DELETE,OF_UPDATE}'
    ELSE ''
    END AS "ROW$POLICY"
  from asd.visual_review_item vri
    LEFT join core.tr on vri.tr_id = tr.tr_id
    LEFT join core.tr_model on tr_model.tr_model_id = tr.tr_model_id
    join asd.visual_review_report vrr on vri.review_id = vrr.review_id
    --left join core.tr_capacity on tr_capacity.tr_capacity_id = tr.tr_capacity_id
    LEFT join core.tr_capacity on tr_capacity.tr_capacity_id = tr_model.tr_capacity_id
  where vri.review_id = l_r.review_id;
end;

$$;


create or REPLACE function asd."jf_visual_review_item_pkg$of_update"(p_id_account numeric, p_attr text) returns text
LANGUAGE plpgsql
AS $$
declare
  l_r asd.visual_review_item%rowtype;
begin
  l_r := asd.jf_visual_review_item_pkg$attr_to_rowtype(p_attr);

  update asd.visual_review_item set
    review_id = l_r.review_id,
    tr_id = l_r.tr_id,
    circle = l_r.circle
  where
    visual_review_item_id = l_r.visual_review_item_id;

  return null;
end;

$$;
