CREATE OR REPLACE FUNCTION asd."jf_ct_stop_visit_result_filter_stops_pkg$of_rows"
  (p_id_account numeric,OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare
  l_ct_round_stop_result_id BIGINT :=jofl.jofl_pkg$extract_number(p_attr, 'ct_round_stop_result_id', true);
begin
  open p_rows for
  WITH t1 AS (select
    s_1.name||' - '||s_2.name as stops,
    si_1.stop_item_id||'-'||si_2.stop_item_id as ids
  from asd.ct_stop_visit_result c
    join rts.stop_item2round sir_1 on sir_1.stop_item2round_id = c.stop_item2round_1_id
    join rts.stop_item2round sir_2 on sir_2.stop_item2round_id = c.stop_item2round_2_id
    join rts.stop_item si_1 on si_1.stop_item_id = sir_1.stop_item_id
    join rts.stop_item si_2 on si_2.stop_item_id = sir_2.stop_item_id
    join rts.stop_location sl_1 on sl_1.stop_location_id = si_1.stop_location_id
    join rts.stop_location sl_2 on sl_2.stop_location_id = si_2.stop_location_id
    join rts.stop s_1 on s_1.stop_id = sl_1.stop_id
    join rts.stop s_2 on s_2.stop_id = sl_2.stop_id
  where c.ct_round_stop_result_id = l_ct_round_stop_result_id)
  SELECT distinct on (ids) * from t1;
end;
$function$;
