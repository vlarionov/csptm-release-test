CREATE OR REPLACE FUNCTION asd.jf_ct_normative_result_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
    l_calculation_task_id 		BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true);
    f_list_move_direction_id	integer[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_move_direction_id', true);
    f_list_tr_capacity_id		integer[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_tr_capacity_id', true);
    f_list_parent_type_id		integer[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_action_type_id', true);
begin
  open p_rows for
  with adata as
  (
  select
    --res.,
    res.ct_round_stop_result_id,
    res.calculation_task_id,
    res.hour_from ,
    res.hour_to ,
    res.tr_type_id,
    res.tr_capacity_id ,
    res.round_id,
    res.min_inter_round_stop_dur,
    res.reg_inter_round_stop_dur,

    (select sum(vr.norm_duration) from asd.ct_stop_visit_result vr where vr.ct_round_stop_result_id=res.ct_round_stop_result_id) as route_round_dur_norm,
    res.min_inter_round_stop_dur + res.reg_inter_round_stop_dur +  (select sum(vr.norm_duration) from asd.ct_stop_visit_result vr where vr.ct_round_stop_result_id=res.ct_round_stop_result_id) as sum_route_round_dur,
    tt.name as route_tt_name,
    cap.short_name as tr_capacity_name,
    r.route_num,    
    rnd.code as round_kind_name,
    to_char(hour_from +(3 || ' hour')::interval, 'HH24:MI') || '-' || to_char(hour_to +(3 || ' hour')::interval, 'HH24:MI') hour_from_to,
    act.action_type_name as round_name,
    (select t.action_type_name from ttb.action_type t where  t.action_type_id = act.parent_type_id)   as round_type_name,
    md.move_direction_name,
    act.parent_type_id,
    md.move_direction_id,
    cap.qty
  from asd.ct_round_stop_result res
    join asd.ct_normative norm on norm.calculation_task_id = res.calculation_task_id
    join asd.calculation_task ct on ct.calculation_task_id = res.calculation_task_id
    join core.tr_type tt on tt.tr_type_id= res.tr_type_id
    join rts.round rnd on rnd.round_id =res.round_id and not rnd.sign_deleted
    join ttb.action_type act on act.action_type_id = rnd.action_type_id
    join rts.route_variant rv on rv.route_variant_id = rnd.route_variant_id and not rv.sign_deleted
    --join rts.route r on r.current_route_variant_id = rv.route_variant_id and not r.sign_deleted
    join rts.route r on r.route_id = rv.route_id and not r.sign_deleted    
    join core.tr_capacity cap on cap.tr_capacity_id = res.tr_capacity_id
    join rts.move_direction md on md.move_direction_id = rnd.move_direction_id
  where res.calculation_task_id = l_calculation_task_id
  	and (md.move_direction_id = any(f_list_move_direction_id) or array_length(f_list_move_direction_id, 1) is null)
    and (cap.tr_capacity_id = any(f_list_tr_capacity_id) or array_length(f_list_tr_capacity_id, 1) is null)
    and ((select t.action_type_id from ttb.action_type t where  t.action_type_id = act.parent_type_id) = any(f_list_parent_type_id) or array_length(f_list_parent_type_id, 1) is null)
    )
  select *
  from adata
  where route_round_dur_norm>0
  order by
    parent_type_id ASC,
    move_direction_id ASC,
    hour_from ASC,
    hour_to ASC,
    qty DESC
;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;