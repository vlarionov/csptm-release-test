CREATE OR REPLACE FUNCTION asd."jf_ct_stop_visit_result_filter_avg_speed_pkg$of_rows"
  (p_id_account numeric,OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare
  l_ct_round_stop_result_id BIGINT :=jofl.jofl_pkg$extract_number(p_attr, 'ct_round_stop_result_id', true);
begin
  open p_rows for
  select DISTINCT avg_speed from asd.ct_stop_visit_result c where c.ct_round_stop_result_id = l_ct_round_stop_result_id
  ORDER BY avg_speed DESC;
end;
$function$;