CREATE OR REPLACE FUNCTION asd."jf_route_variant2ct_pkg$attr_to_rowtype"(p_attr text)
 RETURNS asd.route_variant2ct
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.route_variant2ct%rowtype; 
begin 
   l_r.calculation_task_id := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true); 
   l_r.route_variant_id := jofl.jofl_pkg$extract_number(p_attr, 'route_variant_id', true);
   

   return l_r;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_route_variant2ct_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare
  l_r asd.route_variant2ct%rowtype;
begin
  l_r := asd.jf_route_variant2ct_pkg$attr_to_rowtype(p_attr);

 open p_rows for
 select
   r2ct.calculation_task_id,
   r2ct.route_variant_id,
   r.route_num,
   tt.name as route_variant_tt_name,
   lower(rv.action_period) as act_b,
   upper(rv.action_period) as act_e,
   r.current_route_variant_id = rv.route_variant_id as is_current,
   rv.rv_comment,
   CASE
   WHEN ct.time_calc_begin is null THEN
     'P{A},D{OF_UPDATE, OF_DELETE}'
   ELSE  NULL   END   AS "ROW$POLICY"
 from asd.route_variant2ct r2ct
   join asd.calculation_task ct on ct.calculation_task_id = r2ct.calculation_task_id
   join rts.route_variant rv on rv.route_variant_id = r2ct.route_variant_id
   join rts.route r on r.route_id = rv.route_id
   join core.tr_type tt on tt.tr_type_id = r.tr_type_id
 where ct.calculation_task_id = l_r.calculation_task_id ;



end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_route_variant2ct_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.route_variant2ct%rowtype;
begin 
   l_r := asd.jf_route_variant2ct_pkg$attr_to_rowtype(p_attr);

   delete from  asd.route_variant2ct where  calculation_task_id = l_r.calculation_task_id and 
                                    route_variant_id = l_r.route_variant_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_route_variant2ct_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.route_variant2ct%rowtype;
   l_res boolean;
begin 
   l_r := asd.jf_route_variant2ct_pkg$attr_to_rowtype(p_attr);
   l_res:= asd.jf_ct_graph_section_speed_pkg$of_check_calc_err(l_r.calculation_task_id);

   insert into asd.route_variant2ct select l_r.*;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_route_variant2ct_pkg$of_insert_all"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.route_variant2ct%rowtype;
   l_res boolean;
begin 
   l_r := asd.jf_route_variant2ct_pkg$attr_to_rowtype(p_attr);
   l_res:= asd.jf_ct_graph_section_speed_pkg$of_check_calc_err(l_r.calculation_task_id);

   insert into asd.route_variant2ct 
   select 
        l_r.calculation_task_id,
        r.route_variant_id
   from rts.route_variant r
     join rts.route_variant_variant rv on rv.route_variant_id = r.route_variant_id and rv.route_variant_variant_id = r.current_route_variant_variant_id
   where not exists (select 1 from asd.route_variant2ct  r2ct
   where r2ct.route_variant_id = r.route_variant_id
         and r2ct.calculation_task_id = l_r.calculation_task_id);

   return null;
end;
 $function$
;

create or REPLACE function asd."jf_route_variant2ct_pkg$of_add_all"
  (p_id_account numeric, p_attr text) returns boolean
LANGUAGE plpgsql
AS $$
DECLARE
  l_route_variant_id BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_route_variant_id', true);
  l_calculation_task_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true);
BEGIN
  insert into asd.route_variant2ct
    WITH t1 AS (
        select l_calculation_task_id as calculation_task_id, unnest(l_route_variant_id) as route_variant_id
    )
    SELECT * from t1
    WHERE route_variant_id NOT IN
          (select route_variant_id from asd.route_variant2ct where
           calculation_task_id = l_calculation_task_id);
  return TRUE;
END;
$$;