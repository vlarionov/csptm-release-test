CREATE OR REPLACE FUNCTION asd."jf_routes_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare
  l_review_date date := jofl.jofl_pkg$extract_date(p_attr,'review_date',true);
begin
  open p_rows for
  select distinct r.muid::text,
         r.number,
         r.open_date,
         r.close_date,
         r.comment
  from gis.routes r
       join gis.route_rounds rr on r.current_route_variant_muid =
         rr.route_variant_muid
       join tt.timetable tt on tt.route_muid = r.muid
  where l_review_date between tt.begin_date and
        coalesce(tt.end_date, now() + interval '100 year');

end;
$function$
;