﻿CREATE OR REPLACE FUNCTION asd.jf_hours2ct_pkg$attr_to_rowtype (
  p_attr text
)
RETURNS asd.hours2ct AS
$body$
declare 
   l_r 		asd.hours2ct%rowtype; 
   l_sec_b  int:= jofl.jofl_pkg$extract_number(p_attr, 'sec_b', true);
   l_sec_e  int:= jofl.jofl_pkg$extract_number(p_attr, 'sec_e', true);   
begin 
   --l_r.hour_from := jofl.jofl_pkg$extract_varchar(p_attr, 'hour_from', true); 
   --l_r.hour_to := jofl.jofl_pkg$extract_varchar(p_attr, 'hour_to', true); 
   case 
   		when l_sec_b is not null 
        then l_r.hour_from := (now()::date  + l_sec_b * interval '1 sec')::timestamp;
        else l_r.hour_from := jofl.jofl_pkg$extract_varchar(p_attr, 'hour_from', true); 
   end case;
   case 
   		when l_sec_e is not null 
        then l_r.hour_to := (now()::date  + l_sec_e * interval '1 sec')::timestamp;
        else l_r.hour_to := jofl.jofl_pkg$extract_varchar(p_attr, 'hour_to', true); 
   end case;   
   
   l_r.calculation_task_id := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true);

   return l_r;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION asd.jf_hours2ct_pkg$of_rows (
  p_id_account numeric,
  out p_rows refcursor,
  p_attr text
)
RETURNS refcursor AS
$body$
declare
	l_timezone numeric := 0;
	l_r asd.hours2ct%rowtype;
begin 
 	l_r := asd.jf_hours2ct_pkg$attr_to_rowtype(p_attr);  

    open p_rows for
    select hour_from,
    	   extract(epoch from h.hour_from::time) sec_b,
           hour_to,
           extract(epoch from h.hour_to::time) sec_e,
           calculation_task_id,
           to_char(hour_from +(l_timezone || ' hour')::interval, 'HH24:MI') || '-' || to_char(hour_to +(l_timezone || ' hour')::interval, 'HH24:MI') hour_from_to
    from asd.hours2ct h
    where h.calculation_task_id = l_r.calculation_task_id;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION asd.jf_hours2ct_pkg$of_delete (
  p_id_account numeric,
  p_attr text
)
RETURNS text AS
$body$
declare 
   l_r asd.hours2ct%rowtype;
   l_res boolean;
begin 
   l_r := asd.jf_hours2ct_pkg$attr_to_rowtype(p_attr);
   l_res:= asd.jf_ct_graph_section_speed_pkg$of_check_calc_err(l_r.calculation_task_id);
  
   delete from  asd.hours2ct 
    where hour_from::time = l_r.hour_from::time
   	  and hour_to::time = l_r.hour_to::time 
      and calculation_task_id = l_r.calculation_task_id;

   return null;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_hours2ct_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare 
   l_r asd.hours2ct%rowtype;
   l_res text;
   l_hour_period DOUBLE PRECISION;
begin 
   l_r := asd.jf_hours2ct_pkg$attr_to_rowtype(p_attr);
   l_res := asd.jf_hours2ct_pkg$of_check_date(p_id_account, p_attr );
   l_res:= asd.jf_ct_graph_section_speed_pkg$of_check_calc_err(l_r.calculation_task_id);
   l_hour_period:=coalesce(jofl.jofl_pkg$extract_number(p_attr, 'hour_period', true),60);

   l_r.hour_from:=case when extract (minute from l_r.hour_from) = double precision '0'  then l_r.hour_from else date_trunc( 'hour',l_r.hour_from) +  interval '1 minute'*  l_hour_period end;
   l_r.hour_to:=case when extract (minute from l_r.hour_to) = double precision '0'  then l_r.hour_to else date_trunc( 'hour',l_r.hour_to) +  interval '1 minute'*  l_hour_period end;

  insert into asd.hours2ct select l_r.*;

   return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_hours2ct_pkg$of_check_date"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
  l_r asd.hours2ct%rowtype;
begin
  l_r := asd.jf_hours2ct_pkg$attr_to_rowtype(p_attr);

  if l_r.hour_from > l_r.hour_to then
     RAISE exception '<<Время начала не может быть больше времени окончания.>>';
  end if;

  return null;
end;
$function$
;
