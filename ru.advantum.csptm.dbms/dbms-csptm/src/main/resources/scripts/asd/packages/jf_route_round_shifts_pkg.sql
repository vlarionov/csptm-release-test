CREATE OR REPLACE FUNCTION asd."jf_route_round_shifts_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare

l_route_id text := jofl.jofl_pkg$extract_varchar(p_attr, 'route_id', true);
l_review_date date :=  jofl.jofl_pkg$extract_date(p_attr, 'review_date', true);
l_route_round_type gis.ref_route_round_types.muid%type := jofl.jofl_pkg$extract_number(p_attr, 'route_round_type', true);
l_timetable_entry_num tt.timetable_entry.timetable_entry_num%type := jofl.jofl_pkg$extract_number(p_attr, 'timetable_entry_num', true);

begin
 open p_rows for
select distinct tp.shift
from gis.routes r
     join gis.route_rounds rr on r.current_route_variant_muid =
       rr.route_variant_muid
     join tt.timetable tt on tt.route_muid = r.muid
     join tt.timetable_entry te on te.timetable_id = tt.timetable_id
      join tt.round tr on tr.timetable_entry_id = te.timetable_entry_id
     join tt.timetable_plan tp on tp.round_id = tr.round_id
     join gis.ref_route_round_types rrrt on rrrt.muid = rr.route_round_type_muid
where r.muid::text = l_route_id and
      l_review_date between tt.begin_date and
      coalesce(tt.end_date, now() + interval '100 year') and
      rrrt.muid = l_route_round_type
      and te.timetable_entry_num=l_timetable_entry_num;

end;
$function$
;