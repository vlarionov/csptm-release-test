create or replace function asd.fill_agg_gs_speed
  (v_date         in timestamp,
   v_agg_minute   in integer)
returns bigint
-- ========================================================================
-- Расчет агрегированных значений скорости для ребер графа за календарные сутки
-- v_date       - дата расчета
-- v_agg_minute - к-во минут агрегирования
-- ========================================================================
as $$ declare
   v_dow        smallint;
   v_begin_date timestamp := v_date::date::timestamp;
   v_end_date   timestamp := v_begin_date + interval '1 day';
   v_result     bigint := 0;
begin
   v_dow := extract(isodow from v_date);
   
   insert into asd.agg_gs_speed as p
          (   round_id,     graph_section_id, visit_hour,   tr_capacity_id,   visit_count,   week_day_id,   visit_duration)
   select rug.round_id, gsg.graph_section_id, visit_time, q.tr_capacity_id, q.visit_count, q.week_day_id, q.visit_duration
     from (select route_trajectory_muid, graph_section_muid, tr_capacity_id, week_day_id, visit_time,
                  sum(visit_count) as visit_count,
                  array_agg(visit_duration) as visit_duration
        	 from (select r.route_trajectory_muid, gss.graph_section_muid, tm.tr_capacity_id,
                          v_dow as week_day_id,
                          core.floor_minutes(gss.begin_time, v_agg_minute) as visit_time,
                          1 as visit_count,
                          extract(epoch from (gss.end_time - gss.begin_time)) as visit_duration
                     from asd.graph_section_speed as gss
                    inner join tt.order_round as oro    on oro.order_round_id = gss.order_round_id
                    inner join tt.round as r            on oro.round_id = r.round_id
                    inner join tt.order_list as ol      on oro.order_list_id = ol.order_list_id
                    inner join core.tr as tr            on ol.tr_id = tr.tr_id
                     join core.tr_model tm on tm.tr_model_id = tr.tr_model_id
                    where gss.begin_time >= v_begin_date
                      and gss.begin_time <  v_end_date
                  ) as t
            group by route_trajectory_muid, graph_section_muid, tr_capacity_id, week_day_id, visit_time
          ) q
     join rts.round2gis rug on rug.route_trajectory_muid = q.route_trajectory_muid
     join rts.graph_section2gis gsg on gsg.graph_section_muid = q.graph_section_muid
   on conflict (graph_section_id, round_id, visit_hour, tr_capacity_id)
   do update set (visit_count, week_day_id, visit_duration) =
                 (excluded.visit_count, excluded.week_day_id, excluded.visit_duration)
       where p.visit_count    is distinct from excluded.visit_count or
             p.week_day_id    is distinct from excluded.week_day_id or
             p.visit_duration is distinct from excluded.visit_duration;
   
   get diagnostics v_result = row_count;
   return v_result;
end;
$$ language plpgsql security definer;


create or replace function asd.fill_agg_gs_speed
  (v_date         in timestamp)
returns void
-- ========================================================================
-- Расчет агрегированных значений скорости для ребер графа за календарные сутки
-- 30 минут
-- ========================================================================
as $$ declare
   v_tmp   bigint;
begin
   v_tmp := asd.fill_agg_gs_speed(v_date, 30);
end;
$$ language plpgsql security definer;
