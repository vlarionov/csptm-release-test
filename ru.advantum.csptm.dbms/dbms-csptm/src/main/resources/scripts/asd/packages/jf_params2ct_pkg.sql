CREATE OR REPLACE FUNCTION asd."jf_params2ct_pkg$attr_to_rowtype"(p_attr text)
 RETURNS asd.params2ct
 LANGUAGE plpgsql
AS $function$
declare 
   l_r asd.params2ct%rowtype; 
begin 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 
   l_r.calculation_task_id := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true); 
   l_r.description := jofl.jofl_pkg$extract_varchar(p_attr, 'description', true); 
   l_r.value := jofl.jofl_pkg$extract_number(p_attr, 'value', true); 

   return l_r;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_params2ct_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare
  l_r asd.params2ct%rowtype;
begin
  l_r := asd.jf_params2ct_pkg$attr_to_rowtype(p_attr);
  open p_rows for
  select name,
         calculation_task_id,
         description,
         value
  from asd.params2ct
  where calculation_task_id = l_r.calculation_task_id;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_params2ct_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare 
   l_r asd.params2ct%rowtype;
   l_res boolean;
begin 
 l_r := asd.jf_params2ct_pkg$attr_to_rowtype(p_attr);
 l_res:= asd.jf_ct_graph_section_speed_pkg$of_check_calc_err(l_r.calculation_task_id);

   update asd.params2ct set 
          
          description = l_r.description, 
          value = l_r.value
   where 
          calculation_task_id = l_r.calculation_task_id
		  and name= l_r.name;

   return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_params2ct_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare 
   l_r asd.params2ct%rowtype;
   l_res boolean;
begin 
   l_r := asd.jf_params2ct_pkg$attr_to_rowtype(p_attr);
   l_res:= asd.jf_ct_graph_section_speed_pkg$of_check_calc_err(l_r.calculation_task_id);
   delete from  asd.params2ct where  calculation_task_id = l_r.calculation_task_id
   and name= l_r.name;

   return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_params2ct_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare 
   l_r asd.params2ct%rowtype;
   l_res boolean;
begin 
   l_r := asd.jf_params2ct_pkg$attr_to_rowtype(p_attr);
   l_res:= asd.jf_ct_graph_section_speed_pkg$of_check_calc_err(l_r.calculation_task_id);
   l_r.name=l_r.description;
   insert into asd.params2ct select l_r.*;

   return null;
end;
$function$
;