CREATE OR REPLACE FUNCTION asd."jf_ct_type_pkg$attr_to_rowtype"(p_attr text)
 RETURNS asd.ct_type
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.ct_type%rowtype; 
begin 
   l_r.ct_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'ct_type_id', true); 
   l_r.ct_type_name := jofl.jofl_pkg$extract_varchar(p_attr, 'ct_type_name', true); 

   return l_r;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_type_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$ 
declare 
begin 
 open p_rows for 
      select 
        ct_type_id, 
        ct_type_name
      from asd.ct_type; 
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_type_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.ct_type%rowtype;
begin 
   l_r := asd.jf_ct_type_pkg$attr_to_rowtype(p_attr);

   update asd.ct_type set 
          ct_type_name = l_r.ct_type_name
   where 
          ct_type_id = l_r.ct_type_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_type_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.ct_type%rowtype;
begin 
   l_r := asd.jf_ct_type_pkg$attr_to_rowtype(p_attr);

   delete from  asd.ct_type where  ct_type_id = l_r.ct_type_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_type_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.ct_type%rowtype;
begin 
   l_r := asd.jf_ct_type_pkg$attr_to_rowtype(p_attr);

   insert into asd.ct_type select l_r.*;

   return null;
end;
 $function$
;