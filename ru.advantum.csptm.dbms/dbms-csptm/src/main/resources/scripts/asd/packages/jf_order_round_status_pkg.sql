﻿-- Function: asd."jf_order_round_status_pkg$attr_to_rowtype"(text)

-- DROP FUNCTION asd."jf_order_round_status_pkg$attr_to_rowtype"(text);

CREATE OR REPLACE FUNCTION asd."jf_order_round_status_pkg$attr_to_rowtype"(p_attr text)
  RETURNS asd.order_round_status AS
$BODY$
declare
  l_r asd.order_round_status%rowtype;
begin
  l_r.order_round_status_id := jofl.jofl_pkg$extract_number(p_attr, 'order_round_status_id', true);
  l_r.code := jofl.jofl_pkg$extract_number(p_attr, 'code', true);
  l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true);

  return l_r;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION asd."jf_order_round_status_pkg$attr_to_rowtype"(text)
OWNER TO adv;
------------------------
-- Function: asd."jf_order_round_status_pkg$of_delete"(numeric, text)

-- DROP FUNCTION asd."jf_order_round_status_pkg$of_delete"(numeric, text);

CREATE OR REPLACE FUNCTION asd."jf_order_round_status_pkg$of_delete"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r asd.order_round_status%rowtype;
begin
  l_r := asd.jf_order_round_status_pkg$attr_to_rowtype(p_attr);

  delete from  asd.order_round_status where  order_round_status_id = l_r.order_round_status_id;

  return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION asd."jf_order_round_status_pkg$of_delete"(numeric, text)
OWNER TO adv;
---------------------------
-- Function: asd."jf_order_round_status_pkg$of_insert"(numeric, text)

-- DROP FUNCTION asd."jf_order_round_status_pkg$of_insert"(numeric, text);

CREATE OR REPLACE FUNCTION asd."jf_order_round_status_pkg$of_insert"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r asd.order_round_status%rowtype;
begin
  l_r := asd.jf_order_round_status_pkg$attr_to_rowtype(p_attr);

  insert into asd.order_round_status select l_r.*;

  return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION asd."jf_order_round_status_pkg$of_insert"(numeric, text)
OWNER TO adv;
-------------------------
-- Function: asd."jf_order_round_status_pkg$of_update"(numeric, text)

-- DROP FUNCTION asd."jf_order_round_status_pkg$of_update"(numeric, text);

CREATE OR REPLACE FUNCTION asd."jf_order_round_status_pkg$of_update"(
  p_id_account numeric,
  p_attr text)
  RETURNS text AS
$BODY$
declare
  l_r asd.order_round_status%rowtype;
begin
  l_r := asd.jf_order_round_status_pkg$attr_to_rowtype(p_attr);

  update asd.order_round_status set
    code = l_r.code,
    name = l_r.name
  where
    order_round_status_id = l_r.order_round_status_id;

  return null;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION asd."jf_order_round_status_pkg$of_update"(numeric, text)
OWNER TO adv;
--------------------------

-- Function: asd."jf_order_round_status_pkg$of_rows"(numeric, text)

-- DROP FUNCTION asd."jf_order_round_status_pkg$of_rows"(numeric, text);

CREATE OR REPLACE FUNCTION asd."jf_order_round_status_pkg$of_rows"(
    IN p_id_account numeric,
    OUT p_rows refcursor,
    IN p_attr text)
  RETURNS refcursor AS
$BODY$ 
declare 
begin 
 open p_rows for 
      select 
        order_round_status_id, 
        name, 
        code
      from asd.order_round_status
      order by code;
end;
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION asd."jf_order_round_status_pkg$of_rows"(numeric, text)
  OWNER TO adv;
---------------------------
CREATE OR REPLACE FUNCTION asd.jf_order_round_status_pkg$cn_in_process (
)
RETURNS asd.order_round_status.order_round_status_id%type AS
$body$
begin
 return 3;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;