﻿-- ========================================================================
-- Пакет расчета средней скорости между остановками
-- ========================================================================

create or replace function asd.calc_speed_bs_pkg$calc_result
  (p_calc_task_id   in bigint)
returns void
-- ========================================================================
-- Расчета средней скорости меджу остановками
-- ========================================================================
as $$ declare
   l_ct                   record;
   l_ct_hour              record;
   l_hour_from            time without time zone;
   l_hour_to              time without time zone;
   l_cross_day            boolean;
   l_hour_period          integer;
   
   l_week_days            smallint[];
   l_stop_place           bigint[];
   l_tr_capacity          bigint[];
begin
   
   select ct.calculation_task_id, ct.hour_period,
          ttb.get_local_timestamp_from_utc(ctgss.calc_period_begin)::date::timestamp as calc_period_begin,
          ttb.get_local_timestamp_from_utc(ctgss.calc_period_end)::date::timestamp as calc_period_end,
          ctgss.round_id,
          sia.order_num order_num_1, sib.order_num order_num_2,
          ctgss.is_capacity_dependent
     into l_ct
     from asd.calculation_task as ct
     join asd.ct_speed_between_stops as ctgss on ct.calculation_task_id = ctgss.calculation_task_id
     join rts.stop_item2round sia on sia.stop_item2round_id = ctgss.stop_item2round_1_id
     join rts.stop_item2round sib on sib.stop_item2round_id = ctgss.stop_item2round_2_id
    where ct.calculation_task_id = p_calc_task_id;
   
   l_hour_period := l_ct.hour_period::integer;
   
-- raise notice 'l_ct: %', l_ct;
   
   select * into l_ct_hour from asd.hours2ct as h2ct where h2ct.calculation_task_id = p_calc_task_id;
   
-- raise notice 'l_ct_hour: %', l_ct_hour;
   
   l_hour_from := ttb.get_local_timestamp(l_ct_hour.hour_from)::time;
   l_hour_to   := ttb.get_local_timestamp(l_ct_hour.hour_to)::time;
   l_cross_day := l_hour_from > l_hour_to;
   
-- raise notice 'l_hour_from: %', l_hour_from;
-- raise notice 'l_hour_to: %', l_hour_to;
-- raise notice 'l_cross_day: %', l_cross_day;
   
   select array_agg(sig.stop_place_muid) into l_stop_place
     from rts.stop_item2round si2r
     join rts.stop_item2gis sig on sig.stop_item_id = si2r.stop_item_id
    where si2r.round_id = l_ct.round_id
      and not si2r.sign_deleted;
   
-- raise notice 'l_stop_place: %', l_stop_place;
   
   select array_agg(tcct.tr_capacity_id) into l_tr_capacity
     from asd.tr_capacity2ct tcct
    where tcct.calculation_task_id = p_calc_task_id;
   
-- raise notice 'l_tr_capacity: %', l_tr_capacity;
   
   select array_agg(wdct.week_day_id) into l_week_days
     from asd.week_day2ct wdct
    where wdct.calculation_task_id = p_calc_task_id;
   
-- raise notice 'l_week_days: %', l_week_days;
   
   delete from asd.ct_speed_bs_result where calculation_task_id = p_calc_task_id;
   
   with stop_list as (
      select si2r.stop_item_id,
             lag(si2r.stop_item_id) over (partition by si2r.round_id order by si2r.order_num) as lag_stop_item_id,
             si2r.length_sector
        from rts.stop_item2round si2r
       where si2r.round_id = l_ct.round_id
         and si2r.order_num between l_ct.order_num_1 and l_ct.order_num_2
         and not si2r.sign_deleted
   ),
   time_list as (
      select q.hour_from, q.time_fact, q.lag_time_fact, si2gb.stop_item_id, si2ga.stop_item_id as lag_stop_item_id, tr_capacity_id
         from (select oo.time_fact,
                      lag(oo.time_fact) over (partition by ord.order_round_id order by oo.stop_order_num) as lag_time_fact,
                      oo.stop_place_muid,
                      lag(oo.stop_place_muid) over (partition by ord.order_round_id order by oo.stop_order_num) as lag_stop_place_muid,
                      core.floor_minutes(oo.time_fact, l_hour_period)::time as hour_from,
                      tr.tr_type_id,
                      model.tr_capacity_id
                 from tt.order_fact oo
                 join tt.order_round ord on ord.order_round_id = oo.order_round_id
                 join tt.order_list ol on ol.order_list_id = ord.order_list_id
                 join tt.round rd on rd.round_id = ord.round_id
                 join rts.round2gis rd2g on rd2g.route_trajectory_muid = rd.route_trajectory_muid
                 join core.tr tr on tr.tr_id = ol.tr_id
                 join core.tr_model model ON tr.tr_model_id = model.tr_model_id
                 join core.tr_model md on md.tr_model_id = tr.tr_model_id
                where ol.order_date between l_ct.calc_period_begin and l_ct.calc_period_end
                  and case when l_cross_day 
                         then oo.time_fact::time >= l_hour_from  or oo.time_fact::time <= l_hour_to
                         else oo.time_fact::time >= l_hour_from and oo.time_fact::time <= l_hour_to
                      end
                  and oo.sign_deleted = 0
                  and ord.is_active = 1
                  and ol.sign_deleted = 0
                  and ol.is_active = 1
                  and rd.round_code = '00'
                  and oo.stop_place_muid = any(l_stop_place)
                  and (l_week_days is null or extract(isodow from ol.order_date) = any(l_week_days))
                  and (l_tr_capacity is null or md.tr_capacity_id = any(l_tr_capacity))
              ) q
         join rts.stop_item2gis si2ga on si2ga.stop_place_muid = q.lag_stop_place_muid
                                     and si2ga.tr_type_id = q.tr_type_id
         join rts.stop_item2gis si2gb on si2gb.stop_place_muid = q.stop_place_muid
                                     and si2gb.tr_type_id = q.tr_type_id
        where q.lag_stop_place_muid is not null
   )
   insert into asd.ct_speed_bs_result
         (calculation_task_id, hour_from, hour_to, stop_item_1_id, stop_item_2_id, avg_speed, tr_capacity_id)
   select p_calc_task_id, tl.hour_from, tl.hour_from + (l_hour_period || ' min')::interval, sl.lag_stop_item_id, sl.stop_item_id,
          round(avg(sl.length_sector / (case when extract(epoch from tl.time_fact - tl.lag_time_fact) < 10
                                           then sl.length_sector
                                           else extract(epoch from tl.time_fact - tl.lag_time_fact) / 3.6
                                        end)
               )) as avg_speed,
          case when l_ct.is_capacity_dependent then tl.tr_capacity_id else 1 end as tr_capacity_id
     from time_list tl
     join stop_list sl on sl.stop_item_id = tl.stop_item_id
                      and sl.lag_stop_item_id = tl.lag_stop_item_id
    group by tl.hour_from, sl.stop_item_id, sl.lag_stop_item_id,
             case when l_ct.is_capacity_dependent then tl.tr_capacity_id else 1 end;
end;
$$ language plpgsql;


create or replace function asd.calc_speed_bs_pkg$clean_task
  (p_calc_task_id bigint)
returns void 
-- ========================================================================
-- Удалить задачу
-- ========================================================================
as $$ declare
begin
   delete from asd.ct_speed_bs_result where calculation_task_id = p_calc_task_id;
end;
$$ language plpgsql;

