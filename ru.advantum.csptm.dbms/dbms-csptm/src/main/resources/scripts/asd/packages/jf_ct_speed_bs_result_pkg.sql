﻿CREATE OR REPLACE FUNCTION asd."jf_ct_speed_bs_result_pkg$attr_to_rowtype"(p_attr text)
 RETURNS asd.ct_speed_bs_result
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.ct_speed_bs_result%rowtype; 
begin 
   l_r.ct_speed_bs_result_id := jofl.jofl_pkg$extract_number(p_attr, 'ct_speed_bs_result_id', true); 
   l_r.calculation_task_id := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true);
   l_r.stop_item_1_id = jofl.jofl_pkg$extract_varchar(p_attr, 'stop_item_1_id', true);
   l_r.stop_item_1_id = jofl.jofl_pkg$extract_varchar(p_attr, 'stop_item_1_id', true);
   l_r.hour_from := jofl.jofl_pkg$extract_varchar(p_attr, 'hour_from', true);
   l_r.hour_to := jofl.jofl_pkg$extract_varchar(p_attr, 'hour_to', true);
   l_r.avg_speed := jofl.jofl_pkg$extract_varchar(p_attr, 'avg_speed', true);
   l_r.tr_capacity_id := jofl.jofl_pkg$extract_number(p_attr, 'tr_capacity_id', true);
   return l_r;
end;
 $function$
;
-------------------
	
create or replace function asd.jf_ct_speed_bs_result_pkg$of_rows
  (p_id_account   in  numeric, 
   p_attr         in  text,
   p_rows         out refcursor)
returns refcursor
as $$ declare
   l_r             asd.jf_calc_speed_bs_type;
   l_r_calc_task   asd.calculation_task%rowtype;
begin
   l_r := asd.jf_ct_speed_between_stops_pkg$attr_to_rowtype(p_attr);
   l_r_calc_task := l_r.r_calc_task;
   
   open p_rows for
   select res.calculation_task_id, res.ct_speed_bs_result_id, res.hour_from, res.hour_to, res.avg_speed,
          to_char(res.hour_from + (3 || ' hour')::interval, 'HH24:MI') || '-' || to_char(res.hour_to + (3 || ' hour')::interval, 'HH24:MI') hour_from_to,
          s1.name as stop_1_name,
          s2.name as stop_2_name,
          sir1.order_num as order_num_1,
          sir2.order_num as order_num_2,
          (select short_name from core.tr_capacity t1 where t1.tr_capacity_id = res.tr_capacity_id) tr_capacity
     from asd.ct_speed_bs_result res
     join asd.ct_speed_between_stops bs on bs.calculation_task_id = res.calculation_task_id
     join rts.stop_item sp1 on sp1.stop_item_id = res.stop_item_1_id
     join rts.stop_item sp2 on sp2.stop_item_id = res.stop_item_2_id
     join rts.stop_location sl1 on sl1.stop_location_id = sp1.stop_location_id
     join rts.stop_location sl2 on sl2.stop_location_id = sp2.stop_location_id
     join rts.stop s1 on s1.stop_id = sl1.stop_id
     join rts.stop s2 on s2.stop_id = sl2.stop_id
     join rts.stop_item2round sir1 on sir1.round_id = bs.round_id
                                  and sir1.stop_item_id = res.stop_item_1_id
     join rts.stop_item2round sir2 on sir2.round_id = bs.round_id
                                  and sir2.stop_item_id = res.stop_item_2_id
    where res.calculation_task_id = l_r_calc_task.calculation_task_id
    order by order_num_1, res.hour_from;
end;
$$ language plpgsql;
