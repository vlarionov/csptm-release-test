
do $$
begin
  if not exists(select * from pg_type where typname = 'jf_calc_norm_type') THEN

    CREATE TYPE asd.jf_calc_norm_type AS
      (r_norm asd.ct_normative,
       r_calc_task asd.calculation_task
      );

    ALTER TYPE asd.jf_calc_norm_type
      OWNER TO adv;

  end if;
end$$;

CREATE OR REPLACE FUNCTION asd."jf_ct_normative_pkg$attr_to_rowtype"(p_attr text)
 RETURNS asd.jf_calc_norm_type
 LANGUAGE plpgsql
AS $function$
declare
  l_r_return asd.jf_calc_norm_type;
  l_r_norm asd.ct_normative%rowtype;
  l_r_calc_task asd.calculation_task%rowtype;
begin
  l_r_norm.calculation_task_id:= jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true);
  l_r_norm.calc_period_begin:= jofl.jofl_pkg$extract_date(p_attr, 'calc_period_begin', true);
  l_r_norm.calc_period_end = jofl.jofl_pkg$extract_date(p_attr, 'calc_period_end', true);
  l_r_norm.usr_base_inter_race :=jofl.jofl_pkg$extract_number(p_attr, 'usr_base_inter_race', true);
  l_r_norm.usr_min_inter_race :=jofl.jofl_pkg$extract_number(p_attr, 'usr_min_inter_race', true);
  l_r_norm.usr_stop_parking_time :=jofl.jofl_pkg$extract_number(p_attr, 'usr_stop_parking_time', true);
  l_r_norm.is_active :=jofl.jofl_pkg$extract_boolean(p_attr, 'is_active', true);

  l_r_calc_task.calculation_task_id := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true);
  l_r_calc_task.account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', true);
  l_r_calc_task.ct_status_id := jofl.jofl_pkg$extract_varchar(p_attr, 'ct_status_id', true);
  l_r_calc_task.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true);
  l_r_calc_task.time_create := jofl.jofl_pkg$extract_date(p_attr, 'time_create', true);
  l_r_calc_task.time_calc_begin := jofl.jofl_pkg$extract_date(p_attr, 'time_calc_begin', true);
  l_r_calc_task.time_calc_end := jofl.jofl_pkg$extract_date(p_attr, 'time_calc_end', true);
  l_r_calc_task.ct_type_id := jofl.jofl_pkg$extract_number(p_attr, 'ct_type_id', true);
  l_r_calc_task.detail_capacity := jofl.jofl_pkg$extract_boolean(p_attr, 'detail_capacity', true);
  l_r_calc_task.hour_period := jofl.jofl_pkg$extract_number(p_attr, 'hour_period', true);

  l_r_return.r_norm := l_r_norm;
  l_r_return.r_calc_task := l_r_calc_task;


  return l_r_return;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_normative_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare
  l_time_calc_end TEXT[] := jofl.jofl_pkg$extract_tarray(p_attr, 'F_time_calc_end', true);
  l_time_calc_end_date DATE[] := l_time_calc_end::DATE[];

  l_calculation_task_id BIGINT[] := jofl.jofl_pkg$extract_narray(p_attr, 'F_calculation_task_id', true);
begin
  open p_rows for
  select ct.calculation_task_id,
         ct.account_id,
         ct.ct_status_id,
         ct.name,
         ct.time_create,
         cn.calc_period_begin,
         cn.calc_period_end,
         to_char(cn.calc_period_begin+ interval '3 hour', 'DD.MM.YYYY') || '-' || to_char(cn.calc_period_end + interval '3 hour', 'DD.MM.YYYY') date_interval,
         ct.time_calc_begin,
         ct.time_calc_end,
         cs.name status_name,
		     ct.detail_capacity,
		     cn.is_active,
         cn.usr_stop_parking_time,
         cn.usr_min_inter_race,
         cn.usr_base_inter_race,
         ct.ct_type_id,
         ct.hour_period,
         case
           when ct.ct_status_id = asd.calculation_task_pkg$cn_ct_status_new()
             THEN 'P{A},E{OF_UPDATE,OF_CALC},P{D},D{asd.week_day2ct,asd.tr_capacity2ct,asd.transport_kind2ct,asd.route_variant2ct,asd.params2ct}'
		       when ct.ct_status_id = asd.calculation_task_pkg$cn_ct_status_ready()
             THEN 'P{A},E{OF_ACTIVATE},P{D},D{asd.week_day2ct2,asd.tr_capacity2ct2,asd.transport_kind2ct2,asd.route_variant2ct2,asd.params2ct2}'
		  ELSE ''
         END AS "ROW$POLICY"
  from asd.calculation_task ct
       join asd.ct_normative cn on cn.calculation_task_id = ct.calculation_task_id
       join asd.ct_status cs on cs.ct_status_id = ct.ct_status_id
  WHERE (ct.time_calc_end::DATE = ANY(l_time_calc_end_date) OR array_length(l_time_calc_end_date,1) IS NULL)
  AND (ct.calculation_task_id = ANY(l_calculation_task_id) OR array_length(l_calculation_task_id,1) IS NULL);
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_normative_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
  l_r_return asd.jf_calc_norm_type;
  l_r_norm asd.ct_normative%rowtype;
  l_r_calc_task asd.calculation_task%rowtype;
  l_res text;
begin
  l_r_return := asd.jf_ct_normative_pkg$attr_to_rowtype(p_attr);
  l_r_norm:= l_r_return.r_norm ;
  l_r_calc_task:= l_r_return.r_calc_task ;

  if l_r_calc_task.ct_status_id!=asd.calculation_task_pkg$cn_ct_status_new() then
    return null;
  end if;

  l_res :=  asd.jf_calculation_task_pkg$of_update(p_id_account, row_to_json(l_r_calc_task)::text);

  
  update asd.ct_normative 
  set calc_period_begin = l_r_norm.calc_period_begin,
      calc_period_end = l_r_norm.calc_period_end,
      usr_stop_parking_time = l_r_norm.usr_stop_parking_time,
      usr_min_inter_race =  l_r_norm.usr_min_inter_race,
      usr_base_inter_race= l_r_norm.usr_base_inter_race
  where calculation_task_id = l_r_norm.calculation_task_id;

  return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_normative_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
  l_r_return asd.jf_calc_norm_type;
  l_r_norm asd.ct_normative%rowtype;
  l_r_calc_task asd.calculation_task%rowtype;
  l_res text;
  l_norm_id BIGINT;
begin
  l_r_return := asd.jf_ct_normative_pkg$attr_to_rowtype(p_attr);
  l_r_norm:= l_r_return.r_norm ;
  l_r_calc_task:= l_r_return.r_calc_task ;

  /*if l_r_calc_task.ct_status_id!=asd.calculation_task_pkg$cn_ct_status_new() then
    return null;
  end if;*/
  SELECT norm_id INTO l_norm_id FROM ttb.norm WHERE calculation_task_id = l_r_norm.calculation_task_id;
  l_res := ttb.jf_norm_pkg$of_delete(p_id_account, json_build_object('norm_id',l_norm_id)::TEXT );

  perform asd.calc_norm_pkg$clean_task(l_r_norm.calculation_task_id);

  delete
  from asd.hours2ct
  where calculation_task_id = l_r_norm.calculation_task_id;

  delete
  from asd.week_day2ct
  where calculation_task_id = l_r_norm.calculation_task_id;
 
 delete
  from asd.route2ct
  where calculation_task_id = l_r_norm.calculation_task_id;
 
 delete
  from asd.tr_type2ct
  where calculation_task_id = l_r_norm.calculation_task_id;
 
  delete
  from asd.tr_capacity2ct
  where calculation_task_id = l_r_norm.calculation_task_id;
 
 delete
  from asd.params2ct
  where calculation_task_id = l_r_norm.calculation_task_id;
  
  delete
  from asd.ct_normative
  where calculation_task_id = l_r_norm.calculation_task_id;

  l_res:= asd.jf_calculation_task_pkg$of_delete(p_id_account, row_to_json(l_r_calc_task)::text);


  return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_normative_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
  l_r_return asd.jf_calc_norm_type;
  l_r_norm asd.ct_normative%rowtype;
  l_r_calc_task asd.calculation_task%rowtype;
  l_res text;
begin
  l_r_return := asd.jf_ct_normative_pkg$attr_to_rowtype(p_attr);
  l_r_norm:= l_r_return.r_norm ;
  l_r_calc_task:= l_r_return.r_calc_task ;


  l_r_norm.calculation_task_id:= nextval( 'asd.calculation_task_calculation_task_id_seq' );
  l_r_norm.is_active:=false;
  l_r_norm.usr_base_inter_race:=case  l_r_norm.usr_base_inter_race when -1 then asd.get_constant('BASE_INTER_RACE_STOP') else  l_r_norm.usr_base_inter_race end;
  l_r_norm.usr_min_inter_race:= case  l_r_norm.usr_min_inter_race when -1 then asd.get_constant('MIN_INTER_RACE_STOP') else  l_r_norm.usr_min_inter_race end;
  l_r_calc_task.calculation_task_id:= l_r_norm.calculation_task_id;
  l_r_calc_task.account_id := p_id_account;
  l_r_calc_task.time_create :=now();
  l_r_calc_task.ct_status_id :=0; --новый
  l_r_calc_task.ct_type_id:= asd.calculation_task_pkg$cn_ct_type_normative();
  l_r_calc_task.detail_capacity:=0;
  l_res :=  asd.jf_calculation_task_pkg$of_insert(p_id_account, row_to_json(l_r_calc_task)::text);

  insert into asd.ct_normative select l_r_norm.*;

  return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_normative_pkg$of_calc"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
  l_r_return asd.jf_calc_norm_type;
  l_r_norm asd.ct_normative%rowtype;
  l_r_calc_task asd.calculation_task%rowtype;
  l_cnt integer;
begin
  l_r_return := asd.jf_ct_normative_pkg$attr_to_rowtype(p_attr);
  l_r_norm:= l_r_return.r_norm ;
  l_r_calc_task:= l_r_return.r_calc_task ;
  if l_r_calc_task.ct_status_id!=asd.calculation_task_pkg$cn_ct_status_new() then
     return null;
  end if;

  select count(1) into l_cnt
  from asd.week_day2ct wd2ct
  where wd2ct.calculation_task_id = l_r_calc_task.calculation_task_id;
  if l_cnt = 0 then
    raise exception '<<Не выбраны дни недели для расчета>>';
  end if;

  select count(1) into l_cnt
  from asd.tr_type2ct tr2ct
  where tr2ct.calculation_task_id = l_r_calc_task.calculation_task_id;
  if l_cnt = 0 then
    raise exception '<<Не выбраны виды ТС для расчета>>';
  end if;

  select count(1) into l_cnt
  from asd.route_variant2ct r2ct
  where r2ct.calculation_task_id = l_r_calc_task.calculation_task_id;
  if l_cnt = 0 then
    raise exception '<<Не выбраны варианты маршрута для расчета>>';
  end if;

  select count(1) into l_cnt
  from asd.tr_capacity2ct vt2ct
  where vt2ct.calculation_task_id = l_r_calc_task.calculation_task_id;
  if l_cnt = 0 then
    raise exception '<<Не выбраны вместимости ТС для расчета>>';
  end if;

  update asd.calculation_task
  set ct_status_id=asd.calculation_task_pkg$cn_ct_status_queued()
  where calculation_task_id=l_r_calc_task.calculation_task_id;


  return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_normative_pkg$of_activate"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
  l_r_return asd.jf_calc_norm_type;
  l_r_norm asd.ct_normative%rowtype;
  l_r_calc_task asd.calculation_task%rowtype;

begin
  l_r_return := asd.jf_ct_normative_pkg$attr_to_rowtype(p_attr);
  l_r_norm:= l_r_return.r_norm ;
  l_r_calc_task:= l_r_return.r_calc_task ;
  
  if l_r_calc_task.ct_status_id!=asd.calculation_task_pkg$cn_ct_status_ready() then
     return null;
  end if;

update asd.ct_normative  
set is_active=true
where calculation_task_id= l_r_calc_task.calculation_task_id;


  return null;
end;
$function$
;