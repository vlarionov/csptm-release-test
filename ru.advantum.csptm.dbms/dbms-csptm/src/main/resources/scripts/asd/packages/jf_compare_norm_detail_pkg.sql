CREATE OR REPLACE FUNCTION asd."jf_compare_norm_detail_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare
    l_norm_id BIGINT := jofl.jofl_pkg$extract_number(p_attr, 'norm_id', true);
    l_calculation_task_id BIGINT:= jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true);
    l_utc_offset INTERVAL := ttb.get_utc_offset();
begin
  open p_rows for
  wITH stop_item_names AS (
      select stop_item_id, name from rts.stop_item
        JOIN rts.stop_location USING(stop_location_id)
        JOIN rts.stop USING(stop_id)
  ),
      t2 AS(
        select
          /*l_r.norm_id,*/
          c.tr_type_id,
          c.tr_capacity_id,
          sir_1.stop_item_id si1,
          sir_2.stop_item_id si2,
          (c.hour_from::time + l_utc_offset)::TIME hour_from,
          (c.hour_to::time + l_utc_offset)::TIME hour_to,
          coalesce( asd.round_seconds2min(((avg(c.norm_duration)/60::real)*60)::NUMERIC), 0) rounded,
          n1.name name1,
          n2.name name2
        --ttb.norm_edit_type$auto()
        from asd.ct_stop_visit_result c
          join rts.stop_item2round sir_1 on sir_1.stop_item2round_id = c.stop_item2round_1_id
          join rts.stop_item2round sir_2 on sir_2.stop_item2round_id = c.stop_item2round_2_id
          JOIN stop_item_names n1 on n1.stop_item_id = sir_1.stop_item_id
          JOIN stop_item_names n2 on n2.stop_item_id = sir_2.stop_item_id
        where c.calculation_task_id = l_calculation_task_id
        group by c.tr_type_id,
          c.tr_capacity_id,
          sir_1.stop_item_id,
          sir_2.stop_item_id,
          c.hour_from,
          c.hour_to,
          n1.name,
          n2.name)
  select t2.hour_from::TEXT, t2.hour_to::TEXT, t2.name1, t2.name2,
         t2.rounded, nbs.between_stop_dur, cap.short_name,
         CASE WHEN t2.rounded <> nbs.between_stop_dur THEN true
              else FALSE
         END is_different
  from t2
    JOIN ttb.norm_between_stop nbs ON nbs.hour_from = t2.hour_from
                                      AND nbs.hour_to = t2.hour_to
                                      AND nbs.stop_item_1_id = t2.si1
                                      AND nbs.stop_item_2_id = t2.si2
                                      AND nbs.norm_id = l_norm_id
    JOIN core.tr_capacity cap ON t2.tr_capacity_id = cap.tr_capacity_id;
end;
$function$;