﻿/*возвращает id варианта маршрута за на указанное время*/
CREATE OR REPLACE FUNCTION asd.utils_pkg$get_route_var_id(p_dt TIMESTAMP , p_tr_id NUMERIC )
  RETURNS NUMERIC
LANGUAGE plpgsql
STABLE
AS $function$
declare
  l_res NUMERIC;
begin

  select
    tte.route_variant_muid
  into l_res
  from asd.oper_round_visit orv
    join tt.order_round ord on ord.order_round_id = orv.order_round_id
    join tt.order_list ol on ol.order_list_id= ord.order_list_id and ol.sign_deleted = 0
    join tt.timetable_entry tte on tte.timetable_entry_id = ol.timetable_entry_id
  where
    p_dt between orv.time_fact_begin and orv.time_fact_end
    and ol.tr_id = p_tr_id
    and orv.order_round_status_id in (1,3,4) --пройден, выполняется, пройден частично
  group by tte.route_variant_muid;

  return l_res;
end;
$function$
;


-------------------------------------------------------------------------------------------------------------------------
/*возвращает массив id вариантов маршрута за период*/
CREATE OR REPLACE FUNCTION asd.utils_pkg$get_route_var_id_array(p_dt_b TIMESTAMP , p_dt_e TIMESTAMP , p_tr_id NUMERIC )
  RETURNS table(route_variant_muid BIGINT)
LANGUAGE plpgsql
AS $function$
declare
begin
  return query
   select
      tte.route_variant_muid
    into l_res
    from asd.oper_round_visit orv
      join tt.order_round ord on ord.order_round_id = orv.order_round_id
      join tt.round rd on rd.round_id = ord.round_id
      join tt.timetable_entry tte on tte.timetable_entry_id = rd.timetable_entry_id
      join tt.order_list ol on ol.order_list_id= ord.order_list_id

    where
      (p_dt_b, p_dt_e) overlaps (orv.time_fact_begin ,orv.time_fact_end)
      and ol.tr_id = p_tr_id
      and orv.order_round_status_id in (1,3,4) --пройден, выполняется, пройден частично

    group by tte.route_variant_muid;

  --return l_res;

end;
$function$
;


-------------------------------------------------------------------------------------------------------------------------
/*возвращает строку вариантов маршрута за период*/
CREATE OR REPLACE FUNCTION asd.utils_pkg$get_route_var_num_text(p_dt_b TIMESTAMP , p_dt_e TIMESTAMP , p_tr_id NUMERIC )
  RETURNS text
LANGUAGE plpgsql
AS $function$
declare
  l_res text;

begin
select array_to_string(ARRAY_AGG(r.number ),',')
into l_res
from asd.utils_pkg$get_route_var_id_array(p_dt_b, p_dt_e, p_tr_id) t
  join gis.route_variants rv on rv.muid = t.route_variant_muid
  join gis.routes r on r.muid= rv.route_muid;

  return l_res;
end;


$function$
;