drop function asd.graph_linker_pkg$get_route_trajectories(p_route_trajectory_muids bigint [] );
drop function asd.graph_linker_pkg$get_graph_sections(p_route_trajectory_muids bigint [] );
drop function asd.graph_linker_pkg$get_stop_places(p_route_trajectory_muids bigint [] );
drop function asd.graph_linker_pkg$get_timetables(p_order_date tt.order_list.order_date%type, p_tr_id core.tr.tr_id%type, p_only_planned bool );
drop function asd.graph_linker_pkg$clean_links( p_order_date tt.order_list.order_date%type, p_tr_id core.tr.tr_id%type );

create or replace function asd.graph_linker_pkg$get_route_trajectories(
    p_route_trajectory_muids bigint []
)
    returns table(
        route_trajectory_muid gis.route_trajectories.muid%type,
        length                double precision,
        start_offset          gis.graph_sec2route_traj.graph_section_start_offset%type,
        geometry              gis.route_trajectories.wkt_geom%type
    ) as
$$
begin
    return query
    select
        rt.muid,
        rt.length * 1000 length,
        case when rt.muid = 3082250979181809966
            then 258.131
        else gsrt.graph_section_start_offset end start_offset,
        rt.wkt_geom geometry
    from gis.route_trajectories rt
        left join gis.graph_sec2route_traj gsrt on rt.muid = gsrt.route_trajectory_muid and gsrt.order_num = 1
    where rt.muid = any (p_route_trajectory_muids);
end;
$$
language 'plpgsql'
volatile
called on null input
security invoker;

create or replace function asd.graph_linker_pkg$get_graph_sections(
    p_route_trajectory_muids bigint []
)
    returns table(
        graph_section_muid    gis.graph_sections.muid%type,
        route_trajectory_muid gis.route_trajectories.muid%type,
        order_num             gis.graph_sec2route_traj.order_num%type,
        length                gis.graph_sections.length%type,
        trajectory_offset     double precision,
        geometry              gis.graph_sections.wkt_geom%type
    ) as
$$
begin
    return query
    select
        gs.muid as graph_section_muid,
        gsrt.route_trajectory_muid,
        gsrt.order_num,
        gs.length graph_section_length,
        sum(gs.length)
        over (
            partition by gsrt.route_trajectory_muid
            order by gsrt.order_num ) - gs.length trajectory_offset,
        gs.wkt_geom graph_section_geometry
    from gis.graph_sec2route_traj gsrt
        join gis.graph_sections gs on gsrt.graph_section_muid = gs.muid
    where gsrt.route_trajectory_muid = any (p_route_trajectory_muids)
    order by
        gsrt.route_trajectory_muid,
        gsrt.order_num;
end;
$$
language 'plpgsql'
volatile
called on null input
security invoker;

create or replace function asd.graph_linker_pkg$get_stop_places(
    p_route_trajectory_muids bigint []
)
    returns table(
        stop_place_muid       gis.stop_places.muid%type,
        route_trajectory_muid gis.route_trajectories.muid%type,
        order_num             gis.stop_place2route_traj.order_num%type,
        trajectory_offset     double precision
    ) as
$$
begin
    return query
    select
        sprt.stop_place_muid,
        sprt.route_trajectory_muid,
        sprt.order_num,
        sum(sprt.length_sector)
        over (
            partition by sprt.route_trajectory_muid
            order by sprt.order_num ) * 1000 trajectory_offset
    from gis.stop_place2route_traj sprt
    where sprt.route_trajectory_muid = any (p_route_trajectory_muids)
    order by
        sprt.route_trajectory_muid,
        sprt.order_num;
end;
$$
language 'plpgsql'
volatile
called on null input
security invoker;

create or replace function asd.graph_linker_pkg$get_timetables(
    p_order_date   tt.order_list.order_date%type,
    p_tr_id        core.tr.tr_id%type,
    p_only_planned bool
)
    returns table(
        tr_id                 core.tr.tr_id%type,
        route_trajectory_muid gis.route_trajectories.muid%type,
        order_round_id        tt.order_round.order_round_id%type,
        time_plan_begin       asd.oper_round_visit.time_plan_begin%type,
        time_plan_end         asd.oper_round_visit.time_plan_end%type,
        is_active             tt.order_round.is_active%type
    ) as
$$
begin
    return query
    select
        ol.tr_id,
        r.route_trajectory_muid,
        ord.order_round_id,
        orv.time_plan_begin,
        orv.time_plan_end,
        ord.is_active
    from tt.order_list ol
        join tt.order_round ord on ol.order_list_id = ord.order_list_id
        join tt.round r on ord.round_id = r.round_id
        join asd.oper_round_visit orv on ord.order_round_id = orv.order_round_id
    where ol.order_date = p_order_date
          and (p_tr_id is null or ol.tr_id = p_tr_id)
          and (not p_only_planned or orv.order_round_status_id = tt.const_pkg$ord_status_planned())
          and ol.tr_id is not null
          and ol.is_active = 1
          and ord.is_active = 1
    order by
        ol.tr_id,
        orv.time_plan_begin;
end;
$$
language 'plpgsql'
volatile
called on null input
security invoker;

create or replace function asd.graph_linker_pkg$get_updated_timetables(
    p_order_round_ids bigint []
)
    returns table(
        tr_id                 core.tr.tr_id%type,
        route_trajectory_muid gis.route_trajectories.muid%type,
        order_round_id        tt.order_round.order_round_id%type,
        time_plan_begin       asd.oper_round_visit.time_plan_begin%type,
        time_plan_end         asd.oper_round_visit.time_plan_end%type,
        is_active             tt.order_round.is_active%type
    ) as
$$
begin
    return query
    select
        ol.tr_id,
        r.route_trajectory_muid,
        ord.order_round_id,
        orv.time_plan_begin,
        orv.time_plan_end,
        ord.is_active
    from tt.order_list ol
        join tt.order_round ord on ol.order_list_id = ord.order_list_id
        join tt.round r on ord.round_id = r.round_id
        join asd.oper_round_visit orv on ord.order_round_id = orv.order_round_id
    where ord.order_round_id = any (p_order_round_ids)
          and ol.tr_id is not null
    order by
        ol.tr_id,
        orv.time_plan_begin;
end;
$$
language 'plpgsql'
volatile
called on null input
security invoker;

create or replace function asd.graph_linker_pkg$clean_links(
    p_order_date tt.order_list.order_date%type,
    p_tr_id      core.tr.tr_id%type
)
    returns text as
$$
declare
    l_deleted_links    int;
    l_deleted_sections int;
    l_deleted_stops    int;
    l_nullified_rounds int;
begin
    perform 1
    from tt.order_list ol
        join tt.order_round ord on ol.order_list_id = ord.order_list_id
        join asd.oper_round_visit orv on ord.order_round_id = orv.order_round_id
    where ol.order_date = p_order_date
          and ol.tr_id = p_tr_id
    for update nowait;

    delete from asd.tf_graph_link tgl
    using tt.order_list ol
        join tt.order_round ord on ol.order_list_id = ord.order_list_id
        join asd.oper_round_visit orv on ord.order_round_id = orv.order_round_id
    where ol.order_date = p_order_date
          and ol.tr_id = p_tr_id
          and ol.tr_id = tgl.tr_id
          and tgl.event_time between orv.time_fact_begin and orv.time_fact_end
          and orv.order_round_id = tgl.order_round_id;

    get diagnostics l_deleted_links = row_count;

    delete from asd.graph_section_speed gss
    using tt.order_list ol
        join tt.order_round ord on ol.order_list_id = ord.order_list_id
        join asd.oper_round_visit orv on ord.order_round_id = orv.order_round_id
    where ol.order_date = p_order_date
          and ol.tr_id = p_tr_id
          and ol.tr_id = gss.tr_id
          and gss.order_round_id = orv.order_round_id
          and gss.begin_time between orv.first_graph_section and orv.last_graph_section;

    get diagnostics l_deleted_sections = row_count;

    delete from tt.order_fact orf
    using tt.order_list ol
        join tt.order_round ord on ol.order_list_id = ord.order_list_id
    where ol.order_date = p_order_date
          and ol.tr_id = p_tr_id
          and orf.order_round_id = ord.order_round_id;

    get diagnostics l_deleted_stops = row_count;

    update asd.oper_round_visit orv
    set time_fact_begin       = null,
        packet_id_begin       = null,
        time_fact_end         = null,
        packet_id_end         = null,
        first_graph_section   = null,
        last_graph_section    = null,
        order_round_status_id = case ord.is_active
                                when 1
                                    then tt.const_pkg$ord_status_planned()
                                when 0
                                    then tt.const_pkg$ord_status_canceled()
                                end
    from tt.order_list ol
        join tt.order_round ord on ol.order_list_id = ord.order_list_id
    where ol.order_date = p_order_date
          and ol.tr_id = p_tr_id
          and ord.order_round_id = orv.order_round_id;

    get diagnostics l_nullified_rounds = row_count;

    return json_build_object('deleted_links', l_deleted_links,
                             'deleted_sections', l_deleted_sections,
                             'deleted_stops', l_deleted_stops,
                             'nullified_rounds', l_nullified_rounds) :: text;
end;
$$
language 'plpgsql'
security invoker;

create or replace function asd.graph_linker_pkg$request_recalc(
    p_order_date tt.order_list.order_date%type,
    p_tr_id      core.tr.tr_id%type
)
    returns void as
$$
begin
    perform paa.paa_api_pkg$basic_publish(cmnd.jf_cmnd_ref4unit_pkg$cn_command_rmq_broker(),
                                          '',
                                          'graph-linker-offline',
                                          json_build_object('orderDate', p_order_date :: date,
                                                            'trId', p_tr_id) :: text,
                                          'RECALC_REQUEST');
end;
$$
language 'plpgsql'
security invoker;
