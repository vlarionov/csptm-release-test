create or replace function asd.calculation_task_pkg$cn_ct_status_new() returns smallint language sql as $$select 0::smallint$$;
create or replace function asd.calculation_task_pkg$cn_ct_status_queued() returns smallint language sql as $$select 1::smallint$$;
create or replace function asd.calculation_task_pkg$cn_ct_status_in_progress() returns smallint language sql as $$select 2::smallint$$;
create or replace function asd.calculation_task_pkg$cn_ct_status_ready() returns smallint language sql as $$select 3::smallint$$;
create or replace function asd.calculation_task_pkg$cn_ct_status_error() returns smallint language sql as $$select 4::smallint$$;

create or replace function asd.calculation_task_pkg$cn_ct_type_gs_speed() returns smallint language sql as $$select 1::smallint$$;
create or replace function asd.calculation_task_pkg$cn_ct_type_normative() returns smallint language sql as $$select 2::smallint$$;
create or replace function asd.calculation_task_pkg$cn_ct_type_stop_speed() returns smallint language sql as $$select 3::smallint$$;
create or replace function asd.calculation_task_pkg$cn_ct_type_normative_sched() returns smallint language sql as $$select 4::smallint$$;

create or replace function asd.calculation_task_pkg$calc_tasks()
    returns void
as $$
declare
    l_calc_task record;
    l_ct_status_id asd.ct_status.ct_status_id%type;
begin
    select * into l_calc_task
    from asd.calculation_task
    where ct_status_id = asd.calculation_task_pkg$cn_ct_status_queued()
    order by calculation_task_id
    limit 1;

    if l_calc_task is null then
        return;
    end if;

    update asd.calculation_task
    set time_calc_begin = clock_timestamp(),
        time_calc_end = null,
        ct_status_id = asd.calculation_task_pkg$cn_ct_status_in_progress()
    where calculation_task_id = l_calc_task.calculation_task_id;

    delete from asd.params2ct where calculation_task_id = l_calc_task.calculation_task_id;

    begin
        case l_calc_task.ct_type_id
            when asd.calculation_task_pkg$cn_ct_type_gs_speed() then
                perform asd.calc_gs_speed_pkg$clean_task(l_calc_task.calculation_task_id);
                perform asd.calc_gs_speed_pkg$calc_task(l_calc_task.calculation_task_id);
            when asd.calculation_task_pkg$cn_ct_type_normative() then
                perform asd.calc_norm_pkg$clean_task(l_calc_task.calculation_task_id);
                perform asd.calc_norm_pkg$fill_round_stop_result(l_calc_task.calculation_task_id);
            when asd.calculation_task_pkg$cn_ct_type_stop_speed() then
                perform asd.calc_speed_bs_pkg$clean_task(l_calc_task.calculation_task_id);
                perform asd.calc_speed_bs_pkg$calc_result(l_calc_task.calculation_task_id);
            when asd.calculation_task_pkg$cn_ct_type_normative_sched() then
                -- delete
                -- insert
            else raise notice 'bad cs_type %', l_calc_task.ct_type_id;
        end case;
        l_ct_status_id := asd.calculation_task_pkg$cn_ct_status_ready();
    exception
        when others then
            raise notice '% % %', l_calc_task, SQLERRM, SQLSTATE;
            l_ct_status_id := asd.calculation_task_pkg$cn_ct_status_error();
    end;

    insert into asd.params2ct(calculation_task_id, name, description, value)
        select l_calc_task.calculation_task_id, c.name, c.description, c.value
        from asd.constants c
        where c.subsystem ='ASD';

    update asd.calculation_task
    set time_calc_end = clock_timestamp(),
        ct_status_id = l_ct_status_id
    where calculation_task_id = l_calc_task.calculation_task_id;
end;$$
language plpgsql;

create or replace function asd.calculation_task_pkg$queue_task(p_calculation_task_id in asd.calculation_task.calculation_task_id%type)
    returns void
as $$
declare
begin
    update asd.calculation_task
    set ct_status_id = asd.calculation_task_pkg$cn_ct_status_queued()
    where calculation_task_id = p_calculation_task_id;
end;$$
language plpgsql;