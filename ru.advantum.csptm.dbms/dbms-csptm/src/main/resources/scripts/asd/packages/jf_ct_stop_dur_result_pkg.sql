CREATE OR REPLACE FUNCTION asd."jf_ct_stop_dur_result_pkg$attr_to_rowtype"(p_attr text)
 RETURNS asd.ct_stop_dur_result
 LANGUAGE plpgsql
AS $function$ declare l_r asd.ct_stop_dur_result%rowtype;
begin

    l_r.stop_duration := jofl.jofl_pkg$extract_number(p_attr, 'stop_duration',  true);
    -- l_r.route_trajectory_muid := jofl.jofl_pkg$extract_number(p_attr, 'route_trajectory_muid', true);
    l_r.tr_capacity_id :=  jofl.jofl_pkg$extract_number(p_attr, 'tr_capacity_id', true);
    --l_r.stop_place_order_num := jofl.jofl_pkg$extract_number(p_attr,  'stop_place_order_num', true);
    --l_r.stop_place_muid := jofl.jofl_pkg$extract_number(p_attr, 'stop_place_muid', true);
    l_r.tr_type_id :=  jofl.jofl_pkg$extract_number(p_attr, 'tr_type_id', true);
    l_r.hour_to := jofl.jofl_pkg$extract_date(p_attr, 'hour_to', true);
    l_r.hour_from  := jofl.jofl_pkg$extract_date(p_attr, 'hour_from', true);
    --l_r.week_day_id := jofl.jofl_pkg$extract_varchar(p_attr, 'week_day_id', true);
    l_r.calculation_task_id := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true);
  return l_r;
end; $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_stop_dur_result_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare
    l_calculation_task_id asd.ct_stop_dur_result.calculation_task_id%type := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true);
    l_tr_type_id  asd.ct_stop_dur_result.tr_type_id%type:=  jofl.jofl_pkg$extract_number(p_attr, 'tr_type_id', true);
    l_hour_to     asd.ct_stop_dur_result.hour_to%type:= jofl.jofl_pkg$extract_date(p_attr, 'hour_to', true);
    l_hour_from   asd.ct_stop_dur_result.hour_from%type:= jofl.jofl_pkg$extract_date(p_attr, 'hour_from', true);
    l_round_id    asd.ct_stop_dur_result.round_id%type:= jofl.jofl_pkg$extract_number(p_attr, 'round_id', true);
    l_tr_capacity_id asd.ct_stop_dur_result.tr_capacity_id%type:=  jofl.jofl_pkg$extract_number(p_attr, 'tr_capacity_id', true);

    l_names TEXT[] := jofl.jofl_pkg$extract_tarray(p_attr, 'F_stop_name', true);
begin

  open p_rows for
  select c.ct_stop_visit_result_id,
    c.calculation_task_id,
    s.name stop_name,
    sir.order_num as stop_order_num,
    c.stop_duration
  from asd.ct_stop_dur_result c
    join rts.stop_item2round sir on sir.stop_item2round_id = c.stop_item2round_id
    join rts.stop_item si on si.stop_item_id = sir.stop_item_id
    join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
    join rts.stop s on s.stop_id = sl.stop_id
  where c.calculation_task_id = l_calculation_task_id
        and  c.hour_from = l_hour_from
        and  c.hour_to = l_hour_to
        and  c.round_id = l_round_id
        and  c.tr_capacity_id = l_tr_capacity_id
        and c.tr_type_id = l_tr_type_id
        AND (s.name = ANY(l_names) OR array_length(l_names,1) IS NULL)
  order by sir.order_num;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_stop_dur_result_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
 l_r asd.ct_stop_dur_result%rowtype;
 l_is_active boolean;
 l_status_id integer;
begin
   l_r := asd.jf_ct_stop_dur_result_pkg$attr_to_rowtype(p_attr);

select cn.is_active, c.ct_status_id
into l_is_active,l_status_id
from asd.calculation_task c
join asd.ct_normative cn on cn.calculation_task_id=c.calculation_task_id
where c.calculation_task_id=l_r.calculation_task_id;

if l_is_active is not true
and l_status_id!=asd.calculation_task_pkg$cn_ct_status_ready()
then
   RAISE exception '<<Норматив не активен!>>';
end if;

   update asd.ct_stop_dur_result_pkg set
   stop_duration = l_r.stop_duration
   where ct_stop_visit_result_id = l_r.ct_stop_visit_result_id;

   return null;
end;
$function$
;