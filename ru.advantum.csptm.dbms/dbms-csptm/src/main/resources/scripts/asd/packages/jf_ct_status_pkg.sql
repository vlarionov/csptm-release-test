CREATE OR REPLACE FUNCTION asd."jf_ct_status_pkg$attr_to_rowtype"(p_attr text)
 RETURNS asd.ct_status
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.ct_status%rowtype; 
begin 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 
   l_r.comment := jofl.jofl_pkg$extract_varchar(p_attr, 'comment', true); 
   l_r.ct_status_id := jofl.jofl_pkg$extract_varchar(p_attr, 'ct_status_id', true); 

   return l_r;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_status_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$ 
declare 
begin 
 open p_rows for 
      select 
        name, 
        comment, 
        ct_status_id
      from asd.ct_status; 
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_status_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.ct_status%rowtype;
begin 
   l_r := asd.jf_ct_status_pkg$attr_to_rowtype(p_attr);

   /*update asd.ct_status set 
          name = l_r.name, 
          comment = l_r.comment
   where 
          ct_status_id = l_r.ct_status_id;
*/
   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_status_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.ct_status%rowtype;
begin 
   l_r := asd.jf_ct_status_pkg$attr_to_rowtype(p_attr);

   --delete from  asd.ct_status where  ct_status_id = l_r.ct_status_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_ct_status_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.ct_status%rowtype;
begin 
   l_r := asd.jf_ct_status_pkg$attr_to_rowtype(p_attr);

   --insert into asd.ct_status select l_r.*;

   return null;
end;
 $function$
;