CREATE OR REPLACE FUNCTION asd."jf_calculation_task_pkg$attr_to_rowtype"(p_attr text)
 RETURNS asd.calculation_task
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.calculation_task%rowtype; 
begin 
   l_r.calculation_task_id := jofl.jofl_pkg$extract_number(p_attr, 'calculation_task_id', true); 
   l_r.account_id := jofl.jofl_pkg$extract_number(p_attr, 'account_id', true); 
   l_r.ct_status_id := jofl.jofl_pkg$extract_varchar(p_attr, 'ct_status_id', true); 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 
   l_r.time_create := jofl.jofl_pkg$extract_date(p_attr, 'time_create', true); 
   l_r.time_calc_begin := jofl.jofl_pkg$extract_date(p_attr, 'time_calc_begin', true);
   l_r.time_calc_end := jofl.jofl_pkg$extract_date(p_attr, 'time_calc_end', true); 
   l_r.ct_type_id := jofl.jofl_pkg$extract_varchar(p_attr, 'ct_type_id', true); 
   l_r.detail_capacity := jofl.jofl_pkg$extract_boolean(p_attr, 'detail_capacity', true);
   l_r.hour_period := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'hour_period', true),60);

   return l_r;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_calculation_task_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$ 
declare 
begin 
 open p_rows for 
      select 
        calculation_task_id, 
        account_id, 
        ct_status_id, 
        name, 
        time_create, 
        time_calc_begin,
        time_calc_end, 
        ct_type_id, 
        detail_capacity,
        hour_period
      from asd.calculation_task; 
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_calculation_task_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.calculation_task%rowtype;
begin 
   l_r := asd.jf_calculation_task_pkg$attr_to_rowtype(p_attr);
   
   update asd.calculation_task set 
          account_id = l_r.account_id,
          --ct_status_id = l_r.ct_status_id,
          name = l_r.name,
          time_create = l_r.time_create, 
          time_calc_begin = l_r.time_calc_begin,
          time_calc_end = l_r.time_calc_end, 
          ct_type_id = coalesce(l_r.ct_type_id, ct_type_id),
          detail_capacity = coalesce(l_r.detail_capacity, detail_capacity),
          hour_period = l_r.hour_period
   where 
          calculation_task_id = l_r.calculation_task_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_calculation_task_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.calculation_task%rowtype;
begin 
   l_r := asd.jf_calculation_task_pkg$attr_to_rowtype(p_attr);

   delete from  asd.calculation_task where  calculation_task_id = l_r.calculation_task_id;

   return null;
end;
 $function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_calculation_task_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$ 
declare 
   l_r asd.calculation_task%rowtype;
begin 
   l_r := asd.jf_calculation_task_pkg$attr_to_rowtype(p_attr);

   insert into asd.calculation_task select l_r.*;

   return null;
end;
 $function$
;