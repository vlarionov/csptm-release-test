create or replace function asd."rep_speed_on_route_period$of_rows"(p_id_account numeric, out p_rows refcursor, p_attr text)
    returns refcursor
language plpgsql
as $function$
declare
    l_f_depo_id         text := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_depo_id', true), -1);
    l_f_dt              text := coalesce(jofl.jofl_pkg$extract_varchar(p_attr, 'f_DT', true), '');
    l_f_hh_b            text := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'F_TIME_BGN_INPLACE_K', true), -1);
    l_f_hh_e            text := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'F_TIME_END_INPLACE_K', true), -1);
    l_f_route_id        text := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_route_id', true), -1);
    l_f_b_order_num     text := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_b_order_num', true), -1);
    l_f_e_order_num     text := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'f_e_order_num', true), -1);
    l_move_direction_id text := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'F_DIR_INPLACE_K', true), -1);
    l_timezone          text := coalesce(jofl.jofl_pkg$extract_number(p_attr, 'P_TIMEZONE', true), -1);
    l_action_type_id    smallint;
    l_ts                timestamp :=to_timestamp(l_f_dt, 'yyyy-mm-dd hh24:mi:ss') + (l_timezone || ' hour') :: interval;
    l_date              date :=l_ts :: date;
begin
    select case when l_move_direction_id = 1 :: text
        then 37 :: smallint
           else 38 :: smallint end
    into l_action_type_id;
    open p_rows for
    with stop_item_num as (
        select
            tv.tt_variant_id,
            si2r.order_num,
            si2r.stop_item_id,
            r.round_num,
            route.route_num,
            r.move_direction_id,
            md.move_direction_name,
            tv.order_date,
            rv.route_variant_id,
            si2r.stop_item2round_id,
            si2r.length_sector,
            r.code,
            rs.name,
            ctt.name                           tr_name,
            ce.name_full,
            first_value(rs.name)
            over (
                partition by route.route_num
                order by si2r.order_num )      start_stop_name,
            first_value(rs.name)
            over (
                partition by route.route_num
                order by si2r.order_num desc ) end_stop_name
        from ttb.tt_variant tv
            join rts.route_variant rv on tv.route_variant_id = rv.route_variant_id
            join rts.round r on rv.route_variant_id = r.route_variant_id
            join rts.move_direction md on r.move_direction_id = md.move_direction_id
            join rts.stop_item2round si2r on r.round_id = si2r.round_id
            join rts.stop_item si on si2r.stop_item_id = si.stop_item_id
            join rts.stop_location sl on si.stop_location_id = sl.stop_location_id
            join rts.stop rs on sl.stop_id = rs.stop_id
            join rts.route route on rv.route_variant_id = route.current_route_variant_id
            join core.tr_type ctt on route.tr_type_id = ctt.tr_type_id
            join rts.depo2route d2r on route.route_id = d2r.route_id
            join core.depo cd on d2r.depo_id = cd.depo_id
            join core.entity ce on cd.depo_id = ce.entity_id

        where 1 = 1
              and tv.parent_tt_variant_id notnull
              and r.move_direction_id = l_move_direction_id :: smallint
              and cd.depo_id = l_f_depo_id :: bigint
              and route.route_id = l_f_route_id :: integer
              and tv.order_date = l_date
              and r.code = '00'
        order by order_num
    )
        , time_between_stop as (
        select
            sin.order_num,
            sin.stop_item_id,
            tto.tt_out_id,
            tai.time_begin :: time                                                                                       time_stop_end,
            lag(tai.time_begin)
            over (
                partition by tta.tt_action_id
                order by tto.tt_out_id, tai.time_begin, sin.round_num, sin.move_direction_id, sin.order_num ) :: time as time_stop_begin,
            sin.round_num,
            sin.order_date,
            sin.stop_item2round_id,
            ctc.short_name
        from stop_item_num sin
            join ttb.tt_out tto on sin.tt_variant_id = tto.tt_variant_id
            join core.tr_capacity ctc on tto.tr_capacity_id = ctc.tr_capacity_id

            join ttb.tt_action tta on tto.tt_out_id = tta.tt_out_id
            join ttb.tt_action_item tai on tta.tt_action_id = tai.tt_action_id and sin.stop_item2round_id = tai.stop_item2round_id
        where 1 = 1
              and tta.action_type_id = l_action_type_id
              and tto.parent_tt_out_id notnull
              and tai.time_begin :: time between l_f_hh_b :: integer * '1 h' :: interval and l_f_hh_e :: integer * '1 h' :: interval
        order by tto.tt_out_id, tai.time_begin, sin.round_num, sin.move_direction_id, sin.order_num
    )
        , avg_time as (select
                           tbs.order_num,
                           tbs.short_name,
                           tbs.stop_item_id,
                           tbs.stop_item2round_id,
                           avg(extract(epoch from (tbs.time_stop_end - tbs.time_stop_begin))) avg_time
                       from time_between_stop tbs
                       where tbs.order_num between l_f_b_order_num :: integer and l_f_e_order_num :: integer
                       group by tbs.order_num, tbs.stop_item_id, tbs.stop_item2round_id, tbs.short_name
                       order by tbs.order_num)
    select

        tr_name                                                                                                 as rtk_name,
        name_full                                                                                               as prk_name,
        to_char(l_ts, 'dd.mm.yyyy')                                                                             as dt,
        'С ' || l_f_hh_b || ' по ' || l_f_hh_e                                                                  as period,
        sin.route_num                                                                                           as r_number,
        'C ' || sin.start_stop_name || ' по ' || sin.end_stop_name                                              as st_segment,
        sin.move_direction_name                                                                                 as direction_name,
        coalesce(ca.last_name, '') || ' ' || coalesce(ca.first_name, '') || ' ' || coalesce(ca.middle_name, '') as fio,
        sin.code                                                                                                as rr_code,
        sin.order_num :: text                                                                                   as st_order_num,
        sin.name                                                                                                as st_name,
        a.short_name                                                                                            as capacity_name,
        case when round(sin.length_sector :: numeric * 3.6 :: numeric / a.avg_time :: numeric, 2) > 120
                  or (sin.length_sector :: numeric * 3.6 :: numeric / a.avg_time) isnull
            then 0 :: text
        else round(sin.length_sector :: numeric * 3.6 :: numeric / a.avg_time :: numeric, 2) :: text end        as speed
    from stop_item_num sin
        join core.account ca on ca.account_id = p_id_account
        join avg_time a on a.stop_item_id = sin.stop_item_id
    order by sin.order_num;
end;
$function$;