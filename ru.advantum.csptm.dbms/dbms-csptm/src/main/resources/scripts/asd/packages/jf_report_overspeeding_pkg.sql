CREATE OR REPLACE FUNCTION asd.jf_report_overspeeding_pkg$of_rows(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare

/*
 "f_end_DT":"2018-01-17 21:00:00.0",
{"f_start_DT":"2018-01-17 21:00:00.0",
"f_list_depo_id":"$array[498,504,500,493,506,502,501,499,503,495,494,497,492,505,496]",
 "f_route_muid":"$array[2994039322485557164,3131818787355665443,3326452145007594535,2994039258031436021,3357454857611452895,3149674203462781273,2994039252640472764,2994039262592908648,3374574941536934477]",
 "f_list_stop_id":"$array[2,3,4,5,6]",
 "f_list_tr_id":"$array[114103,114104,114105,114106,114107,114108,114109]",
 "f_list_driver_id":"$array[21398,20166,11633,10566]",
 "f_maxspeed_NUMBER":"50"}
*/

  lf_rep_start timestamp := jofl.jofl_pkg$extract_date(p_attr, 'f_start_DT', true);
  lf_rep_end timestamp := jofl.jofl_pkg$extract_date(p_attr, 'f_end_DT', true);
  lf_depo_arr smallint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_depo_id', true);
  lf_route_arr bigint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_route_muid', true);
  lf_stop_arr smallint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_stop_id', true); 
  -- lf_stop bigint := jofl.jofl_pkg$extract_number(p_attr, 'f_stop_id', true);
  lf_vehicle_arr smallint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_tr_id', true);
  lf_driver_arr smallint[] := jofl.jofl_pkg$extract_narray(p_attr, 'f_list_driver_id', true); 
  --  lf_driver text := jofl.jofl_pkg$extract_varchar(p_attr, 'F_DRIVER_TEXT', true);
  lf_max_speed smallint := jofl.jofl_pkg$extract_number(p_attr, 'f_maxspeed_NUMBER', true);

begin
/*
 -- новый набор полей
  open p_rows for
select
null as tp_name, --  "V" Наименование ТП
null as tr_type, --  "V" Вид ТС
null as route_number, --  "V" Номер маршрута
null as stop_name, --  "V" Наименование ОП
null as tr_licence, --  "V" Гос.номер ТС
null::smallint as garage_num, --  "N" Гар. номер ТС
null as tr_driver, --  "V" Водитель
null::timestamp as time_fact, --  "T" Дата и время
null::numeric as actual_speed, --  "F" Факт. скорость
null::numeric as speeding_value, --  "F" Значение превышения скорости
null::smallint as speeding_count; --  "N" Количество случаев нарушений
*/


 open p_rows for
  with stop_distance as (/* детальные расстояния между соседними остановочными путктами траектории */
                         select si2gf.stop_place_muid as gis_stop_place_muid,
                                si2gt.stop_place_muid as gis_stop_place_muid_prev,
                                rv2g.route_variant_muid,
                                route_data.*
                         from (SELECT r.route_id,
                                      r.tr_type_id,
                                      r.route_num,
                                      rv.route_variant_id,
                                      rd.round_id,
                                      rd.move_direction_id,
                                      rd.action_type_id,
                                      rd.code,
                                      rd.round_num,
                                      si2r.stop_item_id,
                                      si2r.order_num,
                                      lag(si2r.stop_item_id) OVER (PARTITION BY si2r.round_id ORDER BY si2r.order_num ) AS prev_stop_item_id,
                                      si2r.length_sector
                               FROM rts.route r
                                    JOIN rts.route_variant rv ON rv.route_id = r.route_id
                                    JOIN rts.round rd ON rd.route_variant_id = rv.route_variant_id
                                    JOIN rts.stop_item2round si2r ON si2r.round_id = rd.round_id
                               WHERE  not r.sign_deleted
                                      and not rv.sign_deleted
                                      and not rd.sign_deleted and rd.code = '00'
                                      and not si2r.sign_deleted) route_data
                          join rts.stop_item2gis si2gf on si2gf.stop_item_id = route_data.stop_item_id
                          join rts.stop_item2gis si2gt on si2gt.stop_item_id = route_data.prev_stop_item_id
                          join rts.route_variant2gis rv2g on rv2g.route_variant_id = route_data.route_variant_id),
       time_data as (/* фактические данные по прохождению остановок */
                     select time_fact.*,
                            row_number() over () as rn
                     from (SELECT tt_orf.order_round_id,
                                  tt_orf.stop_place_muid,
                                  tt_orf.time_fact,
                                  tt_orf.stop_order_num,
                                  lag(tt_orf.stop_place_muid) OVER (PARTITION BY tt_orf.order_round_id ORDER BY tt_orf.stop_order_num ) AS prev_stop_place_muid,
                                  lag(tt_orf.time_fact) OVER (PARTITION BY tt_orf.order_round_id ORDER BY tt_orf.stop_order_num ) AS prev_time_fact,
               					  case extract(epoch from tt_orf.time_fact - lag(tt_orf.time_fact) OVER (PARTITION BY tt_orf.order_round_id ORDER BY tt_orf.stop_order_num )) < 10
				                   when true then null
				                   else extract(epoch from tt_orf.time_fact - lag(tt_orf.time_fact) OVER (PARTITION BY tt_orf.order_round_id ORDER BY tt_orf.stop_order_num ))
				                  end as transit_sec,
                                  tt_or_rd.order_list_id,
                                  tt_or_rd.order_round_num,
                                  tt_rd.direction_id,
                                  tt_rd.round_code,
                                  tt_rd.round_type,
                                  tt_or_ls.driver_id,
                                  tt_or_ls.tr_id,
                                  tt_tt_en.route_variant_muid,
                                  tt_rd.route_trajectory_muid,
                                  round((asd.get_constant('DEFAULT_SPEED')/60)*3.6) as default_speed_kmph
                           FROM tt.order_fact tt_orf
                            JOIN tt.order_round tt_or_rd ON tt_or_rd.order_round_id = tt_orf.order_round_id
                            JOIN tt.round tt_rd ON tt_rd.round_id = tt_or_rd.round_id
                            JOIN tt.order_list tt_or_ls ON tt_or_ls.order_list_id = tt_or_rd.order_list_id
                            JOIN tt.timetable_entry tt_tt_en ON tt_tt_en.timetable_entry_id = tt_or_ls.timetable_entry_id
                            join core.tr tr on tr.tr_id = tt_or_ls.tr_id
                           WHERE tt_orf.sign_deleted = 0
                                 AND tt_or_rd.is_active = 1
                                 AND tt_or_ls.is_active = 1 AND tt_or_ls.sign_deleted = 0
                                 AND tt_rd.round_code = '00'
                                 /* фильтр по интервалу времени от ... до ... */
                                 AND tt_orf.time_fact BETWEEN lf_rep_start and lf_rep_end
                                 /* фильтр по транспортному средству (по госномеру или по гаражному номеру) */
                                 and (array_length(lf_vehicle_arr, 1) is null or tt_or_ls.tr_id = any (lf_vehicle_arr))
                                 /* фильтр по Траспортному предприятию */
                                 and (array_length(lf_depo_arr, 1) is null or tr.depo_id = any (lf_depo_arr))) time_fact
                     where time_fact.prev_stop_place_muid is not null)
 select report_table.tp_name,
        report_table.tr_type,
        report_table.route_number,
        report_table.tr_driver,
        -- report_table.driver_id,
        report_table.tr_licence,
        report_table.garage_num,
        report_table.order_round_num,
 	      min(report_table.time_fact) as time_fact,
        report_table.stop_name,
        round(avg(report_table.actual_speed))::smallint as actual_speed,
        /* считаем превышение фактической скорости над максимальной (указанной в фильтре или взятой из константны) */
        case sign(round(avg(report_table.actual_speed)) - report_table.max_speed)
         when 1 then (round(avg(report_table.actual_speed)) - report_table.max_speed)
         when 0 then null
         else null
        end::smallint as speeding_value,
        /* считаем количество фактов превышения скорости */
        case count(report_table.speeding_value)
         when 0 then NULL
         else count(report_table.speeding_value)
        end ::smallint as speeding_count
 from (select depo.name_full as tp_name,
              trtp.name as tr_type,
              sd.route_num as route_number,
              drv.fio_tn as tr_driver,
			  drv.driver_id as driver_id,
              vhl.licence as tr_licence,
              vhl.garage_num as garage_num,
              td.order_list_id,
              td.order_round_id,
              td.order_round_num,
 	            td.prev_time_fact as time_fact,
              case  --  coalesce(lf_stop, -1)
               when array_length(lf_stop_arr, 1) is null then null -- -1 then null
               else s.name
              end as stop_name,
              /* рассчитываем скорость движдения, максимальную скорость и величину превышения */
              round(sd.length_sector / td.transit_sec * 3.6) as actual_speed,
              coalesce(lf_max_speed, td.default_speed_kmph) as max_speed,
              CASE round(sd.length_sector / td.transit_sec * 3.6) > coalesce(lf_max_speed, td.default_speed_kmph)
               when true then round(sd.length_sector / td.transit_sec * 3.6) - coalesce(lf_max_speed, td.default_speed_kmph)
               else null
              end::smallint as speeding_value
       from stop_distance sd
        join time_data td on td.stop_place_muid = sd.gis_stop_place_muid and
                             td.prev_stop_place_muid = sd.gis_stop_place_muid_prev AND
                             td.route_variant_muid = sd.route_variant_muid and
                             td.direction_id = sd.move_direction_id and
                             td.stop_order_num = sd.order_num
        join core.tr vhl on vhl.tr_id = td.tr_id
        join core.tr_type trtp on trtp.tr_type_id = vhl.tr_type_id
        join core.entity depo on depo.entity_id = vhl.depo_id
        join (select tt_dr.*,
                     rtrim(tt_dr.driver_last_name ||' '|| tt_dr.driver_name ||' '|| tt_dr.driver_middle_name) ||', '||tt_dr.tab_num as fio_tn
              from tt.driver tt_dr) drv on drv.driver_id = td.driver_id
        join rts.stop_item si on si.stop_item_id = sd.prev_stop_item_id and
                                 si.tr_type_id = vhl.tr_type_id
        join rts.stop_location sl on sl.stop_location_id = si.stop_location_id
        join rts.stop s on s.stop_id = sl.stop_id
       where /* фильтр по маршрутам (с учетом вида транспорта маршрута) */
             (array_length(lf_route_arr, 1) IS NULL OR
              exists (select 1
                      from rts.route2gis f_r2g
                           join rts.route f_rrt on f_rrt.route_id = f_r2g.route_id
                      where f_r2g.route_muid = ANY (lf_route_arr)
                            and f_rrt.route_num = sd.route_num
                            and f_rrt.tr_type_id = sd.tr_type_id))
             /* фильтр по остановке */
            and /* coalesce(lf_stop, s.stop_id) = s.stop_id */
			    ((array_length(lf_stop_arr, 1) is null and s.stop_id = s.stop_id) or s.stop_id = any(lf_stop_arr))) report_table
 where /* фильтр по водителю */
       (array_length(lf_driver_arr, 1) is null or report_table.driver_id = any(lf_driver_arr))
	   -- lf_driver is null or upper(report_table.tr_driver) like '%'||upper(lf_driver)||'%')
group by report_table.tp_name,
         report_table.tr_type,
         report_table.route_number,
         report_table.tr_driver,
		 report_table.driver_id,
         report_table.tr_licence,
         report_table.garage_num,
         report_table.order_list_id,
         report_table.order_round_id,
         report_table.order_round_num,
         case
          when array_length(lf_stop_arr, 1) is null then null
          else report_table.time_fact
         end,
         report_table.stop_name,
         report_table.max_speed;

end;
$function$
;
