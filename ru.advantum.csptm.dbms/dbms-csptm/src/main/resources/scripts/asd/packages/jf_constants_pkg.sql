CREATE OR REPLACE FUNCTION asd."jf_constants_pkg$attr_to_rowtype"(p_attr text)
 RETURNS asd.constants
 LANGUAGE plpgsql
AS $function$
declare 
   l_r asd.constants%rowtype; 
begin 
   l_r.description := jofl.jofl_pkg$extract_varchar(p_attr, 'description', true); 
   l_r.value := jofl.jofl_pkg$extract_number(p_attr, 'value', true); 
   l_r.constant_id := jofl.jofl_pkg$extract_number(p_attr, 'constant_id', true); 
   l_r.name := jofl.jofl_pkg$extract_varchar(p_attr, 'name', true); 
   l_r.subsystem := jofl.jofl_pkg$extract_varchar(p_attr, 'subsystem', true);

   return l_r;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_constants_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare 
begin 
 open p_rows for 
      select
        subsystem,
        description, 
        value, 
        constant_id, 
        name
      from asd.constants c
	  order by c.constant_id; 
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_constants_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare 
   l_r asd.constants%rowtype;
begin 
   l_r := asd.jf_constants_pkg$attr_to_rowtype(p_attr);

   IF l_r.name = 'TIME_FACT_ERROR' THEN
     IF l_r.value > 30 OR l_r.value < 0 THEN
       RAISE EXCEPTION '<<Значение должно быть в диапазоне 0...30 сек.>>';
     END IF;
   END IF;
   update asd.constants set
          description = l_r.description,
          value = l_r.value,
          name = l_r.name,
          subsystem = l_r.subsystem
   where 
          constant_id = l_r.constant_id;

   return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_constants_pkg$of_delete"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare 
   l_r asd.constants%rowtype;
begin 
   l_r := asd.jf_constants_pkg$attr_to_rowtype(p_attr);

   delete from  asd.constants where  constant_id = l_r.constant_id;

   return null;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_constants_pkg$of_insert"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
  l_r asd.constants%rowtype;
begin
  l_r := asd.jf_constants_pkg$attr_to_rowtype(p_attr);
  insert into asd.constants(name, description, value, subsystem)
  values (l_r.name, l_r.description, l_r.value, l_r.subsystem);

  return null;
end;
$function$
;