CREATE OR REPLACE FUNCTION asd."jf_visual_review_finished_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare
begin
  open p_rows for
  select r.review_id,
         r.s_name,
         r.insert_date,
         r.review_date,
         r.route_id::text,
         r.route_name,
         r.route_round_type,
         r.route_round_type_name,
         r.status
  from asd.visual_review_report r
  where r.status = 2 ;
end;
$function$
;