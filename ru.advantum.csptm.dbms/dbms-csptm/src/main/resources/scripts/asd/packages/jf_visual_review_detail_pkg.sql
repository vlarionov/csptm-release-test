﻿CREATE OR REPLACE FUNCTION asd."jf_visual_review_detail_pkg$attr_to_rowtype"(p_attr text)
 RETURNS asd.visual_review_detail
 LANGUAGE plpgsql
AS $function$
declare
 l_r asd.visual_review_detail%rowtype;
begin



  l_r.visual_review_item_id := jofl.jofl_pkg$extract_number(p_attr, 'visual_review_item_id', true);
  l_r.stop_id := jofl.jofl_pkg$extract_number(p_attr, 'stop_id', true);
   l_r.stop_sequence := jofl.jofl_pkg$extract_number(p_attr, 'stop_sequence',
     true);
    l_r.delay_reason :=  jofl.jofl_pkg$extract_varchar(p_attr, 'delay_reason',
      true);
	  l_r.visual_review_detail_id:=  jofl.jofl_pkg$extract_number(p_attr, 'visual_review_detail_id', true);
  

select cast (jofl.jofl_pkg$extract_number(p_attr, 'time_arrival_fact', true) ||
  ' seconds' as interval),
       cast (jofl.jofl_pkg$extract_number(p_attr, 'stop_duration', true) ||
         ' seconds' as interval),
       cast (jofl.jofl_pkg$extract_number(p_attr, 'delay_duration', true) ||
         ' seconds' as interval)
into l_r.time_arrival_fact,
     l_r.stop_duration,
     l_r.delay_duration;


    

  return l_r;
end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_visual_review_detail_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare
begin
  open p_rows for
  select d.visual_review_detail_id,
   d.visual_review_item_id,
         d.stop_id,
         d.stop_name,
     --    extract(epoch from d.time_arrival_plan) time_arrival_plan,
       extract(epoch
  from d.time_arrival_fact) time_arrival_fact,
       extract(epoch
  from d.stop_duration) stop_duration,
       extract(epoch
  from d.delay_duration) delay_duration,
       d.delay_reason,
       d.stop_sequence,
         case
           when r.status in (0,1) THEN 'P{A},E{OF_UPDATE,OF_EXP2EXCEL}'
          ELSE 'P{A},E{OF_EXP2EXCEL}'
         END AS "ROW$POLICY"
  from asd.visual_review_detail d
  join asd.visual_review_item vri on vri.visual_review_item_id=d.visual_review_item_id
  join asd.visual_review_report r on r.review_id=vri.review_id
  where d.visual_review_item_id = jofl.jofl_pkg$extract_number(p_attr, 'visual_review_item_id', true)
  order by d.stop_sequence;

end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."jf_visual_review_detail_pkg$of_update"(p_id_account numeric, p_attr text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare

  l_r asd.visual_review_detail%rowtype;
l_status asd.visual_review_report.status%type;
begin
  l_r := asd.jf_visual_review_detail_pkg$attr_to_rowtype(p_attr);

select v.status
into l_status
from asd.visual_review_item vri
join asd.visual_review_report v on v.review_id=vri.review_id
where vri.visual_review_item_id=l_r.visual_review_item_id;

if l_status=2 then 
return null;
end if;


update asd.visual_review_detail  set
  time_arrival_fact=l_r.time_arrival_fact,
  stop_duration =l_r.stop_duration,
  delay_duration =l_r.delay_duration,
  delay_reason =l_r.delay_reason
where visual_review_detail_id=l_r.visual_review_detail_id;
 

  return null;
end;
$function$
;