CREATE OR REPLACE FUNCTION asd."calc_norm_pkg$clean_task"(p_calc_task_id bigint)
 RETURNS void
 LANGUAGE plpgsql
AS $function$
    declare
    begin
        delete from asd.ct_stop_visit_result where calculation_task_id = p_calc_task_id;
        delete from asd.ct_stop_dur_result where calculation_task_id = p_calc_task_id;
        delete from asd.ct_round_stop_result where calculation_task_id = p_calc_task_id;

    end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd."calc_norm_pkg$make_tmp_data"(p_calc_task_id bigint)
 RETURNS void
 LANGUAGE plpgsql
AS $function$
    declare
        l_ct record;
        l_ct_hour record;
        l_hour_from time without time zone;
        l_hour_to time without time zone;
        l_cross_day boolean;
    begin
        select  norm.calc_period_begin::date as calc_period_begin, norm.calc_period_end::date as calc_period_end
        into l_ct
        from asd.calculation_task as ct
        join asd.ct_normative norm on norm.calculation_task_id = ct.calculation_task_id
        where ct.calculation_task_id = p_calc_task_id;


    DISCARD TEMP;

    CREATE TEMPORARY TABLE tmp_data4calc (
    time_fact  timestamp
   ,stop_order_num smallint
   ,stop_place_muid bigint
   ,order_round_num bigint
   ,tr_type_id bigint
   ,timetable_entry_id bigint
   ,route_variant_id bigint
   ,tr_capacity_id bigint
   ,direction_id bigint
   ,route_trajectory_muid bigint
   ,round_id bigint
   ,tr_id  bigint
   ,week_day_id bigint
   ,min_stop_order_num bigint
   ,max_stop_order_num bigint
   ,order_round_id bigint
   ,lag_time_fact timestamp
   ,last_stop_time_fact timestamp
   ,door_count smallint
   ,calc_period_begin date
   ,calc_period_end date

    ) ON COMMIT PRESERVE ROWS
   TABLESPACE asd_data;

    insert into tmp_data4calc
      WITH t1 AS(
      select
        oo.time_fact
        ,oo.stop_order_num
        ,oo.stop_place_muid
        ,ord.order_round_num
        ,tr.tr_type_id
        ,tte.timetable_entry_id
        --,tt.route_muid
        ,tte.route_variant_muid
        ,tm.tr_capacity_id
        ,rd.direction_id
        ,rd.route_trajectory_muid
        ,rnd2g.round_id
        ,ol.tr_id
        ,extract(ISODOW FROM ol.order_date) as week_day_id
        ,min(oo.stop_order_num) over (partition by ord.order_round_id) as min_stop_order_num
        ,(select count(1) from  gis.stop_place2route_traj sp2rt where  sp2rt.route_trajectory_muid =rd.route_trajectory_muid) as max_stop_order_num
        ,ord.order_round_id
        ,lag( oo.time_fact  ) over (partition by ol.order_list_id,  ol.tr_id  order by oo.time_fact ) as lag_time_fact
        ,case stop_order_num when 1 then  (select max(f.time_fact) from tt.order_fact f where  f.order_round_id = oo.order_round_id and f.sign_deleted = 0 ) else null end  as last_stop_time_fact
        ,tm.door_count
        ,l_ct.calc_period_begin
        ,l_ct.calc_period_end
        , max(stop_order_num) OVER (PARTITION BY oo.order_round_id) max_on_order_round
      from tt.order_fact oo
        join tt.order_round ord on ord.order_round_id = oo.order_round_id
        join tt.order_list ol on ol.order_list_id = ord.order_list_id and ol.sign_deleted = 0
        join tt.round rd on rd.round_id = ord.round_id
        join rts.round2gis rnd2g on rnd2g.route_trajectory_muid = rd.route_trajectory_muid
        join tt.timetable_entry tte on tte.timetable_entry_id = rd.timetable_entry_id
        join rts.route_variant2gis rv2g on rv2g.route_variant_muid = tte.route_variant_muid
        join core.tr tr on tr.tr_id = ol.tr_id
        join core.tr_model tm on tm.tr_model_id =tr.tr_model_id
        join asd.calculation_task ct on ct.calculation_task_id = p_calc_task_id
        join asd.ct_normative norm on norm.calculation_task_id = ct.calculation_task_id
        join asd.oper_round_visit orv on orv.order_round_id = ord.order_round_id
             AND orv.order_round_status_id = 1
    where
      ol.order_date between l_ct.calc_period_begin and l_ct.calc_period_end
      --and oo.time_fact::time between p_tm_b and  p_tm_e
      and oo.sign_deleted = 0
      and exists (select 1
                  from asd.week_day2ct wd2ct
                  where wd2ct.calculation_task_id = p_calc_task_id
                    and wd2ct.week_day_id  = extract(ISODOW FROM ol.order_date))
      and exists (select 1
                  from asd.tr_type2ct tr2ct
                  where tr2ct.calculation_task_id = p_calc_task_id
                   and  tr2ct.tr_type_id = tr.tr_type_id)
      and exists (select 1
                  from asd.route_variant2ct r2ct
                  where r2ct.calculation_task_id = p_calc_task_id
                        and r2ct.route_variant_id = rv2g.route_variant_id)
      and exists (select 1
                  from asd.tr_capacity2ct vt2ct
                  where vt2ct.calculation_task_id = p_calc_task_id
                    and vt2ct.tr_capacity_id = tm.tr_capacity_id ))
      select  time_fact
        ,stop_order_num
        ,stop_place_muid
        ,order_round_num
        ,tr_type_id
        ,timetable_entry_id
        ,route_variant_muid
        ,tr_capacity_id
        ,direction_id
        ,route_trajectory_muid
        ,round_id
        ,tr_id
        ,week_day_id
        ,min_stop_order_num
        ,max_stop_order_num
        ,order_round_id
        ,lag_time_fact
        ,last_stop_time_fact
        ,door_count
        ,calc_period_begin
        ,calc_period_end
      from t1 where max_stop_order_num = max_on_order_round;

     create index ix_tmp_data4calc_order_round_id
        on tmp_data4calc
        using btree
        (order_round_id)
      tablespace asd_idx;

    analyze tmp_data4calc;



    end;
$function$
;
-------------------
CREATE OR REPLACE FUNCTION asd.calc_norm_pkg$fill_round_stop_result(p_calc_task_id bigint)
  RETURNS void AS
$BODY$
declare
  l_dt record;
  l_res record;
  l_norm record;
  l_r_ttb_norm ttb.norm%rowtype;
  l_cnt BIGINT;
  l_calc_name asd.calculation_task.name%type;
begin
  select norm.*
  into l_norm
  from asd.ct_normative norm
  where norm.calculation_task_id = p_calc_task_id;

  /*заполняем временную таблицу данными для анализа*/
  PERFORM asd.calc_norm_pkg$make_tmp_data (p_calc_task_id);


  /*разбиваем на периоды*/
  for l_dt in (
    with setHour as (
        select (generate_series(1,(24/(ct.hour_period/60))::int, 1) * (ct.hour_period||' minute')::interval)::time as tm ,row_number() over() rn
        from  asd.calculation_task ct
        where ct.calculation_task_id= p_calc_task_id
    )
    select tm
      ,lead(tm,1,'24:00:00') over (Partition by rn order by tm) next_tm
    from setHour
    )

  loop
    /*заполняем таблицу результатов всеми возможными вариантами  */
    insert into asd.ct_round_stop_result(calculation_task_id,hour_from,hour_to,tr_type_id
                                        ,tr_capacity_id,round_id,min_inter_round_stop_dur
                                        ,reg_inter_round_stop_dur,round_duration)
      select     ct.calculation_task_id
        ,now()::date + l_dt.tm
        ,now()::date+ l_dt.next_tm
        ,tr2ct.tr_type_id
        ,vt2ct.tr_capacity_id
        ,rnd.round_id
        ,0
        ,0
        ,0
      from asd.calculation_task ct
        join asd.ct_normative norm on norm.calculation_task_id = ct.calculation_task_id
        join asd.tr_type2ct tr2ct on tr2ct.calculation_task_id = ct.calculation_task_id
        join asd.route_variant2ct r2ct on r2ct.calculation_task_id = ct.calculation_task_id
        join rts.round rnd on rnd.route_variant_id = r2ct.route_variant_id and not rnd.sign_deleted
        join asd.tr_capacity2ct vt2ct on vt2ct.calculation_task_id = ct.calculation_task_id
      where ct.calculation_task_id = p_calc_task_id
      group by  ct.calculation_task_id
        ,tr2ct.tr_type_id
        ,vt2ct.tr_capacity_id
        ,rnd.round_id ;

    analyze asd.ct_round_stop_result;

    --asd.calc_norm_pkg$fill_round_stop_result_dt(p_calc_task_id, c_dt.tm, c_dt.next_tm);

    select count(1) into l_cnt from tmp_data4calc where time_fact::time between l_dt.tm and l_dt.next_tm ;
    continue when l_cnt =0;

    /*21.02.2018 #23235
      При формировании нормативов отключить автоматический расчет минимальной межрейсовой стоянки.*/

        /*рассчитываем  значение минимальной межрейсовой стоянки*/
      /*  for l_res in (
          with
              aList as (
                select
                   time_fact-lag_time_fact as interval_round_stop
                  ,row_number() over(partition by tr_type_id, tr_capacity_id, round_id  order by  lag_time_fact - time_fact ) as rn
                  ,row_number() over(partition by tr_type_id, tr_capacity_id, round_id  order by  lag_time_fact - time_fact)/(count(1) over(partition by tr_type_id, tr_capacity_id, round_id )+1)::real*100 as prct_num
                  ,tr_type_id
                  ,route_variant_id
                  ,tr_capacity_id
                  ,round_id
                from tmp_data4calc
                where stop_order_num = 1
                   and (time_fact::time between l_dt.tm and l_dt.next_tm
                    or  lag_time_fact::time between l_dt.tm and l_dt.next_tm )
                   and extract(EPOCH from time_fact-lag_time_fact) <= asd.get_constant('MAX_INTER_RACE_STOP')--,  ttb.rv_tune_pkg$get_break_round_time(in p_route_variant_id   numeric)
            )

          select round(extract (EPOCH from avg(aList.interval_round_stop))) as min_inter_race_dur
            ,aList.tr_type_id
            ,aList.tr_capacity_id
            ,aList.round_id
            ,res.ct_round_stop_result_id
          from aList
            join asd.ct_round_stop_result res on res.calculation_task_id = p_calc_task_id
                                                 and res.hour_from::time = l_dt.tm
                                                 and res.hour_to::time = l_dt.next_tm
                                                 and res.tr_type_id = aList.tr_type_id
                                                 and res.round_id = aList.round_id
                                                 and  (aList.tr_capacity_id is null or res.tr_capacity_id=  aList.tr_capacity_id)
          where prct_num between asd.get_constant('PRC_EXTR') and asd.get_constant('PRC_EXTR') + asd.get_constant('PRC_TRVL_INTER_RACE')
          group by
            aList.tr_type_id
            ,aList.tr_capacity_id
            ,aList.round_id
            ,res.ct_round_stop_result_id)
        loop
          update asd.ct_round_stop_result
          set min_inter_round_stop_dur = l_res.min_inter_race_dur
          where ct_round_stop_result_id = l_res.ct_round_stop_result_id;

        end loop;

        update asd.ct_round_stop_result
        set min_inter_round_stop_dur = l_norm.usr_min_inter_race /*  пользовательское значение*/
        where calculation_task_id = p_calc_task_id
              and  coalesce(min_inter_round_stop_dur,0) = 0 ;
    */
        update asd.ct_round_stop_result
        set min_inter_round_stop_dur = asd.get_constant('MIN_INTER_RACE_STOP')
        where calculation_task_id = p_calc_task_id;

        /*рассчитываем значение регулировочной межрейсовой стоянки*/
        for l_res in
        with
            aSet as (
        select
        last_stop_time_fact - time_fact as duration_route

        ,row_number() over(partition by tr_type_id, tr_capacity_id, round_id  order by last_stop_time_fact - time_fact) as rn
        ,row_number() over(partition by tr_type_id, tr_capacity_id, round_id  order by  last_stop_time_fact - time_fact)/(count(1) over(partition by tr_type_id, tr_capacity_id, round_id )+1)::real*100 as prct_num
        ,tr_type_id
        ,route_variant_id
        ,tr_capacity_id
        ,round_id
        from tmp_data4calc
        where time_fact::time between l_dt.tm and l_dt.next_tm
        and  stop_order_num = 1
        )
        ,aSet_itog as (
        select *
        from aSet
        where prct_num between asd.get_constant('PRC_EXTR') and 100-asd.get_constant('PRC_EXTR')
        )
        select
          aSet.tr_type_id
          ,aSet.tr_capacity_id
          ,aSet.round_id
          ,res.ct_round_stop_result_id
          ,round(extract (EPOCH from PERCENTILE_CONT(asd.get_constant('PRC_ROUTE_MAXTIME')/100) WITHIN GROUP (ORDER BY duration_route ASC) -PERCENTILE_CONT(asd.get_constant('PRC_ROUTE_MINTIME')/100) WITHIN GROUP (ORDER BY duration_route ASC) )) as delta_dur_race
          ,round(extract (EPOCH from PERCENTILE_CONT(asd.get_constant('PRC_ROUTE_MINTIME')/100) WITHIN GROUP (ORDER BY duration_route ASC))  * asd.get_constant('COEF_CALC_INTER_RACE') ) as min_race
          ,round(extract (EPOCH from PERCENTILE_CONT(asd.get_constant('PRC_ROUTE_MINTIME')/100) WITHIN GROUP (ORDER BY duration_route ASC))) as route_dur_percentile
          ,round(extract (EPOCH from min( duration_route)))  as route_min_dur
        from aSet_itog aSet
          join asd.ct_round_stop_result res on res.calculation_task_id = p_calc_task_id
                                               and res.hour_from::time = l_dt.tm
                                               and res.hour_to::time = l_dt.next_tm
                                               and res.tr_type_id = aSet.tr_type_id
                                               and res.round_id = aSet.round_id
                                               and res.tr_capacity_id=  aSet.tr_capacity_id
        where extract (EPOCH from duration_route) >0
        group by
          aSet.tr_type_id
          ,aSet.tr_capacity_id
          ,aSet.round_id
          ,res.ct_round_stop_result_id

        loop

          update asd.ct_round_stop_result
          /*set reg_inter_round_stop_dur = case when l_res.min_race-l_res.delta_dur_race>0 and  l_res.delta_dur_race - min_inter_round_stop_dur>0 then  l_res.delta_dur_race else min_inter_round_stop_dur + l_norm.usr_base_inter_race /*+  пользовательское значение*/ end
            ,round_duration =  case when l_res.min_race-l_res.delta_dur_race>0 and  l_res.delta_dur_race - min_inter_round_stop_dur>0 then  l_res.route_dur_percentile else l_res.route_min_dur end
            */
          set reg_inter_round_stop_dur = case when l_res.min_race-l_res.delta_dur_race>0 and  l_res.delta_dur_race - min_inter_round_stop_dur>0 then  l_res.delta_dur_race - min_inter_round_stop_dur else 0 end
             ,round_duration =  case when l_res.min_race-l_res.delta_dur_race>0 then  l_res.route_dur_percentile else l_res.route_min_dur end

          where ct_round_stop_result_id = l_res.ct_round_stop_result_id;

        end loop;

        /*update asd.ct_round_stop_result
        set reg_inter_round_stop_dur = min_inter_round_stop_dur + l_norm.usr_base_inter_race/*+  пользовательское значение*/
           ,round_duration =  l_res.route_min_dur
        where calculation_task_id = p_calc_task_id
              and  coalesce(reg_inter_round_stop_dur, 0) = 0 ;*/



        /*рассчитываем нормативное значение времени, затрачиваемое на прохождение перегона  */
        for l_res in
          With
          aStop as (
              select
              extract(EPOCH FROM  (oo.time_fact - lag(oo.time_fact ) over (partition by oo.order_round_id order by oo.stop_order_num))) as stop_duration
              ,extract(EPOCH FROM  (oo.time_fact - lag(oo.time_fact ) over (partition by oo.order_round_id order by oo.stop_order_num)))/res.round_duration  as stop_dur_coeff
              ,lag(sir.stop_item2round_id) over (partition by oo.order_round_id order by oo.stop_order_num) as stop_item2round_1_id
              ,sir.stop_item2round_id  as stop_item2round_2_id
              ,oo.time_fact
              ,adata.tr_type_id
              ,adata.tr_capacity_id
              ,adata.round_id
              ,res.ct_round_stop_result_id
              ,res.round_duration
              ,res.tr_capacity_id as fff
              from tt.order_fact oo
              join /*tmp_data4calc*/
                  (with aSet as (
                      select * ,
                        row_number() over(partition by tr_type_id, tr_capacity_id, round_id  order by  last_stop_time_fact - time_fact)/
                        (count(1) over(partition by tr_type_id, tr_capacity_id, round_id )+1)::real*100 as prct_num
                      from tmp_data4calc
                      where time_fact::time between l_dt.tm and l_dt.next_tm and  stop_order_num = 1
                  )
                  select * from aSet where prct_num between asd.get_constant('PRC_EXTR') and 100-asd.get_constant('PRC_EXTR') ) adata
                  on adata.stop_order_num=1 and oo.order_round_id = adata.order_round_id
              join rts.stop_item2gis si2g on si2g.stop_place_muid = oo.stop_place_muid and si2g.tr_type_id= adata.tr_type_id
              join rts.stop_item2round sir on sir.round_id = adata.round_id and sir.stop_item_id = si2g.stop_item_id and sir.order_num = oo.stop_order_num
              join rts.stop_item si on si.stop_item_id = sir.stop_item_id
                   AND NOT si.sign_deleted
              join asd.ct_round_stop_result res on res.calculation_task_id = p_calc_task_id
              and res.hour_from::time = l_dt.tm
              and res.hour_to::time = l_dt.next_tm
              and res.tr_type_id = aData.tr_type_id
              and res.round_id = aData.round_id
              and res.tr_capacity_id= aData.tr_capacity_id
              where adata.time_fact::time between l_dt.tm and l_dt.next_tm
                and oo.sign_deleted = 0
                and res.round_duration>0
            )
          ,aCoeff as (
              select
                stop_item2round_1_id
              ,stop_item2round_2_id
              ,tr_type_id
              ,tr_capacity_id
              ,round_id
              ,ct_round_stop_result_id
              ,round_duration
              ,round(cast (PERCENTILE_CONT(asd.get_constant('PRC_ROUTE_MINTIME')/100) WITHIN GROUP (ORDER BY stop_dur_coeff ASC) as numeric),4) as stop_dur_coeff
              ,avg(stop_duration) as avg_stop_dur
              from aStop
              where stop_item2round_1_id is not null
              group by
               stop_item2round_1_id
              ,stop_item2round_2_id
              ,tr_type_id
              ,tr_capacity_id
              ,round_id
              ,ct_round_stop_result_id
              ,round_duration
            )

            Select
              stop_item2round_1_id
              ,stop_item2round_2_id
              ,aCoeff.tr_type_id
              ,aCoeff.tr_capacity_id
              ,aCoeff.round_id
              ,aCoeff.ct_round_stop_result_id
              ,aCoeff.stop_dur_coeff
              ,aCoeff.round_duration
              /*,round(
                   round_duration * case max(stop_dur_coeff) over (Partition by ct_round_stop_result_id)
                                    when stop_dur_coeff then stop_dur_coeff + sum(stop_dur_coeff) over (Partition by ct_round_stop_result_id)-1
                                    else stop_dur_coeff end)*/
              ,round(round_duration * round(stop_dur_coeff / sum(stop_dur_coeff) over (Partition by ct_round_stop_result_id), 4))
               as norm_stop_dur
              ,sir.length_sector
            from aCoeff
            join rts.stop_item2round sir on sir.stop_item2round_id = stop_item2round_2_id

          loop
            insert into asd.ct_stop_visit_result(calculation_task_id, hour_from, hour_to,
                                                 tr_type_id, tr_capacity_id, round_id,
                                                 stop_item2round_1_id, stop_item2round_2_id, ct_round_stop_result_id,
                                                 norm_duration, avg_speed                                   )
             values (p_calc_task_id,   now()::date+l_dt.tm ,now()::date+l_dt.next_tm,
                     l_res.tr_type_id, l_res.tr_capacity_id, l_res.round_id,
                     l_res.stop_item2round_1_id, l_res.stop_item2round_2_id, l_res.ct_round_stop_result_id,
                     l_res.norm_stop_dur, round(l_res.length_sector/case l_res.norm_stop_dur when 0 then 1 else l_res.norm_stop_dur end ));
          end loop;

         /*Рассчитываем Расчет продолжительности стоянки на остановочных пунктах маршрута
        Время посадки – высадки пассажиров (секунд) = ((Авош + Авыш)*tпас*kнд) / (d*Nтс)
        где:
        Авош, Авыш – количество вошедших и вышедших пассажиров, пассажиров/час - данные из подсистемы “Анализ пассажиропотоков”
        tпас – время посадки - высадки одного пассажира (tпас = 2 секунды) - настраиваемый параметр подсистемы
        kнд – коэффициент неравномерности посадки и высадки пассажиров по дверям маршрутного транспортного средства (kнд = 1,2) - настраиваемый параметр подсистемы.
        d – количество дверей для входа и выхода пассажиров.
        Nтс – интенсивность движения маршрутных ТС (автомобилей/час) - количество ТС, проходящих через ОП (без привязки к маршруту) в течение интервала расчета стоянки.*/
         Insert into asd.ct_stop_dur_result(calculation_task_id ,      hour_from ,      hour_to ,
          tr_type_id ,      tr_capacity_id ,      round_id ,    stop_item2round_id ,           stop_duration )

           select
             p_calc_task_id
             , now()::date + l_dt.tm
             , now()::date + l_dt.next_tm
             ,adata.tr_type_id
             ,adata.tr_capacity_id
             ,adata.round_id
             ,sir.stop_item2round_id
             ,coalesce( round((aai.in_corrected+aai.out_corrected)*asd.get_constant('TIME_LANDING')*asd.get_constant('COEF_UNEVENNESS')/(adata.door_count*count(1) over(partition by sp.muid))),0) as stop_dur

           from tmp_data4calc adata
             join gis.stop_places sp on sp.muid = adata.stop_place_muid
             join rts.stop_item2gis si2g on si2g.stop_place_muid = adata.stop_place_muid and si2g.tr_type_id= adata.tr_type_id
             join rts.stop_item2round sir on sir.round_id = adata.round_id and sir.stop_item_id = si2g.stop_item_id and sir.order_num = adata.stop_order_num
             join askp.asmpp_agg_item aai on aai.order_round_id = adata.order_round_id
                                             and aai.stops_muid = sp.stop_muid
                                             and aai.tr_id = adata.tr_id
                                             and time_start::date between adata.calc_period_begin and adata.calc_period_end
                                             and time_start::time>= l_dt.tm
                                             and time_start::time< l_dt.next_tm
           where adata.time_fact::time between l_dt.tm and l_dt.next_tm
              ;

      end loop;

  -- #23391 для пункта Б, минимальной и регулировочной межрейсовых стоянок = 0
  update asd.ct_round_stop_result
  set min_inter_round_stop_dur = 0, reg_inter_round_stop_dur = 0
  where calculation_task_id = p_calc_task_id
        AND round_id IN (
    select r.round_id from  asd.ct_round_stop_result rsr
      JOIN rts.round r ON r.round_id = rsr.round_id
    where calculation_task_id = p_calc_task_id
          AND move_direction_id = 1 -- 1- прямое
  );

      --на основании расчета сразу создаем норматив
      select name into l_calc_name from asd.calculation_task where calculation_task_id = l_norm.calculation_task_id;

      l_r_ttb_norm.calculation_task_id := l_norm.calculation_task_id;
      l_r_ttb_norm.norm_name := l_calc_name;
      l_r_ttb_norm.norm_desc := 'Создан из расчета '||l_r_ttb_norm.norm_name;


      perform ttb.jf_norm_pkg$of_create_from_calc(2, row_to_json(l_r_ttb_norm)::text);

    end;
    $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION asd.calc_norm_pkg$fill_round_stop_result(p_calc_task_id bigint)
OWNER TO adv;