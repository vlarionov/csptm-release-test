CREATE OR REPLACE FUNCTION asd."jf_ct_normative_filter_date_pkg$of_rows"(p_id_account numeric, OUT p_rows refcursor, p_attr text)
  RETURNS refcursor
LANGUAGE plpgsql
AS $function$
declare
begin
  open p_rows for
  select distinct on (ct.time_calc_end) to_char(ct.time_calc_end, 'YYYY-MM-DD')
    as time_calc_end from asd.calculation_task ct
  ORDER BY ct.time_calc_end ASC;
end;
$function$;