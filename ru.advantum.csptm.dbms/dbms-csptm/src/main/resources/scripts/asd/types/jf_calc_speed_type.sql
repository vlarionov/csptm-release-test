﻿-- Type: asd.jf_calc_speed_type

 --DROP TYPE asd.jf_calc_speed_type;

do $$
begin
  if not exists(select * from pg_type where typname = 'jf_calc_speed_type') THEN

    CREATE TYPE asd.jf_calc_speed_type AS
      (r_gss asd.ct_graph_section_speed,
       r_calc_task asd.calculation_task,
       r_hour asd.hours2ct
      );

    ALTER TYPE asd.jf_calc_speed_type
      OWNER TO adv;

  end if;
end$$;
