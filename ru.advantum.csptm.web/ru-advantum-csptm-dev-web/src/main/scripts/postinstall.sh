#!/bin/sh

#basedir for www
basedir=/u00/www

### x5.advantum.ru
if [ ! -d ${basedir}/x5.advantum.ru/filestore ]; then
    echo "create filestore ${basedir}/ebnd.mgt.advantum.ru/filestore"
    mkdir -p ${basedir}/ebnd.mgt.advantum.ru/filestore
    chown -R wildfly:wildfly ${basedir}/ebnd.mgt.advantum.ru/filestore
fi

if [ ! -L ${basedir}/x5.advantum.ru/dwlstore ]; then
    echo "create link for dwlstore"
    ln -s ${basedir}/ebnd.mgt.advantum.ru/filestore ${basedir}/ebnd.mgt.advantum.ru/dwlstore
fi


