/*! 
 * numeral.js language configuration
 * language : norwegian (bokmål)
 * author : Ove Andersen : https://github.com/azzlack
 */
!function(){var a={delimiters:{thousands:" ",decimal:","},abbreviations:{thousand:"k",million:"m",billion:"b",trillion:"t"},ordinal:function(a){return"."},currency:{symbol:"kr"}};"undefined"!=typeof module&&module.exports&&(module.exports=a),"undefined"!=typeof window&&this.numeral&&this.numeral.language&&(this.numeral.language("nb-no",a),this.numeral.language("nn-no",a),this.numeral.language("no",a),this.numeral.language("nb",a),this.numeral.language("nn",a))}();