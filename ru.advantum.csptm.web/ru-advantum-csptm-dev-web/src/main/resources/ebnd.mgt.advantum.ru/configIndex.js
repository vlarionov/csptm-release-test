require.config({
    baseUrl: 'src',
    waitSeconds: 20,
    paths: {
        md5: '../lib/blueimp-md5/js/md5',
        json: '../lib/json3/lib/json3',
        text: '../lib/requirejs-text/text',
        react: '../lib/react/react',
        'react-dom': '../lib/react/react-dom',
        utlConfig:  '../lib/utlconfig/utlConfig',
        jsoflAuth: '../lib/jsoflauth/jsoflAuth',
        UiLoginForm: '../lib/uiLoginForm/UiLoginForm',
        ajax: '../lib/ajax/ajax',
        'UiElLib':  '../lib/uiellib/UiElLib',
        'react-rnd': '../lib/react-rnd/react-rnd'
    }
});