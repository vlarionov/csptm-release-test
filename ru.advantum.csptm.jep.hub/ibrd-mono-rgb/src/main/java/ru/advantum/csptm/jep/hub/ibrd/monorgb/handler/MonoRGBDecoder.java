package ru.advantum.csptm.jep.hub.ibrd.monorgb.handler;

import org.jboss.netty.buffer.ChannelBuffer;
import ru.advantum.csptm.jep.hub.ibrd.ResultCodeStruct;
import ru.advantum.csptm.jep.hub.ibrd.handler.IbrdDecoder;
import ru.advantum.csptm.jep.hub.ibrd.monorgb.MonoRGBRegistrationStruct;
import ru.advantum.csptm.jep.hub.ibrd.monorgb.command.MonoRGBCommandEnum;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdErrorType;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdResponse;

public class MonoRGBDecoder extends IbrdDecoder {

    @Override

    protected Object decodeBody(int commandNumber, ChannelBuffer buffer) throws Exception {
        if (commandNumber == MonoRGBCommandEnum.REGISTRATION.getNumber()) {
            return readData(buffer, MonoRGBRegistrationStruct.class);
        } else {
            ResultCodeStruct resultCodeStruct = readData(buffer, ResultCodeStruct.class);
            IbrdErrorType errorType = getErrorType(resultCodeStruct.resultCode.get());

            Integer errorByteNumber = null;
            if (errorType == IbrdErrorType.VALUE_OUT_OF_RANGE) {
                errorByteNumber = (int) readTrailer(buffer, 1)[0];
            }

            return new IbrdResponse(commandNumber, errorType, errorByteNumber);
        }

    }

}
