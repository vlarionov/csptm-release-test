package ru.advantum.csptm.jep.hub.ibrd.monorgb.handler;

import lombok.extern.slf4j.Slf4j;
import ru.advantum.csptm.jep.hub.ibrd.DatetimeStruct;
import ru.advantum.csptm.jep.hub.ibrd.handler.IbrdLogic;
import ru.advantum.csptm.jep.hub.ibrd.monorgb.*;
import ru.advantum.csptm.jep.hub.ibrd.monorgb.command.SetMonoRGBDatetimeCommandStruct;
import ru.advantum.csptm.jep.hub.ibrd.monorgb.command.SetMonoRGBForecastCommandStruct;
import ru.advantum.csptm.jep.hub.ibrd.monorgb.command.SetMonoRGBParametersCommandStruct;
import ru.advantum.csptm.jep.hub.ibrd.monorgb.command.SetMonoRGBTemperatureCommandStruct;
import ru.advantum.csptm.jep.service.ibrd.model.*;
import ru.advantum.csptm.jep.service.ibrd.model.monorgb.BrightnessLevel;
import ru.advantum.csptm.jep.service.ibrd.model.monorgb.MonoRGBConfig;
import ru.advantum.csptm.jep.service.ibrd.model.monorgb.MonoRGBInfo;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class MonoRGBLogic extends IbrdLogic<MonoRGBHub> {

    public MonoRGBLogic(MonoRGBHub hub) {
        super(hub);
    }

    @Override
    protected void onCommand(IbrdEvent commandIbrdEvent) {
        IbrdEventType eventType = commandIbrdEvent.getEventType();
        switch (eventType) {
            case SET_DATETIME:
                LocalDateTime dateTime = commandIbrdEvent.getBoardTime();
                commander.sendCommand(new SetMonoRGBDatetimeCommandStruct(new DatetimeStruct(dateTime)), commandIbrdEvent);
                break;
            case SET_CONFIG:
                MonoRGBConfig config = commandIbrdEvent.getMonoRGBConfig();
                commander.sendCommand(new SetMonoRGBParametersCommandStruct(buildParameters(config)), commandIbrdEvent);
                break;
            case SET_INFO:
                MonoRGBInfo info = commandIbrdEvent.getMonoRGBInfo();
                commander.sendCommand(
                        new SetMonoRGBForecastCommandStruct(buildForecast(
                                info.getConfig(),
                                info.getForecastList(),
                                info.getMessageList()
                        )),
                        commandIbrdEvent
                );
                break;
            case SET_TEMPERATURE:
                int temperature = commandIbrdEvent.getTemperature();
                commander.sendCommand(new SetMonoRGBTemperatureCommandStruct(new MonoRGBTemperatureStruct(temperature)), commandIbrdEvent);
                break;
        }
    }

    private MonoRGBParametersStruct buildParameters(MonoRGBConfig config) {
        List<Integer> brightnessLevels = new ArrayList<>(24);
        for (int i = 0; i < 24; i++) {
            brightnessLevels.add(i, 32);
        }
        for (BrightnessLevel brightnessLevel : config.getBrightnessLevels()) {
            brightnessLevels.set(brightnessLevel.getHour(), (int) brightnessLevel.getValue());
        }

        return new MonoRGBParametersStruct(
                new MonoRGBFlagsStruct(true),
                config.getForecastDelay(),
                config.getRunningRowSpeed(),
                config.getDataWaitingTime(),
                brightnessLevels
        );
    }

    private MonoRGBForecastStruct buildForecast(MonoRGBConfig config, List<IbrdForecast> forecastList, List<IbrdMessage> messageList) {
        IbrdMessage message = messageList.stream()
                .findAny()
                .orElse(getEmptyMessage());
        return new MonoRGBForecastStruct(
                buildProperties(config),
                buildRunningText(message),
                config.getRowsNumber(),
                config.getRouteNumberStyle(),
                config.getArrivalTimeStyle(),
                config.getTerminalStyle(),
                buildForecastList(forecastList)
        );
    }

    private IbrdMessage getEmptyMessage() {
        return new IbrdMessage("", new IbrdColor(0, 0, 0));
    }

    private MonoRGBPropertiesStruct buildProperties(MonoRGBConfig config) {
        return new MonoRGBPropertiesStruct(
                config.isDisplayDatetime(),
                config.isDisplayTemperature(),
                true,
                config.getRunningRowPosition(),
                config.getScrollType(),
                config.getScrollDirection()
        );
    }

    private MonoRGBForecastListStruct buildForecastList(List<IbrdForecast> forecastList) {
        if (forecastList.isEmpty()) {
            forecastList.add(new IbrdForecast(null, "", null, ""));
        }
        List<MonoRGBForecastItemStruct> forecastItems = forecastList.stream().map(this::buildForecastItem).collect(Collectors.toList());
        return new MonoRGBForecastListStruct(forecastItems);
    }

    private MonoRGBForecastItemStruct buildForecastItem(IbrdForecast ibrdForecast) {
        return new MonoRGBForecastItemStruct(
                new MonoRGBTextStruct(255, 255, 255, ibrdForecast.getRouteNumber()),
                new MonoRGBTextStruct(255, 255, 255, ibrdForecast.getFormattedArrivalTime()),
                new MonoRGBTextStruct(255, 255, 255, ibrdForecast.getTerminal())
        );
    }

    private MonoRGBRunningTextStruct buildRunningText(IbrdMessage ibrdMessage) {
        String msgText = ibrdMessage.getText();
        if (msgText == null) {
            msgText = "";
        }

        return new MonoRGBRunningTextStruct(new MonoRGBTextStruct(64, 64, 64, msgText));
    }

}

