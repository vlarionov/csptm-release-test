package ru.advantum.csptm.jep.hub.ibrd.monorgb;

import org.jboss.netty.channel.ChannelHandler;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.jep.core.JepHubController;
import ru.advantum.csptm.jep.core.rmq.JepRmqHubController;
import ru.advantum.csptm.jep.hub.ibrd.IbrdHub;
import ru.advantum.csptm.jep.hub.ibrd.IbrdHubConfig;
import ru.advantum.csptm.jep.hub.ibrd.monorgb.handler.MonoRGBDecoder;
import ru.advantum.csptm.jep.hub.ibrd.monorgb.handler.MonoRGBLogic;
import ru.advantum.csptm.jep.netty.BootstrapConfig;

public class MonoRGBHub extends IbrdHub {

    public static void main(String[] args) throws Exception {
        IbrdHubConfig hubConfig = Configuration.unpackConfig(IbrdHubConfig.class, CONFIG_FILE_NAME);
        JepRmqHubController controller = new JepRmqHubController(hubConfig.hub.controllerConfig);

        MonoRGBHub hub = new MonoRGBHub(hubConfig.hub.tcpConfig, controller);
        Runtime.getRuntime().addShutdownHook(new Thread(hub::stop));
        controller.start(hub);
    }

    public MonoRGBHub(BootstrapConfig config, JepHubController controller) {
        super(config, controller);
    }

    @Override
    protected ChannelHandler getDecoder() {
        return new MonoRGBDecoder();
    }

    @Override
    protected ChannelHandler getLogic() {
        return new MonoRGBLogic(this);
    }

}
