package ru.advantum.service.usw.test;

import org.junit.Test;
import ru.advantum.config.Configuration;
import ru.advantum.service.usw.UswNaviServiceConfig;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author Mitsay <mitsay@advantum.ru>
 * @since 11/21/16
 */
public class ModularTests {
    //private static Logger log = LoggerFactory.getLogger(ModularTests.class.getSimpleName());

    public static UswNaviServiceConfig getConfig() {
        if (System.getProperty("advantum.config.dir") == null) {
            System.setProperty("advantum.config.dir", "src/main/resources/extras/examples");
        }
        try {
            return Configuration.unpackConfig(UswNaviServiceConfig.class, UswNaviServiceConfig.CONFIG_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Test
    public void configTest() {
        UswNaviServiceConfig config = getConfig();
        if (config != null) {
            System.out.println(config.toString());
//            SessionFactoryManager.init(config);
//            log.info("pgsql conn OK: " + SessionFactoryManager.PostgreSqlSession.getRadioItemList(1).size());
        } else {
            System.err.println("Config not found");
        }
    }

    @Test
    public void chtime() {
        long time = System.currentTimeMillis();
        System.out.println("now:"+time);
        Date date = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        //        time = new Date(sdf.format(date)).getTime();
        //        System.out.println("utc:"+time);
    }
}