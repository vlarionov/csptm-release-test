package ru.advantum.service.usw.databases.mssql.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("unused")
public class DebriefingRecord {
    private final Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());

    private Integer stationNum;
    private Integer stationType;
    private Integer channel;
    private Integer uniqueId;
    private Integer param;

    private DebriefingRecord() {
    }

    public DebriefingRecord(String stationNum, String stationType, String channel, String uniqueId, String param) {
        this();
        try {
            if (stationNum == null) {
                log.warn("StationNUm is null. Use -1 value");
                stationNum = "-1";
            }
            setStationNum(Integer.valueOf(stationNum));
            setStationType(stationType == null ? 207 : Integer.valueOf(stationType));
            setChannel(channel == null ? 0 : Integer.valueOf(channel));
            setUniqueId(uniqueId == null ? 0 : Integer.valueOf(uniqueId));
            setParam(param == null ? 0 : Integer.valueOf(param));
        } catch (NumberFormatException nfe) {
            log.error("", nfe);
        }
    }

    public int getStationNum() {
        return stationNum;
    }

    public DebriefingRecord setStationNum(int stationNum) {
        this.stationNum = stationNum;
        return this;
    }

    public int getStationType() {
        return stationType;
    }

    public DebriefingRecord setStationType(int stationType) {
        this.stationType = stationType;
        return this;
    }

    public int getChannel() {
        return channel;
    }

    public DebriefingRecord setChannel(int channel) {
        this.channel = channel;
        return this;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public DebriefingRecord setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
        return this;
    }

    public int getParam() {
        return param;
    }

    public DebriefingRecord setParam(int param) {
        this.param = param;
        return this;
    }
}
