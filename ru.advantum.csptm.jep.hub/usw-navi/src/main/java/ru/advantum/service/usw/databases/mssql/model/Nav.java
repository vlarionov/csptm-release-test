package ru.advantum.service.usw.databases.mssql.model;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.MessageEvent;
import ru.advantum.service.usw.utils.Funcs;

import java.net.SocketAddress;
import java.sql.Timestamp;

@SuppressWarnings("unused")
public class Nav implements MessageEvent, Cloneable {
    private long rowId;
    private int receiver;
    private int receiverSystem;
    private int unitID;
    private int unitSystem;
    private int uniqueID;
    private Timestamp timeSys;
    private Timestamp timeNav;
    private double latitude;
    private double longitude;
    private float speed;
    private float azimuth;
    private int mark;
    private int timeIns;
    private int nD_Identificator;
    private int park_Identificator;
    private int timeReal;
    private int dgt0;
    private int dgt1;
    private int dgt2;
    private int dgt3;
    private int dgt4;
    private int dgt5;
    private int dgt6;
    private int dgt7;
    private double track;
    private Long callRequestTime;
    private Long SOSTime;
    private long timeNavSec;

    public long getRowId() {
        return rowId;
    }

    public Nav setRowId(long rowId) {
        this.rowId = rowId;
        return this;
    }

    public int getReceiver() {
        return receiver;
    }

    public Nav setReceiver(int receiver) {
        this.receiver = receiver;
        return this;
    }

    public int getReceiverSystem() {
        return receiverSystem;
    }

    public Nav setReceiverSystem(int receiverSystem) {
        this.receiverSystem = receiverSystem;
        return this;
    }

    public int getUnitID() {
        return unitID;
    }

    public Nav setUnitID(int unitID) {
        this.unitID = unitID;
        return this;
    }

    public int getUnitSystem() {
        return unitSystem;
    }

    public Nav setUnitSystem(int unitSystem) {
        this.unitSystem = unitSystem;
        return this;
    }

    public int getUniqueID() {
        return uniqueID;
    }

    public Nav setUniqueID(int uniqueID) {
        this.uniqueID = uniqueID;
        return this;
    }

    public Timestamp getTimeSys() {
        return timeSys;
    }

    public Nav setTimeSys(Timestamp timeSys) {
        this.timeSys = timeSys;
        return this;
    }

    public Timestamp getTimeNav() {
        return timeNav;
    }

    public Nav setTimeNav(Timestamp timeNav) {
        this.timeNav = timeNav;
        return this;
    }

    public double getLatitude() {
        return latitude;
    }

    public Nav setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public double getLongitude() {
        return longitude;
    }

    public Nav setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public float getSpeed() {
        return speed;
    }

    public Nav setSpeed(float speed) {
        this.speed = speed;
        return this;
    }

    public float getAzimuth() {
        return azimuth;
    }

    public Nav setAzimuth(float azimuth) {
        this.azimuth = azimuth;
        return this;
    }

    public int getMark() {
        return mark;
    }

    public Nav setMark(int mark) {
        this.mark = mark;
        return this;
    }

    public int getTimeIns() {
        return timeIns;
    }

    public Nav setTimeIns(int timeIns) {
        this.timeIns = timeIns;
        return this;
    }

    public int getnD_Identificator() {
        return nD_Identificator;
    }

    public Nav setnD_Identificator(int nD_Identificator) {
        this.nD_Identificator = nD_Identificator;
        return this;
    }

    public int getPark_Identificator() {
        return park_Identificator;
    }

    public Nav setPark_Identificator(int park_Identificator) {
        this.park_Identificator = park_Identificator;
        return this;
    }

    public int getTimeReal() {
        return timeReal;
    }

    public Nav setTimeReal(int timeReal) {
        this.timeReal = timeReal;
        return this;
    }

    public int getDgt0() {
        return dgt0;
    }

    public Nav setDgt0(int dgt0) {
        this.dgt0 = dgt0;
        return this;
    }

    public int getDgt1() {
        return dgt1;
    }

    public Nav setDgt1(int dgt1) {
        this.dgt1 = dgt1;
        return this;
    }

    public int getDgt2() {
        return dgt2;
    }

    public Nav setDgt2(int dgt2) {
        this.dgt2 = dgt2;
        return this;
    }

    public int getDgt3() {
        return dgt3;
    }

    public Nav setDgt3(int dgt3) {
        this.dgt3 = dgt3;
        return this;
    }

    public int getDgt4() {
        return dgt4;
    }

    public Nav setDgt4(int dgt4) {
        this.dgt4 = dgt4;
        return this;
    }

    public int getDgt5() {
        return dgt5;
    }

    public Nav setDgt5(int dgt5) {
        this.dgt5 = dgt5;
        return this;
    }

    public int getDgt6() {
        return dgt6;
    }

    public Nav setDgt6(int dgt6) {
        this.dgt6 = dgt6;
        return this;
    }

    public int getDgt7() {
        return dgt7;
    }

    public Nav setDgt7(int dgt7) {
        this.dgt7 = dgt7;
        return this;
    }

    public double getTrack() {
        return track;
    }

    public Nav setTrack(double track) {
        this.track = track;
        return this;
    }

    public Nav setCallRequestTime(Long callRequestTime) {
        this.callRequestTime = callRequestTime;
        return this;
    }

    public Nav setSOSTime(Long SOSTime) {
        this.SOSTime = SOSTime;
        return this;
    }

    public Long getCallRequestTime() {
        return callRequestTime;
    }

    public Long getSOSTime() {
        return SOSTime;
    }

    @Override
    public String toString() {
        return Funcs.uniToString(this);
    }

    @Override
    public Object clone() {
        Object o = null;
        try {
            o = super.clone();
        } catch(CloneNotSupportedException e) {
        }
        return o;
    }

    @Override
    public Object getMessage() {
        return this;
    }

    @Override
    public SocketAddress getRemoteAddress() {
        return null;
    }

    @Override
    public Channel getChannel() {
        return null;
    }

    @Override
    public ChannelFuture getFuture() {
        return null;
    }

    public long getTimeNavSec() {
        return getTimeNav().getTime() / 1000;
    }


}
