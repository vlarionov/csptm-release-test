package ru.advantum.service.usw.databases;

import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.mapping.VendorDatabaseIdProvider;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.service.usw.UswNaviServiceConfig;
import ru.advantum.service.usw.annotations.AnnotationFactory;
import ru.advantum.service.usw.databases.mssql.mappers.NavMapper;
import ru.advantum.service.usw.databases.mssql.model.DebriefingRecord;
import ru.advantum.service.usw.databases.mssql.model.Nav;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class SessionFactoryManager {
    private static Logger log = LoggerFactory.getLogger(SessionFactoryManager.class);
    private static SqlSessionFactory msSqlSessionFactory;
    private static UswNaviServiceConfig config;

    public static void setConfig(UswNaviServiceConfig newConfig) {
        config = newConfig;
    }

    public static void init() {
        if (config != null) {
            try {
                /*
                 MSSQL initiation
                 */
                DataSource msds = config.mssql.getConnectionPool();
                Configuration mscfg = new Configuration(new Environment(
                        SessionFactoryManager.class.getSimpleName(),
                        new JdbcTransactionFactory(),
                        msds
                ));
                mscfg.setDatabaseId(new VendorDatabaseIdProvider().getDatabaseId(msds));
                addMappers(mscfg);
                msSqlSessionFactory = new SqlSessionFactoryBuilder().build(mscfg);

            } catch (SQLException e) {
                log.error("init", e);
            }
        }
    }

    private static void addMappers(Configuration configuration) {
        List<Class<?>> cList = AnnotationFactory.findMappers("ru.advantum.service.usw.databases.mssql.mappers");
        for (Class<?> clazz : cList) {
            configuration.addMapper(clazz);
        }
        cList.size();
    }

    public static class MssqlSqlSession {
        public static CopyOnWriteArrayList<Nav> getAllAvailableNavs() {
            CopyOnWriteArrayList<Nav> result = new CopyOnWriteArrayList<>();
            if (msSqlSessionFactory == null) {
                init();
            }
            try {


                if (msSqlSessionFactory != null) {
                    log.info("Selection started");
                    try (SqlSession session = msSqlSessionFactory.openSession()) {
                        NavMapper mpr = session.getMapper(NavMapper.class);
                        List<Nav> list = mpr.getAll();
                        log.info("\t Obtained {} records", list.size());
                        result.addAll(list);
                        mpr.deleteAll();
                        log.info("\t Removed {} records", list.size());

                        session.commit();
                    }
                }
            } catch (Exception ex) {
                log.error("MSSQL {}", ex);
            }
            return result;
        }

        public static void uploadDebriefingRecord(DebriefingRecord record) {
            if (msSqlSessionFactory == null) {
                init();
            }
            try {

                if (msSqlSessionFactory != null) {
                    log.info("Insertion of {}", record.getStationNum());
                    try (SqlSession session = msSqlSessionFactory.openSession()) {
                        session.getMapper(NavMapper.class).uploadDebriefingRecord(record);
                        session.commit();
                    }
                }
            } catch (Exception ex) {
                log.error("MSSQL {}", ex);
            }
        }

        public static void cleanDebriefingRecord(Long snum) {
            if (msSqlSessionFactory == null) {
                init();
            }
            try {

                if (msSqlSessionFactory != null) {
                    log.info("Deletion of {}", snum);
                    try (SqlSession session = msSqlSessionFactory.openSession()) {
                        session.getMapper(NavMapper.class).cleanDebriefingRecord(snum);
                        session.commit();
                    }
                }
            } catch (Exception ex) {
                log.error("MSSQL {}", ex);
            }
        }
    }
}