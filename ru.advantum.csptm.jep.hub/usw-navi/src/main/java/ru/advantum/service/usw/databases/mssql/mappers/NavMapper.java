package ru.advantum.service.usw.databases.mssql.mappers;

import org.apache.ibatis.annotations.*;
import ru.advantum.service.usw.annotations.Mapper;
import ru.advantum.service.usw.databases.mssql.model.DebriefingRecord;
import ru.advantum.service.usw.databases.mssql.model.Nav;

import java.util.List;

@SuppressWarnings("unused")
@Mapper
public interface NavMapper {
    @Results({
            @Result(property = "receiver", column = "Receiver"),
            @Result(property = "receiverSystem", column = "ReceiverSystem"),
            @Result(property = "unitID", column = "UnitID"),
            @Result(property = "unitSystem", column = "UnitSystem"),
            @Result(property = "uniqueID", column = "UniqueID"),
            @Result(property = "timeSys", column = "TimeSys"),
            @Result(property = "timeNav", column = "TimeNav"),
            @Result(property = "latitude", column = "Latitude"),
            @Result(property = "longitude", column = "Longitude"),
            @Result(property = "speed", column = "Speed"),
            @Result(property = "azimuth", column = "Azimuth"),
            @Result(property = "mark", column = "Mark"),
            @Result(property = "timeIns", column = "TimeIns"),
            @Result(property = "nD_Identificator", column = "ND_Identificator"),
            @Result(property = "park_Identificator", column = "Park_Identificator"),
            @Result(property = "timeReal", column = "TimeReal"),
            @Result(property = "dgt0", column = "dgt0"),
            @Result(property = "dgt1", column = "dgt1"),
            @Result(property = "dgt2", column = "dgt2"),
            @Result(property = "dgt3", column = "dgt3"),
            @Result(property = "dgt4", column = "dgt4"),
            @Result(property = "dgt5", column = "dgt5"),
            @Result(property = "dgt6", column = "dgt6"),
            @Result(property = "dgt7", column = "dgt7"),
            @Result(property = "track", column = "track")
    })
    @Select("select Receiver" +
            ", ReceiverSystem" +
            ", UnitID" +
            ", UnitSystem" +
            ", UniqueID" +
            ", TimeSys" +
            ", dateadd(HOUR, -3, TimeNav) TimeNav" +
            ", Latitude" +
            ", Longitude" +
            ", Speed" +
            ", Azimuth" +
            ", Mark" +
            ", TimeIns" +
            ", ND_Identificator" +
            ", Park_Identificator" +
            ", TimeReal" +
            ", dgt0" +
            ", dgt1" +
            ", dgt2" +
            ", dgt3" +
            ", dgt4" +
            ", dgt5" +
            ", dgt6" +
            ", dgt7" +
            ", track" +
            " from dbo.Nav WITH (TABLOCK, HOLDLOCK)")
    List<Nav> getAll();

    @Delete("delete from dbo.Nav where UnitID = #{unitID} and TimeSys = #{timeSys} and TimeNav = #{timeNav} and track = #{track}")
    void removeSingle(Nav nav);

    @Delete("delete from dbo.Nav")
    void deleteAll();

    @Insert("delete from dbo.Opros_YKV where StationNum = #{stationNum}")
    void cleanDebriefingRecord(@Param("stationNum") long snum);

    @Insert("insert into dbo.Opros_YKV values (#{stationNum}, #{stationType}, #{channel}, #{uniqueId}, #{param})")
    void uploadDebriefingRecord(DebriefingRecord debriefingRecord);

}
