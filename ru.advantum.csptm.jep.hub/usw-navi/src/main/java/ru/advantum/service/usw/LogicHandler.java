package ru.advantum.service.usw;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.core.rmq.JepRmqHubController;
import ru.advantum.csptm.jep.proto.CommonPacketFlag;
import ru.advantum.csptm.jep.proto.TelematicPacket;
import ru.advantum.csptm.jep.proto.UnitPacket;
import ru.advantum.csptm.jep.proto.UnitPacketBuilder;
import ru.advantum.csptm.service.csd.ComplexSensorProcessor;
import ru.advantum.csptm.service.csd.ExtendedData;
import ru.advantum.hub.util.BitIterable;
import ru.advantum.service.usw.databases.SessionFactoryManager;
import ru.advantum.service.usw.databases.mssql.model.Nav;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

public class LogicHandler implements Runnable {
    private final Logger log = LoggerFactory.getLogger(LogicHandler.class);

    private JepRmqHubController controller;

    LogicHandler(JepRmqHubController controller) {
        setController(controller);
    }

    @Override
    public void run() {
        try {
            int processed = 0;
            CopyOnWriteArrayList<Nav> list = SessionFactoryManager.MssqlSqlSession.getAllAvailableNavs();
            log.debug("Read {} records", list.size());
            for (Nav nav : list) {
                Map<Integer, BigDecimal> extendedSensor = ExtendedData.getCleanExtendedArray();
                //log.debug("rcvd {}", nav);

                long time = nav.getTimeNav().getTime() / 1000;

                Optional<Long> unitId = getController().translateId(String.valueOf(nav.getUnitID()));
                if (unitId.isPresent()) {
                    log.debug("Unit detected: {}", nav);
                    processed++;

                    UnitPacketBuilder unitPacketBuilder = UnitPacket.newBuilder()
                            .setUnitId(unitId.get())
                            .setTrId(nav.getUniqueID())
                            .setEventTime(Instant.ofEpochSecond(time));

                    boolean locValid = true;
                    int mark = nav.getMark();
                    int count = 0;
                    for (Boolean b : new BitIterable(32, mark)) {
                        if (b) {
                            switch (count) {
                                case 2:
                                    unitPacketBuilder.addFlag(CommonPacketFlag.CALL_REQUEST);
                                    extendedSensor.put(ExtendedData.CALL_REQ, new BigDecimal(1));
                                    break;
                                case 3:
                                    unitPacketBuilder.addFlag(CommonPacketFlag.ALARM);
                                    extendedSensor.put(ExtendedData.SOS, new BigDecimal(1));
                                case 13:
                                    unitPacketBuilder.addFlag(CommonPacketFlag.HIST_DATA);
                                    break;
                                case 15:
                                    locValid = false;
                                    break;
                                case 20:
                                    unitPacketBuilder.addFlag("CALL_CONFIRM");
                                    break;
                                case 14:
                                case 21:
                                    unitPacketBuilder.addFlag("INVALID_STATUS");
                                    break;
                                default:
                                    break;
                            }
                        }
                        count++;
                    }


                    unitPacketBuilder.setTelematic(TelematicPacket.newBuilder()
                            .setLocationValid(locValid)
                            .setGpsTime(Instant.ofEpochSecond(time))
                            .setCoordinates((float) nav.getLongitude(), (float) nav.getLatitude())
                            .setHeading((int) nav.getAzimuth())
                            .setSpeed((int) nav.getSpeed())
                            .build())

                            .addSensor(1, new BigDecimal(nav.getDgt0()))
                            .addSensor(2, new BigDecimal(nav.getDgt1()))
                            .addSensor(3, new BigDecimal(nav.getDgt2()))
                            .addSensor(4, new BigDecimal(nav.getDgt3()))
                            .addSensor(5, new BigDecimal(nav.getDgt4()))
                            .addSensor(6, new BigDecimal(nav.getDgt5()))
                            .addSensor(7, new BigDecimal(nav.getDgt6()))
                            .addSensor(8, new BigDecimal(nav.getDgt7()));

                    extendedSensor.put(ExtendedData.ODOMETER, new BigDecimal(nav.getTrack()));
                    extendedSensor.put(ExtendedData.VALIDITY, new BigDecimal(locValid ? 1 : 0));
                    extendedSensor.put(ExtendedData.LON_HSPH, new BigDecimal(nav.getLongitude() > 0 ? 1 : 0));
                    extendedSensor.put(ExtendedData.LAT_HSPH, new BigDecimal(nav.getLatitude() > 0 ? 1 : 0));
                    extendedSensor.put(ExtendedData.MAX_SPD, new BigDecimal(nav.getSpeed()));

                    log.debug("### extendedSensor.size() = {}", extendedSensor.size());
                    unitPacketBuilder.addComplexSensors(
                            ComplexSensorProcessor.decode(1, extendedSensor));


                    getController().sendPackets(unitPacketBuilder.build());
                }
            }
            log.info("Processed {} records", processed);
        } catch (Throwable t) {
            log.error("gather coords", t);
        }

    }

    private JepRmqHubController getController() {
        return controller;
    }

    private void setController(JepRmqHubController controller) {
        this.controller = controller;
    }

}
