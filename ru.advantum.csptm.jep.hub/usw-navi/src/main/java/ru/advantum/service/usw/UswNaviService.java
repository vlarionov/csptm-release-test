package ru.advantum.service.usw;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.jep.core.JepHub;
import ru.advantum.csptm.jep.core.rmq.JepRmqHubController;
import ru.advantum.csptm.jep.proto.CommandPacket;
import ru.advantum.service.usw.databases.SessionFactoryManager;
import ru.advantum.service.usw.databases.mssql.model.DebriefingRecord;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("unused")
public class UswNaviService implements JepHub {

    private static final Logger log = LoggerFactory.getLogger(UswNaviService.class);

    private final UswNaviServiceConfig config;
    private final JepRmqHubController controller;
    private final ScheduledThreadPoolExecutor ex = new ScheduledThreadPoolExecutor(4);

    public UswNaviService(UswNaviServiceConfig config, JepRmqHubController controller) {
        this.config = config;
        this.controller = controller;
    }

    public static void main(String[] args) throws Exception {
        UswNaviServiceConfig config = Configuration.unpackConfig(UswNaviServiceConfig.class, "hub.xml");
        JepRmqHubController controller = new JepRmqHubController(config);
        UswNaviService uswNaviService = new UswNaviService(config, controller);
        controller.start(uswNaviService);
    }

    @Override
    public void start() {
        SessionFactoryManager.setConfig(this.config);
        SessionFactoryManager.init();
        ex.scheduleAtFixedRate(new LogicHandler(this.controller),
                this.config.runtime.startDelaySec,
                this.config.runtime.startIntervalSec,
                TimeUnit.SECONDS);
    }

    @Override
    public void stop() {

    }

    @Override
    public void unitChanged(long l) {

    }

    @Override
    public void sendCommand(CommandPacket commandPacket) {
        if (commandPacket.getCmdName().equals("START")) {
            SessionFactoryManager.MssqlSqlSession.uploadDebriefingRecord(
                    new DebriefingRecord(
                            commandPacket.getParameters().get("NUM"),
                            commandPacket.getParameters().get("TYPE"),
                            commandPacket.getParameters().get("CHANNEL"),
                            commandPacket.getParameters().get("UNIQUE_ID"),
                            commandPacket.getParameters().get("PARAM"))
            );
        } else if (commandPacket.getCmdName().equals("STOP")) {
            String snum = commandPacket.getParameters().get("NUM");
            SessionFactoryManager.MssqlSqlSession.cleanDebriefingRecord(Long.valueOf(snum));
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

}
