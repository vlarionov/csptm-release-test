package ru.advantum.service.usw;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.csptm.jep.core.rmq.JepRmqHubControllerConfig;

@Root(name = "usw-navi")
public class UswNaviServiceConfig extends JepRmqHubControllerConfig {
    public static final String CONFIG_NAME = "hub.xml";

    @Element(name = "runtime", required = false)
    RuntimeParameters runtime = new RuntimeParameters();

    public static class RuntimeParameters {
        @Attribute(name = "mssql-req-start-delay-sec", required = false)
        int startDelaySec = 10;

        @Attribute(name = "mssql-req-interval-sec", required = false)
        int startIntervalSec = 90;

    }

    @Element(name = "mssql")
    public DatabaseConnection mssql = null;

}
