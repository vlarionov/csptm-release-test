package ru.advantum.csptm.jep.hub.debug;

import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.handler.codec.frame.DelimiterBasedFrameDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.jep.core.JepHubController;
import ru.advantum.csptm.jep.core.rmq.JepRmqHubController;
import ru.advantum.csptm.jep.netty.BootstrapConfig;
import ru.advantum.csptm.jep.netty.JepNettyHub;
import ru.advantum.csptm.jep.netty.NettyHubConfig;

import java.nio.ByteOrder;

/**
 * Created by kaganov on 30/11/2016.
 */
public class DebugJepHub extends JepNettyHub {

    private static final Logger LOGGER = LoggerFactory.getLogger(DebugJepHub.class);

    public DebugJepHub(BootstrapConfig bootstrapConfig, JepHubController controller) {
        super(bootstrapConfig, controller);
    }

    @Override
    protected ByteOrder getByteOrder() {
        return ByteOrder.BIG_ENDIAN;
    }

    @Override
    protected void tunePipeline(ChannelPipeline channelPipeline) {
        channelPipeline.addLast("delimiterDecoder", new DelimiterBasedFrameDecoder(10000,
                                                                                   ChannelBuffers.wrappedBuffer(new byte[]{';'})));
        channelPipeline.addLast("frameDecoder", new DbgFrameDecoder());
        channelPipeline.addLast("logic", new DbgLogicHandler(this));
    }

    public static void main(String[] args) throws Exception {
        NettyHubConfig hubConfig = Configuration.unpackConfig(NettyHubConfig.class, "debug-hub-jep.xml");
        JepRmqHubController controller = new JepRmqHubController(hubConfig.controllerConfig);
        DebugJepHub debugJepHub = new DebugJepHub(hubConfig.tcpConfig, controller);
        controller.start(debugJepHub);
    }
}
