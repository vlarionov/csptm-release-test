package ru.advantum.csptm.jep.hub.debug;

import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.MessageEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.netty.JepNettyConnectionHandler;
import ru.advantum.csptm.jep.proto.CommandPacket;
import ru.advantum.csptm.jep.proto.TelematicPacket;
import ru.advantum.csptm.jep.proto.UnitPacket;

import java.time.Instant;
import java.util.Arrays;

public class DbgLogicHandler extends JepNettyConnectionHandler<DebugJepHub> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DbgLogicHandler.class);

    public DbgLogicHandler(DebugJepHub hub) {
        super(hub);
    }

    @Override
    public void onCommandForSend(CommandPacket cmd) {
        LOGGER.info("command [{}]", cmd);
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
        if (e.getMessage() instanceof String[]) {
            final String[] message = (String[]) e.getMessage();
            LOGGER.debug("rcvd {}", Arrays.toString(message));
            String uid = message[0];
            long time = Long.parseLong(message[1]);
            float lon = Float.parseFloat(message[2].replace(",", "."));
            float lat = Float.parseFloat(message[3].replace(",", "."));

            if (getUnitId() == null) {
                initUnitId(uid);
            }

            sendUnitPackets(UnitPacket.newBuilder()
                                    .setUnitId(getUnitId())
                                    .setEventTime(Instant.ofEpochSecond(time))
                                    .setTelematic(TelematicPacket.newBuilder()
                                                          .setLocationValid(true)
                                                          .setGpsTime(Instant.ofEpochSecond(time))
                                                          .setCoordinates(lon, lat)
                                                          .build())
                                    .build());
        }
        super.messageReceived(ctx, e);
    }
}
