package ru.advantum.csptm.jep.hub.debug;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author kaganov
 */
public class DbgClient {

    public static String generateCoords(String id) {
        long time = System.currentTimeMillis() / 1000;
        double lon = 37.378306 + Math.random() * (37.845205 - 37.378306);
        double lat = 55.915231 + Math.random() * (55.915231 - 55.584381);
        return String.format("%s\t%d\t%f\t%f;", id, time, lon, lat);
    }

    public static void main(String[] args) throws Exception {
        String[] uids = new String[]{"001001003", "28102016", "03102016-01", "02102016-01", "02102016-11", "8009600147"};
        ScheduledExecutorService ses = Executors.newScheduledThreadPool(uids.length);

        for (String uid : uids) {
            final Socket s = new Socket("0.0.0.0", 34001);
            ses.scheduleAtFixedRate(
                    () -> {
                        try {
                            s.getOutputStream().write(generateCoords(uid).getBytes());
                        } catch (IOException ex) {
                            ex.printStackTrace(System.out);
                        }
                    },
                    1, 1000, TimeUnit.MILLISECONDS);
        }
    }
}
