package ru.advantum.csptm.jep.hub.debug;

import org.apache.log4j.Logger;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.FrameDecoder;
import org.jboss.netty.util.CharsetUtil;

public class DbgFrameDecoder extends FrameDecoder {

    public static final Logger log = Logger.getLogger(DbgFrameDecoder.class);

    @Override
    protected Object decode(ChannelHandlerContext chc, Channel chnl, ChannelBuffer cb) throws Exception {
        final int readable = cb.writerIndex();
        String raw = null;
        if (readable >= 0) {
            try {
                raw = new String(cb.readBytes(readable).array(), CharsetUtil.US_ASCII)
                        .replace("\r", "")
                        .replace("\n", "");
                log.debug("RCV: " + raw);
                return raw.split("\t");
            } catch (Exception e) {
                log.error(String.format("RCVD [%s]", raw), e);
                throw e;
            }
        }

        return null;
    }
}
