import org.junit.Test;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.jep.hub.everfocus.Config;

public class CommonTests {
    public static Config getConfig() throws Exception {
        System.setProperty("advantum.config.dir", "src/main/resources/extras/examples");
        return Configuration.unpackConfig(Config.class, "hub.xml");
    }


    @Test
    public void configTest() throws Exception {
        Config config = getConfig();
        if (config != null) {
            System.out.println("Version: " + config.version);
        }
    }

}
