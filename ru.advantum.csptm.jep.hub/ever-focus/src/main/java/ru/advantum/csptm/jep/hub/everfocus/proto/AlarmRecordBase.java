package ru.advantum.csptm.jep.hub.everfocus.proto;

import ru.advantum.csptm.jep.hub.everfocus.proto.alarm.AlarmType;
import ru.advantum.csptm.jep.hub.everfocus.proto.alarm.CompressionType;
import ru.advantum.csptm.jep.hub.everfocus.wrappers.IntegerWrapper;
import ru.advantum.csptm.jep.hub.everfocus.wrappers.StringWrapper;

import java.util.Date;

@SuppressWarnings("unused")
public class AlarmRecordBase {
    protected StringWrapper version = new StringWrapper(3);
    protected StringWrapper uniqId = new StringWrapper(10);

    private IntegerWrapper alarmType = new IntegerWrapper(1);
    private IntegerWrapper alarmTime = (IntegerWrapper) new IntegerWrapper(4).setLittleEndian(true);
    private StringWrapper cameraNumber = new StringWrapper(2);
    private StringWrapper compressionType = new StringWrapper(2);
    private StringWrapper alarmSource = new StringWrapper(2);
    private IntegerWrapper imageSize = (IntegerWrapper) new IntegerWrapper(6).setLittleEndian(true);
    private byte[] image = new byte[0];
    private byte[] reserved = null;

    protected AlarmRecordBase(int reservedLength) {
        this.reserved = new byte[reservedLength];
    }

    private Presentation presentation = new Presentation();

    public Presentation getPresentation() {
        return presentation;
    }

    public StringWrapper getVersion() {
        return version;
    }

    public void setVersion(StringWrapper version) {
        this.version = version;
    }

    public StringWrapper getUniqId() {
        return uniqId;
    }

    public void setUniqId(StringWrapper uniqId) {
        this.uniqId = uniqId;
    }

    public IntegerWrapper getAlarmType() {
        return alarmType;
    }

    public void setAlarmType(IntegerWrapper alType) {
        this.alarmType = alType;
    }

    public IntegerWrapper getAlarmTime() {
        return alarmTime;
    }

    public void setAlarmTime(IntegerWrapper alarmTime) {
        this.alarmTime = alarmTime;
    }

    public StringWrapper getCameraNumber() {
        return cameraNumber;
    }

    public void setCameraNumber(StringWrapper cameraNumber) {
        this.cameraNumber = cameraNumber;
    }

    public StringWrapper getCompressionType() {
        return compressionType;
    }

    public void setCompressionType(StringWrapper comprType) {
        this.compressionType = comprType;
    }

    public StringWrapper getAlarmSource() {
        return alarmSource;
    }

    public void setAlarmSource(StringWrapper alarmSource) {
        this.alarmSource = alarmSource;
    }

    public IntegerWrapper getImageSize() {
        return imageSize;
    }

    public void setImageSize(IntegerWrapper imageSize) {
        this.imageSize = imageSize;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public byte[] getReserved() {
        return reserved;
    }


    public class Presentation {
        public long getUTCtime() {
            return getAlarmTime().longValue();
        }

        public AlarmType getAlarmType() {
            return AlarmType.lookup(alarmType.intValue());
        }

        public CompressionType getCompressionType() {
            return CompressionType.lookup(Integer.parseInt(compressionType.asString().isEmpty() ? "-1" : compressionType.asString()));
        }

        public int getAlarmedDevice() {
            return AlarmType.getDeviceNumberByAlarm(getAlarmType(), Integer.parseInt(alarmSource.asString().isEmpty() ? "-1" : alarmSource.asString()));
        }

        public boolean isImageExists() {
            return imageSize.intValue() > 0;
        }

        @Override
        public String toString() {
            return "Presentation: {alarm-type:" + getAlarmType() +
                    ",compression-type:" + getCompressionType() +
                    ",alarmed-device:" + getAlarmedDevice() +
                    ",UTC-time:" + getUTCtime() + "(" + new Date(getUTCtime() * 1000) + ")" +
                    "}";
        }
    }


}
