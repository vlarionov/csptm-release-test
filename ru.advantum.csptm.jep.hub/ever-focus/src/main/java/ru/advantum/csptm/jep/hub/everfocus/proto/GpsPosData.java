package ru.advantum.csptm.jep.hub.everfocus.proto;

import org.jboss.netty.buffer.ChannelBuffer;
import ru.advantum.csptm.jep.hub.everfocus.Funcs;
import ru.advantum.csptm.jep.hub.everfocus.wrappers.IntegerWrapper;

import java.util.Calendar;

@SuppressWarnings("unused")
public class GpsPosData {
    IntegerWrapper utcDate = (IntegerWrapper) new IntegerWrapper(4).setLittleEndian(true);
    IntegerWrapper utcTime = (IntegerWrapper) new IntegerWrapper(4).setLittleEndian(true);
    IntegerWrapper lat = (IntegerWrapper) new IntegerWrapper(4).setLittleEndian(true);
    IntegerWrapper lon = (IntegerWrapper) new IntegerWrapper(4).setLittleEndian(true);

    IntegerWrapper speed = (IntegerWrapper) new IntegerWrapper(2).setLittleEndian(true);
    IntegerWrapper heading = (IntegerWrapper) new IntegerWrapper(2).setLittleEndian(true);
    IntegerWrapper magnetic = (IntegerWrapper) new IntegerWrapper(2).setLittleEndian(true);

    IntegerWrapper magnDir = new IntegerWrapper(1);
    IntegerWrapper valid = new IntegerWrapper(1);
    IntegerWrapper latDir = new IntegerWrapper(1);
    IntegerWrapper lonDir = new IntegerWrapper(1);
    IntegerWrapper mode = new IntegerWrapper(1);
    IntegerWrapper fake = new IntegerWrapper(1);

    IntegerWrapper reserved = (IntegerWrapper) new IntegerWrapper(4).setLittleEndian(true);

    private Presentation presentation;

    public static class Presentation {
        public static int GPS_MULTIPLEXOR = 10000;
        public static double KNOTS_IN_MS = 1.94384449244;
        GpsPosData parent;

        private Presentation(GpsPosData parent) {
            this.parent = parent;
        }

        public long getUTCTime() {
            Calendar calendar = Calendar.getInstance();
            String datestr = Integer.toString(parent.getUtcDate().intValue());
            if (datestr.length() == 6) {
                calendar.set(
                        Integer.parseInt(datestr.substring(4, 6)),
                        Integer.parseInt(datestr.substring(2, 4)) - 1,
                        Integer.parseInt(datestr.substring(0, 2))
                );
            } else if (datestr.equals("0")) {
                calendar.set(0, 0, 0);
            }
            return calendar.getTimeInMillis() + parent.getUtcTime().intValue();
        }

        public float getLAT() {
            return (float) (parent.getLat().longValue() * 1.0 * latSign() / GPS_MULTIPLEXOR);
        }

        public float getLON() {
            return (float) (parent.getLon().longValue() * 1.0 * lonSign() / GPS_MULTIPLEXOR);
        }

        public int getSpeed() {
            double ms = parent.getSpeed().intValue() * 1.0 / 100 / KNOTS_IN_MS;
            return new Double(ms * 18 / 5).intValue();
        }

        public int getHeading() {
            return new Double(parent.getHeading().intValue() * 1.0 / 10).intValue();
        }

        public boolean isValid() {
            return parent.getValid().intValue() == 1;
        }

        private int latSign() {
            return (parent.getLatDir().intValue() == 1 ? 1 : -1);
        }

        private int lonSign() {
            return (parent.getLonDir().intValue() == 1 ? 1 : -1);
        }

        public boolean hasValidData() {
            return getUTCTime() > 0 && getLAT() > 0 && getLON() > 0;
        }

    }

    public Presentation getPresentation() {
        return presentation;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + ":" + Funcs.uniToString(this);
    }

    private GpsPosData() {
        this.presentation = new Presentation(this);
    }

    public GpsPosData(ChannelBuffer buffer) {
        this();
        read(buffer);
    }

    public GpsPosData read(ChannelBuffer buffer) {
        getUtcDate().read(buffer);
        getUtcTime().read(buffer);
        getLat().read(buffer);
        getLon().read(buffer);
        getSpeed().read(buffer);
        getHeading().read(buffer);
        getMagnetic().read(buffer);

        getMagnDir().read(buffer);
        getValid().read(buffer);
        getLatDir().read(buffer);
        getLonDir().read(buffer);
        getMode().read(buffer);
        getFake().read(buffer);

        getReserved().read(buffer);
        return this;
    }


    public IntegerWrapper getUtcDate() {
        return utcDate;
    }

    public void setUtcDate(IntegerWrapper utcDate) {
        this.utcDate = utcDate;
    }

    public IntegerWrapper getUtcTime() {
        return utcTime;
    }

    public void setUtcTime(IntegerWrapper utcTime) {
        this.utcTime = utcTime;
    }

    public IntegerWrapper getLat() {
        return lat;
    }

    public void setLat(IntegerWrapper lat) {
        this.lat = lat;
    }

    public IntegerWrapper getLon() {
        return lon;
    }

    public void setLon(IntegerWrapper lon) {
        this.lon = lon;
    }

    public IntegerWrapper getSpeed() {
        return speed;
    }

    public void setSpeed(IntegerWrapper speed) {
        this.speed = speed;
    }

    public IntegerWrapper getHeading() {
        return heading;
    }

    public void setHeading(IntegerWrapper heading) {
        this.heading = heading;
    }

    public IntegerWrapper getMagnetic() {
        return magnetic;
    }

    public void setMagnetic(IntegerWrapper magnetic) {
        this.magnetic = magnetic;
    }

    public IntegerWrapper getMagnDir() {
        return magnDir;
    }

    public void setMagnDir(IntegerWrapper magnDir) {
        this.magnDir = magnDir;
    }

    public IntegerWrapper getValid() {
        return valid;
    }

    public void setValid(IntegerWrapper valid) {
        this.valid = valid;
    }

    public IntegerWrapper getLatDir() {
        return latDir;
    }

    public void setLatDir(IntegerWrapper latDir) {
        this.latDir = latDir;
    }

    public IntegerWrapper getLonDir() {
        return lonDir;
    }

    public void setLonDir(IntegerWrapper lonDir) {
        this.lonDir = lonDir;
    }

    public IntegerWrapper getMode() {
        return mode;
    }

    public void setMode(IntegerWrapper mode) {
        this.mode = mode;
    }

    public IntegerWrapper getFake() {
        return fake;
    }

    public void setFake(IntegerWrapper fake) {
        this.fake = fake;
    }

    public IntegerWrapper getReserved() {
        return reserved;
    }

    public void setReserved(IntegerWrapper reserved) {
        this.reserved = reserved;
    }
}
