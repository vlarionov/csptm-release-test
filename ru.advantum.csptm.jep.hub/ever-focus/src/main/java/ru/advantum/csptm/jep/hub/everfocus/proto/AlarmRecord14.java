package ru.advantum.csptm.jep.hub.everfocus.proto;

import org.jboss.netty.buffer.ChannelBuffer;
import ru.advantum.csptm.jep.hub.everfocus.Funcs;
import ru.advantum.csptm.jep.hub.everfocus.wrappers.IntegerWrapper;
import ru.advantum.csptm.jep.hub.everfocus.wrappers.StringWrapper;

import java.util.Calendar;
import java.util.TimeZone;

@SuppressWarnings("unused")
public class AlarmRecord14 extends AlarmRecordBase {

    private StringWrapper day = new StringWrapper(2);
    private StringWrapper mon = new StringWrapper(2);
    private StringWrapper year = new StringWrapper(4);
    private StringWrapper hour = new StringWrapper(2);
    private StringWrapper min = new StringWrapper(2);
    private StringWrapper sec = new StringWrapper(2);

    private IntegerWrapper alType = new IntegerWrapper(1);

    private StringWrapper imgSize = new StringWrapper(6);

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + ":" + Funcs.uniToString(this);
    }

    public AlarmRecord14(ChannelBuffer buffer) {
        super(23);
        read(buffer);
    }

    public AlarmRecord14 read(ChannelBuffer in) {
        getUniqId().read(in);
        getDay().read(in);
        getMon().read(in);
        getYear().read(in);
        getHour().read(in);
        getMin().read(in);
        getSec().read(in);
        getAlarmSource().read(in);
        getCameraNumber().read(in);
        getCompressionType().read(in);
        getAlType().read(in);
        getImgSize().read(in);
        in.readBytes(getReserved());

        setAlarmType(getAlarmType().setValue(getAlType().intValue()));
        setAlarmTime(getAlarmTime().setValue(getUTCtime()));
        setImageSize(getImageSize().setValue(getImgSize().intValue()));

        if (getImageSize().intValue() > 0) {
            setImage(new byte[getImageSize().intValue()]);
            in.readBytes(getImage());
        }
        return this;
    }

    private long getUTCtime() {
        Calendar rightNow = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        rightNow.set(Integer.parseInt(year.asString()),
                Integer.parseInt(mon.asString()) - 1,
                Integer.parseInt(day.asString()),
                Integer.parseInt(hour.asString()) - 3,
                Integer.parseInt(min.asString()),
                Integer.parseInt(sec.asString())
        );
        return rightNow.getTimeInMillis() / 1000;
    }

    public StringWrapper getDay() {
        return day;
    }

    public void setDay(StringWrapper day) {
        this.day = day;
    }

    public StringWrapper getMon() {
        return mon;
    }

    public void setMon(StringWrapper mon) {
        this.mon = mon;
    }

    public StringWrapper getYear() {
        return year;
    }

    public void setYear(StringWrapper year) {
        this.year = year;
    }

    public StringWrapper getHour() {
        return hour;
    }

    public void setHour(StringWrapper hour) {
        this.hour = hour;
    }

    public StringWrapper getMin() {
        return min;
    }

    public void setMin(StringWrapper min) {
        this.min = min;
    }

    public StringWrapper getSec() {
        return sec;
    }

    public void setSec(StringWrapper sec) {
        this.sec = sec;
    }

    public IntegerWrapper getAlType() {
        return alType;
    }

    public void setAlType(IntegerWrapper alType) {
        this.alType = alType;
    }

    public StringWrapper getImgSize() {
        return imgSize;
    }

    public void setImgSize(StringWrapper imgSize) {
        this.imgSize = imgSize;
    }
}
