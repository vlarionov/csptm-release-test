package ru.advantum.csptm.jep.hub.everfocus.proto;

import org.jboss.netty.buffer.ChannelBuffer;
import ru.advantum.csptm.jep.hub.everfocus.Funcs;
import ru.advantum.csptm.jep.hub.everfocus.wrappers.IntegerWrapper;
import ru.advantum.csptm.jep.hub.everfocus.wrappers.StringWrapper;

@SuppressWarnings("unused")
public class GpsHdr {
    public static int HDR = 1164332403;
    IntegerWrapper marker = (IntegerWrapper) new IntegerWrapper(4).setLittleEndian(true);
    IntegerWrapper time = (IntegerWrapper) new IntegerWrapper(4).setLittleEndian(true);
    IntegerWrapper seqnum = (IntegerWrapper) new IntegerWrapper(4).setLittleEndian(true);
    IntegerWrapper mac = (IntegerWrapper) new IntegerWrapper(6).setLittleEndian(true);
    IntegerWrapper length = (IntegerWrapper) new IntegerWrapper(4).setLittleEndian(true);
    IntegerWrapper type = new IntegerWrapper(1);

    StringWrapper vehicleId = new StringWrapper(16);
    StringWrapper hddId = new StringWrapper(16);

    StringWrapper reserved = new StringWrapper(9);

    @Override
    public String toString() {
        return this.getClass().getSimpleName()+":"+ Funcs.uniToString(this);
    }

    public GpsHdr(ChannelBuffer buffer) {
        read(buffer);
    }

    public GpsHdr read(ChannelBuffer buffer) {
        getTime().read(buffer);
        getSeqnum().read(buffer);
        getMac().read(buffer);
        getLength().read(buffer);
        getType().read(buffer);

        getVehicleId().read(buffer);
        getHddId().read(buffer);

        getReserved().read(buffer);
        return this;
    }

    public IntegerWrapper getMarker() {
        return marker;
    }

    public void setMarker(IntegerWrapper marker) {
        this.marker = marker;
    }

    public IntegerWrapper getTime() {
        return time;
    }

    public void setTime(IntegerWrapper time) {
        this.time = time;
    }

    public IntegerWrapper getSeqnum() {
        return seqnum;
    }

    public void setSeqnum(IntegerWrapper seqnum) {
        this.seqnum = seqnum;
    }

    public IntegerWrapper getMac() {
        return mac;
    }

    public void setMac(IntegerWrapper mac) {
        this.mac = mac;
    }

    public IntegerWrapper getLength() {
        return length;
    }

    public void setLength(IntegerWrapper length) {
        this.length = length;
    }

    public IntegerWrapper getType() {
        return type;
    }

    public void setType(IntegerWrapper type) {
        this.type = type;
    }

    public StringWrapper getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(StringWrapper vehicleId) {
        this.vehicleId = vehicleId;
    }

    public StringWrapper getHddId() {
        return hddId;
    }

    public void setHddId(StringWrapper hddId) {
        this.hddId = hddId;
    }

    public StringWrapper getReserved() {
        return reserved;
    }

    public void setReserved(StringWrapper reserved) {
        this.reserved = reserved;
    }
}
