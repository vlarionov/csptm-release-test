package ru.advantum.csptm.jep.hub.everfocus;


import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.jboss.netty.channel.ChannelPipeline;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.jep.core.JepHubController;
import ru.advantum.csptm.jep.core.rmq.JepRmqHubController;
import ru.advantum.csptm.jep.hub.everfocus.handler.Decoder;
import ru.advantum.csptm.jep.hub.everfocus.handler.Logic;
import ru.advantum.csptm.jep.netty.JepNettyHub;

import java.nio.ByteOrder;

import static org.apache.log4j.Logger.getRootLogger;

public class Hub extends JepNettyHub {

    private Config config;

    static {
        /*
         * This configuring console logger without log4j.properties file
         */
        if (System.getProperty("log4j.configuration") == null) { // not .isEmpty() - will NPE
            final String PATTERN = "%d{dd-MM-yyyy HH:mm:ss.SSS}: %-5p %c{1}.%M(%L): %m%n";
            ConsoleAppender console = new ConsoleAppender(); //createParentDirs appender
            console.setLayout(new PatternLayout(PATTERN));
            console.setThreshold(Level.DEBUG);
            console.activateOptions();
            getRootLogger().addAppender(console);
        }
    }

    public Hub(Config config, JepHubController controller) {
        super(config.hubConfig.tcpConfig, controller);
        this.config = config;
    }

    public static void main(String[] args) throws Throwable {
        Config config = Configuration.unpackConfig(Config.class, "hub.xml");
        JepRmqHubController controller = new JepRmqHubController(config.hubConfig.controllerConfig);
        Hub hub = new Hub(config, controller);
        Runtime.getRuntime().addShutdownHook(new Thread(hub::stop));
        controller.start(hub);
    }

    public Config getConfig() {
        return config;
    }

    @Override
    protected void tunePipeline(ChannelPipeline pipeline) {
        pipeline.addLast("decoder", new Decoder(getConfig()));
        pipeline.addLast("logic", new Logic(this, getConfig()));
    }

    @Override
    protected ByteOrder getByteOrder() {
        return ByteOrder.LITTLE_ENDIAN;
    }

}
