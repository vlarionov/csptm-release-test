package ru.advantum.csptm.jep.hub.everfocus.wrappers;

import org.apache.commons.lang.SerializationUtils;
import org.jboss.netty.buffer.ChannelBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Comparator;

/**
 * @author Mitsay <mitsay@advantum.ru>
 * @since 15.01.16 17:52.
 */
public class RealWrapper extends AbstractWrapper implements Comparator<RealWrapper> {
    private static final Logger LOGGER = LoggerFactory.getLogger(RealWrapper.class);

    private byte[] bytes = new byte[0];
    private int size = 0;
    private int scale = 0;
    private BigDecimal bd = new BigDecimal(0L);

    public RealWrapper(int size, int scale) {
        setSize(size);
        setScale(scale);
        this.bytes = new byte[size];
    }

    public RealWrapper(int size, int scale, byte[] bytes) {
        this(size, scale);
        setBytes(bytes);
    }

    public RealWrapper(int size, int scale, byte[] bytes, int offset) {
        this(size, scale);
        setBytes(bytes, offset);
    }

    public RealWrapper(int size, int scale, InputStream in) throws IOException {
        this(size, scale);
        read(in);
    }

    public RealWrapper(int size, int scale, ChannelBuffer buffer) {
        this(size, scale);
        read(buffer);
    }

    public RealWrapper(int size, int scale, long value) {
        this(size, scale);
        setValue(value);
    }

    /**
     * Cloner
     *
     * @param other class
     */
    public RealWrapper(RealWrapper other) {
        this(other.getSize(), other.getScale());
        setValue(other.intValue());
    }

    /**
     * Deserializer
     *
     * @param serialized class
     */
    public RealWrapper(byte[] serialized) {
        this((RealWrapper) SerializationUtils.deserialize(serialized));
    }

    @Override
    public int read(InputStream in) throws IOException {
        int ret = in.read(this.bytes);
        bd = new BigDecimal(new BigInteger(this.bytes), getScale());
        checkLE();
        return ret;
    }

    @Override
    public int read(ChannelBuffer buffer) {
        buffer.readBytes(getBytes());
        bd = new BigDecimal(new BigInteger(this.bytes), getScale());
        checkLE();
        return getSize();
    }

    public RealWrapper setValue(short value) {
        bd = new BigDecimal(BigInteger.valueOf(value));
        System.arraycopy(bd.unscaledValue().toByteArray(), 0, this.bytes, 0, getSize());
        return this;
    }

    public RealWrapper setValue(int value) {
        bd = new BigDecimal(BigInteger.valueOf(value));
        System.arraycopy(bd.unscaledValue().toByteArray(), 0, this.bytes, 0, getSize());
        return this;
    }

    public RealWrapper setValue(long value) {
        bd = new BigDecimal(BigInteger.valueOf(value));
        System.arraycopy(bd.unscaledValue().toByteArray(), 0, this.bytes, 0, getSize());
        return this;
    }

    public short shortValue() {
        return bd.shortValue();
    }

    public int intValue() {
        return bd.intValue();
    }

    @Override
    public int uIntValue() {
        return (int) Integer.toUnsignedLong(bd.intValue());
    }

    @Override
    public byte byteValue() {
        return bd.byteValue();
    }

    @Override
    public long longValue() {
        return bd.longValue();
    }

    @Override
    public byte uByteValue() {
        return (byte) (byteValue() & 0xFF);
    }

    public int getSize() {
        return size;
    }

    public RealWrapper setSize(int size) {
        this.size = size;
        return this;
    }

    public boolean isLittleEndian() {
        return littleEndian;
    }

    public RealWrapper setLittleEndian(boolean littleEndian) {
        this.littleEndian = littleEndian;
        return this;
    }

    private void checkLE() {
        if (isLittleEndian()) {
            bd = new BigDecimal(String.valueOf(Long.reverseBytes(bd.longValue())));
        }
    }

    public double doubleValue() {
        return bd.doubleValue();
    }

    public float floatValue() {
        return bd.floatValue();
    }

    public byte[] getBytes() {
        return bytes;
    }

    public RealWrapper setBytes(byte[] bytes) {
        System.arraycopy(bytes, 0, this.bytes, 0, getSize());
        bd = new BigDecimal(new BigInteger(this.bytes));
        checkLE();
        return this;
    }

    @Override
    public byte[] getLEBytes() {
        return new RealWrapper(getSize(), getScale(), Long.reverse(longValue())).getBytes();
    }

    public int getScale() {
        return scale;
    }

    public void setScale(int scale) {
        this.scale = scale;
    }

    public RealWrapper setBytes(byte[] bytes, int offset) {
        System.arraycopy(bytes, offset, this.bytes, 0, getSize());
        bd = new BigDecimal(new BigInteger(this.bytes));
        checkLE();
        return this;
    }

    public byte[] toByteArray() {
        return SerializationUtils.serialize(this);
    }

    @Override
    public String toString() {
        return "{value=" + doubleValue() + "; size=" + getSize() + "; scale=" + getScale() + "}";
    }

    @Override
    public int hashCode() {
        return (intValue() ^ 31) * (getSize() + 1);
    }

    @Override
    public int compare(RealWrapper o1, RealWrapper o2) {
        return (int) (o1.doubleValue() - o2.doubleValue());
    }

    @Override
    public boolean equals(Object other) {
        return (other instanceof RealWrapper
                && intValue() == ((RealWrapper) other).intValue()
                && getSize() == ((RealWrapper) other).getSize());
    }
}
