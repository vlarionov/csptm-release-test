package ru.advantum.csptm.jep.hub.everfocus.handler;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.incident.model.IncidentType;
import ru.advantum.csptm.jep.core.PacketTransferException;
import ru.advantum.csptm.jep.hub.everfocus.Config;
import ru.advantum.csptm.jep.hub.everfocus.Funcs;
import ru.advantum.csptm.jep.hub.everfocus.Hub;
import ru.advantum.csptm.jep.hub.everfocus.proto.AlarmRecord15;
import ru.advantum.csptm.jep.hub.everfocus.proto.AlarmRecordBase;
import ru.advantum.csptm.jep.hub.everfocus.proto.alarm.AlarmType;
import ru.advantum.csptm.jep.hub.everfocus.wrappers.IntegerWrapper;
import ru.advantum.csptm.jep.netty.UnitIdNotFoundException;
import ru.advantum.csptm.jep.proto.CommandPacket;
import ru.advantum.csptm.jep.proto.UnitPacket;
import ru.advantum.csptm.jep.proto.UnitPacketBuilder;
import ru.advantum.csptm.service.csd.ComplexSensorProcessor;
import ru.advantum.tools.HEX;

import java.time.Instant;
import java.util.*;

@SuppressWarnings("unused")
public class Logic extends AbstractLogic<Hub> {
    private static final Logger log = LoggerFactory.getLogger(Logic.class);
    private static final String IMAGE_EXISTS = "ImageExists";
    private static final String STANDARD_INFO = "Vehicle {}, unit {}: [ timestamp = {}, alarm = {}, version {} ]";

    private static final Map<AlarmType, IncidentType.TYPE> EVENTFUL_ALARM_MAP;
    static {
        Map<AlarmType, IncidentType.TYPE> map = new HashMap<>();
        map.put(AlarmType.VideoLoss, IncidentType.TYPE.VIDEO_LOSS);
        map.put(AlarmType.HddFull, IncidentType.TYPE.HDD_FULL);
        map.put(AlarmType.NoDisk, IncidentType.TYPE.NO_DISK);
        map.put(AlarmType.AllVideoLoss, IncidentType.TYPE.ALL_VIDEO_LOSS);
        map.put(AlarmType.FanFail, IncidentType.TYPE.FAN_FAIL);
        map.put(AlarmType.HddTemperature, IncidentType.TYPE.OVER_HDD_TEMPERATURE);
        map.put(AlarmType.HddFail, IncidentType.TYPE.HDD_FAIL);
        EVENTFUL_ALARM_MAP = Collections.unmodifiableMap(map);
    }

    private Config config;
    private TreeMap<Integer, Funcs.CycleSQ> sqTreeMap = new TreeMap<>();


    public Logic(Hub hub, Config config) {
        super(hub);
        this.config = config;
    }

    public Logic() {
        super(null);
    }

    @Override
    public void channelOpen(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        reset();
        sqTreeMap.put(ctx.getChannel().getId(), new Funcs.CycleSQ());
        super.channelOpen(ctx, e);
    }

    @Override
    public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        sqTreeMap.remove(ctx.getChannel().getId());

        setDisconnectTime(Instant.now());
        try {
            sendSessionComplete(getConnectTime(),
                    getDisconnectTime(),
                    getTotalPacketCnt(),
                    getInvalidpacketCnt(),
                    getHistPacketCnt());
        } catch (PacketTransferException pte) {
            log.warn("", pte);
        }

        super.channelClosed(ctx, e);
    }

    @Override
    public void onCommandForSend(CommandPacket commandPacket) {

    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) {
        log.warn("{}", e.getCause());
        closeChannel(ctx);
    }

    private void closeChannel(ChannelHandlerContext ctx) {
        try {
            if (ctx.getChannel().isOpen()) {
                log.debug("Closing channel {}, unit {}", ctx.getChannel(), getUnitId());
                ctx.getChannel().close().await(5);
            }
        } catch (Throwable tr) {
            //log.error("{} {}", tr.getMessage(), tr);
        }
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) {
        if (e.getMessage() instanceof AlarmRecordBase) {
            AlarmRecordBase alarmRecordBase = (AlarmRecordBase) e.getMessage();
            double version = Double.parseDouble(alarmRecordBase.getVersion().asString());
            if (config.version != 0 && version != config.version) {
                log.error("Version {} doesn't support at this instance. Check config or device preferences.", version);
            } else {
                if (!isAuthenticated() && !tryToLogin(alarmRecordBase.getUniqId().asString())) {
                    return;
                }

                setConnectTime(Instant.now());

                if (version == 1.5 || version == 1.4) {
                    boolean failed = alarmRecordBase.getPresentation().getUTCtime() == 0;
                    List<UnitPacket> pkts = converter(ctx, alarmRecordBase, version == 1.5);
                    if (pkts.size() > 0 && !failed) {
                        try {
                            sendUnitPackets(pkts.toArray(new UnitPacket[pkts.size()]));
                            incTotalPacketCnt();

                            log.info(STANDARD_INFO,
                                    getUnitId(),
                                    alarmRecordBase.getUniqId().asString(),
                                    alarmRecordBase.getPresentation().getUTCtime(),
                                    alarmRecordBase.getPresentation().getAlarmType(),
                                    version
                            );
                            log.debug("\t{}", alarmRecordBase.getPresentation());
                        } catch (PacketTransferException pte) {
                            log.error("", pte);
                        }
                    }

                } else {
                    log.warn("Version '{}' () doesn't support by this software.", version, alarmRecordBase.getVersion().asString());
                }

            }
        }
    }

    private boolean tryToLogin(String unitId) {
        try {
            initUnitId(unitId);
        } catch (UnitIdNotFoundException nf1) {
            log.warn("Authentication failed: {}", nf1.getMessage());
            return false;
        }
        return true;
    }

    private boolean isAuthenticated() {
        return getUnitId() != null;
    }

    private List<UnitPacket> converter(ChannelHandlerContext ctx, AlarmRecordBase base, boolean v15) {
        List<UnitPacket> unitPackets = new ArrayList<>();
        UnitPacketBuilder unitPacketBuilder = UnitPacket.newBuilder()
                .setUnitId(getUnitId())
                .setEventTime(Instant.now());

        AlarmRecordBase.Presentation presentation = base.getPresentation();

        if (v15) {
            IntegerWrapper seqNumber = ((AlarmRecord15) base).getSeqNumber();
            unitPacketBuilder.setPacketId(seqNumber.longValue());
            sendReply(base.getAlarmTime(), seqNumber);
        } else {
            unitPacketBuilder.setPacketId(sqTreeMap.get(ctx.getChannel().getId()).getNext());
        }

        AlarmType alarmType = presentation.getAlarmType();
        IncidentType.TYPE eventType = EVENTFUL_ALARM_MAP.get(alarmType);

        if (eventType != null) {
            unitPacketBuilder.addFlag(eventType.name());
            unitPackets.add(unitPacketBuilder.build());
        } else {
            int alarmSource = Integer.parseInt(base.getAlarmSource().asString().isEmpty() ? "-1" : base.getAlarmSource().asString());
            int sensorNum = config.startSensorOffset + AlarmType.getSensorNumberByAlarm(alarmType, alarmSource);

            if (sensorNum >= config.startSensorOffset) {
                unitPacketBuilder.addComplexSensors(ComplexSensorProcessor.decode(sensorNum, Boolean.TRUE));
                if (presentation.isImageExists()) {
                    unitPacketBuilder.addFlag(IMAGE_EXISTS);
                }

                unitPackets.add(unitPacketBuilder.build());
            } else {
                log.debug("Alarm {} of undefined sensor #{}", alarmType, alarmSource);
                incInvalidpacketCnt();
            }
        }

        return unitPackets;
    }

    private void sendReply(IntegerWrapper alarmTime, IntegerWrapper seqNumber) {
        ChannelBuffer sendBuf = ChannelBuffers.dynamicBuffer(16);

        if (alarmTime.intValue() == 0) {
            alarmTime.setValue((int) (System.currentTimeMillis() / 1000));
        }
        sendBuf.writeBytes(alarmTime.getLEBytes());


        if (seqNumber.intValue() == 0) {
            seqNumber.setValue(1);
        }
        sendBuf.writeBytes(seqNumber.getLEBytes());

        log.debug("Reply to {}: {}", getUnitId(), Funcs.bytesToHexString(sendBuf.array(), sendBuf.capacity()));
        sendToUnit(sendBuf.array());
    }


    public static void main(String... a) {
        IntegerWrapper alarmTime = (IntegerWrapper) new IntegerWrapper(4).setLittleEndian(true);
        IntegerWrapper seqNumber = (IntegerWrapper) new IntegerWrapper(4).setLittleEndian(true);


        String in = "fde1d4580e000000";
        ChannelBuffer buffer = ChannelBuffers.copiedBuffer(HEX.stringToByteArray(in));
        alarmTime.read(buffer);
        seqNumber.read(buffer);


        Logic l = new Logic();
        l.sendReply(alarmTime, seqNumber);
    }

}
