package ru.advantum.csptm.jep.hub.everfocus.proto.alarm;

public enum CompressionType {
    MPEG(48),
    JPEG(49),
    MPEG4(50),
    H264(51),
    Unknown(-1);

    int code;

    CompressionType(int code) {
        setCode(code);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public static CompressionType lookup(int code) {
        for (CompressionType t : CompressionType.values()) {
            if (t.getCode() == code) {
                return t;
            }
        }
        return Unknown;
    }

}
