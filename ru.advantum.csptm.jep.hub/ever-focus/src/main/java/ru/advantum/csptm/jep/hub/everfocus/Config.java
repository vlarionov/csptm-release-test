package ru.advantum.csptm.jep.hub.everfocus;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.csptm.jep.netty.NettyHubConfig;

@Root(name = "everfocus-hub")
public class Config {
    @Attribute()
    public double version = 1.4;

    @Attribute(name = "start-sensor-offset", required = false)
    public int startSensorOffset = 3000;

    @Element(name = "hub")
    public final NettyHubConfig hubConfig = new NettyHubConfig();

}
