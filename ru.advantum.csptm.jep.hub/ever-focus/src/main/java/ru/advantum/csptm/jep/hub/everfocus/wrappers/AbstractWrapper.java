package ru.advantum.csptm.jep.hub.everfocus.wrappers;

import org.jboss.netty.buffer.ChannelBuffer;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

/**
 * @author Mitsay <mitsay@advantum.ru>
 * @since 18.01.16 13:34.
 */
public abstract class AbstractWrapper implements Serializable {
    protected boolean littleEndian = false;

    public abstract int read(InputStream in) throws IOException;

    public abstract int read(ChannelBuffer buffer);

    public abstract short shortValue();

    public abstract int intValue();

    public abstract int uIntValue();

    public abstract byte byteValue();

    public abstract long longValue();

    public abstract byte uByteValue();

    public abstract double doubleValue();

    public abstract byte[] getBytes();

    public abstract byte[] getLEBytes();

    public abstract int getSize();

    public boolean isLittleEndian() {
        return littleEndian;
    }

    public AbstractWrapper setLittleEndian(boolean littleEndian) {
        this.littleEndian = littleEndian;
        return this;
    }

}
