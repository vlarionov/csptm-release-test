package ru.advantum.csptm.jep.hub.everfocus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.concurrent.atomic.AtomicLong;

public class Funcs {
    private static final Logger log = LoggerFactory.getLogger(Funcs.class);

    public static String uniToString(Object o) {
        StringBuilder sb = new StringBuilder();
        for (Field f : o.getClass().getDeclaredFields()) {
            try {
                f.setAccessible(true);
                if (f.getGenericType().getTypeName().equals("int")) {
                    sb.append(f.getName()).append(":").append(f.get(o)).append(";");
                } else if (f.getGenericType().getTypeName().equals("boolean")) {
                    sb.append(f.getName()).append(":").append(f.get(o)).append(";");
                } else if (f.getGenericType().getTypeName().equals("long")) {
                    sb.append(f.getName()).append(":").append(f.get(o)).append(";");
                } else if (f.getGenericType().getTypeName().equals("java.lang.String")) {
                    sb.append(f.getName()).append(":").append((String) f.get(o)).append(";");
                } else if (f.getGenericType().getTypeName().equals("java.util.UUID")) {
                    sb.append(f.getName()).append(":").append(f.get(o).toString()).append(";");
                } else {
                    sb.append(f.getName()).append(":\"").append(f.get(o).toString()).append("\";");
                }
            } catch (Exception e) {
                log.error("", e);
            }
        }
        return sb.toString();
    }

    public static String bytesToHexString(final byte[] bytes, int i) {
        final StringBuilder buf = new StringBuilder();
        try {
            int n = 0;
            for (final byte b : bytes) {
                if (n++ < i) {
                    buf.append(String.format("%02X", b & 0xFF));
                } else break;
            }
        } catch (Throwable t) {
            log.warn("{}", t.getMessage());
            log.debug("", t);
        }
        return buf.toString();
    }

    public static class CycleSQ extends AtomicLong {
        private long last;

        public CycleSQ(int initialValue) {
            super(initialValue);
        }

        public CycleSQ() {
            super(0);
        }

        public long getLast() {
            return this.last;
        }

        public long getNext() {
            this.compareAndSet(65536, 0);
            this.last = this.getAndIncrement();
            return this.last;
        }
    }

}
