package ru.advantum.csptm.jep.hub.everfocus.proto.alarm;

@SuppressWarnings("unused")
public enum AlarmType {

    ExtAlarmInput(1),
    MotionDetection(2),
    VideoLoss(3),
    HddFull(4),
    NoDisk(5),
    AllVideoLoss(6),
    FanFail(7),
    HddTemperature(8),
    HddFail(13),
    AlarmInputClose(9),
    AlarmInputOpen(10),
    AlarmOutputClose(11),
    AlarmOutputOpen(12),
    Undefined(-1);


    public static AlarmType lookup(int code) {
        for (AlarmType t : AlarmType.values()) {
            if ((t.getCodeFrom() == code) || (t.getCodeFrom() >= code && t.codeTo <= code)) {
                return t;
            }
        }
        return Undefined;
    }

    public static int getDeviceNumberByAlarm(AlarmType t, int source) {
        switch (t) {
            case MotionDetection:
            case VideoLoss:
            case AllVideoLoss:
                if (source >= 0 && source < 16) {
                    return source + 1;
                }
                break;
            case ExtAlarmInput:
            case AlarmInputClose:
            case AlarmInputOpen:
            case AlarmOutputClose:
            case AlarmOutputOpen:
                if (source >= 0 && source < 24) {
                    return source + 1;
                }
                break;

            default:
                break;

        }
        return Undefined.getCodeFrom();
    }

    public static int getSensorNumberByAlarm(AlarmType t, int source) {
        switch (t) {
            case ExtAlarmInput:
                if (source >= 0 && source < 24) {
                    return source + 1;
                }
                break;
            case AlarmInputClose:
            case AlarmInputOpen:
            case AlarmOutputClose:
            case AlarmOutputOpen:
                if (source >= 0 && source < 24) {
                    return source + 25;
                }
                break;
            case MotionDetection:
            case VideoLoss:
            case HddFull:
            case NoDisk:
            case AllVideoLoss:
            case FanFail:
            case HddTemperature:
            case HddFail:
                if (source >= 0 && source < 16) {
                    return source + 49;
                }
                break;
            default:
                break;

        }
        return Undefined.getCodeFrom();
    }

    int codeFrom;
    int codeTo;

    AlarmType(int codeFrom) {
        setCodeFrom(codeFrom);
    }

    AlarmType(int codeFrom, int codeTo) {
        this(codeFrom);
        setCodeTo(codeTo);
    }

    public int getCodeFrom() {
        return codeFrom;
    }

    public void setCodeFrom(int codeFrom) {
        this.codeFrom = codeFrom;
    }

    public int getCodeTo() {
        return codeTo;
    }

    public void setCodeTo(int codeTo) {
        this.codeTo = codeTo;
    }

}
