package ru.advantum.csptm.jep.hub.everfocus.handler;

import ru.advantum.csptm.jep.netty.JepNettyConnectionHandler;
import ru.advantum.csptm.jep.netty.JepNettyHub;

import java.time.Instant;

public abstract class AbstractLogic<HUB extends JepNettyHub> extends JepNettyConnectionHandler<HUB> {

    private Instant connectTime;
    private Instant disconnectTime;
    private long totalPacketCnt;
    private long invalidpacketCnt;
    private long histPacketCnt;


    public AbstractLogic(HUB hub) {
        super(hub);
    }

    protected void reset() {
        setConnectTime(null);
        setDisconnectTime(null);
        setTotalPacketCnt(0);
        setInvalidpacketCnt(0);
        setHistPacketCnt(0);
    }

    public Instant getConnectTime() {
        return connectTime;
    }

    public void setConnectTime(Instant connectTime) {
        this.connectTime = connectTime;
    }

    public Instant getDisconnectTime() {
        return disconnectTime;
    }

    public void setDisconnectTime(Instant disconnectTime) {
        this.disconnectTime = disconnectTime;
    }

    public long getTotalPacketCnt() {
        return totalPacketCnt;
    }

    public void incTotalPacketCnt() {
        this.totalPacketCnt++;
    }

    public void add2TotalPacketCnt(long totalPacketCnt) {
        this.totalPacketCnt += totalPacketCnt;
    }

    public void setTotalPacketCnt(long totalPacketCnt) {
        this.totalPacketCnt = totalPacketCnt;
    }

    public long getInvalidpacketCnt() {
        return invalidpacketCnt;
    }

    public void incInvalidpacketCnt() {
        this.invalidpacketCnt++;
    }

    public void add2InvalidpacketCnt(long invalidpacketCnt) {
        this.invalidpacketCnt += invalidpacketCnt;
    }

    public void setInvalidpacketCnt(long invalidpacketCnt) {
        this.invalidpacketCnt = invalidpacketCnt;
    }

    public long getHistPacketCnt() {
        return histPacketCnt;
    }

    public void incHistPacketCnt() {
        this.histPacketCnt++;
    }

    public void add2HistPacketCnt(long histPacketCnt) {
        this.histPacketCnt += histPacketCnt;
    }

    public void setHistPacketCnt(long histPacketCnt) {
        this.histPacketCnt = histPacketCnt;
    }

}
