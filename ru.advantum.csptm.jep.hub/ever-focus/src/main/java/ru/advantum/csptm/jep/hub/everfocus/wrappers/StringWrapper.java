package ru.advantum.csptm.jep.hub.everfocus.wrappers;

import org.apache.commons.lang.SerializationUtils;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * @author Mitsay <mitsay@advantum.ru>
 * @since 18.01.16 11:17.
 */
public class StringWrapper extends AbstractWrapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(StringWrapper.class);

    private byte[] bytes = new byte[0];
    private int size = 0;

    public StringWrapper(int size) {
        bytes = new byte[size];
        setSize(size);
    }

    public StringWrapper(byte[] bytes) {
        this(bytes.length);
        System.arraycopy(bytes, 0, this.bytes, 0, getSize());
    }

    /**
     * Cloner
     *
     * @param other class
     */
    public StringWrapper(StringWrapper other) {
        this(other.getSize());
        setBytes(other.getBytes());
    }

    public StringWrapper(ChannelBuffer buffer) {
        this(buffer.writerIndex() - buffer.readerIndex());
        read(buffer);
    }

    public int read(InputStream in) throws IOException {
        int ret = in.read(this.bytes);
        checkLE();
        return ret;
    }

    public int read(ChannelBuffer buffer) {
        buffer.readBytes(getBytes());
        checkLE();
        return getSize();
    }

    private void checkLE() {
        if (isLittleEndian()) {
            setBytes(flipBytes());
        }
    }


    @Override
    public short shortValue() {
        return -1;
    }

    @Override
    public int intValue() {
        return -1;
    }

    @Override
    public int uIntValue() {
        return -1;
    }

    @Override
    public byte byteValue() {
        return -1;
    }

    @Override
    public long longValue() {
        return -1;
    }

    @Override
    public byte uByteValue() {
        return -1;
    }

    @Override
    public double doubleValue() {
        return -1;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public StringWrapper setBytes(byte[] bytes) {
        System.arraycopy(bytes, 0, this.bytes, 0, getSize());
        return this;
    }

    @Override
    public byte[] getLEBytes() {
        return flipBytes();
    }

    private byte[] flipBytes() {
        byte[] doubles = new byte[getSize()];
        for (int i = 0; i < getSize(); i++) {
            doubles[getSize() - i - 1] = bytes[i];
        }
        return doubles;
    }

    public int getSize() {
        return size;
    }

    public StringWrapper setSize(int size) {
        this.size = size;
        return this;
    }

    public ChannelBuffer asChannel() {
        ChannelBuffer ch = ChannelBuffers.dynamicBuffer();
        ch.setBytes(0, getBytes(), 0, getBytes().length);
        return ch;
    }

    public String asString() {
        return new String(getBytes()).replace("\0","");
    }

    public String asString(Charset ch) {
        return new String(getBytes(), ch);
    }

    @Override
    public String toString() {
        return "{asString=" + asString() + "; size=" + getSize() + "}";
    }

    @Override
    public int hashCode() {
        int code = 0;
        for (byte b : getBytes()) {
            code += (int) b;
        }
        return (code ^ 31) * (getSize() + 1);
    }

    @Override
    public boolean equals(Object other) {
        return (other instanceof StringWrapper
                && getSize() == ((StringWrapper) other).getSize()
                && ((new String(getBytes()))
                .equals((new String(((StringWrapper) other).getBytes())))));
    }

    public byte[] toByteArray() {
        return SerializationUtils.serialize(this);
    }
}
