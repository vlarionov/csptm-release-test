package ru.advantum.csptm.jep.hub.everfocus.proto;

import org.jboss.netty.buffer.ChannelBuffer;
import ru.advantum.csptm.jep.hub.everfocus.Funcs;
import ru.advantum.csptm.jep.hub.everfocus.wrappers.IntegerWrapper;
import ru.advantum.csptm.jep.hub.everfocus.wrappers.StringWrapper;

@SuppressWarnings("unused")
public class AlarmRecord15 extends AlarmRecordBase {
    /*
    Эти типы подобраны опытным путем и совпадают с документацией только по длинам
     */
    private StringWrapper vehicleId = new StringWrapper(16);
    private IntegerWrapper seqNumber = (IntegerWrapper) new IntegerWrapper(4).setLittleEndian(true);
    private StringWrapper hddId = new StringWrapper(16);
    private IntegerWrapper imageFlag = new IntegerWrapper(1);

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + ":" + Funcs.uniToString(this);
    }

    public AlarmRecord15(ChannelBuffer buffer) {
        super(61);
        read(buffer);
    }

    public AlarmRecord15 read(ChannelBuffer in) {
        getUniqId().read(in);
        getAlarmSource().read(in);
        getCameraNumber().read(in);
        getCompressionType().read(in);
        getAlarmType().read(in);
        getImageSize().read(in);
        getVehicleId().read(in);
        getAlarmTime().read(in);
        getSeqNumber().read(in);
        getHddId().read(in);
        getImageFlag().read(in);
        in.readBytes(getReserved());

        return this;
    }

    public StringWrapper getHddId() {
        return hddId;
    }

    public void setHddId(StringWrapper hddId) {
        this.hddId = hddId;
    }

    public StringWrapper getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(StringWrapper vehicleId) {
        this.vehicleId = vehicleId;
    }

    public IntegerWrapper getSeqNumber() {
        return seqNumber;
    }

    public void setSeqNumber(IntegerWrapper seqNumber) {
        this.seqNumber = seqNumber;
    }

    public IntegerWrapper getImageFlag() {
        return imageFlag;
    }

    public void setImageFlag(IntegerWrapper imageFlag) {
        this.imageFlag = imageFlag;
    }
}
