package ru.advantum.csptm.jep.hub.everfocus.handler;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.hub.everfocus.Config;
import ru.advantum.csptm.jep.hub.everfocus.proto.AlarmRecord14;
import ru.advantum.csptm.jep.hub.everfocus.proto.AlarmRecord15;
import ru.advantum.csptm.jep.hub.everfocus.proto.GpsHdr;
import ru.advantum.csptm.jep.hub.everfocus.proto.GpsPosData;
import ru.advantum.csptm.jep.hub.everfocus.wrappers.AbstractWrapper;
import ru.advantum.csptm.jep.hub.everfocus.wrappers.IntegerWrapper;
import ru.advantum.csptm.jep.hub.everfocus.wrappers.StringWrapper;
import ru.advantum.csptm.jep.netty.SimpleStateDecoder;
import ru.advantum.tools.HEX;

import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("unused")
public class Decoder extends SimpleStateDecoder {
    private static final Logger log = LoggerFactory.getLogger(Decoder.class);
    private final Config config;

    public Decoder(Config config) {
        this.config = config;
    }

    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel channel, ChannelBuffer buffer, STATE state) {

        if (state == STATE.HANDSHAKE) {
            try {
                AbstractWrapper head = isGPS(buffer);
                if (head instanceof IntegerWrapper) {
                    GpsHdr gpsHdr = new GpsHdr(buffer);
                    gpsHdr.setMarker((IntegerWrapper) head);
                    setState(STATE.DATA);
                    return gpsHdr;

                } else {
                    if (((StringWrapper) head).asString().equals("1.4")) {
                        AlarmRecord14 ar14 = new AlarmRecord14(buffer);
                        ar14.setVersion((StringWrapper) head);
                        return ar14;

                    } else if (((StringWrapper) head).asString().equals("1.5")) {
                        AlarmRecord15 ar15 = new AlarmRecord15(buffer);
                        ar15.setVersion((StringWrapper) head);
                        return ar15;

                    } else {
                        log.warn("Version {} doesn't support by this software.",
                                ((StringWrapper) head).asString());
                        buffer.readBytes(new byte[buffer.writerIndex() - buffer.readerIndex()]);
                        channel.close();
                    }
                }
            } catch (Exception e) {
                log.warn("{}", e.getMessage());
                log.debug("", e);
            }

        } else {
            List<GpsPosData> list = new LinkedList<>();
            while (buffer.readable()) {
                GpsPosData gpd = new GpsPosData(buffer);
                list.add(gpd);
            }
            return list;
        }
        return new byte[32];
    }

    private AbstractWrapper isGPS(ChannelBuffer buffer) {
        IntegerWrapper marker = (IntegerWrapper) new IntegerWrapper(4).setLittleEndian(true);
        marker.read(buffer);
        if (marker.intValue() == GpsHdr.HDR) {
            return marker;
        }
        buffer.readerIndex(buffer.readerIndex() - 4);
        StringWrapper res = new StringWrapper(3);
        res.read(buffer);
        return res;
    }

    public static void main(String... a) {

        String in = "312e3530303030303030303031000000000000000000000000004d61696e5f7469746c65000000000000000000000000000057442d574341563555323731363534000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
        ChannelBuffer buffer = ChannelBuffers.copiedBuffer(HEX.stringToByteArray(in));



        Decoder l = new Decoder(null);
        l.decode(null, null, buffer, STATE.HANDSHAKE);
    }

}
