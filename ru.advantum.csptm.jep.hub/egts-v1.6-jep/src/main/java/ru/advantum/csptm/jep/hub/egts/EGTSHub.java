package ru.advantum.csptm.jep.hub.egts;

import org.jboss.netty.channel.ChannelPipeline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.jep.core.JepHubController;
import ru.advantum.csptm.jep.core.rmq.JepRmqHubController;
import ru.advantum.csptm.jep.hub.egts.handler.EraDecoder;
import ru.advantum.csptm.jep.hub.egts.handler.LogicClient;
import ru.advantum.csptm.jep.hub.egts.packet.EGTSFactory;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensorType;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.SensorNumerator;
import ru.advantum.csptm.jep.netty.JepNettyHub;

import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Map;

public class EGTSHub extends JepNettyHub {

    public static final String CONFIG_FILE_NAME = "hub.xml";
    private static final Logger log = LoggerFactory.getLogger(EGTSHub.class);

    private EGTSHubConfig config;
    private EGTSHub(EGTSHubConfig config, JepHubController controller) {
        super(config.hubConfig.tcpConfig, controller);
        this.config = config;
    }

    @Override
    protected ByteOrder getByteOrder() {
        return ByteOrder.LITTLE_ENDIAN;
    }

    @Override
    protected void tunePipeline(ChannelPipeline pipeline) {
        EGTSFactory factory = new EGTSFactory();
        pipeline.addLast("decoder", new EraDecoder(factory));
        pipeline.addLast("logic", new LogicClient(this, factory));
    }

    public static void main(String[] args) throws Throwable {
        EGTSHubConfig egtsHubConfig = Configuration.unpackConfig(EGTSHubConfig.class, CONFIG_FILE_NAME);
        /*

    Конфигурирование работы с датчиками ЕГТС:

    Тег sensor-numerator-type указывает систему нумерации. Возможные (допустимые) значения:
        DEFAULT, - по умолчанию
        CAN, - что тут комментировать?
        CUSTOM - см. ниже

    Тег sensor-numerator-custom определяет типы датчиков и настройки трансляции номеров для sensor-numerator-type = CUSTOM.
    Если тег sensor-numerator-custom определен и внутри списка есть хотя бы одна запись sensor-define-entry,
    то sensor-numerator-type будет принудительно переставлен в CUSTOM. Если не определено ни одной
    записи sensor-define-entry, то sensor-numerator-type будет оставлен в DEFAULT или CAN, а если изначально был в CUSTOM,
    то переставлен в DEFAULT.

    Элемент списка sensor-define-entry определяет один тип датчиков. Атрибуты:
        sensor-type - текст, см. ниже, уникальность не контролируется, в работу пойдет нижнее определение.
        offset - целое число, с какого номера начинается диапазон
        max-number - - целое число, на каком номере (искл) заканчивается диапазон
    Отслеживание пересечений диапазонов на совести создателя конфига.

    Возможные (допустимые) значения атрибута sensor-type (рег. незав.):
        EMBEDDED,
        DIGITAL_INPUT,
        DIGITAL_OUTPUT,
        ANALOG_SENSOR,
        COUNTER,
        LOOPIN,
        LIQUID_LEVEL,
        PASSENGERS_COUNTER,
        CAN;

     При конфиге из примера ниже, sensor-numerator-type будет принудительно переставлен в CUSTOM,
     встроенные датчики принимаются в диапазоне 0...32,
     аналоговые 32...200,
     жидкостные (соотв. подзапись) 200...255

    <sensor-numerator-type>DEFAULT</sensor-numerator-type>
    <sensor-numerator-custom>
        <sensor-define-entry sensor-type="EMBEDDED" offset="0" max-number="32"/>
        <sensor-define-entry sensor-type="ANALOG_SENSOR" offset="32" max-number="200"/>
        <sensor-define-entry sensor-type="LIQUID_LEVEL" offset="200" max-number="256"/>
    </sensor-numerator-custom>

         */
        if (egtsHubConfig.snCustom != null) {
            SensorNumerator.NumeratorType oldSNT = egtsHubConfig.sensorNumeratorType;

            if (egtsHubConfig.sensorNumeratorType != SensorNumerator.NumeratorType.CUSTOM) {
                egtsHubConfig.sensorNumeratorType = SensorNumerator.NumeratorType.CUSTOM;
                log.warn("<sensor-numerator-custom> list defined then <sensor-numerator-type> sat to {}",
                        egtsHubConfig.sensorNumeratorType);
            }
            Map<EGTSSensorType, SensorNumerator.Settings> settingsMap = new HashMap<>();
            for (EGTSHubConfig.CustomNumeratorEntry e : egtsHubConfig.snCustom) {
                EGTSSensorType t = EGTSSensorType.lookupByName(e.name);
                if (t != null) {
                    settingsMap.put(t, new SensorNumerator.Settings((short) e.offset, (short) e.maxNumber));
                } else {
                    log.warn("\tEntry '{}' couldn't be resolved", e.name);
                }
            }
            if (settingsMap.size() == 0) {
                if (oldSNT == SensorNumerator.NumeratorType.CUSTOM) {
                    oldSNT = SensorNumerator.NumeratorType.DEFAULT;
                }
                egtsHubConfig.sensorNumeratorType = oldSNT;
                log.warn("<sensor-numerator-custom> list empty or malformed then <sensor-numerator-type> sat to {}",
                        egtsHubConfig.sensorNumeratorType);
                settingsMap = null;
            }
            SensorNumerator.setNumeratorType(egtsHubConfig.sensorNumeratorType, settingsMap);
        }
        //**


        JepRmqHubController controller = new JepRmqHubController(egtsHubConfig.hubConfig.controllerConfig);
        EGTSHub hub = new EGTSHub(egtsHubConfig, controller);
        controller.start(hub);
    }

    public EGTSHubConfig getConfig() {
        return this.config;
    }
}
