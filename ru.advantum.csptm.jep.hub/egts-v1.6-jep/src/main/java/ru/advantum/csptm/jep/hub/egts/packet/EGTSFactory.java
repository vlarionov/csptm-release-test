package ru.advantum.csptm.jep.hub.egts.packet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.hub.egts.AbstractFactory;
import ru.advantum.csptm.jep.hub.egts.handler.EraResponseCodes;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_RECORD_RESPONSE;
import ru.advantum.csptm.jep.hub.egts.packet.transp.EGTS_Transport;
import ru.advantum.csptm.jep.hub.egts.packet.transp.EGTS_Transport_HCS;
import ru.advantum.csptm.jep.hub.egts.packet.transp.EGTS_Transport_RTE;
import ru.advantum.csptm.jep.hub.egts.packet.transp.EGTS_Transport_SFRCS;
import ru.advantum.tools.HEX;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EGTSFactory extends AbstractFactory {

    protected static final Logger LOGGER = LoggerFactory.getLogger(EGTSFactory.class);

    private CycleSQ transportSQ = new CycleSQ(1);
    private CycleSQ frameSQ = new CycleSQ(1);
    //    private List<Integer> rnList = new ArrayList<>();

    public EGTSFactory(EGTS_Transport_RTE.RTE RTE) {
        super(RTE);
    }

    public EGTSFactory() {
        this.RTE = null;
    }

    private byte[] makeTransportLayer(PT pt, EGTS_Transport_RTE.RTE RTE, EGTSBase... frames) throws IOException {
        final EGTS_Transport header = new EGTS_Transport();
        final EGTS_Transport_RTE header_RTE = (RTE == null ? null : new EGTS_Transport_RTE());
        final EGTS_Transport_HCS header_HCS = new EGTS_Transport_HCS();
        //..<BINARY>..
        final EGTS_Transport_SFRCS header_SFRCS = new EGTS_Transport_SFRCS();

        final byte $serviceSupport[];

        { // ==> Заголовок Протокола Транспортного уровня
            header.setPRV(PRV_VERSION);
            header.setSKID(SKID);
            header.setPRF(0x00);

            header.setENA(0x00);
            header.setCMP(CMD_UNCOMPRESSED);
            header.setPR(0x00);
            header.setHE(0x00);
            header.setPID(transportSQ.getNext());
            header.setPT((short) pt.getId());

            if ((header_RTE != null)) {
                header_RTE.PRA.set(RTE.PRA);
                header_RTE.RCA.set(RTE.RCA);
                header_RTE.setTTL(RTE.TTL);

                header.setRTE(0x01);
                header.setHL(header.size() + header_RTE.size() + header_HCS.size());
            } else {
                header.setRTE(0x00);
                header.setHL(header.size() + header_HCS.size());
            }

            { // ==> Данные Уровня Поддержки услуг
                $serviceSupport = EGTSBase.toByteArray(frames);

                // ==> Контрольная Сумма Данных Уровня Поддержки Услуг
                if (frames.length > 0) {
                    header.setFDL($serviceSupport.length);
                    header_SFRCS.SFRCS.set(HEX.crc16citt($serviceSupport, EGTSBase.SFRCS_POLYNOMIAL));
                } else {
                    header.setFDL(0);
                    header_SFRCS.SFRCS.set(0);
                }
            }

            header_HCS.HCS.set(HEX.crc8(EGTSBase.toByteArray(header, header_RTE)));
        }

        try (ByteArrayOutputStream total = new ByteArrayOutputStream()) {
            total.write(EGTSBase.toByteArray(header, header_RTE, header_HCS));
            total.write($serviceSupport);
            header_SFRCS.write(total);

            total.flush();
            return total.toByteArray();
        }
    }

    private EGTSBase[] serviceRecordHeaderMaker(int rst,
                                                int sst,
                                                EGTS_SERVICE.OID oid,
                                                EGTS_SERVICE.EVID evid,
                                                EGTS_SERVICE.TM tm,
                                                boolean auth) {
        //        this.rnList.clear();
        EGTSBase[] result = new EGTSBase[2];
        result[0] = new EGTS_SERVICE();
        result[1] = new EGTS_SERVICE_ST();
        if (auth) {
            frameSQ.set(1);
        }
        ((EGTS_SERVICE) result[0]).RN.set(frameSQ.getNext());
        //        this.rnList.add(frameSQ.getLast());

        //        LOGGER.info("Hdr maker: id_tr: {}, sent pkgnum: {} ",
        //                oid != null? oid.value.get() : "0", frameSQ.getLast());

        ((EGTS_SERVICE) result[0]).SSOD.set(0x00);
        ((EGTS_SERVICE) result[0]).RSOD.set(0x00);
        ((EGTS_SERVICE) result[0]).GPR.set(0x00);
        ((EGTS_SERVICE) result[0]).setRPP(0x00);
        ((EGTS_SERVICE) result[0]).TMFE.set(tm != null ? 1 : 0);
        ((EGTS_SERVICE) result[0]).EVFE.set(evid != null ? 1 : 0);
        ((EGTS_SERVICE) result[0]).OBFE.set(oid != null ? 1 : 0);

        ((EGTS_SERVICE_ST) result[1]).RST.set(rst);
        ((EGTS_SERVICE_ST) result[1]).SST.set(sst);
        return result;
    }

    private EGTSBase[] makeServiceLayer(int rst,
                                        int sst,
                                        EGTS_SERVICE_SUBRECORD.Type type,
                                        EGTS_SERVICE.OID oid,
                                        EGTS_SERVICE.EVID evid,
                                        EGTS_SERVICE.TM tm,
                                        EGTSServiceSubRecord[] RD) {
        EGTSBase[] serv = serviceRecordHeaderMaker(rst, sst, oid, evid, tm, type == EGTS_SERVICE_SUBRECORD.Type.EGTS_SR_TERM_IDENTITY);
        final EGTS_SERVICE service = (EGTS_SERVICE) serv[0];
        final EGTS_SERVICE_ST ST = (EGTS_SERVICE_ST) serv[1];
        List<EGTSBase> res = new ArrayList<>();
        res.add(service);
        if (oid != null) {
            res.add(oid);
        }
        if (evid != null) {
            res.add(evid);
        }
        if (tm != null) {
            res.add(tm);
        }
        res.add(ST);
        int totalSize = 0;
        for (EGTSServiceSubRecord rd : RD) {
            int size = 0;
            final EGTSBase[] frames = rd.toFrames();
            for (EGTSBase f : frames) {
                size += f.size();
            }
            res.add(new EGTS_SERVICE_SUBRECORD(type, size));
            res.addAll(Arrays.asList(frames));

            totalSize += size + EGTS_SERVICE_SUBRECORD.SIZE;
        }
        service.RL.set(totalSize); //!
        return res.toArray(new EGTSBase[res.size()]);
    }

    private EGTSBase[] makeResponseSubRecords(int rst,
                                              int sst,
                                              int PID,
                                              EraResponseCodes code,
                                              EGTS_SR_RECORD_RESPONSE... response) {
        final EGTS_PT_RESPONSE pt = new EGTS_PT_RESPONSE();
        pt.RPID.set(PID);
        pt.PR.set(code.getValue());
        List<EGTSBase> frame = new ArrayList<>();
        frame.add(pt);

        frame.addAll(Arrays.asList(makeServiceLayer(rst,
                                                    sst,
                                                    EGTS_SERVICE_SUBRECORD.Type.EGTS_SR_RECORD_RESPONSE,
                                                    null,
                                                    null,
                                                    null,
                                                    response)));

        return frame.toArray(new EGTSBase[frame.size()]);
    }

    public byte[] response(int rst, int sst, int RPID, EraResponseCodes code, EGTS_SR_RECORD_RESPONSE... response)
            throws IOException {
        EGTSBase[] resp = makeResponseSubRecords(rst, sst, RPID, code, response);
        return makeTransportLayer(PT.EGTS_PT_RESPONSE, null, resp);
    }

    public static float fromEGTSLon(long lon, boolean LOHS) {
        return lon * 180f / LONG_CONST * (LOHS ? -1f : 1f);
    }

    public static float fromEGTSLat(long lat, boolean LAHS) {
        return lat * 90f / LONG_CONST * (LAHS ? -1f : 1f);
    }

    public EraResponseCodes validateBasicTransportLayer(final EGTS_Transport header) {
        if (header.getPRV() != PRV_VERSION) {
            return EraResponseCodes.PC_UNS_PROTOCOL;
        }
        if (header.getCMP() != CMD_UNCOMPRESSED) {
            return EraResponseCodes.PC_UNS_PROTOCOL;
        }
        final int pt = header.getPT();
        if (pt != PT.EGTS_PT_APPDATA.getId() && pt != PT.EGTS_PT_RESPONSE.getId()) {
            return EraResponseCodes.PC_SRVC_UNKN;
        }
        return EraResponseCodes.PC_OK;
    }
}
