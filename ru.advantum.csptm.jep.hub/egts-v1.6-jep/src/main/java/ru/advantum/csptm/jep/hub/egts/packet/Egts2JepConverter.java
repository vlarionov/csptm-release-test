package ru.advantum.csptm.jep.hub.egts.packet;

import org.jboss.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.hub.egts.EGTSHubConfig;
import ru.advantum.csptm.jep.hub.egts.handler.EraResponseCodes;
import ru.advantum.csptm.jep.hub.egts.handler.LogicClient;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensorsHolder;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.SensorNumerator;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_LIQUID_LEVEL_SENSOR;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_POS_DATA;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_RECORD_RESPONSE;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_STATE_DATA;
import ru.advantum.csptm.jep.proto.CommonPacketFlag;
import ru.advantum.csptm.jep.proto.TelematicPacket;
import ru.advantum.csptm.jep.proto.UnitPacket;
import ru.advantum.csptm.jep.proto.UnitPacketBuilder;
import ru.advantum.csptm.jep.proto.sensors.ComplexSensorHolder;
import ru.advantum.csptm.jep.proto.sensors.Type;
import ru.advantum.csptm.service.csd.ComplexSensorProcessor;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Egts2JepConverter {

    private static final int EGTS_DEVICE_EVENT_SRC_ALERT = 13; // см. Таблица №3

    private static final Logger log = LoggerFactory.getLogger(Egts2JepConverter.class);

    public UnitPacket[] convert(ChannelHandlerContext ctx, EGTSHubConfig config, EGTSFactory.ServiceData in) {
        final long idTr;
        List<UnitPacket> unitPackets = new ArrayList<>();
        Map<Integer, BigDecimal> sensors = new HashMap<>();
        UnitPacketBuilder lastPacketBuilder = null;

        try {
            if ((idTr = findId(ctx, in)) != LogicClient.DEFAULT_CHANNEL_OWNER_ID) {

                for (EGTSServiceSubRecord p : in.RD) {
                    if (p instanceof EGTSSensorsHolder) {
                        if (lastPacketBuilder != null) {
                            lastPacketBuilder.addSensors(SensorNumerator.extractSensors((EGTSSensorsHolder) p));
                            sensors.clear();
                        } else {
                            sensors.putAll(SensorNumerator.extractSensors((EGTSSensorsHolder) p));
                        }
                    }

                    if (p instanceof EGTS_SR_POS_DATA) {
                        final EGTS_SR_POS_DATA pos = (EGTS_SR_POS_DATA) p;

                        if (lastPacketBuilder != null) {
                            unitPackets.add(lastPacketBuilder.build());
                        }

                        Instant time = Instant.ofEpochSecond((in.tm != null ? in.tm.value.get() : pos.NTM.get()) + EGTSFactory.DATE_01_01_2010);
                        float lon = EGTSFactory.fromEGTSLon(pos.LONG.get(), pos.isLOHS());
                        float lat = EGTSFactory.fromEGTSLat(pos.LAT.get(), pos.isLAHS());
                        boolean alarm = pos.getSRC() == EGTS_DEVICE_EVENT_SRC_ALERT;
                        log.info("Vehicle {}: [ time {}, lon {}, lat {}, alarm {}, din {}, sensors {}]",
                                idTr,
                                time.getEpochSecond(),
                                lon,
                                lat,
                                alarm,
                                pos.getDIN(),
                                sensors.size());

                        lastPacketBuilder = UnitPacket.newBuilder()
                                .setUnitId(idTr)
                                .setEventTime(time)
                                .setDeviceEventId(pos.getSRC())
                                .setTelematic(TelematicPacket.newBuilder()
                                        .setLocationValid(pos.isVLD())
                                        .setGpsTime(time) //Instant.ofEpochSecond(pos.NTM.get() + EGTSFactory.DATE_01_01_2010))
                                        .setCoordinates(lon, lat)
                                        .setAlt(pos.isALTE() ? ((int) (pos.getALT().getALT() * (pos.getALTS() ? -1 : 1))) : 0)
                                        .setSpeed(pos.getSPD() / 10)
                                        .setHeading(pos.getDIR())
                                        .build());

                        if (config.withComplexSensors) {
                            //log.debug("\t####\t {} BINARY sensor value {}", idTr, pos.getDIN());
                            Map<Integer, ComplexSensorHolder> smap = ComplexSensorProcessor.decode(214, Byte.SIZE,
                                    new BigDecimal(pos.getDIN()));
                            lastPacketBuilder.addComplexSensors(smap);
                        }

                        if (!sensors.isEmpty()) {

                            if (config.withComplexSensors) {
                                Map<Integer, BigDecimal> fuels = new HashMap<>();
                                int index = 800;
                                for (Map.Entry<Integer, BigDecimal> es : sensors.entrySet()) {
                                    if (es.getKey() > 191 && es.getKey() < 225) {
                                        fuels.put(index++, new BigDecimal(0)); //status
                                        fuels.put(index++, new BigDecimal(0)); //level mm
                                        fuels.put(index++, es.getValue()); // level liters
                                        fuels.put(index++, new BigDecimal(0)); //temperature
                                        log.debug("###Fuel cplx sensor {} ({}) occured", es.getKey(), index - 3);
                                    }
                                }
                                Map<Integer, ComplexSensorHolder> fmap = ComplexSensorProcessor.decode(Type.FUEL, 800, fuels);
                                lastPacketBuilder.addComplexSensors(fmap);
                            } else {
                                lastPacketBuilder.addSensors(sensors);
                            }
                            sensors.clear();
                        }

                        if (alarm) {
                            lastPacketBuilder.addFlag(CommonPacketFlag.ALARM);
                        }
                    } else if (p instanceof EGTS_SR_LIQUID_LEVEL_SENSOR) {
                        log.info("EGTS_SR_LIQUID_LEVEL_SENSOR resolved with sensor data.");
                    } else if (p instanceof EGTS_SR_STATE_DATA) {
                        log.info("EGTS_SR_STATE_DATA resolved but only sensor data got.");
                    } else if (p instanceof EGTS_SR_RECORD_RESPONSE) {
                        EGTS_SR_RECORD_RESPONSE pkg = (EGTS_SR_RECORD_RESPONSE) p;
                        log.info("EGTS_SR_RECORD_RESPONSE CRN {} Result {}", pkg.CRN, EraResponseCodes.resolve(pkg.RST.get()));
                    } else {
                        log.info("Packet class: {} ignored.", p.getClass());
                    }
                }
            }

            if (unitPackets.size() == 0 && lastPacketBuilder != null) {
                unitPackets.add(lastPacketBuilder.build());
            }

        } catch (Exception e) { //UnitIdNotFoundException e) {
            log.info("Exception {}", e.getMessage());
            log.debug("", e);
//            throw new ConvertException(in.RN, EraResponseCodes.PC_ID_NFOUND, e.getMessage());
        }

        return unitPackets.toArray(new UnitPacket[unitPackets.size()]);
    }

    public abstract long findId(ChannelHandlerContext ctx, EGTSFactory.ServiceData in);
}
