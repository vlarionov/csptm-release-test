package ru.advantum.csptm.jep.hub.egts.handler;

import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.hub.egts.EGTSHub;
import ru.advantum.csptm.jep.hub.egts.packet.EGTSFactory;
import ru.advantum.csptm.jep.hub.egts.packet.EGTSServiceSubRecord;
import ru.advantum.csptm.jep.hub.egts.packet.EGTS_SERVICE_ST;
import ru.advantum.csptm.jep.hub.egts.packet.Egts2JepConverter;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_DISPATCHER_IDENTITY;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_RECORD_RESPONSE;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_TERM_IDENTITY;
import ru.advantum.csptm.jep.netty.JepNettyConnectionHandler;
import ru.advantum.csptm.jep.netty.UnitIdNotFoundException;
import ru.advantum.csptm.jep.proto.CommandPacket;
import ru.advantum.csptm.jep.proto.UnitPacket;
import ru.advantum.tools.HEX;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LogicClient extends JepNettyConnectionHandler<EGTSHub> {

    public static final long DEFAULT_CHANNEL_OWNER_ID = Long.MIN_VALUE;
    private static final Logger LOGGER = LoggerFactory.getLogger(LogicClient.class);
    private final EGTSFactory factory;

    public LogicClient(EGTSHub hub, EGTSFactory factory) {
        super(hub);
        this.factory = factory;
    }

    private final Egts2JepConverter egts2JepConverter = new Egts2JepConverter() {
        @Override
        public long findId(ChannelHandlerContext ctx, EGTSFactory.ServiceData in) {
            Long id;
            if (in.oid != null && in.oid.value != null) {
                try {
                    initUnitId(String.valueOf(in.oid.value.get()), true);
                    id = getUnitId();
                    LOGGER.info("UNIT: {}, ID_UNIT: {}", in.oid.value.get(), id);
                } catch (UnitIdNotFoundException unf) {
                    LOGGER.info("UnitIdNotFound: {}", unf.getMessage());
                    id = DEFAULT_CHANNEL_OWNER_ID;
                }
            } else {
                id = getUnitId();
            }
            return id;

        }
    };

    @Override
    public void onCommandForSend(CommandPacket commandPacket) {

    }

    @Override
    public void channelOpen(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        super.channelOpen(ctx, e);
    }

    @Override
    public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        super.channelClosed(ctx, e);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) {
        if (e.getCause() instanceof IOException
                && e.getCause() != null
                && e.getCause().getMessage() != null
                && e.getCause().getMessage().contains("Buffer not readable")) {
            // do nothing
        } else {
            //   super.exceptionCaught(ctx, e);
            ctx.getChannel().close();
        }
    }

    private void localRequestHandler(ChannelHandlerContext ctx, EraDecodeMessage.DATA in)
            throws IOException {

        final List<EGTS_SR_RECORD_RESPONSE> resp = new ArrayList<>();
        int rst = 0, sst = 0;
        for (EGTSFactory.ServiceData sd : in.data) {
            try {
                rst = sd.ST.getSST();
                switch (sst = sd.ST.getRST()) {
                    case EGTS_SERVICE_ST.EGTS_AUTH_SERVICE: {
                        if (sd.RD.length > 0) {
                            final EGTSServiceSubRecord sub = sd.RD[0];
                            if (sub instanceof EGTS_SR_TERM_IDENTITY) {
                                // терминал
                                final EGTS_SR_TERM_IDENTITY termIdent = (EGTS_SR_TERM_IDENTITY) sub;
                                try {
                                    final long tid = termIdent.TID.get();
                                    initUnitId(String.valueOf(tid), true);

                                    LOGGER.info("TID: {} bound with ID_TR: {}", tid, getUnitId());
                                    resp.add(new EGTS_SR_RECORD_RESPONSE(sd.RN, EraResponseCodes.PC_OK));

                                } catch (UnitIdNotFoundException noUIDtid) {
                                    LOGGER.warn("EGTS_AUTH_SERVICE no UID by tID {}", noUIDtid.getMessage());
                                    try {
                                        String imei = termIdent.IMEI.asString().replace("\0", "");
                                        initUnitId(imei, true);

                                        LOGGER.info("IMEI: {} bound with ID_TR: {}", imei, getUnitId());
                                        resp.add(new EGTS_SR_RECORD_RESPONSE(sd.RN, EraResponseCodes.PC_OK));

                                    } catch (UnitIdNotFoundException noUIDimei) {
                                        LOGGER.warn("EGTS_AUTH_SERVICE no UID by IMEI {}", noUIDimei.getMessage());
                                        resp.add(new EGTS_SR_RECORD_RESPONSE(sd.RN, EraResponseCodes.PC_AUTH_DENIED));
                                    }
                                }
                            }
                            if (sub instanceof EGTS_SR_DISPATCHER_IDENTITY) {
                                // ТП с авторизацией
                                final EGTS_SR_DISPATCHER_IDENTITY ident = (EGTS_SR_DISPATCHER_IDENTITY) sub;
                                LOGGER.info("EGTS_SR_DISPATCHER_IDENTITY DID: {}, DT: {}, DESCR: {}",
                                        ident.DID.get(),
                                        ident.DT.get(),
                                        ident.DSCR);
                                resp.add(new EGTS_SR_RECORD_RESPONSE(sd.RN, EraResponseCodes.PC_OK));
                            }
                        }
                        break;
                    }
                    case EGTS_SERVICE_ST.EGTS_TELEDATA_SERVICE: {
                        UnitPacket[] unitPackets = egts2JepConverter.convert(ctx, getHub().getConfig(), sd);
                        if (unitPackets.length > 0) {
                            sendUnitPackets(unitPackets);
                        }
                        resp.add(new EGTS_SR_RECORD_RESPONSE(sd.RN, EraResponseCodes.PC_OK));
                        break;
                    }
                    default: {
                        LOGGER.error("Incomplete code in SERVICE section!");
                        resp.add(new EGTS_SR_RECORD_RESPONSE(sd.RN, EraResponseCodes.PC_SRVC_UNKN));
                        break;
                    }
                }
            } catch ( Exception ce) {
                LOGGER.warn("localRequestHandler EGTS2AMTS fail: {}", ce.getMessage());
                resp.add(new EGTS_SR_RECORD_RESPONSE(sd.RN, EraResponseCodes.PC_OK));
            }
        }
        byte[] response = factory.response(rst,
                sst,
                in.getPID(),
                EraResponseCodes.PC_OK,
                resp.toArray(new EGTS_SR_RECORD_RESPONSE[resp.size()]));
        LOGGER.debug("<<< SEND RESP: size = {}, bytes: {}", resp.size(), HEX.byteArrayToString(response));
        sendToUnit(response);
    }

    private void localExceptionHandler(EraDecodeMessage.EXCEPTION ex) throws IOException, EgtsException {
        LOGGER.debug(">>> EXCEPTION");
        sendToUnit(factory.response(0, 0, ex.getPID(), ex.getRespCode()));
        throw new EgtsException(String.format("EXCEPTION - RN: %d, TYPE: %s", ex.getPID(), ex.getRespCode()));
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
        if (e.getMessage() instanceof EraDecodeMessage.DATA) {
            localRequestHandler(ctx, (EraDecodeMessage.DATA) e.getMessage());
        }

        // что-то не так... будем слать RESPONSE, что они знали что случилось
        if (e.getMessage() instanceof EraDecodeMessage.EXCEPTION) {
            localExceptionHandler((EraDecodeMessage.EXCEPTION) e.getMessage());
        }
    }
}
