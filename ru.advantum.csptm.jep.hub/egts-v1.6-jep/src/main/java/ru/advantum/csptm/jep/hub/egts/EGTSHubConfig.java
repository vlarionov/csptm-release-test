package ru.advantum.csptm.jep.hub.egts;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.SensorNumerator;
import ru.advantum.csptm.jep.netty.NettyHubConfig;

import java.util.List;

@Root(name = "egts-hub")
public class EGTSHubConfig {
    @Attribute(name="with-complex-sensors", required = false)
    public boolean withComplexSensors = true;

    @Element(name = "sensor-numerator-type", required = false)
    public SensorNumerator.NumeratorType sensorNumeratorType = SensorNumerator.NumeratorType.DEFAULT;

    @ElementList(name = "sensor-numerator-custom", required = false)
    public List<CustomNumeratorEntry> snCustom;


    @Element(name = "sensor-define-entry", required = false)
    public static class CustomNumeratorEntry {
        @Attribute(name = "sensor-type")
        public String name;

        @Attribute(name = "offset")
        public int offset;

        @Attribute(name = "max-number")
        public int maxNumber;
    }

    @Element(name = "hub")
    public final NettyHubConfig hubConfig = new NettyHubConfig();
}
