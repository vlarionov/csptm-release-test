import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.jep.hub.egts.EGTSHub;
import ru.advantum.csptm.jep.hub.egts.EGTSHubConfig;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensorType;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.SensorNumerator;

import java.util.HashMap;
import java.util.Map;

import static org.apache.log4j.Logger.getRootLogger;

public class CommonTests {
    private static final Logger log = LoggerFactory.getLogger(CommonTests.class);

    static {
        /*
         * This configuring console logger without log4j.properties file
         */
        if (System.getProperty("log4j.configuration") == null) { // not .isEmpty() - will NPE
            final String PATTERN = "%d{dd-MM-yyyy HH:mm:ss.SSS}: %-5p %c{1}.%M(%L): %m%n";
            ConsoleAppender console = new ConsoleAppender(); //createParentDirs appender
            console.setLayout(new PatternLayout(PATTERN));
            console.setThreshold(Level.DEBUG);
            console.activateOptions();
            getRootLogger().addAppender(console);
        }
    }

    @Test
    public void configTest() throws Exception {
        System.setProperty("advantum.config.dir", "src/main/resources/extras/examples");
        EGTSHubConfig egtsHubConfig = Configuration.unpackConfig(EGTSHubConfig.class, EGTSHub.CONFIG_FILE_NAME);

        if (egtsHubConfig != null) {
            if (egtsHubConfig.snCustom != null) {
                Map<EGTSSensorType, SensorNumerator.Settings> settingsMap = new HashMap<>();
                for (EGTSHubConfig.CustomNumeratorEntry e : egtsHubConfig.snCustom) {
                    EGTSSensorType t = EGTSSensorType.lookupByName(e.name);
                    if (t != null) {
                        settingsMap.put(t, new SensorNumerator.Settings((short) e.offset, (short) e.maxNumber));
                    }
                }
                if (settingsMap.size() > 0) {
                    SensorNumerator.setNumeratorType(SensorNumerator.NumeratorType.CUSTOM, settingsMap);
                } else {
                    log.warn("SensorNumerator configured as CUSTOM but numbers doesn't defined. Moved to DEFAULT.");
                    SensorNumerator.setNumeratorType(SensorNumerator.NumeratorType.DEFAULT);
                }
                System.out.println(egtsHubConfig.snCustom.size());
            }
        }
    }

}
