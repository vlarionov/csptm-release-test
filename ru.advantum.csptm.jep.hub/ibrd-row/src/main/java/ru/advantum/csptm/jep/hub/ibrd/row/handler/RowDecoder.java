package ru.advantum.csptm.jep.hub.ibrd.row.handler;

import org.jboss.netty.buffer.ChannelBuffer;
import ru.advantum.csptm.jep.hub.ibrd.ResultCodeStruct;
import ru.advantum.csptm.jep.hub.ibrd.handler.IbrdDecoder;
import ru.advantum.csptm.jep.hub.ibrd.row.RowRegistrationStruct;
import ru.advantum.csptm.jep.hub.ibrd.row.TelematicsStruct;
import ru.advantum.csptm.jep.hub.ibrd.row.command.RowCommandEnum;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdErrorType;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdResponse;
import ru.advantum.csptm.jep.service.ibrd.model.row.TelematicsResponse;

public class RowDecoder extends IbrdDecoder {

    @Override
    protected Object decodeBody(int commandNumber, ChannelBuffer buffer) throws Exception {
        if (commandNumber == RowCommandEnum.REGISTRATION.getNumber()) {
            return readData(buffer, RowRegistrationStruct.class);
        } else {
            ResultCodeStruct resultCodeStruct = readData(buffer, ResultCodeStruct.class);
            IbrdErrorType errorType = getErrorType(resultCodeStruct.resultCode.get());

            Integer errorByteNumber = null;
            if (errorType == IbrdErrorType.VALUE_OUT_OF_RANGE) {
                errorByteNumber = (int) readTrailer(buffer, 1)[0];
            }

            if (commandNumber == RowCommandEnum.SET_BLOCKS.getNumber()) {
                //skip command number
                readTrailer(buffer, 1);

                resultCodeStruct = readData(buffer, ResultCodeStruct.class);
                errorType = getSetBlocksErrorType(resultCodeStruct.resultCode.get());

                if (errorType == IbrdErrorType.NULL) {
                    int blocksNumber = readTrailer(buffer, 1)[0];
                    //skip block info bytes
                    readTrailer(buffer, blocksNumber);
                }
            }

            if (commandNumber == RowCommandEnum.TELEMATICS_REQUEST.getNumber() && errorType == IbrdErrorType.NULL) {
                TelematicsStruct telematicsStruct = readData(buffer, TelematicsStruct.class);
                return new TelematicsResponse(
                        commandNumber,
                        telematicsStruct.flags.isCoordsValid(),
                        telematicsStruct.parseLon(),
                        telematicsStruct.parseLat()
                );
            }

            return new IbrdResponse(commandNumber, errorType, errorByteNumber);
        }
    }

    private IbrdErrorType getSetBlocksErrorType(int errorCode) {
        switch (errorCode) {
            case 0:
                return IbrdErrorType.NULL;
            case 1:
                return IbrdErrorType.ROW_NOT_REPLY;
            case 2:
                return IbrdErrorType.UNKNOWN_COMMAND;
            case 3:
                return IbrdErrorType.CHECK_SUM_NOT_MUCH;
            case 4:
                return IbrdErrorType.INVALID_PACKAGE;
            default:
                throw new IllegalArgumentException("Unknown error code");
        }
    }


}
