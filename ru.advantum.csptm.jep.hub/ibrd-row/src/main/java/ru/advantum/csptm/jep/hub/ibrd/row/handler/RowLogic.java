package ru.advantum.csptm.jep.hub.ibrd.row.handler;

import lombok.val;
import ru.advantum.csptm.jep.hub.ibrd.DatetimeStruct;
import ru.advantum.csptm.jep.hub.ibrd.handler.IbrdLogic;
import ru.advantum.csptm.jep.hub.ibrd.row.*;
import ru.advantum.csptm.jep.hub.ibrd.row.command.SetRowBlocksCommandStruct;
import ru.advantum.csptm.jep.hub.ibrd.row.command.SetRowDatetimeCommandStruct;
import ru.advantum.csptm.jep.hub.ibrd.row.command.SetRowParametersCommandStruct;
import ru.advantum.csptm.jep.hub.ibrd.row.command.TelematicsRequestCommandStruct;
import ru.advantum.csptm.jep.service.ibrd.model.*;
import ru.advantum.csptm.jep.service.ibrd.model.row.RowConfig;
import ru.advantum.csptm.jep.service.ibrd.model.row.RowInfo;
import ru.advantum.csptm.jep.service.ibrd.model.row.TelematicsResponse;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RowLogic extends IbrdLogic<RowHub> {

    public RowLogic(RowHub hub) {
        super(hub);
    }

    @Override
    public void onCommand(IbrdEvent commandIbrdEvent) {
        IbrdEventType eventType = commandIbrdEvent.getEventType();
        switch (eventType) {
            case SET_DATETIME:
                LocalDateTime dateTime = commandIbrdEvent.getBoardTime();
                commander.sendCommand(new SetRowDatetimeCommandStruct(new DatetimeStruct(dateTime)), commandIbrdEvent);
                break;
            case SET_CONFIG:
                RowConfig config = commandIbrdEvent.getRowConfig();
                commander.sendCommand(new SetRowParametersCommandStruct(buildParameters(config)), commandIbrdEvent);
                break;
            case SET_INFO:
                RowInfo info = commandIbrdEvent.getRowInfo();
                commander.sendCommand(
                        new SetRowBlocksCommandStruct(buildBlockList(
                                info.getMessageList(),
                                info.getForecastList(),
                                info.getRunningRowSpeed(),
                                info.getBlockRate()
                        )),
                        commandIbrdEvent
                );
                break;
            case TELEMATICS_REQUEST:
                commander.sendCommand(new TelematicsRequestCommandStruct(), commandIbrdEvent);
                break;
        }
    }

    @Override
    protected IbrdEvent buildResponseIbrdEvent(IbrdResponse response, IbrdEvent requestIbrdEvent) {
        if (response instanceof TelematicsResponse) {
            val ibrdResponseEvent = new IbrdEvent(
                    LocalDateTime.now(),
                    IbrdEventType.TELEMATICS_RESPONSE,
                    requestIbrdEvent.getIbrdId(),
                    requestIbrdEvent.getChannelId()
            );
            ibrdResponseEvent.setCommandEventId(requestIbrdEvent.getEventId());
            ibrdResponseEvent.setErrorType(response.getErrorType());
            ibrdResponseEvent.setCoordsValid(((TelematicsResponse) response).isCoordsValid());
            ibrdResponseEvent.setLon(((TelematicsResponse) response).getLon());
            ibrdResponseEvent.setLat(((TelematicsResponse) response).getLat());

            return ibrdResponseEvent;
        }

        return super.buildResponseIbrdEvent(response, requestIbrdEvent);
    }

    private RowParametersStruct buildParameters(RowConfig config) {
        return new RowParametersStruct(
                new RowFlagsStruct(
                        config.isDisplayDate(),
                        config.isDisplayTime(),
                        config.isDisplayTemperature(),
                        false,
                        RowFlagsStruct.TimeProvider.GSM,
                        RowFlagsStruct.TemperatureProvider.SENSOR
                ),
                0,
                Duration.ofSeconds(3),
                config.getServerWaitingTime(),
                config.getDataWaitingTime(),
                config.getRebootTime()
        );
    }

    private RowBlockListStruct buildBlockList(List<IbrdMessage> messageList, List<IbrdForecast> forecastList, int speed, Duration blockRate) {
        if (messageList == null) {
            messageList = Collections.singletonList(
                    new IbrdMessage("", new IbrdColor(0,0,0))
            );
        }
        if (forecastList.isEmpty()) {
            forecastList = Arrays.asList(
                    new IbrdForecast(null, "", null, ""),
                    new IbrdForecast(null, "", null, "")
            );
        }

        List<RowBlockItemStruct> blockItems = new ArrayList<>();

        if (forecastList.size() % 2 == 1) {
            forecastList.add(new IbrdForecast(null, "", null, ""));
        }
        for (int i = 0; i < forecastList.size(); i++) {
            IbrdForecast forecast = forecastList.get(i);
            RowForecastStruct ibrdRowForecast = new RowForecastStruct(
                    speed,
                    blockRate,
                    new RowTextStruct(forecast.getRouteNumber()),
                    new RowTextStruct(forecast.getFormattedArrivalTime()),
                    new RowTextStruct(forecast.getTerminal())
            );
            blockItems.add(new RowBlockItemStruct(1 + i % 2, 1 + i / 2, ibrdRowForecast));
        }
        for (int i = 0 ; i < messageList.size() ; i++) {
            IbrdMessage message = messageList.get(i);
            RowRunningTextStruct rowRunningTextStruct = new RowRunningTextStruct(speed , new RowTextStruct(message.getText()) );
            blockItems.add(new RowBlockItemStruct(3, i + 1,  rowRunningTextStruct));
        }
        return new RowBlockListStruct(blockItems);
    }


}
