package ru.advantum.csptm.jep.hub.ibrd.row;

import org.jboss.netty.channel.ChannelHandler;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.jep.core.JepHubController;
import ru.advantum.csptm.jep.core.rmq.JepRmqHubController;
import ru.advantum.csptm.jep.hub.ibrd.IbrdHub;
import ru.advantum.csptm.jep.hub.ibrd.IbrdHubConfig;
import ru.advantum.csptm.jep.hub.ibrd.row.handler.RowDecoder;
import ru.advantum.csptm.jep.hub.ibrd.row.handler.RowLogic;
import ru.advantum.csptm.jep.netty.BootstrapConfig;

public class RowHub extends IbrdHub {

    public static void main(String[] args) throws Exception {
        IbrdHubConfig hubConfig = Configuration.unpackConfig(IbrdHubConfig.class, CONFIG_FILE_NAME);
        JepRmqHubController controller = new JepRmqHubController(hubConfig.hub.controllerConfig);

        RowHub hub = new RowHub(hubConfig.hub.tcpConfig, controller);
        Runtime.getRuntime().addShutdownHook(new Thread(hub::stop));
        controller.start(hub);
    }

    public RowHub(BootstrapConfig config, JepHubController controller) {
        super(config, controller);
    }

    @Override
    protected ChannelHandler getDecoder() {
        return new RowDecoder();
    }

    @Override
    protected ChannelHandler getLogic() {
        return new RowLogic(this);
    }

}
