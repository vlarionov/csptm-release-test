package ru.advantum.csptm.jep.hub.granit.handler;

import com.google.gson.Gson;
import javolution.io.Struct;
import org.apache.commons.lang.RandomStringUtils;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.MessageEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.core.PacketTransferException;
import ru.advantum.csptm.jep.hub.granit.AbstractLogic;
import ru.advantum.csptm.jep.hub.granit.GranitHub;
import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.connection.ConnectionStorage;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;
import ru.advantum.csptm.jep.hub.granit.protocol.G6BaseCommand;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Npl;
import ru.advantum.csptm.jep.hub.granit.protocol.enums.ServerCommand;
import ru.advantum.csptm.jep.hub.granit.protocol.enums.UnitMessage;
import ru.advantum.csptm.jep.hub.granit.protocol.internal.MessageWrapper;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.G6NphHeader;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.G6NphResult;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.commands.*;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.external.device.G6NphNPHSedDeviceResult;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.external.device.G6NphSedDeviceData;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.external.device.G6NphSedDeviceTitleData;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.external.device.G6NphSrvExternalDeviceConstant;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.generic.controls.G6NhpSrvGenericControlsConstant;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.generic.controls.G6NphConnAuthString;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.generic.controls.G6NphConnRequest;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.generic.controls.G6NphPria;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.G6NphSrvNavdata;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.G6NphSrvNavdataConstant;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells.*;
import ru.advantum.csptm.jep.netty.UnitIdNotFoundException;
import ru.advantum.csptm.jep.proto.*;
import ru.advantum.csptm.jep.proto.sensors.ComplexSensorHolder;
import ru.advantum.csptm.jep.proto.sensors.Type;
import ru.advantum.csptm.service.csd.ComplexSensorProcessor;
import ru.advantum.csptm.service.csd.ExtendedData;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.stream.Collectors;

public class GranitLogic extends AbstractLogic<GranitHub> {

    private static final Logger log = LoggerFactory.getLogger(GranitLogic.class);
    private static final Gson gson = new Gson();

    private final GranitHubConfig config;

    public GranitLogic(GranitHub hub, GranitHubConfig config) {
        super(hub);
        this.config = config;
    }

    public GranitHubConfig getConfig() {
        return config;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    /**
     * Sending command to unit
     *
     * @param commandPacket - command at internal format
     */
    @Override
    public void onCommandForSend(CommandPacket commandPacket) {
        try {
            if (!this.config.ndtp.silentModeOn) {
                G6BaseCommand executor;
                ServerCommand cmd = ServerCommand.lookup(commandPacket.getCmdName());
                if (cmd != ServerCommand.UNDEFINED) {
                    try {
                        executor = cmd.getClazz().getConstructor().newInstance();
                        switch (cmd) {
                            case NPH_GET_BALANCE: {
                                ((G6NphGetBalance) executor).cmd_req.set(commandPacket.getParameter("cmd_req".toUpperCase()).getBytes());
                                break;
                            }
                            case NPH_GET_PRDO: {
                                ((G6NphGetPrdo) executor).output_id.set(Short.valueOf(commandPacket.getParameter("OUTPUT_ID".toUpperCase())));
                                break;
                            }
                            case NPH_RESET_INT_STATE: {
                                ((G6NphResetIntState) executor).state.set(Short.valueOf(commandPacket.getParameter("state".toUpperCase())));
                                break;
                            }
                            case NPH_SET_CURTIME: {
                                ((G6NphSetCurtime) executor).cur_time.set(Long.valueOf(commandPacket.getParameter("cur_time".toUpperCase())));
                                break;
                            }
                            case NPH_SET_LOADFIRM: {
                                G6NphSetLoadfirm ex = (G6NphSetLoadfirm) executor;
                                int ipaddress = Integer.reverseBytes((int) G6NphPria.ip2int(commandPacket.getParameter("ip_address".toUpperCase())));
                                ex.ip_address.set(ipaddress);//2603172874L);

                                String strPort = commandPacket.getParameter("port".toUpperCase());
                                ex.port.set(strPort == null ? 21 : Short.valueOf(strPort));
                                String strUrl = commandPacket.getParameter("URL".toUpperCase());
                                ex.URL.set((strUrl == null ? "\0" : (strUrl + "\0")).getBytes());
                                ex.dir_file.set((commandPacket.getParameter("dir_file".toUpperCase()) + "\0").getBytes());
                                ex.file_name.set((commandPacket.getParameter("file_name".toUpperCase()) + "\0").getBytes());
                                ex.User.set((commandPacket.getParameter("User".toUpperCase()) + "\0").getBytes());
                                ex.Password.set((commandPacket.getParameter("Password".toUpperCase()) + "\0").getBytes());

                                break;
                            }
                            case NPH_SET_PRDO: {
                                G6NphSetPrdo ex = (G6NphSetPrdo) executor;

                                ex.output_id.set(Short.valueOf(commandPacket.getParameter("output_id".toUpperCase())));
                                ex.mode.set(Short.valueOf(commandPacket.getParameter("mode".toUpperCase())));
                                break;
                            }
                            case NPH_SET_PRIA:
                            case NPH_SET_PRIA_EXT: {
                                G6NphSetPria ex = (G6NphSetPria) executor;

                                int ipaddress = Integer.reverseBytes((int) G6NphPria.ip2int(commandPacket.getParameter("ip_address".toUpperCase())));
                                ex.ip_address.set(ipaddress);

                                String strPort = commandPacket.getParameter("port".toUpperCase());
                                ex.port.set(strPort == null ? 9201 : Short.valueOf(strPort));
                                String strApn = commandPacket.getParameter("APN");
                                ex.APN.set((strApn == null ? "mosgortrans.msk\0" : (strApn + "\0")).getBytes());

                                String strUrl = commandPacket.getParameter("URL".toUpperCase());

                                ex.URL.set((strUrl == null ? "\0" : (strUrl + "\0")).getBytes());
                                String strUser = commandPacket.getParameter("User".toUpperCase());
                                ex.User.set((strUser == null ? "mts\0" : (strUser + "\0")).getBytes());
                                String strPassword = commandPacket.getParameter("Password".toUpperCase());
                                ex.Password.set((strPassword == null ? "mts\0" : (strPassword + "\0")).getBytes());

                                break;
                            }
                            case NPH_SET_PRNAV: {
                                G6NphSetPrnav ex = (G6NphSetPrnav) executor;

                                ex.sendDataInterval.set(Long.valueOf(commandPacket.getParameter("sendDataInterval".toUpperCase())));
                                ex.sendDataInterval_stop.set(Long.valueOf(commandPacket.getParameter("sendDataInterval_stop".toUpperCase())));
                                ex.dist.set(Long.valueOf(commandPacket.getParameter("dist".toUpperCase())));
                                ex.angle.set(Long.valueOf(commandPacket.getParameter("angle".toUpperCase())));
                                break;
                            }
                            case NPH_SET_ROUTE_AUTOINFORMER: {
                                G6NphSetRouteAutoinformer ex = (G6NphSetRouteAutoinformer) executor;

                                ex.route.set(commandPacket.getParameter("route".toUpperCase()).getBytes());
                                ex.run.set(commandPacket.getParameter("run".toUpperCase()).getBytes());
                                break;
                            }
                            case NPH_SED_DEVICE_TITLE_DATA: {

                                G6NphSedDeviceTitleData ex = new G6NphSedDeviceTitleData(commandPacket.getParameter("message".toUpperCase()),
                                                                                         "UTF8", "Cp1251");

                                ex.message_id.set(Integer.valueOf(commandPacket.getParameter("message_id".toUpperCase())));
                                ex.num_packet.set(0x8000);//Short.valueOf(commandPacket.getParameter("num_packet".toUpperCase())));
/*
                                ex.message_id.set(1498139450);//Short.valueOf(commandPacket.getParameter("message_id".toUpperCase())));
                                ex.address_from.set(G6NphSrvExternalDeviceConstant.DeviceAddress.lookup(commandPacket.getParameter("address_from_type".toUpperCase())) << 4
                                        | Short.valueOf(commandPacket.getParameter("address_from_num".toUpperCase())));
                                ex.address_to.set(G6NphSrvExternalDeviceConstant.DeviceAddress.lookup(commandPacket.getParameter("address_to_type".toUpperCase())) << 4
                                        | Short.valueOf(commandPacket.getParameter("address_to_num".toUpperCase())));
                                ex.type_data.set(
                                        G6NphSrvExternalDeviceConstant
                                                .TypeDataFieldValues
                                                .lookup(
                                                        commandPacket.getParameter("type_data".toUpperCase())
                                                )
                                );
*/

                                //DEBUG
                                ex.type_data.set(1);
                                ex.address_from.set(0x30);
                                ex.address_to.set(0x30);

                                executor = ex;
                            }
                            case NPH_SGC_SERVICE_REQUEST: {
                                String service2request = commandPacket.getParameter("data".toUpperCase());
                                if (service2request != null) {
                                    executor = new G6NphSgcServiceRequest(G6NphHeader.G6NphServiceType.lookup(service2request));
                                }
                            }
                            default:
                                break;
                        }
                    } catch (Exception e) {
                        log.error("", e);
                        executor = null;
                    }
                /*
                Sender
                 */
                    if (executor != null) {
                        sendResponse(
                                executor
                                , cmd.getServiceId()
                                , cmd.getCode()
                                , commandPacket.getCmdId()
                                , Boolean.TRUE
                                , G6Npl.NplPeerAddress.NPL_ADDRESS_BROAD_DEVICES.getCode()   // NPL_ADDRESS_BROAD_DEVICES as least one
                        );
                        log.debug("Command {} [requestId {}, service {}, type {}, body |{}|] sent to {}", cmd, commandPacket.getCmdId(), cmd.getServiceId()
                                , cmd.getCode(), executor.extractString(), commandPacket.getUnitId());
                    }
                } else {
                    log.error("Unrecognized command {}", commandPacket.getCmdName());
                }
            }
        } catch (Throwable t) {
            log.debug("", t);
        }
    }

    private void onCommandRepliesSend(List<UnitPacket> packets) {
        try {
            sendUnitPackets(packets.toArray(new UnitPacket[packets.size()]));
        } catch (PacketTransferException e) {
            log.error("", e);
        }
    }

    @Override
    public void channelOpen(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        reset();
        ConnectionStorage.addLock(ctx);
        super.channelOpen(ctx, e);
    }

    @Override
    public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        try {
            makeByebye();
            ConnectionStorage.closeConnection(ctx);
            ConnectionStorage.removeLock(ctx);
        } catch (Exception e1) {
            log.debug("", e1);
        }
        super.channelClosed(ctx, e);
    }

    private List<UnitPacket> convertData(GranitMessage.DATA data, boolean historical) {
        List<UnitPacket> unitPackets = new ArrayList<>();

        UnitPacketBuilder unitPacketBuilder = UnitPacket.newBuilder()
                .setUnitId(getUnitId())
                .setEventTime(Instant.now());
        if (historical) {
            unitPacketBuilder.addFlag(CommonPacketFlag.HIST_DATA);
        }

        int sensorCount = 0; // log means only
        int navNum = 0; // кол-во блоков с навигацией

        boolean onBoardAlarm = Boolean.FALSE;
        boolean crutchAlarm = Boolean.FALSE;
        boolean callRequest = Boolean.FALSE;
        long timeStamp = 0;
        int fuelStartNum = G6NphSrvNavdataConstant.G6Cell08Numbers.BASE_OFF;
        int temperatureStartNum = G6NphSrvNavdataConstant.G6Cell16Numbers.BASE_OFF;

        Map<Integer, BigDecimal> extendedSensor = ExtendedData.getCleanExtendedArray(); //ksynina crutch #20369

        for (G6Base g6Base : data.getData()) {
            G6CellNav00 cellNav00;

            if (g6Base instanceof G6CellNav00) {
                navNum++;
                if (navNum > 1) {
                    unitPackets.add(unitPacketBuilder.build());
                    unitPacketBuilder = UnitPacket.newBuilder()
                            .setUnitId(getUnitId())
                            .setEventTime(Instant.now());
                    log.warn("Additional TrInfo!");
                }

                cellNav00 = (G6CellNav00) g6Base;

                if (cellNav00.isAlarm()) {
                    unitPacketBuilder.addFlag(CommonPacketFlag.ALARM); // onboard button
                    onBoardAlarm = Boolean.TRUE;
                }
                if (cellNav00.isCallRequest()) {
                    unitPacketBuilder.addFlag(CommonPacketFlag.CALL_REQUEST);
                    callRequest = Boolean.TRUE;
                }
                timeStamp = cellNav00.getTimestamp();
                unitPacketBuilder
                        .setEventTime(Instant.ofEpochSecond(cellNav00.getTimestamp()))
                        .setTelematic(TelematicPacket
                                              .newBuilder()
                                              .setCoordinates(cellNav00.getProcessedLon(), cellNav00.getProcessedLat())
                                              .setSpeed(cellNav00.getSpeedAvg())
                                              .setAlt(cellNav00.getAltitude())
                                              .setGpsTime(Instant.ofEpochSecond(timeStamp))
                                              .setHeading(cellNav00.getCourse())
                                              .setLocationValid(cellNav00.getLocationValid())
                                              .build());

                extendedSensor.putAll(cellNav00.getSensorData());
            }

            /*
                    ***  Crutches  ***
                    ==================
                    There're some crutches below. Each //*** starts comment of crutch, //*EoC - end of one
            */

            //***  Original MGT alarm crutch: "collective farm" alarm button
/*
http://redmine.advantum.ru/issues/20673#change-105536
            if (config.ndtp.customAlarmDinNum > 0 && g6Base instanceof G6CellIntSensor02) {

                G6CellIntSensor02 g6CellIntSensor02 = (G6CellIntSensor02) g6Base;

                if (
                        (
                                (g6CellIntSensor02.getDi_in().get() & (int) Math.pow(2, config.ndtp.customAlarmDinNum - 1))
                                        >> (config.ndtp.customAlarmDinNum - 1)
                        ) == config.ndtp.customAlarmDinOn
                        ) {

                    unitPacketBuilder.addFlag(CommonPacketFlag.ALARM);
                    crutchAlarm = Boolean.TRUE;
                }

            }
*/
            //*EoC

            if (config.ndtp.sensors) {
                if (config.ndtp.sensorsInClassicalModeOn) { // default false
                    boolean passby = Boolean.FALSE; // crutch's flag

                    if (config.ndtp.crutchUsi08Status2On && g6Base instanceof G6CellUsi08) {

                        G6CellUsi08 g6CellUsi08 = (G6CellUsi08) g6Base;
                        BigDecimal instance = g6CellUsi08.getSensorData()
                                .get(this.config.ndtp.cell08Numbers.det_status + g6CellUsi08.getCellNumber());
                        if (instance != null) {
                            passby = instance.intValue() == 2;
                        }
                    }
                    if (!passby && g6Base instanceof IG6BaseCell) {
                        Map<Integer, BigDecimal> sensorData = ((IG6BaseCell) g6Base).getSensorData();

                        if (config.ndtp.cells != null) {
                            for (Map.Entry<Integer, BigDecimal> entry : sensorData.entrySet()) {

                                GranitHubConfig.Ndtp.Cell foundCell;
                                if ((foundCell = config.ndtp.lookup(((IG6BaseCell) g6Base).getCell(), entry.getKey())) != null) {

                                    BigDecimal entryValue = entry.getValue();
                                    if (foundCell.cast) {
                                        if (entryValue.intValue() != 0) {
                                            entryValue = new BigDecimal(1);
                                        }
                                    }
                                    sensorData.remove(entry.getKey());
                                    sensorData.put(foundCell.jepNumber, entryValue);
                                    sensorCount++;
                                }
                            }
                        }
                        if (sensorData.get(-1) != null) {
                            sensorData.remove(-1);
                        }

                        sensorCount += sensorData.size();
                        unitPacketBuilder.addSensors(sensorData);
                    }
                } else {
                    //
                    // CSPTM-specific mode
                    //
                    if (g6Base instanceof IG6BaseCell) {
                        if (g6Base instanceof G6CellUsi08) {

                            Map<Integer, ComplexSensorHolder> cp = ComplexSensorProcessor.decode(
                                    Type.FUEL,
                                    fuelStartNum++,
                                    ((IG6BaseCell) g6Base).getSensorData()
                            );
                            unitPacketBuilder.addComplexSensors(cp);
                        } else if (g6Base instanceof G6CellIntSensor02) {
                            G6CellIntSensor02 g6CellIntSensor02 = (G6CellIntSensor02) g6Base;

                            //Binary both
                            Map<Integer, ComplexSensorHolder> dinbin = ComplexSensorProcessor.decode(
                                    config.ndtp.cell02Numbers.di_in,
                                    Byte.SIZE,
                                    g6CellIntSensor02.getSensorData().get(config.ndtp.cell02Numbers.di_in)
                            );
                            unitPacketBuilder.addComplexSensors(dinbin);

                            Map<Integer, ComplexSensorHolder> doutbin = ComplexSensorProcessor.decode(
                                    config.ndtp.cell02Numbers.di_out,
                                    Byte.SIZE,
                                    g6CellIntSensor02.getSensorData().get(config.ndtp.cell02Numbers.di_out)
                            );
                            unitPacketBuilder.addComplexSensors(doutbin);

                            //Analog both
                            unitPacketBuilder.addComplexSensors(
                                    ComplexSensorProcessor.decode(
                                            config.ndtp.cell02Numbers.an_in0,
                                            new BigDecimal(g6CellIntSensor02.getAn_in0()),
                                            new BigDecimal(g6CellIntSensor02.getAn_in1()),
                                            new BigDecimal(g6CellIntSensor02.getAn_in2()),
                                            new BigDecimal(g6CellIntSensor02.getAn_in3())
                                    )
                            );
                            unitPacketBuilder.addComplexSensors(
                                    ComplexSensorProcessor.decode(
                                            config.ndtp.cell02Numbers.di0_counter,
                                            new BigDecimal(g6CellIntSensor02.getDi0_counter()),
                                            new BigDecimal(g6CellIntSensor02.getDi1_counter()),
                                            new BigDecimal(g6CellIntSensor02.getDi2_counter()),
                                            new BigDecimal(g6CellIntSensor02.getDi3_counter())
                                    )
                            );

                            // Extended
                            extendedSensor.put(ExtendedData.TRACK, new BigDecimal(g6CellIntSensor02.getOdometer()));
                            extendedSensor.put(ExtendedData.GSM_LVL, new BigDecimal(g6CellIntSensor02.getCsq()));
                            extendedSensor.put(ExtendedData.GPRS_STATE, new BigDecimal(g6CellIntSensor02.getGprs_state()));
                            extendedSensor.put(ExtendedData.ACC_ENGY, new BigDecimal(g6CellIntSensor02.getAccel_energy()));

                        } else if (g6Base instanceof G6CellIrma04) {
                            G6CellIrma04 g6CellIrma04 = (G6CellIrma04) g6Base;

                            Map<Integer, ComplexSensorHolder> im = ComplexSensorProcessor.decode(
                                    G6NphSrvNavdataConstant.G6Cell04Numbers.BASE_OFF,
                                    Type.IRMA,
                                    g6CellIrma04.getSensorData());

                            if (log.isDebugEnabled()) {
                                log.debug("IRMA ComplexSensorProcessor {}; {}", im.size(),
                                          im.entrySet().stream()
                                                  .map(e -> String.format("%d, %s", e.getKey(), Arrays.toString(e.getValue().getValues())))
                                                  .collect(Collectors.joining("; ")));
                            }
                            unitPacketBuilder.addComplexSensors(im);

                        } else if (g6Base instanceof G6CellTermo16) {
                            G6CellTermo16 g6CellTermo16 = (G6CellTermo16) g6Base;

                            Map<Integer, ComplexSensorHolder> ts = ComplexSensorProcessor.decode(
                                    Type.TEMPERATURE,
                                    temperatureStartNum++,
                                    g6CellTermo16.getSensorData()
                            );
                            if (log.isDebugEnabled()) {
                                log.debug("TEMPERATURE ComplexSensorProcessor {}; {}", ts.size(),
                                        ts.entrySet().stream()
                                                .map(e -> String.format("%d, %s", e.getKey(), Arrays.toString(e.getValue().getValues())))
                                                .collect(Collectors.joining("; ")));
                            }

                            unitPacketBuilder.addComplexSensors(ts);
                        }
                    }
                }
            }
        }

        unitPacketBuilder.addComplexSensors(
                ComplexSensorProcessor.decode(1, extendedSensor));

        UnitPacket ub = unitPacketBuilder.build();
        unitPackets.add(ub);

        if (config.ndtp.detailInfoLog) {
            log.info("Vehicle {}: eventtime {} uploads {} records, callRequest {}, crutchAlarm {}, onBoardAlarm {}, with {} sensor records",
                     getUnitId(),
                     timeStamp,
                     unitPackets.size(),
                     callRequest,
                     crutchAlarm,
                     onBoardAlarm,
                     sensorCount);
        }
        return unitPackets;
    }

    private void convertAndLoad(GranitMessage msg, boolean historical) {
        try {
            String channelStringId = String.format("0x%08x", getChannel().getId());
            if (msg instanceof GranitMessage.HANDSHAKE) {

                log.debug("Channel {}: handshake of UnitId: {}", channelStringId, getUnitId());
                sendUnitPackets(makeHandshake());
                incTotalPacketCnt();
            } else if (msg instanceof GranitMessage.DATA) {

                if (getUnitId() == null) {
                    log.debug("No unit id data; channel {}", channelStringId);
                } else {
                    if (config.ndtp.detailInfoLog) {
                        log.debug("Channel {}: data of UnitId: {}", channelStringId, getUnitId());
                    }
                    List<UnitPacket> unitPackets = convertData((GranitMessage.DATA) msg, historical);
                    incTotalPacketCnt();
                    if (historical) {
                        incHistPacketCnt();
                    }

                    sendUnitPackets(unitPackets.toArray(new UnitPacket[unitPackets.size()]));
                }
            }
        } catch (PacketTransferException e) {
            log.debug("ERROR: ", e);
        }
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws GranitException, IOException {
        if (e.getMessage() instanceof MessageWrapper) {
            MessageWrapper nplWrp = (MessageWrapper) e.getMessage();
            if (nplWrp.getNpl() != null) {
                if (!nplWrp.getNpl().validateSignature()) {
                    sendCrcError();
                    incInvalidpacketCnt();
                    log.warn("Invalid signature [{}] {}", getUnitId(), ctx.getChannel());
                } else {
                    ndtp6resolver(ctx, nplWrp);
                    if (getUnitId() != null && ConnectionStorage.findConnection(getUnitId()).replies() > 0) {
                        onCommandRepliesSend(repliesProcessor(ConnectionStorage.findConnection(getUnitId()).getCommandReplies()));
                    }
                }
            }
        }
    }

    /**
     * Unit replies convertor 2 internal JSON
     *
     * @param commandReplies - G6-formatted replies
     * @return - JEF formatted replies
     */
    private List<UnitPacket> repliesProcessor(ConcurrentSkipListMap<Long, TreeMap<Long, G6Base>> commandReplies) {
        List<UnitPacket> result = new ArrayList<>();
        try {
            for (Map.Entry<Long, TreeMap<Long, G6Base>> reply : commandReplies.entrySet()) {
                final long cmdId = reply.getKey();
                for (Map.Entry<Long, G6Base> entry : reply.getValue().entrySet()) {
                    final UnitMessage msgtype = UnitMessage.lookupByClassName(entry.getValue().getClass().getSimpleName());

                    ConnectionStorage.findConnection(getUnitId()).removeReply(cmdId);

                    if (msgtype != UnitMessage.UNDEFINED && msgtype != UnitMessage.NPH_SED_DEVICE_TITLE_DATA) {
                        final Map<String, String> commandParameters = new HashMap<>();
                        G6Base g6b = entry.getValue();

                        Class<? extends G6Base> clazz = g6b.getClass();
                        for (Field field : clazz.getDeclaredFields()) {

                            try {
                                field.setAccessible(true);
                                Object value = field.get(g6b);
                                if (value instanceof Struct.Unsigned32) {
                                    if (g6b instanceof G6NphPria && field.getName().equalsIgnoreCase("ip_address")) {
                                        commandParameters.put(field.getName(), G6NphPria.int2ip((int) ((G6NphPria) g6b).ip_address.get()));
                                    } else {
                                        commandParameters.put(field.getName(), String.valueOf(((Struct.Unsigned32) value).get()));
                                    }
                                } else if (value instanceof Struct.Unsigned16) {
                                    commandParameters.put(field.getName(), String.valueOf(((Struct.Unsigned16) value).get()));
                                } else if (value instanceof Struct.Unsigned8) {
                                    commandParameters.put(field.getName(), String.valueOf(((Struct.Unsigned8) value).get()));
                                } else if (value instanceof Struct.Signed32) {
                                    commandParameters.put(field.getName(), String.valueOf(((Struct.Signed32) value).get()));
                                } else if (value instanceof Struct.Signed16) {
                                    commandParameters.put(field.getName(), String.valueOf(((Struct.Signed16) value).get()));
                                } else if (value instanceof Struct.Signed8) {
                                    commandParameters.put(field.getName(), String.valueOf(((Struct.Signed8) value).get()));
                                } else if (value instanceof Struct.Bool) {
                                    commandParameters.put(field.getName(), String.valueOf(((Struct.Bool) value).get()));
                                } else if (value instanceof Struct.ByteSet) {
                                    if (g6b instanceof G6NphSedDeviceTitleData) {
                                        commandParameters.put(field.getName(), ((G6NphSedDeviceTitleData) g6b).getDataAsCp1251String());
                                    } else {
                                        commandParameters.put(field.getName(), getZeroEndString(((Struct.ByteSet) value).get()));
                                    }
                                } else if (value instanceof Struct.BitField) {
                                    commandParameters.put(field.getName(), String.valueOf(((Struct.BitField) value).longValue()));
                                } else {
                                    commandParameters.put(field.getName(), String.valueOf(value));
                                }
                            } catch (IllegalAccessException e) {
                                log.error("", e);
                            }
                        }

                        result.add(createAndAddCmdUnitPacket(msgtype.name(), new CommandPacket(getUnitId(),
                                                                                               cmdId,
                                                                                               Instant.now(),
                                                                                               msgtype.name(),
                                                                                               commandParameters)));
                    } else if (msgtype == UnitMessage.NPH_SED_DEVICE_TITLE_DATA) {

                        G6NphSedDeviceTitleData sedDeviceTitleData = ((G6NphSedDeviceTitleData) entry.getValue());

                        String msg = sedDeviceTitleData.getScript().getScriptTags().get("MSG");

                        if (msg != null && msg.length() > 0) {

                            result.add(UnitPacket.newBuilder()
                                               .setUnitId(getUnitId())
                                               .setEventTime(Instant.now())
                                               .addAttribute(CommonPacketFlag.TEXT_MESSAGE.name(), msg)
                                               .addAttribute(CommonPacketFlag.TEXT_MESSAGE_ID.name(),
                                                             ((G6NphSedDeviceTitleData) entry.getValue()).getScript().getScriptTags().get("ID"))
                                               .build()
                            );
                        } else {
                            final Map<String, String> commandParameters = new HashMap<>();
                            commandParameters.put("STATUS", ((G6NphSedDeviceTitleData) entry.getValue()).getScript().getScriptTags().get("STATUS"));
                            commandParameters.put("MESSAGE_ID", ((G6NphSedDeviceTitleData) entry.getValue()).getScript().getScriptTags().get("ID"));

                            result.add(createAndAddCmdUnitPacket(msgtype.name(), new CommandPacket(getUnitId(),
                                                                                                   cmdId,
                                                                                                   Instant.now(),
                                                                                                   msgtype.name(),
                                                                                                   commandParameters)));
                        }
                    } else {
                        log.debug("Message {} for {}", msgtype, entry.getValue().getClass().getSimpleName());
                    }
                }
            }
        } catch (Exception e) {
            log.debug("", e);
        }
        return result;
    }

    private UnitPacket createAndAddCmdUnitPacket(String msgname, CommandPacket pkt) {
        UnitPacket up = createCmdUnitPacket(getUnitId(), msgname, pkt);
        if (!pkt.getCmdName().equals("NPH_SED_DEVICE_RESULT")
                && !pkt.getCmdName().equals("NPH_SIM_IMSI")
                && !pkt.getCmdName().equals("NPH_RESULT")
                ) {
            log.debug("Command {}", up);
        }
        return up;
    }

    private UnitPacket createCmdUnitPacket(Long unitID, String typename, CommandPacket mainpkt) {
        UnitPacketBuilder unitPacketBuilder = UnitPacket.newBuilder()
                .setUnitId(unitID)
                .setEventTime(Instant.now())
                .addFlag(CommonPacketFlag.COMMAND_REPLY)
                .addAttribute(typename,
                              gson.toJson(mainpkt));
        return unitPacketBuilder.build();
    }

    private void ndtp6resolver(ChannelHandlerContext ctx,
                               MessageWrapper nplWrp) throws IOException, GranitException {
        if (nplWrp != null && nplWrp.getTrailer() != null) {
            ByteArrayInputStream bis = new ByteArrayInputStream(nplWrp.getTrailer());
            while (bis.available() > 0) {
                final G6NphHeader nph = readNphHeader(bis);
                if (nph.getType() == G6NphHeader.NPH_RESULT) {
                    doNphResulrProcessor(ctx, nph, bis);
                } else {
                    switch (nph.getServiceId()) {

                        case G6NphHeader.G6NphServiceType.NPH_SRV_NAVDATA:
                            doSrvNavDataProcessor(ctx, nph, bis, nplWrp);
                            break;

                        case G6NphHeader.G6NphServiceType.NPH_SRV_EXTERNAL_DEVICE:
                            doSrvExternalDeviceProcessor(ctx, nph, bis);
                            break;

                        case G6NphHeader.G6NphServiceType.NPH_SRV_GENERIC_CONTROLS:
                            doSrvGenericControlsProcessor(ctx, nph, nplWrp.getNpl().getDataSize(), bis);
                            break;

                        case G6NphHeader.G6NphServiceType.NPH_SRV_DEBUG:
                        case G6NphHeader.G6NphServiceType.NPH_SRV_CLIENT_LIST:
                        case G6NphHeader.G6NphServiceType.NPH_SRV_FILE_TRANSFER:
                            bisCleaner(bis);
                            break;

                        default:
                            break;
                    }
                }
            }
        }
    }

    private void doNphResulrProcessor(ChannelHandlerContext ctx, G6NphHeader nph,
                                      ByteArrayInputStream bis) throws IOException {
        if (getUnitId() != null && ConnectionStorage
                .findConnection(getUnitId())
                .getId() == getUnitId()) {

            G6NphResult resp = readNphResult(bis);
/*
            log.debug("\t response NPH_RESULT on requestID {} found: {}",
                    nph.getRequestId(),
                    G6NphResult.NPH_RESULT_CODES.lookup((int) resp.getError())
            );
*/

            ConnectionStorage.findConnection(getUnitId()).put(nph.getRequestId(), resp);
        } else {
            closeChannel(ctx);
        }
    }

    private void doSrvNavDataProcessor(ChannelHandlerContext ctx, G6NphHeader nph, ByteArrayInputStream bis,
                                       MessageWrapper nplWrp) throws IOException, GranitException.TypeNavDataException {

        if (nph.getType() == G6NphSrvNavdataConstant.Packages.NPH_SND_HISTORY
                || nph.getType() == G6NphSrvNavdataConstant.Packages.NPH_SND_REALTIME) {

            if (getUnitId() != null && ConnectionStorage
                    .findConnection(getUnitId())
                    .getId() == getUnitId()) {

                List<G6Base> cellList = new LinkedList<>();
                synchronized (ConnectionStorage.getLock(ctx)) {
                    //log.debug("\tType NPH_SND_HISTORY or NPH_SND_REALTIME found");
                    StringBuilder sb = new StringBuilder().append("{ UnitID ").append(getUnitId()).append(":\n");
                    while (bis.available() > 0) {
                        G6NphSrvNavdata srv = readNphSrvNavdata(bis);
                        Optional<G6Base> cell = CellFactory.createPrototype(this.config.ndtp, srv.getType(), srv.getNumber());
                        if (cell.isPresent()) {
                            cell.get().read(bis);
                            cellList.add(cell.get());
                            sb.append("{").append(cell.get().extractString()).append("}\n");
                        } else {
                            log.warn("Addition navigation data is not supported UnitID {} typeId {}; {} [{}]",
                                     getUnitId(),
                                     srv.getType(),
                                     getChannel(),
                                     nplWrp);
                            bisCleaner(bis);
                        }
                    }
                    if (config.ndtp.detailInfoLog) {
                        log.debug(sb.append("}").toString());
                    }
                    if (nph.getNph_flag_request()) {
                        sendNphResultResponse(new G6NphResult(G6NphResult.NPH_RESULT_CODES.NPH_RESULT_OK.code()),
                                              nph.getServiceId(),
                                              nph.getRequestId());
                    }
                }

                convertAndLoad(new GranitMessage.DATA(nph.getServiceId(),
                                                      nph.getRequestId(),
                                                      nph.getNph_flag_request(),
                                                      cellList.toArray(new G6Base[cellList.size()])),
                               nph.getType() == G6NphSrvNavdataConstant.Packages.NPH_SND_HISTORY);
            } else {
                closeChannel(ctx);
            }
        } else {
            log.warn("{}", String.format("Unknown type:%d for service NPH_SRV_NAVDATA", nph.getType()));
        }
    }

    private void doSrvExternalDeviceProcessor(ChannelHandlerContext ctx, G6NphHeader nph, ByteArrayInputStream bis) {
        if (getUnitId() != null && ConnectionStorage
                .findConnection(getUnitId())
                .getId() == getUnitId()) {

            switch (nph.getType()) {
                case G6NphSrvExternalDeviceConstant.Packages.NPH_SED_DEVICE_RESULT: {
                    G6NphNPHSedDeviceResult pkg = new G6NphNPHSedDeviceResult();
                    try {
                        pkg.read(bis);
                        if (config.ndtp.detailInfoLog) {
                            log.debug("\tType NPH_SED_DEVICE_RESULT {} found {}",
                                      G6NphSrvExternalDeviceConstant.Packages.NPH_SED_DEVICE_RESULT,
                                      pkg.extractString()
                            );
                        }
                        ConnectionStorage.findConnection(getUnitId()).put(nph.getRequestId(), pkg);

                        synchronized (ConnectionStorage.getLock(ctx)) {
                            sendResponse(
                                    new G6NphNPHSedDeviceResult(pkg.num_packet.intValue(),
                                                                G6NphResult.NPH_RESULT_CODES.NPH_RESULT_OK.code(),
                                                                pkg.message_id.intValue()
                                    ),
                                    nph.getServiceId(),
                                    G6NhpSrvGenericControlsConstant.Packages.NPH_SED_DEVICE_RESULT,
                                    nph.getRequestId(),
                                    Boolean.FALSE,

                                    G6Npl.NplPeerAddress.NPL_ADDRESS_BROAD_DISPATCHERS.getCode());
                        }
                    } catch (IOException ignored) {
                    }
                }
                break;
                case G6NphSrvExternalDeviceConstant.Packages.NPH_SED_DEVICE_TITLE_DATA: {

                    G6NphSedDeviceTitleData pkg = new G6NphSedDeviceTitleData(bis.available());
                    try {
                        pkg.read(bis);
                        log.debug("\tType NPH_SED_DEVICE_TITLE_DATA {} cmdId {} found {}",
                                  G6NphSrvExternalDeviceConstant.Packages.NPH_SED_DEVICE_TITLE_DATA,
                                  nph.getRequestId(),
                                  pkg.extractString()
                        );
                        ConnectionStorage.findConnection(getUnitId()).put(nph.getRequestId(), pkg);

                        synchronized (ConnectionStorage.getLock(ctx)) {
                            sendResponse(
                                    new G6NphNPHSedDeviceResult(pkg.num_packet.intValue(),
                                                                G6NphResult.NPH_RESULT_CODES.NPH_RESULT_OK.code(),
                                                                pkg.message_id.intValue()
                                    ),
                                    nph.getServiceId(),
                                    G6NhpSrvGenericControlsConstant.Packages.NPH_SED_DEVICE_RESULT,
                                    nph.getRequestId(),
                                    Boolean.FALSE,
                                    pkg.isStatusMessage() ? G6Npl.NplPeerAddress.NPL_ADDRESS_BROAD_DEVICES.getCode() :
                                    G6Npl.NplPeerAddress.NPL_ADDRESS_BROAD_DISPATCHERS.getCode()
                            );
                        }
                    } catch (IOException ignored) {
                    }
                }
                break;
                case G6NphSrvExternalDeviceConstant.Packages.NPH_SED_DEVICE_DATA: {
                    G6NphSedDeviceData pkg = new G6NphSedDeviceData();
                    try {
                        pkg.read(bis);
/*
                        log.debug("\tType NPH_SED_DEVICE_DATA {} found {}",
                                G6NphSrvExternalDeviceConstant.Packages.NPH_SED_DEVICE_DATA,
                                pkg.extractString()
                        );
*/
                        ConnectionStorage.findConnection(getUnitId()).put(nph.getRequestId(), pkg, (long) pkg.num_packet.get());
                    } catch (IOException ignored) {
                    }
                }
                break;
                default:
                    break;
            }
        } else {
            closeChannel(ctx);
        }
    }

    private void doSrvGenericControlsProcessor(ChannelHandlerContext ctx, G6NphHeader nph, int nplDataSize,
                                               ByteArrayInputStream bis)
            throws GranitException {
        switch (nph.getType()) {

            case G6NhpSrvGenericControlsConstant.Packages.NPH_SGC_CONN_REQUEST:
                synchronized (ConnectionStorage.getLock(ctx)) {
                    G6NphConnRequest connMsg = decodeConnect(nph, bis);
                    if (connMsg != null) {
                        GranitMessage.HANDSHAKE handshakeMsg = new GranitMessage.HANDSHAKE(nph.getServiceId(),
                                                                                           nph.getRequestId(),
                                                                                           connMsg.getPeerAddress(),
                                                                                           nplDataSize,
                                                                                           connMsg.isEncrypted());
                        try {
                            initUnitId(Long.toString(connMsg.getPeerAddress()));

                            ConnectionStorage.openConnection(ctx, getUnitId());
                            try {
                                sendSessionStart(Instant.now());
                            } catch (PacketTransferException e) {
                                log.warn("Can't send SessionStart: {}", e.getMessage());
                            }
                            log.debug("\tPEER_ADDRESS {}, UNIT_ID {}, ENCODED {}",
                                      handshakeMsg.getPeerAddress(),
                                      getUnitId(),
                                      handshakeMsg.isEncoded()
                            );

                            final G6Base pkg;
                            if (handshakeMsg.isEncoded()) {
                                pkg = new G6NphConnAuthString(RandomStringUtils.randomAlphabetic(handshakeMsg.getDataSize()));
                            } else {
                                pkg = new G6NphResult(G6NphResult.NPH_RESULT_CODES.NPH_RESULT_OK.code());
                            }

                            convertAndLoad(handshakeMsg, nph.getType() == G6NphSrvNavdataConstant.Packages.NPH_SND_HISTORY);

                            sendNphResultResponse(
                                    (G6NphResult) pkg,
                                    handshakeMsg.getServiceId(),
                                    handshakeMsg.getRequestId());
                        } catch (UnitIdNotFoundException ex) {
                            log.warn("Unit not found: {}", ex.getMessage());

                            sendNphResultResponse(
                                    new G6NphResult(G6NphResult.NPH_RESULT_CODES.NPH_RESULT_CLIENT_NOT_REGISTERED.code()),
                                    handshakeMsg.getServiceId(),
                                    handshakeMsg.getRequestId());

                            closeChannel(ctx);
                        }
                    }
                }
                break;

            case G6NhpSrvGenericControlsConstant.Packages.NPH_SGC_CONN_AUTH_STRING:
                if (getUnitId() != null && ConnectionStorage
                        .findConnection(getUnitId())
                        .getId() == getUnitId()) {

                    //TODO later
                    bisCleaner(bis);
                    log.error("Secured connection doesn't support now");
                } else {
                    closeChannel(ctx);
                }
                break;

            case G6NhpSrvGenericControlsConstant.Packages.NPH_SGC_SERVICE_REQUEST:
                if (getUnitId() != null && ConnectionStorage
                        .findConnection(getUnitId())
                        .getId() == getUnitId()) {

                    try {

                        G6NphSgcServiceRequest req = new G6NphSgcServiceRequest(true);
                        req.read(bis);
                        //log.debug("NPH_SGC_SERVICE_REQUEST reply/req: {}", req);

                        synchronized (ConnectionStorage.getLock(ctx)) {
                            sendResponse(
                                    new G6Base(),
                                    nph.getServiceId(),
                                    G6NhpSrvGenericControlsConstant.Packages.NPH_SGC_SERVICES,
                                    nph.getRequestId(),
                                    false, G6Npl.NplPeerAddress.NPL_ADDRESS_BROAD_DISPATCHERS.getCode()); // all
                        }
                    } catch (IOException e) {
                        throw new GranitException(e);
                    }
                } else {
                    closeChannel(ctx);
                }
                break;

            case G6NhpSrvGenericControlsConstant.Packages.NPH_SIM_IMSI:
            case G6NhpSrvGenericControlsConstant.Packages.NPH_SGC_PEER_DESC:
            case G6NhpSrvGenericControlsConstant.Packages.NPH_PRDO:
            case G6NhpSrvGenericControlsConstant.Packages.NPH_PRIA:
            case G6NhpSrvGenericControlsConstant.Packages.NPH_PRNAV:
            case G6NhpSrvGenericControlsConstant.Packages.NPH_INFO:
            case G6NhpSrvGenericControlsConstant.Packages.NPH_BALANCE:
            case G6NhpSrvGenericControlsConstant.Packages.NPH_ROUTE_AUTOINFORMER:

                if (getUnitId() != null && ConnectionStorage
                        .findConnection(getUnitId())
                        .getId() == getUnitId()) {

                    Class<? extends G6Base> clazz = UnitMessage.lookupExecutor(nph.getType());
                    if (clazz != null) {

                        try {
                            G6Base executor = clazz.getConstructor().newInstance();
                            executor.read(bis);

                            ConnectionStorage.findConnection(getUnitId()).put(nph.getRequestId(), executor);

                            if (config.ndtp.detailInfoLog) {
                                log.debug("\tCommand/Message: {} (#{}), requestId {} found: {}, total replies: {}",
                                          UnitMessage.lookup(nph.getType()),
                                          nph.getType(),
                                          nph.getRequestId(),
                                          executor.extractString(),
                                          ConnectionStorage.findConnection(getUnitId()).replies()
                                );
                            }
                        } catch (InstantiationException
                                | IllegalAccessException
                                | InvocationTargetException
                                | NoSuchMethodException
                                | IOException e) {
                            throw new GranitException(e);
                        }
                    }
                } else {
                    log.debug("\tCommand/Message received but unit {} offline", getUnitId());
                    closeChannel(ctx);
                }
                break;

            default:
                break;
        }
    }

    public void sendCrcError() {
        sendNphResultResponse(
                new G6NphResult(G6NphResult.NPH_RESULT_CODES.NPH_RESULT_PACKET_INVALID_FORMAT.code()),
                0,
                0);
    }

    private void closeChannel(ChannelHandlerContext ctx) {
        try {
            if (ctx != null && ctx.getChannel() != null && ctx.getChannel().isOpen()) {
                ctx.getChannel().close();
            }
        } catch (Throwable ignored) {
            //log.error("{} {}", tr.getMessage(), tr);
        }
    }

    private G6NphSrvNavdata readNphSrvNavdata(ByteArrayInputStream bis) throws IOException {
        final G6NphSrvNavdata snd = new G6NphSrvNavdata();
        snd.read(bis);
        return snd;
    }

    private synchronized void sendNphResultResponse(G6NphResult g6NphResult, int serviceId, long requestId) {

        if (!this.config.ndtp.silentModeOn) {
            try {
                final byte[] response = CellFactory.wrapNphResultPacket(g6NphResult, serviceId, requestId);
                sendToUnit(response);
            } catch (IOException e) {
                log.error("", e);
            }
        }
    }

    private void sendResponse(G6Base g6NphResult,
                              int serviceId,
                              int type,
                              long requestId,
                              boolean ack,
                              Integer nplPeerAddress) {
        if (!this.config.ndtp.silentModeOn) {
            try {
                sendToUnit(CellFactory
                                   .wrapNphPacket(g6NphResult,
                                                  serviceId,
                                                  type,
                                                  requestId,
                                                  ack,
                                                  nplPeerAddress));
            } catch (IOException e) {
                log.error("", e);
            }
        }
    }

    private G6NphConnRequest decodeConnect(final G6NphHeader nph, ByteArrayInputStream bis) {
        final G6NphConnRequest ncr = new G6NphConnRequest();
        try {
            if (nph.getType() == G6NhpSrvGenericControlsConstant.Packages.NPH_SGC_CONN_REQUEST
                    && nph.getServiceId() == G6NphHeader.G6NphServiceType.NPH_SRV_GENERIC_CONTROLS) {
                ncr.read(bis);
                if (ncr.getProtoVersionHigh() != G6NphConnRequest.NDTP_VERSION_HIGH) {
                    log.warn(String.format("Unknown protocol version:%d", ncr.getProtoVersionHigh()));
                } else {

                    log.debug("NPH Connect Request hdr: {}", ncr.extractString());

                    return ncr;
                }
            } else {
                log.warn(
                        String.format("Handshake state and unknown nph type (%d) and nph serviceId (%d)",
                                      nph.getType(),
                                      nph.getServiceId()));
            }
        } catch (IOException e) {
            log.error("", e);
        }
        return null;
    }

    private G6NphResult readNphResult(ByteArrayInputStream bis) throws IOException {
        G6NphResult result = new G6NphResult();
        result.read(bis);
        return result;
    }

    private G6NphHeader readNphHeader(ByteArrayInputStream bis) throws IOException {
        G6NphHeader nph = new G6NphHeader();
        nph.read(bis);
        return nph;
    }

    private UnitPacket makeHandshake() {
        setConnectTime(Instant.now());
        return UnitPacket.newBuilder()
                .setUnitId(getUnitId())
                .setEventTime(Instant.now())
                .setDeviceEventId(0)
                .build();
    }

    private void makeByebye() {
        setDisconnectTime(Instant.now());
        try {
            sendSessionComplete(getConnectTime(),
                                getDisconnectTime(),
                                getTotalPacketCnt(),
                                getInvalidpacketCnt(),
                                getHistPacketCnt());
        } catch (PacketTransferException e) {
            log.warn("", e);
        }
    }

    private void bisCleaner(ByteArrayInputStream bis) {
        bis.skip(bis.available());
    }

    private static String getZeroEndString(byte[] bytes) {
        int index = 0;
        for (; index < bytes.length; index++) {
            if (bytes[index] == 0 || bytes[index] == '\u0000') {
                break;
            }
        }
        byte[] res = new byte[index];
        System.arraycopy(bytes, 0, res, 0, index);
        return new String(res);
    }
}
