package ru.advantum.csptm.jep.hub.granit.handler;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Npl;
import ru.advantum.csptm.jep.hub.granit.protocol.internal.MessageWrapper;
import ru.advantum.csptm.jep.netty.SimpleStateDecoder;

public class GranitDecoder extends SimpleStateDecoder {

    private static final Logger log = LoggerFactory.getLogger(GranitDecoder.class);
    private final GranitHubConfig.Ndtp ndtpConfig;

    public GranitDecoder(GranitHubConfig.Ndtp ndtpConfig) {
        this.ndtpConfig = ndtpConfig;
    }

    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel channel,
                            ChannelBuffer buffer, STATE state) throws Exception {
        final G6Npl npl = readData(buffer, G6Npl.class);
        if (ndtpConfig.detailInfoLog) {
            G6Npl.NplPeerAddress addr = G6Npl.NplPeerAddress.lookup(npl.getPeerAddress());
            log.debug("Read NPL: {}, addr {}", npl.extractString(), addr);
        }

        return new MessageWrapper(npl, readTrailer(buffer, npl.getDataSize()));
    }
}
