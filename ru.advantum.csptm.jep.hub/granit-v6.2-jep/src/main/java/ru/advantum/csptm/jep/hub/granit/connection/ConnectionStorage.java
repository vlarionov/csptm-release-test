package ru.advantum.csptm.jep.hub.granit.connection;

import org.jboss.netty.channel.ChannelHandlerContext;

import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

public class ConnectionStorage {
    private static final ConcurrentSkipListMap<ChannelHandlerContextExt, DeviceWrapper> connections = new ConcurrentSkipListMap<>();
    private static final ConcurrentSkipListMap<Integer, Object> locks = new ConcurrentSkipListMap<>();

    public static void addLock(ChannelHandlerContext ctx) {
        locks.put(ctx.getChannel().getId(), new Object());
    }

    public static Object getLock(ChannelHandlerContext ctx) {
        locks.computeIfAbsent(ctx.getChannel().getId(), k -> new Object());
        return locks.get(ctx.getChannel().getId());
    }

    public static void removeLock(ChannelHandlerContext ctx) {
        locks.remove(ctx.getChannel().getId());
    }

    public static void openConnection(ChannelHandlerContext ctx, long unitId) {
        connections.put(new ChannelHandlerContextExt(ctx), new DeviceWrapper(unitId));
    }

    public static void closeConnection(ChannelHandlerContext ctx) {
        for( Map.Entry<ChannelHandlerContextExt, DeviceWrapper> entry : connections.entrySet()) {
            if ( entry.getKey().equals(ctx)) {
                connections.remove(entry.getKey());
            }
        }
    }

    public static DeviceWrapper findConnection(Long unitId) {
        if (unitId != null) {
            for (Map.Entry<ChannelHandlerContextExt, DeviceWrapper> entry : connections.entrySet()) {
                if (entry.getValue().getId() == unitId) {
                    return entry.getValue();
                }
            }
        }
        return new DeviceWrapper();
    }

    public static ChannelHandlerContextExt findConnectionCahnnel(Long unitId) {
        if (unitId != null) {
            for (Map.Entry<ChannelHandlerContextExt, DeviceWrapper> entry : connections.entrySet()) {
                if (entry.getValue().getId() == unitId) {
                    return entry.getKey();
                }
            }
        }
        return null;
    }

}
