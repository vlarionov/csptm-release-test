package ru.advantum.csptm.jep.hub.granit.connection;

import org.jboss.netty.channel.*;

public class ChannelHandlerContextExt implements ChannelHandlerContext, Comparable<ChannelHandlerContextExt> {
    private ChannelHandlerContext ctx;


    public ChannelHandlerContextExt(ChannelHandlerContext ctx) {
        setCtx(ctx);
    }

    @Override
    public int hashCode() {
        if (getCtx() != null && getCtx().getChannel() != null) {
            return getCtx().getChannel().getId();
        }
        return -1;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (o instanceof ChannelHandlerContextExt) {
            return bothEquals((ChannelHandlerContextExt) o);
        } else if (o instanceof ChannelHandlerContext) {
            return bothEquals((ChannelHandlerContext) o);
        }
        return false;
    }

    private boolean bothEquals(ChannelHandlerContextExt other) {
        return other.getChannel().getId().equals(getChannel().getId()) &&
                (other.getName() == null || other.getName().equals(getName()));
    }

    private boolean bothEquals(ChannelHandlerContext other) {
        return other.getChannel().getId().equals(getChannel().getId()) &&
                (other.getName() == null || other.getName().equals(getName()));
    }

    private ChannelHandlerContext getCtx() {
        return ctx;
    }

    private void setCtx(ChannelHandlerContext ctx) {
        this.ctx = ctx;
    }

    @Override
    public Channel getChannel() {
        return getCtx().getChannel();
    }

    @Override
    public ChannelPipeline getPipeline() {
        return getCtx().getPipeline();
    }

    @Override
    public String getName() {
        return getCtx().getName();
    }

    @Override
    public ChannelHandler getHandler() {
        return getCtx().getHandler();
    }

    @Override
    public boolean canHandleUpstream() {
        return getCtx().canHandleUpstream();
    }

    @Override
    public boolean canHandleDownstream() {
        return getCtx().canHandleDownstream();
    }

    @Override
    public void sendUpstream(ChannelEvent channelEvent) {
        getCtx().sendUpstream(channelEvent);
    }

    @Override
    public void sendDownstream(ChannelEvent channelEvent) {
        getCtx().sendDownstream(channelEvent);
    }

    @Override
    public Object getAttachment() {
        return getCtx().getAttachment();
    }

    @Override
    public void setAttachment(Object o) {
        getCtx().setAttachment(o);
    }

    @Override
    public int compareTo(ChannelHandlerContextExt o) {
        return getChannel() != null ? getChannel().getId().compareTo(o.getChannel().getId()) : 0;
    }
}
