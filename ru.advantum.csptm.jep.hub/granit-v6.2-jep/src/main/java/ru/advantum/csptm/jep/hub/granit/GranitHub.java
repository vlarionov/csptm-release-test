package ru.advantum.csptm.jep.hub.granit;

import org.jboss.netty.channel.ChannelPipeline;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.jep.core.JepHubController;
import ru.advantum.csptm.jep.core.rmq.JepRmqHubController;
import ru.advantum.csptm.jep.hub.granit.handler.GranitDecoder;
import ru.advantum.csptm.jep.hub.granit.handler.GranitLogic;
import ru.advantum.csptm.jep.netty.JepNettyHub;

import java.nio.ByteOrder;

public class GranitHub extends JepNettyHub {

    private GranitHubConfig config;

    public GranitHub(GranitHubConfig config, JepHubController controller) {
        super(config.hubConfig.tcpConfig, controller);
        this.config = config;
    }

    @Override
    protected void tunePipeline(ChannelPipeline pipeline) {
        pipeline.addLast("decoder", new GranitDecoder(config.ndtp));
        pipeline.addLast("logic", new GranitLogic(this, this.config));
    }

    @Override
    protected ByteOrder getByteOrder() {
        return ByteOrder.LITTLE_ENDIAN;
    }

    public static void main(String[] args) throws Throwable {
        GranitHubConfig granitHubConfig = Configuration.unpackConfig(GranitHubConfig.class, "hub.xml");
        JepRmqHubController controller = new JepRmqHubController(granitHubConfig.hubConfig.controllerConfig);

        GranitHub hub = new GranitHub(granitHubConfig, controller);
        Runtime.getRuntime().addShutdownHook(new Thread(hub::stop));
        controller.start(hub);
    }
}
