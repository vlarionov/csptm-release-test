package ru.advantum.csptm.jep.hub.granit.connection;

import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.util.TreeMap;
import java.util.concurrent.ConcurrentSkipListMap;

public class DeviceWrapper {
    private long id = Long.MIN_VALUE;
    private ConcurrentSkipListMap<Long, TreeMap<Long, G6Base>> commandReplies = new ConcurrentSkipListMap<>();

    public DeviceWrapper() {
    }

    public DeviceWrapper(long id) {
        setId(id);
    }

    public long getId() {
        return id;
    }

    public DeviceWrapper setId(long id) {
        this.id = id;
        return this;
    }

    public DeviceWrapper removeReply(long id) {
        commandReplies.remove(id);
        return this;
    }

    private TreeMap<Long, G6Base> getOrCreate(Long id) {
        TreeMap<Long, G6Base> res = commandReplies.computeIfAbsent(id, k -> new TreeMap<>());
        return res;
    }

    public DeviceWrapper put(long id, G6Base g6) {
        put(id, g6, 0L);
        return this;
    }

    public DeviceWrapper put(long id, G6Base g6, Long subnum) {
        getOrCreate(id).put(subnum, g6);
        return this;
    }

    public int replies() {
        return commandReplies.size();
    }

    public ConcurrentSkipListMap<Long, TreeMap<Long, G6Base>> getCommandReplies() {
        return commandReplies;
    }
}
