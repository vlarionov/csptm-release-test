package ru.advantum.csptm.jep.hub.granit.test;

import org.junit.Test;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.commands.G6NphSetLoadfirm;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.generic.controls.G6NphPria;

public class CommonTests {
    private static GranitHubConfig getConfig() throws Exception {
        System.setProperty("advantum.config.dir", "src/main/resources/extras/examples");
        return Configuration.unpackConfig(GranitHubConfig.class, "hub.xml");
    }


    @Test
    public void configTest() throws Exception {
        GranitHubConfig config = getConfig();
        if (config != null) {
            System.out.println("cell-08: det_status=" + config.ndtp.cell08Numbers.det_status
                    + ", level_l=" + config.ndtp.cell08Numbers.level_l
                    + ", level_mm=" + config.ndtp.cell08Numbers.level_mm
                    + ", temperature08=" + config.ndtp.cell08Numbers.temperature08);
            for (GranitHubConfig.Ndtp.Cell e : config.ndtp.cells) {
                System.out.println("cell: " + e.cellNumber + ", ndtp number:" + e.ndtpNumber + " as jep number:" + e.jepNumber + " cast:" + e.cast);

            }

        }
    }

    @Test
    public void timestamp() {

        G6NphSetLoadfirm ex = new G6NphSetLoadfirm();
        //2603172874
        String address = "10.68.41.155";

        String[] ipAddressInArray = address.split("\\.");
        long result = G6NphPria.ip2int(address);
        int r2 = Integer.reverseBytes((int) result);
        int a = 0;
    }

}
