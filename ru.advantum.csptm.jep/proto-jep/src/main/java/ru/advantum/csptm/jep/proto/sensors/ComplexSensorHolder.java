package ru.advantum.csptm.jep.proto.sensors;

import java.math.BigDecimal;

/**
 * Составной датчик.
 */
public class ComplexSensorHolder {

    /**
     * Тип датчика.
     */
    private final Type type;

    /**
     * Значения датчика.
     */
    private final BigDecimal[] values;

    public ComplexSensorHolder(Type type, BigDecimal[] values) {
        this.type = type;
        this.values = new BigDecimal[values.length];
        System.arraycopy(values, 0, this.values, 0, values.length);
    }

    public ComplexSensorHolder(Type type, BigDecimal value) {
        this(type, new BigDecimal[]{value});
    }

    public ComplexSensorHolder(Type type, int value) {
        this(type, new BigDecimal[]{new BigDecimal(value)});
    }

    public Type getType() {
        return type;
    }

    public BigDecimal[] getValues() {
        return values;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("ComplexSensorHolder: {type=" + type + ", values:[");
        for (BigDecimal value : values) {
            sb.append(value.intValue()).append(",");
        }
        return sb.deleteCharAt(sb.lastIndexOf(",")).append("]}").toString();
    }
}
