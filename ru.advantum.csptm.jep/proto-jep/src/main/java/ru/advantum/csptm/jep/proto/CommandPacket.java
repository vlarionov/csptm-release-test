package ru.advantum.csptm.jep.proto;

import java.time.Instant;
import java.util.Collections;
import java.util.Map;

/**
 * Команда для блока.
 */
public class CommandPacket {

    /**
     * Идентификатор блока.
     */
    private final long unitId;

    /**
     * Идентификатор команды.
     */
    private final long cmdId;

    /**
     * Время отправки.
     */
    private final Instant timeSent;

    /**
     * Текстовое название команды по протоколу.
     */
    private final String cmdName;

    /**
     * Параметры.
     */
    private final Map<String, String> parameters;

    public CommandPacket(long unitId,
                         long cmdId,
                         Instant timeSent,
                         String cmdName,
                         Map<String, String> parameters) {
        this.unitId = unitId;
        this.cmdId = cmdId;
        this.timeSent = timeSent;
        this.cmdName = cmdName;
        this.parameters = parameters;
    }

    public long getUnitId() {
        return unitId;
    }

    public long getCmdId() {
        return cmdId;
    }

    public Instant getTimeSent() {
        return timeSent;
    }

    public String getCmdName() {
        return cmdName;
    }

    public String getParameter(String parameter) {
        return parameters.get(parameter);
    }

    public Map<String, String> getParameters() {
        return Collections.unmodifiableMap(parameters);
    }

    @Override
    public String toString() {
        return "CommandPacket:{" +
                "unitId=" + getUnitId() +
                ", cmdId=" + getCmdId() +
                ", timeSent=" + getTimeSent() +
                ", cmdName=" + getCmdName() +
                ", parameters=" + parameters.size() +
                "}";
    }

}
