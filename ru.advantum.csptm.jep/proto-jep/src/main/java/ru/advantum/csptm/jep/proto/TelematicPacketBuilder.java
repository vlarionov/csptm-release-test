package ru.advantum.csptm.jep.proto;

import java.time.Instant;

public class TelematicPacketBuilder {

    private boolean locationValid;
    private Instant gpsTime;
    private float lon;
    private float lat;
    private Integer alt;
    private Integer speed;
    private Integer heading;

    public TelematicPacketBuilder setLocationValid(boolean locationValid) {
        this.locationValid = locationValid;
        return this;
    }

    public TelematicPacketBuilder setGpsTime(Instant gpsTime) {
        this.gpsTime = gpsTime;
        return this;
    }

    public TelematicPacketBuilder setCoordinates(float lon, float lat) {
        this.lon = lon;
        this.lat = lat;
        return this;
    }

    public TelematicPacketBuilder setAlt(int alt) {
        this.alt = alt;
        return this;
    }

    public TelematicPacketBuilder setSpeed(int speed) {
        this.speed = speed;
        return this;
    }

    public TelematicPacketBuilder setHeading(int heading) {
        this.heading = heading;
        return this;
    }

    public TelematicPacket build() {
        return new TelematicPacket(locationValid, gpsTime, lon, lat, alt, speed, heading);
    }
}