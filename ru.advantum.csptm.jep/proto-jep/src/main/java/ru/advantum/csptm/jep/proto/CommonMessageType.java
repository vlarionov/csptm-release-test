package ru.advantum.csptm.jep.proto;

/**
 * Created by kaganov on 05/12/2016.
 * Типы пакетов для отправки в брокер сообщений.
 */
public enum CommonMessageType {

    /**
     * Координаты.
     *
     * @see UnitPacketsBundle
     */
    COORDS,
    /**
     * Старт хаба.
     *
     * @see HubStart
     */
    HUB_START,
    /**
     * Обновление блоков для хаба.
     *
     * @see UnitBoundChange
     */
    UNIT_BOUNDS,
    /**
     * Команда для блока.
     *
     * @see CommandPacket
     */
    COMMAND,
    /**
     * Сессии блока.
     *
     * @see UnitSessionBundle
     */
    UNIT_SESSION

}
