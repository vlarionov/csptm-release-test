package ru.advantum.csptm.jep.proto;

/**
 * Created by kaganov on 05/12/2016.
 * Информация об изменении набора блоков, обрабатываемых хабом.
 */
public class UnitBoundChange {

    /**
     * Номер блока.
     */
    private final String unitNum;
    /**
     * Идентификатор блока.
     */
    private final long unitId;
    /**
     * Тип действия (добавление, изменение).
     */
    private final ChangeType changeType;

    public UnitBoundChange() {
        this(null, 0, ChangeType.ADDED);
    }

    public UnitBoundChange(String unitNum, long unitId, ChangeType changeType) {
        this.unitNum = unitNum;
        this.unitId = unitId;
        this.changeType = changeType;
    }

    public String getUnitNum() {
        return unitNum;
    }

    public long getUnitId() {
        return unitId;
    }

    public ChangeType getChangeType() {
        return changeType;
    }

    public enum ChangeType {
        DELETED, ADDED
    }
}
