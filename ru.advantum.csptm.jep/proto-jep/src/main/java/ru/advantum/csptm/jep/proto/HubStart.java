package ru.advantum.csptm.jep.proto;

/**
 * Created by kaganov on 02/12/2016.
 * Сообщение о запуске хаба.
 */
public class HubStart {

    /**
     * Идентификатор хаба.
     */
    private final int hubId;

    public HubStart(int hubId) {
        this.hubId = hubId;
    }

    public int getHubId() {
        return hubId;
    }

    @Override
    public String toString() {
        return "HubStart{" +
                "hubId=" + hubId +
                '}';
    }
}
