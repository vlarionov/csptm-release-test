package ru.advantum.csptm.jep.proto;

import java.time.Instant;

/**
 * Created by kaganov on 03/04/2017.
 * Текущее состояние ТС.
 */
public class TrState {

    /**
     * Время обновления состояния.
     */
    private final Instant stateUpdate;
    /**
     * Пакет, содержащий все последнее состояние.
     */
    private final UnitPacket packet;

    public TrState(Instant stateUpdate, UnitPacket packet) {
        this.stateUpdate = stateUpdate;
        this.packet = packet;
    }

    public Instant getStateUpdate() {
        return stateUpdate;
    }

    public UnitPacket getPacket() {
        return packet;
    }
}
