package ru.advantum.csptm.jep.proto;

import java.time.Instant;

/**
 * Created by kaganov on 24/11/2016.
 * Телематическая информация от блока.
 */
public class TelematicPacket {

    /**
     * Признак корректности координат.
     */
    private final boolean locationValid;
    /**
     * Время координат.
     */
    private final Instant gpsTime;
    /**
     * Долгота.
     */
    private final float lon;
    /**
     * Широта.
     */
    private final float lat;
    /**
     * Высота.
     */
    private final Integer alt;
    /**
     * Скорость.
     */
    private final Integer speed;
    /**
     * Направление.
     */
    private final Integer heading;

    public static TelematicPacketBuilder newBuilder() {
        return new TelematicPacketBuilder();
    }

    TelematicPacket(boolean locationValid, Instant gpsTime, float lon, float lat,
                    Integer alt, Integer speed, Integer heading) {
        this.locationValid = locationValid;
        this.gpsTime = gpsTime;
        this.lon = lon;
        this.lat = lat;
        this.alt = alt;
        this.speed = speed;
        this.heading = heading;
    }

    public boolean isLocationValid() {
        return locationValid;
    }

    public Instant getGpsTime() {
        return gpsTime;
    }

    public float getLon() {
        return lon;
    }

    public float getLat() {
        return lat;
    }

    public Integer getAlt() {
        return alt;
    }

    public Integer getSpeed() {
        return speed;
    }

    public Integer getHeading() {
        return heading;
    }

    @Override
    public String toString() {
        return "TelematicPacket{" +
                "locationValid=" + locationValid +
                ", gpsTime=" + gpsTime +
                ", lon=" + lon +
                ", lat=" + lat +
                ", alt=" + alt +
                ", speed=" + speed +
                ", heading=" + heading +
                '}';
    }
}
