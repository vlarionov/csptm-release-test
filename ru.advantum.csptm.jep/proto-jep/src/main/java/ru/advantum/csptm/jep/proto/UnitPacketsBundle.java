package ru.advantum.csptm.jep.proto;

import java.util.Arrays;
import java.util.List;

/**
 * @author kaganov
 * Массив пакетов, получаемых от блока.
 */
public class UnitPacketsBundle {

    /**
     * Идентификатор хаба (системы приема-передачи данных).
     */
    private final int hubId;
    /**
     * Набор пакетов.
     */
    private final List<UnitPacket> packets;

    public static UnitPacketsBundle of(int hubId, UnitPacket... packets) {
        return new UnitPacketsBundle(hubId, Arrays.asList(packets));
    }

    public static UnitPacketsBundle of(int hubId, List<UnitPacket> packets) {
        return new UnitPacketsBundle(hubId, packets);
    }

    private UnitPacketsBundle(int hubId, List<UnitPacket> packets) {
        this.hubId = hubId;
        this.packets = packets;
    }

    public int getHubId() {
        return hubId;
    }

    public List<UnitPacket> getPackets() {
        return packets;
    }

    @Override
    public String toString() {
        return "UnitPacketsBundle{" +
                "hubId=" + hubId +
                ", packets=" + packets +
                '}';
    }
}
