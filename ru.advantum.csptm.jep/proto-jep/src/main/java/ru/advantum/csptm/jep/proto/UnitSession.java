package ru.advantum.csptm.jep.proto;

import java.time.Instant;

/**
 * Created by kaganov on 23/06/2017.
 * Сессия блока.
 */
public class UnitSession {

    /**
     * Идентификатор хаба (системы приема-передачи данных).
     */
    private final int hubId;
    /**
     * Идентификатор блока.
     */
    private final long unitId;
    /**
     * Идентификатор ТС.
     */
    private final Long trId;
    /**
     * Время подключения к хабу.
     */
    private final Instant connectTime;
    /**
     * Время отключения от хаба.
     */
    private final Instant disconnectTime;
    /**
     * Общее количество переданных пакетов.
     */
    private final long totalPacketCnt;
    /**
     * Количество переданных пакетов, помеченных блоком как некорректные.
     */
    private final long invalidPacketCnt;
    /**
     * Количество переданных пакетов, помеченных блоком как исторические.
     */
    private final long histPacketCnt;
    /**
     * Внутренний номер сесии (не является уникальным сам по себе).
     */
    private final int sessionId;

    public UnitSession(int hubId,
                       long unitId,
                       Long trId,
                       Instant connectTime,
                       Instant disconnectTime,
                       long totalPacketCnt,
                       long invalidPacketCnt,
                       long histPacketCnt,
                       Integer sessionId) {

        this.hubId = hubId;
        this.unitId = unitId;
        this.trId = trId;
        this.connectTime = connectTime;
        this.disconnectTime = disconnectTime;
        this.totalPacketCnt = totalPacketCnt;
        this.invalidPacketCnt = invalidPacketCnt;
        this.histPacketCnt = histPacketCnt;
        this.sessionId = sessionId;
    }

    public UnitSession(int hubId,
                       long unitId,
                       Instant connectTime,
                       Instant disconnectTime,
                       long totalPacketCnt,
                       long invalidPacketCnt,
                       long histPacketCnt,
                       Integer sessionId) {
        this(hubId,
             unitId,
             null,
             connectTime,
             disconnectTime,
             totalPacketCnt,
             invalidPacketCnt,
             histPacketCnt,
             sessionId);
    }

    public UnitSession(int hubId,
                       long unitId,
                       Instant connectTime,
                       Integer sessionId) {
        this(hubId,
             unitId,
             null,
             connectTime,
             null,
             0,
             0,
             0,
             sessionId);
    }

    public int getHubId() {
        return hubId;
    }

    public long getUnitId() {
        return unitId;
    }

    public Long getTrId() {
        return trId;
    }

    public Instant getConnectTime() {
        return connectTime;
    }

    public Instant getDisconnectTime() {
        return disconnectTime;
    }

    public long getTotalPacketCnt() {
        return totalPacketCnt;
    }

    public long getInvalidPacketCnt() {
        return invalidPacketCnt;
    }

    public long getHistPacketCnt() {
        return histPacketCnt;
    }

    public Integer getSessionId() {
        return sessionId;
    }

    @Override
    public String toString() {
        return "UnitSession{" +
                "hubId=" + hubId +
                ", unitId=" + unitId +
                ", trId=" + trId +
                ", connectTime=" + connectTime +
                ", disconnectTime=" + disconnectTime +
                ", totalPacketCnt=" + totalPacketCnt +
                ", invalidPacketCnt=" + invalidPacketCnt +
                ", histPacketCnt=" + histPacketCnt +
                ", sessionId=" + sessionId +
                '}';
    }
}
