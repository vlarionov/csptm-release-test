package ru.advantum.csptm.jep.proto;

import ru.advantum.csptm.jep.proto.sensors.ComplexSensorHolder;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.*;

public class UnitPacketBuilder {

    private Long packetId;
    private Long trId;
    private Long unitId;
    private Instant eventTime;
    private Instant receiveTime;
    private long deviceEventId;
    private TelematicPacket telematic;
    private Map<Integer, BigDecimal> sensors;
    private Map<Integer, ComplexSensorHolder> complexSensors;
    private Set<String> flags;
    private Map<String, Object> attributes;

    public UnitPacketBuilder fromUnitPacket(UnitPacket unitPacket) {
        this.packetId = unitPacket.getPacketId();
        this.trId = unitPacket.getTrId();
        this.unitId = unitPacket.getUnitId();
        this.eventTime = unitPacket.getEventTime();
        this.receiveTime = unitPacket.getReceiveTime();
        this.deviceEventId = unitPacket.getDeviceEventId();
        this.telematic = unitPacket.getTelematic();
        this.sensors = unitPacket.getSensors() == null ? null : new HashMap<>(unitPacket.getSensors());
        this.complexSensors = unitPacket.getComplexSensors() == null ? null : new TreeMap<>(unitPacket.getComplexSensors());
        this.flags = unitPacket.getFlags() == null ? null : new HashSet<>(unitPacket.getFlags());
        this.attributes = unitPacket.getAttributes() == null ? null : new HashMap<>(unitPacket.getAttributes());

        return this;
    }

    public UnitPacketBuilder setPacketId(Long packetId) {
        this.packetId = packetId;
        return this;
    }

    public UnitPacketBuilder setTrId(long trId) {
        this.trId = trId;
        return this;
    }

    public UnitPacketBuilder setUnitId(long unitId) {
        this.unitId = unitId;
        return this;
    }

    public UnitPacketBuilder setEventTime(Instant eventTime) {
        this.eventTime = eventTime;
        return this;
    }

    public UnitPacketBuilder setReceiveTime(Instant receiveTime) {
        this.receiveTime = receiveTime;
        return this;
    }

    public UnitPacketBuilder setDeviceEventId(long deviceEventId) {
        this.deviceEventId = deviceEventId;
        return this;
    }

    public UnitPacketBuilder setTelematic(TelematicPacket telematic) {
        this.telematic = telematic;
        return this;
    }

    public UnitPacketBuilder setSensors(Map<Integer, BigDecimal> sensors) {
        this.sensors = new HashMap<>(sensors);
        return this;
    }

    public UnitPacketBuilder setComplexSensors(Map<Integer, ComplexSensorHolder> complexSensors) {
        this.complexSensors = new TreeMap<>(complexSensors);
        return this;
    }

    public UnitPacketBuilder addComplexSensors(int sensorId, ComplexSensorHolder value) {
        if (this.complexSensors == null) {
            this.complexSensors = new TreeMap<>();
        }
        this.complexSensors.put(sensorId, value);
        return this;
    }

    public UnitPacketBuilder addComplexSensors(Map<Integer, ComplexSensorHolder> complexSensors) {
        if (this.complexSensors == null) {
            this.complexSensors = new TreeMap<>();
        }
        this.complexSensors.putAll(complexSensors);
        return this;
    }

    public UnitPacketBuilder addSensor(int sensorId, BigDecimal value) {
        if (sensors == null) {
            sensors = new HashMap<>();
        }
        sensors.put(sensorId, value);

        return this;
    }

    public UnitPacketBuilder addSensors(Map<Integer, BigDecimal> sensors) {
        if (this.sensors == null) {
            this.sensors = new HashMap<>();
        }
        this.sensors.putAll(sensors);

        return this;
    }

    public UnitPacketBuilder setFlags(Set<String> flags) {
        this.flags = new HashSet<>(flags);
        return this;
    }

    public <T extends Enum<?>> UnitPacketBuilder addFlag(T flag) {
        return addFlag(flag.name());
    }

    public UnitPacketBuilder addFlag(String flag) {
        if (flags == null) {
            flags = new HashSet<>();
        }
        flags.add(flag);

        return this;
    }

    public UnitPacketBuilder addFlags(Set<String> flags) {
        if (this.flags == null) {
            this.flags = new HashSet<>();
        }
        this.flags.addAll(flags);

        return this;
    }

    public UnitPacketBuilder setAttributes(Map<String, Object> attributes) {
        this.attributes = new HashMap<>(attributes);
        return this;
    }

    public UnitPacketBuilder addAttribute(String attrName, Object attrValue) {
        if (attributes == null) {
            attributes = new HashMap<>();
        }
        attributes.put(attrName, attrValue);

        return this;
    }

    public UnitPacketBuilder addAttributes(Map<String, Object> attributes) {
        if (this.attributes == null) {
            this.attributes = new HashMap<>();
        }
        this.attributes.putAll(attributes);

        return this;
    }

    public UnitPacket build() {
        Objects.requireNonNull(unitId, "unitId can't be null");
        Objects.requireNonNull(eventTime, "eventTime can't be null");

        if (receiveTime == null) {
            receiveTime = Instant.now();
        }

        return new UnitPacket(packetId, trId, unitId, eventTime, receiveTime, deviceEventId, telematic, sensors, complexSensors, flags, attributes);
    }
}