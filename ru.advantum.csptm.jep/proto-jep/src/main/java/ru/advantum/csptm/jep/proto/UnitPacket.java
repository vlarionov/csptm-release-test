package ru.advantum.csptm.jep.proto;

import ru.advantum.csptm.jep.proto.sensors.ComplexSensorHolder;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Map;
import java.util.Set;

/**
 * Created by kaganov on 23/11/2016.
 *
 * Пакет, получаемый от блоков.
 */
public class UnitPacket {

    /**
     * Идентификатор пакета, сам по себе не уникален, уникальна тройка {@code packetId, trId, eventTime}.
     */
    private final Long packetId;
    /**
     * Идентификатор ТС (из связки с блоком), от которого пришел пакет.
     */
    private final Long trId;
    /**
     * Идентификатор блока.
     */
    private final long unitId;
    /**
     * Время пакета, полученное от блока.
     */
    private final Instant eventTime;
    /**
     * Время получения пакета хабом.
     */
    private final Instant receiveTime;
    /**
     * Идентификатор события блока.
     */
    private final long deviceEventId;
    /**
     * Данные о телематике.
     */
    private final TelematicPacket telematic;
    /**
     * Датчики, ключом является логический номер присылаемой сущности, которое описывает "номер датчика" блока.
     */
    private final Map<Integer, BigDecimal> sensors;
    /**
     * Составные датчики, ключом является логический номер присылаемой сущности, которое описывает "номер датчика" блока.
     */
    private final Map<Integer, ComplexSensorHolder> complexSensors;
    /**
     * Флаги пакета (ALARM, HIST_DATA, COMMAND_REPLY, CALL_REQUEST и т.д.).
     */
    private final Set<String> flags;
    /**
     * Атрибуты пакета (TEXT_MESSAGE, дополнительные атрибуты, содержащиеся в ответах на команды).
     */
    private final Map<String, Object> attributes;

    public static UnitPacketBuilder newBuilder() {
        return new UnitPacketBuilder();
    }

    UnitPacket(Long packetId, Long trId, long unitId, Instant eventTime, Instant receiveTime, long deviceEventId,
               TelematicPacket telematic, Map<Integer, BigDecimal> sensors,
               Map<Integer, ComplexSensorHolder> complexSensors,
               Set<String> flags,
               Map<String, Object> attributes) {
        this.packetId = packetId;
        this.trId = trId;
        this.unitId = unitId;
        this.eventTime = eventTime;
        this.receiveTime = receiveTime;
        this.deviceEventId = deviceEventId;
        this.telematic = telematic;
        this.sensors = sensors;
        this.complexSensors = complexSensors;
        this.flags = flags;
        this.attributes = attributes;
    }

    public Long getPacketId() {
        return packetId;
    }

    public Long getTrId() {
        return trId;
    }

    public long getUnitId() {
        return unitId;
    }

    public Instant getEventTime() {
        return eventTime;
    }

    public Instant getReceiveTime() {
        return receiveTime;
    }

    public long getDeviceEventId() {
        return deviceEventId;
    }

    public TelematicPacket getTelematic() {
        return telematic;
    }

    public Map<Integer, BigDecimal> getSensors() {
        return sensors;
    }

    public Map<Integer, ComplexSensorHolder> getComplexSensors() {
        return complexSensors;
    }

    public Set<String> getFlags() {
        return flags;
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    @Override
    public String toString() {
        return "UnitPacket{" +
                "packetId='" + packetId + '\'' +
                ", trId=" + trId +
                ", unitId=" + unitId +
                ", eventTime=" + eventTime +
                ", receiveTime=" + receiveTime +
                ", deviceEventId=" + deviceEventId +
                ", telematic=" + telematic +
                ", sensors=" + sensors +
                ", complexSensors=" + complexSensors +
                ", flags=" + flags +
                ", attributes=" + attributes +
                '}';
    }
}
