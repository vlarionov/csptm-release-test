package ru.advantum.csptm.jep.proto;

import java.util.Arrays;
import java.util.List;

/**
 * Created by kukushkin on 9/4/17.
 * Массив сессий блоков.
 */
public class UnitSessionBundle {

    /**
     * Идентификатор хаба (системы приема-передачи данных).
     */
    private final int hubId;
    /**
     * Набор сессий.
     */
    private final List<UnitSession> packets;

    public static UnitSessionBundle of(int hubId, UnitSession... packets) {
        return new UnitSessionBundle(hubId, Arrays.asList(packets));
    }

    public static UnitSessionBundle of(int hubId, List<UnitSession> packets) {
        return new UnitSessionBundle(hubId, packets);
    }

    private UnitSessionBundle(int hubId, List<UnitSession> packets) {
        this.hubId = hubId;
        this.packets = packets;
    }

    public List<UnitSession> getPackets() {
        return packets;
    }

    public int getHubId() {
        return hubId;
    }
}
