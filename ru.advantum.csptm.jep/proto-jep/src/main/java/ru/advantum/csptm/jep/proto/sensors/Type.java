package ru.advantum.csptm.jep.proto.sensors;

/**
 * Типы составных датчиков.
 */
public enum Type {
    /**
     * ДУТ.
     */
    FUEL(4),
    /**
     * Дискретный датчик.
     */
    BINARY,
    /**
     * Аналоговый датчик.
     */
    ANALOG,
    /**
     * Дополнительная информация от блока (кол-во спутников, качество связи и т.д.).
     */
    EXTENDED,
    /**
     * ИРМА, датчик пассажиропотоков.
     */
    IRMA,
    /**
     * Температурный датчик.
     */
    TEMPERATURE(2),
    /**
     * Неизвестный датчик.
     */
    UNKNOWN;

    int maxArraySize = 0;

    Type() {
    }

    Type(int maxArraySize) {
        setMaxArraySize(maxArraySize);
    }

    public int getMaxArraySize() {
        return maxArraySize;
    }

    public void setMaxArraySize(int maxArraySize) {
        this.maxArraySize = maxArraySize;
    }
}
