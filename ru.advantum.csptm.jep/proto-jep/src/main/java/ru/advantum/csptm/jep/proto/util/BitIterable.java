package ru.advantum.csptm.jep.proto.util;

import java.util.Iterator;

/**
 * @author kaganov
 */
public class BitIterable implements Iterable<Boolean> {

    private final int bitCnt;
    private final long value;

    public BitIterable(int bitCnt, long value) {
        this.bitCnt = bitCnt;
        this.value = value;
    }

    public static BitIterable ofValue(byte value) {
        return new BitIterable(Byte.SIZE, value);
    }

    public static BitIterable ofValue(short value) {
        return new BitIterable(Short.SIZE, value);
    }

    public static BitIterable ofValue(int value) {
        return new BitIterable(Integer.SIZE, value);
    }

    public static BitIterable ofValue(long value) {
        return new BitIterable(Long.SIZE, value);
    }

    @Override
    public Iterator<Boolean> iterator() {
        return new Iterator<Boolean>() {
            private int curBit = 0;
            private long curValue = value;

            @Override
            public boolean hasNext() {
                return curBit < bitCnt;
            }

            @Override
            public Boolean next() {
                boolean val = (curValue & 1) == 1;
                curBit++;
                curValue >>= 1;
                return val;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };
    }
}
