package ru.advantum.csptm.jep.proto;

/**
 * Created by kaganov on 05/12/2016.
 * Часто используемые флаги.
 */
public enum CommonPacketFlag {

    /**
     * Тревожное событие.
     */
    ALARM,
    /**
     * Не писать пакет в БД.
     */
    NOT_WRITABLE,
    /**
     * Исторические данные.
     */
    HIST_DATA,
    /**
     * Ответ на команду.
     */
    COMMAND_REPLY,
    /**
     * Запрос голосовой связи.
     */
    CALL_REQUEST,
    /**
     * Текстовое сообщение.
     */
    TEXT_MESSAGE,
    /**
     * Идентификатор текстового сообщения.
     */
    TEXT_MESSAGE_ID

}
