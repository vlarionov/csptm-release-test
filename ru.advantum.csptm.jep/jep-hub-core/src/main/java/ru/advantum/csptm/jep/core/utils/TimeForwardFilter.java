package ru.advantum.csptm.jep.core.utils;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Envelope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.proto.UnitPacket;
import ru.advantum.rabbitmq.consumer.TypedConsumer;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by kaganov on 28/04/2017.
 * Фильтр, который пропускает координаты только по возрастанию времени
 */
public class TimeForwardFilter implements TypedConsumer<UnitPacket> {

    private static final Logger LOGGER = LoggerFactory.getLogger(TimeForwardFilter.class);

    // TODO: задавать через конструктор
    private static final Duration DEF_MAX_FUTURE_PACKET_TIME = Duration.of(1, ChronoUnit.HOURS);

    private final TypedConsumer<UnitPacket> delegate;
    private final ConcurrentMap<Long, Instant> lastPacketTimes;

    public TimeForwardFilter(TypedConsumer<UnitPacket> delegate) {
        this.delegate = delegate;
        this.lastPacketTimes = new ConcurrentHashMap<>();
    }

    @Override
    public void consume(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, UnitPacket message) {
        if (message.getEventTime().isAfter(Instant.now().plus(DEF_MAX_FUTURE_PACKET_TIME))) {
            // координата из будущего, отбрасываем
            // TODO: метрика/логирование
            return;
        }

        Instant lastTime = lastPacketTimes.computeIfAbsent(message.getTrId(), trId -> message.getEventTime());
        // вторую координату с одним и тем же временем тоже не пропускаем
        if (!message.getEventTime().isAfter(lastTime)) {
            // координата из прошлого, отбрасываем
            // TODO: метрика/логирование
            return;
        }

        delegate.consume(consumerTag, envelope, properties, message);
    }
}
