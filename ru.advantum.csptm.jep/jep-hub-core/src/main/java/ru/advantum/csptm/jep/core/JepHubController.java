package ru.advantum.csptm.jep.core;

import ru.advantum.csptm.jep.proto.UnitPacket;
import ru.advantum.csptm.jep.proto.UnitSession;

import java.util.Optional;

/**
 * Created by kaganov on 28/11/2016.
 */
public interface JepHubController {
    // TODO: javadoc

    int getHubId();

    void start(JepHub hub) throws Exception;

    void stop();

    void sendPackets(UnitPacket... packets) throws PacketTransferException;

    void sendSessionComplete(UnitSession session) throws PacketTransferException;

    boolean isReady();

    Optional<Long> translateId(String unitNum);

    void sendSessionStart(UnitSession session) throws PacketTransferException;
}
