package ru.advantum.csptm.jep.core.rmq;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.rabbitmq.config.RmqConnectionConfig;
import ru.advantum.rabbitmq.config.RmqConsumerConfig;
import ru.advantum.rabbitmq.config.RmqDestinationConfig;

/**
 * Created by kaganov on 12/12/2016.
 */
@Root(name = "rmq-service")
public class JepRmqServiceConfig {

    @Element(name = "rmq-connection")
    public RmqConnectionConfig connection;

    @Element(name = "rmq-consumer", required = false)
    public RmqConsumerConfig consumer;

    @Element(name = "rmq-destination", required = false)
    public RmqDestinationConfig destination;

    public JepRmqServiceConfig() {
        connection = new RmqConnectionConfig();
        consumer = null;
        destination = null;
    }
}
