package ru.advantum.csptm.jep.core.rmq;

import com.rabbitmq.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.core.JepHub;
import ru.advantum.csptm.jep.core.JepHubController;
import ru.advantum.csptm.jep.core.PacketTransferException;
import ru.advantum.csptm.jep.proto.*;
import ru.advantum.rabbitmq.consumer.RmqJsonTalker;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by kaganov on 29/11/2016.
 */
public class JepRmqHubController implements JepHubController, BlockedListener, RecoveryListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(JepRmqHubController.class);

    private final JepRmqHubControllerConfig config;
    private final AtomicBoolean connectionBlocked = new AtomicBoolean(false);
    private final ConcurrentMap<String, Long> unitBounds = new ConcurrentHashMap<>();

    private JepHub hub;
    private RmqJsonTalker talker;
    private RmqBufferSender<UnitPacket, UnitPacketsBundle> unitPacketSender;
    private RmqBufferSender<UnitSession, UnitSessionBundle> unitSessionSender;

    public JepRmqHubController(JepRmqHubControllerConfig config) {
        this.config = Objects.requireNonNull(config);
    }

    @Override
    public int getHubId() {
        return config.hubId;
    }

    @Override
    public void start(JepHub hub) throws IOException, TimeoutException, InterruptedException {
        this.hub = hub;

        String configRoutingKey = config.coordsDestination.routingKey;
        String routingKey;
        if (configRoutingKey != null && !configRoutingKey.isEmpty()) {
            routingKey = configRoutingKey;
        } else {
            routingKey = Integer.toString(getHubId());
        }
        LOGGER.info("Effective routing key for coords [{}]", routingKey);

        Connection connection = config.connection.newConnection();

        connection.addBlockedListener(this);
        if (connection instanceof RecoverableConnection) {
            ((RecoverableConnection) connection).addRecoveryListener(this);
        }

        talker = RmqJsonTalker.newBuilder()
                .setConsumerConfig(true, true, config.consumer)
                .addConsumer(CommonMessageType.UNIT_BOUNDS.name(), UnitBoundChange[].class,
                             (consumerTag, envelope, properties, message) -> consumeUnitBoundChanges(message))
                .addConsumer(CommonMessageType.COMMAND.name(), CommandPacket[].class,
                             (consumerTag, envelope, properties, message) -> consumeCommandPacket(message))
                .connect(connection);

        talker.basicPublish(config.managerDestination.exchange,
                            config.managerDestination.routingKey,
                            CommonMessageType.HUB_START.name(),
                            new HubStart(getHubId()));

        unitPacketSender = new RmqBufferSender<>(talker,
                                                 up -> UnitPacketsBundle.of(getHubId(), up),
                                                 config.coordsDestination.exchange,
                                                 routingKey,
                                                 CommonMessageType.COORDS.name());

        unitSessionSender = new RmqBufferSender<>(talker,
                                                  up -> UnitSessionBundle.of(getHubId(), up),
                                                  config.coordsDestination.exchange,
                                                  routingKey,
                                                  CommonMessageType.UNIT_SESSION.name());

        this.hub.start();
    }

    @Override
    public void stop() {
        try {
            talker.gentlyStopConsumer(10, 2);
            talker.close();
        } catch (Exception e) {
            LOGGER.error("stop", e);
        }
    }

    private void consumeCommandPacket(CommandPacket[] message) {
        for (CommandPacket cmd : message) {
            try {
                hub.sendCommand(cmd);
            } catch (Exception e) {
                LOGGER.error("Consume command {}", cmd, e);
            }
        }
    }

    private void consumeUnitBoundChanges(UnitBoundChange[] message) {
        for (UnitBoundChange unitBoundChange : message) {
            final Long prevUnitId;
            final Long newUnitId;

            if (unitBoundChange.getChangeType() == UnitBoundChange.ChangeType.ADDED) {
                prevUnitId = unitBounds.put(unitBoundChange.getUnitNum(), unitBoundChange.getUnitId());
                newUnitId = unitBoundChange.getUnitId();
            } else if (unitBoundChange.getChangeType() == UnitBoundChange.ChangeType.DELETED) {
                prevUnitId = unitBounds.remove(unitBoundChange.getUnitNum());
                newUnitId = null;
            } else {
                prevUnitId = null;
                newUnitId = null;
                LOGGER.info("Unknown change type [{}]", unitBoundChange.getChangeType());
            }

            LOGGER.info("Unit id for unit num changed; unit num [{}], old id [{}], new id [{}]",
                        unitBoundChange.getUnitNum(), prevUnitId, newUnitId);
            if (prevUnitId != null && !prevUnitId.equals(newUnitId)) {
                LOGGER.info("Hub notify [{}]", prevUnitId);
                hub.unitChanged(prevUnitId);
            }
        }
    }

    @Override
    public synchronized void sendPackets(UnitPacket... packets) throws PacketTransferException {
        try {
            for (UnitPacket packet : packets) {
                unitPacketSender.basicPublish(packet);
            }
        } catch (InterruptedException e) {
            throw new PacketTransferException(e);
        }
    }

    @Override
    public void sendSessionComplete(UnitSession session) throws PacketTransferException {
        try {
            unitSessionSender.basicPublish(session);
        } catch (InterruptedException e) {
            throw new PacketTransferException(e);
        }
    }

    @Override
    public boolean isReady() {
        return talker.isWritable() && !connectionBlocked.get() && !unitBounds.isEmpty();
    }

    @Override
    public Optional<Long> translateId(String unitNum) {
        return Optional.ofNullable(unitBounds.get(unitNum));
    }

    @Override
    public void sendSessionStart(UnitSession session) throws PacketTransferException {
        try {
            unitSessionSender.basicPublish(session);
        } catch (InterruptedException e) {
            throw new PacketTransferException(e);
        }
    }

    @Override
    public void handleBlocked(String reason) {
        LOGGER.error("Connection blocked [{}]", reason);
        connectionBlocked.set(true);
        hub.pause();
    }

    @Override
    public void handleUnblocked() {
        LOGGER.info("Connection unblocked");
        connectionBlocked.set(false);
        hub.resume();
    }

    @Override
    public void handleRecovery(Recoverable recoverable) {
        connectionBlocked.set(false);
    }

    @Override
    public void handleRecoveryStarted(Recoverable recoverable) {

    }
}
