package ru.advantum.csptm.jep.core;

/**
 * Created by kaganov on 29/11/2016.
 */
public class PacketTransferException extends Exception {

    public PacketTransferException(String message) {
        super(message);
    }

    public PacketTransferException(Throwable cause) {
        super(cause);
    }
}
