package ru.advantum.csptm.jep.core;

import ru.advantum.csptm.jep.proto.CommandPacket;

/**
 * Created by kaganov on 28/11/2016.
 */
public interface JepHub {
    // TODO: javadoc

    void start();

    void stop();

    void unitChanged(long unitId);

    void sendCommand(CommandPacket cmd) throws Exception;

    void pause();

    void resume();
}
