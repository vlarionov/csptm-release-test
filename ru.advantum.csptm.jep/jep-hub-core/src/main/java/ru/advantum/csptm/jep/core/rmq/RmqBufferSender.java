package ru.advantum.csptm.jep.core.rmq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.rabbitmq.consumer.RmqJsonTalker;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.Function;

public class RmqBufferSender<T, R> {

    private static final int DELAY_SECONDS = 1;
    private static final int QUEUE_SIZE = 5000;
    private static final int MAX_SEND_LIST_SIZE = 500;

    private static final Logger LOGGER = LoggerFactory.getLogger(RmqBufferSender.class);

    private final RmqJsonTalker rmqTalker;
    private final BlockingQueue<T> itemsQueue;
    private final Function<List<T>, R> listAggregator;

    private final String exchange;
    private final String routingKey;
    private final String type;

    public RmqBufferSender(RmqJsonTalker rmqTalker, Function<List<T>, R> listAggregator, String exchange,
                           String routingKey, String type) {
        this.rmqTalker = rmqTalker;
        this.listAggregator = listAggregator;
        this.exchange = exchange;
        this.routingKey = routingKey;
        this.type = type;
        this.itemsQueue = new ArrayBlockingQueue<>(QUEUE_SIZE);

        ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();
        ses.scheduleAtFixedRate(this::sendQueue, DELAY_SECONDS, DELAY_SECONDS, TimeUnit.SECONDS);
    }

    private void sendQueue() {
        try {
            List<T> itemsToSend = new ArrayList<>(itemsQueue.size());
            itemsQueue.drainTo(itemsToSend);

            if (itemsToSend.isEmpty()) {
                return;
            }

            while (!itemsToSend.isEmpty()) {
                int maxInd = Math.min(MAX_SEND_LIST_SIZE, itemsToSend.size());
                List<T> chunkToSend = itemsToSend.subList(0, maxInd);
                R aggMessage = listAggregator.apply(chunkToSend);

                rmqTalker.basicPublish(exchange, routingKey, type, aggMessage);
                LOGGER.info("Aggregated {} messages", chunkToSend.size());

                itemsToSend = itemsToSend.subList(maxInd, itemsToSend.size());
            }
        } catch (Throwable ex) {
            LOGGER.error("sendQueue", ex);
        }
    }

    public void basicPublish(T message) throws InterruptedException {
        itemsQueue.offer(message, 1, TimeUnit.HOURS);
    }
}
