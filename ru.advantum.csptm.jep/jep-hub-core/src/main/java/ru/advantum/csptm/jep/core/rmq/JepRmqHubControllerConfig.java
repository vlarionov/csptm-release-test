package ru.advantum.csptm.jep.core.rmq;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.rabbitmq.config.RmqConnectionConfig;
import ru.advantum.rabbitmq.config.RmqConsumerConfig;
import ru.advantum.rabbitmq.config.RmqDestinationConfig;

/**
 * Created by kaganov on 02/12/2016.
 */
@Root(name = "rmq-hub-controller")
public class JepRmqHubControllerConfig {

    @Attribute(name = "hubId")
    public int hubId;

    @Element(name = "rmq-connection")
    public RmqConnectionConfig connection;

    @Element(name = "rmq-consumer")
    public RmqConsumerConfig consumer;

    @Element(name = "rmq-coords")
    public RmqDestinationConfig coordsDestination;

    @Element(name = "rmq-manager")
    public RmqDestinationConfig managerDestination;

    public JepRmqHubControllerConfig() {
        hubId = 0;
        connection = new RmqConnectionConfig();
        consumer = new RmqConsumerConfig();
        coordsDestination = new RmqDestinationConfig();
        managerDestination = new RmqDestinationConfig();
    }
}
