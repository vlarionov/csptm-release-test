package ru.advantum.csptm.jep.spring.rmq;

import org.springframework.amqp.core.Message;

import java.lang.reflect.Type;

/**
 * Created by kaganov on 02/06/2017.
 */
public interface MessageTypeResolver {

    Type resolveJavaType(Message message);

    String resolveAmqpType(Type messageType);
}
