package ru.advantum.csptm.jep.spring.rmq;

import com.fatboyindustrial.gsonjavatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.rabbitmq.config.RmqDestinationConfig;

@Configuration
@ConditionalOnBean(ConcurrentJepRmqServiceConfig.class)
public class RabbitConfigurerSupport {

    private final ConcurrentJepRmqServiceConfig config;

    @Autowired
    public RabbitConfigurerSupport(ConcurrentJepRmqServiceConfig config) {
        this.config = config;
    }

    @Bean
    public ConcurrentJepRmqServiceConfig concurrentJepRmqServiceConfig() {
        return config;
    }

    @Bean
    public JepConnectionFactory jepConnectionFactory() {
        return new JepConnectionFactory(config);
    }

    @Bean
    public Gson gson() {
        return Converters.registerAll(new GsonBuilder()).create();
    }

    @Bean
    @ConditionalOnMissingBean(MessageTypeResolver.class)
    public MessageTypeResolver defaultMessageTypeResolver() {
        return new MethodArgumentTypeResolver();
    }

    @Bean
    public MessageConverter messageConverter(MessageTypeResolver typeResolver) {
        return new GsonMessageConverter(gson(), typeResolver);
    }

    @Bean
    public ConnectionFactory connectionFactory() throws Exception {
        return new CachingConnectionFactory(jepConnectionFactory());
    }

    @Bean
    public RabbitTemplate rabbitTemplate(MessageConverter messageConverter) throws Exception {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory());
        rabbitTemplate.setMessageConverter(messageConverter);

        RmqDestinationConfig destinationConfig = config.destination;
        if (destinationConfig == null) {
            return rabbitTemplate;
        }

        if (destinationConfig.exchange != null) {
            rabbitTemplate.setExchange(destinationConfig.exchange);
        }

        if (destinationConfig.routingKey != null) {
            rabbitTemplate.setRoutingKey(destinationConfig.routingKey);
        }

        return rabbitTemplate;
    }

    @Bean
    public JepListenerContainerFactory jepListenerContainerFactory(MessageConverter messageConverter) throws Exception {
        return new JepListenerContainerFactory(connectionFactory(), messageConverter, config);
    }
}
