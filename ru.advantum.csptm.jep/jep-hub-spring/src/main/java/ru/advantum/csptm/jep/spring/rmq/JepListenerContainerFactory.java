package ru.advantum.csptm.jep.spring.rmq;

import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.support.converter.MessageConverter;

public class JepListenerContainerFactory extends SimpleRabbitListenerContainerFactory {
    public JepListenerContainerFactory(
            ConnectionFactory connectionFactory,
            MessageConverter messageConverter,
            ConcurrentJepRmqServiceConfig config
    ) {
        setMessageConverter(messageConverter);
        setConnectionFactory(connectionFactory);
        setConcurrentConsumers(config.concurrency);
        setMaxConcurrentConsumers(config.maxConcurrency);
        setAcknowledgeMode(config.acknowledgeMode);
    }
}
