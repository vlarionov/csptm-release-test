package ru.advantum.csptm.jep.spring.rmq;

import com.google.gson.Gson;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.support.converter.AbstractMessageConverter;
import org.springframework.amqp.support.converter.MessageConversionException;

import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.charset.UnsupportedCharsetException;

public class GsonMessageConverter extends AbstractMessageConverter {

    private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

    private final Gson gson;
    private final MessageTypeResolver resolver;

    public GsonMessageConverter(Gson gson, MessageTypeResolver resolver) {
        this.gson = gson;
        this.resolver = resolver;
    }

    @Override
    protected Message createMessage(Object object, MessageProperties messageProperties) {
        messageProperties.setContentEncoding(DEFAULT_CHARSET.name());
        messageProperties.setContentType(MessageProperties.CONTENT_TYPE_JSON);
        messageProperties.setType(resolver.resolveAmqpType(object.getClass()));
        return new Message(
                gson.toJson(object).getBytes(),
                messageProperties
        );
    }

    @Override
    public Object fromMessage(Message message) throws MessageConversionException {
        MessageProperties properties = message.getMessageProperties();
        String contentEncoding = properties.getContentEncoding();

        Charset charset = DEFAULT_CHARSET;
        if (contentEncoding != null) {
            try {
                charset = Charset.forName(contentEncoding);
            } catch (UnsupportedCharsetException ignored) {
            }
        }

        Type type = resolver.resolveJavaType(message);
        String json = new String(message.getBody(), charset);

        if (String.class == type) {
            return json;
        } else {
            return gson.fromJson(json, type);
        }
    }
}