package ru.advantum.csptm.jep.spring.rmq;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.springframework.amqp.core.AcknowledgeMode;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

@Root(name = "rmq-service")
public class ConcurrentJepRmqServiceConfig extends JepRmqServiceConfig {
    @Attribute(required = false)
    public int concurrency = 1;

    @Attribute(required = false)
    public int maxConcurrency = 1;

    @Attribute(required = false)
    public AcknowledgeMode acknowledgeMode = AcknowledgeMode.AUTO;
}
