package ru.advantum.csptm.jep.netty;

import com.codahale.metrics.*;
import com.google.gson.Gson;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelPipeline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.core.JepHub;
import ru.advantum.csptm.jep.core.JepHubController;
import ru.advantum.csptm.jep.core.PacketTransferException;
import ru.advantum.csptm.jep.proto.CommandPacket;
import ru.advantum.csptm.jep.proto.CommonPacketFlag;
import ru.advantum.csptm.jep.proto.UnitPacket;
import ru.advantum.csptm.jep.proto.UnitSession;

import java.nio.ByteOrder;
import java.time.Instant;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import static com.codahale.metrics.MetricRegistry.name;

/**
 * Created by kaganov on 29/11/2016.
 */
public abstract class JepNettyHub implements JepHub {

    private static final String JMX_METRICS_DOMAIN = "ru.advantum.csptm.jep.hub";

    private static final MetricRegistry metrics = SharedMetricRegistries.getOrCreate("hub");
    private static final Logger LOGGER = LoggerFactory.getLogger(JepNettyHub.class);
    private static final Gson GSON = new Gson();

    private final BootstrapConfig bootstrapConfig;
    private final JepHubController controller;
    private final ConcurrentMap<Long, JepNettyConnectionHandler> unitHandlers = new ConcurrentHashMap<>();

    private final Counter packagesCounter = metrics.counter(name(JepNettyHub.class, "packages", "sent"));
    private final Meter packagesMeter = metrics.meter(name(JepNettyHub.class, "packages", "meter"));
    private final Counter connectionCounter = metrics.counter(name(JepNettyHub.class, "connection", "count"));
    private final Meter connectionOpenMeter = metrics.meter(name(JepNettyHub.class, "connection", "rate", "open"));
    private final Meter connectionCloseMeter = metrics.meter(name(JepNettyHub.class, "connection", "rate", "close"));
    private final Meter unknownUnitIdMeter = metrics.meter(name(JepNettyHub.class, "unknownUnitId", "rate"));

    public JepNettyHub(BootstrapConfig bootstrapConfig, JepHubController controller) {
        this.bootstrapConfig = bootstrapConfig;
        this.controller = controller;
    }

    // TODO: возможно выделить интерфейс

    protected abstract ByteOrder getByteOrder();

    protected abstract void tunePipeline(ChannelPipeline channelPipeline);

    @Override
    public void start() {
        Slf4jReporter.forRegistry(metrics)
                .outputTo(LOGGER)
                .withLoggingLevel(Slf4jReporter.LoggingLevel.INFO)
                .build()
                .start(5, TimeUnit.SECONDS);

        JmxReporter.forRegistry(metrics).inDomain(JMX_METRICS_DOMAIN).build().start();

        NettyServerInitializer serverCreator = new NettyServerInitializer(bootstrapConfig,
                                                                          getByteOrder(),
                                                                          this::tunePipeline);
        serverCreator.start();
    }

    @Override
    public void stop() {
        controller.stop();
        pause();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ignored) {
        }
    }

    @Override
    public void unitChanged(long unitId) {
        Optional.ofNullable(unitHandlers.get(unitId))
                .map(JepNettyConnectionHandler::getChannel)
                .ifPresent(Channel::close);
    }

    @Override
    public void pause() {
        unitHandlers.values().forEach(h -> h.getChannel().close());
    }

    @Override
    public void resume() {

    }

    @Override
    public void sendCommand(CommandPacket cmd) throws PacketTransferException {
        final JepNettyConnectionHandler unitHandler = unitHandlers.get(cmd.getUnitId());
        if (unitHandler != null) {
            unitHandler.onCommandForSend(cmd);
        } else {
            LOGGER.debug("Command listener for {} not found", cmd.getUnitId());
            sendUnitNotConnected(cmd);
        }
    }

    private void sendUnitNotConnected(CommandPacket cmd) throws PacketTransferException {
        sendPackets(UnitPacket.newBuilder()
                            .setUnitId(cmd.getUnitId())
                            .setEventTime(Instant.now())
                            .addFlag(CommonPacketFlag.COMMAND_REPLY)
                            .addAttribute(CommonPacketFlag.COMMAND_REPLY.name(),
                                          // обратная совместимость с существующим решением
                                          GSON.toJson(new CommandPacket(cmd.getUnitId(),
                                                                        cmd.getCmdId(),
                                                                        Instant.now(),
                                                                        "UNIT_NOT_FOUND",
                                                                        Collections.singletonMap("error", "-1"))))
                            .build());
    }

    public void sendPackets(UnitPacket... packets) throws PacketTransferException {
        packagesCounter.inc(packets.length);
        packagesMeter.mark(packets.length);
        controller.sendPackets(packets);
    }

    public void sendSessionComplete(long unitId,
                                    Instant connectTime,
                                    Instant disconnectTime,
                                    long totalPacketCnt,
                                    long invalidPacketCnt,
                                    long histPacketCnt,
                                    Integer sessionId) throws PacketTransferException {
        controller.sendSessionComplete(new UnitSession(controller.getHubId(),
                                                       unitId,
                                                       connectTime,
                                                       disconnectTime,
                                                       totalPacketCnt,
                                                       invalidPacketCnt,
                                                       histPacketCnt,
                                                       sessionId));
    }

    public void sendSessionStart(long unitId,
                                 Instant connectTime,
                                 Integer sessionId) throws PacketTransferException {
        controller.sendSessionStart(new UnitSession(controller.getHubId(),
                                                    unitId,
                                                    connectTime,
                                                    sessionId));
    }

    // TODO: уточнить s2s
    public void registerUnitHandler(String unitNum,
                                    JepNettyConnectionHandler<?> handler,
                                    boolean s2sModePermit) throws UnitIdNotFoundException {
        Long unitId;
        Optional<Long> translatedId = controller.translateId(unitNum);
        if (translatedId.isPresent()) {
            unitId = translatedId.get();
        } else {
            unknownUnitIdMeter.mark();
            throw new UnitIdNotFoundException(unitNum);
        }

        if (!s2sModePermit && handler.getUnitId() != null && !handler.getUnitId().equals(unitId)) {
            throw new UnitIdNotFoundException(String.format("Unit id changed from %d to %d", handler.getUnitId(), unitId));
        }

        handler.setUnitId(unitId);

        final JepNettyConnectionHandler oldHandler = unitHandlers.put(unitId, handler);

        // здесь именно равенство ссылок. может быть равно, если несколько раз вызывают init на одном и том же handler'е
        if (handler != oldHandler && oldHandler != null) {
            LOGGER.info("Old channel [{}] for unitId [{}] will be closed", oldHandler.getChannel(), unitId);
            oldHandler.getChannel().close();
        }
    }

    public boolean isReady() {
        return controller.isReady();
    }

    public void channelOpened(@SuppressWarnings("unused") JepNettyConnectionHandler<?> handler) {
        connectionCounter.inc();
        connectionOpenMeter.mark();
    }

    public void channelClosed(JepNettyConnectionHandler<?> handler) {
        connectionCounter.dec();
        connectionCloseMeter.mark();
        if (handler.getUnitId() != null) {
            unitHandlers.remove(handler.getUnitId(), handler);
        }
    }
}
