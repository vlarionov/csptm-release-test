package ru.advantum.csptm.jep.netty;

/**
 * Created by kaganov on 29/11/2016.
 */
public class UnitIdNotFoundException extends Exception {

    UnitIdNotFoundException(String unitNum) {
        super(unitNum);
    }

}
