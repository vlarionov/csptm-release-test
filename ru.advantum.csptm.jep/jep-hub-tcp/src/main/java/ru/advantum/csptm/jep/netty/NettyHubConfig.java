package ru.advantum.csptm.jep.netty;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.csptm.jep.core.rmq.JepRmqHubControllerConfig;

/**
 * Created by kaganov on 08/12/2016.
 */
@Root(name = "hub")
public class NettyHubConfig {

    @Element(name = "controller")
    public final JepRmqHubControllerConfig controllerConfig;

    @Element(name = "tcp")
    public final NettyTcpConfig tcpConfig;

    public NettyHubConfig() {
        controllerConfig = new JepRmqHubControllerConfig();
        tcpConfig = new NettyTcpConfig();
    }
}
