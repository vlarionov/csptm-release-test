package ru.advantum.csptm.jep.netty;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.buffer.HeapChannelBufferFactory;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.jboss.netty.handler.timeout.IdleStateHandler;
import org.jboss.netty.util.HashedWheelTimer;

import java.net.InetSocketAddress;
import java.nio.ByteOrder;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * Created by kaganov on 29/11/2016.
 */
public class NettyServerInitializer {

    private static final HashedWheelTimer TIMER = new HashedWheelTimer();

    private final BootstrapConfig config;
    private final ByteOrder byteOrder;
    private final Consumer<ChannelPipeline> pipelineTuner;

    public NettyServerInitializer(BootstrapConfig config, ByteOrder byteOrder,
                                  Consumer<ChannelPipeline> pipelineTuner) {
        this.config = config;
        this.byteOrder = byteOrder;
        this.pipelineTuner = pipelineTuner;
    }

    private void setupBootstrap(ServerBootstrap bootstrap) {
        bootstrap.setOption("reuseAddress", true);
        bootstrap.setOption("tcpNoDelay", config.isTcpNoDelay());
        bootstrap.setOption("keepAlive", config.isKeepAlive());

        // Options for its children
        bootstrap.setOption("child.tcpNoDelay", config.isTcpNoDelay());
        bootstrap.setOption("child.keepAlive", config.isKeepAlive());
        bootstrap.setOption("child.reuseAddress", true);

        // buffers
        bootstrap.setOption("receiveBufferSize", config.getReceiveBufferSize());
        bootstrap.setOption("sendBufferSize", config.getSendBufferSize());

        bootstrap.setOption("writeBufferHighWaterMark", config.getWriteBufferHighWaterMark());

        bootstrap.setOption("localAddress", new InetSocketAddress(config.getHost(), config.getPort()));

        bootstrap.setOption("bufferFactory", new HeapChannelBufferFactory(byteOrder));
        bootstrap.setOption("child.bufferFactory", new HeapChannelBufferFactory(byteOrder));
    }

    public void start() {
        int workerCount = Runtime.getRuntime().availableProcessors();

        ExecutorService bossExecutor = Executors.newSingleThreadExecutor();
        ExecutorService workerExecutor = Executors.newCachedThreadPool();
        ChannelFactory channelFactory = new NioServerSocketChannelFactory(bossExecutor, workerExecutor, workerCount);
        ServerBootstrap bootstrap = new ServerBootstrap(channelFactory);

        setupBootstrap(bootstrap);

        bootstrap.setPipelineFactory(() -> {
            ChannelPipeline pipeline = Channels.pipeline();
            IdleStateHandler stateHandler = new IdleStateHandler(TIMER,
                                                                 config.getIdleTimeout(),
                                                                 config.getIdleTimeout(),
                                                                 config.getIdleTimeout(),
                                                                 TimeUnit.MILLISECONDS);
            pipeline.addFirst("writeTimeoutHandler",
                              stateHandler);
            pipelineTuner.accept(pipeline);

            return pipeline;
        });

        bootstrap.bind();
    }
}
