package ru.advantum.csptm.jep.netty;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by kaganov on 02/12/2016.
 */
@Root(name = "tcp")
public class NettyTcpConfig implements BootstrapConfig {

    @Attribute
    public final String host;

    @Attribute
    public final int port;

    @Attribute(required = false)
    public final long idleTimeout;

    @Attribute(required = false)
    public final int sendBufferSize;

    @Attribute(required = false)
    public final int receiveBufferSize;

    @Attribute(required = false)
    public final int writeBufferHighWaterMark;

    @Attribute(required = false)
    public final boolean tcpNoDelay;

    @Attribute(required = false)
    public final long connectTimeoutMillis;

    @Attribute(required = false)
    public final boolean keepAlive;

    public NettyTcpConfig() {
        host = null;
        port = 0;
        idleTimeout = 60000L;
        sendBufferSize = 1048576;
        receiveBufferSize = 1048576;
        writeBufferHighWaterMark = 10 * 64 * 1024;
        tcpNoDelay = true;
        connectTimeoutMillis = 10000;
        keepAlive = true;
    }

    @Override
    public String getHost() {
        return host;
    }

    @Override
    public int getPort() {
        return port;
    }

    @Override
    public long getIdleTimeout() {
        return idleTimeout;
    }

    @Override
    public int getSendBufferSize() {
        return sendBufferSize;
    }

    @Override
    public int getReceiveBufferSize() {
        return receiveBufferSize;
    }

    @Override
    public int getWriteBufferHighWaterMark() {
        return writeBufferHighWaterMark;
    }

    @Override
    public boolean isTcpNoDelay() {
        return tcpNoDelay;
    }

    @Override
    public long getConnectTimeoutMillis() {
        return connectTimeoutMillis;
    }

    @Override
    public boolean isKeepAlive() {
        return keepAlive;
    }
}
