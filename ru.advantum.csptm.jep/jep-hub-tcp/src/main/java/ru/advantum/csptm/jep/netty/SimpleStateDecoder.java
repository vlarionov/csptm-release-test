package ru.advantum.csptm.jep.netty;

import javolution.io.Struct;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.handler.codec.replay.ReplayingDecoder;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Created by kaganov on 13/12/2016.
 */
public abstract class SimpleStateDecoder extends ReplayingDecoder<SimpleStateDecoder.STATE> {

    public enum STATE {

        HANDSHAKE, DATA
    }

    public SimpleStateDecoder() {
        super(STATE.HANDSHAKE, false);
    }

    protected void bufferCheck(ChannelBuffer buffer) throws Exception {
        if (!buffer.readable()) {
            throw new IOException(String.format("Buffer not readable: rindex: %s, windex: %s, readable: %s",
                                                buffer.readerIndex(), buffer.writerIndex(), buffer.readable()));
        }
    }

    protected final <T extends Struct> T readData(ChannelBuffer buffer, Class<T> bs) throws Exception {
        T packet = bs.newInstance();
        int bsSize = packet.size();

        ByteBuffer bb = ByteBuffer.allocate(bsSize);
        bb.order(buffer.order());

        byte buff[] = new byte[bsSize];

        buffer.readBytes(buff);

        bb.put(buff);
        packet.setByteBuffer(bb.asReadOnlyBuffer().order(buffer.order()), 0);

        return packet;
    }

    protected final byte[] readTrailer(ChannelBuffer buffer, int size) {
        byte buff[] = new byte[size];
        buffer.readBytes(buff);
        return buff;
    }
}
