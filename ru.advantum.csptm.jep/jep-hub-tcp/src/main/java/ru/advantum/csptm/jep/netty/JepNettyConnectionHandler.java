package ru.advantum.csptm.jep.netty;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.*;
import org.jboss.netty.handler.timeout.IdleState;
import org.jboss.netty.handler.timeout.IdleStateAwareChannelHandler;
import org.jboss.netty.handler.timeout.IdleStateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.core.PacketTransferException;
import ru.advantum.csptm.jep.proto.CommandPacket;
import ru.advantum.csptm.jep.proto.UnitPacket;

import java.io.IOException;
import java.time.Instant;

/**
 * Created by kaganov on 29/11/2016.
 */
public abstract class JepNettyConnectionHandler<HUB extends JepNettyHub> extends IdleStateAwareChannelHandler {

    public static final Logger LOGGER = LoggerFactory.getLogger(JepNettyConnectionHandler.class);

    private final HUB hub;

    private Long unitId;
    private ChannelHandlerContext globalCtx;

    protected JepNettyConnectionHandler(HUB hub) {
        this.hub = hub;
    }

    protected final HUB getHub() {
        return hub;
    }

    public void setUnitId(long unitId) {
        this.unitId = unitId;
    }

    public Long getUnitId() {
        return unitId;
    }

    protected final void initUnitId(String unitNum, boolean s2sModePermit) throws UnitIdNotFoundException {
        getHub().registerUnitHandler(unitNum, this, s2sModePermit);
    }

    protected final void initUnitId(String unitNum) throws UnitIdNotFoundException {
        initUnitId(unitNum, false);
    }

    private void sendToUnit(ChannelHandlerContext ctx, ChannelBuffer buff) {
        Channel channel = ctx.getChannel();
        ChannelEvent sendingEvent = new DownstreamMessageEvent(channel, Channels.future(channel),
                                                               buff, channel.getRemoteAddress());
        ctx.sendDownstream(sendingEvent);
    }

    public void sendToUnit(byte[] data) {
        if (globalCtx != null) {
            sendToUnit(globalCtx, ChannelBuffers.wrappedBuffer(data));
        } else {
            throw new IllegalStateException("Channel is not opened, global context is null");
        }
    }

    public void sendUnitPackets(UnitPacket... packets) throws PacketTransferException {
        hub.sendPackets(packets);
    }

    public void sendSessionComplete(Instant connectTime,
                                    Instant disconnectTime,
                                    long totalPacketCnt,
                                    long invalidPacketCnt,
                                    long histPacketCnt) throws PacketTransferException {
        if (unitId != null) {
            hub.sendSessionComplete(unitId,
                                    connectTime,
                                    disconnectTime,
                                    totalPacketCnt,
                                    invalidPacketCnt,
                                    histPacketCnt,
                                    globalCtx.getChannel().getId());
        }
    }

    // TODO: сохранять уже это в самом Handler'е
    public void sendSessionStart(Instant connectTime) throws PacketTransferException {
        if (unitId != null) {
            LOGGER.debug("Send SessionStart unitId: {} sessionId {}", unitId, globalCtx.getChannel().getId());
            hub.sendSessionStart(unitId, connectTime, globalCtx.getChannel().getId());
        } else {
            LOGGER.debug("Can't send SessionStart unitId is null");
        }
    }

    public Channel getChannel() {
        return globalCtx.getChannel();
    }

    public void onCommandForSend(CommandPacket cmd) {

    }

    @Override
    public void channelIdle(ChannelHandlerContext ctx, IdleStateEvent e) throws Exception {
        if (e.getState() == IdleState.ALL_IDLE) {
            LOGGER.info(String.format("IDLE(ALL) %s. Close channel.", ctx.getChannel()));
            ctx.getChannel().close();
        }

        super.channelIdle(ctx, e);
    }

    @Override
    public void channelOpen(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        LOGGER.info("OPEN {}", ctx.getChannel());
        hub.channelOpened(this);
        globalCtx = ctx;
        if (!hub.isReady()) {
            LOGGER.error("Hub is not ready, disconnect.");
            ctx.getChannel().close();
            return;
        }

        super.channelOpen(ctx, e);
    }

    @Override
    public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        LOGGER.info("CLOSE [{}] {}", unitId, ctx.getChannel());
        hub.channelClosed(this);
        super.channelClosed(ctx, e);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) {  //throws Exception {
        try {
            if (e.getCause() instanceof IOException) {
                LOGGER.info("Unit [{}] {} throws IOException: {}", unitId, ctx.getChannel(), e.getCause().getMessage());
            } else if (e.getCause() instanceof PacketTransferException) {
                LOGGER.error("Packet transfer", e.getCause());
            } else if (e.getCause() instanceof UnitIdNotFoundException) {
                LOGGER.info("Unit ID not found [{}] ", e.getCause().getMessage());
            } else {
                LOGGER.error("ChannelException", e.getCause());
            }

            LOGGER.debug("Channel {} will closed. ExceptionCaught", ctx.getChannel());
            ctx.getChannel().close();
        } catch (Throwable t) {
            LOGGER.debug("Throwable exceptionCaught", t);
        }
    }
}
