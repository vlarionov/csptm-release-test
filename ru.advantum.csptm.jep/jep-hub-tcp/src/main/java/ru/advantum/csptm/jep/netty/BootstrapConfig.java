package ru.advantum.csptm.jep.netty;

/**
 * Created by kaganov on 29/11/2016.
 */
public interface BootstrapConfig {

    String getHost();

    int getPort();

    long getIdleTimeout();

    int getSendBufferSize();

    int getReceiveBufferSize();

    int getWriteBufferHighWaterMark();

    boolean isTcpNoDelay();

    long getConnectTimeoutMillis();

    boolean isKeepAlive();
}
