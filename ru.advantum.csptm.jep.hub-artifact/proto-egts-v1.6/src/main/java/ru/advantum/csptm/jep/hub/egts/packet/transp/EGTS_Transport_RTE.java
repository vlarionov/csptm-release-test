package ru.advantum.csptm.jep.hub.egts.packet.transp;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSBase;

/**
 *
 * @author Ostap Gerasimchuk, Oleg Mitsay
 * @since May 29, 2013 12:53:39 PM
 */
public class EGTS_Transport_RTE extends EGTSBase {

    public static class RTE {
        public final int PRA;
        public final int RCA;
        public final int TTL;

        public RTE(int PRA, int RCA, int TTL) {
            this.PRA = PRA;
            this.RCA = RCA;
            this.TTL = TTL;
        }

    }

    public final Unsigned16 PRA = new Unsigned16(16);
    public final Unsigned16 RCA = new Unsigned16(16);
    private final BitField TTL = new BitField(8);

    public int getTTL() {
        return TTL.intValue();
    }

    public void setTTL(int TTL) {
        this.TTL.set(TTL);
    }
}
