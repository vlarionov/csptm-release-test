package ru.advantum.csptm.jep.hub.egts.packet;

import ru.advantum.csptm.jep.hub.egts.packet.subrec.*;

/**
 *
 * @author Ostap Gerasimchuk, Oleg Mitsay
 * @since May 29, 2013 3:37:37 PM
 */
public class EGTS_SERVICE_SUBRECORD extends EGTSBase {

    public static enum Type {
        UNSUPPORTED(-1, null),
        EGTS_SR_RECORD_RESPONSE(0, ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_RECORD_RESPONSE.class),
        EGTS_SR_TERM_IDENTITY(1, ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_TERM_IDENTITY.class),
        EGTS_SR_MODULE_DATA(2, EGTS_SR_MODULE_DATA.class),
        EGTS_SR_DISPATCHER_IDENTITY(5, ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_DISPATCHER_IDENTITY.class),
        EGTS_SR_AUTH_PARAMS(6, ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_AUTH_PARAMS.class),
        EGTS_SR_AUTH_INFO(7,EGTS_SR_AUTH_INFO.class),
        EGTS_SR_RESULT_CODE(9, ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_RESULT_CODE.class),
        EGTS_SR_POS_DATA(16, EGTS_SR_POS_DATA.class),
        EGTS_SR_EXT_POS_DATA(17, EGTS_SR_EXT_POS_DATA.class),
        EGTS_SR_AD_SENSORS_DATA(18, EGTS_SR_AD_SENSORS_DATA.class),
        EGTS_SR_COUNTERS_DATA(19, EGTS_SR_COUNTERS_DATA.class),
        // В протоколе 1.6 у этой подзаписи код 20. В протоколе 1.7 -- 21. В протоколе 1.6 нет подзаписи 21
        EGTS_SR_STATE_DATA(21, EGTS_SR_STATE_DATA.class),
        EGTS_SR_ABS_AN_SENS_DATA(24, EGTS_SR_ABS_AN_SENS_DATA.class),
        EGTS_SR_LIQUID_LEVEL_SENSOR(27, EGTS_SR_LIQUID_LEVEL_SENSOR.class),
        EGTS_SR_CAN_DATA(29, EGTS_SR_CAN_DATA.class);

        private final int id;
        private final Class<? extends EGTSServiceSubRecord> packetClass;
        private Type(int id, Class<? extends EGTSServiceSubRecord> packetClass) {
            this.id = id;
            this.packetClass = packetClass;
        }

        public static Type resolve(int id) {
            for (Type t : values()) {
                if (t.id == id) {
                    return t;
                }
            }
            return UNSUPPORTED;
        }

        public int getId() {
            return id;
        }

        public Class<? extends EGTSServiceSubRecord> getPacketClass() {
            return packetClass;
        }

    }

    public static final int SIZE = new EGTS_SERVICE_SUBRECORD().size();
    private final Unsigned8 SRT = new Unsigned8(8);
    public final Unsigned16 SRL = new Unsigned16(16);
    public EGTS_SERVICE_SUBRECORD() {
        super();
    }

    public EGTS_SERVICE_SUBRECORD(Type SRT, int SRL) {
        this();
        this.SRT.set((short) SRT.getId());
        this.SRL.set(SRL);
    }

    public int getSRT() {
        return SRT.get();
    }

    public void setSRT(int SRT) {
        this.SRT.set((short) SRT);
    }
}
