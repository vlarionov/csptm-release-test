package ru.advantum.csptm.jep.hub.egts.packet.subrec;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSServiceSubRecord;

import java.io.IOException;
import java.io.InputStream;
/**
 *
 * @author Ostap Gerasimchuk, Oleg Mitsay
 * @since Apr 1, 2014 9:50:51 PM
 */
public class EGTS_SR_DISPATCHER_IDENTITY extends EGTSServiceSubRecord {

    public final Unsigned8 DT = new Unsigned8();
    public final Unsigned32 DID = new Unsigned32();

    public String DSCR = null;

    @Override
    public int readOptional(int subrecordLength, InputStream in) throws IOException {
        int len = 0;

        if (subrecordLength > size()) {
            final byte raw[] = new byte[subrecordLength - size()];
            len += in.read(raw);
            DSCR = new String(raw, "WINDOWS-1251");
        }

        return len;
    }

}
