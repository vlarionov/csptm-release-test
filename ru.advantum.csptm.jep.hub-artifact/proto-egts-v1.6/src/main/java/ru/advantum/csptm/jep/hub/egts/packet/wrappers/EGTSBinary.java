package ru.advantum.csptm.jep.hub.egts.packet.wrappers;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSBase;

/**
 * @author mitsay
 * @since 16.10.2015.
 */
public class EGTSBinary extends EGTSBase {
    public BitField VAL = null;

    public EGTSBinary(int length ) {
        this.VAL = new BitField(length);
    }

    public int get() {
        return VAL.intValue();
    }

    public String asString() {
        return Integer.toString(get());
    }

}
