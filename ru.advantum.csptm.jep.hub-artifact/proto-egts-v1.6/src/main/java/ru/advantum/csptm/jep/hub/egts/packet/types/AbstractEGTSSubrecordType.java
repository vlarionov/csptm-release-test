package ru.advantum.csptm.jep.hub.egts.packet.types;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSServiceSubRecord;
import ru.advantum.csptm.jep.hub.egts.packet.EGTS_SERVICE;
import ru.advantum.csptm.jep.hub.egts.packet.EGTS_SERVICE_SUBRECORD;

/**
 * @author mitsay on 15.10.2015.
 */
public abstract class AbstractEGTSSubrecordType {

    public EGTS_SERVICE_SUBRECORD.Type type;
    public final EGTS_SERVICE.OID oid;
    public final EGTS_SERVICE.TM tm;

    public AbstractEGTSSubrecordType(EGTS_SERVICE.OID oid, EGTS_SERVICE.TM tm) {
        this.tm = tm;
        this.oid = oid;
    }

    public abstract EGTSServiceSubRecord getRecord();
}
