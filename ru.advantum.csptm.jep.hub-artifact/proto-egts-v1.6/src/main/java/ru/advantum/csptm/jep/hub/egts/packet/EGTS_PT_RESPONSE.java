package ru.advantum.csptm.jep.hub.egts.packet;

/**
 *
 * @author Ostap Gerasimchuk, Oleg Mitsay
 * @since May 29, 2013 1:40:49 PM
 */
public class EGTS_PT_RESPONSE extends EGTSServiceSubRecord {

    public final Unsigned16 RPID = new Unsigned16();
    public final Unsigned8 PR = new Unsigned8();
}
