package ru.advantum.csptm.jep.hub.egts.packet;

import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author Ostap Gerasimchuk, Oleg Mitsay
 * @since May 29, 2013 4:28:49 PM
 */
public class EGTSServiceSubRecord extends EGTSBase {

    protected int packetNum;

    public EGTSBase[] toFrames() {
        return new EGTSBase[]{this};
    }

    public void setPacketNum(int packetNum) {
        this.packetNum = packetNum;
    }

    public int readOptional(int subrecordLength, InputStream in) throws IOException {
        return 0;
    }

    public boolean isValid() {
        return true;
    }
}
