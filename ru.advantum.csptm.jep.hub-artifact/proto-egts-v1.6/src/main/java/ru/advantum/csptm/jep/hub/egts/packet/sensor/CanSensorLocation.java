package ru.advantum.csptm.jep.hub.egts.packet.sensor;

/**
 *
 * @author kaganov
 */
public class CanSensorLocation {

    private final int paramGroup;
    private final int dataType;

    public CanSensorLocation(int paramGroup, int dataType) {
        this.paramGroup = paramGroup;
        this.dataType = dataType;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + this.paramGroup;
        hash = 83 * hash + this.dataType;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CanSensorLocation other = (CanSensorLocation) obj;
        if (this.paramGroup != other.paramGroup) {
            return false;
        }
        return this.dataType == other.dataType;
    }

}
