package ru.advantum.csptm.jep.hub.egts.packet.types;

import ru.advantum.csptm.jep.hub.egts.packet.EGTS_SERVICE;
import ru.advantum.csptm.jep.hub.egts.packet.EGTS_SERVICE_SUBRECORD;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_AD_SENSORS_DATA;

/**
 * @author mitsay
 * @since  20.10.2015.
 */
public class EGTSAdSensors extends AbstractEGTSSubrecordType {
    private final EGTS_SR_AD_SENSORS_DATA data;

    public EGTSAdSensors(EGTS_SR_AD_SENSORS_DATA data, EGTS_SERVICE.OID oid, EGTS_SERVICE.TM tm) {
        super(oid, tm);
        type = EGTS_SERVICE_SUBRECORD.Type.EGTS_SR_AD_SENSORS_DATA;
        this.data = data;
    }

    public EGTS_SR_AD_SENSORS_DATA getRecord() {
        return data;
    }
}
