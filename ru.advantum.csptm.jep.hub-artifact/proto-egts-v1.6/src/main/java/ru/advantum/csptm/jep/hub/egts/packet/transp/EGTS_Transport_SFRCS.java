package ru.advantum.csptm.jep.hub.egts.packet.transp;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSBase;

/**
 *
 * @author Ostap Gerasimchuk, Oleg Mitsay
 * @since May 29, 2013 1:11:44 PM
 */
public class EGTS_Transport_SFRCS extends EGTSBase {

    public final Unsigned16 SFRCS = new Unsigned16();

}
