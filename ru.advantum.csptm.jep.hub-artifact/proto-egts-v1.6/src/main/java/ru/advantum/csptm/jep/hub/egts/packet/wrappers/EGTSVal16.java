package ru.advantum.csptm.jep.hub.egts.packet.wrappers;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSBase;

/**
 *
 * @author kaganov
 */
public class EGTSVal16 extends EGTSBase {

    public final Unsigned16 VAL = new Unsigned16();

    public int get() {
        return VAL.get();
    }

    public String asString() {
        return Integer.toString(get());
    }
}
