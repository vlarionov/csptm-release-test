package ru.advantum.csptm.jep.hub.egts.packet.subrec;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSServiceSubRecord;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensor;
import ru.advantum.csptm.jep.hub.egts.packet.EGTSBase;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensorType;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensorsHolder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
/**
 *
 * @author Ostap Gerasimchuk, Oleg Mitsay
 * @since May 29, 2013 3:58:48 PM
 */
public class EGTS_SR_POS_DATA extends EGTSServiceSubRecord implements EGTSSensorsHolder {

    public final Unsigned32 NTM = new Unsigned32(32);
    public final Unsigned32 LAT = new Unsigned32(32);
    public final Unsigned32 LONG = new Unsigned32(32);
    // FLG
    public final BitField VLD = new BitField(1); // СКАУТ!!!!
    public final BitField FIX = new BitField(1);
    public final BitField CS = new BitField(1);
    public final BitField BB = new BitField(1);
    public final BitField MV = new BitField(1);
    public final BitField LAHS = new BitField(1);
    public final BitField LOHS = new BitField(1);
    public final BitField ALTE = new BitField(1); // СКАУТ!!!!;

    private final BitField SPD = new BitField(14);
    public final BitField ALTS = new BitField(1);
    public final BitField DIRH = new BitField(1);
    private final BitField DIR = new BitField(8);
    private final BitField ODM = new BitField(8 * 3);
    private final BitField DIN = new BitField(8);
    private final BitField SRC = new BitField(8);

    private EGTS_SR_POS_DATA_ALT ALT = null;

    public int SRCDh = 0;
    public int SRCDl = 0;


    public void setVLD(boolean VLD) {
        this.VLD.set(VLD?1:0);
    }
    public boolean isVLD() {
        return this.VLD.intValue()==1;
    }
    public void setFIX(boolean FIX) {
        this.FIX.set(FIX?1:0);
    }
    public boolean getFIX() {
        return this.FIX.intValue()==1;
    }
    public void setCS(boolean CS) {
        this.CS.set(CS?1:0);
    }
    public boolean getCS() {
        return this.CS.intValue()==1;
    }
    public void setBB(boolean BB) {
        this.BB.set(BB?1:0);
    }
    public boolean getBB() {
        return this.BB.intValue()==1;
    }
    public void setMV(boolean MV) {
        this.MV.set(MV?1:0);
    }
    public boolean getMV() {
        return this.MV.intValue()==1;
    }
    public void setLAHS(boolean LAHS) {
        this.LAHS.set(LAHS?1:0);
    }
    public boolean isLAHS() {
        return this.LAHS.intValue()==1;
    }
    public void setLOHS(boolean LOHS) {
        this.LOHS.set(LOHS?1:0);
    }
    public boolean isLOHS() {
        return this.LOHS.intValue()==1;
    }
    public void setALTE(boolean ALTE) {
        this.ALTE.set(ALTE?1:0);
    }
    public boolean isALTE() {
        return this.ALTE.intValue()==1;
    }
    public void setALTS(boolean ALTS) {
        this.ALTS.set(ALTS?1:0);
    }
    public boolean getALTS() {
        return this.ALTS.intValue()==1;
    }
    public void setDIRH(boolean DIRH) {
        this.DIRH.set(DIRH?1:0);
    }
    public boolean getDIRH() {
        return this.DIRH.intValue()==1;
    }



    public int getSPD() {
        return SPD.intValue();
    }

    public long getODM() {
        return ODM.longValue();
    }

    public int getDIR() {
        return DIR.intValue();
    }


    public int getDIN() {
        return DIN.intValue();
    }


    public int getSRC() {
        return SRC.intValue();
    }

    public void setSPD(int SPD) {
        this.SPD.set(SPD);
    }

    public void setDIR(int DIR) {
        this.DIR.set(DIR);
    }

    public void setODM(long ODM) {
        this.ODM.set(ODM);
    }

    public void setDIN(int DIN) {
        this.DIN.set(DIN);
    }

    public void setSRC(int SRC) {
        this.SRC.set(SRC);
    }

    @Override
    public EGTSBase[] toFrames() {
        if (ALT != null) {
            return new EGTSBase[]{this, ALT};
        }
        return super.toFrames();
    }

    public EGTS_SR_POS_DATA_ALT getALT() {
        return ALT;
    }

    public void setALT(EGTS_SR_POS_DATA_ALT ALT) {
        this.ALT = ALT;
    }

    @Override
    public int readOptional(int subrecordLength, InputStream in) throws IOException {
        int len = 0;

        if (isALTE()) {
            ALT = new EGTS_SR_POS_DATA_ALT();
            len += ALT.read(in);
        }

        if (size() + len < subrecordLength) {
            len += 2;
            // дочитываем SRCD
            SRCDh = in.read();
            SRCDl = in.read();
        }

        return len;
    }

    @Override
    public List<EGTSSensor> getSensors() {
        return EGTSSensor.byteToBitSensors(EGTSSensorType.DIGITAL_INPUT,
                                           0,
                                           (byte) DIN.intValue());
    }

}
