package ru.advantum.csptm.jep.hub.egts.packet.wrappers;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSBase;

/**
 * @author mitsay
 * @since 17.10.2015.
 */
public class EGTSBoolean extends EGTSBase {
    public BitField VAL = new BitField(1);

    public boolean TRUE() {
        return VAL.intValue() == 1;
    }

    public boolean FALSE() {
        return VAL.intValue() == 0;
    }

    public void set( boolean val ) {
        this.VAL.set(val ? 1 : 0);
    }

    public int getInt() {
        return VAL.intValue();
    }

}
