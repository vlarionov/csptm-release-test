package ru.advantum.csptm.jep.hub.egts.packet.subrec;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSBase;
/**
 *
 * @author Ostap Gerasimchuk, Oleg Mitsay
 * @since May 29, 2013 4:16:41 PM
 */

public class EGTS_SR_POS_DATA_ALT extends EGTSBase {
    private final BitField ALT = new BitField(24);

    public long getALT() {
        return ALT.longValue();
    }

    public void setALT(long ALT) {
        this.ALT.set(ALT);
    }

}
