package ru.advantum.csptm.jep.hub.egts.packet.transp;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSBase;

/**
 *
 * @author Ostap Gerasimchuk, Oleg Mitsay
 * @since May 29, 2013 12:34:52 PM
 */
public class EGTS_Transport extends EGTSBase {
    public static final int EGTS_PT_RESPONSE = 0x00;
    public static final int EGTS_PT_APPDATA = 0x01;

    private final  BitField PRV = new BitField(8);
    private final  BitField SKID = new BitField(8);
    private final  BitField PR = new BitField(2);
    private final  BitField CMP = new BitField(1);
    private final  BitField ENA = new BitField(2);
    private final  BitField RTE = new BitField(1);
    private final  BitField PRF = new BitField(2);
    private final  BitField HL = new BitField(8);
    private final  BitField HE = new BitField(8);
    private final  BitField FDL = new BitField(16);
    private final  BitField PID = new BitField(16);
    private final  BitField PT = new BitField(8);



    public int getSKID() {
        return SKID.intValue();
    }

    public int getPR() {
        return PR.intValue();
    }

    public int getCMP() {
        return CMP.intValue();
    }

    public int getENA() {
        return ENA.intValue();
    }

    public int getRTE() {
        return RTE.intValue();
    }

    public int getPRF() {
        return PRF.intValue();
    }

    public int getHL() {
        return HL.intValue();
    }

    public int getHE() {
        return HE.intValue();
    }

    public int getFDL() {
        return FDL.intValue();
    }

    public int getPID() {
        return PID.intValue();
    }

    public short getPT() {
        return PT.shortValue();
    }

    public int getPRV() {

        return PRV.intValue();
    }

    public void setPRV(int PRV) {
        this.PRV.set(PRV);
    }

    public void setSKID(int SKID) {
        this.SKID.set(SKID);
    }

    public void setPR(int PR) {
        this.PR.set(PR);
    }

    public void setCMP(int CMP) {
        this.CMP.set(CMP);
    }

    public void setENA(int ENA) {
        this.ENA.set(ENA);
    }

    public void setRTE(int RTE) {
        this.RTE.set(RTE);
    }

    public void setPRF(int PRF) {
        this.PRF.set(PRF);
    }

    public void setHL(int HL) {
        this.HL.set(HL);
    }

    public void setHE(int HE) {
        this.HE.set(HE);
    }

    public void setFDL(int FDL) {
        this.FDL.set(FDL);
    }

    public void setPID(int PID) {
        this.PID.set(PID);
    }

    public void setPT(short PT) {
        this.PT.set(PT);
    }

}
