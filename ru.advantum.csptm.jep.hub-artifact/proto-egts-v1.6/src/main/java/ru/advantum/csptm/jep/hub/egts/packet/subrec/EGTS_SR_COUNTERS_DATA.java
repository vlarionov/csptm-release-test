package ru.advantum.csptm.jep.hub.egts.packet.subrec;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSServiceSubRecord;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensor;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensorType;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensorsHolder;
import ru.advantum.csptm.jep.hub.egts.packet.wrappers.EGTSVal24;
import ru.advantum.csptm.jep.proto.util.BitIterable;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author kaganov
 */
public class EGTS_SR_COUNTERS_DATA extends EGTSServiceSubRecord implements EGTSSensorsHolder {

    public final Unsigned8 CFE = new Unsigned8();

    private final List<EGTSSensor> sensors = new ArrayList<>();

    @Override
    public List<EGTSSensor> getSensors() {
        return sensors;
    }

    @Override
    public int readOptional(int subrecordLength, InputStream in) throws IOException {
        int len = 0;

        int dioNum = 0;
        for (Boolean b : new BitIterable(Byte.SIZE, CFE.get())) {
            if (b) {
                final EGTSVal24 v = new EGTSVal24();
                len += v.read(in);
                sensors.add(new EGTSSensor(EGTSSensorType.COUNTER,
                                           packetNum * 8 + dioNum,
                                           v.getVAL()));
            }
            dioNum++;
        }

        return len;
    }
}
