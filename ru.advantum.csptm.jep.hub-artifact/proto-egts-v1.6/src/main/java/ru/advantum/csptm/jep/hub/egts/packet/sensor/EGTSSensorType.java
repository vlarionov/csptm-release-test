package ru.advantum.csptm.jep.hub.egts.packet.sensor;

/**
 *
 * @author kaganov
 */
public enum EGTSSensorType {

    EMBEDDED,
    DIGITAL_INPUT,
    DIGITAL_OUTPUT,
    ANALOG_SENSOR,
    COUNTER,
    LOOPIN,
    LIQUID_LEVEL,
    PASSENGERS_COUNTER,
    CAN;

    public static EGTSSensorType lookupByName(String name) {
        for(EGTSSensorType t: EGTSSensorType.values()) {
            if ( t.name().toUpperCase().equals(name.toUpperCase())) {
                return t;
            }
        }
        return null;
    }


    }
