package ru.advantum.csptm.jep.hub.egts.packet.wrappers;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSBase;

/**
 *
 * @author kaganov
 */
public class EGTSVal8 extends EGTSBase {

    public final Unsigned8 VAL = new Unsigned8();

    public int get() {
        return VAL.get();
    }

    public String asString() {
        return Integer.toString(get());
    }
}
