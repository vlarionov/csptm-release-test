package ru.advantum.csptm.jep.hub.egts.packet.subrec;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSServiceSubRecord;
import ru.advantum.csptm.jep.hub.egts.handler.EraResponseCodes;

/**
 *
 * @author Ostap Gerasimchuk, Oleg Mitsay
 * @since May 29, 2013 1:37:52 PM
 */
public class EGTS_SR_RECORD_RESPONSE extends EGTSServiceSubRecord {
    public final Unsigned16 CRN = new Unsigned16();
    public final Unsigned8 RST = new Unsigned8();

    public EGTS_SR_RECORD_RESPONSE() {
        super();
    }

    public EGTS_SR_RECORD_RESPONSE(int CRN, EraResponseCodes RST) {
        this();
        this.CRN.set(CRN);
        this.RST.set(RST.getValue());
    }


}
