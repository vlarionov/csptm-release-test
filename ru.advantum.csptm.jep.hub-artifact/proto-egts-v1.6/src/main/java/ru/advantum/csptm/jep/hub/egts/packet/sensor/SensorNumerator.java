package ru.advantum.csptm.jep.hub.egts.packet.sensor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author kaganov
 */
public class SensorNumerator {

    public enum NumeratorType {

        DEFAULT,
        CAN,
        CUSTOM
    }

    public static class Settings {

        public final short offset;
        public final short maxNum;

        public Settings(short offset, short maxNum) {
            this.offset = offset;
            this.maxNum = maxNum;
        }
    }

    private static final short DEFAULT_OFFSET_STEP = 32;

    protected static final Logger log = LoggerFactory.getLogger(SensorNumerator.class);

    private static ConcurrentHashMap<EGTSSensorType, Settings> settings = new ConcurrentHashMap<>();

    static {
        setNumeratorType(NumeratorType.DEFAULT);
    }

    public static void setNumeratorType(NumeratorType type) {
        setNumeratorType(type, null);
    }

    @SuppressWarnings("PointlessArithmeticExpression") // так нагляднее
    public static void setNumeratorType(NumeratorType type, Map<EGTSSensorType, Settings> settingsMap) {
        final ConcurrentHashMap<EGTSSensorType, Settings> newSettings = new ConcurrentHashMap<>();

        switch (type) {
            case DEFAULT:
                newDefaultSettings(newSettings);
                break;
            case CAN:
                newSettings.put(EGTSSensorType.EMBEDDED,
                        new Settings((short) (0 * DEFAULT_OFFSET_STEP), DEFAULT_OFFSET_STEP));
                newSettings.put(EGTSSensorType.DIGITAL_INPUT,
                        new Settings((short) (1 * DEFAULT_OFFSET_STEP), DEFAULT_OFFSET_STEP));
                newSettings.put(EGTSSensorType.CAN, new Settings((short) 100, (short) 155));

                break;
            case CUSTOM:
                if (settingsMap!= null && settingsMap.size() > 0) {
                    for(EGTSSensorType t: EGTSSensorType.values() ) {
                        Settings settings = settingsMap.get(t);
                        if ( settings != null ) {
                            newSettings.put(t, settings);
                        }
                    }
                } else {
                    newDefaultSettings(newSettings);
                }
                break;
            default:
                log.error("Invalid NumeratorType {}", type);
                throw new AssertionError();
        }

        settings = newSettings;
    }

    private static void newDefaultSettings(ConcurrentHashMap<EGTSSensorType, Settings> newSettings) {
        newSettings.put(EGTSSensorType.EMBEDDED,
                new Settings((short) (0 * DEFAULT_OFFSET_STEP), DEFAULT_OFFSET_STEP));
        newSettings.put(EGTSSensorType.DIGITAL_INPUT,
                new Settings((short) (1 * DEFAULT_OFFSET_STEP), DEFAULT_OFFSET_STEP));
        newSettings.put(EGTSSensorType.DIGITAL_OUTPUT,
                new Settings((short) (2 * DEFAULT_OFFSET_STEP), DEFAULT_OFFSET_STEP));
        newSettings.put(EGTSSensorType.ANALOG_SENSOR,
                new Settings((short) (3 * DEFAULT_OFFSET_STEP), DEFAULT_OFFSET_STEP));
        newSettings.put(EGTSSensorType.COUNTER,
                new Settings((short) (4 * DEFAULT_OFFSET_STEP), DEFAULT_OFFSET_STEP));
        newSettings.put(EGTSSensorType.LOOPIN,
                new Settings((short) (5 * DEFAULT_OFFSET_STEP), DEFAULT_OFFSET_STEP));
        newSettings.put(EGTSSensorType.LIQUID_LEVEL,
                new Settings((short) (6 * DEFAULT_OFFSET_STEP), DEFAULT_OFFSET_STEP));
        newSettings.put(EGTSSensorType.PASSENGERS_COUNTER,
                new Settings((short) (7 * DEFAULT_OFFSET_STEP), DEFAULT_OFFSET_STEP));
    }

    public static Map<Integer, BigDecimal> extractSensors(EGTSSensorsHolder packet) {
        Map<Integer, BigDecimal> sensors = new HashMap<>();

        Settings setting;
        for (EGTSSensor egtsSensor : packet.getSensors()) {
            setting = settings.get(egtsSensor.getType());
            if (setting == null) {
                log.warn("No settings for type {}", egtsSensor.getType());
                continue;
            }
            if (egtsSensor.getIdSensor() >= 0 && egtsSensor.getIdSensor() < setting.maxNum) {
                final int translatedId = setting.offset + egtsSensor.getIdSensor();

                log.debug("Adding sensor {} {} {}", packet.getClass(), translatedId, egtsSensor.getValue());
                sensors.put(translatedId, BigDecimal.valueOf(egtsSensor.getValue()));
            } else {
                log.warn("Invalid sensor number {} {}", egtsSensor.getType(), egtsSensor.getIdSensor());
            }
        }

        return sensors;
    }
}
