package ru.advantum.csptm.jep.hub.egts.packet.wrappers;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSBase;

/**
 *
 * @author kaganov
 */
public class EGTSVal64 extends EGTSBase {

    public final Signed64 VAL = new Signed64();

    public long get() {
        return VAL.get();
    }

    public String asString() {
        return Long.toString(get());
    }
}
