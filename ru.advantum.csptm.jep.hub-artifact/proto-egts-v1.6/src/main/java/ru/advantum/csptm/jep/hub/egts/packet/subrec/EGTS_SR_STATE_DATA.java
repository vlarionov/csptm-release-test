package ru.advantum.csptm.jep.hub.egts.packet.subrec;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSServiceSubRecord;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensor;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensorType;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensorsHolder;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Ostap Gerasimchuk, Oleg Mitsay
 * @since Mar 26, 2014 5:26:29 PM
 */
public class EGTS_SR_STATE_DATA extends EGTSServiceSubRecord implements EGTSSensorsHolder {

    public final Unsigned8 ST = new Unsigned8();
    public final Unsigned8 MPSV = new Unsigned8();
    public final Unsigned8 BBV = new Unsigned8();
    public final Unsigned8 IBV = new Unsigned8();
    public final BitField BBU = new BitField (1);
    public final BitField IBU = new BitField (1);
    public final BitField NMS = new BitField (1);

    public final BitField R1 = new BitField (1);
    public final BitField R2 = new BitField (1);
    public final BitField R3 = new BitField (1);
    public final BitField R4 = new BitField (1);
    public final BitField R5 = new BitField (1);
    public boolean getBBU() {
        return BBU.byteValue() != 0;
    }

    public boolean getIBU() {
        return IBU.byteValue() != 0;
    }

    public boolean getNMS() {
        return NMS.byteValue() != 0;
    }

    public boolean getR1() {
        return R1.byteValue() != 0;
    }

    public boolean getR2() {
        return R2.byteValue() != 0;
    }

    public boolean getR3() {
        return R3.byteValue() != 0;
    }

    public boolean getR4() {
        return R4.byteValue() != 0;
    }

    public boolean getR5() {
        return R5.byteValue() != 0;
    }


    @Override
    public List<EGTSSensor> getSensors() {
        return Arrays.asList(new EGTSSensor(EGTSSensorType.EMBEDDED, EGTSSensor.EGTS_SENSOR_STATE_ST, ST.get()),
                             new EGTSSensor(EGTSSensorType.EMBEDDED, EGTSSensor.EGTS_SENSOR_STATE_MPSV, MPSV.get()),
                             new EGTSSensor(EGTSSensorType.EMBEDDED, EGTSSensor.EGTS_SENSOR_STATE_BBV, BBV.get()),
                             new EGTSSensor(EGTSSensorType.EMBEDDED, EGTSSensor.EGTS_SENSOR_STATE_IBV, IBV.get()),
                             new EGTSSensor(EGTSSensorType.EMBEDDED, EGTSSensor.EGTS_SENSOR_STATE_BBU, getBBU() ? 1 : 0),
                             new EGTSSensor(EGTSSensorType.EMBEDDED, EGTSSensor.EGTS_SENSOR_STATE_IBU, getIBU() ? 1 : 0),
                             new EGTSSensor(EGTSSensorType.EMBEDDED, EGTSSensor.EGTS_SENSOR_STATE_NMS, getNMS() ? 1 : 0)
        );
    }

}
