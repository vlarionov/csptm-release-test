package ru.advantum.csptm.jep.hub.egts.packet.subrec;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSBase;
import ru.advantum.csptm.jep.hub.egts.packet.EGTSServiceSubRecord;
import ru.advantum.csptm.jep.hub.egts.packet.EGTS_SERVICE;
import ru.advantum.csptm.jep.hub.egts.packet.wrappers.EGTSBinary;
import ru.advantum.csptm.jep.hub.egts.packet.wrappers.EGTSString;
import ru.advantum.tools.HEX;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
/**
 * @author mitsay
 * @since 30.10.15.
 */
public class EGTS_SR_AUTH_INFO extends EGTSServiceSubRecord {
    public EGTSString UNM = null;
    public EGTSBinary D1 = new EGTSBinary(8);
    public EGTSString UPSW = null;
    public EGTSBinary D2 = new EGTSBinary(8);

    public EGTS_SR_AUTH_INFO() {
        D1.VAL.set((short) 0);
        D2.VAL.set((short) 0);
    }

    @Override
    public EGTSBase[] toFrames() {
        List<EGTSBase> list = new ArrayList<>();
//        list.add(this);
        if (UNM == null) {
            UNM = new EGTSString(0);
        }
        list.add(UNM);
        list.add(D1);
        if (UPSW == null) {
            UPSW = new EGTSString(0);
        }
        list.add(UPSW);
        list.add(D2);
        return list.toArray(new EGTSBase[0]);
    }

    public String getUNM() {
        if (UNM != null ) {
            return new String(UNM.VAL.get());
        } else {
            return null;
        }
    }

    public String getUNM(Charset charset) {
        if (UNM != null ) {
            return new String(UNM.VAL.get(), charset);
        } else {
            return null;
        }
    }


    public void setUNM(String unm) {
        UNM = new EGTSString(unm.length());
        UNM.VAL.set(unm.getBytes());
    }

    public void setUNM(String unm, Charset charset) {
        UNM = new EGTSString(unm.length());
        UNM.VAL.set(unm.getBytes(charset));
    }

    public String getUPSW() {
        if (UPSW != null ) {
            return new String(UPSW.VAL.get());
        } else {
            return null;
        }
    }

    public String getUPSW(Charset charset) {
        if (UPSW != null ) {
            return new String(UPSW.VAL.get(), charset);
        } else {
            return null;
        }
    }
    public void setUPSW(String upsw) {
        UPSW = new EGTSString(upsw.length());
        UPSW.VAL.set(upsw.getBytes());
    }

    public void setUPSW(String upsw, Charset charset) {
        UPSW = new EGTSString(upsw.length());
        UPSW.VAL.set(upsw.getBytes(charset));
    }

    public void setUPSW(String upsw, Charset charset, EGTS_SERVICE.TM tm) throws NoSuchAlgorithmException {
        String toMD5 = "";
        if ( charset == null ) {
            toMD5 = (new String(upsw.getBytes())) + tm.value.get();
        } else {
            toMD5 = (new String(upsw.getBytes(charset))) + tm.value.get();
        }
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        String hashed = HEX.byteArrayToStringU(md5.digest(toMD5.getBytes())).replace(" ","");
        UPSW = new EGTSString(hashed.getBytes().length);
        UPSW.VAL.set(hashed.getBytes());
    }
}
