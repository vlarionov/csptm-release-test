package ru.advantum.csptm.jep.hub.egts.packet.transp;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSBase;

/**
 *
 * @author Ostap Gerasimchuk, Oleg Mitsay
 * @since May 29, 2013 1:09:17 PM
 */

public class EGTS_Transport_HCS extends EGTSBase {
    public final BitField HCS = new BitField(8);

}
