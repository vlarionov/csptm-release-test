package ru.advantum.csptm.jep.hub.egts.packet;

import javolution.io.Struct;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteOrder;

/**
 *
 * @author Ostap Gerasimchuk, Oleg Mitsay
 * @since May 29, 2013 12:35:14 PM
 */
public class EGTSBase extends Struct {

    public static final int SFRCS_POLYNOMIAL = 0x1021;

    @Override
    public ByteOrder byteOrder() {
        return ByteOrder.LITTLE_ENDIAN;
    }

    @Override
    public boolean isPacked() {
        return true;
    }

    public byte[] toByteArray() throws IOException {
        return toByteArray(this);
    }

    public static byte[] toByteArray(EGTSBase ... frame) throws IOException {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            for (EGTSBase frm : frame) {
                if (frm != null) {
                    frm.write(bos);
                }
            }
            bos.flush();
            return bos.toByteArray();
        }
    }

}
