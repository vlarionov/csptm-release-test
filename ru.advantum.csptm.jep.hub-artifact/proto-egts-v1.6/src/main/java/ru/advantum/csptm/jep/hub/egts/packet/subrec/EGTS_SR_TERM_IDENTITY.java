package ru.advantum.csptm.jep.hub.egts.packet.subrec;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSBase;
import ru.advantum.csptm.jep.hub.egts.packet.EGTSServiceSubRecord;
import ru.advantum.csptm.jep.hub.egts.packet.wrappers.EGTSBinary;
import ru.advantum.csptm.jep.hub.egts.packet.wrappers.EGTSString;
import ru.advantum.csptm.jep.hub.egts.packet.wrappers.EGTSVal16;
import ru.advantum.csptm.jep.hub.egts.packet.wrappers.EGTSVal8;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * EGTS_SR_TERM_IDENTITY of EGTS immplementation class.
 * Optional fields exists permanently (Javolution case), look to flags.
 * Tested only IMEI functionality!
 *
 * @author mitsay
 * @since 20.10.2015
 */
public class EGTS_SR_TERM_IDENTITY extends EGTSServiceSubRecord {

    public Unsigned32 TID = new Unsigned32();
    public BitField HDIDE = new BitField(1);
    public BitField IMEIE = new BitField(1);
    public BitField IMSIE = new BitField(1);
    public BitField LNGCE = new BitField(1);
    public BitField SSRA = new BitField(1);
    public BitField NIDE = new BitField(1);
    public BitField BSE = new BitField(1);
    public BitField MNE = new BitField(1);

    public EGTSVal16 HDID = new EGTSVal16();
    public EGTSString IMEI = new EGTSString(15);
    public EGTSString IMSI = new EGTSString(16);
    public EGTSString LNGC = new EGTSString(3);
    public EGTSBinary NID = new EGTSBinary(3);
    public EGTSVal8 BS = new EGTSVal8();
    public EGTSString MSISDN = new EGTSString(15);

    @Override
    public EGTSBase[] toFrames() {
        List<EGTSBase> list = new ArrayList<>();
        list.add(this);
        if ( isHDIDE()) {
            list.add(HDID);
        }
        if ( isIMEIE()) {
            list.add(IMEI);
        }
        if ( isIMSIE()) {
            list.add(IMSI);
        }
        if ( isLNGCE()) {
            list.add(LNGC);
        }
        if ( isNIDE()) {
            list.add(NID);
        }
        if ( isBSE()) {
            list.add(BS);
        }
        if(isMNE()) {
            list.add(MSISDN);
        }
        return list.toArray(new EGTSBase[0]);
    }

    @Override
    public int readOptional(int subrecordLength, InputStream in) throws IOException {
        int len = 0;
        if ( isHDIDE()) {
            len += HDID.read(in);
        }
        if ( isIMEIE()) {
            len += IMEI.read(in);
        }
        if ( isIMSIE()) {
            len += IMSI.read(in);
        }
        if ( isLNGCE()) {
            len += LNGC.read(in);
        }
        if ( isNIDE()) {
            len += NID.read(in);
        }
        if ( isBSE()) {
            len += BS.read(in);
        }
        if(isMNE()) {
            len += MSISDN.read(in);
        }
        return len;
    }


    public boolean isHDIDE() {
        return HDIDE.intValue() == 1;
    }

    public void setHDIDE(boolean HDIDE) {
        this.HDIDE.set(HDIDE ? 1 : 0);
    }

    public boolean isIMEIE() {
        return IMEIE.intValue() == 1;
    }

    public void setIMEIE(boolean IMEIE) {
        this.IMEIE.set(IMEIE ? 1 : 0);
    }

    public boolean isIMSIE() {
        return IMSIE.intValue() == 1;
    }

    public void setIMSIE(boolean IMSIE) {
        this.IMSIE.set(IMSIE ? 1 : 0);
    }

    public boolean isLNGCE() {
        return LNGCE.intValue() == 1;
    }

    public void setLNGCE(boolean LNGCE) {
        this.LNGCE.set(LNGCE ? 1 : 0);
    }

    public boolean isSSRA() {
        return SSRA.intValue() == 1;
    }

    public void setSSRA(boolean SSRA) {
        this.SSRA.set(SSRA ? 1 : 0);
    }

    public boolean isNIDE() {
        return NIDE.intValue() == 1;
    }

    public void setNIDE(boolean NIDE) {
        this.NIDE.set(NIDE ? 1 : 0);
    }

    public boolean isBSE() {
        return BSE.intValue() == 1;
    }

    public void setBSE(boolean BSE) {
        this.BSE.set(BSE ? 1 : 0);
    }

    public boolean isMNE() {
        return MNE.intValue() == 1;
    }

    public void setMNE(boolean MNE) {
        this.MNE.set(MNE ? 1 : 0);
    }

    public void setIMEI(String val) {
        IMEI.VAL.set(val.getBytes());
    }

    /**
     * It doesn't tested - no cases.
     */
    public void setIMSI(String val) {
        IMSI.VAL.set(val.getBytes());
    }

    /**
     * It doesn't tested - no cases.
     */
    public void setLNGC(String val) {
        LNGC.VAL.set(val.getBytes());
    }

    /**
     * It doesn't tested - no cases.
     */
    public void setNID(long val) {
        NID.VAL.set(val);
    }

    /**
     * It doesn't tested - no cases.
     */
    public void setMSISDN(String val) {
        MSISDN.VAL.set(val.getBytes());
    }

}
