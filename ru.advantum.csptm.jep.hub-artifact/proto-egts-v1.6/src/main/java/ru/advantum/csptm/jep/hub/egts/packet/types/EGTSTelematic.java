package ru.advantum.csptm.jep.hub.egts.packet.types;

import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_POS_DATA;
import ru.advantum.csptm.jep.hub.egts.packet.EGTS_SERVICE;
import ru.advantum.csptm.jep.hub.egts.packet.EGTS_SERVICE_SUBRECORD;

/**
 * @since mitsay on 15.10.2015.
 */
public class EGTSTelematic extends AbstractEGTSSubrecordType {

    public final EGTS_SR_POS_DATA pos;

    public EGTSTelematic(EGTS_SR_POS_DATA pos, EGTS_SERVICE.OID oid, EGTS_SERVICE.TM tm) {
        super(oid, tm);
        type = EGTS_SERVICE_SUBRECORD.Type.EGTS_SR_POS_DATA;
        this.pos = pos;
    }

    public EGTS_SR_POS_DATA getRecord() {
        return pos;
    }
}

