package ru.advantum.csptm.jep.hub.egts.packet.subrec;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSServiceSubRecord;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensor;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensorType;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensorsHolder;

import java.util.Collections;
import java.util.List;

/**
 *
 * @author kaganov
 */
public class EGTS_SR_ABS_AN_SENS_DATA extends EGTSServiceSubRecord implements EGTSSensorsHolder {

    public final Unsigned8 ASN = new Unsigned8();
    private final BitField ASV = new BitField(24);

    @Override
    public List<EGTSSensor> getSensors() {
        // у них нумерация с 1, а у нас с 0.
        return Collections.singletonList(new EGTSSensor(EGTSSensorType.ANALOG_SENSOR, (byte) (ASN.get() - 1), ASV.longValue()));
    }


    public long getASV() {
        return ASV.longValue();
    }

    public void setASV(long ASV) {
        this.ASV.set(ASV);
    }

}
