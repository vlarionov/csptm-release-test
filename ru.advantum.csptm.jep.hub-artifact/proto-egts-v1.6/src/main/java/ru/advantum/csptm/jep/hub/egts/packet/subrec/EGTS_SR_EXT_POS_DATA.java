package ru.advantum.csptm.jep.hub.egts.packet.subrec;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSServiceSubRecord;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensor;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensorType;
import ru.advantum.csptm.jep.hub.egts.packet.wrappers.EGTSVal16;
import ru.advantum.csptm.jep.hub.egts.packet.wrappers.EGTSVal8;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensorsHolder;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
/**
 * @author kaganov
 */
public class EGTS_SR_EXT_POS_DATA extends EGTSServiceSubRecord implements EGTSSensorsHolder {
    public boolean getVFE() {
        return VFE.intValue() == 1;
    }

    public void setVFE(boolean VFE) {
        this.VFE.set(VFE ? 1 : 0);
    }

    public boolean getHFE() {
        return HFE.intValue() == 1;
    }

    public void setHFE(boolean HFE) {
        this.HFE.set(HFE ? 1 : 0);
    }

    public boolean getPFE() {
        return PFE.intValue() == 1;
    }

    public void setPFE(boolean PFE) {
        this.PFE.set(PFE ? 1 : 0);
    }

    public boolean getSFE() {
        return SFE.intValue() == 1;
    }

    public void setSFE(boolean SFE) {
        this.SFE.set(SFE ? 1 : 0);
    }

    public boolean getNSFE() {
        return NSFE.intValue() == 1;
    }

    public void setNSFE(boolean NSFE) {
        this.NSFE.set(NSFE ? 1 : 0);
    }

    public final BitField VFE = new BitField(1);
    public final BitField HFE = new BitField(1);
    public final BitField PFE = new BitField(1);
    public final BitField SFE = new BitField(1);
    public final BitField NSFE = new BitField(1);
    private final BitField UNUSED = new BitField(3);

    private final List<EGTSSensor> sensors = new ArrayList<>();

    public void addSensor(EGTSSensor sensor) {
        sensors.add(sensor);
    }


    public short getUNUSED() {
        return UNUSED.shortValue();
    }

    public void setUNUSED(short UNUSED) {
        this.UNUSED.set(UNUSED);
    }

    @Override
    public List<EGTSSensor> getSensors() {
        return sensors;
    }

    @Override
    public int readOptional(int subrecordLength, InputStream in) throws IOException {
        int len = 0;

        if (getVFE()) {
            final EGTSVal16 v = new EGTSVal16();
            len += v.read(in);
            addSensor(new EGTSSensor(EGTSSensorType.EMBEDDED,
                                     EGTSSensor.EGTS_SENSOR_EXT_POS_VDOP,
                                     v.VAL.get()));
        }

        if (getHFE()) {
            final EGTSVal16 v = new EGTSVal16();
            len += v.read(in);
            addSensor(new EGTSSensor(EGTSSensorType.EMBEDDED,
                    EGTSSensor.EGTS_SENSOR_EXT_POS_HDOP,
                    v.VAL.get()));
        }

        if (getPFE()) {
            final EGTSVal16 v = new EGTSVal16();
            len += v.read(in);
            addSensor(new EGTSSensor(EGTSSensorType.EMBEDDED,
                    EGTSSensor.EGTS_SENSOR_EXT_POS_PDOP,
                    v.VAL.get()));
        }

        if (getSFE()) {
            final EGTSVal8 v = new EGTSVal8();
            len += v.read(in);
            addSensor(new EGTSSensor(EGTSSensorType.EMBEDDED,
                    EGTSSensor.EGTS_SENSOR_EXT_POS_SAT,
                    v.VAL.get()));
        }

        if (getNSFE()) {
            final EGTSVal16 v = new EGTSVal16();
            len += v.read(in);
            addSensor(new EGTSSensor(EGTSSensorType.EMBEDDED,
                    EGTSSensor.EGTS_SENSOR_EXT_POS_NS,
                    v.VAL.get()));
        }

        return len;
    }

}
