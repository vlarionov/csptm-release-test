package ru.advantum.csptm.jep.hub.egts.packet.subrec;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSBase;
/**
 *
 * @author kaganov
 */
public class EGTS_SR_CAN_DATA_ITEM extends EGTSBase {

    public final Unsigned8 paramGroup = new Unsigned8();
    public final Unsigned8 dataType = new Unsigned8();
    public final Unsigned8 dataLength = new Unsigned8();

}
