package ru.advantum.csptm.jep.hub.egts.packet.sensor;


import ru.advantum.csptm.jep.proto.util.BitIterable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kaganov
 */
public class EGTSSensor {

    public static final byte EGTS_SENSOR_STATE_ST = 0;
    public static final byte EGTS_SENSOR_STATE_MPSV = 1;
    public static final byte EGTS_SENSOR_STATE_BBV = 2;
    public static final byte EGTS_SENSOR_STATE_IBV = 3;
    public static final byte EGTS_SENSOR_STATE_BBU = 4;
    public static final byte EGTS_SENSOR_STATE_IBU = 5;
    public static final byte EGTS_SENSOR_STATE_NMS = 6;
    public static final byte EGTS_SENSOR_EXT_POS_VDOP = 7;
    public static final byte EGTS_SENSOR_EXT_POS_HDOP = 8;
    public static final byte EGTS_SENSOR_EXT_POS_PDOP = 9;
    public static final byte EGTS_SENSOR_EXT_POS_SAT = 10;
    public static final byte EGTS_SENSOR_EXT_POS_NS = 11;

    private final EGTSSensorType type;
    private final int idSensor;
    private final long value;

    public static List<EGTSSensor> byteToBitSensors(EGTSSensorType type, int idOffset, byte val) {
        List<EGTSSensor> sensors = new ArrayList<>();

        byte bitNum = 0;
        for (Boolean b : BitIterable.ofValue(val)) {
            sensors.add(new EGTSSensor(type, idOffset * 8 + bitNum, b ? 1 : 0));
            bitNum++;
        }

        return sensors;
    }

    public EGTSSensor(EGTSSensorType type, int idSensor, long value) {
        this.type = type;
        this.idSensor = idSensor;
        this.value = value;
    }

    public EGTSSensorType getType() {
        return type;
    }

    public int getIdSensor() {
        return idSensor;
    }

    public long getValue() {
        return value;
    }
}
