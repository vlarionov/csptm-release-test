package ru.advantum.csptm.jep.hub.egts.packet.subrec;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSServiceSubRecord;

/**
 * @author mitsay
 * @since 21.10.2015.
 */
public class EGTS_SR_RESULT_CODE extends EGTSServiceSubRecord {
    public final Unsigned8 RCD = new Unsigned8();
}
