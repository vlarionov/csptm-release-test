package ru.advantum.csptm.jep.hub.egts.packet.subrec;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSServiceSubRecord;

/**
 * @author mitsay
 * @since 30.10.15.
 */
public class EGTS_SR_AUTH_PARAMS extends EGTSServiceSubRecord {
    public BitField ENA = new BitField(2);
    public BitField PKEI = new BitField(1);
    public BitField ISLEI = new BitField(1);
    public BitField MSEI = new BitField(1);
    public BitField SSEI = new BitField(1);
    public BitField EXEI = new BitField(1);
    public BitField FILL = new BitField(1);

    public Unsigned16 PKE = new Unsigned16();
    public BitField PBK = new BitField(4096);
    public Unsigned16 ISL = new Unsigned16();
    public Unsigned16 MSZ = new Unsigned16();
    public BitField SS = new BitField(2040);
    public BitField D1 = new BitField(8);
    public BitField EXP = new BitField(2040);
    public BitField D2 = new BitField(8);

    public EGTS_SR_AUTH_PARAMS() {
    }

}
