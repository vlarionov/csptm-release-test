package ru.advantum.csptm.jep.hub.egts.handler;

/**
 *
 * @author Ostap Gerasimchuk, Oleg Mitsay
 * @since May 28, 2013 5:15:47 PM
 */
public enum EraResponseCodes {

    UNKNOWN(Byte.MIN_VALUE),
    PC_OK((short) 0),
    PC_UNS_PROTOCOL((short) 128),
    PC_INC_DATAFORM((short) 132),
    PC_HEADERCRC_ERROR((short) 137),
    PC_DATACRC_ERROR((short) 138),
    PC_INVDATALEN((short)139),
    PC_SRVC_UNKN((short) 150),
    PC_ID_NFOUND((short) 153),
    PC_ROUTE_NFOUND((short)140),
    PC_AUTH_DENIED((short)151)

    ;

    private final short value;

    private EraResponseCodes(short value) {
        this.value = value;
    }

    /**
     * @return the value
     */
    public short getValue() {
        return value;
    }

    public static EraResponseCodes resolve(short value) {
        for (EraResponseCodes c : values()) {
            if (c.value == value) {
                return c;
            }
        }
        return UNKNOWN;

    }
}
