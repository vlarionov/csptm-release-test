package ru.advantum.csptm.jep.hub.egts.packet.subrec;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSServiceSubRecord;

import java.io.IOException;
import java.io.InputStream;
/**
 *
 * @author Ostap Gerasimchuk, Oleg Mitsay
 * @since Mar 26, 2014 2:34:39 PM
 */
public class EGTS_SR_MODULE_DATA extends EGTSServiceSubRecord {

    public final Unsigned8 MT = new Unsigned8();
    public final Unsigned32 VID = new Unsigned32();
    public final Unsigned16 FWV = new Unsigned16();
    public final Unsigned16 SWV = new Unsigned16();
    public final Unsigned8 MD = new Unsigned8();
    public final Unsigned8 ST = new Unsigned8();

    public String SRN_N_DSCR;

    @Override
    public int readOptional(int subrecordLength, InputStream in) throws IOException {
        int len = 0;

        if (subrecordLength > size()) {
            final byte raw[] = new byte[subrecordLength - size()];
            len += in.read(raw);
            SRN_N_DSCR = new String(raw);
        }

        return len;
    }

}
