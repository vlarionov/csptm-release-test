package ru.advantum.csptm.jep.hub.egts.packet.types;

import ru.advantum.csptm.jep.hub.egts.packet.EGTS_SERVICE_SUBRECORD;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_ABS_AN_SENS_DATA;
import ru.advantum.csptm.jep.hub.egts.packet.EGTS_SERVICE;

/**
 * @author mitsay
 * @since  15.10.2015.
 */
public class EGTSAbsAnSensors extends AbstractEGTSSubrecordType {
    private final EGTS_SR_ABS_AN_SENS_DATA data;

    public EGTSAbsAnSensors(EGTS_SR_ABS_AN_SENS_DATA data, EGTS_SERVICE.OID oid, EGTS_SERVICE.TM tm) {
        super(oid, tm);
        type = EGTS_SERVICE_SUBRECORD.Type.EGTS_SR_ABS_AN_SENS_DATA;
        this.data = data;
    }

    public EGTS_SR_ABS_AN_SENS_DATA getRecord() {
        return data;
    }
}
