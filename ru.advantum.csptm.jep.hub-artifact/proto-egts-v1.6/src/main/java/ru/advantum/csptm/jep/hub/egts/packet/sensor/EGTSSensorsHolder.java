package ru.advantum.csptm.jep.hub.egts.packet.sensor;

import java.util.List;

/**
 *
 * @author kaganov
 */
public interface EGTSSensorsHolder {

    List<EGTSSensor> getSensors();

}
