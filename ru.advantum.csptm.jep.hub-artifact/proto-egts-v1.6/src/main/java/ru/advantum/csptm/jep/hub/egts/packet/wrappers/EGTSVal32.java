package ru.advantum.csptm.jep.hub.egts.packet.wrappers;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSBase;

/**
 *
 * @author kaganov
 */
public class EGTSVal32 extends EGTSBase {

    public final Unsigned32 VAL = new Unsigned32();

    public long get() {
        return VAL.get();
    }

    public String asString() {
        return Long.toString(get());
    }
}
