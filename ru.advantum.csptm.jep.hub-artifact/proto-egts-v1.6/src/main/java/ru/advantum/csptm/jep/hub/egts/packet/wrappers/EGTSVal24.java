package ru.advantum.csptm.jep.hub.egts.packet.wrappers;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSBase;

/**
 *
 * @author kaganov
 */
public class EGTSVal24 extends EGTSBase {

    private final BitField VAL = new BitField(24);

    public long getVAL() {
        return VAL.longValue();
    }

    public void setVAL(long VAL) {
        this.VAL.set(VAL);
    }

    public long get() {
        return getVAL();
    }

    public String asString() {
        return Long.toString(get());
    }
}
