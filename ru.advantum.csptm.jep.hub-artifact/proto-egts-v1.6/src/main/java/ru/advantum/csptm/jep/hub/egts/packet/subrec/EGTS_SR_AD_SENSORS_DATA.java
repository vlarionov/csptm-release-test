package ru.advantum.csptm.jep.hub.egts.packet.subrec;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSBase;
import ru.advantum.csptm.jep.hub.egts.packet.EGTSServiceSubRecord;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensor;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensorType;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensorsHolder;
import ru.advantum.csptm.jep.hub.egts.packet.wrappers.EGTSVal24;
import ru.advantum.csptm.jep.hub.egts.packet.wrappers.EGTSVal8;
import ru.advantum.csptm.jep.proto.util.BitIterable;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author kaganov
 */
public class EGTS_SR_AD_SENSORS_DATA extends EGTSServiceSubRecord implements EGTSSensorsHolder {

    public final Unsigned8 DIOE = new Unsigned8();
    public final Unsigned8 DOUT = new Unsigned8();
    public final Unsigned8 ASFE = new Unsigned8();

    public EGTSVal24 ANS[] = new EGTSVal24[8];

    private final List<EGTSSensor> sensors = new ArrayList<>();

    public void setASN(int number, long value) {

        ANS[number] = new EGTSVal24();
        ANS[number].setVAL(value);
    }

    @Override
    public EGTSBase[] toFrames() {
        List<EGTSBase> list = new ArrayList<>();
        for (EGTSVal24 AN : ANS) {
            if (AN != null) {
                if (!list.contains(this)) {
                    list.add(this);
                }
                list.add(AN);
            }
        }
        if (list.size() > 0) {
            return list.toArray(new EGTSBase[list.size()]);
        } else {
            return super.toFrames();
        }
    }

    @Override
    public List<EGTSSensor> getSensors() {
        return sensors;
    }

    @Override
    public int readOptional(int subrecordLength, InputStream in) throws IOException {
        int len = 0;

        int dioNum = 0;
        for (Boolean b : new BitIterable(Byte.SIZE, DIOE.get())) {
            if (b) {
                final EGTSVal8 digSens = new EGTSVal8();
                len += digSens.read(in);
                sensors.addAll(EGTSSensor.byteToBitSensors(EGTSSensorType.DIGITAL_INPUT,
                                                           // +1 потому что нумерация начинается с тех датчиков, которые в EGTS_SR_POS_DATA
                                                           packetNum * 8 + dioNum + 1,
                                                           (byte) digSens.VAL.get()));
            }
            dioNum++;
        }

        int ansNum = 0;
        for (Boolean b : new BitIterable(Byte.SIZE, ASFE.get())) {
            if (b) {
                final EGTSVal24 ansSens = new EGTSVal24();
                len += ansSens.read(in);
                sensors.add(new EGTSSensor(EGTSSensorType.ANALOG_SENSOR,
                                           packetNum * 8 + ansNum + 1,
                                           ansSens.getVAL()));
            }
            ansNum++;
        }

        return len;
    }
}
