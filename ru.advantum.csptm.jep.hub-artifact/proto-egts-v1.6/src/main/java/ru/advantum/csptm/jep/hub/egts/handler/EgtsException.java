package ru.advantum.csptm.jep.hub.egts.handler;

/**
 *
 * @author kaganov
 */
public class EgtsException extends Exception {

    public EgtsException(String msg) {
        super(msg);
    }
}
