package ru.advantum.csptm.jep.hub.egts.packet.subrec;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSServiceSubRecord;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensor;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensorType;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensorsHolder;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
/**
 * @author kaganov
 */
public class EGTS_SR_LIQUID_LEVEL_SENSOR extends EGTSServiceSubRecord implements EGTSSensorsHolder {

    private final BitField LLSN = new BitField(3);
    public BitField RDF = new BitField(1);
    private final BitField LLSVU = new BitField(2);
    public BitField LLSEF = new BitField(1);
    public BitField UNUSED = new BitField(1);
    public final Unsigned16 MADDR = new Unsigned16();
    public final Unsigned32 LLSD = new Unsigned32();


    public int getRDF() {
        return RDF.intValue();
    }

    public boolean isLLSEF() {
        return LLSEF.intValue() == 0; // no errors, else = invalid data
    }

    public boolean getUNUSED() {
        return UNUSED.intValue() == 1;
    }

    public short getLLSN() {
        return LLSN.shortValue();
    }

    public short getLLSVU() {
        return LLSVU.shortValue();
    }

    public void setLLSN(short LLSN) {
        this.LLSN.set(LLSN);
    }

    public void setLLSVU(short LLSVU) {
        this.LLSVU.set(LLSVU);
    }

    @Override
    public List<EGTSSensor> getSensors() {
        // сдвиг от MADDR идет на 256 в некоторых случаях, поэтому оставляем каст к byte...
        final byte byteNum = (byte) (LLSN.shortValue() + 8 * MADDR.get());
        return Collections.singletonList(new EGTSSensor(EGTSSensorType.LIQUID_LEVEL, byteNum, LLSD.get()));
    }

    @Override
    public int readOptional(int subrecordLength, InputStream in) throws IOException {
        int len = 0;

        if (getRDF() == 1) {
            len += in.skip(subrecordLength - size());
        }

        return len;
    }

    @Override
    public boolean isValid() {
        return isLLSEF() && ( getRDF() == 0 );
    }

}
