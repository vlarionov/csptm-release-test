package ru.advantum.csptm.jep.hub.egts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.hub.egts.handler.EraResponseCodes;
import ru.advantum.csptm.jep.hub.egts.packet.*;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_LIQUID_LEVEL_SENSOR;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_POS_DATA;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_TERM_IDENTITY;
import ru.advantum.csptm.jep.hub.egts.packet.transp.EGTS_Transport;
import ru.advantum.csptm.jep.hub.egts.packet.transp.EGTS_Transport_HCS;
import ru.advantum.csptm.jep.hub.egts.packet.transp.EGTS_Transport_RTE;
import ru.advantum.csptm.jep.hub.egts.packet.transp.EGTS_Transport_SFRCS;
import ru.advantum.tools.HEX;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Mitsay <mitsay@advantum.ru>
 * @since 26.05.16 16:32.
 */
public class AbstractFactory {
    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractFactory.class);

    public static final long DATE_01_01_2010 = 1262304000L;
    protected static final long LONG_CONST = 0xFFFFFFFFL;
    protected static final byte PRV_VERSION = 0x01;
    protected static final byte CMD_UNCOMPRESSED = 0x00;
    protected static final byte SKID = 0x00;

    protected EGTS_Transport_RTE.RTE RTE;
    protected final Map<Class, AtomicInteger> packetsCnt = new HashMap<>();

    public static class StructureException extends Exception {
        public StructureException(String message) {
            super(message);
        }
    }

    public static class ServiceData {
        public final int RN;
        public final EGTS_SERVICE.OID oid;
        public final EGTS_SERVICE.EVID evid;
        public final EGTS_SERVICE.TM tm;
        public final EGTSServiceSubRecord[] RD;
        public final EGTS_SERVICE_ST ST;

        public ServiceData(int RN,
                           EGTS_SERVICE_ST ST,
                           EGTS_SERVICE.OID oid,
                           EGTS_SERVICE.EVID evid,
                           EGTS_SERVICE.TM tm,
                           EGTSServiceSubRecord[] RD) {
            this.RN = RN;
            this.oid = oid;
            this.evid = evid;
            this.tm = tm;
            this.RD = RD;
            this.ST = ST;
        }
    }

    public enum PT {
        EGTS_PT_RESPONSE(0x00),
        EGTS_PT_APPDATA(0x01);

        private final int id;

        PT(int id) {
            this.id = id;
        }

        /**
         * @return the id
         */
        public int getId() {
            return id;
        }
    }

    protected class CycleSQ extends AtomicInteger {
        private int last;

        public CycleSQ(int initialValue) {
            super(initialValue);
        }

        public int getLast() {
            return this.last;
        }

        public int getNext() {
            compareAndSet(65536, 0);
            this.last = getAndIncrement();
            return this.last;
        }
    }

    protected void resetPacketNum() {
        packetsCnt.clear();
    }


    public AbstractFactory() {}

    public AbstractFactory(EGTS_Transport_RTE.RTE rte) {
        this.RTE = rte;
    }

    protected int getPacketNum(EGTSServiceSubRecord packet) {
        AtomicInteger ai = packetsCnt.get(packet.getClass());
        if (ai == null) {
            ai = new AtomicInteger();
            packetsCnt.put(packet.getClass(), ai);

            return 0;
        } else {
            return ai.incrementAndGet();
        }
    }

    public ServiceData[] readServiceData(PT type, byte buffer[]) throws StructureException,
                                                                        InstantiationException, IllegalAccessException {
        List<ServiceData> data = new ArrayList<>();
        boolean respProcessed = false;
        try {
            InputStream in = new ByteArrayInputStream(buffer);
            while (in.available() > 0) {
                final List<EGTSServiceSubRecord> subs = new ArrayList<>();
                // ЕСЛИ ЭТО RESP!!!
                if (type == PT.EGTS_PT_RESPONSE && !respProcessed) {
                    final EGTS_PT_RESPONSE r = new EGTS_PT_RESPONSE();
                    r.read(in);
                    subs.add(r);
                    respProcessed = true;
                }
                if (in.available() > 0) {
                    final EGTS_SERVICE service = new EGTS_SERVICE();
                    final EGTS_SERVICE_ST ST = new EGTS_SERVICE_ST();
                    EGTS_SERVICE.OID oid;
                    final EGTS_SERVICE.EVID evid;
                    final EGTS_SERVICE.TM tm;
                    service.read(in);
                    if (service.getOBFE()) {
                        (oid = new EGTS_SERVICE.OID()).read(in);
                    } else {
                        oid = null;
                    }
                    if (service.getEVFE()) {
                        (evid = new EGTS_SERVICE.EVID()).read(in);
                    } else {
                        evid = null;
                    }
                    if (service.getTMFE()) {
                        (tm = new EGTS_SERVICE.TM()).read(in);
                    } else {
                        tm = null;
                    }
                    if (in.available() > 0) {
                        ST.read(in);
                        int lenRD = service.RL.get();
                        final int startLen = lenRD;
                        while (lenRD > 0 && in.available() > 0) {
                            final EGTS_SERVICE_SUBRECORD ssr = new EGTS_SERVICE_SUBRECORD();
                            int r = ssr.read(in);
                            if (r <= 0) {
                                throw new StructureException(
                                        String.format("Corrupted Subrecord Data %d / lenRd: %d av: %d",
                                                startLen,
                                                lenRD,
                                                in.available()));
                            }
                            lenRD -= r;

                            EGTS_SERVICE_SUBRECORD.Type subrecType = EGTS_SERVICE_SUBRECORD.Type.resolve(ssr.getSRT());
                            if (subrecType == EGTS_SERVICE_SUBRECORD.Type.UNSUPPORTED) {

                                LOGGER.debug("UNSUPPORTED PACKET SRT: {} SRL: {}. Unsupported, skip.",
                                        ssr.getSRT(), ssr.SRL.get());
                                lenRD -= in.skip(ssr.SRL.get());
                            } else if (in.available() > 0) {

                                final EGTSServiceSubRecord packet = subrecType.getPacketClass().newInstance();
                                if (packet instanceof EGTS_SR_POS_DATA) {
                                    resetPacketNum();
                                }
                                packet.setPacketNum(getPacketNum(packet));
                                lenRD -= packet.read(in);
                                lenRD -= packet.readOptional(ssr.SRL.get(), in);
                                if (packet instanceof EGTS_SR_TERM_IDENTITY) {

                                    EGTS_SR_TERM_IDENTITY idr = (EGTS_SR_TERM_IDENTITY) packet;
                                    if (oid == null && idr.isIMEIE()) {
                                        try {
                                            oid = new EGTS_SERVICE.OID(
                                                    Long.parseLong(
                                                            idr.IMEI.asString())
                                            );
                                        } catch (NumberFormatException nfe) {
                                            LOGGER.warn("Invalid (non-parserable as number) IMEI: {}", idr.IMEI.asString());
                                        }
                                    }
//                                    LOGGER.debug("\ttID={}, IMEIE = {}, IMEI = {} OID sat to {} ",
//                                            idr.TID.get(),
//                                            idr.isIMEIE(),
//                                            idr.isIMEIE() ? idr.IMEI != null ? idr.IMEI.asString() : "null" : "not sat",
//                                            oid.value != null ? oid.value.get() : "null"
//                                    );
                                }
                                if (packet.isValid()) {
                                    if (packet instanceof EGTS_SR_LIQUID_LEVEL_SENSOR) {
                                        LOGGER.debug("#########    EGTS_SR_LIQUID_LEVEL_SENSOR add");
                                    }
                                    subs.add(packet);
                                } else {
                                    if (packet instanceof EGTS_SR_LIQUID_LEVEL_SENSOR) {
                                        LOGGER.debug("Invalid EGTS_SR_LIQUID_LEVEL_SENSOR {}: "
                                                        + "RDF={}, LLSEF={}, UNUSED={} "
                                                        + " MADDR={}, LLSD={}",
                                                ((EGTS_SR_LIQUID_LEVEL_SENSOR) packet).getLLSN(),
                                                ((EGTS_SR_LIQUID_LEVEL_SENSOR) packet).getRDF(),
                                                ((EGTS_SR_LIQUID_LEVEL_SENSOR) packet).isLLSEF(),
                                                ((EGTS_SR_LIQUID_LEVEL_SENSOR) packet).getUNUSED(),
                                                ((EGTS_SR_LIQUID_LEVEL_SENSOR) packet).MADDR.get(),
                                                ((EGTS_SR_LIQUID_LEVEL_SENSOR) packet).LLSD.get()
                                        );
                                    } else {
                                        LOGGER.info("Invalid packet SRT {} SRL {} Class {}",
                                                ssr.getSRT(), ssr.SRL.get(), packet.getClass());
                                    }
                                }
                            }
                        }
                        data.add(new ServiceData(service.RN.get(),
                                ST,
                                oid,
                                evid,
                                tm,
                                subs.toArray(new EGTSServiceSubRecord[subs.size()])));
                    } else {
                        LOGGER.debug("Only EGTS_PT_RESPONSE occured. Nothing else.");
                    }
                }
            }
        } catch (IOException ignored) {
        }
        return data.toArray(new ServiceData[data.size()]);
    }


    public PT resolvePT(final EGTS_Transport header) {
        return header.getPT() == PT.EGTS_PT_APPDATA.getId()
                ? PT.EGTS_PT_APPDATA
                : PT.EGTS_PT_RESPONSE;
    }

    public EraResponseCodes validateTrailer(byte buffer[], EGTS_Transport_SFRCS SFRCS) {
        return HEX.crc16citt(buffer, EGTSBase.SFRCS_POLYNOMIAL) == SFRCS.SFRCS.get() ? EraResponseCodes.PC_OK : EraResponseCodes.PC_DATACRC_ERROR;
    }

    public EraResponseCodes validateBasicTransportLayer(final EGTS_Transport header) {
        if (header.getPRV() != PRV_VERSION) {
            return EraResponseCodes.PC_UNS_PROTOCOL;
        }
        if (header.getCMP() != CMD_UNCOMPRESSED) {
            return EraResponseCodes.PC_UNS_PROTOCOL;
        }
        final int pt = header.getPT();
        if (pt != PT.EGTS_PT_APPDATA.getId() && pt != PT.EGTS_PT_RESPONSE.getId()) {
            return EraResponseCodes.PC_SRVC_UNKN;
        }
        return EraResponseCodes.PC_OK;
    }


    public EraResponseCodes validateFullTransportLayer(final EGTS_Transport header,
                                                       final EGTS_Transport_RTE rte,
                                                       final EGTS_Transport_HCS hcs) throws IOException {
        int crc8 = HEX.crc8(EGTSBase.toByteArray(header, rte));
        if ((crc8 & 0xFF) != (hcs.HCS.byteValue() & 0xFF)) {
            return EraResponseCodes.PC_HEADERCRC_ERROR;
        }
        assert RTE != null;
        if ((rte != null) && (rte.PRA.get() != RTE.RCA || rte.RCA.get() != RTE.PRA)) {
            return EraResponseCodes.PC_ROUTE_NFOUND;
        }
        if (header.getFDL() == 0) {
            return EraResponseCodes.PC_INVDATALEN;
        }
        return EraResponseCodes.PC_OK;
    }

}
