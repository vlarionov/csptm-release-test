package ru.advantum.csptm.jep.hub.egts.packet;

/**
 *
 * @author Ostap Gerasimchuk, Oleg Mitsay
 * @since May 29, 2013 2:59:02 PM
 */
public class EGTS_SERVICE_ST extends EGTSBase {
    public static final int EGTS_AUTH_SERVICE = 0x01;
    public static final int EGTS_TELEDATA_SERVICE = 0x02;

    public final int getSST() {
        return SST.intValue();
    }

    public final int getRST() {
        return RST.intValue();
    }

    public BitField SST = new BitField(8);
    public BitField RST = new BitField(8);

    public EGTS_SERVICE_ST() {
        super();
    }

    public void setSST(int SST) {
        this.SST.set(SST);
    }

    public void setRST(int RST) {
        this.RST.set(RST);
    }

}
