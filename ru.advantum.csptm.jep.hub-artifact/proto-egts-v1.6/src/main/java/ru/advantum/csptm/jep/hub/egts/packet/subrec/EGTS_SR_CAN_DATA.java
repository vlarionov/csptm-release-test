package ru.advantum.csptm.jep.hub.egts.packet.subrec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.hub.egts.packet.EGTSServiceSubRecord;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.CanSensorLocation;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensor;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensorType;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.EGTSSensorsHolder;
import ru.advantum.csptm.jep.hub.egts.packet.wrappers.EGTSVal16;
import ru.advantum.csptm.jep.hub.egts.packet.wrappers.EGTSVal32;
import ru.advantum.csptm.jep.hub.egts.packet.wrappers.EGTSVal64;
import ru.advantum.csptm.jep.hub.egts.packet.wrappers.EGTSVal8;
import ru.advantum.csptm.jep.proto.util.BitIterable;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
/**
 *
 * @author kaganov
 */
public class EGTS_SR_CAN_DATA extends EGTSServiceSubRecord implements EGTSSensorsHolder {

    protected static final Logger log = LoggerFactory.getLogger(EGTS_SR_CAN_DATA.class);

    private static final ConcurrentHashMap<CanSensorLocation, Integer> settings;
    private static final Set<CanSensorLocation> bitFields;

    static {
        settings = new ConcurrentHashMap<>();
        settings.put(new CanSensorLocation(0, 1), 0);
        settings.put(new CanSensorLocation(0, 2), 1);
        settings.put(new CanSensorLocation(0, 3), 2);
        settings.put(new CanSensorLocation(0, 4), 3);
        settings.put(new CanSensorLocation(1, 1), 4);
        settings.put(new CanSensorLocation(1, 2), 5);
        settings.put(new CanSensorLocation(1, 3), 6);
        settings.put(new CanSensorLocation(1, 4), 7);
        settings.put(new CanSensorLocation(1, 5), 8);
        settings.put(new CanSensorLocation(2, 1), 9);
        settings.put(new CanSensorLocation(2, 2), 10);
        settings.put(new CanSensorLocation(2, 3), 11);
        settings.put(new CanSensorLocation(2, 4), 12);
        settings.put(new CanSensorLocation(3, 1), 13);
        settings.put(new CanSensorLocation(3, 2), 14);
        settings.put(new CanSensorLocation(3, 3), 15);
        settings.put(new CanSensorLocation(3, 4), 16);
        settings.put(new CanSensorLocation(3, 5), 17);
        settings.put(new CanSensorLocation(3, 6), 18);
        settings.put(new CanSensorLocation(3, 7), 19);
        settings.put(new CanSensorLocation(3, 8), 20);
        settings.put(new CanSensorLocation(3, 9), 21);
        settings.put(new CanSensorLocation(3, 10), 22);
        settings.put(new CanSensorLocation(3, 11), 23);
        settings.put(new CanSensorLocation(3, 12), 24);
        settings.put(new CanSensorLocation(3, 13), 25);
        settings.put(new CanSensorLocation(3, 14), 26);
        settings.put(new CanSensorLocation(3, 15), 27);
        settings.put(new CanSensorLocation(4, 1), 28);
        settings.put(new CanSensorLocation(5, 1), 60);
        settings.put(new CanSensorLocation(6, 1), 92);
        settings.put(new CanSensorLocation(6, 2), 124);
        settings.put(new CanSensorLocation(6, 3), 125);
        settings.put(new CanSensorLocation(6, 4), 126);
        settings.put(new CanSensorLocation(6, 5), 127);
        settings.put(new CanSensorLocation(6, 6), 128);
        settings.put(new CanSensorLocation(6, 7), 129);
        settings.put(new CanSensorLocation(6, 8), 130);

        bitFields = Collections.newSetFromMap(new ConcurrentHashMap<CanSensorLocation, Boolean>());
        bitFields.add(new CanSensorLocation(4, 1));
        bitFields.add(new CanSensorLocation(5, 1));
        bitFields.add(new CanSensorLocation(6, 1));
    }

    public final Unsigned16 CANFormat = new Unsigned16();

    private final List<EGTSSensor> sensors = new ArrayList<>();

    @Override
    public List<EGTSSensor> getSensors() {
        return sensors;
    }

    @Override
    public int readOptional(int subrecordLength, InputStream in) throws IOException {
        int len = 0;

        final int packetSize = size();

        while (len + packetSize < subrecordLength) {
            if (in.available() <= 0) {
                throw new IOException("End of stream reached while reading CAN data!");
            }

            EGTS_SR_CAN_DATA_ITEM canData = new EGTS_SR_CAN_DATA_ITEM();
            len += canData.read(in);

            final short paramGroup = canData.paramGroup.get();
            final CanSensorLocation canSensorLocation = new CanSensorLocation(paramGroup, canData.dataType.get());

            Integer idSensor = settings.get(canSensorLocation);

            log.debug("CAN_DATA GROUP {} TYPE {} LEN {}",
                      canData.paramGroup.get(), canData.dataType.get(), canData.dataLength.get());

            if (idSensor == null) {
                log.warn("Invalid CAN data {} {}", paramGroup, canData.dataType.get());
                len += in.skip(canData.dataLength.get());
                continue;
            }

            long sensorValue;
            switch (canData.dataLength.get()) {
                case 1: {
                    EGTSVal8 packet = new EGTSVal8();
                    len += packet.read(in);
                    sensorValue = packet.VAL.get();
                    break;
                }
                case 2: {
                    EGTSVal16 packet = new EGTSVal16();
                    len += packet.read(in);
                    sensorValue = packet.VAL.get();
                    break;
                }
                case 4: {
                    EGTSVal32 packet = new EGTSVal32();
                    len += packet.read(in);
                    sensorValue = packet.VAL.get();
                    break;
                }
                case 8: {
                    EGTSVal64 packet = new EGTSVal64();
                    len += packet.read(in);
                    sensorValue = packet.VAL.get();
                    break;
                }
                default:
                    len += in.skip(canData.dataLength.get());
                    continue;
            }

            if (bitFields.contains(canSensorLocation)) {
                byte bitNum = 0;
                for (Boolean b : BitIterable.ofValue(sensorValue)) {
                    sensors.add(new EGTSSensor(EGTSSensorType.CAN, idSensor + bitNum, b ? 1 : 0));
                    bitNum++;
                }
            } else {
                sensors.add(new EGTSSensor(EGTSSensorType.CAN, idSensor, sensorValue));
            }
        }

        return len;
    }

    @Override
    public boolean isValid() {
        return CANFormat.get() == 0;
    }

}
