package ru.advantum.csptm.jep.hub.egts.packet;

/**
 * @author Ostap Gerasimchuk, Oleg Mitsay
 * @since May 29, 2013 2:51:57 PM
 */
public class EGTS_SERVICE extends EGTSBase {

    public static class OID extends EGTSBase {

        public final Unsigned32 value = new Unsigned32();

        public OID() {
            super();
        }

        public OID(long oid) {
            super();
            this.value.set(oid);
        }

        public void setValue(long oid) {
            this.value.set(oid);
        }

    }

    public static class EVID extends EGTSBase {

        public final Unsigned32 value = new Unsigned32();

        public EVID() {
            super();
        }

        public EVID(long evid) {
            super();
            this.value.set(evid);
        }

    }

    public static class TM extends EGTSBase {

        public final Unsigned32 value = new Unsigned32();

        public TM() {
            super();
        }

        public TM(long tm) {
            super();
            this.value.set(tm);
        }

    }

    public final Unsigned16 RL = new Unsigned16(16);
    public final Unsigned16 RN = new Unsigned16(16);
    //RFL


    public Unsigned16 getRL() {
        return RL;
    }

    public Unsigned16 getRN() {
        return RN;
    }

    public boolean getOBFE() {
        return OBFE.byteValue() != 0;
    }

    public void setOBFE(boolean OBFE) {
        this.OBFE.set(OBFE ? 1 : 0);
    }

    public boolean getEVFE() {
        return EVFE.byteValue() != 0;
    }

    public void setEVFE(boolean EVFE) {
        this.EVFE.set(EVFE ? 1 : 0);
    }

    public boolean getTMFE() {
        return TMFE.byteValue() != 0;
    }

    public void setTMFE(boolean TMFE) {
        this.TMFE.set(TMFE ? 1 : 0);
    }

    public boolean getGPR() {
        return GPR.byteValue() != 0;
    }

    public void setGPR(boolean GPR) {
        this.GPR.set(GPR ? 1 : 0);
    }

    public boolean getRSOD() {
        return RSOD.byteValue() != 0;
    }

    public void setRSOD(boolean RSOD) {
        this.RSOD.set(RSOD ? 1 : 0);
    }

    public boolean getSSOD() {
        return SSOD.byteValue() != 0;
    }

    public void setSSOD(boolean SSOD) {
        this.SSOD.set(SSOD ? 1 : 0);
    }

    public BitField OBFE = new BitField(1);
    public BitField EVFE = new BitField(1);
    public BitField TMFE = new BitField(1);
    private final BitField RPP = new BitField(2);
    public BitField GPR = new BitField(1);
    public BitField RSOD = new BitField(1);
    public BitField SSOD = new BitField(1);

    public int getRPP() {
        return RPP.intValue();
    }

    public void setRPP(int RPP) {
        this.RPP.set(RPP);
    }
}
