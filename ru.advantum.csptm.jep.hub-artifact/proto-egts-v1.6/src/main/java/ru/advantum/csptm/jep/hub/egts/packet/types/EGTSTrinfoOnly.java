package ru.advantum.csptm.jep.hub.egts.packet.types;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSServiceSubRecord;
import ru.advantum.csptm.jep.hub.egts.packet.EGTS_SERVICE;

/**
 * @author mitsay 16.10.2015
 */
public class EGTSTrinfoOnly extends AbstractEGTSSubrecordType {

    public EGTSTrinfoOnly(EGTS_SERVICE.OID oid, EGTS_SERVICE.TM tm) {
        super(oid, tm);
    }

    @Override
    public EGTSServiceSubRecord getRecord() {
        return null;
    }
}
