package ru.advantum.csptm.jep.hub.egts.handler;

import ru.advantum.csptm.jep.hub.egts.AbstractFactory;

public class EraDecodeMessage {

    public int getPID() {
        return PID;
    }

    public static class RESPONSE extends EraDecodeMessage {

        public RESPONSE(int PID, AbstractFactory.ServiceData[] data) {
            super(PID, data);
        }
    }

    public static class EXCEPTION extends EraDecodeMessage {
        private final EraResponseCodes respCode;
        public EXCEPTION(int PID, EraResponseCodes respCode) {
            super(PID, null);
            this.respCode = respCode;
        }

        /**
         * @return the respCode
         */
        public EraResponseCodes getRespCode() {
            return respCode;
        }

    }

    public static class DATA extends EraDecodeMessage {

        public DATA(int PID, AbstractFactory.ServiceData[] data) {
            super(PID, data);
        }
    }
    protected final AbstractFactory.ServiceData data[];
    private final int PID;

    private EraDecodeMessage(int PID, AbstractFactory.ServiceData[] data) {
        this.data = data;
        this.PID = PID;
    }

    /**
     * @return the data
     */
    public AbstractFactory.ServiceData[] getData() {
        return data;
    }
}
