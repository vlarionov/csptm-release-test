package ru.advantum.csptm.jep.hub.egts.handler;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.hub.egts.AbstractFactory;
import ru.advantum.csptm.jep.hub.egts.packet.transp.EGTS_Transport;
import ru.advantum.csptm.jep.hub.egts.packet.transp.EGTS_Transport_HCS;
import ru.advantum.csptm.jep.hub.egts.packet.transp.EGTS_Transport_RTE;
import ru.advantum.csptm.jep.hub.egts.packet.transp.EGTS_Transport_SFRCS;
import ru.advantum.csptm.jep.netty.SimpleStateDecoder;

import static ru.advantum.csptm.jep.hub.egts.AbstractFactory.PT.EGTS_PT_APPDATA;
import static ru.advantum.csptm.jep.hub.egts.AbstractFactory.PT.EGTS_PT_RESPONSE;

public class EraDecoder extends SimpleStateDecoder {

    public static final long DATE_01_01_2010 = 1262304000L;

    private static final Logger log = LoggerFactory.getLogger(EraDecoder.class);

    private final AbstractFactory factory;

    public EraDecoder(AbstractFactory factory) {
        super();
        this.factory = factory;
    }

    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel channel, ChannelBuffer buffer,
                            STATE state) throws Exception {
        bufferCheck(buffer);

        final EGTS_Transport header = readData(buffer, EGTS_Transport.class);
        final int PID = header.getPID();

        final EraResponseCodes headerCheck;
        if ((headerCheck = factory.validateBasicTransportLayer(header)) == EraResponseCodes.PC_OK) {
            final EGTS_Transport_RTE RTE;
            if (header.getRTE() == 0x01) {
                RTE = readData(buffer, EGTS_Transport_RTE.class);
            } else {
                RTE = null;
            }

            final EGTS_Transport_HCS HCS = readData(buffer, EGTS_Transport_HCS.class);

            final EraResponseCodes headerFullCheck;
            if ((headerFullCheck = factory.validateFullTransportLayer(header, RTE, HCS)) == EraResponseCodes.PC_OK) {

                final byte[] recordData = readTrailer(buffer, header.getFDL());
                final EGTS_Transport_SFRCS SFRCS = readData(buffer, EGTS_Transport_SFRCS.class);

                final EraResponseCodes trailerCheck;
                if ((trailerCheck = factory.validateTrailer(recordData, SFRCS)) == EraResponseCodes.PC_OK) {
                    //log.debug(state.toString());
                    switch (factory.resolvePT(header)) {
                        case EGTS_PT_APPDATA: {
                            try {
                                return new EraDecodeMessage.DATA(PID, factory.readServiceData(EGTS_PT_APPDATA, recordData));
                            } catch (AbstractFactory.StructureException ex) {
                                log.error("EGTS_PT_APPDATA:", ex);
                                return new EraDecodeMessage.EXCEPTION(PID, EraResponseCodes.PC_INC_DATAFORM);
                            }
                        }

                        case EGTS_PT_RESPONSE: {
                            return new EraDecodeMessage.RESPONSE(PID, factory.readServiceData(EGTS_PT_RESPONSE, recordData));
                        }

                        default: {
                            log.error("Code not complete, unsupported PT in transport-header: {} ", header.getPT());
                            return new EraDecodeMessage.EXCEPTION(PID, EraResponseCodes.PC_SRVC_UNKN);
                        }
                    }
                } else {
                    log.warn("validateTrailer: {}", trailerCheck);
                    return new EraDecodeMessage.EXCEPTION(PID, trailerCheck);
                }
            } else {
                log.warn("validateFullTransportLayer: {}", headerFullCheck);
                return new EraDecodeMessage.EXCEPTION(PID, headerFullCheck);
            }
        } else {
            log.warn("validateBasicTransportLayer: {}", headerCheck);
            return new EraDecodeMessage.EXCEPTION(PID, headerCheck);
        }
    }
}
