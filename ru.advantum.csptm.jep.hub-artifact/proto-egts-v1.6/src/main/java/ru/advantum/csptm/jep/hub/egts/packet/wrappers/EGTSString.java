package ru.advantum.csptm.jep.hub.egts.packet.wrappers;

import ru.advantum.csptm.jep.hub.egts.packet.EGTSBase;

/**
 * @author mitsay
 * @since 16.10.2015.
 */

public class EGTSString extends EGTSBase {
    public ByteSet VAL = null;
    private int length = 0;

    public EGTSString(int length) {
        this.VAL = new ByteSet(length);
        this.length = length;
    }

    public String asString() {
        return new String(VAL.get());
    }


}
