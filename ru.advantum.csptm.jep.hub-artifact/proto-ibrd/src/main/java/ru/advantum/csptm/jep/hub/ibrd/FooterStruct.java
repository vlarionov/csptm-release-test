package ru.advantum.csptm.jep.hub.ibrd;

public class FooterStruct extends BaseStruct {
    public static final short PACKAGE_END_SIGN = 0xAE;

    public final Unsigned16 checkSum = new Unsigned16();

    public final Unsigned8 packageEnd = new Unsigned8();

    public FooterStruct() {
    }

    public FooterStruct(int checkSum) {
        addInitializer(() -> {
            this.checkSum.set(checkSum);
            this.packageEnd.set(PACKAGE_END_SIGN);
        });
    }

    @Override
    public String toString() {
        return "FooterStruct{" +
                "checkSum=" + checkSum +
                ", packageEnd=" + packageEnd +
                '}';
    }
}
