package ru.advantum.csptm.jep.hub.ibrd.monorgb.command;

import ru.advantum.csptm.jep.hub.ibrd.CommandStruct;
import ru.advantum.csptm.jep.hub.ibrd.monorgb.MonoRGBTemperatureStruct;

public class SetMonoRGBTemperatureCommandStruct extends CommandStruct {
    public SetMonoRGBTemperatureCommandStruct(MonoRGBTemperatureStruct temperatureStruct) {
        super(MonoRGBCommandEnum.SET_TEMPERATURE.getNumber(), temperatureStruct);
    }
}
