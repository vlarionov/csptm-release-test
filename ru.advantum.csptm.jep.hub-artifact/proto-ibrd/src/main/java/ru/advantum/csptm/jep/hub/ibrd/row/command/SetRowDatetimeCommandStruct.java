package ru.advantum.csptm.jep.hub.ibrd.row.command;

import ru.advantum.csptm.jep.hub.ibrd.CommandStruct;
import ru.advantum.csptm.jep.hub.ibrd.DatetimeStruct;

public class SetRowDatetimeCommandStruct extends CommandStruct {
    public SetRowDatetimeCommandStruct(DatetimeStruct datetimeStruct) {
        super(RowCommandEnum.SET_DATETIME.getNumber(), datetimeStruct);
    }
}
