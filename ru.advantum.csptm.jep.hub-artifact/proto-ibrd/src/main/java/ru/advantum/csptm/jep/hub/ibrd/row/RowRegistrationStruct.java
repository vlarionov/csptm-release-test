package ru.advantum.csptm.jep.hub.ibrd.row;

import ru.advantum.csptm.jep.hub.ibrd.RegistrationStruct;

public class RowRegistrationStruct extends RegistrationStruct {

    public final Unsigned16 rowStatuses = new Unsigned16();

    public final Unsigned8 deviceStatuses = new Unsigned8();

    @Override
    public String toString() {
        return "RowRegistrationStruct{" +
                "imei=" + imei.get() +
                ", version=" + version.get() +
                ", rowStatuses=" + rowStatuses.get() +
                ", deviceStatuses=" + deviceStatuses.get() +
                '}';
    }
}
