package ru.advantum.csptm.jep.hub.ibrd.monorgb.command;

import ru.advantum.csptm.jep.hub.ibrd.CommandStruct;
import ru.advantum.csptm.jep.hub.ibrd.monorgb.MonoRGBForecastStruct;

public class SetMonoRGBForecastCommandStruct extends CommandStruct {
    public SetMonoRGBForecastCommandStruct(MonoRGBForecastStruct forecast) {
        super(MonoRGBCommandEnum.SET_FORECAST.getNumber(), forecast);
    }
}
