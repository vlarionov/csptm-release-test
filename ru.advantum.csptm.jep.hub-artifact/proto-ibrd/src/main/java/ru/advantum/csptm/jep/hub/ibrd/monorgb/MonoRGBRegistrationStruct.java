package ru.advantum.csptm.jep.hub.ibrd.monorgb;

import ru.advantum.csptm.jep.hub.ibrd.RegistrationStruct;

public class MonoRGBRegistrationStruct extends RegistrationStruct {

    public final Unsigned8 status = new Unsigned8();

    @Override
    public String toString() {
        return "MonoRGBRegistrationStruct{" +
                "imei=" + imei +
                ", version=" + version +
                ", status=" + status +
                '}';
    }
}
