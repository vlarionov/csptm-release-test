package ru.advantum.csptm.jep.hub.ibrd.monorgb.command;

public enum MonoRGBCommandEnum {
    REGISTRATION(0xFF),
    SET_FORECAST(1),
    SET_PARAMETERS(2),
    SET_DATETIME(5),
    SET_TEMPERATURE(7);

    private final int number;

    MonoRGBCommandEnum(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }
}
