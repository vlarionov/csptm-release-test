package ru.advantum.csptm.jep.hub.ibrd.row.command;

import ru.advantum.csptm.jep.hub.ibrd.CommandStruct;

public class TelematicsRequestCommandStruct extends CommandStruct {
    public TelematicsRequestCommandStruct() {
        super(RowCommandEnum.TELEMATICS_REQUEST.getNumber(), null);
    }
}
