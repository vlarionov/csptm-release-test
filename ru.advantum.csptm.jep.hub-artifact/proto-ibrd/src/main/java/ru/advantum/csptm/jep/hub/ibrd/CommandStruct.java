package ru.advantum.csptm.jep.hub.ibrd;

public class CommandStruct extends BaseStruct {
    public final HeaderStruct header;

    public final BaseStruct body;

    public final FooterStruct footer;

    public CommandStruct(int commandNumber, BaseStruct body) {
        this(
                buildHeader(commandNumber, body),
                body,
                new FooterStruct(0)
        );
    }

    public short getCommandNumber() {
        return header.commandNumber.get();
    }

    private static HeaderStruct buildHeader(int commandNumber, BaseStruct body) {
        int packageSize = 0;
        if (body != null) {
            packageSize = body.size();
        }
        return new HeaderStruct(1 + packageSize, (short) commandNumber);
    }

    private CommandStruct(HeaderStruct header, BaseStruct body, FooterStruct footer) {
        this.header = inner(header);
        this.body = body != null ? inner(body) : null;
        this.footer = inner(footer);

        addInitializer(() -> {
            header.init();
            if (body != null) {
                body.init();
            }
            footer.init();
            footer.checkSum.set(getCheckSum(this));
        });
    }

    private int getCheckSum(BaseStruct base) {
        byte[] data = base.getBytes();
        int result = 0;
        for (int i = 1; i < data.length - 1; i++) {
            result += data[i] & 0xFF;
        }
        return result;
    }

    @Override
    public String toString() {
        return "CommandStruct{" +
                "header=" + header +
                ", body=" + body +
                ", footer=" + footer +
                '}';
    }
}
