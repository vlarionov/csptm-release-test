package ru.advantum.csptm.jep.hub.ibrd.row.command;

import ru.advantum.csptm.jep.hub.ibrd.CommandStruct;
import ru.advantum.csptm.jep.hub.ibrd.row.RowBlockListStruct;
import ru.advantum.csptm.jep.hub.ibrd.row.RowSubCommandStruct;

public class SetRowBlocksCommandStruct extends CommandStruct {
    private static final int SUB_COMMAND_NUMBER = 1;

    public SetRowBlocksCommandStruct(RowBlockListStruct blockList) {
        super(
                RowCommandEnum.SET_BLOCKS.getNumber(),
                new RowSubCommandStruct(
                        SUB_COMMAND_NUMBER,
                        blockList
                )
        );
    }
}
