package ru.advantum.csptm.jep.hub.ibrd.monorgb;

import ru.advantum.csptm.jep.hub.ibrd.BaseStruct;
import ru.advantum.csptm.jep.service.ibrd.model.monorgb.FontStyle;

public class MonoRGBForecastStruct extends BaseStruct {
    public final MonoRGBPropertiesStruct properties;

    public final MonoRGBRunningTextStruct runningText;

    public final Unsigned8 rowNumber;

    public final Unsigned8 routeNumberStyle;

    public final Unsigned8 arrivalTimeStyle;

    public final Unsigned8 terminalStyle;

    public final MonoRGBForecastListStruct forecastList;

    public MonoRGBForecastStruct(
            MonoRGBPropertiesStruct properties,
            MonoRGBRunningTextStruct runningText,
            int rowNumber,
            FontStyle routeNumberStyle,
            FontStyle arrivalTimeStyle,
            FontStyle terminalStyle,
            MonoRGBForecastListStruct forecastList
    ) {
        this.properties = inner(properties);
        this.runningText = inner(runningText);
        this.rowNumber = new Unsigned8();
        this.routeNumberStyle = new Unsigned8();
        this.arrivalTimeStyle = new Unsigned8();
        this.terminalStyle = new Unsigned8();
        this.forecastList = inner(forecastList);

        addInitializer(() -> {
            this.properties.init();
            //this.properties.set(properties.getBytes()[0]);//init();
            this.runningText.init();
            this.rowNumber.set((short) rowNumber);
            this.routeNumberStyle.set(getTextStyleValue(routeNumberStyle));
            this.arrivalTimeStyle.set(getTextStyleValue(arrivalTimeStyle));
            this.terminalStyle.set(getTextStyleValue(terminalStyle));
            this.forecastList.init();
        });
    }

    private short getTextStyleValue(FontStyle fontStyle) {
        switch (fontStyle) {
            case SLIM_MONOSPACED:
                return 0;
            case NORMAL_MONOSPACED:
                return 1;
            case SLIM_NOT_MONOSPACED:
                return 2;
            case NORMAL_NOT_MONOSPACED:
                return 3;
            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    public String toString() {
        return "MonoRGBForecastStruct{" +
                "properties=" + properties +
                ", runningText=" + runningText +
                ", rowNumber=" + rowNumber.get() +
                ", routeNumberStyle=" + routeNumberStyle.get() +
                ", arrivalTimeStyle=" + arrivalTimeStyle.get() +
                ", terminalStyle=" + terminalStyle.get() +
                ", forecastList=" + forecastList +
                '}';
    }
}
