package ru.advantum.csptm.jep.hub.ibrd.row;

import java.time.Duration;

public class RowForecastStruct extends RowBlockStruct {
    public final Unsigned8 blockRate;

    public final RowTextStruct routeNumber;

    public final RowTextStruct arrivalTime;

    public final RowTextStruct terminal;

    public RowForecastStruct(int speed, Duration blockRate, RowTextStruct routeNumber, RowTextStruct arrivalTime, RowTextStruct terminal) {
        super(2, speed);
        this.blockRate = new Unsigned8();
        this.routeNumber = inner(routeNumber);
        this.arrivalTime = inner(arrivalTime);
        this.terminal = inner(terminal);

        addInitializer(() -> {
            this.blockRate.set((short) blockRate.getSeconds());
            this.routeNumber.init();
            this.arrivalTime.init();
            this.terminal.init();
        });
    }

    @Override
    public String toString() {
        return "RowForecastStruct{" +
                "routeNumber=" + routeNumber +
                ", arrivalTime=" + arrivalTime +
                ", terminal=" + terminal +
                ", blockType=" + blockType.get() +
                ", speed=" + speed.get() +
                '}';
    }
}
