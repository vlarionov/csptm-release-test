package ru.advantum.csptm.jep.hub.ibrd.monorgb;

import ru.advantum.csptm.jep.hub.ibrd.BaseStruct;

public class MonoRGBForecastItemStruct extends BaseStruct {
//    public final Unsigned8 size;

    public final MonoRGBTextStruct routeNumber;

    public final MonoRGBTextStruct arrivalTime;

    public final MonoRGBTextStruct terminal;


    public MonoRGBForecastItemStruct(MonoRGBTextStruct routeNumber, MonoRGBTextStruct arrivalTime, MonoRGBTextStruct terminal) {
//        this.size = new Unsigned8();
        this.routeNumber = inner(routeNumber);
        this.arrivalTime = inner(arrivalTime);
        this.terminal = inner(terminal);

        addInitializer(() -> {
//            this.size.set((short) (routeNumber.size() + arrivalTime.size() + terminal.size()));
            this.routeNumber.init();
            this.arrivalTime.init();
            this.terminal.init();
        });
    }

    @Override
    public String toString() {
        return "MonoRGBForecastItemStruct{" +
//                "size=" + size.get() +
                ", routeNumber=" + routeNumber +
                ", arrivalTime=" + arrivalTime +
                ", terminal=" + terminal +
                '}';
    }
}
