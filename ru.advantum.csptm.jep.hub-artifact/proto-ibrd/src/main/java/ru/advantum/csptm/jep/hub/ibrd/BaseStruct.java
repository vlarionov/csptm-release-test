package ru.advantum.csptm.jep.hub.ibrd;

import javolution.io.Struct;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

public class BaseStruct extends Struct {
    private List<Initializer> initializers = new ArrayList<>();

    @Override
    public ByteOrder byteOrder() {
        return ByteOrder.LITTLE_ENDIAN;
    }

    @Override
    public boolean isPacked() {
        return true;
    }

    public byte[] getBytes() {
        try(ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            this.write(outputStream);
            return outputStream.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void init() {
        initializers.forEach(Initializer::init);
    }

    protected void addInitializer(Initializer initializer) {
        initializers.add(initializer);
    }

    protected interface Initializer {
        void init();
    }
}
