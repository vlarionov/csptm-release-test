package ru.advantum.csptm.jep.hub.ibrd.monorgb;

import ru.advantum.csptm.jep.hub.ibrd.BaseStruct;

import java.util.Arrays;
import java.util.List;

public class MonoRGBForecastListStruct extends BaseStruct {
    public final Unsigned8 forecastsNumber;

    public final MonoRGBForecastItemStruct[] forecastItems;

    public MonoRGBForecastListStruct(List<MonoRGBForecastItemStruct> forecastItems) {
        this.forecastsNumber = new Unsigned8();
        this.forecastItems = array(forecastItems.toArray(new MonoRGBForecastItemStruct[forecastItems.size()]));

        addInitializer(() -> {
            forecastsNumber.set((short) forecastItems.size());
            forecastItems.forEach(BaseStruct::init);
        });
    }

    @Override
    public String toString() {
        return "MonoRGBForecastListStruct{" +
                "forecastsNumber=" + forecastsNumber.get() +
                ", forecastItems=" + Arrays.toString(forecastItems) +
                '}';
    }

}
