package ru.advantum.csptm.jep.hub.ibrd.row;

import ru.advantum.csptm.jep.hub.ibrd.BaseStruct;

import java.util.Arrays;

public class TelematicsStruct extends BaseStruct {
    public final ByteSet unusedBytes = new ByteSet(3);

    public final TelematicsFlagsStruct flags = inner(new TelematicsFlagsStruct());

    public final UTF8String lat = new UTF8String(10);

    public final UTF8String lon = new UTF8String(11);

    public float parseLat() {
        int deg = Integer.parseInt(lat.get().substring(0, 2));
        float min = Float.parseFloat(lat.get().substring(2, 9));
        return deg + (min / 60);
    }

    public float parseLon() {
        int deg = Integer.parseInt(lon.get().substring(0, 3));
        float min = Float.parseFloat(lon.get().substring(3, 10));
        return deg + (min / 60);
    }

    @Override
    public String toString() {
        return "TelematicsStruct{" +
                "unusedBytes=" + Arrays.toString(unusedBytes.get()) +
                ", lat=" + lat.get() +
                ", lon=" + lon.get() +
                '}';
    }
}
