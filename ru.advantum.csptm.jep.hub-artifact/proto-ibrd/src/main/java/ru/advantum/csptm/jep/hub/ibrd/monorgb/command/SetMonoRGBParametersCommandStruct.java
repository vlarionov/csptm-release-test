package ru.advantum.csptm.jep.hub.ibrd.monorgb.command;

import ru.advantum.csptm.jep.hub.ibrd.CommandStruct;
import ru.advantum.csptm.jep.hub.ibrd.monorgb.MonoRGBParametersStruct;

public class SetMonoRGBParametersCommandStruct extends CommandStruct {
    public SetMonoRGBParametersCommandStruct(MonoRGBParametersStruct parameters) {
        super(MonoRGBCommandEnum.SET_PARAMETERS.getNumber(), parameters);
    }
}
