package ru.advantum.csptm.jep.hub.ibrd.monorgb;

import ru.advantum.csptm.jep.hub.ibrd.BaseStruct;

public class MonoRGBFlagsStruct extends BaseStruct {
    public final BitField datetimeFromGSM = new BitField(1);

    public final BitField reserve = new BitField(7);

    public MonoRGBFlagsStruct() {
    }

    public MonoRGBFlagsStruct(boolean datetimeFromGSM) {
        addInitializer(() -> {
            this.datetimeFromGSM.set(datetimeFromGSM ? 1 : 0);
            this.reserve.set(0);
        });
    }

    @Override
    public String toString() {
        return "MonoRGBFlagsStruct{" +
                "datetimeFromGSM=" + datetimeFromGSM.intValue() +
                ", reserve=" + reserve.intValue() +
                '}';
    }
}
