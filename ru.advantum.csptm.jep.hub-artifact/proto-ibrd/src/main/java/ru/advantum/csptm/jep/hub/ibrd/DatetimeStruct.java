package ru.advantum.csptm.jep.hub.ibrd;

import java.time.LocalDateTime;

public class DatetimeStruct extends BaseStruct {
    public final Unsigned8 seconds = new Unsigned8();
    public final Unsigned8 minutes = new Unsigned8();
    public final Unsigned8 hours = new Unsigned8();
    public final Unsigned8 weekDay = new Unsigned8();
    public final Unsigned8 day = new Unsigned8();
    public final Unsigned8 month = new Unsigned8();
    public final Unsigned8 year = new Unsigned8();

    public DatetimeStruct(LocalDateTime dateTime) {
        addInitializer(() -> {
            seconds.set((short) dateTime.getSecond());
            minutes.set((short) dateTime.getMinute());
            hours.set((short) dateTime.getHour());
            weekDay.set((short) (dateTime.getDayOfWeek().getValue() - 1));
            day.set((short) dateTime.getDayOfMonth());
            month.set((short) dateTime.getMonthValue());
            year.set((short) (dateTime.getYear() % 1000));
        });
    }

    @Override
    public String toString() {
        return "DatetimeStruct{" +
                "seconds=" + seconds.get() +
                ", minutes=" + minutes.get() +
                ", hours=" + hours.get() +
                ", weekDay=" + weekDay.get() +
                ", day=" + day.get() +
                ", month=" + month.get() +
                ", year=" + year.get() +
                '}';
    }
}
