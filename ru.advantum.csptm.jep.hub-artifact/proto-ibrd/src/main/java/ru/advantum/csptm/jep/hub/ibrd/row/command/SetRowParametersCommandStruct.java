package ru.advantum.csptm.jep.hub.ibrd.row.command;

import ru.advantum.csptm.jep.hub.ibrd.CommandStruct;
import ru.advantum.csptm.jep.hub.ibrd.row.RowParametersStruct;

public class SetRowParametersCommandStruct extends CommandStruct {
    public SetRowParametersCommandStruct(RowParametersStruct parameters) {
        super(RowCommandEnum.SET_PARAMETERS.getNumber(), parameters);
    }
}
