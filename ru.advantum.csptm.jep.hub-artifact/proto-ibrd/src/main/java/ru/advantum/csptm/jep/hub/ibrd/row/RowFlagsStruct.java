package ru.advantum.csptm.jep.hub.ibrd.row;

import ru.advantum.csptm.jep.hub.ibrd.BaseStruct;

public class RowFlagsStruct extends BaseStruct {
    public final BitField displayDate = new BitField(1);

    public final BitField displayTime = new BitField(1);

    public final BitField displayTemperature = new BitField(1);

    public final BitField displayGSMSignalStrength = new BitField(1);

    public final BitField timeProvider = new BitField(1);

    public final BitField temperatureProvider = new BitField(1);

    public final BitField reserve = new BitField(2);

    public RowFlagsStruct(
            boolean displayDate,
            boolean displayTime,
            boolean displayTemperature,
            boolean displayGSMSignalStrength,
            TimeProvider timeProvider,
            TemperatureProvider temperatureProvider
    ) {
        addInitializer(() -> {
            this.displayDate.set(displayDate ? 1 : 0);
            this.displayTime.set(displayTime ? 1 : 0);
            this.displayTemperature.set(displayTemperature ? 1 : 0);
            this.displayGSMSignalStrength.set(displayGSMSignalStrength ? 1 : 0);
            this.timeProvider.set(getTimeProviderValue(timeProvider));
            this.temperatureProvider.set(getTemperatureProviderValue(temperatureProvider));
            this.reserve.set(0);
        });
    }

    private long getTimeProviderValue(TimeProvider timeProvider) {
        switch (timeProvider) {
            case GPS_GLONASS:
                return 0;
            case GSM:
                return 1;
            default:
                throw new IllegalArgumentException();
        }
    }

    private long getTemperatureProviderValue(TemperatureProvider temperatureProvider) {
        switch (temperatureProvider) {
            case SENSOR:
                return 0;
            case SERVER:
                return 1;
            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    public String toString() {
        return "RowFlagsStruct{" +
                "displayDate=" + displayDate.intValue() +
                ", displayTime=" + displayTime.intValue() +
                ", displayTemperature=" + displayTemperature.intValue() +
                ", displayGSMSignalStrength=" + displayGSMSignalStrength.intValue() +
                ", timeProvider=" + timeProvider.intValue() +
                ", temperatureProvider=" + temperatureProvider.intValue() +
                ", reserve=" + reserve.intValue() +
                '}';
    }

    public enum TimeProvider {
        GPS_GLONASS, GSM;
    }

    public enum TemperatureProvider {
        SENSOR, SERVER
    }


}
