package ru.advantum.csptm.jep.hub.ibrd;

public abstract class RegistrationStruct extends BaseStruct {

    public final UTF8String imei = new UTF8String(16);

    public final Unsigned8 version = new Unsigned8();

}
