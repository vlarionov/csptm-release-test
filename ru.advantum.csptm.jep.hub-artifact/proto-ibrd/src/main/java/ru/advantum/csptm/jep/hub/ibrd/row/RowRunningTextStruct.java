package ru.advantum.csptm.jep.hub.ibrd.row;

public class RowRunningTextStruct extends RowBlockStruct {
    public final RowTextStruct text;

    public RowRunningTextStruct(int speed, RowTextStruct text) {
        super(1, speed);
        this.text = inner(text);

        addInitializer(this.text::init);
    }

    @Override
    public String toString() {
        return "RowRunningTextStruct{" +
                "text=" + text +
                ", blockType=" + blockType.get() +
                ", speed=" + speed.get() +
                '}';
    }
}
