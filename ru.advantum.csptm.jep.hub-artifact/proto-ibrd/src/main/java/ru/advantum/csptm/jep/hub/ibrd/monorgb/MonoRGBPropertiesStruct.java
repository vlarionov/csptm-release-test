package ru.advantum.csptm.jep.hub.ibrd.monorgb;

import ru.advantum.csptm.jep.hub.ibrd.BaseStruct;
import ru.advantum.csptm.jep.service.ibrd.model.monorgb.RunningRowPosition;
import ru.advantum.csptm.jep.service.ibrd.model.monorgb.ScrollDirection;
import ru.advantum.csptm.jep.service.ibrd.model.monorgb.ScrollType;

public class MonoRGBPropertiesStruct extends BaseStruct {
    public final BitField displayTime = new BitField(1);

    public final BitField displayTemperature = new BitField(1);

    public final BitField displayRunningRow = new BitField(1);

    public final BitField runningRowPosition = new BitField(1);

    public final BitField scrollType = new BitField(2);

    public final BitField scrollDirection = new BitField(1);

    public final BitField reserve = new BitField(1);

    public MonoRGBPropertiesStruct(
            boolean displayTime,
            boolean displayTemperature,
            boolean displayRunningRow,
            RunningRowPosition runningRowPosition,
            ScrollType scrollType,
            ScrollDirection scrollDirection
    ) {
        addInitializer(() -> {
            this.displayTime.set(displayTime ? 1 : 0);
            this.displayTemperature.set(displayTemperature ? 1 : 0);
            this.displayRunningRow.set(displayRunningRow ? 1 : 0);
            this.runningRowPosition.set(getRunningRowPositionValue(runningRowPosition));
            this.scrollType.set(getScrollTypeValue(scrollType));
            this.scrollDirection.set(getScrollDirectionValue(scrollDirection));
            this.reserve.set(0);
        });
    }


//    @Override
//    public ByteOrder byteOrder() {
//        return ByteOrder.BIG_ENDIAN;
//    }

    private long getRunningRowPositionValue(RunningRowPosition runningRowPosition) {
        switch (runningRowPosition) {
            case DOWN:
                return 0;
            case UP:
                return 1;
            default:
                throw new IllegalArgumentException();
        }
    }

    private long getScrollTypeValue(ScrollType scrollType) {
        switch (scrollType) {
            case NONE:
                return 0;
            case ONE_BY_ONE:
                return 1;
            default:
                throw new IllegalArgumentException();
        }
    }

    private long getScrollDirectionValue(ScrollDirection scrollDirection) {
        switch (scrollDirection) {
            case UP:
                return 0;
            case DOWN:
                return 1;
            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    public String toString() {
        return "MonoRGBPropertiesStruct{" +
                "displayTime=" + displayTime.intValue() +
                ", displayTemperature=" + displayTemperature.intValue() +
                ", displayRunningRow=" + displayRunningRow.intValue() +
                ", runningRowPosition=" + runningRowPosition.intValue() +
                ", scrollType=" + scrollType.intValue() +
                ", scrollDirection=" + scrollDirection.intValue() +
                ", reserve=" + reserve.intValue() +
                '}';
    }
}
