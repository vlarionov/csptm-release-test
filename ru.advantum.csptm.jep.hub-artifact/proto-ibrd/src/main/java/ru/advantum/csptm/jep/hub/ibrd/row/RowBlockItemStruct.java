package ru.advantum.csptm.jep.hub.ibrd.row;

import ru.advantum.csptm.jep.hub.ibrd.BaseStruct;

public class RowBlockItemStruct extends BaseStruct {
    public final Unsigned8 blockNumber;

    public final Unsigned8 rowNumber;

    public final RowBlockStruct block;

    public RowBlockItemStruct(int blockNumber, int rowNumber, RowBlockStruct block) {
        this.blockNumber = new Unsigned8();
        this.rowNumber = new Unsigned8();
        this.block = inner(block);

        addInitializer(() -> {
            this.blockNumber.set((short) blockNumber);
            this.rowNumber.set((short) rowNumber);
            this.block.init();
        });
    }

    @Override
    public String toString() {
        return "RowBlockItemStruct{" +
                ", blockNumber=" + blockNumber.get() +
                ", rowNumber=" + rowNumber.get() +
                ", block=" + block +
                '}';
    }
}
