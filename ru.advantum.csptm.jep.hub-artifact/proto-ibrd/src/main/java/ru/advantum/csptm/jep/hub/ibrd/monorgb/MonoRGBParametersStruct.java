package ru.advantum.csptm.jep.hub.ibrd.monorgb;

import ru.advantum.csptm.jep.hub.ibrd.BaseStruct;

import java.io.ByteArrayOutputStream;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;


public class MonoRGBParametersStruct extends BaseStruct {

    public final MonoRGBFlagsStruct flags;

    public final Unsigned8 scrollDelay;

    public final Unsigned8 runningRowSpeed;

    public final Unsigned8 dataWaitingTime;

    public final ByteSet brightnessLevels;

    public MonoRGBParametersStruct() {
        this.flags = new MonoRGBFlagsStruct();
        this.scrollDelay = new Unsigned8();
        this.runningRowSpeed = new Unsigned8();
        this.dataWaitingTime = new Unsigned8();
        this.brightnessLevels = new ByteSet(24);
    }

    public MonoRGBParametersStruct(MonoRGBFlagsStruct flags, Duration scrollDelay, int runningRowSpeed, Duration dataWaitingTime, List<Integer> brightnessLevels) {
        this.flags = inner(flags);
        this.scrollDelay = new Unsigned8();
        this.runningRowSpeed = new Unsigned8();
        this.dataWaitingTime = new Unsigned8();
        this.brightnessLevels = new ByteSet(24);

        addInitializer(() -> {
            this.scrollDelay.set((short) scrollDelay.getSeconds());
            this.runningRowSpeed.set((short) runningRowSpeed);
            this.dataWaitingTime.set((short) dataWaitingTime.toMinutes());

            ByteArrayOutputStream brightnessLevelsOutputSteam = new ByteArrayOutputStream();
            brightnessLevels.forEach(brightnessLevelsOutputSteam::write);
            this.brightnessLevels.set(brightnessLevelsOutputSteam.toByteArray());
        });
    }

    @Override
    public String toString() {
        return "MonoRGBParametersStruct{" +
                "flags=" + flags +
                ", scrollDelay=" + scrollDelay.get() +
                ", runningRowSpeed=" + runningRowSpeed.get() +
                ", dataWaitingTime=" + dataWaitingTime.get() +
                ", brightnessLevels=" + Arrays.toString(brightnessLevels.get()) +
                '}';
    }
}
