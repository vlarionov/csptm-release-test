package ru.advantum.csptm.jep.hub.ibrd.row;

import ru.advantum.csptm.jep.hub.ibrd.BaseStruct;

public abstract class RowBlockStruct extends BaseStruct {
    public final Unsigned8 blockType;

    public final Unsigned8 speed;

    public RowBlockStruct(int blockType, int speed) {
        this.blockType = new Unsigned8();
        this.speed = new Unsigned8();

        addInitializer(() -> {
            this.blockType.set((short) blockType);
            this.speed.set((short) speed);
        });
    }

    @Override
    public String toString() {
        return "RowBlockStruct{" +
                "blockType=" + blockType.get() +
                ", speed=" + speed.get() +
                '}';
    }
}
