package ru.advantum.csptm.jep.hub.ibrd.monorgb;

import ru.advantum.csptm.jep.hub.ibrd.BaseStruct;

import java.nio.charset.Charset;
import java.util.Arrays;

public class MonoRGBTextStruct extends BaseStruct {
    private static final Charset TEXT_CHARSET = Charset.forName("windows-1251");

    private final Unsigned8 rColor;

    private final Unsigned8 gColor;

    private final Unsigned8 bColor;

    private final ByteSet text;

    private final Unsigned8 end;

    public MonoRGBTextStruct(int rColor, int gColor, int bColor, String text) {
        this.rColor = null;//new Unsigned8();
        this.gColor = null;//new Unsigned8();
        this.bColor = null;//new Unsigned8();

        byte[] textBytes = text.getBytes(TEXT_CHARSET);
        this.text = new ByteSet(textBytes.length);
        this.end = new Unsigned8();

        addInitializer(() -> {
            //this.rColor.set((short) rColor);
            //this.gColor.set((short) gColor);
            //this.bColor.set((short) bColor);
            this.text.set(textBytes);
            this.end.set((short) 0);
        });
    }

    @Override
    public String toString() {
        return "MonoRGBTextStruct{" +
                "rColor=" + //rColor.get() +
                ", gColor=" + //gColor.get() +
                ", bColor=" + //bColor.get() +
                ", text=" + Arrays.toString(text.get()) +
                ", end=" + end.get() +
                '}';
    }
}
