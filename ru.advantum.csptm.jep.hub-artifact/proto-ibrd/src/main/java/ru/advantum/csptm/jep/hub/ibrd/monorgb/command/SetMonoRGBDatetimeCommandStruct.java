package ru.advantum.csptm.jep.hub.ibrd.monorgb.command;

import ru.advantum.csptm.jep.hub.ibrd.CommandStruct;
import ru.advantum.csptm.jep.hub.ibrd.DatetimeStruct;

public class SetMonoRGBDatetimeCommandStruct extends CommandStruct {
    public SetMonoRGBDatetimeCommandStruct(DatetimeStruct datetimeStruct) {
        super(MonoRGBCommandEnum.SET_DATETIME.getNumber(), datetimeStruct);
    }
}