package ru.advantum.csptm.jep.hub.ibrd.monorgb;

import ru.advantum.csptm.jep.hub.ibrd.BaseStruct;

public class MonoRGBRunningTextStruct extends BaseStruct {
    public final Unsigned8 size;

    public final MonoRGBTextStruct text;

    public MonoRGBRunningTextStruct(MonoRGBTextStruct text) {
        this.size = new Unsigned8();
        this.text = inner(text);

        addInitializer(() -> {
            size.set((short) text.size());
            text.init();
        });
    }

    @Override
    public String toString() {
        return "MonoRGBRunningTextStruct{" +
                "size=" + size.get() +
                ", text=" + text +
                '}';
    }
}
