package ru.advantum.csptm.jep.hub.ibrd.row;

import ru.advantum.csptm.jep.hub.ibrd.BaseStruct;

import java.util.Arrays;
import java.util.List;

public class RowBlockListStruct extends BaseStruct {
    public final Unsigned8 blocksNumber;

    public final RowBlockItemStruct[] blockItems;

    public RowBlockListStruct(List<RowBlockItemStruct> blockItems) {
        this.blocksNumber = new Unsigned8();
        this.blockItems = array(blockItems.toArray(new RowBlockItemStruct[blockItems.size()]));

        addInitializer(() -> {
            blocksNumber.set((short) blockItems.size());
            blockItems.forEach(BaseStruct::init);
        });
    }

    @Override
    public String toString() {
        return "RowBlockListStruct{" +
                "blocksNumber=" + blocksNumber.get() +
                ", blockItems=" + Arrays.toString(blockItems) +
                '}';
    }
}
