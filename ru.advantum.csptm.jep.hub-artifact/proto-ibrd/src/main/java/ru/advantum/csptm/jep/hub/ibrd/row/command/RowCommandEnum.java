package ru.advantum.csptm.jep.hub.ibrd.row.command;

public enum RowCommandEnum {
    REGISTRATION(0xFF),
    SET_BLOCKS(1),
    SET_PARAMETERS(2),
    SET_DATETIME(5),
    TELEMATICS_REQUEST(9);

    private final int number;

    RowCommandEnum(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }
}
