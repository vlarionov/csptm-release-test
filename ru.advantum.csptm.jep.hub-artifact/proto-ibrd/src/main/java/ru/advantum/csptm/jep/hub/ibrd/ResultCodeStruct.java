package ru.advantum.csptm.jep.hub.ibrd;

public class ResultCodeStruct extends BaseStruct {
    public final Unsigned8 resultCode = new Unsigned8();

    @Override
    public String toString() {
        return "ResultCodeStruct{" +
                "resultCode=" + resultCode +
                '}';
    }
}
