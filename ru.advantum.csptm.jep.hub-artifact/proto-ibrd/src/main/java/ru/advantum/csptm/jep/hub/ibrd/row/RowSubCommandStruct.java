package ru.advantum.csptm.jep.hub.ibrd.row;

import ru.advantum.csptm.jep.hub.ibrd.BaseStruct;

public class RowSubCommandStruct extends BaseStruct {
    public final Unsigned8 subCommandNumber;

    public final BaseStruct body;

    public RowSubCommandStruct(int subCommandNumber, BaseStruct body) {
        this.subCommandNumber = new Unsigned8();
        this.body = body != null ? inner(body) : null;

        addInitializer(() -> {
            this.subCommandNumber.set((short) subCommandNumber);
            if (body != null) {
                body.init();
            }
        });
    }

    @Override
    public String toString() {
        return "RowSubCommandStruct{" +
                "subCommandNumber=" + subCommandNumber.get() +
                ", body=" + body +
                '}';
    }
}
