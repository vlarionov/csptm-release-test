package ru.advantum.csptm.jep.hub.ibrd;

public class HeaderStruct extends BaseStruct {
    public static final short PACKAGE_START_SIGN = 0xA5;

    public final Unsigned8 packageStart = new Unsigned8();

    public final Unsigned16 packageSize = new Unsigned16();

    public final Unsigned8 commandNumber = new Unsigned8();

    public HeaderStruct() {
    }

    public HeaderStruct(
            int packageSize,
            short commandNumber
    ) {
        addInitializer(() -> {
            this.packageStart.set(PACKAGE_START_SIGN);
            this.packageSize.set(packageSize);
            this.commandNumber.set(commandNumber);
        });
    }

    @Override
    public String toString() {
        return "HeaderStruct{" +
                "packageStart=" + packageStart.get() +
                ", packageSize=" + packageSize.get() +
                ", commandNumber=" + commandNumber.get() +
                '}';
    }
}
