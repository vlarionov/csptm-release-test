package ru.advantum.csptm.jep.hub.ibrd.row;

import ru.advantum.csptm.jep.hub.ibrd.BaseStruct;

import java.nio.ByteOrder;
import java.time.Duration;

public class RowParametersStruct extends BaseStruct {
    public final RowFlagsStruct flags;

    public final Unsigned8 timezone;

    public final Unsigned8 zeroRowUpdatePeriod;

    public final Unsigned8 serverWaitingTime;

    public final Unsigned8 dataWaitingTime;

    public final Unsigned8 rebootTime;

    @Override
    public ByteOrder byteOrder() {
        return ByteOrder.BIG_ENDIAN;
    }

    public RowParametersStruct(
            RowFlagsStruct flags,
            int timezone,
            Duration zeroRowUpdatePeriod,
            Duration serverWaitingTime,
            Duration dataWaitingTime,
            Duration rebootTime
    ) {
        this.flags = inner(flags);
        this.timezone = new Unsigned8();
        this.zeroRowUpdatePeriod = new Unsigned8();
        this.serverWaitingTime = new Unsigned8();
        this.dataWaitingTime = new Unsigned8();
        this.rebootTime = new Unsigned8();

        addInitializer(() -> {
            this.flags.init();
            this.timezone.set((short) timezone);
            this.zeroRowUpdatePeriod.set((short) zeroRowUpdatePeriod.getSeconds());
            this.serverWaitingTime.set((short) serverWaitingTime.toMinutes());
            this.dataWaitingTime.set((short) dataWaitingTime.toMinutes());
            this.rebootTime.set((short) rebootTime.toHours());
        });
    }

    @Override
    public String toString() {
        return "RowParametersStruct{" +
                "flags=" + flags +
                ", timezone=" + timezone.get() +
                ", zeroRowUpdatePeriod=" + zeroRowUpdatePeriod.get() +
                ", serverWaitingTime=" + serverWaitingTime.get() +
                ", dataWaitingTime=" + dataWaitingTime.get() +
                ", rebootTime=" + rebootTime.get() +
                '}';
    }
}
