package ru.advantum.csptm.jep.hub.ibrd.monorgb;

import ru.advantum.csptm.jep.hub.ibrd.BaseStruct;

import java.nio.charset.Charset;

public class MonoRGBTemperatureStruct extends BaseStruct {
    private static final Charset TEXT_CHARSET = Charset.forName("windows-1251");
    private static final String TEXT_FORMAT = "%+03d";

    public final ByteSet temperature = new ByteSet(3);

    public MonoRGBTemperatureStruct(int temperature) {
        addInitializer(() -> this.temperature.set(encode(temperature)));
    }

    private byte[] encode(int temperature) {
        return String.format(TEXT_FORMAT, temperature).getBytes(TEXT_CHARSET);
    }

}
