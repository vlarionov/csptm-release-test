package ru.advantum.csptm.jep.hub.ibrd.row;

import ru.advantum.csptm.jep.hub.ibrd.BaseStruct;

import java.nio.charset.Charset;
import java.util.Arrays;

public class RowTextStruct extends BaseStruct {
    private static final Charset TEXT_CHARSET = Charset.forName("windows-1251");

    public final ByteSet text;

    public final Unsigned8 end;


    public RowTextStruct(String text) {
        byte[] textBytes = text.getBytes(TEXT_CHARSET);
        this.text = new ByteSet(textBytes.length);
        this.end = new Unsigned8();

        addInitializer(() -> {
            this.text.set(textBytes);
            this.end.set((short) 0);
        });
    }

    @Override
    public String toString() {
        return "RowTextStruct{" +
                "text=" + Arrays.toString(text.get()) +
                ", end=" + end.get() +
                '}';
    }

}
