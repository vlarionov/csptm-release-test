package ru.advantum.csptm.jep.hub.ibrd.row;

import ru.advantum.csptm.jep.hub.ibrd.BaseStruct;


public class TelematicsFlagsStruct extends BaseStruct {

    public final BitField coordsEmpty = new BitField(1);
    public final BitField coordsFalse = new BitField(1);
    public final BitField unusedBits= new BitField(6);

    public  boolean isCoordsValid() {
        return (coordsEmpty.intValue() == 0) && (coordsFalse.intValue() == 0);
    }

}
