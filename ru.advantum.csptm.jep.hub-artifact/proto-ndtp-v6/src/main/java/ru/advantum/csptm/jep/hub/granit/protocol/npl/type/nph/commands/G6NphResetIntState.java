package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.commands;

import ru.advantum.csptm.jep.hub.granit.protocol.G6BaseCommand;

public class G6NphResetIntState extends G6BaseCommand {

    public final Unsigned32 state = new Unsigned32();

    @Override
    public String extractString() {
        return this.getClass().getSimpleName() + ": {"
                + "state=" + state.get()
                + "}";
    }

}
