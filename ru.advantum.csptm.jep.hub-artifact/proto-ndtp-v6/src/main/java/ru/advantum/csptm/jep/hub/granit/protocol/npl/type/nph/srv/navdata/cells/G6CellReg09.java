package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class G6CellReg09 extends G6Base implements IG6BaseCell {

    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Данные от регистратора . Type=9, N=0 (1…n)
     * поле	Длина	тип	описание
     * 40	struct
     * <ID>	8	unsigned int64	ID регистратора
     * <Name>	32	unsigned int8	Массив данных
     * считанный из регистратора размером 32 байта. Не воспринимать как строку символов заканчивающейся 0.
     * </editor-fold>
     */

    private final Signed64 id = new Signed64();
    private final Unsigned8[] name = array(new Unsigned8[32]);

    public G6CellReg09(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    private byte[] getNameBytes() {
        byte[] ret = new byte[name.length];
        for (int i = 0; i < ret.length; i++) {
            ret[i] = (byte) name[i].get();
        }
        return ret;
    }

    @Override
    public String extractString() {
        return "G6CellReg09{" + "id=" + id + ", name=" + Arrays.toString(name) + '}';
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        Map<Integer, BigDecimal> sensors = new HashMap<>(3);

        sensors.put(ndtpConfig.cell09Numbers.id, BigDecimal.valueOf(id.get()));
        // TODO: не отдаем name!

        return sensors;
    }

    @Override
    public int getCell() {
        return 9;
    }

}
