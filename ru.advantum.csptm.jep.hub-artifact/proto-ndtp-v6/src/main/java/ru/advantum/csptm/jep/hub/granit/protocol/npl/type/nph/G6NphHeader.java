package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph;

import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.external.device.G6NphSrvExternalDeviceConstant;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class G6NphHeader extends G6Base {

    /*common packets for all services*/
    public final static int NPH_RESULT = 0;
    private final BitField serviceId = new BitField(16);
    private final BitField type = new BitField(16);
    private final BitField nph_flag_request = new BitField(1);
    private final BitField flags$ = new BitField(15);
    private final BitField requestId = new BitField(32);

    public G6NphHeader() {
    }

    public G6NphHeader(int serviceId, int type, boolean nph_flag_request, long requestId) {
        this.serviceId.set(serviceId);
        this.type.set(type);
        this.setNph_flag_request(nph_flag_request);
        this.requestId.set(requestId);
    }

    public boolean getNph_flag_request() {
        return this.nph_flag_request.intValue() == 1;
    }

    public void setNph_flag_request(boolean nph_flag_request) {
        this.nph_flag_request.set(nph_flag_request ? 1 : 0);
    }

    public int getFlags$() {
        return flags$.intValue();
    }

    public void setFlags$(int flags$) {
        this.flags$.set(flags$);
    }

    public int getServiceId() {
        return serviceId.intValue();
    }

    public int getType() {
        return type.intValue();
    }

    public long getRequestId() {
        return requestId.longValue();
    }

    @Override
    public String extractString() {
        return "NphHeader{" + "serviceId=" + serviceId +
                ", type=" + type
                + ", flags:{"
                + "nph_flag_request=>" + getNph_flag_request() + ","
                + "flags=" + flags$
                + "}, requestId=" + requestId + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.serviceId.intValue());
        hash = 23 * hash + Objects.hashCode(this.type.intValue());
        hash = 23 * hash + Objects.hashCode(this.getNph_flag_request());
        hash = 23 * hash + Objects.hashCode(this.flags$.intValue());
        hash = 23 * hash + Objects.hashCode(this.requestId.intValue());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final G6NphHeader other = (G6NphHeader) obj;
        return ((this.serviceId.intValue() == other.serviceId.intValue())
                && (this.type.intValue() == other.type.intValue())
                && (this.getNph_flag_request() == other.getNph_flag_request())
                && (this.flags$.intValue() == other.flags$.intValue())
                && (this.requestId.intValue() == other.requestId.intValue()));
    }

    public static class G6NphServiceType {

        public final static int NPH_SRV_GENERIC_CONTROLS = 0;
        public final static int NPH_SRV_NAVDATA = 1;
        public final static int NPH_SRV_FILE_TRANSFER = 3;
        public final static int NPH_SRV_CLIENT_LIST = 4;
        public final static int NPH_SRV_EXTERNAL_DEVICE = 5;
        public final static int NPH_SRV_DEBUG = 6;

        private static final Map<String, Integer> values = new HashMap<>();

        static {
            values.put("NPH_SRV_GENERIC_CONTROLS", NPH_SRV_GENERIC_CONTROLS);
            values.put("NPH_SRV_NAVDATA", NPH_SRV_NAVDATA);
            values.put("NPH_SRV_FILE_TRANSFER", NPH_SRV_FILE_TRANSFER);
            values.put("NPH_SRV_CLIENT_LIST", NPH_SRV_CLIENT_LIST);
            values.put("NPH_SRV_EXTERNAL_DEVICE", NPH_SRV_EXTERNAL_DEVICE);
            values.put("NPH_SRV_DEBUG", NPH_SRV_DEBUG);
        }

        public static int lookup(String name) {
            for (Map.Entry<String, Integer> e : values.entrySet()) {
                if (e.getKey().equals(name)) {
                    return e.getValue();
                }
            }
            return G6NphSrvExternalDeviceConstant.CONSTANT_NOT_FOUND;
        }

        public static String lookup(int type) {
            for (Map.Entry<String, Integer> entry : values.entrySet()) {
                if (entry.getValue().equals(type)) {
                    return entry.getKey();
                }
            }
            return G6NphSrvExternalDeviceConstant.CONSTANT_VALUE_NOT_FOUND;
        }

    }
}
