package ru.advantum.csptm.jep.hub.granit.protocol;

public class G6BaseCommand extends G6Base {
    private CommandSendStatus cmdSendStatus = CommandSendStatus.Undefined;

    public CommandSendStatus getCmdSendStatus() {
        return cmdSendStatus;
    }

    public void setCmdSendStatus(CommandSendStatus cmdSendStatus) {
        this.cmdSendStatus = cmdSendStatus;
    }

    public enum CommandSendStatus {
        Ready,
        Sent,
        UnitUnavailable,
        Undefined;

        public static CommandSendStatus lookup(int code) {
            for (CommandSendStatus css : CommandSendStatus.values()) {
                if (css.ordinal() == code) {
                    return css;
                }
            }
            return Undefined;
        }

        public static int lookup(CommandSendStatus c) {
            for (CommandSendStatus css : CommandSendStatus.values()) {
                if (css.equals(c)) {
                    return css.ordinal();
                }
            }
            return Undefined.ordinal();
        }
    }
}
