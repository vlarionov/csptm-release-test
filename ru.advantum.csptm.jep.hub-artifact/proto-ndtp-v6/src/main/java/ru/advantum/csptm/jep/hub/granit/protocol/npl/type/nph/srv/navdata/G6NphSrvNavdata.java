package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata;

import javolution.io.Struct;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

/**
 * @author Kotin <kotin@advantum.ru>
 *         <p>
 *         Навигационные данные передаются в типе обслуживания NPH_SRV_NAVDATA.
 *         Существует два типа пакетов: --NPH_SND_REALTIME – передача навигационных
 *         данных в реальном времени; --NPH_SND_HISTORY – передача навигационных данных
 *         сохраненной в памяти устройства («ретроспективы»).
 */
public class G6NphSrvNavdata extends G6Base {

    Struct.Unsigned8 type = new Struct.Unsigned8();

    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Тип ячейки (определяет длину и содержимое). Различаются следующие типы: 0
     * – основные навигационные данные; 1 – дополнительные навигационные данные;
     * 2 – данные от внутренних портов; 3 - данные от устройства Корона; 4 -
     * данные от устройства IRMA
     * </editor-fold>
     */

    Struct.Unsigned8 number = new Struct.Unsigned8();

    /*
     * Определяет навигационный приемник: N=0 – GPS приемник, N=1 – GLONASS
     * приемник.
     */
    public G6NphSrvNavdata() {
    }

    public G6NphSrvNavdata(short type, short number) {
        this.type.set(type);
        this.number.set(number);
    }

    public short getNumber() {
        return number.get();
    }

    public short getType() {
        return type.get();
    }

    @Override
    public String extractString() {
        return "\nNphSrvNavdata{" + "type=" + type + ", number=" + number + '}';
    }
}
