package ru.advantum.csptm.jep.hub.granit.protocol.npl.error;

/**
 * Created by zlobina on 28.09.15.
 * ошибки протокола NPL
 */
public enum ErrorCodesEnum {
    NPL_ERR_OK(0, "запрос выполнен успешно"),
    NPL_ERR_DECRYPTION_FAILED(1, null),
    NPL_ERR_CRC_FAILED(2, null),
    NPL_ERR_UNDEFINED(0xFFFFFFFF, "код для ошибок не имеющих описания"),
    NPL_ERR_INVALID_PEER_ADDRESS(100, "недопустимый адрес участника соединения"),
    NPL_ERR_PEER_NOT_AVAILABLE(101, "участника соединения недоступен"),
    NPL_ERR_PEER_PERM_DENIED(102, "доступ запрещен");

    private final int code;
    private final String note;

    ErrorCodesEnum(int code, String note) {
        this.code = code;
        this.note = note;
    }

    public int getCode() {
        return code;
    }

}
