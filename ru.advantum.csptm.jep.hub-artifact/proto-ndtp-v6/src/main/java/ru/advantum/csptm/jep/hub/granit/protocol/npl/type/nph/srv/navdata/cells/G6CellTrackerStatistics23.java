package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;

@SuppressWarnings("unused")
public class G6CellTrackerStatistics23 extends G6Base implements IG6BaseCell {

    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Статистические данные от трекера
     * Type=23, N=0 (1…n)
     * поле	длина	Тип	описание
     * 16	struct
     * <cnt_ack>	4	unsigned int32	Количество подтвержденных отметок с начала работы прибора
     * <cnt_ack_realtime>	4	unsigned int32	Количество отметок, подтвержденных как realtime (с начала работы прибора)
     * <cnt_noack>	4	unsigned int32	Количество отметок, которые не получилось отправить (с начала работы прибора)
     * <cnt_connect>	4	unsigned int32	Количество соединений с сервером (с начала работы прибора)
     * </editor-fold>
     */
    private final Unsigned32 cnt_ack = new Unsigned32();
    private final Unsigned32 cnt_ack_realtime = new Unsigned32();
    private final Unsigned32 cnt_noack = new Unsigned32();
    private final Unsigned32 cnt_connect = new Unsigned32();

    public G6CellTrackerStatistics23(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    @Override
    public String extractString() {
        return "G6CellTrackerStatistics23{" +
                "cnt_ack=" + cnt_ack +
                ", cnt_ack_realtime=" + cnt_ack_realtime +
                ", cnt_noack=" + cnt_noack +
                ", cnt_connect=" + cnt_connect +
                '}';
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        return Collections.emptyMap();
    }

    @Override
    public int getCell() {
        return 23;
    }

}
