package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;

@SuppressWarnings("unused")
public class G6CellAlcohol2nd21 extends G6Base implements IG6BaseCell {

    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Данные от спиртометр2
     * Type=21, N=0 (1…n)
     * поле	длина	Тип	описание
     * 180	struct
     * Status
     * 2	unsigned int16	(бит   0) Device Status Flag = 1:
     * считано текущее состояние устройства
     * ( всегда присутствует в ячейке ), действительны:
     * Current Section ( номер отсека текущей операции )
     * <p>
     * (бит   1) Last Record Flag = 1:
     * считана запись архива, действительны:
     * <p>
     * AlcoEvent_Section                  ( тип события / отсек ),
     * Sensor_Serial_Number ( серийный номер датчика ),
     * Counter_Start_Value  ( счетчик в начале операции ),
     * Counter_Stop_Value     ( счетчик в конце операции ),
     * Starting Time                    ( время начала операции ),
     * End_Time              ( время завершения опрерации ),
     * Final_Temperatur ( температура в конце операции ).
     * <p>
     * (бит   2) Record Counter Flag = 1:
     * считан счетчик записей архива
     * ( если он равен 0, то чтение архива не производится )
     * <p>
     * (бит   3) Temperature Flag = 1: температура считана
     * <p>
     * (бит   4) Readed Record Flag = 1:
     * считана последняя прочитанная запись архива
     * <p>
     * ( бит   5) Current Time Flag = 1:
     * считано текущее время устройства
     * <p>
     * (бит   6) Current Volume Flag = 1:
     * считан текущий слитый / налитый объём
     * <p>
     * (бит   7) Product Type Flag = 1:
     * считан код вида продукции
     * <p>
     * (бит   8) Product Code Flag = 1:
     * считан код продукции
     * <p>
     * (бит   9) Organization Code Flag = 1:
     * считан код организации
     * <p>
     * (бит 12) Installation Date & Time Flag = 1:
     * считана дата и время установки средства измерения
     * <p>
     * (бит 13) Error Description Flag = 1:
     * считано описание ошибки
     * <p>
     * (бит 14) Status Express Cell = 1: внеочередная ячейка ( могут быть недействительны ( ...Flag = 0 )
     * любые данные, кроме Device_Status_Section )
     * RecordCounter	2	unsigned int16	количество записей в архиве
     * Device_Status_Section	2	unsigned int16	текущее состояние устройства: тип события, номер отсека
     * Current_Time	4	unsigned int32	текущее время устройства
     * Current_Volume	2	unsigned int16	текущий слитый / налитый объём
     * AlcoEvent_Section	2	unsigned int16	запись: тип события / номер отсека
     * Sensor_Serial_Number	8	unsigned int64	запись: серийный номер датчика
     * Counter_Start_Value	4	unsigned int32	запись: показания счетчика в начале операции
     * Counter_Stop_Value	4	unsigned int32	запись: показания счетчика в конце операции
     * Starting_Time	4	unsigned int32	запись: время начала операции
     * End_Time	4	unsigned int32	запись: время завершения операции
     * Final_Temperature	2	unsigned int16	запись: температура жидкости в конце операции
     * Temperature	2	unsigned int16	Температура t1, t2
     * Reserved16	2	unsigned int16	Reserved
     * Readed_
     * AlcoEvent_Section	2	unsigned int16	последняя прочитанная запись:
     * тип события / номер отсека
     * Readed_
     * Sensor_Serial_Number	8	unsigned int64	последняя прочитанная запись:
     * серийный номер датчика
     * Readed_
     * Counter_Start_Value	4	unsigned int32	последняя прочитанная запись:
     * показания счетчика в начале операции
     * Readed_
     * Counter_Stop_Value	4	unsigned int32	последняя прочитанная запись:
     * показания счетчика в конце операции
     * Readed_Starting_Time	4	unsigned int32	последняя прочитанная запись:
     * время начала операции
     * Readed_End_Time	4	unsigned int32	последняя прочитанная запись:
     * время завершения операции
     * Readed_
     * Final_Temperature	2	unsigned int16	последняя прочитанная запись:
     * температура жидкости в конце операции
     * Product_Type_dentifier	4	unsigned int8	код вида продукции
     * Product_Code	20	unsigned int8	код продукции
     * Organization_Code	18	unsigned int8	код организации
     * Installation_Date_Time	16	unsigned int8	дата и время установки средства измерения
     * Error_Description	50	unsigned int8	описание ошибки
     * </editor-fold>
     */
    private final Unsigned16 status = new Unsigned16();
    private final Unsigned16 recordCounter = new Unsigned16();
    private final Unsigned16 deviceStatusSection = new Unsigned16();
    private final Unsigned32 currentTime = new Unsigned32();
    private final Unsigned16 currentVolume = new Unsigned16();
    private final Unsigned16 alcoEventSection = new Unsigned16();
    private final ByteSet sensorSerialNumber = new ByteSet(8);
    private final Unsigned32 counterStartValue = new Unsigned32();
    private final Unsigned32 counterStopValue = new Unsigned32();
    private final Unsigned32 startTime = new Unsigned32();
    private final Unsigned32 endTime = new Unsigned32();
    private final Unsigned16 finalTemperature = new Unsigned16();
    private final Unsigned16 temperature = new Unsigned16();
    private final Unsigned16 reserved16 = new Unsigned16();

    private final Unsigned16 readAlcoEventSection = new Unsigned16();
    private final ByteSet readSensorSerialNumber = new ByteSet(8);
    private final Unsigned32 readCounterStartValue = new Unsigned32();
    private final Unsigned32 readCounterStopValue = new Unsigned32();
    private final Unsigned32 readStartTime = new Unsigned32();
    private final Unsigned32 readEndTime = new Unsigned32();
    private final Unsigned16 readFinalTemperature = new Unsigned16();

    private final Unsigned32 productType = new Unsigned32();
    private final ByteSet productCode = new ByteSet(20);
    private final ByteSet organizationCode = new ByteSet(18);
    private final ByteSet installDateTime = new ByteSet(16);
    private final ByteSet errorDescription = new ByteSet(50);

    public G6CellAlcohol2nd21(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    @Override
    public String extractString() {
        return "G6CellAlcohol2nd21{" +
                "status=" + status +
                '}';
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        return Collections.emptyMap();
    }

    @Override
    public int getCell() {
        return 21;
    }

}
