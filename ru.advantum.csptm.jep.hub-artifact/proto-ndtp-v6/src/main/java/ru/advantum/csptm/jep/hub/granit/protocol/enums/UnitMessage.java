package ru.advantum.csptm.jep.hub.granit.protocol.enums;

import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.G6NphHeader;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.G6NphResult;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.external.device.G6NphNPHSedDeviceResult;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.external.device.G6NphSedDeviceTitleData;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.external.device.G6NphSrvExternalDeviceConstant;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.generic.controls.*;

public enum UnitMessage {
    NPH_RESULT(G6NphHeader.NPH_RESULT, G6NphResult.class),
    NPH_SIM_IMSI(G6NhpSrvGenericControlsConstant.Packages.NPH_SIM_IMSI, G6NphSimIMSI.class),
    NPH_SGC_CONN_AUTH_STRING(G6NhpSrvGenericControlsConstant.Packages.NPH_SGC_CONN_AUTH_STRING, G6Base.class),
    NPH_SGC_SERVICE_REQUEST(G6NhpSrvGenericControlsConstant.Packages.NPH_SGC_SERVICE_REQUEST, G6Base.class),
    NPH_SGC_PEER_DESC(G6NhpSrvGenericControlsConstant.Packages.NPH_SGC_PEER_DESC, G6NphPeerDesc.class),
    NPH_PRDO(G6NhpSrvGenericControlsConstant.Packages.NPH_PRDO, G6NphPrdo.class),
    NPH_PRIA(G6NhpSrvGenericControlsConstant.Packages.NPH_PRIA, G6NphPria.class),
    NPH_PRNAV(G6NhpSrvGenericControlsConstant.Packages.NPH_PRNAV, G6NphPrnav.class),
    NPH_INFO(G6NhpSrvGenericControlsConstant.Packages.NPH_INFO, G6NphInfo.class),
    NPH_BALANCE(G6NhpSrvGenericControlsConstant.Packages.NPH_BALANCE, G6NphBalance.class),
    NPH_ROUTE_AUTOINFORMER(G6NhpSrvGenericControlsConstant.Packages.NPH_ROUTE_AUTOINFORMER, G6NphRouteAutoinformer.class),
    NPH_SED_DEVICE_RESULT(G6NhpSrvGenericControlsConstant.Packages.NPH_SED_DEVICE_RESULT, G6NphNPHSedDeviceResult.class),
    NPH_SED_DEVICE_TITLE_DATA(G6NphSrvExternalDeviceConstant.Packages.NPH_SED_DEVICE_TITLE_DATA, G6NphSedDeviceTitleData.class),
    UNDEFINED(Integer.MIN_VALUE, null);

    private int code;
    private Class<? extends G6Base> clazz;

    UnitMessage(int code, Class<? extends G6Base> clazz) {
        setCode(code);
        setClazz(clazz);
    }

    public static Class<? extends G6Base> lookupExecutor(int code) {
        for (UnitMessage um : UnitMessage.values()) {
            if (um.getCode() == code) {
                return um.getClazz();
            }
        }
        return UNDEFINED.getClazz();
    }

    public static UnitMessage lookup(int code) {
        for (UnitMessage um : UnitMessage.values()) {
            if (um.getCode() == code) {
                return um;
            }
        }
        return UNDEFINED;
    }

    public static UnitMessage lookupByClassName(String className) {
        for (UnitMessage um : UnitMessage.values()) {
            if (um != UNDEFINED && um.getClazz().getSimpleName().equals(className)) {
                return um;
            }
        }
        return UNDEFINED;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Class<? extends G6Base> getClazz() {
        return clazz;
    }

    public void setClazz(Class<? extends G6Base> clazz) {
        this.clazz = clazz;
    }
}
