/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@SuppressWarnings("unused")
public class G6CellIntSensor02 extends G6Base implements IG6BaseCell {

    private final Unsigned16 an_in0 = new Unsigned16();
    private final Unsigned16 an_in1 = new Unsigned16();
    private final Unsigned16 an_in2 = new Unsigned16();
    private final Unsigned16 an_in3 = new Unsigned16();
    private final Unsigned8 di_in = new Unsigned8();
    private final Unsigned8 di_out = new Unsigned8();
    private final Unsigned16 di0_counter = new Unsigned16();
    private final Unsigned16 di1_counter = new Unsigned16();
    private final Unsigned16 di2_counter = new Unsigned16();
    private final Unsigned16 di3_counter = new Unsigned16();
    private final Unsigned32 odometer = new Unsigned32();
    private final Unsigned8 csq = new Unsigned8();
    private final Unsigned8 gprs_state = new Unsigned8();
    private final Unsigned8 accel_energy = new Unsigned8();
    private final Signed8 ext_volt = new Signed8();

    public int getCsq() {
        return csq.get();
    }

    public int getGprs_state() {
        return gprs_state.get();
    }

    public int getAccel_energy() {
        return accel_energy.get();
    }

    public int getExt_volt() {
        return ext_volt.get();
    }

    public G6CellIntSensor02(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    public long getOdometer() {
        return odometer.get();
    }

    public int getDi_in() {
        return di_in.get();
    }

    public int getAn_in0() {
        return an_in0.get();
    }

    public int getAn_in1() {
        return an_in1.get();
    }

    public int getAn_in2() {
        return an_in2.get();
    }

    public int getAn_in3() {
        return an_in3.get();
    }

    public int getDi_out() {
        return di_out.get();
    }

    public int getDi0_counter() {
        return di0_counter.get();
    }

    public int getDi1_counter() {
        return di1_counter.get();
    }

    public int getDi2_counter() {
        return di2_counter.get();
    }

    public int getDi3_counter() {
        return di3_counter.get();
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        Map<Integer, BigDecimal> sensors = new ConcurrentHashMap<>(14);

        sensors.put(ndtpConfig.cell02Numbers.an_in0, BigDecimal.valueOf(an_in0.get()));
        sensors.put(ndtpConfig.cell02Numbers.an_in1, BigDecimal.valueOf(an_in1.get()));
        sensors.put(ndtpConfig.cell02Numbers.an_in2, BigDecimal.valueOf(an_in2.get()));
        sensors.put(ndtpConfig.cell02Numbers.an_in3, BigDecimal.valueOf(an_in3.get()));

        sensors.put(ndtpConfig.cell02Numbers.di_in, BigDecimal.valueOf(di_in.get()));
        sensors.put(ndtpConfig.cell02Numbers.di_out, BigDecimal.valueOf(di_out.get()));

        sensors.put(ndtpConfig.cell02Numbers.di0_counter, BigDecimal.valueOf(di0_counter.get()));
        sensors.put(ndtpConfig.cell02Numbers.di1_counter, BigDecimal.valueOf(di1_counter.get()));
        sensors.put(ndtpConfig.cell02Numbers.di2_counter, BigDecimal.valueOf(di2_counter.get()));
        sensors.put(ndtpConfig.cell02Numbers.di3_counter, BigDecimal.valueOf(di3_counter.get()));

        sensors.put(ndtpConfig.cell02Numbers.csq, BigDecimal.valueOf(csq.get()));
        sensors.put(ndtpConfig.cell02Numbers.gprs_state, BigDecimal.valueOf(gprs_state.get()));
        sensors.put(ndtpConfig.cell02Numbers.accel_energy, BigDecimal.valueOf(accel_energy.get()));
        sensors.put(ndtpConfig.cell02Numbers.ext_volt, BigDecimal.valueOf(ext_volt.get()));
        sensors.put(ndtpConfig.cell02Numbers.odometer, BigDecimal.valueOf(odometer.get()));

        return sensors;
    }

    @Override
    public String extractString() {
        return "G6CellIntSensor02\n\t{" + "an_in0=" + an_in0 + ", an_in1=" + an_in1 + ", an_in2=" + an_in2 + ", an_in3=" + an_in3 + ", di_in=" + di_in + ", di_out=" + di_out + ", di0_counter=" + di0_counter + ", di1_counter=" + di1_counter + ", di2_counter=" + di2_counter + ", di3_counter=" + di3_counter + ", odometer=" + odometer + ", csq=" + csq + ", gprs_state=" + gprs_state + ", accel_energy=" + accel_energy + ", ext_volt=" + ext_volt + '}';
    }

    @Override
    public int getCell() {
        return 2;
    }

}
