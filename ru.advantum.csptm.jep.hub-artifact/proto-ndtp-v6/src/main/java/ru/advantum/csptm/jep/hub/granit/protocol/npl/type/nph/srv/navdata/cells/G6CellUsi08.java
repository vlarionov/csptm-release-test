/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@SuppressWarnings("unused")
public class G6CellUsi08 extends G6Base implements IG6BaseCell {

    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Данные от топливного датчика УЗИ-M . Type=8, N=0 (1…n)
     * поле	длина	тип	Описание
     * 6	struct
     * <det_status>	1	unsigned int8	Состояние датчика топлива.
     * <level_mm>	2	unsigned int16	Уровень топлива в баке в мм
     * <level_l>	2	unsigned int16	Уровень топлива в баке в литрах
     * <temperature>	1	unsigned int8	Температура бака
     * </editor-fold>
     */

    private final Unsigned8 det_status = new Unsigned8();
    private final Unsigned16 level_mm = new Unsigned16();
    private final Unsigned16 level_l = new Unsigned16();
    private final Unsigned8 temperature = new Unsigned8();

    public G6CellUsi08(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    public G6CellUsi08(GranitHubConfig.Ndtp ndtpConfig, int cellNumber) {
        this(ndtpConfig);
        this.cellNumber = cellNumber;
    }


    @Override
    public String extractString() {
        return "G6CellUsi08\n\t{ cell-num=" + getCellNumber() +
                ",det_status=" + det_status +
                ", level_mm=" + level_mm +
                ", level_l=" + level_l +
                ", temperature=" + temperature + '}';
    }

    public int getLevel_mm() {
        return level_mm.get();
    }

    public int getLevel_l() {
        return level_l.get();
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        Map<Integer, BigDecimal> sensors = new ConcurrentHashMap<>(4);
        int offset = getCellNumber() * 4;
        sensors.put(ndtpConfig.cell08Numbers.det_status + offset, new BigDecimal(this.det_status.get()));
        sensors.put(ndtpConfig.cell08Numbers.level_mm + offset, new BigDecimal(getLevel_mm()));
        sensors.put(ndtpConfig.cell08Numbers.level_l + offset, new BigDecimal(getLevel_l()));
        sensors.put(ndtpConfig.cell08Numbers.temperature08 + offset, new BigDecimal(this.temperature.get()));

        return sensors;
    }

    @Override
    public int getCell() {
        return 8;
    }
}
