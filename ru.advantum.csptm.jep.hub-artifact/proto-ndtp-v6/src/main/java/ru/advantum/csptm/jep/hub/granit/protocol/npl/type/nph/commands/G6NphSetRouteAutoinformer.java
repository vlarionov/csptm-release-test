package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.commands;

import ru.advantum.csptm.jep.hub.granit.protocol.G6BaseCommand;

public class G6NphSetRouteAutoinformer extends G6BaseCommand {

    public final ByteSet route = new ByteSet(30);
    public final ByteSet run = new ByteSet(30);

    @Override
    public String extractString() {
        return this.getClass().getSimpleName() + ": {"
                + "route=" + route.asString()
                + "run=" + run.asString()
                + "}";
    }

}
