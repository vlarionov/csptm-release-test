package ru.advantum.csptm.jep.hub.granit.protocol.enums;

import ru.advantum.csptm.jep.hub.granit.protocol.G6BaseCommand;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.G6NphHeader;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.commands.*;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.external.device.G6NphSedDeviceTitleData;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.external.device.G6NphSrvExternalDeviceConstant;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.generic.controls.G6NhpSrvGenericControlsConstant;

public enum ServerCommand {
    NPH_SGC_PEER_DESC_REQUEST(G6NhpSrvGenericControlsConstant.Packages.NPH_SGC_PEER_DESC_REQUEST, G6BaseCommand.class), // w/o params
    NPH_SET_PRDO(G6NhpSrvGenericControlsConstant.Packages.NPH_SET_PRDO, G6NphSetPrdo.class),
    NPH_GET_PRDO(G6NhpSrvGenericControlsConstant.Packages.NPH_GET_PRDO, G6NphGetPrdo.class),
    NPH_SET_PRIA(G6NhpSrvGenericControlsConstant.Packages.NPH_SET_PRIA, G6NphSetPria.class),
    NPH_SET_PRIA_EXT(G6NhpSrvGenericControlsConstant.Packages.NPH_SET_PRIA, G6NphSetPria.class), // w/o params разница в номере сервера. пусть так взлетитт
    NPH_GET_PRIA(G6NhpSrvGenericControlsConstant.Packages.NPH_GET_PRIA, G6BaseCommand.class), // w/o params
    NPH_SET_PRNAV(G6NhpSrvGenericControlsConstant.Packages.NPH_SET_PRNAV, G6NphSetPrnav.class),
    NPH_GET_PRNAV(G6NhpSrvGenericControlsConstant.Packages.NPH_GET_PRNAV, G6BaseCommand.class), // w/o params
    NPH_GET_INFO(G6NhpSrvGenericControlsConstant.Packages.NPH_GET_INFO, G6BaseCommand.class), // w/o params
    NPH_GET_BALANCE(G6NhpSrvGenericControlsConstant.Packages.NPH_GET_BALANCE, G6NphGetBalance.class),
    NPH_SET_CURTIME(G6NhpSrvGenericControlsConstant.Packages.NPH_SET_CURTIME, G6NphSetCurtime.class),
    NPH_SET_ROUTE_AUTOINFORMER(G6NhpSrvGenericControlsConstant.Packages.NPH_SET_ROUTE_AUTOINFORMER, G6NphSetRouteAutoinformer.class),
    NPH_GET_ROUTE_AUTOINFORMER(G6NhpSrvGenericControlsConstant.Packages.NPH_GET_ROUTE_AUTOINFORMER, G6BaseCommand.class), // w/o params
    NPH_RESET_INT_STATE(G6NhpSrvGenericControlsConstant.Packages.NPH_RESET_INT_STATE, G6NphResetIntState.class),
    NPH_GET_SIM_IMSI(G6NhpSrvGenericControlsConstant.Packages.NPH_GET_SIM_IMSI, G6BaseCommand.class), // w/o params
    NPH_SET_LOADFIRM(G6NhpSrvGenericControlsConstant.Packages.NPH_SET_LOADFIRM, G6NphSetLoadfirm.class),
    NPH_SED_DEVICE_TITLE_DATA(G6NphSrvExternalDeviceConstant.Packages.NPH_SED_DEVICE_TITLE_DATA, G6NphSedDeviceTitleData.class,
            G6NphHeader.G6NphServiceType.NPH_SRV_EXTERNAL_DEVICE),
    NPH_SGC_SERVICE_REQUEST(G6NhpSrvGenericControlsConstant.Packages.NPH_SGC_SERVICE_REQUEST, G6NphSgcServiceRequest.class),
    UNDEFINED(Integer.MIN_VALUE, null);


    private int code;
    private Class<? extends G6BaseCommand> clazz;
    private int serviceId = G6NphHeader.G6NphServiceType.NPH_SRV_GENERIC_CONTROLS;

    private ServerCommand(int code, Class<? extends G6BaseCommand> clazz) {
        setCode(code);
        setClazz(clazz);
    }

    private ServerCommand(int code, Class<? extends G6BaseCommand> clazz, int serviceId) {
        this(code, clazz);
        setServiceId(serviceId);
    }

    public static Class<? extends G6BaseCommand> lookupExecutor(int code) {
        for (ServerCommand um : ServerCommand.values()) {
            if (um.getCode() == code) {
                return um.getClazz();
            }
        }
        return UNDEFINED.getClazz();
    }

    public static ServerCommand lookup(String name) {
        for (ServerCommand um : ServerCommand.values()) {
            if (um.name().toUpperCase().equals(name.toUpperCase())) {
                return um;
            }
        }
        return UNDEFINED;
    }

    public static ServerCommand lookup(int code) {
        for (ServerCommand um : ServerCommand.values()) {
            if (um.getCode() == code) {
                return um;
            }
        }
        return UNDEFINED;
    }

    public static ServerCommand lookupByClassName(String className) {
        for (ServerCommand um : ServerCommand.values()) {
            if (um.getClazz().getSimpleName().equals(className)) {
                return um;
            }
        }
        return UNDEFINED;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Class<? extends G6BaseCommand> getClazz() {
        return clazz;
    }

    public void setClazz(Class<? extends G6BaseCommand> clazz) {
        this.clazz = clazz;
    }
}