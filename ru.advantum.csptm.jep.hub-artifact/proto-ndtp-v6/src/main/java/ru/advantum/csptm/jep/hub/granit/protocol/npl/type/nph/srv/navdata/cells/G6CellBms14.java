/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import javolution.io.Struct;
import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class G6CellBms14 extends G6Base implements IG6BaseCell {

    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Данные от датчика BMS Liaz. Type=14, N=0 (1…n)
     * поле	длина	тип	описание
     * 13	struct
     * <max_temperature>	2	unsigned int16	Максимальная температура
     * <min_cell_voltage>	2	unsigned int16	Минимальное напряжение ячейки * 0,0015 (Вольт)
     * <max_cell_voltage>	2	unsigned int16	Максимальное напряжение ячейки * 0,0015 (Вольт)
     * <voltage>	4	unsigned int32	Общее напряжение в вольтах * 0,0015 (Вольт)
     * <code_error>	1	unsigned int8	Коды ошибок от устройства (если установлен бит, то)
     * 0 бит — Зафиксировано превышение допустимого напряжения на одной из ячеек
     * 1 бит — зафиксировано напряжение на одной из ячеек ниже допустимого уровня
     * 2 бит — зафиксировано превышение температуры на одной из ячеек
     * 3 бит — зафиксировано превышение максимально допустимого тока заряда или разряда
     * <current>	4	unsigned int32	Ток батареи в Амперах
     * </editor-fold>
     */

    private final Struct.BitField max_temperature = new Struct.BitField(16);
    private final Struct.BitField min_cell_voltage = new Struct.BitField(16);
    private final Struct.BitField max_cell_voltage = new Struct.BitField(16);

    private final Struct.BitField voltage = new Struct.BitField(32);

    private final Struct.BitField code_error0 = new Struct.BitField(1);
    private final Struct.BitField code_error1 = new Struct.BitField(1);
    private final Struct.BitField code_error2 = new Struct.BitField(1);
    private final Struct.BitField code_error3 = new Struct.BitField(1);
    private final Struct.BitField code_error$ = new Struct.BitField(4);
    private final Struct.BitField current = new Struct.BitField(32);

    public G6CellBms14(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    public boolean getCode_error0() {
        return this.code_error0.intValue() == 1;
    }

    public void setCode_error0(boolean code_error0) {
        this.code_error0.set(code_error0 ? 1 : 0);
    }

    public boolean getCode_error1() {
        return this.code_error1.intValue() == 1;
    }

    public void setCode_error1(boolean code_error1) {
        this.code_error1.set(code_error1 ? 1 : 0);
    }

    public boolean getCode_error2() {
        return this.code_error2.intValue() == 1;
    }

    public void setCode_error2(boolean code_error2) {
        this.code_error2.set(code_error2 ? 1 : 0);
    }

    public boolean getCode_error3() {
        return this.code_error3.intValue() == 1;
    }

    public void setCode_error3(boolean code_error3) {
        this.code_error3.set(code_error3 ? 1 : 0);
    }

    public void setCode_error$(short code_error$) {
        this.code_error$.set(code_error$);
    }

    public short getCode_error$() {
        return code_error$.shortValue();
    }

    public void setCode_error$(int code_error$) {
        this.code_error$.set(code_error$);
    }

    @Override
    public String extractString() {
        return "G6CellBms14{" + "max_temperature=" + max_temperature +
                ", min_cell_voltage=" + min_cell_voltage +
                ", max_cell_voltage=" + max_cell_voltage +
                ", voltage=" + voltage +
                ", code_error0=" + code_error0 +
                ", code_error1=" + code_error1 +
                ", code_error2=" + code_error2 +
                ", code_error3=" + code_error3 +
                ", code_error$=" + code_error$ +
                ", current=" + current + '}';
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        Map<Integer, BigDecimal> sensors = new HashMap<>(9);

        sensors.put(ndtpConfig.cell14Numbers.max_temperature, BigDecimal.valueOf(max_temperature.intValue()));

        sensors.put(ndtpConfig.cell14Numbers.min_cell_voltage, BigDecimal.valueOf(min_cell_voltage.intValue()));
        sensors.put(ndtpConfig.cell14Numbers.max_cell_voltage, BigDecimal.valueOf(max_cell_voltage.intValue()));
        sensors.put(ndtpConfig.cell14Numbers.voltage, BigDecimal.valueOf(voltage.intValue()));

        sensors.put(ndtpConfig.cell14Numbers.code_error0, BigDecimal.valueOf(getCode_error0() ? 1 : 0));
        sensors.put(ndtpConfig.cell14Numbers.code_error1, BigDecimal.valueOf(getCode_error1() ? 1 : 0));
        sensors.put(ndtpConfig.cell14Numbers.code_error2, BigDecimal.valueOf(getCode_error2() ? 1 : 0));
        sensors.put(ndtpConfig.cell14Numbers.code_error3, BigDecimal.valueOf(getCode_error3() ? 1 : 0));

        sensors.put(ndtpConfig.cell14Numbers.current, BigDecimal.valueOf(current.intValue()));
        sensors.put(ndtpConfig.cell14Numbers.code_error$, BigDecimal.valueOf(code_error$.longValue()));

        return sensors;
    }

    @Override
    public int getCell() {
        return 14;
    }

}
