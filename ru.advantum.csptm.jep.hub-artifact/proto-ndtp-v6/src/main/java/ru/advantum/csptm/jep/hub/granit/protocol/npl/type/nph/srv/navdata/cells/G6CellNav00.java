package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class G6CellNav00 extends G6Base implements IG6BaseCell {

    public static final float COORDS_DIVIDER = 10_000_000F;

    public G6CellNav00(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    private final BitField timestamp = new BitField(32);
    private final BitField longitude = new BitField(32);
    private final BitField latitude = new BitField(32);

    //bit0 – состояние вызова на голосовую связь (1 – есть запрос на голосовую связь. 0 – нет запроса на голосовую связь)
    private final BitField extraDopBit0 = new BitField(1);
    private final BitField extraDopBit1 = new BitField(1);//bit1 – флаг тревожной информации ( один из параметров находится в диапазоне тревоги)
    private final BitField extraDopBit2 = new BitField(1);//bit2 – состояние SOS (1 – SOS, 0 – нет SOS)
    private final BitField extraDopBit3 = new BitField(1);//флаг первоначального включения

    private final BitField extraDopBit4 = new BitField(1);//флаг работы от встроенного аккумулятора
    private final BitField extraDopBit5 = new BitField(1);//bit5 - полушарие широты ( 1 – N, 0 – S )
    private final BitField extraDopBit6 = new BitField(1);//- полушарие долготы ( 1 – E, 0 – W )
    private final BitField extraDopBit7 = new BitField(1);//bit7 - достоверность навигационных данных ( 1- достоверны, 0– нет );
    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * bit7 - достоверность навигационных данных ( 1- достоверны, 0– нет ); bit6
     * - полушарие долготы ( 1 – E, 0 – W ); bit5 - полушарие широты ( 1 – N, 0
     * – S ); bit4 - флаг работы от встроенного аккумулятора; bit3 – флаг
     * первоначального включения; bit2 – состояние SOS (1 – SOS, 0 – нет SOS)
     * bit1 – флаг тревожной информации ( один из параметров находится в
     * диапазоне тревоги) bit0 – состояние вызова на голосовую связь (1 – есть
     * запрос на голосовую связь. 0 – нет запроса на голосовую связь)
     * <p>
     * </editor-fold>
     */

    private final BitField batVoltage = new BitField(8);//Напряжение батареи, 1бит = 20мВ
    private final BitField speedAvg = new BitField(16);//Средняя скорость за период в км/ч
    private final BitField speedMax = new BitField(16);//Максимальная скорость за период в км/ч
    private final BitField course = new BitField(16);//Направление движения (0 - 360)
    private final BitField track = new BitField(16);//Пройденный путь, м
    private final BitField altitude = new BitField(16);//Высота над уровнем моря в метрах ( -18000 - +18000)
    private final BitField nsat = new BitField(8);//Количество видимых спутников
    private final BitField pdop = new BitField(8);//PDOP

    public boolean getExtraDopBit0() {
        return this.extraDopBit0.intValue() == 1;
    }

    public void setExtraDopBit0(boolean extraDopBit0) {
        this.extraDopBit0.set(extraDopBit0 ? 1 : 0);
    }

    public boolean getExtraDopBit1() {
        return this.extraDopBit1.intValue() == 1;
    }

    public void setExtraDopBit1(boolean extraDopBit1) {
        this.extraDopBit1.set(extraDopBit1 ? 1 : 0);
    }

    public boolean getExtraDopBit2() {
        return this.extraDopBit2.intValue() == 1;
    }

    public void setExtraDopBit2(boolean extraDopBit2) {
        this.extraDopBit2.set(extraDopBit2 ? 1 : 0);
    }

    public boolean getExtraDopBit3() {
        return this.extraDopBit3.intValue() == 1;
    }

    public void setExtraDopBit3(boolean extraDopBit3) {
        this.extraDopBit3.set(extraDopBit3 ? 1 : 0);
    }

    public boolean getExtraDopBit4() {
        return this.extraDopBit4.intValue() == 1;
    }

    public void setExtraDopBit4(boolean extraDopBit4) {
        this.extraDopBit4.set(extraDopBit4 ? 1 : 0);
    }

    public boolean getExtraDopBit5() {
        return this.extraDopBit5.intValue() == 1;
    }

    public void setExtraDopBit5(boolean extraDopBit5) {
        this.extraDopBit5.set(extraDopBit5 ? 1 : 0);
    }

    public boolean getExtraDopBit6() {
        return this.extraDopBit6.intValue() == 1;
    }

    public void setExtraDopBit6(boolean extraDopBit6) {
        this.extraDopBit6.set(extraDopBit6 ? 1 : 0);
    }

    public boolean getExtraDopBit7() {
        return this.extraDopBit7.intValue() == 1;
    }

    public void setExtraDopBit7(boolean extraDopBit7) {
        this.extraDopBit7.set(extraDopBit7 ? 1 : 0);
    }

    public short getBatVoltage() {
        return batVoltage.shortValue();
    }

    public long getLatitude() {
        return latitude.longValue();
    }

    public long getLongitude() {
        return longitude.longValue();
    }

    public short getNsat() {
        return nsat.shortValue();
    }

    public short getPdop() {
        return pdop.shortValue();
    }

    public int getSpeedMax() {
        return speedMax.intValue();
    }

    public int getTrack() {
        return track.intValue();
    }

    public float getProcessedLon() {
        return (longitude.longValue() / COORDS_DIVIDER) * (getExtraDopBit6() ? 1 : -1);
    }//*

    public float getProcessedLat() {
        return (latitude.longValue() / COORDS_DIVIDER) * (getExtraDopBit5() ? 1 : -1);
    }//*

    public boolean getLocationValid() {
        return getExtraDopBit7();
    }//*

    public int getSpeedAvg() {
        return speedAvg.intValue();
    }//*

    public int getCourse() {
        return course.intValue();
    }//*

    public long getTimestamp() {
        return timestamp.longValue();
    }//*

    public int getAltitude() {
        return altitude.intValue();
    }//*


    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Управляющий байт extraDop содержит следующую информацию :
     * bit7 - достоверность навигационных данных ( 1- достоверны, 0– нет );
     * bit6 - полушарие долготы ( 1 – E, 0 – W );
     * bit5 - полушарие широты ( 1 – N, 0 – S );
     * bit4 - флаг работы от встроенного аккумулятора;
     * bit3 – флаг первоначального включения;
     * bit2 – состояние SOS (1 – SOS, 0 – нет SOS)
     * bit1 – флаг тревожной информации ( один из параметров находится в диапазоне тревоги)
     * bit0 – состояние вызова на голосовую связь (1 – есть запрос на голосовую связь. 0 – нет запроса на голосовую связь)
     * </editor-fold>
     */
    public boolean isAlarm() {
        return (getExtraDopBit2() || getExtraDopBit1());
    }//*

    public boolean isCallRequest() {
        return getExtraDopBit0();
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        Map<Integer, BigDecimal> sensors = new HashMap<>(6);

        //ksynina crutch #20369 && 18662#change-103570

        sensors.put(ndtpConfig.cell00Numbers.distance_num, BigDecimal.valueOf(getTrack()));
        sensors.put(ndtpConfig.cell00Numbers.alt_num, BigDecimal.valueOf(getAltitude()));
        // достоверность навигационных данных ( 1- достоверны, 0 – нет )
        sensors.put(ndtpConfig.cell00Numbers.bit8on_num, BigDecimal.valueOf(getExtraDopBit7() ? 1 : 0));
        // полушарие долготы ( 1 – E, 0 – W )
        sensors.put(ndtpConfig.cell00Numbers.bit7on_num, BigDecimal.valueOf(getExtraDopBit6() ? 1 : 0));
        // полушарие широты ( 1 – N, 0 – S );
        sensors.put(ndtpConfig.cell00Numbers.bit6on_num, BigDecimal.valueOf(getExtraDopBit5() ? 1 : 0));
        // флаг работы от встроенного аккумулятора;
        sensors.put(ndtpConfig.cell00Numbers.bit5on_num, BigDecimal.valueOf(getExtraDopBit4() ? 1 : 0));
        // флаг первоначального включения;
        sensors.put(ndtpConfig.cell00Numbers.bit4on_num, BigDecimal.valueOf(getExtraDopBit3() ? 1 : 0));
        // состояние SOS (1 – SOS, 0 – нет SOS)
        sensors.put(ndtpConfig.cell00Numbers.bit3on_num, BigDecimal.valueOf(getExtraDopBit2() ? 1 : 0));
        // флаг тревожной информации ( один из параметров находится в диапазоне тревоги)
        sensors.put(ndtpConfig.cell00Numbers.bit2on_num, BigDecimal.valueOf(getExtraDopBit1() ? 1 : 0));
        // состояние вызова на голосовую связь (1 – есть запрос на голосовую связь. 0 – нет запроса на голосовую связь)
        sensors.put(ndtpConfig.cell00Numbers.bit1on_num, BigDecimal.valueOf(getExtraDopBit0() ? 1 : 0));
        sensors.put(ndtpConfig.cell00Numbers.bat_voltage_num, BigDecimal.valueOf(getBatVoltage()));
        sensors.put(ndtpConfig.cell00Numbers.speed_max_num, BigDecimal.valueOf(getSpeedMax()));
        sensors.put(ndtpConfig.cell00Numbers.nsats_num, BigDecimal.valueOf(getNsat()));

        if (sensors.get(-1) != null) {
            sensors.remove(-1);
        }

        return sensors;
    }

    @Override
    public int getCell() {
        return 0;
    }


    @Override
    public String extractString() {
        return "G6CellNavData\n\t{" + "timestamp=" + timestamp +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", extraDopBit0=" + extraDopBit0 +
                ", extraDopBit1=" + extraDopBit1 +
                ", extraDopBit2=" + extraDopBit2 +
                ", extraDopBit3=" + extraDopBit3 +
                ", extraDopBit4=" + extraDopBit4 +
                ", extraDopBit5=" + extraDopBit5 +
                ", extraDopBit6=" + extraDopBit6 +
                ", extraDopBit7=" + extraDopBit7 +
                ", batVoltage=" + batVoltage +
                ", speedAvg=" + speedAvg +
                ", speedMax=" + speedMax +
                ", course=" + course +
                ", track=" + track +
                ", altitude=" + altitude +
                ", nsat=" + nsat +
                ", pdop=" + pdop + '}';
    }
}
