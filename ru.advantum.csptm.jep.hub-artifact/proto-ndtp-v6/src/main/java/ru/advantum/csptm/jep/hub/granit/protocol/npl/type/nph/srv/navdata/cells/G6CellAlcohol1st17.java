package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;

@SuppressWarnings("unused")
public class G6CellAlcohol1st17 extends G6Base implements IG6BaseCell {

    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Данные от спиртометра
     * Type=17, N=0 (1…n)
     * поле	длина	Тип	описание
     * 46	struct
     * Status	2	unsigned int16	(бит 0) Device Status Flag  - считано текущее состояние устройства
     * ( всегда присутствует в ячейке )
     * <p>
     * (бит 1) Last Record Flag - считана запись архива (поля присутствуют)
     * AlcoEvent  ( тип события / отсек ),
     * Alcohol Volume ( слитый / налитый объём ),
     * Starting Time ( время начала операции ) и
     * End_Time ( время завершения опрерации )
     * <p>
     * (бит 2) Record Counter Flag - считан счетчик записей архива ( если он равен 0, то чтение архива не производится )
     * <p>
     * (бит 3) Temperature Flag      - температура считана
     * <p>
     * (бит 4) Readed Record Flag   - считана последняя прочитанная запись архива
     * <p>
     * ( бит 5) Current Time Flag      - считано текущее время устройства
     * <p>
     * (бит 6) Current Volume Flag  - считано значения текущей операции
     * ( поля содержат данные )
     * Current Volume (текущий слитый / налитый объём )
     * Current Section ( номер отсека текущей операции )
     * <p>
     * (бит 14) Status Express Cell   - внеочередная ячейка
     * ( могут отсутствовать все данные, кроме Device Status)
     * AlcoEvent	2	unsigned int16	тип события / номер отсека
     * Alcohol Volume	2	unsigned int16	слитый / налитый объём
     * Starting Time	4	unsigned int32	время начала операции
     * End Time	4	unsigned int32	время завершения операции
     * Record Counter	2	unsigned int16	счетчик записей архива
     * Temperature	2	unsigned int16	Температура t1, t2
     * Readed AlcoEvent	2	unsigned int16	тип события / номер отсека
     * Readed Alcohol Volume	2	unsigned int16	слитый / налитый объём
     * Readed Starting Time	4	unsigned int32	время начала операции
     * Readed End Time	4	unsigned int32	время завершения операции
     * Current Time	4	unsigned int32	текущее время устройства
     * Device Status	2	unsigned int16	текущее состояние устройства
     * Current Volume	2	unsigned int16	текущий слитый / налитый объём
     * Current Section	2	unsigned int16	номер отсека текущей операции
     * Reserved16	2	unsigned int16
     * Reserved32	4	unsigned int32
     * </editor-fold>
     */
    private final Unsigned16 status = new Unsigned16();
    private final Unsigned16 alcoEvent = new Unsigned16();
    private final Unsigned16 alcoholVolume = new Unsigned16();
    private final Unsigned32 startTime = new Unsigned32();
    private final Unsigned32 endTime = new Unsigned32();
    private final Unsigned16 recordCounter = new Unsigned16();
    private final Unsigned16 temperature = new Unsigned16();
    private final Unsigned16 readAlcoEvent = new Unsigned16();
    private final Unsigned16 readAlcoholVolume = new Unsigned16();
    private final Unsigned32 readStartTime = new Unsigned32();
    private final Unsigned32 readEndTime = new Unsigned32();
    private final Unsigned32 currentTime = new Unsigned32();
    private final Unsigned16 deviceStatus = new Unsigned16();
    private final Unsigned16 currentVolume = new Unsigned16();
    private final Unsigned16 currentSection = new Unsigned16();
    private final Unsigned16 reserved16 = new Unsigned16();
    private final Unsigned32 reserved32 = new Unsigned32();

    public G6CellAlcohol1st17(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    @Override
    public String extractString() {
        return "G6CellAlcohol1st17{" +
                "status=" + status +
                '}';
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        return Collections.emptyMap();
    }

    @Override
    public int getCell() {
        return 17;
    }

}
