package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.commands;

import ru.advantum.csptm.jep.hub.granit.protocol.G6BaseCommand;

import static ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.generic.controls.G6NphPria.int2ip;

public class G6NphSetPria extends G6BaseCommand {

    public final Unsigned32 ip_address = new Unsigned32();
    public final Unsigned16 port = new Unsigned16();
    public final ByteSet URL = new ByteSet(40);
    public final ByteSet APN = new ByteSet(40);
    public final ByteSet User = new ByteSet(10);
    public final ByteSet Password = new ByteSet(10);

    @Override
    public String extractString() {
        return this.getClass().getSimpleName() + ": {"
                + "ip_address=" + int2ip((int) ip_address.get())
                + ",port=" + port.get()
                + ",URL=" + URL.asString()
                + ",APN=" + APN.asString()
                + ",User=" + User.asString()
                + ",Password=" + Password.asString()
                + "}";
    }

}
