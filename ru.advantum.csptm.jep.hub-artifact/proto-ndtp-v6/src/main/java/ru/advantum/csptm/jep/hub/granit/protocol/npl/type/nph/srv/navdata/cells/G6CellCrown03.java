/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class G6CellCrown03 extends G6Base implements IG6BaseCell {

    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Данные от устройства Корона. Type=3, N=0 (1)
     * поле	длина	тип	описание
     * 14	struct
     * <odometer>	4	unsigned int32	Одометр. Циклический счётчик пробега в метрах.
     * <zone>	2	unsigned int16	Номер зоны для позонной оплаты
     * <corona_door_in1>	1	unsigned int8	Количество вошедших пассажиров через первую дверь.
     * <corona_door_in2>	1	unsigned int8	Количество вошедших пассажиров через вторую дверь.
     * <corona_door_in3>	1	unsigned int8	Количество вошедших пассажиров через третью дверь.
     * <corona_door_in4>	1	unsigned int8	Количество вошедших пассажиров через четвертую дверь.
     * <corona_door_out1>	1	unsigned int8	Количество вышедших пассажиров через первую дверь.
     * <corona_door_out2>	1	unsigned int8	Количество вышедших пассажиров через вторую дверь.
     * <corona_door_out3>	1	unsigned int8	Количество вышедших пассажиров через третью дверь.
     * <corona_door_out4>	1	unsigned int8	Количество вышедших пассажиров через четвертую дверь.
     * </editor-fold>
     */

    private final Unsigned32 odometer = new Unsigned32();
    private final Unsigned16 zone = new Unsigned16();
    private final Unsigned8 corona_door_in1 = new Unsigned8();
    private final Unsigned8 corona_door_in2 = new Unsigned8();
    private final Unsigned8 corona_door_in3 = new Unsigned8();
    private final Unsigned8 corona_door_in4 = new Unsigned8();
    private final Unsigned8 corona_door_out1 = new Unsigned8();
    private final Unsigned8 corona_door_out2 = new Unsigned8();
    private final Unsigned8 corona_door_out3 = new Unsigned8();
    private final Unsigned8 corona_door_out4 = new Unsigned8();

    public G6CellCrown03(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    @Override
    public String extractString() {
        return "CellCrownData{"
                + "odometer=" + odometer
                + ", zone=" + zone
                + ", corona_door_in1=" + corona_door_in1
                + ", corona_door_in2=" + corona_door_in2
                + ", corona_door_in3=" + corona_door_in3
                + ", corona_door_in4=" + corona_door_in4
                + ", corona_door_out1=" + corona_door_out1
                + ", corona_door_out2=" + corona_door_out2
                + ", corona_door_out3=" + corona_door_out3
                + ", corona_door_out4=" + corona_door_out4
                + '}';
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        Map<Integer, BigDecimal> sensors = new HashMap<>(9);

        sensors.put(ndtpConfig.cell03Numbers.zoneCrown, BigDecimal.valueOf(zone.get()));
        sensors.put(ndtpConfig.cell03Numbers.corona_door_in1, BigDecimal.valueOf(corona_door_in1.get()));
        sensors.put(ndtpConfig.cell03Numbers.corona_door_in2, BigDecimal.valueOf(corona_door_in2.get()));
        sensors.put(ndtpConfig.cell03Numbers.corona_door_in3, BigDecimal.valueOf(corona_door_in3.get()));
        sensors.put(ndtpConfig.cell03Numbers.corona_door_in4, BigDecimal.valueOf(corona_door_in4.get()));

        sensors.put(ndtpConfig.cell03Numbers.corona_door_out1, BigDecimal.valueOf(corona_door_out1.get()));
        sensors.put(ndtpConfig.cell03Numbers.corona_door_out2, BigDecimal.valueOf(corona_door_out2.get()));
        sensors.put(ndtpConfig.cell03Numbers.corona_door_out3, BigDecimal.valueOf(corona_door_out3.get()));
        sensors.put(ndtpConfig.cell03Numbers.corona_door_out4, BigDecimal.valueOf(corona_door_out4.get()));
        sensors.put(ndtpConfig.cell03Numbers.odometerCrown, BigDecimal.valueOf(odometer.get()));
        return sensors;
    }

    @Override
    public int getCell() {
        return 3;
    }

}
