/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.advantum.csptm.jep.hub.granit.protocol;

/**
 * @author Kotin <kotin@advantum.ru>
 */

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.Arrays;

public class BlowFishEncryptor {

    public static void main(String[] args) throws Exception {
        blowfishEncrypt("plaintextfile", "ciphertextfile");
    }

    public static void blowfishEncrypt(String f1, String f2) throws Exception {
        String Key = "Something";
        byte[] KeyData = Key.getBytes();
        SecretKeySpec KS = new SecretKeySpec(KeyData, "Blowfish");
        Cipher cipher = Cipher.getInstance("Blowfish");
        cipher.init(Cipher.ENCRYPT_MODE, KS);
        byte[] doFinal = cipher.doFinal(f2.getBytes());
        String valueOf = Arrays.toString(doFinal);
        System.out.println(valueOf);
    }
}
