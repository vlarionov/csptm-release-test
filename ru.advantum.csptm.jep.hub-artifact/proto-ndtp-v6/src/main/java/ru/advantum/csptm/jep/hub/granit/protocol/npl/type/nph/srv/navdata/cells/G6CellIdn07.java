/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class G6CellIdn07 extends G6Base implements IG6BaseCell {

    public final static short OK = 0;
    public final static short ALARM = 1;
    public final static short BREAK = 2;
    public final static short SHORT2GROUND = 3;
    public final static short SHORT2VOLUME = 4;
    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Данные от цифрового датчика – интеллектуальный датчик навигатора. Type=7, N=0 (1…n)
     * поле	длина	тип	Описание
     * 1	struct
     * <value>	1	unsigned int8	Состояние интеллектуального цифрового датчика. Принимает значения:
     * 0 – Норма
     * 1 – Тревога
     * 2 – Обрыв
     * 3 – Замыкание на массу
     * 4 – Замыкание на питание
     * //</editor-fold>
     */

    public final Unsigned8 value = new Unsigned8();

    public G6CellIdn07(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    @Override
    public String extractString() {
        return "G6CellIdn07{" + "value=" + value + '}';
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        Map<Integer, BigDecimal> sensors = new HashMap<>(1);

        sensors.put(ndtpConfig.cell07Numbers.value, BigDecimal.valueOf(value.get()));

        return sensors;
    }

    @Override
    public int getCell() {
        return 7;
    }

}
