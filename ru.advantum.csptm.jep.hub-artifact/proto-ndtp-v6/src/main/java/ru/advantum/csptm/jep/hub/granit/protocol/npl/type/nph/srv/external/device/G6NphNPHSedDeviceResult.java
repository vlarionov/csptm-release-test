package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.external.device;

import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

public class G6NphNPHSedDeviceResult extends G6Base {

    public BitField num_packet = new BitField(16);
    public BitField error_packet = new BitField(32);
    public BitField message_id = new BitField(16);

    public G6NphNPHSedDeviceResult() {
    }

    public G6NphNPHSedDeviceResult(int numpacket, long errorpacket, int messageid) {
        num_packet.set(numpacket);
        error_packet.set(errorpacket);
        message_id.set(messageid);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()
                + ":{num_packet=" + num_packet
                + ", error_packet=" + error_packet
                + ", message_id=" + message_id
                + "}";
    }
}
