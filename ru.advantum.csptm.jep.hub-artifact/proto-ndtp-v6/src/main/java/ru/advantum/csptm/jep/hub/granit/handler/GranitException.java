/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.advantum.csptm.jep.hub.granit.handler;

/**
 * @author Kotin <kotin@advantum.ru>
 */

public class GranitException extends Exception {

    public GranitException(Exception e) {
        super(e);
    }

    public GranitException(String message) {
        super(message);
    }

    public GranitException(String message, Throwable e) {
        super(message, e);
    }

    public static class WrongSignature extends GranitException {

        public WrongSignature(String message) {
            super(message);
        }
    }

    public static class InvalidCrc extends GranitException {

        public InvalidCrc(String message) {
            super(message);
        }
    }

    public static class UnknownProtocol extends GranitException {

        public UnknownProtocol(String message) {
            super(message);
        }
    }

    public static class Type01NavDataException extends GranitException {

        //Дополнительные  навигационные данные Type=1, N=0 (1)
        //(структура на данный момент не реализована).
        public Type01NavDataException() {
            super("Addition navigation data Type=01 is not supported");
        }
    }

    public static class Type11NavDataException extends GranitException {

        //Дополнительные  навигационные данные Type=1, N=0 (1)
        //(структура на данный момент не реализована).
        public Type11NavDataException() {
            super("Addition navigation data Type=11 is not supported");
        }
    }

    public static class TypeNavDataException extends GranitException {

        //(структура на данный момент не реализована).
        public TypeNavDataException(int typeId) {
            super(String.format("Addition navigation data Type=%d is not supported", typeId));
        }

        public TypeNavDataException(int typeId, Throwable reason) {
            super(String.format("Addition navigation data Type=%d is not supported", typeId), reason);
        }
    }
}
