/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class G6CellLls15 extends G6Base implements IG6BaseCell {

    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Данные от датчика LLS Sensor Type=15, N=0 (1…n)
     * поле	длина	Тип	описание
     * 50	struct
     * <Status>	2	unsigned int16
     * <main_float_level>	4	unsigned int32	Уровень основного поплавка
     * <Temperature_average>	4	unsigned int32	Средняя температура в продукте
     * <Percent_of_volume>	4	unsigned int32	Процентное заполнение по объему
     * <total_Volume>	4	unsigned int32	Общий объем
     * <Weight>	4	unsigned int32	Масса
     * <Density>	4	unsigned int32	Плотность
     * <Net_Standard_Volume>	4	unsigned int32	Объем основного продукта
     * <Level_of_water>	4	unsigned int32	Уровень подтоварной воды
     * <Pressure>	4	unsigned int32	Давление
     * <Vapor_temperature_average>	4	unsigned int32	Средняя температура в паровой фазе
     * <Vapor_Weight>	4	unsigned int32	Масса паровой фазы
     * <Liquid_phase_Weight>	4	unsigned int32	Масса жидкой фазы
     * </editor-fold>
     */

    private final Unsigned16 status = new Unsigned16();
    private final Unsigned32 main_float_level = new Unsigned32();
    private final Unsigned32 temperature_average = new Unsigned32();
    private final Unsigned32 percent_of_volume = new Unsigned32();
    private final Unsigned32 total_Volume = new Unsigned32();
    private final Unsigned32 weight = new Unsigned32();
    private final Unsigned32 density = new Unsigned32();
    private final Unsigned32 net_Standard_Volume = new Unsigned32();
    private final Unsigned32 level_of_water = new Unsigned32();
    private final Unsigned32 pressure = new Unsigned32();
    private final Unsigned32 vapor_temperature_average = new Unsigned32();
    private final Unsigned32 vapor_Weight = new Unsigned32();
    private final Unsigned32 liquid_phase_Weight = new Unsigned32();

    public G6CellLls15(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    @Override
    public String extractString() {
        return "G6CellLls15{" + "status=" + status +
                ", main_float_level=" + main_float_level +
                ", temperature_average=" + temperature_average +
                ", percent_of_volume=" + percent_of_volume +
                ", total_Volume=" + total_Volume +
                ", weight=" + weight +
                ", density=" + density +
                ", net_Standard_Volume=" + net_Standard_Volume +
                ", level_of_water=" + level_of_water +
                ", pressure=" + pressure +
                ", vapor_temperature_average=" + vapor_temperature_average +
                ", vapor_Weight=" + vapor_Weight +
                ", liquid_phase_Weight=" + liquid_phase_Weight + '}';
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        Map<Integer, BigDecimal> sensors = new HashMap<>(13);

        sensors.put(ndtpConfig.cell15Numbers.status15, BigDecimal.valueOf(status.get()));

        sensors.put(ndtpConfig.cell15Numbers.main_float_level, BigDecimal.valueOf(main_float_level.get()));
        sensors.put(ndtpConfig.cell15Numbers.temperature_average, BigDecimal.valueOf(temperature_average.get()));
        sensors.put(ndtpConfig.cell15Numbers.percent_of_volume, BigDecimal.valueOf(percent_of_volume.get()));
        sensors.put(ndtpConfig.cell15Numbers.total_Volume, BigDecimal.valueOf(total_Volume.get()));

        sensors.put(ndtpConfig.cell15Numbers.weight15, BigDecimal.valueOf(weight.get()));
        sensors.put(ndtpConfig.cell15Numbers.density15, BigDecimal.valueOf(density.get()));

        sensors.put(ndtpConfig.cell15Numbers.net_Standard_Volume, BigDecimal.valueOf(net_Standard_Volume.get()));
        sensors.put(ndtpConfig.cell15Numbers.level_of_water, BigDecimal.valueOf(level_of_water.get()));
        sensors.put(ndtpConfig.cell15Numbers.pressure, BigDecimal.valueOf(pressure.get()));
        sensors.put(ndtpConfig.cell15Numbers.vapor_temperature_average, BigDecimal.valueOf(vapor_temperature_average.get()));
        sensors.put(ndtpConfig.cell15Numbers.vapor_Weight, BigDecimal.valueOf(vapor_Weight.get()));
        sensors.put(ndtpConfig.cell15Numbers.liquid_phase_Weight, BigDecimal.valueOf(liquid_phase_Weight.get()));

        return sensors;
    }

    @Override
    public int getCell() {
        return 15;
    }

}
