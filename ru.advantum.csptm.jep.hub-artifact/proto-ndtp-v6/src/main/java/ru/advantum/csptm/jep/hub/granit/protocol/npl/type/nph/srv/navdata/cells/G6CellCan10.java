/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class G6CellCan10 extends G6Base implements IG6BaseCell {

    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Данные от CAN модуля M333. Type=10, N=0 (1…n)
     * поле	длина	Тип	описание
     * 37	struct
     * <SecFlagStatus>	4	unsigned int32	Флаг работоспособности устройства и флаги безопасностности (Security state flags)
     * <AllTimeEngine>	4	unsigned int32	Полное время работы двигателя ч * 100. То есть 1ч 15 мин = 125
     * <AllTrack>	4	unsigned int32	Полный пробег транспортного средства км * 100
     * <AllFuelConsum>	4	unsigned int32	Полный расход топлива л
     * <FuelLevel>	2	unsigned int16	bit15 – определяет единицу ихмерения уровня топлива. Если равен 1, то проценты, если 0 — то литры.
     * bit0 .. bit14 – уровень топлива
     * <SpeedTurnEngine>	2	unsigned int16	Скорость оборотов двигателя rpm
     * <TEngine>	2	signed int16	Температура двигателя °C
     * <Speed>	1	unsigned int8	Скорость тр средства км/ч
     * <PressureAxis>	2	unsigned int16	Давление на ось 1 кг * 10
     * <PressureAxis>	2	unsigned int16	Давление на ось 2 кг * 10
     * <PressureAxis>	2	unsigned int16	Давление на ось 3 кг * 10
     * <PressureAxis>	2	unsigned int16	Давление на ось 4 кг * 10
     * <PressureAxis>	2	unsigned int16	Давление на ось 5 кг * 10
     * <FlagAlarm>	4	unsigned int32	Контроллеры аварии (см. описание ниже)
     * </editor-fold>
     */

    private final Unsigned32 secFlagStatus = new Unsigned32();
    private final Unsigned32 allTimeEngine = new Unsigned32();
    private final Unsigned32 allTrack = new Unsigned32();
    private final Unsigned32 allFuelConsum = new Unsigned32();
    private final Unsigned16 fuelLevel = new Unsigned16();
    private final Unsigned16 speedTurnEngine = new Unsigned16();
    private final Signed16 tEngine = new Signed16();
    private final Unsigned8 speed = new Unsigned8();
    private final Unsigned16[] pressureAxis = array(new Unsigned16[5]);
    private final Unsigned32 flagAlarm = new Unsigned32();

    public G6CellCan10(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * <SecFlagStatus>	 - Основные флаги работы автомобиля.
     * Если значение равно 0xFFFFFFFF — то устройство CAN модуль не найден.
     * bit 0 .. bit 7
     * 1 - автомобиль закрыт при помощи штатного брелока
     * 2 - автомобиль открыт при помощи штатного брелока
     * 3 - багажник открыт при помощи штатного брелока
     * 4 - модуль выслал сигнал rearmingu в сигнализацию
     * 7 - модуль перешел в режим экономии энергии „sleep mode”
     * bit 8 - открыта дверь водителя
     * bit 9 - открыта дверь пассажира
     * bit 10 - открыт багажник
     * bit 11 - открыт капот
     * bit 12 - затянут рычаг ручного тормоза (информация доступна только при включенном зажигании)
     * bit 13 - нажат ножной тормоз (информация доступна только при включенном зажигании )
     * bit 14 - двигатель работает (информация доступна только при включенном зажигании )
     * bit 16 - зажигание включено
     * bit 17 – штатная сигнализация поставлена на охрану (находится в режиме тревоги)
     * bit 18 - автомобиль закрыт при помощи штатного брелока
     * bit 19 - ключ находится в замке зажигания
     * bit 20 – включено динамичное зажигание 2
     * <p>
     * <FlagAlarm> - флаги кoнтроллеров аварий
     * Устройство само решает, сколько ячеек будет в пакете.
     * Структура пакета ограничивает только то, что ячейки следуют одна за другой и размер ячейки определяется полем <Type>.
     * </editor-fold>
     */

    @Override
    public String extractString() {
        return "G6CellCan10{" + "secFlagStatus=" + secFlagStatus +
                ", allTimeEngine=" + allTimeEngine +
                ", allTrack=" + allTrack +
                ", allFuelConsum=" + allFuelConsum +
                ", fuelLevel=" + fuelLevel +
                ", speedTurnEngine=" + speedTurnEngine +
                ", tEngine=" + tEngine +
                ", speed=" + speed +
                ", pressureAxis=" + Arrays.toString(pressureAxis) +
                ", flagAlarm=" + flagAlarm + '}';
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        Map<Integer, BigDecimal> sensors = new HashMap<>(14);

        sensors.put(ndtpConfig.cell10Numbers.secFlagStatus, BigDecimal.valueOf(secFlagStatus.get()));
        sensors.put(ndtpConfig.cell10Numbers.allTimeEngine, BigDecimal.valueOf(allTimeEngine.get()));
        sensors.put(ndtpConfig.cell10Numbers.allTrack, BigDecimal.valueOf(allTrack.get()));
        sensors.put(ndtpConfig.cell10Numbers.allFuelConsum, BigDecimal.valueOf(allFuelConsum.get()));
        sensors.put(ndtpConfig.cell10Numbers.fuelLevel, BigDecimal.valueOf(fuelLevel.get()));

        sensors.put(ndtpConfig.cell10Numbers.speedTurnEngine, BigDecimal.valueOf(speedTurnEngine.get()));
        sensors.put(ndtpConfig.cell10Numbers.tEngine, BigDecimal.valueOf(tEngine.get()));
        sensors.put(ndtpConfig.cell10Numbers.speed, BigDecimal.valueOf(speed.get()));

        sensors.put(ndtpConfig.cell10Numbers.pressureAxis0, BigDecimal.valueOf(pressureAxis[0].get()));
        sensors.put(ndtpConfig.cell10Numbers.pressureAxis1, BigDecimal.valueOf(pressureAxis[1].get()));
        sensors.put(ndtpConfig.cell10Numbers.pressureAxis2, BigDecimal.valueOf(pressureAxis[2].get()));
        sensors.put(ndtpConfig.cell10Numbers.pressureAxis3, BigDecimal.valueOf(pressureAxis[3].get()));
        sensors.put(ndtpConfig.cell10Numbers.pressureAxis4, BigDecimal.valueOf(pressureAxis[4].get()));

        sensors.put(ndtpConfig.cell10Numbers.flagAlarm, BigDecimal.valueOf(flagAlarm.get()));

        return sensors;
    }

    @Override
    public int getCell() {
        return 10;
    }

}
