package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.generic.controls;

import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

public class G6NphBalance extends G6Base {

    public final ByteSet balance = new ByteSet(70);

    @Override
    public String extractString() {
        return this.getClass().getSimpleName() + ": {"
                + "cmd_req=" + balance.asString()
                + "}";
    }

}
