package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.generic.controls;

import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

public class G6NphRouteAutoinformer extends G6Base {

    public final ByteSet route = new ByteSet(30);
    public final ByteSet run = new ByteSet(30);

    @Override
    public String extractString() {
        return this.getClass().getSimpleName() + ": {"
                + "route=" + route.asString()
                + "run=" + run.asString()
                + "}";
    }

}
