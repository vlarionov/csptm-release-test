package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.external.device;

import javolution.io.Struct;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

public class G6NphSedDeviceData extends G6Base {

    public Struct.Unsigned16 message_id = new Struct.Unsigned16();
    public Struct.Unsigned16 num_packet = new Struct.Unsigned16();
    public ByteSet data;

    public G6NphSedDeviceData() {
    }

    public G6NphSedDeviceData(String message, String srcEnc, String dstEnc) {
        byte[] bytes = (new String(message.getBytes(Charset.forName(srcEnc)))).getBytes(Charset.forName(dstEnc));
        this.data = new ByteSet(bytes.length);
        this.data.set(bytes);
    }

    @Override
    public int read(InputStream in) throws IOException {
        Struct.Unsigned32 length = new Struct.Unsigned32();
        length.struct().read(in);
        this.data = new ByteSet((int) length.get());
        this.data.struct().read(in);
        return (int) (length.get() + 1);
    }

    @Override
    public String extractString() {
        return this.getClass().getSimpleName() + ": {"
                + "message_id=" + message_id.get()
                + ",num_packet=" + num_packet.get()
                + ",data" + (data != null ? data.asString() : "")
                + "}";
    }

}
