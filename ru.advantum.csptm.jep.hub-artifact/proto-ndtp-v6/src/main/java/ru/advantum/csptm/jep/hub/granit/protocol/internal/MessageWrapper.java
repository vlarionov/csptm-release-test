package ru.advantum.csptm.jep.hub.granit.protocol.internal;

import ru.advantum.csptm.jep.hub.granit.protocol.G6Npl;
import ru.advantum.tools.HEX;

/**
 * Wrap data frame between decoder and logic
 *
 * @author Mitsay <mitsay@advantum.ru>
 * @since 27.11.15.
 */
public class MessageWrapper {

    private final G6Npl npl;
    private final byte[] trailer;

    public MessageWrapper(G6Npl npl, byte[] trailer) {
        this.npl = npl;
        this.trailer = trailer;
    }

    public G6Npl getNpl() {
        return npl;
    }

    public byte[] getTrailer() {
        return trailer;
    }

    @Override
    public String toString() {
        return "MessageWrapper{" +
                "npl=" + npl.extractString() +
                ", trailer=" + HEX.byteArrayToId(trailer) +
                '}';
    }
}
