package ru.advantum.csptm.jep.hub.granit.handler;

import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

/**
 * @author Kotin <kotin@advantum.ru>
 */
public abstract class GranitMessage {

    protected final int serviceId;
    protected final long requestId;

    public GranitMessage(int serviceId, long requestId) {
        this.serviceId = serviceId;
        this.requestId = requestId;
    }

    public int getServiceId() {
        return serviceId;
    }

    public long getRequestId() {
        return requestId;
    }

    public abstract int getOrder();

    public static class HANDSHAKE extends GranitMessage {

        private final int dataSize;
        private final boolean isEncoded;
        private final long peerAddress;

        public HANDSHAKE(int serviceId, long requestId, long peerAddress, int dataSize, boolean isEncoded) {
            super(serviceId, requestId);
            this.dataSize = dataSize;
            this.isEncoded = isEncoded;
            this.peerAddress = peerAddress;
        }

        public int getDataSize() {
            return dataSize;
        }

        public boolean isEncoded() {
            return isEncoded;
        }

        @Override
        public int getOrder() {
            return 0;
        }

        public long getPeerAddress() {
            return this.peerAddress;
        }
    }

    public static class DATA extends GranitMessage {

        protected final boolean requireReply;
        protected final G6Base data[];

        public DATA(int serviceId, long requestId, boolean requireReply, G6Base[] data) {
            super(serviceId, requestId);
            this.requireReply = requireReply;
            this.data = data;
        }

        public boolean isRequireReply() {
            return requireReply;
        }

        public G6Base[] getData() {
            return data;
        }

        @Override
        public int getOrder() {
            return 1;
        }
    }
}
