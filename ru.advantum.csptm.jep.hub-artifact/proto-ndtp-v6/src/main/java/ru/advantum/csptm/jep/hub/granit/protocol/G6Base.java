package ru.advantum.csptm.jep.hub.granit.protocol;

import javolution.io.Struct;
import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;

import java.nio.ByteOrder;

/**
 * @author Kotin <kotin@advantum.ru>
 */

public class G6Base extends Struct {

    protected int cellNumber;
    protected final GranitHubConfig.Ndtp ndtpConfig;

    public G6Base() {
        this.ndtpConfig = new GranitHubConfig.Ndtp();
    }

    public G6Base(GranitHubConfig.Ndtp ndtpConfig) {
        this.ndtpConfig = ndtpConfig;
    }

    @Override
    public ByteOrder byteOrder() {
        return ByteOrder.LITTLE_ENDIAN;
    }

    @Override
    public boolean isPacked() {
        return true;
    }

    public String extractString() {
        return "G6Base{" + '}';
    }

    protected void setCellNumber(int cellNumber) {
        this.cellNumber = cellNumber;
    }

    public int getCellNumber() {
        return this.cellNumber;
    }
}
