/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class G6CellCAN18 extends G6Base implements IG6BaseCell {

    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Данные от CAN Sensors (коробки расширителя портов для прибора гранит-навигатор 4.хх)
     * Type=18, N=0 (1…n)
     * поле	длина	Тип	описание
     * 50	struct
     * <an_in0>	2	unsigned int16	Значение 0 аналогового входа в 16 битном формате
     * <an_in1>	2	unsigned int16	Значение 1 аналогового входа в 16 битном формате
     * <an_in2>	2	unsigned int16	Значение 2 аналогового входа в 16 битном формате
     * <an_in3>	2	unsigned int16	Значение 3 аналогового входа в 16 битном формате
     * <an_in4>	2	unsigned int16	Значение 4 аналогового входа в 16 битном формате
     * <an_in5>	2	unsigned int16	Значение 5 аналогового входа в 16 битном формате
     * <an_in6>	2	unsigned int16	Значение 6 аналогового входа в 16 битном формате
     * <an_in7>	2	unsigned int16	Значение 7 аналогового входа в 16 битном формате
     * <di_in>	1	unsigned int8	Значение цифровых входов
     * <di_out>	1	unsigned int8	Состояние дискретных выходов
     * <di0_counter>	4	unsigned int32	Количество импульсов на дискретном входе 0 с предыдущей нав. отметки
     * <di1_counter>	4	unsigned int32	Количество импульсов на дискретном входе 1 с предыдущей нав. отметки
     * <di2_counter>	4	unsigned int32	Количество импульсов на дискретном входе 2 с предыдущей нав. отметки
     * <di3_counter>	4	unsigned int32	Количество импульсов на дискретном входе 3 с предыдущей нав. отметки
     * <di4_counter>	4	unsigned int32	Количество импульсов на дискретном входе 4 с предыдущей нав. отметки
     * <di5_counter>	4	unsigned int32	Количество импульсов на дискретном входе 5 с предыдущей нав. отметки
     * <di6_counter>	4	unsigned int32	Количество импульсов на дискретном входе 6 с предыдущей нав. отметки
     * <di7_counter>	4	unsigned int32	Количество импульсов на дискретном входе 7 с предыдущей нав. отметки
     * </editor-fold>
     */

    private final Unsigned16 an_in0 = new Unsigned16();
    private final Unsigned16 an_in1 = new Unsigned16();
    private final Unsigned16 an_in2 = new Unsigned16();
    private final Unsigned16 an_in3 = new Unsigned16();
    private final Unsigned16 an_in4 = new Unsigned16();
    private final Unsigned16 an_in5 = new Unsigned16();
    private final Unsigned16 an_in6 = new Unsigned16();
    private final Unsigned16 an_in7 = new Unsigned16();
    private final Unsigned8 di_in = new Unsigned8();
    private final Unsigned8 di_out = new Unsigned8();
    private final Unsigned32 di0_counter = new Unsigned32();
    private final Unsigned32 di1_counter = new Unsigned32();
    private final Unsigned32 di2_counter = new Unsigned32();
    private final Unsigned32 di3_counter = new Unsigned32();
    private final Unsigned32 di4_counter = new Unsigned32();
    private final Unsigned32 di5_counter = new Unsigned32();
    private final Unsigned32 di6_counter = new Unsigned32();
    private final Unsigned32 di7_counter = new Unsigned32();

    public G6CellCAN18(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    @Override
    public String extractString() {
        return "G6CellCAN18{" +
                "an_in0=" + an_in0 +
                ", an_in1=" + an_in1 +
                ", an_in2=" + an_in2 +
                ", an_in3=" + an_in3 +
                ", an_in4=" + an_in4 +
                ", an_in5=" + an_in5 +
                ", an_in6=" + an_in6 +
                ", an_in7=" + an_in7 +
                ", di_in=" + di_in +
                ", di_out=" + di_out +
                ", di0_counter=" + di0_counter +
                ", di1_counter=" + di1_counter +
                ", di2_counter=" + di2_counter +
                ", di3_counter=" + di3_counter +
                ", di4_counter=" + di4_counter +
                ", di5_counter=" + di5_counter +
                ", di6_counter=" + di6_counter +
                ", di7_counter=" + di7_counter +
                '}';
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        Map<Integer, BigDecimal> sensors = new HashMap<>(19);

        sensors.put(ndtpConfig.cell18Numbers.can_an_in0, BigDecimal.valueOf(an_in0.get()));
        sensors.put(ndtpConfig.cell18Numbers.can_an_in1, BigDecimal.valueOf(an_in1.get()));
        sensors.put(ndtpConfig.cell18Numbers.can_an_in2, BigDecimal.valueOf(an_in2.get()));
        sensors.put(ndtpConfig.cell18Numbers.can_an_in3, BigDecimal.valueOf(an_in3.get()));
        sensors.put(ndtpConfig.cell18Numbers.can_an_in4, BigDecimal.valueOf(an_in4.get()));
        sensors.put(ndtpConfig.cell18Numbers.can_an_in5, BigDecimal.valueOf(an_in5.get()));
        sensors.put(ndtpConfig.cell18Numbers.can_an_in6, BigDecimal.valueOf(an_in6.get()));
        sensors.put(ndtpConfig.cell18Numbers.can_an_in7, BigDecimal.valueOf(an_in7.get()));

        sensors.put(ndtpConfig.cell18Numbers.can_di_in, BigDecimal.valueOf(di_in.get()));
        sensors.put(ndtpConfig.cell18Numbers.can_di_out, BigDecimal.valueOf(di_out.get()));

        sensors.put(ndtpConfig.cell18Numbers.can_di0_counter, BigDecimal.valueOf(di0_counter.get()));
        sensors.put(ndtpConfig.cell18Numbers.can_di1_counter, BigDecimal.valueOf(di1_counter.get()));
        sensors.put(ndtpConfig.cell18Numbers.can_di2_counter, BigDecimal.valueOf(di2_counter.get()));
        sensors.put(ndtpConfig.cell18Numbers.can_di3_counter, BigDecimal.valueOf(di3_counter.get()));
        sensors.put(ndtpConfig.cell18Numbers.can_di4_counter, BigDecimal.valueOf(di4_counter.get()));
        sensors.put(ndtpConfig.cell18Numbers.can_di5_counter, BigDecimal.valueOf(di5_counter.get()));
        sensors.put(ndtpConfig.cell18Numbers.can_di6_counter, BigDecimal.valueOf(di6_counter.get()));
        sensors.put(ndtpConfig.cell18Numbers.can_di7_counter, BigDecimal.valueOf(di7_counter.get()));

        return sensors;
    }

    @Override
    public int getCell() {
        return 18;
    }

}
