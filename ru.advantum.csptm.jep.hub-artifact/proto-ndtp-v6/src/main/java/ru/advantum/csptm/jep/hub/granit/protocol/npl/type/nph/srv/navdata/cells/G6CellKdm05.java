/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class G6CellKdm05 extends G6Base implements IG6BaseCell {

    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * <PGM_enable>	1	unsigned int8	Признак работы оборудования распределителя:
     * дискретный параметр со значениями: 1 - включено, 0 - выключено.
     * <PGM_width>	1	unsigned int8	Параметр: ширина посыпки/распределения ПГМ (метры).
     * Диапазон значений 0- 15 метров. Точность: единицы метров.
     * <PGM_density>	2	unsigned int16	Параметр: плотность посыпки или распределения ПГМ (г/м2).
     * Диапазон значений: 0 - 511 г./м2; Точность: единицы граммов.
     * <plough_state>	1	unsigned int8	Признак: плуг опущен/поднят: дискретный параметр со значениями: 1 - опущен,   0 - поднят.
     * <brush_state>	1	unsigned int8	Признак щетки включены/выключены (средний гребень опущен/поднят):
     * дискретный параметр со значениями: 1 - включены,   0 - выключены.
     * //</editor-fold>
     */

    private final Unsigned8 pgmEnable = new Unsigned8();
    private final Unsigned8 pgmWidth = new Unsigned8();
    private final Unsigned16 pgmDensity = new Unsigned16();
    private final Unsigned8 ploughState = new Unsigned8();
    private final Unsigned8 brushState = new Unsigned8();

    public G6CellKdm05(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    @Override
    public String extractString() {
        return "G6CellKdm05{" + "pgmEnable=" + pgmEnable +
                ", pgmWidth=" + pgmWidth +
                ", pgmDensity=" + pgmDensity +
                ", ploughState=" + ploughState +
                ", brushState=" + brushState + '}';
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        Map<Integer, BigDecimal> sensors = new HashMap<>(5);

        sensors.put(ndtpConfig.cell05Numbers.pgmEnable, BigDecimal.valueOf(pgmEnable.get()));
        sensors.put(ndtpConfig.cell05Numbers.pgmWidth, BigDecimal.valueOf(pgmWidth.get()));
        sensors.put(ndtpConfig.cell05Numbers.pgmDensity, BigDecimal.valueOf(pgmDensity.get()));
        sensors.put(ndtpConfig.cell05Numbers.ploughState, BigDecimal.valueOf(ploughState.get()));
        sensors.put(ndtpConfig.cell05Numbers.brushState, BigDecimal.valueOf(brushState.get()));

        return sensors;
    }

    @Override
    public int getCell() {
        return 5;
    }

}
