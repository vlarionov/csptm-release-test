package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.generic.controls;

import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

public class G6NphInfo extends G6Base {

    public final ByteSet data_ver = new ByteSet(15);
    public final ByteSet ver_navig = new ByteSet(10);
    public final Unsigned32 lat = new Unsigned32();
    public final Unsigned32 lon = new Unsigned32();
    public final Unsigned8 num_sat = new Unsigned8();
    public final Unsigned16 speed = new Unsigned16();

    @Override
    public String extractString() {
        return this.getClass().getSimpleName() + ": {"
                + "data_ver=" + data_ver.asString()
                + ",ver_navig=" + ver_navig.asString()
                + ",lat=" + lat.get()
                + ",lon=" + lon.get()
                + ",num_sat=" + num_sat.get()
                + ",speed=" + speed.get()
                + "}";
    }

}
