package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.generic.controls;

import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.nio.ByteBuffer;

public class G6NphPria extends G6Base {

    public final Unsigned32 ip_address = new Unsigned32();
    public final Unsigned16 port = new Unsigned16();
    public final ByteSet URL = new ByteSet(40);
    public final ByteSet APN = new ByteSet(40);
    public final ByteSet User = new ByteSet(10);
    public final ByteSet Password = new ByteSet(10);

    public static String int2ip(int ip) {
        ByteBuffer buffer = ByteBuffer.allocate(32);
        buffer.putInt(ip);
        buffer.position(0);
        byte[] bytes = new byte[4];
        buffer.get(bytes);
        StringBuilder sb = new StringBuilder();
        for (int i = bytes.length - 1; i >= 0; i--) {
            sb.append(String.format("%d", bytes[i] & 0xFF)).append(".");
        }
        return sb.deleteCharAt(sb.lastIndexOf(".")).toString();
    }

    public static long ip2int(String address) {
        String[] ipAddressInArray = address.split("\\.");
        long result = 0L;

        for(int i = 0; i < ipAddressInArray.length; ++i) {
            int power = 3 - i;
            int ip = Integer.parseInt(ipAddressInArray[i]);
            result = (long)((double)result + (double)ip * Math.pow(256.0D, (double)power));
        }

         return result; //(Long.reverseBytes(result) >> 32 ) & 0xFFFFFFFF;
    }

/*
    public static String int2ip(int ipAddress) throws UnknownHostException {
        byte[] bytes = BigInteger.valueOf(ipAddress).toByteArray();
        InetAddress address = InetAddress.getByAddress(bytes);
        return address.getHostAddress();
    }
*/

    @Override
    public String extractString() {
        return this.getClass().getSimpleName() + ": {"
                + "ip_address=" + int2ip((int) ip_address.get()) + " (" + ip_address.get() + ")"
                + ",port=" + port.get()
                + ",URL=" + URL.asString()
                + ",APN=" + APN.asString()
                + ",User=" + User.asString()
                + ",Password=" + Password.asString()
                + "}";
    }
}
