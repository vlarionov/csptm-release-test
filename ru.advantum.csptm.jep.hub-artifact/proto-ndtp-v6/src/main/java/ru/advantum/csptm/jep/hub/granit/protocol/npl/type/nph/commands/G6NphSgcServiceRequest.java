package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.commands;

import javolution.io.Struct;
import ru.advantum.csptm.jep.hub.granit.protocol.G6BaseCommand;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.G6NphHeader;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class G6NphSgcServiceRequest extends G6BaseCommand {
    private Struct.Unsigned16 data;
    private List<Unsigned16> dataList;

    public G6NphSgcServiceRequest() {
    }

    public G6NphSgcServiceRequest(int type2request) {
        data = new Struct.Unsigned16();
        data.set(type2request);
    }

    public G6NphSgcServiceRequest(boolean test) {
        dataList = new ArrayList<>();
    }

    @Override
    public int read(InputStream in) throws IOException {
        int len = 0;
        if (data != null) {
            len += data.struct().read(in);
        } else if (dataList != null) {
            Struct.Unsigned16 val = new Struct.Unsigned16();
            len += val.struct().read(in);
            dataList.add(val);
        }
        return len;
    }

    @Override
    public String toString() {
        String sd = this.getClass().getSimpleName()
                + ":{data=";
        sd += (data == null ? "all" : G6NphHeader.G6NphServiceType.lookup(data.get()));
        if (dataList != null) {
            for (Struct.Unsigned16 val : dataList) {
                sd += "" + val.get() + ";";
            }
        }
        return sd + "}";
    }

}
