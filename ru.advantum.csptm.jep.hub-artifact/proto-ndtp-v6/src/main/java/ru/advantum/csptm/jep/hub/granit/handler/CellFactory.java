/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.advantum.csptm.jep.hub.granit.handler;

import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Npl;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.G6NphHeader;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.G6NphResult;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells.*;
import ru.advantum.tools.HEX;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author Kotin <kotin@advantum.ru>
 */
public enum CellFactory {

    /*Ячейка передачи основных навигационных данных*/
    P_G6CellNav00(0, G6CellNav00.class),
    /*Данные от внутренних датчиков */
    P_G6CellIntSensor02(2, G6CellIntSensor02.class),
    /*Данные от устройства Корона*/
    P_G6CellCrown03(3, G6CellCrown03.class),
    /*Данные от устройства IRMA*/
    P_G6CellIrma04(4, G6CellIrma04.class, true),
    /*Данные от пульта КДМ.*/
    P_G6CellKdm05(5, G6CellKdm05.class),
    /*Данные от счетчика – интеллектуальный датчик навигатора.*/
    P_G6CellIdn06(6, G6CellIdn06.class),
    /*Данные от цифрового датчика – интеллектуальный датчик навигатора.*/
    P_G6CellIdn07(7, G6CellIdn07.class),
    /*Данные от топливного датчика УЗИ-M.*/
    P_G6CellUsi08(8, G6CellUsi08.class, true),
    /*Данные от регистратора.*/
    P_G6CellReg09(9, G6CellReg09.class),
    /*Данные от CAN модуля M333.*/
    P_G6CellCan10(10, G6CellCan10.class),
    /*Данные от RFID.*/
    P_G6CellRfid12(12, G6CellRfid12.class),
    /*Данные от датчика уровня продукта в отсеке.*/
    P_G6CellPlo13(13, G6CellPlo13.class),
    /*Данные от датчика BMS Liaz..*/
    P_G6CellBms14(14, G6CellBms14.class),
    /*Данные от датчика LLS Sensor.*/
    P_G6CellLls15(15, G6CellLls15.class),
    /*Данные от датчика температуры TERMO.*/
    P_G6CellTermo16(16, G6CellTermo16.class),
    /*
    Более полная реализация ndtp
    */
    /*1-й спиртометр*/
    P_G6CellAlcohol1st17(17, G6CellAlcohol1st17.class),
    /*CAN*/
    P_G6CellCAN18(18, G6CellCAN18.class),
    /*Станции GSM*/
    P_G6CellGSMstations19(19, G6CellGSMstations19.class),
    /*CAN M333 (щетки и посыпалки)*/
    P_G6CellM333CAN20(20, G6CellM333CAN20.class),
    /*2-й спиртометр*/
    P_G6CellAlcohol2nd21(21, G6CellAlcohol2nd21.class),
    /*Статистика связи с сервером*/
    P_G6CellServerStatistics22(22, G6CellServerStatistics22.class),
    /*Статистика трекера*/
    P_G6CellTrackerStatistics23(23, G6CellTrackerStatistics23.class),
    /*Упакованные данные всяких датчиков (пока только марвис, чтото дорожно-экплуатац.)*/
    P_G6CellZipSensorData100(100, G6CellZipSensorData100.class);

    private final static Map<Integer, CellFactory> LOOKUP;

    static {
        LOOKUP = new HashMap<>();

        for (CellFactory cell : CellFactory.values()) {
            LOOKUP.put(cell.typeId, cell);
        }
    }

    final int typeId;
    final Class<? extends G6Base> protoType;
    final boolean withNumber;

    CellFactory(int typeId, Class<? extends G6Base> protoType, boolean withNumber) {
        this.typeId = typeId;
        this.protoType = protoType;
        this.withNumber = withNumber;
    }

    CellFactory(int typeId, Class<? extends G6Base> protoType) {
        this(typeId, protoType, false);
    }

    public static Optional<G6Base> createPrototype(GranitHubConfig.Ndtp ndtpConfig,
                                                   int typeId,
                                                   int number) throws GranitException.TypeNavDataException {
        CellFactory cell = LOOKUP.get(typeId);
        if (cell == null) {
            return Optional.empty();
        }

        try {
            if (cell.withNumber) {
                return Optional.of(cell.protoType.getConstructor(GranitHubConfig.Ndtp.class, int.class).newInstance(ndtpConfig, number));
            } else {
                return Optional.of(cell.protoType.getConstructor(GranitHubConfig.Ndtp.class).newInstance(ndtpConfig));
            }
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new GranitException.TypeNavDataException(typeId, e);
        }
    }

    public static byte[] wrapNphPacket(G6Base nphPacket,
                                       int serviceId,
                                       int type,
                                       long requestId,
                                       boolean ack,
                                       Integer nplPeerAddress) throws IOException {
        try (ByteArrayOutputStream byteResponse = new ByteArrayOutputStream();
             ByteArrayOutputStream trailer = new ByteArrayOutputStream()) {
            final G6NphHeader nphHeader = new G6NphHeader(serviceId,
                                                          type,
                                                          ack,
                                                          requestId); // подтверждение, наверное, надо отправлять с тем же requestId

            nphHeader.write(trailer);
            nphPacket.write(trailer);
            byte[] buff = trailer.toByteArray();

            int crc = HEX.crc16modbus(buff);
            // чек-сумма у них записывается в big-endian. поэтому перевернем ее, чтобы не париться.
            crc = ((crc & 0xFF00) >> 8) | ((crc & 0xFF) << 8);

            final G6Npl nplHeader = new G6Npl(G6Npl.SIGNATURE,
                                              nphHeader.size() + nphPacket.size(),
                                              false, // шифровать не будем
                                              false, // Если выставить флаг подсчета контрольной суммы true не работает
                                              false,
                                              crc,
                                              (short) G6Npl.NplType.NPL_TYPE_NPH.getCode(),
                                              nplPeerAddress == null ? G6Npl.NplPeerAddress.NPL_ADDRESS_SERVER.getCode() : nplPeerAddress,
                                              0 // или не будем
            );

            nplHeader.write(byteResponse);
            byteResponse.write(buff);

            return byteResponse.toByteArray();
        }
    }

    public static byte[] wrapNphResultPacket(G6NphResult nphPacket, int serviceId, long requestId) throws IOException {
        try (ByteArrayOutputStream byteResponse = new ByteArrayOutputStream();
             ByteArrayOutputStream trailer = new ByteArrayOutputStream()) {
            final G6NphHeader nphHeader = new G6NphHeader(serviceId,
                                                          G6NphHeader.NPH_RESULT,
                                                          true, // поглядим, что будет, если "пакет требует подтверждения"
                                                          requestId); // подтверждение, наверное, надо отправлять с тем же requestId

            nphHeader.write(trailer);
            nphPacket.write(trailer);
            byte[] buff = trailer.toByteArray();

            int crc = HEX.crc16modbus(buff);
            // чек-сумма у них записывается в big-endian. поэтому перевернем ее, чтобы не париться.
            crc = ((crc & 0xFF00) >> 8) | ((crc & 0xFF) << 8);

            final G6Npl nplHeader = new G6Npl(G6Npl.SIGNATURE,
                                              nphHeader.size() + nphPacket.size(),
                                              false, // шифровать не будем
                                              // Если выставить флаг подсчета контрольной суммы true не работает
                                              false,
                                              false,
                                              crc,
                                              (short) G6Npl.NplType.NPL_TYPE_NPH.getCode(),
                                              G6Npl.NplPeerAddress.NPL_ADDRESS_SERVER.getCode(),
                                              // (int) (System.currentTimeMillis() % Integer.MAX_VALUE, // относительно уникальный requestId
                                              // NPL_REQUEST_ID.incrementAndGet()/* % Short.MAX_VALUE*/ // давайте попробуем нормальный уникальный
                                              0 // или не будем
            );

            nplHeader.write(byteResponse);
            byteResponse.write(buff);

            return byteResponse.toByteArray();
        }
    }

}
