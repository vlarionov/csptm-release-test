package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.generic.controls;

import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

public class G6NphPrnav extends G6Base {

    public final Unsigned32 sendDataInterval = new Unsigned32();
    public final Unsigned32 sendDataInterval_stop = new Unsigned32();
    public final Unsigned32 angle = new Unsigned32();
    public final Unsigned32 dist = new Unsigned32();

    @Override
    public String extractString() {
        return this.getClass().getSimpleName() + ": {"
                + "sendDataInterval=" + sendDataInterval.get()
                + ",sendDataInterval_stop=" + sendDataInterval_stop.get()
                + ",angle=" + angle.get()
                + ",dist=" + dist.get()
                + "}";
    }

}
