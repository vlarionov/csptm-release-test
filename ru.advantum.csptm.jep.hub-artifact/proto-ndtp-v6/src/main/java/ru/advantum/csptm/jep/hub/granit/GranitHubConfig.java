package ru.advantum.csptm.jep.hub.granit;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.G6NphSrvNavdataConstant;
import ru.advantum.csptm.jep.netty.NettyHubConfig;

import java.util.ArrayList;
import java.util.List;
@SuppressWarnings("unused")
@Root(name = "granit-hub")
public class GranitHubConfig {


    @Element(name = "hub")
    public final NettyHubConfig hubConfig = new NettyHubConfig();

    @Element(name = "ndtp", required = false)
    public Ndtp ndtp = new Ndtp();

    @Root(name = "ndtp")
    public static class Ndtp {
        @Attribute(name = "detail-info-log", required = false)
        public boolean detailInfoLog = true;
        @Attribute(name = "sensors", required = false)
        public boolean sensors = true;

        @Attribute(name = "silent-mode-on", required = false)
        public boolean silentModeOn = false;

        @Attribute(name = "sensors-classical-mode-on", required = false)
        public boolean sensorsInClassicalModeOn = false;

        @Attribute(name = "custom-alarm-din-number", required = false)
        public int customAlarmDinNum = 0;

        @Attribute(name = "custom-alarm-din-on", required = false)
        public int customAlarmDinOn = 0;

        @Attribute(name = "crutch-usi08-status2-on", required = false)
        public boolean crutchUsi08Status2On = false;

        @ElementList(name = "cells", required = false)
        public List<Cell> cells = new ArrayList<>();

        public Ndtp() {
        }

        @Element(name="cell-00", required = false)
        public G6NphSrvNavdataConstant.G6Cell00UnusedNumbers cell00Numbers = new G6NphSrvNavdataConstant.G6Cell00UnusedNumbers();
        @Element(name="cell-02", required = false)
        public G6NphSrvNavdataConstant.G6Cell02Numbers cell02Numbers = new G6NphSrvNavdataConstant.G6Cell02Numbers();
        @Element(name="cell-03", required = false)
        public G6NphSrvNavdataConstant.G6Cell03Numbers cell03Numbers = new G6NphSrvNavdataConstant.G6Cell03Numbers();
        @Element(name="cell-04", required = false)
        public G6NphSrvNavdataConstant.G6Cell04Numbers cell04Numbers = new G6NphSrvNavdataConstant.G6Cell04Numbers();
        @Element(name="cell-05", required = false)
        public G6NphSrvNavdataConstant.G6Cell05Numbers cell05Numbers = new G6NphSrvNavdataConstant.G6Cell05Numbers();
        @Element(name="cell-06", required = false)
        public G6NphSrvNavdataConstant.G6Cell06Numbers cell06Numbers = new G6NphSrvNavdataConstant.G6Cell06Numbers();
        @Element(name="cell-07", required = false)
        public G6NphSrvNavdataConstant.G6Cell07Numbers cell07Numbers = new G6NphSrvNavdataConstant.G6Cell07Numbers();
        @Element(name="cell-08", required = false)
        public G6NphSrvNavdataConstant.G6Cell08Numbers cell08Numbers = new G6NphSrvNavdataConstant.G6Cell08Numbers();
        @Element(name="cell-09", required = false)
        public G6NphSrvNavdataConstant.G6Cell09Numbers cell09Numbers = new G6NphSrvNavdataConstant.G6Cell09Numbers();
        @Element(name="cell-10", required = false)
        public G6NphSrvNavdataConstant.G6Cell10Numbers cell10Numbers = new G6NphSrvNavdataConstant.G6Cell10Numbers();
        @Element(name="cell-12", required = false)
        public G6NphSrvNavdataConstant.G6Cell12Numbers cell12Numbers = new G6NphSrvNavdataConstant.G6Cell12Numbers();
        @Element(name="cell-13", required = false)
        public G6NphSrvNavdataConstant.G6Cell13Numbers cell13Numbers = new G6NphSrvNavdataConstant.G6Cell13Numbers();
        @Element(name="cell-14", required = false)
        public G6NphSrvNavdataConstant.G6Cell14Numbers cell14Numbers = new G6NphSrvNavdataConstant.G6Cell14Numbers();
        @Element(name="cell-15", required = false)
        public G6NphSrvNavdataConstant.G6Cell15Numbers cell15Numbers = new G6NphSrvNavdataConstant.G6Cell15Numbers();
        @Element(name="cell-16", required = false)
        public G6NphSrvNavdataConstant.G6Cell16Numbers cell16Numbers = new G6NphSrvNavdataConstant.G6Cell16Numbers();
        @Element(name="cell-17", required = false)
        public G6NphSrvNavdataConstant.G6Cell17Numbers cell17Numbers = new G6NphSrvNavdataConstant.G6Cell17Numbers();
        @Element(name="cell-18", required = false)
        public G6NphSrvNavdataConstant.G6Cell18Numbers cell18Numbers = new G6NphSrvNavdataConstant.G6Cell18Numbers();
        @Element(name="cell-19", required = false)
        public G6NphSrvNavdataConstant.G6Cell19Numbers cell19Numbers = new G6NphSrvNavdataConstant.G6Cell19Numbers();
        @Element(name="cell-20", required = false)
        public G6NphSrvNavdataConstant.G6Cell20Numbers cell20Numbers = new G6NphSrvNavdataConstant.G6Cell20Numbers();
        @Element(name="cell-21", required = false)
        public G6NphSrvNavdataConstant.G6Cell21Numbers cell21Numbers = new G6NphSrvNavdataConstant.G6Cell21Numbers();
        @Element(name="cell-22", required = false)
        public G6NphSrvNavdataConstant.G6Cell22Numbers cell22Numbers = new G6NphSrvNavdataConstant.G6Cell22Numbers();

        public static class Cell {
            @Attribute(name = "cell-number", required = false)
            public int cellNumber = 2;
            @Attribute(name = "ndtp-sensor-number", required = false)
            public int ndtpNumber = 0;
            @Attribute(name = "jep-sensor-number", required = false)
            public int jepNumber = 2;
            @Attribute(name = "boolean-cast", required = false)
            public boolean cast = false;

        }

        public Cell lookup( int cn, int fn) {
            for( Cell c: cells) {
                if ( c.cellNumber == cn && c.ndtpNumber == fn) {
                    return c;
                }
            }
            return null;
        }

    }



}
