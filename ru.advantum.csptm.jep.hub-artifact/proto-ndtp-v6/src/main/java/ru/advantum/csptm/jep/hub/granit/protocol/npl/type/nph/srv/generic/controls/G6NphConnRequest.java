package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.generic.controls;

import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

/**
 * @author Kotin <kotin@advantum.ru> Пакет запроса установки соединения
 * NPH_SGC_CONN_REQUEST
 */
public class G6NphConnRequest extends G6Base {

    public static final int NDTP_VERSION_HIGH = 6;
    public static final int NDTP_VERSION_LOW = 2;
    private final BitField protoVersionHigh = new BitField(16);
    private final BitField protoVersionLow = new BitField(16);
    private final BitField connectionEncryptionFlag = new BitField(1);
    private final BitField connectionCrcFlag = new BitField(1);
    private final BitField connectionSimulateFlag = new BitField(1);
    private final BitField connectionFlags$ = new BitField(13);
    private final BitField peerAddress = new BitField(32);
    private final BitField maxPacketSize = new BitField(32);
    private final BitField var = new BitField(32);
    ;
    public ByteSet imei = new ByteSet(15);
    public ByteSet imsi = new ByteSet(16);

    public G6NphConnRequest(int protoVersionHigh,
                            int protoVersionLow,
                            boolean connectionEncryptionFlag,
                            boolean connectionCrcFlag,
                            boolean connectionSimulateFlag,
                            long peerAddress,
                            long maxPacketSize) {
        this.protoVersionHigh.set(protoVersionHigh);
        this.protoVersionLow.set(protoVersionLow);
        this.setConnectionEncryptionFlag(connectionEncryptionFlag);
        this.setConnectionCrcFlag(connectionCrcFlag);
        this.setConnectionSimulateFlag(connectionSimulateFlag);
        this.peerAddress.set(peerAddress);
        this.maxPacketSize.set(maxPacketSize);
    }

    public G6NphConnRequest() {
    }

    public static InetAddress intToInetAddress(Integer value) {
        ByteBuffer buffer = ByteBuffer.allocate(32);
        buffer.putInt(value);
        buffer.position(0);
        byte[] bytes = new byte[4];
        buffer.get(bytes);
        try {
            return InetAddress.getByAddress(bytes);
        } catch (UnknownHostException ignored) {
        }
        return InetAddress.getLoopbackAddress();
    }

    public boolean getConnectionEncryptionFlag() {
        return this.connectionEncryptionFlag.intValue() == 1;
    }

    public void setConnectionEncryptionFlag(boolean connectionEncryptionFlag) {
        this.connectionEncryptionFlag.set(connectionEncryptionFlag ? 1 : 0);
    }

    public boolean getConnectionCrcFlag() {
        return this.connectionCrcFlag.intValue() == 1;
    }

    public void setConnectionCrcFlag(boolean connectionCrcFlag) {
        this.connectionCrcFlag.set(connectionCrcFlag ? 1 : 0);
    }

    public boolean getConnectionSimulateFlag() {
        return this.connectionSimulateFlag.intValue() == 1;
    }

    public void setConnectionSimulateFlag(boolean connectionSimulateFlag) {
        this.connectionSimulateFlag.set(connectionSimulateFlag ? 1 : 0);
    }

    public int getConnectionFlags$() {
        return connectionFlags$.intValue();
    }

    public void setConnectionFlags$(int connectionFlags$) {
        this.connectionFlags$.set(connectionFlags$);
    }

    public boolean isEncrypted() {
        return getConnectionEncryptionFlag();
    }

    public long getPeerAddress() {
        return peerAddress.longValue();
    }

    public int getProtoVersionHigh() {
        return protoVersionHigh.intValue();
    }

    public int getProtoVersionLow() {
        return protoVersionLow.intValue();
    }

    public String getIMEI() {
        return imei.asString();
    }

    public String getIMSI() {
        return imsi.asString();
    }

    @Override
    public String extractString() {
        return "G6NphConnRequest:\n\t{" + "protoVersionHigh=" + protoVersionHigh.intValue() +
                ", protoVersionLow=" + protoVersionLow.intValue() +
                ", connectionEncryptionFlag=" + getConnectionEncryptionFlag() +
                ", connectionCrcFlag=" + getConnectionCrcFlag() +
                ", connectionSimulateFlag=" + getConnectionSimulateFlag() +
                ", connectionFlags$=" + getConnectionFlags$() +
                ", maxPacketSize=" + maxPacketSize.intValue() +
                ", peerAddress=" + peerAddress.intValue() +
                ", imei=" + imei.asString() +
                ", imsi=" + imsi.asString() +
                '}';
    }
}
