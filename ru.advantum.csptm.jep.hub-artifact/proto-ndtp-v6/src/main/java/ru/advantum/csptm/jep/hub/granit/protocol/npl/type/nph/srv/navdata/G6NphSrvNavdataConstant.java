package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata;

import org.simpleframework.xml.Attribute;
import ru.advantum.csptm.service.csd.ExtendedData;

@SuppressWarnings("unused")
public class G6NphSrvNavdataConstant {

    public static class G6Cell00UnusedNumbers {
        @Attribute(required = false)
        public int distance_num = ExtendedData.ODOMETER;
        @Attribute(required = false)
        public int alt_num = ExtendedData.ALT;
        @Attribute(required = false)
        public int bit8on_num = ExtendedData.VALIDITY;
        @Attribute(required = false)
        public int bit7on_num = ExtendedData.LON_HSPH;
        @Attribute(required = false)
        public int bit6on_num = ExtendedData.LAT_HSPH;
        @Attribute(required = false)
        public int bit5on_num = ExtendedData.IBATT;
        @Attribute(required = false)
        public int bit4on_num = ExtendedData.FST_SWON;
        @Attribute(required = false)
        public int bit3on_num = ExtendedData.SOS;
        @Attribute(required = false)
        public int bit2on_num = ExtendedData.ALARM;
        @Attribute(required = false)
        public int bit1on_num = ExtendedData.CALL_REQ;
        @Attribute(required = false)
        public int bat_voltage_num = ExtendedData.BATT_V;
        @Attribute(required = false)
        public int speed_max_num = ExtendedData.MAX_SPD;
        @Attribute(required = false)
        public int nsats_num = ExtendedData.SATS;
/*
        @Attribute(required = false)
        public int track_num = 14;
        @Attribute(required = false)
        public int pdop_num = 15;
*/

    }

    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Данные от внутренних датчиков Type=2, N=0(1)
     */
    public static class G6Cell02Numbers {
        public final static int BASE_OFF = 210;
        @Attribute(required = false)
        public int an_in0 = BASE_OFF;
        @Attribute(required = false)
        public int an_in1 = BASE_OFF + 1;
        @Attribute(required = false)
        public int an_in2 = BASE_OFF + 2;
        @Attribute(required = false)
        public int an_in3 = BASE_OFF + 3;
        @Attribute(required = false)
        public int di_in = BASE_OFF + 4;
        @Attribute(required = false)
        public int di_out = BASE_OFF + 12;
        @Attribute(required = false)
        public int di0_counter = BASE_OFF + 20;
        @Attribute(required = false)
        public int di1_counter = BASE_OFF + 21;
        @Attribute(required = false)
        public int di2_counter = BASE_OFF + 22;
        @Attribute(required = false)
        public int di3_counter = BASE_OFF + 23;
        @Attribute(required = false)
        public int odometer = BASE_OFF + 24;
        @Attribute(required = false)
        public int csq = BASE_OFF + 25;
        @Attribute(required = false)
        public int gprs_state = BASE_OFF + 26;
        @Attribute(required = false)
        public int accel_energy = BASE_OFF + 27;
        @Attribute(required = false)
        public int ext_volt = BASE_OFF + 28;
    }

    public static class G6Cell03Numbers {
        /**
         * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
         * Данные от устройства Корона. Type=3, N=0 (1)
         * поле	длина	тип	описание
         * 14	struct
         * <odometer>	4	unsigned int32	Одометр. Циклический счётчик пробега в метрах.
         * <zone>	2	unsigned int16	Номер зоны для позонной оплаты
         * <corona_door_in1>	1	unsigned int8	Количество вошедших пассажиров через первую дверь.
         * <corona_door_in2>	1	unsigned int8	Количество вошедших пассажиров через вторую дверь.
         * <corona_door_in3>	1	unsigned int8	Количество вошедших пассажиров через третью дверь.
         * <corona_door_in4>	1	unsigned int8	Количество вошедших пассажиров через четвертую дверь.
         * <corona_door_out1>	1	unsigned int8	Количество вышедших пассажиров через первую дверь.
         * <corona_door_out2>	1	unsigned int8	Количество вышедших пассажиров через вторую дверь.
         * <corona_door_out3>	1	unsigned int8	Количество вышедших пассажиров через третью дверь.
         * <corona_door_out4>	1	unsigned int8	Количество вышедших пассажиров через четвертую дверь.
         * </editor-fold>
         */
        @Attribute(required = false)
        public int odometerCrown = 15;
        @Attribute(required = false)
        public int zoneCrown = 16;
        @Attribute(required = false)
        public int corona_door_in1 = 17;
        @Attribute(required = false)
        public int corona_door_in2 = 18;
        @Attribute(required = false)
        public int corona_door_in3 = 19;
        @Attribute(required = false)
        public int corona_door_in4 = 20;
        @Attribute(required = false)
        public int corona_door_out1 = 21;
        @Attribute(required = false)
        public int corona_door_out2 = 22;
        @Attribute(required = false)
        public int corona_door_out3 = 23;
        @Attribute(required = false)
        public int corona_door_out4 = 24;

    }

    public static class G6Cell04Numbers {
        public final static int BASE_OFF = 400;
        /**
         * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
         * Данные от устройства IRMA. Type=4, N=0 (1)
         * поле	длина	тип	описание
         * 15	struct
         * <odometer>	4	unsigned int32	Одометр. Циклический счётчик пробега в метрах.
         * <zone>	2	unsigned int16	Номер зоны для позонной оплаты
         * <irma_door_in1>	1	unsigned int8	Количество вошедших пассажиров через первую дверь.
         * <irma_door_in2>	1	unsigned int8	Количество вошедших пассажиров через вторую дверь.
         * <irma_door_in3>	1	unsigned int8	Количество вошедших пассажиров через третью дверь.
         * <irma_door_in4>	1	unsigned int8	Количество вошедших пассажиров через четвертую дверь.
         * <irma_door_out1>	1	unsigned int8	Количество вышедших пассажиров через первую дверь.
         * <irma_door_out2>	1	unsigned int8	Количество вышедших пассажиров через вторую дверь.
         * <irma_door_out3>	1	unsigned int8	Количество вышедших пассажиров через третью дверь.
         * <irma_door_out4>	1	unsigned int8	Количество вышедших пассажиров через четвертую дверь.
         * <irma_present_door>	1	unsigned int8	Поле – битовая маска присутствия двери (bit3..bit0)
         * 1 -  дверь присутствует
         * 0 – дверь не найдена
         * битовая маска закрытия двери (bit7..bit4). То есть если бит установлен в 1, то дверь закрывалась на остановке.
         * 1 -  дверь закрывалась
         * 0 – дверь не закрывалась
         * </editor-fold>
         */
        @Attribute(required = false)
        public int odometerIrma = BASE_OFF + 1;// unsigned int32	Одометр. Циклический счётчик пробега в метрах.
        @Attribute(required = false)
        public int zoneIrma = BASE_OFF + 2;//	unsigned int16	Номер зоны для позонной оплаты
        @Attribute(required = false)
        public int irma_door_in1 = BASE_OFF + 3;//Количество вошедших пассажиров через первую дверь.
        @Attribute(required = false)
        public int irma_door_in2 = BASE_OFF + 4;//Количество вошедших пассажиров через вторую дверь.
        @Attribute(required = false)
        public int irma_door_in3 = BASE_OFF + 5;//Количество вошедших пассажиров через третью дверь.
        @Attribute(required = false)
        public int irma_door_in4 = BASE_OFF + 6;//Количество вошедших пассажиров через четвертую дверь.
        @Attribute(required = false)
        public int irma_door_out1 = BASE_OFF + 7;//Количество вышедших пассажиров через первую дверь.
        @Attribute(required = false)
        public int irma_door_out2 = BASE_OFF + 8;//Количество вышедших пассажиров через вторую дверь.
        @Attribute(required = false)
        public int irma_door_out3 = BASE_OFF + 9;//Количество вышедших пассажиров через третью дверь.
        @Attribute(required = false)
        public int irma_door_out4 = BASE_OFF + 10;//Количество вышедших пассажиров через четвертую дверь.
        @Attribute(required = false)
        public int irma_present_door1 = BASE_OFF + 11;
        @Attribute(required = false)
        public int irma_present_door2 = BASE_OFF + 12;
        @Attribute(required = false)
        public int irma_present_door3 = BASE_OFF + 13;
        @Attribute(required = false)
        public int irma_present_door4 = BASE_OFF + 14;
        @Attribute(required = false)
        public int irma_closed_door1 = BASE_OFF + 15;
        @Attribute(required = false)
        public int irma_closed_door2 = BASE_OFF + 16;
        @Attribute(required = false)
        public int irma_closed_door3 = BASE_OFF + 17;
        @Attribute(required = false)
        public int irma_closed_door4 = BASE_OFF + 18;

    }

    public static class G6Cell08Numbers {
        public final static int BASE_OFF = 800;
        /**
         * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
         * Данные от топливного датчика УЗИ-M . Type=8, N=0 (1…n)
         * поле	длина	тип	Описание
         * 6	struct
         * <det_status>	1	unsigned int8	Состояние датчика топлива.
         * <level_mm>	2	unsigned int16	Уровень топлива в баке в мм
         * <level_l>	2	unsigned int16	Уровень топлива в баке в литрах
         * <temperature>	1	unsigned int8	Температура бака
         * </editor-fold>
         */
        @Attribute(required = false)
        public int det_status = BASE_OFF;
        @Attribute(required = false)
        public int level_mm = BASE_OFF + 1;
        @Attribute(required = false)
        public int level_l = BASE_OFF + 2;
        @Attribute(required = false)
        public int temperature08 = BASE_OFF + 3;

    }

    public static class G6Cell09Numbers {
        /**
         * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
         * Данные от регистратора . Type=9, N=0 (1…n)
         * поле	Длина	тип	описание
         * 40	struct
         * <ID>	8	unsigned int64	ID регистратора
         * <Name>	32	unsigned int8	Массив данных
         * считанный из регистратора размером 32 байта. Не воспринимать как строку символов заканчивающейся 0.
         * </editor-fold>
         */
        @Attribute(required = false)
        public int id = 172;
        @Attribute(required = false)
        public int name = 173;

    }

    public static class G6Cell10Numbers {
        public final static int BASE_OFF = 1000;
        /**
         * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
         * Данные от CAN модуля M333. Type=10, N=0 (1…n)
         * поле	длина	Тип	описание
         * 37	struct
         * <SecFlagStatus>	4	unsigned int32	Флаг работоспособности устройства и флаги безопасностности (Security state flags)
         * <AllTimeEngine>	4	unsigned int32	Полное время работы двигателя ч * 100. То есть 1ч 15 мин = 125
         * <AllTrack>	4	unsigned int32	Полный пробег транспортного средства км * 100
         * <AllFuelConsum>	4	unsigned int32	Полный расход топлива л
         * <FuelLevel>	2	unsigned int16	bit15 – определяет единицу ихмерения уровня топлива.
         * Если равен 1, то проценты, если 0 — то литры.
         * bit0 .. bit14 – уровень топлива
         * <SpeedTurnEngine>	2	unsigned int16	Скорость оборотов двигателя rpm
         * <TEngine>	2	signed int16	Температура двигателя °C
         * <Speed>	1	unsigned int8	Скорость тр средства км/ч
         * <PressureAxis>	2	unsigned int16	Давление на ось 1 кг * 10
         * <PressureAxis>	2	unsigned int16	Давление на ось 2 кг * 10
         * <PressureAxis>	2	unsigned int16	Давление на ось 3 кг * 10
         * <PressureAxis>	2	unsigned int16	Давление на ось 4 кг * 10
         * <PressureAxis>	2	unsigned int16	Давление на ось 5 кг * 10
         * <FlagAlarm>	4	unsigned int32	Контроллеры аварии (см. описание ниже)
         * </editor-fold>
         */
        @Attribute(required = false)
        public int secFlagStatus = BASE_OFF;
        @Attribute(required = false)
        public int allTimeEngine = BASE_OFF + 1;
        @Attribute(required = false)
        public int allTrack = BASE_OFF + 2;
        @Attribute(required = false)
        public int allFuelConsum = BASE_OFF + 3;
        @Attribute(required = false)
        public int fuelLevel = BASE_OFF + 4;
        @Attribute(required = false)
        public int speedTurnEngine = BASE_OFF + 5;
        @Attribute(required = false)
        public int tEngine = BASE_OFF + 6;
        @Attribute(required = false)
        public int speed = BASE_OFF + 7;
        @Attribute(required = false)
        public int pressureAxis0 = BASE_OFF + 8;
        @Attribute(required = false)
        public int pressureAxis1 = BASE_OFF + 9;
        @Attribute(required = false)
        public int pressureAxis2 = BASE_OFF + 10;
        @Attribute(required = false)
        public int pressureAxis3 = BASE_OFF + 11;
        @Attribute(required = false)
        public int pressureAxis4 = BASE_OFF + 12;
        @Attribute(required = false)
        public int flagAlarm = BASE_OFF + 13;

    }

    public static class G6Cell12Numbers {
        /**
         * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
         * Данные от RFID. Type=12, N=0
         * поле	длина	Тип	описание
         * 5	struct
         * <Key>	5	unsigned int8	Считанный RFID ключ
         * </editor-fold>
         */
        @Attribute(required = false)
        public int rfidKey = 135;

    }

    public static class G6Cell13Numbers {
        /**
         * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
         * Данные от датчика уровня продукта в отсеке . Type=13, N=0 (1…n)
         * поле	длина	Тип	Описание
         * 13	struct
         * <density>	4	float	Плотность продукта
         * <temperature>	4	float	Температура продукта
         * <level>	4	float	Уровень продукта
         * <level_unit>	1	unsigned int8	Единица измерения
         * 1 - %
         * 2 — мм
         * 3 м3
         * </editor-fold>
         */
        @Attribute(required = false)
        public int density13 = 174;
        @Attribute(required = false)
        public int temperature13 = 175;
        @Attribute(required = false)
        public int level = 176;
        @Attribute(required = false)
        public int levelUnit = 177;

    }

    public static class G6Cell14Numbers {
        /**
         * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
         * Данные от датчика BMS Liaz. Type=14, N=0 (1…n)
         * поле	длина	тип	описание
         * 13	struct
         * <max_temperature>	2	unsigned int16	Максимальная температура
         * <min_cell_voltage>	2	unsigned int16	Минимальное напряжение ячейки * 0,0015 (Вольт)
         * <max_cell_voltage>	2	unsigned int16	Максимальное напряжение ячейки * 0,0015 (Вольт)
         * <voltage>	4	unsigned int32	Общее напряжение в вольтах * 0,0015 (Вольт)
         * <code_error>	1	unsigned int8	Коды ошибок от устройства (если установлен бит, то)
         * 0 бит — Зафиксировано превышение допустимого напряжения на одной из ячеек
         * 1 бит — зафиксировано напряжение на одной из ячеек ниже допустимого уровня
         * 2 бит — зафиксировано превышение температуры на одной из ячеек
         * 3 бит — зафиксировано превышение максимально допустимого тока заряда или разряда
         * <current>	4	unsigned int32	Ток батареи в Амперах
         * </editor-fold>
         */
        @Attribute(required = false)
        public int max_temperature = 135;
        @Attribute(required = false)
        public int min_cell_voltage = 136;
        @Attribute(required = false)
        public int max_cell_voltage = 137;
        @Attribute(required = false)
        public int voltage = 138;
        @Attribute(required = false)
        public int code_error0 = 139;
        @Attribute(required = false)
        public int code_error1 = 140;
        @Attribute(required = false)
        public int code_error2 = 141;
        @Attribute(required = false)
        public int code_error3 = 142;
        @Attribute(required = false)
        public int code_error$ = 143;
        @Attribute(required = false)
        public int current = 144;

    }

    public static class G6Cell15Numbers {
        /**
         * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
         * Данные от датчика LLS Sensor Type=15, N=0 (1…n)
         * поле	длина	Тип	описание
         * 50	struct
         * <Status>	2	unsigned int16
         * <main_float_level>	4	unsigned int32	Уровень основного поплавка
         * <Temperature_average>	4	unsigned int32	Средняя температура в продукте
         * <Percent_of_volume>	4	unsigned int32	Процентное заполнение по объему
         * <total_Volume>	4	unsigned int32	Общий объем
         * <Weight>	4	unsigned int32	Масса
         * <Density>	4	unsigned int32	Плотность
         * <Net_Standard_Volume>	4	unsigned int32	Объем основного продукта
         * <Level_of_water>	4	unsigned int32	Уровень подтоварной воды
         * <Pressure>	4	unsigned int32	Давление
         * <Vapor_temperature_average>	4	unsigned int32	Средняя температура в паровой фазе
         * <Vapor_Weight>	4	unsigned int32	Масса паровой фазы
         * <Liquid_phase_Weight>	4	unsigned int32	Масса жидкой фазы
         * </editor-fold>
         */
        @Attribute(required = false)
        public int status15 = 145;
        @Attribute(required = false)
        public int main_float_level = 146;
        @Attribute(required = false)
        public int temperature_average = 147;
        @Attribute(required = false)
        public int percent_of_volume = 148;
        @Attribute(required = false)
        public int total_Volume = 149;
        @Attribute(required = false)
        public int weight15 = 150;
        @Attribute(required = false)
        public int density15 = 151;
        @Attribute(required = false)
        public int net_Standard_Volume = 152;
        @Attribute(required = false)
        public int level_of_water = 153;
        @Attribute(required = false)
        public int pressure = 154;
        @Attribute(required = false)
        public int vapor_temperature_average = 155;
        @Attribute(required = false)
        public int vapor_Weight = 156;
        @Attribute(required = false)
        public int liquid_phase_Weight = 157;

    }

    public static class G6Cell16Numbers {
        public final static int BASE_OFF = 1600;
        /**
         * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
         * Данные от датчика температуры TERMO Type=16, N=0
         * поле	длина	Тип	описание
         * 8	struct
         * <Status>	4	unsigned int32	Если не 0 то нет связи с датчиком
         * <temp>	4	signed int32	Температура в градусах Цельсия
         * </editor-fold>
         */
        @Attribute(required = false)
        public int status16 = BASE_OFF;
        @Attribute(required = false)
        public int temp16 = BASE_OFF + 1;

    }

    public static class G6Cell05Numbers {
        /**
         * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
         * Данные от пульта КДМ. Type 5, N=0(1)
         * <PGM_enable>	1	unsigned int8	Признак работы оборудования распределителя:
         * дискретный параметр со значениями: 1 - включено, 0 - выключено.
         * <PGM_width>	1	unsigned int8	Параметр: ширина посыпки/распределения ПГМ (метры).
         * Диапазон значений 0- 15 метров. Точность: единицы метров.
         * <PGM_density>	2	unsigned int16	Параметр: плотность посыпки или распределения ПГМ (г/м2).
         * Диапазон значений: 0 - 511 г./м2; Точность: единицы граммов.
         * <plough_state>	1	unsigned int8	Признак: плуг опущен/поднят: дискретный параметр со значениями:
         * 1 - опущен,   0 - поднят.
         * <brush_state>	1	unsigned int8	Признак щетки включены/выключены (средний гребень опущен/поднят):
         * дискретный параметр со значениями: 1 - включены,   0 - выключены.
         * </editor-fold>
         */
        @Attribute(required = false)
        public int pgmEnable = 160;
        @Attribute(required = false)
        public int pgmWidth = 161;
        @Attribute(required = false)
        public int pgmDensity = 162;
        @Attribute(required = false)
        public int ploughState = 163;
        @Attribute(required = false)
        public int brushState = 164;
    }

    public static class G6Cell06Numbers {
        @Attribute(required = false)
        public int num_impulse_min = 165;
        @Attribute(required = false)
        public int num_impulse_max = 166;
        @Attribute(required = false)
        public int time = 167;
        @Attribute(required = false)
        public int num_overflow = 168;
        @Attribute(required = false)
        public int num_impulse = 169;
        @Attribute(required = false)
        public int pres_impulse = 170;

    }

    public static class G6Cell07Numbers {
        /**
         * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
         * Данные от цифрового датчика – интеллектуальный датчик навигатора. Type=7, N=0 (1…n)
         * поле	длина	тип	Описание
         * 1	struct
         * <value>	1	unsigned int8	Состояние интеллектуального цифрового датчика. Принимает значения:
         * 0 – Норма
         * 1 – Тревога
         * 2 – Обрыв
         * 3 – Замыкание на массу
         * 4 – Замыкание на питание
         * </editor-fold>
         */
        @Attribute(required = false)
        public int value = 171;

    }

    public static class G6Cell17Numbers {
        /**
         * Type 17
         */
        @Attribute(required = false)
        public int alcoEvent = 208;
        @Attribute(required = false)
        public int alcoholVolume = 209;
        @Attribute(required = false)
        public int startTime17 = 210;
        @Attribute(required = false)
        public int endTime17 = 211;
        @Attribute(required = false)
        public int recordCounter17 = 212;
        @Attribute(required = false)
        public int temperature17 = 213;
        @Attribute(required = false)
        public int readAlcoEvent = 214;
        @Attribute(required = false)
        public int readAlcoholVolume = 215;
        @Attribute(required = false)
        public int readStartTime17 = 216;
        @Attribute(required = false)
        public int readEndTime17 = 217;
        @Attribute(required = false)
        public int currentTime17 = 218;
        @Attribute(required = false)
        public int deviceStatus = 219;
        @Attribute(required = false)
        public int currentVolume17 = 220;
        @Attribute(required = false)
        public int currentSection = 221;
        @Attribute(required = false)
        public int reserved1617 = 222;
        @Attribute(required = false)
        public int reserved32 = 223;

    }

    public static class G6Cell18Numbers {
        /**
         * Type 18 (additions)
         */
        @Attribute(required = false)
        public int can_an_in0 = 160;
        @Attribute(required = false)
        public int can_an_in1 = 161;
        @Attribute(required = false)
        public int can_an_in2 = 162;
        @Attribute(required = false)
        public int can_an_in3 = 163;
        @Attribute(required = false)
        public int can_an_in4 = 164;
        @Attribute(required = false)
        public int can_an_in5 = 165;
        @Attribute(required = false)
        public int can_an_in6 = 166;
        @Attribute(required = false)
        public int can_an_in7 = 167;
        @Attribute(required = false)
        public int can_di_in = 168;
        @Attribute(required = false)
        public int can_di_out = 169;
        @Attribute(required = false)
        public int can_di0_counter = 170;
        @Attribute(required = false)
        public int can_di1_counter = 171;
        @Attribute(required = false)
        public int can_di2_counter = 172;
        @Attribute(required = false)
        public int can_di3_counter = 173;
        @Attribute(required = false)
        public int can_di4_counter = 174;
        @Attribute(required = false)
        public int can_di5_counter = 175;
        @Attribute(required = false)
        public int can_di6_counter = 176;
        @Attribute(required = false)
        public int can_di7_counter = 177;

    }

    public static class G6Cell19Numbers {
        /**
         * Type 19 (not all)
         */
        @Attribute(required = false)
        public int mcc = 178;
        @Attribute(required = false)
        public int mnc = 179;
        @Attribute(required = false)
        public int lac = 180;
        @Attribute(required = false)
        public int cid = 181;
        @Attribute(required = false)
        public int rssi = 182;
        @Attribute(required = false)
        public int time_adv = 183;

    }

    public static class G6Cell20Numbers {
        /**
         * Type 20
         */
        @Attribute(required = false)
        public int flag_high = 243;
        @Attribute(required = false)
        public int flag_low = 244;

    }

    public static class G6Cell21Numbers {
        /**
         * Type 21
         */
        @Attribute(required = false)
        public int status = 224;
        @Attribute(required = false)
        public int recordCounter = 225;
        @Attribute(required = false)
        public int deviceStatusSection = 226;
        @Attribute(required = false)
        public int currentTime = 227;
        @Attribute(required = false)
        public int currentVolume = 228;
        @Attribute(required = false)
        public int alcoEventSection = 228;
        @Attribute(required = false)
        public int counterStartValue = 229;
        @Attribute(required = false)
        public int counterStopValue = 230;
        @Attribute(required = false)
        public int startTime = 231;
        @Attribute(required = false)
        public int endTime = 232;
        @Attribute(required = false)
        public int finalTemperature = 233;
        @Attribute(required = false)
        public int temperature = 234;
        @Attribute(required = false)
        public int reserved16 = 235;
        @Attribute(required = false)
        public int readAlcoEventSection = 236;
        @Attribute(required = false)
        public int readCounterStartValue = 237;
        @Attribute(required = false)
        public int readCounterStopValue = 238;
        @Attribute(required = false)
        public int readStartTime = 239;
        @Attribute(required = false)
        public int readEndTime = 240;
        @Attribute(required = false)
        public int readFinalTemperature = 241;
        @Attribute(required = false)
        public int productType = 242;

    }

    public static class G6Cell22Numbers {
        /**
         * Type 22
         */
        @Attribute(required = false)
        public int id_max = 184;
        @Attribute(required = false)
        public int id_min = 185;
        @Attribute(required = false)
        public int tm_oldest = 186;
        @Attribute(required = false)
        public int tm_oldest_unack = 197;
        @Attribute(required = false)
        public int cnt_unack = 188;
        @Attribute(required = false)
        public int cnt_unack_losted = 189;
        @Attribute(required = false)
        public int lac1 = 190;
        @Attribute(required = false)
        public int cid1 = 191;
        @Attribute(required = false)
        public int rssi1 = 192;
        @Attribute(required = false)
        public int lac2 = 193;
        @Attribute(required = false)
        public int cid2 = 194;
        @Attribute(required = false)
        public int rssi2 = 195;
        @Attribute(required = false)
        public int lac3 = 196;
        @Attribute(required = false)
        public int cid3 = 197;
        @Attribute(required = false)
        public int rssi3 = 198;
        @Attribute(required = false)
        public int lac4 = 199;
        @Attribute(required = false)
        public int cid4 = 200;
        @Attribute(required = false)
        public int rssi4 = 201;
        @Attribute(required = false)
        public int lac5 = 202;
        @Attribute(required = false)
        public int cid5 = 203;
        @Attribute(required = false)
        public int rssi5 = 204;
        @Attribute(required = false)
        public int lac6 = 205;
        @Attribute(required = false)
        public int cid6 = 206;
        @Attribute(required = false)
        public int rssi6 = 207;

    }

    public class Packages {

        public static final int NPH_SND_RESULT = 0;
        public static final int NPH_SND_HISTORY = 100;
        public static final int NPH_SND_REALTIME = 101;
    }
}
