package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.commands;

import ru.advantum.csptm.jep.hub.granit.protocol.G6BaseCommand;

public class G6NphSetCurtime extends G6BaseCommand {

    public final Unsigned32 cur_time = new Unsigned32();

    @Override
    public String extractString() {
        return this.getClass().getSimpleName() + ": {"
                + "state=" + cur_time.get()
                + "}";
    }

}
