/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.advantum.csptm.jep.hub.granit.protocol;

/**
 * @author Kotin <kotin@advantum.ru>
 *         Формат пакета NPL, протокола нижнего уровня (NPL)
 */
public class G6Npl extends G6Base {

    final public static short SIGNATURE = 0x7E7E;
    /**
     * заголовок пакета NPL	<signature>	2	int16
     * Содержит сведения о сигнатуре  пакета NPL, всегда принимает значение равное 0x7E7E
     */
    private final BitField signature = new BitField(16);
    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Определяет размер данных находящихся в поле <data>.
     * Для незашифрованных и защифрованных пакетов <data_size> всегда равно размеру NPH данных.
     * Если данные передаются в зашифрованном виде, размер поля <data> равен длине данных выровненной
     * по границе 8 байт (требования алгоритма Blowfish).
     * При этом дополнительные байты в расшифрованном пакете не используются.
     * К примеру, если длина NPH пакета равна 18, то <data_size> = 18, а длина поля <data>  равна 24.
     * </editor-fold>
     */
    private final BitField dataSize = new BitField(16);//<data_size>	2	unsigned int16
    private final BitField nfl_flag_encryption = new BitField(1);
    private final BitField npl_flag_crc = new BitField(1);
    private final BitField npl_flag_delay = new BitField(1);
    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * <flags>	2	int16	Определяет опции пакета:
     * NPL_FLAG_CRC- определяет расчет контрольной суммы пакета (CRC). Принимает значения: 0 – нет, 1- да.
     * Если отправитель пакета рассчитывает CRC он должен выставить данный флаг (1),
     * в таком случае получатель имеет возможность проверить валидность пакета.
     * NPL_FLAG_ENCRYPTION – определяет шифрование поля <data>, принимает значение: 0 – нет, 1 – да.
     * Поле <data> шифруется по алгоритму Blowfish**.
     * NPL_FLAG_DELAY – определяет необходимо ли сохранить команду на сервере, если устройство или АРМ недоступны сейчас.
     * 0  - пакет не надо сохранять на сервере в очереди отложенных команд ни при каких обстоятелствах,
     * 1 — пакет необходимо сохранить в очереди отложенных команд только тогда, когда адресат сейчас недоступен.
     * Адресатом пакета может быть только конкретное устройство или АРМ.
     * NPL packet flags (short int)
     * #define NPL_FLAG_ENCRYPTION                     0x0001
     * #define NPL_FLAG_CRC                            0x0002
     * #define NPL_FLAG_DELAY                          0x0004
     * </editor-fold>
     */
    private final BitField flags$ = new BitField(13);
    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Содержит контрольную сумму поля <data>.
     * CRC всегда рассчитывается и проверяется по незашифрованному пакету,
     * что позволяет дополнительно проверять правильность шифрования пакета.
     * То есть при формировании зашифрованного пакета сначала рассчитывается CRC,
     * затем шифруются данные.
     * При разборе пакета сначала расшифровываются данные, затем проверяется CRC.
     * </editor-fold>
     */
    private final BitField crc = new BitField(16);//<crc>	2	unsigned int16
    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Указывает, тип передаваемых данных. Существует три типа данных:
     * NPL_TYPE_ERROR - ошибка протокола NPL 0x01
     * NPL_TYPE_NPH - пакет данных NPH 0x02
     * NPL_TYPE_DEBUG - отладочная информация 0x03
     * </editor-fold>
     */
    private final BitField type = new BitField(8);//	<type>	1	byte
    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Определяет адрес участника соединения:
     * NPL_ADDRESS_SERVER - сервер. 0x00000000
     * NPL_ADDRESS_BROAD_DISPATHERS - все диспетчеры в группе 0x00000001
     * NPL_ADDRESS_BROAD_DEVICES - все мобильные устройства в группе 0x00000002
     * NPL_ADDRESS_BROAD_ALL - все участники клиенты в группе
     * NPL_ADDRESS_RESERVED_MIN... NPL_ADDRESS_RESERVED_MAX - зарезервированные адреса. 0x00000003 - 0x000003FF
     * NPL_ADDRESS_DEVICE_MIN... NPL_ADDRESS_DEVICE_MAX - адреса мобильных устройств. 0x00000400 - 0x7FFFFFFF
     * NFL_ADDRESS_DISPATCHER_MIN... NFL_ADDRESS_DISPATCHER_MAX - адреса диспетчеров. 0x80000000 - 0xFFFFFFFE
     * NPL_ADDRESS_INVALID - не используется.
     * </editor-fold>
     */
    private final BitField peerAddress = new BitField(32);//<peer_address>	4	unsigned int32
    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Идентификатор пакета (ID) рекомендуется делать уникальным
     * хотя бы в рамках одной сессии передачи данных.
     * Например, выбрать некоторое значение ID при установке соединения
     * и для каждого последующего пакета увеличивать его ID на единицу.
     * При достижении 0xFFFFFFFF следующее значение ID будет равно 0x00000000 и т.д.
     * Пакеты протокола NPL однонаправленные, подтверждения не требуют.
     * </editor-fold>
     **/
    private final BitField requestId = new BitField(16);//<request_id>	2	unsigned int16

    public G6Npl(short signatureI
            , int dataSizeI
            , boolean flagEncryptionB
            , boolean flagCrcB
            , boolean flagDelayB
            , int crcI
            , short typeS
            , long peerAddressL
            , int requestIdI) {
        this.signature.set(signatureI);
        this.dataSize.set(dataSizeI);
        this.setNfl_flag_encryption(flagEncryptionB);
        this.setNpl_flag_crc(flagCrcB);
        this.setNpl_flag_delay(flagDelayB);
        this.type.set(typeS);
        this.crc.set(crcI);
        this.peerAddress.set(peerAddressL);
        this.requestId.set(requestIdI);
    }

    public G6Npl() {
    }

    public boolean getNfl_flag_encryption() {
        return this.nfl_flag_encryption.intValue() == 1;
    }

    public void setNfl_flag_encryption(boolean nfl_flag_encryption) {
        this.nfl_flag_encryption.set(nfl_flag_encryption ? 1 : 0);
    }

    public boolean getNpl_flag_crc() {
        return this.npl_flag_crc.intValue() == 1;
    }

    public void setNpl_flag_crc(boolean npl_flag_crc) {
        this.npl_flag_crc.set(npl_flag_crc ? 1 : 0);
    }

    public boolean getNpl_flag_delay() {
        return this.npl_flag_delay.intValue() == 1;
    }

    public void setNpl_flag_delay(boolean npl_flag_delay) {
        this.npl_flag_delay.set(npl_flag_delay ? 1 : 0);
    }

    public short getType() {
        return type.shortValue();
    }

    public int getPeerAddress() {
        return peerAddress.intValue();
    }

    @Override
    public String extractString() {
        return "NplProtocol{" + "signature=" + String.format("%x", signature.intValue())
                + ", dataSize=" + dataSize
                + ", flags:{"
                + "nfl_flag_encryption=>" + getNfl_flag_encryption()
                + ", npl_flag_crc=>" + getNpl_flag_crc()
                + ", npl_flag_delay=>" + getNpl_flag_delay()
                + "}, crc=" + crc
                + ", type=" + type
                + ", peerAddress=" + peerAddress
                + ", requestId=" + requestId + '}';
    }

    @Override
    public int hashCode() {
        return extractString().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (toString().equals(obj.toString())) {
            return true;
        }

        G6Npl G6NplObj = (G6Npl) obj;

        //noinspection SimplifiableIfStatement
        if ((G6NplObj.signature.intValue() == this.signature.intValue()) &&
                (G6NplObj.dataSize.intValue() == this.dataSize.intValue()) &&
                (G6NplObj.getNfl_flag_encryption() == this.getNfl_flag_encryption()) &&
                (G6NplObj.getNpl_flag_delay() == this.getNpl_flag_delay()) &&
                (G6NplObj.getNpl_flag_crc() == this.getNpl_flag_crc()) &&
              /*  (G6NplObj.npl_flag_crc.get() && (G6NplObj.getCRC() == this.getCRC())) &&*/
                G6NplObj.type.intValue() == this.type.intValue() &&
                G6NplObj.peerAddress.intValue() == this.peerAddress.intValue() &&
                G6NplObj.requestId.intValue() == this.requestId.intValue()) {
            return true;
        }

        return G6NplObj.extractString().equals(this.extractString());
    }

    public boolean validateSignature() {
        return signature.intValue() == SIGNATURE;
    }

    public boolean validateCRC() {
        return !getNpl_flag_crc() || crc.intValue() == getCRC();
    }

    public int getDataSize() {
        return dataSize.intValue();
    }

    public short getSignature() {
        return signature.shortValue();
    }

    private int getCRC() {
        return crc.intValue();
    }

    public int getFlags$() {
        return flags$.intValue();
    }

    public void setFlags$(int flags$) {
        this.flags$.set(flags$);
    }

    public enum NplPeerAddress {
        NPL_ADDRESS_SERVER(0x00000000),
        NPL_ADDRESS_BROAD_DISPATCHERS(0x00000001),
        NPL_ADDRESS_BROAD_DEVICES(0x00000002),
        NPL_ADDRESS_RESERVED_MIN(0x00000003),
        NPL_ADDRESS_RESERVED_MAX(0x000003FF),
        NPL_ADDRESS_RESERVED(NPL_ADDRESS_RESERVED_MIN, NPL_ADDRESS_RESERVED_MAX),
        NPL_ADDRESS_DEVICE_MIN(0x00000400),
        NPL_ADDRESS_DEVICE_MAX(0x7FFFFFFF),
        NPL_ADDRESS_DEVICE(NPL_ADDRESS_DEVICE_MIN, NPL_ADDRESS_DEVICE_MAX),
        NFL_ADDRESS_DISPATCHER_MIN(0x80000000),
        NFL_ADDRESS_DISPATCHER_MAX(0xFFFFFFFE),
        NFL_ADDRESS_DISPATCHER(NFL_ADDRESS_DISPATCHER_MIN, NFL_ADDRESS_DISPATCHER_MAX),
        NPL_ADDRESS_INVALID(0xFFFFFFFF);
        private int code;
        private int min;
        private int max;

        NplPeerAddress(int code) {
            this.code = code;
            this.min = Integer.MIN_VALUE;
            this.max = Integer.MAX_VALUE;
        }

        NplPeerAddress(NplPeerAddress min, NplPeerAddress max) {
            this.code = -1;
            this.min = min.code;
            this.max = max.code;
        }

        public static NplPeerAddress lookup(int code) {
            for (NplPeerAddress el : NplPeerAddress.values()) {
                if (el.getCode() == code) {
                    return el;
                }
            }
            return null;
        }

        public static NplPeerAddress lookupByInterval(int value) {
            for (NplPeerAddress el : NplPeerAddress.values()) {
                if (el.getCode() == -1 && (el.getMin() <= value && el.getMax() >= value)) {
                    return el;
                }
            }
            return null;
        }

        public int getCode() {
            return this.code;
        }

        public int getMin() {
            return this.min;
        }

        public int getMax() {
            return this.max;
        }
    }

    /**
     * NPL_TYPE_ERROR - ошибка протокола NPL 0x01
     * NPL_TYPE_NPH - пакет данных NPH 0x02
     * NPL_TYPE_DEBUG - отладочная информация 0x03
     */
    public enum NplType {
        NPL_TYPE_ERROR(0x01),
        NPL_TYPE_NPH(0x02),
        NPL_TYPE_DEBUG(0x03);
        private int code;

        NplType(int code) {
            this.code = code;
        }

        public static NplType lookup(int code) {
            for (NplType el : NplType.values()) {
                if (el.getCode() == code) {
                    return el;
                }
            }
            return null;
        }

        public int getCode() {
            return this.code;
        }
    }
}

