package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.commands;

import ru.advantum.csptm.jep.hub.granit.protocol.G6BaseCommand;

public class G6NphGetBalance extends G6BaseCommand {

    public final ByteSet cmd_req = new ByteSet(20);

    @Override
    public String extractString() {
        return this.getClass().getSimpleName() + ": {"
                + "cmd_req=" + cmd_req.asString()
                + "}";
    }

}
