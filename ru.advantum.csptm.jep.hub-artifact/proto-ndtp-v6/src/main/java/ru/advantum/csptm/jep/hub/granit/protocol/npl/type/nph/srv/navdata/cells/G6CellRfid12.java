package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import javolution.io.Struct;
import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class G6CellRfid12 extends G6Base implements IG6BaseCell {

    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Данные от RFID. Type=12, N=0
     * поле	длина	Тип	описание
     * 5	struct
     * <Key>	5	unsigned int8	Считанный RFID ключ
     * </editor-fold>
     */

    private final Struct.Unsigned8 key[] = array(new Struct.Unsigned8[5]);

    public G6CellRfid12(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    private byte[] getKeyByte() {
        byte[] ret = new byte[key.length];
        for (int i = 0; i < key.length; i++) {
            ret[i] = (byte) key[i].get();
        }
        return ret;
    }

    @Override
    public String extractString() {
        return "G6CellRfid12{" + "key=" + Arrays.toString(key) + '}';
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        Map<Integer, BigDecimal> sensors = new HashMap<>(1);
        sensors.put(ndtpConfig.cell12Numbers.rfidKey,
                new BigDecimal(new BigInteger(getKeyByte()).longValue()));
        return sensors;
    }

    @Override
    public int getCell() {
        return 12;
    }

}
