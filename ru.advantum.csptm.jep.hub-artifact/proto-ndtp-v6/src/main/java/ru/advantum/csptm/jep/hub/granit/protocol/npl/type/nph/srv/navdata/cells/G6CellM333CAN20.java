package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;

@SuppressWarnings("unused")
public class G6CellM333CAN20 extends G6Base implements IG6BaseCell {

    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Данные о базовых станциях GSM (для прибора гранит-навигатор 4.хх)
     * Type=20, N=0 (1…n)
     * поле	длина	Тип	описание
     * 8	struct
     * <flag_high>	4	unsigned int32	Битовая маска флагов
     * <p>
     * Бит  (значенеи 1 — вкл или активно, 0 в противном случае)
     * 11 (0x0400) Статус включение передней гидравлики
     * <flag_low>	4	unsigned int32	Битовая маска флагов
     * <p>
     * Бит  (значенеи 1 — вкл или активно, 0 в противном случае)
     * 1   (0x0001) Состояние щеток
     * 2   (0x0002) Подача воды
     * 3   (0x0004) Состояние пылесоса
     * 4   (0x0008) Выгрузка из бункера
     * 5   (0x0010) Мойка высокого давления
     * 6   (0x0020) Расеивание песка с солью
     * 7   (0x0040) Низкий уровень песка в бункере
     * 10 (0x0200) Низкий уровень жидкости в бункере
     * 11 (0x0400) Статус включения задней гидравлики
     * 12 (0x0800) Статус завода автономного двигателя
     * 13 (0x1000) Джойстик вправо
     * 14 (0x2000) Джойстик влево
     * 15 (0x4000) Джойстик вперед
     * 16 (0x8000) Джойстик назад
     */
    private final Unsigned32 flag_high = new Unsigned32();
    private final Unsigned32 flag_low = new Unsigned32();

    public G6CellM333CAN20(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    @Override
    public String extractString() {
        return "G6CellM333CAN20{" +
                "flag_high=" + flag_high +
                ", flag_low=" + flag_low +
                '}';
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        return Collections.emptyMap();
    }

    @Override
    public int getCell() {
        return 20;
    }

}
