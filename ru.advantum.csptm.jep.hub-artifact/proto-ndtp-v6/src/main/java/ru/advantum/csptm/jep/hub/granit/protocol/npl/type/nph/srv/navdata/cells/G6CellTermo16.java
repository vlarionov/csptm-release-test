/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class G6CellTermo16 extends G6Base implements IG6BaseCell {

    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Данные от датчика температуры TERMO Type=16, N=0
     * поле	длина	Тип	описание
     * 8	struct
     * <Status>	4	unsigned int32	Если не 0 то нет связи с датчиком
     * <temp>	4	signed int32	Температура в градусах Цельсия
     * </editor-fold>
     */

    private final Unsigned32 status = new Unsigned32();
    private final Signed32 temp = new Signed32();

    public G6CellTermo16(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    @Override
    public String extractString() {
        return "G6CellTermo16{" + "status=" + status + ", temp=" + temp + '}';
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        Map<Integer, BigDecimal> sensors = new HashMap<>(2);

        sensors.put(ndtpConfig.cell16Numbers.status16, BigDecimal.valueOf(status.get()));
        sensors.put(ndtpConfig.cell16Numbers.temp16, BigDecimal.valueOf(temp.get()));

        return sensors;
    }

    @Override
    public int getCell() {
        return 16;
    }

}
