package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class G6CellPlo13 extends G6Base implements IG6BaseCell {

    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Данные от датчика уровня продукта в отсеке . Type=13, N=0 (1…n)
     * поле	длина	Тип	Описание
     * 13	struct
     * <density>	4	float	Плотность продукта
     * <temperature>	4	float	Температура продукта
     * <level>	4	float	Уровень продукта
     * <level_unit>	1	unsigned int8	Единица измерения
     * 1 - %
     * 2 — мм
     * 3 м3
     * </editor-fold>
     */

    private final Float32 density = new Float32();
    private final Float32 temperature = new Float32();
    private final Float32 level = new Float32();
    private final Unsigned8 levelUnit = new Unsigned8();

    public G6CellPlo13(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    @Override
    public String extractString() {
        return "G6CellPlo13{" + "density=" + density +
                ", temperature=" + temperature +
                ", level=" + level +
                ", levelUnit=" + levelUnit + '}';
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        Map<Integer, BigDecimal> sensors = new HashMap<>(4);

        sensors.put(ndtpConfig.cell13Numbers.density13, BigDecimal.valueOf(density.get()));
        sensors.put(ndtpConfig.cell13Numbers.temperature13, BigDecimal.valueOf(temperature.get()));
        sensors.put(ndtpConfig.cell13Numbers.level, BigDecimal.valueOf(level.get()));
        sensors.put(ndtpConfig.cell13Numbers.levelUnit, BigDecimal.valueOf(levelUnit.get()));

        return sensors;
    }

    @Override
    public int getCell() {
        return 13;
    }

}
