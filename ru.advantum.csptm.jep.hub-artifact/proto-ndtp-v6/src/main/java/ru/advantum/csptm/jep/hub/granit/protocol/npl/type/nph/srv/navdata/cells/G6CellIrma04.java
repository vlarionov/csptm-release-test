/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

@SuppressWarnings("unused")
public class G6CellIrma04 extends G6Base implements IG6BaseCell {

    private final BitField odometer = new BitField(32);// unsigned int32	Одометр. Циклический счётчик пробега в метрах.
    private final BitField zone = new BitField(16);//	unsigned int16	Номер зоны для позонной оплаты
    private final BitField irma_door_in1 = new BitField(8);//Количество вошедших пассажиров через первую дверь.
    private final BitField irma_door_in2 = new BitField(8);//Количество вошедших пассажиров через вторую дверь.
    private final BitField irma_door_in3 = new BitField(8);//Количество вошедших пассажиров через третью дверь.
    private final BitField irma_door_in4 = new BitField(8);//Количество вошедших пассажиров через четвертую дверь.
    private final BitField irma_door_out1 = new BitField(8);//Количество вышедших пассажиров через первую дверь.
    private final BitField irma_door_out2 = new BitField(8);//Количество вышедших пассажиров через вторую дверь.
    private final BitField irma_door_out3 = new BitField(8);//Количество вышедших пассажиров через третью дверь.
    private final BitField irma_door_out4 = new BitField(8);//Количество вышедших пассажиров через четвертую дверь.

    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * <irma_present_door>	1	unsigned int8	Поле – битовая маска
     * присутствия двери(bit3..bit0)
     * 1 -  дверь присутствует
     * 0 – дверь не найдена
     * битовая маска закрытия двери (bit7..bit4). То есть если бит установлен в 1, то дверь закрывалась на остановке.
     * 1 -  дверь закрывалась
     * 0 – дверь не закрывалась
     * </editor-fold>
     */

    private final BitField irma_present_door1 = new BitField(1);
    private final BitField irma_present_door2 = new BitField(1);
    private final BitField irma_present_door3 = new BitField(1);
    private final BitField irma_present_door4 = new BitField(1);
    private final BitField irma_closed_door1 = new BitField(1);
    private final BitField irma_closed_door2 = new BitField(1);
    private final BitField irma_closed_door3 = new BitField(1);
    private final BitField irma_closed_door4 = new BitField(1);

    public G6CellIrma04(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    public G6CellIrma04(GranitHubConfig.Ndtp ndtpConfig, int cellNumber) {
        this(ndtpConfig);
        this.cellNumber = cellNumber;
    }

    public boolean getIrma_present_door1() {
        return this.irma_present_door1.intValue() == 1;
    }

    public void setIrma_present_door1(boolean irma_present_door1) {
        this.irma_present_door1.set(irma_present_door1 ? 1 : 0);
    }

    public boolean getIrma_present_door2() {
        return this.irma_present_door2.intValue() == 1;
    }

    public void setIrma_present_door2(boolean irma_present_door2) {
        this.irma_present_door2.set(irma_present_door2 ? 1 : 0);
    }

    public boolean getIrma_present_door3() {
        return this.irma_present_door3.intValue() == 1;
    }

    public void setIrma_present_door3(boolean irma_present_door3) {
        this.irma_present_door3.set(irma_present_door3 ? 1 : 0);
    }

    public boolean getIrma_present_door4() {
        return this.irma_present_door4.intValue() == 1;
    }

    public void setIrma_present_door4(boolean irma_present_door4) {
        this.irma_present_door4.set(irma_present_door4 ? 1 : 0);
    }

    public boolean getIrma_closed_door1() {
        return this.irma_closed_door1.intValue() == 1;
    }

    public void setIrma_closed_door1(boolean irma_closed_door1) {
        this.irma_closed_door1.set(irma_closed_door1 ? 1 : 0);
    }

    public boolean getIrma_closed_door2() {
        return this.irma_closed_door2.intValue() == 1;
    }

    public void setIrma_closed_door2(boolean irma_closed_door2) {
        this.irma_closed_door2.set(irma_closed_door2 ? 1 : 0);
    }

    public boolean getIrma_closed_door3() {
        return this.irma_closed_door3.intValue() == 1;
    }

    public void setIrma_closed_door3(boolean irma_closed_door3) {
        this.irma_closed_door3.set(irma_closed_door3 ? 1 : 0);
    }

    public boolean getIrma_closed_door4() {
        return this.irma_closed_door4.intValue() == 1;
    }

    public void setIrma_closed_door4(boolean irma_closed_door4) {
        this.irma_closed_door4.set(irma_closed_door4 ? 1 : 0);
    }

    @Override
    public String extractString() {
        return "G6CellIrmaData{"
                + "odometer=" + odometer
                + ", zone=" + zone
                + ", irma_door_in1=" + irma_door_in1
                + ", irma_door_in2=" + irma_door_in2
                + ", irma_door_in3=" + irma_door_in3
                + ", irma_door_in4=" + irma_door_in4
                + ", irma_door_out1=" + irma_door_out1
                + ", irma_door_out2=" + irma_door_out2
                + ", irma_door_out3=" + irma_door_out3
                + ", irma_door_out4=" + irma_door_out4
                + ", irma_present_door1=" + irma_present_door1
                + ", irma_present_door2=" + irma_present_door2
                + ", irma_present_door3=" + irma_present_door3
                + ", irma_present_door4=" + irma_present_door4
                + ", irma_closed_door1=" + irma_closed_door1
                + ", irma_closed_door2=" + irma_closed_door2
                + ", irma_closed_door3=" + irma_closed_door3
                + ", irma_closed_door4=" + irma_closed_door4
                + '}';
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        Map<Integer, BigDecimal> sensors = new ConcurrentSkipListMap<>();
        int offset = getCell() * 100 + getCellNumber() * 18;
        sensors.put(ndtpConfig.cell04Numbers.odometerIrma + offset, BigDecimal.valueOf(odometer.longValue()));
        sensors.put(ndtpConfig.cell04Numbers.zoneIrma + offset, BigDecimal.valueOf(zone.shortValue()));
        sensors.put(ndtpConfig.cell04Numbers.irma_door_in1 + offset, BigDecimal.valueOf(irma_door_in1.intValue()));
        sensors.put(ndtpConfig.cell04Numbers.irma_door_in2 + offset, BigDecimal.valueOf(irma_door_in2.intValue()));
        sensors.put(ndtpConfig.cell04Numbers.irma_door_in3 + offset, BigDecimal.valueOf(irma_door_in3.intValue()));
        sensors.put(ndtpConfig.cell04Numbers.irma_door_in4 + offset, BigDecimal.valueOf(irma_door_in4.intValue()));
        sensors.put(ndtpConfig.cell04Numbers.irma_door_out1 + offset, BigDecimal.valueOf(irma_door_out1.intValue()));
        sensors.put(ndtpConfig.cell04Numbers.irma_door_out2 + offset, BigDecimal.valueOf(irma_door_out2.intValue()));
        sensors.put(ndtpConfig.cell04Numbers.irma_door_out3 + offset, BigDecimal.valueOf(irma_door_out3.intValue()));
        sensors.put(ndtpConfig.cell04Numbers.irma_door_out4 + offset, BigDecimal.valueOf(irma_door_out4.intValue()));

        sensors.put(ndtpConfig.cell04Numbers.irma_present_door1 + offset, BigDecimal.valueOf(getIrma_present_door1() ? 1 : 0));
        sensors.put(ndtpConfig.cell04Numbers.irma_present_door2 + offset, BigDecimal.valueOf(getIrma_present_door2() ? 1 : 0));
        sensors.put(ndtpConfig.cell04Numbers.irma_present_door3 + offset, BigDecimal.valueOf(getIrma_present_door3() ? 1 : 0));
        sensors.put(ndtpConfig.cell04Numbers.irma_present_door4 + offset, BigDecimal.valueOf(getIrma_present_door4() ? 1 : 0));

        sensors.put(ndtpConfig.cell04Numbers.irma_closed_door1 + offset, BigDecimal.valueOf(getIrma_closed_door1() ? 1 : 0));
        sensors.put(ndtpConfig.cell04Numbers.irma_closed_door2 + offset, BigDecimal.valueOf(getIrma_closed_door2() ? 1 : 0));
        sensors.put(ndtpConfig.cell04Numbers.irma_closed_door3 + offset, BigDecimal.valueOf(getIrma_closed_door3() ? 1 : 0));
        sensors.put(ndtpConfig.cell04Numbers.irma_closed_door4 + offset, BigDecimal.valueOf(getIrma_closed_door4() ? 1 : 0));

        return sensors;
    }

    @Override
    public int getCell() {
        return 4;
    }

}
