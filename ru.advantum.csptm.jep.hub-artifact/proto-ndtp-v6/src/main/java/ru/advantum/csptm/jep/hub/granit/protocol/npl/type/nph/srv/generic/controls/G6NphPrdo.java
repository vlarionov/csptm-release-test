package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.generic.controls;

import javolution.io.Struct;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

/**
 * @author mitsay
 * @since 26.11.15.
 */
public class G6NphPrdo extends G6Base {

    public final Struct.Unsigned8 output_id = new Struct.Unsigned8();
    public final Struct.Unsigned8 mode = new Struct.Unsigned8();

    @Override
    public String extractString() {
        return "G6NphPrdo: {" + "output_id=" + output_id.get() + ",mode=" + mode.get() + "}";
    }

    public int getOutput_id() {
        return output_id.get();
    }

    public int getMode() {
        return mode.get();
    }
}
