package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.client.list;

public class G6NphSrvClientListConstant {

    public class Packages {

        public final static int NPH_SCL_RESULT = 0;
        public final static int NPH_SCL_CLIENT_LIST_REQUEST = 100;
        public final static int NPH_SCL_CLIENT_LIST = 101;
        public final static int NPH_SCL_CLIENT_STATUS_REQUEST = 102;
    }
}
