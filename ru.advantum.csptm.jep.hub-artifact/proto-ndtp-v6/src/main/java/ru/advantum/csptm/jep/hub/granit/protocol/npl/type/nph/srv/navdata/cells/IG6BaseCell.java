package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import java.math.BigDecimal;
import java.util.Map;

/**
 * @author Kotin <kotin@advantum.ru>
 */
public interface IG6BaseCell {

    Map<Integer, BigDecimal> getSensorData();

    int getCell();
}
