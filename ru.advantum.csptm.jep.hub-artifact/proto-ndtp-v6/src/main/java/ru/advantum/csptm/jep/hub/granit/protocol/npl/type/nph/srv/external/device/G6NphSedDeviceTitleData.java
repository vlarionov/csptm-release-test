package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.external.device;

import javolution.io.Struct;
import ru.advantum.csptm.jep.hub.granit.protocol.G6BaseCommand;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

public class G6NphSedDeviceTitleData extends G6BaseCommand {

    private static final int SINGLE_PACKAGE = 0x8000;

    public BitField message_id = new BitField(16);
    public BitField num_packet = new BitField(16);
    public BitField address_from = new BitField(16);
    public BitField address_to = new BitField(16);
    public BitField type_data = new BitField(32);

    public Struct.ByteSet data_packet;

    private Script script;

    public G6NphSedDeviceTitleData(int av) {
        if (av < 12) {
            throw new IllegalArgumentException("G6NphSedDeviceTitleData called with " + av);
        }
        data_packet = new Struct.ByteSet(av - 12);
    }

    public G6NphSedDeviceTitleData() {
        this(12);
    }

    public G6NphSedDeviceTitleData(String message, String srcEnc, String dstEnc) {
        byte[] buf = new String(message.getBytes(Charset.forName(srcEnc))).getBytes(Charset.forName(dstEnc));
        data_packet = new Struct.ByteSet(buf.length);
        this.data_packet.set(buf);
    }

    public G6NphSedDeviceTitleData(String message) {
        byte[] buf = message.getBytes();
        data_packet = new Struct.ByteSet(buf.length);
        this.data_packet.set(buf);
    }

    @Override
    public int read(InputStream in) throws IOException {
        int fulllength = super.read(in);
        this.script = new Script(this.data_packet.get());
        return fulllength;
    }

    public Script getScript() {
        return script;
    }

    public boolean isStatusMessage() {
        String status = script.getScriptTags().get("STATUS");
        return status != null && status.length() > 0;
    }

    public String getDataAsCp1251String() {
        return (data_packet != null ? new String(data_packet.get(), Charset.forName("Cp1251")) : "");
    }

    public void setData_packet(byte[] bytes) {
        this.data_packet.set(bytes);
        //System.arraycopy(bytes, 0, this.data_packet.get(), 0, bytes.length);
    }

    @Override
    public String extractString() {
        return this.getClass().getSimpleName() + ": {"
                + "message_id=" + message_id.intValue()
                + ",num_packet=" + num_packet.intValue()
                + ",address_from=" + address_from.intValue()
                + ",address_to=" + address_to.intValue()
                + ",type_data=" + type_data.intValue()
                + ",data:" + getDataAsCp1251String()
                + ",script:" + script
                + "}";
    }


}
