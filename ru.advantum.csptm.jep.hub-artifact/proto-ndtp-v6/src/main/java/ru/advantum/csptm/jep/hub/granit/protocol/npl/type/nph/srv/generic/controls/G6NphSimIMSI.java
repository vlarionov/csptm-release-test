package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.generic.controls;

import javolution.io.Struct;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

public class G6NphSimIMSI extends G6Base {

    public final Struct.ByteSet imsi = new Struct.ByteSet(20);

    public String asString(int i) {
        int l = imsi.asString().length();
        if (l < i) {
            i = l;
        }

        return new String(imsi.asString().substring(0, i).getBytes());
    }

    public String asString() {
        return asString(15);
    }

    public G6NphSimIMSI() {
        super(null);
    }

    @Override
    public String extractString() {
        return this.getClass().getSimpleName() + ": {"
                + "imsi=" + this.asString()
                + "}";
    }

}
