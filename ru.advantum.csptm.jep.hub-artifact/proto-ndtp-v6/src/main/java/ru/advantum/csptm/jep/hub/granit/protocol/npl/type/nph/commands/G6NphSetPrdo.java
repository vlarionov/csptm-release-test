package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.commands;

import ru.advantum.csptm.jep.hub.granit.protocol.G6BaseCommand;

public class G6NphSetPrdo extends G6BaseCommand {
    public Unsigned8 output_id = new Unsigned8();
    public Unsigned8 mode = new Unsigned8();
}
