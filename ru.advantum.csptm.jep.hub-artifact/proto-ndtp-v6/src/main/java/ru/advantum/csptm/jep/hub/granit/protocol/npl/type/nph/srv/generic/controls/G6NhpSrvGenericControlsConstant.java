package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.generic.controls;

import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.G6NphHeader;

/**
 * Created by zlobina on 28.09.15.
 * NPH_SRV_GENERIC_CONTROLS packets
 */
public class G6NhpSrvGenericControlsConstant {

    public class Packages {

        public final static int NPH_SGC_RESULT = G6NphHeader.NPH_RESULT;
        public final static int NPH_SGC_CONN_REQUEST = 100;
        public final static int NPH_SGC_CONN_AUTH_STRING = 101;
        public final static int NPH_SED_DEVICE_RESULT = 102;
        public final static int NPH_SGC_SERVICE_REQUEST = 110;
        public final static int NPH_SGC_SERVICES_REQUEST = 111;
        public final static int NPH_SGC_SERVICES = 112;
        public final static int NPH_SGC_PEER_DESC_REQUEST = 120;
        public final static int NPH_SGC_PEER_DESC = 121;
        // DIGITAL OUTPUT
        public final static int NPH_SET_PRDO = 150;
        public final static int NPH_GET_PRDO = 151;
        public final static int NPH_PRDO = 152;

        public final static int NPH_SET_PRIA = 170;
        public final static int NPH_GET_PRIA = 171;
        public final static int NPH_PRIA = 172;

        public final static int NPH_SET_PRNAV = 175;
        public final static int NPH_GET_PRNAV = 176;
        public final static int NPH_PRNAV = 177;

        public final static int NPH_SET_LOADFIRM = 180;

        // Get information about navigator
        public final static int NPH_GET_INFO = 185;
        public final static int NPH_INFO = 186;

        // Get route on SIM card
        public final static int NPH_GET_BALANCE = 190;
        public final static int NPH_BALANCE = 191;

        // Command for set current time
        public final static int NPH_SET_CURTIME = 195;

        // Command for Aotoinformer
        public final static int NPH_SET_ROUTE_AUTOINFORMER = 200;
        public final static int NPH_GET_ROUTE_AUTOINFORMER = 201;
        public final static int NPH_ROUTE_AUTOINFORMER = 202;
        public final static int NPH_RESET_INT_STATE = 205;

        public final static int NPH_GET_SIM_IMSI = 210;
        public final static int NPH_SIM_IMSI = 211;
    }
}
