package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class G6CellServerStatistics22 extends G6Base implements IG6BaseCell {

    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Статистические данные о качестве связи с сервером
     * Type=22, N=0 (1…n)
     * поле	длина	Тип	описание
     * 24	struct
     * <id_max>	4	unsigned int32	Максимальное значение идентификатора сквозной нумерации навигационных данных в памяти навигатора.
     * <id_min>	4	unsigned int32	Минимальное значение идентификатора сквозной нумерации навигационных данных в памяти навигатора.
     * <tm_oldest>	4	unsigned int32	Время самой ранней отметки в файловом хранилище.
     * <tm_oldest_unack>	4	unsigned int32	Время самой ранней неподтвержденной отметки в файловом хранилище.
     * <cnt_unack>	4	unsigned int32	Счетчик неподтвержденных пакетов.
     * <cnt_unack_losted>	4	unsigned int32	Счетчик неподтвержденных утерянных пакетов.
     * </editor-fold>
     */
    private final Unsigned32 id_max = new Unsigned32();
    private final Unsigned32 id_min = new Unsigned32();
    private final Unsigned32 tm_oldest = new Unsigned32();
    private final Unsigned32 tm_oldest_unack = new Unsigned32();
    private final Unsigned32 cnt_unack = new Unsigned32();
    private final Unsigned32 cnt_unack_losted = new Unsigned32();

    public G6CellServerStatistics22(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    @Override
    public String extractString() {
        return "G6CellServerStatistics22{" +
                "id_max=" + id_max +
                ", id_min=" + id_min +
                ", tm_oldest=" + tm_oldest +
                ", tm_oldest_unack=" + tm_oldest_unack +
                ", cnt_unack=" + cnt_unack +
                ", cnt_unack_losted=" + cnt_unack_losted +
                '}';
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        Map<Integer, BigDecimal> sensors = new HashMap<>(6);

        sensors.put(ndtpConfig.cell22Numbers.id_max, BigDecimal.valueOf(id_max.get()));
        sensors.put(ndtpConfig.cell22Numbers.id_min, BigDecimal.valueOf(id_min.get()));
        sensors.put(ndtpConfig.cell22Numbers.tm_oldest, BigDecimal.valueOf(tm_oldest.get()));
        sensors.put(ndtpConfig.cell22Numbers.tm_oldest_unack, BigDecimal.valueOf(tm_oldest_unack.get()));
        sensors.put(ndtpConfig.cell22Numbers.cnt_unack, BigDecimal.valueOf(cnt_unack.get()));
        sensors.put(ndtpConfig.cell22Numbers.cnt_unack_losted, BigDecimal.valueOf(cnt_unack_losted.get()));

        return sensors;
    }

    @Override
    public int getCell() {
        return 22;
    }

}
