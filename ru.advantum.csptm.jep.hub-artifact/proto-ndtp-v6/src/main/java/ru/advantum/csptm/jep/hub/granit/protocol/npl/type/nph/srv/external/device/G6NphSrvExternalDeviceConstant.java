package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.external.device;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class G6NphSrvExternalDeviceConstant {
    public final static int CONSTANT_NOT_FOUND = -1;
    public final static String CONSTANT_VALUE_NOT_FOUND = "";

    /**
     * NPH_SRV_EXTERNAL_DEVICE packets
     */
    public static class Packages {

        public final static int NPH_SED_DEVICE_TITLE_DATA = 100;
        public final static int NPH_SED_DEVICE_DATA = 101;
        public final static int NPH_SED_DEVICE_RESULT = 102;
    }

    /**
     * NPH_SRV_EXTERNAL_DEVICE device address
     */
    public static class DeviceAddress {

        public final static int NPH_SED_ADDR_DISPLAY_DEVICE = 0x0;
        public final static int NPH_SED_ADDR_VOICE_DRIVER_DEVICE = 0x10;
        public final static int NAH_SAD_ADDR_VOICE_DRIVER_PASSENGERS = 0x20;
        public final static int NPH_SED_ADDR_INTERNAL_DISPLAY = 0x30;
        public final static int NPH_SED_ADDR_EXTERNAL_DISPLAY = 0x40;
        public final static int NPH_SED_ADDR_IRMA_DEVICE = 0x50;
        public final static int NPH_SED_ADDR_BPKRD_DEVICE = 0x60;
        public final static int NPH_SED_ADDR_TEMPERATURE_ON_BOARD = 0x70;
        public final static int NPH_SED_ADDR_JPG_CAMERA = 0x80;
        public final static int NPH_SED_ADDR_INT_PARAMETERS = 0x90;

        private final static Map<String, Integer> devices = new HashMap<>();

        static {
            devices.put("NPH_SED_ADDR_DISPLAY_DEVICE", DeviceAddress.NPH_SED_ADDR_DISPLAY_DEVICE);
            devices.put("NPH_SED_ADDR_VOICE_DRIVER_DEVICE", DeviceAddress.NPH_SED_ADDR_VOICE_DRIVER_DEVICE);
            devices.put("NAH_SAD_ADDR_VOICE_DRIVER_PASSENGERS", DeviceAddress.NAH_SAD_ADDR_VOICE_DRIVER_PASSENGERS);
            devices.put("NPH_SED_ADDR_INTERNAL_DISPLAY", DeviceAddress.NPH_SED_ADDR_INTERNAL_DISPLAY);
            devices.put("NPH_SED_ADDR_IRMA_DEVICE", DeviceAddress.NPH_SED_ADDR_IRMA_DEVICE);
            devices.put("NPH_SED_ADDR_EXTERNAL_DISPLAY", DeviceAddress.NPH_SED_ADDR_EXTERNAL_DISPLAY);
            devices.put("NPH_SED_ADDR_BPKRD_DEVICE", DeviceAddress.NPH_SED_ADDR_BPKRD_DEVICE);
            devices.put("NPH_SED_ADDR_TEMPERATURE_ON_BOARD", DeviceAddress.NPH_SED_ADDR_TEMPERATURE_ON_BOARD);
            devices.put("NPH_SED_ADDR_JPG_CAMERA", DeviceAddress.NPH_SED_ADDR_JPG_CAMERA);
            devices.put("NPH_SED_ADDR_INT_PARAMETERS", DeviceAddress.NPH_SED_ADDR_INT_PARAMETERS);
        }


        public static int lookup(String deviceAddressString) {
            for (Map.Entry<String, Integer> entry : devices.entrySet()) {
                if (entry.getKey().equals(deviceAddressString)) {
                    return entry.getValue();
                }
            }
            return CONSTANT_NOT_FOUND;
        }

        public static String lookup(int deviceAddressInt) {
            for (Map.Entry<String, Integer> entry : devices.entrySet()) {
                if (entry.getValue().equals(deviceAddressInt)) {
                    return entry.getKey();
                }
            }
            return CONSTANT_VALUE_NOT_FOUND;
        }
    }


    public static class TypeDataFieldValues {

        /*NPH_SRV_EXTERNAL_DEVICE type packet*/
        public final static int NPH_SED_TYPE_MSG_SCRIPT_DISPLAY = 0x1;
        public final static int NPH_SED_TYPE_JPG_PHOTO = 0x2;
        public final static int NPH_SED_TYPE_VECTOR_MAP = 0x3;
        public final static int NPH_SED_TYPE_VOICE_LPC10 = 0x4;
        public final static int NPH_SED_TYPE_VOICE_GSM630 = 0x5;

        /* !!Не найдены коды в спецификации в. 6.2*. */
        public final static int NPH_SED_TYPE_VOICE_SPEEC = 0x6;
        public final static int NPH_SED_TYPE_PAR_REQUEST = 0x7;// Пакет запроса параметров
        public final static int NPH_SED_TYPE_PAR_DATA = 0x8;// Пакет параметров Если приходит для устройства то это
        // значит установить, если в АРМ, то ответ на запрос
        public final static int NPH_SED_TYPE_PAR_ID = 0x9;// Пакет ID параметров. Может приходить только к навигатору.
        /* !!Не найдены коды в спецификации в. 6.2*. */
        // На данный пакет навигатор ответит пааетом данных парамтеров
        private final static Map<String, Integer> tyeDataMap = new HashMap<>();

        static {
            tyeDataMap.put("NPH_SED_TYPE_MSG_SCRIPT_DISPLAY", TypeDataFieldValues.NPH_SED_TYPE_MSG_SCRIPT_DISPLAY);
            tyeDataMap.put("NPH_SED_TYPE_JPG_PHOTO", TypeDataFieldValues.NPH_SED_TYPE_JPG_PHOTO);
            tyeDataMap.put("NPH_SED_TYPE_VECTOR_MAP", TypeDataFieldValues.NPH_SED_TYPE_VECTOR_MAP);
            tyeDataMap.put("NPH_SED_TYPE_VOICE_LPC10", TypeDataFieldValues.NPH_SED_TYPE_VOICE_LPC10);
            tyeDataMap.put("NPH_SED_TYPE_VOICE_GSM630", TypeDataFieldValues.NPH_SED_TYPE_VOICE_GSM630);
            tyeDataMap.put("NPH_SED_TYPE_VOICE_SPEEC", TypeDataFieldValues.NPH_SED_TYPE_VOICE_SPEEC);
            tyeDataMap.put("NPH_SED_TYPE_PAR_REQUEST", TypeDataFieldValues.NPH_SED_TYPE_PAR_REQUEST);
            tyeDataMap.put("NPH_SED_TYPE_PAR_DATA", TypeDataFieldValues.NPH_SED_TYPE_PAR_DATA);
            tyeDataMap.put("NPH_SED_TYPE_PAR_ID", TypeDataFieldValues.NPH_SED_TYPE_PAR_ID);
        }


        public static int lookup(String typeDataString) {
            for (Map.Entry<String, Integer> entry : tyeDataMap.entrySet()) {
                if (entry.getKey().equals(typeDataString)) {
                    return entry.getValue();
                }
            }
            return CONSTANT_NOT_FOUND;
        }

        public static String lookup(int typeDataInt) {
            for (Map.Entry<String, Integer> entry : tyeDataMap.entrySet()) {
                if (entry.getValue().equals(typeDataInt)) {
                    return entry.getKey();
                }
            }
            return CONSTANT_VALUE_NOT_FOUND;
        }
    }
}
