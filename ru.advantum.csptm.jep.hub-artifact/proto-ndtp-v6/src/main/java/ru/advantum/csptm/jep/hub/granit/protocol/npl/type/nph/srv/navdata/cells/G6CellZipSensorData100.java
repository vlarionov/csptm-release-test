package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;

@SuppressWarnings("unused")
public class G6CellZipSensorData100 extends G6Base implements IG6BaseCell {

    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Запакованные данные от датчика
     * Type=100, N=0 (1…n)
     * поле	длина	Тип	описание
     * N	struct
     * <id_sensor>	1	unsigned int8	Код типа упакованных данных датчика.
     * Структуры датчика и соответсвующие коды:
     * <p>Данные от датчика MARWIS (id_sensor=1)
     * <Road_Surface_Temperature>	4	unsigned int32
     * <Dewpoint_Temperature>	4	unsigned int32
     * <Relative_Humidity_over_RST>	4	unsigned int32
     * <Water_Film_Height>	4	unsigned int32
     * <Road_Condition>	4	unsigned int32
     * <Ice_Percentage>	4	unsigned int32
     * <Friction>	4	unsigned int32
     * <RxTime>	4	unsigned int32
     * <longitude>	4	unsigned int32
     * <latitude>	4	unsigned int32
     * <p>
     * </p>
     * <flag>	1	unsigned int8	Bit0 - флаг упаковки данных <data>
     * 1 - упакованные  данные
     * 0 — данные не упакованы
     * <len_data>	2	unsigned int16	Длина данных поля <data>.
     * <data>	<len_data>	int8[]	Данные датчика. Распакованные данные должны быть кратны структуре данных датчика.
     * </editor-fold>
     */
    private final Unsigned8 id_sensor = new Unsigned8();
    private final Unsigned8 flag = new Unsigned8();
    private final Unsigned16 len_data = new Unsigned16();
    private final ByteSet data = new ByteSet(40); //раз никаких кроме марвис нету, то пока 40

    public G6CellZipSensorData100(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    @Override
    public String extractString() {
        return "G6CellTrackerStatistics23{" +
                "id_sensor=" + id_sensor +
                ", flag=" + flag +
                ", len_data=" + len_data +
                '}';
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        return Collections.emptyMap();
    }

    @Override
    public int getCell() {
        return 100;
    }

}
