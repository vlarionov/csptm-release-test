package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.commands;

import ru.advantum.csptm.jep.hub.granit.protocol.G6BaseCommand;
import ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.generic.controls.G6NphConnRequest;

public class G6NphSetLoadfirm extends G6BaseCommand {

    public final Unsigned32 ip_address = new Unsigned32();
    public final Unsigned16 port = new Unsigned16();
    public final ByteSet URL = new ByteSet(70);
    public final ByteSet dir_file = new ByteSet(100);
    public final ByteSet file_name = new ByteSet(30);
    public final ByteSet User = new ByteSet(20);
    public final ByteSet Password = new ByteSet(20);

    @Override
    public String extractString() {
        return this.getClass().getSimpleName() + ": {"
                + "ip_address=" + new String(G6NphConnRequest.intToInetAddress((int) ip_address.get()).getAddress())
                + ", ip_address (int)=" + ip_address.get()
                + ", port=" + port.get()
                + ", URL=" + URL.asString()
                + ", dir_file=" + dir_file.asString()
                + ", file_name=" + file_name.asString()
                + ", User=" + User.asString()
                + ", Password=" + Password.asString()
                + "}";
    }

}
