/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class G6CellGSMstations19 extends G6Base implements IG6BaseCell {

    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Данные о базовых станциях GSM (для прибора гранит-навигатор 4.хх)
     * Type=19, N=0 (1…n)
     * поле	длина	Тип	описание
     * 40	struct
     * <MCC>	2	unsigned int16	Код страны
     * <MNC>	1	unsigned int8	Код оператора
     * Данные от рабочей вышки GSM
     * <LAC>	2	unsigned int16	LAC
     * <CID>	2	unsigned int16	CID
     * <RSSI>	1	unsigned int8	RSSI
     * <time_adv>	2	unsigned int16	timing advance
     * Данные от других видимых вышек GSM
     * <LAC_1>	2	unsigned int16	LAC
     * <CID_1>	2	unsigned int16	CID
     * <RSSI_1>	1	unsigned int8	RSSI
     * <LAC_2>	2	unsigned int16	LAC
     * <CID_2>	2	unsigned int16	CID
     * <RSSI_2>	1	unsigned int8	RSSI
     * <LAC_3>	2	unsigned int16	LAC
     * <CID_3>	2	unsigned int16	CID
     * <RSSI_3>	1	unsigned int8	RSSI
     * <LAC_4>	2	unsigned int16	LAC
     * <CID_4>	2	unsigned int16	CID
     * <RSSI_4>	1	unsigned int8	RSSI
     * <LAC_5>	2	unsigned int16	LAC
     * <CID_5>	2	unsigned int16	CID
     * <RSSI_5>	1	unsigned int8	RSSI
     * <LAC_6>	2	unsigned int16	LAC
     * <CID_6>	2	unsigned int16	CID
     * <RSSI_6>	1	unsigned int8	RSSI
     * </editor-fold>
     */

    private final Unsigned16 mcc = new Unsigned16();
    private final Unsigned8 mnc = new Unsigned8();

    private final Unsigned16 lac = new Unsigned16();
    private final Unsigned16 cid = new Unsigned16();
    private final Unsigned8 rssi = new Unsigned8();
    private final Unsigned16 time_adv = new Unsigned16();

    private final Unsigned16 lac1 = new Unsigned16();
    private final Unsigned16 cid1 = new Unsigned16();
    private final Unsigned8 rssi1 = new Unsigned8();

    private final Unsigned16 lac2 = new Unsigned16();
    private final Unsigned16 cid2 = new Unsigned16();
    private final Unsigned8 rssi2 = new Unsigned8();

    private final Unsigned16 lac3 = new Unsigned16();
    private final Unsigned16 cid3 = new Unsigned16();
    private final Unsigned8 rssi3 = new Unsigned8();

    private final Unsigned16 lac4 = new Unsigned16();
    private final Unsigned16 cid4 = new Unsigned16();
    private final Unsigned8 rssi4 = new Unsigned8();

    private final Unsigned16 lac5 = new Unsigned16();
    private final Unsigned16 cid5 = new Unsigned16();
    private final Unsigned8 rssi5 = new Unsigned8();

    private final Unsigned16 lac6 = new Unsigned16();
    private final Unsigned16 cid6 = new Unsigned16();
    private final Unsigned8 rssi6 = new Unsigned8();

    public G6CellGSMstations19(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    @Override
    public String extractString() {
        return "G6CellGSMstations19{" +
                "mcc=" + mcc +
                ", mnc=" + mnc +
                ", lac=" + lac +
                ", cid=" + cid +
                ", rssi=" + rssi +
                ", time_adv=" + time_adv +
                ", lac_1=" + lac1 +
                ", cid_1=" + cid1 +
                ", rssi_1=" + rssi1 +
                ", lac_2=" + lac2 +
                ", cid_2=" + cid2 +
                ", rssi_2=" + rssi2 +
                ", lac_3=" + lac3 +
                ", cid_3=" + cid3 +
                ", rssi_3=" + rssi3 +
                ", lac_4=" + lac4 +
                ", cid_4=" + cid4 +
                ", rssi_4=" + rssi4 +
                ", lac_5=" + lac5 +
                ", cid_5=" + cid5 +
                ", rssi_5=" + rssi5 +
                ", lac_6=" + lac6 +
                ", cid_6=" + cid6 +
                ", rssi_6=" + rssi6 +
                '}';
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        Map<Integer, BigDecimal> sensors = new HashMap<>(6);

        sensors.put(ndtpConfig.cell19Numbers.mcc, BigDecimal.valueOf(mcc.get()));
        sensors.put(ndtpConfig.cell19Numbers.mnc, BigDecimal.valueOf(mnc.get()));
        sensors.put(ndtpConfig.cell19Numbers.lac, BigDecimal.valueOf(lac.get()));
        sensors.put(ndtpConfig.cell19Numbers.cid, BigDecimal.valueOf(cid.get()));
        sensors.put(ndtpConfig.cell19Numbers.rssi, BigDecimal.valueOf(rssi.get()));
        sensors.put(ndtpConfig.cell19Numbers.time_adv, BigDecimal.valueOf(time_adv.get()));
        /*
        Остальное добавим если вдруг кому-то понадобится
        */
        return sensors;
    }

    @Override
    public int getCell() {
        return 19;
    }

}
