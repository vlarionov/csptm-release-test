/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.navdata.cells;

import ru.advantum.csptm.jep.hub.granit.GranitHubConfig;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class G6CellIdn06 extends G6Base implements IG6BaseCell {
    /**
     * <editor-fold defaultstate="collapsed" desc="Выдержки из протокола">
     * Данные от счетчика – интеллектуальный датчик навигатора. Type=6, N=0 (1…n)
     * поле	длина	тип	Описание
     * 9	struct
     * Количество импульсов за интервал времени (например в 1 секунду)
     * <num_impulse_min>	2	unsigned int16	Минимальное количество импульсов
     * за период времени <time> между двумя навигационными отметками.
     * <num_impulse_max>	2	unsigned int16	Максимальное количество импульсов
     * за период времени <time> между двумя навигационными отметками..
     * <time>	1	unsigned int8	Интервал времени за который счетчик считает количество импульсов.
     * Подсчет количества раз переполнения импульсов определенного значения (например 1000 импульсов )
     * <num_overflow>	1	unsigned int8	Количество переполнения счетчика
     * (сколько раз счетчик просчитал импульсов больше <num_impulse>).
     * <num_impulse>	2	unsigned int16	Количество импульсов при подсчете по кругу.
     * Информация об импульсах
     * <pres_ impulse>	1	unsigned int8	Флаг присутствия импульсов.
     * </editor-fold>
     */

    private final Unsigned16 num_impulse_min = new Unsigned16();
    private final Unsigned16 num_impulse_max = new Unsigned16();
    private final Unsigned8 time = new Unsigned8();
    private final Unsigned8 num_overflow = new Unsigned8();
    private final Unsigned16 num_impulse = new Unsigned16();
    private final Unsigned8 pres_impulse = new Unsigned8();

    public G6CellIdn06(GranitHubConfig.Ndtp ndtpConfig) {
        super(ndtpConfig);
    }

    @Override
    public String extractString() {
        return "G6CellIdn06{" + "num_impulse_min=" + num_impulse_min +
                ", num_impulse_max=" + num_impulse_max +
                ", time=" + time +
                ", num_overflow=" + num_overflow +
                ", num_impulse=" + num_impulse +
                ", pres_impulse=" + pres_impulse + '}';
    }

    @Override
    public Map<Integer, BigDecimal> getSensorData() {
        Map<Integer, BigDecimal> sensors = new HashMap<>(6);

        sensors.put(ndtpConfig.cell06Numbers.num_impulse_min, BigDecimal.valueOf(num_impulse_min.get()));
        sensors.put(ndtpConfig.cell06Numbers.num_impulse_max, BigDecimal.valueOf(num_impulse_max.get()));
        sensors.put(ndtpConfig.cell06Numbers.time, BigDecimal.valueOf(time.get()));
        sensors.put(ndtpConfig.cell06Numbers.num_overflow, BigDecimal.valueOf(num_overflow.get()));
        sensors.put(ndtpConfig.cell06Numbers.num_impulse, BigDecimal.valueOf(num_impulse.get()));
        sensors.put(ndtpConfig.cell06Numbers.pres_impulse, BigDecimal.valueOf(pres_impulse.get()));

        return sensors;
    }

    @Override
    public int getCell() {
        return 6;
    }

}
