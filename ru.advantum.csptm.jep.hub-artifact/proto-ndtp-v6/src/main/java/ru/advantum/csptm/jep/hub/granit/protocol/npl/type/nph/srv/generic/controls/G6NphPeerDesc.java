package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.generic.controls;

import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author mitsay
 * @since 26.11.15.
 */
public class G6NphPeerDesc extends G6Base {

    public String value;

    @Override
    public String extractString() {
        return this.getClass().getSimpleName() + ": {"
                + "value=" + value
                + "}";
    }


    @Override
    public int read(InputStream in) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        while (true) {
            byte[] b = new byte[1];
            if (b[0] == 0x00) {
                break;
            }
            in.read(b);
            baos.write(b);
        }
        int ret = baos.toByteArray().length;
        value = new String(baos.toByteArray());
        baos.close();
        return ret;
    }
}
