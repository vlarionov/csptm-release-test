package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph;

import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

/**
 * @author Kotin <kotin@advantum.ru>
 */

public class G6NphResult extends G6Base {

    private final Unsigned32 error = new Unsigned32();

    public G6NphResult(long error) {
        this.error.set(error);
    }

    public G6NphResult() {
    }

    public long getError() {
        return error.get();
    }

    public G6NphResult setError(long error) {
        this.error.set(error);
        return this;
    }

    public G6NphResult setError(NPH_RESULT_CODES error) {
        this.error.set(error.code());
        return this;
    }

    public G6NphResult setUnavailable() {
        this.error.set(NPH_RESULT_CODES.NPH_RESULT_DEVICE_NOT_AVAILABLE.code());
        return this;
    }

    @Override
    public String extractString() {
        return "NphResult{" + "error=" + error + '}';
    }

    public enum NPH_RESULT_CODES {
        NPH_RESULT_OK(0, "запрос выполнен успешно"),
        NPH_RESULT_UNDEFINED(0xFFFFFFFF, "код для ошибок не имеющих описания"),
        NPH_RESULT_BUSY(1, "участник соединения не может обработать пакет в данный момент"),
        NPH_RESULT_SERVICE_NOT_SUPPORTED(100, "тип обслуживания не поддерживается"),
        NPH_RESULT_SERVICE_NOT_ALLOWED(101, "тип обслуживания запрещен для данного участника соединения"),
        NPH_RESULT_SERVICE_NOT_AVIALABLE(102, "тип обслуживания не доступен в данный момент"),
        NPH_RESULT_PACKET_NOT_SUPPORTED(200, "неизвестный тип пакета, либо тип пакет не поддерживается"),
        NPH_RESULT_PACKET_INVALID_FORMAT(202, "неверный формат пакета"),
        NPH_RESULT_PACKET_INVALID_PARAMETER(203, "неверный параметр пакета"),
        //Ошибки установки соединения:
        NPH_RESULT_PROTO_VER_NOT_SUPPORTED(300, "версия протокола не поддерживается"),
        NPH_RESULT_CLIENT_NOT_REGISTERED(301, "клиент не зарегистрирован на сервере (в БД)"),
        NPH_RESULT_CLIENT_TYPE_NOT_SUPPORTED(302, "тип клиента не поддерживается"),
        NPH_RESULT_CLIENT_AUTH_FAILED(303, "ошибка аутентификации клиента"),

        NPH_RESULT_DEVICE_NOT_AVAILABLE(-1, "устройство недоступно");


        private final long code;
        private final String note;

        NPH_RESULT_CODES(long code, String note) {
            this.code = code;
            this.note = note;
        }

        public static NPH_RESULT_CODES lookup(int code) {
            for (NPH_RESULT_CODES el : NPH_RESULT_CODES.values()) {
                if (el.code() == code) {
                    return el;
                }
            }
            return null;
        }

        public long code() {
            return code;
        }

    }
}
