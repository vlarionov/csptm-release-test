package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.external.device;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class Script implements Serializable {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private final String NAVSCR = "NAVSCR";

    private static final String sID = "<ID>";
    private static final String eID = "</ID>";
    private static final String sFROM = "<FROM>";
    private static final String eFROM = "</FROM>";
    private static final String sTO = "<TO>";
    private static final String eTO = "</TO>";
    private static final String sTYPE = "<TYPE>";
    private static final String eTYPE = "</TYPE>";
    private static final String sSTATUS = "<STATUS>";
    private static final String eSTATUS = "</STATUS>";
    private static final String sMSG = "<MSG id=";
    private static final String eMSG = "</MSG>";

    private static Map<String, String[]> encTags = new HashMap<>();

    static {
        encTags.put("id", new String[]{sID, eID});
        encTags.put("from", new String[]{sFROM, eFROM});
        encTags.put("to", new String[]{sTO, eTO});
        encTags.put("type", new String[]{sTYPE, eTYPE});
        encTags.put("status", new String[]{sSTATUS, eSTATUS});
        encTags.put("msg", new String[]{sMSG, eMSG});
    }

    private byte[] data;
    private String message;
    private Map<String, String> tags = new HashMap<>();

    private String encoding = "Cp1251";

    public Script(byte[] data) {
        parse(data);
    }

    public Script(byte[] data, String encoding) {
        this.encoding = encoding;
        parse(this.data);
    }

    public byte[] encode(Map<String, String> content) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        return baos.toByteArray();
    }

    public Script parse(byte[] data) {
        this.data = new byte[data.length];
        System.arraycopy(data, 0, this.data, 0, data.length);
        message = new String(this.data, Charset.forName(encoding));
        if (message.contains("<" + NAVSCR) && message.contains("</" + NAVSCR)) {
            try {
                tags.put(getCleanTagName(sID), message.substring(message.indexOf(sID) + sID.length(), message.indexOf(eID)));
                tags.put(getCleanTagName(sFROM), message.substring(message.indexOf(sFROM) + sFROM.length(), message.indexOf(eFROM)));
                tags.put(getCleanTagName(sTO), message.substring(message.indexOf(sTO) + sTO.length(), message.indexOf(eTO)));
                tags.put(getCleanTagName(sTYPE), message.substring(message.indexOf(sTYPE) + sTYPE.length(), message.indexOf(eTYPE)));


                int off1 = message.indexOf(sMSG);
                int off2 = message.substring(off1 + 1).indexOf(">") + 2;
                if (off1 != -1 && off2 != -1) {
                    tags.put(getCleanTagName(eMSG), message.substring(off1 + off2, message.indexOf(eMSG)));
                } else if (message.contains(sSTATUS)) {
                    tags.put(getCleanTagName(sSTATUS), message.substring(message.indexOf(sSTATUS) + sSTATUS.length(), message.indexOf(eSTATUS)));
                }
            } catch (Exception ignored) {
                log.debug("SCRIPT ERROR: {} at \n{}", ignored.getMessage(), message);
            }
        }
        return this;
    }

    private String getCleanTagName(String tag) {
        return tag.replace("<", "").replace(">", "").replace("/", "");
    }

    public Map<String, String> getScriptTags() {
        return tags;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder().append(this.getClass().getSimpleName()).append(":{");
        for (Map.Entry<String, String> e : tags.entrySet()) {
            sb.append(e.getKey()).append(":").append(e.getValue()).append(";");
        }
        return sb.append("}").toString();
    }

    public static void main(String... s) {
        //new Script("<NAVSCR ver=1.0><ID>1488177392</ID><FROM>USER</FROM><TO>SERVER</TO><TYPE>QUERY</TYPE><MSG id=9>17/02/27 06:36:32 aaaaaaaaa</MSG></NAVSCR>".getBytes());
        new Script("<NAVSCR ver=1.0><ID>238</ID><FROM>USER</FROM><TO>SERVER</TO><TYPE>ANSWER</TYPE><STATUS>1</STATUS></NAVSCR>".getBytes());
    }
}