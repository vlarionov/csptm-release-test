package ru.advantum.csptm.jep.hub.granit.protocol.npl.type.nph.srv.generic.controls;

import javolution.io.Struct;
import ru.advantum.csptm.jep.hub.granit.protocol.G6Base;

/**
 * Created by zlobina on 07.10.15.
 */
public class G6NphConnAuthString extends G6Base {

    private final Struct.ByteSet data;

    public G6NphConnAuthString(int dataSize) {
        data = new Struct.ByteSet(dataSize);
    }

    public G6NphConnAuthString(String data) {
        this.data = new Struct.ByteSet(data.length());
        this.data.set(data.getBytes());
    }

    public byte[] getData() {
        return data.get();
    }

    public void setData(String data) {
        this.data.set(data.getBytes());
    }
}
