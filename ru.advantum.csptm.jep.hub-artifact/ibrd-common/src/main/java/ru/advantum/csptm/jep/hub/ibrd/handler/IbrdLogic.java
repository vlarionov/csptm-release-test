package ru.advantum.csptm.jep.hub.ibrd.handler;

import com.fatboyindustrial.gsonjavatime.Converters;
import com.google.gson.GsonBuilder;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang.StringUtils;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.MessageEvent;
import ru.advantum.csptm.jep.core.PacketTransferException;
import ru.advantum.csptm.jep.hub.ibrd.CommandStruct;
import ru.advantum.csptm.jep.hub.ibrd.IbrdHub;
import ru.advantum.csptm.jep.hub.ibrd.RegistrationStruct;
import ru.advantum.csptm.jep.netty.JepNettyConnectionHandler;
import ru.advantum.csptm.jep.proto.CommandPacket;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdEvent;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdEventType;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdResponse;
import ru.advantum.csptm.jep.service.ibrd.model.serializer.IbrdEventToPacketAdapter;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;

@Slf4j
public abstract class IbrdLogic<H extends IbrdHub> extends JepNettyConnectionHandler<H> {

    protected static final IbrdEventToPacketAdapter adapter = new IbrdEventToPacketAdapter(Converters.registerAll(new GsonBuilder()).create());

    protected final SynchronizedCommander commander = new SynchronizedCommander();

    private final ConnectionEventHandler connectionEventHandler = new ConnectionEventHandler(this::sendIbrdEvent);

    protected IbrdLogic(H hub) {
        super(hub);
        log.info("channel created");
    }

    public void sendIbrdEvent(IbrdEvent ibrdEvent) {
        try {
            super.sendUnitPackets(adapter.toUnitPacket(ibrdEvent));
        } catch (PacketTransferException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onCommandForSend(CommandPacket commandPacket) {
        IbrdEvent commandEvent = adapter.toIbrdEvent(commandPacket);
        String commandPacketChannelId = commandEvent.getChannelId();
        if (getChannel().getId().toString().equals(commandPacketChannelId)) {
            onCommand(commandEvent);
        }
    }

    protected abstract void onCommand(IbrdEvent commandIbrdEvent);

    @Override
    public final void messageReceived(ChannelHandlerContext ctx, MessageEvent event) throws Exception {
        super.messageReceived(ctx, event);
        Object message = event.getMessage();

        if (message == null) {
            return;
        }

        if (message instanceof IbrdResponse) {
            commander.release((IbrdResponse) message);
        } else {
            onMessageReceived(message);
        }
    }

    @Override
    public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        super.channelClosed(ctx, e);
        connectionEventHandler.onDisconnect(getChannel().getId(), getUnitId());
    }

    protected IbrdEvent buildResponseIbrdEvent(IbrdResponse response, IbrdEvent requestIbrdEvent) {
        val ibrdResponseEvent = new IbrdEvent(
                LocalDateTime.now(),
                IbrdEventType.COMMAND_RESPONSE,
                requestIbrdEvent.getIbrdId(),
                requestIbrdEvent.getChannelId()
        );
        ibrdResponseEvent.setCommandEventId(requestIbrdEvent.getEventId());
        ibrdResponseEvent.setErrorType(response.getErrorType());
        return ibrdResponseEvent;
    }

    private void onMessageReceived(Object message) throws Exception {
        if (message instanceof RegistrationStruct) {
            RegistrationStruct registration = (RegistrationStruct) message;
            String imei = registration.imei.get();
            if (StringUtils.isEmpty(imei)) {
                return;
            }
            initUnitId(imei);
            connectionEventHandler.onConnect(getChannel().getId(), getUnitId());
        }
    }

    @Value
    private static class SendCommandTask {
        CommandStruct commandStruct;
        IbrdEvent ibrdEvent;
    }

    /**
     * Отвечает за работу с внутренней очередью команд
     */
    public class SynchronizedCommander {

        private final Queue<SendCommandTask> commandQueue = new LinkedBlockingQueue<>();

        private final Semaphore commandWaitingSemaphore = new Semaphore(1);

        public synchronized void sendCommand(CommandStruct command, IbrdEvent commandIbrdEvent) {
            command.init();
            commandQueue.add(new SendCommandTask(command, commandIbrdEvent));
            trySendCommand();
        }

        public synchronized void release(IbrdResponse response) throws Exception {
            SendCommandTask task = commandQueue.peek();
            if (task == null || task.commandStruct.getCommandNumber() != response.getCommandNumber()) {
                log.warn("Unexpected command response: {}", response);
            } else {
                commandQueue.remove(task);
                sendUnitPackets(adapter.toUnitPacket(buildResponseIbrdEvent(response, task.getIbrdEvent())));
                commandWaitingSemaphore.release();
                trySendCommand();
            }
        }

        private void trySendCommand() {
            if (commandWaitingSemaphore.tryAcquire()) {
                try {
                    SendCommandTask task = commandQueue.element();
                    CommandStruct commandStruct = task.commandStruct;
                    byte[] commandBytes = commandStruct.getBytes();
                    sendToUnit(commandBytes);
                    log.debug("sent command {}\n bytes {}", commandStruct, Arrays.toString(commandBytes));
                } catch (NoSuchElementException e) {
                    commandWaitingSemaphore.release();
                }
            }
        }
    }

    /**
     * Работа с соединениями  хаб <-> ИТОП
     */
    private static class ConnectionEventHandler {
        private final IbrdEventSender unitUnitPacketSender;

        private static final ConcurrentHashMap<Long, Integer> unitToLastChanelIdMap = new ConcurrentHashMap<>();

        ConnectionEventHandler(IbrdEventSender unitPacketSender) {
            this.unitUnitPacketSender = unitPacketSender;
        }

        void onConnect(int channelId, Long unitId) {
            if (unitId != null) {
                log.info("channel {} opened  ", channelId);
                Integer previousChanelId = unitToLastChanelIdMap.put(unitId, channelId);
                if (previousChanelId != null) {
                    sendDisconnect(previousChanelId, unitId);
                }
                sendConnect(channelId, unitId);
            }
        }

        void onDisconnect(int channelId, Long unitId) {
            if (unitId != null) {
                log.info("channel {} closed ", channelId);
                if (unitToLastChanelIdMap.remove(unitId, channelId)) {
                    sendDisconnect(channelId, unitId);
                }
            }
        }

        private void sendConnect(int channelId, Long unitId) {
            val ibrdResponseEvent = new IbrdEvent(
                    LocalDateTime.now(),
                    IbrdEventType.CONNECT,
                    unitId.intValue(),
                    String.valueOf(channelId)
            );
            unitUnitPacketSender.send(ibrdResponseEvent);
        }

        private void sendDisconnect(int channelId, Long unitId) {
            val ibrdResponseEvent = new IbrdEvent(
                    LocalDateTime.now(),
                    IbrdEventType.DISCONNECT,
                    unitId.intValue(),
                    String.valueOf(channelId)
            );
            unitUnitPacketSender.send(ibrdResponseEvent);
        }

        private interface IbrdEventSender {
            void send(IbrdEvent ibrdEvent);
        }
    }
}
