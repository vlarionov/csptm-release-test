package ru.advantum.csptm.jep.hub.ibrd.handler;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.hub.ibrd.FooterStruct;
import ru.advantum.csptm.jep.hub.ibrd.HeaderStruct;
import ru.advantum.csptm.jep.netty.SimpleStateDecoder;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdErrorType;

public abstract class IbrdDecoder extends SimpleStateDecoder {

    private static final Logger LOGGER = LoggerFactory.getLogger(IbrdDecoder.class);

    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel channel, ChannelBuffer buffer, STATE state) throws Exception {
        bufferCheck(buffer);
        LOGGER.debug("Package received");

        HeaderStruct header = readData(buffer, HeaderStruct.class);
        LOGGER.debug("header: {}", header);

        final Object body = decodeBody(header.commandNumber.get(), buffer);
        LOGGER.debug("body: {}", body);

        FooterStruct footer = readData(buffer, FooterStruct.class);
        LOGGER.debug("footer: {}", footer);

        if (header.packageStart.get() != HeaderStruct.PACKAGE_START_SIGN) {
            LOGGER.warn("received invalid header");
            return null;
        }

        return body;
    }

    protected abstract Object decodeBody(int commandNumber, ChannelBuffer buffer) throws Exception;

    protected IbrdErrorType getErrorType(int errorCode) {
        switch (errorCode) {
            case 0:
                return IbrdErrorType.NULL;
            case 1:
                return IbrdErrorType.PACKET_LENGTH_EXCEEDED;
            case 2:
                return IbrdErrorType.CHECK_SUM_NOT_MUCH;
            case 3:
                return IbrdErrorType.UNEXPECTED_PACKAGE_END;
            case 4:
                return IbrdErrorType.VALUE_OUT_OF_RANGE;
            case 5:
                return IbrdErrorType.COMMAND_CAN_NOT_BE_EXECUTED;
            default:
                throw new IllegalArgumentException("Unknown error code");
        }
    }


}
