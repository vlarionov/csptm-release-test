package ru.advantum.csptm.jep.hub.ibrd.handler;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class IbrdDownstreamShielding extends SimpleChannelDownstreamHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(IbrdDownstreamShielding.class);

    @Override
    public void writeRequested(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
        if (e.getMessage() instanceof ChannelBuffer) {
            ChannelBuffer messageBuffer = (ChannelBuffer) e.getMessage();
            byte[] messageBytes = messageBuffer.array();
            byte[] shieldedBytes = shield(messageBytes);
            Channel channel = ctx.getChannel();
            MessageEvent shieldedMessageEvent =  new DownstreamMessageEvent(
                    channel,
                    Channels.future(channel),
                    ChannelBuffers.wrappedBuffer(shieldedBytes),
                    channel.getRemoteAddress()
            );
            ctx.sendDownstream(shieldedMessageEvent);
        }
    }

    private byte[] shield(byte[] bytes) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        outputStream.write(bytes[0]);
        for (int i =1; i< bytes.length -1 ; i++) {
            byte currentByte = bytes[i];
            if (currentByte == (byte) 0xA5) {
                outputStream.write(new byte[]{(byte) 0xAA, 0x05});
            } else if (currentByte == (byte) 0xAE) {
                outputStream.write(new byte[]{(byte) 0xAA, 0x0E});
            } else if (currentByte == (byte) 0xAA) {
                outputStream.write(new byte[]{(byte) 0xAA, 0x0A});
            } else {
                outputStream.write(currentByte);
            }
        }
        outputStream.write(bytes[bytes.length - 1]);
        byte[] result = outputStream.toByteArray();
        LOGGER.debug("Shielded \n{}\nto\n{}", Arrays.toString(bytes), Arrays.toString(result));
        return result;
    }
}
