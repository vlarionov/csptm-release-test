package ru.advantum.csptm.jep.hub.ibrd;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.csptm.jep.netty.NettyHubConfig;

@Root(name = "ibrd-hub")
public class IbrdHubConfig {
    @Element
    public final NettyHubConfig hub = new NettyHubConfig();
}
