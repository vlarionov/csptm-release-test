package ru.advantum.csptm.jep.hub.ibrd;

import org.jboss.netty.channel.ChannelHandler;
import org.jboss.netty.channel.ChannelPipeline;
import ru.advantum.csptm.jep.core.JepHubController;
import ru.advantum.csptm.jep.hub.ibrd.handler.IbrdDownstreamShielding;
import ru.advantum.csptm.jep.netty.BootstrapConfig;
import ru.advantum.csptm.jep.netty.JepNettyHub;

import java.nio.ByteOrder;

public abstract class IbrdHub extends JepNettyHub {
    protected static final String CONFIG_FILE_NAME = "hub.xml";

    protected IbrdHub(BootstrapConfig config, JepHubController controller) {
        super(config, controller);
    }

    @Override
    protected ByteOrder getByteOrder() {
        return ByteOrder.LITTLE_ENDIAN;
    }

    @Override
    protected void tunePipeline(ChannelPipeline channelPipeline) {
        channelPipeline.addLast("decoder", getDecoder());
        channelPipeline.addLast("shielding", new IbrdDownstreamShielding());
        channelPipeline.addLast("logic", getLogic());
    }

    protected abstract ChannelHandler getDecoder();

    protected abstract ChannelHandler getLogic();

}
