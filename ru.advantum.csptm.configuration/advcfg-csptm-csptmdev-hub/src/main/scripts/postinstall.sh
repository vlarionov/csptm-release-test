#!/bin/sh

 #echo "install ok"

#touch /tmp/pi.log
#date >> /tmp/pi.log

#cat /tmp/pi.log

#basedir for logs
basedir=/usr/local/advantum
logbasedir=/var/log/advantum

if [ ! -d ${logbasedir} ];then
    mkdir ${logbasedir}
fi

############## hubs & services
### create dirs and links for services
services=`find ${basedir}/service/* -maxdepth 0 -type d`

for service in $services
do
    servicename=`basename ${service}`
    if [ ! -d ${logbasedir}/service/${servicename} ];then
        echo "find new service ${servicename} - create dir for log"
        mkdir -p ${logbasedir}/service/${servicename}
    fi

# check if link exist for logs on each service
    if [ ! -L ${basedir}/service/${servicename}/log ]; then
        echo "log link for service ${servicename} not exist, create new soft link"
        ln -s ${logbasedir}/service/${servicename} ${basedir}/service/${servicename}/log
    fi
done


### create dirs and links for hubs
hubs=`find ${basedir}/hubs/* -maxdepth 0 -type d`

for hub in $hubs
do
    hubname=`basename ${hub}`
    if [ ! -d ${logbasedir}/hubs/${hubname} ];then
        echo "find new hub ${hubname} - create dir for log"
        mkdir -p ${logbasedir}/hubs/${hubname}
    fi

# check if link exist for logs on each hub
    if [ ! -L ${basedir}/hubs/${hubname}/log ]; then
        echo "log link for hub ${hubname} not exist, create new soft link"
        ln -s ${logbasedir}/hubs/${hubname} ${basedir}/hubs/${hubname}/log
    fi

done

############# nexus assist
#for tgt in `cat nexus-assist.proj | grep target | cut -d"=" -f 2`; do nexus-assist snapshot $tgt; done

