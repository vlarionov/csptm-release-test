#!/bin/sh
. /etc/rc.d/init.d/functions

jvm_memory="-Xms16m -Xmx32m -Xss1m"
hubdir="/usr/local/advantum/hubs"
cd $hubdir

function hublist() {
	for hubtypes in `for dirs in \`find ./ -maxdepth 1 -type d | sort\`; do basename $dirs | grep -v "^\."; done`
	do
		if [ -d "./${hubtypes}/conf" ];then
			for hubs in `for dirhub in \`find ./${hubtypes}/conf/* -maxdepth 1 -type d | sort\`; do basename $dirhub | grep -v "^\."; done`
			do
				if [ -s "./${hubtypes}/conf/${hubs}/hub.xml" ];then
					if [ -s "./${hubtypes}/conf/${hubs}/log4j.prop" ];then
#						hubid=`xmlstarlet sel -T -t -m //"hub-config" -v @hubId -n ${hubtypes}/conf/${hubs}/hub.xml`
						echo "${hubtypes}/conf/${hubs}"
#						echo "hubid:$hubid"
					fi
				fi
			done
		fi
	done
}

function check_exist_hub() {
	hubexist=`hublist | grep "^$hubname$"`
	hubexist_exit="$?"
	if [ "$hubexist_exit" -ne "0" ];then
		echo "hub: $hubname not exist"
		return 1
	fi
}

function check_type_hub() {
	typehub=`echo $hubname | cut -d"/" -f 1`
}

function check_run_hub() {
	check_type_hub $hubname
	pidfile="$typehub/log/`basename $hubname`.pid"
	if [ -s $pidfile ];then
		pid=`cat $pidfile`
	else
#		echo "$pidfile not exist or empty"
		return 1
	fi

	if [ "`ps -fp $pid | grep $pid | grep $hubname`" ];then
		#echo "Process is running"
		return 0
	fi

	return 2
	
}

function starthub() {
	hubname=$1
#	echo "Start $hubname"
	check_exist_hub $hubname
	if [ "$?" -ne "0" ];then
        echo -n "$hubname: not exist"
		failure
		echo
		exit 1
    fi
	check_run_hub $hubname

	if [ "$?" -eq "0" ];then
		echo -n "$hubname: already running."
		success 
		echo
	else
		if [ -e "${hubname}/stop" ];then
			echo -n "${hubname}: must not be run"
			failure
			echo
			return 1
		fi
		check_type_hub $hubname
		echo -n "$hubname: starting..."
		success
		echo 
		java=`which java`
		lang="user.language=en"
		log4j="log4j.configuration=file:$hubname/log4j.prop"
		cfgdir="advantum.config.dir=$hubname"
		if [ -s "${hubname}/java.prop" ];then
			. ${hubname}/java.prop
#		else
#			#default_value
#			jvm_memory="-Xms16m -Xmx32m -Xss1m"
		fi
		jar=`find $typehub/ -maxdepth 1 -type f -name *jar`
		#crushlog="$typehub/log/`basename $hubname`/`basename $hubname`.crush"
		crushlog="$typehub/log/`basename $hubname`.crush"
		$java -D$lang -D$log4j -D$cfgdir $jvm_memory -jar $jar  > $crushlog 2>&1 &
		pid="$!"
		#pidfile="$typehub/log/`basename $hubname`/`basename $hubname`.pid"
		pidfile="$typehub/log/`basename $hubname`.pid"
		echo $pid > $pidfile
	fi
}

function start() {
	#at-v3.0/conf/barnaul/hub.xml
	hubname=$2
	if [ "$2" = "" ];then
		echo "Nothing to start"
		echo "Use '$0 start all' or see '$0 list' for list hubs"
		exit
	fi

	if [ "$2" = "all" ];then
		for hubname in $(hublist)
		do
			starthub $hubname
		done
	else
		if [ "$2" != "" ];then
			starthub $hubname
		fi
	fi
}

function stophub() {
	check_exist_hub $hubname
	if [ "$?" -ne "0" ];then
		echo -n "$hubname: not exist 11"
		failure
		echo
		exit 1
	fi
    check_run_hub $hubname
	if [ "$?" -ne "0" ];then
		echo -n "$hubname: not running."
		passed
		echo
	else
	    check_type_hub $hubname
    	echo -n "$hubname: stopped."
		success
		echo 
	    pidfile="$typehub/log/`basename $hubname`.pid"
		kill -9 `cat $pidfile`
		rm -f $pidfile
	fi
}

function stop() {
	hubname=$2
    if [ "$2" = "" ];then
        echo "Nothing to stop"
		echo "Use '$0 stop all' or see '$0 list' for list hubs"
        exit
    fi

    if [ "$2" = "all" ];then
		echo "Are you sure to stop all HUBS?"
		pass=`cat /dev/urandom | tr -dc A-Z | head -c 3`
		echo "Type '$pass' to continue"
		read uinput
		if [ "$pass" != "$uinput" ];then
			echo "Wrong.. Exiting without stoping hubs"
			exit
		fi
		
        for hubname in $(hublist)
        do
            stophub $hubname
        done
    else
        if [ "$2" != "" ];then
            stophub $hubname
        fi
    fi
}

function displayList() {
#	hublist | perl -pe 's/\A(.*conf.*)\n/$1 /g'
#                       hubid=`xmlstarlet sel -T -t -m //"hub-config" -v @hubId -n ${hubtypes}/conf/${hubs}/hub.xml`
#                        echo "${hubtypes}/conf/${hubs}"
#                       echo "hubid:$hubid"
	for hubname in $(hublist)
	do
		hubid=`xmlstarlet sel -T -t -m //"hub-config" -v @hubId -n ${hubname}/hub.xml`
		if [ "$hubid" == "" -a -e $(dirname $(dirname ${hubname}))/hubid.alternative ]
		then
			# echo $(dirname ${hubname})
			# echo "${hubname} hubid:9999"
			
			strlet=$(cat $(dirname $(dirname ${hubname}))/hubid.alternative)

			hubid=`xmlstarlet sel -T -t -m //"${strlet}" -v @hubId -n ${hubname}/hub.xml`
		fi

		if [ -s "${hubname}/java.prop" ]; then
			jmem=`cat ${hubname}/java.prop | cut -d"=" -f 2`
		else
			jmem=$jvm_memory
		fi

#		echo "${hubname} hubid:$hubid java:\"${jmem}\""
		printf "${hubname}\thubid:$hubid\tjava:\"${jmem}\"\n"
	done
}

function status() {
	for hubname in $(hublist)
	do
		echo -n "$hubname"
		check_run_hub $hubname
		if [ "$?" -eq "0" ];then
			success
		else
			warning
		fi
		echo 
	done
}

function showlog() {
	if [ "$2" != "" ]
	then
		type=`echo $2 | awk '{split($1, a, "/"); print a[1];}'`	
		name=`echo $2 | awk '{split($1, a, "/"); print a[3];}'`
		hlog=$hubdir/$type/log/$name.log
		if [ -f $hlog ]
		then
			less $hlog
		else
			echo "Wrong type or hub name, file $hlog not found" 
		fi 
	else
		echo "Usage: hub log [hubname], please use one of following:"
		hublist
	fi
}

function mkconf() {
	type=$2
	name=$3
	logconfig=$hubdir/$type/conf/$name/log4j.prop
	hubconf=$hubdir/$type/conf/$name/hub.xml

	if [ "$type" != "" -a "$name" != "" -a -d $hubdir/$type ]
	then
		mkdir $hubdir/$type/conf/$name -p
	
		if [ ! -e $logconfig ]
		then
			echo "log4j.rootLogger=, file" > $logconfig
			echo "logs_location=/usr/local/advantum/hubs/$type/log" >> $logconfig
			echo "ConversationPattern=%d %c %-4r %-5p [%t] %3x - %m%n" >> $logconfig
			echo "log4j.appender.file=org.apache.log4j.RollingFileAppender" >> $logconfig
			echo "log4j.appender.file.layout=org.apache.log4j.PatternLayout" >> $logconfig
			echo "log4j.appender.file.layout.ConversionPattern=\${ConversationPattern}" >> $logconfig
			echo "log4j.appender.file.file=\${logs_location}/$name.log" >> $logconfig
			echo "log4j.appender.file.threshold=INFO" >> $logconfig
			echo "log4j.appender.file.MaxBackupIndex=2" >> $logconfig
			echo "log4j.appender.file.MaxFileSize=20480KB" >> $logconfig
			echo "$logconfig was created, please check"
		fi 

        	if [ ! -e $hubconf ]
	        then
	                cp $hubdir/$type/sample-config.xml $hubconf
	                echo "$hubconf was created, please check"
	                vi $hubconf
	        fi
	else
		echo "Usage: ./hub.sh mkconf \$hubtype \$hubname"
		echo "Example: ./hub.sh mkconf transnavi-v3.0 testhub"
	fi
}

function capture() {
	port=$2
	tshark -l -i ens32 -f "port $port" -T fields -e frame.date -e frame.time -e ip.src -e ip.dst -e tcp.srcport -e tcp.dstport -e data
}

function hubneedrun() {

#	echo "hub need to be running"

	hardware=`uname -i`
	if [ "$hardware" = "i386" ];then
		export TNS_ADMIN=/usr/lib/oracle/11.2/client/network/admin
		sqlplus=sqlplus
	elif [ "$hardware" = "x86_64" ];then
		export TNS_ADMIN=/usr/lib/oracle/11.2/client64/network/admin
		sqlplus=sqlplus64
	else
		echo "unknown hardware"
		exit 1
	fi

	dblogin=$2
	dbpass=$3
	dbname=$4

	sqlres=`$sqlplus -s $dblogin/$dbpass@$dbname <<EOF
	set heading off
	set feedback off
	--set colsep ","


	SELECT DISTINCT U.ID_HUB
	FROM MON.UNIT2TR U2T
			,MON.UNIT    U
			,MON.TR      T
			,MON.HUB     H
	WHERE U2T.ID_UNIT = U.ID_UNIT
			AND T.ID_TR = U2T.ID_TR
			AND T.IS_SERVED = 1
			AND H.ID_HUB = U.ID_HUB
			AND H.ACTIVE = 1;

EOF
`

	tmphubfile=/tmp/hubids_`date +%s`.tmp
#	$0 dlist > $tmphubfile
	#fix hubs without hubid
	$0 dlist | grep -v "hubid:$" > $tmphubfile
#	echo tmphubfile:$tmphubfile


	for hubid in $sqlres
	do
#		echo hid:$hubid
		hubname=`cat $tmphubfile | grep -E "hubid:$hubid[[:space:]]" | awk '{print $1}'`
#or		hubname=`cat $tmphubfile | grep -E "hubid:$hubid[[:space:]]" | sed 's/\s.*//'`
		if [ "${hubname}" = "" ];then
			errmsg="hub:$hubid not found on server / $errmsg"
		else
			check_run_hub $hubname
			if [ "$?" -ne "0" ];then
				errmsg="$hubname not started / $errmsg"
			fi
		fi
	done

	/bin/rm -f $tmphubfile

	if [ "$errmsg" = "" ];then
		echo "Ok"
		return 0
	else
		echo "Critical:$errmsg"
		return 2
	fi

}

case $1 in
	dlist)
		displayList
	;;
	list)
		hublist
	;;
	start)
		start $@
	;;
	stop)
		stop $@
	;;
	restart)
		stop $@
		sleep 3
		start $@
	;;
	status)
		status
	;;
	log)
		showlog $@
	;;
	mkconf)
		mkconf $@
	;;
	capture)
		capture $@
	;;
	hubneedrun)
		hubneedrun $@
	;;
	*)
		echo "Usage: hub [list|start|stop|restart|status|log|mkconf|hubneedrun]"
	;;
esac

