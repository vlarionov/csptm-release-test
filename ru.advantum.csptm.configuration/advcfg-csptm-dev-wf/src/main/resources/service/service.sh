#!/bin/sh

##edited at 20170518 separate stop and kill function
. /etc/rc.d/init.d/functions

servicedir="/usr/local/advantum/service"

cd $servicedir

function servicelist() {
    for services in `for dirs in \`find ./ -maxdepth 1 -type d | sort\`; do basename $dirs | grep -v "^\."; done`
    do
        if [ -s "./${services}/advconf/log4j.prop" -o -s "./${services}/advconf/logback.xml" ];then
            echo "$services"
        fi

		#searching multinode services
		if [ -d "./${services}/nodes" ];then
			for servnode in `find ./${services}/nodes/* -maxdepth 1 -type d | sort`
			do
				#echo orig:$servnode
				if [ -s "$servnode/log4j.prop" -o -s "./${servnode}/logback.xml" ];then
					echo $servnode | sed -e 's!./\(.*\)/\(.*\)/\(.*\)!\1/\3!'
				fi
			done
		fi
    done

}

function check_is_multiservice() {
	#fix
    if [[ "$1" == */* ]];then
        service=`echo $1 | cut -d"/" -f1`
        servicename=`echo $1 | cut -d"/" -f2`
    else
        servicename=""
    fi
}

function check_exist_service() {
	#fix
#	echo "serviceexist=servicelist | grep $service${servicename:+/$servicename}"
	serviceexist=`servicelist | grep "^$service${servicename:+/$servicename}$"`
	serviceexist_exit="$?"
#	echo exist:${serviceexist_exit}
	if [ "$serviceexist_exit" -eq "0" ];then
#		echo "service: ${service}${servicename:+/$servicename}:exist"
#		echo se:$serviceexist_exit
		return $serviceexist_exit
	fi

#	echo "service: ${service}${servicename:+/$servicename}: not exist!!"
#	echo se:$serviceexist_exit
	return $serviceexist_exit
}

function check_run_service() {
	#fix
	pidfile="$service/log/${servicename:-$service}.pid"
#	pidfile="$service/log/${service}.pid"
#	echo " "
#	echo p:$pidfile
    if [ -s $pidfile ];then
    	pid=`cat $pidfile`
    else
#	   	echo "$pidfile not exist or empty"
		servicerun_exit=1
    	return $servicerun_exit
    fi

    if [ "`ps -fp $pid | grep $pid | grep ${servicename:-$service}`" ];then
#	    echo "Process is running"
		servicerun_exit=0
    	return $servicerun_exit
    fi

	servicerun_exit=2
    return $servicerun_exit
        
}

function status() {
	#fix
	for service in $(servicelist)
	do
#		echo s1:$service
		check_is_multiservice ${service}
#		echo "check status"
#		echo ss1:$service
#	    echo sn1:$servicename
#		echo "check_exist_service $service${servicename:+/$servicename}"
		check_exist_service $service${servicename:+/$servicename}
#		echo ss2:$service
#	    echo sn2:$servicename
#		echo "check_run_service $service${servicename:+/$servicename}"
		check_run_service $service${servicename:+/$servicename}
#		echo ss3:$service
#	    echo sn3:$servicename
#		echo "###################################"

		echo -n "$service${servicename:+/$servicename}"
			check_run_service $servicename
		if [ "$serviceexist_exit" -eq "0" -a "$servicerun_exit" -eq "0" ];then
			success
		else
			warning
		fi
		echo
	done
}

function stop() {
	#fix
	service=$2
    if [ "$2" = "" ];then
        echo "Nothing to stop"
                echo "Use '$0 stop all' or see '$0 list' for list services"
        exit
    fi

    if [ "$2" = "all" ];then
		echo "Are you sure to stop all SERVICES?"
        pass=`cat /dev/urandom | tr -dc A-Z | head -c 3`
        echo "Type '$pass' to continue"
        read uinput
        if [ "$pass" != "$uinput" ];then
        	echo "Wrong.. Exiting without stoping services"
            exit
        fi
                
        for service in $(servicelist)
        do
            stopservice $service
        done
    else
        if [ "$2" != "" ];then
            echo "stopservice $service"
            stopservice $service
        fi
    fi
}

function killpid() {
    #kill service 
    service=$2
    if [ "$2" = "" ];then
        echo "Nothing to kill"
                echo "Use '$0 kill all' or see '$0 list' for list services"
        exit
    fi

    if [ "$2" = "all" ];then
        echo "Are you sure to kill all SERVICES?"
        pass=`cat /dev/urandom | tr -dc A-Z | head -c 3`
        echo "Type '$pass' to continue"
        read uinput
        if [ "$pass" != "$uinput" ];then
            echo "Wrong.. Exiting without killing services"
            exit
        fi

        for service in $(servicelist)
        do
            killservice $service
        done
    else
        if [ "$2" != "" ];then
            echo "killservice $service"
            killservice $service
        fi
    fi
}

function stopservice() {
	#fix
	check_is_multiservice ${service}
    check_exist_service $service${servicename:+/$servicename}
	if [ "$?" -eq "1" ];then
		echo "$service${servicename:+/$servicename} not exist. nothing to stop"
		return 10
	fi
	
    check_run_service $service${servicename:+/$servicename}
    if [ "$?" -ne "0" ];then
    	echo -n "$service${servicename:+/$servicename}: not running."
    	passed
    	echo
    else
    	echo -n "$service${servicename:+/$servicename}: trying to stop "
    	pidfile="$service/log/${servicename:-$service}.pid"

### find timeout in xml config
		if [ -z "$servicename" ];then
			cfgxml=`find ${servicedir}/$service/advconf/ -maxdepth 1 -type f -name *.xml | grep -v logback.xml`
        else
			#multinode service
            if [ -f ${servicedir}/$service/nodes/${servicename}/node.prop ]
            then
                cfgxml=${servicedir}/$service/nodes/${servicename}/node.prop
            else
                cfgxml=`find ${servicedir}/$service/nodes/${servicename} -maxdepth 1 -type f -name *.xml | grep -v logback.xml`
            fi
		fi
		stoptimeout=`xmlstarlet sel -T -t -m /"*" -v @timeout-sec -n $cfgxml`
		stoptimeout=$((stoptimeout+5))
		kill -15 `cat $pidfile`
		i=1
		while ps -p `cat $pidfile` > /dev/null
		do
			if [ "$i" -le  "$stoptimeout" ]; then
				sleep 1
				((i++))
				echo -n "."
			else
				failure
				echo
				echo
				echo "You can also try: $0 kill $service${servicename:+/$servicename}"
				return 1
			fi
		done

    	rm -f $pidfile
		success
		echo
    fi
}

function killservice() {
    #fix
    check_is_multiservice ${service}
    check_exist_service $service${servicename:+/$servicename}
    if [ "$?" -eq "1" ];then
        echo "$service${servicename:+/$servicename} not exist. nothing to kill"
        return 10
    fi
    
    check_run_service $service${servicename:+/$servicename}
    if [ "$?" -ne "0" ];then
        echo -n "$service${servicename:+/$servicename}: not running."
        passed
        echo
    else
        echo -n "$service${servicename:+/$servicename}: killed."
        success
        echo 
        pidfile="$service/log/${servicename:-$service}.pid"
        kill -9 `cat $pidfile`
        rm -f $pidfile
    fi
}


function startservice() {
#        servicename=$1
#       echo "Start $servicename"
	check_is_multiservice ${service}
    check_exist_service $service${servicename:+/$servicename}
    check_run_service $service${servicename:+/$servicename}

    if [ "$?" -eq "0" ];then
    	echo -n "$service${servicename:+/$servicename}: already running."
    	success 
    	echo
    else
    	echo -n "$service${servicename:+/$servicename}: starting..."
        success
        echo 
        java=`which java`
        lang="user.language=en"
		if [ -z "$servicename" ];then
			if [ -e "${servicedir}/$service/advconf/stop" ];then
				echo -n "$service${servicename:+/$servicename} : must not be run. \"stop\" file exist."
				failure
				echo
				return 1
			fi
	        javalogger="log4j.configuration=file:${servicedir}/$service/advconf/log4j.prop"
	        if [ -e "${servicedir}/$service/advconf/logback.xml" ];then
                    javalogger="logging.config=file:${servicedir}/$service/advconf/logback.xml"
            fi    
        	cfgdir="advantum.config.dir=${servicedir}/$service/advconf"
			javaprop="${servicedir}/$service/advconf/java.prop"
		else
			if [ -e "${servicedir}/$service/nodes/${servicename}/stop" ];then
                echo -n "$service${servicename:+/$servicename} : must not be run. \"stop\" file exist."
                failure
                echo
                return 1
            fi
	        javalogger="log4j.configuration=file:${servicedir}/$service/nodes/${servicename}/log4j.prop"
	        if [ -e "${servicedir}/$service/nodes/${servicename}/logback.xml" ];then
                    javalogger="logging.config=file:${servicedir}/$service/nodes/${servicename}/logback.xml"
            fi
        	cfgdir="advantum.config.dir=${servicedir}/$service/nodes/${servicename}"
			if [ -f ${servicedir}/$service/nodes/${servicename}/node.prop ]
			then
				cfgxml=${servicedir}/$service/nodes/${servicename}/node.prop
			else
				cfgxml=`basename \`find ${servicedir}/$service/nodes/${servicename} -maxdepth 1 -type f -name *.xml | grep -v logback.xml\``
			fi
			javaprop="${servicedir}/$service/nodes/${servicename}/java.prop"
		fi
		
		if [ -s "${javaprop}" ];then
			. ${javaprop}
		else
			#default_value
			jvm_memory="-Xms32m -Xmx64m -Xss1m"
		fi
        jar=`find $service/ -maxdepth 1 -type f -name *jar`
        crushlog="$service/log/${servicename:-$service}.crush"
#        echo "$java -D$lang -D$javalogger -D$cfgdir $jvm_memory -jar $jar ${servicename:+$cfgxml $servicename}"
		$java -Duser.timezone=GMT -D$lang -D$javalogger -D$cfgdir $jvm_memory -jar $jar ${servicename:+$cfgxml $servicename} > $crushlog 2>&1 &
		echo "$java -Duser.timezone=GMT -D$lang -D$javalogger -D$cfgdir $jvm_memory -jar $jar ${servicename:+$cfgxml $servicename}"
        pid="$!"
        pidfile="$service/log/${servicename:-$service}.pid"
        echo $pid > $pidfile
	fi
}

function start() {
	#mobile-server-v1/advconf/mobile-server.xml
    service=$2
    if [ "$2" = "" ];then
    	echo "Nothing to start"
        echo "Use '$0 start all' or see '$0 list' for list services"
        exit
    fi

    if [ "$2" = "all" ];then
    	for service in $(servicelist)
        do
        	startservice $service
        done
    else
    	if [ "$2" != "" ];then
        	startservice $service
        fi
    fi
}

function monitor(){
	service=$2
	if [ "$#" -ne "4" ];then
        echo "Not valid parameters.. Must be type '$0 servicename logrows maxwarn maxcrit'"
        exit 1
	fi
}


case $1 in
	list)
		servicelist
	;;
	start)
		start $@
	;;
	stop)
		stop $@
	;;
	kill)
		killpid $@
	;;
	restart)
		stop $@
		sleep 3
		start $@
	;;
	status)
		status
	;;
	log)
		echo "under construction"
		exit 1
		showlog $@
	;;
	monitor)
		monitor $2
	;;
	*)
		echo "Usage: service2.sh [list|start|stop|restart|status|log|monitor]"
	;;
esac

