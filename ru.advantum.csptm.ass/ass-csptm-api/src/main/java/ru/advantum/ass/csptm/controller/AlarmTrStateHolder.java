package ru.advantum.ass.csptm.controller;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import ru.advantum.ass.csptm.controller.dto.IncidentTrailDto;

/**
 * Created by kaganov on 12/01/2017.
 */
@Component
public class AlarmTrStateHolder {
    private static final Logger LOGGER = LoggerFactory.getLogger(AlarmTrStateHolder.class);

    private final Gson gson;
    private final JedisPool jedisPool;


    private final AtomicReference<Map<Long, Optional<IncidentTrailDto>>> incidentTrState = new AtomicReference<>();


    @Autowired
    public AlarmTrStateHolder(Gson gson, JedisPool jedisPool) {
        this.gson = gson;
        this.jedisPool = jedisPool;
    }

    public Map<Long, Optional<IncidentTrailDto>> getAlarms() {
        return incidentTrState.get();
    }

    @Scheduled(fixedDelay = 5000L, initialDelay = 5000L)
    @PostConstruct
    public void updateState() {
        try {
            try (Jedis jedis = jedisPool.getResource()) {
                IncidentTrailDto[] ir = gson.fromJson(jedis.get(IncidentTrailDto.INCIDENT_RIBBON_MAP_NAME), IncidentTrailDto[].class);
                if (ir != null) {
                    Map<Long, Optional<IncidentTrailDto>> a = Arrays.asList(ir).stream()
                            .filter(t -> t.getTrId() != null)
                            .filter(t -> t.getIncidentTypeId() !=null)
                            .collect(Collectors.groupingBy(IncidentTrailDto::getTrId,
                                            Collectors.maxBy(Comparator.comparing(IncidentTrailDto::getIncidentId))));
                    incidentTrState.set(a);
                    LOGGER.info("update incidents for trs count: {}.", a.size());
                }
            }
        } catch (Throwable ex) {
            LOGGER.error("updateAlarms", ex);
        }
    }

}
