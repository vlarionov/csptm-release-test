package ru.advantum.ass.csptm.controller.dto;

import java.time.Instant;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Builder
@Data
public class IncidentTrailDto {

    public static final String INCIDENT_RIBBON_MAP_NAME = "incident-ribbon";
    @SerializedName("incident_id")
    private Long incidentId;
    @NonNull
    @SerializedName("incident_initiator_id")
    private Short incidentInitiatorId;
    @NonNull
    @SerializedName("incident_status_id")
    private Short incidentStatusId;
    @NonNull
    @SerializedName("incident_type_id")
    private Short incidentTypeId;
    @NonNull
    @SerializedName("time_start")
    private Instant timeStart;
    @SerializedName("parent_incident_id")
    private Long parentIncidentId;
    @SerializedName("account_dd")
    private Integer accountId;
    @SerializedName("tr_id")
    private Long trId;
    @SerializedName("unit_id")
    private Long unitId;
    @SerializedName("incident_type_name")
    private String inctdentTypeName;
    @SerializedName("tr_licence")
    private String trLicence;
    @SerializedName("tr_garage_name")
    private String trGarageNum;
    @SerializedName("incident_type_short_name")
    private String inctdentTypeShortName;
    @SerializedName("tt_variant_id")
    private Integer ttVariantId;
    @SerializedName("tt_variant_name")
    private String ttVariantName;
    @SerializedName("driver_id")
    private Integer drivertId;
    @SerializedName("driver_name")
    private String driverName;
    @SerializedName("is_alarm")
    private Boolean isAlarm;
    @SerializedName("incident_reason_id")
    private Integer incidentReasonId;
    @SerializedName("incident_reason_name")
    private String incidentReasonName;
    @SerializedName("route_id")
    private Integer routeId;
    @SerializedName("incomming_message")
    private String incommingMessage;
    @SerializedName("incomming_message_id")
    private String incommingMessageId;
    @SerializedName("out_num")
    private String outNum;
    @SerializedName("shift_num")
    private String shiftNum;
    @SerializedName("tr_type_name")
    private String trTypeName;
    @SerializedName("stop_item_id")
    private Integer stopItemId;
    @SerializedName("stop_item_name")
    private String stopItemName;
}


