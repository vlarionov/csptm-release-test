package ru.advantum.ass.csptm.dao;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import lombok.val;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component
public class AccountPermissionsDao {
    private final SqlSessionFactory sqlSessionFactory;

    @Autowired
    public AccountPermissionsDao(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }

    @PostConstruct
    public void addMapper() {
        sqlSessionFactory.getConfiguration().addMapper(AccountPermissionsMapper.class);
    }

    @Cacheable("availableEventTypes")
    public Set<Short> getAvailableIncidentTypes(Long idJOFLAccount) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            AccountPermissionsMapper accountPermissionsMapper = sqlSession.getMapper(AccountPermissionsMapper.class);
            List<Short> availableIncidentTypesIds = accountPermissionsMapper.findAvailableIncidentsTypesIds(idJOFLAccount);
            return new HashSet<>(availableIncidentTypesIds);
        }
    }

    @Cacheable("availableTrs")
    public Set<Long> getAvailableTrs(Long idJOFLAccount) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            val accountTrVisibilityMapper = sqlSession.getMapper(AccountPermissionsMapper.class);
            return new HashSet<>(accountTrVisibilityMapper.findAvailableTrs(idJOFLAccount));
        }
    }

    public interface AccountPermissionsMapper {
        List<Short> findAvailableIncidentsTypesIds(@Param("idJOFLAccount") Long idJOFLAccount);
        List<Long> findAvailableTrs(@Param("idJOFLAccount") Long idJOFLAccount);
    }

}
