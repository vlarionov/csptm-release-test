package ru.advantum.ass.csptm.controller;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import ru.advantum.ass.csptm.controller.dto.TrStateDto;
import ru.advantum.csptm.jep.proto.TrState;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * Created by kaganov on 12/01/2017.
 */
@Component
public class NavigationStateHolder {

    private static final String SS_MAP_NAME = "statesaver-state";

    private static final Logger LOGGER = LoggerFactory.getLogger(NavigationStateHolder.class);

    private final Gson gson;
    private final JedisPool jedisPool;
    private final AtomicReference<Map<Long, TrStateDto>> trsState = new AtomicReference<>();

    public NavigationStateHolder(Gson gson, JedisPool jedisPool) {
        this.gson = gson;
        this.jedisPool = jedisPool;
    }


    @PostConstruct
    @Scheduled(fixedDelay = 5_000, initialDelay = 5_000)
    public void updateState() {
        try {
            try (Jedis jedis = jedisPool.getResource()) {
                Map<String, String> stringState = jedis.hgetAll(SS_MAP_NAME);
                Map<Long, TrState> state = stringState.entrySet().stream()
                        .collect(Collectors.toMap(e -> Long.valueOf(e.getKey()),
                                                  e -> gson.fromJson(e.getValue(), TrState.class)));
                LOGGER.info("New state size {}", state.size());
                trsState.set(toDto(state));
            }
        } catch (Throwable ex) {
            LOGGER.error("updateState", ex);
        }
    }

    private Map<Long,TrStateDto> toDto( Map<Long,TrState> stateMap) {
        return stateMap.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> new TrStateDto(e.getValue())));
    }

    public Map<Long, TrStateDto> getTrLocation() {
        return trsState.get();
    }
}
