package ru.advantum.ass.csptm.session;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import java.time.Instant;

/**
 * @author kaganov
 */
@Component
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CsptmSession {

    private Instant lastLocationRequest;

    public Instant getLastLocationRequest() {
        return lastLocationRequest;
    }

    public void setLastLocationRequest(Instant lastLocationRequest) {
        this.lastLocationRequest = lastLocationRequest;
    }
}
