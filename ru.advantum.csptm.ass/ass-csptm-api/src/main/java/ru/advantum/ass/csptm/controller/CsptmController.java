package ru.advantum.ass.csptm.controller;

import java.time.Instant;
import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.advantum.ass.csptm.controller.dto.TrStateDto;
import ru.advantum.ass.csptm.controller.dto.UnitPacketDto;
import ru.advantum.ass.csptm.dao.AccountPermissionsDao;
import ru.advantum.ass.csptm.session.CsptmSession;
import ru.advantum.ass.jofl.exception.PermissionDeniedException;
import ru.advantum.ass.jofl.session.JoflSession;

/**
 * @author kaganov
 */
@RestController
@RequestMapping(method = {RequestMethod.GET, RequestMethod.POST})
public class CsptmController {

    @Autowired
    private RmqController rmqController;

    private static class AlarmWebDto {
        private final short level;
        private final String name;
        private final boolean isConfirm;
        private final short type;
        private final int priority;

        AlarmWebDto(short level, String name, boolean isConfirm, short type, int priority) {
            this.level = level;
            this.name = name;
            this.isConfirm = isConfirm;
            this.type = type;
            this.priority = priority;
        }

        public short getLevel() {
            return this.level;
        }

        public String getName() {
            return this.name;
        }

        public boolean isConfirm() {
            return this.isConfirm;
        }

        public short getType() {
            return this.type;
        }

        public int getPriority() {
            return this.priority;
        }
    }

    private final JoflSession joflSession;
    private final CsptmSession csptmSession;
    private final NavigationStateHolder stateHolder;
    private final AlarmTrStateHolder alarmTrStateHolder;
    private final AccountPermissionsDao accountPermissionsDao;

    @Autowired
    public CsptmController(JoflSession joflSession, CsptmSession csptmSession, NavigationStateHolder stateHolder,
                           AlarmTrStateHolder alarmTrStateHolder, AccountPermissionsDao accountPermissionsDao) {
        this.joflSession = joflSession;
        this.csptmSession = csptmSession;
        this.stateHolder = stateHolder;
        this.alarmTrStateHolder = alarmTrStateHolder;
        this.accountPermissionsDao = accountPermissionsDao;
    }

    private void checkValidSession() {
        if (joflSession.getIdJOFLAccount() == null) {
            throw new PermissionDeniedException("Session is not valid!");
        }
    }

    @RequestMapping("/csptm.getLocation.aws")
    public Map<Long, UnitPacketDto> getLocation(@RequestParam(name = "force", defaultValue = "false") boolean force) {
        checkValidSession();
        Instant lastLocationRequest = csptmSession.getLastLocationRequest();
        val trStateEntries = stateHolder.getTrLocation().entrySet();
        Stream<Map.Entry<Long, TrStateDto>> trsStateStream = trStateEntries.stream();
        if (!force && lastLocationRequest != null) {
            val availableTrIds = accountPermissionsDao.getAvailableTrs(joflSession.getIdJOFLAccount());
            trsStateStream = trsStateStream
                    .filter(entry -> availableTrIds.contains(entry.getKey()))
                    .filter(en -> !en.getValue().getLastUpdateInstant().isBefore(lastLocationRequest));
        }
        Map<Long, UnitPacketDto> trsState = trsStateStream.collect(Collectors.toMap(Map.Entry::getKey, en -> en.getValue().getPacket()));
        Optional<Instant> maxTime = trStateEntries.stream()
                .map(Map.Entry::getValue)
                .map(TrStateDto::getLastUpdateInstant)
                .max(Comparator.naturalOrder());

        maxTime.ifPresent(csptmSession::setLastLocationRequest);

        return trsState;
    }


    @RequestMapping("/csptm.getAlarmTr.aws")
    public Map<Long, AlarmWebDto> getAlarmTR(@RequestParam(name = "force", defaultValue = "false") boolean force) {
        checkValidSession();
        val availableAlarms = accountPermissionsDao.getAvailableIncidentTypes(joflSession.getIdJOFLAccount());
        return alarmTrStateHolder.getAlarms().entrySet()
                .stream()
                .filter(entry -> availableAlarms.contains(entry.getValue().get().getIncidentTypeId()))
                .collect(Collectors.toMap(Map.Entry::getKey, v ->
                                                    new AlarmWebDto((short) 1
                                                            , v.getValue().get().getInctdentTypeShortName()
                                                            , false
                                                            , v.getValue().get().getIncidentTypeId()
                                                            , 0)
                ));
    }

    @RequestMapping(value = "/getAlarms", method = RequestMethod.GET)
    public Map<Long,Set<AlarmTypeName>> getTransport() {
        return rmqController.getTrAlarms().getTrAlarms();
    }

}
