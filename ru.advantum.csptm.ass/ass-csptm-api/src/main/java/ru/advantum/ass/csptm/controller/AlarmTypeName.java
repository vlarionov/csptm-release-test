package ru.advantum.ass.csptm.controller;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

@EqualsAndHashCode
@Data
@FieldDefaults(level= AccessLevel.PRIVATE)
@AllArgsConstructor
@ToString
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AlarmTypeName {
    @JsonProperty("iType")
    Short incidentType;
    @JsonProperty("n")
    String incidenName;
}
