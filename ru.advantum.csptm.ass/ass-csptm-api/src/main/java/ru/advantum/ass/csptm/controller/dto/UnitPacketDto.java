package ru.advantum.ass.csptm.controller.dto;

import lombok.Value;
import ru.advantum.csptm.jep.proto.UnitPacket;


@Value
public class UnitPacketDto {
    /**
     * Данные о телематике, идентичны TelematicPacket
     * за исключением:
     * - нет высоты
     * - вместо gpsTime используется eventTime
     * - координаты лежат в массиве
     */
     int v;
    long t;
    float[] l;
    int s;
    int h;

    public UnitPacketDto(UnitPacket unitPacket) {
        if (unitPacket.getTelematic() == null) {
            this.v = 0;
            this.t = 0;
            this.l = null;
            this.s = 0;
            this.h = 0;
        } else {
            this.v = unitPacket.getTelematic().isLocationValid() ? 1 : 0;
            this.t = unitPacket.getEventTime().getEpochSecond();
            this.l = new float[]{unitPacket.getTelematic().getLon(), unitPacket.getTelematic().getLat()};
            this.s = unitPacket.getTelematic().getSpeed();
            this.h = unitPacket.getTelematic().getHeading();
        }

    }


}
