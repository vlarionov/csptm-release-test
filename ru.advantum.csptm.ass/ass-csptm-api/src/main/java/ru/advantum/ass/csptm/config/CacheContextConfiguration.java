package ru.advantum.ass.csptm.config;


import com.google.common.cache.CacheBuilder;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
@EnableCaching
public class CacheContextConfiguration extends CachingConfigurerSupport {
    private static final long CACHE_DURATION_IN_SECONDS = 60;

    @Override
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager() {
            @Override
            protected Cache createConcurrentMapCache(final String name) {
                return new ConcurrentMapCache(
                        name,
                        CacheBuilder.newBuilder()
                                .expireAfterWrite(
                                        CACHE_DURATION_IN_SECONDS,
                                        TimeUnit.SECONDS
                                )
                                .build()
                                .asMap(),
                        true
                );
            }
        };
    }
}
