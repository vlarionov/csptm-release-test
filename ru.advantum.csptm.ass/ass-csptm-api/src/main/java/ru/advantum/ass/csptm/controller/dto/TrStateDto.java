package ru.advantum.ass.csptm.controller.dto;

import lombok.Data;
import ru.advantum.csptm.jep.proto.TrState;

import java.time.Instant;

@Data
public class TrStateDto {
    private final Instant lastUpdateInstant;
    private final UnitPacketDto packet;

    public TrStateDto(TrState trState) {
        this(trState.getStateUpdate(), new UnitPacketDto(trState.getPacket()));
    }

    public TrStateDto(Instant stateUpdate, UnitPacketDto packet) {
        this.lastUpdateInstant = stateUpdate;
        this.packet = packet;
    }
}
