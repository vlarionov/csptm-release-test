package ru.advantum.ass.csptm.config;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Protocol;

/**
 * @author kaganov
 */
@Configuration
@EnableTransactionManagement
@ConditionalOnMissingBean(JedisPool.class)
public class CsptmContextConfiguration {

    @Bean
    public JedisPool jedis(RedisProperties properties) {
        return new JedisPool(
                new GenericObjectPoolConfig(),
                properties.getHost(),
                properties.getPort(),
                Protocol.DEFAULT_TIMEOUT,
                properties.getPassword(),
                Protocol.DEFAULT_DATABASE
        );
    }
}
