package ru.advantum.ass.jofl.servletconf;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by kaganov on 20.12.16.
 */
@Configuration
@ComponentScan(basePackages = {"ru.advantum.ass.csptm"})
public class CsptmConfiguration {

}
