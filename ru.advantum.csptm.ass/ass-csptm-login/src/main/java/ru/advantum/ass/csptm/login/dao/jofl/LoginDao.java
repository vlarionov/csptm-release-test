package ru.advantum.ass.csptm.login.dao.jofl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.SQLErrorCodeSQLExceptionTranslator;
import org.springframework.stereotype.Repository;
import ru.advantum.ass.csptm.login.exception.AccountLockedException;
import ru.advantum.ass.csptm.login.exception.AccountNotFoundException;
import ru.advantum.ass.csptm.login.exception.IncorrectPasswordException;
import ru.advantum.ass.csptm.login.exception.LoginException;
import ru.advantum.ass.jofl.utils.JoflUtil;

import javax.sql.DataSource;
import java.sql.SQLException;

@Repository
public class LoginDao {
    private static final String KEY_CALL_AUTHORIZE = "AUTHORIZE";

    private final JdbcTemplate jdbcTemplate;

    private final JoflUtil joflUtil;

    @Autowired
    public LoginDao(DataSource dataSource, JoflUtil joflUtil) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.jdbcTemplate.setExceptionTranslator(new LoginErrorCodeExceptionTranslator());
        this.joflUtil = joflUtil;
    }

    /**
     * @return idJoflAccount
     */
    public long authenticate(String domain, String username, String password)
            throws AccountNotFoundException, IncorrectPasswordException, AccountLockedException {
        String sql = joflUtil.getSql(KEY_CALL_AUTHORIZE, domain);
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{username, password}, long.class);
        } catch (PermissionDeniedDataAccessException e) {
            if (e.getCause() instanceof LoginException) {
                throw (LoginException) e.getCause();
            }
            throw e;
        }
    }

    private static class LoginErrorCodeExceptionTranslator extends SQLErrorCodeSQLExceptionTranslator {
        @Override
        protected DataAccessException customTranslate(String task, String sql, SQLException sqlEx) {
            switch (sqlEx.getSQLState()) {
                case "20401":
                    return new PermissionDeniedDataAccessException("Account not found", new AccountNotFoundException());
                case "20402":
                    return new PermissionDeniedDataAccessException("Incorrect password", new IncorrectPasswordException());
                case "20423":
                    return new PermissionDeniedDataAccessException("Account locked", new AccountLockedException());
            }
            return null;
        }
    }

}
