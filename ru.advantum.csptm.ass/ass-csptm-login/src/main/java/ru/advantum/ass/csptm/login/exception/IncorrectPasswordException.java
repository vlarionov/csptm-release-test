package ru.advantum.ass.csptm.login.exception;

import ru.advantum.ass.csptm.login.model.csptm.LoginEventCode;

public class IncorrectPasswordException extends LoginException {
    @Override
    public LoginEventCode getLoginEventCode() {
        return LoginEventCode.INCORRECT_PASSWORD;
    }
}
