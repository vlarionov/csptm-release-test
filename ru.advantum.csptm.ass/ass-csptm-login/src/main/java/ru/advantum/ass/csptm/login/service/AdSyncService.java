package ru.advantum.ass.csptm.login.service;

import ru.advantum.ass.csptm.login.model.csptm.AdAccount;
import ru.advantum.ass.csptm.login.model.csptm.AdGroup;

import java.util.List;

public interface AdSyncService {
    void syncAdGroups();

    void syncAdGroup(Integer id);

    void syncAdGroups(List<AdGroup> adGroups);

    AdAccount syncAdAccount(Integer adInstanceId, String adUsername);
}
