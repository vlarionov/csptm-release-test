package ru.advantum.ass.csptm.login.model.ldap;

import java.time.LocalDateTime;
import java.util.List;

public class LdapPerson {
    private final String domain;

    private final String commonName;

    private final String firstName;

    private final String lastName;

    private final LocalDateTime whenCreated;

    private final LocalDateTime whenChanged;

    private final LocalDateTime accountExpires;

    private final String mail;

    private final String telephoneNumber;

    private final String secondTelephoneNumber;

    private final String objectGUID;

    private final String sAMAccountName;

    private final List<String> memberOf;

    public LdapPerson(String domain, String commonName, String firstName, String lastName, LocalDateTime whenCreated, LocalDateTime whenChanged, LocalDateTime accountExpires, String mail, String telephoneNumber, String secondTelephoneNumber, String objectGUID, String sAMAccountName, List<String> memberOf) {
        this.domain = domain;
        this.commonName = commonName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.whenCreated = whenCreated;
        this.whenChanged = whenChanged;
        this.accountExpires = accountExpires;
        this.mail = mail;
        this.telephoneNumber = telephoneNumber;
        this.secondTelephoneNumber = secondTelephoneNumber;
        this.objectGUID = objectGUID;
        this.sAMAccountName = sAMAccountName;
        this.memberOf = memberOf;
    }

    public String getDomain() {
        return domain;
    }

    public String getCommonName() {
        return commonName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDateTime getWhenCreated() {
        return whenCreated;
    }

    public LocalDateTime getWhenChanged() {
        return whenChanged;
    }

    public LocalDateTime getAccountExpires() {
        return accountExpires;
    }

    public String getMail() {
        return mail;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public String getSecondTelephoneNumber() {
        return secondTelephoneNumber;
    }

    public String getObjectGUID() {
        return objectGUID;
    }

    public String getsAMAccountName() {
        return sAMAccountName;
    }

    public List<String> getMemberOf() {
        return memberOf;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LdapPerson person = (LdapPerson) o;

        return objectGUID.equals(person.objectGUID);
    }

    @Override
    public int hashCode() {
        return objectGUID.hashCode();
    }
}