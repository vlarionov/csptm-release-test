package ru.advantum.ass.csptm.login.exception;

import ru.advantum.ass.csptm.login.model.csptm.LoginEventCode;

public abstract class LoginException extends RuntimeException {

    public LoginException() {
    }

    public LoginException(String message) {
        super(message);
    }

    public LoginException(String message, Throwable cause) {
        super(message, cause);
    }

    public LoginException(Throwable cause) {
        super(cause);
    }

    public LoginException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public abstract LoginEventCode getLoginEventCode();
}
