package ru.advantum.ass.csptm.login.dao.ldap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.CommunicationException;
import org.springframework.ldap.NameNotFoundException;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.filter.Filter;
import org.springframework.ldap.filter.HardcodedFilter;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.ldap.query.SearchScope;
import org.springframework.stereotype.Repository;
import ru.advantum.ass.csptm.login.dao.csptm.AdDao;
import ru.advantum.ass.csptm.login.exception.AdLoginException;
import ru.advantum.ass.csptm.login.model.csptm.AdInstance;
import ru.advantum.ass.csptm.login.model.ldap.LdapGroup;
import ru.advantum.ass.csptm.login.model.ldap.LdapPerson;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.ldap.query.LdapQueryBuilder.query;

@Repository
public class LdapDao {
    private static final String CN_ATTRIBUTE = "cn";
    private static final String DC_ATTRIBUTE = "DC";
    private static final String OBJECT_CLASS_ATTRIBUTE = "objectClass";
    private static final String OBJECT_GUID_ATTRIBUTE = "objectGUID";
    private static final String S_AM_ACCOUNT_NAME_ATTRIBUTE = "sAMAccountName";

    private static final Filter PERSON_OBJECT_CLASSES_FILTER = buildObjectClassesFilter("top", "user", "person", "organizationalPerson");
    private static final Filter GROUP_OBJECT_CLASSES_FILTER = buildObjectClassesFilter("top", "group");

    private static final String CONNECTION_TIMEOUT = "15000";

    private final AttributesMapper<LdapPerson> personAttributesMapper;
    private final AttributesMapper<LdapGroup> groupAttributesMapper;

    private final AdDao adDao;

    private final Map<Integer, LdapTemplate> ldapTemplateMap = new HashMap<>();

    @Autowired
    public LdapDao(
            AdDao adDao,
            AttributesMapper<LdapPerson> personAttributesMapper,
            AttributesMapper<LdapGroup> groupAttributesMapper
    ) {
        this.adDao = adDao;
        this.personAttributesMapper = personAttributesMapper;
        this.groupAttributesMapper = groupAttributesMapper;
    }

    @PostConstruct
    public void init() throws Exception {
        for (AdInstance adInstance : adDao.findAdInstances()) {
            LdapContextSource contextSource = new LdapContextSource();
            contextSource.setUrl(adInstance.getLdapUrl());
            contextSource.setUserDn(adInstance.getLdapUserDn());
            contextSource.setPassword(adInstance.getLdapPassword());
            Map<String, Object> baseEnvironmentProperties = new HashMap<>();
            baseEnvironmentProperties.put("java.naming.ldap.attributes.binary", OBJECT_GUID_ATTRIBUTE);
            baseEnvironmentProperties.put("com.sun.jndi.ldap.connect.timeout", CONNECTION_TIMEOUT);
            contextSource.setBaseEnvironmentProperties(baseEnvironmentProperties);
            contextSource.afterPropertiesSet();
            LdapTemplate ldapTemplate = new LdapTemplate(contextSource);
            ldapTemplate.setIgnorePartialResultException(true);
            ldapTemplate.afterPropertiesSet();
            ldapTemplateMap.put(adInstance.getAdInstanceId(), ldapTemplate);
        }
    }

    public LdapGroup findGroupByName(Integer adInstanceId, String domain, String name) {
        Filter filter = new AndFilter()
                .and(GROUP_OBJECT_CLASSES_FILTER)
                .and(new EqualsFilter(CN_ATTRIBUTE, name));

        LdapQuery query = query()
                .searchScope(SearchScope.SUBTREE)
                .base(buildBaseDn(domain))
                .filter(filter);
        try {
            return ldapTemplateMap.get(adInstanceId).search(query, groupAttributesMapper).stream().findAny().orElse(null);
        } catch (NameNotFoundException e) {
            return null;
        }
    }

    public LdapGroup findGroupByGUID(Integer adInstanceId, String domain, String guid) {
        Filter filter = new AndFilter()
                .and(GROUP_OBJECT_CLASSES_FILTER)
                .and(new HardcodedFilter(String.format("(%s=%s)", OBJECT_GUID_ATTRIBUTE, guid)));

        LdapQuery query = query()
                .searchScope(SearchScope.SUBTREE)
                .base(buildBaseDn(domain))
                .filter(filter);
        try {
            return ldapTemplateMap.get(adInstanceId).search(query, groupAttributesMapper).stream().findAny().orElse(null);
        } catch (NameNotFoundException e) {
            return null;
        }
    }

    public List<LdapGroup> findGroups(Integer adInstanceId, List<String> distinguishedNames) {
        if (distinguishedNames.isEmpty()) {
            return Collections.emptyList();
        }
        return distinguishedNames
                .stream()
                .map(distinguishedName -> ldapTemplateMap.get(adInstanceId).lookup(distinguishedName, groupAttributesMapper))
                .collect(Collectors.toList());
    }


    /**
     * @param username like username@domain.com
     */
    public LdapPerson findPersonByUsername(Integer adInstanceId, String username) {
        String[] usernameAndDomain = username.split("@");
        String simpleUsername = usernameAndDomain[0];
        String domain = usernameAndDomain[1];

        Filter filter = new AndFilter()
                .and(PERSON_OBJECT_CLASSES_FILTER)
                .and(new EqualsFilter(S_AM_ACCOUNT_NAME_ATTRIBUTE, simpleUsername));

        LdapQuery query = query()
                .searchScope(SearchScope.SUBTREE)
                .base(buildBaseDn(domain))
                .filter(filter);
        try {
            return ldapTemplateMap.get(adInstanceId).search(query, personAttributesMapper).stream().findAny().orElse(null);
        } catch (NameNotFoundException e) {
            return null;
        }
    }


    public List<LdapPerson> findPersons(Integer adInstanceId, List<String> distinguishedNames) {
        if (distinguishedNames.isEmpty()) {
            return Collections.emptyList();
        }
        return distinguishedNames
                .stream()
                .map(distinguishedName -> ldapTemplateMap.get(adInstanceId).lookup(distinguishedName, personAttributesMapper))
                .collect(Collectors.toList());
    }


    private String buildBaseDn(String domain) {
        return Arrays.stream(domain.split("\\."))
                .map(String::toLowerCase)
                .map(dc -> DC_ATTRIBUTE + "=" + dc)
                .collect(Collectors.joining(","));
    }

    private static Filter buildObjectClassesFilter(String... objectClasses) {
        return Stream.of(objectClasses)
                .map(objectClass -> new EqualsFilter(OBJECT_CLASS_ATTRIBUTE, objectClass))
                .collect(AndFilter::new, AndFilter::and, AndFilter::and);
    }

    /**
     * @return adInstanceId
     */
    public int findAuthorizationAdInstance(String domain, String username, String password) {
        String baseDn = buildBaseDn(domain);
        String filter = new EqualsFilter(S_AM_ACCOUNT_NAME_ATTRIBUTE, username).encode();
        for (Map.Entry<Integer, LdapTemplate> ldapTemplateEntry : ldapTemplateMap.entrySet()) {
            try {
                if (ldapTemplateEntry.getValue().authenticate(baseDn, filter, password)) {
                    return ldapTemplateEntry.getKey();
                }
            } catch (CommunicationException ignored) {
            }
        }
        throw new AdLoginException();
    }

}
