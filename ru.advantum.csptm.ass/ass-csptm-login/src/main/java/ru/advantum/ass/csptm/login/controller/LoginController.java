package ru.advantum.ass.csptm.login.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.web.bind.annotation.*;
import ru.advantum.ass.csptm.login.dao.jofl.LoginDao;
import ru.advantum.ass.csptm.login.dao.ldap.LdapDao;
import ru.advantum.ass.csptm.login.exception.*;
import ru.advantum.ass.csptm.login.model.csptm.AdAccount;
import ru.advantum.ass.csptm.login.model.csptm.LoginEvent;
import ru.advantum.ass.csptm.login.service.AdSyncService;
import ru.advantum.ass.csptm.login.service.CrossRealmKrbTicketValidator;
import ru.advantum.ass.jofl.session.JoflSession;
import ru.advantum.ass.jofl.utils.JoflUtil;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RestController
@RequestMapping(method = {RequestMethod.GET, RequestMethod.POST})
public class LoginController {
    public static final String DO_LOGIN_PATH = "/jofl.doLogin.aws";
    public static final String DO_SSO_LOGIN_PATH = "/jofl.doSSOLogin.aws";
    public static final String DO_AD_LOGIN_PATH = "/jofl.doADLogin.aws";

    private final JoflSession joflSession;

    private final LdapDao ldapDao;

    private final LoginDao loginDao;

    private final CrossRealmKrbTicketValidator krbTicketValidator;

    private final AdSyncService adAccountService;

    private final LoginEventHolder loginEventHolder;

    @Autowired
    public LoginController(
            JoflSession joflSession,
            LoginDao loginDao,
            CrossRealmKrbTicketValidator krbTicketValidator,
            AdSyncService adAccountService,
            LdapDao ldapDao,
            LoginEventHolder loginEventHolder
    ) {
        this.joflSession = joflSession;
        this.loginDao = loginDao;
        this.krbTicketValidator = krbTicketValidator;
        this.adAccountService = adAccountService;
        this.ldapDao = ldapDao;
        this.loginEventHolder = loginEventHolder;
    }

    @RequestMapping("/jofl.doCheckSession.aws")
    public String doCheckSession(Locale locale) {
        if (joflSession.getIdJOFLAccount() == null) {
            throw new SessionNotExistsException();
        }
        return locale.toString();//todo
    }

    /**
     * Вход по внутреннему аккаунту
     *
     * @param passwdHash только hash, без SEED
     */
    @RequestMapping(DO_LOGIN_PATH)
    public void doLogin(
            @RequestParam("userName") String userName,
            @RequestParam("passwdHash") String passwdHash,
            HttpServletRequest request
    ) {
        if (joflSession.getIdJOFLAccount() != null) {
            throw new SessionAlreadyExistsException();
        }

        loginEventHolder.getLoginEvent().setEnteredLogin(userName);

        if (userName == null || userName.isEmpty() || passwdHash == null) {
            throw new AccountNotFoundException();
        }

        long idJoflAccount = loginDao.authenticate(JoflUtil.getDomain(request), userName, passwdHash);

        loginEventHolder.getLoginEvent().setIdJoflAccount(idJoflAccount);

        joflSession.setIdJOFLAccount(idJoflAccount);
        joflSession.setAccountName(userName);
    }

    /**
     * Вход по Kerberos
     */
    @RequestMapping(DO_SSO_LOGIN_PATH)
    public void doSSOLogin(
            @RequestHeader(value = AUTHORIZATION, required = false) String authorizationHeader
    ) {
        if (joflSession.getIdJOFLAccount() != null) {
            throw new SessionAlreadyExistsException();
        }

        if (authorizationHeader == null) {
            throw new SpnegoHeaderMissingException();
        }

        CrossRealmKrbTicketValidator.LocalKrbTicketValidation ticketValidation;
        try {
            byte[] base64Token = authorizationHeader.substring(authorizationHeader.indexOf(" ") + 1).getBytes("UTF-8");
            byte[] kerberosTicket = Base64.decode(base64Token);
            ticketValidation = krbTicketValidator.validate(kerberosTicket);
        } catch (Exception e) {
            throw new SSOLoginException();
        }

        String username = ticketValidation.getKerberosTicketValidation().username();
        loginEventHolder.getLoginEvent().setEnteredLogin(username);

        authorizeAdAccount(ticketValidation.getAdInstanceId(), username);
    }

    /**
     * Вход по AD логину не из домена
     */
    @RequestMapping(DO_AD_LOGIN_PATH)
    public void doADLogin(
            @RequestParam("userName") String userName,
            @RequestParam("passwd") String passwd
    ) {
        if (joflSession.getIdJOFLAccount() != null) {
            throw new SessionAlreadyExistsException();
        }

        LoginEvent loginEvent = loginEventHolder.getLoginEvent();
        loginEvent.setEnteredLogin(userName);

        String[] usernameAndDomain = userName.split("@");
        if (usernameAndDomain.length != 2) {
            throw new AdLoginException();
        }

        int adInstanceId = ldapDao.findAuthorizationAdInstance(usernameAndDomain[1], usernameAndDomain[0], passwd);

        authorizeAdAccount(adInstanceId, userName);
    }

    private void authorizeAdAccount(int adInstanceId, String username) {
        //todo make auth like loginDao.authenticate?
        AdAccount adAccount = adAccountService.syncAdAccount(adInstanceId, username);

        if (adAccount == null) {
            throw new AccountNotFoundException();
        }

        if (adAccount.getLocked()) {
            throw new AccountLockedException();
        }

        joflSession.setIdJOFLAccount(adAccount.getJoflAccountId());
        joflSession.setAccountName(adAccount.getAdName());

        loginEventHolder.getLoginEvent().setIdJoflAccount(joflSession.getIdJOFLAccount());
    }

}
