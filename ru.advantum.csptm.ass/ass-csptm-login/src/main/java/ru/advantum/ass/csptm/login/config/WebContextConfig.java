package ru.advantum.ass.csptm.login.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import ru.advantum.ass.csptm.login.controller.LoginController;
import ru.advantum.ass.csptm.login.controller.LoginEventHandler;

@Configuration
public class WebContextConfig extends WebMvcConfigurerAdapter {

    private final LoginEventHandler loginEventHandler;

    public WebContextConfig(LoginEventHandler loginEventHandler) {
        this.loginEventHandler = loginEventHandler;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginEventHandler).addPathPatterns(
                LoginController.DO_LOGIN_PATH,
                LoginController.DO_SSO_LOGIN_PATH,
                LoginController.DO_AD_LOGIN_PATH
        );
    }
}
