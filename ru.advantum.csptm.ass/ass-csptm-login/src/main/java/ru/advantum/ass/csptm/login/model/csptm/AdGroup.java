package ru.advantum.ass.csptm.login.model.csptm;

public class AdGroup  {
    private Integer groupId;

    private Integer adInstanceId;

    private String adDomain;

    private String adName;

    private String adGUID;

    public AdGroup(Integer groupId, Integer adInstanceId, String adDomain, String adName, String adGUID) {
        this.groupId = groupId;
        this.adInstanceId = adInstanceId;
        this.adDomain = adDomain;
        this.adName = adName;
        this.adGUID = adGUID;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getAdInstanceId() {
        return adInstanceId;
    }

    public void setAdInstanceId(Integer adInstanceId) {
        this.adInstanceId = adInstanceId;
    }

    public String getAdDomain() {
        return adDomain;
    }

    public void setAdDomain(String adDomain) {
        this.adDomain = adDomain;
    }

    public String getAdName() {
        return adName;
    }

    public void setAdName(String adName) {
        this.adName = adName;
    }

    public String getAdGUID() {
        return adGUID;
    }

    public void setAdGUID(String adGUID) {
        this.adGUID = adGUID;
    }
}
