package ru.advantum.ass.csptm.login.service;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.kerberos.authentication.KerberosTicketValidation;
import org.springframework.security.kerberos.authentication.KerberosTicketValidator;
import org.springframework.security.kerberos.authentication.sun.SunJaasKerberosTicketValidator;
import org.springframework.stereotype.Component;
import ru.advantum.ass.csptm.login.dao.csptm.AdDao;
import ru.advantum.ass.csptm.login.model.csptm.AdInstance;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Component
public class CrossRealmKrbTicketValidator implements InitializingBean {
    private final AdDao adDao;

    private final List<LocalKrbTicketValidator> localKrbTicketValidators = new ArrayList<>();

    @Autowired
    public CrossRealmKrbTicketValidator(AdDao adDao) {
        this.adDao = adDao;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        File javaSecurityLoginConfigFile = Files.createTempFile(null, null).toFile();
        Properties javaSecurityLoginProperties = new Properties();
        javaSecurityLoginProperties.setProperty("refreshKrb5Config", String.valueOf(true));
        try(FileOutputStream outputStream = new FileOutputStream(javaSecurityLoginConfigFile)){
            javaSecurityLoginProperties.store(outputStream, null);
        }
        System.setProperty("java.security.auth.login.config", javaSecurityLoginConfigFile.getAbsolutePath());

        for (AdInstance adInstance : adDao.findAdInstances()) {
            localKrbTicketValidators.add(new LocalKrbTicketValidator(adInstance));
        }
    }

    public synchronized LocalKrbTicketValidation validate(byte[] token) {
        for (LocalKrbTicketValidator localKrbTicketValidator : localKrbTicketValidators) {
            AdInstance properties = localKrbTicketValidator.adInstanceProperties;
            System.setProperty("java.security.krb5.realm", properties.getKrbRealm());
            System.setProperty("java.security.krb5.kdc", properties.getKrbKdc());
            System.setProperty("java.security.krb5.conf", properties.getKrbConfPath());
            System.setProperty("java.security.krb5.debug", String.valueOf(properties.isKrbDebug()));

            try {
                return new LocalKrbTicketValidation(
                        localKrbTicketValidator.adInstanceProperties.getAdInstanceId(),
                        localKrbTicketValidator.validator.validateTicket(token)
                );
            } catch (Exception ignored) {
            }
        }
        throw new BadCredentialsException("Kerberos validation not successful");
    }

    public static class LocalKrbTicketValidation {
        private final Integer adInstanceId;

        private final KerberosTicketValidation kerberosTicketValidation;

        public LocalKrbTicketValidation(Integer adInstanceId, KerberosTicketValidation kerberosTicketValidation) {
            this.adInstanceId = adInstanceId;
            this.kerberosTicketValidation = kerberosTicketValidation;
        }

        public Integer getAdInstanceId() {
            return adInstanceId;
        }

        public KerberosTicketValidation getKerberosTicketValidation() {
            return kerberosTicketValidation;
        }
    }

    private static class LocalKrbTicketValidator {
        private final AdInstance adInstanceProperties;

        private final KerberosTicketValidator validator;

        private LocalKrbTicketValidator(AdInstance adInstanceProperties) throws Exception {
            this.adInstanceProperties = adInstanceProperties;

            SunJaasKerberosTicketValidator ticketValidator = new SunJaasKerberosTicketValidator();
            ticketValidator.setServicePrincipal(adInstanceProperties.getKrbPrincipal());
            ticketValidator.setKeyTabLocation(new FileSystemResource(adInstanceProperties.getKrbKeytabPath()));
            ticketValidator.setDebug(adInstanceProperties.isKrbDebug());
            ticketValidator.afterPropertiesSet();
            validator = ticketValidator;
        }
    }
}
