package ru.advantum.ass.csptm.login.exception;

import ru.advantum.ass.csptm.login.model.csptm.LoginEventCode;

public class SSOLoginException extends LoginException {
    @Override
    public LoginEventCode getLoginEventCode() {
        return LoginEventCode.SSO_FAIL;
    }
}
