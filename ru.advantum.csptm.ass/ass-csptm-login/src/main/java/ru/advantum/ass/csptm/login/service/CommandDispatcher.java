package ru.advantum.ass.csptm.login.service;

import java.nio.charset.StandardCharsets;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommandDispatcher {

    private static final Logger LOG = LoggerFactory.getLogger(CommandDispatcher.class);

    @Data
    private static class AdSyncMessage{
        @SerializedName("group_id")
        private Integer groupId;
    }

    private final AdSyncService adSyncService;

    @Autowired
    public CommandDispatcher(AdSyncService adSyncService) {
        this.adSyncService = adSyncService;
    }

    @RabbitListener(queues = "ad-sync", exclusive = true)
    public void receiveMessage(Message message) {
        try {
            String s = new String(message.getBody(), StandardCharsets.UTF_8);

            LOG.info("Received msg from queue ad-sync. {}.", s);

            Gson gson = new Gson();
            AdSyncMessage msg = gson.fromJson(s, AdSyncMessage.class);
            if (msg == null) {
                LOG.error("Failed to parse message \"{}\"", s);
                return;
            }
            if (msg.getGroupId() != null) {
                adSyncService.syncAdGroup(msg.getGroupId());
            } else {
                adSyncService.syncAdGroups();
            }
        }catch(Exception e){
            LOG.error("Failed to process message. {}", message, e);
        }
    }

}
