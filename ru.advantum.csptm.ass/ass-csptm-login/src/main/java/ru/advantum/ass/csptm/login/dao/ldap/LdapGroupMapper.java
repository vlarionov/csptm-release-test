package ru.advantum.ass.csptm.login.dao.ldap;

import org.springframework.stereotype.Component;
import ru.advantum.ass.csptm.login.model.ldap.LdapGroup;

import javax.naming.NamingException;

@Component
public class LdapGroupMapper extends AbstractLdapMapper<LdapGroup> {
    @Override
    protected LdapGroup mapFromAttributes(AttributesWrapper attributes) throws NamingException {
        return new LdapGroup(
                attributes.getDomain(),
                attributes.getString("cn"),
                attributes.getObjectGUID(),
                attributes.getStringList("member")
        );
    }
}
