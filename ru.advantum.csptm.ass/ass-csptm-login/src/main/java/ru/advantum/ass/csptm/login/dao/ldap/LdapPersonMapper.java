package ru.advantum.ass.csptm.login.dao.ldap;

import org.springframework.stereotype.Component;
import ru.advantum.ass.csptm.login.model.ldap.LdapPerson;

import javax.naming.NamingException;

@Component
public class LdapPersonMapper extends AbstractLdapMapper<LdapPerson> {

    @Override
    protected LdapPerson mapFromAttributes(AbstractLdapMapper.AttributesWrapper attributes) throws NamingException {
        return new LdapPerson(
                attributes.getDomain(),
                attributes.getString("cn"),
                attributes.getString("givenName"),
                attributes.getString("sn"),
                attributes.getLocalDateTime("whenCreated"),
                attributes.getLocalDateTime("whenChanged"),
                attributes.getAccountExpires(),
                attributes.getString("mail"),
                attributes.getString("telephoneNumber"),
                attributes.getStringList("otherTelephone").stream().findFirst().orElse(null),
                attributes.getObjectGUID(),
                attributes.getString("sAMAccountName"),
                attributes.getStringList("memberOf")
        );
    }


}
