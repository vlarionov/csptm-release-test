package ru.advantum.ass.csptm.login.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "login")
public class LoginProperties {
    /**
     * Максимальный интервал неактивности сессии в секундах, после которого она инвалидируется
     */
    public int sessionMaxInactiveInterval = 3600;

}
