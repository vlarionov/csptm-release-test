package ru.advantum.ass.jofl.servletconf;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "ru.advantum.ass.csptm.login")
public class LoginServletContextConfig {
}
