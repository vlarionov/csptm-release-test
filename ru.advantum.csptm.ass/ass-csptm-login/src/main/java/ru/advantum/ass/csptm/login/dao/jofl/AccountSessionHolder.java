package ru.advantum.ass.csptm.login.dao.jofl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpSession;
import lombok.val;
import org.springframework.context.ApplicationListener;
import org.springframework.security.web.session.HttpSessionDestroyedEvent;
import org.springframework.stereotype.Component;
import ru.advantum.ass.csptm.login.dao.csptm.AuthEventDao;
import ru.advantum.ass.csptm.login.model.csptm.LogoutEventCode;


@Component
public class AccountSessionHolder implements ApplicationListener<HttpSessionDestroyedEvent> {

    private HashMap<Long, List<HttpSession>> idJoflAccountMap = new HashMap<>();
    private final AuthEventDao authEventDao;

    public HashMap<Long, List<HttpSession>> getIdJoflAccountMap() {
        return idJoflAccountMap;
    }

    public AccountSessionHolder(AuthEventDao authEventDao) {
        this.authEventDao = authEventDao;
    }

    public void addAccountSession(Long idJoflAccount, HttpSession httpSession) {
        idJoflAccountMap.computeIfAbsent(idJoflAccount, key -> new ArrayList<>()).add(httpSession);
    }

    public void invalidateSession(Long idJoflAccount) {
        val sessions = idJoflAccountMap.remove(idJoflAccount);
        if (sessions == null) {
            return;
        }
        sessions.forEach(HttpSession::invalidate);
    }

    @Override
    public void onApplicationEvent(HttpSessionDestroyedEvent event) {
        long expirationTime = event.getSession().getLastAccessedTime() + event.getSession().getMaxInactiveInterval() * 1000;
        long now = System.currentTimeMillis();

        LogoutEventCode code = LogoutEventCode.MANUALLY;
        if (expirationTime < now) {
            code = LogoutEventCode.SESSION_EXPIRED;
        }
        authEventDao.closeSession(event.getId(), code);

        int index=-1;
        if(idJoflAccountMap.size()!=0)
        for (Long idJoflAccount :idJoflAccountMap.keySet()){
            for (int i = 0; i < idJoflAccountMap.get(idJoflAccount).size(); i++) {
                if(idJoflAccountMap.get(idJoflAccount).get(i).getId().equals(event.getId())){
                    index=i;
                }
                if(index!=-1) {
                    idJoflAccountMap.get(idJoflAccount).remove(index);
                }
                index=-1;
            }
        }
    }
}
