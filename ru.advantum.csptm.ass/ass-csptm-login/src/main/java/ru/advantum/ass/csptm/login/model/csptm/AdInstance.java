package ru.advantum.ass.csptm.login.model.csptm;

public class AdInstance {
    private Integer adInstanceId;

    private String ldapUrl;

    private String ldapUserDn;

    private String ldapPassword;

    private String krbRealm;

    private String krbKdc;

    private String krbPrincipal;

    private String krbConfPath;

    private String krbKeytabPath;

    private boolean krbDebug;

    public AdInstance() {
    }

    public AdInstance(Integer adInstanceId, String ldapUrl, String ldapUserDn, String ldapPassword, String krbRealm, String krbKdc, String krbPrincipal, String krbConfPath, String krbKeytabPath, boolean krbDebug) {
        this.adInstanceId = adInstanceId;
        this.ldapUrl = ldapUrl;
        this.ldapUserDn = ldapUserDn;
        this.ldapPassword = ldapPassword;
        this.krbRealm = krbRealm;
        this.krbKdc = krbKdc;
        this.krbPrincipal = krbPrincipal;
        this.krbConfPath = krbConfPath;
        this.krbKeytabPath = krbKeytabPath;
        this.krbDebug = krbDebug;
    }

    public Integer getAdInstanceId() {
        return adInstanceId;
    }

    public void setAdInstanceId(Integer adInstanceId) {
        this.adInstanceId = adInstanceId;
    }

    public String getLdapUrl() {
        return ldapUrl;
    }

    public void setLdapUrl(String ldapUrl) {
        this.ldapUrl = ldapUrl;
    }

    public String getLdapUserDn() {
        return ldapUserDn;
    }

    public void setLdapUserDn(String ldapUserDn) {
        this.ldapUserDn = ldapUserDn;
    }

    public String getLdapPassword() {
        return ldapPassword;
    }

    public void setLdapPassword(String ldapPassword) {
        this.ldapPassword = ldapPassword;
    }

    public String getKrbRealm() {
        return krbRealm;
    }

    public void setKrbRealm(String krbRealm) {
        this.krbRealm = krbRealm;
    }

    public String getKrbKdc() {
        return krbKdc;
    }

    public void setKrbKdc(String krbKdc) {
        this.krbKdc = krbKdc;
    }

    public String getKrbPrincipal() {
        return krbPrincipal;
    }

    public void setKrbPrincipal(String krbPrincipal) {
        this.krbPrincipal = krbPrincipal;
    }

    public String getKrbConfPath() {
        return krbConfPath;
    }

    public void setKrbConfPath(String krbConfPath) {
        this.krbConfPath = krbConfPath;
    }

    public String getKrbKeytabPath() {
        return krbKeytabPath;
    }

    public void setKrbKeytabPath(String krbKeytabPath) {
        this.krbKeytabPath = krbKeytabPath;
    }

    public boolean isKrbDebug() {
        return krbDebug;
    }

    public void setKrbDebug(boolean krbDebug) {
        this.krbDebug = krbDebug;
    }
}