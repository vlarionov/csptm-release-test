package ru.advantum.ass.csptm.login.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.kerberos.web.authentication.SpnegoEntryPoint;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.advantum.ass.csptm.login.exception.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class LoginExceptionHandler {
    private final LoginEventHolder loginEventHolder;

    private final SpnegoEntryPoint spnegoEntryPoint;

    @Autowired
    public LoginExceptionHandler(LoginEventHolder loginEventHolder, SpnegoEntryPoint spnegoEntryPoint) {
        this.loginEventHolder = loginEventHolder;
        this.spnegoEntryPoint = spnegoEntryPoint;
    }

    @ExceptionHandler(SpnegoHeaderMissingException.class)
    public void handleSpnegoHeaderMissingException(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        spnegoEntryPoint.commence(request, response, null);
    }

    @ExceptionHandler(AccountLockedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public void handleAccountLockedException(AccountLockedException e) {
        loginEventHolder.getLoginEvent().setResultCode(e.getLoginEventCode());
    }

    @ExceptionHandler(SessionNotExistsException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public void handleSessionNotExistsException() {
    }

    @ExceptionHandler(SessionAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.OK)
    public void handleSessionAlreadyExistsException() {
    }

    @ExceptionHandler(LoginException.class)
    public ResponseEntity<String> handleLoginException(LoginException e) {
        loginEventHolder.getLoginEvent().setResultCode(e.getLoginEventCode());
        return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
    }
}
