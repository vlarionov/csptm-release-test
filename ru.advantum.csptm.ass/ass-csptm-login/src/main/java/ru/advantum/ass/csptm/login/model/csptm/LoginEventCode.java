package ru.advantum.ass.csptm.login.model.csptm;

public enum LoginEventCode {
    SUCCESS,
    ACCOUNT_NOT_FOUND,
    INCORRECT_PASSWORD,
    ACCOUNT_LOCKED,
    SSO_FAIL,
    AD_FAIL
}
