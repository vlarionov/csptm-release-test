package ru.advantum.ass.csptm.login.dao.ldap;

import org.springframework.ldap.core.AttributesMapper;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class AbstractLdapMapper<T> implements AttributesMapper<T> {
    @Override
    public T mapFromAttributes(Attributes attributes) throws NamingException {
        return mapFromAttributes(new AttributesWrapper(attributes));
    }

    protected abstract T mapFromAttributes(AttributesWrapper attributes) throws NamingException;

    protected static class AttributesWrapper {
        private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

        private final Attributes attributes;

        private AttributesWrapper(Attributes attributes) {
            this.attributes = attributes;
        }

        public String getString(String attributeName) throws NamingException {
            Attribute attribute = attributes.get(attributeName);
            if (attribute == null) {
                return null;
            }
            return attribute.get().toString();
        }

        public List<String> getStringList(String attributeName) throws NamingException {
            Attribute attribute = attributes.get(attributeName);
            if (attribute == null) {
                return Collections.emptyList();
            }
            List<String> result = new ArrayList<>(attribute.size());
            for (int i = 0; i < attribute.size(); i++) {
                result.add(attribute.get(i).toString());
            }
            return result;
        }

        /**
         * @param attributeName - value of attribute like "20161004085554.0Z"
         */
        public LocalDateTime getLocalDateTime(String attributeName) throws NamingException {
            String value = getString(attributeName);
            if (value == null) {
                return null;
            }
            //ldap always uses UTC zone
            String valueWithoutZone = value.substring(0, value.indexOf("."));
            return LocalDateTime.parse(valueWithoutZone, DATE_TIME_FORMATTER);
        }

        public String getDomain() throws NamingException {
            String distinguishedName = getString("distinguishedname");
            if (distinguishedName == null) {
                return null;
            }
            StringBuilder domain = new StringBuilder();
            String[] dnComponents = distinguishedName.split(",");
            for (int i = dnComponents.length - 1; i >= 0; i--) {
                String[] dnComponent = dnComponents[i].split("=");
                String dnComponentName = dnComponent[0];
                String dnComponentValue = dnComponent[1];
                if (!"DC".equals(dnComponentName)) {
                    return domain.toString();
                }
                if (i != dnComponents.length - 1) {
                    domain.insert(0, ".");
                }
                domain.insert(0, dnComponentValue);
            }
            return domain.toString();
        }


        public LocalDateTime getAccountExpires() throws NamingException {
            /*
                Value represents the number of 100 nanosecond intervals since January 1, 1601 (UTC).
                A value of 0 or 0x7FFFFFFFFFFFFFFF (9223372036854775807) indicates that the account never expires.
            */
            String stringValue = getString("accountExpires");
            if (stringValue == null || "0".equals(stringValue) || "9223372036854775807".equals(stringValue)) {
                return null;
            }

            LocalDateTime startTime = LocalDateTime.of(1601, 1, 1, 0, 0);
            long durationInMillis = Long.valueOf(stringValue) / 10_000L;

            return startTime.plus(durationInMillis, ChronoUnit.MILLIS);
        }

        public String getObjectGUID() throws NamingException {
            byte[] encodedBytes = (byte[]) attributes.get("objectGUID").get();
            if (encodedBytes == null) {
                return null;
            }
            StringBuilder decodedObjectGUID = new StringBuilder();
            for (byte anObjectGUID : encodedBytes) {
                String transformed = prefixZeros((int) anObjectGUID & 0xFF);
                decodedObjectGUID.append("\\");
                decodedObjectGUID.append(transformed);
            }
            return decodedObjectGUID.toString();
        }

        private static String prefixZeros(int value) {
            if (value <= 0xF) {
                return "0" + Integer.toHexString(value);
            } else {
                return Integer.toHexString(value);
            }
        }
    }


}
