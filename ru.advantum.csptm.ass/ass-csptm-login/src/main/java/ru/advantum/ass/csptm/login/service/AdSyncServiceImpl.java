package ru.advantum.ass.csptm.login.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.CommunicationException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.advantum.ass.csptm.login.dao.csptm.AdDao;
import ru.advantum.ass.csptm.login.dao.ldap.LdapDao;
import ru.advantum.ass.csptm.login.model.csptm.AdAccount;
import ru.advantum.ass.csptm.login.model.csptm.AdGroup;
import ru.advantum.ass.csptm.login.model.ldap.LdapGroup;
import ru.advantum.ass.csptm.login.model.ldap.LdapPerson;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AdSyncServiceImpl implements AdSyncService {
    private static final long SYNC_AD_GROUPS_RATE = 600_000L;

    private static final Logger LOG = LoggerFactory.getLogger(AdSyncService.class);

    private final AdDao adDao;

    private final LdapDao ldapDao;

    @Autowired
    public AdSyncServiceImpl(AdDao adDao, LdapDao ldapDao) {
        this.adDao = adDao;
        this.ldapDao = ldapDao;
    }

    @Scheduled(fixedRate = SYNC_AD_GROUPS_RATE)
    @Override
    public void syncAdGroups() {
        try {
            LocalDateTime startExecutionTime = LocalDateTime.now();

            List<AdGroup> adGroups = adDao.findAdGroups();
            syncAdGroups(adGroups);

            long durationInSeconds = ChronoUnit.SECONDS.between(
                    startExecutionTime,
                    LocalDateTime.now()
            );
            LOG.info("{} groups sync within {} seconds", adGroups.size(), durationInSeconds);
        } catch (Exception e) {
            LOG.error("Group sync with AD error", e);
        }
    }

    @Override
    public void syncAdGroup(Integer id) {
        try {
            List<AdGroup> adGroups = adDao.findAdGroups().stream().filter(g -> g.getGroupId().equals(id)).collect(
                    Collectors.toList());
            syncAdGroups(adGroups);
            LOG.info("group with id={} synced", id);

        } catch (Exception e) {
            LOG.error("Group sync with AD failed, id={}",id, e);
        }
    }

    @Override
    public void syncAdGroups(List<AdGroup> adGroups) {
        List<AdGroup> adGroupsFromLdap = new ArrayList<>();
        Set<AdAccount> adAccountsFromLdap = new HashSet<>();
        Map<AdGroup, List<AdAccount>> adGroupAccountsMap = new HashMap<>();

        for (AdGroup adGroup : adGroups) {
            try {
                LdapGroup ldapGroup = findLdapGroup(adGroup);

                if (ldapGroup != null) {
                    adGroup = toAdGroup  (adGroup.getAdInstanceId(), ldapGroup);
                } else {
                    adGroup.setAdGUID(null);
                }

                List<LdapPerson> ldapGroupPersons;
                if (ldapGroup == null) {
                    ldapGroupPersons = Collections.emptyList();
                } else {
                    ldapGroupPersons = ldapDao.findPersons(adGroup.getAdInstanceId(), ldapGroup.getMembers());
                }

                final AdGroup finalAdGroup = adGroup;
                List<AdAccount> groupAdAccountsFromLdap = ldapGroupPersons.stream().map(ldapPerson -> toAdAccount(finalAdGroup.getAdInstanceId(), ldapPerson)).collect(Collectors.toList());

                adGroupsFromLdap.add(adGroup);
                adAccountsFromLdap.addAll(groupAdAccountsFromLdap);
                adGroupAccountsMap.put(adGroup, groupAdAccountsFromLdap);
            } catch (CommunicationException e) {
                LOG.warn("Communication error on sync AD group, id:{}. {}", adGroup.getGroupId(), e.getMessage());
            } catch (Exception e) {
                LOG.error("Error on sync AD group, id: " + adGroup.getGroupId(), e);
            }
        }

        adDao.updateGroups(adGroupsFromLdap, adAccountsFromLdap, adGroupAccountsMap);
    }

    @Override
    public AdAccount syncAdAccount(Integer adInstanceId, String adUsername) {
        LdapPerson ldapPerson = ldapDao.findPersonByUsername(adInstanceId, adUsername);
        if (ldapPerson == null) {
            return null;
        }
        List<LdapGroup> ldapGroups = ldapDao.findGroups(adInstanceId, ldapPerson.getMemberOf());

        AdAccount adAccount = toAdAccount(adInstanceId, ldapPerson);
        List<AdGroup> adGroups = ldapGroups.stream().map(ldapGroup -> toAdGroup(adInstanceId, ldapGroup)).collect(Collectors.toList());

        return adDao.updateAccount(adAccount, adGroups);
    }

    private LdapGroup findLdapGroup(AdGroup adGroup) {
        if (adGroup.getAdGUID() == null) {
            return ldapDao.findGroupByName(adGroup.getAdInstanceId(), adGroup.getAdDomain(), adGroup.getAdName());
        } else {
            return ldapDao.findGroupByGUID(adGroup.getAdInstanceId(), adGroup.getAdDomain(), adGroup.getAdGUID());
        }
    }

    private AdGroup toAdGroup(Integer adInstanceId, LdapGroup ldapGroup) {
        return new AdGroup(
                null,
                adInstanceId,
                ldapGroup.getDomain(),
                ldapGroup.getCommonName(),
                ldapGroup.getObjectGUID()
        );
    }

    private AdAccount toAdAccount(Integer adInstanceId, LdapPerson ldapPerson) {
        return new AdAccount(
                null,
                null,
                adInstanceId,
                ldapPerson.getDomain(),
                ldapPerson.getCommonName(),
                ldapPerson.getObjectGUID(),
                ldapPerson.getFirstName(),
                ldapPerson.getLastName() != null ? ldapPerson.getLastName() : "",
                ldapPerson.getWhenCreated(),
                ldapPerson.getWhenChanged(),
                ldapPerson.getAccountExpires(),
                ldapPerson.getAccountExpires() != null && ldapPerson.getAccountExpires().isAfter(LocalDateTime.now()),
                ldapPerson.getMail(),
                ldapPerson.getTelephoneNumber(),
                ldapPerson.getSecondTelephoneNumber(),
                ldapPerson.getsAMAccountName()
        );
    }

}
