package ru.advantum.ass.csptm.login.model.ldap;

import java.util.List;

public class LdapGroup {
    private final String domain;

    private final String commonName;

    private final String objectGUID;

    private final List<String> members;

    public LdapGroup(String domain, String commonName, String objectGUID, List<String> members) {
        this.domain = domain;
        this.commonName = commonName;
        this.objectGUID = objectGUID;
        this.members = members;
    }

    public String getDomain() {
        return domain;
    }

    public String getCommonName() {
        return commonName;
    }

    public String getObjectGUID() {
        return objectGUID;
    }

    public List<String> getMembers() {
        return members;
    }


}
