package ru.advantum.ass.csptm.login.model.csptm;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class AdAccount {
    private Long accountId;

    private Long joflAccountId;

    private Integer adInstanceId;

    private String adDomain;

    private String adName;

    private String adGUID;

    private String firstName;

    private String lastName;

    private LocalDateTime dtInsert;

    private LocalDateTime dtUpdate;

    private LocalDateTime dtLock;

    private Boolean isLocked;

    private String email;

    private String cellPhone;

    private String workPhone;

    private String adLogin;

    public AdAccount() {
    }

    public AdAccount(Long accountId, Long joflAccountId, Integer adInstanceId, String adDomain, String adName, String adGUID, String firstName, String lastName, LocalDateTime dtInsert, LocalDateTime dtUpdate, LocalDateTime dtLock, Boolean isLocked, String email, String cellPhone, String workPhone, String adLogin) {
        this.accountId = accountId;
        this.joflAccountId = joflAccountId;
        this.adInstanceId = adInstanceId;
        this.adDomain = adDomain;
        this.adName = adName;
        this.adGUID = adGUID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dtInsert = dtInsert;
        this.dtUpdate = dtUpdate;
        this.dtLock = dtLock;
        this.isLocked = isLocked;
        this.email = email;
        this.cellPhone = cellPhone;
        this.workPhone = workPhone;
        this.adLogin = adLogin;
    }

    public AdAccount(
            Long accountId,
            Integer adInstanceId,
            String adDomain,
            String adName,
            String adGUID,
            String firstName,
            String lastName,
            Timestamp dtInsert,
            Timestamp dtUpdate,
            Timestamp dtLock,
            Boolean isLocked,
            String email,
            String cellPhone,
            String workPhone,
            Long joflAccountId,
            String adLogin) {
        this(
                accountId,
                joflAccountId,
                adInstanceId,
                adDomain,
                adName,
                adGUID,
                firstName,
                lastName,
                dtInsert != null ? dtInsert.toLocalDateTime() : null,
                dtUpdate != null ? dtUpdate.toLocalDateTime() : null,
                dtLock != null ? dtLock.toLocalDateTime() : null,
                isLocked,
                email,
                cellPhone,
                workPhone,
                adLogin
        );
    }

    public String getAdLogin() {
        return adLogin;
    }

    public void setAdLogin(String adLogin) {
        this.adLogin = adLogin;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getJoflAccountId() {
        return joflAccountId;
    }

    public void setJoflAccountId(Long joflAccountId) {
        this.joflAccountId = joflAccountId;
    }

    public Integer getAdInstanceId() {
        return adInstanceId;
    }

    public String getAdDomain() {
        return adDomain;
    }

    public void setAdDomain(String adDomain) {
        this.adDomain = adDomain;
    }

    public String getAdName() {
        return adName;
    }

    public void setAdName(String adName) {
        this.adName = adName;
    }

    public String getAdGUID() {
        return adGUID;
    }

    public void setAdGUID(String adGUID) {
        this.adGUID = adGUID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDateTime getDtInsert() {
        return dtInsert;
    }

    public void setDtInsert(LocalDateTime dtInsert) {
        this.dtInsert = dtInsert;
    }

    public LocalDateTime getDtUpdate() {
        return dtUpdate;
    }

    public void setDtUpdate(LocalDateTime dtUpdate) {
        this.dtUpdate = dtUpdate;
    }

    public LocalDateTime getDtLock() {
        return dtLock;
    }

    public void setDtLock(LocalDateTime dtLock) {
        this.dtLock = dtLock;
    }

    public Boolean getLocked() {
        return isLocked;
    }

    public void setLocked(Boolean locked) {
        isLocked = locked;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }
}
