package ru.advantum.ass.csptm.login.dao.csptm;

import java.time.LocalDateTime;
import java.util.Collection;

import javax.annotation.PostConstruct;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.advantum.ass.csptm.login.model.csptm.LoginEvent;
import ru.advantum.ass.csptm.login.model.csptm.LogoutEventCode;

@Component
public class AuthEventDao {
    private final SqlSessionFactory sqlSessionFactory;

    @Autowired
    public AuthEventDao(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }

    @PostConstruct
    public void init() {
        sqlSessionFactory.getConfiguration().addMapper(AuthEventMapper.class);

        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            AuthEventMapper authEventMapper = sqlSession.getMapper(AuthEventMapper.class);
            authEventMapper.closeSessions(LogoutEventCode.DISCONNECT);
            sqlSession.commit();
        }
    }

    public void save(LoginEvent loginEvent) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            AuthEventMapper authEventMapper = sqlSession.getMapper(AuthEventMapper.class);
            authEventMapper.saveLoginEvent(loginEvent);
            sqlSession.commit();
        }
    }

    public void closeSession(String jSessionId, LogoutEventCode code) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            AuthEventMapper authEventMapper = sqlSession.getMapper(AuthEventMapper.class);
            LocalDateTime closeDate = LocalDateTime.now();
            authEventMapper.closeSession(jSessionId, closeDate, code);
            sqlSession.commit();
        }
    }

    public void updateLastRequests(Collection<String> jSessionIds) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            AuthEventMapper authEventMapper = sqlSession.getMapper(AuthEventMapper.class);
            String[] jSessionIdArray = jSessionIds.toArray(new String[jSessionIds.size()]);
            authEventMapper.updateLastRequests(jSessionIdArray);
            sqlSession.commit();
        }
    }


    public interface AuthEventMapper {
        void saveLoginEvent(LoginEvent loginEvent);

        void updateLastRequests(@Param("jSessionIds") String[] jSessionId);

        void closeSessions(LogoutEventCode code);

        void closeSession(
                @Param("jSessionId") String jSessionId,
                @Param("closeDate") LocalDateTime closeDate,
                @Param("closeCode") LogoutEventCode closeCode
        );
    }
}
