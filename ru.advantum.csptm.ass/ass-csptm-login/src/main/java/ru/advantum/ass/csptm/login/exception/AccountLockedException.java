package ru.advantum.ass.csptm.login.exception;


import ru.advantum.ass.csptm.login.model.csptm.LoginEventCode;

public class AccountLockedException extends LoginException {

    @Override
    public LoginEventCode getLoginEventCode() {
        return LoginEventCode.ACCOUNT_LOCKED;
    }
}
