package ru.advantum.ass.csptm.login.service;

import com.google.gson.Gson;
import java.nio.charset.StandardCharsets;
import lombok.Value;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.ass.csptm.login.dao.jofl.AccountSessionHolder;

@Service
public class LogoutService {

    private final AccountSessionHolder accountSessionHolder;

    @Autowired
    public LogoutService(AccountSessionHolder accountSessionHolder) {
        this.accountSessionHolder = accountSessionHolder;
    }

    @RabbitListener(queues = "ass-login-service")
    public void receiveMessage(Message message) {
        Gson gson=new Gson();
        LogoutTask logoutTask=gson.fromJson(new String(message.getBody(), StandardCharsets.UTF_8), LogoutTask.class);
        accountSessionHolder.invalidateSession(logoutTask.getIdJoflAccount());
    }

    @Value
    private static class LogoutTask {
        Long idJoflAccount;
    }

}
