package ru.advantum.ass.csptm.login.model.csptm;

public enum  LogoutEventCode {
    MANUALLY,
    SESSION_EXPIRED,
    DISCONNECT
}
