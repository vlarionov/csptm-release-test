package ru.advantum.ass.csptm.login.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import ru.advantum.ass.csptm.login.model.csptm.LoginEvent;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class LoginEventHolder {
    private final LoginEvent loginEvent = new LoginEvent();

    public LoginEvent getLoginEvent() {
        return loginEvent;
    }
}
