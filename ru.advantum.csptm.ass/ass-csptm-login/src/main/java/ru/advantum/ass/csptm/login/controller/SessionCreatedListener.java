package ru.advantum.ass.csptm.login.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.web.session.HttpSessionCreatedEvent;
import org.springframework.stereotype.Component;
import ru.advantum.ass.csptm.login.config.LoginProperties;

@Component
public class SessionCreatedListener implements ApplicationListener<HttpSessionCreatedEvent> {

    private final int sessionMaxInactiveInterval;

    @Autowired
    public SessionCreatedListener(LoginProperties loginProperties) {
        this.sessionMaxInactiveInterval = loginProperties.sessionMaxInactiveInterval;
    }

    @Override
    public void onApplicationEvent(HttpSessionCreatedEvent event) {
        event.getSession().setMaxInactiveInterval(sessionMaxInactiveInterval);
    }

}
