package ru.advantum.ass.csptm.login.controller;

import com.google.common.base.MoreObjects;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import ru.advantum.ass.csptm.login.dao.csptm.AuthEventDao;
import ru.advantum.ass.csptm.login.dao.jofl.AccountSessionHolder;
import ru.advantum.ass.csptm.login.model.csptm.LoginEvent;
import ru.advantum.ass.csptm.login.model.csptm.LoginEventCode;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.Collection;

import static java.util.stream.Collectors.toList;


@Component
@Slf4j
public class LoginEventHandler implements HandlerInterceptor {

    private final LoginEventHolder loginEventHolder;

    private final AuthEventDao authEventDao;

    private final AccountSessionHolder accountSessionHolder;

    @Autowired
    public LoginEventHandler(LoginEventHolder loginEventHolder, AuthEventDao authEventDao,
                             AccountSessionHolder userSession) {
        this.loginEventHolder = loginEventHolder;
        this.authEventDao = authEventDao;
        this.accountSessionHolder = userSession;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        LoginEvent loginEvent = loginEventHolder.getLoginEvent();
        loginEvent.setEventDate(LocalDateTime.now());
        loginEvent.setIpAddress(getRemoteAddr(request));
        loginEvent.setJSessionId(request.getSession(true).getId());
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        loginEventHolder.getLoginEvent().setResultCode(LoginEventCode.SUCCESS);
        accountSessionHolder.addAccountSession(loginEventHolder.getLoginEvent().getIdJoflAccount(), request.getSession());
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        if (loginEventHolder.getLoginEvent().getResultCode() != null) {
            authEventDao.save(loginEventHolder.getLoginEvent());
        }
    }

    private String getRemoteAddr(HttpServletRequest request) {
        return MoreObjects.firstNonNull(
                request.getHeader("X-Forwarded-For"),
                request.getRemoteAddr()
        );
    }

    @Scheduled(fixedRate = 60_000)
    public void updateLastRequestDate() {
        try {
            Collection<String> jSessionIds = accountSessionHolder.getIdJoflAccountMap()
                    .values().stream()
                    .flatMap(Collection::stream)
                    .map(HttpSession::getId)
                    .distinct()
                    .collect(toList());
            authEventDao.updateLastRequests(jSessionIds);
        } catch (Exception e) {
            log.error("", e);
        }
    }
}
