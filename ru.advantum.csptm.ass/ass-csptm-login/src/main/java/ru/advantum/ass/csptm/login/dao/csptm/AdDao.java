package ru.advantum.ass.csptm.login.dao.csptm;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.advantum.ass.csptm.login.model.csptm.AdAccount;
import ru.advantum.ass.csptm.login.model.csptm.AdGroup;
import ru.advantum.ass.csptm.login.model.csptm.AdInstance;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Repository
public class AdDao {

    private final SqlSessionFactory sqlSessionFactory;

    @Autowired
    public AdDao(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }


    @PostConstruct
    public void addMapper() {
        sqlSessionFactory.getConfiguration().addMapper(AdMapper.class);
    }

    public List<AdInstance> findAdInstances() {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            return sqlSession.getMapper(AdMapper.class).findAdInstances();
        }
    }

    public List<AdGroup> findAdGroups() {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            return sqlSession.getMapper(AdMapper.class).findGroups();
        }
    }

    public void updateGroups(Collection<AdGroup> adGroups, Collection<AdAccount> adAccounts, Map<AdGroup, List<AdAccount>> adGroupAccountsMap) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            AdMapper adMapper = sqlSession.getMapper(AdMapper.class);

            adGroups.forEach(adMapper::updateGroup);
            adAccounts.forEach(adMapper::updateAccount);
            for (AdGroup adGroupFromLdap : adGroupAccountsMap.keySet()) {
                adMapper.updateGroupAccounts(
                        adGroupFromLdap.getGroupId(),
                        adGroupFromLdap.getAdGUID(),
                        adGroupAccountsMap.get(adGroupFromLdap).stream().map(AdAccount::getAdGUID).toArray(String[]::new)
                );
            }
            sqlSession.commit();
        }
    }

    public AdAccount updateAccount(AdAccount adAccount, List<AdGroup> accountAdGroups) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            AdMapper adMapper = sqlSession.getMapper(AdMapper.class);

            boolean isGroupExists = false;
            for (AdGroup adGroup : accountAdGroups) {
                isGroupExists =  adMapper.updateGroup(adGroup) || isGroupExists;
            }
            if (!isGroupExists) {
                return null;
            }

            adMapper.updateAccount(adAccount);
            adMapper.updateAccountGroups(
                    adAccount.getAdGUID(),
                    accountAdGroups.stream().map(AdGroup::getAdGUID).toArray(String[]::new)
            );
            sqlSession.commit();
            return adMapper.findAccountByGUID(adAccount.getAdGUID());
        }
    }

    public interface AdMapper {
        List<AdInstance> findAdInstances();

        AdAccount findAccountByGUID(@Param("guid") String guid);

        List<AdGroup> findGroups();

        /**
         * @return is exists
         */
        boolean updateGroup(AdGroup adGroup);

        void updateAccount(AdAccount adAccount);

        void updateAccountGroups(@Param("accountGUID") String accountGUID, @Param("groupGUIDs") String[] groupGUIDs);

        void updateGroupAccounts(@Param("groupId") Integer groupId, @Param("groupGUID") String groupGUID, @Param("accountGUIDs") String[] accountGUIDs);
    }
}
