package ru.advantum.ass.csptm.login.model.csptm;

import java.time.LocalDateTime;

public class LoginEvent {
    private Integer loginEventId;

    private LocalDateTime eventDate;

    private Long idJoflAccount;

    private String enteredLogin;

    private LoginEventCode resultCode;

    private String ipAddress;

    private boolean ssoLogin;

    private String jSessionId;

    public LoginEvent() {
    }

    public LoginEvent(Integer loginEventId, LocalDateTime eventDate, Long idJoflAccount, String enteredLogin, LoginEventCode resultCode, String ipAddress, boolean ssoLogin, String jSessionId) {
        this.loginEventId = loginEventId;
        this.eventDate = eventDate;
        this.idJoflAccount = idJoflAccount;
        this.enteredLogin = enteredLogin;
        this.resultCode = resultCode;
        this.ipAddress = ipAddress;
        this.ssoLogin = ssoLogin;
        this.jSessionId = jSessionId;
    }

    public Integer getLoginEventId() {
        return loginEventId;
    }

    public void setLoginEventId(Integer loginEventId) {
        this.loginEventId = loginEventId;
    }

    public LocalDateTime getEventDate() {
        return eventDate;
    }

    public void setEventDate(LocalDateTime eventDate) {
        this.eventDate = eventDate;
    }

    public Long getIdJoflAccount() {
        return idJoflAccount;
    }

    public void setIdJoflAccount(Long idJoflAccount) {
        this.idJoflAccount = idJoflAccount;
    }

    public String getEnteredLogin() {
        return enteredLogin;
    }

    public void setEnteredLogin(String enteredLogin) {
        this.enteredLogin = enteredLogin;
    }

    public LoginEventCode getResultCode() {
        return resultCode;
    }

    public void setResultCode(LoginEventCode resultCode) {
        this.resultCode = resultCode;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public boolean isSsoLogin() {
        return ssoLogin;
    }

    public void setSsoLogin(boolean ssoLogin) {
        this.ssoLogin = ssoLogin;
    }

    public String getJSessionId() {
        return jSessionId;
    }

    public void setJSessionId(String jSessionId) {
        this.jSessionId = jSessionId;
    }
}
