package ru.advantum.ass.csptm;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.advantum.ass.csptm.login.dao.jofl.LoginDao;
import ru.advantum.ass.jofl.db.AccountDao;
import ru.advantum.ass.jofl.db.JoflDao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AssApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@WebAppConfiguration
@ActiveProfiles("test")
public class MvcTest {

    private boolean visited;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private LoginDao loginDao;

    @MockBean
    private JoflDao joflDao;

    @MockBean
    private AccountDao accountDaoCommon;

    private MockMvc mockMvc;

    private MockHttpSession sess;

    @Before
    public void setup() throws Exception {
        initMocks(this);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        sess = new MockHttpSession();
        visited = false;
        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            String s = (String)args[0];
            String s2 = (String)args[1];
            String s3 = (String)args[2];

            assertEquals(s, "testHost");
            assertEquals(s2, "testName");
            assertEquals(s3, "testPwd");
            visited = true;
            return 2L;
        }).when(loginDao).authenticate(anyString(), anyString(), anyString());

        mockMvc.perform(get("/jofl.doLogin.aws").session(sess)
                .param("userName","testName")
                .param("passwdHash","testPwd")
                .header("Host", "testHost")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                .andExpect(status().isOk());
        assertTrue(visited);

    }

    @Test
    public void doHello() throws Exception {
        mockMvc.perform(get("/jofl.doHelo.aws").session(sess)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                .andExpect(status().isOk());
    }

    @Test
    public void getRowsUnauthorized() throws Exception {
        mockMvc.perform(get("/jofl.getRows.aws").param("m", "foo")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                .andExpect(status().isForbidden());
    }

    @Test
    public void getRows() throws Exception {
        mockMvc.perform(get("/jofl.getRows.aws").param("m", "foo")
                .session(sess)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                .andExpect(status().isOk());
    }

    @Test
    public void doAction() throws Exception {
        mockMvc.perform(post("/jofl.doAction.aws").session(sess)
                .param("m", "bar")
                .param("action", "OF_UPDATE")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                .andExpect(status().isOk());
    }

    @Test
    public void doLogout() throws Exception {
        mockMvc.perform(get("/jofl.doLogout.aws").session(sess)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                .andExpect(status().isOk());
        assertTrue(visited);
        mockMvc.perform(get("/jofl.getMenu.aws").session(sess)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                .andExpect(status().isForbidden());
    }

    @Test
    public void doGetMenu() throws Exception {
        mockMvc.perform(get("/jofl.getMenu.aws").session(sess)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                .andExpect(status().isOk());
    }

    @Test
    public void doGetAlarmTr() throws Exception {
        mockMvc.perform(get("/csptm.getAlarmTr.aws").session(sess)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                .andExpect(status().isOk());
    }
}

