package ru.advantum.ass.csptm;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Collections;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.SessionTrackingMode;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import ru.advantum.ass.jofl.toolbox.CallableResource;
import ru.advantum.csptm.common.JedisConfig;
import ru.advantum.csptm.common.ReportConfig;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableEurekaClient
@ComponentScan(basePackages = "ru.advantum.ass.jofl.servletconf")
@Import({JedisConfig.class, ReportConfig.class})
public class AssApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        buildApplication(new SpringApplicationBuilder()).run(args);
    }

    private static SpringApplicationBuilder buildApplication(SpringApplicationBuilder builder) {
        return builder.sources(AssApplication.class);
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        servletContext.setInitParameter("defaultHtmlEscape", "true");
        servletContext.setSessionTrackingModes(sessionTrackingModes());
        super.onStartup(servletContext);
    }

    private Set<SessionTrackingMode> sessionTrackingModes() {
        return Collections.singleton(SessionTrackingMode.COOKIE);
    }

    @Bean
    public TaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(2);
        return scheduler;
    }

    @Bean
    public CallableResource callableResource(
            EurekaClient discoveryClient,
            @Value("${csptm.props.ass.jofl}") String resource
    ) throws IOException
    {
        InstanceInfo instance = discoveryClient.getNextServerFromEureka("configserver", false);
        URI path = URI.create(instance.getHomePageUrl()).resolve(resource);
        try (InputStream inputStream = path.toURL().openStream()) {
            return new CallableResource().build(inputStream);
        }
    }

    @Configuration
    @EnableSwagger2
    public static class SwaggerConfig extends WebMvcConfigurerAdapter {
        @Override
        public void addResourceHandlers(ResourceHandlerRegistry registry) {
            registry.addResourceHandler("/**").addResourceLocations("classpath:/META-INF/resources/");
        }
    }

}
