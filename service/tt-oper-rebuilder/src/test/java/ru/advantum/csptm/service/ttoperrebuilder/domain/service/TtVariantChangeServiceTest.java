package ru.advantum.csptm.service.ttoperrebuilder.domain.service;


import com.google.common.collect.ImmutableSortedSet;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;
import ru.advantum.csptm.service.ttoperrebuilder.TTOperRebuilderApplication;
import ru.advantum.csptm.service.ttoperrebuilder.application.NormService;
import ru.advantum.csptm.service.ttoperrebuilder.domain.factory.ImmutableCloneTtVariantFactory;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.Entity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtActionItem;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtOut;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtVariant;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtVariantRepository;

import java.time.Duration;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.util.Optional.ofNullable;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TTOperRebuilderApplication.class})
@EnableTransactionManagement
@Transactional
@Slf4j
public class TtVariantChangeServiceTest {
    private final Random random = new Random();

    @Autowired
    private TtVariantRepository ttVariantRepository;

    @Autowired
    private TtVariantChangeService ttVariantChangeService;

    @Test
    public void test0() {
        forEachTestTtVariant(ttVariant ->
                forEachTtActionItem(ttVariant, trActionItem ->
                        timesNotChanged(trActionItem, new ImmutableCloneTtVariantFactory(ttVariant).build())));
    }

    @Test
    public void test1() {
        forEachTestTtVariant(ttVariant ->
                forEachFlexibleTtActionItem(ttVariant, trActionItem ->
                        whenDurationChanged(trActionItem, newTtVariant ->
                                startTimeNotChanged(trActionItem, newTtVariant))));
    }

    @Test
    public void test2() {
        forEachTestTtVariant(ttVariant ->
                forEachFlexibleTtActionItem(ttVariant, trActionItem ->
                        whenDurationChanged(trActionItem, newTtVariant ->
                                durationChanged(trActionItem, newTtVariant))));
    }

    @Test
    public void test3() {
        forEachTestTtVariant(ttVariant ->
                forEachFlexibleTtActionItem(ttVariant, flexibleTtActionItem ->
                        whenDurationChanged(flexibleTtActionItem, newTtVariant ->
                                forEachOtherTrOutsTtActionItem(ttVariant, flexibleTtActionItem, trActionItem ->
                                        timesNotChanged(trActionItem, newTtVariant)))));
    }

    @Test
    public void test4() {
        forEachTestTtVariant(ttVariant ->
                forEachFlexibleTtActionItem(ttVariant, flexibleTtActionItem ->
                        whenDurationChanged(flexibleTtActionItem, newTtVariant ->
                                forEachSameTrOutNextTtActionItem(flexibleTtActionItem, trActionItem ->
                                        timesShifted(trActionItem, newTtVariant, newDuration(flexibleTtActionItem).minus(flexibleTtActionItem.getDuration()))))));
    }

    @Test
    public void test5() {
        forEachTestTtVariant(ttVariant ->
                forEachFlexibleTtActionItem(ttVariant, trBreak ->
                        whenDurationChanged(trBreak, newTtVariant ->
                                trActionItemsNumberNotChanged(trBreak.getTtVariant(), newTtVariant))));
    }

    @Test
    public void test6() {
        forEachTestTtVariant(ttVariant ->
                forEachFlexibleTtActionItem(ttVariant, trBreak ->
                        whenDurationChanged(trBreak, newTtVariant ->
                                trActionsNumberNotChanged(trBreak.getTtVariant(), newTtVariant))));
    }

    @Test
    public void test7() {
        forEachTestTtVariant(ttVariant ->
                forEachFlexibleTtActionItem(ttVariant, trBreak ->
                        whenDurationChanged(trBreak, newTtVariant ->
                                forEachTtActionItem(ttVariant, trActionItem ->
                                        stopNotChanged(trActionItem, newTtVariant)))));
    }

    @Test
    public void test8() {
        forEachTestTtVariant(ttVariant ->
                forEachFlexibleTtActionItem(ttVariant, trBreak ->
                        whenDurationChanged(trBreak, newTtVariant ->
                                forEachTtActionItem(ttVariant, trActionItem ->
                                        previousNotChanged(trActionItem, newTtVariant)))));
    }

    @Test
    public void test9() {
        forEachTestTtVariant(ttVariant ->
                forEachFlexibleTtActionItem(ttVariant, trBreak ->
                        whenDurationChanged(trBreak, newTtVariant ->
                                forEachTtActionItem(ttVariant, trActionItem ->
                                        nextNotChanged(trActionItem, newTtVariant)))));
    }


    @Test
    public void test10() {
        forEachTestTtVariant(ttVariant ->
                existTtActionItem(ttVariant, TtActionItem::isFlexible));
    }

    @Test
    public void test12() {
        forEachTestTtVariant(ttVariant ->
                forEachTtActionItem(ttVariant,
                        this::continuousWithPreviousAndNextTtActionItem));
    }

    @Test
    public void test13() {
        forEachTestTtVariant(ttVariant ->
                forEachFlexibleTtActionItem(ttVariant, flexibleItem ->
                        whenDurationChanged(flexibleItem, newTtVariant ->
                                forEachTtActionItem(newTtVariant,
                                        this::continuousWithPreviousAndNextTtActionItem))));
    }

    @Test
    public void test14() {
        forEachTestTtVariant(ttVariant ->
                forEachTtOut(ttVariant,
                        this::ttActionItemsContinuous));
    }

    @Test
    public void test15() {
        forEachTestTtVariant(ttVariant ->
                forEachFlexibleTtActionItem(ttVariant, flexibleItem ->
                        whenDurationChanged(flexibleItem, newTtVariant ->
                                forEachTtOut(newTtVariant,
                                        this::ttActionItemsContinuous))));
    }

    private void forEachTtOut(TtVariant ttVariant, Consumer<TtOut> consumer) {
        ttVariant.getTtOuts().forEach(consumer);
    }

    private void ttActionItemsContinuous(TtOut ttOut) {
        ttOut.getSortedTtActionItems().stream().reduce(
                (current, next) -> {
                    assertEquals(current.getEndTime(), next.getStartTime());
                    return next;
                }
        );
    }

    private void continuousWithPreviousAndNextTtActionItem(TtActionItem ttActionItem) {
        Optional.ofNullable(ttActionItem.getPreviousTtActionItem())
                .ifPresent(previous -> assertEquals(previous.getEndTime(), ttActionItem.getStartTime()));

        Optional.ofNullable(ttActionItem.getNextTtActionItem())
                .ifPresent(next -> assertEquals(next.getStartTime(), ttActionItem.getEndTime()));
    }

    private void trActionItemsNumberNotChanged(TtVariant routeTT, TtVariant newTtVariant) {
        assertEquals(routeTT.getTtActionItems().size(), newTtVariant.getTtActionItems().size());
    }

    private void trActionsNumberNotChanged(TtVariant routeTT, TtVariant newTtVariant) {
        assertEquals(routeTT.getTtActions().size(), newTtVariant.getTtActions().size());
    }

    private void forEachFlexibleTtActionItem(TtVariant ttVariant, Consumer<TtActionItem> trActionItemConsumer) {
        ttVariant.getTtActionItems().stream()
                .filter(TtActionItem::isFlexible)
                .forEach(trActionItemConsumer);
    }

    private void forEachTtActionItem(TtVariant ttVariant, Consumer<TtActionItem> trActionItemConsumer) {
        ttVariant.getTtActionItems().forEach(trActionItemConsumer);
    }

    private void existTtActionItem(TtVariant ttVariant, Predicate<TtActionItem> predicate) {
        assertTrue(ttVariant.getTtActionItems().stream().anyMatch(predicate));
    }

    private void whenDurationChanged(TtActionItem trActionItem, Consumer<TtVariant> ttVariantConsumer) {
        val newDuration = newDuration(trActionItem);
        log.info("duration changed, starTime {}, old {}, new {}", trActionItem.getStartTime(), trActionItem.getDuration(), newDuration);
        ttVariantConsumer.accept(ttVariantChangeService.changeDuration(trActionItem, newDuration));
    }

    private Duration newDuration(TtActionItem ttActionItem) {
        return ttActionItem.getDuration().plusMinutes(5).multipliedBy(2);
    }

    private void forEachOtherTrOutsTtActionItem(TtVariant ttVariant, TtActionItem trActionItem, Consumer<TtActionItem> ttActionItemConsumer) {
        ttVariant.getTtActionItems().stream()
                .filter(trActionItem1 -> !trActionItem.getTtOut().equals(trActionItem1.getTtOut()))
                .forEach(ttActionItemConsumer);
    }

    private void forEachSameTrOutNextTtActionItem(TtActionItem trActionItem, Consumer<TtActionItem> trActionItemConsumer) {
        ((ImmutableSortedSet<TtActionItem>) trActionItem.getTtOut()
                .getTtActionItems())
                .tailSet(trActionItem, false)
                .forEach(trActionItemConsumer);
    }

    private void timesNotChanged(TtActionItem trActionItem, TtVariant newTtVariant) {
        val newTtActionItem = newTtVariant.getTtActionItemIdMap().get(trActionItem.getId());
        assertEquals(trActionItem.getStartTime(), newTtActionItem.getStartTime());
        assertEquals(trActionItem.getDuration(), newTtActionItem.getDuration());
    }

    private void timesShifted(TtActionItem trActionItem, TtVariant newTtVariant, Duration shift) {
        val newTtActionItem = newTtVariant.getTtActionItemIdMap().get(trActionItem.getId());
        assertEquals(trActionItem.getStartTime().plus(shift), newTtActionItem.getStartTime());
        assertEquals(trActionItem.getDuration(), newTtActionItem.getDuration());
    }

    private void startTimeNotChanged(TtActionItem trActionItem, TtVariant newTtVariant) {
        val newTtActionItem = newTtVariant.getTtActionItemIdMap().get(trActionItem.getId());
        assertEquals(trActionItem.getStartTime(), newTtActionItem.getStartTime());
    }

    private void durationChanged(TtActionItem trActionItem, TtVariant newTtVariant) {
        val newTtActionItem = newTtVariant.getTtActionItemIdMap().get(trActionItem.getId());
        assertEquals(newDuration(trActionItem), newTtActionItem.getDuration());
    }

    private void stopNotChanged(TtActionItem trActionItem, TtVariant newTtVariant) {
        val newTtActionItem = newTtVariant.getTtActionItemIdMap().get(trActionItem.getId());
        assertEquals(trActionItem.getStopItem().getId(), newTtActionItem.getStopItem().getId());
    }

    private void previousNotChanged(TtActionItem trActionItem, TtVariant newTtVariant) {
        val newTtActionItem = newTtVariant.getTtActionItemIdMap().get(trActionItem.getId());
        assertEquals(
                ofNullable(trActionItem.getPreviousTtActionItem()).map(Entity::getId).orElse(null),
                ofNullable(newTtActionItem.getPreviousTtActionItem()).map(Entity::getId).orElse(null)
        );
    }


    private void nextNotChanged(TtActionItem trActionItem, TtVariant newTtVariant) {
        val newTtActionItem = newTtVariant.getTtActionItemIdMap().get(trActionItem.getId());
        assertEquals(
                ofNullable(trActionItem.getNextTtActionItem()).map(Entity::getId).orElse(null),
                ofNullable(newTtActionItem.getNextTtActionItem()).map(Entity::getId).orElse(null)
        );
    }

    private void forEachTestTtVariant(Consumer<TtVariant> consumer) {
        val planIds = Arrays.asList(
                2683818,
                2683807,
                2683843
        );
        val operIds = new ArrayList<Integer>();
        Stream.concat(planIds.stream(), operIds.stream())
                .flatMap(this::generateModifications)
                .forEach(consumer);
    }


    public Stream<TtVariant> generateModifications(Integer ttVariantId) {
        return Stream.of(ttVariantRepository.find(ttVariantId))
                .flatMap(ttVariant -> Stream.of(ttVariant, ttVariantChangeService.supplementBreaks(ttVariant)))
                .flatMap(ttVariant -> Stream.of(ttVariant, ttVariantChangeService.recalculateRoundDurations(ttVariant)))
                .flatMap(ttVariant -> Stream.concat(
                        Stream.of(ttVariant),
                        ttVariant.getTtActionItems().stream()
                                .filter(TtActionItem::isFlexible)
                                .map(ttActionItem -> {
                                    val durationMillis = (long) (ttActionItem.getDuration().plusMinutes(5).toMillis() * random.nextGaussian()) * random.nextInt(2);
                                    val newDuration = Duration.ofMillis(durationMillis).abs();
                                    return ttVariantChangeService.changeDuration(ttActionItem, newDuration);
                                }))
                );

    }

    @TestComponent(value = "normServiceImpl")
    public static class NormServiceImpl implements NormService {
        @Override
        public Optional<Duration> getDuration(int normId, int startStopItemId, int endStopItemId, LocalTime hour, ZoneOffset zoneOffset) {
            return Optional.empty();
        }
    }

}