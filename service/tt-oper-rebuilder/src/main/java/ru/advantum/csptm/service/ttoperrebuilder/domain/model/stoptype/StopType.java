package ru.advantum.csptm.service.ttoperrebuilder.domain.model.stoptype;


import ru.advantum.csptm.service.ttoperrebuilder.domain.model.Entity;

public interface StopType extends Entity<Short> {
    short TERMINAL = 7;

    short TERMINAL_A = 10;
    short TERMINAL_B = 11;
}
