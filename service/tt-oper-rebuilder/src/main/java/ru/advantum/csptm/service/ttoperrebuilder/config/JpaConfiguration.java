package ru.advantum.csptm.service.ttoperrebuilder.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "ru.advantum.csptm.service.ttoperrebuilder.port.persistent.jpa")
public class JpaConfiguration {
}
