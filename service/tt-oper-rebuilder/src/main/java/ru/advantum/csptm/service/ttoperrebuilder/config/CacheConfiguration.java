package ru.advantum.csptm.service.ttoperrebuilder.config;

import com.google.common.cache.CacheBuilder;
import lombok.val;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.service.ttoperrebuilder.port.persistent.NormRepositoryImpl;
import ru.advantum.csptm.service.ttoperrebuilder.port.persistent.jpa.PersistentActionTypeRepository;

import java.util.concurrent.TimeUnit;

@Configuration
@EnableCaching
public class CacheConfiguration {

    @Bean
    public CacheManager cacheManager() {
        val manager = new GuavaCacheManager(
                PersistentActionTypeRepository.CACHE_NAME,
                NormRepositoryImpl.CACHE_NAME
        );
        val builder =  CacheBuilder.newBuilder().expireAfterWrite(1, TimeUnit.DAYS);
        manager.setCacheBuilder(builder);
        return manager;
    }
}
