package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem;

import org.immutables.value.Value;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.stoptype.StopType;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.AbstractTtVariantEntity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtAction;

import java.util.Comparator;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedSet;

import static com.google.common.collect.ImmutableSortedSet.toImmutableSortedSet;
import static java.util.stream.Collectors.toSet;

@Value.Immutable
public abstract class AbstractStopItem extends AbstractTtVariantEntity<Integer> implements StopItem {

    @Override
    @Value.Lazy
    public Set<TtAction> getTtActions() {
        return getTtVariant().getTtActions().stream()
                .filter(this::isStartOf)
                .collect(toSet());
    }

    @Override
    @Value.Lazy
    public NavigableSet<TtAction> getSortedTtActions() {
        return getTtActions()
                .stream()
                .collect(toImmutableSortedSet(Comparator.naturalOrder()));
    }

    @Override
    @Value.Lazy
    public SortedSet<TtAction> getSortedRounds() {
        return getSortedTtActions().stream()
                .filter(TtAction::isRound)
                .collect(toImmutableSortedSet(Comparator.naturalOrder()));
    }

    @Override
    @Value.Lazy
    public boolean isStartOf(TtAction trAction) {
        return this.equals(trAction.getStartStopItem());
    }

    @Override
    @Value.Lazy
    public boolean isFloatingBreakDuration() {
        return isTerminal();
    }

    @Override
    @Value.Lazy
    public boolean isTerminal() {
        return getStopTypes().stream()
                .anyMatch(stopType -> stopType.getId().equals(StopType.TERMINAL_A) );
    }
}
