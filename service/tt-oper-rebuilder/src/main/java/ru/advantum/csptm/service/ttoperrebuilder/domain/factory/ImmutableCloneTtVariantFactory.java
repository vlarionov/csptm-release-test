package ru.advantum.csptm.service.ttoperrebuilder.domain.factory;

import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.*;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.driver.FakeDriver;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.driver.ImmutableFakeDriver;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem.ImmutableStopItem;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem.StopItem;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem2round.ImmutableStopItem2Round;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem2round.StopItem2Round;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class ImmutableCloneTtVariantFactory implements TtVariantFactory {
    private final TtVariantContext sourceContext;

    public ImmutableCloneTtVariantFactory(TtVariant ttVariant) {
        this.sourceContext = ttVariant.getContext();
    }

    public ImmutableTtActionItem.Builder mapTtActionItemBuilder(TtActionItem sourceItem) {
        return ImmutableTtActionItem.builder().from(sourceItem);
    }

    public Stream<ImmutableTtActionItem.Builder> getTtActionItemBuilderStream(Stream<TtActionItem> sourceItemStream) {
        return sourceItemStream.map(this::mapTtActionItemBuilder);
    }

    public TtVariant build() {
        TtVariant sourceTtVariant = sourceContext.getTtVariant();
        ImmutableTtVariant.Builder ttVariantBuilder = ImmutableTtVariant.builder().from(sourceTtVariant);
        List<ImmutableTtOut.Builder> ttOutBuilders = sourceContext.getTtOuts().values().stream()
                .map(trOut -> ImmutableTtOut.builder().from(trOut))
                .collect(toList());
        List<ImmutableTtAction.Builder> ttActionBuilders = sourceContext.getTtActions().values().stream()
                .map(ttAction -> ImmutableTtAction.builder().from(ttAction))
                .collect(toList());
        Stream<TtActionItem> sourceItemStream = sourceContext.getTtActionItems().values().stream();
        List<ImmutableTtActionItem.Builder> ttActionItemBuilders = getTtActionItemBuilderStream(sourceItemStream).collect(toList());

        List<ImmutableStopItem.Builder> stopItems = sourceContext.getStopItems().values().stream()
                .map(stop -> ImmutableStopItem.builder().id(stop.getId()).stopTypes(stop.getStopTypes()))
                .collect(toList());
        List<ImmutableStopItem2Round.Builder> stopItem2Rounds = sourceContext.getStopItem2Rounds().values().stream()
                .map(stopItem2Round -> ImmutableStopItem2Round.builder()
                        .id(stopItem2Round.getId())
                        .stopItemId(stopItem2Round.getStopItem().getId())
                )
                .collect(toList());
        List<ImmutableFakeDriver.Builder> drivers = sourceContext.getFakeDrivers().values().stream()
                .map(driver -> ImmutableFakeDriver.builder().id(driver.getId()))
                .collect(toList());

        TtVariantContext context = new TtVariantContext(
                ttVariantBuilder::build,
                () -> ttOutBuilders.stream().map(builder -> (TtOut) builder.build()).collect(toList()),
                () -> ttActionBuilders.stream().map(builder -> (TtAction) builder.build()).collect(toList()),
                () -> ttActionItemBuilders.stream().map(builder -> (TtActionItem) builder.build()).collect(toList()),
                () -> stopItems.stream().map(builder -> (StopItem) builder.build()).collect(toList()),
                () -> stopItem2Rounds.stream().map(builder -> (StopItem2Round) builder.build()).collect(Collectors.toList()),
                () -> drivers.stream().map(builder -> (FakeDriver) builder.build()).collect(toList())
        );
        ttVariantBuilder.context(context);
        ttOutBuilders.forEach(ttOut -> ttOut.ttVariantContext(context));
        ttActionBuilders.forEach(ttAction -> ttAction.ttVariantContext(context));
        ttActionItemBuilders.forEach(ttActionItem -> ttActionItem.ttVariantContext(context));
        stopItems.forEach(stopItem -> stopItem.ttVariantContext(context));
        stopItem2Rounds.forEach(stopItem2Round -> stopItem2Round.ttVariantContext(context));
        drivers.forEach(drShift -> drShift.ttVariantContext(context));

        return context.getTtVariant();
    }

}