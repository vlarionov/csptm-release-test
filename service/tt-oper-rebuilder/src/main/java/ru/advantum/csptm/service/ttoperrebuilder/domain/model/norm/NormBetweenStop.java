package ru.advantum.csptm.service.ttoperrebuilder.domain.model.norm;

import java.time.Duration;
import java.time.LocalTime;

public interface NormBetweenStop extends Comparable<NormBetweenStop> {
    Integer getNormId();

    Duration getDuration();

    LocalTime getHourFrom();

    LocalTime getHourTo();

    Integer getStopItem1Id();

    Integer getStopItem2Id();

}
