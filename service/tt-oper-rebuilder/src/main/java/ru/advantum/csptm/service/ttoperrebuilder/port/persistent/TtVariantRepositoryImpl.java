package ru.advantum.csptm.service.ttoperrebuilder.port.persistent;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.advantum.csptm.service.ttoperrebuilder.domain.factory.ImmutableCloneTtVariantFactory;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.Entity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.actiontype.ActionType;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtVariant;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtVariantRepository;
import ru.advantum.csptm.service.ttoperrebuilder.port.persistent.jpa.PersistentActionTypeRepository;
import ru.advantum.csptm.service.ttoperrebuilder.port.persistent.jpa.PersistentTtVariantRepository;

import java.sql.Timestamp;
import java.util.Set;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@Repository
@Slf4j
public class TtVariantRepositoryImpl implements TtVariantRepository {

    private final PersistentTtVariantRepository persistentTtVariantRepository;

    private final PersistentActionTypeRepository actionTypeRepository;

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public TtVariantRepositoryImpl(PersistentTtVariantRepository persistentTtVariantRepository, PersistentActionTypeRepository actionTypeRepository, JdbcTemplate jdbcTemplate) {
        this.persistentTtVariantRepository = persistentTtVariantRepository;
        this.actionTypeRepository = actionTypeRepository;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public TtVariant find(int id) {
        actionTypeRepository.findAll();
        val ttVariantEntity = persistentTtVariantRepository.findOne(id);
        return new ImmutableCloneTtVariantFactory(ttVariantEntity).build();
    }

    //todo make save by hibernate
    @Override
    public void save(TtVariant ttVariant) {
        log.info("TtVariant save started, id {}", ttVariant.getId());

        val updateTtActionArgs = ttVariant.getTtActions().stream()
                .map(trAction -> new Object[]{
                        Timestamp.from(trAction.getStartTime()),
                        trAction.getDuration().toMillis() / 1000,
                        Timestamp.from(trAction.getStartTime()),
                        Timestamp.from(trAction.getEndTime()),
                        trAction.getId()
                })
                .collect(toList());


        Set<Long> ttActionItemIdsFromDb = persistentTtVariantRepository.findOne(ttVariant.getId()).getTtActionItemIdMap().keySet();
        Set<Long> newItemIds = ttVariant.getTtActionItemIdMap()
                .keySet().stream()
                .filter(ttActionItemId -> !ttActionItemIdsFromDb.contains(ttActionItemId))
                .collect(toSet());

        val itemIdsToRemove = ttVariant.getTtActionItems().stream()
                .filter(ttActionItem -> ttActionItem.getActionType().instanceOf(ActionType.REGULAR_TIME) ||
                        ttActionItem.getActionType().instanceOf(ActionType.CORRECTION_TIME))
                .filter(ttActionItem -> ttActionItem.getDuration().isZero())
                .map(Entity::getId)
                .filter(ttActionItemIdsFromDb::contains)
                .collect(toSet());
        val itemIdsToInsert = ttVariant.getTtActionItems().stream()
                .filter(ttActionItem -> newItemIds.contains(ttActionItem.getId()))
                .filter(ttActionItem -> !ttActionItem.getDuration().isZero())
                .map(Entity::getId)
                .collect(toSet());
        val itemIdsToUpdate = ttVariant.getTtActionItems().stream()
                .map(Entity::getId)
                .filter(ttActionItemIdsFromDb::contains)
                .filter(ttActionItemId -> !itemIdsToRemove.contains(ttActionItemId))
                .collect(toSet());

        val insertTtActionItemArgs = ttVariant.getTtActionItems().stream()
                .filter(ttActionItem -> itemIdsToInsert.contains(ttActionItem.getId()))
                .map(ttActionItem -> new Object[]{
                        ttActionItem.getId(),
                        ttActionItem.getTtAction().getId(),
                        Timestamp.from(ttActionItem.getStartTime()),
                        Timestamp.from(ttActionItem.getEndTime()),
                        ttActionItem.getStopItem2Round().getId(),
                        ttActionItem.getActionType().getId(),
                        false,
                        ttActionItem.getDrShiftId(),
                        ttActionItem.getDriverId()
                })
                .collect(toList());
        val updateTtActionItemArgs = ttVariant.getTtActionItems().stream()
                .filter(ttActionItem -> itemIdsToUpdate.contains(ttActionItem.getId()))
                .map(trActionItem -> new Object[]{
                        Timestamp.from(trActionItem.getStartTime()),
                        Timestamp.from(trActionItem.getTtAction().isRound() ? trActionItem.getStartTime() : trActionItem.getEndTime()),
                        trActionItem.getId()
                })
                .collect(toList());

        jdbcTemplate.batchUpdate(
                "UPDATE ttb.tt_action " +
                        "SET " +
                        "  dt_action = ?, " +
                        "  action_dur = ?, " +
                        "  action_range = tsrange(?, ?) " +
                        "WHERE tt_action_id = ?",
                updateTtActionArgs
        );

        log.info("TtActions saved, size {}", ttVariant.getTtActions().size());
        jdbcTemplate.batchUpdate(
                "UPDATE ttb.tt_action_item SET sign_deleted = TRUE WHERE tt_action_item_id = ?",
                itemIdsToRemove.stream().map(ttActionItemId -> new Object[]{ttActionItemId}).collect(toList())
        );
        jdbcTemplate.batchUpdate(
                "INSERT INTO ttb.tt_action_item(tt_action_item_id, tt_action_id, time_begin, time_end, stop_item2round_id, action_type_id, sign_deleted, dr_shift_id, driver_id) " +
                        "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?) ",
                insertTtActionItemArgs
        );
        jdbcTemplate.batchUpdate(
                "UPDATE ttb.tt_action_item " +
                        "SET " +
                        "time_begin = ?, " +
                        "time_end = ? " +
                        "WHERE tt_action_item_id = ?",
                updateTtActionItemArgs
        );

        log.info("TtActionItems saved, size {}", ttVariant.getTtActionItems().size());

        log.info("TtVariant saved");
    }

    @Override
    public Long generateTtActionItemId() {
        return jdbcTemplate.queryForObject("SELECT nextval('ttb.tt_action_item_tt_action_item_id_seq')", Long.class);
    }


    @Override
    public void updateCreateStatus(int ttVariantId, boolean isSuccess) {
        int SUCCESS_STATUS = 3;
        int ERROR_STATUS = 4;
        int status = isSuccess ? SUCCESS_STATUS : ERROR_STATUS;
        String sql = "UPDATE ttb.tt_variant SET tt_create_status_id = ? WHERE tt_variant_id = ? AND order_date IS NULL";
        jdbcTemplate.update(sql, status, ttVariantId);
    }


}