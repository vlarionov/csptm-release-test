package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant;

import com.google.common.collect.ComparisonChain;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.val;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.actiontype.ActionType;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.actiontype.ImmutableActionType;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.actiontype.PersistentActionType;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem2round.PersistentStopItem2Round;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem2round.StopItem2Round;

import javax.annotation.Nullable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.util.Optional;
import java.util.function.Function;

@Entity
@Table(schema = "ttb")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PersistentTtActionItem extends AbstractTtActionItem {

    @ManyToOne(targetEntity = PersistentTtAction.class)
    TtAction ttAction;

    Integer driverId;

    @ManyToOne(targetEntity = PersistentActionType.class, fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    ActionType actionType;

    @ManyToOne(targetEntity = PersistentStopItem2Round.class, fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    StopItem2Round stopItem2Round;

    Short drShiftId;

    //    @Temporal(TemporalType.TIMESTAMP)
    Timestamp timeBegin;

    //    @Temporal(TemporalType.TIMESTAMP)
    Timestamp timeEnd;

    @Override
    public ActionType getActionType() {
        return ImmutableActionType.builder().from(actionType).build();
    }

    @Override
    public Long getStopItem2RoundId() {
        return stopItem2Round.getId();
    }

    @Override
    @Transient
    public Integer getTtActionId() {
        return ttAction.getId();
    }

    @Nullable
    @Override
    public TtActionItem getPreviousTtActionItem() {
        return super.getPreviousTtActionItem();
    }

    @Nullable
    @Override
    public TtActionItem getNextTtActionItem() {
        return super.getNextTtActionItem();
    }

    @Nullable
    @Override
    public Long getNextTtActionItemId() {
        val items = getTtOut().getSortedTtActionItems();
        return Optional.ofNullable(items.higher(this ))
                .map(TtActionItem::getId).orElse(null);
    }

    @Nullable
    @Override
    public Long getPreviousTtActionItemId() {
        val items = getTtOut().getSortedTtActionItems();
        return Optional.ofNullable(items.lower(this ))
                .map(TtActionItem::getId).orElse(null);
    }

    private transient volatile Instant startTime1 = null;
    @Override
    @Transient
    public Function<TtActionItem, Instant> startTimeProvider() {
        if (this.startTime1 == null) {
            this.startTime1 = timeBegin.toInstant();
        }
        return (ttActionItem) -> this.startTime1;
    }

    @Override
    @Transient
    public TtVariant getTtVariant() {
        return getTtAction().getTtVariant();
    }

    @Override
    public TtVariantContext getTtVariantContext() {
        return getTtVariant().getContext();
    }

    @Override
    public Function<TtActionItem, Duration> durationProvider() {
        //because in db startTime == endTime for round items
        Function<TtActionItem, Duration> superDurationProvider = super.durationProvider();
        if (getTtAction().isRound()) {
            return superDurationProvider;
        }

        return ttActionItem -> Duration.between(timeBegin.toLocalDateTime(), timeEnd.toLocalDateTime());
    }

    @Override
    public int compareTo(TimedAction other) {
        PersistentTtActionItem otherActionItem = (PersistentTtActionItem) other;
        return ComparisonChain.start()
                .compare(timeBegin, otherActionItem.timeBegin)
                .compare(timeEnd, otherActionItem.timeEnd)
                .compare(getId(), otherActionItem.getId())
                .result();
    }

}
