package ru.advantum.csptm.service.ttoperrebuilder;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class TTOperRebuilderApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(TTOperRebuilderApplication.class).run(args);
    }

}
