package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem;


import ru.advantum.csptm.service.ttoperrebuilder.domain.model.stoptype.StopType;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtAction;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtVariantEntity;

import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedSet;

public interface StopItem extends TtVariantEntity<Integer> {

    Set<TtAction> getTtActions();

    NavigableSet<TtAction> getSortedTtActions();

    SortedSet<TtAction> getSortedRounds();

    Set<StopType> getStopTypes();

    boolean isFloatingBreakDuration();

    boolean isStartOf(TtAction trAction);

    boolean isTerminal();

}
