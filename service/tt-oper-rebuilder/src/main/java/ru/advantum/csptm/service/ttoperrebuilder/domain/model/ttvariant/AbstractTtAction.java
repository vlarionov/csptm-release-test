package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant;

import com.google.common.collect.ImmutableSortedSet;
import lombok.val;
import org.immutables.value.Value;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.actiontype.ActionType;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem.StopItem;

import javax.annotation.Nullable;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.time.Duration;
import java.time.Instant;
import java.util.Comparator;
import java.util.Set;

import static com.google.common.collect.ImmutableSortedSet.toImmutableSortedSet;

@Value.Immutable
@MappedSuperclass
public abstract class AbstractTtAction extends AbstractTtVariantEntity<Integer> implements TtAction {
    public abstract Integer getTtOutId();

    @Override
    @Value.Lazy
    public TtOut getTtOut() {
        return getTtVariantContext().getTtOuts().get(getTtOutId());
    }

    @Override
    @Value.Lazy
    @Transient
    public Set<TtActionItem> getTtActionItems() {
        return getTtVariant().getTtActionTtActionItemsMap().get(getId());
    }

    @Override
    @Value.Lazy
    public ImmutableSortedSet<TtActionItem> getSortedTtActionItems() {
        return getTtActionItems().stream().collect(toImmutableSortedSet(Comparator.naturalOrder()));
    }

    @Override
    @Value.Lazy
    public ImmutableSortedSet<TtAction> getPreviousTtActions() {
        return ImmutableSortedSet.copyOf(getTtOut().getSortedTtActions().headSet(this));
    }

    @Override
    @Value.Lazy
    public Instant getStartTime() {
        return getFirstTtActionItem().getStartTime();
    }

    @Override
    @Value.Lazy
    public Instant getEndTime() {
        return getLastTtActionItem().getEndTime();
    }

    @Override
    @Value.Lazy
    @Nullable
    public TtAction getPreviousTtAction() {
        return getTtOut().getSortedTtActions().lower(this);
    }

    @Override
    @Value.Lazy
    public TtActionItem getFirstTtActionItem() {
        return getSortedTtActionItems().first();
    }

    @Override
    @Value.Lazy
    public TtActionItem getLastTtActionItem() {
        return getSortedTtActionItems().last();
    }

    @Override
    @Value.Lazy
    public StopItem getStartStopItem() {
        return getFirstTtActionItem().getStopItem();
    }

    @Override
    @Value.Lazy
    public StopItem getEndStopItem() {
        return getLastTtActionItem().getStopItem();
    }

    @Override
    @Value.Lazy
    @Nullable
    public TtAction getPreviousTtBreak() {
        val previousTrAction = getPreviousTtAction();
        if (previousTrAction == null || previousTrAction.isBreak()) {
            return previousTrAction;
        }

        return previousTrAction.getPreviousTtBreak();
    }

    @Override
    @Value.Lazy
    @Nullable
    public TtAction getMainNextRound() {
        val nextTrAction = getTtOut().getSortedTtActions().higher(this);
        if (nextTrAction == null || nextTrAction.getActionType().instanceOf(ActionType.MAIN_ROUND)) {
            return nextTrAction;
        }

        return nextTrAction.getMainNextRound();
    }

    @Override
    @Value.Lazy
    @Nullable
    public TtAction getPreviousMainRoundOnStop() {
        return getStartStopItem().getSortedTtActions()
                .headSet(this, false).stream()
                .sorted(Comparator.reverseOrder())
                .filter(ttAction -> ttAction.getActionType().instanceOf(ActionType.MAIN_ROUND))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Value.Lazy
    @Nullable
    public TtAction getNextMainTtRoundOnStop() {
        return getStartStopItem().getSortedTtActions()
                .tailSet(this, false).stream()
                .sorted()
                .filter(ttAction -> ttAction.getActionType().instanceOf(ActionType.MAIN_ROUND))
                .findFirst()
                .orElse(null);
    }


    @Override
    @Value.Lazy
    public Duration getIntervalDiff() {
        val previousRound = getPreviousMainRoundOnStop();
        val nextRound = getNextMainTtRoundOnStop();
        if (previousRound == null || nextRound == null) {
            return Duration.ZERO;
        }
        return Duration.ofMinutes(nextRound.getInterval().minus(getInterval()).toMinutes()/2);
    }

    @Override
    @Value.Lazy
    public double getIntervalDiffMultiplier() {
        if (getInterval().equals(Duration.ZERO) || getNextMainTtRoundOnStop() == null || getNextMainTtRoundOnStop().getInterval().equals(Duration.ZERO)) {
            return 1;
        }

        return (1.0 * getInterval().plus(getNextMainTtRoundOnStop().getInterval()).toMillis()) / (getInterval().toMillis() * getNextMainTtRoundOnStop().getInterval().toMillis());
    }

    @Override
    @Value.Lazy
    public boolean isRound() {
        return getActionType().instanceOf(ActionType.ROUND);
    }

    @Override
    @Value.Lazy
    public boolean isBreak() {
        return getActionType().instanceOf(ActionType.BREAK);
    }

    @Override
    @Value.Lazy
    public Duration getInterval() {
        TtAction previousRound = getPreviousMainRoundOnStop();
        if (previousRound == null) {
            return Duration.ZERO;
        }
        return Duration.between(previousRound.getStartTime(), this.getStartTime());
    }

}
