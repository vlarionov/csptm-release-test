package ru.advantum.csptm.service.ttoperrebuilder.domain.model.actiontype;


import ru.advantum.csptm.service.ttoperrebuilder.domain.model.Entity;

import javax.annotation.Nullable;

public interface ActionType extends Entity<Short> {
    short ROUND = 31;

    short MAIN_ROUND = 1;

    short BREAK = 32;

    short BN_ROUND_BREAK = 24;

    short MIN_TIME = 27;

    short REGULAR_TIME = 28;

    short CORRECTION_TIME = 29;

    short BASE_TIME = 30;

    short DINNER = 22;

    short PAUSE = 23;

    ActionType getParentType();

    @Nullable
    String getActionTypeCode();

    @Nullable
    String getActionTypeName();

    boolean instanceOf(short otherId);

    default boolean isFlexible() {
        return instanceOf(REGULAR_TIME)
                || instanceOf(CORRECTION_TIME)
//                || instanceOf(PAUSE)
                || instanceOf(DINNER);
//                || instanceOf(BASE_TIME)
//                || instanceOf(MIN_TIME);
    }
}
