package ru.advantum.csptm.service.ttoperrebuilder.domain.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.immutables.value.Value;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
public class AbstractEntity<PK> implements Entity<PK> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private PK id;

    @Override
    @Value.Default
    public PK getId() {
        return id;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof Entity)) return false;
//        Entity<?> that = (Entity<?>) o;
//        return Objects.equals(getId(), that.getId());
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(getId());
//    }
//
//    @Override
//    public String toString() {
//        return "AbstractEntity{" +
//                "id=" + id +
//                '}';
//    }
}
