package ru.advantum.csptm.service.ttoperrebuilder.domain.service;


import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtVariant;

import java.time.Instant;

public interface  IntervalImprovementService {

    TtVariant improveIntervals(TtVariant ttVariant);

    TtVariant improveIntervals(TtVariant ttVariant, Instant fromTime);

}
