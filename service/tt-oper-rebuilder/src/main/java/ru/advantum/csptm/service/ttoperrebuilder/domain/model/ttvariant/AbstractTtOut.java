package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant;

import com.google.common.collect.ImmutableSortedSet;
import org.immutables.value.Value;

import javax.persistence.MappedSuperclass;
import java.time.Instant;
import java.util.Comparator;
import java.util.NavigableSet;
import java.util.Set;

import static com.google.common.collect.ImmutableSet.toImmutableSet;
import static com.google.common.collect.ImmutableSortedSet.toImmutableSortedSet;

@Value.Immutable
@MappedSuperclass
public abstract class AbstractTtOut extends AbstractTtVariantEntity<Integer> implements TtOut {


    @Override
    @Value.Lazy
    public Set<TtAction> getTtActions() {
        return getTtVariant().getTtActions().stream()
                .filter(ttAction -> getId().equals(ttAction.getTtOut().getId()))
                .collect(toImmutableSet());
    }

    @Override
    @Value.Lazy
    public NavigableSet<TtAction> getSortedTtActions() {
        return ImmutableSortedSet.copyOf(getTtActions());
    }

    @Override
    @Value.Lazy
    public Set<TtActionItem> getTtActionItems() {
        return getTtActions().stream()
                .flatMap(trAction -> trAction.getTtActionItems().stream())
                .collect(toImmutableSortedSet(Comparator.naturalOrder()));
    }

    @Value.Lazy
    @Override
    public NavigableSet<TtActionItem> getSortedTtActionItems() {
        return ImmutableSortedSet.copyOf(getTtActionItems());
    }

    @Override
    @Value.Lazy
    public Instant getStartTime() {
        return getSortedTtActionItems().first().getStartTime();
    }

    @Override
    @Value.Lazy
    public Instant getEndTime() {
        return getSortedTtActionItems().last().getEndTime();
    }

}
