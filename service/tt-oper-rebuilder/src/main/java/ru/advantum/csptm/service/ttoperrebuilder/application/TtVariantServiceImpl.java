package ru.advantum.csptm.service.ttoperrebuilder.application;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtVariant;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtVariantRepository;
import ru.advantum.csptm.service.ttoperrebuilder.domain.service.IntervalImprovementService;
import ru.advantum.csptm.service.ttoperrebuilder.domain.service.ModeValidationService;

import java.time.Instant;

@Service
@Slf4j
public class TtVariantServiceImpl implements TtVariantService {
    private final TtVariantRepository ttVariantRepository;

    private final IntervalImprovementService intervalImprovementService;

    private final ModeValidationService normValidationService;

    private final IncidentService incidentService;

    @Autowired
    public TtVariantServiceImpl(
            TtVariantRepository ttVariantRepository,
            IntervalImprovementService intervalImprovementService,
            ModeValidationService normValidationService,
            IncidentService incidentService
    ) {
        this.ttVariantRepository = ttVariantRepository;
        this.intervalImprovementService = intervalImprovementService;
        this.normValidationService = normValidationService;
        this.incidentService = incidentService;
    }

    @Transactional
    @Override
    public void improveIntervals(int ttVariantId) {
        try {
            val ttVariant = ttVariantRepository.find(ttVariantId);
            val normViolations = normValidationService.validate(ttVariant);
            if (!normViolations.isEmpty()) {
                incidentService.sendIncidentBundle(normViolations);
                ttVariantRepository.updateCreateStatus(ttVariant.getId(), false);
                return;
            }

            TtVariant improvedTtVariant;
            if (ttVariant.isPlan()) {
                improvedTtVariant = intervalImprovementService.improveIntervals(ttVariant);
            } else {
                improvedTtVariant = intervalImprovementService.improveIntervals(ttVariant, Instant.now());
            }
            ttVariantRepository.save(improvedTtVariant);
            ttVariantRepository.updateCreateStatus(improvedTtVariant.getId(), true);
            incidentService.sendUpdateIncident(improvedTtVariant.getId());
        } catch (Exception e) {
            log.error("", e);
            ttVariantRepository.updateCreateStatus(ttVariantId, false);
        }
    }
}
