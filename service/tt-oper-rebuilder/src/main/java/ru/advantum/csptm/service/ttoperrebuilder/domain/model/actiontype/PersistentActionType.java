package ru.advantum.csptm.service.ttoperrebuilder.domain.model.actiontype;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Entity
@Table(schema = "ttb")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@AttributeOverride(name = "id", column = @Column(name = "action_type_id"))
public class PersistentActionType extends AbstractActionType {
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "parent_type_id", referencedColumnName = "action_type_id")
    @Fetch(FetchMode.JOIN)
    PersistentActionType parentType;

    String actionTypeCode;

    String actionTypeName;

}