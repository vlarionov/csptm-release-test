package ru.advantum.csptm.service.ttoperrebuilder.domain.service;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.actiontype.ActionType;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.mode.ModeProperties;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.mode.ModeViolation;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.mode.ViolationReason;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtAction;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtVariant;

import java.time.Duration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.concat;

@Service
@Slf4j
@EnableConfigurationProperties(ModeProperties.class)
public class ModeValidationServiceImpl implements ModeValidationService {

    private final ModeProperties modeProperties;

    @Autowired
    public ModeValidationServiceImpl(ModeProperties modeProperties) {
        this.modeProperties = modeProperties;
    }

    @Override
    public List<ModeViolation> validate(TtVariant ttVariant) {
        return getViolationStream(ttVariant).collect(toList());
    }

    @Override
    public Stream<ModeViolation> getViolationStream(TtVariant ttVariant) {
        val ttVariantId = ttVariant.getId();
        return concat(
                toModeViolation(
                        ViolationReason.DR_SHIFT_DURATION,
                        ttVariantId,
                        validateTtOutsForDrShiftDuration(ttVariant)
                ),
                concat(
                        toModeViolation(
                                ViolationReason.MAX_DR_SHIFT_DURATION_BEFORE_FIRST_DINNER_OR_PAUSE,
                                ttVariantId,
                                validateTtOutsForMaxDrShiftDurationBeforeFirstDinnerOrPause(ttVariant)
                        ),
                        concat(toModeViolation(
                                ViolationReason.MAX_DR_SHIFT_DURATION_AFTER_LAST_BREAK,
                                ttVariantId,
                                validateTtOutsForMaxDrShiftDurationAfterLastBreak(ttVariant)
                                ),
                                concat(toModeViolation(
                                        ViolationReason.MIN_TOTAL_BN_ROUND_BREAKS_DURATION_BEFORE_FIRST_DINNER_OR_PAUSE,
                                        ttVariantId,
                                        validateTtOutsForMinTotalBrRoundBreaksDurationBeforeFirstDinnerOrPause(ttVariant)
                                        ),
                                        toModeViolation(
                                                ViolationReason.BREAK_DURATION,
                                                ttVariantId,
                                                validateTtOutsForBreakDuration(ttVariant)
                                        )))
                )
        );
    }

    @Override
    public boolean isValid(TtVariant ttVariant) {
        return getViolationStream(ttVariant).findFirst().isPresent();
    }

    private Stream<ModeViolation> toModeViolation(short violationReason, Integer ttVariantId, Set<Integer> ttOutIds) {
        if (ttOutIds.isEmpty()) {
            return Stream.empty();
        }
        return Stream.of(new ModeViolation(new ViolationReason(violationReason), ttVariantId, ttOutIds));
    }


    private Set<Integer> validateTtOutsForDrShiftDuration(TtVariant ttVariant) {
        val result = new HashSet<Integer>();

        for (val driver : ttVariant.getFakeDrivers()) {
            if (driver.getDuration().compareTo(modeProperties.getMaxDrShiftDuration()) > 0) {
                result.add(driver.getTtOut().getId());
            } else if (driver.getDuration().compareTo(modeProperties.getMinDrShiftDuration()) < 0) {
                result.add(driver.getTtOut().getId());
            }
        }

        return result;
    }

    private Set<Integer> validateTtOutsForMaxDrShiftDurationBeforeFirstDinnerOrPause(TtVariant ttVariant) {
        val result = new HashSet<Integer>();

        for (val driver : ttVariant.getFakeDrivers()) {
            val firstDinnerOrPause = driver.getFirstDinnerOrPause();
            if (firstDinnerOrPause == null) {
                continue;
            }

            val durationBeforeFirstDinnerOrPause = driver.getDurationBefore(firstDinnerOrPause.getStartTime());
            if (durationBeforeFirstDinnerOrPause.compareTo(modeProperties.getMaxDrShiftDurationBeforeFirstDinnerOrPause()) > 0) {
                result.add(driver.getTtOut().getId());
            }
        }

        return result;
    }

    private Set<Integer> validateTtOutsForMaxDrShiftDurationAfterLastBreak(TtVariant ttVariant) {
        val result = new HashSet<Integer>();

        for (val driver : ttVariant.getFakeDrivers()) {
            if (driver.getDrShiftDurationAfterLastBreak().compareTo(modeProperties.getMaxDrShiftDurationAfterLastBreak()) > 0) {
                result.add(driver.getTtOut().getId());
            }
        }

        return result;
    }

    private Set<Integer> validateTtOutsForMinTotalBrRoundBreaksDurationBeforeFirstDinnerOrPause(TtVariant ttVariant) {
        val result = new HashSet<Integer>();

        for (val driver : ttVariant.getFakeDrivers()) {
            val firstDinnerOrPause = driver.getFirstDinnerOrPause();
            if (firstDinnerOrPause == null) {
                continue;
            }
            val durationBeforeFirstDinnerOrPause = Duration.between(driver.getStartTime(), firstDinnerOrPause.getStartTime());
            if (durationBeforeFirstDinnerOrPause.compareTo(modeProperties.getMaxDurationBeforeFirstDinnerOrPauseForCheckTotalBnRoundBreaksBefore()) < 0) {
                continue;
            }
            driver.getSortedBnRoundBreaks().headSet(firstDinnerOrPause).stream()
                    .map(TtAction::getDuration)
                    .reduce(Duration::plus)
                    .filter(totalDuration -> totalDuration.compareTo(modeProperties.getMinTotalBnRoundBreaksDurationBeforeFirstDinnerOrPause()) < 0)
                    .ifPresent(totalDuration -> result.add(driver.getTtOut().getId()));
        }

        return result;
    }

    private Set<Integer> validateTtOutsForBreakDuration(TtVariant ttVariant) {
        val result = new HashSet<Integer>();

        for (val driver : ttVariant.getFakeDrivers()) {
            driver.getSortedBreaks().stream()
                    .filter(ttAction -> ttAction.getActionType().instanceOf(ActionType.DINNER))
                    .forEach(dinner -> {
                        if (dinner.getDuration().compareTo(modeProperties.getMaxDinnerDuration()) > 0) {
                            result.add(driver.getTtOut().getId());
                        }
                        if (dinner.getDuration().compareTo(modeProperties.getMinDinnerDuration()) < 0) {
                            result.add(driver.getTtOut().getId());
                        }
                    });

            driver.getSortedBreaks().stream()
                    .filter(ttAction -> ttAction.getActionType().instanceOf(ActionType.PAUSE))
                    .map(TtAction::getDuration)
                    .reduce(Duration::plus)
                    .filter(pausesDuration -> pausesDuration.compareTo(modeProperties.getMaxTotalPauseDuration()) > 0)
                    .ifPresent(pausesDuration -> result.add(driver.getTtOut().getId()));
        }

        return result;
    }

}
