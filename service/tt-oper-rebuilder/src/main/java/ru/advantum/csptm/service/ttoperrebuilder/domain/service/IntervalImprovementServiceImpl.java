package ru.advantum.csptm.service.ttoperrebuilder.domain.service;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtAction;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtActionItem;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtVariant;

import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import static java.time.Duration.ofMillis;
import static java.time.Duration.ofMinutes;
import static java.util.stream.Collectors.toList;

@Service
@Slf4j
public class IntervalImprovementServiceImpl implements IntervalImprovementService {

    private final TtVariantChangeService ttVariantService;

    private final ModeValidationService modeValidationService;

    private final RandomDataGenerator random = new RandomDataGenerator();

    @Autowired
    public IntervalImprovementServiceImpl(TtVariantChangeService ttVariantService, ModeValidationService modeValidationService) {
        this.ttVariantService = ttVariantService;
        this.modeValidationService = modeValidationService;
    }

    @Override
    public TtVariant improveIntervals(TtVariant ttVariant) {
        return improveIntervals(ttVariant, ttVariant.getStartTime());
    }

    @Override
    public TtVariant improveIntervals(TtVariant ttVariant, Instant afterTime) {
        ttVariant = ttVariantService.recalculateRoundDurations(ttVariant);
        ttVariant = ttVariantService.supplementBreaks(ttVariant);
        TtVariant bestTtVariant = ttVariant;
        int maxIteration = 100;
        for (Iteration iteration = new Iteration(0, maxIteration, new ConcurrentHashMap<>());
             iteration.getNumber() < iteration.getMaxNumber();
             iteration = new Iteration(iteration.number + 1, maxIteration, iteration.getFlexibleItemStats())
                ) {
            List<Long> flexibleItemIds = bestTtVariant.getSortedTtActionItems().stream()
                    .filter(TtActionItem::isFlexible)
                    .sorted()
                    .map(TtActionItem::getId)
                    .collect(toList());
            try {
                bestTtVariant = iterateAndCheck(bestTtVariant, flexibleItemIds, iteration);
                for (Long flexibleItemId : flexibleItemIds) {
                    bestTtVariant = iterateAndCheck(bestTtVariant, Collections.singletonList(flexibleItemId), iteration);
                }
                log.info("Iteration {}, deviation {}", iteration.getNumber(), getIntervalDeviation(bestTtVariant));
            } catch (Exception e) {
                log.error("", e);
            }
        }
        log.info("intervals improved");
        return bestTtVariant;
    }


    private TtVariant iterateAndCheck(TtVariant currentTtVariant, List<Long> targetItemIds, Iteration iteration) {
        val newTtVariant = iterate(currentTtVariant, targetItemIds, iteration);
        val isModeValid = modeValidationService.isValid(newTtVariant);
        if (isNewBetter(currentTtVariant, newTtVariant, iteration) && isModeValid) {
            iteration.getFlexibleItemStats().clear();
            return newTtVariant;
        } else {
            return currentTtVariant;
        }
    }

    private TtVariant iterate(TtVariant currentTtVariant, List<Long> targetItemIds, Iteration iteration) {
        TtVariant result = currentTtVariant;

        for (Long flexibleItemId : targetItemIds) {
            val flexibleItem = result.getTtActionItemIdMap().get(flexibleItemId);
            result = changeDurationOptimal(flexibleItem, iteration);
        }
        return result;
    }

    private TtVariant changeDurationOptimal(TtActionItem flexibleItem, Iteration iteration) {
        val oldTtVariant = flexibleItem.getTtVariant();
        val oldDuration = flexibleItem.getDuration();
        val newDuration = getOptimalDuration(flexibleItem, iteration);

        val isNotChanged = newDuration.equals(oldDuration);
        if (isNotChanged) {
            return oldTtVariant;
        }

        boolean isOldValid = modeValidationService.isValid(flexibleItem.getTtVariant());
        val newTtVariant = ttVariantService.changeDuration(flexibleItem, newDuration);
        if (modeValidationService.isValid(newTtVariant)) {
            return newTtVariant;
        } else {
            if (isOldValid) {
                iteration.getFlexibleItemStats().computeIfAbsent(flexibleItem.getId(), (id) -> new AtomicInteger(1)).incrementAndGet();
            }
            return newTtVariant;// flexibleItem.getTtVariant();
        }
    }

    private Duration getOptimalDuration(TtActionItem flexibleItem, Iteration iteration) {
        TtAction nextRound = flexibleItem.getTtAction().getMainNextRound();
        if (nextRound == null) {
            return flexibleItem.getDuration();
        }
        Duration intervalDiff = nextRound.getIntervalDiff();
        double mu = 1;
        double sigma = 0.25 * (iteration.getMaxNumber() - iteration.getNumber()) / iteration.getMaxNumber();
        val itemViolations = iteration.flexibleItemStats.get(flexibleItem.getId());
        if (itemViolations != null && itemViolations.get() != 0) {
            mu = mu / itemViolations.get();
            sigma = sigma / itemViolations.get();
        }
        double randomMultiplier = random.nextGaussian(mu, sigma);
        Duration randomIntervalDiff = ofMillis((long) (intervalDiff.toMillis() * randomMultiplier));
        randomIntervalDiff = ofMinutes(randomIntervalDiff.toMinutes());

        Duration newDuration = flexibleItem.getDuration().plus(randomIntervalDiff);
        if (newDuration.isNegative()) {
            newDuration = Duration.ZERO;
        }
        val nextRoundOnStop = nextRound.getNextMainTtRoundOnStop();
        if (nextRoundOnStop != null) {
            if (nextRound.getStartTime().plus(newDuration).minus(flexibleItem.getDuration()).compareTo(nextRoundOnStop.getStartTime()) > 0) {
                newDuration = flexibleItem.getDuration();
            }
        }

        return newDuration;
    }

    private boolean isNewBetter(TtVariant oldTtVariant, TtVariant newTtVariant, Iteration iteration) {
        val newLastItem = newTtVariant.getSortedTtActionItems().last();
        if (newLastItem.getEndTime().compareTo(oldTtVariant.getEndTime()) > 0) {
            log.info("last round violation");
            newLastItem.getTtOut().getTtActionItems().stream()
                    .filter(TtActionItem::isFlexible)
                    .forEach(ttActionItem -> iteration.flexibleItemStats.computeIfAbsent(
                            ttActionItem.getId(),
                            (o) -> new AtomicInteger(1)).updateAndGet(i -> 2 * i)
                    );

            return false;
        }

        val newBetter = getIntervalDeviation(newTtVariant) < getIntervalDeviation(oldTtVariant);
        if (newBetter) {
            log.info("newBetter");
        }
        return newBetter;
    }

    private double getIntervalDeviation(TtVariant ttVariant) {
        return ttVariant.getSortedRounds().stream()
                .filter(ttAction -> ttAction.getFirstTtActionItem().getStopItem().isTerminal())
                .mapToDouble(round -> {
                    long interval = round.getInterval().toMillis();
                    val nextRound = round.getNextMainTtRoundOnStop();
                    if (nextRound != null) {
                        interval = interval * nextRound.getInterval().toMillis();
                    }
                    if (interval == 0) {
                        return 0;
                    }
                    return Math.pow(Math.abs(Math.pow(round.getIntervalDiff().toMillis(), 2) / (interval)), 0.5);
                })
                .sum();
    }

    @lombok.Value
    private static class Iteration {
        int number, maxNumber;

        Map<Long, AtomicInteger> flexibleItemStats;

    }

}
