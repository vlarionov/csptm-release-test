package ru.advantum.csptm.service.ttoperrebuilder.domain.service;


import ru.advantum.csptm.service.ttoperrebuilder.domain.model.mode.ModeViolation;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtVariant;

import java.util.List;
import java.util.stream.Stream;

public interface ModeValidationService {

    List<ModeViolation> validate(TtVariant ttVariant);

    Stream<ModeViolation> getViolationStream(TtVariant ttVariant);

    boolean isValid(TtVariant newTtVariant);
}
