package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant;


import ru.advantum.csptm.service.ttoperrebuilder.domain.model.Entity;

public interface TtVariantEntity<PK> extends Entity<PK> {

    TtVariant getTtVariant();

}
