package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem2round;

import org.immutables.value.Value;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.AbstractTtVariantEntity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem.StopItem;

@Value.Immutable
public abstract class AbstractStopItem2Round  extends AbstractTtVariantEntity<Long> implements StopItem2Round {

    public abstract Integer getStopItemId();

    @Override
    @Value.Lazy
    public StopItem getStopItem() {
        return getTtVariantContext().getStopItems().get(getStopItemId());
    }

}
