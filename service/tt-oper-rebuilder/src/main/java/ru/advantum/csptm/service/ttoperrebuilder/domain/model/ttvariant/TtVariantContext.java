package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant;


import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.Entity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.driver.FakeDriver;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem.StopItem;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem2round.StopItem2Round;

import java.util.Collection;
import java.util.Map;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

/**
 * IoC container
 */
public class TtVariantContext {

    private Supplier<TtVariant> ttVariant;

    private Supplier<Map<Integer, TtOut>> ttOuts;

    private Supplier<Map<Integer, TtAction>> ttActions;

    private Supplier<Map<Long, TtActionItem>> ttActionItems;

    private Supplier<Map<Integer, StopItem>> stopItems;

    private Supplier<Map<Long, StopItem2Round>> stopItem2Rounds;

    private Supplier<Map<Integer, FakeDriver>> drivers;

    public TtVariantContext(
            Supplier<TtVariant> ttVariant,
            Supplier<Collection<? extends TtOut>> ttOuts,
            Supplier<Collection<? extends TtAction>> ttActions,
            Supplier<Collection<? extends TtActionItem>> ttActionItems,
            Supplier<Collection<? extends StopItem>> stopItems,
            Supplier<Collection<? extends StopItem2Round>> stopItem2Rounds,
            Supplier<Collection<? extends FakeDriver>> drivers
    ) {
        this.ttVariant = Suppliers.memoize(ttVariant);
        this.ttOuts = toIdMap(ttOuts);
        this.ttActions = toIdMap(ttActions);
        this.ttActionItems = toIdMap(ttActionItems);
        this.stopItems = toIdMap(stopItems);
        this.stopItem2Rounds = toIdMap(stopItem2Rounds);
        this.drivers = toIdMap(drivers);
    }

    private <K, V extends Entity<K>> Supplier<Map<K, V>> toIdMap(Supplier<Collection<? extends V>> values) {
        return Suppliers.memoize(() -> values.get().stream().distinct().collect(toMap(Entity::getId, identity())));
    }

    public TtVariant getTtVariant() {
        return ttVariant.get();
    }

    public Map<Integer, TtOut> getTtOuts() {
        return ttOuts.get();
    }

    public Map<Integer, TtAction> getTtActions() {
        return ttActions.get();
    }

    public Map<Long, TtActionItem> getTtActionItems() {
        return ttActionItems.get();
    }

    public Map<Integer, StopItem> getStopItems() {
        return stopItems.get();
    }

    public Map<Long, StopItem2Round> getStopItem2Rounds() {
        return stopItem2Rounds.get();
    }

    public Map<Integer, FakeDriver> getFakeDrivers() {
        return drivers.get();
    }

}
