package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant;

public interface TtVariantRepository {
    TtVariant find(int id);

    void save(TtVariant routeTT);

    Long generateTtActionItemId();

    void updateCreateStatus(int ttVariantId, boolean isSuccess);
}
