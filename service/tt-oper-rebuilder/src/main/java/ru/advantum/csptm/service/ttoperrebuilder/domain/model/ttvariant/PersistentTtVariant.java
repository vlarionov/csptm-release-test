package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.val;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Where;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.stoptype.StopType;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.driver.AbstractFakeDriver;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.driver.FakeDriver;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem.AbstractStopItem;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem.StopItem;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem2round.AbstractStopItem2Round;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem2round.StopItem2Round;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Entity
@Table(schema = "ttb")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PersistentTtVariant extends AbstractTtVariant {
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "ttVariant", targetEntity = PersistentTtOut.class)
    @Fetch(FetchMode.JOIN)
    @Where(clause = "not sign_deleted")
    Set<TtOut> ttOuts;

    Integer normId;

    LocalDate orderDate;

    @Transient
    TtVariantContext context;

    @Override
    public Instant getEndTime() {
        return getSortedTtActionItems().last().getEndTime();
    }

    @PostLoad
    public void init() {
        TtVariant ttVariant = this;
        val ttOuts = this.ttOuts;
        val ttActions = ttOuts.stream().flatMap(ttOut -> ttOut.getTtActions().stream()).collect(toList());
        val ttActionItems = ttActions.stream().flatMap(ttAction -> ttAction.getTtActionItems().stream()).collect(toList());
        List<StopItem2Round> stopItem2Rounds = ttActionItemStream()
                .map(TtActionItem::getStopItem2Round)
                .distinct()
                .map(stopItem2Round -> new AbstractStopItem2Round() {

                    @Override
                    public Integer getStopItemId() {
                        return stopItem2Round.getStopItem().getId();
                    }

                    @Override
                    public Long getId() {
                        return stopItem2Round.getId();
                    }

                    @Override
                    public TtVariantContext getTtVariantContext() {
                        return ttVariant.getContext();
                    }

                    @Override
                    public TtVariant getTtVariant() {
                        return ttVariant;
                    }

                })
                .collect(toList());
        List<StopItem> stopItems = ttActionItemStream()
                .map(TtActionItem::getStopItem2Round)
                .map(StopItem2Round::getStopItem)
                .distinct()
                .map(stopItem -> new AbstractStopItem() {
                    @Override
                    public Set<StopType> getStopTypes() {
                        return stopItem.getStopTypes();
                    }

                    @Override
                    public Integer getId() {
                        return stopItem.getId();
                    }

                    @Override
                    public TtVariantContext getTtVariantContext() {
                        return ttVariant.getContext();
                    }

                    @Override
                    public TtVariant getTtVariant() {
                        return ttVariant;
                    }

                })
                .collect(toList());
        List<FakeDriver> fakeDrivers = ttActionItemStream()
                .map(FakeDriver::getFakeDriverId)
                .filter(Objects::nonNull)
                .distinct()
                .map(driverId -> new AbstractFakeDriver() {
                    @Override
                    public Integer getId() {
                        return driverId;
                    }

                    @Override
                    public TtVariantContext getTtVariantContext() {
                        return ttVariant.getContext();
                    }

                    @Override
                    public TtVariant getTtVariant() {
                        return ttVariant;
                    }

                })
                .collect(toList());

        this.context = new TtVariantContext(
                () -> this,
                () -> ttOuts,
                () -> ttActions,
                () -> ttActionItems,
                () -> stopItems,
                () -> stopItem2Rounds,
                () -> fakeDrivers
        );
    }

    private Stream<PersistentTtActionItem> ttActionItemStream() {
        return ttOuts.stream()
                .flatMap(ttOut -> ttOut.getTtActions().stream())
                .flatMap(ttAction -> ttAction.getTtActionItems().stream().map(PersistentTtActionItem.class::cast));
    }

}
