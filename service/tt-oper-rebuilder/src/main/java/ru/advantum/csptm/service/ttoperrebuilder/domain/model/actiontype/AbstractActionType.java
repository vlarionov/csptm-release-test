package ru.advantum.csptm.service.ttoperrebuilder.domain.model.actiontype;

import org.immutables.value.Value;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.AbstractEntity;

import javax.persistence.MappedSuperclass;

@Value.Immutable
@MappedSuperclass
public abstract class AbstractActionType extends AbstractEntity<Short> implements ActionType {

    @Override
    public boolean instanceOf(short otherId) {
        return getId() == otherId || getParentType() != null && getParentType().instanceOf(otherId);
    }

}
