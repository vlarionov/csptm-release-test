package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant;


import ru.advantum.csptm.service.ttoperrebuilder.domain.model.Entity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.driver.FakeDriver;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem.StopItem;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedSet;

public interface TtVariant extends Entity<Integer> {
    LocalDate getOrderDate();

    boolean isPlan();

    ZoneOffset getTimeZoneOffset();

    Instant getStartTime();

    Instant getEndTime();

    Integer getNormId();

    Set<TtOut> getTtOuts();

    Set<TtAction> getTtActions();

    SortedSet<TtAction> geSortedTtActions();

    Set<TtActionItem> getTtActionItems();

    NavigableSet<TtActionItem> getSortedTtActionItems();

    Set<StopItem> getStopItems();

    Set<FakeDriver> getFakeDrivers();

    SortedSet<TtAction> getSortedRounds();

    TtAction getRoundWithMaxInterval();

    Duration getMaxInterval();

    TtAction getLastRound();

    SortedSet<TtAction> getTtActionsOfLastRound();

    TtAction findRoundWithMaxInterval(Set<TtAction> excludedRounds);

    Map<Long, TtActionItem> getTtActionItemIdMap();

    Map<Integer, Set<TtActionItem>> getTtActionTtActionItemsMap();

    TtVariantContext getContext();

//    UnivariateRealFunction getSplineFunction();
//
//    double getAvgInterval();
//
//    double getIntervalDispersion();

//    double getAvgInterval();
//
//    double getIntervalDeviation();



//    Map<Short, DrShift> getDrShiftMap();

//    Map<Integer, TtOut> getTtOutIdMap();
//
//    Map<Integer, TtAction> getTtActionMap();
//
//    Map<Short, ActionType> getActionTypeIdMap();


//    Map<Integer, StopItem> getStopItemIdMap();


//    Map<Long, TtActionItem> getTtActionItemIdMap();
}
