package ru.advantum.csptm.service.ttoperrebuilder.application;

import com.fatboyindustrial.gsonjavatime.Converters;
import com.google.gson.GsonBuilder;
import lombok.val;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.incident.model.Incident;
import ru.advantum.csptm.incident.model.IncidentBundle;
import ru.advantum.csptm.incident.model.IncidentPayload;
import ru.advantum.csptm.incident.model.IncidentType;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.mode.ModeViolation;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.springframework.amqp.core.MessageProperties.CONTENT_TYPE_JSON;

@Service
public class IncidentServiceImpl implements IncidentService {
    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public IncidentServiceImpl(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
        rabbitTemplate.setExchange(Incident.INCIDENT_ROUTER_EXCHANGE);
        rabbitTemplate.setBeforePublishPostProcessors(message -> {
            message.getMessageProperties().setContentType(CONTENT_TYPE_JSON);
            message.getMessageProperties().setContentEncoding(StandardCharsets.UTF_8.name());
            message.getMessageProperties().setType(IncidentBundle.class.getSimpleName());
            return message;
        });
    }

    @Override
    public void sendUpdateIncident(Integer ttVariantId) {
        val type = IncidentType.TYPE.TIMETABLE_UPDATE.get();
        val payload = IncidentPayload.builder()
                .ttVariantList(Collections.singletonList(ttVariantId))
                .build();
        val initiatorId = Incident.INITIATOR.SYSTEM.get();
        val incidentStatus = Incident.STATUS.NEW.get();
        val incident = Incident.builder()
                .incidentInitiatorId(initiatorId)
                .timeStart(Instant.now())
                .incidentStatusId(incidentStatus)
                .incidentTypeId(type)
                .incidentPayload(payload)
                .build();
        val bundle = IncidentBundle.builder().incidentBundle(Collections.singletonList(incident)).build();
        val gson = Converters.registerAll(new GsonBuilder()).create();
        val bundleString = gson.toJson(bundle);
        rabbitTemplate.convertAndSend(String.valueOf(type), bundleString);
    }

    @Override
    public void sendIncidentBundle(List<ModeViolation> normViolations) {
        val incidentBundle = IncidentBundle.builder().incidentBundle(toIncidentList(normViolations)).build();
        val gson = Converters.registerAll(new GsonBuilder()).create();
        val bundleString = gson.toJson(incidentBundle);
        rabbitTemplate.convertAndSend(String.valueOf(IncidentType.TYPE.MODE_VIOLATION.get()), bundleString);
    }

    private List<Incident> toIncidentList(List<ModeViolation> normViolations) {
        return normViolations.stream()
                .map(this::toIncident)
                .collect(toList());
    }

    private Incident toIncident(ModeViolation normViolation) {
        val payload = IncidentPayload.builder()
                .ttVariantList(singletonList(normViolation.getTtVariantId()))
                .ttOutList(new ArrayList<>(normViolation.getTtOutIds()))
                .incidentReason(normViolation.getReason().getId())
                .build();
        return Incident.builder()
                .incidentTypeId(IncidentType.TYPE.MODE_VIOLATION.get())
                .incidentStatusId(Incident.STATUS.NEW.get())
                .incidentInitiatorId(Incident.INITIATOR.SYSTEM.get())
                .timeStart(Instant.now())
                .incidentPayload(payload)
                .build();
    }
}
