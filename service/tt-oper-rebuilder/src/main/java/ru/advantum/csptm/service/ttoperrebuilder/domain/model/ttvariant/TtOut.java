package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant;

import java.time.Instant;
import java.util.Collection;
import java.util.NavigableSet;

public interface TtOut extends TtVariantEntity<Integer>, TimedAction {

    Collection<TtAction> getTtActions();

    NavigableSet<TtAction> getSortedTtActions();

    Instant getStartTime();

    Collection<TtActionItem> getTtActionItems();

    NavigableSet<TtActionItem> getSortedTtActionItems();

    int getTtOutNum();
}
