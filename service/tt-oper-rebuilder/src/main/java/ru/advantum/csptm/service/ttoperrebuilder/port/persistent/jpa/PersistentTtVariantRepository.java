package ru.advantum.csptm.service.ttoperrebuilder.port.persistent.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.PersistentTtVariant;

@Repository
public interface PersistentTtVariantRepository extends JpaRepository<PersistentTtVariant, Integer> {


}
