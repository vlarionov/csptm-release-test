package ru.advantum.csptm.service.ttoperrebuilder.application;

import ru.advantum.csptm.service.ttoperrebuilder.domain.model.mode.ModeViolation;

import java.util.List;

public interface IncidentService {
    void sendUpdateIncident(Integer ttVariantId);

    void sendIncidentBundle(List<ModeViolation> normViolations);
}
