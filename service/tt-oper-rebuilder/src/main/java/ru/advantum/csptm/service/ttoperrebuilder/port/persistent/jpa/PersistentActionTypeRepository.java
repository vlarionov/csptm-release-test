package ru.advantum.csptm.service.ttoperrebuilder.port.persistent.jpa;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.actiontype.PersistentActionType;

@RepositoryRestResource
public interface PersistentActionTypeRepository extends JpaRepository<PersistentActionType, Short> {
    String CACHE_NAME = "actionTypes";

    @Cacheable(CACHE_NAME)
    @Override
    PersistentActionType findOne(Short id);
}
