package ru.advantum.csptm.service.ttoperrebuilder.application;

import java.time.Duration;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.Optional;

public interface NormService {

    Optional<Duration> getDuration(int normId, int startStopItemId, int endStopItemId, LocalTime hour, ZoneOffset zoneOffset);

}
