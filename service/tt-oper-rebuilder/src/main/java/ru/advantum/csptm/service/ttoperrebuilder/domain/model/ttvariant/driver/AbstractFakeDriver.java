package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.driver;

import com.google.common.collect.ImmutableSortedSet;
import org.immutables.value.Value;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.actiontype.ActionType;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.AbstractTtVariantEntity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtAction;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtActionItem;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtOut;

import javax.annotation.Nullable;
import javax.persistence.MappedSuperclass;
import java.time.Duration;
import java.time.Instant;
import java.util.Set;
import java.util.SortedSet;

import static com.google.common.collect.ImmutableSortedSet.toImmutableSortedSet;
import static java.util.Comparator.naturalOrder;
import static java.util.stream.Collectors.toSet;

@Value.Immutable
@MappedSuperclass
public abstract class AbstractFakeDriver extends AbstractTtVariantEntity<Integer> implements FakeDriver {

    @Override
    @Value.Lazy
    public Instant getStartTime() {
        return getSortedTtActions().first().getStartTime();
    }

    @Override
    @Value.Lazy
    public Instant getEndTime() {
        return getSortedTtActions().last().getEndTime();
    }

    @Override
    @Value.Lazy
    public Duration getDuration() {
        return getTtActions().stream()
                .filter(this::drShiftDurationTtActionFilter)
                .map(TtAction::getDuration)
                .reduce(Duration::plus).orElse(null);
    }

    @Override
    public Duration getDurationBefore(Instant toInstant) {
        return getTtActions().stream()
                .filter(ttAction -> ttAction.getEndTime().compareTo(toInstant) <= 0)
                .filter(this::drShiftDurationTtActionFilter)
                .map(TtAction::getDuration)
                .reduce(Duration::plus).orElse(null);
    }

    private boolean drShiftDurationTtActionFilter(TtAction ttAction) {
        return  !(ttAction.getActionType().instanceOf(ActionType.BREAK) &&
                ttAction.getDuration().compareTo(Duration.ofMinutes(30)) > 0);
    }

    //todo fix if drShift cross out
    @Override
    @Value.Lazy
    public TtOut getTtOut() {
        return getTtVariant().getTtActionItems().stream()
                .filter(this::contains)
                .findAny()
                .map(TtActionItem::getTtOut).orElse(null);
    }

    @Override
    @Value.Lazy
    public Set<TtAction> getTtActions() {
        return getTtOut().getTtActionItems().stream()
                .filter(this::contains)
                .map(TtActionItem::getTtAction)
                .distinct()
                .collect(toSet());
    }

    @Override
    @Value.Lazy
    public SortedSet<TtAction> getSortedTtActions() {
        return getTtActions().stream().collect(toImmutableSortedSet(naturalOrder()));
    }

    @Override
    @Value.Lazy
    public ImmutableSortedSet<TtAction> getSortedBreaks() {
        return getTtActions().stream()
                .filter(TtAction::isBreak)
                .collect(toImmutableSortedSet(naturalOrder()));
    }

    @Override
    @Value.Lazy
    public SortedSet<TtAction> getSortedBnRoundBreaks() {
        return getSortedBreaks().stream()
                .filter(ttAction -> ttAction.getActionType().instanceOf(ActionType.BN_ROUND_BREAK))
                .collect(toImmutableSortedSet(naturalOrder()));
    }

    @Override
    @Value.Lazy
    @Nullable
    public TtAction getFirstDinnerOrPause() {
        return getSortedBreaks().stream()
                .filter(trAction -> trAction.getActionType().instanceOf(ActionType.DINNER) || trAction.getActionType().instanceOf(ActionType.PAUSE))
                .findFirst().orElse(null);
    }

    @Override
    @Value.Lazy
    public TtAction getLastBreak() {
        return getSortedBreaks().last();
    }

    @Override
    @Value.Lazy
    public Duration getDrShiftDurationAfterLastBreak() {
        return getSortedBreaks().tailSet(getLastBreak()).stream()
                .filter(this::drShiftDurationTtActionFilter)
                .map(TtAction::getDuration)
                .reduce(Duration::plus).orElse(null);
    }

    private boolean contains(TtActionItem ttActionItem) {
        return getId().equals(FakeDriver.getFakeDriverId(ttActionItem));
    }

}
