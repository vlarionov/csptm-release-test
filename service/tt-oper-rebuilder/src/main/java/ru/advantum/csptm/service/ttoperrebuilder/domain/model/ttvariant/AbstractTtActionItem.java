package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.immutables.value.Value;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem.StopItem;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem2round.StopItem2Round;

import javax.annotation.Nullable;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.time.Duration;
import java.time.Instant;
import java.util.Optional;
import java.util.function.Function;

@Value.Immutable
@Slf4j
@MappedSuperclass
public abstract class AbstractTtActionItem extends AbstractTtVariantEntity<Long> implements TtActionItem {

    public abstract Long getStopItem2RoundId();

    public abstract Integer getTtActionId();

    @Nullable
    public abstract Integer getDriverId();

    public abstract Short getDrShiftId();

    @Nullable
    public abstract Long getNextTtActionItemId();

    @Nullable
    public abstract Long getPreviousTtActionItemId();

    @Override
    @Transient
    @Value.Lazy
    public StopItem2Round getStopItem2Round() {
        return getTtVariantContext().getStopItem2Rounds().get(getStopItem2RoundId());
    }

    @Value.Auxiliary
    @Transient
    public abstract Function<TtActionItem, Instant> startTimeProvider();

    @Value.Auxiliary
    @Value.Default
    public Function<TtActionItem, Duration> durationProvider() {
        return (ttActionItem) -> {
            Instant endTime = Optional.ofNullable(ttActionItem.getNextTtActionItem()).orElse(ttActionItem).getStartTime();
            return Duration.between(ttActionItem.getStartTime(), endTime);
        };
    }

    @Override
    @Value.Lazy
    public TtAction getTtAction() {
        return getTtVariantContext().getTtActions().get(getTtActionId());
    }

    @Override
    @Value.Lazy
    public Instant getStartTime() {
        return startTimeProvider().apply(this);
    }

    @Override
    @Value.Lazy
    public Duration getDuration() {
        return durationProvider().apply(this);
    }

    @Override
    @Value.Lazy
    @Nullable
    public TtActionItem getPreviousTtActionItem() {
        return getTtVariantContext().getTtActionItems().get(getPreviousTtActionItemId());
    }

    @Override
    @Value.Lazy
    @Nullable
    public TtActionItem getNextTtActionItem() {
        return getTtVariantContext().getTtActionItems().get(getNextTtActionItemId());
    }

    @Override
    @Value.Lazy
    public TtOut getTtOut() {
        return getTtAction().getTtOut();
    }

    @Override
    @Value.Lazy
    public StopItem getStopItem() {
        return getStopItem2Round().getStopItem();
    }

    @Override
    @Value.Lazy
    public boolean isFlexible() {
        boolean isFlexibleType = getActionType().isFlexible() || getTtAction().getActionType().isFlexible();
        boolean isStopFloatingBreakDuration = getStopItem().isFloatingBreakDuration();
        return isFlexibleType && isStopFloatingBreakDuration;
    }

//    @Override
//    public int compareTo(TimedAction other) {
//        val startTimeDiff = this.getStartTime().compareTo(other.getStartTime());
//        if (startTimeDiff != 0) {
//            return startTimeDiff;
//        }
//        val durationDiff = this.getDuration().compareTo(other.getDuration());
//        if (durationDiff != 0) {
//            return durationDiff;
//        }
//
//        TtActionItem self = this;
//        TtActionItem otherEntity = (TtActionItem) other;
//        TtActionItem next = this.getNextTtActionItem();
//        while (next != null) {
//            if (next.getId().equals(otherEntity.getId())) {
//                return -1;
//            }
//            next = next.getNextTtActionItem();
//        }
//
//        return self.getId().intValue() - otherEntity.getId().intValue();
//    }

    @Override
    public int compareTo(TimedAction other) {
        TtActionItem self = this;
        TtActionItem otherEntity = (TtActionItem) other;
        if (self.getId().equals(otherEntity.getId())) {
            return 0;
        }

        val startTimeDiff = self.getStartTime().compareTo(otherEntity.getStartTime());
        if (startTimeDiff != 0) {
            return startTimeDiff;
        }
        val durationDiff = self.getDuration().compareTo(otherEntity.getDuration());
        if (durationDiff != 0) {
            return durationDiff;
        }

        if (self.getTtOut().getId().equals(otherEntity.getTtOut().getId())) {
            TtActionItem next = this.getNextTtActionItem();
            while (next != null) {
                if (next.getId().equals(otherEntity.getId())) {
                    return -1;
                }
                next = next.getNextTtActionItem();
            }
            //return 1;
        }

        return self.getId().intValue() - otherEntity.getId().intValue();
    }
}