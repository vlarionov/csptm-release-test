package ru.advantum.csptm.service.ttoperrebuilder.domain.factory;

import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtVariant;

public interface TtVariantFactory {
    TtVariant build();
}
