package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant;

import org.immutables.value.Value;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.AbstractEntity;

import javax.persistence.MappedSuperclass;


@MappedSuperclass
public abstract class AbstractTtVariantEntity<PK> extends AbstractEntity<PK> {
    @Value.Auxiliary
    public abstract TtVariantContext getTtVariantContext();

    @Value.Lazy
    public TtVariant getTtVariant() {
        return getTtVariantContext().getTtVariant();
    }
}
