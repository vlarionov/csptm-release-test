package ru.advantum.csptm.service.ttoperrebuilder.domain.service;


import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtActionItem;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtVariant;

import java.time.Duration;

public interface TtVariantChangeService {

    TtVariant supplementBreaks(TtVariant ttVariant);

    TtVariant recalculateRoundDurations(TtVariant ttVariant);

    TtVariant changeDuration(TtActionItem ttActionItem, Duration newDuration);

}
