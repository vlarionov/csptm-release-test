package ru.advantum.csptm.service.ttoperrebuilder.application;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.norm.NormBetweenStop;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.norm.NormRepository;

import java.time.Duration;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@Service
@Slf4j
public class NormServiceImpl implements NormService {

    private final NormRepository normRepository;

    @Autowired
    public NormServiceImpl(NormRepository normRepository) {
        this.normRepository = normRepository;
    }

    @Override
    public Optional<Duration> getDuration(int normId, int startStopItemId, int endStopItemId, LocalTime hour, ZoneOffset zoneOffset) {
        if (startStopItemId == endStopItemId) {
            return Optional.of(Duration.ZERO);
        }

        hour = hour.plusHours(3).minusSeconds(zoneOffset.getTotalSeconds());
        val normMap  = normRepository.findNormNavigableMap(normId);
        val normKey = new NormRepository.NormKey(startStopItemId, endStopItemId);
        val hourKey = new NormRepository.HourKey(hour, hour);
        val result = ofNullable(normMap.get(normKey))
                .map(navigableMap -> navigableMap.floorEntry(hourKey))
                .map(Map.Entry::getValue)
                .map(NormBetweenStop::getDuration);

        if (!result.isPresent()) {
            log.warn("norm not found, normId {}, startStopItemId {}, endStopItemId, {}, hour {}", normId, startStopItemId, endStopItemId, hour);
        }

        return result;
    }

}
