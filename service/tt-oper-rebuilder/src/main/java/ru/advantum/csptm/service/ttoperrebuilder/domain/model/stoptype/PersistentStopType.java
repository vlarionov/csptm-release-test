package ru.advantum.csptm.service.ttoperrebuilder.domain.model.stoptype;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "rts")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PersistentStopType extends AbstractEntity<Short> implements StopType {

}