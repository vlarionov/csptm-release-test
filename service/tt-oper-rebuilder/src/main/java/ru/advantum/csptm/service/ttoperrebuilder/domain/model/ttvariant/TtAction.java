package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant;

import org.immutables.value.Value;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.actiontype.ActionType;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem.StopItem;

import javax.persistence.Transient;
import java.time.Duration;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedSet;

public interface TtAction extends TtVariantEntity<Integer>, TimedAction {

    Set<TtActionItem> getTtActionItems();

    TtOut getTtOut();

    ActionType getActionType();


    TtAction getNextMainTtRoundOnStop();

    Duration getIntervalDiff();

    @Value.Lazy
    @Transient
    double getIntervalDiffMultiplier();

    boolean isRound();

    boolean isBreak();

    NavigableSet<TtActionItem> getSortedTtActionItems();

    SortedSet<TtAction> getPreviousTtActions();

    TtAction getPreviousTtAction();

    TtActionItem getFirstTtActionItem();

    TtActionItem getLastTtActionItem();

    StopItem getStartStopItem();

    StopItem getEndStopItem();

    TtAction getPreviousTtBreak();

    TtAction getMainNextRound();

    TtAction getPreviousMainRoundOnStop();

    Duration getInterval();

}
