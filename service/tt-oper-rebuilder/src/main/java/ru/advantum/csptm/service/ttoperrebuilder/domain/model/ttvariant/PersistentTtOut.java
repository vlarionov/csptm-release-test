package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Table(schema = "ttb")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PersistentTtOut extends AbstractTtOut {

    int ttOutNum;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "ttOut", targetEntity = PersistentTtAction.class)
    @Fetch(FetchMode.JOIN)
    @Where(clause = "not sign_deleted")
    Set<TtAction> ttActions;

    @ManyToOne(targetEntity = PersistentTtVariant.class)
    TtVariant ttVariant;

    private transient NavigableSet<TtActionItem> sorted;

    @Override
    public NavigableSet<TtActionItem> getSortedTtActionItems() {
        if (sorted == null) {
            sorted = new TreeSet<>(getTtActionItems());
        }
        return sorted;
    }

    @Override
    public TtVariantContext getTtVariantContext() {
        return getTtVariant().getContext();
    }

}