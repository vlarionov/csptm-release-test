package ru.advantum.csptm.service.ttoperrebuilder.domain.model.mode;

import lombok.Value;

import java.util.Set;

@Value
public class ModeViolation {
   ViolationReason reason;

    Integer ttVariantId;

    Set<Integer> ttOutIds;
}
