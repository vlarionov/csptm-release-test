package ru.advantum.csptm.service.ttoperrebuilder.port.persistent;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.norm.NormBetweenStop;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.norm.NormRepository;
import ru.advantum.csptm.service.ttoperrebuilder.port.persistent.jpa.PersistentNormBetweenStopRepository;

import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

@Repository
public class NormRepositoryImpl implements NormRepository {
    public static final String CACHE_NAME = "norms";

    private final PersistentNormBetweenStopRepository persistentNormBetweenStopRepository;

    @Autowired
    public NormRepositoryImpl(PersistentNormBetweenStopRepository persistentNormBetweenStopRepository) {
        this.persistentNormBetweenStopRepository = persistentNormBetweenStopRepository;
    }

    @Override
    @Cacheable(CACHE_NAME)
    public Map<NormKey, NavigableMap<HourKey, NormBetweenStop>> findNormNavigableMap(int normId) {
        val result = new HashMap<NormKey, NavigableMap<HourKey, NormBetweenStop>>();
        for (NormBetweenStop normBetweenStops : persistentNormBetweenStopRepository.findAllByNormId(normId)) {
            val normKey = new NormKey(normBetweenStops.getStopItem1Id(), normBetweenStops.getStopItem2Id());
            val hourKey = new HourKey(normBetweenStops.getHourFrom(), normBetweenStops.getHourTo());
            result.computeIfAbsent(normKey, v -> new TreeMap<>()).put(hourKey, normBetweenStops);
        }
        return result;
    }

}
