package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedSet;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.immutables.value.Value;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.AbstractEntity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.driver.FakeDriver;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem.StopItem;

import javax.annotation.Nullable;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Stream;

import static com.google.common.collect.ImmutableMap.toImmutableMap;
import static com.google.common.collect.ImmutableSortedSet.toImmutableSortedSet;

@Value.Immutable
@Slf4j
@MappedSuperclass
public abstract class AbstractTtVariant extends AbstractEntity<Integer> implements TtVariant {

    @Value.Auxiliary
    @Transient
    public abstract TtVariantContext getContext();

    @Override
    public ZoneOffset getTimeZoneOffset() {
        return isPlan() ? ZoneOffset.ofHours(3) : ZoneOffset.UTC;
    }

    @Nullable
    public abstract LocalDate getOrderDate();

    @Override
    @Value.Lazy
    public boolean isPlan() {
        return getOrderDate() == null;
    }

    @Override
    @Value.Lazy
    public Set<TtOut> getTtOuts() {
        return ImmutableSet.copyOf(getContext().getTtOuts().values());
    }

    @Override
    @Value.Lazy
    public SortedSet<TtAction> geSortedTtActions() {
        return ImmutableSortedSet.copyOf(getTtActions());
    }

    @Transient
    @Value.Lazy
    public Map<Long, TtActionItem> getTtActionItemIdMap() {
        return ImmutableMap.copyOf(getContext().getTtActionItems());
    }

    @Override
    @Value.Lazy
    public Set<TtActionItem> getTtActionItems() {
        return ImmutableSet.copyOf(getContext().getTtActionItems().values());
    }

    @Override
    @Value.Lazy
    public NavigableSet<TtActionItem> getSortedTtActionItems() {
        return ImmutableSortedSet.copyOf(getTtActionItems());
    }

    @Override
    @Value.Lazy
    public Map<Integer, Set<TtActionItem>> getTtActionTtActionItemsMap() {
        Map<Integer, List<TtActionItem>> result = new HashMap<>();
        for (TtActionItem ttActionItem : getContext().getTtActionItems().values()) {
            val ttActionItems = result.computeIfAbsent(ttActionItem.getTtAction().getId(), (id) -> new ArrayList<>());
            ttActionItems.add(ttActionItem);
        }
        return result.entrySet().stream().collect(toImmutableMap(
                Map.Entry::getKey,
                entry -> ImmutableSet.copyOf(entry.getValue())
        ));
    }

    @Override
    @Value.Lazy
    public Set<TtAction> getTtActions() {
        return ImmutableSet.copyOf(getContext().getTtActions().values());
    }

    @Override
    @Value.Lazy
    public Set<StopItem> getStopItems() {
        return ImmutableSet.copyOf(getContext().getStopItems().values());
    }

    @Override
    @Value.Lazy
    public Set<FakeDriver> getFakeDrivers() {
        return ImmutableSet.copyOf(getContext().getFakeDrivers().values());
    }

    @Override
    @Value.Lazy
    public Instant getStartTime() {
        return getSortedTtActionItems().first().getStartTime();
    }

    @Override
    @Value.Lazy
    public TtAction getLastRound() {
        Stream<TtAction> reversedTrActions = trActionStream().sorted(Comparator.reverseOrder());
        return trRoundStream(reversedTrActions).findFirst().orElse(null);
    }

    @Override
    @Value.Lazy
    public Duration getMaxInterval() {
        return getRoundWithMaxInterval().getInterval();
    }

    @Override
    @Value.Lazy
    public SortedSet<TtAction> getSortedRounds() {
        return trRoundStream()
                .collect(toImmutableSortedSet(Comparator.naturalOrder()));
    }

    @Override
    @Value.Lazy
    public TtAction getRoundWithMaxInterval() {
        return findRoundWithMaxInterval(Collections.emptySet());
    }

    @Override
    @Value.Lazy
    public SortedSet<TtAction> getTtActionsOfLastRound() {
        return ImmutableSortedSet.copyOf(getLastRound().getPreviousTtActions());
    }

    @Override
    public TtAction findRoundWithMaxInterval(Set<TtAction> excludedRounds) {
        return getSortedRounds().stream()
                .filter(round -> !excludedRounds.contains(round))
                .max(Comparator.comparing(TtAction::getInterval))
                .orElse(null);
    }

    private Stream<TtAction> trActionStream() {
        return getTtActions().stream();
    }

    private Stream<TtAction> trRoundStream() {
        return trRoundStream(trActionStream());
    }

    private Stream<TtAction> trRoundStream(Stream<TtAction> trActionStream) {
        return trActionStream
                .filter(TtAction::isRound);
    }



}
