package ru.advantum.csptm.service.ttoperrebuilder.domain.service;

import com.google.common.base.MoreObjects;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.service.ttoperrebuilder.application.NormService;
import ru.advantum.csptm.service.ttoperrebuilder.domain.factory.ImmutableCloneTtVariantFactory;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.Entity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.actiontype.ActionType;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.*;
import ru.advantum.csptm.service.ttoperrebuilder.port.persistent.jpa.PersistentActionTypeRepository;

import java.time.*;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

@Service
@Slf4j
public class TtVariantChangeServiceImpl implements TtVariantChangeService {
    private final NormService normService;

    private final PersistentActionTypeRepository actionTypeRepository;

    private final TtVariantRepository ttVariantRepository;

    @Autowired
    public TtVariantChangeServiceImpl(NormService normService, PersistentActionTypeRepository actionTypeRepository, TtVariantRepository ttVariantRepository) {
        this.normService = normService;
        this.actionTypeRepository = actionTypeRepository;
        this.ttVariantRepository = ttVariantRepository;
    }

//    @Override
//    public TtVariant supplementBreaks(TtVariant ttVariant) {
//        Map<Long, ImmutableTtActionItem.Builder> changedItems = new HashMap<>();
//        Map<Long, ImmutableTtActionItem.Builder> newItems = new HashMap<>();
//        for (TtAction ttAction : ttVariant.getTtActions()) {
//            val isStartedFromMinTime = ttAction.getFirstTtActionItem().getActionType().instanceOf(ActionType.MIN_TIME);
//            if (!isStartedFromMinTime) {
//                continue;
//            }
//
//            val minTimeItem = ttAction.getFirstTtActionItem();
//            val regularTimeItem = ttAction.getSortedTtActionItems().stream()
//                    .filter(ttActionItem -> ttActionItem.getActionType().instanceOf(ActionType.REGULAR_TIME))
//                    .findFirst().orElse(null);
//            val correctionTimeItem = ttAction.getSortedTtActionItems().stream()
//                    .filter(ttActionItem -> ttActionItem.getActionType().instanceOf(ActionType.CORRECTION_TIME))
//                    .findFirst().orElse(null);
//
//            val regularTimeActionTime = actionTypeRepository.findOne(ActionType.REGULAR_TIME);
//            val correctionTimeActionTime = actionTypeRepository.findOne(ActionType.CORRECTION_TIME);
//
//            ImmutableTtActionItem.Builder minTimeItemBuilder = ImmutableTtActionItem.builder().from(minTimeItem);
//            ImmutableTtActionItem.Builder regularTimeItemBuilder = ImmutableTtActionItem.builder();
//            ImmutableTtActionItem.Builder correctionTimeItemBuilder = ImmutableTtActionItem.builder();
//
//            changedItems.put(minTimeItem.getId(), minTimeItemBuilder);
//
//            Long regularTimeItemId;
//            Instant regularTimeItemEndTime;
//            if (regularTimeItem == null) {
//                regularTimeItemId = ttVariantRepository.generateTtActionItemId();
//                regularTimeItemBuilder
//                        .id(regularTimeItemId)
//                        .ttActionId(minTimeItem.getTtAction().getId())
//                        .actionType(regularTimeActionTime)
//                        .startTimeProvider((unused) -> minTimeItem.getEndTime())
//                        .durationProvider((unused) -> Duration.ZERO)
//                        .driverId(minTimeItem.getDriverId())
//                        .drShiftId(minTimeItem.getDrShiftId())
//                        .stopItem2RoundId(minTimeItem.getStopItem2Round().getId())
//                        .previousTtActionItemId(minTimeItem.getId());
//                regularTimeItemEndTime = minTimeItem.getEndTime();
//                newItems.put(regularTimeItemId, regularTimeItemBuilder);
//            } else {
//                regularTimeItemBuilder.from(regularTimeItem);
//                regularTimeItemId = regularTimeItem.getId();
//                regularTimeItemEndTime = regularTimeItem.getEndTime();
//                changedItems.put(regularTimeItemId, regularTimeItemBuilder);
//            }
//            minTimeItemBuilder.nextTtActionItemId(regularTimeItemId);
//
//            Long correctionTimeItemId;
//            Instant correctionTimeItemEndTime;
//            if (correctionTimeItem == null) {
//                correctionTimeItemId = ttVariantRepository.generateTtActionItemId();
//                correctionTimeItemBuilder
//                        .id(correctionTimeItemId)
//                        .ttActionId(minTimeItem.getTtAction().getId())
//                        .actionType(correctionTimeActionTime)
//                        .startTimeProvider((unused) -> regularTimeItemEndTime)
//                        .durationProvider((unused) -> Duration.ZERO)
//                        .driverId(minTimeItem.getDriverId())
//                        .drShiftId(minTimeItem.getDrShiftId())
//                        .stopItem2RoundId(minTimeItem.getStopItem2Round().getId());
//                newItems.put(correctionTimeItemId, correctionTimeItemBuilder);
//                correctionTimeItemEndTime = regularTimeItemEndTime;
//            } else {
//                correctionTimeItemBuilder.from(correctionTimeItem);
//                correctionTimeItemId = correctionTimeItem.getId();
//                changedItems.put(correctionTimeItemId, correctionTimeItemBuilder);
//                correctionTimeItemEndTime = correctionTimeItem.getEndTime();
//            }
//            correctionTimeItemBuilder.previousTtActionItemId(regularTimeItemId);
//            regularTimeItemBuilder.nextTtActionItemId(correctionTimeItemId);
//
//            TtActionItem afterCorrectionTimeItem = minTimeItem.getTtAction().getLastTtActionItem().getNextTtActionItem();
//            if (afterCorrectionTimeItem != null) {
//                ImmutableTtActionItem.Builder afterCorrectionTimeItemBuilder = ImmutableTtActionItem.builder()
//                        .from(afterCorrectionTimeItem)
//                        .startTimeProvider((item) -> correctionTimeItemEndTime)
//                        .previousTtActionItemId(correctionTimeItemId);
//                correctionTimeItemBuilder.nextTtActionItemId(afterCorrectionTimeItem.getId());
//                changedItems.put(afterCorrectionTimeItem.getId(), afterCorrectionTimeItemBuilder);
//            }
//        }
//
//        return new ImmutableCloneTtVariantFactory(ttVariant) {
//            @Override
//            public Stream<ImmutableTtActionItem.Builder> getTtActionItemBuilderStream(Stream<TtActionItem> sourceItemStream) {
//                Stream<ImmutableTtActionItem.Builder> newItemStream = newItems.values().stream();
//                Stream<ImmutableTtActionItem.Builder> changedItemStream = sourceItemStream.map(sourceItem -> changedItems.getOrDefault(sourceItem.getId(), super.mapTtActionItemBuilder(sourceItem)));
//                return Stream.concat(newItemStream, changedItemStream);
//            }
//        }
//                .build();
//    }

    @Override
    public TtVariant supplementBreaks(TtVariant ttVariant) {
        Map<Long, ImmutableTtActionItem.Builder> changedItems = new HashMap<>();
        Map<Long, ImmutableTtActionItem.Builder> newItems = new HashMap<>();
        for (TtAction ttAction : ttVariant.getTtActions()) {
            val isStartedFromMinTime = ttAction.getFirstTtActionItem().getActionType().instanceOf(ActionType.MIN_TIME);
            if (!isStartedFromMinTime) {
                continue;
            }

            val minTimeItem = ttAction.getFirstTtActionItem();
            val regularTimeItem = ttAction.getSortedTtActionItems().stream()
                    .filter(ttActionItem -> ttActionItem.getActionType().instanceOf(ActionType.REGULAR_TIME))
                    .findFirst().orElse(null);
            val correctionTimeItem = ttAction.getSortedTtActionItems().stream()
                    .filter(ttActionItem -> ttActionItem.getActionType().instanceOf(ActionType.CORRECTION_TIME))
                    .findFirst().orElse(null);

            val regularTimeActionTime = actionTypeRepository.findOne(ActionType.REGULAR_TIME);
            val correctionTimeActionTime = actionTypeRepository.findOne(ActionType.CORRECTION_TIME);

            ImmutableTtActionItem.Builder minTimeItemBuilder = ImmutableTtActionItem.builder().from(minTimeItem);
            ImmutableTtActionItem.Builder regularTimeItemBuilder = ImmutableTtActionItem.builder();
            ImmutableTtActionItem.Builder correctionTimeItemBuilder = ImmutableTtActionItem.builder();

            changedItems.put(minTimeItem.getId(), minTimeItemBuilder);

            Long regularTimeItemId;
            Instant regularTimeItemEndTime;
            if (regularTimeItem == null) {
                regularTimeItemId = ttVariantRepository.generateTtActionItemId();
                regularTimeItemEndTime = minTimeItem.getEndTime();
                regularTimeItemBuilder
                        .id(regularTimeItemId)
                        .ttActionId(minTimeItem.getTtAction().getId())
                        .actionType(regularTimeActionTime)
                        .startTimeProvider((unused) -> minTimeItem.getEndTime())
                        .durationProvider((unused) -> Duration.ZERO)
                        .driverId(minTimeItem.getDriverId())
                        .drShiftId(minTimeItem.getDrShiftId())
                        .stopItem2RoundId(minTimeItem.getStopItem2Round().getId())
                        .previousTtActionItemId(minTimeItem.getId());
                newItems.put(regularTimeItemId, regularTimeItemBuilder);
            } else {
                regularTimeItemBuilder.from(regularTimeItem);
                regularTimeItemId = regularTimeItem.getId();
                regularTimeItemEndTime = regularTimeItem.getEndTime();
                changedItems.put(regularTimeItemId, regularTimeItemBuilder);
            }
            minTimeItemBuilder.nextTtActionItemId(regularTimeItemId);

            Long correctionTimeItemId;
            Instant correctionTimeItemEndTime;
            if (correctionTimeItem == null) {
                correctionTimeItemId = ttVariantRepository.generateTtActionItemId();
                correctionTimeItemEndTime = regularTimeItemEndTime;
                correctionTimeItemBuilder
                        .id(correctionTimeItemId)
                        .ttActionId(minTimeItem.getTtAction().getId())
                        .actionType(correctionTimeActionTime)
                        .startTimeProvider((unused) -> regularTimeItemEndTime)
                        .durationProvider((unused) -> Duration.ZERO)
                        .driverId(minTimeItem.getDriverId())
                        .drShiftId(minTimeItem.getDrShiftId())
                        .stopItem2RoundId(minTimeItem.getStopItem2Round().getId());
                newItems.put(correctionTimeItemId, correctionTimeItemBuilder);
            } else {
                correctionTimeItemBuilder.from(correctionTimeItem);
                correctionTimeItemId = correctionTimeItem.getId();
                correctionTimeItemEndTime = correctionTimeItem.getEndTime();
                changedItems.put(correctionTimeItemId, correctionTimeItemBuilder);
            }
            regularTimeItemBuilder.nextTtActionItemId(correctionTimeItemId);
            correctionTimeItemBuilder.previousTtActionItemId(regularTimeItemId);

            TtActionItem afterCorrectionTimeItem = minTimeItem.getTtAction().getLastTtActionItem().getNextTtActionItem();
            if (afterCorrectionTimeItem != null) {
                ImmutableTtActionItem.Builder afterCorrectionTimeItemBuilder = ImmutableTtActionItem.builder()
                        .from(afterCorrectionTimeItem)
                        .startTimeProvider((item) -> correctionTimeItemEndTime)
                        .previousTtActionItemId(correctionTimeItemId);
                correctionTimeItemBuilder.nextTtActionItemId(afterCorrectionTimeItem.getId());
                changedItems.put(afterCorrectionTimeItem.getId(), afterCorrectionTimeItemBuilder);
            }
        }

        return new ImmutableCloneTtVariantFactory(ttVariant) {
            @Override
            public Stream<ImmutableTtActionItem.Builder> getTtActionItemBuilderStream(Stream<TtActionItem> sourceItemStream) {
                Stream<ImmutableTtActionItem.Builder> newItemStream = newItems.values().stream();
                Stream<ImmutableTtActionItem.Builder> changedItemStream = sourceItemStream.map(sourceItem -> changedItems.getOrDefault(sourceItem.getId(), super.mapTtActionItemBuilder(sourceItem)));
                return Stream.concat(newItemStream, changedItemStream);
            }
        }
                .build();
    }

    @Override
    public TtVariant recalculateRoundDurations(TtVariant ttVariant) {
        return new ImmutableCloneTtVariantFactory(ttVariant) {
            @Override
            public ImmutableTtActionItem.Builder mapTtActionItemBuilder(TtActionItem sourceItem) {
                val builder = super.mapTtActionItemBuilder(sourceItem);

                builder.startTimeProvider(ttActionItem -> sourceItem.getStartTime());

                val isRound = sourceItem.getTtAction().isRound();
                if (isRound) {
                    builder.durationProvider(ttActionItem -> {
                        val normId = ttActionItem.getTtVariant().getNormId();
                        val startStopItemId = ttActionItem.getStopItem().getId();
                        val endStopItemId = MoreObjects.firstNonNull(ttActionItem.getNextTtActionItem(), ttActionItem).getStopItem().getId();
                        val hour = LocalTime.from(LocalDateTime.ofInstant(ttActionItem.getStartTime(), ZoneOffset.UTC));
                        return normService.getDuration(normId, startStopItemId, endStopItemId, hour, ttVariant.getTimeZoneOffset()).orElse(sourceItem.getDuration());
                    });
                } else {
                    builder.durationProvider(ttActionItem -> sourceItem.getDuration());
                }
                return builder;
            }
        }.build();
    }

    @Override
    public TtVariant changeDuration(TtActionItem ttActionItem, Duration newDuration) {
        val actionItems = ttActionItem.getTtOut().getSortedTtActionItems();
        val shiftedTrActionItemsIds = actionItems.tailSet(ttActionItem, false).stream().map(Entity::getId).collect(toSet());

        return new ImmutableCloneTtVariantFactory(ttActionItem.getTtVariant()) {
            @Override
            public ImmutableTtActionItem.Builder mapTtActionItemBuilder(TtActionItem sourceItem) {
                ImmutableTtActionItem.Builder builder = super.mapTtActionItemBuilder(sourceItem);

                boolean isShifted = shiftedTrActionItemsIds.contains(sourceItem.getId());
                if (isShifted) {
                    builder.startTimeProvider(ttActionItem1 -> ttActionItem1.getPreviousTtActionItem().getEndTime());
                } else {
                    builder.startTimeProvider(ttActionItem1 -> sourceItem.getStartTime());
                }

                boolean isRound = sourceItem.getTtAction().isRound();
                if (ttActionItem.equals(sourceItem)) {
                    builder.durationProvider(ttActionItem1 -> newDuration);
                } else if (isShifted && isRound) {
                    builder.durationProvider(ttActionItem1 -> {
                        val normId = ttActionItem1.getTtVariant().getNormId();
                        val startStopItemId = ttActionItem1.getStopItem().getId();
                        val endStopItemId = MoreObjects.firstNonNull(ttActionItem1.getNextTtActionItem(), ttActionItem1).getStopItem().getId();
                        val hour = LocalTime.from(LocalDateTime.ofInstant(ttActionItem.getStartTime(), ZoneOffset.UTC));
                        val zoneOffset = ttActionItem1.getTtVariant().getTimeZoneOffset();
                        return normService.getDuration(normId, startStopItemId, endStopItemId, hour, zoneOffset).orElse(sourceItem.getDuration());
                    });
                } else {
                    builder.durationProvider(ttActionItem1 -> sourceItem.getDuration());
                }
                return builder;
            }
        }.build();
    }


}
