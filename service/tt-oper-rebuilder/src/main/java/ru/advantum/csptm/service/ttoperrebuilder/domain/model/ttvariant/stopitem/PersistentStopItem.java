package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem;

import com.google.common.collect.ImmutableSet;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.stoptype.PersistentStopType;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.stoptype.StopType;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtVariantContext;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(schema = "rts")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PersistentStopItem extends AbstractStopItem implements StopItem {
    @ManyToMany(fetch = FetchType.EAGER, targetEntity = PersistentStopType.class)
    @JoinTable(
            schema = "rts",
            name = "stop_item_type",
            joinColumns = @JoinColumn(name = "stop_item_id"),
            inverseJoinColumns = @JoinColumn(name = "stop_type_id")
    )
    @Fetch(FetchMode.JOIN)
    Set<StopType> stopTypes;

    @Override
    public Set<StopType> getStopTypes() {
        return ImmutableSet.copyOf(stopTypes);
    }

    @Transient
    TtVariantContext ttVariantContext;

}
