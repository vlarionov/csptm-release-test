package ru.advantum.csptm.service.ttoperrebuilder.port.persistent.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.norm.PersistentNormBetweenStop;

import java.util.List;

public interface PersistentNormBetweenStopRepository extends JpaRepository<PersistentNormBetweenStop, Long> {

    List<PersistentNormBetweenStop> findAllByNormId(Integer normId);

}
