package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem2round;


import ru.advantum.csptm.service.ttoperrebuilder.domain.model.Entity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem.StopItem;

public interface StopItem2Round extends Entity<Long> {

    StopItem getStopItem();

}
