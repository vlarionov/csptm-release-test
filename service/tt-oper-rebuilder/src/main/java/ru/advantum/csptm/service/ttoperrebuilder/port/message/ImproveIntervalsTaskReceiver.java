package ru.advantum.csptm.service.ttoperrebuilder.port.message;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;
import ru.advantum.csptm.service.ttoperrebuilder.application.TtVariantService;
import ru.advantum.csptm.service.ttoperrebuilder.config.RmqConfiguration;

@Profile("rmq")
@Component
@Slf4j
public class ImproveIntervalsTaskReceiver {

    private final TtVariantService ttVariantService;

    @Autowired
    public ImproveIntervalsTaskReceiver(TtVariantService ttVariantService) {
        this.ttVariantService = ttVariantService;
    }

    //todo make message converter, exception handler, retry template
    @RabbitListener(queues = RmqConfiguration.QUEUE_NAME)
    public void receive(GenericMessage genericMessage) {
        try {
            String jsonMessage = new String((byte[]) genericMessage.getPayload());
            val task = new ObjectMapper().readValue(jsonMessage, Task.class);
            ttVariantService.improveIntervals(task.ttVariantId);
        } catch (Throwable e) {
            log.error("", e);
        }
    }

    @Value
    public static class Task {
        int ttVariantId;

    }

}