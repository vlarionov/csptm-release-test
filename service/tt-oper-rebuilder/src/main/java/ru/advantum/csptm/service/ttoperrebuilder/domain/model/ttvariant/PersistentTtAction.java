package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Where;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.actiontype.ActionType;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.actiontype.ImmutableActionType;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.actiontype.PersistentActionType;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(schema = "ttb")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PersistentTtAction extends AbstractTtAction {
    @ManyToOne(targetEntity = PersistentTtOut.class)
    TtOut ttOut;

    @ManyToOne(targetEntity = PersistentActionType.class, fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    ActionType actionType;

    @OneToMany( targetEntity = PersistentTtActionItem.class, fetch = FetchType.EAGER, mappedBy = "ttAction")
    @Fetch(FetchMode.JOIN)
    @Where(clause = "not sign_deleted")
    Set<TtActionItem> ttActionItems;

    @Override
    public Integer getTtOutId() {
        return ttOut.getId();
    }

    @Override
    public ActionType getActionType() {
        return ImmutableActionType.builder().from(actionType).build();
    }

    @Override
    public TtVariantContext getTtVariantContext() {
        return getTtVariant().getContext();
    }

    @Override
    public TtVariant getTtVariant() {
        return ttOut.getTtVariant();
    }


}
