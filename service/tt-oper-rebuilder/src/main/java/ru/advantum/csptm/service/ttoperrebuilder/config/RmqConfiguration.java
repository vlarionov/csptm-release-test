package ru.advantum.csptm.service.ttoperrebuilder.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RmqConfiguration {
    public static final String QUEUE_NAME = "tt-oper-rebuilder";

    @Bean
    public Queue taskQueue() {
        return new Queue(QUEUE_NAME);
    }

}
