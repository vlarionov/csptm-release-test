package ru.advantum.csptm.service.ttoperrebuilder.domain.model.mode;

import lombok.Value;

@Value
public class ViolationReason {
    public static final short DR_SHIFT_DURATION = 251;
    public static final short MAX_DR_SHIFT_DURATION_BEFORE_FIRST_DINNER_OR_PAUSE = 252;
    public static final short MIN_TOTAL_BN_ROUND_BREAKS_DURATION_BEFORE_FIRST_DINNER_OR_PAUSE = 253;
    public static final short BREAK_DURATION = 254;
    public static final short MAX_DR_SHIFT_DURATION_AFTER_LAST_BREAK = 255;

    int id;

}
