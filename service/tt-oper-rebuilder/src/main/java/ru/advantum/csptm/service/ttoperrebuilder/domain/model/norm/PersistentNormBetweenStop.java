package ru.advantum.csptm.service.ttoperrebuilder.domain.model.norm;

import com.google.common.collect.ComparisonChain;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.Duration;
import java.time.LocalTime;

@Entity
@Table(schema = "ttb")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PersistentNormBetweenStop extends AbstractEntity<Long> implements NormBetweenStop {

    Integer normId;

    @Column(name = "stop_item_1_id")
    Integer stopItem1Id;

    @Column(name = "stop_item_2_id")
    Integer stopItem2Id;

    LocalTime hourFrom;

    LocalTime hourTo;

    Integer betweenStopDur;

    @Override
    public Duration getDuration() {
        return Duration.ofSeconds(betweenStopDur);
    }

    @Override
    public int compareTo(NormBetweenStop other) {
        return ComparisonChain.start()
                .compare(hourFrom, other.getHourFrom())
                .compare(hourTo, other.getHourTo())
                .result();
    }
}
