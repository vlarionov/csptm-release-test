package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.driver;


import org.immutables.value.Value;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.*;

import java.time.Duration;
import java.time.Instant;
import java.util.Set;
import java.util.SortedSet;

public interface FakeDriver extends TtVariantEntity<Integer>, TimedAction {

    Duration getDurationBefore(Instant toInstant);

    TtOut getTtOut();

    Set<TtAction> getTtActions();

    SortedSet<TtAction> getSortedTtActions();

    SortedSet<TtAction> getSortedBreaks();

    SortedSet<TtAction> getSortedBnRoundBreaks();

    TtAction getFirstDinnerOrPause();

    TtAction getLastBreak();

    //todo refactor if possible
    static Integer getFakeDriverId(TtActionItem ttActionItem) {
        if (!ttActionItem.getTtVariant().isPlan()) {
            return ttActionItem.getDriverId();
        }

        return ttActionItem.getDrShiftId() * Short.MAX_VALUE + ttActionItem.getTtOut().getId();
    }

    @Value.Lazy
    Duration getDrShiftDurationAfterLastBreak();
}
