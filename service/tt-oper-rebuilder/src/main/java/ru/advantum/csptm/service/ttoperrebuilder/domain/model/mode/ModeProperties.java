package ru.advantum.csptm.service.ttoperrebuilder.domain.model.mode;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.Duration;

@ConfigurationProperties(prefix = "ttOperRebuilder.mode")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ModeProperties {

    Duration maxDrShiftDuration = Duration.ofHours(11).plusMinutes(20);

    Duration minDrShiftDuration = Duration.ofHours(6);//Duration.ofHours(9).plusMinutes(30);

    Duration maxDrShiftDurationBeforeFirstDinnerOrPause = Duration.ofHours(5).plusMinutes(30);//Duration.ofHours(5);

    Duration maxDrShiftDurationAfterLastBreak = Duration.ofHours(5).plusMinutes(40);

    Duration maxDurationBeforeFirstDinnerOrPauseForCheckTotalBnRoundBreaksBefore = Duration.ofHours(4);

    Duration minTotalBnRoundBreaksDurationBeforeFirstDinnerOrPause = Duration.ofMinutes(15);

    Duration minDinnerDuration = Duration.ofMinutes(30);

    Duration maxDinnerDuration = Duration.ofHours(2);

    Duration maxTotalPauseDuration = Duration.ofHours(3);


//
//    Duration maxDrShiftDuration = Duration.ofHours(13);//Duration.ofHours(11).plusMinutes(20);
//
//    Duration minDrShiftDuration = Duration.ofHours(3);//Duration.ofHours(9).plusMinutes(30);
//
//    Duration maxDrShiftDurationBeforeFirstDinnerOrPause = Duration.ofHours(10);//Duration.ofHours(5);
//
//    Duration maxDrShiftDurationAfterLastBreak = Duration.ofHours(13);// Duration.ofHours(5).plusMinutes(40);
//
//    Duration minTotalBnRoundBreaksDurationBeforeFirstDinnerOrPause = Duration.ofMinutes(5);//Duration.ofMinutes(15);
//
//    Duration minDinnerDuration = Duration.ofMinutes(30);//Duration.ofMinutes(30);
//
//    Duration maxDinnerDuration = Duration.ofHours(2);
//
//    Duration maxTotalPauseDuration = Duration.ofHours(3);



}
