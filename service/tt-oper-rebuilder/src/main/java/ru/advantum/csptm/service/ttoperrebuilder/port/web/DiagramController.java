package ru.advantum.csptm.service.ttoperrebuilder.port.web;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.advantum.csptm.service.ttoperrebuilder.domain.service.IntervalImprovementService;
import ru.advantum.csptm.service.ttoperrebuilder.domain.service.ModeValidationService;
import ru.advantum.csptm.service.ttoperrebuilder.domain.service.TtVariantChangeService;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtVariant;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtVariantRepository;
import ru.advantum.csptm.service.ttoperrebuilder.domain.factory.ImmutableCloneTtVariantFactory;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Profile("local")
@RestController
@Slf4j
@EnableScheduling
public class DiagramController {

    private final TtVariantRepository ttVariantRepository;

    private final IntervalImprovementService intervalImprovementService;

    private final TtVariantChangeService ttVariantService;

    private final ModeValidationService modeValidationService;

    private final List<TtVariant> ttVariantIterations = new ArrayList<>();


    @Autowired
    public DiagramController(TtVariantRepository ttVariantRepository, IntervalImprovementService intervalImprovementService, TtVariantChangeService ttVariantService, ModeValidationService modeValidationService) {
        this.ttVariantRepository = ttVariantRepository;
        this.intervalImprovementService = intervalImprovementService;
        this.ttVariantService = ttVariantService;
        this.modeValidationService = modeValidationService;
    }

    @RequestMapping(value = "/data/{iteration}", method = {RequestMethod.GET, RequestMethod.POST})
    public Collection<TrActionDto> getDataIteration(@PathVariable("iteration") int iteration) {
        val ttVariant = ttVariantIterations.get(iteration);
        return ttVariant.getTtActions().stream()
                .map(ttAction -> {
                    val actionType = ttAction.getActionType();
                    return new TrActionDto(
                            actionType.getActionTypeCode(),
                            actionType.getActionTypeName(),
                            ttAction.getStartStopItem().getId(),
                            ttAction.getEndStopItem().getId(),
                            ttAction.getTtOut().getTtOutNum(),
                            ttAction.getTtOut().getId(),
                            ttAction.getStartTime(),
                            ttAction.getEndTime(),
                            ttAction.getId(),
                            ttAction.getTtOut().getId()
                    );
                })
                .collect(toList());
    }

    @RequestMapping(value = "/data", method = {RequestMethod.GET, RequestMethod.POST})
    public Collection<TrActionDto> getData() {
        return getDataIteration(0);
    }

    @RequestMapping(value = "/ttVariant", method = {RequestMethod.GET, RequestMethod.POST})
    public TtVariant getTtVariant() {
        return new ImmutableCloneTtVariantFactory(ttVariantIterations.get(0)).build();
    }


    @PostConstruct
    public void init() {
        log.info("find ttVariant");
        TtVariant persistentTtVariant = ttVariantRepository.find(2683639);
        //plan: 2683807,2683843,2683818,2683422
        //fact: ? 2683699,2683450,2682546
        ttVariantIterations.add(persistentTtVariant);
    }
    @Scheduled(fixedRate = Long.MAX_VALUE)
    @Transactional
    public void createIteration() {
        TtVariant persistentTtVariant = ttVariantIterations.get(0);
        log.info("supplementBreaks");
        persistentTtVariant = ttVariantService.supplementBreaks(persistentTtVariant);
        log.info("validate {}", modeValidationService.validate(persistentTtVariant));
        val improvedTtVariant = intervalImprovementService.improveIntervals(persistentTtVariant);
        ttVariantIterations.add(improvedTtVariant);
    }

    @Getter
    @FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
    @AllArgsConstructor
    private static class TrActionDto {

        String actionTypeCode;

        String actionTypeName;

        long stopIdB;

        long stopIdE;

        int ttOutNum;

        Integer trId;

        Instant timeBegin;

        Instant timeEnd;

        long ttActionId;

        Integer ttOutId;

    }
}
