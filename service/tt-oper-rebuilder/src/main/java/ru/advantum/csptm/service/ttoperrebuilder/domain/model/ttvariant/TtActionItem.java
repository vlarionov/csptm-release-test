package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant;


import ru.advantum.csptm.service.ttoperrebuilder.domain.model.actiontype.ActionType;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem.StopItem;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem2round.StopItem2Round;

public interface TtActionItem extends TtVariantEntity<Long>, TimedAction {

    StopItem2Round getStopItem2Round();

    StopItem getStopItem();

    TtActionItem getPreviousTtActionItem();

    TtAction getTtAction();

    TtActionItem getNextTtActionItem();

    TtOut getTtOut();

    ActionType getActionType();

    Integer getDriverId();

    boolean isFlexible();

    Short getDrShiftId();
}
