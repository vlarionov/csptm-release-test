package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem2round;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.AbstractTtVariantEntity;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.TtVariantContext;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem.PersistentStopItem;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant.stopitem.StopItem;

import javax.persistence.*;

@Entity
@Table(schema = "rts")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PersistentStopItem2Round extends AbstractTtVariantEntity<Long> implements StopItem2Round {
    @ManyToOne(targetEntity = PersistentStopItem.class, fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    StopItem stopItem;

    @Transient
    TtVariantContext ttVariantContext;
}
