package ru.advantum.csptm.service.ttoperrebuilder.domain.model.norm;

import com.google.common.collect.ComparisonChain;
import lombok.Value;

import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;

public interface NormRepository {

    Map<NormKey, NavigableMap<HourKey, NormBetweenStop>> findNormNavigableMap(int normId);

    @Value
    class NormKey {
        Integer startStopItemId;

        Integer endStopItemId;
    }

    @Value
    class HourKey implements Comparable<HourKey> {

        LocalTime hourFrom;

        LocalTime hourTo;

        @Override
        public int compareTo(HourKey other) {
            return ComparisonChain.start()
                    .compare(hourFrom, other.hourFrom)
                    .compare(hourTo, other.hourTo)
                    .result();
        }
    }
}
