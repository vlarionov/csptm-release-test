package ru.advantum.csptm.service.ttoperrebuilder.domain.model.ttvariant;

import lombok.val;
import org.immutables.value.Value;
import ru.advantum.csptm.service.ttoperrebuilder.domain.model.Entity;

import java.time.Duration;
import java.time.Instant;

//todo fix inheritance to composition
public interface TimedAction extends Comparable<TimedAction> {
    @Value.Lazy
    default Instant getStartTime() {
        return getEndTime().minus(getDuration());
    }

    @Value.Lazy
    default Duration getDuration() {
        return Duration.between(getStartTime(), getEndTime());
    }

    @Value.Lazy
    default Instant getEndTime() {
        return getStartTime().plus(getDuration());
    }

    @Override
    default int compareTo(TimedAction other) {
        val startTimeDiff = this.getStartTime().compareTo(other.getStartTime());
        if (startTimeDiff != 0) {
            return startTimeDiff;
        }
        val durationDiff = this.getDuration().compareTo(other.getDuration());
        if (durationDiff != 0) {
            return durationDiff;
        }

        Entity<Number> self = (Entity<Number>) this;
        Entity<Number> otherEntity = (Entity<Number>) other;
        return self.getId().intValue() - otherEntity.getId().intValue();
    }

}
