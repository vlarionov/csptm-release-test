package ru.advantum.csptm.jep.service.statesaver;

import com.fatboyindustrial.gsonjavatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.jep.proto.*;
import ru.advantum.rabbitmq.consumer.RmqJsonTalker;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.*;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * Created by kaganov on 12/12/2016.
 * Modified by abc
 */
public class StateSaver {

    private static final Logger LOGGER = LoggerFactory.getLogger(StateSaver.class);
    private static final String SS_MAP_NAME = "statesaver-state";
    private static final Gson GSON = Converters.registerAll(new GsonBuilder()).create();

    private final StateSaverConfig config;
    private final ConcurrentMap<Long, TrState> currentState = new ConcurrentHashMap<>();
    private final ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();
    private final ScheduledExecutorService des = Executors.newSingleThreadScheduledExecutor();

    private JedisPool jedisPool;
    private DataSource datasource;
    private RmqJsonTalker rmqJsonTalker;
    private String dbFuncName;

    public StateSaver(StateSaverConfig config) {
        this.config = config;
    }

    private void initState() {
        LOGGER.info("Getting redis state");
        try (Jedis jedis = jedisPool.getResource()) {
            Map<String, String> stringState = jedis.hgetAll(SS_MAP_NAME);
            Map<Long, TrState> state = stringState.entrySet().stream()
                    .collect(Collectors.toMap(e -> Long.valueOf(e.getKey()),
                                              e -> GSON.fromJson(e.getValue(), TrState.class)));
            currentState.putAll(state);
        }
        LOGGER.info("State initialized size [{}]", currentState.size());
    }

    private void initDB() throws SQLException {
        this.datasource = config.database.getConnectionPool();

        this.dbFuncName = "{ ? = call " + config.dbSchema + ".statesaver_pkg$save_tr_last_state(" +
                "?::integer, " +
                "?::bigint, " +
                "?::integer, " +
                "?::timestamp, " +
                "?::integer, " +
                "?::bool, " +
                "?::timestamp, " +
                "?::float4, " +
                "?::float4, " +
                "?::integer, " +
                "?::integer, " +
                "?::integer) }";
    }

    private void dumpState() {
        LOGGER.info("Dumping state to redis");
        try (Jedis jedis = jedisPool.getResource()) {
            LOGGER.info("Converting map to string");
            Map<String, String> stringState = currentState.entrySet().stream()
                    .collect(Collectors.toMap(e -> Long.toString(e.getKey()),
                                              e -> GSON.toJson(e.getValue())));
            LOGGER.info("Converted");
            jedis.hmset(SS_MAP_NAME, stringState);
        } catch (Throwable ex) {
            LOGGER.error("Dump state", ex);
        }
        LOGGER.info("State dumped");
    }

    private void saveToDB(CallableStatement stmt, UnitPacket packet) throws SQLException {
        if (packet != null &&
                packet.getPacketId() != null &&
                packet.getTrId() != null &&
                packet.getEventTime() != null &&
                packet.getTelematic() != null) {

            stmt.registerOutParameter(1, Types.INTEGER);
            stmt.setLong(2, packet.getTrId());
            stmt.setLong(3, packet.getPacketId());
            stmt.setLong(4, packet.getUnitId());
            stmt.setTimestamp(5, Timestamp.from(packet.getEventTime()));
            stmt.setLong(6, packet.getDeviceEventId());
            stmt.setBoolean(7, packet.getTelematic().isLocationValid());

            if (packet.getTelematic().getGpsTime() == null) {
                stmt.setNull(8, Types.TIMESTAMP);
            } else {
                stmt.setTimestamp(8, Timestamp.from(packet.getTelematic().getGpsTime()));
            }

            stmt.setFloat(9, packet.getTelematic().getLon());
            stmt.setFloat(10, packet.getTelematic().getLat());

            if (packet.getTelematic().getAlt() == null) {
                stmt.setNull(11, Types.INTEGER);
            } else {
                stmt.setInt(11, packet.getTelematic().getAlt());
            }

            if (packet.getTelematic().getSpeed() == null) {
                stmt.setNull(12, Types.INTEGER);
            } else {
                stmt.setInt(12, packet.getTelematic().getSpeed());
            }

            if (packet.getTelematic().getHeading() == null) {
                stmt.setNull(13, Types.INTEGER);
            } else {
                stmt.setInt(13, packet.getTelematic().getHeading());
            }

            stmt.execute();
        }
    }

    private void dumpStateToDb() {
        LOGGER.info("Dumping state to db");
        try (Connection conn = datasource.getConnection();
             CallableStatement stmt = conn.prepareCall(this.dbFuncName)) {

            for (Long k : currentState.keySet()) {
                saveToDB(stmt, currentState.get(k).getPacket());
            }
            conn.commit();
        } catch (Throwable ex) {
            LOGGER.error("Dump state to db", ex);
        }
        LOGGER.info("State dumped to db");
    }

    public void start() throws IOException, TimeoutException, SQLException {
        jedisPool = new JedisPool(new GenericObjectPoolConfig(),
                                  config.redisConfig.host,
                                  config.redisConfig.port,
                                  config.redisConfig.timeout,
                                  config.redisConfig.password);

        initState();

        initDB();

        rmqJsonTalker = RmqJsonTalker.newBuilder().setConsumerConfig(true, true, config.serviceConfig.consumer)
                .addConsumer(CommonMessageType.COORDS.name(),
                             UnitPacketsBundle.class,
                             (tag, envelope, basicProperties, message) -> consumeCoords(message))
                .connect(config.serviceConfig.connection.newConnection());

        ses.scheduleAtFixedRate(this::dumpState, config.redisDumpPeriod, config.redisDumpPeriod, TimeUnit.SECONDS);

        des.scheduleAtFixedRate(this::dumpStateToDb, config.dbDumpPeriod, config.dbDumpPeriod, TimeUnit.SECONDS);

        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));

        LOGGER.info("StateSaver started");
    }

    public void stop() {
        LOGGER.info("Stopping StateSaver");

        rmqJsonTalker.gentlyStopConsumer(10, 1);

        ses.shutdown();
        des.shutdown();

        try {
            ses.awaitTermination(5, TimeUnit.SECONDS);
            des.awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOGGER.error("stop", e);
        }

        dumpState();

        LOGGER.info("Stopping complete");
    }

    private TrState mergePackets(TrState oldValue, TrState newValue) {
        if (oldValue.getPacket().getEventTime().isAfter(newValue.getPacket().getEventTime())) {
            return oldValue;
        }

        // #22229 отсечение пакетов из будущего
        if (ChronoUnit.MINUTES.between(Instant.now(), newValue.getPacket().getEventTime()) > config.maxFutureMins) {
            LOGGER.debug("Future packet {}", newValue.getPacket());
            return oldValue;
        }

        UnitPacketBuilder builder = UnitPacket.newBuilder().fromUnitPacket(oldValue.getPacket())
                .setPacketId(newValue.getPacket().getPacketId())
                .setUnitId(newValue.getPacket().getUnitId())
                .setEventTime(newValue.getPacket().getEventTime())
                .setReceiveTime(newValue.getPacket().getReceiveTime())
                .setDeviceEventId(newValue.getPacket().getDeviceEventId());

        if (newValue.getPacket().getTelematic() != null && newValue.getPacket().getTelematic().isLocationValid()) {
            builder.setTelematic(newValue.getPacket().getTelematic());
        }

        if (newValue.getPacket().getFlags() != null) {
            builder.addFlags(newValue.getPacket().getFlags());
        }

        if (newValue.getPacket().getSensors() != null) {
            builder.addSensors(newValue.getPacket().getSensors());
        }

        if (newValue.getPacket().getAttributes() != null) {
            builder.addAttributes(newValue.getPacket().getAttributes());
        }

        return new TrState(newValue.getStateUpdate(), builder.build());
    }

    private void consumeCoords(UnitPacketsBundle message) {
        for (UnitPacket unitPacket : message.getPackets()) {
            Long trId = unitPacket.getTrId();
            if (trId == null) {
                LOGGER.warn("null trId [{}]", unitPacket);
                continue;
            }
            currentState.merge(trId, new TrState(Instant.now(), unitPacket), this::mergePackets);
        }
    }

    public static void main(String[] args) throws Exception {
        StateSaverConfig config = Configuration.unpackConfig(StateSaverConfig.class, "statesaver.xml");
        new StateSaver(config).start();
    }
}
