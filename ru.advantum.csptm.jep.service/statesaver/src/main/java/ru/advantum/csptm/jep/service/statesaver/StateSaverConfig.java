package ru.advantum.csptm.jep.service.statesaver;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.config.common.db.RedisClientConfig;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

/**
 * Created by kaganov on 12/12/2016.
 */
@Root(name = "statesaver")
public class StateSaverConfig {

    @Attribute(name = "redis-dump-period", required = false)
    public final int redisDumpPeriod;

    @Attribute(name = "db-dump-period", required = false)
    public final int dbDumpPeriod;

    @Attribute(name = "max-future-mins", required = false)
    public final int maxFutureMins;

    @Element(name = "redis-client")
    public final RedisClientConfig redisConfig;

    @Element(name = "rmq-service")
    public final JepRmqServiceConfig serviceConfig;


    @Element(name = "database", required = true)
    public DatabaseConnection database;

    @Element(name = "db-schema", required = false)
    public final String dbSchema;

    public StateSaverConfig() {
        redisDumpPeriod = 5;
        redisConfig = new RedisClientConfig();
        serviceConfig = new JepRmqServiceConfig();
        dbDumpPeriod = 30;
        dbSchema = "core";
        maxFutureMins = 1;
    }
}
