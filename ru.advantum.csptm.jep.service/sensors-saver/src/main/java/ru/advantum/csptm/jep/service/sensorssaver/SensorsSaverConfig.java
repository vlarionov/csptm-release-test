package ru.advantum.csptm.jep.service.sensorssaver;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.csptm.artifact.csvsaver.config.CsvSaverConfig;
import ru.advantum.csptm.dbidgen.DbIdGeneratorConfig;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

@Root(name = "coords-saver")
public class SensorsSaverConfig {
    @Element(name = "rmq-service")
    private JepRmqServiceConfig rmqConfig;

    @Element(name = "csv-saver-binary-sensor-data")
    private CsvSaverConfig csvSaverBinarySensorConfig;

    @Element(name = "csv-saver-fuel-sensor-data")
    private CsvSaverConfig csvSaverFuelSensorConfig;

    @Element(name = "csv-saver-extended-data")
    private CsvSaverConfig csvSaverExtendedDataConfig;

    @Element(name = "csv-saver-irma")
    private CsvSaverConfig csvSaverIRMAConfig;

    @Element(name = "csv-saver-incoming-message")
    private CsvSaverConfig csvSaverIncomingMessageConfig;

    @Element(name = "csv-saver-termo-data")
    private CsvSaverConfig csvSaverTermoDataConfig;

    @Element(name = "csv-saver-unit-session")
    private CsvSaverConfig csvSaverUnitSessionConfig;

    @Element(name = "csv-saver-integer-sensor-data")
    private CsvSaverConfig csvSaverIntegerSensorConfig;

    @Element(name = "csv-saver-ask-receipt")
    private CsvSaverConfig csvSaverAskReceiptConfig;

    @Element(name = "csv-saver-incident")
    private CsvSaverConfig csvSaverIncidentConfig;

    public JepRmqServiceConfig getRmqConfig() {
        return rmqConfig;
    }

    public CsvSaverConfig getCsvSaverBinarySensorConfig() {
        return csvSaverBinarySensorConfig;
    }

    public CsvSaverConfig getCsvSaverFuelSensorConfig() {
        return csvSaverFuelSensorConfig;
    }

    public CsvSaverConfig csvSaverExtendedDataConfig() {
        return csvSaverExtendedDataConfig;
    }

    public CsvSaverConfig csvSaverIRMAConfig() {
        return csvSaverIRMAConfig;
    }

    public CsvSaverConfig csvSaverIncomingMessageConfig() {
        return csvSaverIncomingMessageConfig;
    }

    public CsvSaverConfig csvSaverUnitSessionConfig() {
        return csvSaverUnitSessionConfig;
    }

    public CsvSaverConfig csvSaverTermoDataConfig() {
        return csvSaverTermoDataConfig;
    }

    public CsvSaverConfig csvSaverIntegerSensorConfig() {
        return csvSaverIntegerSensorConfig;
    }

    public CsvSaverConfig csvSaverAskReceiptConfig() {
        return csvSaverAskReceiptConfig;
    }

    public CsvSaverConfig csvSaverIncidentConfig() {
        return csvSaverIncidentConfig;
    }

    @Element(name = "database")
    public final DatabaseConnection database;

    @Element(name = "db-id-generator")
    public DbIdGeneratorConfig dbIdGeneratorConfig;

    public SensorsSaverConfig() {
        database = new DatabaseConnection();
    }


}