package ru.advantum.csptm.jep.service.sensorssaver;

import com.google.gson.Gson;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.artifact.csvsaver.CsvSaver;
import ru.advantum.csptm.artifact.csvsaver.apache.print.ApacheRecordPrinter;
import ru.advantum.csptm.artifact.csvsaver.limiters.Limiter;
import ru.advantum.csptm.dbidgen.DbIdGenerator;
import ru.advantum.csptm.incident.model.Incident;
import ru.advantum.csptm.incident.model.IncidentBundle;
import ru.advantum.csptm.jep.proto.*;
import ru.advantum.csptm.jep.proto.sensors.ComplexSensorHolder;
import ru.advantum.csptm.jep.proto.sensors.Type;
import ru.advantum.csptm.service.csd.*;
import ru.advantum.csptm.service.receipt.AckReceipt;
import ru.advantum.csptm.service.receipt.AckReceiptBundle;
import ru.advantum.csptm.service.sensor.updater.SensorSettingsUpdater;
import ru.advantum.csptm.service.sensor.updater.map.SensorAttrib;
import ru.advantum.csptm.service.sensor.updater.map.TrSensorNum;
import ru.advantum.rabbitmq.consumer.RmqJsonTalker;

import java.io.IOException;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeoutException;


/**
 * Created by kukushkin on 5/15/17.
 */

public class SensorsSaver {
    private static final Logger LOGGER = LoggerFactory.getLogger(SensorsSaver.class);
    private static final SensorSettingsUpdater sensorSettingsUpdater = new SensorSettingsUpdater();
    private final DbIdGenerator dbIdGenerator;
    private static final Gson GSON = new Gson();
    private static final String CONFIG_FILE_NAME = "sensors-saver.xml";
    private static final String[] CSV_HEADERS_BINARY_SENSOR = {
            "packet_id",
            "event_time",
            "sensor_id",
            "tr_id",
            "equipment_type_id",
            "val"
    };

    private static final String[] CSV_HEADERS_FUEL_SENSOR = {
            "packet_id",
            "event_time",
            "sensor_id",
            "tr_id",
            "level_mm",
            "level_l",
            "temperature",
            "switched_on"
    };

    private static final String[] CSV_HEADERS_EXTENDED_DATA = {
            "packet_id",
            "event_time",
            "tr_id",
            "odometer",
            "alt",
            "is_validity",
            "is_east_lon",
            "is_north_lat",
            "is_internal_battery",
            "is_first_switch_on",
            "is_sos",
            "is_alarm",
            "is_voice_call",
            "battery_voltage",
            "max_period_speed",
            "satellites_count",
            "track_length",
            "gsm_level",
            "gprs_state",
            "accel_energy"

    };

    private static final String[] CSV_HEADERS_IRMA = {
            "packet_id",
            "event_time",
            "sensor_id",
            "tr_id",
            "in1",
            "in2",
            "in3",
            "in4",
            "out1",
            "out2",
            "out3",
            "out4",
            "has_door1",
            "has_door2",
            "has_door3",
            "has_door4",
            "is_closed_door1",
            "is_closed_door2",
            "is_closed_door3",
            "is_closed_door4",
            "odometer",
            "zone"
    };

    private static final String[] CSV_HEADERS_INCOMING_MESSAGE = {
            "packet_id",
            "event_time",
            "tr_id Integer",
            "message_text",
            "message_id"
    };

    private static final String[] CSV_HEADERS_TERMO_DATA = {
            "packet_id",
            "event_time",
            "tr_id",
            "unit_id",
            "sensor_id",
            "val",
            "status"
    };

    private static final String[] CSV_HEADERS_UNIT_SESSION = {
            "tr_id",
            "unit_id",
            "time_start",
            "time_stop",
            "total_packet_qty",
            "invalid_packet_qty",
            "hist_packet_qty",
            "session_id"
    };

    private static final String[] CSV_HEADERS_INTEGER_SENSOR = {
            "packet_id",
            "event_time",
            "unit_id",
            "tr_id",
            "sensor_id",
            "equipment_type_id",
            "val"
    };

    private static final String[] CSV_HEADERS_ACK_RECEIPT = {
            "packet_id",
            "time_ack",
            "service_id"
    };


    private static final String[] CSV_HEADERS_INCIDENT = {
            "incident_id",
            "incident_initiator_id",
            "incident_status_id",
            "incident_type_id",
            "time_start",
            "tr_id",
            "unit_id",
            "time_finish",
            "account_id",
            "parent_incident_id",
            "incident_params"
    };

    private final SensorsSaverConfig config;
    private volatile Limiter limiterBinarySensor;
    private volatile Limiter limiterFuelSensor;
    private volatile Limiter limiterExtended;
    private volatile Limiter limiterIRMA;
    private volatile Limiter limiterIncomingMessage;
    private volatile Limiter limiterTermoData;
    private volatile Limiter limiterUnitSession;
    private volatile Limiter limiterIntegerSensor;
    private volatile Limiter limiterAckReceipt;
    private volatile Limiter limiterIncident;

    private volatile RmqJsonTalker rmqJsonTalker;

    private SensorsSaver(SensorsSaverConfig config) throws SQLException {
        this.config = config;
        dbIdGenerator = new DbIdGenerator(config.dbIdGeneratorConfig, config.database.getConnectionPool());

    }

    public static void main(String[] args) {
        try {
            SensorsSaverConfig config = Configuration.unpackConfig(
                    SensorsSaverConfig.class,
                    CONFIG_FILE_NAME
            );
            new SensorsSaver(config).start();
        } catch (Exception e) {
            LOGGER.error("Error on startup", e);
        }
    }

    private void start() throws Exception {
        initWriter();
        initConsumer();
        initShutdownHook();

        sensorSettingsUpdater.start(config.database);

        LOGGER.info("SensorSaver started");
    }

    private void initWriter() {
        limiterBinarySensor = CsvSaver.createLimiter(
                config.getCsvSaverBinarySensorConfig(),
                CSV_HEADERS_BINARY_SENSOR,
                SensorData.class,
                new BinarySensorRecordPrinter()
        );

        limiterFuelSensor = CsvSaver.createLimiter(
                config.getCsvSaverFuelSensorConfig(),
                CSV_HEADERS_FUEL_SENSOR,
                SensorData.class,
                new FuelSensorRecordPrinter()
        );


        limiterExtended = CsvSaver.createLimiter(
                config.csvSaverExtendedDataConfig(),
                CSV_HEADERS_EXTENDED_DATA,
                SensorData.class,
                new ExtendedDataRecordPrinter()
        );

        limiterIRMA = CsvSaver.createLimiter(
                config.csvSaverIRMAConfig(),
                CSV_HEADERS_IRMA,
                SensorData.class,
                new IRMARecordPrinter()
        );

        limiterIncomingMessage = CsvSaver.createLimiter(
                config.csvSaverIncomingMessageConfig(),
                CSV_HEADERS_INCOMING_MESSAGE,
                UnitPacket.class,
                new IncomingMessageRecordPrinter()
        );

        limiterTermoData = CsvSaver.createLimiter(
                config.csvSaverTermoDataConfig(),
                CSV_HEADERS_TERMO_DATA,
                SensorData.class,
                new TermoDataRecordPrinter()
        );

        limiterUnitSession = CsvSaver.createLimiter(
                config.csvSaverUnitSessionConfig(),
                CSV_HEADERS_UNIT_SESSION,
                UnitSessionBundle.class,
                new UnitSessionRecordPrinter()

        );

        limiterIntegerSensor = CsvSaver.createLimiter(
                config.csvSaverIntegerSensorConfig(),
                CSV_HEADERS_INTEGER_SENSOR,
                SensorData.class,
                new IntegerSensorRecordPrinter()

        );

        limiterAckReceipt = CsvSaver.createLimiter(
                config.csvSaverAskReceiptConfig(),
                CSV_HEADERS_ACK_RECEIPT,
                AckReceipt.class,
                new AckReceiptRecordPrinter()

        );

        limiterIncident = CsvSaver.createLimiter(
                config.csvSaverIncidentConfig(),
                CSV_HEADERS_INCIDENT,
                Incident.class,
                new IncidentRecordPrinter()

        );
    }

    private void initConsumer() throws IOException, TimeoutException {
        RmqJsonTalker.Builder rmqBuilder = RmqJsonTalker.newBuilder();

        rmqBuilder.setConsumerConfig(
                true,
                true,
                config.getRmqConfig().consumer
        );

        rmqBuilder.addConsumer(
                CommonMessageType.COORDS.name(),
                UnitPacketsBundle.class,
                (tag, envelope, basicProperties, message) -> consumeSensors(message)
        );

        rmqBuilder.addConsumer(AckReceiptBundle.class.getSimpleName(),
                AckReceiptBundle.class,
                (tag, envelope, basicProperties, message) -> consumeAckReceipt(message)
        );

        rmqBuilder.addConsumer(CommonMessageType.UNIT_SESSION.name(),
                UnitSessionBundle.class,
                (tag, envelope, basicProperties, message) -> consumeUnitSession(message)
        );

        rmqBuilder.addConsumer(IncidentBundle.class.getSimpleName(),
                IncidentBundle.class,
                (tag, envelope, basicProperties, message) -> consumeIncidentBundle(message)
        );
        rmqJsonTalker = rmqBuilder.connect(
                config.getRmqConfig().connection.newConnection()
        );
    }

    private void initShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                rmqJsonTalker.cancelConsumer();
            } catch (IOException e) {
                LOGGER.error("Error on cancel consumer", e);
            }
            try {
                rmqJsonTalker.close();
            } catch (IOException | TimeoutException e) {
                LOGGER.error("Error on close talker", e);
            }
            LOGGER.info("RmqJsonTalker stopped");

            try {
                limiterBinarySensor.close();
            } catch (Exception e) {
                LOGGER.error("Error on close limiterBinarySensor", e);
            }
            try {
                limiterFuelSensor.close();
            } catch (Exception e) {
                LOGGER.error("Error on close limiterFuelSensor", e);
            }
            try {
                limiterExtended.close();
            } catch (Exception e) {
                LOGGER.error("Error on close limiterExtended", e);
            }
            try {
                limiterIRMA.close();
            } catch (Exception e) {
                LOGGER.error("Error on close limiterIRMA", e);
            }
            try {
                limiterIncomingMessage.close();
            } catch (Exception e) {
                LOGGER.error("Error on close limiterIncomingMessage", e);
            }
            try {
                limiterTermoData.close();
            } catch (Exception e) {
                LOGGER.error("Error on close limiterTermoData", e);
            }
            try {
                limiterUnitSession.close();
            } catch (Exception e) {
                LOGGER.error("Error on close limiterUnitSession", e);
            }
            try {
                limiterIntegerSensor.close();
            } catch (Exception e) {
                LOGGER.error("Error on close limiterIntegerSensor", e);
            }
            try {
                limiterAckReceipt.close();
            } catch (Exception e) {
                LOGGER.error("Error on close limiterAckReceipt", e);
            }
            try {
                limiterIncident.close();
            } catch (Exception e) {
                LOGGER.error("Error on close limiterIncident", e);
            }
            LOGGER.info("Limiters stopped");
        }));
    }

    private void consumeUnitSession(UnitSessionBundle message) {
        try {
            limiterUnitSession.append(message);
        } catch (Exception e) {
            LOGGER.error("UnitSession write error", e.fillInStackTrace());
        }
    }

    private void consumeAckReceipt(AckReceiptBundle message) {
        try {
            limiterAckReceipt.append(message);
        } catch (Exception e) {
            LOGGER.error("AckReceipt write error", e.fillInStackTrace());
        }
    }

    private void consumeIncidentBundle(IncidentBundle incidentBundle) {
        try {
            incidentBundle.getIncidentBundle().forEach(a -> {
                        try {
                            if (a.getIncidentId() == null) {
                                a.setIncidentId(dbIdGenerator.nextId());
                            }

                            limiterIncident.append(a);
                        } catch (Exception e) {
                            LOGGER.error("consumeIncidentBundle write error", e.fillInStackTrace());
                        }
                    }
                );
        } catch (Exception e) {
            LOGGER.error("AckReceipt write error", e.fillInStackTrace());
        }
    }

    private void consumeSensors(UnitPacketsBundle message) {
        message.getPackets().forEach(unitPacket -> {
            if (unitPacket != null && unitPacket.getComplexSensors() != null) {
                Map<Integer, ComplexSensorHolder> complexSensors = unitPacket.getComplexSensors();
                complexSensors.forEach((Integer num, ComplexSensorHolder sensor) ->
                {
                    try {
                        SensorData sensorData = new SensorData(unitPacket, sensor, num);
                        if (sensor.getType() == Type.BINARY) {
                            limiterBinarySensor.append(sensorData);
                        } else if (sensor.getType() == Type.FUEL) {
                            limiterFuelSensor.append(sensorData);
                        } else if (sensor.getType() == Type.EXTENDED) {
                            limiterExtended.append(sensorData);
                        } else if (sensor.getType() == Type.IRMA) {
                            limiterIRMA.append(sensorData);
                        } else if (sensor.getType() == Type.TEMPERATURE) {
                            limiterTermoData.append(sensorData);
                        } else if (sensor.getType() == Type.ANALOG) {
                            limiterIntegerSensor.append(sensorData);
                        }

                    } catch (Exception e) {
                        LOGGER.error("UnitPacket write error", e.fillInStackTrace());
                    }
                });
            }

            if (unitPacket != null && unitPacket.getAttributes() != null && unitPacket.getAttributes().containsKey(CommonPacketFlag.TEXT_MESSAGE.name())) {
                try {
                    limiterIncomingMessage.append(unitPacket);
                } catch (Exception e) {
                    LOGGER.error("UnitPacket write error", e.fillInStackTrace());
                }
            }
        });
    }


    private static class BinarySensorRecordPrinter implements ApacheRecordPrinter<SensorData> {
        @Override
        public void printRecord(SensorData sensorData, CSVPrinter csvPrinter) {
            SensorAttrib sensorAttrib =  sensorSettingsUpdater.getTrSensorMapping().get().get(new TrSensorNum(sensorData.getUnitPacket().getUnitId(), sensorData.getNum()));
            if (sensorAttrib != null) {
                BinarySensor bs = (BinarySensor) ComplexSensorProcessor.build(sensorData.getNum(), sensorData.getSensor());
                try {
                    csvPrinter.printRecord(
                            sensorData.getUnitPacket().getPacketId(),
                            DateTimeFormatter.ISO_INSTANT.format(sensorData.getUnitPacket().getEventTime()),
                            sensorAttrib.getSensorId(),
                            sensorData.getUnitPacket().getTrId(),
                            sensorAttrib.getSensorTypeId(),
                            bs.getValue()
                    );
                } catch (IOException e) {
                    LOGGER.error("BinarySensor write error", e);
                }
            }
        }
    }



    private static class FuelSensorRecordPrinter implements ApacheRecordPrinter<SensorData> {
        @Override
        public void printRecord(SensorData sensorData, CSVPrinter csvPrinter) {
            SensorAttrib sensorAttrib =  sensorSettingsUpdater.getTrSensorMapping().get().get(new TrSensorNum(sensorData.getUnitPacket().getUnitId(), sensorData.getNum()));
            if (sensorAttrib != null) {
                FuelSensor fs = (FuelSensor) ComplexSensorProcessor.build(sensorData.getNum(), sensorData.getSensor());
                try {
                    csvPrinter.printRecord(
                            sensorData.getUnitPacket().getPacketId(),
                            DateTimeFormatter.ISO_INSTANT.format(sensorData.getUnitPacket().getEventTime()),
                            sensorAttrib.getSensorId(),
                            sensorData.getUnitPacket().getTrId(),
                            fs.getLevelMM(),
                            fs.getLevelL(),
                            fs.getTemperature(),
                            fs.isOk()
                    );
                } catch (IOException e) {
                    LOGGER.error("FuelSensor write error", e);
                }
            }
        }
    }

    private static class ExtendedDataRecordPrinter implements ApacheRecordPrinter<SensorData> {
        @Override
        public void printRecord(SensorData sensorData, CSVPrinter csvPrinter) {
                try {

                    ExtendedData cs = (ExtendedData) ComplexSensorProcessor.build(sensorData.getNum(), sensorData.getSensor());
                    csvPrinter.printRecord(
                            sensorData.getUnitPacket().getPacketId(),
                            DateTimeFormatter.ISO_INSTANT.format(sensorData.getUnitPacket().getEventTime()),
                            sensorData.getUnitPacket().getTrId(),
                            cs.getOdometer(),
                            cs.getAlt(),
                            cs.isValidity(),
                            cs.isLonHalfSphere(),
                            cs.isLatHalfSphere(),
                            cs.isInternalBattery(),
                            cs.isFirstSwitchOn(),
                            cs.isSos(),
                            cs.isAlarm(),
                            cs.isCallRequest(),
                            cs.getBatteryVoltage(),
                            cs.getMaxSpeedPeriod(),
                            cs.getSatellitesCount(),
                            cs.getTrackLength(),
                            cs.getGsmLevel(),
                            cs.getGprsState(),
                            cs.getAccelEnergy()

                    );
                } catch (Exception e) {
                    LOGGER.error("ExtendedData write error", e);
                }
        }
    }

    private static class IRMARecordPrinter implements ApacheRecordPrinter<SensorData> {
        @Override
        public void printRecord(SensorData sensorData, CSVPrinter csvPrinter) {
            try {
                SensorAttrib sensorAttrib =  sensorSettingsUpdater.getTrSensorMapping().get().get(new TrSensorNum(sensorData.getUnitPacket().getUnitId(), sensorData.getNum()));
                if (sensorAttrib != null) {
                    Irma cs = (Irma) ComplexSensorProcessor.build(sensorData.getNum(), sensorData.getSensor());
                    LOGGER.debug(cs.toString());
                    csvPrinter.printRecord(
                            sensorData.getUnitPacket().getPacketId(),
                            DateTimeFormatter.ISO_INSTANT.format(sensorData.getUnitPacket().getEventTime()),
                            sensorAttrib.getSensorId(),
                            sensorData.getUnitPacket().getTrId(),
                            cs.getIrma_door_in1(),
                            cs.getIrma_door_in2(),
                            cs.getIrma_door_in3(),
                            cs.getIrma_door_in4(),
                            cs.getIrma_door_out1(),
                            cs.getIrma_door_out2(),
                            cs.getIrma_door_out3(),
                            cs.getIrma_door_out4(),
                            cs.isIrma_present_door1(),
                            cs.isIrma_present_door2(),
                            cs.isIrma_present_door3(),
                            cs.isIrma_present_door4(),
                            cs.isIrma_closed_door1(),
                            cs.isIrma_closed_door2(),
                            cs.isIrma_closed_door3(),
                            cs.isIrma_closed_door4(),
                            cs.getOdometer(),
                            cs.getZone()
                    );
                }
            } catch (Exception e) {
                LOGGER.error("Irma write error", e);
            }
        }
    }

    private static class IncomingMessageRecordPrinter implements ApacheRecordPrinter<UnitPacket> {
        @Override
        public void printRecord(UnitPacket unitPacket, CSVPrinter csvPrinter) {
            try {
                String message = (String) unitPacket.getAttributes().get(CommonPacketFlag.TEXT_MESSAGE.name());
                String message_id = (String) unitPacket.getAttributes().get(CommonPacketFlag.TEXT_MESSAGE_ID.name());
                csvPrinter.printRecord(
                        unitPacket.getPacketId(),
                        DateTimeFormatter.ISO_INSTANT.format(unitPacket.getEventTime()),
                        unitPacket.getTrId(),
                        message,
                        message_id
                );
            } catch (Exception e) {
                LOGGER.error("IncomingMessage write error", e);
            }
        }
    }


    private static class TermoDataRecordPrinter implements ApacheRecordPrinter<SensorData> {
        @Override
        public void printRecord(SensorData sensorData, CSVPrinter csvPrinter) {
            SensorAttrib sensorAttrib =  sensorSettingsUpdater.getTrSensorMapping().get().get(new TrSensorNum(sensorData.getUnitPacket().getUnitId(), sensorData.getNum()));
            if (sensorAttrib != null) {
                Temperature td = (Temperature) ComplexSensorProcessor.build(sensorData.getNum(), sensorData.getSensor());
                try {
                    csvPrinter.printRecord(
                            sensorData.getUnitPacket().getPacketId(),
                            DateTimeFormatter.ISO_INSTANT.format(sensorData.getUnitPacket().getEventTime()),
                            sensorData.getUnitPacket().getTrId(),
                            sensorData.getUnitPacket().getUnitId(),
                            sensorAttrib.getSensorId(),
                            td.getValue(),
                            td.getStatus()
                    );
                } catch (IOException e) {
                    LOGGER.error("BinarySensor write error", e);
                }
            }
        }
    }


    private static class UnitSessionRecordPrinter implements ApacheRecordPrinter<UnitSessionBundle> {
        @Override
        public void printRecord(UnitSessionBundle unitSessionBundle, CSVPrinter csvPrinter) {
            if (unitSessionBundle != null) {
                try {
                    for (UnitSession unitSession : unitSessionBundle.getPackets()) {
                        if (unitSession != null) {
                            csvPrinter.printRecord(
                                    unitSession.getTrId(),
                                    unitSession.getUnitId(),
                                    DateTimeFormatter.ISO_INSTANT.format(unitSession.getConnectTime()),
                                    (unitSession.getDisconnectTime() == null) ? null : DateTimeFormatter.ISO_INSTANT.format(unitSession.getDisconnectTime()),
                                    unitSession.getTotalPacketCnt(),
                                    unitSession.getInvalidPacketCnt(),
                                    unitSession.getHistPacketCnt(),
                                    unitSession.getSessionId()
                            );
                        }
                    }
                } catch (IOException e) {
                    LOGGER.error("UnitSession write error", e);
                }
            }
        }
    }

    private static class AckReceiptRecordPrinter implements ApacheRecordPrinter<AckReceiptBundle> {
        @Override
        public void printRecord(AckReceiptBundle ackReceipBundle, CSVPrinter csvPrinter) {
            if (ackReceipBundle != null) {
                for (AckReceipt ackReceipt : ackReceipBundle.getReceipts()) {
                    if (ackReceipt != null) {
                        try {
                            csvPrinter.printRecord(
                                    ackReceipt.getPacketId(),
                                    ackReceipt.getDateTime(),
                                    ackReceipt.getServiceId()
                            );
                        } catch (IOException e) {
                            LOGGER.error("AckReceipt write error", e);
                        }
                    }
                }
            }
        }
    }

    private static class IntegerSensorRecordPrinter implements ApacheRecordPrinter<SensorData> {
        @Override
        public void printRecord(SensorData sensorData, CSVPrinter csvPrinter) {
            SensorAttrib sensorAttrib =  sensorSettingsUpdater.getTrSensorMapping().get().get(new TrSensorNum(sensorData.getUnitPacket().getUnitId(), sensorData.getNum()));
            if (sensorAttrib != null) {
                AnalogSensor as = (AnalogSensor) ComplexSensorProcessor.build(sensorData.getNum(), sensorData.getSensor());
                try {
                    csvPrinter.printRecord(
                            sensorData.getUnitPacket().getPacketId(),
                            DateTimeFormatter.ISO_INSTANT.format(sensorData.getUnitPacket().getEventTime()),
                            sensorData.getUnitPacket().getUnitId(),
                            sensorData.getUnitPacket().getTrId(),
                            sensorAttrib.getSensorId(),
                            sensorAttrib.getSensorTypeId(),
                            as.getValue().longValue()
                    );
                } catch (IOException e) {
                    LOGGER.error("BinarySensor write error", e);
                }
            }
        }
    }

    private static class IncidentRecordPrinter implements ApacheRecordPrinter<Incident> {
        @Override
        public void printRecord(Incident incident, CSVPrinter csvPrinter) {
            if (incident != null) {
                        try {
                            Long tr_id = null;
                            Long unit_id = null;
                            if (Objects.nonNull(incident.getIncidentPayload()) &&
                                    Objects.nonNull(incident.getIncidentPayload().getTrList())
                                    ){
                                tr_id = incident.getIncidentPayload().getTrList().stream().findFirst().orElse(null);
                            }

                            if (Objects.nonNull(incident.getIncidentPayload()) &&
                                    Objects.nonNull(incident.getIncidentPayload().getUnitList())
                                    ){
                                unit_id = incident.getIncidentPayload().getUnitList().stream().findFirst().orElse(null);
                            }

                            csvPrinter.printRecord(
                                    incident.getIncidentId(), //incident_id
                                    incident.getIncidentInitiatorId(), //incident_initiator_id
                                    incident.getIncidentStatusId(), //incident_status_id
                                    incident.getIncidentTypeId(), //incident_type_id
                                    DateTimeFormatter.ISO_INSTANT.format(incident.getTimeStart()), //time_start
                                    incident.getTrId() != null ? incident.getTrId() : tr_id,
                                    incident.getUnitId() != null ? incident.getUnitId() : unit_id,
                                    incident.getTimeFinish(), //time_finish
                                    incident.getAccountId(), //account_id
                                    incident.getParentIncidentId(), //parent_incident_id
                                    GSON.toJson(incident.getIncidentPayload()).replace("\"", "\"\"") //incident_params
                            );
                        } catch (IOException e) {
                            LOGGER.error("Incident write error", e);
                        }
            }
        }
    }


    private class SensorData {
        private final UnitPacket unitPacket;
        private final ComplexSensorHolder sensor;
        private final Integer num;

        SensorData(UnitPacket unitPacket, ComplexSensorHolder sensor, Integer num) {
            this.unitPacket = unitPacket;
            this.sensor = sensor;
            this.num = num;

        }

        public UnitPacket getUnitPacket() {
            return unitPacket;
        }

        public ComplexSensorHolder getSensor() {
            return sensor;
        }

        public Integer getNum() {
            return num;
        }
    }

}