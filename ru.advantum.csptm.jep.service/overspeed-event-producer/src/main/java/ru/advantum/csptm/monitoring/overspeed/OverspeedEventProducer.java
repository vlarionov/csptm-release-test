package ru.advantum.csptm.monitoring.overspeed;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.dbidgen.DbIdGenerator;
import ru.advantum.csptm.incident.model.*;
import ru.advantum.csptm.jep.service.glinker.model.event.LinkEvent;
import ru.advantum.csptm.jep.service.glinker.model.event.SimpleUnitPacket;
import ru.advantum.rabbitmq.consumer.RmqJsonTalker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by Leonid on 10.04.2017.
 */
public class OverspeedEventProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(OverspeedEventProducer.class);
    private final OverspeedEventProducerConfig config;
    private RmqJsonTalker rmqTalker;
    private final DbIdGenerator dbIdGenerator;
    private final int limit;
    private final int threshold;

    private final Map<Long, Integer> speedMap;

    public OverspeedEventProducer(final OverspeedEventProducerConfig config) throws Exception {
        this.config = config;
        speedMap = new ConcurrentHashMap<>();
        dbIdGenerator = new DbIdGenerator(config.dbIdGeneratorConfig, config.database.getConnectionPool());
        OverspeedConstantsReceiver constantsReceiver = new OverspeedConstantsReceiver("csptm-dev");
        limit = constantsReceiver.limit;
        threshold = constantsReceiver.threshold;
    }

    public void start() throws IOException, TimeoutException {

        rmqTalker = RmqJsonTalker.newBuilder()
                .setConsumerConfig(true, true, config.serviceConfig.consumer)
                .addConsumer(ru.advantum.csptm.jep.service.glinker.model.event.EventType.PACKET_LINK.name(),
                             LinkEvent[].class,
                             (consumerTag, envelope, basicProperties, message) -> consumeLinkEvents(message))
                .connect(config.serviceConfig.connection.newConnection(), config.serviceConfig.connection.newConnection());
    }

    private void consumeLinkEvents(LinkEvent[] linkEvents) {
        List<Incident> incidentBundle = new ArrayList<>();
        for (LinkEvent event : linkEvents) {
            try {
                Integer speed = event.getSimpleUnitPacket().getSpeed();
                if (speed > this.limit && event.getType().equals(LinkEvent.LinkType.LINKED)) {
                    long trId = event.getSimpleUnitPacket().getTrId();
                    int eventsCnt = speedMap.compute(trId, (key, curValue) -> curValue == null ? 1 : curValue + 1);

                    if (eventsCnt >= this.threshold) {
                        SimpleUnitPacket unitPacket = event.getSimpleUnitPacket();
                        LOGGER.debug("overspeed! {}", unitPacket);
                        final Integer deviation = speed - this.limit;

                        Incident incident = Incident.builder()
                                .incidentId(dbIdGenerator.nextId())
                                .incidentTypeId(IncidentType.TYPE.OVER_SPEED.get())
                                .timeStart(unitPacket.getEventTime())
                                .trId(unitPacket.getTrId())
                                .unitId(unitPacket.getUnitId())
                                .incidentStatusId(Incident.STATUS.NEW.get())
                                .incidentInitiatorId(Incident.INITIATOR.SYSTEM.get())
                                .incidentPayload(IncidentPayload.builder()
                                                         .pointStart(Point.of(unitPacket.getLon(), unitPacket.getLat()))
                                                         .factVal(speed.longValue())
                                                         .deviationVal(deviation.longValue())
                                                         .build()
                                )
                                .build();

                        incidentBundle.add(incident);
                    }

                    return;
                }

                if (event.getType().isGoodLink()) {
                    speedMap.remove(event.getSimpleUnitPacket().getTrId());
                }
            } catch (Exception e) {
                LOGGER.error("LinkEvent processing error", e);
            }
        }
        if (!incidentBundle.isEmpty()) {
            sendIncidentBundle(IncidentBundle.builder().incidentBundle(incidentBundle).build());
        }
    }


    private void sendIncidentBundle(IncidentBundle incidentBundle) {
        while (true) {
            try {
                rmqTalker.basicPublish(Incident.INCIDENT_ROUTER_EXCHANGE,
                                       Short.toString(IncidentType.TYPE.OVER_SPEED.get()),
                                       IncidentBundle.class.getSimpleName(),
                                       incidentBundle);
                break;
            } catch (Throwable ex) {
                LOGGER.error("send unit packets", ex);
                try {
                    TimeUnit.SECONDS.sleep(config.sendRetryPeriod);
                } catch (InterruptedException e) {
                    LOGGER.error("sleep", ex);
                }
            }
        }
    }

    public static void main(String[] args) throws Exception {
        OverspeedEventProducerConfig config = Configuration.unpackConfig(OverspeedEventProducerConfig.class, "overspeed-event-producer.xml");
        SqlMapperSingleton.registerInstance("csptm-dev", config.database.getConnectionPool());

        new OverspeedEventProducer(config).start();
    }
}
