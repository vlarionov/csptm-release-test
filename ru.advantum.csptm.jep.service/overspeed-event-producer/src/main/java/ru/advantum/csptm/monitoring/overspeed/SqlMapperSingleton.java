package ru.advantum.csptm.monitoring.overspeed;

import javax.sql.DataSource;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Класс получения доступа до БД
 */
public class SqlMapperSingleton {
    private static volatile Map<String, DataSource> dsMap;

    public static DataSource getInstance(final String name) {
        createMap();

        return dsMap.get(name);
    }

    /**
     * Метод создания карты соединений
     */
    private static void createMap() {
        if (null == dsMap) {
            synchronized (SqlMapperSingleton.class) {
                if (null == dsMap) {
                    dsMap = new ConcurrentHashMap<>();
                }
            }
        }
    }

    public static void registerInstance(final String name, DataSource dataSource) {
        createMap();

        dsMap.put(name, dataSource);
    }
}
