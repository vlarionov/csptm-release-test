package ru.advantum.csptm.monitoring.overspeed;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by Leonid on 11.04.2017.
 */
public class OverspeedConstantsReceiver {
    public final Integer limit;

    public final String limitConstant = "C_OVERSPEED_LIMIT";

    public final String thresholdConstant = "C_OVERSPEED_THRESHOLD";

    public final Integer threshold;

    private final String sql = "select value from asd.constants where name = ?";

    public OverspeedConstantsReceiver(final String databaseName) throws Exception {
        try(Connection connection = SqlMapperSingleton.getInstance(databaseName).getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, limitConstant);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            limit = resultSet.getInt(1);
            resultSet.close();
            statement.close();

            statement = connection.prepareStatement(sql);
            statement.setString(1, thresholdConstant);
            resultSet = statement.executeQuery();
            resultSet.next();
            threshold = resultSet.getInt(1);
            resultSet.close();
            statement.close();
        }
    }
}
