package ru.advantum.csptm.monitoring.overspeed;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.csptm.dbidgen.DbIdGeneratorConfig;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

/**
 * Created by Leonid on 10.04.2017.
 */
@Root(name = "event-producer-config")
public class OverspeedEventProducerConfig {
    @Element(name = "db-id-generator")
    public DbIdGeneratorConfig dbIdGeneratorConfig;

    @Attribute(name = "send-retry-period", required = false)
    public final int sendRetryPeriod;

    @Element(name = "rmq-service")
    public final JepRmqServiceConfig serviceConfig;

    @Element(name = "database")
    public DatabaseConnection database;

    public OverspeedEventProducerConfig() {
        sendRetryPeriod = 5;
        serviceConfig = new JepRmqServiceConfig();
    }
}
