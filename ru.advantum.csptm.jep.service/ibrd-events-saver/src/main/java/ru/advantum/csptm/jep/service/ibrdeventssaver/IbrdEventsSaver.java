package ru.advantum.csptm.jep.service.ibrdeventssaver;

import com.google.common.base.MoreObjects;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.artifact.csvsaver.CsvSaver;
import ru.advantum.csptm.artifact.csvsaver.apache.print.ApacheRecordPrinter;
import ru.advantum.csptm.artifact.csvsaver.limiters.Limiter;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdEvent;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdEventType;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdForecast;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdMessage;
import ru.advantum.rabbitmq.consumer.RmqJsonTalker;

import java.io.IOException;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

public class IbrdEventsSaver {

    private static final Logger LOGGER = LoggerFactory.getLogger(IbrdEventsSaver.class);
    private static final String CONFIG_FILE_NAME = "ibrd-events-saver.xml";

    private static final String[] CSV_HEADERS = {
            "event_id",
            "event_date",
            "event_type_id",
            "info_board_id",
            "channel_id",
            "message",
            "command_event_id",
            "error_type_id",
            "is_coords_valid",
            "lat",
            "lon",
            "forecasts_ids"
    };

    private final IbrdEventsSaverConfig config;

    private volatile Limiter limiter;

    private volatile RmqJsonTalker rmqJsonTalker;

    public IbrdEventsSaver(IbrdEventsSaverConfig config) {
        this.config = config;
    }

    public static void main(String[] args) throws Exception {
        try {
            IbrdEventsSaverConfig config = Configuration.unpackConfig(
                    IbrdEventsSaverConfig.class,
                    CONFIG_FILE_NAME
            );
            new IbrdEventsSaver(config).start();
        } catch (Exception e) {
            LOGGER.error("Error on startup", e);
        }
    }

    private void start() throws Exception {
        initWriter();
        initConsumer();
        initShutdownHook();

        LOGGER.info("IbrdEventSaver started");
    }

    private void initWriter() throws Exception {
        limiter = CsvSaver.createLimiter(
                config.getCsvSaverConfig(),
                CSV_HEADERS,
                IbrdEvent.class,
                new RecordPrinter()
        );
    }

    private void initConsumer() throws IOException, TimeoutException {
        RmqJsonTalker.Builder rmqBuilder = RmqJsonTalker.newBuilder();

        rmqBuilder.setConsumerConfig(
                true,
                true,
                config.getRmqConfig().consumer
        );

        rmqBuilder.addConsumer(
                "IbrdEvent",
                IbrdEvent.class,
                (tag, envelope, basicProperties, message) -> consumeEvents(message)
        );

        rmqJsonTalker = rmqBuilder.connect(
                config.getRmqConfig().connection.newConnection()
        );
    }

    private void initShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                rmqJsonTalker.cancelConsumer();
            } catch (IOException e) {
                LOGGER.error("Error on cancel consumer", e);
            }
            try {
                rmqJsonTalker.close();
                LOGGER.info("RmqJsonTalker stopped");
            } catch (IOException | TimeoutException e) {
                LOGGER.error("Error on close talker", e);
            }

            try {
                limiter.close();
                LOGGER.info("Limiter stopped");
            } catch (Exception e) {
                LOGGER.error("Error on close limiter", e);
            }
        }));
    }

    private void consumeEvents(IbrdEvent ibrdEvent) {
        try {
            limiter.append(ibrdEvent);
        } catch (Exception e) {
            LOGGER.error("IbrdEvent write error", e);
        }

    }

    private static class RecordPrinter implements ApacheRecordPrinter<IbrdEvent> {
        @Override
        public void printRecord(IbrdEvent ibrdEvent, CSVPrinter csvPrinter) throws Exception {
            String message = null;
            String forecasts = null;
            String errorTypeId = null;
            String isCoordsValid = null;

            if (ibrdEvent.getErrorType() != null) {
                errorTypeId = String.valueOf(ibrdEvent.getErrorType().getId());
            }
            if (ibrdEvent.getEventType().equals(IbrdEventType.SET_INFO)) {
                message = extractMessagesText(ibrdEvent);
                forecasts = Arrays.toString(extractForecasts(ibrdEvent)).replace("[", "{").replace("]", "}");
            }
            if (ibrdEvent.getCoordsValid() != null) {
                isCoordsValid = ibrdEvent.getCoordsValid().toString();
            }

            csvPrinter.printRecord(
                    ibrdEvent.getEventId(),
                    DateTimeFormatter.ISO_INSTANT.format(ibrdEvent.getEventDate().toInstant(ZoneOffset.UTC)),
                    ibrdEvent.getEventType().getId(),
                    ibrdEvent.getIbrdId(),
                    ibrdEvent.getChannelId(),
                    message,
                    ibrdEvent.getCommandEventId(),
                    errorTypeId,
                    isCoordsValid,
                    ibrdEvent.getLat(),
                    ibrdEvent.getLon(),
                    forecasts
            );

        }

        private Object[] extractForecasts(IbrdEvent event) {
            return MoreObjects.firstNonNull(event.getRowInfo(), event.getMonoRGBInfo())
                    .getForecastList().stream()
                    .filter(forecast -> forecast.getId() != null)//todo fix filter, create empty forecast in hub
                    .map(IbrdForecast::getId)
                    .toArray();
        }

        private String extractMessagesText(IbrdEvent event) {
            return MoreObjects.firstNonNull(event.getRowInfo(), event.getMonoRGBInfo())
                    .getMessageList()
                    .stream()
                    .map(IbrdMessage::getText)
                    .collect(Collectors.joining(" "));
        }
    }
}


