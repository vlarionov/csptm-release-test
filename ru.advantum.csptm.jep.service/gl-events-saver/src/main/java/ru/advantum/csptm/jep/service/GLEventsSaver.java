package ru.advantum.csptm.jep.service;

import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.artifact.csvsaver.CsvSaver;
import ru.advantum.csptm.artifact.csvsaver.apache.print.ApacheRecordPrinter;
import ru.advantum.csptm.artifact.csvsaver.limiters.Limiter;
import ru.advantum.csptm.jep.service.glinker.model.event.EventType;
import ru.advantum.csptm.jep.service.glinker.model.event.GraphSectionPass;
import ru.advantum.csptm.jep.service.glinker.model.event.LinkEvent;
import ru.advantum.csptm.jep.service.glinker.model.event.SimpleUnitPacket;
import ru.advantum.rabbitmq.consumer.RmqJsonTalker;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeoutException;

/**
 * Created by Leonid on 27.02.2017.
 */
public class GLEventsSaver {

    private static final Logger LOGGER = LoggerFactory.getLogger(GLEventsSaver.class);

    private static final String CONFIG_FILE_NAME = "gl-events-saver.xml";

    private static final String[] CSV_GRAPH_SECTION_HEADERS = {
            "TR_ID",
            "TT_ACTION_ID",
            "GRAPH_SECTION_ID",
            "ORDER_NUM",
            "BEGIN_TIME",
            "END_TIME"
    };

    private static final String[] CSV_LINK_EVENT_HEADERS = {
            "PACKET_ID",
            "TR_ID",
            "UNIT_ID",
            "EVENT_TIME",
            "TT_ACTION_ID",
            "GRAPH_SECTION_ID",
            "ORDER_NUM",
            "GRAPH_SECTION_OFFSET",
            "ROUND_OFFSET"
    };

    private final GLEventsSaverConfig config;

    private volatile Limiter graphSectionLimiter;
    private volatile Limiter linkEventLimiter;

    private volatile RmqJsonTalker rmqJsonTalker;

    public GLEventsSaver(final GLEventsSaverConfig config) {
        this.config = config;
    }

    public static void main(String[] args) {
        try {
            GLEventsSaverConfig config = Configuration.unpackConfig(GLEventsSaverConfig.class, CONFIG_FILE_NAME);
            new GLEventsSaver(config).start();
        } catch (Exception e) {
            LOGGER.error("Error on startup", e);
        }
    }

    private void start() throws Exception {
        initWriters();
        initConsumers();
        initShutdownHook();

        LOGGER.info("GL events saver started");
    }

    private void initWriters() {
        graphSectionLimiter = CsvSaver.createLimiter(config.getGraphSectionSaver(), CSV_GRAPH_SECTION_HEADERS,
                                                     GraphSectionPass.class, new GraphSectionPrinter());
        linkEventLimiter = CsvSaver.createLimiter(config.getLinkEventSaverConfig(), CSV_LINK_EVENT_HEADERS,
                                                  LinkEvent.class, new LinkEventsPrinter());
    }

    private void initConsumers() throws IOException, TimeoutException {
        RmqJsonTalker.Builder rmqBuilder = RmqJsonTalker.newBuilder();

        rmqBuilder.setConsumerConfig(true, true, config.getRmqConfig().consumer);
        rmqBuilder.addConsumer(EventType.GRAPH_SECTION_PASS.name(), GraphSectionPass[].class,
                               (tag, envelope, basicProperties, message) -> consumeGraphSection(message));
        rmqBuilder.addConsumer(EventType.PACKET_LINK.name(), LinkEvent[].class,
                               (tag, envelope, basicProperties, message) -> consumeLinkEvents(message));

        rmqJsonTalker = rmqBuilder.connect(config.getRmqConfig().connection.newConnection());
    }

    private void initShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                rmqJsonTalker.cancelConsumer();
            } catch (IOException e) {
                LOGGER.error("Error on cancel consumer", e);
            }
            try {
                rmqJsonTalker.close();
            } catch (IOException | TimeoutException e) {
                LOGGER.error("Error on close talker", e);
            }
            LOGGER.info("RmqJsonTalker stopped");

            try {
                graphSectionLimiter.close();
            } catch (Exception e) {
                LOGGER.error("Error on close graphSectionLimiter", e);
            }
            try {
                linkEventLimiter.close();
            } catch (Exception e) {
                LOGGER.error("Error on close linkEventLimiter", e);
            }
            LOGGER.info("Limiters stopped");
        }));
    }

    private void consumeGraphSection(GraphSectionPass[] graphSectionPass) {
        for (GraphSectionPass gs : graphSectionPass) {
            try {
                graphSectionLimiter.append(gs);
            } catch (Exception e) {
                LOGGER.error("GraphSection write error", e);
            }
        }
    }

    private void consumeLinkEvents(LinkEvent[] linkEvents) {
        for (LinkEvent event : linkEvents) {
            try {
                if (event.getType().isGoodLink()) {
                    linkEventLimiter.append(event);
                }
            } catch (Exception e) {
                LOGGER.error("LinkEvent write error", e);
            }
        }
    }

    private static class LinkEventsPrinter implements ApacheRecordPrinter<LinkEvent> {

        @Override
        public void printRecord(LinkEvent linkEvent, CSVPrinter csvPrinter) throws Exception {
            SimpleUnitPacket simpleUnitPacket = linkEvent.getSimpleUnitPacket();
            csvPrinter.printRecord(simpleUnitPacket.getPacketId(),
                                   simpleUnitPacket.getTrId(),
                                   simpleUnitPacket.getUnitId(),
                                   DateTimeFormatter.ISO_INSTANT.format(simpleUnitPacket.getEventTime()),
                                   linkEvent.getTtActionId(),
                                   linkEvent.getSectionLink().getGraphSectionId(),
                                   linkEvent.getSectionLink().getGraphSectionOrderNum(),
                                   linkEvent.getSectionLink().getSectionOffset(),
                                   linkEvent.getSectionLink().getRoundOffset());
        }
    }

    private static class GraphSectionPrinter implements ApacheRecordPrinter<GraphSectionPass> {

        @Override
        public void printRecord(GraphSectionPass graphSectionPass, CSVPrinter csvPrinter) throws Exception {
            csvPrinter.printRecord(graphSectionPass.getTrId(),
                                   graphSectionPass.getTtActionId(),
                                   graphSectionPass.getGraphSectionId(),
                                   graphSectionPass.getGraphSectionOrderNum(),
                                   DateTimeFormatter.ISO_INSTANT.format(graphSectionPass.getStartTime()),
                                   DateTimeFormatter.ISO_INSTANT.format(graphSectionPass.getEndTime()));
        }
    }
}
