package ru.advantum.csptm.jep.service;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.csptm.artifact.csvsaver.config.CsvSaverConfig;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

/**
 * Created by Leonid on 27.02.2017.
 */
@Root(name = "gl-events-saver")
public class GLEventsSaverConfig {
    @Element(name = "rmq-service")
    private JepRmqServiceConfig rmqConfig;

    @Element(name = "link-event-saver")
    private CsvSaverConfig linkEventSaverConfig;

    @Element(name = "graph-section-saver")
    private CsvSaverConfig graphSectionSaver;

    public JepRmqServiceConfig getRmqConfig() {
        return rmqConfig;
    }

    public CsvSaverConfig getLinkEventSaverConfig() {
        return linkEventSaverConfig;
    }

    public CsvSaverConfig getGraphSectionSaver() {
        return graphSectionSaver;
    }
}
