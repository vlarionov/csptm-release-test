package ru.advantum.csptm.monitoring.command;

/**
 * Created by kukushkin on 6/1/17.
 */
public enum  CommandStatus {
    NEW(0),        // Новое сообщение
    SENT(1),       // Сообщение передано
    NOT_ON_LINE(2),      // Ошибка
    ERROR(3);        // Не определено ;

    private int status;

    CommandStatus(int status) {
        this.status = status;
    }


    public int getStatus() {
        return status;
    }

}
