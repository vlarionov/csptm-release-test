package ru.advantum.csptm.monitoring.command;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.time.Instant;

interface CommandResponseHandlerMapper {

    @Select("select cmnd.insert_command_response(#{cmndRequestId}, #{responseName}, #{responseTime}, #{responseParam})")

    Long insertCommandResponse(
            @Param("cmndRequestId") Long cmndRequestId,
            @Param("responseTime") Instant responseTime,
            @Param("responseName") String responseName,
            @Param("responseParam") String responseParam);

    @Select("select cmnd.insert_sms_ask(#{cmndRequestId}, #{status})")

    Long insertSmsAsk(
            @Param("cmndRequestId") Long cmndRequestId,
            @Param("status") Integer status
    );

    @Select("select cmnd.insert_sms(#{phone}, #{providerId}, #{text}, #{deliveryTime})")

    Long insertSms(
            @Param("phone") String phone,
            @Param("providerId") String providerId,
            @Param("text") String text,
            @Param("deliveryTime") Instant deliveryTime
    );

}
