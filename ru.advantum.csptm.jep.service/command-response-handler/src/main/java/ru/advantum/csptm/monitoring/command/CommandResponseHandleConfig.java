package ru.advantum.csptm.monitoring.command;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

/**
 * Created by kukushkin on 31.05.2017.
 */

@Root(name = "command-responce-saver-config")
public class CommandResponseHandleConfig {

        @Attribute(name = "send-retry-period", required = false)
        public final int sendRetryPeriod;

        @Element(name = "rmq-service")
        public final JepRmqServiceConfig serviceConfig;

        @Element(name = "database")
        public DatabaseConnection database;

        public CommandResponseHandleConfig() {
            sendRetryPeriod = 5;
            serviceConfig = new JepRmqServiceConfig();
        }
}
