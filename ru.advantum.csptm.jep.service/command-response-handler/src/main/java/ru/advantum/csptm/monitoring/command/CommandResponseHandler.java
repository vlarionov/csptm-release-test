package ru.advantum.csptm.monitoring.command;

import com.google.gson.Gson;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.jep.proto.*;
import ru.advantum.csptm.service.sms.model.SmsAckMessage;
import ru.advantum.csptm.service.sms.model.SmsMessage;
import ru.advantum.csptm.service.sms.model.SmsStatus;
import ru.advantum.rabbitmq.consumer.RmqJsonTalker;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Objects;
import java.util.concurrent.TimeoutException;

/**
 * Created by kukushkin on 31.05.2017.
 */
public class CommandResponseHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommandResponseHandler.class);
    private final CommandResponseHandleConfig config;
    private static final Gson gson = new Gson();

    private CommandResponseHandler(CommandResponseHandleConfig config) {
        this.config = config;
    }

    private void start() throws IOException, TimeoutException, SQLException {

        SqlMapperSingleton.setConfiguration(config.database.getConnectionPool());

        RmqJsonTalker rmqTalker = RmqJsonTalker.newBuilder()
                .setConsumerConfig(true, true, config.serviceConfig.consumer)
                .addConsumer(CommonMessageType.COORDS.name(),
                        UnitPacketsBundle.class,
                        (consumerTag, envelope, basicProperties, message) -> consumeCoords(message))
                .addConsumer(SmsAckMessage.class.getSimpleName(),
                        SmsAckMessage.class,
                        (consumerTag, envelope, basicProperties, message) -> consumeAskMessage(message))
                .addConsumer(SmsMessage.class.getSimpleName(),
                        SmsMessage.class,
                        (consumerTag, envelope, basicProperties, message) -> consumeSmsMessage(message))
                .connect(config.serviceConfig.connection.newConnection());
    }

    private void consumeCoords(UnitPacketsBundle unitPacketsBundle) {
        for (UnitPacket unitPacket : unitPacketsBundle.getPackets()) {
            if (!Objects.isNull(unitPacket.getFlags()) && unitPacket.getFlags().contains(CommonPacketFlag.COMMAND_REPLY.name())) {
                if (unitPacket.getAttributes() != null) {
                    unitPacket.getAttributes().forEach((s, o) -> {
                        CommandPacket cp = gson.fromJson((String) o, CommandPacket.class);
                        LOGGER.info("Got command reply [{}] [{}]", s, o);
                        try (SqlSession session = SqlMapperSingleton.getInstance().openSession()) {
                            CommandResponseHandlerMapper mapper = session.getMapper(CommandResponseHandlerMapper.class);
                                LOGGER.info("Insert command response");
                                Long id = mapper.insertCommandResponse(cp.getCmdId()
                                        , cp.getTimeSent()
                                        , cp.getCmdName()
                                        , gson.toJson(cp.getParameters()));

                                session.commit();
                                LOGGER.info("Insert command response with id [{}]", id);
                        } catch (Throwable ex) {
                            LOGGER.error("Insert command response", ex);
                        }
                    });
                }
            }



        }
    }

    private void consumeAskMessage(SmsAckMessage askMessage) {
        LOGGER.info("Got SmsAckMessage [{}] [{}] [{}]", askMessage.getId(), askMessage.getStatus(), askMessage.toString());
        try (SqlSession session = SqlMapperSingleton.getInstance().openSession()) {
            CommandResponseHandlerMapper mapper = session.getMapper(CommandResponseHandlerMapper.class);
            LOGGER.info("Insert SmsAckMessage ");

            int status = CommandStatus.ERROR.getStatus();

            if (askMessage.getStatus() == SmsStatus.SENT) {
                status = CommandStatus.SENT.getStatus();
            }
            Long id = mapper.insertSmsAsk(askMessage.getId(), status);

            session.commit();
            LOGGER.info("Insert SmsAckMessage  with id [{}]", id);
        } catch (Throwable ex) {
            LOGGER.error("Insert SmsAckMessage", ex);
        }
    }

    private void consumeSmsMessage(SmsMessage smsMessage) {
        LOGGER.info("Got smsMessage [{}] [{}] [{}] [{}] [{}] [{}]",
                smsMessage.getId(),
                smsMessage.getCreationDate(),
                smsMessage.getDeliveryDate(),
                smsMessage.getPhone(),
                smsMessage.getProviderID(),
                smsMessage.getText());

        try (SqlSession session = SqlMapperSingleton.getInstance().openSession()) {
            CommandResponseHandlerMapper mapper = session.getMapper(CommandResponseHandlerMapper.class);
            LOGGER.info("Insert SmsMessage ");

            Long id = mapper.insertSms(smsMessage.getPhone(), smsMessage.getProviderID(), smsMessage.getText(), smsMessage.getDeliveryDate());

            session.commit();
            LOGGER.info("Insert Sms  with id [{}]", id);
        } catch (Throwable ex) {
            LOGGER.error("Insert sms", ex);
        }
    }



    public static void main(String[] args) throws Exception {
        CommandResponseHandleConfig config = Configuration.unpackConfig(CommandResponseHandleConfig.class, "command-response-handler.xml");
        new CommandResponseHandler(config).start();
    }
}
