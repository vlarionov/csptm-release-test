package ru.advantum.csptm.service.testkill;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

/**
 * Created by kaganov on 03/05/2017.
 */
@Root(name = "test-kill-jep-service")
public class TestKillJepServiceConfig {

    @Element(name = "rmq-service")
    public final JepRmqServiceConfig serviceConfig = new JepRmqServiceConfig();
}
