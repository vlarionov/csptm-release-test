package ru.advantum.csptm.service.testkill;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.advantum.csptm.jep.proto.CommonMessageType;
import ru.advantum.csptm.jep.proto.UnitPacket;
import ru.advantum.csptm.jep.proto.UnitPacketsBundle;
import ru.advantum.csptm.jep.proto.sensors.ComplexSensorHolder;
import ru.advantum.csptm.spring.rmq.TypeAdder;

import javax.annotation.PreDestroy;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by kaganov on 03/05/2017.
 */
@Component
@RabbitListener(queues = "${test-kill-jep-service.rmq-service.rmq-consumer.queue}")
public class TestKillJepService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestKillJepService.class);

    private final Gson gson;
    private final AtomicLong opCounter = new AtomicLong(0);
    private final AmqpTemplate amqpTemplate;

    @Autowired
    public TestKillJepService(Gson gson, AmqpTemplate amqpTemplate) {
        this.gson = gson;
        this.amqpTemplate = amqpTemplate;
    }

    @RabbitHandler
    public void consume(String message) throws InterruptedException {
        opCounter.incrementAndGet();
        LOGGER.info("Got message {} [{}]", message.getClass(), message);
        Thread.sleep(1000);

        amqpTemplate.convertAndSend("Reply " + message);
    }

    @RabbitHandler
    public void consume(UnitPacketsBundle message) throws InterruptedException {
        //        opCounter.incrementAndGet();
        //        LOGGER.info("Got message {} [{}]", message.getClass(), message);
        //        Thread.sleep(2);
        for (UnitPacket unitPacket : message.getPackets()) {
            Map<Integer, ComplexSensorHolder> complexSensors = unitPacket.getComplexSensors();
            if (complexSensors != null) {
                if (complexSensors.containsKey(214)) {
                    if (Objects.equals(complexSensors.get(214).getValues()[0], BigDecimal.ONE)) {
                        LOGGER.info("{}", unitPacket);
                    }
                }
            }
        }
    }

    @RabbitHandler
    public void consume(Long[] message) throws InterruptedException {
        opCounter.incrementAndGet();
        LOGGER.info("Got message {} [{}]", message.getClass(), Arrays.toString(message));
        Thread.sleep(2);
    }

    //    @PostConstruct
    public void fillQueue() {
        for (int i = 0; i < 100_000; i++) {
            amqpTemplate.convertAndSend("Hello " + i);
        }
    }

    //    @Scheduled(fixedDelay = 500)
    public void fireMessages() throws InterruptedException {
        int l = (int) (opCounter.get() % 3);
        switch (l) {
            case 0:
                amqpTemplate.convertAndSend((Object) ("Hello " + opCounter.get()),
                                            TypeAdder.of("STR"));
                break;
            case 1:
                amqpTemplate.convertAndSend(UnitPacketsBundle.of(-1,
                                                                 UnitPacket.newBuilder()
                                                                         .setUnitId(opCounter.get())
                                                                         .setEventTime(Instant.now())
                                                                         .build()),
                                            TypeAdder.of(CommonMessageType.COORDS.name()));
                break;
            case 2:
                amqpTemplate.convertAndSend(new Long[]{(long) l, opCounter.get()},
                                            TypeAdder.of("LONGS"));
                break;
        }
    }

    //    @Scheduled(fixedDelay = 1000)
    private void showMsg() {
        LOGGER.info("OK {}", Math.round(Math.random() * 100));
    }

    @PreDestroy
    public void stop() {
        LOGGER.info("Service stopping");
    }
}
