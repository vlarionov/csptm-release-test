package ru.advantum.csptm.service.testkill;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;
import ru.advantum.csptm.jep.proto.UnitPacketsBundle;
import ru.advantum.csptm.spring.properties.AbstractPropertySourceFactory;
import ru.advantum.csptm.spring.rmq.ConsumerProperties;
import ru.advantum.csptm.spring.rmq.MessageTypeResolver;
import ru.advantum.csptm.spring.rmq.MultiTypeResolver;

import java.util.concurrent.TimeUnit;

/**
 * Created by kaganov on 01/06/2017.
 */
@SpringBootApplication
@EnableScheduling
@PropertySource(value = "", name = Application.CONFIG_FILE_NAME, factory = Application.ConfigPropertySourceFactory.class)
public class Application {

    public static final String CONFIG_FILE_NAME = "test-kill-jep-service.xml";

    @Bean
    public JepRmqServiceConfig rmqConfig(TestKillJepServiceConfig config) {
        return config.serviceConfig;
    }

    @Bean
    public ConsumerProperties consumerProperties() {
        return ConsumerProperties.newBuilder()
                .setShutdownTimeout(TimeUnit.SECONDS.toMillis(10))
                .build();
    }

    @Bean
    public MessageTypeResolver messageTypeResolver() {
        return MultiTypeResolver.newBuilder()
                .addPair("STR", String.class)
                .addPair("COORDS", UnitPacketsBundle.class)
                .addPair("LONGS", Long[].class)
                .build();
    }

    @Bean
    public static TestKillJepServiceConfig applicationConfig() throws Exception {
        return ru.advantum.config.Configuration.unpackConfig(
                TestKillJepServiceConfig.class,
                CONFIG_FILE_NAME
        );
    }

    public static void main(String[] args) throws InterruptedException {
        new SpringApplicationBuilder(Application.class)
                .bannerMode(Banner.Mode.OFF)
                .web(false)
                .run(args);
    }

    static class ConfigPropertySourceFactory extends AbstractPropertySourceFactory<TestKillJepServiceConfig> {

        @Override
        public TestKillJepServiceConfig getConfig() throws Exception {
            return applicationConfig();
        }
    }
}
