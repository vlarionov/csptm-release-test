package ru.advantum.csptm.jep.service.manager.model;

/**
 * Created by kaganov on 05/12/2016.
 */
public class UnitBound {

    private final int hubId;
    private final String unitNum;
    private final long unitId;

    public UnitBound() {
        hubId = 0;
        unitNum = null;
        unitId = 0;
    }

    public boolean isDistinctFrom(UnitBound other) {
        return other == null || unitId != other.unitId;
    }

    public int getHubId() {
        return hubId;
    }

    public String getUnitNum() {
        return unitNum;
    }

    public long getUnitId() {
        return unitId;
    }

    @Override
    public String toString() {
        return "UnitBound{" +
                "hubId=" + hubId +
                ", unitNum='" + unitNum + '\'' +
                ", unitId=" + unitId +
                '}';
    }
}
