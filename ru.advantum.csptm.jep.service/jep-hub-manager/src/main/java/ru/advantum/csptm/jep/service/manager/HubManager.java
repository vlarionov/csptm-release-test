package ru.advantum.csptm.jep.service.manager;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.jep.service.manager.db.HubManagerMapper;
import ru.advantum.csptm.jep.service.manager.db.SqlMapperSingleton;
import ru.advantum.csptm.jep.service.manager.model.HubQueue;
import ru.advantum.csptm.jep.service.manager.model.UnitBound;
import ru.advantum.csptm.jep.service.manager.unitlist.UnitListController;
import ru.advantum.csptm.jep.proto.CommonMessageType;
import ru.advantum.csptm.jep.proto.HubStart;
import ru.advantum.csptm.jep.proto.UnitBoundChange;
import ru.advantum.rabbitmq.consumer.RmqJsonTalker;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * Created by kaganov on 05/12/2016.
 */
public class HubManager implements HubUnitListSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(HubManager.class);

    private final HubManagerConfig config;
    private final UnitListController unitListController = new UnitListController(this);
    private final AtomicReference<Map<Integer, String>> hubQueues = new AtomicReference<>(new HashMap<>());
    private final ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();

    private RmqJsonTalker rmqTalker;

    public HubManager(HubManagerConfig config) {
        this.config = config;
    }

    public void start() throws SQLException, IOException, TimeoutException {
        SqlMapperSingleton.setConfiguration(config.database.getConnectionPool());

        Connection connection = config.connection.newConnection();

        Channel channel = connection.createChannel();
        channel.queueDeclare(config.consumer.queue, false, false, false, null);
        channel.close();

        rmqTalker = RmqJsonTalker.newBuilder()
                .setConsumerConfig(true, true, config.consumer)
                .addConsumer(CommonMessageType.HUB_START.name(), HubStart.class,
                             (consumerTag, envelope, basicProperties, message) -> consumeHubStart(message))
                .connect(connection);

        updateHubQueues();

        ses.scheduleAtFixedRate(this::updateHubQueues, 15, 30, TimeUnit.SECONDS);
        ses.scheduleAtFixedRate(this::updateUnitBounds, 0, 30, TimeUnit.SECONDS);

        LOGGER.info("HubManager started");
    }

    @Override
    public boolean sendUnitBoundsChange(int hubId, List<UnitBoundChange> unitBoundChanges) {
        String hubQueue = hubQueues.get().get(hubId);
        if (hubQueue != null) {
            try {
                rmqTalker.basicPublish("", hubQueue, CommonMessageType.UNIT_BOUNDS.name(), unitBoundChanges);
                return true;
            } catch (IOException ex) {
                LOGGER.error("Send unit bounds change hubId {}", hubId, ex);
                return false;
            }
        } else {
            LOGGER.warn("No queue for hub {}", hubId);
            return false;
        }
    }

    private void updateHubQueues() {
        try (SqlSession session = SqlMapperSingleton.getInstance().openSession()) {
            HubManagerMapper mapper = session.getMapper(HubManagerMapper.class);
            List<HubQueue> hubQueuesList = mapper.getHubQueues();
            LOGGER.info("Hubs cnt {}", hubQueuesList.size());

            Set<String> newQueues = hubQueuesList.stream().map(HubQueue::getHubQueue).collect(Collectors.toSet());
            newQueues.removeAll(hubQueues.get().values());

            for (String newQueue : newQueues) {
                LOGGER.info("New queue {}", newQueue);
                rmqTalker.getWriteChannel().queueDeclare(newQueue, false, false, false, null);
            }

            hubQueues.set(hubQueuesList.stream().collect(Collectors.toMap(HubQueue::getHubId, HubQueue::getHubQueue)));
        } catch (Throwable ex) {
            LOGGER.error("updateHubQueues", ex);
        }
    }

    private void updateUnitBounds() {
        try {
            unitListController.run();
        } catch (Throwable ex) {
            LOGGER.error("updateUnitBounds", ex);
        }
    }

    private void consumeHubStart(HubStart message) {
        try {
            LOGGER.info("Hub started {}", message.getHubId());
            Optional<List<UnitBound>> hubBounds = unitListController.getHubBounds(message.getHubId());
            if (hubBounds.isPresent()) {
                List<UnitBoundChange> unitBoundChanges = hubBounds.get().stream()
                        .map(ub -> new UnitBoundChange(ub.getUnitNum(),
                                                       ub.getUnitId(),
                                                       UnitBoundChange.ChangeType.ADDED))
                        .collect(Collectors.toList());
                sendUnitBoundsChange(message.getHubId(), unitBoundChanges);
            }
        } catch (Throwable ex) {
            LOGGER.error("consumeHubStart {}", message, ex);
        }
    }

    public static void main(String[] args) throws Exception {
        HubManagerConfig config = Configuration.unpackConfig(HubManagerConfig.class, "jep-hub-manager.xml");
        new HubManager(config).start();
    }
}
