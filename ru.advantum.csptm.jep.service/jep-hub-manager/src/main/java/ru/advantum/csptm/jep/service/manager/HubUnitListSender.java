package ru.advantum.csptm.jep.service.manager;

import ru.advantum.csptm.jep.proto.UnitBoundChange;

import java.util.List;

/**
 * Created by kaganov on 07/12/2016.
 */
public interface HubUnitListSender {

    boolean sendUnitBoundsChange(int hubId, List<UnitBoundChange> unitBoundChanges);

}
