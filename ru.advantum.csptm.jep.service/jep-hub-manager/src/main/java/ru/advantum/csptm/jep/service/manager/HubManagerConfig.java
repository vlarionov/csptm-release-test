package ru.advantum.csptm.jep.service.manager;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.rabbitmq.config.RmqConnectionConfig;
import ru.advantum.rabbitmq.config.RmqConsumerConfig;

/**
 * Created by kaganov on 05/12/2016.
 */
@Root(name = "hub-manager")
public class HubManagerConfig {

    @Element(name = "database")
    public final DatabaseConnection database;

    @Element(name = "rmq-connection")
    public final RmqConnectionConfig connection;

    @Element(name = "manager-consumer")
    public final RmqConsumerConfig consumer;

    public HubManagerConfig() {
        database = new DatabaseConnection();
        connection = new RmqConnectionConfig();
        consumer = new RmqConsumerConfig();
    }
}
