package ru.advantum.csptm.jep.service.manager.unitlist;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.proto.UnitBoundChange;
import ru.advantum.csptm.jep.service.manager.HubUnitListSender;
import ru.advantum.csptm.jep.service.manager.db.HubManagerMapper;
import ru.advantum.csptm.jep.service.manager.db.SqlMapperSingleton;
import ru.advantum.csptm.jep.service.manager.model.UnitBound;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import static java.util.stream.Collectors.*;

/**
 * Created by kaganov on 06/12/2016.
 */
public class UnitListController implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(UnitListController.class);

    private final AtomicReference<Map<Integer, Map<String, UnitBound>>> unitBounds = new AtomicReference<>(new HashMap<>());
    private final HubUnitListSender unitListSender;

    public UnitListController(HubUnitListSender unitListSender) {
        this.unitListSender = unitListSender;
    }

    private static UnitBoundChange toUbChange(UnitBound ub, UnitBoundChange.ChangeType changeType) {
        return new UnitBoundChange(ub.getUnitNum(), ub.getUnitId(), changeType);
    }

    private Map<Integer, Map<String, UnitBound>> getNewUnitBounds() {
        try (SqlSession session = SqlMapperSingleton.getInstance().openSession()) {
            HubManagerMapper mapper = session.getMapper(HubManagerMapper.class);
            List<UnitBound> unitBoundList = mapper.getUnitBounds();
            LOGGER.info("UnitBounds cnt {}", unitBoundList.size());

            return unitBoundList.stream().collect(groupingBy(UnitBound::getHubId,
                                                             toMap(UnitBound::getUnitNum,
                                                                   ub -> ub)));
        }
    }

    public Optional<List<UnitBound>> getHubBounds(int hubId) {
        Map<String, UnitBound> hubBounds = unitBounds.get().get(hubId);
        if (hubBounds == null) {
            return Optional.empty();
        } else {
            return Optional.of(new ArrayList<>(hubBounds.values()));
        }
    }

    @Override
    public void run() {
        Map<Integer, Map<String, UnitBound>> newBounds = getNewUnitBounds();
        Map<Integer, Map<String, UnitBound>> oldBounds = unitBounds.get();

        Map<Integer, List<UnitBoundChange>> hubChanges = new HashMap<>();

        Set<Integer> newHubs = new HashSet<>(newBounds.keySet());
        newHubs.removeAll(oldBounds.keySet());

        newHubs.forEach(hubId -> {
            List<UnitBoundChange> boundChangeList = newBounds.get(hubId).values().stream()
                    .map(ub -> toUbChange(ub, UnitBoundChange.ChangeType.ADDED))
                    .collect(toList());

            LOGGER.info("New hub {}, trs {}", hubId, boundChangeList.size());
            hubChanges.put(hubId, boundChangeList);
        });

        Set<Integer> existingHubs = new HashSet<>(newBounds.keySet());
        existingHubs.retainAll(oldBounds.keySet());

        existingHubs.forEach(hubId -> {
            List<UnitBoundChange> hubBoundChanges = hubChanges.computeIfAbsent(hubId, k -> new ArrayList<>());
            Map<String, UnitBound> oldHubBounds = oldBounds.get(hubId);
            Map<String, UnitBound> newHubBounds = newBounds.get(hubId);

            Set<String> deletedBounds = new HashSet<>(oldHubBounds.keySet());
            deletedBounds.removeAll(newHubBounds.keySet());

            deletedBounds.stream()
                    .map(unitNum -> toUbChange(oldHubBounds.get(unitNum), UnitBoundChange.ChangeType.DELETED))
                    .forEach(hubBoundChanges::add);

            newHubBounds.values().stream()
                    // null тоже отличаются, поэтому если в старых связках нет этого unitNum, то все хорошо будет
                    .filter(ub -> ub.isDistinctFrom(oldHubBounds.get(ub.getUnitNum())))
                    .map(ub -> toUbChange(ub, UnitBoundChange.ChangeType.ADDED))
                    .forEach(hubBoundChanges::add);
        });

        boolean sendSuccessful = hubChanges.entrySet().stream()
                .allMatch((hubUnitBoundChanges) -> {
                    int hubId = hubUnitBoundChanges.getKey();
                    List<UnitBoundChange> unitBoundChanges = hubUnitBoundChanges.getValue();
                    if (!unitBoundChanges.isEmpty()) {
                        LOGGER.info("Hub {} changes cnt {}", hubId, unitBoundChanges.size());
                        if (!unitListSender.sendUnitBoundsChange(hubId, unitBoundChanges)) {
                            LOGGER.error("Send failed, abort update");
                            return false;
                        } else {
                            LOGGER.info("Send successful");
                            return true;
                        }
                    } else {
                        return true;
                    }
                });

        if (sendSuccessful) {
            LOGGER.info("Sent tr updates finished");
            unitBounds.set(newBounds);
        }
    }
}
