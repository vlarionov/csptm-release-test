package ru.advantum.csptm.jep.service.manager.model;

/**
 * Created by kaganov on 06/12/2016.
 */
public class HubQueue {

    private final int hubId;
    private final String hubQueue;

    public HubQueue() {
        hubId = 0;
        hubQueue = null;
    }

    public int getHubId() {
        return hubId;
    }

    public String getHubQueue() {
        return hubQueue;
    }
}
