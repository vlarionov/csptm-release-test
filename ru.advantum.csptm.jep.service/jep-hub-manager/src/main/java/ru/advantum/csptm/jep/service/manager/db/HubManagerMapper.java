package ru.advantum.csptm.jep.service.manager.db;

import ru.advantum.csptm.jep.service.manager.model.HubQueue;
import ru.advantum.csptm.jep.service.manager.model.UnitBound;

import java.util.List;

/**
 * Created by kaganov on 05/12/2016.
 */
public interface HubManagerMapper {

    List<HubQueue> getHubQueues();

    List<UnitBound> getUnitBounds();
}
