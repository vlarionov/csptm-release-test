package ru.advantum.csptm.jep.service.egts.packet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.hub.egts.packet.EGTS_SERVICE;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_ABS_AN_SENS_DATA;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_AD_SENSORS_DATA;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_POS_DATA;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_POS_DATA_ALT;
import ru.advantum.csptm.jep.hub.egts.packet.types.AbstractEGTSSubrecordType;
import ru.advantum.csptm.jep.hub.egts.packet.types.EGTSAdSensors;
import ru.advantum.csptm.jep.hub.egts.packet.types.EGTSTelematic;
import ru.advantum.csptm.jep.hub.egts.packet.types.EGTSTrinfoOnly;
import ru.advantum.hub.util.IConvertor;
import ru.advantum.protoV3.Event;
import ru.advantum.protoV3.data.KVFloat;
import ru.advantum.protoV3.data.KVSLong;
import ru.advantum.protoV3.data.KVULong;
import ru.advantum.protocore3.CorePacket;

import java.util.*;

public class COV_AMTS2EGTS implements IConvertor<COV_Object.AMTSRecord, List<AbstractEGTSSubrecordType>> {

    protected static final Logger LOGGER = LoggerFactory.getLogger(COV_AMTS2EGTS.class);

    private static final int KV_NO_KEY = -256;
    private static final long KV_NO_VALUE = -10101;

    public COV_AMTS2EGTS() {
    }

    @Override
    public List<AbstractEGTSSubrecordType> convert(COV_Object.AMTSRecord in) throws ConvertException {
        List<AbstractEGTSSubrecordType> result = new ArrayList<AbstractEGTSSubrecordType>();
        final EGTS_SERVICE.OID oid = new EGTS_SERVICE.OID(in.info.idTr.get());
        final EGTS_SERVICE.TM tm = new EGTS_SERVICE.TM(in.info.utcTime.get() - EGTSFactory.DATE_01_01_2010);
        if (in.loc != null) {
            result.add(createSrPosData(in, oid, tm));
        }
        if (in.sensors.size() > 0) {
            Map<Integer, Map<Double, CorePacket>> map = new TreeMap<>();
            for (CorePacket kv : in.sensors) {
//                result.add(new EGTSAbsAnSensors( createSrAbsAnSensData(kv), oid, tm));
                int key = getKVKey(kv);
                if (key == KV_NO_KEY) {
                    continue;
                }
                double absPkgNumber = key * 1.0 / 8;
                int pkgMem = (int) absPkgNumber;
                if (absPkgNumber > pkgMem * 1.0) {
                    pkgMem += 1;
                }
                if (!map.containsKey(pkgMem)) {
                    map.put(pkgMem, new HashMap<Double, CorePacket>());
                }
                map.get(pkgMem).put(absPkgNumber, kv);
            }
            result.addAll(createSrAdSensData(map, oid, tm));
        }
        if (result.size() == 0) {
            result.add(new EGTSTrinfoOnly(oid, tm));
        }
        return result;
    }

    private EGTSTelematic createSrPosData(COV_Object.AMTSRecord in, EGTS_SERVICE.OID oid,
                                          EGTS_SERVICE.TM tm ) {
        final EGTS_SR_POS_DATA telematic = new EGTS_SR_POS_DATA();
        telematic.NTM.set(in.loc.gpsTime.get() - EGTSFactory.DATE_01_01_2010);
        telematic.LAT.set(EGTSFactory.toEGTSLat(in.loc.lat.get()));
        telematic.setLAHS(in.loc.lat.get() < 0);
        telematic.LONG.set(EGTSFactory.toEGTSLon(in.loc.lon.get()));
        telematic.setLOHS(in.loc.lon.get() < 0);
        telematic.setMV(in.info.idEvent.get() == Event.MOVING.getKey());
        telematic.setCS(Boolean.FALSE); // 0x00 - WGS84
        telematic.setFIX(Boolean.FALSE);
        telematic.setVLD(in.loc.isLocationValid());
        telematic.setBB((in.info.utcTime.get() < System.currentTimeMillis() / 1000L - 10 * 60));
        telematic.setSPD(in.loc.speed.get() * 10);
        telematic.setDIR(in.loc.heading.get());
        // хардкод для SOLT, передача разных тревог через idDeviceEevent
        if (in.info.idDeviceEvent.get() == 13) {
            telematic.setSRC(13);
        } else if (in.info.idDeviceEvent.get() == 15) {
            telematic.setSRC(15);
        } else {
            telematic.setSRC(in.info.idEvent.get() == Event.RED_BUTTON.getKey() ? 13 : 16); // Таблица №3. Появление данных
            // от внешнего сервиса
        }
        telematic.setODM(0); //TODO
        //  DIRH ?
        if (in.loc.alt.get() != 0) {
            final EGTS_SR_POS_DATA_ALT ALT = new EGTS_SR_POS_DATA_ALT();
            ALT.setALT(Math.abs(in.loc.alt.get()));
            telematic.setALTE(Boolean.TRUE);
            telematic.setALTS(in.loc.alt.get() < 0);
            telematic.setALT(ALT);
        } else {
            telematic.setALTE(Boolean.FALSE);
        }
        return new EGTSTelematic(telematic, oid, tm);
    }

    private List<AbstractEGTSSubrecordType> createSrAdSensData(
            Map<Integer, Map<Double, CorePacket>> map,
            EGTS_SERVICE.OID oid,
            EGTS_SERVICE.TM tm) {
        List<AbstractEGTSSubrecordType> result = new ArrayList<>();
        int currPkgNumber = 1;
        for (Map.Entry<Integer, Map<Double, CorePacket>> entry : map.entrySet()) {
            int pkgNumber = entry.getKey();
            // create info pkg
            EGTS_SR_AD_SENSORS_DATA lls = new EGTS_SR_AD_SENSORS_DATA();
            for (Map.Entry<Double, CorePacket> subentry : entry.getValue().entrySet()) {
                double subtract = subentry.getKey() - ( pkgNumber * 1.0 - 1 );
                int bitNumber = (int) (subtract * 8.0) - 1;
                short asfe = lls.ASFE.get();
                lls.ASFE.set(asfe |= (1 << bitNumber));
                lls.setASN(bitNumber, getKVValue(subentry.getValue()));
            }
            // create empty pkgs if need
            for (int e = currPkgNumber; e < pkgNumber; e++) {
                EGTS_SR_AD_SENSORS_DATA r = new EGTS_SR_AD_SENSORS_DATA();
                result.add(new EGTSAdSensors(r, oid, tm));
            }
            // write info pkg
            result.add(new EGTSAdSensors(lls, oid, tm));
            currPkgNumber = pkgNumber + 1;
        }
        return result;
    }

    private EGTS_SR_ABS_AN_SENS_DATA createSrAbsAnSensData(CorePacket kv) {
        EGTS_SR_ABS_AN_SENS_DATA lls = new EGTS_SR_ABS_AN_SENS_DATA();
        lls.ASN.set((short) getKVKey(kv));
        lls.setASV(getKVValue(kv));
        return lls;
    }

    private int getKVKey(CorePacket kv) {
        if (kv instanceof KVFloat) {
            return ((KVFloat) kv).key.get();
        }
        if (kv instanceof KVSLong) {
            return ((KVSLong) kv).key.get();
        }
        if (kv instanceof KVULong) {
            return ((KVULong) kv).key.get();
        }
        return KV_NO_KEY;
    }

    private long getKVValue(CorePacket kv) {
        if (kv instanceof KVFloat) {
            return (long) ((KVFloat) kv).value.get();
        }
        if (kv instanceof KVSLong) {
            return ((KVSLong) kv).value.get();
        }
        if (kv instanceof KVULong) {
            return ((KVULong) kv).value.get();
        }
        return KV_NO_VALUE;
    }
}
