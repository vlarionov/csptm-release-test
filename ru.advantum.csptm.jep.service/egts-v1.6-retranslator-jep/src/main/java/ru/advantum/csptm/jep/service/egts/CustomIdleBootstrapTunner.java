package ru.advantum.csptm.jep.service.egts;

import org.jboss.netty.bootstrap.Bootstrap;
import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.bootstrap.ConnectionlessBootstrap;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.*;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.DefaultChannelGroup;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.jboss.netty.channel.socket.oio.OioClientSocketChannelFactory;
import org.jboss.netty.channel.socket.oio.OioServerSocketChannelFactory;
import org.jboss.netty.handler.timeout.IdleStateHandler;
import org.jboss.netty.util.HashedWheelTimer;
import org.jboss.netty.util.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.amts.common.LogicHandler;
import ru.advantum.amts.hub.HubConfig;

import java.lang.reflect.Constructor;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.concurrent.*;

@SuppressWarnings("unused")
public abstract class CustomIdleBootstrapTunner {
    private final ChannelGroup channelGroup;
    private String logicHandlerID = null;
    public static final Logger log = LoggerFactory.getLogger(CustomIdleBootstrapTunner.class);
    private final Timer TIMER = new HashedWheelTimer();
    protected Bootstrap bootstrap;
    private final EGTSNsConfig.EGTSDest config;
    private final Class<? extends Bootstrap> prototype;
    private SocketAddress host;

    public CustomIdleBootstrapTunner(HubConfig config, Class<? extends Bootstrap> prototype) {
        this.config = (EGTSNsConfig.EGTSDest) config;
        this.prototype = prototype;
        this.channelGroup = this.initChannelGroup();
    }

    private ChannelGroup initChannelGroup() {
        return this.channelGroupFactory();
    }

    protected ChannelGroup channelGroupFactory() {
        return new DefaultChannelGroup("unit-channel-group");
    }

    protected abstract void tunePipeline(ChannelPipeline var1);

    public void initBootstrap() throws Exception {
        if (this.bootstrap == null) {
            Constructor bsConstructor = this.prototype.getConstructor(ChannelFactory.class);
            this.bootstrap = (Bootstrap) bsConstructor.newInstance(this.createChannelFactory(this.prototype));
            this.tuneBootstrap();
            this.bootstrap.setPipelineFactory(() -> {
                ChannelPipeline pipeline = Channels.pipeline();
                pipeline.addFirst("writeTimeoutHandler", new IdleStateHandler(
                        CustomIdleBootstrapTunner.this.TIMER,
                        CustomIdleBootstrapTunner.this.config.handler.idleTimeout,
                        CustomIdleBootstrapTunner.this.config.handler.idleTimeout,
                        CustomIdleBootstrapTunner.this.config.handler.idleTimeout,
                        TimeUnit.MILLISECONDS));
                CustomIdleBootstrapTunner.this.tunePipeline(pipeline);
                CustomIdleBootstrapTunner.this.linkLookup(pipeline);
                return pipeline;
            });
        }
    }

    protected void linkLookup(ChannelPipeline pipeline) {
        this.setLogicHandlerID(null);
        log.info("Pipeline name for LogicHandler wasn\'t set.");
    }

    public String getLogicHandlerID() {
        return this.logicHandlerID;
    }

    protected void setLogicHandlerID(String logicHandlerID) {
        this.logicHandlerID = logicHandlerID;
        log.info(String.format("Handler extenders stored with \'%s\' name in pipeline. It\'ll be use in future.", this.getLogicHandlerID()));
    }

    protected void tuneBootstrap() {
        this.bootstrap.setOption("reuseAddress", Boolean.TRUE);
        this.bootstrap.setOption("tcpNoDelay", this.config.handler.tcpNoDelay);
        this.bootstrap.setOption("keepAlive", this.config.handler.keepAlive);
        this.bootstrap.setOption("child.tcpNoDelay", this.config.handler.tcpNoDelay);
        this.bootstrap.setOption("child.keepAlive", this.config.handler.keepAlive);
        this.bootstrap.setOption("child.reuseAddress", Boolean.TRUE);
        this.bootstrap.setOption("receiveBufferSize", this.config.handler.receiveBufferSize);
        this.bootstrap.setOption("sendBufferSize", this.config.handler.sendBufferSize);
        this.bootstrap.setOption("writeBufferHighWaterMark", this.config.handler.writeBufferHighWaterMark);
        if (!(this.bootstrap instanceof ServerBootstrap) && !(this.bootstrap instanceof ConnectionlessBootstrap)) {
            this.bootstrap.setOption("connectTimeoutMillis", this.config.handler.connectTimeoutMillis);
        } else {
            this.bootstrap.setOption("localAddress", new InetSocketAddress(this.config.host, this.config.port));
        }

    }

    protected ChannelFactory createChannelFactory(Class<? extends Bootstrap> prototype) {
        int workerCount = Runtime.getRuntime().availableProcessors();
        if (prototype.isAssignableFrom(ClientBootstrap.class)) {
            if (!this.config.nio) {
                return new OioClientSocketChannelFactory(new ScheduledThreadPoolExecutor(1));
            } else {
                return new NioClientSocketChannelFactory(Executors.newCachedThreadPool(), Executors.newCachedThreadPool());
            }
        } else {
            if (!this.config.nio) {
                return new OioServerSocketChannelFactory(new ScheduledThreadPoolExecutor(1), new ScheduledThreadPoolExecutor(1));
            } else {
                ExecutorService bossExecutor = Executors.newSingleThreadExecutor();
                ExecutorService workerExecutor = Executors.newCachedThreadPool();
                return new NioServerSocketChannelFactory(bossExecutor, workerExecutor, workerCount);
            }
        }
    }

    public Channel bind() throws Throwable {
        this.initBootstrap();
        Channel channel;
        if (this.bootstrap instanceof ServerBootstrap) {
            channel = ((ServerBootstrap) this.bootstrap).bind();
            log.info("Server started at " + channel.getLocalAddress());
            return channel;
        } else if (this.bootstrap instanceof ConnectionlessBootstrap) {
            channel = ((ConnectionlessBootstrap) this.bootstrap).bind(new InetSocketAddress(this.config.host, this.config.port));
            log.info("UDP Server started at " + channel.getLocalAddress());
            return channel;
        } else {
            throw new Throwable("Can bind only ServerBootstrap");
        }
    }

    public ChannelFuture connect() throws Throwable {
        this.initBootstrap();
        ChannelFuture future = null;
        if (this.bootstrap instanceof ClientBootstrap) {
            if (this.host == null) {
                this.host = new InetSocketAddress(this.config.host, this.config.port);
            }

            ClientBootstrap client = (ClientBootstrap) this.bootstrap;
            future = client.connect(this.host);
            future.awaitUninterruptibly();

            //assert future.isDone();
        } else {
            throw new Throwable("Can latch only ClientBootstrap");
        }
        return future;
    }

    public ChannelFuture restoreConnect(ChannelFuture present) {
        ClientBootstrap client = (ClientBootstrap) this.bootstrap;
        if (!present.getChannel().isOpen()) {
            log.info("Restore connection...");
            ChannelFuture future = client.connect(this.host);
            future.awaitUninterruptibly();

            //assert future.isDone();

            return future;
        } else {
            log.info("Connection already opened.");
            return present;
        }
    }

    public void latchConnect(long reconnectMills) throws Throwable {
        CountDownLatch latch = new CountDownLatch(1);
        ChannelFuture future = null;
        try {
            future = this.connect();
        } catch (Throwable ignored) {
        }

        while (!latch.await(reconnectMills, TimeUnit.MILLISECONDS)) {
            try {
                if (future == null || !future.getChannel().isOpen()) {
                    future = this.restoreConnect(future);
                }
            } catch (Throwable e1) {
                log.warn("", e1);
            }
        }
    }

    public ChannelGroup getChannelGroup() {
        return this.channelGroup;
    }

    public final LogicHandler locateChannelHandlerId(long id) {

        for (Channel channel : this.getChannelGroup()) {
            ChannelHandler channelHandler = channel.getPipeline().get(this.getLogicHandlerID());
            if (channelHandler instanceof LogicHandler) {
                if (((LogicHandler) channelHandler).getChannelOwnerID() == id) {
                    return (LogicHandler) channelHandler;
                }
            } else {
                log.error("Wrong class type handler in ChannelGroup!!!");
            }
        }

        return null;
    }

}
