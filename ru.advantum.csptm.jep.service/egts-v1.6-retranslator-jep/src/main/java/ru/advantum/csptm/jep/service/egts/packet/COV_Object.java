package ru.advantum.csptm.jep.service.egts.packet;

import ru.advantum.protoV3.data.TelematicBase;
import ru.advantum.protoV3.data.TrInfo;
import ru.advantum.protocore3.CorePacket;

import java.util.ArrayList;
import java.util.List;

public class COV_Object {

    public static class AMTSRecord {

        public final TrInfo info;
        public  TelematicBase loc = null;
        public  List<CorePacket> sensors = new ArrayList<>();

        public AMTSRecord(TrInfo info, TelematicBase loc) {
            this.info = info;
            this.loc = loc;
        }

        public AMTSRecord(TrInfo info, List<CorePacket> sensors) {
            this.info = info;
            this.sensors = new ArrayList<>(sensors);
        }

        public AMTSRecord(TrInfo info, TelematicBase loc, List<CorePacket> sensors) {
            this.info = info;
            this.loc = loc;
            this.sensors = new ArrayList<>(sensors);
        }

        public CorePacket[] toCorePackets() {
            if (sensors.size() > 0) {
                List<CorePacket> corePackets = new ArrayList<>(sensors);
                if ( loc != null ) {
                    corePackets.add(0, loc);
                }
                corePackets.add(0, info);
                return corePackets.toArray(new CorePacket[corePackets.size()]);
            } else {
                return new CorePacket[]{info, loc};
            }
        }

    }
}
