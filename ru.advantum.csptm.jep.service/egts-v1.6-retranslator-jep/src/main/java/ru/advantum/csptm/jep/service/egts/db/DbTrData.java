package ru.advantum.csptm.jep.service.egts.db;

@SuppressWarnings("unused")
public class DbTrData {

    private final long idTr;
    private final String optString;
    private final String id4Send;

    public DbTrData(long idTr, String optString, String id4Send) {
        this.idTr = idTr;
        this.optString = optString;
        this.id4Send = id4Send;
    }

    public long getIdTr() {
        return idTr;
    }

    public String getOptString() {
        return optString;
    }

    public String getId4Send() {
        return id4Send;
    }

}
