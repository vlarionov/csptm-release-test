package ru.advantum.csptm.jep.service.egts;

import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.buffer.HeapChannelBufferFactory;
import org.jboss.netty.channel.ChannelPipeline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.jep.hub.egts.handler.EraDecoder;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.SensorNumerator;
import ru.advantum.csptm.jep.netty.SimpleStateDecoder;
import ru.advantum.csptm.jep.service.egts.handler.LogicClient;
import ru.advantum.csptm.jep.service.egts.handler.MathParserFacade;
import ru.advantum.csptm.jep.service.egts.packet.EGTSFactory;

import java.nio.ByteOrder;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class EGTSNavServer extends CustomIdleBootstrapTunner {

    private static final Logger log = LoggerFactory.getLogger(EGTSNavServer.class);
    private static final ExecutorService EX = Executors.newFixedThreadPool(2);
    private SimpleStateDecoder decoder;
    private LogicClient logic;
    private static EGTSNsConfig config;
    public static MathParserFacade mathParser;

    private EGTSNavServer() throws Exception {
        super(config.dest, ClientBootstrap.class);

        EGTSFactory factory = new EGTSFactory();
        this.decoder = new EraDecoder(factory);
        this.logic = new LogicClient(factory, config);

    }

    public static void main(String... arg) throws Throwable {
        if (System.getProperty("advantum.config.dir") == null) {
            System.setProperty("advantum.config.dir", "src/main/resources/extras/examples");
        }
        config = Configuration.unpackConfig(EGTSNsConfig.class, EGTSNsConfig.CONFIG_NAME);
        if (config != null) {
            if (config.sensorNumeratorType != null
                    && config.sensorNumeratorType != SensorNumerator.NumeratorType.DEFAULT) {
                SensorNumerator.setNumeratorType(config.sensorNumeratorType);
            }

            //it looks for <ns-tr-id-modifier .../> element in the config, if it's not there then initialize default MathParser
            //and relay id as it is
            if (config.trIdModifier != null) {
                mathParser = new MathParserFacade(config.trIdModifier.getFormula(), config.trIdModifier.getPrecision(),
                        config.trIdModifier.getLeft(), config.trIdModifier.getRight());
            } else {
                mathParser = new MathParserFacade(null, null, null, null);
            }

            if (!mathParser.isAvailable()) {
                log.warn("TR_ID modifier is not initialized. It will relay as it is.");
            } else {
                log.info("TR_ID modifier initialized. Formula - " + config.trIdModifier.getFormula());
            }


            final EGTSNavServer client = new EGTSNavServer();

            EX.submit(() -> {
                try {
                    log.info("RECEIVER CONNECT LATCH");
                    client.latchConnect(config.dest.reconnect);
                } catch (Throwable t) {
                    log.error("{}", t);
                }

            });

        }
    }

    @Override
    protected void tunePipeline(ChannelPipeline pipeline) {
        pipeline.addLast("decoder", this.decoder);
        pipeline.addLast("logic", this.logic);
    }

    @Override
    protected void tuneBootstrap() {
        super.tuneBootstrap();
        bootstrap.setOption("bufferFactory", new HeapChannelBufferFactory(ByteOrder.LITTLE_ENDIAN));
        bootstrap.setOption("child.bufferFactory", new HeapChannelBufferFactory(ByteOrder.LITTLE_ENDIAN));
    }

    @Override
    protected void linkLookup(ChannelPipeline pipeline) {
    }

}
