package ru.advantum.csptm.jep.service.egts;

import org.simpleframework.xml.Attribute;

public class EGTSNsIdModifierConfig {
    @Attribute(name = "formula")
    private String formula;

    @Attribute(name = "left", required = false)
    private String left;

    @Attribute(name = "right", required = false)
    private String right;

    @Attribute(name = "precision")
    private Integer precision;

    public String getFormula() {
        return formula;
    }

    public String getLeft() {
        return left;
    }

    public String getRight() {
        return right;
    }

    public Integer getPrecision() {
        return precision;
    }
}
