package ru.advantum.csptm.jep.service.egts.db;

import java.util.Map;

public interface DbTrReaderMapper {

    Map<String, Object> getTrsOpts(Map<String, Object> params);

}
