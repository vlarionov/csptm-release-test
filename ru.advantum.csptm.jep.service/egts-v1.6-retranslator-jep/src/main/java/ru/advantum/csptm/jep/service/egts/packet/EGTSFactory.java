package ru.advantum.csptm.jep.service.egts.packet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.hub.egts.AbstractFactory;
import ru.advantum.csptm.jep.hub.egts.handler.EraResponseCodes;
import ru.advantum.csptm.jep.hub.egts.packet.*;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_AUTH_INFO;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_DISPATCHER_IDENTITY;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_RECORD_RESPONSE;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_TERM_IDENTITY;
import ru.advantum.csptm.jep.hub.egts.packet.transp.EGTS_Transport;
import ru.advantum.csptm.jep.hub.egts.packet.transp.EGTS_Transport_HCS;
import ru.advantum.csptm.jep.hub.egts.packet.transp.EGTS_Transport_RTE;
import ru.advantum.csptm.jep.hub.egts.packet.transp.EGTS_Transport_SFRCS;
import ru.advantum.csptm.jep.hub.egts.packet.types.AbstractEGTSSubrecordType;
import ru.advantum.csptm.jep.service.egts.EGTSNsConfig;
import ru.advantum.hub.util.IConvertor;
import ru.advantum.protoV3.data.TelematicBase;
import ru.advantum.protoV3.data.TrInfo;
import ru.advantum.protocore3.CorePacket;
import ru.advantum.tools.HEX;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class EGTSFactory extends AbstractFactory {
    protected static final Logger LOGGER = LoggerFactory.getLogger(EGTSFactory.class);

    private CycleSQ transportSQ = new CycleSQ(1);
    private CycleSQ frameSQ = new CycleSQ(1);
    private LinkedList<Integer> rnList = new LinkedList<>();
    private final IConvertor<COV_Object.AMTSRecord, List<AbstractEGTSSubrecordType>> cov_amts2egts$ = new COV_AMTS2EGTS();

    private byte[] makeTransportLayer(PT pt, EGTS_Transport_RTE.RTE RTE, EGTSBase... frames) throws IOException {
        final EGTS_Transport header = new EGTS_Transport();
        final EGTS_Transport_RTE header_RTE = (RTE == null ? null : new EGTS_Transport_RTE());
        final EGTS_Transport_HCS header_HCS = new EGTS_Transport_HCS();
        //..<BINARY>..
        final EGTS_Transport_SFRCS header_SFRCS = new EGTS_Transport_SFRCS();

        final byte $serviceSupport[];

        { // ==> Заголовок Протокола Транспортного уровня
            header.setPRV(PRV_VERSION);
            header.setSKID(SKID);
            header.setPRF(0x00);

            header.setENA(0x00);
            header.setCMP(CMD_UNCOMPRESSED);
            header.setPR(0x00);
            header.setHE(0x00);
            header.setPID(transportSQ.getNext());
            header.setPT((short) pt.getId());

            if ((header_RTE != null)) {
                header_RTE.PRA.set(RTE.PRA);
                header_RTE.RCA.set(RTE.RCA);
                header_RTE.setTTL(RTE.TTL);

                header.setRTE(0x01);
                header.setHL(header.size() + header_RTE.size() + header_HCS.size());
            } else {
                header.setRTE(0x00);
                header.setHL(header.size() + header_HCS.size());
            }

            { // ==> Данные Уровня Поддержки услуг
                $serviceSupport = EGTSBase.toByteArray(frames);

                // ==> Контрольная Сумма Данных Уровня Поддержки Услуг
                if (frames.length > 0) {
                    header.setFDL($serviceSupport.length);
                    header_SFRCS.SFRCS.set(HEX.crc16citt($serviceSupport, EGTSBase.SFRCS_POLYNOMIAL));
                } else {
                    header.setFDL(0);
                    header_SFRCS.SFRCS.set(0);
                }

            }

            header_HCS.HCS.set(HEX.crc8(EGTSBase.toByteArray(header, header_RTE)));
        }

        try (ByteArrayOutputStream total = new ByteArrayOutputStream()) {
            total.write(EGTSBase.toByteArray(header, header_RTE, header_HCS));
            total.write($serviceSupport);
            header_SFRCS.write(total);

            total.flush();
            return total.toByteArray();
        }
    }

    private EGTSBase[] serviceRecordHeaderMaker(int rst,
                                                int sst,
                                                EGTS_SERVICE.OID oid,
                                                EGTS_SERVICE.EVID evid,
                                                EGTS_SERVICE.TM tm,
                                                boolean auth) {
        this.rnList.clear();
        EGTSBase[] result = new EGTSBase[2];
        result[0] = new EGTS_SERVICE();
        result[1] = new EGTS_SERVICE_ST();
        if (auth) {
            frameSQ.set(1);
        }
        ((EGTS_SERVICE) result[0]).RN.set(frameSQ.getNext());
        this.rnList.add(frameSQ.getLast());

        LOGGER.debug("Hdr maker: id_tr: {}, sent pkgnum: {} ",
                oid != null ? oid.value.get() : "NULL", frameSQ.getLast());

        ((EGTS_SERVICE) result[0]).SSOD.set(0x00);
        ((EGTS_SERVICE) result[0]).RSOD.set(0x00);
        ((EGTS_SERVICE) result[0]).GPR.set(0x00);
        ((EGTS_SERVICE) result[0]).setRPP(0x00);
        // НАДО допилить...
        ((EGTS_SERVICE) result[0]).TMFE.set(tm != null ? 1 : 0);
        ((EGTS_SERVICE) result[0]).EVFE.set(evid != null ? 1 : 0);
        ((EGTS_SERVICE) result[0]).OBFE.set(oid != null ? 1 : 0);

        ((EGTS_SERVICE_ST) result[1]).RST.set(rst);
        ((EGTS_SERVICE_ST) result[1]).SST.set(sst);
        return result;
    }

    private EGTSBase[] makeServiceLayer(int rst, int sst, List<AbstractEGTSSubrecordType> egts) {
        List<EGTSBase> res = new ArrayList<>();
        EGTS_SERVICE.OID oid = null;
        EGTS_SERVICE.TM tm = null;
        if (egts.size() > 0) {
            oid = egts.get(0).oid;
            tm = egts.get(0).tm;
        }
        EGTSBase[] serv = serviceRecordHeaderMaker(rst, sst, oid, null, tm, false);
        final EGTS_SERVICE service = (EGTS_SERVICE) serv[0];
        final EGTS_SERVICE_ST ST = (EGTS_SERVICE_ST) serv[1];
        res.add(service);
        if (oid != null) {
            res.add(oid);
        }
        if (tm != null) {
            res.add(tm);
        }
        res.add(ST);
        int totalSize = 0;
        for (AbstractEGTSSubrecordType some : egts) {
            for (EGTSServiceSubRecord rd : new EGTSServiceSubRecord[]{some.getRecord()}) {
                if (rd != null) {
                    int size = 0;
                    final EGTSBase[] frames = rd.toFrames();
                    for (EGTSBase f : frames) {
                        size += f.size();
                    }
                    res.add(new EGTS_SERVICE_SUBRECORD(some.type, size));
                    res.addAll(Arrays.asList(frames));

                    totalSize += size + EGTS_SERVICE_SUBRECORD.SIZE;
                }
            }
        }
        service.RL.set(totalSize);
        return res.toArray(new EGTSBase[res.size()]);
    }


    private EGTSBase[] makeServiceLayer(int rst,
                                        int sst,
                                        EGTS_SERVICE_SUBRECORD.Type type,
                                        EGTS_SERVICE.OID oid,
                                        EGTS_SERVICE.EVID evid,
                                        EGTS_SERVICE.TM tm,
                                        EGTSServiceSubRecord[] RD) {
        EGTSBase[] serv = serviceRecordHeaderMaker(rst, sst, oid, evid, tm, type == EGTS_SERVICE_SUBRECORD.Type.EGTS_SR_TERM_IDENTITY);
        final EGTS_SERVICE service = (EGTS_SERVICE) serv[0];
        final EGTS_SERVICE_ST ST = (EGTS_SERVICE_ST) serv[1];
        List<EGTSBase> res = new ArrayList<>();
        res.add(service);
        if (oid != null) {
            res.add(oid);
        }
        if (evid != null) {
            res.add(evid);
        }
        if (tm != null) {
            res.add(tm);
        }
        res.add(ST);
        int totalSize = 0;
        for (EGTSServiceSubRecord rd : RD) {
            int size = 0;
            final EGTSBase[] frames = rd.toFrames();
            for (EGTSBase f : frames) {
                size += f.size();
            }
            res.add(new EGTS_SERVICE_SUBRECORD(type, size));
            res.addAll(Arrays.asList(frames));

            totalSize += size + EGTS_SERVICE_SUBRECORD.SIZE;
        }
        service.RL.set(totalSize); //!
        return res.toArray(new EGTSBase[res.size()]);

    }

    private EGTSBase[] makeResponseSubRecords(int rst,
                                              int sst,
                                              int PID,
                                              EraResponseCodes code,
                                              EGTS_SR_RECORD_RESPONSE... response) {
        final EGTS_PT_RESPONSE pt = new EGTS_PT_RESPONSE();
        pt.RPID.set(PID);
        pt.PR.set(code.getValue());
        List<EGTSBase> frame = new ArrayList<>();
        frame.add(pt);

        frame.addAll(Arrays.asList(makeServiceLayer(rst,
                sst,
                EGTS_SERVICE_SUBRECORD.Type.EGTS_SR_RECORD_RESPONSE,
                null,
                null,
                null,
// Palchik0ff 20-nov-2015 11:28
//                new EGTS_SERVICE.TM(
//                        System.currentTimeMillis() / 1000L - DATE_01_01_2010),
                response)));

        return frame.toArray(new EGTSBase[frame.size()]);
    }

    public byte[] response(int rst, int sst, int RPID, EraResponseCodes code, EGTS_SR_RECORD_RESPONSE... response)
            throws IOException {
        EGTSBase[] resp = makeResponseSubRecords(rst, sst, RPID, code, response);
        return makeTransportLayer(PT.EGTS_PT_RESPONSE, null, resp);
    }

    public static float fromEGTSLon(long lon, boolean LOHS) {
        return lon * 180f / LONG_CONST * (LOHS ? -1f : 1f);
    }

    public static float fromEGTSLat(long lat, boolean LAHS) {
        return lat * 90f / LONG_CONST * (LAHS ? -1f : 1f);
    }

    public static long toEGTSLon(float lon) {
        return Math.abs((long) (((lon / 180f)) * LONG_CONST));
    }

    public static long toEGTSLat(float lat) {
        return Math.abs((long) ((lat / 90f) * LONG_CONST));
    }

    public CorePacket.BINARY createAuthorization(long dispatcherIdentify) throws IOException {
        final EGTS_SR_DISPATCHER_IDENTITY identity = new EGTS_SR_DISPATCHER_IDENTITY();
        identity.DID.set(dispatcherIdentify);
        identity.DT.set((short) 0);
        final EGTSBase[] serviceLayer = makeServiceLayer(EGTS_SERVICE_ST.EGTS_AUTH_SERVICE,
                EGTS_SERVICE_ST.EGTS_AUTH_SERVICE,
                EGTS_SERVICE_SUBRECORD.Type.EGTS_SR_DISPATCHER_IDENTITY,
                null,
                null,
                null,
                new EGTSServiceSubRecord[]{identity});
        return new CorePacket.BINARY(makeTransportLayer(PT.EGTS_PT_APPDATA, this.RTE, serviceLayer));
    }

    public CorePacket.BINARY createAuthorization(EGTSNsConfig.EGTSDest configDest) throws IOException, NoSuchAlgorithmException {
        EGTSBase[] serviceLayer = new EGTSBase[0];
        EGTSNsConfig.EGTSDest.EGTSDestOptions.EGTSAuthService authService = configDest.destOptions.egtsAuthService;
        if (
                authService != null
                        && authService.authInfo != null
                        && authService.authInfo.userName != null
                        && authService.authInfo.password != null
                ) {
            EGTS_SERVICE.TM tm = new EGTS_SERVICE.TM(((int) (System.currentTimeMillis() / 1000)) - DATE_01_01_2010);
            final EGTS_SR_AUTH_INFO authInfo = new EGTS_SR_AUTH_INFO();
            if (authService.rtnAuthFormat) {
                transportSQ = new CycleSQ(1);
                frameSQ = new CycleSQ(1);
                authInfo.setUNM(authService.authInfo.userName); //, Charset.forName("Cp1251")
                authInfo.setUPSW(authService.authInfo.password, null, tm);
                LOGGER.debug("RTN EGTS_SR_AUTH_INFO format occured: User name: {}, password: before {} after {} ",
                        authInfo.getUNM(Charset.forName("UTF-8")),
                        authService.authInfo.password,
                        authInfo.getUPSW());

            } else {
                authInfo.setUNM(authService.authInfo.userName);
                authInfo.setUPSW(authService.authInfo.password);
            }
            serviceLayer = makeServiceLayer(EGTS_SERVICE_ST.EGTS_AUTH_SERVICE,
                    EGTS_SERVICE_ST.EGTS_AUTH_SERVICE,
                    EGTS_SERVICE_SUBRECORD.Type.EGTS_SR_AUTH_INFO,
                    null,
                    null,
                    tm,
                    new EGTSServiceSubRecord[]{authInfo});
        }
        return new CorePacket.BINARY(makeTransportLayer(PT.EGTS_PT_APPDATA, this.RTE, serviceLayer));
    }

    public short getLastRn() {
        return this.rnList.getLast().shortValue();
    }

    public CorePacket.BINARY send(
            final TrInfo info,
            final TelematicBase loc,
            final List<CorePacket> list,
            EGTSNsConfig.EGTSDest.EGTSDestOptions dest) throws IOException {
        try {
            final List<AbstractEGTSSubrecordType> egts = cov_amts2egts$.convert(new COV_Object.AMTSRecord(info, loc, list));
            EGTSBase[] auth = new EGTSBase[0];
            EGTSBase[] serviceLayer = makeServiceLayer(EGTS_SERVICE_ST.EGTS_TELEDATA_SERVICE,
                    EGTS_SERVICE_ST.EGTS_TELEDATA_SERVICE,
                    egts);
            EGTSBase[] fullPkg = new EGTSBase[serviceLayer.length];
            int index = 0;
            if (dest != null) {
                if (dest.egtsAuthService != null) {
                    auth = createAuthorizationRequest(info, dest);
                    fullPkg = new EGTSBase[auth.length + serviceLayer.length];
                    for (EGTSBase l : auth) {
                        fullPkg[index++] = l;
                    }
                }
            }
            for (EGTSBase s : serviceLayer) {
                fullPkg[index++] = s;
            }
            LOGGER.debug("Converting full packages for id {} auth {} teledata {}", info.idTr.get(), auth.length, serviceLayer.length);
            return new CorePacket.BINARY(makeTransportLayer(PT.EGTS_PT_APPDATA, this.RTE, fullPkg));
        } catch (IConvertor.ConvertException ce) {
            throw new IOException(ce);
        }

    }


    public EGTSBase[] createAuthorizationRequest(TrInfo info, EGTSNsConfig.EGTSDest.EGTSDestOptions dest) throws IOException {
        if (dest.egtsAuthService.terminalIdentity != null) {
            final EGTS_SR_TERM_IDENTITY identity = new EGTS_SR_TERM_IDENTITY();
            identity.TID.set(info.idTr.get());
            if (dest.egtsAuthService.terminalIdentity.id2imea) {
                identity.IMEIE.set(1);
                identity.setIMEI(String.format("%d", identity.TID.get()));
            }
            return makeServiceLayer(EGTS_SERVICE_ST.EGTS_AUTH_SERVICE,
                    EGTS_SERVICE_ST.EGTS_AUTH_SERVICE,
                    EGTS_SERVICE_SUBRECORD.Type.EGTS_SR_TERM_IDENTITY,
                    new EGTS_SERVICE.OID(info.idTr.get()),
                    null,
                    null,
                    new EGTSServiceSubRecord[]{identity});
        }
        return new EGTSBase[0];
    }

}
