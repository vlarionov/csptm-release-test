package ru.advantum.csptm.jep.service.egts.db;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name = "trreader")
public class DbTrReaderConfig {

    @Attribute(required = false)
    public long updateTrRate = 300000;

}
