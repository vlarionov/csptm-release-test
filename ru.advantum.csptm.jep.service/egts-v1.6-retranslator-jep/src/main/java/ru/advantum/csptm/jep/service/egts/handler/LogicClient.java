package ru.advantum.csptm.jep.service.egts.handler;

import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.handler.timeout.IdleStateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.core.rmq.RmqBufferSender;
import ru.advantum.csptm.jep.hub.egts.handler.EgtsException;
import ru.advantum.csptm.jep.hub.egts.handler.EraDecodeMessage;
import ru.advantum.csptm.jep.hub.egts.handler.EraResponseCodes;
import ru.advantum.csptm.jep.hub.egts.packet.EGTSServiceSubRecord;
import ru.advantum.csptm.jep.hub.egts.packet.EGTS_PT_RESPONSE;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_RECORD_RESPONSE;
import ru.advantum.csptm.jep.hub.egts.packet.subrec.EGTS_SR_RESULT_CODE;
import ru.advantum.csptm.jep.proto.*;
import ru.advantum.csptm.jep.proto.sensors.ComplexSensorHolder;
import ru.advantum.csptm.jep.service.egts.EGTSNavServer;
import ru.advantum.csptm.jep.service.egts.EGTSNsConfig;
import ru.advantum.csptm.jep.service.egts.db.DbTrData;
import ru.advantum.csptm.jep.service.egts.db.DbTrReader;
import ru.advantum.csptm.jep.service.egts.packet.EGTSFactory;
import ru.advantum.csptm.service.receipt.AckReceipt;
import ru.advantum.csptm.service.receipt.AckReceiptBundle;
import ru.advantum.hub.V3.V3IdleHandler;
import ru.advantum.hub.util.IConvertor;
import ru.advantum.protoV3.Event;
import ru.advantum.protoV3.data.KVULong;
import ru.advantum.protoV3.data.TelematicBase;
import ru.advantum.protoV3.data.TrInfo;
import ru.advantum.protocore3.CorePacket;
import ru.advantum.rabbitmq.consumer.RmqJsonTalker;
import ru.advantum.service.egts.cache.CacheManager;
import ru.advantum.service.egts.cache.entity.InfoWrapper;
import ru.advantum.service.egts.cache.entity.sensor.DigitalSensor;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class LogicClient extends V3IdleHandler {

    private static final Logger log = LoggerFactory.getLogger(LogicClient.class);

    private final EGTSFactory factory;
    private final EGTSNsConfig config;
    private final NSDataFilter nsDataFilter;
    private final DbTrReader dbTrReader;

    private boolean authenticationRequired;
    private volatile ChannelHandlerContext ctx = null;
    private RmqJsonTalker talker = null;
    private RmqBufferSender<AckReceipt, AckReceiptBundle> receiptsSender;

    public LogicClient(EGTSFactory factory, EGTSNsConfig config) throws Exception {
        super();
        this.factory = factory;
        this.config = config;

        dbTrReader = new DbTrReader(
                config.trFilter,
                this.config.database.getConnectionPool(),
                config.idNS,
                DbTrReader.ServiceType.ERA
        );
        dbTrReader.init();
        this.nsDataFilter = new NSDataFilter(dbTrReader);
        this.talker = RmqJsonTalker.newBuilder()
                .setConsumerConfig(true, true, config.serviceConfig.consumer)
                .addConsumer(CommonMessageType.COORDS.name(),
                        UnitPacketsBundle.class,
                        (consumerTag, envelope, basicProperties, message) -> consumeCoords(message))
                .connect(config.serviceConfig.connection.newConnection());
        this.receiptsSender = new RmqBufferSender<>(
                this.talker,
                up -> AckReceiptBundle.of(config.idNS, up),
                this.config.serviceConfig.destination.exchange,
                this.config.serviceConfig.destination.routingKey,
                CommonMessageType.COORDS.name());

    }

    private boolean isRepeatsAllowed() {
        return config.dest.attempts != -1;
    }

    private void consumeCoords(UnitPacketsBundle unitPacketsBundle) {

        while (true) {
            if (this.ctx != null) {
                try {
                    retranslation(
                            nsDataFilter.convert(unitPacketsBundle.getPackets()),
                            false
                    );

                    if (isRepeatsAllowed()) {
                        retranslation(
                                getUntransmittedAsPackets(
                                        CacheManager.getUntransmitted(config.dest.attemptDelay)
                                ),
                                true
                        );
                    }
                    break;
                } catch (IConvertor.ConvertException ignored) {
                } catch (RuntimeException e) {
                    log.error(e.getMessage());
                    ctx.getChannel().close();
                }
            } else {
                log.warn("Channel is not connected...");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ignored) {
                }
            }
        }
    }

    private List<UnitPacket> getUntransmittedAsPackets(Set<Long> untansm) {
        List<UnitPacket> result = new ArrayList<>();

        for (Long sentId : untansm) {
            InfoWrapper iw = CacheManager.getSentInfoWrapper(sentId);
            TelematicBase tb = iw.getTelematicBase();
            TrInfo ti = iw.getTrInfo();
            UnitPacketBuilder unitPacketBuilder = UnitPacket.newBuilder()
                    .setUnitId(iw.getUnitId())
                    .setEventTime(Instant.ofEpochSecond(ti.utcTime.get()))
                    .setTelematic(TelematicPacket
                            .newBuilder()
                            .setCoordinates(tb.lon.get(), tb.lat.get())
                            .setSpeed(tb.speed.get())
                            .setAlt(tb.alt.get())
                            .setGpsTime(Instant.ofEpochSecond(tb.gpsTime.get()))
                            .setHeading(tb.heading.get())
                            .setLocationValid(tb.isLocationValid())
                            .build());
            if (iw.isAlarm()) {
                unitPacketBuilder.addFlag(CommonPacketFlag.ALARM);
            }
            for (DigitalSensor ds : iw.getDigitals()) {
                unitPacketBuilder.addSensor(ds.getNumber(), new BigDecimal(ds.getValue()));
            }
            result.add(unitPacketBuilder.build());
        }
        return result;
    }

    private long extractId4Send(UnitPacket packet) {
        List<DbTrData> trOpts = dbTrReader.getTrOpts(packet.getUnitId());
        if (trOpts.isEmpty()) {
            log.warn("No opts for {}", packet.getUnitId());
            return packet.getUnitId();
        } else {
            return Long.valueOf(trOpts.get(0).getId4Send());
        }
    }

    private void retranslation(List<UnitPacket> packets, boolean repeat) {
        if (packets.size() > 0) {
            log.debug("RCV pkgs {}", packets.size());

            for (UnitPacket unitPacket : packets) {
                TrInfo info = new TrInfo();
                info.idTr.set(EGTSNavServer.mathParser.evaluateToLong(extractId4Send(unitPacket)));
                info.utcTime.set(unitPacket.getEventTime().getEpochSecond());

                boolean alarm = false;
                if (unitPacket.getAttributes() != null && unitPacket.getAttributes().containsKey(CommonPacketFlag.ALARM.name())) {
                    info.idEvent.set(Event.RED_BUTTON.getKey());
                    alarm = true;
                }
                info.idDeviceEvent.set(unitPacket.getDeviceEventId());
                log.debug("\tTrInfo: {}", info.stack().replace("\n", ""));

                TelematicBase telematicBase = new TelematicBase();
                TelematicPacket tp = unitPacket.getTelematic();
                if (tp != null) {

                    telematicBase.gpsTime.set(tp.getGpsTime().getEpochSecond());
                    telematicBase.lat.set(tp.getLat());
                    telematicBase.lon.set(tp.getLon());
                    if (tp.getAlt() != null) {
                        telematicBase.alt.set(tp.getAlt());
                    }
                    if (tp.getHeading() != null) {
                        telematicBase.heading.set(tp.getHeading());
                    }
                    if (tp.getSpeed() != null) {
                        telematicBase.speed.set(tp.getSpeed());
                    }
                    telematicBase.setLocationValid(tp.isLocationValid());
                    log.debug("\tTelematicBase: {}", telematicBase.stack().replace("\n", ""));

                    List<CorePacket> kvArray = new ArrayList<>();
                    if (unitPacket.getSensors() != null && unitPacket.getSensors().size() > 0) {
                        for (Map.Entry<Integer, BigDecimal> e : unitPacket.getSensors().entrySet()) {

                            KVULong kvRecord = new KVULong(e.getKey().shortValue(), e.getValue().longValue());
                            kvArray.add(kvRecord);
                            log.debug("\tKV: {}", kvRecord.stack().replace("\n", ""));
                        }
                    }

                    if (unitPacket.getComplexSensors() != null && unitPacket.getComplexSensors().size() > 0) {
                        for(Map.Entry<Integer, ComplexSensorHolder> e: unitPacket.getComplexSensors().entrySet()) {
                            //todo will done if need
                            int a = 0;
                        }
                    }

                    try {
                        if (repeat) {
                            CacheManager.removeUntransmitted(unitPacket.getPacketId());
                        }
                        sendDirect(this.ctx, factory.send(info, telematicBase, kvArray, config.dest.destOptions));
                        short rn = factory.getLastRn();
                        log.info("Sent tr: {}, utcTime: {}, telematic valid {}, sensors: {} repeat {}",
                                info.idTr.get(),
                                info.utcTime.get(),
                                telematicBase.isLocationValid(),
                                kvArray.size(),
                                repeat
                        );
                        if (isRepeatsAllowed()) {

                            Integer attCounter = CacheManager.put(
                                    new InfoWrapper(config.idNS, unitPacket.getPacketId(), info, telematicBase, kvArray)
                                            .setUnitId(unitPacket.getUnitId())
                                            .setAlarm(alarm),
                                    rn
                            );
                            if (repeat && attCounter > config.dest.attempts) {
                                CacheManager.removeUntransmitted(unitPacket.getPacketId());
                                throw new RuntimeException("Max sent attempts (" + attCounter + ") for sent id "
                                        + unitPacket.getPacketId() + " achieved. Reconnect.");
                            }
                        }
                    } catch (Exception e) {
                        log.error("{}", e.getMessage());
                    }
                } else {
                    log.warn("TelematicPacket of {} is null. Malformed?", unitPacket.getTrId());
                }
            }
        }
    }

    @Override
    public long getChannelOwnerID() {
        return 1L;
    }

    @Override
    public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        log.info("OPEN: [{}] {}", ctx.getChannel(), ctx.getChannel().getRemoteAddress());
        super.channelConnected(ctx, e);

        this.ctx = ctx;

        if (this.config.dest.dispatcherId != 0) {

            // this's simple auth as platform to platform
            log.info("Send DISPATCHER-ID: {}", this.config.dest.dispatcherId);
            sendDirect(this.ctx, factory.createAuthorization(this.config.dest.dispatcherId));
        } else if (
                this.config.dest.destOptions != null
                        && this.config.dest.destOptions.egtsAuthService != null
                        && this.config.dest.destOptions.egtsAuthService.rtnAuthFormat
                ) {

            // this's auth as vehicle (in RTN version) to platform
            log.info("Send RTN-format authorization info: {}", this.config.dest.dispatcherId);
            setAuthenticationRequired(Boolean.TRUE);
            sendDirect(this.ctx, factory.createAuthorization(this.config.dest));
        }
    }

    @Override
    public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        this.ctx = null;
        log.info("CLOSE: [{}]", ctx.getChannel());
        super.channelClosed(ctx, e);
    }

    @Override
    public void channelIdle(ChannelHandlerContext ctx, IdleStateEvent e) throws Exception {
/*
        if (e.getState() == IdleState.ALL_IDLE) {
            log.info(String.format("IDLE(ALL) %s. Close channel.", ctx.getChannel()));
            ctx.getChannel().close();
        }
*/
        super.channelIdle(ctx, e);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
        e.getCause().printStackTrace();
        if (e.getCause() instanceof IOException) {
            log.info("{}",
                    String.format("Device [%s] IOException when connected. Please check error stack.", ctx.getChannel()));
            log.info("{}", e.getCause());
        } else {
            if (e.getCause() instanceof EgtsException) {
                log.warn("EGTS Exception caught {}", e.getCause());
            } else {
                log.error("Exception caught {}", e.getCause());
            }
            ctx.getChannel().close().awaitUninterruptibly();
        }
    }

    private void localResponseHandler(EraDecodeMessage.RESPONSE resp) throws Exception {
        if (resp.getData().length > 0) {

            for (EGTSFactory.ServiceData sd : resp.getData()) {

                EraResponseCodes respCode = EraResponseCodes.UNKNOWN;
                StringBuilder warnMessage = new StringBuilder();
                boolean warn = false;
                int RPID;

                for (EGTSServiceSubRecord sub : sd.RD) {
                    if (sub instanceof EGTS_PT_RESPONSE) {

                        final EGTS_PT_RESPONSE ptResp = (EGTS_PT_RESPONSE) sub;
                        warn = (respCode = EraResponseCodes.resolve(ptResp.PR.get())) != EraResponseCodes.PC_OK;
                        RPID = ptResp.RPID.get();
                        warnMessage.append(String.format("Subrecord: %s, [RPID: %d, PR: %s]",
                                sub.getClass().getSimpleName(),
                                RPID,
                                respCode));

                    } else if (sub instanceof EGTS_SR_RECORD_RESPONSE) {

                        final EGTS_SR_RECORD_RESPONSE srRecordResp = (EGTS_SR_RECORD_RESPONSE) sub;
                        respCode = EraResponseCodes.resolve(srRecordResp.RST.get());
                        warnMessage.append(String.format(" ->> %s [CRN: %d, RST: %s]",
                                sub.getClass().getSimpleName(),
                                srRecordResp.CRN.get(),
                                respCode));

                        if (respCode != EraResponseCodes.PC_OK) {
                            warn = true;
                        }

                        if (respCode == EraResponseCodes.PC_OK || respCode == EraResponseCodes.PC_AUTH_DENIED) {

                            if (isRepeatsAllowed()) {
                                Long packetId = CacheManager.remove(srRecordResp.CRN.get());
                                if (packetId != null) {
                                    this.receiptsSender.basicPublish(new AckReceipt(packetId, (short) config.idNS));
                                }
                            }
                        }
                    } else if (sub instanceof EGTS_SR_RESULT_CODE) {

                        final EGTS_SR_RESULT_CODE srResultCode = (EGTS_SR_RESULT_CODE) sub;
                        respCode = EraResponseCodes.resolve(srResultCode.RCD.get());
                        warnMessage.append(String.format(" ->> %s [RCD: %s] ", sub.getClass().getSimpleName(), respCode));
                        if (respCode != EraResponseCodes.PC_OK) {
                            warn = true;
                        }
                    } else {
                        warnMessage.append(String.format(" ->> UNDEFINED: %s", sub.toString()));
                    }
                }
                if (warn) {
                    log.info("ATTENTION: {}", warnMessage.toString());
                    if (isAuthenticationRequired()) { //&& respCode == EraResponseCodes.PC_AUTH_DENIED ) {

                        String msg = "Authorization required, response code";
                        log.error("{} is NOT EGTS_PC_OK ({}). Have to exit.", msg, respCode);
                        throw new RuntimeException(msg + " = " + respCode);
                    }
                } else {
                    if (isAuthenticationRequired()) {

                        log.info("Authorization marked as SUCCESSFUL");
                        setAuthenticationRequired(Boolean.FALSE);
                    }

                    log.debug("All's OK: {}", warnMessage.toString());
                }
            }
        }
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
        if (e.getMessage() instanceof EraDecodeMessage.RESPONSE) {
            localResponseHandler((EraDecodeMessage.RESPONSE) e.getMessage());
        }
    }

    private boolean isAuthenticationRequired() {
        return this.authenticationRequired;
    }

    private void setAuthenticationRequired(Boolean authenticationRequired) {
        this.authenticationRequired = authenticationRequired;
    }
}
