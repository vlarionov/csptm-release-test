package ru.advantum.csptm.jep.service.egts.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.service.egts.mathparser.BracerParser;

import java.math.BigDecimal;
import java.text.ParseException;

@SuppressWarnings("unused")
public class MathParserFacade {

    protected static final Logger LOGGER = LoggerFactory.getLogger(MathParserFacade.class);

    private BracerParser mathParser;
    private String left;
    private String right;
    private Integer precision;

    /**
     * Initialize MathParser with the formula and the precision, left and right string are empty.
     * @param formula contains string representation of formula, like "var*100" - "var" is used to indicate variable
     *                you can use only one variable in your formula
     * @param precision used only in evaluateToString method, it means number of digits after dot
     */
    public MathParserFacade(String formula, Integer precision) {
        this(formula, precision, null, null);
    }

    /**
     * Initialize MathParser, if formula or precision is null, Parser relay incoming number as it is
     * @param formula see the first constructor
     * @param precision see the first constructor
     * @param left left string to concat in evaluateToString method
     * @param right right string to concat in evaluateToString method
     * @see MathParserFacade
     */
    public MathParserFacade(String formula, Integer precision, String left, String right) {
        this.left = left == null ? "" : left;
        this.right = right == null ? "" : right;

        try {
            if (formula == null || precision == null) {
                mathParser = null;
            } else {
                this.precision = precision;
                mathParser = new BracerParser(precision);
                mathParser.parse(formula);

                //test evaluate to make sure everything work, if it doesn't then return TR_ID as it is
                mathParser.evaluate(1);
            }
        } catch (ParseException e) {
            mathParser = null;
        }
    }

    /**
     *
     * @return true if parser was initialized right and works fine, otherwise false
     */
    public boolean isAvailable() {
        return mathParser != null;
    }

    /**
     * Modify number according to formula and return.
     * If MathParser was initialized wrong or there is any exception, then relay number as it is.
     * Precision does not matter in this method.
     * @param number any long number, used like variable in formula
     * @return modified number
     */
    public Long evaluateToLong(Long number) {
        if (!isAvailable()) {
            return number;
        } else {
            try {
                if (mathParser.getPrecision() > 0) {
                    mathParser.setPrecision(0);
                }

                return Long.valueOf(mathParser.evaluate(number));
            } catch (Exception  e) {
                LOGGER.warn(e.getMessage());
                return number;
            }
        }
    }

    /**
     * Modify number according to formula and return its string representation.
     * Concatenate left and right parameters to result.
     * This method is using the precision parameter that sets number of digits after dot.
     * If MathParser was initialized wrong or there is any exception, then relay number as it is
     * @param number any long number, used like variable in formula
     * @return string representation of modified number with precision and left and right parts
     */
    public String evaluateToString(Long number) {
        if (!isAvailable()) {
            return left + new BigDecimal(number).setScale(precision == null ? 0 : precision).toString() + right;
        } else {
            try {
                return left + mathParser.evaluate(number) + right;
            } catch(NullPointerException npe) {
                return null;
            } catch (Exception  e) {
                return left + new BigDecimal(number).setScale(precision == null ? 0 : precision).toString() + right;
            }
        }
    }
}
