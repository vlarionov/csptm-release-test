package ru.advantum.csptm.jep.service.egts.handler;

import ru.advantum.csptm.jep.proto.UnitPacket;
import ru.advantum.csptm.jep.service.egts.db.DbTrReader;
import ru.advantum.hub.util.IConvertor;

import java.util.ArrayList;
import java.util.List;

public class NSDataFilter implements IConvertor<List<UnitPacket>, List<UnitPacket>> {
    private final DbTrReader validator;

    NSDataFilter(DbTrReader validator) {
        this.validator = validator;
    }


    @Override
    public List<UnitPacket> convert(List<UnitPacket> in) throws ConvertException {
        if (validator == null) {
            return null;
        }

        List<UnitPacket> list = new ArrayList<>();

        for (UnitPacket p : in) {
            if (validator.containsTr(p.getUnitId())) {
                list.add(p);
            }
        }


        return list;
    }


}
