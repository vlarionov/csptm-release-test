package ru.advantum.csptm.jep.service.egts;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.amts.hub.HubConfig;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;
import ru.advantum.csptm.jep.hub.egts.packet.sensor.SensorNumerator;
import ru.advantum.csptm.jep.service.egts.db.DbTrReaderConfig;
import ru.advantum.hub.V3.amtsconnect.AMTSHubMinimalConfig;
import ru.advantum.ns.client.NSConnectConf;

@SuppressWarnings("unused")
@Root(name = "egts-hybrid")
public class EGTSNsConfig {
    static String CONFIG_NAME = "egts-v1.6-retranslator.xml";

    public static class EGTSDest extends HubConfig {
        @Attribute(name = "service-tcp-nio", required = false)
        public boolean nio = false;

        @Attribute(name = "attempts", required = false)
        public int attempts = 3; // -1 is disabling here

        @Attribute(name = "attempt-delay-secs", required = false)
        public int attemptDelay = 60;

        public static class EGTSDestOptions {

            public static class EGTSAuthService {
                @Attribute(required = false)
                public boolean rtnAuthFormat = false;
                @Attribute(required = false)
                public boolean authEachVehicle = false;

                public static class AuthInfo {
                    @Attribute()
                    public String userName = null;
                    @Attribute()
                    public String password = null;
                }

                public static class TerminalIdentity {
                    @Attribute(required = false)
                    public boolean id2imea = false;
                }

                @Element(required = false, name = "auth-info")
                public AuthInfo authInfo = null;
                @Element(required = false, name = "terminal-identity")
                public TerminalIdentity terminalIdentity = null;
            }

            @Element(required = false, name = "egts-auth-service")
            public EGTSAuthService egtsAuthService = null;
        }

        @Element(required = false, name = "egts-destination-options")
        public EGTSDestOptions destOptions = null;

        @Attribute(required = false)
        public long reconnect = 10000L;

        @Attribute(required = false)
        public long dispatcherId = 0;
    }

    public static class NSConnectEx extends NSConnectConf {

        @Attribute(required = false)
        public long reconnect = 10000L;
    }

    @Attribute(name = "idNS")
    public int idNS = 0;

    @Element(name = "egts-destination")
    public EGTSDest dest = new EGTSDest();

    @Element(name = "hub", required = false)
    public AMTSHubMinimalConfig hub = null;

    @Element(name = "ns-tr-filter", required = false)
    public DbTrReaderConfig trFilter = null;

    @Element(name = "ns-tr-id-modifier", required = false)
    public EGTSNsIdModifierConfig trIdModifier = null;

    @Element(name = "sensor-numerator-type", required = false)
    public SensorNumerator.NumeratorType sensorNumeratorType = SensorNumerator.NumeratorType.DEFAULT;

    @Attribute(name = "send-retry-period", required = false)
    public int sendRetryPeriod = 5;

    @Element(name = "rmq-service")
    public JepRmqServiceConfig serviceConfig = new JepRmqServiceConfig();

    @Element(name = "database")
    public DatabaseConnection database = new DatabaseConnection();

}
