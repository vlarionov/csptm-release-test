package ru.advantum.csptm.jep.service.egts.db;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class DbTrReader {

    public enum ServiceType {

        ORIG,
        ERA,
        OLYMP
    }

    private static final Logger log = LoggerFactory.getLogger(DbTrReader.class);
    private final ScheduledExecutorService EX = Executors.newSingleThreadScheduledExecutor();
    private final DbTrReaderConfig config;
    private final DataSource ds;
    private final long idNavserver;
    private final ServiceType serviceType;
    private final AtomicReference<Map<Long, List<DbTrData>>> trs = new AtomicReference<>();

    public DbTrReader(DbTrReaderConfig config, DataSource ds, long idNavserver, ServiceType serviceType) {
        this.config = config;
        this.ds = ds;
        this.idNavserver = idNavserver;
        this.serviceType = serviceType;
    }

    public DbTrReader init() throws SQLException {
        log.info("DbTrReader started. IdNav {} ServiceType {}", idNavserver, serviceType);
        SqlMapperSingleton.setConfiguration(ds);
        updateTrs();

        EX.scheduleAtFixedRate(this::updateTrs, config.updateTrRate, config.updateTrRate, TimeUnit.MILLISECONDS);
        return this;
    }

    protected boolean validateOpts(long idTr, String opts) {
        return true;
    }

    private void updateTrs() {
        try {
            log.debug("Updating tr list. IdNav {} ServiceType {}", idNavserver, serviceType);

            try (SqlSession sqlSession = SqlMapperSingleton.getInstance().openSession()) {
                DbTrReaderMapper mapper = sqlSession.getMapper(DbTrReaderMapper.class);

                Map<String, Object> params = new HashMap<>();
                params.put("idNavserver", idNavserver);
                params.put("serviceType", serviceType.name());

                mapper.getTrsOpts(params);

                List<DbTrData> dbTrDatas = (List<DbTrData>) params.get("rows");
                Map<Long, List<DbTrData>> result = dbTrDatas.stream().collect(Collectors.groupingBy(DbTrData::getIdTr));

                log.info("readed and stored {} trs", result.size());
                log.debug("Finished updating tr list. IdNav {} ServiceType {} Cnt {}",
                        idNavserver, serviceType, result.size());
                trs.set(result);
            }
        } catch (Throwable ex) {
            log.error("updateTrs", ex);
        }
    }

    public List<DbTrData> getTrOpts(long idTr) {
        return trs.get().get(idTr);
    }

    public boolean containsTr(long idTr) {
        return trs.get().containsKey(idTr);
    }

}
