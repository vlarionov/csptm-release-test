package ru.advantum.csptm.monitoring.event;

import com.fatboyindustrial.gsonjavatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Protocol;
import ru.advantum.config.common.db.RedisClientConfig;
import ru.advantum.csptm.incident.model.AlarmWeb;
import ru.advantum.csptm.incident.model.IncidentType;
import ru.advantum.csptm.incident.model.LastAlarm;
import java.time.Instant;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AutonomicMoveStateHolder {
    private final JedisPool jedisPool;

    private final Gson gson = Converters.registerAll(new GsonBuilder()).setDateFormat("yyyy-MM-dd HH:mm:ss.SSS").create();

    private final Map<Long, AutonomicMoveState> autonomicMoveStates = new HashMap<>();

    public AutonomicMoveStateHolder(RedisClientConfig config) {
        this.jedisPool = new JedisPool(
                new GenericObjectPoolConfig(),
                config.host,
                config.port,
                Protocol.DEFAULT_TIMEOUT,
                config.password,
                Protocol.DEFAULT_DATABASE
        );
    }

    public void restore() {
        try (Jedis jedis = jedisPool.getResource()) {
            Map<Long, AutonomicMoveState> restoredEventTrState = jedis.hgetAll(AlarmWeb.TR_ALARM_MAP_NAME)
                    .entrySet()
                    .stream()
                    .flatMap(this::deserialize)
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
            autonomicMoveStates.putAll(restoredEventTrState);
        }
    }

    public AutonomicMoveState get(Long trId) {
        return autonomicMoveStates.get(trId);
    }

    public void put(Long trId, AutonomicMoveState autonomicMoveState) {
        autonomicMoveStates.put(trId, autonomicMoveState);
    }

    private Stream<Map.Entry<Long, AutonomicMoveState>> deserialize(Map.Entry<String, String> serializedEntry) {
        Long trId = Long.valueOf(serializedEntry.getKey());

        Map<IncidentType.TYPE, LastAlarm> lastAlarmMap = gson.fromJson(
                serializedEntry.getValue(),
                new TypeToken<Map<IncidentType.TYPE, LastAlarm>>() {
                }.getType()
        );
        LastAlarm lastAutonomicMoveAlarm = lastAlarmMap.get(IncidentType.TYPE.AUTONOMIC_MOVE);
        if (lastAutonomicMoveAlarm != null && !lastAutonomicMoveAlarm.isConfirm()) {
            return Stream.of(new AbstractMap.SimpleEntry<>(trId, new AutonomicMoveState(true, lastAutonomicMoveAlarm.getLastTime())));
        } else {
            return Stream.empty();
        }
    }



    public static class AutonomicMoveState {
        public final boolean isActive;

        public final Instant stateTime;

        public AutonomicMoveState(boolean isActive, Instant stateTime) {
            this.isActive = isActive;
            this.stateTime = stateTime;
        }
    }
}
