package ru.advantum.csptm.monitoring.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.dbidgen.DbIdGenerator;
import ru.advantum.csptm.incident.model.*;
import ru.advantum.csptm.jep.proto.CommonMessageType;
import ru.advantum.csptm.jep.proto.CommonPacketFlag;
import ru.advantum.csptm.jep.proto.UnitPacket;
import ru.advantum.csptm.jep.proto.UnitPacketsBundle;
import ru.advantum.csptm.jep.proto.sensors.ComplexSensorHolder;
import ru.advantum.csptm.jep.proto.sensors.Type;
import ru.advantum.csptm.service.csd.BinarySensor;
import ru.advantum.csptm.service.csd.ComplexSensorProcessor;
import ru.advantum.csptm.service.sensor.SensorType;
import ru.advantum.csptm.service.sensor.updater.SensorSettingsUpdater;
import ru.advantum.csptm.service.sensor.updater.map.SensorAttrib;
import ru.advantum.csptm.service.sensor.updater.map.TrSensorNum;
import ru.advantum.rabbitmq.consumer.RmqJsonTalker;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

/**
 * Created by kukushkin on 04.04.2017.
 */
public class EventProducer {
    private static final Logger LOGGER = LoggerFactory.getLogger(EventProducer.class);
    private final EventProducerConfig config;
    private RmqJsonTalker rmqTalker;
    private final DbIdGenerator dbIdGenerator;
    private final SensorSettingsUpdater sensorSettingsUpdater = new SensorSettingsUpdater();
    private final AutonomicMoveStateHolder autonomicMoveStateHolder;

    private static final IncidentType.TYPE[] FLAGGED_EVENTS_TYPES = {
            IncidentType.TYPE.VIDEO_LOSS,
            IncidentType.TYPE.HDD_FULL,
            IncidentType.TYPE.NO_DISK,
            IncidentType.TYPE.ALL_VIDEO_LOSS,
            IncidentType.TYPE.FAN_FAIL,
            IncidentType.TYPE.OVER_HDD_TEMPERATURE,
            IncidentType.TYPE.HDD_FAIL
    };

    private EventProducer(EventProducerConfig config) throws SQLException {
        this.config = config;
        dbIdGenerator = new DbIdGenerator(config.dbIdGeneratorConfig, config.database.getConnectionPool());
        autonomicMoveStateHolder = new AutonomicMoveStateHolder(config.redisConfig);
    }

    private void start() throws IOException, TimeoutException, SQLException {
        autonomicMoveStateHolder.restore();

/*        if (config.serviceConfig.destination.routingKey != null && !config.serviceConfig.destination.routingKey.isEmpty()) {
            routingKey = config.serviceConfig.destination.routingKey;
            LOGGER.info("Using static routing key [{}]", routingKey);
        }
*/
        sensorSettingsUpdater.start(config.database);

        rmqTalker = RmqJsonTalker.newBuilder()
                .setConsumerConfig(true, true, config.serviceConfig.consumer)
                .addConsumer(CommonMessageType.COORDS.name(),
                        UnitPacketsBundle.class,
                        (consumerTag, envelope, basicProperties, message) -> consumeCoords(message))
                .connect(config.serviceConfig.connection.newConnection(), config.serviceConfig.connection.newConnection());
    }

    private void consumeCoords(UnitPacketsBundle unitPacketsBundle) {
        List<Incident> incidentList = new ArrayList<>();
        for (UnitPacket unitPacket : unitPacketsBundle.getPackets()) {
            Long unitId = unitPacket.getUnitId();

            if (!Objects.isNull(unitPacket.getFlags())) {
                Set<String> flags = unitPacket.getFlags();
                if (flags.contains(CommonPacketFlag.ALARM.name())) {
                    incidentList.add(getIncident(unitPacket, IncidentType.TYPE.RED_BUTTON));
                }
                for (IncidentType.TYPE eventType : FLAGGED_EVENTS_TYPES) {
                    if (flags.contains(eventType.name())) {
                        sendMessage(getIncident(unitPacket, eventType), Incident.class.getSimpleName(), "SAVE_PROTOCOL");
                    }
                }
            }

            if (unitPacket.getComplexSensors() != null) {
                Map<Integer, ComplexSensorHolder> complexSensors = unitPacket.getComplexSensors();
                complexSensors.forEach((Integer num, ComplexSensorHolder sensor) -> {
                            SensorAttrib sensorAttrib = sensorSettingsUpdater.getTrSensorMapping().get().get(new TrSensorNum(unitId, num));
                            if (sensor.getType() == Type.BINARY && sensorAttrib != null) {
                                try {
                                    BinarySensor binarySensor = (BinarySensor) ComplexSensorProcessor.build(num, sensor);
                                    int sensorTypeId = sensorAttrib.getSensorTypeId();
                                    if (sensorTypeId == SensorType.AUTONOMIC_MOVE.getType()) {
                                        Incident incident = handleAutonomicMove(unitPacket, binarySensor);
                                        if (incident != null) {
                                            incidentList.add(incident);
                                        }
                                    } else if (sensorTypeId == SensorType.REVERSE_MOVE.getType()) {
                                        incidentList.add(getIncident(unitPacket, IncidentType.TYPE.REVERSE_MOVE));
                                    } else if (sensorTypeId == SensorType.SMOKE.getType()) {
                                        incidentList.add(getIncident(unitPacket, IncidentType.TYPE.SMOKE));
                                    } else if (sensorTypeId == SensorType.ALARM_BUTTON.getType() && binarySensor.getValue().longValue() == 0L) {
                                        incidentList.add(getIncident(unitPacket, IncidentType.TYPE.RED_BUTTON));
                                    }
                                } catch (Exception e) {
                                    LOGGER.error("", e);
                                }
                            }
                        }
                );
            }


            if (unitPacket != null && unitPacket.getAttributes() != null && unitPacket.getAttributes().containsKey(CommonPacketFlag.TEXT_MESSAGE.name())) {
                Incident incident = getIncident(unitPacket, IncidentType.TYPE.INCOMMING_MESSAGE);
                incident.getIncidentPayload().setIncommingMessage((String) unitPacket.getAttributes().get(CommonPacketFlag.TEXT_MESSAGE.name()));
                incident.getIncidentPayload().setIncommingMessageId((String) unitPacket.getAttributes().get(CommonPacketFlag.TEXT_MESSAGE_ID.name()));
                incidentList.add(incident);
            }
        }

        if (incidentList.size() > 0)  {
            Map<Short, List<Incident>> collected = incidentList.stream().collect(Collectors.groupingBy(Incident::getIncidentTypeId));
            collected.forEach((k, v) ->
            {
                {
                    sendIncidentBundle(k, IncidentBundle.builder().incidentBundle(v).build());
                }
            });

        }
    }

    private Incident handleAutonomicMove(UnitPacket unitPacket, BinarySensor binarySensor) {
        Long trId = unitPacket.getTrId();

        boolean isInAutonomicMoveCurrent = (!binarySensor.getValue().equals(BigDecimal.ZERO));
        Instant currentEventTime = unitPacket.getEventTime();

        boolean isInAutonomicMovePrevious = false;
        Instant previousEventTime = null;

        AutonomicMoveStateHolder.AutonomicMoveState previousState = autonomicMoveStateHolder.get(trId);
        if (previousState != null) {
            isInAutonomicMovePrevious = previousState.isActive;
            previousEventTime = previousState.stateTime;
        }

        if (previousEventTime != null && !currentEventTime.isAfter(previousEventTime)) {
            return null;
        }

        if (isInAutonomicMoveCurrent != isInAutonomicMovePrevious) {
            autonomicMoveStateHolder.put(trId, new AutonomicMoveStateHolder.AutonomicMoveState(isInAutonomicMoveCurrent, currentEventTime));
            if (isInAutonomicMoveCurrent) {
                return getIncident(unitPacket, IncidentType.TYPE.AUTONOMIC_MOVE);
            }
        }
        return null;
    }

    private Incident getIncident(UnitPacket unitPacket, IncidentType.TYPE type) {
        Point point = null;
        if (unitPacket.getTelematic() != null && unitPacket.getTelematic() != null) {
            point = Point.of(unitPacket.getTelematic().getLon(), unitPacket.getTelematic().getLat());
        }
            return Incident.builder()
                    .incidentId(dbIdGenerator.nextId())
                    .incidentTypeId(type.get())
                    .timeStart(unitPacket.getEventTime())
                    .trId(unitPacket.getTrId())
                    .unitId(unitPacket.getUnitId())
                    .incidentStatusId(Incident.STATUS.NEW.get())
                    .incidentInitiatorId(Incident.INITIATOR.SYSTEM.get())
                    .incidentPayload(IncidentPayload.builder().pointStart(point).build())
                    .build();

    }


    private <T> void sendMessage(T message, String type, String routingKey) {
        while (true) {
            try {
                rmqTalker.basicPublish(config.serviceConfig.destination.exchange, routingKey, type, message);
                break;
            } catch (Throwable ex) {
                LOGGER.error("send message", ex);
                try {
                    TimeUnit.SECONDS.sleep(config.sendRetryPeriod);
                } catch (InterruptedException e) {
                    LOGGER.error("sleep", ex);
                }
            }
        }
    }

    private void sendIncidentBundle(Short incidentTypeId, IncidentBundle message) {
        while (true) {
            try {
                rmqTalker.basicPublish(Incident.INCIDENT_ROUTER_EXCHANGE, incidentTypeId.toString(), IncidentBundle.class.getSimpleName(), message);
                break;
            } catch (Throwable ex) {
                LOGGER.error("send message", ex);
                try {
                    TimeUnit.SECONDS.sleep(config.sendRetryPeriod);
                } catch (InterruptedException e) {
                    LOGGER.error("sleep", ex);
                }
            }
        }
    }

    public static void main(String[] args) throws Exception {
        EventProducerConfig config = Configuration.unpackConfig(EventProducerConfig.class, "monitoring-event-producer.xml");
        new EventProducer(config).start();
    }


}
