package ru.advantum.csptm.jep.service.coordssaver;

import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.artifact.csvsaver.CsvSaver;
import ru.advantum.csptm.artifact.csvsaver.apache.print.ApacheRecordPrinter;
import ru.advantum.csptm.artifact.csvsaver.limiters.Limiter;
import ru.advantum.csptm.jep.proto.*;
import ru.advantum.rabbitmq.consumer.RmqJsonTalker;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.concurrent.TimeoutException;


public class CoordsSaver {
    private static final Logger LOGGER = LoggerFactory.getLogger(CoordsSaver.class);

    private static final String CONFIG_FILE_NAME = "coords-saver.xml";

    private static final String[] CSV_HEADERS = {
            "packetId",
            "trId",
            "unitId",
            "eventTime",
            "deviceEventId",
            "locationValid",
            "gpsTime",
            "lon",
            "lat",
            "alt",
            "speed",
            "heading",
            "receiveTime",
            "is_hist_data"
    };

    private final CoordsSaverConfig config;

    private volatile Limiter limiter;

    private volatile RmqJsonTalker rmqJsonTalker;

    public CoordsSaver(CoordsSaverConfig config) {
        this.config = config;
    }

    public static void main(String[] args) throws Exception {
        try {
            CoordsSaverConfig config = Configuration.unpackConfig(
                    CoordsSaverConfig.class,
                    CONFIG_FILE_NAME
            );
            new CoordsSaver(config).start();
        } catch (Exception e) {
            LOGGER.error("Error on startup", e);
        }
    }

    private void start() throws Exception {
        initWriter();
        initConsumer();
        initShutdownHook();

        LOGGER.info("CoordsSaver started");
    }

    private void initWriter() throws Exception {
        limiter = CsvSaver.createLimiter(
                config.getCsvSaverConfig(),
                CSV_HEADERS,
                UnitPacket.class,
                new RecordPrinter()
        );
    }

    private void initConsumer() throws IOException, TimeoutException {
        RmqJsonTalker.Builder rmqBuilder = RmqJsonTalker.newBuilder();

        rmqBuilder.setConsumerConfig(
                true,
                true,
                config.getRmqConfig().consumer
        );

        rmqBuilder.addConsumer(
                CommonMessageType.COORDS.name(),
                UnitPacketsBundle.class,
                (tag, envelope, basicProperties, message) -> consumeCoords(message)
        );

        rmqJsonTalker = rmqBuilder.connect(
                config.getRmqConfig().connection.newConnection()
        );
    }

    private void initShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                rmqJsonTalker.cancelConsumer();
            } catch (IOException e) {
                LOGGER.error("Error on cancel consumer", e);
            }
            try {
                rmqJsonTalker.close();
                LOGGER.info("RmqJsonTalker stopped");
            } catch (IOException | TimeoutException e) {
                LOGGER.error("Error on close talker", e);
            }

            try {
                limiter.close();
                LOGGER.info("Limiter stopped");
            } catch (Exception e) {
                LOGGER.error("Error on close limiter", e);
            }
        }));
    }

    private void consumeCoords(UnitPacketsBundle message) {
        message.getPackets().forEach(unitPacket -> {
            try {
                limiter.append(unitPacket);
            } catch (Exception e) {
                LOGGER.error("UnitPacket write error", e);
            }
        });
    }

    private static class RecordPrinter implements ApacheRecordPrinter<UnitPacket> {
        @Override
        public void printRecord(UnitPacket unitPacket, CSVPrinter csvPrinter) throws Exception {
            TelematicPacket telematicPacket = unitPacket.getTelematic();

            boolean histData = false;
            if (!Objects.isNull(unitPacket.getFlags()) && unitPacket.getFlags().contains(CommonPacketFlag.HIST_DATA.name())) {
                histData = true;
            }

            if (telematicPacket != null) {
                csvPrinter.printRecord(
                        unitPacket.getPacketId(),
                        unitPacket.getTrId(),
                        unitPacket.getUnitId(),
                        DateTimeFormatter.ISO_INSTANT.format(unitPacket.getEventTime()),
                        unitPacket.getDeviceEventId(),
                        telematicPacket.isLocationValid(),
                        telematicPacket.getGpsTime(),
                        telematicPacket.getLon(),
                        telematicPacket.getLat(),
                        telematicPacket.getAlt(),
                        telematicPacket.getSpeed(),
                        telematicPacket.getHeading(),
                        DateTimeFormatter.ISO_INSTANT.format(unitPacket.getReceiveTime()),
                        histData
                );
            } else {
                csvPrinter.printRecord(
                        unitPacket.getPacketId(),
                        unitPacket.getTrId(),
                        unitPacket.getUnitId(),
                        DateTimeFormatter.ISO_INSTANT.format(unitPacket.getEventTime()),
                        unitPacket.getDeviceEventId(),
                        false,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        DateTimeFormatter.ISO_INSTANT.format(unitPacket.getReceiveTime()),
                        histData
                );
            }

        }
    }
}