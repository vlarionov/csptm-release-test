package ru.advantum.csptm.jep.service.coordssaver;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.csptm.artifact.csvsaver.config.CsvSaverConfig;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

@Root(name = "coords-saver")
public class CoordsSaverConfig {
    @Element(name = "rmq-service")
    private JepRmqServiceConfig rmqConfig;

    @Element(name = "csv-saver")
    private CsvSaverConfig csvSaverConfig;


    public JepRmqServiceConfig getRmqConfig() {
        return rmqConfig;
    }

    public CsvSaverConfig getCsvSaverConfig() {
        return csvSaverConfig;
    }
}