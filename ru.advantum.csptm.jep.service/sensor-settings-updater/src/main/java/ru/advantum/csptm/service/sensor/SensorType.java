package ru.advantum.csptm.service.sensor;

/**
 * Created by kukushkin on 5/16/17.
 */
public enum  SensorType {
    AUTONOMIC_MOVE(54),
    FUEL_DUT(3);

    private final int type;

    SensorType(int type) {
        this.type = type;
    }

    public int getTypre() {
        return type;
    }
}
