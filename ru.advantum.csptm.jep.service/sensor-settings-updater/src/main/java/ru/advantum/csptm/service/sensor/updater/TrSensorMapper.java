package ru.advantum.csptm.service.sensor.updater;

import org.apache.ibatis.annotations.Select;

import java.util.List;

interface TrSensorMapper {

    @Select("select unit_id \"unitId\", sensor_num \"sensorNum\", sensor_id  \"sensorId\", equipment_type_id \"equipmentTypeId\" from snsr.v_sensors")
    List<TrSensorLink> getTrSensor();

    class TrSensorLink {

        final long unitId;
        final long sensorId;
        final long sensorNum;
        final int equipmentTypeId;


        TrSensorLink() {
            this.unitId = 0;
            this.sensorId = 0;
            this.sensorNum = 0;
            this.equipmentTypeId  = 0;
        }
    }
}
