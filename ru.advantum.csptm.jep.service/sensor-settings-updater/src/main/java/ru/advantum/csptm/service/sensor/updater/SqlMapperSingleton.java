package ru.advantum.csptm.service.sensor.updater;

import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.mapping.VendorDatabaseIdProvider;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.SQLException;

public class SqlMapperSingleton {

    protected static final Logger log = LoggerFactory.getLogger(SqlMapperSingleton.class);
    private static volatile SqlSessionFactory instance;
    private static Environment environment;
    private static Configuration configuration;

    private SqlMapperSingleton() {
    }

    public static SqlSessionFactory getInstance() {
        if (null == instance) {
            synchronized (SqlMapperSingleton.class) {
                if (null == instance) {
                    instance = new SqlSessionFactoryBuilder().build(configuration);
                    instance.getConfiguration().addMapper(TrSensorMapper.class);
                }
            }
        }
        return instance;
    }

    public static void setConfiguration(DataSource dataSource) throws SQLException {
        SqlMapperSingleton.environment = new Environment("development", new JdbcTransactionFactory(), dataSource);
        SqlMapperSingleton.configuration = new Configuration(environment);
        final String databaseId = new VendorDatabaseIdProvider().getDatabaseId(dataSource);
        log.info("Database ID {}", databaseId);
        SqlMapperSingleton.configuration.setDatabaseId(databaseId);
    }
}
