package ru.advantum.csptm.service.sensor.updater.map;

/**
 * Created by kukushkin on 5/15/17.
 */
public class SensorAttrib {
    private final int sensorTypeId;
    private final long sensorId;

    public SensorAttrib(int sensorTypeId, long sensorId) {
        this.sensorTypeId = sensorTypeId;
        this.sensorId = sensorId;
    }

    public int getSensorTypeId() {
        return sensorTypeId;
    }

    public long getSensorId() {
        return sensorId;
    }
}
