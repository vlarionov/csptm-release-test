package ru.advantum.csptm.jep.service.glinker.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.advantum.csptm.jep.service.glinker.model.event.GraphSectionPass;
import ru.advantum.csptm.jep.service.glinker.model.event.LinkEvent;
import ru.advantum.csptm.jep.service.glinker.model.event.StopItemPass;

import java.util.List;

@Getter
@RequiredArgsConstructor
public class ComposedLinkedEvent {

    private final LinkEvent linkEvent;
    private final List<StopItemPass> stopItemPasses;
    private final List<GraphSectionPass> graphSectionPasses;
}
