package ru.advantum.csptm.jep.service.glinker.linkers;

import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.service.glinker.config.PersimmonConfig;
import ru.advantum.csptm.jep.service.glinker.db.TTStorageAccessor;
import ru.advantum.csptm.jep.service.glinker.linkers.listener.LinkEventListener;
import ru.advantum.csptm.jep.service.glinker.model.ComposedLinkedEvent;
import ru.advantum.csptm.jep.service.glinker.model.Round;
import ru.advantum.csptm.jep.service.glinker.model.Tr;
import ru.advantum.csptm.jep.service.glinker.model.TtAction;
import ru.advantum.csptm.jep.service.glinker.model.event.*;
import ru.advantum.csptm.jep.service.glinker.model.state.FacadeState;
import ru.advantum.csptm.jep.service.glinker.model.state.TrLinkerState;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class TrTimetableLinker {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrTimetableLinker.class);

    private final PersimmonConfig config;
    private final LinkEventListener listener;
    private final Tr tr;
    private final TtActionManager ttActionManager;
    private TtAction currentTtAction;
    private final Duration minsBeforeRoundStart;
    private final Duration maxTimeBetweenPackets;

    private final TrLinkerState state;
    private LinkerFacade linkerFacade;
    private TTStorageAccessor storageAccessor;
    private NormProvider normProvider;

    TrTimetableLinker(TrLinkerState state,
            PersimmonConfig config,
            LinkEventListener listener,
            TtActionManager ttActionManager,
            StateHolder stateHolder,
            TTStorageAccessor storageAccessor,
            NormProvider normProvider)
    {
        this.config = config;
        this.listener = listener;
        this.tr = state.tr;
        this.ttActionManager = ttActionManager;
        this.minsBeforeRoundStart = Duration.of(config.getMinsBeforeTtActionStart(), ChronoUnit.MINUTES);
        this.maxTimeBetweenPackets = Duration.of(config.getMaxTimeBetweenPackets(), ChronoUnit.MINUTES);
        this.storageAccessor = storageAccessor;
        this.normProvider = normProvider;

        this.state = state;

        currentTtAction = stateHolder.getAction(state.currentTtActionId);
        if (currentTtAction != null) {
            final Optional<Round> round = this.storageAccessor.getRoundCached(currentTtAction.getRoundId());
            round.ifPresent(round1 -> this.linkerFacade = new LinkerFacade(state.facadeState,
                    config,
                    state.currentTtActionId,
                    round1,
                    state.prevActionLastStop,
                    state.prevActionLastGraphSection));
        }
    }

    public void link(SimpleUnitPacket packet) {
        if (!packet.isLocationValid()) {
            return;
        }

        if (state.lastPacket == null) {
            state.lastPacket = packet;
            return;
        }

        if (!state.lastPacket.getEventTime().isBefore(packet.getEventTime())) {
            LOGGER.trace("{} {} bad order of time in packets {} {}",
                    tr,
                    currentTtAction==null?null:currentTtAction.getTtActionId(),
                    packet.getEventTime(),
                    state.lastPacket.getEventTime());
            // неправильный порядок
            return;
        }

        if (currentTtAction == null) {
            if (!initTtAction(packet)) {
                return;
            }
        }

        if (packet.getEventTime().isBefore(currentTtAction.getPlanBegin().minus(minsBeforeRoundStart))) {
            LOGGER.debug("{} {} too early to start linking {} {} {}",
                    tr,
                    currentTtAction.getTtActionId(),
                    currentTtAction.getPlanBegin(),
                    currentTtAction.getPlanEnd(),
                    packet.getEventTime());
            state.lastPacket = packet;
            return;
        }

        // пора бы уже закончить. может что-то случилось?
        if (checkForceFinish(packet)) {
            finishCurrentTtAction(LinkEvent.LinkType.ROUND_END_ERROR);
            return;
        }

        // все хорошо, давайте вязать.

        state.lastPacket = packet;

        final ComposedLinkedEvent link;
        try {
            link = linkerFacade.link(packet);
        } catch (Throwable ex) {
            LOGGER.error(String.format("%d %d LinkerFacade link", tr.getTrId(), currentTtAction.getTtActionId()), ex);
            throw ex;
        }

        LinkEvent linkEvent = link.getLinkEvent();
        List<GraphSectionPass> graphSectionPasses = link.getGraphSectionPasses();
        List<StopItemPass> stopItemPasses = link.getStopItemPasses();

        boolean secondStopWasNotVisited = state.thisActionSecondStop == null;

        state.add(stopItemPasses);

        boolean secondStopIsNowVisited = state.thisActionSecondStop != null;

        if (secondStopWasNotVisited && secondStopIsNowVisited) {
            LOGGER.debug("{} {} After second stop, checking if recalc is needed", tr, currentTtAction.getTtActionId());
            stopItemPasses.addAll(recalcPrevTtActionLastStopAndThisTtActionFirstStop());
        }

        listener.onLink(linkEvent);
        graphSectionPasses.forEach(listener::onSectionVisit);
        stopItemPasses.forEach(listener::onStopItemVisit);
        if (linkEvent.getType().isFinished()) {
            LOGGER.debug("{} {} tt action ended {}",
                    tr,
                    currentTtAction.getTtActionId(),
                    linkEvent);

            //возможно благополучно, нахуя тут ROUND_END_ERROR неизвестно?
            finishCurrentTtAction(LinkEvent.LinkType.ROUND_END_ERROR);
        }
    }

    private List<StopItemPass> recalcPrevTtActionLastStopAndThisTtActionFirstStop() {
        ArrayList<StopItemPass> result = new ArrayList<>();
        boolean prevEnd = containsTechnicalAtPrevEnd();
        boolean thisBegin = containsTechnicalAtThisBegin();
        if(prevEnd || thisBegin) {
            long normThis = getNorm(thisBegin, state.thisActionFirstStop, state.thisActionSecondStop);
            long normPrev = getNorm(prevEnd, state.prevActionPrelastStop, state.prevActionLastStop);

            if(normThis == 0) thisBegin = false;
            if(normPrev == 0) prevEnd = false;

            StopItemPass to = thisBegin?state.thisActionSecondStop:state.thisActionFirstStop;
            StopItemPass from = prevEnd?state.prevActionPrelastStop:state.prevActionLastStop;

            if(from != null && to != null){
                Duration realDur = Duration.between(from.getPassTime(),
                        to.getPassTime());
                long realDurSeconds = realDur.getSeconds();
                if (realDurSeconds <= normPrev + normThis) {
                    double coeff = (double) realDurSeconds / (double) (normPrev + normThis);
                    normPrev = (long) (normPrev * coeff);
                    normThis = (long) (normThis * coeff);
                }

            }

            if (prevEnd) {
                result.add(new StopItemPass(
                        state.prevActionLastStop.getTrId(),
                        state.prevActionLastStop.getTtActionId(),
                        state.prevActionLastStop.getRoundId(),
                        state.prevActionLastStop.getStopItemId(),
                        state.prevActionLastStop.getStopOrderNum(),
                        state.prevActionPrelastStop.getPassTime().plus(normPrev, ChronoUnit.SECONDS)));

            }
            if (thisBegin) {
                result.add(new StopItemPass(
                        state.thisActionFirstStop.getTrId(),
                        state.thisActionFirstStop.getTtActionId(),
                        state.thisActionFirstStop.getRoundId(),
                        state.thisActionFirstStop.getStopItemId(),
                        state.thisActionFirstStop.getStopOrderNum(),
                        state.thisActionSecondStop.getPassTime().minus(normThis, ChronoUnit.SECONDS)));
            }
            LOGGER.info("{} {} Recalced stops at the edges={}", tr, currentTtAction.getTtActionId(), result);
        }
        return result;
    }

    private long getNorm(boolean isTechnicalEdge, StopItemPass from, StopItemPass to){

        if (isTechnicalEdge) {
            LocalTime time = LocalDateTime.ofInstant(from.getPassTime(), ZoneOffset.UTC)
                    .toLocalTime().plusHours(3); //moscow time
            Optional<Integer> normPrevOpt = normProvider.getNorm(tr.getNormId(),
                    from.getStopItemId(),
                    to.getStopItemId(),
                    time,
                    tr.getCapacity(),
                    tr.getTrType()
            );
            if (normPrevOpt.isPresent()) {
                return (long) normPrevOpt.get();
            }
        }
        return 0;
    }
    /*private boolean containsTechnicalAtTheEdges() {
        return (state.prevActionLastStop != null && state.thisActionFirstStop != null && state.prevActionPrelastStop != null)
                && (storageAccessor.isTechnical(state.prevActionLastStop)
                || storageAccessor.isTechnical(state.thisActionFirstStop));
    }*/

    private boolean containsTechnicalAtThisBegin() {
        return (state.thisActionFirstStop != null && storageAccessor.isTechnical(state.thisActionFirstStop)
                && state.thisActionSecondStop != null && !storageAccessor.isTechnical(state.thisActionSecondStop));
    }

    private boolean containsTechnicalAtPrevEnd() {
        return (state.prevActionLastStop != null && state.prevActionPrelastStop != null
                && storageAccessor.isTechnical(state.prevActionLastStop)&& !storageAccessor.isTechnical(state.prevActionPrelastStop));
    }

    private boolean initTtAction(SimpleUnitPacket packet) {
        while (takeNextTtAction()) {
            LOGGER.debug("{} {} starting new round {}",
                    tr,
                    currentTtAction.getTtActionId(),
                    currentTtAction);
            double roundDuration =
                    ChronoUnit.SECONDS.between(currentTtAction.getPlanBegin(), currentTtAction.getPlanEnd());
            double intoRoundDuration =
                    ChronoUnit.SECONDS.between(currentTtAction.getPlanBegin(), packet.getEventTime());
            if (roundDuration == 0) {
                LOGGER.debug("{} {} invalid round, zero length {} {} {} {}",
                        tr,
                        currentTtAction.getTtActionId(),
                        currentTtAction.getPlanBegin(),
                        currentTtAction.getPlanEnd(),
                        packet.getEventTime(),
                        config.getTtActionTooLateRatio());

                flushRound();
            } else if (intoRoundDuration / roundDuration > config.getTtActionTooLateRatio()) {
                LOGGER.debug("{} {} too late to start linking {} {} {} {} {}",
                        tr,
                        currentTtAction.getTtActionId(),
                        currentTtAction.getPlanBegin(),
                        currentTtAction.getPlanEnd(),
                        packet.getEventTime(),
                        intoRoundDuration / roundDuration,
                        config.getTtActionTooLateRatio());

                flushRound();
            } else {
                return true;
            }
        }

        return false;
    }

    private boolean takeNextTtAction() {
        currentTtAction = ttActionManager.getNextTtActionForTr(tr.getTrId());
        state.currentTtActionId = currentTtAction == null ? null : currentTtAction.getTtActionId();
        if (currentTtAction != null) {
            final Optional<Round> round = storageAccessor.getRoundCached(currentTtAction.getRoundId());
            if (round.isPresent()) {
                FacadeState newFacadeState = new FacadeState();
                state.facadeState = newFacadeState;
                linkerFacade = new LinkerFacade(newFacadeState,
                        config,
                        currentTtAction.getTtActionId(),
                        round.get(),
                        state.prevActionLastStop,
                        state.prevActionLastGraphSection);
                return true;
            }
        }
        return false;

    }

    /**
     * Закончить текущее действие
     *
     * @param endErrorLinkType тип последнего события в случае, если привязка не закончилась благополучно
     *                         фак мой мозг, если привязка закончилась благополучно то тоже передается ROUND_END_ERROR
     */
    private void finishCurrentTtAction(LinkEvent.LinkType endErrorLinkType) {
        final TtActionCompleteEvent ttActionCompleteEvent = linkerFacade.finishTtActionEvent(endErrorLinkType);

        listener.onTtActionComplete(ttActionCompleteEvent);
        state.changeToNextTtAction();
        if (ttActionCompleteEvent.getEventEnd() != null && ttActionCompleteEvent.getEventEnd().getType().isGoodLink()) {
            //state.prevActionLastStop = ttActionCompleteEvent.getStopItemEnd();
            //state.prevActionLastGraphSection = ttActionCompleteEvent.getGraphSectionEnd();
        } else {
            state.prevActionLastStop = null;
            state.prevActionLastGraphSection = null;
        }

        LOGGER.debug("{} {} finish tt action {} {} {} {} {} {}",
                tr,
                currentTtAction.getTtActionId(),
                ttActionCompleteEvent.getEventBegin(),
                ttActionCompleteEvent.getEventEnd(),
                ttActionCompleteEvent.getStopItemBegin(),
                ttActionCompleteEvent.getStopItemEnd(),
                ttActionCompleteEvent.getGraphSectionBegin(),
                ttActionCompleteEvent.getGraphSectionEnd());

        flushRound();
    }

    private void flushRound() {
        LOGGER.debug("{} {} flushing round", tr, currentTtAction.getTtActionId());
        ttActionManager.onTtActionFinish(currentTtAction);
        currentTtAction = null;
        linkerFacade = null;
    }

    private boolean checkForceFinish(SimpleUnitPacket packet) {
        Duration timeBetweenPackets = Duration.between(state.lastPacket.getEventTime(), packet.getEventTime());
        if (packet.getEventTime().isAfter(currentTtAction.getPlanEnd())) {
            // потеря связи. текущее действие надо завершать.
            if (timeBetweenPackets.compareTo(maxTimeBetweenPackets) >= 0) {
                LOGGER.debug("{} {} lost connect; force finish {} {}",
                        tr,
                        state.currentTtActionId,
                        packet.getEventTime(),
                        state.lastPacket.getEventTime());
                return true;
            }

            // пора заканчивать действие, а мы не начали или проблемы какие-то
            if (!linkerFacade.isStarted() || linkerFacade.isInvalidLinking()) {
                LOGGER.debug("{} {} tt action problems; force finish {}",
                        tr,
                        currentTtAction.getTtActionId(),
                        linkerFacade.getState());
                return true;
            }
        }
        return false;
    }

    /*public TrLinkerState getState() {
        return new TrLinkerState(trId,
                state.lastPacket,
                currentTtAction.getTtActionId(),
                linkerFacade == null ? null : linkerFacade.getState(),
                state.prevActionLastStop,
                state.prevActionLastGraphSection);
    }*/

    /**
     * Обновление атрибутов действия (времени)
     */
    public void updateRound(TtAction ttAction) {
        if (ttAction.equals(currentTtAction)) {
            LOGGER.debug("{} {} updating live round {} {}",
                    tr,
                    currentTtAction.getTtActionId(),
                    currentTtAction,
                    ttAction);
            currentTtAction = ttAction;
        }
    }

    public boolean cancelTtAction(TtAction ttAction) {
        if (ttAction.equals(currentTtAction)) {
            LOGGER.debug("{} {} cancelling live round {} {}",
                    tr,
                    currentTtAction.getTtActionId(),
                    currentTtAction,
                    ttAction);
            finishCurrentTtAction(LinkEvent.LinkType.TT_ACTION_END_CANCEL);
            return true;
        }
        return false;
    }

    public long getTr() {
        return tr.getTrId();
    }
}
