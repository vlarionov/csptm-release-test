package ru.advantum.csptm.jep.service.glinker.model.state;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import ru.advantum.csptm.jep.service.glinker.model.event.LinkEvent;

import java.time.Instant;

@Getter
@ToString
@AllArgsConstructor
public class OffsetItemTimeState {

    /**
     * Остановка, для которой мы еще не посчитали время прохождения.
     */
    public int notYetLinkedIndex;

    /**
     * Последняя привязанная координата.
     */
    public LinkEvent lastLinkEvent;
/*
    *//**
     * Время начала проезда последней секции.
     *//*
    public Instant lastItemBeginTime;*/

    /**
     * Текущая скорость.
     */
    public double speed;

    public OffsetItemTimeState() {
        this(0, null, 0);
    }
}
