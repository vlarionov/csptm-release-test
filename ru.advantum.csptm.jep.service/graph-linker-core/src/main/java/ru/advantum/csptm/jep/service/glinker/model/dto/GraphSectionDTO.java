package ru.advantum.csptm.jep.service.glinker.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(force = true)
public class GraphSectionDTO {

    private final long graphSectionId;
    private final long roundId;
    private final int orderNum;
    private final double length;
    private final double roundOffset;
    private final String wktGeometry;
}
