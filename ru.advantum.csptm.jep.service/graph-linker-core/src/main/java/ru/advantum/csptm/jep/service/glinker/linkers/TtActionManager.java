package ru.advantum.csptm.jep.service.glinker.linkers;

import ru.advantum.csptm.jep.service.glinker.model.TtAction;

public interface TtActionManager {

    TtAction getNextTtActionForTr(long trId);

    void onTtActionFinish(TtAction ttAction);
}
