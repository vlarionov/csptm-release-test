package ru.advantum.csptm.jep.service.glinker.linkers;

import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.jep.service.glinker.db.GraphMapper;
import ru.advantum.csptm.jep.service.glinker.model.dto.NormBetweenStop;

@Primary
@Service
@Slf4j
public class NormProviderImpl implements NormProvider {
    private final GraphMapper mapper;

    private LoadingCache<StopPair, NormsBetweenStops> cache;

    @Autowired
    public NormProviderImpl(GraphMapper graphMapper) {
        this.mapper = graphMapper;

        cache = CacheBuilder.newBuilder()
                .expireAfterWrite(24, TimeUnit.HOURS)
                .build(
                        new CacheLoader<StopPair, NormsBetweenStops>() {
                            public NormsBetweenStops load(StopPair key) {
                                List<NormBetweenStop> ns = mapper.getNorms(key.stop1, key.stop2);
                                if(ns.size() == 0){
                                    log.error("No norms between stops: "+key.stop1+", "+key.stop2);
                                }
                                return new NormsBetweenStops(ns);
                            }
                        });

    }

    @Override
    public Optional<Integer> getNorm(int normId, long stop1, long stop2, LocalTime time, short capacity, long trType) {
        StopPair key = new StopPair(stop1, stop2);
        try {
            NormsBetweenStops norms = cache.get(key);
            return norms.get(capacity, time, normId, trType);
        } catch (ExecutionException e) {
            log.error("Caught exception while retrieving norms. "+e);
        }
        return Optional.empty();
    }

    private static class StopPair{
        private long stop1;
        private long stop2;
        private StopPair(long stop1, long stop2) {
            this.stop1 = stop1;
            this.stop2 = stop2;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            StopPair stopPair = (StopPair) o;

            if (stop1 != stopPair.stop1) {
                return false;
            }
            return stop2 == stopPair.stop2;
        }

        @Override
        public int hashCode() {
            int result = (int) (stop1 ^ (stop1 >>> 32));
            result = 31 * result + (int) (stop2 ^ (stop2 >>> 32));
            return result;
        }
    }


    private static class NormsBetweenStops {
        private static LocalTime zeroTime = LocalTime.parse("00:00:00");
        private static LocalTime topTime = LocalTime.parse("23:59:59.9999999");
        private Map<Key, SortedSet<TimeEntry>> normMap;

        private NormsBetweenStops(List<NormBetweenStop> norms) {
            this.normMap = new HashMap<>();
            for(NormBetweenStop n : norms){
                Key key = new Key(n.getNormId(), n.getCapacity(), n.getTrType());
                SortedSet<TimeEntry> normList = normMap.computeIfAbsent(key, k -> new TreeSet<>());
                normList.add(new TimeEntry(n.getHourTo(), n.getDuration()));
            }
        }

        private Optional<Integer> get(short capacity, LocalTime time, int normId, long trType){
            Key key = new Key(normId, capacity, trType);
            SortedSet<TimeEntry> timeTree = normMap.get(key);
            if(timeTree == null){
                log.warn("No normative for normId="+normId+", capacity="+capacity+", trType="+trType);
                return Optional.empty();
            }
            SortedSet<TimeEntry> subTree = timeTree.tailSet(new TimeEntry(time));
            if(subTree.isEmpty()) return Optional.empty();
            TimeEntry result = subTree.first();
            return Optional.of(result.duration);
        }

        private static class Key{
            private int normId;
            private short capacity;
            private long trType;

            private Key(int normId, short capacity, long trType) {
                this.normId = normId;
                this.capacity = capacity;
                this.trType = trType;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) {
                    return true;
                }
                if (o == null || getClass() != o.getClass()) {
                    return false;
                }

                Key key = (Key) o;

                if (normId != key.normId) {
                    return false;
                }
                if (capacity != key.capacity) {
                    return false;
                }
                return trType == key.trType;
            }

            @Override
            public int hashCode() {
                int result = normId;
                result = 31 * result + (int) capacity;
                result = 31 * result + (int) (trType ^ (trType >>> 32));
                return result;
            }
        }

        private static class TimeEntry implements Comparable<TimeEntry>{
            private LocalTime timeTo;
            private int duration;

            private TimeEntry(LocalTime timeTo) {
                this.timeTo = timeTo;
            }

            private TimeEntry(LocalTime timeTo, int duration) {
                if(timeTo.equals(zeroTime)){
                    this.timeTo = topTime;
                }else {
                    this.timeTo = timeTo;
                }
                this.duration = duration;
            }

            @Override
            public int compareTo(TimeEntry o) {
                return timeTo.compareTo(o.timeTo);
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) {
                    return true;
                }
                if (o == null || getClass() != o.getClass()) {
                    return false;
                }

                TimeEntry timeEntry = (TimeEntry) o;

                if (duration != timeEntry.duration) {
                    return false;
                }
                return timeTo.equals(timeEntry.timeTo);
            }

            @Override
            public int hashCode() {
                int result = timeTo.hashCode();
                result = 31 * result + duration;
                return result;
            }
        }
    }
}
