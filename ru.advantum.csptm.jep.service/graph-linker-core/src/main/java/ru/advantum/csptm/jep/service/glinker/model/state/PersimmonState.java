package ru.advantum.csptm.jep.service.glinker.model.state;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import ru.advantum.csptm.jep.service.glinker.model.event.SectionLink;
import ru.advantum.csptm.jep.service.glinker.model.event.SimpleUnitPacket;

/**
 * Created by kaganov on 11/05/2017.
 * Текущее состояние привязки для сериализации.
 */
@Getter
@ToString
@AllArgsConstructor
public class PersimmonState {

    /**
     * Предыдущая привязка.
     */
    public SectionLink lastLink;

    /**
     * Предыдущий пакет
     */
    public SimpleUnitPacket lastPacket;

    /**
     * Начали привязку, т.е. нашли первый локальный минимум.
     */
    public boolean started;

    /**
     * Факт, что мы "почти" закончили рейс.
     * Так что если ТС остановится или поедет назад, то будем считать, что закончили совсем.
     */
    public boolean readyToStop;

    /**
     * Текущее количество проблемных привязок.
     */
    public int errorsLinkingCnt;

    public PersimmonState() {
        this(null, null, false, false, 0);
    }
}
