package ru.advantum.csptm.jep.service.glinker.linkers;

import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.jep.service.glinker.config.PersimmonConfig;
import ru.advantum.csptm.jep.service.glinker.db.TTStorageAccessor;
import ru.advantum.csptm.jep.service.glinker.linkers.listener.LinkEventListener;
import ru.advantum.csptm.jep.service.glinker.model.Timetable;
import ru.advantum.csptm.jep.service.glinker.model.Tr;
import ru.advantum.csptm.jep.service.glinker.model.TtAction;
import ru.advantum.csptm.jep.service.glinker.model.event.SimpleUnitPacket;
import ru.advantum.csptm.jep.service.glinker.model.state.TrLinkerState;

@Service
public class GraphLinkerController implements TtActionManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(GraphLinkerController.class);

    private PersimmonConfig config;
    private LinkEventListener eventListener;
    private StateHolder stateHolder;
    private Map<Long, TrTimetableLinker> linkers = new HashMap<>();
    private TTStorageAccessor storageAccessor;
    private NormProvider normProvider;

    @Autowired
    public GraphLinkerController(PersimmonConfig config,
            LinkEventListener eventListener, StateHolder stateHolder, TTStorageAccessor storageAccessor,
            NormProvider normProvider) {
        this.config = config;
        this.eventListener = eventListener;
        this.stateHolder = stateHolder;
        this.storageAccessor = storageAccessor;
        this.normProvider = normProvider;
    }

    public void init(){
        linkers = stateHolder.getLinkerStates().entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey,
                        e -> new TrTimetableLinker(e.getValue(),
                                this.config,
                                eventListener,
                                this,
                                stateHolder,
                                storageAccessor,
                                normProvider)));
    }

    @Override
    public synchronized TtAction getNextTtActionForTr(long trId) {
        Collection<TtAction> actions = stateHolder.getTtActionsByTr(trId);
        if (actions != null && !actions.isEmpty()) {
            return actions.stream().min(Comparator.comparing(TtAction::getPlanBegin)).orElse(null);
        } else {
            return null;
        }
    }

    @Override
    public synchronized void onTtActionFinish(TtAction ttAction) {
        stateHolder.removeTtAction(ttAction.getTtActionId());

    }

    public synchronized void cleanOldTtActions() {
        LOGGER.info("Cleaning old tt actions");
        final Duration oldestActionLifeDuration = Duration.ofDays(1);
        final Instant now = Instant.now();
        int totalRemoved = 0;
        for (Long tr : stateHolder.getTrs()) {
            final TrTimetableLinker linker = linkers.get(tr);

            final List<TtAction> oldActions = stateHolder.getTtActionsByTr(tr).stream()
                    .filter(or -> Duration.between(or.getPlanBegin(), now).compareTo(oldestActionLifeDuration) >= 0)
                    .collect(Collectors.toList());

            if (!oldActions.isEmpty()) {
                LOGGER.debug("Old rounds: tr {} size {}", tr, oldActions.size());
                for (TtAction oldAction : oldActions) {
                    cancel(linker, oldAction);

                    stateHolder.removeTtAction(oldAction.getTtActionId());
                }
                totalRemoved += oldActions.size();
            }
        }
        LOGGER.info("Cleaned {} rounds older than {}, number of active linkers={}", totalRemoved, oldestActionLifeDuration, linkers.size());
    }

    private void cancel(TrTimetableLinker linker, TtAction ttAction){
        if(linker == null) return;
        boolean cancelled = linker.cancelTtAction(ttAction);
        if(cancelled){
            linkers.remove(linker.getTr());
            stateHolder.getLinkerStates().remove(linker.getTr());
        }
    }
    public synchronized void updateTimetables(List<Timetable> timetables) {
        for (Timetable timetable : timetables) {
            long trId = timetable.getTrId();
            Tr tr = new Tr(timetable.getTrId(), timetable.getCapacity(), timetable.getNormId(), timetable.getTrType());
            TrTimetableLinker linkerNewTr = linkers.computeIfAbsent(trId,
                    newTr -> {
                        TrLinkerState newState = new TrLinkerState(tr);
                        stateHolder.getLinkerStates().put(newTr, newState);
                        return new TrTimetableLinker(newState,
                                config,
                                eventListener,
                                this,
                                stateHolder,
                                storageAccessor,
                                normProvider);
                    });


            for (TtAction ttAction : timetable.getTtActions()) {
                Long prevTrId = stateHolder.getTrByAction(ttAction.getTtActionId());
                if (prevTrId != null && tr.getTrId() != prevTrId) {
                    cancel(linkers.get(prevTrId), ttAction);
                    stateHolder.removeTtAction(ttAction.getTtActionId());
                }

                if (ttAction.isActive()) {
                    linkerNewTr.updateRound(ttAction);
                    stateHolder.newTtAction(ttAction, trId);
                } else {
                    cancel(linkerNewTr, ttAction);
                }
            }
        }
        LOGGER.info("timetables updated, number of active linkers={}",linkers.size());
    }

    public synchronized void link(final SimpleUnitPacket packet) {
        TrTimetableLinker trTimetableLinker = linkers.get(packet.getTrId());
        if (trTimetableLinker != null) {
            trTimetableLinker.link(packet);
        }else{
            LOGGER.trace("No linker for tr id={}",packet.getTrId());
        }
    }
}
