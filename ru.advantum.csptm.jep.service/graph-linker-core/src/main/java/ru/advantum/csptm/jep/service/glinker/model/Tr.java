package ru.advantum.csptm.jep.service.glinker.model;

import lombok.Data;

@Data
public class Tr {
    private final long trId;
    private final short capacity;
    private final int normId;
    private final long trType;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Tr tr = (Tr) o;

        return trId == tr.trId;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) (trId ^ (trId >>> 32));
        return result;
    }
}
