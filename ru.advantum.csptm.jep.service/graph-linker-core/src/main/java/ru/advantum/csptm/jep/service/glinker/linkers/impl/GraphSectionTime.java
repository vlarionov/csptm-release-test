package ru.advantum.csptm.jep.service.glinker.linkers.impl;

import ru.advantum.csptm.jep.service.glinker.config.PersimmonConfig;
import ru.advantum.csptm.jep.service.glinker.model.GraphSection;
import ru.advantum.csptm.jep.service.glinker.model.Round;
import ru.advantum.csptm.jep.service.glinker.model.event.GraphSectionPass;
import ru.advantum.csptm.jep.service.glinker.model.event.LinkEvent;
import ru.advantum.csptm.jep.service.glinker.model.state.FacadeState;
import ru.advantum.csptm.jep.service.glinker.model.state.OffsetItemTimeState;

import java.util.List;
import java.util.stream.Collectors;

public class GraphSectionTime {

    private final OffsetItemTime<GraphSection> offsetItemTime;
    private final long ttActionId;

    public GraphSectionTime(OffsetItemTimeState state,
                            PersimmonConfig config,
                            long ttActionId,
                            Round round,
                            GraphSectionPass previousTtActionLastSection,
            FacadeState facadeState) {
        this.offsetItemTime = new OffsetItemTime<>(ttActionId,
                                                   config,
                                                   round.getGraphSections(),
                                                   previousTtActionLastSection == null ? null : previousTtActionLastSection.getStartTime(),
                                                   state,
                facadeState);

        this.ttActionId = ttActionId;
    }

    /**
     * @param currentEvent событие связки координаты ТС с маршрутом
     * @return Список секций, которые прошло ТС с прошлого вызова этого метода
     */
    public List<GraphSectionPass> link(final LinkEvent currentEvent) {
        return offsetItemTime.link(currentEvent).stream()
                .map(gsp -> new GraphSectionPass(currentEvent.getSimpleUnitPacket().getTrId(),
                                                 ttActionId,
                                                 gsp.getItem(),
                                                 gsp.getPassBeginTime(),
                                                 gsp.getPassEndTime()))
                .collect(Collectors.toList());
    }

    /*@Override
    public OffsetItemTimeState getState() {
        return offsetItemTime.getState();
    }*/
}
