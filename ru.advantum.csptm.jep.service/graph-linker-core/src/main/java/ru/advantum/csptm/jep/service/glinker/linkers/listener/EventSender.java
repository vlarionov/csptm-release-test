package ru.advantum.csptm.jep.service.glinker.linkers.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.jep.service.glinker.model.event.*;
import ru.advantum.csptm.spring.rmq.TypeAdder;

import java.util.ArrayList;
import java.util.List;

@Service
public class EventSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventSender.class);

    private static final int MAX_SEND_LIST_SIZE = 100;

    private final RabbitTemplate rabbitTemplate;

    private final EventCollector eventCollector;

    @Autowired
    public EventSender(RabbitTemplate rabbitTemplate, EventCollector eventCollector) {
        this.rabbitTemplate = rabbitTemplate;
        this.eventCollector = eventCollector;
    }

    private <T> void sendList(List<T> l, String type) {
        while (!l.isEmpty()) {
            int maxInd = Math.min(MAX_SEND_LIST_SIZE, l.size());
            rabbitTemplate.convertAndSend(type, l.subList(0, maxInd), TypeAdder.of(type));
            l = l.subList(maxInd, l.size());
        }
    }

    public void sendEvents() {
        try {
            List<LinkEvent> linkEventsToSend = new ArrayList<>();
            List<StopItemPass> sipEventsToSend = new ArrayList<>();
            List<TtActionCompleteEvent> tacEventsToSend = new ArrayList<>();
            List<GraphSectionPass> gspEventsToSend = new ArrayList<>();
            eventCollector.getLinkEvents().drainTo(linkEventsToSend);
            eventCollector.getSipEvents().drainTo(sipEventsToSend);
            eventCollector.getTacEvents().drainTo(tacEventsToSend);
            eventCollector.getGspEvents().drainTo(gspEventsToSend);

            if(linkEventsToSend.size()+sipEventsToSend.size()+gspEventsToSend.size()+tacEventsToSend.size()>0) {
                LOGGER.debug("LinkEvent {}, StopItemPass {}, GraphSectionPass {}, TtActionCompleteEvent {}",
                        linkEventsToSend.size(), sipEventsToSend.size(), gspEventsToSend.size(),
                        tacEventsToSend.size());

                sendList(linkEventsToSend, EventType.PACKET_LINK.name());
                sendList(sipEventsToSend, EventType.STOP_ITEM_PASS.name());
                sendList(gspEventsToSend, EventType.GRAPH_SECTION_PASS.name());
                sendList(tacEventsToSend, EventType.TT_ACTION_COMPLETE.name());
            }
        } catch (Throwable ex) {
            LOGGER.error("sendEvents", ex);
        }
    }
}
