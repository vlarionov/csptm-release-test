package ru.advantum.csptm.jep.service.glinker.linkers;

import ru.advantum.csptm.jep.service.glinker.config.PersimmonConfig;
import ru.advantum.csptm.jep.service.glinker.linkers.impl.GraphSectionTime;
import ru.advantum.csptm.jep.service.glinker.linkers.impl.Persimmon;
import ru.advantum.csptm.jep.service.glinker.linkers.impl.StopItemTime;
import ru.advantum.csptm.jep.service.glinker.model.ComposedLinkedEvent;
import ru.advantum.csptm.jep.service.glinker.model.Round;
import ru.advantum.csptm.jep.service.glinker.model.event.*;
import ru.advantum.csptm.jep.service.glinker.model.state.FacadeState;

import java.util.List;

public class LinkerFacade {

    private final long ttActionId;
    private final Round round;
    private final Persimmon persimmon;
    private final StopItemTime stopItemTime;
    private final GraphSectionTime graphSectionTime;

    private final FacadeState state;

    LinkerFacade(FacadeState state,
                        PersimmonConfig config,
                        long ttActionId,
                        Round round,
                        StopItemPass previousTtActionLastStop,
                        GraphSectionPass previousTtActionLastSection) {
        this.ttActionId = ttActionId;
        this.round = round;
        persimmon = new Persimmon(state.getPersimmonState(), config, ttActionId, round);
        stopItemTime = new StopItemTime(state.getStopItemTimeState(), config, ttActionId, round, previousTtActionLastStop,state);
        graphSectionTime = new GraphSectionTime(state.getGraphSectionTimeState(), config, ttActionId, round, previousTtActionLastSection,state);

        this.state = state;

    }

    public ComposedLinkedEvent link(SimpleUnitPacket packet) {
        LinkEvent linkEvent = persimmon.link(packet);
        List<StopItemPass> stopItemPasses = stopItemTime.link(linkEvent);
        List<GraphSectionPass> graphSectionPasses = graphSectionTime.link(linkEvent);

        // сохранить первые и последние события для создания TtActionCompleteEvent
        if (linkEvent.getType().isGoodLink()) {
            state.add(linkEvent);
            state.addStopItemPasses(stopItemPasses);
            state.addGraphSectionPasses(graphSectionPasses);
        }

        return new ComposedLinkedEvent(linkEvent, stopItemPasses, graphSectionPasses);
    }

    public boolean isStarted() {
        return persimmon.isStarted();
    }

    public boolean isInvalidLinking() {
        return persimmon.isInvalidLinking();
    }

    /**
     * Генерация события о закончившемся действии.
     *
     * @param endErrorLinkType тип последнего события в случае, если привязка не закончилась благополучно
     * @return событие о закончившемся действии
     */
    public TtActionCompleteEvent finishTtActionEvent(LinkEvent.LinkType endErrorLinkType) {
        // если последнее событие было не "закончили, все ок", то надо проставить пакет последней нормальной привязки
        LinkEvent lastLinkEvent = state.getLastLinkedEvent();
        if (lastLinkEvent != null && lastLinkEvent.getType() != LinkEvent.LinkType.ROUND_END) {
            lastLinkEvent = new LinkEvent(ttActionId,
                                          round.getRoundId(),
                                          endErrorLinkType,
                                          lastLinkEvent.getSectionLink(),
                                          lastLinkEvent.getSimpleUnitPacket());
        }

        return new TtActionCompleteEvent(ttActionId,
                                         round.getRoundId(),
                                         state.getFirstLinkedEvent(),
                                         lastLinkEvent,
                                         state.getFirstStopItemPass(),
                                         state.getLastStopItemPass(),
                                         state.getFirstGsPass(),
                                         state.getLastGsPass());
    }

    public FacadeState getState() {
        return state;
    }
}
