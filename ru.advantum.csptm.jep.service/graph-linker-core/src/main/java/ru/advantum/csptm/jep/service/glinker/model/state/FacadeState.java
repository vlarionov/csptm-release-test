package ru.advantum.csptm.jep.service.glinker.model.state;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import ru.advantum.csptm.jep.service.glinker.model.event.GraphSectionPass;
import ru.advantum.csptm.jep.service.glinker.model.event.LinkEvent;
import ru.advantum.csptm.jep.service.glinker.model.event.StopItemPass;

@Getter
@ToString
@AllArgsConstructor
public class FacadeState {

    public LinkEvent firstLinkedEvent;
    public LinkEvent lastLinkedEvent;
    public StopItemPass firstStopItemPass;
    public StopItemPass lastStopItemPass;
    public GraphSectionPass firstGsPass;
    public GraphSectionPass lastGsPass;

    public PersimmonState persimmonState;
    public OffsetItemTimeState graphSectionTimeState;
    public OffsetItemTimeState stopItemTimeState;

    public FacadeState() {
        this(null,
             null,
             null,
             null,
             null,
             null,
             new PersimmonState(),
             new OffsetItemTimeState(),
             new OffsetItemTimeState());
    }


    public void add(StopItemPass item) {
        if (firstStopItemPass == null) {
            firstStopItemPass = item;
        }
        lastStopItemPass = item;
    }

    public void add(GraphSectionPass item) {
        if (firstGsPass == null) {
            firstGsPass = item;
        }
        lastGsPass = item;
    }

    public void add(LinkEvent item) {
        if (firstLinkedEvent == null) {
            firstLinkedEvent = item;
        }
        lastLinkedEvent = item;
    }

    public void addGraphSectionPasses(List<GraphSectionPass> items) {
        if (!items.isEmpty()) {
            if (firstGsPass == null) {
                firstGsPass = items.get(0);
            }
            lastGsPass = items.get(items.size() - 1);
        }
    }

    public void addLinkEvents(List<LinkEvent> items) {
        if (!items.isEmpty()) {
            if (firstLinkedEvent == null) {
                firstLinkedEvent = items.get(0);
            }
            lastLinkedEvent = items.get(items.size() - 1);
        }
    }

    public void addStopItemPasses(List<StopItemPass> items) {
        if (!items.isEmpty()) {
            if (firstStopItemPass == null) {
                firstStopItemPass = items.get(0);
            }
            lastStopItemPass = items.get(items.size() - 1);
        }
    }
}
