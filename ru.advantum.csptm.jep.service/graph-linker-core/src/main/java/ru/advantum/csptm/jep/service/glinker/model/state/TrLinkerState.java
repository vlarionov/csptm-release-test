package ru.advantum.csptm.jep.service.glinker.model.state;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.ToString;
import ru.advantum.csptm.jep.service.glinker.model.Tr;
import ru.advantum.csptm.jep.service.glinker.model.event.GraphSectionPass;
import ru.advantum.csptm.jep.service.glinker.model.event.SimpleUnitPacket;
import ru.advantum.csptm.jep.service.glinker.model.event.StopItemPass;

//@Data
@ToString
@AllArgsConstructor
public class TrLinkerState {

    public Tr tr;
    public SimpleUnitPacket lastPacket;
    public Long currentTtActionId;
    public FacadeState facadeState;
    public StopItemPass prevActionLastStop;
    public StopItemPass prevActionPrelastStop;
    public StopItemPass thisActionFirstStop;
    public StopItemPass thisActionSecondStop;
    public StopItemPass thisActionLastStop;
    public StopItemPass thisActionPrelastStop;
    public GraphSectionPass prevActionLastGraphSection;

    public TrLinkerState(Tr tr) {
        this(tr, null, null, new FacadeState(), null, null, null, null, null, null, null);
    }

    public void add(List<StopItemPass> stopItemPasses){
        if(stopItemPasses.isEmpty()){
            return;
        }
        if(stopItemPasses.size()<=4){
            stopItemPasses.forEach(this::add);
        }else{
            add(stopItemPasses.get(0));
            add(stopItemPasses.get(1));
            add(stopItemPasses.get(stopItemPasses.size()-2));
            add(stopItemPasses.get(stopItemPasses.size()-1));
        }
    }
    public void changeToNextTtAction(){
       prevActionLastStop = thisActionLastStop;
       prevActionPrelastStop = thisActionPrelastStop;
       thisActionFirstStop = null;
       thisActionSecondStop = null;
       thisActionPrelastStop = null;
       thisActionLastStop = null;
    }

    public void add(StopItemPass stopItemPass){
        if(thisActionFirstStop == null){
            thisActionFirstStop = stopItemPass;
        }else{
            if(thisActionSecondStop == null){
                thisActionSecondStop = stopItemPass;
            }
        }
        thisActionPrelastStop = thisActionLastStop;
        thisActionLastStop = stopItemPass;
    }
}
