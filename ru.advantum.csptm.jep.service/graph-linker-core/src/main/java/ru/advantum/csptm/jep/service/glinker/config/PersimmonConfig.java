package ru.advantum.csptm.jep.service.glinker.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Primary
@Validated
@Component
@ConfigurationProperties(prefix = "graph-linker.persimmon")
public class PersimmonConfig {

    /**
     * На сколько метров вперёд можно привязывать новую координату относительно предыдущей.
     */
    private int sectionMeterLag=1000;

    /**
     * После какой доли длины рейса, рейс может считаться пройденным.
     */
    private double roundCompleteRatio=0.98;

    /**
     * Максимальная длина, которая может остаться от длины рейса после вычитания roundCompleteRatio.
     */
    private double maxRoundLengthRemainder=200;

    /**
     * Минимальная различимая разница между двумя дробными числами в рамках алгоритма.
     */
    private double epsilon=1e-7;

    /**
     * Максимальное расстояние от точки трека до ближайшей секции, при которой привязка все еще считается валидной.
     */
    private double maxRoundDistance=50;

    /**
     * Максимальная скорость (м/с) от одной точки до другой, при которой это еще не считается выбросом.
     */
    private long maxSpeedMps=30;

    /**
     * Количество ошибок в процессе привязки, которое приводит к завершению привязки рейса.
     */
    private int maxErrorsLinking=10;

    /**
     * Минимальная скорость между точками для расчета посещения секций и остановок.
     */
    private double minSpeed=1;

    /**
     * Минут до начала действия, когда начинается привязка.
     */
    private int minsBeforeTtActionStart=5;

    /**
     * Минут между последовательными пакетами, с которых начинается потеря связи.
     */
    private int maxTimeBetweenPackets=5;

    /**
     * При какой пройденной с первым пакетом доле продолжительности действия мы даже не пытаемся его привязать.
     */
    private double ttActionTooLateRatio=0.6;

    /*
    * Скорость, которая будет использоваться для расчета времени пройденных точек в начале раунда, если
    * посчитанная скорость окажется ниже
    */
    public int minimumSpeedToExtrapolatePassedItemTimesAtTheBeginning = 2;

    public int getSectionMeterLag() {
        return sectionMeterLag;
    }

    public double getRoundCompleteRatio() {
        return roundCompleteRatio;
    }

    public double getMaxRoundLengthRemainder() {
        return maxRoundLengthRemainder;
    }

    public double getEpsilon() {
        return epsilon;
    }

    public double getMaxRoundDistance() {
        return maxRoundDistance;
    }

    public long getMaxSpeedMps() {
        return maxSpeedMps;
    }

    public int getMaxErrorsLinking() {
        return maxErrorsLinking;
    }

    public double getMinSpeed() {
        return minSpeed;
    }

    public int getMinsBeforeTtActionStart() {
        return minsBeforeTtActionStart;
    }

    public int getMaxTimeBetweenPackets() {
        return maxTimeBetweenPackets;
    }

    public double getTtActionTooLateRatio() {
        return ttActionTooLateRatio;
    }

    public void setSectionMeterLag(int sectionMeterLag) {
        this.sectionMeterLag = sectionMeterLag;
    }

    public void setRoundCompleteRatio(double roundCompleteRatio) {
        this.roundCompleteRatio = roundCompleteRatio;
    }

    public void setMaxRoundLengthRemainder(double maxRoundLengthRemainder) {
        this.maxRoundLengthRemainder = maxRoundLengthRemainder;
    }

    public void setEpsilon(double epsilon) {
        this.epsilon = epsilon;
    }

    public void setMaxRoundDistance(double maxRoundDistance) {
        this.maxRoundDistance = maxRoundDistance;
    }

    public void setMaxSpeedMps(long maxSpeedMps) {
        this.maxSpeedMps = maxSpeedMps;
    }

    public void setMaxErrorsLinking(int maxErrorsLinking) {
        this.maxErrorsLinking = maxErrorsLinking;
    }

    public void setMinSpeed(double minSpeed) {
        this.minSpeed = minSpeed;
    }

    public void setMinsBeforeTtActionStart(int minsBeforeTtActionStart) {
        this.minsBeforeTtActionStart = minsBeforeTtActionStart;
    }

    public void setMaxTimeBetweenPackets(int maxTimeBetweenPackets) {
        this.maxTimeBetweenPackets = maxTimeBetweenPackets;
    }

    public void setTtActionTooLateRatio(double ttActionTooLateRatio) {
        this.ttActionTooLateRatio = ttActionTooLateRatio;
    }

    public int getMinimumSpeedToExtrapolatePassedItemTimesAtTheBeginning() {
        return minimumSpeedToExtrapolatePassedItemTimesAtTheBeginning;
    }

    public void setMinimumSpeedToExtrapolatePassedItemTimesAtTheBeginning(
            int minimumSpeedToExtrapolatePassedItemTimesAtTheBeginning)
    {
        this.minimumSpeedToExtrapolatePassedItemTimesAtTheBeginning =
                minimumSpeedToExtrapolatePassedItemTimesAtTheBeginning;
    }

    @Override
    public String toString() {
        return "PersimmonConfig{" +
                "sectionMeterLag=" + sectionMeterLag +
                ", roundCompleteRatio=" + roundCompleteRatio +
                ", maxRoundLengthRemainder=" + maxRoundLengthRemainder +
                ", epsilon=" + epsilon +
                ", maxRoundDistance=" + maxRoundDistance +
                ", maxSpeedMps=" + maxSpeedMps +
                ", maxErrorsLinking=" + maxErrorsLinking +
                ", minSpeed=" + minSpeed +
                ", minsBeforeTtActionStart=" + minsBeforeTtActionStart +
                ", maxTimeBetweenPackets=" + maxTimeBetweenPackets +
                ", ttActionTooLateRatio=" + ttActionTooLateRatio +
                '}';
    }
}
