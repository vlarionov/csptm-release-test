package ru.advantum.csptm.jep.service.glinker.linkers.impl;

import com.vividsolutions.jts.geom.Geometry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.artifact.geotools.GeoTools;
import ru.advantum.csptm.jep.service.glinker.config.PersimmonConfig;
import ru.advantum.csptm.jep.service.glinker.linkers.util.LinkerUtils;
import ru.advantum.csptm.jep.service.glinker.model.GraphSection;
import ru.advantum.csptm.jep.service.glinker.model.Round;
import ru.advantum.csptm.jep.service.glinker.model.event.LinkEvent;
import ru.advantum.csptm.jep.service.glinker.model.event.SectionLink;
import ru.advantum.csptm.jep.service.glinker.model.event.SimpleUnitPacket;
import ru.advantum.csptm.jep.service.glinker.model.state.PersimmonState;

import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;


public class Persimmon {

    private static final Logger LOGGER = LoggerFactory.getLogger(Persimmon.class);

    private final long ttActionId;
    private final Round round;
    private final PersimmonConfig config;

    private final PersimmonState state;

    public Persimmon(PersimmonState state, PersimmonConfig config, long ttActionId, Round round) {
        this.ttActionId = ttActionId;
        this.round = round;
        this.config = config;

        this.state = state;
    }

    private LinkEvent createLinkEvent(LinkEvent.LinkType type,
                                      SectionLink sectionLink,
                                      SimpleUnitPacket simpleUnitPacket) {
        return new LinkEvent(ttActionId, round.getRoundId(), type, sectionLink, simpleUnitPacket);
    }

    private Geometry pointFromPacket(SimpleUnitPacket packet) {
        return GeoTools.INSTANCE.createPoint(packet.getLon(), packet.getLat());
    }

    public LinkEvent link(SimpleUnitPacket packet) {
        final double packetsTimeDiff;
        if (state.lastPacket != null) {
            packetsTimeDiff = ChronoUnit.MILLIS.between(state.lastPacket.getEventTime(), packet.getEventTime()) / 1000.0;
        } else {
            packetsTimeDiff = Double.NaN;
        }

        {
            final Optional<LinkEvent> errorEvent = checkPoint(packet, packetsTimeDiff);
            if (errorEvent.isPresent()) {
                return errorEvent.get();
            }
        }

        final Optional<SectionLink> sectionLinkOpt = linkPoint(packet);
        if (!sectionLinkOpt.isPresent()) {
            return processNoSectionLink(packet);
        }

        SectionLink sectionLink = sectionLinkOpt.get();

        {
            final Optional<LinkEvent> errorEvent = checkSectionLink(packet, sectionLink, packetsTimeDiff);
            if (errorEvent.isPresent()) {
                return errorEvent.get();
            }
        }

        state.lastPacket = packet;

        if (isReadyToStop(sectionLink)) {
            // мы уже почти готовы сказать, что рейс закончен
            // если перестанет увеличиваться пройденное расстояние, то говорим, что рейс пройден
            state.readyToStop = true;
        }

        boolean justStarted = false;
        if (!state.started && state.lastLink != null) {
            if (state.lastLink.getRoundOffset() < sectionLink.getRoundOffset()) {
                // нашли локальный минимум, можно привязывать
                state.started = true;
                justStarted = true;
            }
        }

        final LinkEvent.LinkType resultType;
        if (!state.started && state.readyToStop) {
            // еще толком не начали, а уже пора заканчивать -- ошибка
            resultType = LinkEvent.LinkType.ROUND_END_ERROR;
        } else if (!state.started) {
            // ждем начала
            resultType = LinkEvent.LinkType.WAITING_FOR_LINKING_START;
            state.lastLink = sectionLink;
        } else if (justStarted && state.readyToStop) {
            // еще толком не начали, а уже пора заканчивать -- ошибка
            resultType = LinkEvent.LinkType.ROUND_END_ERROR;
        } else if (sectionLink.getRoundOffset() >= round.getLength()) {
            // по-честному доехали до конца рейса
            resultType = LinkEvent.LinkType.ROUND_END;
        } else if (state.readyToStop && state.lastLink.getRoundOffset() >= sectionLink.getRoundOffset() - config.getEpsilon()) {
            // перестали двигаться вперёд после достижения config.roundCompleteRatio доли от длины маршрута
            resultType = LinkEvent.LinkType.ROUND_END;
            sectionLink = state.lastLink;
        } else if (state.lastLink.getRoundOffset() <= sectionLink.getRoundOffset()) {
            if (justStarted) {
                resultType = LinkEvent.LinkType.ROUND_BEGIN;
            } else {
                resultType = LinkEvent.LinkType.LINKED;
            }
            state.lastLink = sectionLink;
            state.errorsLinkingCnt = 0;
        } else {
            resultType = LinkEvent.LinkType.INVALID_ORDER_OFFSET;
        }

        final LinkEvent resultEvent = createLinkEvent(resultType, sectionLink, packet);

        LOGGER.debug("{} {} {} {} {} {} {} {} {} {} {}",
                     packet.getTrId(),
                     ttActionId,
                     resultEvent.getType(),
                     packet.getEventTime(),
                     packet.getPacketId(),
                     resultEvent.getSectionLink().getGraphSectionId(),
                     resultEvent.getSectionLink().getGraphSectionOrderNum(),
                     resultEvent.getSectionLink().getSectionOffset(),
                     resultEvent.getSectionLink().getRoundOffset(),
                     resultEvent.getSectionLink().getRoundOffset() / round.getLength(),
                state);

        return resultEvent;
    }

    private boolean isReadyToStop(SectionLink sectionLink) {
        return sectionLink.getRoundOffset() / round.getLength() >= config.getRoundCompleteRatio() &&
                round.getLength() - sectionLink.getRoundOffset() <= config.getMaxRoundLengthRemainder();
    }

    /**
     * Проверка на порядок координат, скорость между точками по глобусу и расстояние до рейса.
     */
    private Optional<LinkEvent> checkPoint(SimpleUnitPacket packet, double packetsTimeDiff) {
        if (!packet.isLocationValid()) {
            return Optional.of(createLinkEvent(LinkEvent.LinkType.INVALID_COORDINATE, null, packet));
        }

        final Geometry point = pointFromPacket(packet);
        if (state.lastPacket != null) {
            if (packetsTimeDiff <= 0) {
                // координата из прошлого, игнорируем
                LOGGER.debug("{} {} INVALID_ORDER_TIME {} {} {}",
                             packet.getTrId(),
                             ttActionId,
                             packet.getEventTime(),
                             packet.getPacketId(),
                        state);
                return Optional.of(createLinkEvent(LinkEvent.LinkType.INVALID_ORDER_TIME, null, packet));
            } else {
                // проверка на то, что от одной точки до другой мы добрались не слишком быстро
                Geometry lastPoint = pointFromPacket(state.lastPacket);
                double distance = lastPoint.distance(point);
                double speed = distance / packetsTimeDiff;
                if (speed >= config.getMaxSpeedMps()) {
                    LOGGER.debug("{} {} TOO_HIGH_SPEED {} {} {} {} {} {} {}",
                                 packet.getTrId(),
                                 ttActionId,
                                 packet.getEventTime(),
                                 packet.getPacketId(),
                                 packetsTimeDiff,
                                 distance,
                                 speed,
                                 config.getMaxSpeedMps(),
                            state);

                    return Optional.of(createLinkEvent(LinkEvent.LinkType.TOO_HIGH_SPEED, null, packet));
                }
            }
        }

        double roundDistance = round.getGeometry().distance(point);
        if (roundDistance > config.getMaxRoundDistance() && !state.started) {
            LOGGER.debug("{} {} TOO_FAR_FROM_ROUND {} {} {} {} {}",
                         packet.getTrId(),
                         ttActionId,
                         packet.getEventTime(),
                         packet.getPacketId(),
                         roundDistance,
                         config.getMaxRoundDistance(),
                    state);
            return Optional.of(createLinkEvent(LinkEvent.LinkType.TOO_FAR_FROM_ROUND, null, packet));
        }

        return Optional.empty();
    }

    /**
     * Привязка точки к нужной секции. Привязка идет окном в 5 секций от последней зафиксированной.
     */
    private Optional<SectionLink> linkPoint(SimpleUnitPacket packet) {
        final Geometry point = pointFromPacket(packet);

        int firstSectionInd;
        // пока не начали, вязать можно к любой секции.
        if (state.lastLink == null || !state.started) {
            firstSectionInd = 0;
        } else {
            firstSectionInd = state.lastLink.getGraphSectionOrderNum() - 1;
        }

        int linkIteration = 0;
        SectionDist closestSection;
        while (firstSectionInd < round.getGraphSections().size()) {
            final int lastSectionInd = getSectionsWindowLength(firstSectionInd);
            List<GraphSection> activeSections = round.getGraphSections().subList(firstSectionInd, lastSectionInd);
            closestSection = findClosestSection(point, activeSections);

            if (closestSection.dist >= config.getMaxRoundDistance()) {
                LOGGER.debug("{} {} closest section is too far {} {} {} {} {} {} {}",
                             packet.getTrId(),
                             ttActionId,
                             linkIteration,
                             packet.getEventTime(),
                             packet.getPacketId(),
                             closestSection.section.getOrderNum(),
                             closestSection.dist,
                             config.getMaxRoundDistance(),
                        state);
                firstSectionInd = lastSectionInd;
            } else {
                final double projectionLength = projectOnSection(point, closestSection.section);
                final double roundOffset = closestSection.section.getRoundOffset() + projectionLength;

                LOGGER.debug("{} {} found section {} {} {} {} {} {} {}",
                             packet.getTrId(),
                             ttActionId,
                             linkIteration,
                             packet.getEventTime(),
                             packet.getPacketId(),
                             closestSection.section.getOrderNum(),
                             closestSection.dist,
                             config.getMaxRoundDistance(),
                        state);

                return Optional.of(new SectionLink(closestSection.section,
                                                   projectionLength,
                                                   roundOffset));
            }
            linkIteration += 1;
        }

        return Optional.empty();
    }

    private int getSectionsWindowLength(int from) {
        List<GraphSection> sections = round.getGraphSections();

        double sumLength = sections.get(from).getLength();
        if (state.lastLink != null && from == state.lastLink.getGraphSectionOrderNum() - 1) {
            sumLength -= state.lastLink.getSectionOffset();
        }

        for (from = from + 1; from < sections.size() && sumLength < config.getSectionMeterLag(); from++) {
            sumLength += sections.get(from).getLength();
        }

        return from;
    }

    private SectionDist findClosestSection(Geometry point, List<GraphSection> sections) {
        return sections.stream()
                .map(gs -> new SectionDist(gs, gs.getGeometry().distance(point)))
                .min(Comparator.comparing(gs -> gs.dist))
                .orElseThrow(() -> new IllegalArgumentException("empty sections list"));
    }

    private double projectOnSection(Geometry point, GraphSection section) {
        return section.getIndexedLine().project(point.getCoordinate());
    }

    private LinkEvent processNoSectionLink(SimpleUnitPacket packet) {
        if (state.readyToStop) {
            LinkEvent resultEvent = createLinkEvent(LinkEvent.LinkType.ROUND_END, state.lastLink, packet);
            LOGGER.debug("{} {} too far from all sections, but ready to stop {} {} {}",
                         packet.getTrId(),
                         ttActionId,
                         packet.getEventTime(),
                         packet.getPacketId(),
                    state);
            return resultEvent;
        } else {
            state.errorsLinkingCnt++;
            LOGGER.debug("{} {} TOO_FAR_FROM_GRAPH_SECTIONS {} {} {}",
                         packet.getTrId(),
                         ttActionId,
                         packet.getEventTime(),
                         packet.getPacketId(),
                    state);
            return createLinkEvent(LinkEvent.LinkType.TOO_FAR_FROM_GRAPH_SECTIONS, null, packet);
        }
    }

    /**
     * Проверка на расстояние от предыдущей привязки до рассматриваемой и отсутствие проблем с привязкой.
     */
    private Optional<LinkEvent> checkSectionLink(SimpleUnitPacket packet, SectionLink sectionLink,
                                                 double packetsTimeDiff) {
        if (state.lastLink != null) {
            double distance = sectionLink.getRoundOffset() - state.lastLink.getRoundOffset();
            double speed = packetsTimeDiff == 0 ? 0 : distance / packetsTimeDiff;
            if (speed >= config.getMaxSpeedMps()) {
                LOGGER.debug("{} {} TOO_HIGH_SPEED {} {} {} {} {} {} {} {}",
                             packet.getTrId(),
                             ttActionId,
                             packet.getEventTime(),
                             packet.getPacketId(),
                             sectionLink.getGraphSectionOrderNum(),
                             sectionLink.getRoundOffset(),
                             distance,
                             packetsTimeDiff,
                             speed,
                        state);
                return Optional.of(createLinkEvent(LinkEvent.LinkType.TOO_HIGH_SPEED, null, packet));
            }
        }

        return Optional.empty();
    }


    public PersimmonState getState() {
        return state;
    }

    public boolean isStarted() {
        return state.started;
    }

    public boolean isInvalidLinking() {
        return state.errorsLinkingCnt >= config.getMaxErrorsLinking();
    }

    private static class SectionDist {

        final GraphSection section;
        final double dist;

        SectionDist(GraphSection section, double dist) {
            this.section = section;
            this.dist = dist;
        }
    }

}
