package ru.advantum.csptm.jep.service.glinker.model;

import com.vividsolutions.jts.geom.Geometry;
import lombok.Getter;
import ru.advantum.csptm.artifact.geotools.GeoTools;

import java.util.List;

@Getter
public class Round {

    private final long roundId;
    private final int length;
    private final float graphSectionStartOffset;
    private final Geometry geometry;
    private final List<GraphSection> graphSections;
    private final List<StopItem> stopItems;

    public Round(long roundId,
                 int length,
                 float graphSectionStartOffset,
                 String wktGeometry,
                 List<GraphSection> graphSections,
                 List<StopItem> stopItems) {
        this.roundId = roundId;
        this.length = length;
        this.graphSectionStartOffset = graphSectionStartOffset;
        this.geometry = GeoTools.INSTANCE.convertFromWkt(wktGeometry);
        if (graphSections != null) {
            for (int i = 0; i < graphSections.size(); i++) {
                if (graphSections.get(i).getOrderNum() != i + 1) {
                    throw new IllegalArgumentException(String.format("Order num of graph section doesn't equal index; %d %d %s",
                                                                     roundId,
                                                                     i,
                                                                     graphSections.get(i)));
                }
            }
        }
        this.graphSections = graphSections;
        this.stopItems = stopItems;
    }

    public boolean isValid() {
        return graphSections != null && stopItems != null;
    }

    @Override
    public String toString() {
        return "Round{" +
                "roundId=" + roundId +
                ", length=" + length +
                ", graphSectionStartOffset=" + graphSectionStartOffset +
                ", graphSections=" + graphSections +
                ", stopItems=" + stopItems +
                '}';
    }
}
