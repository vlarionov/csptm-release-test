package ru.advantum.csptm.jep.service.glinker.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor
public class Timetable {

    private final long trId;
    private final short capacity;
    private final int normId;
    private final long trType;
    private final List<TtAction> ttActions;

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("trId", trId)
                .append("capacity", capacity)
                .append("normId", normId)
                .append("trType", trType)
                .append("ttActions", ttActions.size())
                .toString();
    }
}
