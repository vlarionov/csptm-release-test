package ru.advantum.csptm.jep.service.glinker.linkers.util;

import ru.advantum.csptm.jep.service.glinker.model.event.LinkEvent;
import ru.advantum.csptm.jep.service.glinker.model.event.SimpleUnitPacket;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

/**
 * Created by kaganov on 12/05/2017.
 */
public class LinkerUtils {

    /**
     * Скорость между последним зарегистрированным событием и новым событием.
     */
    public static double speedBetweenLinkEvents(LinkEvent from, LinkEvent to) {
        //расстояние между двумя последними событиями, между которыми находится остановка
        double distanceBetweenTrack = distanceBetweenLinkEvents(from, to);
        //время прохождения
        double secondsBetweenTrack = getSecondsBetweenPackets(from.getSimpleUnitPacket(), to.getSimpleUnitPacket());
        //скорость прохождения, считаем что тс двигалось равномерно
        return distanceBetweenTrack / secondsBetweenTrack;
    }

    public static double getSecondsBetweenPackets(SimpleUnitPacket from, SimpleUnitPacket to) {
        return ChronoUnit.MILLIS.between(from.getEventTime(), to.getEventTime()) / 1000.0;
    }

    /**
     * Расстояние между последним зарегистрированным событием и новым событием(в метрах).
     */
    public static double distanceBetweenLinkEvents(LinkEvent from, LinkEvent to) {
        //расстояние между двумя событиями
        return to.getSectionLink().getRoundOffset() - from.getSectionLink().getRoundOffset();
    }

    /**
     * Момент времени, в который точка проедет расстояние distanceM со скоростью speedMps,
     * если начнет движение в beginTime.
     */
    public static Instant travelEndTime(Instant beginTime, double speedMps, double distanceM) {
        //        return beginTime.minusSeconds((long) (distanceM  / speedMps));
        return beginTime.minusMillis(Math.round(distanceM * 1000 / speedMps));
    }

    public static boolean invalidSpeed(double minSpeedConfig, double calculatedSpeed) {
        return Double.isNaN(calculatedSpeed) || calculatedSpeed <= minSpeedConfig;
    }

    public static boolean invalidDistance(double minDistanceConfig, double calculatedDistance) {
        return Double.isNaN(calculatedDistance) || calculatedDistance <= minDistanceConfig;
    }

    public static double maxNotNan(double d1, double d2) {
        if (Double.isNaN(d1)) {
            return d2;
        }
        if (Double.isNaN(d2)) {
            return d1;
        }
        return Math.max(d1, d2);
    }
}
