package ru.advantum.csptm.jep.service.glinker.linkers;

import java.time.Instant;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.jep.service.glinker.model.Tr;
import ru.advantum.csptm.jep.service.glinker.model.TtAction;
import ru.advantum.csptm.jep.service.glinker.model.state.TrLinkerState;

@Slf4j
@Service
public class StateHolder {
    private Instant createTime;
    private Map<Long, TtAction> ttActions;
    private Map<Long, Long> ttActions2TrId;
    private Map<Long, Map<Long, TtAction>> trTtActions;
    private Map<Long, TrLinkerState> linkerStates;

    public void setAll(Map<Long, TtAction> ttActions, Map<Long, Long> ttActions2TrId, Map<Long, TrLinkerState> linkerStates, Instant createTime) {
        this.ttActions = ttActions;
        this.ttActions2TrId = ttActions2TrId;
        trTtActions = ttActions.entrySet().stream()
                .collect(Collectors.groupingBy(e -> ttActions2TrId.get(e.getValue().getTtActionId()),
                        Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));

        this.linkerStates = linkerStates;
        this.createTime = createTime;
    }

    public Map<Long, Long> getTtActions2TrId() {
        return Collections.unmodifiableMap(ttActions2TrId);
    }


    public Map<Long, Map<Long, TtAction>> getTrTtActions() {
        return Collections.unmodifiableMap(trTtActions);
    }

    public Set<Long> getTrs() {
        return Collections.unmodifiableSet(trTtActions.keySet());
    }

    public TtAction getAction(Long id){
        if(id == null) return null;
        return ttActions.get(id);
    }

    public Collection<TtAction> getTtActionsByTr(Long trId){
        if(trId == null) return Collections.emptySet();
        if(trTtActions.get(trId)!=null) {
            return Collections.unmodifiableCollection(trTtActions.get(trId).values());
        }else return Collections.emptySet();
    }

    public Map<Long, TrLinkerState> getLinkerStates() {
        return linkerStates;
    }

    public Instant getCreateTime() {
        return createTime;
    }

    public void removeTtAction(Long ttActionId){
        if(ttActionId == null) return;
        Long trId = ttActions2TrId.remove(ttActionId);
        if (trId != null) {

            Map<Long, TtAction> trRounds = trTtActions.get(trId);
            if (trRounds != null) {
                trRounds.remove(ttActionId);
            }  else {
                log.error("No trsTtActions for TtAction! {} {}", trId, ttActionId);
            }
        } else {
            log.error("Tt action that isn't in ttActions2TrId map {}", ttActionId);
        }

        ttActions.remove(ttActionId);
    }

    public void newTtAction(TtAction ttAction, Long trId){
        if(ttAction == null || trId == null) return;
        ttActions2TrId.put(ttAction.getTtActionId(), trId);
        ttActions.put(ttAction.getTtActionId(), ttAction);
        trTtActions.computeIfAbsent(trId, newTrId ->new HashMap<>());
        trTtActions.get(trId).put(ttAction.getTtActionId(), ttAction);

    }

    public Long getTrByAction(Long ttActionId){
        if(ttActionId == null) return null;
        return ttActions2TrId.get(ttActionId);
    }

    @PostConstruct
    public void instantiateEmpty(){
        log.info("Instantiating empty state");
        createTime=Instant.now();
        ttActions= new HashMap<>();
        ttActions2TrId=new HashMap<>();
        trTtActions = new HashMap<>();
        linkerStates = new HashMap<>();
    }

    @Override
    public String toString() {
        return "GraphLinkerState{" +
                "createTime=" + createTime +
                ", linkersSize=" + linkerStates.size() +
                ", trRoundsSize=" + trTtActions.size() +
                ", ttActions2Tr2TrIdSize=" + ttActions2TrId.size() +
                '}';
    }
}
