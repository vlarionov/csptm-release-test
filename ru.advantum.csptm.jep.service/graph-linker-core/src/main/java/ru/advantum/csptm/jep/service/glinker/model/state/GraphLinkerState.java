package ru.advantum.csptm.jep.service.glinker.model.state;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.jep.service.glinker.model.TtAction;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;


@Getter
@RequiredArgsConstructor
public class GraphLinkerState {

    private final Instant createTime;
    private final Map<Long, TrLinkerState> linkers;
    private final Map<Long, Map<Long, TtAction>> trTtActions;
    private final Map<Long, Long> ttActions2TrId;

    public GraphLinkerState() {
        this(Instant.now(), new HashMap<>(), new HashMap<>(), new HashMap<>());
    }

    @Override
    public String toString() {
        return "GraphLinkerState{" +
                "createTime=" + createTime +
                ", linkersSize=" + linkers.size() +
                ", trRoundsSize=" + trTtActions.size() +
                ", ttActions2Tr2TrIdSize=" + ttActions2TrId.size() +
                '}';
    }
}
