package ru.advantum.csptm.jep.service.glinker.db;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.*;

public class IntArrayTypeHandler extends BaseTypeHandler<Integer[]> {

    @Override
    public void setNonNullParameter(PreparedStatement ps,
            int i,
            Integer[] parameter,
            JdbcType jdbcType) throws SQLException {
        ps.setArray(i, ps.getConnection().createArrayOf("int8", parameter));
    }

    @Override
    public Integer[] getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return toIntArr(rs.getArray(columnName));
    }

    @Override
    public Integer[] getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return toIntArr(rs.getArray(columnIndex));
    }

    @Override
    public Integer[] getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return toIntArr(cs.getArray(columnIndex));
    }

    private Integer[] toIntArr(Array array) throws SQLException {
        if (array == null) {
            return null;
        }
        Object[] arrayObjects = (Object[]) array.getArray();
        if (arrayObjects == null) {
            return null;
        }
        /*Long[] result = new Long[arrayObjects.length];

        for (Object arrayObject : arrayObjects) {
            result.add((long) arrayObject);
        }
        return result;*/
        //List<Long> list = Arrays.asList((Long[])arrayObjects);
        return (Integer[]) arrayObjects;
    }
}
