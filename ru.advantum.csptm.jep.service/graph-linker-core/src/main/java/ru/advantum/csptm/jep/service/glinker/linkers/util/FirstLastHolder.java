package ru.advantum.csptm.jep.service.glinker.linkers.util;

import java.util.List;

public class FirstLastHolder<T> {

    private T first;
    private T last;

    public FirstLastHolder(T first, T last) {
        this.first = first;
        this.last = last;
    }

    public void add(T item) {
        if (first == null) {
            first = item;
        }
        last = item;
    }

    public void add(List<T> items) {
        if (!items.isEmpty()) {
            if (first == null) {
                first = items.get(0);
            }
            last = items.get(items.size() - 1);
        }
    }

    public T getFirst() {
        return first;
    }

    public T getLast() {
        return last;
    }
}
