package ru.advantum.csptm.jep.service.glinker.linkers.listener;

import lombok.Getter;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.jep.service.glinker.model.event.GraphSectionPass;
import ru.advantum.csptm.jep.service.glinker.model.event.LinkEvent;
import ru.advantum.csptm.jep.service.glinker.model.event.StopItemPass;
import ru.advantum.csptm.jep.service.glinker.model.event.TtActionCompleteEvent;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@Primary
@Service
@Getter
public class EventCollector implements LinkEventListener {

    private final BlockingQueue<LinkEvent> linkEvents = new LinkedBlockingQueue<>();
    private final BlockingQueue<StopItemPass> sipEvents = new LinkedBlockingQueue<>();
    private final BlockingQueue<TtActionCompleteEvent> tacEvents = new LinkedBlockingQueue<>();
    private final BlockingQueue<GraphSectionPass> gspEvents = new LinkedBlockingQueue<>();

    @Override
    public void onLink(LinkEvent event) {
        linkEvents.add(event);
    }

    @Override
    public void onStopItemVisit(StopItemPass event) {
        sipEvents.add(event);
    }

    @Override
    public void onTtActionComplete(TtActionCompleteEvent event) {
        tacEvents.add(event);
    }

    @Override
    public void onSectionVisit(GraphSectionPass event) {
        gspEvents.add(event);
    }

}
