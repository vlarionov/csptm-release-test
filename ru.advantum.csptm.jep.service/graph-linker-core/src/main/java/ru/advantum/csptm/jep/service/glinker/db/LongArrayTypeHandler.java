package ru.advantum.csptm.jep.service.glinker.db;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.*;

public class LongArrayTypeHandler extends BaseTypeHandler<Long[]> {

    @Override
    public void setNonNullParameter(PreparedStatement ps,
                                    int i,
                                    Long[] parameter,
                                    JdbcType jdbcType) throws SQLException {
        ps.setArray(i, ps.getConnection().createArrayOf("int8", parameter));
    }

    @Override
    public Long[] getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return toLongList(rs.getArray(columnName));
    }

    @Override
    public Long[] getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return toLongList(rs.getArray(columnIndex));
    }

    @Override
    public Long[] getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return toLongList(cs.getArray(columnIndex));
    }

    private Long[] toLongList(Array array) throws SQLException {
        if (array == null) {
            return null;
        }
        Object[] arrayObjects = (Object[]) array.getArray();
        if (arrayObjects == null) {
            return null;
        }
        /*Long[] result = new Long[arrayObjects.length];

        for (Object arrayObject : arrayObjects) {
            result.add((long) arrayObject);
        }
        return result;*/
        //List<Long> list = Arrays.asList((Long[])arrayObjects);
        return (Long[]) arrayObjects;
    }
}
