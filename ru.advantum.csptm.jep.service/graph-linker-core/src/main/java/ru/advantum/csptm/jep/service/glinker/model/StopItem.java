package ru.advantum.csptm.jep.service.glinker.model;

import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Data
public class StopItem implements LinkingItem {

    private final long stopItemId;
    private final long roundId;
    private final int orderNum;
    private final double roundOffset;
    private final boolean technical;

    @Override
    public double getOffset() {
        return roundOffset;
    }

    @Override
    public double getLength() {
        return 0;
    }
}
