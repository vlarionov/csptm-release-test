package ru.advantum.csptm.jep.service.glinker.linkers.impl;

import ru.advantum.csptm.jep.service.glinker.config.PersimmonConfig;
import ru.advantum.csptm.jep.service.glinker.model.Round;
import ru.advantum.csptm.jep.service.glinker.model.StopItem;
import ru.advantum.csptm.jep.service.glinker.model.event.LinkEvent;
import ru.advantum.csptm.jep.service.glinker.model.event.StopItemPass;
import ru.advantum.csptm.jep.service.glinker.model.state.FacadeState;
import ru.advantum.csptm.jep.service.glinker.model.state.OffsetItemTimeState;

import java.util.List;
import java.util.stream.Collectors;

public class StopItemTime {

    private final OffsetItemTime<StopItem> offsetItemTime;
    private final long ttActionId;

    public StopItemTime(OffsetItemTimeState state,
                        PersimmonConfig config,
                        long ttActionId,
                        Round round,
                        StopItemPass previousTtActionLastStop,
            FacadeState facadeState) {
        this.offsetItemTime = new OffsetItemTime<>(ttActionId,
                                                   config,
                                                   round.getStopItems(),
                                                   previousTtActionLastStop == null ? null : previousTtActionLastStop.getPassTime(),
                                                   state,
                facadeState);

        this.ttActionId = ttActionId;
    }

    /**
     * @param currentEvent событие связки координаты ТС с маршрутом
     * @return Список остановок, которые прошло ТС с прошлого вызова этого метода
     */
    public List<StopItemPass> link(LinkEvent currentEvent) {
        return offsetItemTime.link(currentEvent).stream()
                .map(ip -> new StopItemPass(currentEvent.getSimpleUnitPacket().getTrId(),
                                            ttActionId,
                                            ip.getItem().getRoundId(),
                                            ip.getItem().getStopItemId(),
                                            ip.getItem().getOrderNum(),
                                            ip.getPassBeginTime()))
                .collect(Collectors.toList());
    }
    
}
