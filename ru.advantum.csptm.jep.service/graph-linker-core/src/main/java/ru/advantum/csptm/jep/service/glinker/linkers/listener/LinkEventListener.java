package ru.advantum.csptm.jep.service.glinker.linkers.listener;

import ru.advantum.csptm.jep.service.glinker.model.event.GraphSectionPass;
import ru.advantum.csptm.jep.service.glinker.model.event.LinkEvent;
import ru.advantum.csptm.jep.service.glinker.model.event.StopItemPass;
import ru.advantum.csptm.jep.service.glinker.model.event.TtActionCompleteEvent;

/**
 * Created by kaganov on 18/02/2017.
 */
public interface LinkEventListener {

    void onLink(LinkEvent event);

    void onStopItemVisit(StopItemPass event);

    void onTtActionComplete(TtActionCompleteEvent event);

    void onSectionVisit(GraphSectionPass event);
}
