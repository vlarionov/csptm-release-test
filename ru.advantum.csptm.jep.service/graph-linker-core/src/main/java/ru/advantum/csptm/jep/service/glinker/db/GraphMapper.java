package ru.advantum.csptm.jep.service.glinker.db;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import ru.advantum.csptm.jep.service.glinker.model.StopItem;
import ru.advantum.csptm.jep.service.glinker.model.Timetable;
import ru.advantum.csptm.jep.service.glinker.model.dto.GraphSectionDTO;
import ru.advantum.csptm.jep.service.glinker.model.dto.NormBetweenStop;
import ru.advantum.csptm.jep.service.glinker.model.dto.RoundDTO;

import java.time.LocalDate;
import java.util.List;

@Mapper
public interface GraphMapper {

    List<RoundDTO> getRounds(@Param("round_ids") Long[] roundIds);

    List<GraphSectionDTO> getGraphSections(@Param("round_ids") Long[] roundIds);

    List<StopItem> getStopItems(@Param("round_ids") Long[] roundIds);

    List<Timetable> getTimetables(@Param("order_date") LocalDate orderDate,
                                     @Param("tr_id") Long trId,
                                     @Param("only_planned") boolean onlyPlanned);

    List<Timetable> getUpdatedTimetables(@Param("tt_variant_ids") Integer[] ttVariantIds,
                                            @Param("tt_out_ids") Integer[] ttOutIds,
                                            @Param("tt_action_ids") Integer[] ttRoundIds);

    List<NormBetweenStop> getNorms(@Param("stop1") long stop1,
            @Param("stop2") long stop2);
}
