package ru.advantum.csptm.jep.service.glinker.linkers.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.jep.service.glinker.config.PersimmonConfig;
import ru.advantum.csptm.jep.service.glinker.linkers.util.LinkerUtils;
import ru.advantum.csptm.jep.service.glinker.model.LinkingItem;
import ru.advantum.csptm.jep.service.glinker.model.event.LinkEvent;
import ru.advantum.csptm.jep.service.glinker.model.event.LinkingItemPass;
import ru.advantum.csptm.jep.service.glinker.model.state.FacadeState;
import ru.advantum.csptm.jep.service.glinker.model.state.OffsetItemTimeState;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class OffsetItemTime<T extends LinkingItem> {

    private static final Logger LOGGER = LoggerFactory.getLogger(OffsetItemTime.class);
    private final PersimmonConfig config;
    private final long ttActionId;
    private final OffsetItemTimeState state;
    private final List<T> items;
    private final Instant previousTtActionLastItemBegin;
    private final FacadeState facadeState;

    public OffsetItemTime(long ttActionId,
            PersimmonConfig config,
            List<T> items,
            Instant previousTtActionLastItemBegin,
            OffsetItemTimeState state,
            FacadeState facadeState)
    {
        this.config = config;
        this.ttActionId = ttActionId;

        this.items = items;
        this.previousTtActionLastItemBegin = previousTtActionLastItemBegin;

        this.state = state;
        this.facadeState = facadeState;
    }

    public List<LinkingItemPass<T>> link(LinkEvent currentEvent) {
        if (currentEvent.getType() != LinkEvent.LinkType.WAITING_FOR_LINKING_START &&
                !currentEvent.getType().isGoodLink()) {
            return Collections.emptyList();
        }

        if (currentEvent.getType().equals(LinkEvent.LinkType.WAITING_FOR_LINKING_START)) {
            state.lastLinkEvent = currentEvent;
            LOGGER.debug("{} {} WAITING_FOR_LINKING_START event {} {}",
                    currentEvent.getSimpleUnitPacket().getTrId(),
                    ttActionId,
                    currentEvent,
                    state);

            return Collections.emptyList();
        }

        if (state.lastLinkEvent == null) {
            LOGGER.debug("{} {} lastLinkEvent is not initialised {} {}",
                    currentEvent.getSimpleUnitPacket().getTrId(),
                    ttActionId,
                    currentEvent,
                    state);

            throw new IllegalArgumentException("lastLinkEvent is not initialised");
        }

        LinkEvent prevLinkEvent = state.lastLinkEvent;

        state.lastLinkEvent = currentEvent;

        final double currentRoundOffset = currentEvent.getSectionLink().getRoundOffset();

        final List<T> itemsToLink;

        if (currentEvent.getType().equals(LinkEvent.LinkType.ROUND_END)) {
            itemsToLink = items.subList(state.notYetLinkedIndex, items.size());
        } else {
            itemsToLink = items.subList(state.notYetLinkedIndex, items.size())
                    .stream()
                    .filter((item -> item.getOffset() + item.getLength() < currentRoundOffset))

                    .collect(Collectors.toList());
        }


        if (itemsToLink.isEmpty()) {
            return Collections.emptyList();
        }

        LOGGER.trace("{} {} items left to link {} {} {}",
                currentEvent.getSimpleUnitPacket().getTrId(),
                ttActionId,
                itemsToLink.size(),
                currentEvent,
                state);

        double currentSpeed = LinkerUtils.speedBetweenLinkEvents(prevLinkEvent, currentEvent);

        //double currentDistance = LinkerUtils.distanceBetweenLinkEvents(state.lastLinkEvent, currentEvent);

        double averageRoundSpeed = 0;
        if (facadeState.getFirstLinkedEvent() != null) {
            averageRoundSpeed = LinkerUtils.speedBetweenLinkEvents(
                    facadeState.getFirstLinkedEvent(),
                    currentEvent);
        }


        if (currentEvent.getType().equals(LinkEvent.LinkType.ROUND_END)) {

            state.speed = LinkerUtils.maxNotNan(LinkerUtils.maxNotNan(currentSpeed, state.speed), averageRoundSpeed);

        } else {
            // мы в самом начале, посчитаем скорость от последнего элемента предыдущего действия
            if (state.speed == 0 && previousTtActionLastItemBegin != null) {
                final long msecFromPreviousTtAction = ChronoUnit.MILLIS.between(previousTtActionLastItemBegin,
                        currentEvent.getSimpleUnitPacket().getEventTime());
                final double speedFromPreviousTtAction =
                        (currentRoundOffset - items.get(0).getOffset()) / (msecFromPreviousTtAction / 1000.0);

                currentSpeed = LinkerUtils.maxNotNan(currentSpeed, speedFromPreviousTtAction);
            }

            state.speed = LinkerUtils.maxNotNan(currentSpeed, averageRoundSpeed);
        }

        final Instant movingTimeStart;
        // если ROUND_END находится на том же месте, что и предыдущее событие, то "доезжать" до конца рейса мы начнём
        // с предыдущего пакета
        if (currentEvent.getType().equals(LinkEvent.LinkType.ROUND_END) &&
                Math.abs(currentEvent.getSectionLink().getRoundOffset() -
                        prevLinkEvent.getSectionLink().getRoundOffset()) <= config.getEpsilon()) {
            movingTimeStart = prevLinkEvent.getSimpleUnitPacket().getEventTime();
        } else {
            movingTimeStart = currentEvent.getSimpleUnitPacket().getEventTime();
        }

        final List<LinkingItemPass<T>> linkingItemPasses = new ArrayList<>();
        double speed = state.speed;
        if(state.notYetLinkedIndex==0){
            speed = Math.max(config.getMinimumSpeedToExtrapolatePassedItemTimesAtTheBeginning(), speed);
        }
        for (final T itemToLink : itemsToLink) {
            final double distanceFromItemBegin = currentRoundOffset - itemToLink.getOffset();
            Instant itemBeginTime = LinkerUtils.travelEndTime(movingTimeStart, speed, distanceFromItemBegin);

            // можем закончить элемент
            if (currentEvent.getType() == LinkEvent.LinkType.ROUND_END
                    || currentRoundOffset >= itemToLink.getOffset() + itemToLink.getLength()) {
                // расстояние между последней координатой и концом элемента
                final double distanceFromItemEnd = currentRoundOffset - (itemToLink.getOffset() + itemToLink.getLength());

                final Instant timeAtItemEnd = LinkerUtils.travelEndTime(movingTimeStart, speed, distanceFromItemEnd);

                final LinkingItemPass<T> linkingItemPass;
                linkingItemPass = new LinkingItemPass<>(itemToLink, itemBeginTime, timeAtItemEnd);
                state.notYetLinkedIndex++;
                LOGGER.debug("{} {} item passed {} {} {} {}",
                        currentEvent.getSimpleUnitPacket().getTrId(),
                        ttActionId,
                        linkingItemPass,
                        movingTimeStart,
                        distanceFromItemEnd,
                        state);
                linkingItemPasses.add(linkingItemPass);
            }
        }

        return linkingItemPasses;
    }

    public OffsetItemTimeState getState() {
        return state;
    }
}
