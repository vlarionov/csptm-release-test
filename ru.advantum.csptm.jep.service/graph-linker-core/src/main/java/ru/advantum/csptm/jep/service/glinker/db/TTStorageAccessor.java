package ru.advantum.csptm.jep.service.glinker.db;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.advantum.csptm.jep.service.glinker.model.GraphSection;
import ru.advantum.csptm.jep.service.glinker.model.Round;
import ru.advantum.csptm.jep.service.glinker.model.StopItem;
import ru.advantum.csptm.jep.service.glinker.model.Timetable;
import ru.advantum.csptm.jep.service.glinker.model.TtAction;
import ru.advantum.csptm.jep.service.glinker.model.dto.GraphSectionDTO;
import ru.advantum.csptm.jep.service.glinker.model.dto.RoundDTO;
import ru.advantum.csptm.jep.service.glinker.model.event.StopItemPass;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

@Component
public class TTStorageAccessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(TTStorageAccessor.class);

    private final ConcurrentMap<Long, Round> roundCache = new ConcurrentHashMap<>();
    private final GraphMapper mapper;
    private Table<Long, Long, StopItem> stopItemsTable = HashBasedTable.create();//stopItemId, roundId to StopItem

    @Autowired
    public TTStorageAccessor(@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") GraphMapper mapper) {
        this.mapper = mapper;
    }

    public void updateRoundsCache(Long[] roundIds) {
        if (roundIds.length == 0) {
            return;
        }

        LOGGER.info("Loading rounds {}", roundIds.length);

        List<RoundDTO> roundDTOs = mapper.getRounds(roundIds);
        LOGGER.info("roundDTOs {}", roundDTOs.size());

        List<GraphSectionDTO> graphSectionsDTO = mapper.getGraphSections(roundIds);
        LOGGER.info("graphSectionsDTO {}", graphSectionsDTO.size());

        // раскладываем секции в правильный класс, а потом в мапы по рейсам, отсортированные по порядковому номеру
        Map<Long, List<GraphSection>> gs2rt = graphSectionsDTO.stream()
                .map(gsDTO -> new GraphSection(gsDTO.getGraphSectionId(),
                                               gsDTO.getRoundId(),
                                               gsDTO.getOrderNum(),
                                               gsDTO.getLength(),
                                               gsDTO.getRoundOffset(),
                                               gsDTO.getWktGeometry()))
                .collect(groupingBy(GraphSection::getRoundId,
                                    collectingAndThen(toList(), l -> l.stream()
                                            .sorted(Comparator.comparing(GraphSection::getOrderNum))
                                            .collect(toList()))));

        List<StopItem> stopItems = mapper.getStopItems(roundIds);
        LOGGER.info("stopItems {}", stopItems.size());
        stopItems.forEach(si -> stopItemsTable.put(si.getStopItemId(), si.getRoundId(), si));
        // раскладываем остановки в мапы по рейсам, отсортированные по порядковому номеру
        Map<Long, List<StopItem>> sp2rt = stopItems.stream()
                .collect(groupingBy(StopItem::getRoundId,
                                    collectingAndThen(toList(), l -> l.stream()
                                            .sorted(Comparator.comparing(StopItem::getOrderNum))
                                            .collect(toList()))));

        roundDTOs.forEach(roundDTO -> roundCache.put(roundDTO.getRoundId(), new Round(roundDTO.getRoundId(),
                                                            roundDTO.getLength(),
                                                            roundDTO.getGraphSectionStartOffset(),
                                                            roundDTO.getWktGeometry(),
                                                            gs2rt.get(roundDTO.getRoundId()),
                                                            sp2rt.get(roundDTO.getRoundId()))));
    }

    public boolean isTechnical(StopItemPass sip){
        StopItem si = stopItemsTable.get(sip.getStopItemId(), sip.getRoundId());
        if(si == null){
            LOGGER.error("Internal Error. Stop not found with stopId="+sip.getStopItemId()+", roundId="+sip.getRoundId());
            return false;
        }
        return si.isTechnical();
    }

    public Optional<Round> getRoundCached(long roundId) {
        Round r = roundCache.get(roundId);
        if(r == null){
            updateRoundsCache(new Long[]{roundId});
            roundCache.get(roundId);
        }
        if(r == null){
            LOGGER.error("Failed to retrieve round with id="+roundId);
        }
        return Optional.ofNullable(r);
    }

    public void recalcFields(TtAction ttAction) {
        final Optional<Round> round = getRoundCached(ttAction.getRoundId());
        ttAction.setActive(ttAction.isActive() && round.isPresent() && round.get().isValid());
    }

    private void timetableRecalcFields(Timetable timetable) {
        timetable.getTtActions().forEach(this::recalcFields);
    }

    private void updateRoundsCache(List<Timetable> timetables) {
        Long[] uniqueRoundIds = timetables.stream()
                .flatMap(tt -> tt.getTtActions().stream())
                .map(TtAction::getRoundId)
                .distinct()
                .filter(roundId -> !roundCache.containsKey(roundId))
                .toArray(Long[]::new);
        LOGGER.info("Unique rounds not in cache {}", uniqueRoundIds.length);

        updateRoundsCache(uniqueRoundIds);
    }

    public List<Timetable> getTimetables(Integer[] ttVariantIds,
            Integer[] ttOutIds,
            Integer[] ttRoundIds) {
        List<Timetable> timetables;
        timetables = mapper.getUpdatedTimetables(ttVariantIds, ttOutIds, ttRoundIds);

        updateRoundsCache(timetables);

        timetables.forEach(this::timetableRecalcFields);

        return timetables;
    }

    public List<Timetable> getTimetables(LocalDate date, Long trId, boolean onlyPlanned) {
        List<Timetable> timetables;

        LOGGER.info("Getting timetables {} {}", date, trId);
        timetables = mapper.getTimetables(date, trId, onlyPlanned);

        LOGGER.info("Got timetables {}", timetables.size());

        updateRoundsCache(timetables);

        timetables.forEach(this::timetableRecalcFields);

        return timetables;
    }

    @Scheduled(initialDelay = 60 * 60 * 1000, fixedDelay = 60 * 60 * 1000)
    public void updateCache() {
        LOGGER.info("Updating cache");
        updateRoundsCache(roundCache.keySet().toArray(new Long[0]));
        LOGGER.info("Cache updated");
    }
}
