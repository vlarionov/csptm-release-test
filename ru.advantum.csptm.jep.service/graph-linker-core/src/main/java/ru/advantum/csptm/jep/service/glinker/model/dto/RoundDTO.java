package ru.advantum.csptm.jep.service.glinker.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@NoArgsConstructor(force = true)
@RequiredArgsConstructor
public class RoundDTO {

    private final long roundId;
    private final int length;
    private final float graphSectionStartOffset;
    private final String wktGeometry;
}
