package ru.advantum.csptm.jep.service.glinker.model.dto;

import java.time.LocalTime;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@ToString
public class NormBetweenStop implements Comparable<NormBetweenStop>{
    private int normId;
    private LocalTime hourFrom;
    private LocalTime hourTo;
    private short capacity;
    private int duration;
    private long trType;

    public NormBetweenStop(int normId, LocalTime hourFrom, LocalTime hourTo, short capacity, int duration,
            long trType) {
        this.normId = normId;
        this.hourFrom = hourFrom;
        this.hourTo = hourTo;
        this.capacity = capacity;
        this.duration = duration;
        this.trType = trType;
    }

    @Override
    public int compareTo(NormBetweenStop o) {
        return hourFrom.compareTo(o.getHourFrom());
    }
}
