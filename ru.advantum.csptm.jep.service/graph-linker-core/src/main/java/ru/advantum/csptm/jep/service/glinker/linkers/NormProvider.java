package ru.advantum.csptm.jep.service.glinker.linkers;

import java.time.LocalTime;
import java.util.Optional;

public interface NormProvider {
    //норматив в секундах для участка от stop1 до stop2
    //time время московское
    Optional<Integer> getNorm(int normId, long stop1, long stop2, LocalTime time, short capacity, long trType);
}
