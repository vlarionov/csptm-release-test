package ru.advantum.csptm.jep.service.glinker.model;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(of = "ttActionId")
public class TtAction {

    private final long ttActionId;
    private final long roundId;
    private final Instant planBegin;
    private final Instant planEnd;
    private boolean active;

    @Override
    public String toString() {
        return "TtAction{" + "ttActionId=" + ttActionId +
                ", round=" + roundId +
                ", planBegin=" + planBegin +
                ", planEnd=" + planEnd +
                ", active=" + active +
                '}';
    }

    public TtAction(long ttActionId, long roundId, LocalDateTime planBegin, LocalDateTime planEnd, boolean active) {
        this.ttActionId = ttActionId;
        this.roundId = roundId;
        this.planBegin = planBegin.toInstant(ZoneOffset.UTC);
        this.planEnd = planEnd.toInstant(ZoneOffset.UTC);
        this.active = active;
    }

    public long getTtActionId() {
        return ttActionId;
    }

    public long getRoundId() {
        return roundId;
    }

    public Instant getPlanBegin() {
        return planBegin;
    }

    public Instant getPlanEnd() {
        return planEnd;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
