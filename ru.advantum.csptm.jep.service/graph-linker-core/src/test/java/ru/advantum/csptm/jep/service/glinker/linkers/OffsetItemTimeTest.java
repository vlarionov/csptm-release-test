package ru.advantum.csptm.jep.service.glinker.linkers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.advantum.csptm.jep.service.glinker.config.PersimmonConfig;
import ru.advantum.csptm.jep.service.glinker.linkers.impl.OffsetItemTime;
import ru.advantum.csptm.jep.service.glinker.linkers.impl.Persimmon;
import ru.advantum.csptm.jep.service.glinker.model.GraphSection;
import ru.advantum.csptm.jep.service.glinker.model.LinkingItem;
import ru.advantum.csptm.jep.service.glinker.model.Round;
import ru.advantum.csptm.jep.service.glinker.model.StopItem;
import ru.advantum.csptm.jep.service.glinker.model.event.LinkEvent;
import ru.advantum.csptm.jep.service.glinker.model.event.LinkingItemPass;
import ru.advantum.csptm.jep.service.glinker.model.event.SectionLink;
import ru.advantum.csptm.jep.service.glinker.model.event.SimpleUnitPacket;
import ru.advantum.csptm.jep.service.glinker.model.state.FacadeState;
import ru.advantum.csptm.jep.service.glinker.model.state.OffsetItemTimeState;
import ru.advantum.csptm.jep.service.glinker.model.state.PersimmonState;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OffsetItemTimeTest {

    private final Round simpleRound = JsonLoader.loadRound("src/test/resources/round_36011_simple.json");
    private final PersimmonConfig defaultConfig = new PersimmonConfig() {
    };

    @Test
    public void whenGoodLinksThenGoodStopTimes() {
        final OffsetItemTime<StopItem> stopItemTime = new OffsetItemTime<>(1,
                                                                           defaultConfig,
                                                                           simpleRound.getStopItems(),
                                                                           null,
                                                                           new OffsetItemTimeState(), new FacadeState());

        List<LinkEvent> links = new ArrayList<>();
        links.add(makeLinkEvent(LinkEvent.LinkType.WAITING_FOR_LINKING_START,
                                simpleRound,
                                0,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 0)));
        links.add(makeLinkEvent(LinkEvent.LinkType.ROUND_BEGIN,
                                simpleRound,
                                133,
                                LocalDateTime.of(2018, 2, 5, 10, 1, 12)));
        links.add(makeLinkEvent(LinkEvent.LinkType.LINKED,
                                simpleRound,
                                732,
                                LocalDateTime.of(2018, 2, 5, 10, 2, 4, 589_000_000)));

        final List<List<LinkingItemPass<StopItem>>> stopItemPasses = new ArrayList<>();

        for (LinkEvent link : links) {
            stopItemPasses.add(stopItemTime.link(link));
        }

        Assertions.assertTrue(stopItemPasses.get(0).isEmpty());
        Assertions.assertTrue(stopItemPasses.get(1).size() == 1);
        Assertions.assertTrue(stopItemPasses.get(2).size() == 1);

        //Assertions.assertTrue(stopItemPasses.get(1).get(0).getPassBeginTime().equals(links.get(0).getSimpleUnitPacket().getEventTime()));
        Assertions.assertTrue(passMatch(stopItemPasses.get(2).get(0), LocalDateTime.of(2018, 2, 5, 10, 1, 58, 44_000_000)));
    }

    @Test
    public void whenSeveralPointsThenOnlyLastOneCounts() {
        final OffsetItemTime<StopItem> stopItemTime = new OffsetItemTime<>(1,
                                                                           defaultConfig,
                                                                           simpleRound.getStopItems(),
                                                                           null,
                                                                           new OffsetItemTimeState(), new FacadeState());

        List<LinkEvent> links = new ArrayList<>();
        links.add(makeLinkEvent(LinkEvent.LinkType.WAITING_FOR_LINKING_START,
                                simpleRound,
                                0,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 0)));
        links.add(makeLinkEvent(LinkEvent.LinkType.ROUND_BEGIN,
                                simpleRound,
                                133,
                                LocalDateTime.of(2018, 2, 5, 10, 1, 12)));
        links.add(makeLinkEvent(LinkEvent.LinkType.LINKED,
                                simpleRound,
                                577,
                                LocalDateTime.of(2018, 2, 5, 10, 2, 4, 589_000_000)));
        links.add(makeLinkEvent(LinkEvent.LinkType.LINKED,
                                simpleRound,
                                700,
                                LocalDateTime.of(2018, 2, 5, 10, 2, 13, 300_000_000)));

        final List<List<LinkingItemPass<StopItem>>> stopItemPasses = new ArrayList<>();

        for (LinkEvent link : links) {
            stopItemPasses.add(stopItemTime.link(link));
        }

        Assertions.assertTrue(stopItemPasses.get(0).isEmpty());
        Assertions.assertTrue(stopItemPasses.get(1).size() == 1);
        Assertions.assertTrue(stopItemPasses.get(2).isEmpty());
        Assertions.assertTrue(passMatch(stopItemPasses.get(3).get(0), LocalDateTime.of(2018, 2, 5, 10, 2, 10, 590_000_000)));
    }

    @Test
    public void whenSeveralStopsThenGoodTimes() {
        final OffsetItemTime<StopItem> stopItemTime = new OffsetItemTime<>(1,
                                                                           defaultConfig,
                                                                           simpleRound.getStopItems(),
                                                                           null,
                                                                           new OffsetItemTimeState(), new FacadeState());

        List<LinkEvent> links = new ArrayList<>();
        links.add(makeLinkEvent(LinkEvent.LinkType.WAITING_FOR_LINKING_START,
                                simpleRound,
                                0,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 0)));
        links.add(makeLinkEvent(LinkEvent.LinkType.ROUND_BEGIN,
                                simpleRound,
                                133,
                                LocalDateTime.of(2018, 2, 5, 10, 1, 12)));
        links.add(makeLinkEvent(LinkEvent.LinkType.LINKED,
                                simpleRound,
                                577,
                                LocalDateTime.of(2018, 2, 5, 10, 2, 4, 589_000_000)));
        links.add(makeLinkEvent(LinkEvent.LinkType.LINKED,
                                simpleRound,
                                1512,
                                LocalDateTime.of(2018, 2, 5, 10, 2, 58, 900_000_000)));

        final List<List<LinkingItemPass<StopItem>>> stopItemPasses = new ArrayList<>();

        for (LinkEvent link : links) {
            stopItemPasses.add(stopItemTime.link(link));
        }

        Assertions.assertTrue(stopItemPasses.get(0).isEmpty());
        Assertions.assertTrue(stopItemPasses.get(1).size() == 1);
        Assertions.assertTrue(stopItemPasses.get(2).isEmpty());
        Assertions.assertTrue(passMatch(stopItemPasses.get(3).get(0), LocalDateTime.of(2018, 2, 5, 10, 2, 9, 400_000_000)));
        Assertions.assertTrue(passMatch(stopItemPasses.get(3).get(1), LocalDateTime.of(2018, 2, 5, 10, 2, 34, 450_000_000)));
        Assertions.assertTrue(passMatch(stopItemPasses.get(3).get(2), LocalDateTime.of(2018, 2, 5, 10, 2, 47, 150_000_000)));
    }

    @Test
    public void whenStartThenDriveBack() {
        final OffsetItemTime<StopItem> stopItemTime = new OffsetItemTime<>(1,
                                                                           defaultConfig,
                                                                           simpleRound.getStopItems(),
                                                                           null,
                                                                           new OffsetItemTimeState(), new FacadeState());

        List<LinkEvent> links = new ArrayList<>();
        links.add(makeLinkEvent(LinkEvent.LinkType.WAITING_FOR_LINKING_START,
                                simpleRound,
                                700,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 0)));
        links.add(makeLinkEvent(LinkEvent.LinkType.ROUND_BEGIN,
                                simpleRound,
                                789,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 8, 650_000_000)));

        final List<List<LinkingItemPass<StopItem>>> stopItemPasses = new ArrayList<>();

        for (LinkEvent link : links) {
            stopItemPasses.add(stopItemTime.link(link));
        }

        Assertions.assertTrue(stopItemPasses.get(0).isEmpty());
        Assertions.assertTrue(passMatch(stopItemPasses.get(1).get(0), LocalDateTime.of(2018, 2, 5, 9, 58, 51, 900_000_000)));
        Assertions.assertTrue(passMatch(stopItemPasses.get(1).get(1), LocalDateTime.of(2018, 2, 5, 9, 59, 56, 300_000_000)));
    }

    @Test
    public void whenStartAndNoPrevStopThenTimeIsFarAway() {
        final OffsetItemTime<StopItem> stopItemTime = new OffsetItemTime<>(1,
                                                                           defaultConfig,
                                                                           simpleRound.getStopItems(),
                                                                           null,
                                                                           new OffsetItemTimeState(), new FacadeState());

        List<LinkEvent> links = new ArrayList<>();
        links.add(makeLinkEvent(LinkEvent.LinkType.WAITING_FOR_LINKING_START,
                                simpleRound,
                                700,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 0)));
        links.add(makeLinkEvent(LinkEvent.LinkType.ROUND_BEGIN,
                                simpleRound,
                                789,
                                LocalDateTime.of(2018, 2, 5, 10, 1, 18, 650_000_000)));

        final List<List<LinkingItemPass<StopItem>>> stopItemPasses = new ArrayList<>();

        for (LinkEvent link : links) {
            stopItemPasses.add(stopItemTime.link(link));
        }

        Assertions.assertTrue(stopItemPasses.get(0).isEmpty());
        Assertions.assertTrue(passMatch(stopItemPasses.get(1).get(0), LocalDateTime.of(2018, 2, 5, 9, 54, 44, 150_000_000)));
        Assertions.assertTrue(passMatch(stopItemPasses.get(1).get(1), LocalDateTime.of(2018, 2, 5, 10, 00, 15, 150_000_000)));
    }

    @Test
    public void whenStartAndPrevStopThenStopIsNotBeforePrevStop() {
        final OffsetItemTime<StopItem> stopItemTime = new OffsetItemTime<>(1,
                                                                           defaultConfig,
                                                                           simpleRound.getStopItems(),
                                                                           LocalDateTime.of(2018, 2, 5, 9, 55, 48, 650_000_000).toInstant(ZoneOffset.UTC),
                                                                           new OffsetItemTimeState(), new FacadeState());

        List<LinkEvent> links = new ArrayList<>();
        links.add(makeLinkEvent(LinkEvent.LinkType.WAITING_FOR_LINKING_START,
                                simpleRound,
                                700,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 0)));
        links.add(makeLinkEvent(LinkEvent.LinkType.ROUND_BEGIN,
                                simpleRound,
                                789,
                                LocalDateTime.of(2018, 2, 5, 10, 1, 18, 650_000_000)));

        final List<List<LinkingItemPass<StopItem>>> stopItemPasses = new ArrayList<>();

        for (LinkEvent link : links) {
            stopItemPasses.add(stopItemTime.link(link));
        }

        Assertions.assertTrue(stopItemPasses.get(0).isEmpty());
        Assertions.assertTrue(passMatch(stopItemPasses.get(1).get(0), LocalDateTime.of(2018, 2, 5, 9, 55, 48, 650_000_000)));
        Assertions.assertTrue(passMatch(stopItemPasses.get(1).get(1), LocalDateTime.of(2018, 2, 5, 10, 0, 25, 700_000_000)));
    }

    @Test
    public void whenStateSaveAndStateLoadThenResultIsTheSame() {
        List<SimpleUnitPacket> track = JsonLoader.loadTrack("src/test/resources/track_36011_good.json");

        Persimmon persimmon = new Persimmon(new PersimmonState(), defaultConfig, 1, simpleRound);
        List<LinkEvent> links = new ArrayList<>();
        for (SimpleUnitPacket packet : track) {
            links.add(persimmon.link(packet));
        }

        final OffsetItemTime<StopItem> stopItemTime = new OffsetItemTime<>(1,
                                                                           defaultConfig,
                                                                           simpleRound.getStopItems(),
                                                                           null,
                                                                           new OffsetItemTimeState(), new FacadeState());
        List<List<LinkingItemPass<StopItem>>> stopItemPassesContinuous = new ArrayList<>();
        for (LinkEvent link : links) {
            stopItemPassesContinuous.add(stopItemTime.link(link));
        }

        final OffsetItemTime<StopItem> stopItemTimeFirst = new OffsetItemTime<>(1,
                                                                                defaultConfig,
                                                                                simpleRound.getStopItems(),
                                                                                null,
                                                                                new OffsetItemTimeState(), new FacadeState());
        List<List<LinkingItemPass<StopItem>>> stopItemPassesParted = new ArrayList<>();
        for (LinkEvent link : links.subList(0, 10)) {
            stopItemPassesParted.add(stopItemTimeFirst.link(link));
        }
        final OffsetItemTimeState state = stopItemTimeFirst.getState();
        final OffsetItemTime<StopItem> stopItemTimeSecond = new OffsetItemTime<>(1,
                                                                                 defaultConfig,
                                                                                 simpleRound.getStopItems(),
                                                                                 null,
                                                                                 state, new FacadeState());
        for (LinkEvent link : links.subList(10, links.size())) {
            stopItemPassesParted.add(stopItemTimeSecond.link(link));
        }

        Assertions.assertEquals(stopItemPassesContinuous, stopItemPassesParted);
    }

    /*@Test
    public void whenSlowStartThenWaitForGoodSpeed() {
        final OffsetItemTime<StopItem> stopItemTime = new OffsetItemTime<>(1,
                                                                           defaultConfig,
                                                                           simpleRound.getStopItems(),
                                                                           null,
                                                                           new OffsetItemTimeState(), new FacadeState());

        List<LinkEvent> links = new ArrayList<>();
        links.add(makeLinkEvent(LinkEvent.LinkType.WAITING_FOR_LINKING_START,
                                simpleRound,
                                698,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 0)));
        links.add(makeLinkEvent(LinkEvent.LinkType.ROUND_BEGIN,
                                simpleRound,
                                710,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 24)));
        links.add(makeLinkEvent(LinkEvent.LinkType.LINKED,
                                simpleRound,
                                831,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 26, 600_000_000)));

        final List<List<LinkingItemPass<StopItem>>> stopItemPasses = new ArrayList<>();

        for (LinkEvent link : links) {
            stopItemPasses.add(stopItemTime.link(link));
        }

        Assertions.assertTrue(stopItemPasses.get(0).isEmpty());
        Assertions.assertTrue(stopItemPasses.get(1).isEmpty());
        Assertions.assertTrue(passMatch(stopItemPasses.get(2).get(0), LocalDateTime.of(2018, 2, 5, 9, 57, 40, 400_000_000)));
        Assertions.assertTrue(passMatch(stopItemPasses.get(2).get(1), LocalDateTime.of(2018, 2, 5, 9, 59, 52, 800_000_000)));
    }
*/
    @Test
    public void whenGoodEndThenGoodTime() {
        final OffsetItemTime<StopItem> stopItemTime = new OffsetItemTime<>(1,
                                                                           defaultConfig,
                                                                           simpleRound.getStopItems(),
                                                                           null,
                                                                           new OffsetItemTimeState(), new FacadeState());

        List<LinkEvent> links = new ArrayList<>();
        links.add(makeLinkEvent(LinkEvent.LinkType.WAITING_FOR_LINKING_START,
                                simpleRound,
                                89,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 0)));
        links.add(makeLinkEvent(LinkEvent.LinkType.ROUND_BEGIN,
                                simpleRound,
                                202,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 24)));
        links.add(makeLinkEvent(LinkEvent.LinkType.ROUND_END,
                                simpleRound,
                                831,
                                LocalDateTime.of(2018, 2, 5, 10, 1, 13, 680_000_000)));

        final List<List<LinkingItemPass<StopItem>>> stopItemPasses = new ArrayList<>();

        for (LinkEvent link : links) {
            stopItemPasses.add(stopItemTime.link(link));
        }

        List<LocalDateTime> expectedPassTimes = Arrays.asList(LocalDateTime.of(2018, 2, 5, 10, 1, 0, 331_000_000),
                                                              LocalDateTime.of(2018, 2, 5, 10, 1, 34, 220_000_000),
                                                              LocalDateTime.of(2018, 2, 5, 10, 1, 52, 230_000_000),
                                                              LocalDateTime.of(2018, 2, 5, 10, 2, 18, 214_000_000),
                                                              LocalDateTime.of(2018, 2, 5, 10, 2, 49, 490_000_000),
                                                              LocalDateTime.of(2018, 2, 5, 10, 3, 35, 540_000_000),
                                                              LocalDateTime.of(2018, 2, 5, 10, 4, 6, 30_000_000));

        Assertions.assertTrue(stopItemPasses.get(0).isEmpty());
        Assertions.assertTrue(stopItemPasses.get(1).size() == 1);

        for (int i = 0; i < expectedPassTimes.size(); i++) {
            Assertions.assertTrue(passMatch(stopItemPasses.get(2).get(i), expectedPassTimes.get(i)));
        }
    }

    @Test
    public void whenStopAtEndThenTimeFromPreviousLink() {
        final OffsetItemTime<StopItem> stopItemTime = new OffsetItemTime<>(1,
                                                                           defaultConfig,
                                                                           simpleRound.getStopItems(),
                                                                           null,
                                                                           new OffsetItemTimeState(), new FacadeState());

        List<LinkEvent> links = new ArrayList<>();
        links.add(makeLinkEvent(LinkEvent.LinkType.WAITING_FOR_LINKING_START,
                                simpleRound,
                                89,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 0)));
        links.add(makeLinkEvent(LinkEvent.LinkType.ROUND_BEGIN,
                                simpleRound,
                                202,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 24)));
        links.add(makeLinkEvent(LinkEvent.LinkType.LINKED,
                                simpleRound,
                                831,
                                LocalDateTime.of(2018, 2, 5, 10, 1, 13, 680_000_000)));
        links.add(makeLinkEvent(LinkEvent.LinkType.ROUND_END,
                                simpleRound,
                                831,
                                LocalDateTime.of(2018, 2, 5, 10, 2, 13, 680_000_000)));

        final List<List<LinkingItemPass<StopItem>>> stopItemPasses = new ArrayList<>();

        for (LinkEvent link : links) {
            stopItemPasses.add(stopItemTime.link(link));
        }

        List<LocalDateTime> expectedPassTimes = Arrays.asList(LocalDateTime.of(2018, 2, 5, 10, 1, 34, 220_000_000),
                                                              LocalDateTime.of(2018, 2, 5, 10, 1, 52, 230_000_000),
                                                              LocalDateTime.of(2018, 2, 5, 10, 2, 18, 214_000_000),
                                                              LocalDateTime.of(2018, 2, 5, 10, 2, 49, 490_000_000),
                                                              LocalDateTime.of(2018, 2, 5, 10, 3, 35, 540_000_000),
                                                              LocalDateTime.of(2018, 2, 5, 10, 4, 6, 30_000_000));

        Assertions.assertTrue(stopItemPasses.get(0).isEmpty());
        Assertions.assertTrue(stopItemPasses.get(1).size() == 1);
        Assertions.assertTrue(stopItemPasses.get(2).size() == 1);

        for (int i = 0; i < expectedPassTimes.size(); i++) {
            Assertions.assertTrue(passMatch(stopItemPasses.get(3).get(i), expectedPassTimes.get(i)));
        }
    }

    @Test
    public void whenSlowEndThenTimeFromPreviousSpeed() {
        final OffsetItemTime<StopItem> stopItemTime = new OffsetItemTime<>(1,
                                                                           defaultConfig,
                                                                           simpleRound.getStopItems(),
                                                                           null,
                                                                           new OffsetItemTimeState(), new FacadeState());

        List<LinkEvent> links = new ArrayList<>();
        links.add(makeLinkEvent(LinkEvent.LinkType.WAITING_FOR_LINKING_START,
                                simpleRound,
                                89,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 0)));
        links.add(makeLinkEvent(LinkEvent.LinkType.ROUND_BEGIN,
                                simpleRound,
                                202,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 24)));
        links.add(makeLinkEvent(LinkEvent.LinkType.LINKED,
                                simpleRound,
                                831,
                                LocalDateTime.of(2018, 2, 5, 10, 1, 13, 680_000_000)));
        links.add(makeLinkEvent(LinkEvent.LinkType.ROUND_END,
                                simpleRound,
                                832,
                                LocalDateTime.of(2018, 2, 5, 10, 2, 13, 680_000_000)));

        final List<List<LinkingItemPass<StopItem>>> stopItemPasses = new ArrayList<>();

        for (LinkEvent link : links) {
            stopItemPasses.add(stopItemTime.link(link));
        }

        List<LocalDateTime> expectedPassTimes = Arrays.asList(LocalDateTime.of(2018, 2, 5, 10, 2, 34, 220_000_000),
                                                              LocalDateTime.of(2018, 2, 5, 10, 2, 52, 230_000_000),
                                                              LocalDateTime.of(2018, 2, 5, 10, 3, 18, 214_000_000),
                                                              LocalDateTime.of(2018, 2, 5, 10, 3, 49, 490_000_000),
                                                              LocalDateTime.of(2018, 2, 5, 10, 4, 35, 540_000_000),
                                                              LocalDateTime.of(2018, 2, 5, 10, 5, 6, 30_000_000));

        Assertions.assertTrue(stopItemPasses.get(0).isEmpty());
        Assertions.assertTrue(stopItemPasses.get(1).size() == 1);
        Assertions.assertTrue(stopItemPasses.get(2).size() == 1);

        for (int i = 0; i < expectedPassTimes.size(); i++) {
            Assertions.assertTrue(passMatch(stopItemPasses.get(3).get(i), expectedPassTimes.get(i)));
        }
    }

    /*@Test
    public void whenSlowRoundThenNoPasses() {
        final OffsetItemTime<StopItem> stopItemTime = new OffsetItemTime<>(1,
                                                                           defaultConfig,
                                                                           simpleRound.getStopItems(),
                                                                           null,
                                                                           new OffsetItemTimeState(), new FacadeState());

        List<LinkEvent> links = new ArrayList<>();
        links.add(makeLinkEvent(LinkEvent.LinkType.WAITING_FOR_LINKING_START,
                                simpleRound,
                                89,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 0)));
        links.add(makeLinkEvent(LinkEvent.LinkType.ROUND_BEGIN,
                                simpleRound,
                                202,
                                LocalDateTime.of(2018, 2, 5, 12, 0, 24)));
        links.add(makeLinkEvent(LinkEvent.LinkType.ROUND_END,
                                simpleRound,
                                832,
                                LocalDateTime.of(2018, 2, 5, 14, 2, 13, 680_000_000)));

        final List<List<LinkingItemPass<StopItem>>> stopItemPasses = new ArrayList<>();

        for (LinkEvent link : links) {
            stopItemPasses.add(stopItemTime.link(link));
        }

        Assertions.assertTrue(stopItemPasses.get(0).isEmpty());
        Assertions.assertTrue(stopItemPasses.get(1).isEmpty());
        Assertions.assertTrue(stopItemPasses.get(2).isEmpty());
    }
*/
    @Test
    public void whenBadStartThenNoPasses() {
        final OffsetItemTime<StopItem> stopItemTime = new OffsetItemTime<>(1,
                                                                           defaultConfig,
                                                                           simpleRound.getStopItems(),
                                                                           null,
                                                                           new OffsetItemTimeState(), new FacadeState());

        List<LinkEvent> links = new ArrayList<>();
        links.add(makeLinkEvent(LinkEvent.LinkType.TOO_FAR_FROM_ROUND,
                                simpleRound,
                                89,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 0)));
        links.add(makeLinkEvent(LinkEvent.LinkType.TOO_FAR_FROM_ROUND,
                                simpleRound,
                                189,
                                LocalDateTime.of(2018, 2, 5, 10, 1, 0)));
        links.add(makeLinkEvent(LinkEvent.LinkType.WAITING_FOR_LINKING_START,
                                simpleRound,
                                189,
                                LocalDateTime.of(2018, 2, 5, 10, 2, 0)));
        links.add(makeLinkEvent(LinkEvent.LinkType.ROUND_BEGIN,
                                simpleRound,
                                302,
                                LocalDateTime.of(2018, 2, 5, 10, 3, 24)));
        links.add(makeLinkEvent(LinkEvent.LinkType.TOO_FAR_FROM_GRAPH_SECTIONS,
                                simpleRound,
                                400,
                                LocalDateTime.of(2018, 2, 5, 10, 4, 0)));
        links.add(makeLinkEvent(LinkEvent.LinkType.ROUND_END,
                                simpleRound,
                                832,
                                LocalDateTime.of(2018, 2, 5, 10, 5, 13, 680_000_000)));

        final List<List<LinkingItemPass<StopItem>>> stopItemPasses = new ArrayList<>();

        for (LinkEvent link : links) {
            stopItemPasses.add(stopItemTime.link(link));
        }

        Assertions.assertTrue(stopItemPasses.get(0).isEmpty());
        Assertions.assertTrue(stopItemPasses.get(1).isEmpty());
        Assertions.assertTrue(stopItemPasses.get(2).isEmpty());
        Assertions.assertTrue(stopItemPasses.get(4).isEmpty());
    }

    @Test
    public void whenInvalidOrderOfLinksThenException() {
        final OffsetItemTime<StopItem> stopItemTime = new OffsetItemTime<>(1,
                                                                           defaultConfig,
                                                                           simpleRound.getStopItems(),
                                                                           null,
                                                                           new OffsetItemTimeState(), new FacadeState());

        Assertions.assertThrows(Exception.class,
                                () -> stopItemTime.link(makeLinkEvent(LinkEvent.LinkType.ROUND_BEGIN,
                                                                      simpleRound,
                                                                      89,
                                                                      LocalDateTime.of(2018, 2, 5, 10, 0, 0))));
        Assertions.assertThrows(Exception.class,
                                () -> stopItemTime.link(makeLinkEvent(LinkEvent.LinkType.LINKED,
                                                                      simpleRound,
                                                                      89,
                                                                      LocalDateTime.of(2018, 2, 5, 10, 0, 0))));
    }

    @Test
    public void whenGoodLinksThenGoodSectionTimes() {
        final OffsetItemTime<GraphSection> graphSectionTime = new OffsetItemTime<>(1,
                                                                                   defaultConfig,
                                                                                   simpleRound.getGraphSections(),
                                                                                   null,
                                                                                   new OffsetItemTimeState(), new FacadeState());

        List<LinkEvent> links = new ArrayList<>();
        links.add(makeLinkEvent(LinkEvent.LinkType.WAITING_FOR_LINKING_START,
                                simpleRound,
                                0,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 0)));
        links.add(makeLinkEvent(LinkEvent.LinkType.ROUND_BEGIN,
                                simpleRound,
                                133,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 11, 367_000_000)));
        links.add(makeLinkEvent(LinkEvent.LinkType.LINKED,
                                simpleRound,
                                340,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 52, 767_000_000)));
        links.add(makeLinkEvent(LinkEvent.LinkType.LINKED,
                                simpleRound,
                                500,
                                LocalDateTime.of(2018, 2, 5, 10, 1, 8, 214_000_000)));
        links.add(makeLinkEvent(LinkEvent.LinkType.LINKED,
                                simpleRound,
                                732,
                                LocalDateTime.of(2018, 2, 5, 10, 1, 36, 292_000_000)));

        final List<List<LinkingItemPass<GraphSection>>> graphSectionPasses = new ArrayList<>();

        for (LinkEvent link : links) {
            graphSectionPasses.add(graphSectionTime.link(link));
        }

        Assertions.assertTrue(graphSectionPasses.get(0).isEmpty());
        Assertions.assertTrue(passMatch(graphSectionPasses.get(1).get(0),
                                        LocalDateTime.of(2018, 2, 5, 9, 59, 57, 496_000_000),
                                        LocalDateTime.of(2018, 2, 5, 10, 0, 5, 154_000_000)));
        Assertions.assertTrue(passMatch(graphSectionPasses.get(1).get(1),
                                        LocalDateTime.of(2018, 2, 5, 10, 0, 5, 154_000_000),
                                        LocalDateTime.of(2018, 2, 5, 10, 0, 7, 188_000_000)));
        Assertions.assertTrue(passMatch(graphSectionPasses.get(2).get(0),
                                        LocalDateTime.of(2018, 2, 5, 10, 0, 1, 605_000_000),
                                        LocalDateTime.of(2018, 2, 5, 10, 0, 45, 447_000_000)));
        Assertions.assertTrue(passMatch(graphSectionPasses.get(2).get(1),
                                        LocalDateTime.of(2018, 2, 5, 10, 0, 45, 447_000_000),
                                        LocalDateTime.of(2018, 2, 5, 10, 0, 50, 667_000_000)));
        Assertions.assertTrue(graphSectionPasses.get(3).isEmpty());
        Assertions.assertTrue(passMatch(graphSectionPasses.get(4).get(0),
                                        LocalDateTime.of(2018, 2, 5, 10, 0, 47, 589_000_000),
                                        LocalDateTime.of(2018, 2, 5, 10, 1, 15, 475_000_000)));
        Assertions.assertTrue(passMatch(graphSectionPasses.get(4).get(1),
                                        LocalDateTime.of(2018, 2, 5, 10, 1, 15, 475_000_000),
                                        LocalDateTime.of(2018, 2, 5, 10, 1, 21, 866_000_000)));
    }

    @Test
    public void whenStartAndNoPrevSectionThenTimeIsFarAway() {
        final OffsetItemTime<GraphSection> graphSectionTime = new OffsetItemTime<>(1,
                                                                                   defaultConfig,
                                                                                   simpleRound.getGraphSections(),
                                                                                   null,
                                                                                   new OffsetItemTimeState(), new FacadeState());

        List<LinkEvent> links = new ArrayList<>();
        links.add(makeLinkEvent(LinkEvent.LinkType.WAITING_FOR_LINKING_START,
                                simpleRound,
                                40,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 7, 333_000_000)));
        links.add(makeLinkEvent(LinkEvent.LinkType.ROUND_BEGIN,
                                simpleRound,
                                75,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 19)));

        final List<List<LinkingItemPass<GraphSection>>> graphSectionPasses = new ArrayList<>();

        for (LinkEvent link : links) {
            graphSectionPasses.add(graphSectionTime.link(link));
        }

        Assertions.assertTrue(graphSectionPasses.get(0).isEmpty());
        Assertions.assertTrue(passMatch(graphSectionPasses.get(1).get(0),
                                        LocalDateTime.of(2018, 2, 5, 9, 59, 44, 233_000_000),
                                        LocalDateTime.of(2018, 2, 5, 10, 0, 14)));
    }

    @Test
    public void whenStartAndPrevSectionThenTimeSectionIsNotBehind() {
        final OffsetItemTime<GraphSection> graphSectionTime = new OffsetItemTime<>(1,
                                                                                   defaultConfig,
                                                                                   simpleRound.getGraphSections(),
                                                                                   LocalDateTime.of(2018, 2, 5, 10, 0, 2, 680_000_000).toInstant(ZoneOffset.UTC),
                                                                                   new OffsetItemTimeState(), new FacadeState());

        List<LinkEvent> links = new ArrayList<>();
        links.add(makeLinkEvent(LinkEvent.LinkType.WAITING_FOR_LINKING_START,
                                simpleRound,
                                40,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 7, 333_000_000)));
        links.add(makeLinkEvent(LinkEvent.LinkType.ROUND_BEGIN,
                                simpleRound,
                                75,
                                LocalDateTime.of(2018, 2, 5, 10, 0, 19)));

        final List<List<LinkingItemPass<GraphSection>>> graphSectionPasses = new ArrayList<>();

        for (LinkEvent link : links) {
            graphSectionPasses.add(graphSectionTime.link(link));
        }

        Assertions.assertTrue(graphSectionPasses.get(0).isEmpty());
        Assertions.assertTrue(passMatch(graphSectionPasses.get(1).get(0),
                                        LocalDateTime.of(2018, 2, 5, 10, 0, 2, 680_000_000),
                                        LocalDateTime.of(2018, 2, 5, 10, 0, 16, 600_000_000)));
    }

    private <T extends LinkingItem> boolean passMatch(LinkingItemPass<T> pass,
                                                      LocalDateTime passBegin) {
        return passMatch(pass, passBegin, passBegin);
    }

    private <T extends LinkingItem> boolean passMatch(LinkingItemPass<T> pass,
                                                      LocalDateTime passBegin,
                                                      LocalDateTime passEnd) {

        return Duration.between(pass.getPassBeginTime(), passBegin.toInstant(ZoneOffset.UTC)).abs()
                .compareTo(Duration.of(1, ChronoUnit.SECONDS)) <= 0
                &&
                Duration.between(pass.getPassEndTime(), passEnd.toInstant(ZoneOffset.UTC)).abs()
                        .compareTo(Duration.of(1, ChronoUnit.SECONDS)) <= 0;
    }

    private LinkEvent makeLinkEvent(LinkEvent.LinkType linkType,
                                    Round round,
                                    double roundOffset,
                                    LocalDateTime eventTime) {

        for (GraphSection graphSection : round.getGraphSections()) {
            if (roundOffset >= graphSection.getRoundOffset() && roundOffset <= graphSection.getRoundOffset() + graphSection.getLength()) {
                return new LinkEvent(1,
                                     round.getRoundId(),
                                     linkType,
                                     new SectionLink(graphSection,
                                                     roundOffset - graphSection.getRoundOffset(),
                                                     roundOffset),
                                     new SimpleUnitPacket(1,
                                                          1,
                                                          1,
                                                          eventTime.toInstant(ZoneOffset.UTC),
                                                          true,
                                                          0f,
                                                          0f,
                                                          0));
            }
        }

        throw new IllegalArgumentException();
    }
}
