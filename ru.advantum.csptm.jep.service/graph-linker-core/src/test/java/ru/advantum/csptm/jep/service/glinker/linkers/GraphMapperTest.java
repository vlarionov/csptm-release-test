package ru.advantum.csptm.jep.service.glinker.linkers;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import ru.advantum.csptm.jep.service.glinker.db.GraphMapper;
import ru.advantum.csptm.jep.service.glinker.model.Timetable;
import ru.advantum.csptm.spring.rmq.ConsumerProperties;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {GraphMapperTest.Config.class})
@ActiveProfiles("test")
public class GraphMapperTest {

    @Autowired
    private GraphMapper graphMapper;
   
    @Test
    public void testGraphMapper(){
        List<Timetable> timetables = graphMapper.getUpdatedTimetables(new Integer[]{1,2}
                , new Integer[]{1,2}
                , new Integer[]{1,2});
        Assert.assertNotNull(timetables);
    }

    @Configuration
    @ComponentScan(basePackages = "ru.advantum.csptm.jep.service.glinker")
    @MapperScan(basePackages = "ru.advantum.csptm.jep.service.glinker")
    @EnableAutoConfiguration
    @Profile("test")
    public static class Config {

        @Bean
        public ConsumerProperties consumerProperties() {
            return ConsumerProperties.newBuilder()
                    .setShutdownTimeout(TimeUnit.SECONDS.toMillis(10))
                    .build();
        }
    }


}
