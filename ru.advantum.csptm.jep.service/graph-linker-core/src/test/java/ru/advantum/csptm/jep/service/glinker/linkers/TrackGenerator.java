package ru.advantum.csptm.jep.service.glinker.linkers;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import org.opengis.referencing.operation.TransformException;
import ru.advantum.csptm.artifact.geotools.GeoTools;
import ru.advantum.csptm.jep.service.glinker.linkers.util.LinkerUtils;
import ru.advantum.csptm.jep.service.glinker.model.GraphSection;
import ru.advantum.csptm.jep.service.glinker.model.Round;
import ru.advantum.csptm.jep.service.glinker.model.event.SimpleUnitPacket;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class TrackGenerator {

    private static final Random RANDOM = new Random(System.currentTimeMillis());

    private final Round round;
    private final Instant beginTime;
    private final double speed;
    private final long trId;
    private final long unitId;
    private final double firstSectionOffset;
    private final List<SimpleUnitPacket> track = new ArrayList<>();

    private long lastPacketId = 0;

    public TrackGenerator(Round round, Instant beginTime, double speed, long trId, long unitId) {
        this.round = round;
        this.beginTime = beginTime;
        this.speed = speed;
        this.trId = trId;
        this.unitId = unitId;
        this.firstSectionOffset = round.getGraphSections().get(0).getRoundOffset();
    }

    private List<SimpleUnitPacket> generateRandomPoints(GraphSection graphSection,
                                                        int pointCnt) throws TransformException {
        double[] indexes = new double[pointCnt];

        for (int i = 0; i < indexes.length; i++) {
            indexes[i] = RANDOM.nextDouble() * graphSection.getLength();
        }

        Arrays.sort(indexes);

        List<SimpleUnitPacket> result = new ArrayList<>();

        for (double index : indexes) {
            result.add(generatePacket(graphSection, index));
        }
        return result;
    }

    private SimpleUnitPacket generatePacket(GraphSection graphSection,
                                            double metersFromSectionBegin) throws TransformException {
        final Instant sectionBeginTime = LinkerUtils.travelEndTime(beginTime,
                                                                   speed,
                                                                   firstSectionOffset - graphSection.getRoundOffset());
        final Coordinate pointOnSection = graphSection.getIndexedLine().extractPoint(metersFromSectionBegin,
                                                                                     (RANDOM.nextDouble() - 0.5) * 50);
        final Geometry geoPoint = GeoTools.INSTANCE.inverseTransform(GeoTools.INSTANCE.getGeometryFactory().createPoint(pointOnSection));
        return new SimpleUnitPacket(++lastPacketId,
                                    trId,
                                    unitId,
                                    sectionBeginTime.plusMillis((long) (metersFromSectionBegin / speed * 1000)),
                                    true,
                                    (float) geoPoint.getCoordinate().x,
                                    (float) geoPoint.getCoordinate().y,
                                    (int) (speed * 3.6));
    }

    public TrackGenerator randomPointsOnSection(int graphSectionNum, int pointCount) throws TransformException {
        final GraphSection graphSection = round.getGraphSections().get(graphSectionNum - 1);

        track.addAll(generateRandomPoints(graphSection, pointCount));

        return this;
    }

    public TrackGenerator ratioPointOnSection(int graphSectionNum, double pointRatio) throws TransformException {
        final GraphSection graphSection = round.getGraphSections().get(graphSectionNum - 1);
        track.add(generatePacket(graphSection, pointRatio * graphSection.getLength()));

        return this;
    }

    public TrackGenerator pointOnRound(double roundOffset) throws TransformException {
        for (GraphSection graphSection : round.getGraphSections()) {
            if (roundOffset >= graphSection.getRoundOffset() && roundOffset <= graphSection.getRoundOffset() + graphSection.getLength()) {
                track.add(generatePacket(graphSection,
                                         roundOffset - graphSection.getRoundOffset()));
                break;
            }
        }


        return this;
    }

    public TrackGenerator pointRatioOnRound(double pointRatio) throws TransformException {
        return pointOnRound(pointRatio * round.getLength());
    }

    public List<SimpleUnitPacket> build() {
        return track;
    }
}
