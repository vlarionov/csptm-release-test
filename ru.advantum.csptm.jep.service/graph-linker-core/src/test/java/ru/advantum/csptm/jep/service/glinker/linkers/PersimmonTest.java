package ru.advantum.csptm.jep.service.glinker.linkers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.advantum.csptm.jep.service.glinker.config.PersimmonConfig;
import ru.advantum.csptm.jep.service.glinker.linkers.impl.Persimmon;
import ru.advantum.csptm.jep.service.glinker.model.Round;
import ru.advantum.csptm.jep.service.glinker.model.event.LinkEvent;
import ru.advantum.csptm.jep.service.glinker.model.event.SimpleUnitPacket;
import ru.advantum.csptm.jep.service.glinker.model.state.PersimmonState;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class PersimmonTest {

    private final Round simpleRound = JsonLoader.loadRound("src/test/resources/round_36011_simple.json");
    //    private final Round simpleReverseRound = JsonLoader.loadRound("src/test/resources/round_36012_simple_reverse.json");
    private final Round loopRound = JsonLoader.loadRound("src/test/resources/round_32370_loop.json");
    private final Round longRound = JsonLoader.loadRound("src/test/resources/round_67358_long.json");
    private final PersimmonConfig defaultConfig = new PersimmonConfig() {
    };
    //
    //    @Test
    //    public void datagen() throws TransformException {
    //        final List<SimpleUnitPacket> track1 = new TrackGenerator(simpleRound, Instant.now(), 5, 1, 1)
    ////                .pointRatioOnRound(0.1)
    //                .pointRatioOnRound(0.29)
    //                .pointRatioOnRound(0.79)
    ////                .pointRatioOnRound(0.4)
    ////                .pointRatioOnRound(0.5)
    ////                .pointRatioOnRound(0.6)
    ////                .pointRatioOnRound(0.7)
    ////                .pointRatioOnRound(0.8)
    ////                .pointRatioOnRound(0.9)
    //                //                .pointRatioOnRound(0.95)
    ////                .pointRatioOnRound(0.97)
    ////                .pointRatioOnRound(0.97)
    ////                .pointRatioOnRound(0.985)
    ////                .pointRatioOnRound(0.985)
    ////                .pointOnRound(longRound.getLength() - 310)
    ////                .pointOnRound(longRound.getLength() - 150)
    ////                .pointOnRound(longRound.getLength() - 150)
    //                .build();
    //        //
    //                final List<SimpleUnitPacket> track = JsonLoader.loadTrack("src/test/resources/track_36011_invalid_offset.json");
    //
    //        for (SimpleUnitPacket simpleUnitPacket : track) {
    //            System.out.println(String.format("track_geoms.append('POINT(%2$f %3$f)') # %1$s", simpleUnitPacket.getEventTime(), simpleUnitPacket.getLon(), simpleUnitPacket.getLat()));
    //        }
    ////
    ////                JsonLoader.saveTrack(track, "src/test/resources/track_36011_invalid_offset.json");
    //    }

    @Test
    public void whenGoodTrackThenGoodLinks() {
        Persimmon persimmon = new Persimmon(new PersimmonState(), defaultConfig, 1, simpleRound);
        List<SimpleUnitPacket> track = JsonLoader.loadTrack("src/test/resources/track_36011_good.json");
        List<LinkEvent> links = new ArrayList<>();
        for (SimpleUnitPacket packet : track) {
            links.add(persimmon.link(packet));
        }

        final int[] calculatedSectionLinks = links.stream().mapToInt(l -> l.getSectionLink().getGraphSectionOrderNum()).toArray();
        final int[] expectedSectionLinks = new int[]{1, 1, 4, 4, 5, 5, 5, 5, 7, 7, 7, 7, 9, 9, 9, 9};

        List<LinkEvent.LinkType> expectedTypes = new ArrayList<>(links.size());
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.WAITING_FOR_LINKING_START));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.ROUND_BEGIN));
        expectedTypes.addAll(Collections.nCopies(14, LinkEvent.LinkType.LINKED));

        Assertions.assertEquals(expectedTypes,
                                links.stream().map(LinkEvent::getType).collect(Collectors.toList()));

        Assertions.assertArrayEquals(expectedSectionLinks,
                                     calculatedSectionLinks,
                                     "Incorrect section order numbers");
    }

    @Test
    public void whenTooFarFromRoundThenBadLinks() {
        Persimmon persimmon = new Persimmon(new PersimmonState(), defaultConfig, 1, simpleRound);
        List<SimpleUnitPacket> track = JsonLoader.loadTrack("src/test/resources/track_36011_good_from_reverse.json");
        List<LinkEvent> links = new ArrayList<>();
        for (SimpleUnitPacket packet : track) {
            links.add(persimmon.link(packet));
        }

        List<LinkEvent.LinkType> expectedTypes = new ArrayList<>(links.size());
        expectedTypes.addAll(Collections.nCopies(3, LinkEvent.LinkType.TOO_FAR_FROM_ROUND));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.WAITING_FOR_LINKING_START));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.ROUND_BEGIN));
        expectedTypes.addAll(Collections.nCopies(11, LinkEvent.LinkType.LINKED));

        Assertions.assertEquals(expectedTypes,
                                links.stream().map(LinkEvent::getType).collect(Collectors.toList()));
    }

    @Test
    public void whenGoodLoopedTrackThenGoodLinks() {
        Persimmon persimmon = new Persimmon(new PersimmonState(), defaultConfig, 1, loopRound);
        List<SimpleUnitPacket> track = JsonLoader.loadTrack("src/test/resources/track_32370_good.json");
        List<LinkEvent> links = new ArrayList<>();
        for (SimpleUnitPacket packet : track) {
            links.add(persimmon.link(packet));
        }

        final int[] calculatedSectionLinks = links.stream().mapToInt(l -> l.getSectionLink().getGraphSectionOrderNum()).toArray();
        final int[] expectedSectionLinks = new int[]{1, 1, 2, 3, 3, 3, 5, 5, 5, 5, 9, 9, 9, 9, 9, 11, 11, 13, 13, 13, 13, 19, 19, 19, 19, 22, 22, 22, 22, 23, 23, 28, 28, 29, 30, 31, 32, 33};

        List<LinkEvent.LinkType> expectedTypes = new ArrayList<>(links.size());
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.WAITING_FOR_LINKING_START));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.ROUND_BEGIN));
        expectedTypes.addAll(Collections.nCopies(35, LinkEvent.LinkType.LINKED));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.ROUND_END));

        Assertions.assertEquals(expectedTypes,
                                links.stream().map(LinkEvent::getType).collect(Collectors.toList()));

        Assertions.assertArrayEquals(expectedSectionLinks,
                                     calculatedSectionLinks,
                                     "Incorrect section order numbers");
    }

    @Test
    public void whenGoodStartThenLastLinkIsRoundBegin() {
        Persimmon persimmon = new Persimmon(new PersimmonState(), defaultConfig, 1, simpleRound);
        List<SimpleUnitPacket> track = JsonLoader.loadTrack("src/test/resources/track_36011_start.json");
        List<LinkEvent> links = new ArrayList<>();
        for (SimpleUnitPacket packet : track) {
            links.add(persimmon.link(packet));
        }

        List<LinkEvent.LinkType> expectedTypes = new ArrayList<>(links.size());
        expectedTypes.addAll(Collections.nCopies(4, LinkEvent.LinkType.WAITING_FOR_LINKING_START));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.ROUND_BEGIN));

        Assertions.assertEquals(expectedTypes,
                                links.stream().map(LinkEvent::getType).collect(Collectors.toList()));
    }

    @Test
    public void whenOldPacketsThenBadLink() {
        Persimmon persimmon = new Persimmon(new PersimmonState(), defaultConfig, 1, simpleRound);
        List<SimpleUnitPacket> track = JsonLoader.loadTrack("src/test/resources/track_36011_old.json");
        List<LinkEvent> links = new ArrayList<>();
        for (SimpleUnitPacket packet : track) {
            links.add(persimmon.link(packet));
        }

        List<LinkEvent.LinkType> expectedTypes = new ArrayList<>(links.size());
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.WAITING_FOR_LINKING_START));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.ROUND_BEGIN));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.LINKED));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.INVALID_ORDER_TIME));
        expectedTypes.addAll(Collections.nCopies(3, LinkEvent.LinkType.LINKED));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.INVALID_ORDER_TIME));
        expectedTypes.addAll(Collections.nCopies(6, LinkEvent.LinkType.LINKED));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.INVALID_ORDER_TIME));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.LINKED));

        Assertions.assertEquals(expectedTypes,
                                links.stream().map(LinkEvent::getType).collect(Collectors.toList()));
    }

    @Test
    public void whenOutlierOrTooLargeSpeedThenBadLink() {
        Persimmon persimmon = new Persimmon(new PersimmonState(), defaultConfig, 1, simpleRound);
        List<SimpleUnitPacket> track = JsonLoader.loadTrack("src/test/resources/track_36011_outlier.json");
        List<LinkEvent> links = new ArrayList<>();
        for (SimpleUnitPacket packet : track) {
            links.add(persimmon.link(packet));
        }

        List<LinkEvent.LinkType> expectedTypes = new ArrayList<>(links.size());
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.WAITING_FOR_LINKING_START));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.ROUND_BEGIN));
        expectedTypes.addAll(Collections.nCopies(6, LinkEvent.LinkType.LINKED));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.TOO_FAR_FROM_GRAPH_SECTIONS));
        expectedTypes.addAll(Collections.nCopies(3, LinkEvent.LinkType.LINKED));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.TOO_HIGH_SPEED));
        expectedTypes.addAll(Collections.nCopies(3, LinkEvent.LinkType.LINKED));

        Assertions.assertEquals(expectedTypes,
                                links.stream().map(LinkEvent::getType).collect(Collectors.toList()));
    }

    @Test
    public void whenInvalidOffsetThenBadLinks() {
        Persimmon persimmon = new Persimmon(new PersimmonState(), defaultConfig, 1, simpleRound);
        List<SimpleUnitPacket> track = JsonLoader.loadTrack("src/test/resources/track_36011_invalid_offset.json");
        List<LinkEvent> links = new ArrayList<>();
        for (SimpleUnitPacket packet : track) {
            links.add(persimmon.link(packet));
        }

        List<LinkEvent.LinkType> expectedTypes = new ArrayList<>(links.size());
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.WAITING_FOR_LINKING_START));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.ROUND_BEGIN));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.LINKED));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.INVALID_ORDER_OFFSET));
        expectedTypes.addAll(Collections.nCopies(4, LinkEvent.LinkType.LINKED));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.INVALID_ORDER_OFFSET));

        Assertions.assertEquals(expectedTypes,
                                links.stream().map(LinkEvent::getType).collect(Collectors.toList()));
    }

    @Test
    public void whenInvalidCoordThenBadLink() {
        Persimmon persimmon = new Persimmon(new PersimmonState(), defaultConfig, 1, simpleRound);
        List<SimpleUnitPacket> track = JsonLoader.loadTrack("src/test/resources/track_36011_invalid.json");
        List<LinkEvent> links = new ArrayList<>();
        for (SimpleUnitPacket packet : track) {
            links.add(persimmon.link(packet));
        }

        List<LinkEvent.LinkType> expectedTypes = new ArrayList<>(links.size());
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.WAITING_FOR_LINKING_START));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.ROUND_BEGIN));
        expectedTypes.addAll(Collections.nCopies(7, LinkEvent.LinkType.LINKED));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.INVALID_COORDINATE));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.LINKED));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.INVALID_COORDINATE));
        expectedTypes.addAll(Collections.nCopies(4, LinkEvent.LinkType.LINKED));

        Assertions.assertEquals(expectedTypes,
                                links.stream().map(LinkEvent::getType).collect(Collectors.toList()));
    }

    @Test
    public void whenTooLargeSpeedOnSectionThenBadLink() {
        Persimmon persimmon = new Persimmon(new PersimmonState(), defaultConfig, 1, loopRound);
        List<SimpleUnitPacket> track = JsonLoader.loadTrack("src/test/resources/track_32370_too_fast_section.json");
        List<LinkEvent> links = new ArrayList<>();
        for (SimpleUnitPacket packet : track) {
            links.add(persimmon.link(packet));
        }

        List<LinkEvent.LinkType> expectedTypes = new ArrayList<>(links.size());
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.WAITING_FOR_LINKING_START));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.ROUND_BEGIN));
        expectedTypes.addAll(Collections.nCopies(6, LinkEvent.LinkType.LINKED));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.TOO_HIGH_SPEED));

        Assertions.assertEquals(expectedTypes,
                                links.stream().map(LinkEvent::getType).collect(Collectors.toList()));
    }

    @Test
    public void whenStateSaveAndStateLoadThenResultIsTheSame() {
        List<SimpleUnitPacket> track = JsonLoader.loadTrack("src/test/resources/track_36011_good.json");

        Persimmon persimmon = new Persimmon(new PersimmonState(), defaultConfig, 1, simpleRound);
        List<LinkEvent> linksContinuous = new ArrayList<>();
        for (SimpleUnitPacket packet : track) {
            linksContinuous.add(persimmon.link(packet));
        }

        Persimmon persimmonFirstHalf = new Persimmon(new PersimmonState(), defaultConfig, 1, simpleRound);
        List<LinkEvent> linksSaveRestore = new ArrayList<>();
        for (SimpleUnitPacket packet : track.subList(0, 10)) {
            linksSaveRestore.add(persimmonFirstHalf.link(packet));
        }
        final PersimmonState firstHalfState = persimmonFirstHalf.getState();
        Persimmon persimmonSecondHalf = new Persimmon(firstHalfState, defaultConfig, 1, simpleRound);
        for (SimpleUnitPacket packet : track.subList(10, track.size())) {
            linksSaveRestore.add(persimmonSecondHalf.link(packet));
        }

        Assertions.assertEquals(linksContinuous, linksSaveRestore,
                                "State restore shouldn't change the outcome of linking");
    }

    @Test
    public void whenStopAndRoundEndThenGoodRoundEndLink() {
        Persimmon persimmon = new Persimmon(new PersimmonState(), defaultConfig, 1, simpleRound);
        List<SimpleUnitPacket> track = JsonLoader.loadTrack("src/test/resources/track_36011_end.json");
        List<LinkEvent> links = new ArrayList<>();
        for (SimpleUnitPacket packet : track) {
            links.add(persimmon.link(packet));
        }

        List<LinkEvent.LinkType> expectedTypes = new ArrayList<>(links.size());
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.WAITING_FOR_LINKING_START));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.ROUND_BEGIN));
        expectedTypes.addAll(Collections.nCopies(28, LinkEvent.LinkType.LINKED));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.ROUND_END));

        Assertions.assertEquals(expectedTypes,
                                links.stream().map(LinkEvent::getType).collect(Collectors.toList()));
    }

    @Test
    public void whenStopBeforeEndThenNoRoundEnd() {
        Persimmon persimmon = new Persimmon(new PersimmonState(), defaultConfig, 1, loopRound);
        List<SimpleUnitPacket> track = JsonLoader.loadTrack("src/test/resources/track_32370_end_precise.json");
        List<LinkEvent> links = new ArrayList<>();
        for (SimpleUnitPacket packet : track) {
            links.add(persimmon.link(packet));
        }

        List<LinkEvent.LinkType> expectedTypes = new ArrayList<>(links.size());
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.WAITING_FOR_LINKING_START));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.ROUND_BEGIN));
        expectedTypes.addAll(Collections.nCopies(5, LinkEvent.LinkType.LINKED));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.ROUND_END));

        Assertions.assertEquals(expectedTypes,
                                links.stream().map(LinkEvent::getType).collect(Collectors.toList()));
    }

    @Test
    public void whenStartAtTheEndThenEndError() {
        Persimmon persimmon = new Persimmon(new PersimmonState(), defaultConfig, 1, simpleRound);
        List<SimpleUnitPacket> track = JsonLoader.loadTrack("src/test/resources/track_36011_end_error.json");
        List<LinkEvent> links = new ArrayList<>();
        for (SimpleUnitPacket packet : track) {
            links.add(persimmon.link(packet));
        }

        List<LinkEvent.LinkType> expectedTypes = new ArrayList<>(links.size());
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.ROUND_END_ERROR));

        Assertions.assertEquals(expectedTypes,
                                links.stream().map(LinkEvent::getType).collect(Collectors.toList()));
    }

    @Test
    public void whenStartMoveAndEndThenEndError() {
        Persimmon persimmon = new Persimmon(new PersimmonState(), defaultConfig, 1, simpleRound);
        List<SimpleUnitPacket> track = JsonLoader.loadTrack("src/test/resources/track_36011_start_end_error.json");
        List<LinkEvent> links = new ArrayList<>();
        for (SimpleUnitPacket packet : track) {
            links.add(persimmon.link(packet));
        }

        List<LinkEvent.LinkType> expectedTypes = new ArrayList<>(links.size());
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.WAITING_FOR_LINKING_START));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.ROUND_END_ERROR));

        Assertions.assertEquals(expectedTypes,
                                links.stream().map(LinkEvent::getType).collect(Collectors.toList()));
    }

    @Test
    public void whenStopBeforeLongEndThenNoRoundEnd() {
        Persimmon persimmon = new Persimmon(new PersimmonState(), defaultConfig, 1, longRound);
        List<SimpleUnitPacket> track = JsonLoader.loadTrack("src/test/resources/track_67358_end_precise.json");
        List<LinkEvent> links = new ArrayList<>();
        for (SimpleUnitPacket packet : track) {
            links.add(persimmon.link(packet));
        }

        List<LinkEvent.LinkType> expectedTypes = new ArrayList<>(links.size());
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.WAITING_FOR_LINKING_START));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.ROUND_BEGIN));
        expectedTypes.addAll(Collections.nCopies(8, LinkEvent.LinkType.LINKED));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.ROUND_END));

        Assertions.assertEquals(expectedTypes,
                                links.stream().map(LinkEvent::getType).collect(Collectors.toList()));
    }

    @Test
    public void whenGoBackAndRoundEndThenGoodRoundEndLink() {
        Persimmon persimmon = new Persimmon(new PersimmonState(), defaultConfig, 1, simpleRound);
        List<SimpleUnitPacket> track = JsonLoader.loadTrack("src/test/resources/track_36011_end_back.json");
        List<LinkEvent> links = new ArrayList<>();
        for (SimpleUnitPacket packet : track) {
            links.add(persimmon.link(packet));
        }

        List<LinkEvent.LinkType> expectedTypes = new ArrayList<>(links.size());
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.WAITING_FOR_LINKING_START));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.ROUND_BEGIN));
        expectedTypes.addAll(Collections.nCopies(27, LinkEvent.LinkType.LINKED));
        expectedTypes.addAll(Collections.nCopies(1, LinkEvent.LinkType.ROUND_END));

        Assertions.assertEquals(expectedTypes,
                                links.stream().map(LinkEvent::getType).collect(Collectors.toList()));
    }

}
