package ru.advantum.csptm.jep.service.glinker.linkers;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import com.google.common.collect.Lists;
import org.junit.Test;
import ru.advantum.csptm.jep.service.glinker.db.GraphMapper;
import ru.advantum.csptm.jep.service.glinker.model.StopItem;
import ru.advantum.csptm.jep.service.glinker.model.Timetable;
import ru.advantum.csptm.jep.service.glinker.model.dto.GraphSectionDTO;
import ru.advantum.csptm.jep.service.glinker.model.dto.NormBetweenStop;
import ru.advantum.csptm.jep.service.glinker.model.dto.RoundDTO;

import static org.junit.Assert.assertEquals;

public class NormProviderTest {

    @Test
    public void test(){
        GraphMapper mapper = new GraphMapper() {
            @Override
            public List<RoundDTO> getRounds(Long[] roundIds) {
                return null;
            }

            @Override
            public List<GraphSectionDTO> getGraphSections(Long[] roundIds) {
                return null;
            }

            @Override
            public List<StopItem> getStopItems(Long[] roundIds) {
                return null;
            }

            @Override
            public List<Timetable> getTimetables(LocalDate orderDate, Long trId, boolean onlyPlanned) {
                return null;
            }

            @Override
            public List<Timetable> getUpdatedTimetables(Integer[] ttVariantIds, Integer[] ttOutIds, Integer[] ttRoundIds) {
                return null;
            }

            @Override
            public List<NormBetweenStop> getNorms(long stop1, long stop2) {
                if(stop1 == 1 && stop2 == 2)
                    return Lists.newArrayList(
                        new NormBetweenStop(1, LocalTime.parse("00:00:00"), LocalTime.parse("06:00:00"), (short)1, 1, 1l),
                        new NormBetweenStop(1, LocalTime.parse("06:00:00"), LocalTime.parse("07:00:00"), (short)1, 2, 1l),
                        new NormBetweenStop(1, LocalTime.parse("07:00:00"), LocalTime.parse("08:00:00"), (short)1, 3, 1l),
                        new NormBetweenStop(1, LocalTime.parse("08:00:00"), LocalTime.parse("00:00:00"), (short)1, 4, 1l),

                        new NormBetweenStop(2, LocalTime.parse("07:00:00"), LocalTime.parse("08:00:00"), (short)1, 7, 1l),
                        new NormBetweenStop(1, LocalTime.parse("07:00:00"), LocalTime.parse("08:00:00"), (short)2, 7, 1l),
                        new NormBetweenStop(1, LocalTime.parse("07:00:00"), LocalTime.parse("08:00:00"), (short)1, 7, 2l)
                            );
                if(stop1 == 5 && stop2 == 6){
                    return Lists.newArrayList(
                            new NormBetweenStop(1, LocalTime.parse("00:00:00"), LocalTime.parse("06:00:00"), (short)1, 1, 1l),
                            new NormBetweenStop(1, LocalTime.parse("06:00:00"), LocalTime.parse("07:00:00"), (short)1, 2, 1l),
                            new NormBetweenStop(1, LocalTime.parse("07:00:00"), LocalTime.parse("08:00:00"), (short)1, 3, 1l),
                            new NormBetweenStop(1, LocalTime.parse("08:00:00"), LocalTime.parse("00:00:00"), (short)1, 4, 1l),

                            new NormBetweenStop(2, LocalTime.parse("07:00:00"), LocalTime.parse("08:00:00"), (short)1, 7, 1l),
                            new NormBetweenStop(1, LocalTime.parse("07:00:00"), LocalTime.parse("08:00:00"), (short)2, 7, 1l),
                            new NormBetweenStop(1, LocalTime.parse("07:00:00"), LocalTime.parse("08:00:00"), (short)1, 7, 2l)
                            );
                } else return Lists.newArrayList();
            }
        };
        NormProviderImpl p = new NormProviderImpl(mapper);
        assertEquals(new Integer(1), p.getNorm(1, 1,2,LocalTime.parse("00:00:00"),(short)1,1).get());
        assertEquals(new Integer(4), p.getNorm(1, 1,2,LocalTime.parse("23:59:59.99"),(short)1,1).get());


        assertEquals(new Integer(2), p.getNorm(1, 1,2,LocalTime.parse("06:59:59.99"),(short)1,1).get());
        assertEquals(new Integer(2), p.getNorm(1, 1,2,LocalTime.parse("06:00:00.001"),(short)1,1).get());
        assertEquals(new Integer(1), p.getNorm(1, 1,2,LocalTime.parse("05:30:00"),(short)1,1).get());
        assertEquals(new Integer(3), p.getNorm(1, 1,2,LocalTime.parse("07:30:00"),(short)1,1).get());
        assertEquals(new Integer(4), p.getNorm(1, 1,2,LocalTime.parse("08:30:00"),(short)1,1).get());

        assertEquals(false, p.getNorm(1, 1,2,LocalTime.parse("08:30:00"),(short)1,20).isPresent());
        assertEquals(false, p.getNorm(1, 1,2,LocalTime.parse("08:30:00"),(short)20,1).isPresent());
        assertEquals(false, p.getNorm(20, 1,2,LocalTime.parse("08:30:00"),(short)1,1).isPresent());
        assertEquals(new Integer(7), p.getNorm(2, 1,2,LocalTime.parse("07:30:00"),(short)1,1).get());
        assertEquals(new Integer(7), p.getNorm(1, 1,2,LocalTime.parse("07:30:00"),(short)2,1).get());
        assertEquals(new Integer(7), p.getNorm(1, 1,2,LocalTime.parse("07:30:00"),(short)1,2).get());

        assertEquals(false, p.getNorm(1, 1,2,LocalTime.parse("06:30:00"),(short)2,2).isPresent());
        assertEquals(false, p.getNorm(2, 1,2,LocalTime.parse("06:30:00"),(short)1,2).isPresent());
        assertEquals(false, p.getNorm(2, 1,2,LocalTime.parse("06:30:00"),(short)2,1).isPresent());


        assertEquals(false, p.getNorm(2, 2,2,LocalTime.parse("06:30:00"),(short)2,1).isPresent());
        assertEquals(false, p.getNorm(2, 1,1,LocalTime.parse("06:30:00"),(short)2,1).isPresent());

        assertEquals(new Integer(1), p.getNorm(1, 5,6,LocalTime.parse("05:30:00"),(short)1,1).get());

    }
}
