package ru.advantum.csptm.jep.service.glinker.linkers;

import com.fatboyindustrial.gsonjavatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Data;
import ru.advantum.csptm.jep.service.glinker.model.GraphSection;
import ru.advantum.csptm.jep.service.glinker.model.Round;
import ru.advantum.csptm.jep.service.glinker.model.StopItem;
import ru.advantum.csptm.jep.service.glinker.model.dto.GraphSectionDTO;
import ru.advantum.csptm.jep.service.glinker.model.dto.RoundDTO;
import ru.advantum.csptm.jep.service.glinker.model.event.SimpleUnitPacket;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class JsonLoader {

    private static final Gson GSON = Converters.registerAll(new GsonBuilder().setPrettyPrinting()).create();

    public static Round loadRound(String filename) {
        try (FileReader fr = new FileReader(filename)) {
            final RoundJson roundJson = GSON.fromJson(fr, RoundJson.class);
            return new Round(roundJson.round.getRoundId(),
                             roundJson.round.getLength(),
                             roundJson.round.getGraphSectionStartOffset(),
                             roundJson.round.getWktGeometry(),
                             roundJson.sections.stream().map(gsDTO -> new GraphSection(gsDTO.getGraphSectionId(),
                                                                                       gsDTO.getRoundId(),
                                                                                       gsDTO.getOrderNum(),
                                                                                       gsDTO.getLength(),
                                                                                       gsDTO.getRoundOffset(),
                                                                                       gsDTO.getWktGeometry()))
                                     .collect(Collectors.toList()),
                             roundJson.stops);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static List<SimpleUnitPacket> loadTrack(String filename) {
        try (FileReader fr = new FileReader(filename)) {
            final SimpleUnitPacket[] track = GSON.fromJson(fr, SimpleUnitPacket[].class);
            return Arrays.asList(track);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static void saveTrack(List<SimpleUnitPacket> track, String filename) {
        try (FileWriter writer = new FileWriter(filename)) {
            GSON.toJson(track, writer);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Data
    private static class RoundJson {

        private final RoundDTO round;
        private final List<StopItem> stops;
        private final List<GraphSectionDTO> sections;
    }
}
