package ru.advantum.csptm.service.audit.notifier;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.csptm.jep.spring.rmq.ConcurrentJepRmqServiceConfig;

@Root(name = "sys-event-notifier")
public class SysEventNotifierConfig {
    @Attribute
    public long checkEventsRate;

    @Element(name = "database")
    public DatabaseConnection databaseConfig;

    @Element(name = "rmq-service")
    public ConcurrentJepRmqServiceConfig rmqServiceConfig;
}
