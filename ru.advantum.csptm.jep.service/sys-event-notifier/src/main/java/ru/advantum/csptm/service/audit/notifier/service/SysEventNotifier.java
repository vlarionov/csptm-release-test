package ru.advantum.csptm.service.audit.notifier.service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.advantum.csptm.service.audit.notifier.db.SysEventNotificationMapper;
import ru.advantum.csptm.service.audit.notifier.model.SysEventMessage;
import ru.advantum.csptm.service.audit.notifier.model.SysEventNotification;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class SysEventNotifier {
    private static final Logger LOGGER = LoggerFactory.getLogger(SysEventNotifier.class);

    private static final String EMAIL_ROUTING_KEY = "email";
    private static final String PHONE_ROUTING_KEY = "phone";

    private final Gson gson = new Gson();

    private final SysEventNotificationMapper sysEventNotificationMapper;

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public SysEventNotifier(SysEventNotificationMapper sysEventNotificationMapper, RabbitTemplate rabbitTemplate) {
        this.sysEventNotificationMapper = sysEventNotificationMapper;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Scheduled(fixedRateString = "${sys-event-notifier.checkEventsRate}")
    public void checkEvents() {
        try {
            sysEventNotificationMapper.findNonNotified().forEach(this::notify);
        } catch (Exception e) {
            LOGGER.error("Error on notification of system event", e);
        }
    }

    private void notify(SysEventNotification notification) {
        List<String> messages = buildMessages(notification);

        if (notification.isEmailNotification() != null && notification.isEmailNotification() && notification.getEmail() != null) {
            messages.forEach(
                    message -> rabbitTemplate.convertAndSend(
                            EMAIL_ROUTING_KEY,
                            new SysEventMessage(notification.getEmail(), message)
                    )
            );

        }
        if (notification.isPhoneNotification() != null && notification.isPhoneNotification() && notification.getCellPhone() != null) {
            messages.forEach(
                    message -> rabbitTemplate.convertAndSend(
                            PHONE_ROUTING_KEY,
                            new SysEventMessage(notification.getCellPhone(), message)
                    )
            );
        }

        sysEventNotificationMapper.setNotified(notification.getSysEventId());
    }

    private List<String> buildMessages(SysEventNotification notification) {
        List<String> messages = new ArrayList<>();
        JsonArray attributesArray = gson.fromJson(notification.getEventAttributes(), JsonArray.class);
        for (int i = 0; i < attributesArray.size(); i++) {
            JsonElement attributesElement = attributesArray.get(i);
            if (attributesElement.isJsonObject()) {
                String message = notification.getMessageTemplate();
                for (Map.Entry<String, JsonElement> attribute : attributesElement.getAsJsonObject().entrySet()) {
                    message = message.replaceAll("\\$\\{" + attribute.getKey() +  "\\}", attribute.getValue().getAsString());
                }
                messages.add(message);
            }
        }
        return messages;
    }


}
