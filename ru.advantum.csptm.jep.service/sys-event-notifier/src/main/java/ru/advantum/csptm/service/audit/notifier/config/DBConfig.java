package ru.advantum.csptm.service.audit.notifier.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.service.audit.notifier.SysEventNotifierConfig;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
public class DBConfig {
    @Bean
    public DataSource dataSource(SysEventNotifierConfig config) throws SQLException {
        return config.databaseConfig.getConnectionPool();
    }

}
