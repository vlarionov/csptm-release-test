package ru.advantum.csptm.service.audit.notifier.config;

import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.jep.spring.rmq.RabbitConfigurerSupport;
import ru.advantum.csptm.service.audit.notifier.SysEventNotifierConfig;

@Configuration
public class RabbitConfig extends RabbitConfigurerSupport {
    protected RabbitConfig(SysEventNotifierConfig config) {
        super(config.rmqServiceConfig);
    }
}
