package ru.advantum.csptm.service.audit.notifier.db;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import ru.advantum.csptm.service.audit.notifier.model.SysEventNotification;

import java.util.List;

@Mapper
public interface SysEventNotificationMapper {
    @Select("SELECT * FROM adm.sys_audit_pkg$find_non_notified_events()")
    List<SysEventNotification> findNonNotified();

    @Update("SELECT * FROM adm.sys_audit_pkg$set_sys_event_notified(#{sysEventId})")
    void setNotified(Integer sysEventId);
}
