package ru.advantum.csptm.service.audit.notifier;


import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.advantum.csptm.jep.spring.properties.AbstractPropertySourceFactory;

import static ru.advantum.csptm.service.audit.notifier.Application.CONFIG_FILE_NAME;

@SpringBootApplication
@PropertySource(value = "", name = CONFIG_FILE_NAME, factory = Application.ConfigPropertySourceFactory.class)
@EnableScheduling
public class Application {
    public static final String CONFIG_FILE_NAME = "sys-event-notifier.xml";

    public static void main(String[] args) {
        new SpringApplicationBuilder(Application.class).run(args);
    }

    @Bean
    public static SysEventNotifierConfig applicationConfig() throws Exception {
        return ru.advantum.config.Configuration.unpackConfig(
                SysEventNotifierConfig.class,
                CONFIG_FILE_NAME
        );
    }

    public static class ConfigPropertySourceFactory extends AbstractPropertySourceFactory<SysEventNotifierConfig> {
        @Override
        public SysEventNotifierConfig getConfig() throws Exception {
            return applicationConfig();
        }
    }
}
