package ru.advantum.csptm.service.audit.notifier.model;

public class SysEventNotification {
    private final Integer sysEventId;

    private final String eventAttributes;

    private final String messageTemplate;

    private final String email;

    private final Boolean isEmailNotification;

    private final String cellPhone;

    private final Boolean isPhoneNotification;

    public SysEventNotification(Integer sysEventId, String eventAttributes, String messageTemplate, String email, Boolean isEmailNotification, String cellPhone, Boolean isPhoneNotification) {
        this.sysEventId = sysEventId;
        this.eventAttributes = eventAttributes;
        this.messageTemplate = messageTemplate;
        this.email = email;
        this.isEmailNotification = isEmailNotification;
        this.cellPhone = cellPhone;
        this.isPhoneNotification = isPhoneNotification;
    }

    public Integer getSysEventId() {
        return sysEventId;
    }

    public String getEventAttributes() {
        return eventAttributes;
    }

    public String getMessageTemplate() {
        return messageTemplate;
    }

    public String getEmail() {
        return email;
    }

    public Boolean isEmailNotification() {
        return isEmailNotification;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public Boolean isPhoneNotification() {
        return isPhoneNotification;
    }
}
