<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>ru.advantum.csptm.service</groupId>
    <artifactId>sys-event-notifier</artifactId>
    <version>1.0-SNAPSHOT</version>

    <description>Сервис оповещений пользователей о системных событиях</description>

    <developers>
        <developer>
            <id>larionov</id>
            <name>Larionov Vladislav</name>
            <email>larionov@advantum.pro</email>
        </developer>
    </developers>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <project.svn.root>svn://svn.advantum.ru/csptm</project.svn.root>

        <main.class>ru.advantum.csptm.service.audit.notifier.Application</main.class>

        <change-template.version>1.5</change-template.version>
        <jep-hub-spring.version>1.0-SNAPSHOT</jep-hub-spring.version>

        <spring-boot.version>1.4.3.RELEASE</spring-boot.version>
        <spring-boot-log4j.version>1.3.8.RELEASE</spring-boot-log4j.version>
        <mybatis-boot.version>1.2.0</mybatis-boot.version>
        <mybatis-typehandlers.version> 1.0.2</mybatis-typehandlers.version>
        <postdriver.version>9.4-1200-jdbc41</postdriver.version>
        <guava.version>18.0</guava.version>

    </properties>

    <scm>
        <developerConnection>scm:svn:${project.svn.root}/artifact-group/${project.groupId}/${project.artifactId}/</developerConnection>
    </scm>

    <dependencies>
        <dependency>
            <groupId>ru.advantum</groupId>
            <artifactId>change-template</artifactId>
            <version>${change-template.version}</version>
        </dependency>
        <dependency>
            <groupId>ru.advantum.csptm.jep</groupId>
            <artifactId>jep-hub-spring</artifactId>
            <version>${jep-hub-spring.version}</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter</artifactId>
            <version>${spring-boot.version}</version>
            <exclusions>
                <exclusion>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-starter-logging</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-log4j</artifactId>
            <version>${spring-boot-log4j.version}</version>
        </dependency>

        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
            <version>${mybatis-boot.version}</version>
        </dependency>
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis-typehandlers-jsr310</artifactId>
            <version>${mybatis-typehandlers.version}</version>
        </dependency>

        <dependency>
            <groupId>org.postgresql</groupId>
            <artifactId>postgresql</artifactId>
            <version>${postdriver.version}</version>
            <exclusions>
                <exclusion>
                    <groupId>org.slf4j</groupId>
                    <artifactId>slf4j-simple</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <version>${guava.version}</version>
        </dependency>
    </dependencies>
    <dependencyManagement>
        <dependencies>
            <dependency>
                <!-- Import dependency management from Spring Boot -->
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring-boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <version>${spring-boot.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>repackage</goal>
                        </goals>
                        <configuration>
                            <mainClass>${main.class}</mainClass>
                            <classifier>jar-with-dependencies</classifier>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <version>2.10</version>
                <executions>
                    <execution>
                        <id>copy</id>
                        <phase>package</phase>
                        <goals>
                            <goal>unpack</goal>
                        </goals>
                        <configuration>
                            <artifactItems>
                                <artifactItem>
                                    <groupId>ru.advantum</groupId>
                                    <artifactId>change-template</artifactId>
                                    <overWrite>true</overWrite>
                                    <outputDirectory>${basedir}/src/main/resources</outputDirectory>
                                    <destFileName>template.vm</destFileName>
                                    <includes>announcements/*.vm</includes>
                                </artifactItem>
                            </artifactItems>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-changes-plugin</artifactId>
                <version>2.11</version>
                <configuration>
                    <templateDirectory>announcements</templateDirectory>
                    <template>template.vm</template>

                    <templateEncoding>UTF-8</templateEncoding>
                    <outputEncoding>UTF-8</outputEncoding>
                    <announcementFile>announcement.vm</announcementFile>
                    <mailContentType>text/html; charset=utf-8</mailContentType>

                    <escapeHTML>true</escapeHTML>

                    <smtpHost>smtpdev.advantum.ru</smtpHost>
                    <smtpPort implementation="java.lang.Integer">25</smtpPort>

                    <toAddresses>
                        <toAddress>
                            dev@advantum.ru
                        </toAddress>
                    </toAddresses>
                </configuration>
                <executions>
                    <execution>
                        <id>check-changes</id>
                        <phase>deploy</phase>
                        <goals>
                            <goal>changes-check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>2.5.1</version>
                <configuration>
                    <tagBase>${project.svn.root}/artifact-tags/${project.groupId}/${project.artifactId}/
                    </tagBase>
                    <tagNameFormat>v@{project.version}</tagNameFormat>

                    <goals>deploy changes:announcement-mail</goals>
                    <checkModificationExcludes>${project.name}.iml</checkModificationExcludes>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>2.4</version>
            </plugin>
        </plugins>
    </build>

    <distributionManagement>
        <repository>
            <id>advantum</id>
            <url>http://nexus.advantum.ru/content/repositories/adv-public</url>
        </repository>
        <snapshotRepository>
            <id>advantum</id>
            <url>http://nexus.advantum.ru/content/repositories/adv-snapshot</url>
        </snapshotRepository>
    </distributionManagement>

</project>