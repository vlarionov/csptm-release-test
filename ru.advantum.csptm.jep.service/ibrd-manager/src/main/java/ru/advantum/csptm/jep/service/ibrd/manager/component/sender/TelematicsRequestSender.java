package ru.advantum.csptm.jep.service.ibrd.manager.component.sender;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.jep.service.ibrd.manager.component.IbrdCommander;
import ru.advantum.csptm.jep.service.ibrd.manager.component.IbrdEventBuilder;
import ru.advantum.csptm.jep.service.ibrd.manager.controller.IbrdSubscriber;
import ru.advantum.csptm.jep.service.ibrd.manager.controller.IbrdState;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdEventType;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdProtocol;
import ru.advantum.csptm.jep.service.ibrd.model.TelematicsRequest;

import java.time.Duration;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class TelematicsRequestSender implements IbrdSubscriber {

    private final IbrdCommander ibrdCommander;

    private final Set<Integer> ibrdIdsForRequest = ConcurrentHashMap.newKeySet();

    private final IbrdEventBuilder eventBuilder;

    @Autowired
    public TelematicsRequestSender(IbrdCommander ibrdCommander, IbrdEventBuilder eventBuilder) {
        this.ibrdCommander = ibrdCommander;
        this.eventBuilder = eventBuilder;
    }

    public void addRequest(TelematicsRequest request) {
        ibrdIdsForRequest.add(request.getIbrdId());
    }

    @Override
    public Duration getRate(IbrdProtocol protocol) {
        return null;
    }

    @Override
    public Set<IbrdProtocol> getSupportedProtocols() {
        return Collections.singleton(IbrdProtocol.ROW);
    }

    @Override
    public void executeTask(IbrdState ibrdState) {
        if (ibrdIdsForRequest.remove(ibrdState.getIbrdId())) {
            val event = eventBuilder.build(ibrdState.getIbrdId(), ibrdState.getChannelId(), IbrdEventType.TELEMATICS_REQUEST);
            ibrdCommander.sendCommandEvent(event);
        }
    }
}

