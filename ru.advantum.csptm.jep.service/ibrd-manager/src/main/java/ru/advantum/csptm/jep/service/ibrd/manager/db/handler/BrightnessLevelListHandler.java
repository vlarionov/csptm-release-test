package ru.advantum.csptm.jep.service.ibrd.manager.db.handler;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.postgresql.util.PGobject;
import ru.advantum.csptm.jep.service.ibrd.model.monorgb.BrightnessLevel;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class BrightnessLevelListHandler extends BaseTypeHandler<List<BrightnessLevel>> {
    private static final Gson gson = new Gson();

    private static final Type type = new TypeToken<List<BrightnessLevel>>() {
    }.getType();

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, List<BrightnessLevel> parameter, JdbcType jdbcType) throws SQLException {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<BrightnessLevel> getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return handle(rs.getObject(columnName));
    }

    @Override
    public List<BrightnessLevel> getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return handle(rs.getObject(columnIndex));
    }

    @Override
    public List<BrightnessLevel> getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return handle(cs.getObject(columnIndex));
    }

    private List<BrightnessLevel> handle(Object result) {
        return gson.fromJson(((PGobject) result).getValue(), type);
    }

}
