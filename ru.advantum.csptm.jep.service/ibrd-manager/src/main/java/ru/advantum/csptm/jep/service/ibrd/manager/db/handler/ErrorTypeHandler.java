package ru.advantum.csptm.jep.service.ibrd.manager.db.handler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdErrorType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ErrorTypeHandler extends BaseTypeHandler<IbrdErrorType> {
    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, IbrdErrorType parameter, JdbcType jdbcType) throws SQLException {
        ps.setInt(i, parameter.getId());
    }

    @Override
    public IbrdErrorType getNullableResult(ResultSet rs, String columnName) throws SQLException {
        throw new UnsupportedOperationException();
    }

    @Override
    public IbrdErrorType getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        throw new UnsupportedOperationException();
    }

    @Override
    public IbrdErrorType getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        throw new UnsupportedOperationException();
    }
}