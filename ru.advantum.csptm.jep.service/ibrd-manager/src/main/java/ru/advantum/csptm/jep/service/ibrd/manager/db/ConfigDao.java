package ru.advantum.csptm.jep.service.ibrd.manager.db;

import org.apache.ibatis.annotations.Arg;
import org.apache.ibatis.annotations.ConstructorArgs;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;
import ru.advantum.csptm.jep.service.ibrd.manager.db.handler.BrightnessLevelListHandler;
import ru.advantum.csptm.jep.service.ibrd.manager.db.handler.DurationHandler;
import ru.advantum.csptm.jep.service.ibrd.model.monorgb.MonoRGBConfig;
import ru.advantum.csptm.jep.service.ibrd.model.row.RowConfig;

import java.time.Duration;
import java.util.List;

@Repository
public class ConfigDao {

    private final ConfigMapper mapper;

    @Autowired
    public ConfigDao(ConfigMapper mapper) {
        this.mapper = mapper;
    }

    @Cacheable("config")
    public RowConfig findRowConfig(int ibrdId) {
        return mapper.findRowConfig(ibrdId);
    }

    @Cacheable("config")
    public MonoRGBConfig findMonoRGBConfig(int ibrdId) {
        return mapper.findMonoRGBConfig(ibrdId);
    }

    @Scheduled(fixedRate = 60_000L)
    @CacheEvict("config")
    public void evictCache() {
    }

    @Mapper
    private interface ConfigMapper {

        @Select("SELECT * FROM ibrd.config_pkg$find_row_config(#{infoBoardId})")
        @ConstructorArgs({
                @Arg(column = "is_display_temperature", javaType = boolean.class),
                @Arg(column = "running_row_speed", javaType = short.class),
                @Arg(column = "data_waiting_time", javaType = Duration.class, typeHandler = DurationHandler.class),
                @Arg(column = "max_forecast_interval", javaType = Duration.class, typeHandler = DurationHandler.class),
                @Arg(column = "is_limit_forecasts_by_rows", javaType = boolean.class),
                @Arg(column = "is_display_time", javaType = boolean.class),
                @Arg(column = "is_display_date", javaType = boolean.class),
                @Arg(column = "scroll_speed", javaType = short.class),
                @Arg(column = "block_rate", javaType = Duration.class, typeHandler = DurationHandler.class),
                @Arg(column = "server_waiting_time", javaType = Duration.class, typeHandler = DurationHandler.class),
                @Arg(column = "reboot_time", javaType = Duration.class, typeHandler = DurationHandler.class)
        })
        RowConfig findRowConfig(int ibrdId);

        @Select("SELECT * FROM ibrd.config_pkg$find_mono_rgb_config(#{infoBoardId})")
        @ConstructorArgs({
                @Arg(column = "is_display_temperature", javaType = boolean.class),
                @Arg(column = "running_row_speed", javaType = short.class),
                @Arg(column = "data_waiting_time", javaType = Duration.class, typeHandler = DurationHandler.class),
                @Arg(column = "max_forecast_interval", javaType = Duration.class, typeHandler = DurationHandler.class),
                @Arg(column = "is_limit_forecasts_by_rows", javaType = boolean.class),
                @Arg(column = "is_display_datetime", javaType = boolean.class),
                @Arg(column = "running_row_position_id", javaType = int.class),
                @Arg(column = "scroll_type_id", javaType = int.class),
                @Arg(column = "scroll_direction_id", javaType = int.class),
                @Arg(column = "rows_number", javaType = short.class),
                @Arg(column = "route_number_style_id", javaType = int.class),
                @Arg(column = "arrival_time_style_id", javaType = int.class),
                @Arg(column = "terminal_style_id", javaType = int.class),
                @Arg(column = "forecast_delay", javaType = Duration.class, typeHandler = DurationHandler.class),
                @Arg(column = "brightness_levels", javaType = List.class, typeHandler = BrightnessLevelListHandler.class)
        })
        MonoRGBConfig findMonoRGBConfig(int ibrdId);
    }
}
