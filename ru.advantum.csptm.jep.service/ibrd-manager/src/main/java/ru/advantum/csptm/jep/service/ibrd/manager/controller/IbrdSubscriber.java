package ru.advantum.csptm.jep.service.ibrd.manager.controller;

import com.google.common.collect.Sets;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdProtocol;

import java.time.Duration;
import java.util.Set;

public interface IbrdSubscriber {
    Duration getRate(IbrdProtocol protocol);

    void executeTask(IbrdState ibrdState);

    default Set<IbrdProtocol> getSupportedProtocols() {
        return Sets.newHashSet(IbrdProtocol.values());
    }

}
