package ru.advantum.csptm.jep.service.ibrd.manager.config;

import com.google.gson.Gson;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;
import ru.advantum.csptm.jep.proto.UnitPacketsBundle;
import ru.advantum.csptm.jep.service.glinker.model.event.Forecast;
import ru.advantum.csptm.jep.service.ibrd.manager.IbrdManagerConfig;
import ru.advantum.csptm.jep.service.ibrd.model.TelematicsRequest;
import ru.advantum.csptm.jep.service.ibrd.model.serializer.IbrdEventToPacketAdapter;
import ru.advantum.csptm.spring.rmq.ConsumerProperties;
import ru.advantum.csptm.spring.rmq.MessageTypeResolver;
import ru.advantum.csptm.spring.rmq.MultiTypeResolver;

@Configuration
public class RmqConfig {
    @Bean
    public JepRmqServiceConfig jepRmqServiceConfig(IbrdManagerConfig config) {
        return config.rmqServiceConfig;
    }

    @Bean
    public MessageTypeResolver messageTypeResolver() {
        return MultiTypeResolver.newBuilder()
                .addPair("COORDS", UnitPacketsBundle.class)
                .addPair("FORECASTS", Forecast[].class)
                .addPair(TelematicsRequest.RMQ_TYPE, TelematicsRequest.class)
                .build();
    }

    @Bean
    public ConsumerProperties consumerProperties() {
        return ConsumerProperties.newBuilder().setAcknowledgeMode(AcknowledgeMode.AUTO).build();
    }

    @Bean
    public IbrdEventToPacketAdapter ibrdEventToPacketAdapter(Gson gson) {
        return new IbrdEventToPacketAdapter(gson);
    }
}
