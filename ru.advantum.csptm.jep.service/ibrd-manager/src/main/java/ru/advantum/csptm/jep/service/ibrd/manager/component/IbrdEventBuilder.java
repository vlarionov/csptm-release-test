package ru.advantum.csptm.jep.service.ibrd.manager.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.advantum.csptm.dbidgen.DbIdGenerator;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdEvent;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdEventType;

import java.time.LocalDateTime;

@Component
public class IbrdEventBuilder {
    private final DbIdGenerator dbIdGenerator;

    @Autowired
    public IbrdEventBuilder(DbIdGenerator dbIdGenerator) {
        this.dbIdGenerator = dbIdGenerator;
    }

    public IbrdEvent build(int ibrdId, String channelId, IbrdEventType type) {
        IbrdEvent event = new IbrdEvent(LocalDateTime.now(), type, ibrdId, channelId);
        event.setEventId(dbIdGenerator.nextId());
        return event;
    }
}
