package ru.advantum.csptm.jep.service.ibrd.manager;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.csptm.dbidgen.DbIdGeneratorConfig;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;
import ru.advantum.rabbitmq.config.RmqDestinationConfig;

import java.time.Duration;

@Root(name = "ibrd-manager")
public class IbrdManagerConfig {

    @Attribute(name = "update-info-rate", required = false)
    public Long updateInfoRate = 30_000L;

    @Element(name = "rmq-service")
    public IbrdJepRmqServiceConfig rmqServiceConfig;

    @Element
    public DatabaseConnection database;

    @Element(name = "db-id-generator")
    public DbIdGeneratorConfig dbIdGeneratorConfig;

    public Duration getUpdateInfoRate() {
        return Duration.ofMillis(updateInfoRate);
    }

    private static class IbrdJepRmqServiceConfig extends JepRmqServiceConfig {
        @Element(name = "rmq-event-destination")
        public final RmqDestinationConfig eventDestination = null;

    }
}
