package ru.advantum.csptm.jep.service.ibrd.manager.component.sender;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.jep.service.ibrd.manager.component.IbrdCommander;
import ru.advantum.csptm.jep.service.ibrd.manager.component.IbrdEventBuilder;
import ru.advantum.csptm.jep.service.ibrd.manager.controller.IbrdSubscriber;
import ru.advantum.csptm.jep.service.ibrd.manager.controller.IbrdState;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdEventType;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdProtocol;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Service
@Slf4j
public class IbrdDatetimeSender implements IbrdSubscriber {

    private final IbrdCommander ibrdCommander;

    private final IbrdEventBuilder ibrdEventBuilder;

    @Autowired
    public IbrdDatetimeSender(IbrdCommander ibrdCommander, IbrdEventBuilder ibrdEventBuilder) {
        this.ibrdCommander = ibrdCommander;
        this.ibrdEventBuilder = ibrdEventBuilder;
    }

    @Override
    public Duration getRate(IbrdProtocol protocol) {
        return protocol == IbrdProtocol.ROW ? null : Duration.ofMinutes(30);
    }

    @Override
    public void executeTask(IbrdState ibrdState) {
        val event = ibrdEventBuilder.build(ibrdState.getIbrdId(), ibrdState.getChannelId(), IbrdEventType.SET_DATETIME);
        event.setBoardTime(LocalDateTime.ofInstant(Instant.now(), ZoneId.of("Europe/Moscow")));
        ibrdCommander.sendCommandEvent(event);
    }
}
