package ru.advantum.csptm.jep.service.ibrd.manager.db;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.StatementType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdProtocol;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository
public class IbrdDao {
    private final IbrdMapper mapper;

    @Autowired
    public IbrdDao(IbrdMapper mapper) {
        this.mapper = mapper;
    }

    @Cacheable("protocols")
    public IbrdProtocol findProtocol(int ibrdId) {
        Integer protocolId = mapper.findProtocolId(ibrdId);
        return protocolId != null ? IbrdProtocol.find(protocolId) : null;
    }

    @Cacheable("hubs")
    public Long findHubId(int ibrdId) {
        return mapper.findHubId(ibrdId);
    }

    @Mapper
    private interface IbrdMapper {
        @Select("SELECT * FROM ibrd.info_board_pkg$find_protocol_id(#{ibrdId})")
        @Options(useCache = false, statementType = StatementType.CALLABLE)
        Integer findProtocolId(int ibrdId);

        @Select("SELECT * FROM ibrd.info_board_pkg$find_hub_id(#{ibrdId})")
        @Options(useCache = false, statementType = StatementType.CALLABLE)
        Long findHubId(int ibrdId);

    }
}
