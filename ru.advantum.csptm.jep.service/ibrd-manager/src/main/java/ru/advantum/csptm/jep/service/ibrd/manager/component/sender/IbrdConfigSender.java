package ru.advantum.csptm.jep.service.ibrd.manager.component.sender;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.jep.service.ibrd.manager.component.IbrdCommander;
import ru.advantum.csptm.jep.service.ibrd.manager.component.IbrdEventBuilder;
import ru.advantum.csptm.jep.service.ibrd.manager.controller.IbrdSubscriber;
import ru.advantum.csptm.jep.service.ibrd.manager.controller.IbrdState;
import ru.advantum.csptm.jep.service.ibrd.manager.db.ConfigDao;
import ru.advantum.csptm.jep.service.ibrd.manager.db.IbrdDao;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdEventType;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdProtocol;

import java.time.Duration;

@Service
public class IbrdConfigSender implements IbrdSubscriber {

    private final IbrdDao ibrdDao;
    private final ConfigDao configDao;
    private final IbrdCommander ibrdCommander;
    private final IbrdEventBuilder eventBuilder;

    @Autowired
    public IbrdConfigSender(IbrdDao ibrdDao, ConfigDao configDao, IbrdCommander ibrdCommander, IbrdEventBuilder eventBuilder) {
        this.ibrdDao = ibrdDao;
        this.configDao = configDao;
        this.ibrdCommander = ibrdCommander;
        this.eventBuilder = eventBuilder;
    }


    @Override
    public Duration getRate(IbrdProtocol protocol) {
        return protocol == IbrdProtocol.ROW ? null : Duration.ofSeconds(30);
    }

    @Override
    public void executeTask(IbrdState ibrdState) {
        val ibrdId = ibrdState.getIbrdId();
        val protocol = ibrdDao.findProtocol(ibrdId);
        val event = eventBuilder.build(ibrdId, ibrdState.getChannelId(), IbrdEventType.SET_CONFIG);
        switch (protocol) {
            case ROW:
                val rowConfig = configDao.findRowConfig(ibrdId);
                event.setRowConfig(rowConfig);
                ibrdCommander.sendCommandEvent(event);
                break;
            case MONO:
            case RGB:
                val monoRGBConfig = configDao.findMonoRGBConfig(ibrdId);
                event.setMonoRGBConfig(monoRGBConfig);
                ibrdCommander.sendCommandEvent(event);
                break;
            default:
                throw new IllegalArgumentException("Unsupported protocol " + protocol);
        }
    }

}
