package ru.advantum.csptm.jep.service.ibrd.manager.db;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.StatementType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdMessage;

import java.util.List;

@Repository
public class MessageDao {

    private final MessageMapper mapper;

    @Autowired
    public MessageDao(MessageMapper mapper) {
        this.mapper = mapper;
    }

    public List<IbrdMessage> findCurrent(int ibrdId) {
        return mapper.find(ibrdId);
    }

    @Mapper
    private interface MessageMapper {
        @Select("SELECT * FROM ibrd.message_pkg$find(#{ibrdId})")
        @Options(useCache = false, statementType = StatementType.CALLABLE)
        List<IbrdMessage> find(@Param("ibrdId") int ibrdId);
    }

}
