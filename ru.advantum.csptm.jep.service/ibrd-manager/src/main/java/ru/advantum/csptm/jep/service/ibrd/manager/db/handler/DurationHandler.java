package ru.advantum.csptm.jep.service.ibrd.manager.db.handler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.postgresql.util.PGInterval;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.Period;

public class DurationHandler extends BaseTypeHandler<Duration> {
    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, Duration parameter, JdbcType jdbcType) throws SQLException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Duration getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return handle(rs.getObject(columnName));
    }

    @Override
    public Duration getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return handle(rs.getObject(columnIndex));
    }

    @Override
    public Duration getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return handle(cs.getObject(columnIndex));
    }

    private Duration handle(Object result) {
        PGInterval interval = (PGInterval) result;
        if (interval == null) {
            return null;
        }
        Period period = Period.ofYears(interval.getYears())
                .withMonths(interval.getMonths())
                .withDays(interval.getDays());
        return Duration.ofDays(period.getDays())
                .plusHours(interval.getHours())
                .plusMinutes(interval.getMinutes())
                .plusNanos((long) (1_000_000_000 * interval.getSeconds()));
    }

}
