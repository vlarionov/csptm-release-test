package ru.advantum.csptm.jep.service.ibrd.manager;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Application {
    public static final String CONFIG_FILE_NAME = "ibrd-manager.xml";

    public static void main(String[] args) {
        new SpringApplicationBuilder(Application.class)
                .web(false)
                .bannerMode(Banner.Mode.LOG)
                .run(args);
    }

    @Bean
    public static IbrdManagerConfig applicationConfig() throws Exception {
        return ru.advantum.config.Configuration.unpackConfig(
                IbrdManagerConfig.class,
                CONFIG_FILE_NAME
        );
    }

}
