package ru.advantum.csptm.jep.service.ibrd.manager.controller;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.advantum.csptm.dbidgen.DbIdGenerator;
import ru.advantum.csptm.jep.proto.UnitPacketsBundle;
import ru.advantum.csptm.jep.service.ibrd.manager.component.IbrdEventSaver;
import ru.advantum.csptm.jep.service.ibrd.manager.component.sender.TelematicsRequestSender;
import ru.advantum.csptm.jep.service.ibrd.manager.db.IbrdDao;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdEvent;
import ru.advantum.csptm.jep.service.ibrd.model.TelematicsRequest;
import ru.advantum.csptm.jep.service.ibrd.model.serializer.IbrdEventToPacketAdapter;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Component
@RabbitListener
@Slf4j
public class IbrdController {

    private final TelematicsRequestSender telematicsRequestSender;

    private final IbrdEventToPacketAdapter eventToPacketAdapter;

    private final DbIdGenerator dbIdGenerator;

    private final IbrdEventSaver ibrdEventSaver;

    private final List<IbrdSubscriber> ibrdSubscribers;

    private final IbrdDao ibrdDao;

    private final ConcurrentHashMap<Integer, IbrdState> ibrdIdToStateMap = new ConcurrentHashMap<>();

    @Autowired
    public IbrdController(TelematicsRequestSender telematicsRequestSender, IbrdEventToPacketAdapter eventToPacketAdapter, DbIdGenerator dbIdGenerator, IbrdEventSaver ibrdEventSaver, List<IbrdSubscriber> ibrdSubscribers, IbrdDao ibrdDao) {
        this.telematicsRequestSender = telematicsRequestSender;
        this.eventToPacketAdapter = eventToPacketAdapter;
        this.dbIdGenerator = dbIdGenerator;
        this.ibrdEventSaver = ibrdEventSaver;
        this.ibrdSubscribers = ibrdSubscribers;
        this.ibrdDao = ibrdDao;
    }

    @RabbitHandler
    public void receiveUnitPacketBundle(UnitPacketsBundle unitPacketsBundle) {
        unitPacketsBundle.getPackets().forEach(unitPacket -> {
            val event = eventToPacketAdapter.toIbrdEvent(unitPacket);
            event.setEventId(dbIdGenerator.nextId());
            log.info("Event received: " + event);
            ibrdEventSaver.sendToSave(event);
            receiveIbrdEvent(event);
        });
    }

    @RabbitHandler
    public void receiveTelematicsRequest(TelematicsRequest telematicsRequest) {
        telematicsRequestSender.addRequest(telematicsRequest);
    }

    @Scheduled(fixedRate = 3_000L)
    public void executeTasks() {
        ibrdIdToStateMap.values().forEach(this::executeTasks);
    }

    private void receiveIbrdEvent(IbrdEvent event) {
        val eventType = event.getEventType();
        switch (eventType) {
            case CONNECT:
                val subscriberToLastTaskTimeMap = ibrdSubscribers.stream()
                        .collect(Collectors.toMap(subscriber -> subscriber, subscriber -> LocalDateTime.MIN));
                val ibrdState = new IbrdState(event.getIbrdId(), event.getChannelId(), subscriberToLastTaskTimeMap);
                ibrdIdToStateMap.put(event.getIbrdId(), ibrdState);
                executeTasks(ibrdState);
                break;
            case DISCONNECT:
                ibrdIdToStateMap.remove(event.getIbrdId());
                break;
        }
    }

    private void executeTasks(IbrdState ibrdState) {
        synchronized (ibrdState) {
            val subscriberToLastTaskTimeMap = ibrdState.getSubscriberToLastTaskTimeMap();
            subscriberToLastTaskTimeMap.forEach((subscriber, lastTaskTime) -> {
                try {
                    val protocol = ibrdDao.findProtocol(ibrdState.getIbrdId());
                    if (!subscriber.getSupportedProtocols().contains(protocol)) {
                        return;
                    }

                    if (!LocalDateTime.MIN.equals(lastTaskTime)) {
                        val rate = subscriber.getRate(protocol);
                        if (rate == null || lastTaskTime.plus(rate).isAfter(LocalDateTime.now())) {
                            return;
                        }
                    }

                    subscriber.executeTask(ibrdState);
                    subscriberToLastTaskTimeMap.put(subscriber, LocalDateTime.now());
                } catch (Exception e) {
                    log.error("Error on ibrd task", e);
                }
            });
        }
    }


}
