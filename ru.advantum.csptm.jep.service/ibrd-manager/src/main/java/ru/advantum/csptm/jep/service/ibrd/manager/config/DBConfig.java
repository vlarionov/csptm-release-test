package ru.advantum.csptm.jep.service.ibrd.manager.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.dbidgen.DbIdGenerator;
import ru.advantum.csptm.jep.service.ibrd.manager.IbrdManagerConfig;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
public class DBConfig {
    @Bean
    public DataSource dataSource(IbrdManagerConfig config) throws SQLException {
        return config.database.getConnectionPool();
    }

    @Bean
    public DbIdGenerator dbIdGenerator(IbrdManagerConfig config, DataSource dataSource) throws SQLException {
        return new DbIdGenerator(config.dbIdGeneratorConfig, dataSource);
    }


}
