package ru.advantum.csptm.jep.service.ibrd.manager.component.sender;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.advantum.csptm.jep.service.ibrd.manager.component.IbrdCommander;
import ru.advantum.csptm.jep.service.ibrd.manager.component.IbrdEventBuilder;
import ru.advantum.csptm.jep.service.ibrd.manager.controller.IbrdSubscriber;
import ru.advantum.csptm.jep.service.ibrd.manager.controller.IbrdState;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdEventType;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdProtocol;

import java.time.Duration;
import java.util.*;

@Service
@Slf4j
public class IbrdTemperatureSender implements IbrdSubscriber {
    private final IbrdCommander ibrdCommander;

    private final TemperatureProvider temperatureProvider;

    private final IbrdEventBuilder ibrdEventBuilder;


    @Autowired
    public IbrdTemperatureSender(IbrdCommander ibrdCommander, TemperatureProvider temperatureProvider, IbrdEventBuilder ibrdEventBuilder) {
        this.ibrdCommander = ibrdCommander;
        this.temperatureProvider = temperatureProvider;
        this.ibrdEventBuilder = ibrdEventBuilder;
    }

    @Override
    public Duration getRate(IbrdProtocol protocol) {
        return Duration.ofMinutes(30);
    }

    @Override
    public Set<IbrdProtocol> getSupportedProtocols() {
        return Sets.newHashSet(IbrdProtocol.MONO, IbrdProtocol.RGB);
    }

    @Override
    public void executeTask(IbrdState ibrdState) {
        val event = ibrdEventBuilder.build(ibrdState.getIbrdId(), ibrdState.getChannelId(), IbrdEventType.SET_TEMPERATURE);
        event.setTemperature(temperatureProvider.getCurrentTemperature());
        ibrdCommander.sendCommandEvent(event);
    }

    @Component
    public static class TemperatureProvider {
        private static final String CACHE_NAME = "temperature";
        private static final long EVICT_CACHE_RATE = 60 * 60 * 1000L;

        private static final String URL = "http://api.openweathermap.org/data/2.5/weather?id={id}&APPID={APPID}";

        private static final Map<String, String> PARAMS = new HashMap<>();
        private static final double KELVIN_TO_CELSIUS_DELTA = -273.15;

        static {
            PARAMS.put("id", "524901");
            PARAMS.put("APPID", "c1c208f862e36bfb98523eaa750d8c38");
        }

        private final RestTemplate restTemplate;

        @Autowired
        public TemperatureProvider(RestTemplate restTemplate) {
            this.restTemplate = restTemplate;
        }

        @Cacheable(CACHE_NAME)
        public int getCurrentTemperature() {
            val response = restTemplate.getForObject(URL, JsonNode.class, PARAMS);
            return (int) (response.get("main").get("temp").asDouble() + KELVIN_TO_CELSIUS_DELTA);
        }

        @Scheduled(fixedRate = EVICT_CACHE_RATE, initialDelay = EVICT_CACHE_RATE)
        @CacheEvict(CACHE_NAME)
        public void evictCache() {
            // Intentionally blank
        }

    }
}
