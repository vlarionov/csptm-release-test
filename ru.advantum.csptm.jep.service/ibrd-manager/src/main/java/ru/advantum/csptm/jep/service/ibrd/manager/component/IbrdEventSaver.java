package ru.advantum.csptm.jep.service.ibrd.manager.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdEvent;
import ru.advantum.csptm.spring.rmq.TypeAdder;

@Component
@Slf4j
public class IbrdEventSaver {
    private final RabbitTemplate rabbitTemplate;

    private final String exchange;

    @Autowired
    public IbrdEventSaver(
            RabbitTemplate rabbitTemplate,
            @Value("#{applicationConfig.rmqServiceConfig.eventDestination.exchange}") String exchange
    ) {
        this.rabbitTemplate = rabbitTemplate;
        this.exchange = exchange;
    }

    public void sendToSave(IbrdEvent event) {
        rabbitTemplate.convertAndSend(exchange, "", event, TypeAdder.of("IbrdEvent"));
    }

}
