package ru.advantum.csptm.jep.service.ibrd.manager.controller;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Data
public class IbrdState {
    private final int ibrdId;

    private final String channelId;

    private final ConcurrentHashMap<IbrdSubscriber, LocalDateTime> subscriberToLastTaskTimeMap;

    public IbrdState(int ibrdId, String channelId, Map<IbrdSubscriber, LocalDateTime> subscriberToLastTaskTimeMap) {
        this.ibrdId = ibrdId;
        this.channelId = channelId;
        this.subscriberToLastTaskTimeMap = new ConcurrentHashMap<>(subscriberToLastTaskTimeMap);
    }


}
