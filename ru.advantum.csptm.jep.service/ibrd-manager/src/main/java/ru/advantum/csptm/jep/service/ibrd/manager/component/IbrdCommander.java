package ru.advantum.csptm.jep.service.ibrd.manager.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.jep.proto.CommandPacket;
import ru.advantum.csptm.jep.proto.CommonMessageType;
import ru.advantum.csptm.jep.service.ibrd.manager.db.IbrdDao;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdEvent;
import ru.advantum.csptm.jep.service.ibrd.model.serializer.IbrdEventToPacketAdapter;
import ru.advantum.csptm.spring.rmq.TypeAdder;

@Service
@Slf4j
public class IbrdCommander {
    private final IbrdDao ibrdDao;

    private final RabbitTemplate rabbitTemplate;

    private final IbrdEventToPacketAdapter ibrdEventToPacketAdapter;

    private final IbrdEventSaver ibrdEventSaver;

    @Autowired
    public IbrdCommander(IbrdDao ibrdDao, RabbitTemplate rabbitTemplate, IbrdEventToPacketAdapter ibrdEventToPacketAdapter, IbrdEventSaver ibrdEventSaver) {
        this.ibrdDao = ibrdDao;
        this.ibrdEventToPacketAdapter = ibrdEventToPacketAdapter;
        this.rabbitTemplate = rabbitTemplate;
        this.ibrdEventSaver = ibrdEventSaver;
    }

    public void sendCommandEvent(IbrdEvent event) {
        String hubRoutingKey = getHubRoutingKey(event.getIbrdId());
        if (hubRoutingKey == null) {
            return;
        }

        rabbitTemplate.convertAndSend(
                hubRoutingKey,
                new CommandPacket[]{ibrdEventToPacketAdapter.toCommandPacket(event)},
                TypeAdder.of(CommonMessageType.COMMAND.name())
        );
        log.info("Event sent: " + event);

        ibrdEventSaver.sendToSave(event);
    }

    private String getHubRoutingKey(int ibrdId) {
        Long hubId = ibrdDao.findHubId(ibrdId);
        if (hubId == null) {
            log.warn("Unknown hubId for ibrdId {}");
            return null;
        }
        return String.format("hub_%d", hubId);
    }
}
