package ru.advantum.csptm.jep.service.ibrd.manager.component.sender;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.jep.service.ibrd.manager.IbrdManagerConfig;
import ru.advantum.csptm.jep.service.ibrd.manager.component.IbrdCommander;
import ru.advantum.csptm.jep.service.ibrd.manager.component.IbrdEventBuilder;
import ru.advantum.csptm.jep.service.ibrd.manager.controller.IbrdState;
import ru.advantum.csptm.jep.service.ibrd.manager.controller.IbrdSubscriber;
import ru.advantum.csptm.jep.service.ibrd.manager.db.ConfigDao;
import ru.advantum.csptm.jep.service.ibrd.manager.db.ForecastDao;
import ru.advantum.csptm.jep.service.ibrd.manager.db.IbrdDao;
import ru.advantum.csptm.jep.service.ibrd.manager.db.MessageDao;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdConfig;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdEventType;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdForecast;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdProtocol;
import ru.advantum.csptm.jep.service.ibrd.model.monorgb.MonoRGBInfo;
import ru.advantum.csptm.jep.service.ibrd.model.row.RowInfo;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class IbrdInfoSender implements IbrdSubscriber {
    private static final int FORECASTS_LIMIT = 40;

    private final IbrdDao ibrdDao;

    private final ConfigDao configDao;

    private final MessageDao messageDao;

    private final ForecastDao forecastDao;

    private final IbrdCommander ibrdCommander;

    private final IbrdEventBuilder ibrdEventBuilder;

    private final Duration updateInfoRate;

    @Autowired
    public IbrdInfoSender(IbrdDao ibrdDao, ConfigDao configDao, MessageDao messageDao, ForecastDao forecastDao, IbrdCommander ibrdCommander, IbrdEventBuilder ibrdEventBuilder, IbrdManagerConfig config) {
        this.ibrdDao = ibrdDao;
        this.configDao = configDao;
        this.messageDao = messageDao;
        this.forecastDao = forecastDao;
        this.ibrdCommander = ibrdCommander;
        this.ibrdEventBuilder = ibrdEventBuilder;
        this.updateInfoRate = config.getUpdateInfoRate();
    }


    @Override
    public Duration getRate(IbrdProtocol protocol) {
        return protocol == IbrdProtocol.ROW ? null : updateInfoRate;
    }

    @Override
    public void executeTask(IbrdState ibrdState) {
        val ibrdId = ibrdState.getIbrdId();
        val protocol = ibrdDao.findProtocol(ibrdId);
        if (protocol == null) {
            log.warn("Protocol not found, ibrdId: {}", ibrdId);
            return;
        }
        val forecasts = forecastDao.findCurrent(ibrdId);

        val event = ibrdEventBuilder.build(ibrdId, ibrdState.getChannelId(), IbrdEventType.SET_INFO);

        switch (protocol) {
            case ROW:
                val rowConfig = configDao.findRowConfig(ibrdId);
                val rowInfo = new RowInfo(
                        messageDao.findCurrent(ibrdId),
                        filter(forecasts, rowConfig),
                        rowConfig.getRunningRowSpeed(),
                        rowConfig.getBlockRate()
                );
                event.setRowInfo(rowInfo);
                break;
            case MONO:
            case RGB:
                val monoRGBConfig = configDao.findMonoRGBConfig(ibrdId);
                val monoRGBInfo = new MonoRGBInfo(
                        messageDao.findCurrent(ibrdId),
                        filter(forecasts, monoRGBConfig),
                        monoRGBConfig
                );
                event.setMonoRGBInfo(monoRGBInfo);
                break;
            default:
                throw new IllegalArgumentException("Unsupported protocol " + protocol);
        }

        ibrdCommander.sendCommandEvent(event);
    }

    private List<IbrdForecast> filter(List<IbrdForecast> forecasts, IbrdConfig config) {
        val now = LocalDateTime.now();
        return forecasts.stream()
                .filter(forecast -> forecast.getArrivalTime().isBefore(now.plus(config.getMaxForecastInterval())))
                .sorted(Comparator.comparing(IbrdForecast::getArrivalTime))
                .limit(config.isLimitForecastsByRows() ? config.getForecastRowNumber() : forecasts.size())
                .limit(FORECASTS_LIMIT)
                .collect(Collectors.toList());
    }

}
