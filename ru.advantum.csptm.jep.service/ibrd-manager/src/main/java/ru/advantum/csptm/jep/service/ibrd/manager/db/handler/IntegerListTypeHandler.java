package ru.advantum.csptm.jep.service.ibrd.manager.db.handler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class IntegerListTypeHandler extends BaseTypeHandler<List<Integer>> {
    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, List<Integer> parameter, JdbcType jdbcType) throws SQLException {
        ps.setArray(i, ps.getConnection().createArrayOf("int4", parameter.toArray(new Integer[parameter.size()])));
    }

    @Override
    public List<Integer> getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return toIntegerList(rs.getArray(columnName));
    }

    @Override
    public List<Integer> getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return toIntegerList(rs.getArray(columnIndex));
    }

    @Override
    public List<Integer> getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return toIntegerList(cs.getArray(columnIndex));
    }

    private List<Integer> toIntegerList(Array array) throws SQLException {
        if (array == null) {
            return null;
        }
        Object[] arrayObjects = (Object[]) array.getArray();
        if (arrayObjects == null) {
            return null;
        }
        List<Integer> result = new ArrayList<>();
        for (Object arrayObject : arrayObjects) {
            result.add((int) arrayObject);
        }
        return result;
    }

}