package ru.advantum.csptm.jep.service.ibrd.manager.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdForecast;

import java.util.List;

@Repository
public class ForecastDao {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ForecastDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<IbrdForecast> findCurrent(int ibrdId) {
        return jdbcTemplate.query(
                "SELECT * FROM ibrd.forecast_pkg$find_current(?)",
                new Object[]{ibrdId},
                (rs, rowNum) -> new IbrdForecast(
                        rs.getLong(1),
                        rs.getString(2),
                        rs.getTimestamp(3).toLocalDateTime(),
                        rs.getString(4)
                )
        );
    }

}
