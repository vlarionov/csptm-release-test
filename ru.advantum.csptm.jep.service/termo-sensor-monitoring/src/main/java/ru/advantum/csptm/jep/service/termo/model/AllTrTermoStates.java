package ru.advantum.csptm.jep.service.termo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class AllTrTermoStates {
    @JsonProperty("trs")
    Map<Long,TrTermoState> trMap = new HashMap<>();
}
