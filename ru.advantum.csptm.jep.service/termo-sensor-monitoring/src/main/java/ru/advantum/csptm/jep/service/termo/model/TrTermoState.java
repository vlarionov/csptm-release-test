package ru.advantum.csptm.jep.service.termo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.collections4.queue.CircularFifoQueue;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TrTermoState {
    @JsonProperty("time")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime time;
    @JsonProperty("tr_id")
    private long trId;
    @JsonProperty("mean_c")
    private Float meanCabinT;
    @JsonProperty("mean_s")
    private Float meanSalonT;
    @JsonProperty("vals")
    private Map<Short,String> vals = new HashMap<>();
    @JsonProperty("salon_termo")
    private CircularFifoQueue<TimeTermo> salonTqueue;
    @JsonProperty("pack_id")
    private long lastPacketId;
    @JsonProperty("salon_not_norm")
    private boolean outOfRangeS = false;
    @JsonProperty("cabin_not_norm")
    private boolean outOfRangeC = false;
    public TrTermoState(){
        salonTqueue =  new CircularFifoQueue(10);
    }
}
