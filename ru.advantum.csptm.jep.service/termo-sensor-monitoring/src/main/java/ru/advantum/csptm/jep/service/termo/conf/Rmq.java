package ru.advantum.csptm.jep.service.termo.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import ru.advantum.rabbitmq.config.RmqConnectionConfig;
import ru.advantum.rabbitmq.config.RmqConsumerConfig;

@Configuration
public class Rmq {
    @Autowired
    Environment environment;

    @Bean
    RmqConsumerConfig rmqConsumerConfig() {
        RmqConsumerConfig rmqConsumerConfig = new RmqConsumerConfig();
        rmqConsumerConfig.queue = environment.getProperty("rmq.consumer.queue");
        return rmqConsumerConfig;
    }

    @Bean
    RmqConnectionConfig rmqConnectionConfig() {
        RmqConnectionConfig connection = new RmqConnectionConfig();
        connection.host = environment.getProperty("spring.rabbitmq.host");
        connection.virtualHost = environment.getProperty("spring.rabbitmq.virtual-host");
        connection.username = environment.getProperty("spring.rabbitmq.username");
        connection.password = environment.getProperty("spring.rabbitmq.password");
        connection.heartbeatInterval = Integer.valueOf(environment.getProperty("spring.rabbitmq.requested-heartbeat"));
        connection.automaticRecovery = true;
        return connection;
    }
}
