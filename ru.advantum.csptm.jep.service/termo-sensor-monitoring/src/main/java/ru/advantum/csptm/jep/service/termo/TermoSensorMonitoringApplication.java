package ru.advantum.csptm.jep.service.termo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@SpringBootApplication
@EnableScheduling
public class TermoSensorMonitoringApplication {
    public static void main(String[] args) throws IOException, TimeoutException {
        SpringApplication.run(TermoSensorMonitoringApplication.class, args);
    }
}
