package ru.advantum.csptm.jep.service.termo.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.sql.Date;
import java.time.LocalDateTime;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TermoNormativ {
    long normId;
    Date dateFrom;
    Date dateTo;
    Short checkDelay;
    Short tCabFrom;
    Short tCabTo;
    Short tSaloonFrom;
    Short tSaloonTo;
    Short allowedSensorDifference;
    Short allowedSensorDiffInPack;
    String comment;
    String normName;
    Boolean isDefault;
    Short month;
    Long trModelId;
}
