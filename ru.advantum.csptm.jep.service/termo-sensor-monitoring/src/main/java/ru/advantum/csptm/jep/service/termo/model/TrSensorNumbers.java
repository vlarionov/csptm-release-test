package ru.advantum.csptm.jep.service.termo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TrSensorNumbers {
    @JsonProperty("tr_id")
    private Long trId;
    @JsonProperty("sensor_num")
    private String sensorNum;
}
