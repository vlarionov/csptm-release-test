package ru.advantum.csptm.jep.service.termo;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.java.Log;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import ru.advantum.csptm.dbidgen.DbIdGenerator;
import ru.advantum.csptm.dbidgen.DbIdGeneratorConfig;
import ru.advantum.csptm.incident.model.*;
import ru.advantum.csptm.jep.proto.CommonMessageType;
import ru.advantum.csptm.jep.proto.UnitPacket;
import ru.advantum.csptm.jep.proto.UnitPacketsBundle;
import ru.advantum.csptm.jep.proto.sensors.ComplexSensorHolder;
import ru.advantum.csptm.jep.proto.sensors.Type;
import ru.advantum.csptm.jep.service.termo.model.*;
import ru.advantum.rabbitmq.config.RmqConnectionConfig;
import ru.advantum.rabbitmq.config.RmqConsumerConfig;
import ru.advantum.rabbitmq.consumer.RmqJsonTalker;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoField;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Service
@Log
public class TermoSensor {
    @Autowired
    Jedis jedis;
    @Autowired
    RmqConsumerConfig rmqConsumerConfig;
    @Autowired
    RmqConnectionConfig connection;
    @Autowired
    ObjectMapper mapper;
    DbIdGeneratorConfig dbIdGeneratorConfig =
            new DbIdGeneratorConfig("ttb.incident_handler_id_generator_seq", 100);
    private Set<Long> trID = new HashSet<>();
    @Value("${redis.application.key}")
    private String redisKey;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private Environment env;
    private TrTermoState termoState = new TrTermoState();
    private AllTrTermoStates all = new AllTrTermoStates();
    private Map<MonthModel, TermoNormativ> termoNormativMap = new HashMap<>();
    // состояние по всем машинам
    //private Map<String, TrTermoState> trMap = new HashMap<>();
    private Map<Long, Long> trModelId = new HashMap<>();
    private List<Long> trWithSensor;
    private List<Short> minMaxTemperature = Arrays.asList((short) -5, (short) 40);
    private TrSensorNumbers[] sensorNumbers;
    // k - id tr, v - номера выходов ДТ
    private Map<Long, List<Integer>> trSensorNumbers = new HashMap<>();
    private RmqJsonTalker rmqTalker;
    private DbIdGenerator dbIdGenerator;
    // количество отклонений от норматива, после которого считается начало нарушения
    private int deviationThreshold;
    // количество показаний в нормативе, после которого считается окончание нарушения
    private int normThreshold;
    private TermoNormativ tn;

    @PostConstruct
    void init() throws IOException, TimeoutException, SQLException {
        log.log(Level.INFO, "Start service");

        dbIdGenerator = new DbIdGenerator(dbIdGeneratorConfig, jdbcTemplate.getDataSource());

        // инициализация в redis
        //jedis.set(redisKey, mapper.writeValueAsString(all));
        String termoStateStr = jedis.get(redisKey);
        try {
            all = mapper.readValue(termoStateStr, AllTrTermoStates.class);
        } catch (Exception e) {
            log.log(Level.SEVERE, "Error read from redis");
        }
        trWithSermoSensor();
        rmqTalker = RmqJsonTalker.newBuilder().setConsumerConfig(true, true, rmqConsumerConfig)
                .addConsumer(CommonMessageType.COORDS.name(),
                        UnitPacketsBundle.class,
                        (tag, envelope, basicProperties, message) -> {
                            try {
                                consumeCoords(message);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        })
                .connect(connection.newConnection());
    }

    public void consumeCoords(UnitPacketsBundle message) throws IOException {
        termoState.getVals().clear();
        // при восстановлении объекта из JSON длина очереди устанавливается по умолчанию (32),
        // следующие строки для ограничения размера очереди до 10 элементов
        CircularFifoQueue circularFifoQueue;
        TrTermoState termoState;
        Incident incident;
        for (Map.Entry<Long, TrTermoState> entry : all.getTrMap().entrySet()) {
            termoState = entry.getValue();
            circularFifoQueue = new CircularFifoQueue(10);
            circularFifoQueue.addAll(termoState.getSalonTqueue());
            termoState.setSalonTqueue(circularFifoQueue);
        }
        Long trId = -1l;
        for (UnitPacket unitPacket : message.getPackets()) {
            trId = unitPacket.getTrId();
            // если к ТС не привязаны датчиками температуры, то не анализируется
            if (trId == null || !trWithSensor.contains(trId)) {
                //LOGGER.warn("null trId [{}]", unitPacket);
                continue;
            }
            //currentState.merge(trId, new TrState(Instant.now(), unitPacket), this::mergePackets);
            //System.out.println(new TrState(Instant.now(), unitPacket));
            //List<String> list;

            //Set<String> keyss = jedis.keys("*");
            Map<Integer, ComplexSensorHolder> complexSensors = unitPacket.getComplexSensors();
            if (complexSensors == null) {
                continue;
            }
            //trID.add(trId);
            termoState = all.getTrMap().get(trId);
            if (termoState == null) {
                termoState = new TrTermoState();
                all.getTrMap().put(trId, termoState);
            }
            // очистка предыдущих показаний
            termoState.getVals().clear();
            termoState.setMeanCabinT(null);
            termoState.setMeanSalonT(null);
            // этот пакет уже обработан
            if (termoState.getLastPacketId() == unitPacket.getPacketId()) {
                continue;
            }
            short zoneNum = -1;
            for (Integer sensorKey : complexSensors.keySet()) {
                if (complexSensors.get(sensorKey).getType().equals(Type.TEMPERATURE)) {
                    // если датчик не привязан, отбрасываем
                    if (!trSensorNumbers.get(trId).contains(sensorKey)) {
                        continue;
                    }
                    termoState.setTrId(trId);
                    termoState.setTime(LocalDateTime.ofInstant(unitPacket.getEventTime(), ZoneOffset.UTC));
                    zoneNum = complexSensors.get(sensorKey).getValues()[0].shortValue();
                    if (zoneNum == 0) {// особая зона, это сбои
                        String prevValue = StringUtils.defaultString(termoState.getVals().get(zoneNum));
                        if (StringUtils.isBlank(prevValue)) {
                            prevValue = complexSensors.get(sensorKey).getValues()[1].toString();
                        } else {
                            prevValue = prevValue + "," + complexSensors.get(sensorKey).getValues()[1].toString();
                        }
                        termoState.getVals().put(zoneNum, prevValue);
                        continue;
                    }
                    termoState.getVals().put(complexSensors.get(sensorKey).getValues()[0].shortValue(),
                            complexSensors.get(sensorKey).getValues()[1].toString());
                }
            }


            {
                // проверка, что все показания в допустимом диапазоне
                Iterator<Map.Entry<Short, String>> it = termoState.getVals().entrySet().iterator();
                Map.Entry<Short, String> termoDataMapEntry;
                while (it.hasNext()) {
                    termoDataMapEntry = it.next();
                    String temperature = termoDataMapEntry.getValue();
                    if (temperature == null) {
                        it.remove();
                        continue;
                    }
                }
                // средняя в кабине - показания датчика 1
                Short cabinT = Short.valueOf(StringUtils.defaultString(termoState.getVals().get(Short.valueOf((short) 1)), "-500"));
                if (cabinT != null && (cabinT > minMaxTemperature.get(0) && cabinT < minMaxTemperature.get(1))) {
                    termoState.setMeanCabinT(Float.valueOf(cabinT));
                }
                //средння в салоне
                int cntr = 0;
                float meanT = 0;
                for (Short zone : termoState.getVals().keySet()) {
                    // зоны с номером больше 1
                    if (zone < 2) {
                        continue;
                    }
                    Short temperature = Short.valueOf(termoState.getVals().get(zone));
                    if (temperature != null
                            && (temperature > minMaxTemperature.get(0) && temperature < minMaxTemperature.get(1))) {
                        meanT += temperature;
                        cntr++;
                    }
                }
                if (!Float.isNaN(meanT / cntr)) {
                    termoState.setMeanSalonT(meanT / cntr);
                    termoState.getSalonTqueue().add(new TimeTermo(termoState.getTime(), termoState.getMeanSalonT(),
                            termoState.getMeanCabinT()));
                }
            }
            // анализ N последних показаний датчика для определения того, что наступило отклонение от температуры, или что
            // температура вернулась к норме
            tn = getNormativ(trId);
            int queueSize = termoState.getSalonTqueue().size() - 1;
            Float salonT;
            Float cabinT;
            short outOfRangeCounter = 0;
            short outOfRangeCounterCab = 0;
            short inRangeCounter = 0;
            short inRangeCounterCab = 0;
            // если есть норматив, и есть с чем сравнивать, проверяем на соответствие
            if (tn != null && queueSize >= Integer.max(deviationThreshold, normThreshold)) {
                for (int i = queueSize;
                     i > queueSize - Integer.max(deviationThreshold, normThreshold);
                     i--) {
                    salonT = termoState.getSalonTqueue().get(i).getTermoVal();
                    cabinT = termoState.getSalonTqueue().get(i).getTermoCab();
                    if (salonT < tn.getTSaloonFrom() || salonT > tn.getTSaloonTo()) {
                        outOfRangeCounter++;
                    } else {
                        inRangeCounter++;
                    }
                    if (cabinT != null && (cabinT < tn.getTCabFrom() || cabinT > tn.getTCabTo())) {
                        outOfRangeCounterCab++;
                    } else {
                        inRangeCounterCab++;
                    }
                }
            }
            List<Incident> incidentList = new ArrayList<>();
            incident = Incident.builder()
                    .incidentId(dbIdGenerator.nextId())
                    .incidentTypeId(IncidentType.TYPE.SALON_TERMO_DEVIATION.get())
                    .timeStart(Instant.ofEpochMilli(System.currentTimeMillis()))
                    .trId(unitPacket.getTrId())
                    .unitId(unitPacket.getUnitId())
                    .incidentStatusId(Incident.STATUS.NEW.get())
                    .incidentInitiatorId(Incident.INITIATOR.SYSTEM.get())
                    .isAlarmOn(false)// по умолчанию выключен
                    .incidentPayload(IncidentPayload.builder().pointStart(Point.of(54f, 33f)).build())
                    .build();
            // порог превышен, включаем тревогу
            if (outOfRangeCounter >= deviationThreshold) {
                incident.setIsAlarmOn(true);
                if (all.getTrMap().get(unitPacket.getTrId()).isOutOfRangeS() == false) {
                    writeNotification2Db(unitPacket.getTrId());
                    all.getTrMap().get(unitPacket.getTrId()).setOutOfRangeS(true);
                }
            }
            if (inRangeCounter >= normThreshold) {
                all.getTrMap().get(unitPacket.getTrId()).setOutOfRangeS(false);
            }
            if (inRangeCounter >= normThreshold || outOfRangeCounter >= deviationThreshold) {
                incidentList.add(incident);
            }

            incident = Incident.builder()
                    .incidentId(dbIdGenerator.nextId())
                    .incidentTypeId(IncidentType.TYPE.CABIN_TERMO_DEVIATION.get())
                    .timeStart(Instant.ofEpochMilli(System.currentTimeMillis()))
                    .trId(unitPacket.getTrId())
                    .unitId(unitPacket.getUnitId())
                    .incidentStatusId(Incident.STATUS.NEW.get())
                    .incidentInitiatorId(Incident.INITIATOR.SYSTEM.get())
                    .isAlarmOn(false)// по умолчанию выключен
                    .incidentPayload(IncidentPayload.builder().pointStart(Point.of(54f, 33f)).build())
                    .build();
            // порог превышен, включаем тревогу
            if (outOfRangeCounterCab >= deviationThreshold) {
                incident.setIsAlarmOn(true);
            }
            if (inRangeCounterCab >= normThreshold || outOfRangeCounterCab >= deviationThreshold) {
                incidentList.add(incident);
            }

            if (incidentList.size() > 0) {
                Map<Short, List<Incident>> collected = incidentList.stream().collect(Collectors.groupingBy(Incident::getIncidentTypeId));
                collected.forEach((k, v) ->
                {
                    {
                        while (true) {
                            try {
                                rmqTalker.basicPublish(Incident.INCIDENT_ROUTER_EXCHANGE, k.toString(),
                                        IncidentBundle.class.getSimpleName(),
                                        IncidentBundle.builder().incidentBundle(v).build());
                                break;
                            } catch (Throwable ex) {
                                //LOGGER.error("send message", ex);
                                try {
                                    TimeUnit.SECONDS.sleep(/*config.sendRetryPeriod*/10_000);
                                } catch (InterruptedException e) {
                                    //LOGGER.error("sleep", ex);
                                }
                            }
                        }
                    }
                });
            }
        }

        //--------------------------------------------------------------------------------------------------------------
        jedis.set(redisKey, mapper.writeValueAsString(all));
    }

    // обновление списка тс с датчиками раз в 10 мин
    @Scheduled(fixedRate = 600_000)
    void trWithSermoSensor() throws IOException {
        log.log(Level.INFO, "Request TR ids");
        // из-за проблем в кодировке русских букв
        trWithSensor = jdbcTemplate.queryForList(
                new String(env.getProperty("monitor.termo.trSql").getBytes("ISO-8859-1"), "UTF-8"), Long.class);
        // мин и максимальное показание датчика температуры
        minMaxTemperature = jdbcTemplate.queryForList(
                new String(env.getProperty("monitor.termo.minMaxTermoSql").getBytes("ISO-8859-1"), "UTF-8"), Short.class);

        String sensorNumbrsString = jdbcTemplate.queryForObject(
                new String(env.getProperty("monitor.termo.sensorNumSql").getBytes("ISO-8859-1"), "UTF-8"), String.class);
        sensorNumbers = mapper.readValue(sensorNumbrsString, TrSensorNumbers[].class);
        List<Integer> sensorNums;
        for (TrSensorNumbers sNumbers : sensorNumbers) {
            sensorNums = Arrays.stream(sNumbers.getSensorNum().split(","))
                    .mapToInt(Integer::parseInt)
                    .boxed()
                    .collect(toList());
            trSensorNumbers.put(sNumbers.getTrId(), sensorNums);
        }
        //********************* все нормативы для ТС
        List<TermoNormativ> tNorms = jdbcTemplate.query(env.getProperty("monitor.termo.termoNormativSql"),
                new BeanPropertyRowMapper(TermoNormativ.class));
        for (TermoNormativ tNorm : tNorms) {
            termoNormativMap.put(new MonthModel((int) tNorm.getMonth(), tNorm.getTrModelId()), tNorm);
        }
        //********************* tr_id - модель id
        List<TrModelId> trModelIdList = jdbcTemplate.query(env.getProperty("monitor.termo.trModelIdSql"),
                new BeanPropertyRowMapper(TrModelId.class));
        trModelIdList.forEach(o -> trModelId.put(o.getTrId(), o.getModelId()));
        // количество подряд неправильных и правильных значений
        deviationThreshold = jdbcTemplate.queryForObject(env.getProperty("monitor.termo.deviationThresholdSql"), Integer.class);
        normThreshold = jdbcTemplate.queryForObject(env.getProperty("monitor.termo.normThresholdSql"), Integer.class);
    }

    // возвращает норматив для ТС
    private TermoNormativ getNormativ(Long trId) {
        Long modelId = trModelId.get(trId);
        int month = LocalDate.now().get(ChronoField.MONTH_OF_YEAR);
        TermoNormativ normativ = termoNormativMap.get(new MonthModel(month, modelId));
        if (normativ == null) {
            // норматив по умолчанию
            normativ = termoNormativMap.get(new MonthModel(month, null));
        }
        return normativ;
    }

    // запись в таблицу БД, на вставку в которую срабатывает триггер отправки уведомления
    private void writeNotification2Db(Long trId) {
        log.log(Level.INFO, "Notification, tr_id " + trId);
        jdbcTemplate.update(env.getProperty("monitor.termo.notification"), trId);
    }
}
