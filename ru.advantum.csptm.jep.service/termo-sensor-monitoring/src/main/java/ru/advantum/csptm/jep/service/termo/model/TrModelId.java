package ru.advantum.csptm.jep.service.termo.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TrModelId {
    Long trId;
    Long modelId;
}
