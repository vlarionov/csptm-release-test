package ru.advantum.csptm.jep.service.termo.conf;


import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

@Configuration
public class RedisConfig {

    @Autowired
    Environment environment;

    @Bean
    public JedisPool jedisPool() {
        return new JedisPool(new GenericObjectPoolConfig(),
                environment.getProperty("spring.redis.host"),
                Integer.valueOf(environment.getProperty("spring.redis.port")),
                Integer.valueOf(environment.getProperty("spring.redis.timeout")),
                environment.getProperty("spring.redis.password"));
    }

    @Bean
    public Jedis jedis() {
        return jedisPool().getResource();
    }
}
