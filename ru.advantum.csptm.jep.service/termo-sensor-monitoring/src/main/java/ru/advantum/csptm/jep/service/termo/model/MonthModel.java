package ru.advantum.csptm.jep.service.termo.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

@EqualsAndHashCode
@Data
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
// используется как ключ в mape с нормативами
public class MonthModel {
    Integer month;
    Long trModelId;
}
