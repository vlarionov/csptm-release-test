package ru.advantum.csptm.jep.service.glinker.online.config;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Validated
@Component
@ConfigurationProperties(prefix = "graph-linker")
public class GraphLinkerConfig {

    /**
     * Время до принудительной остановки service.sh
     */
    @Valid
    @NotNull
    private int timeoutSec;

    @Valid
    @NotNull
    private String redisStateLocation;

    public int getTimeoutSec() {
        return timeoutSec;
    }

    public void setTimeoutSec(int timeoutSec) {
        this.timeoutSec = timeoutSec;
    }

    public String getRedisStateLocation() {
        return redisStateLocation;
    }

    public void setRedisStateLocation(String redisStateLocation) {
        this.redisStateLocation = redisStateLocation;
    }
}
