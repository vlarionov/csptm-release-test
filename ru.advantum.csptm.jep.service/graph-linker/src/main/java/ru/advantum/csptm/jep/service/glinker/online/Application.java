package ru.advantum.csptm.jep.service.glinker.online;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.advantum.csptm.common.JedisConfig;
import ru.advantum.csptm.common.RmqServiceConfig;
import ru.advantum.csptm.jep.service.glinker.online.config.BaseGLConfiguration;

@SpringBootApplication
@EnableScheduling
@EnableEurekaClient
@Import({BaseGLConfiguration.class, JedisConfig.class, RmqServiceConfig.class})
public class Application {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

}
