package ru.advantum.csptm.jep.service.glinker.online.config;

import java.util.concurrent.TimeUnit;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import ru.advantum.csptm.incident.model.IncidentBundle;
import ru.advantum.csptm.jep.proto.CommonMessageType;
import ru.advantum.csptm.jep.proto.UnitPacketsBundle;
import ru.advantum.csptm.jep.service.glinker.online.Application;
import ru.advantum.csptm.spring.rmq.ConsumerProperties;
import ru.advantum.csptm.spring.rmq.MessageTypeResolver;
import ru.advantum.csptm.spring.rmq.MultiTypeResolver;

@Configuration
@ComponentScan(basePackages = "ru.advantum.csptm.jep.service.glinker", excludeFilters = { @ComponentScan.Filter(type= FilterType.ASSIGNABLE_TYPE, value=Application.class)})
@MapperScan(basePackages = "ru.advantum.csptm.jep.service.glinker")
public class BaseGLConfiguration {

    @Bean
    public ConsumerProperties consumerProperties() {
        return ConsumerProperties.newBuilder()
                .setShutdownTimeout(TimeUnit.SECONDS.toMillis(10))
                .build();
    }

    @Bean
    public MessageTypeResolver messageTypeResolver() {
        return MultiTypeResolver.newBuilder()
                .addPair(CommonMessageType.COORDS.name(), UnitPacketsBundle.class)
                .addPair(IncidentBundle.class.getSimpleName(), IncidentBundle.class)
                .build();
    }
}
