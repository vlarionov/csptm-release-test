package ru.advantum.csptm.jep.service.glinker.online.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.incident.model.Incident;
import ru.advantum.csptm.incident.model.IncidentBundle;
import ru.advantum.csptm.incident.model.IncidentPayload;
import ru.advantum.csptm.jep.proto.UnitPacketsBundle;
import ru.advantum.csptm.jep.service.glinker.db.TTStorageAccessor;
import ru.advantum.csptm.jep.service.glinker.linkers.GraphLinkerController;
import ru.advantum.csptm.jep.service.glinker.linkers.StateHolder;
import ru.advantum.csptm.jep.service.glinker.linkers.listener.EventCollector;
import ru.advantum.csptm.jep.service.glinker.linkers.listener.EventSender;
import ru.advantum.csptm.jep.service.glinker.model.Timetable;
import ru.advantum.csptm.jep.service.glinker.model.event.SimpleUnitPacket;
import ru.advantum.csptm.jep.service.glinker.online.model.state.dto.GraphLinkerStateDTO;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

// TODO: использовать фильтр TimeForwardFilter вместо того, чтобы руками все время считать
@Service
@RabbitListener
public class GraphLinkerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GraphLinkerService.class);

    private final TTStorageAccessor storageAccessor;
    private final StateManager stateManager;
    private final EventSender eventSender;

    private GraphLinkerController graphLinkerController;
    private StateHolder stateHolder;

    @Autowired
    public GraphLinkerService(TTStorageAccessor storageAccessor,
                              StateManager stateManager,
                              EventSender eventSender,
                              GraphLinkerController graphLinkerController,
                              StateHolder stateHolder) {
        this.stateManager = stateManager;
        this.storageAccessor = storageAccessor;
        this.eventSender = eventSender;
        this.graphLinkerController = graphLinkerController;
        this.stateHolder = stateHolder;
    }

    @RabbitHandler
    public void consumeIncidentBundle(IncidentBundle incidentBundle) {
        LOGGER.info("receive {} incidents", incidentBundle.getIncidentBundle().size());

        final Integer[] ttVariantIds = incidentBundle.getIncidentBundle()
                .stream()
                .map(Incident::getIncidentPayload)
                .map(IncidentPayload::getTtVariantList)
                .filter(Objects::nonNull)
                .flatMap(List::stream)
                .toArray(Integer[]::new);

        final Integer[] ttOutIds = incidentBundle.getIncidentBundle()
                .stream()
                .map(Incident::getIncidentPayload)
                .map(IncidentPayload::getTtOutList)
                .filter(Objects::nonNull)
                .flatMap(List::stream)
                .toArray(Integer[]::new);

        final Integer[] ttActionIds = incidentBundle.getIncidentBundle()
                .stream()
                .map(Incident::getIncidentPayload)
                .map(IncidentPayload::getTtActionList)
                .filter(Objects::nonNull)
                .flatMap(List::stream)
                .toArray(Integer[]::new);

        LOGGER.info("receive ttVariantIds {} ttOutIds {} ttActionIds {}",
                    ttVariantIds.length, ttOutIds.length, ttActionIds.length);

        List<Timetable> timetables = storageAccessor.getTimetables(ttVariantIds, ttOutIds, ttActionIds);

        graphLinkerController.updateTimetables(timetables);
    }

    @RabbitHandler
    public void consumeCoords(UnitPacketsBundle message) {
        message.getPackets().stream()
                .map(SimpleUnitPacket::from)
                .forEach(p -> {
                    try {
                        graphLinkerController.link(p);
                    }catch (Throwable t){
                        LOGGER.error("Got exception while processing packet "+p+". ", t);
                    }
                });
        eventSender.sendEvents();
    }

    @PostConstruct
    public void start() {
        Optional<GraphLinkerStateDTO> stateDTOOpt = stateManager.readStateDTO();
        LOGGER.info("State DTO [{}]", stateDTOOpt);
        if(stateDTOOpt.isPresent()){
            GraphLinkerStateDTO stateDTO = stateDTOOpt.get();
            if (Instant.now().isBefore(stateDTO.getCreateTime().plus(3, ChronoUnit.HOURS))) {
                stateManager.dtoToState(stateDTO);
            }else {
                LOGGER.info("State is too old {}", stateDTO);
            }
        }

        LOGGER.info("Inited GraphLinkerState {}", stateHolder);
        graphLinkerController.init();

        if (stateHolder.getTtActions2TrId().isEmpty()) {
            List<Timetable> timetables = storageAccessor.getTimetables(LocalDate.now(), null, true);
            graphLinkerController.updateTimetables(timetables);
        }
    }

    @Scheduled(initialDelay = 20 * 60 * 1000, fixedDelay = 60 * 60 * 1000)
    private void cleanOldTtActions() {
        graphLinkerController.cleanOldTtActions();
    }

    @PreDestroy
    public void stop() {
        LOGGER.info("Shutting down");
        LOGGER.info("Saving GraphLinkerController state");
        stateManager.saveState();
        LOGGER.info("State saved");
    }
}
