package ru.advantum.csptm.jep.service.glinker.online.model.state.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.advantum.csptm.jep.service.glinker.model.TtAction;
import ru.advantum.csptm.jep.service.glinker.model.state.TrLinkerState;

import java.time.Instant;
import java.util.Map;

@Getter
@RequiredArgsConstructor
public class GraphLinkerStateDTO {

    private final Instant createTime;
    private final TrLinkerState[] linkers;
    private final TtAction[] ttActions;
    private final Map<Long, Long> ttActions2TrId;

    @Override
    public String toString() {
        return "GraphLinkerStateDTO{" +
                "createTime=" + createTime +
                ", linkersSize=" + linkers.length +
                ", roundsSize=" + ttActions.length +
                ", orderRound2TrIdSize=" + ttActions2TrId.size() +
                '}';
    }
}
