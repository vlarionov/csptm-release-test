package ru.advantum.csptm.jep.service.glinker.online.service;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import ru.advantum.csptm.jep.service.glinker.db.TTStorageAccessor;
import ru.advantum.csptm.jep.service.glinker.linkers.StateHolder;
import ru.advantum.csptm.jep.service.glinker.model.TtAction;
import ru.advantum.csptm.jep.service.glinker.model.state.TrLinkerState;
import ru.advantum.csptm.jep.service.glinker.online.config.GraphLinkerConfig;
import ru.advantum.csptm.jep.service.glinker.online.model.state.dto.GraphLinkerStateDTO;

import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class StateManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(StateManager.class);

    private final TTStorageAccessor storageAccessor;
    private final JedisPool jedisPool;
    private final String glMapName;
    // TODO: перевести на spring jedis
    private final Gson gson;

    private StateHolder stateHolder;

    @Autowired
    public StateManager(TTStorageAccessor storageAccessor,
                        JedisPool jedisPool,
                        GraphLinkerConfig graphLinkerConfig,
                        Gson gson,
                        StateHolder stateHolder) {
        this.storageAccessor = storageAccessor;
        this.jedisPool = jedisPool;
        this.glMapName = graphLinkerConfig.getRedisStateLocation();
        this.gson = gson;
        this.stateHolder = stateHolder;
    }

    private GraphLinkerStateDTO stateToDTO() {
        TrLinkerState[] linkerStates = stateHolder.getLinkerStates().values().toArray(new TrLinkerState[0]);

        TtAction[] actions = stateHolder.getTrTtActions().entrySet().stream()
                .flatMap(e -> e.getValue().entrySet().stream().map(Map.Entry::getValue))
                .toArray(TtAction[]::new);

        return new GraphLinkerStateDTO(Instant.now(), linkerStates, actions, stateHolder.getTtActions2TrId());
    }

    public Optional<GraphLinkerStateDTO> readStateDTO() {
        LOGGER.info("Reading state from {}", glMapName);
        try (Jedis jedis = jedisPool.getResource()) {
            String stateString = jedis.get(glMapName);
            if (stateString == null || stateString.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(gson.fromJson(stateString, GraphLinkerStateDTO.class));
        }
    }

    public void dtoToState(GraphLinkerStateDTO stateDTO) {
        LOGGER.info("Initing GraphLinkerController from state {}", stateDTO);
        Long[] rounds = Arrays.stream(stateDTO.getTtActions())
                .map(TtAction::getRoundId)
                .distinct()
                .toArray(Long[]::new);

        storageAccessor.updateRoundsCache(rounds);

        Map<Long, TtAction> ttActions = new HashMap<>();
        for (TtAction ttAction : stateDTO.getTtActions()) {
            storageAccessor.recalcFields(ttAction);
            ttActions.put(ttAction.getTtActionId(), ttAction);
        }

        Map<Long, TrLinkerState> linkers = Arrays.stream(stateDTO.getLinkers())
                .collect(Collectors.toMap(ts -> ts.tr.getTrId(), ts -> ts));

        stateHolder.setAll(ttActions, stateDTO.getTtActions2TrId(), linkers, stateDTO.getCreateTime());
    }

    public void saveState() {
        LOGGER.info("State to save {} to {}", stateHolder, glMapName);
        GraphLinkerStateDTO stateDTO = stateToDTO();
        LOGGER.info("State converted to DTO {}", stateDTO);
        String stateString = gson.toJson(stateDTO);
        LOGGER.info("Converted to JSON");
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set(glMapName, stateString);
        }
    }

}
