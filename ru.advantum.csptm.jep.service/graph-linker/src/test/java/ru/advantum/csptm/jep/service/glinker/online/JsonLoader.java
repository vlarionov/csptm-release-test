package ru.advantum.csptm.jep.service.glinker.online;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.fatboyindustrial.gsonjavatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Data;
import ru.advantum.csptm.jep.proto.UnitPacket;
import ru.advantum.csptm.jep.service.glinker.model.GraphSection;
import ru.advantum.csptm.jep.service.glinker.model.Round;
import ru.advantum.csptm.jep.service.glinker.model.StopItem;
import ru.advantum.csptm.jep.service.glinker.model.dto.GraphSectionDTO;
import ru.advantum.csptm.jep.service.glinker.model.dto.RoundDTO;
import ru.advantum.csptm.jep.service.glinker.model.event.SimpleUnitPacket;

public class JsonLoader {

    private static final Gson GSON = Converters.registerAll(new GsonBuilder().setPrettyPrinting()).create();

    public static RoundJson loadRound(String filename) {
        try (FileReader fr = new FileReader(filename)) {
            return GSON.fromJson(fr, RoundJson.class);

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static List<UnitPacket> loadTrack(String filename) {
        try (FileReader fr = new FileReader(filename)) {
            final UnitPacket[] track = GSON.fromJson(fr, UnitPacket[].class);
            return Arrays.asList(track);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static void saveTrack(List<SimpleUnitPacket> track, String filename) {
        try (FileWriter writer = new FileWriter(filename)) {
            GSON.toJson(track, writer);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Data
    public static class RoundJson {

        public final RoundDTO round;
        public final List<StopItem> stops;
        public final List<GraphSectionDTO> sections;
    }
}
