package ru.advantum.csptm.jep.service.glinker.online;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.ListUtils;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import ru.advantum.csptm.incident.model.Incident;
import ru.advantum.csptm.incident.model.IncidentBundle;
import ru.advantum.csptm.incident.model.IncidentPayload;
import ru.advantum.csptm.incident.model.Point;
import ru.advantum.csptm.jep.proto.UnitPacket;
import ru.advantum.csptm.jep.proto.UnitPacketsBundle;
import ru.advantum.csptm.jep.service.glinker.db.GraphMapper;
import ru.advantum.csptm.jep.service.glinker.db.TTStorageAccessor;
import ru.advantum.csptm.jep.service.glinker.linkers.StateHolder;
import ru.advantum.csptm.jep.service.glinker.model.StopItem;
import ru.advantum.csptm.jep.service.glinker.model.Timetable;
import ru.advantum.csptm.jep.service.glinker.model.TtAction;
import ru.advantum.csptm.jep.service.glinker.model.dto.GraphSectionDTO;
import ru.advantum.csptm.jep.service.glinker.model.dto.NormBetweenStop;
import ru.advantum.csptm.jep.service.glinker.model.dto.RoundDTO;
import ru.advantum.csptm.jep.service.glinker.model.event.StopItemPass;
import ru.advantum.csptm.jep.service.glinker.online.config.BaseGLConfiguration;
import ru.advantum.csptm.jep.service.glinker.online.service.GraphLinkerService;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.MockitoAnnotations.initMocks;
import static ru.advantum.csptm.jep.service.glinker.online.JsonLoader.loadRound;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {GraphLinkerTest.TestConfig.class, BaseGLConfiguration.class})
@ActiveProfiles("test")
public class GraphLinkerTest {

    @MockBean
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private GraphLinkerService graphLinkerService;

    @Autowired
    private StateHolder stateHolder;

    @Autowired
    private TTStorageAccessor storageAccessor;

    private List<StopItemPass> events = new ArrayList<>();

    @Before
    public void setup() {
        initMocks(this);
        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            String s = (String)args[0];
            List l = (List)args[1];
            l.forEach(o -> {if(o instanceof StopItemPass) events.add((StopItemPass)o);});
            return null;
        }).when(rabbitTemplate).convertAndSend(anyString(), any(List.class) , any(MessagePostProcessor.class));

    }

    private static JsonLoader.RoundJson simpleRound;
    private static JsonLoader.RoundJson simpleReverseRound;
    private List<UnitPacketsBundle> bundles;
    private List<UnitPacketsBundle> bundlesReverse;
    private static long trId = 1;
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    private static final LocalDateTime t1 = LocalDateTime.parse("2018-02-01 08:50", formatter);
    private static final LocalDateTime t2 = LocalDateTime.parse("2018-02-01 09:05", formatter);
    private static final LocalDateTime t3 = LocalDateTime.parse("2018-02-01 09:30", formatter);
    private static final long round1Id = 36011;
    private static final long round2Id = 36012;

    public void init(String test){
        stateHolder.instantiateEmpty();
        events = new ArrayList<>();
        simpleRound = loadRound("src/test/resources/"+test+"/round_36011_simple.json");
        simpleReverseRound = loadRound("src/test/resources/"+test+"/round_36012_simple_reverse.json");
        bundles = getBundles("src/test/resources/"+test+"/track_36011_good.json");
        bundlesReverse = getBundles("src/test/resources/"+test+"/track_36011_good_reverse.json");
        graphLinkerService.start();
        storageAccessor.updateCache();
    }

    @Test
    public void technicalStopsWithLongStop(){
        List expected = Lists.newArrayList(
                new StopItemPass(1,1,36011,94095,1, Instant.parse("2018-02-01T08:50:47.489Z")),
                new StopItemPass(1,1,36011,92447,2, Instant.parse("2018-02-01T08:52:59.867Z")),
                new StopItemPass(1,1,36011,133659,3, Instant.parse("2018-02-01T08:54:25.673Z")),
                new StopItemPass(1,1,36011,94099,4, Instant.parse("2018-02-01T08:55:11.288Z")),
                new StopItemPass(1,1,36011,94103,5, Instant.parse("2018-02-01T08:56:34.457Z")),
                new StopItemPass(1,1,36011,94455,6, Instant.parse("2018-02-01T08:57:58.787Z")),
                new StopItemPass(1,1,36011,89959,7, Instant.parse("2018-02-01T09:00:13.150Z")),
                new StopItemPass(1,1,36011,94791,8, Instant.parse("2018-02-01T09:06:25.167Z")),
                new StopItemPass(1,2,36012,94791,1, Instant.parse("2018-02-01T09:12:14.447Z")),
                new StopItemPass(1,2,36012,94447,2, Instant.parse("2018-02-01T09:14:44.370Z")),
                new StopItemPass(1,1,36011,94791,8, Instant.parse("2018-02-01T09:00:43.150Z")),
                new StopItemPass(1,2,36012,94791,1, Instant.parse("2018-02-01T09:13:54.370Z"))
        );
        go("technicalStopsWithLongStop", expected);
    }

    @Test
    public void notTechnicalStops(){
        List expected = Lists.newArrayList(
                new StopItemPass(1,1,36011,94095,1, Instant.parse("2018-02-01T08:50:47.489Z")),
                new StopItemPass(1,1,36011,92447,2, Instant.parse("2018-02-01T08:52:59.867Z")),
                new StopItemPass(1,1,36011,133659,3, Instant.parse("2018-02-01T08:54:25.673Z")),
                new StopItemPass(1,1,36011,94099,4, Instant.parse("2018-02-01T08:55:11.288Z")),
                new StopItemPass(1,1,36011,94103,5, Instant.parse("2018-02-01T08:56:34.457Z")),
                new StopItemPass(1,1,36011,94455,6, Instant.parse("2018-02-01T08:57:58.787Z")),
                new StopItemPass(1,1,36011,89959,7, Instant.parse("2018-02-01T09:00:13.150Z")),
                new StopItemPass(1,1,36011,94791,8, Instant.parse("2018-02-01T09:06:25.167Z")),
                new StopItemPass(1,2,36012,94791,1, Instant.parse("2018-02-01T09:12:14.447Z")),
                new StopItemPass(1,2,36012,94447,2, Instant.parse("2018-02-01T09:14:44.370Z"))
        );
        go("notTechnicalStops", expected);
    }

    @Test
    public void technicalStopsWithFastPassage(){
        List expected = Lists.newArrayList(
                new StopItemPass(1,1,36011,94095,1, Instant.parse("2018-02-01T08:50:47.489Z")),
                new StopItemPass(1,1,36011,92447,2, Instant.parse("2018-02-01T08:52:59.867Z")),
                new StopItemPass(1,1,36011,133659,3, Instant.parse("2018-02-01T08:54:25.673Z")),
                new StopItemPass(1,1,36011,94099,4, Instant.parse("2018-02-01T08:55:11.288Z")),
                new StopItemPass(1,1,36011,94103,5, Instant.parse("2018-02-01T08:56:34.457Z")),
                new StopItemPass(1,1,36011,94455,6, Instant.parse("2018-02-01T08:57:58.787Z")),
                new StopItemPass(1,1,36011,89959,7, Instant.parse("2018-02-01T08:59:26.417Z")),
                new StopItemPass(1,1,36011,94791,8, Instant.parse("2018-02-01T08:59:55.172Z")),
                new StopItemPass(1,2,36012,94791,1, Instant.parse("2018-02-01T09:00:14.447Z")),
                new StopItemPass(1,2,36012,94447,2, Instant.parse("2018-02-01T09:00:31.648Z")),
                new StopItemPass(1,1,36011,94791,8, Instant.parse("2018-02-01T08:59:50.417Z")),
                new StopItemPass(1,2,36012,94791,1, Instant.parse("2018-02-01T08:59:51.648Z"))
        );
        go("technicalStopsWithFastPassage", expected);
    }

    @Test
    public void updateTimetables(){
        init("technicalStopsWithLongStop");
        ArrayList<Incident> incidents = Lists.newArrayList(getIncident(1L),
                getIncident(2L));
        IncidentBundle bundle = IncidentBundle.builder().incidentBundle(incidents).build();
        graphLinkerService.consumeIncidentBundle(bundle);
        assertEquals(events, Lists.<StopItemPass>emptyList());
    }

    private void go(String test, List expected) {
            init(test);
            for (UnitPacketsBundle b : bundles) {
                graphLinkerService.consumeCoords(b);
            }
            for (UnitPacketsBundle b : bundlesReverse) {
                graphLinkerService.consumeCoords(b);
            }

            assertEquals(events, expected);
    }

    private Incident getIncident(long id){
        return Incident.builder()
                .incidentId(id)
                .accountId(2)
                .incidentInitiatorId((short)3)
                .incidentPayload(getIncidentPayload())
                .incidentStatusId((short)5)
                .incidentStatusId((short)6)
                .incidentTypeId((short)7)
                .isAlarmOn(false)
                .parentIncidentId(7L)
                .timeFinish(Instant.now())
                .timeStart(Instant.now())
                .trId(10L)
                .unitId(11L)
                .build();
    }

    private IncidentPayload getIncidentPayload(){
        return IncidentPayload.builder()
                .incidentReason(1)
                .deviationVal(2L)
                .driverList(getIntList())
                .factVal(4L)
                .incommingMessage("asd")
                .incommingMessageId("5")
                .orderList(getIntList())
                .pointFinish(Point.of((float)37.47610591, (float)55.85503916))
                .pointStart(Point.of((float)37.47610591, (float)55.85503916))
                .routeVarList(getIntList())
                .stopItemList(getIntList())
                .trList(getLongList())
                .ttActionList(getIntList())
                .ttOutList(getIntList())
                .ttVariantList(getIntList())
                .unitList(getLongList())
                .build();
    }

    private List<Integer> getIntList(){
        return Lists.newArrayList(1,2,3);
    }
    private List<Long> getLongList(){
        return Lists.newArrayList(1L,2L,3L);
    }

    private List<UnitPacketsBundle> getBundles(String filename){
        List<UnitPacket> packets = JsonLoader.loadTrack(filename);
        List<UnitPacketsBundle> bundles = packets.stream().map(p -> UnitPacketsBundle.of(0, p)).collect(Collectors.toList());
        return bundles;
    }

    @Profile("test")
    @Configuration
    @EnableAutoConfiguration(exclude = {RabbitAutoConfiguration.class})
    public static class TestConfig {
        @Bean
        @Primary
        public JedisPool jedisPool(){
            JedisPool mockJP = Mockito.mock(JedisPool.class);
            Jedis mockJ = Mockito.mock(Jedis.class);
            doAnswer(i -> null).when(mockJ).get(anyString());
            doAnswer(i -> mockJ).when(mockJP).getResource();
            return mockJP;
        }

        @Bean
        @Primary
        public GraphMapper graphMapper(){
            return new GraphMapper() {
                @Override
                public List<RoundDTO> getRounds(Long[] roundIds) {
                    if (simpleRound == null) return new ArrayList<>();
                    return Lists.newArrayList(simpleRound.round, simpleReverseRound.round);
                }

                @Override
                public List<GraphSectionDTO> getGraphSections(Long[] roundIds) {
                    if (simpleRound == null) return new ArrayList<>();
                    return ListUtils.union(simpleRound.sections, simpleReverseRound.sections);
                }

                @Override
                public List<StopItem> getStopItems(Long[] roundIds) {
                    if (simpleRound == null) return new ArrayList<>();
                    return ListUtils.union(simpleRound.stops, simpleReverseRound.stops);
                }

                @Override
                public List<Timetable> getTimetables(LocalDate orderDate, Long tId, boolean onlyPlanned) {
                    TtAction tt1 = new TtAction(1, round1Id, t1, t2, true);
                    TtAction tt2 = new TtAction(2, round2Id, t2, t3, true);
                    return Lists.newArrayList(new Timetable(trId, (short)53, 15, 1, Lists.newArrayList(tt1, tt2)));
                }

                @Override
                public List<Timetable> getUpdatedTimetables(Integer[] ttVariantIds, Integer[] ttOutIds, Integer[] ttRoundIds) {
                    TtAction tt1 = new TtAction(1, round1Id, t1, t2, true);
                    TtAction tt2 = new TtAction(2, round2Id, t2, t3, true);
                    return Lists.newArrayList(new Timetable(trId, (short)53, 15, 1, Lists.newArrayList(tt1, tt2)));
                }

                @Override
                public List<NormBetweenStop> getNorms(long stop1, long stop2) {
                    if(stop1 == 89959 && stop2 == 94791)
                        return com.google.common.collect.Lists.newArrayList(
                                new NormBetweenStop(15, LocalTime.parse("00:00:00"), LocalTime.parse("23:00:00"), (short)53, 30, 1L)
                        );
                    if(stop1 == 94791 && stop2 == 94447){
                        return com.google.common.collect.Lists.newArrayList(
                                new NormBetweenStop(15, LocalTime.parse("00:00:00"), LocalTime.parse("23:00:00"), (short)53, 50, 1L)
                        );
                    } else return com.google.common.collect.Lists.newArrayList();
                }
            };

        }

    }
}
