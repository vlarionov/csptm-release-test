package ru.advantum.csptm.jep.service.trmapper;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;
import ru.advantum.rabbitmq.config.RmqDestinationConfig;

/**
 * Created by kaganov on 12/12/2016.
 */
@Root(name = "unit-tr-mapper")
public class UnitTrMapperConfig {

    @Attribute(name = "send-retry-period", required = false)
    public final int sendRetryPeriod;

    @Element(name = "rmq-service")
    public final JepRmqServiceConfig serviceConfig;

    @Element(name = "rmq-session-destination", required = false)
    public final RmqDestinationConfig sessionDestination;

    @Element(name = "database")
    public final DatabaseConnection database;

    @Element(name = "rnis-depot-code", required = false)
    public final String rnisDepotCode;

    public UnitTrMapperConfig() {
        sendRetryPeriod = 5;
        serviceConfig = new JepRmqServiceConfig();
        sessionDestination = new RmqDestinationConfig();
        database = new DatabaseConnection();
        rnisDepotCode = "";
    }
}
