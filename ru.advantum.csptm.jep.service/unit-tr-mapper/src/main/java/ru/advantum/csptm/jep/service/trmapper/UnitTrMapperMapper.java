package ru.advantum.csptm.jep.service.trmapper;

import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by kaganov on 12/12/2016.
 */
interface UnitTrMapperMapper {

    @Select("select unit_id \"unitId\", tr_id \"trId\" from core.unit2tr")
    List<UnitTrLink> getUnitTrLinks();

    @Select("select object_id \"unitId\", object_id \"trId\" from rnis.com_vehicles where depot_code=#{depotCode} and sign_deleted=0")
    List<UnitTrMapperMapper.UnitTrLink> getUnitTrLinksRnis(String depotCode);

    class UnitTrLink {

        final long unitId;
        final long trId;

        UnitTrLink() {
            this.unitId = 0;
            this.trId = 0;
        }
    }
}
