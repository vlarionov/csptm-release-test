package ru.advantum.csptm.jep.service.trmapper;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.jep.proto.*;
import ru.advantum.rabbitmq.consumer.RmqJsonTalker;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * Created by kaganov on 12/12/2016.
 */
public class UnitTrMapper {

    private static final Logger LOGGER = LoggerFactory.getLogger(UnitTrMapper.class);

    private final UnitTrMapperConfig config;
    private final AtomicReference<Map<Long, Long>> unitTrMapping = new AtomicReference<>(new HashMap<>());
    private final Random r = new Random();
    private final AtomicLong packetId = new AtomicLong(r.nextLong());
    private final ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();

    private RmqJsonTalker rmqTalker;
    private String routingKey = null;

    public UnitTrMapper(UnitTrMapperConfig config) {
        this.config = config;
        LOGGER.info("Initial packetId {}", packetId);
    }

    public void start() throws IOException, TimeoutException, SQLException {
        SqlMapperSingleton.setConfiguration(config.database.getConnectionPool());

        updateUnitTrMapping();
        ses.scheduleAtFixedRate(this::updateUnitTrMapping, 60, 60, TimeUnit.SECONDS);

        if (config.serviceConfig.destination.routingKey != null && !config.serviceConfig.destination.routingKey.isEmpty()) {
            routingKey = config.serviceConfig.destination.routingKey;
            LOGGER.info("Using static routing key [{}]", routingKey);
        }

        rmqTalker = RmqJsonTalker.newBuilder()
                .setSendRetryPeriodSec(config.sendRetryPeriod)
                .setConsumerConfig(true, true, config.serviceConfig.consumer)
                .addConsumer(CommonMessageType.COORDS.name(),
                             UnitPacketsBundle.class,
                             (consumerTag, envelope, basicProperties, message) -> consumeCoords(message))
                .addConsumer(CommonMessageType.UNIT_SESSION.name(),
                             UnitSessionBundle.class,
                             (consumerTag, envelope, basicProperties, message) -> consumeSession(message))
                .connect(config.serviceConfig.connection.newConnection(), config.serviceConfig.connection.newConnection());

        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

    private void updateUnitTrMapping() {
        try (SqlSession session = SqlMapperSingleton.getInstance().openSession()) {
            LOGGER.info("Updating unit tr mapping");
            UnitTrMapperMapper mapper = session.getMapper(UnitTrMapperMapper.class);
            if (config.rnisDepotCode.isEmpty()) {
                unitTrMapping.set(mapper.getUnitTrLinks().stream().collect(Collectors.toMap(utl -> utl.unitId, utl -> utl.trId)));
            } else {
                unitTrMapping.set(mapper.getUnitTrLinksRnis(config.rnisDepotCode).stream().collect(Collectors.toMap(utl -> utl.unitId, utl -> utl.trId)));
            }

            LOGGER.info("Updated size [{}]", unitTrMapping.get().size());
        } catch (Throwable ex) {
            LOGGER.error("update unit tr mapping", ex);
        }
    }

    private void consumeCoords(UnitPacketsBundle unitPacketsBundle) {
        List<UnitPacket> convertedPackets = new ArrayList<>(unitPacketsBundle.getPackets().size());
        for (UnitPacket unitPacket : unitPacketsBundle.getPackets()) {
            UnitPacketBuilder unitPacketBuilder = UnitPacket.newBuilder().fromUnitPacket(unitPacket);
            Long trId = unitTrMapping.get().get(unitPacket.getUnitId());
            if (trId == null) {
                LOGGER.error("No existing unit -> tr link [{}]", unitPacket.getUnitId());
            } else {
                unitPacketBuilder.setPacketId(packetId.incrementAndGet());
                unitPacketBuilder.setTrId(trId);
                convertedPackets.add(unitPacketBuilder.build());
            }
        }
        if (!convertedPackets.isEmpty()) {
            sendUnitPacketsBundle(UnitPacketsBundle.of(unitPacketsBundle.getHubId(),
                                                       convertedPackets.toArray(new UnitPacket[convertedPackets.size()])));
        }
    }

    private void consumeSession(UnitSessionBundle unitSessionBundle) {
        List<UnitSession> convertedPackets = new ArrayList<>(unitSessionBundle.getPackets().size());
        for (UnitSession unitSession : unitSessionBundle.getPackets()) {
            Long trId = unitTrMapping.get().get(unitSession.getUnitId());
            if (trId == null) {
                LOGGER.error("No existing unit -> tr link [{}]", unitSession.getUnitId());
            } else {
                convertedPackets.add(
                        new UnitSession(unitSession.getHubId(),
                                unitSession.getUnitId(),
                                trId,
                                unitSession.getConnectTime(),
                                unitSession.getDisconnectTime(),
                                unitSession.getTotalPacketCnt(),
                                unitSession.getInvalidPacketCnt(),
                                unitSession.getHistPacketCnt(),
                                unitSession.getSessionId() ));
            }
        }
        if (!convertedPackets.isEmpty()) {
            sendUnitSessionBundle(UnitSessionBundle.of(unitSessionBundle.getHubId(),
                    convertedPackets.toArray(new UnitSession[convertedPackets.size()])));
        }
    }

    private void sendUnitPacketsBundle(UnitPacketsBundle unitPacketsBundle) {
        try {
            rmqTalker.basicPublish(config.serviceConfig.destination.exchange,
                                   routingKey == null ? Integer.toString(unitPacketsBundle.getHubId()) : routingKey,
                                   CommonMessageType.COORDS.name(),
                                   unitPacketsBundle);
        } catch (InterruptedException e) {
            LOGGER.error("sendUnitPacketsBundle", e);
        }
    }


    private void sendUnitSessionBundle(UnitSessionBundle unitSessionBundle) {
        try {
            final String sessionRoutingKey;
            if (config.sessionDestination.routingKey.isEmpty()) {
                sessionRoutingKey = Integer.toString(unitSessionBundle.getHubId());
            } else {
                sessionRoutingKey = config.sessionDestination.routingKey;
            }

            rmqTalker.basicPublish(config.sessionDestination.exchange,
                                   sessionRoutingKey,
                                   CommonMessageType.UNIT_SESSION.name(),
                                   unitSessionBundle);
        } catch (InterruptedException e) {
            LOGGER.error("sendUnitSessionBundle", e);
        }
    }

    private void stop() {
        rmqTalker.gentlyStopConsumer(10, 2);
        try {
            ses.shutdown();
            ses.awaitTermination(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            LOGGER.error("Can't stop send from queue");
        }
    }

    public static void main(String[] args) throws Exception {
        UnitTrMapperConfig config = Configuration.unpackConfig(UnitTrMapperConfig.class, "unit-tr-mapper.xml");
        new UnitTrMapper(config).start();
    }
}
