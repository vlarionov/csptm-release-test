package ru.advantum.csptm.jep.service.diagnostics.manager.dao;

import com.google.common.base.MoreObjects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.advantum.csptm.jep.service.diagnostics.manager.model.InvalidPackagesNumbersValidationConfig;
import ru.advantum.csptm.jep.service.diagnostics.manager.model.InvalidPackagesRateValidationConfig;
import ru.advantum.csptm.jep.service.diagnostics.manager.model.PackagesTimeRateValidationConfig;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Repository
public class ConfigDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigDao.class);

    private final JdbcTemplate jdbcTemplate;

    private volatile ConcurrentMap<Long, InvalidPackagesNumbersValidationConfig> invalidPackagesNumberValidationConfigs;
    private volatile ConcurrentMap<Long, InvalidPackagesRateValidationConfig> invalidPackagesRateValidationConfigs;
    private volatile ConcurrentMap<Long, PackagesTimeRateValidationConfig> packagesTimeRateValidationConfigs;

    private InvalidPackagesNumbersValidationConfig defaultInvalidPackagesNumbersValidationConfig;
    private InvalidPackagesRateValidationConfig defaultInvalidPackagesRateValidationConfig;
    private PackagesTimeRateValidationConfig defaultPackagesTimeRateValidationConfig;

    @Autowired
    public ConfigDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public InvalidPackagesNumbersValidationConfig findInvalidPackagesNumberValidationConfig(Long unitId) {
        return MoreObjects.firstNonNull(
                invalidPackagesNumberValidationConfigs.get(unitId),
                defaultInvalidPackagesNumbersValidationConfig
        );
    }

    public InvalidPackagesRateValidationConfig findInvalidPackagesRateValidationConfig(Long unitId) {
        return MoreObjects.firstNonNull(
                invalidPackagesRateValidationConfigs.get(unitId),
                defaultInvalidPackagesRateValidationConfig
        );
    }

    public PackagesTimeRateValidationConfig findPackagesTimeRateValidationConfig(Long unitId) {
        return MoreObjects.firstNonNull(
                packagesTimeRateValidationConfigs.get(unitId),
                defaultPackagesTimeRateValidationConfig
        );
    }

    @PostConstruct
    public void initDefaultConfigs() {
        defaultInvalidPackagesNumbersValidationConfig = jdbcTemplate.query(
                "SELECT * FROM kdbo.tel_conf_pkg$default_invalid_pkg_number_conf()",
                rs -> {
                    rs.next();
                    return new InvalidPackagesNumbersValidationConfig(
                            rs.getInt("invalid_packets_to_disable"),
                            rs.getInt("valid_packets_to_enable")
                    );
                }
        );

        defaultInvalidPackagesRateValidationConfig = jdbcTemplate.query(
                "SELECT * FROM kdbo.tel_conf_pkg$default_invalid_pkg_rate_conf()",
                rs -> {
                    rs.next();
                    return new InvalidPackagesRateValidationConfig(
                            rs.getInt("invalid_packets_number"),
                            rs.getInt("sampling_size")
                    );
                }
        );

        defaultPackagesTimeRateValidationConfig = jdbcTemplate.query(
                "SELECT * FROM kdbo.tel_conf_pkg$default_pkg_time_rate_conf()",
                rs -> {
                    rs.next();
                    return new PackagesTimeRateValidationConfig(
                            Duration.ofMinutes(rs.getInt("package_waiting_duration"))
                    );
                }
        );
    }

    public void updateConfigsSafely() {
        try {
            updateConfigs();
        } catch (Exception e) {
            LOGGER.error("Update config error", e);
        }
    }

    @PostConstruct
    public void updateConfigs() {
        LOGGER.info("Start configs update");

        invalidPackagesNumberValidationConfigs = jdbcTemplate.query(
                "SELECT * FROM kdbo.tel_conf_pkg$invalid_pkg_number_conf()",
                rs -> {
                    HashMap<Long, InvalidPackagesNumbersValidationConfig> result = new HashMap<>();
                    while (rs.next()) {
                        result.put(
                                rs.getLong("unit_id"),
                                new InvalidPackagesNumbersValidationConfig(
                                        rs.getInt("invalid_packets_to_disable"),
                                        rs.getInt("valid_packets_to_enable")
                                )
                        );
                    }
                    return new ConcurrentHashMap<>(result);
                }
        );

        invalidPackagesRateValidationConfigs = jdbcTemplate.query(
                "SELECT * FROM kdbo.tel_conf_pkg$invalid_pkg_rate_conf()",
                rs -> {
                    HashMap<Long, InvalidPackagesRateValidationConfig> result = new HashMap<>();
                    while (rs.next()) {
                        result.put(
                                rs.getLong("unit_id"),
                                new InvalidPackagesRateValidationConfig(
                                        rs.getInt("invalid_packets_number"),
                                        rs.getInt("sampling_size")
                                )
                        );
                    }
                    return new ConcurrentHashMap<>(result);
                }
        );
        packagesTimeRateValidationConfigs = jdbcTemplate.query(
                "SELECT * FROM kdbo.tel_conf_pkg$pkg_time_rate_conf()",
                rs -> {
                    HashMap<Long, PackagesTimeRateValidationConfig> result = new HashMap<>();
                    while (rs.next()) {
                        result.put(
                                rs.getLong("unit_id"),
                                new PackagesTimeRateValidationConfig(
                                        Duration.ofMinutes(rs.getInt("package_waiting_duration"))
                                )
                        );
                    }
                    return new ConcurrentHashMap<>(result);
                }
        );

        LOGGER.info("End of configs update");
    }

}