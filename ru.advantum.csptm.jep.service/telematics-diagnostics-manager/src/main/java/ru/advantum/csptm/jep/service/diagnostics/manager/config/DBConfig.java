package ru.advantum.csptm.jep.service.diagnostics.manager.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import ru.advantum.csptm.dbidgen.DbIdGenerator;
import ru.advantum.csptm.jep.service.diagnostics.manager.TelematicsDiagnosticsManagerConfig;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
public class DBConfig {
    @Bean
    public DataSource dataSource(TelematicsDiagnosticsManagerConfig config) throws SQLException {
        return config.database.getConnectionPool();
    }

    @Bean
    public DataSourceTransactionManager dataSourceTransactionManager(DataSource dataSource) {
        DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager();
        dataSourceTransactionManager.setDataSource(dataSource);
        return dataSourceTransactionManager;
    }

    @Bean
    public DbIdGenerator dbIdGenerator(TelematicsDiagnosticsManagerConfig config, DataSource dataSource) throws SQLException {
        return new DbIdGenerator(config.dbIdGeneratorConfig, dataSource);
    }
}
