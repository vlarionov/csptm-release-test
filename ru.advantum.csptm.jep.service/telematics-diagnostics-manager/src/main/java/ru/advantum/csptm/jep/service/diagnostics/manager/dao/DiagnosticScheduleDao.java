package ru.advantum.csptm.jep.service.diagnostics.manager.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;

@Repository
public class DiagnosticScheduleDao {
    private final JdbcTemplate jdbcTemplate;

    private volatile Map<Long, Instant> unitIdToDiagnosticStartTimeMap;

    public DiagnosticScheduleDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @PostConstruct
    public void init() {
        unitIdToDiagnosticStartTimeMap = jdbcTemplate.query(
                "SELECT * FROM kdbo.diagnostic_period_pkg$find_active()",
                rs -> {
                    HashMap<Long, Instant> result = new HashMap<>();
                    while (rs.next()) {
                        result.put(
                                rs.getLong("equipment_id"),
                                rs.getTimestamp("start_date").toInstant()
                        );
                    }
                    return result;
                }
        );
    }

    public Map<Long, Instant> getUnitIdToDiagnosticStartTimeMap() {
        return Collections.unmodifiableMap(unitIdToDiagnosticStartTimeMap);
    }

    public void setUnitIdToDiagnosticStartTimeMap(Map<Long, Instant> unitIdToDiagnosticStartTimeMap) {
        this.unitIdToDiagnosticStartTimeMap = unitIdToDiagnosticStartTimeMap;
    }

    public List<Long> nextDiangosedUnitsIds() {
        return jdbcTemplate.queryForList("SELECT * FROM kdbo.v_unit4diagnostic", Long.class);
    }

    public Instant getDiagnosticStartTime(long unitId) {
        return unitIdToDiagnosticStartTimeMap.get(unitId);
    }

    public void saveDiagnosticStart(long unitId, Instant startTime) {
        SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName("kdbo")
                .withProcedureName("diagnostic_period_pkg$start");
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("p_equipment_id", unitId)
                .addValue("p_start_date", Timestamp.from(startTime));
        call.execute(parameterSource);
    }

    public void saveDiagnosticEnds(Set<Long> unitIds) {
        Timestamp now = Timestamp.from(Instant.now());
        for (long unitId : unitIds) {
            SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate)
                    .withSchemaName("kdbo")
                    .withProcedureName("diagnostic_period_pkg$finish");
            SqlParameterSource parameterSource = new MapSqlParameterSource()
                    .addValue("p_equipment_id", unitId)
                    .addValue("p_end_date", now);
            call.execute(parameterSource);
        }
    }

}
