package ru.advantum.csptm.jep.service.diagnostics.manager.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;
import ru.advantum.csptm.jep.proto.UnitPacketsBundle;
import ru.advantum.csptm.jep.service.diagnostics.manager.TelematicsDiagnosticsManagerConfig;
import ru.advantum.csptm.spring.rmq.ConsumerProperties;
import ru.advantum.csptm.spring.rmq.MessageTypeResolver;
import ru.advantum.csptm.spring.rmq.MultiTypeResolver;

@Configuration
public class RmqConfig {
    @Bean
    public JepRmqServiceConfig jepRmqServiceConfig(TelematicsDiagnosticsManagerConfig config) {
        return config.rmqServiceConfig;
    }

    @Bean
    public MessageTypeResolver messageTypeResolver() {
        return MultiTypeResolver.newBuilder()
                .addPair("COORDS", UnitPacketsBundle.class)
                .build();
    }

    @Bean
    public ConsumerProperties defaultConsumerProperties() {
        return ConsumerProperties.newBuilder().setConcurrency(1).setMaxConcurrency(1).build();
    }
}
