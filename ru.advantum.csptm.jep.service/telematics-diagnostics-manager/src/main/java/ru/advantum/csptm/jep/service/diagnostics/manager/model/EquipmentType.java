package ru.advantum.csptm.jep.service.diagnostics.manager.model;

public enum  EquipmentType {
    BNST(2),BNSR(9), KBTOB(10);

    private final short id;

    EquipmentType(int id) {
        this.id = (short) id;
    }

    public static EquipmentType find(short id) {
        for (EquipmentType equipmentType : EquipmentType.values()) {
            if (equipmentType.id == id) {
                return equipmentType;
            }
        }
        return null;
    }
}
