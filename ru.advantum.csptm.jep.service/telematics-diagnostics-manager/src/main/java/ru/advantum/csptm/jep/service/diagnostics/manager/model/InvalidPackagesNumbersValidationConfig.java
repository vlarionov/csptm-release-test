package ru.advantum.csptm.jep.service.diagnostics.manager.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class InvalidPackagesNumbersValidationConfig implements Serializable {
    private final int invalidPacketsToDisable;

    private final int validPacketsToEnable;

    @JsonCreator
    public InvalidPackagesNumbersValidationConfig(
            @JsonProperty("invalidPacketsToDisable") int invalidPacketsToDisable,
            @JsonProperty("validPacketsToEnable") int validPacketsToEnable
    ) {
        this.invalidPacketsToDisable = invalidPacketsToDisable;
        this.validPacketsToEnable = validPacketsToEnable;
    }

    public int getInvalidPacketsToDisable() {
        return invalidPacketsToDisable;
    }

    public int getValidPacketsToEnable() {
        return validPacketsToEnable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InvalidPackagesNumbersValidationConfig that = (InvalidPackagesNumbersValidationConfig) o;

        if (invalidPacketsToDisable != that.invalidPacketsToDisable) return false;
        return validPacketsToEnable == that.validPacketsToEnable;
    }

    @Override
    public int hashCode() {
        int result = invalidPacketsToDisable;
        result = 31 * result + validPacketsToEnable;
        return result;
    }
}
