package ru.advantum.csptm.jep.service.diagnostics.manager.model;

public enum UnitDisablingReason {
    OK,
    INVALID_PACKAGES_NUMBER,
    INVALID_PACKAGES_RATE,
    PACKAGES_TIME_RATE
}
