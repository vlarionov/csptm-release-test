package ru.advantum.csptm.jep.service.diagnostics.manager.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class UnitStateDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(UnitStateDao.class);

    private final RedisMapDumper<Long, Boolean> mapDumper;

    /**
     * unitId -> isUnitValid
     */
    private volatile ConcurrentHashMap<Long, Boolean> unitStateMap = new ConcurrentHashMap<>();

    @Autowired
    public UnitStateDao(RedisMapDumper<Long, Boolean> mapDumper) {
        this.mapDumper = mapDumper;
        mapDumper.bindMap(unitStateMap, "unit-states", Long.class, Boolean.class);
    }

    @PostConstruct
    public void init() {
        mapDumper.restoreState();
    }

    public Boolean isValid(long unitId) {
        return unitStateMap.get(unitId);
    }

    public void updateStateMap(Map<Long, Boolean> newUnitStateMap) {
        LOGGER.info("Start update of unit state map, size {}", unitStateMap.size());

        unitStateMap = new ConcurrentHashMap<>(newUnitStateMap);

        mapDumper.changeMap(unitStateMap);
        mapDumper.dumpState();

        LOGGER.info("End update of unit state map, size {}", unitStateMap.size());
    }

}
