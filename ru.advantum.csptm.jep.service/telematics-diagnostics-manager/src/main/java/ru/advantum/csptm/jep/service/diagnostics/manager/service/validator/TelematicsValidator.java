package ru.advantum.csptm.jep.service.diagnostics.manager.service.validator;

import ru.advantum.csptm.jep.proto.UnitPacket;
import ru.advantum.csptm.jep.service.diagnostics.manager.model.EquipmentType;
import ru.advantum.csptm.jep.service.diagnostics.manager.model.UnitDisablingReason;

import java.util.List;

public interface TelematicsValidator {

    void validate(UnitPacket unitPacket);

    boolean isUnitValid(long unitId);

    UnitDisablingReason getUnitDisablingReason();

    List<EquipmentType> getEquipmentTypes();
}