package ru.advantum.csptm.jep.service.diagnostics.manager.service.validator;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.jep.proto.UnitPacket;
import ru.advantum.csptm.jep.service.diagnostics.manager.dao.ConfigDao;
import ru.advantum.csptm.jep.service.diagnostics.manager.dao.RedisDumpScheduler;
import ru.advantum.csptm.jep.service.diagnostics.manager.dao.RedisMapDumper;
import ru.advantum.csptm.jep.service.diagnostics.manager.model.EquipmentType;
import ru.advantum.csptm.jep.service.diagnostics.manager.model.InvalidPackagesRateValidationConfig;
import ru.advantum.csptm.jep.service.diagnostics.manager.model.UnitDisablingReason;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

@Service
public class InvalidPackagesRateValidator implements TelematicsValidator,
        RedisDumpScheduler.RedisDumpSubscriber<Long, InvalidPackagesRateValidator.LastPacketsSampling> {

    /**
     * unitId -> sampling of unit's last packets states
     */
    private final ConcurrentHashMap<Long, LastPacketsSampling> unitStateMap = new ConcurrentHashMap<>();

    private final ConfigDao configDao;

    private final RedisMapDumper<Long, LastPacketsSampling> mapDumper;

    @Autowired
    public InvalidPackagesRateValidator(ConfigDao configDao, RedisMapDumper<Long, LastPacketsSampling> mapDumper) {
        this.configDao = configDao;
        this.mapDumper = mapDumper;

        mapDumper.bindMap(unitStateMap, "invalid-packages-rate-validator-state", Long.class, LastPacketsSampling.class);
    }

    @Override
    public void validate(UnitPacket unitPacket) {
        if (unitPacket.getTelematic() == null) {
            return;
        }

        boolean isValid = unitPacket.getTelematic().isLocationValid();
        LastPacketsSampling lastPacketsSampling = getUnitLastPacketsSampling(unitPacket.getUnitId());

        lastPacketsSampling.add(isValid);
    }

    @Override
    public boolean isUnitValid(long unitId) {
        LastPacketsSampling lastPacketsSampling = unitStateMap.get(unitId);

        return lastPacketsSampling == null || lastPacketsSampling.isUnitValid();
    }

    @Override
    public UnitDisablingReason getUnitDisablingReason() {
        return UnitDisablingReason.INVALID_PACKAGES_RATE;
    }

    private LastPacketsSampling getUnitLastPacketsSampling(long unitId) {
        LastPacketsSampling lastPacketsSampling = unitStateMap.get(unitId);
        InvalidPackagesRateValidationConfig config = configDao.findInvalidPackagesRateValidationConfig(unitId);

        if (lastPacketsSampling == null || !lastPacketsSampling.config.equals(config)) {
            lastPacketsSampling = new LastPacketsSampling(config);
            unitStateMap.put(unitId, lastPacketsSampling);
        }

        return lastPacketsSampling;
    }

    @Override
    public RedisMapDumper<Long, LastPacketsSampling> getDumper() {
        return mapDumper;
    }

    @Override
    public List<EquipmentType> getEquipmentTypes() {
        return Arrays.asList(
                EquipmentType.BNST,
                EquipmentType.BNSR
        );
    }

    public static class LastPacketsSampling implements Serializable {

        private int invalidPacketsCount;

        private final Queue<Boolean> packetsStatesSequence;

        private final InvalidPackagesRateValidationConfig config;

        private LastPacketsSampling(InvalidPackagesRateValidationConfig config) {
            this.config = config;
            this.packetsStatesSequence = new ConcurrentLinkedQueue<>();
        }

        @JsonCreator
        public LastPacketsSampling(
                @JsonProperty("invalidPacketsCount") int invalidPacketsCount,
                @JsonProperty("packetsStatesSequence") Queue<Boolean> packetsStatesSequence,
                @JsonProperty("config") InvalidPackagesRateValidationConfig config
        ) {
            this.invalidPacketsCount = invalidPacketsCount;
            this.packetsStatesSequence = new ConcurrentLinkedQueue<>(packetsStatesSequence);
            this.config = config;
        }

        synchronized void add(Boolean packetValid) {
            if (packetsStatesSequence.size() >= config.getSamplingSize()) {
                boolean isRemovedValid = packetsStatesSequence.remove();
                if (!isRemovedValid) {
                    invalidPacketsCount--;
                }
            }
            if (!packetValid) {
                invalidPacketsCount++;
            }

            packetsStatesSequence.add(packetValid);

        }

        synchronized boolean isUnitValid() {
            return packetsStatesSequence.size() < config.getSamplingSize() || invalidPacketsCount <= config.getInvalidPacketsNumber();
        }

        public int getInvalidPacketsCount() {
            return invalidPacketsCount;
        }

        public Queue<Boolean> getPacketsStatesSequence() {
            return packetsStatesSequence;
        }

        public InvalidPackagesRateValidationConfig getConfig() {
            return config;
        }

    }
}
