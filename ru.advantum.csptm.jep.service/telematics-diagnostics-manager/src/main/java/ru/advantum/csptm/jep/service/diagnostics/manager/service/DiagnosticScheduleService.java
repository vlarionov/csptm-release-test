package ru.advantum.csptm.jep.service.diagnostics.manager.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.advantum.csptm.jep.service.diagnostics.manager.dao.DiagnosticScheduleDao;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class DiagnosticScheduleService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DiagnosticScheduleService.class);

    private final DiagnosticScheduleDao diagnosticScheduleDao;

    @Autowired
    public DiagnosticScheduleService(DiagnosticScheduleDao diagnosticScheduleDao) {
        this.diagnosticScheduleDao = diagnosticScheduleDao;
    }

    @Transactional
    public Set<Long> updateAndGetDiangosedNowUnitsIds() {
        Map<Long, Instant> unitIdToDiagnosticStartTimeMap = diagnosticScheduleDao.getUnitIdToDiagnosticStartTimeMap();

        LOGGER.info("Start update of diagnosed now units, size {}", unitIdToDiagnosticStartTimeMap.size());

        List<Long> nextDiangosedUnitsIds = diagnosticScheduleDao.nextDiangosedUnitsIds();

        Map<Long, Instant> newUnitIdToDiagnosticStartTimeMap = new HashMap<>(nextDiangosedUnitsIds.size());
        for (long unitId : nextDiangosedUnitsIds) {
            Instant unitDiagnosticStartTime = unitIdToDiagnosticStartTimeMap.get(unitId);
            if (unitDiagnosticStartTime == null) {
                unitDiagnosticStartTime = Instant.now();
                diagnosticScheduleDao.saveDiagnosticStart(unitId, unitDiagnosticStartTime);
            }
            newUnitIdToDiagnosticStartTimeMap.put(unitId, unitDiagnosticStartTime);
        }

        Map<Long, Instant> oldUnitIdToDiagnosticStartTimeMap = new HashMap<>(unitIdToDiagnosticStartTimeMap);
        oldUnitIdToDiagnosticStartTimeMap.keySet().removeAll(newUnitIdToDiagnosticStartTimeMap.keySet());
        diagnosticScheduleDao.saveDiagnosticEnds(oldUnitIdToDiagnosticStartTimeMap.keySet());

        diagnosticScheduleDao.setUnitIdToDiagnosticStartTimeMap(newUnitIdToDiagnosticStartTimeMap);

        LOGGER.info("End update of diagnosed now units, size {}", unitIdToDiagnosticStartTimeMap.size());

        return unitIdToDiagnosticStartTimeMap.keySet();
    }
}
