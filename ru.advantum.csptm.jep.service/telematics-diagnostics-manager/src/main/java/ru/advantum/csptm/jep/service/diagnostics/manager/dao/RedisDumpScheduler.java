package ru.advantum.csptm.jep.service.diagnostics.manager.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class RedisDumpScheduler {
    private static final String MEMORY_DUMP_RATE_KEY = "${telematics-diagnostics-manager.memoryDumpRate}";

    private static final Logger LOGGER = LoggerFactory.getLogger(RedisDumpScheduler.class);

    private final List<RedisMapDumper<?, ?>> redisMapDumpers;

    @Autowired
    public RedisDumpScheduler(List<RedisDumpSubscriber<?, ?>> redisDumpSubscribers) {
        this.redisMapDumpers = redisDumpSubscribers.stream().map(RedisDumpSubscriber::getDumper).collect(Collectors.toList());
    }

    @PostConstruct
    public void init() {
        redisMapDumpers.forEach(RedisMapDumper::restoreState);
    }

    @Scheduled(fixedDelayString = MEMORY_DUMP_RATE_KEY, initialDelayString = MEMORY_DUMP_RATE_KEY)
    public void dumpMaps() {
        try {
            LOGGER.info("Start of redis maps dump");
            redisMapDumpers.forEach(RedisMapDumper::dumpState);
            LOGGER.info("End of redis maps dump");
        } catch (Exception e) {
            LOGGER.error("", e);
        }
    }

    public interface RedisDumpSubscriber<K,V> {
        RedisMapDumper<K, V> getDumper();
    }

}
