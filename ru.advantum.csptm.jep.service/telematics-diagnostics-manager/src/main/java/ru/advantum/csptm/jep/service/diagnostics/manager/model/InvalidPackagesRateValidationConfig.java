package ru.advantum.csptm.jep.service.diagnostics.manager.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class InvalidPackagesRateValidationConfig implements Serializable {
    private final int invalidPacketsNumber;

    private final int samplingSize;

    @JsonCreator
    public InvalidPackagesRateValidationConfig(
            @JsonProperty("invalidPacketsNumber") int invalidPacketsNumber,
            @JsonProperty("samplingSize") int samplingSize
    ) {
        this.invalidPacketsNumber = invalidPacketsNumber;
        this.samplingSize = samplingSize;
    }

    public int getInvalidPacketsNumber() {
        return invalidPacketsNumber;
    }

    public int getSamplingSize() {
        return samplingSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InvalidPackagesRateValidationConfig config = (InvalidPackagesRateValidationConfig) o;

        if (invalidPacketsNumber != config.invalidPacketsNumber) return false;
        return samplingSize == config.samplingSize;
    }

    @Override
    public int hashCode() {
        int result = invalidPacketsNumber;
        result = 31 * result + samplingSize;
        return result;
    }
}
