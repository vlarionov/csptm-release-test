package ru.advantum.csptm.jep.service.diagnostics.manager.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.advantum.csptm.jep.service.diagnostics.manager.model.EquipmentType;

import java.util.HashMap;
import java.util.Map;

@Repository
public class UnitDao {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UnitDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Cacheable("unitIdToTrIdMap")
    public Map<Long, Long> getUnitIdToTrIdMap() {
        return jdbcTemplate.query(
                "SELECT u2t.unit_id, u2t.tr_id FROM core.unit2tr u2t WHERE u2t.sys_period @> now()",
                rs -> {
                    HashMap<Long, Long> result = new HashMap<>();
                    while (rs.next()) {
                        result.put(
                                rs.getLong("unit_id"),
                                rs.getLong("tr_id")
                        );
                    }
                    return result;
                });
    }

    @Cacheable("unitIdToEquipmentTypeIdMap")
    public Map<Long, EquipmentType> getUnitIdToEquipmentTypeIdMap() {
        return jdbcTemplate.query(
                "SELECT * FROM kdbo.telematics_diagnostics_pkg$get_unit2eq_type()",
                rs -> {
                    HashMap<Long, EquipmentType> result = new HashMap<>();
                    while (rs.next()) {
                        EquipmentType equipmentType = EquipmentType.find(
                                rs.getShort("equipment_type_id")
                        );
                        if (equipmentType != null) {
                            result.put(rs.getLong("unit_id"), equipmentType);
                        }
                    }
                    return result;
                });
    }

    @Transactional
    public void updateBNSRRequestList(long unitId, boolean isUnitEnabled) {
        SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName("kdbo")
                .withProcedureName("telematics_diagnostics_pkg$update_bnsr_request_list");
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("p_unit_id", unitId)
                .addValue("p_is_unit_enabled", isUnitEnabled);
        call.execute(parameterSource);
    }
}
