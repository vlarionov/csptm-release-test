package ru.advantum.csptm.jep.service.diagnostics.manager.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.Duration;

public class PackagesTimeRateValidationConfig implements Serializable {
    private final Duration packageWaitingDuration;

    @JsonCreator
    public PackagesTimeRateValidationConfig(@JsonProperty("packageWaitingDuration") Duration packageWaitingDuration) {
        this.packageWaitingDuration = packageWaitingDuration;
    }

    public Duration getPackageWaitingDuration() {
        return packageWaitingDuration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PackagesTimeRateValidationConfig that = (PackagesTimeRateValidationConfig) o;

        return packageWaitingDuration != null ? packageWaitingDuration.equals(that.packageWaitingDuration) : that.packageWaitingDuration == null;
    }

    @Override
    public int hashCode() {
        return packageWaitingDuration != null ? packageWaitingDuration.hashCode() : 0;
    }
}
