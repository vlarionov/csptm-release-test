package ru.advantum.csptm.jep.service.diagnostics.manager.service;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.jep.proto.CommonPacketFlag;
import ru.advantum.csptm.jep.proto.UnitPacket;
import ru.advantum.csptm.jep.proto.UnitPacketsBundle;
import ru.advantum.csptm.jep.service.diagnostics.manager.service.validator.TelematicsValidator;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TelematicsReceiver {
    private final List<TelematicsValidator> validators;

    private final Map<Long, Instant> lastUnitsPacketsTimes = new HashMap<>();

    @Autowired
    public TelematicsReceiver(List<TelematicsValidator> validators) {
        this.validators = validators;
    }


    @RabbitListener
    public synchronized void receive(UnitPacketsBundle unitPacketsBundle) {
        unitPacketsBundle.getPackets().forEach(this::receive);
    }

    private void receive(UnitPacket unitPacket) {
        if (unitPacket.getFlags() != null && unitPacket.getFlags().contains(CommonPacketFlag.HIST_DATA.name())) {
            return;
        }

        if (isLatecomer(unitPacket)) {
            return;
        }

        lastUnitsPacketsTimes.put(unitPacket.getUnitId(), unitPacket.getEventTime());

        validators.forEach(validator -> validator.validate(unitPacket));
    }

    private boolean isLatecomer(UnitPacket unitPacket) {
        Instant lastUnitPacketTime = lastUnitsPacketsTimes.get(unitPacket.getUnitId());
        return lastUnitPacketTime != null && unitPacket.getEventTime().isBefore(lastUnitPacketTime);
    }

}
