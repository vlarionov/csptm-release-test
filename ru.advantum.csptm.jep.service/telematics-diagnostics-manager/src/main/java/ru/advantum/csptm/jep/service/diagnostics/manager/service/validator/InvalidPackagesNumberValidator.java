package ru.advantum.csptm.jep.service.diagnostics.manager.service.validator;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.jep.proto.UnitPacket;
import ru.advantum.csptm.jep.service.diagnostics.manager.dao.ConfigDao;
import ru.advantum.csptm.jep.service.diagnostics.manager.dao.RedisDumpScheduler;
import ru.advantum.csptm.jep.service.diagnostics.manager.dao.RedisMapDumper;
import ru.advantum.csptm.jep.service.diagnostics.manager.model.EquipmentType;
import ru.advantum.csptm.jep.service.diagnostics.manager.model.InvalidPackagesNumbersValidationConfig;
import ru.advantum.csptm.jep.service.diagnostics.manager.model.UnitDisablingReason;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class InvalidPackagesNumberValidator implements TelematicsValidator,
        RedisDumpScheduler.RedisDumpSubscriber<Long, InvalidPackagesNumberValidator.PacketsStatesCounter> {
    /**
     * unitId -> last valid and invalid packets numbers
     */
    private final ConcurrentHashMap<Long, PacketsStatesCounter> unitStateMap = new ConcurrentHashMap<>();

    private final ConfigDao configDao;

    private final RedisMapDumper<Long, PacketsStatesCounter> mapDumper;

    @Autowired
    public InvalidPackagesNumberValidator(
            ConfigDao configDao,
            RedisMapDumper<Long, PacketsStatesCounter> mapDumper
    ) {
        this.configDao = configDao;
        this.mapDumper = mapDumper;
        mapDumper.bindMap(unitStateMap, "invalid-packages-number-validator-state", Long.class, PacketsStatesCounter.class);
    }

    @Override
    public void validate(UnitPacket unitPacket) {
        if (unitPacket.getTelematic() == null) {
            return;
        }

        long unitId = unitPacket.getUnitId();
        PacketsStatesCounter counter = getUnitStatesCounter(unitId);

        boolean isValid = unitPacket.getTelematic().isLocationValid();
        counter.add(isValid);
    }

    @Override
    public boolean isUnitValid(long unitId) {
        PacketsStatesCounter counter = unitStateMap.get(unitId);
        return counter == null || counter.isUnitValid();
    }

    @Override
    public UnitDisablingReason getUnitDisablingReason() {
        return UnitDisablingReason.INVALID_PACKAGES_NUMBER;
    }

    @Override
    public List<EquipmentType> getEquipmentTypes() {
        return Arrays.asList(
                EquipmentType.BNST,
                EquipmentType.BNSR
        );
    }

    private PacketsStatesCounter getUnitStatesCounter(long unitId) {
        PacketsStatesCounter counter = unitStateMap.get(unitId);
        InvalidPackagesNumbersValidationConfig config = configDao.findInvalidPackagesNumberValidationConfig(unitId);

        if (counter == null || !counter.config.equals(config)) {
            counter = new PacketsStatesCounter(config);
            unitStateMap.put(unitId, counter);
        }

        return counter;
    }

    @Override
    public RedisMapDumper<Long, PacketsStatesCounter> getDumper() {
        return mapDumper;
    }

    public static class PacketsStatesCounter implements Serializable {

        private int invalidCount;

        private int validCount;

        private boolean unitValid = true;

        private final InvalidPackagesNumbersValidationConfig config;

        public PacketsStatesCounter(InvalidPackagesNumbersValidationConfig config) {
            this.config = config;
        }

        @JsonCreator
        public PacketsStatesCounter(
                @JsonProperty("invalidCount") int invalidCount,
                @JsonProperty("validCount") int validCount,
                @JsonProperty("unitValid") boolean unitValid,
                @JsonProperty("config") InvalidPackagesNumbersValidationConfig config
        ) {
            this.invalidCount = invalidCount;
            this.validCount = validCount;
            this.unitValid = unitValid;
            this.config = config;
        }

        synchronized void add(boolean packetValid) {
            if (packetValid) {
                invalidCount = 0;
                validCount++;
            } else {
                invalidCount++;
                validCount = 0;
            }

            if (unitValid) {
                unitValid = invalidCount <= config.getInvalidPacketsToDisable();
            } else {
                unitValid = validCount >= config.getValidPacketsToEnable();
            }
        }

        public int getInvalidCount() {
            return invalidCount;
        }

        public int getValidCount() {
            return validCount;
        }

        public synchronized boolean isUnitValid() {
            return unitValid;
        }

        public InvalidPackagesNumbersValidationConfig getConfig() {
            return config;
        }

    }


}

