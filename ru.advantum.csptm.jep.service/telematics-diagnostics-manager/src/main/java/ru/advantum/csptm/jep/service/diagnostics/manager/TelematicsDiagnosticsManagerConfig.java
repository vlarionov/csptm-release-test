package ru.advantum.csptm.jep.service.diagnostics.manager;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.config.common.db.RedisClientConfig;
import ru.advantum.csptm.dbidgen.DbIdGeneratorConfig;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;


@Root(name = "telematics-diagnostics-manager")
public class TelematicsDiagnosticsManagerConfig {
    @Element(name = "rmq-service")
    public JepRmqServiceConfig rmqServiceConfig;

    @Element(name = "redis-client")
    public RedisClientConfig redisConfig;

    @Element
    public DatabaseConnection database;

    @Element(name = "db-id-generator")
    public DbIdGeneratorConfig dbIdGeneratorConfig;

    @Attribute
    public long unitCheckRate;

    @Attribute
    public long memoryDumpRate;
}
