package ru.advantum.csptm.jep.service.diagnostics.manager.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.dbidgen.DbIdGenerator;
import ru.advantum.csptm.incident.model.Incident;
import ru.advantum.csptm.incident.model.IncidentBundle;
import ru.advantum.csptm.jep.service.diagnostics.manager.dao.ConfigDao;
import ru.advantum.csptm.jep.service.diagnostics.manager.dao.UnitDao;
import ru.advantum.csptm.jep.service.diagnostics.manager.dao.UnitStateDao;
import ru.advantum.csptm.jep.service.diagnostics.manager.model.EquipmentType;
import ru.advantum.csptm.jep.service.diagnostics.manager.model.UnitDisablingReason;
import ru.advantum.csptm.jep.service.diagnostics.manager.service.validator.TelematicsValidator;
import ru.advantum.csptm.spring.rmq.TypeAdder;
import ru.advantum.csptm.incident.model.IncidentType;
import java.time.Instant;
import java.util.*;

@Service
public class UnitStateUpdater {
    private static final Logger LOGGER = LoggerFactory.getLogger(UnitStateUpdater.class);

    private final RabbitTemplate rabbitTemplate;

    private final DbIdGenerator dbIdGenerator;

    private final UnitStateDao unitStateDao;

    private final UnitDao unitDao;

    private final ConfigDao configDao;

    private final DiagnosticScheduleService diagnosticScheduleService;

    private final List<TelematicsValidator> validators;

    @Autowired
    public UnitStateUpdater(
            RabbitTemplate rabbitTemplate,
            DbIdGenerator dbIdGenerator,
            UnitStateDao unitStateDao,
            UnitDao unitDao,
            ConfigDao configDao,
            DiagnosticScheduleService diagnosticScheduleService,
            List<TelematicsValidator> validators
    ) {
        this.rabbitTemplate = rabbitTemplate;
        this.dbIdGenerator = dbIdGenerator;
        this.unitStateDao = unitStateDao;
        this.unitDao = unitDao;
        this.configDao = configDao;
        this.diagnosticScheduleService = diagnosticScheduleService;
        this.validators = validators;
    }

    @Scheduled(fixedRateString = "${telematics-diagnostics-manager.unitCheckRate}")
    public void updateUnitsStates() {
        try {
            configDao.updateConfigsSafely();

            Map<Long, Boolean> resultStates = new HashMap<>();
            Map<Long, Long> unitIdToTrIdMap = unitDao.getUnitIdToTrIdMap();
            for (Long unitId : diagnosticScheduleService.updateAndGetDiangosedNowUnitsIds()) {
                Long trId = unitIdToTrIdMap.get(unitId);
                if (trId != null) {
                    updateUnitsState(unitId, trId, resultStates);
                }
            }
            unitStateDao.updateStateMap(resultStates);
        } catch (Exception e) {
            LOGGER.error("", e);
        }

    }

    private void updateUnitsState(long unitId, long trId, Map<Long, Boolean> resultStates) {
        Boolean isPreviousStateValid = unitStateDao.isValid(unitId);

        if (isPreviousStateValid == null) {
            resultStates.put(unitId, true);
            return;
        }

        boolean isCurrentStateValid = true;
        UnitDisablingReason disablingReason = UnitDisablingReason.OK;

        EquipmentType equipmentType = unitDao.getUnitIdToEquipmentTypeIdMap().get(unitId);
        if (equipmentType == null) {
            LOGGER.warn("Unknown equipment type, unitId: {}", unitId);
            return;
        }

        for (TelematicsValidator validator : validators) {
            if (validator.getEquipmentTypes().contains(equipmentType)) {
                if (!validator.isUnitValid(unitId)) {
                    isCurrentStateValid = false;
                    disablingReason = validator.getUnitDisablingReason();
                    break;
                }
            }
        }

        if (isPreviousStateValid != isCurrentStateValid) {
            LOGGER.info("unit state changed: unitId: {}, isValid: {}, reason: {}", unitId, isCurrentStateValid, disablingReason);
            unitDao.updateBNSRRequestList(unitId, isCurrentStateValid);
            sendMonitoringEvent(unitId, trId, disablingReason);
        }

        resultStates.put(unitId, isCurrentStateValid);
    }

    private void sendMonitoringEvent(long unitId, long trId, UnitDisablingReason disablingReason) {
        final IncidentBundle incidentBundle = IncidentBundle.builder().incidentBundle(Collections.singletonList(
                Incident.builder()
                        .incidentId(dbIdGenerator.nextId())
                        .incidentTypeId(disablingReasonToEventType(disablingReason).get())
                        .timeStart(Instant.now())
                        .trId(trId)
                        .unitId(unitId)
                        .incidentStatusId(Incident.STATUS.NEW.get())
                        .incidentInitiatorId(Incident.INITIATOR.SYSTEM.get())
                        .build())).build();
        rabbitTemplate.convertAndSend(Incident.INCIDENT_ROUTER_EXCHANGE
                , Short.toString(disablingReasonToEventType(disablingReason).get())
                , incidentBundle
                , TypeAdder.of(IncidentBundle.class.getSimpleName())
        );
    }

    private IncidentType.TYPE disablingReasonToEventType(UnitDisablingReason disablingReason) {
        switch (disablingReason) {
            case OK:
                return IncidentType.TYPE.OK;
            case INVALID_PACKAGES_NUMBER:
                return IncidentType.TYPE.UNIT_DISABLED_BY_INVALID_PACKAGES_NUMBERS;
            case INVALID_PACKAGES_RATE:
                return IncidentType.TYPE.UNIT_DISABLED_BY_INVALID_PACKAGES_RATE;
            case PACKAGES_TIME_RATE:
                return IncidentType.TYPE.UNIT_DISABLED_BY_PACKAGES_TIME_RATE;
            default:
                throw new IllegalArgumentException("Unknown reason: " + disablingReason);
        }
    }


}
