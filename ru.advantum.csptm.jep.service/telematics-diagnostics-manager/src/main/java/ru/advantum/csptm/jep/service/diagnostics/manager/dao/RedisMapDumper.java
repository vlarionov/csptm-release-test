package ru.advantum.csptm.jep.service.diagnostics.manager.dao;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.type.MapLikeType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RedisMapDumper<K, V> {

    private final RedisTemplate<String, Map<K, V>> redisTemplate;

    private BoundValueOperations<String, Map<K, V>> redisOperations;

    private volatile Map<K, V> map;

    @Autowired
    public RedisMapDumper(RedisTemplate<String, Map<K, V>> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public void bindMap(ConcurrentHashMap<K, V> map, String redisKey, Class<K> keyClass, Class<V> valueClass) {
        bindMap(map, redisKey, TypeFactory.defaultInstance().constructMapLikeType(HashMap.class, keyClass, valueClass));
    }

    public void bindMap(ConcurrentHashMap<K, V> map, String redisKey, JavaType keyType, JavaType valueType) {
        bindMap(map, redisKey, TypeFactory.defaultInstance().constructMapLikeType(HashMap.class, keyType, valueType));
    }

    public void bindMap(ConcurrentHashMap<K, V> map, String redisKey, MapLikeType mapType) {
        this.map = map;

        redisTemplate.setKeySerializer(new StringRedisSerializer());

        Jackson2JsonRedisSerializer valueSerializer = new Jackson2JsonRedisSerializer(mapType);
        ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();
        valueSerializer.setObjectMapper(mapper);
        redisTemplate.setValueSerializer(valueSerializer);
        redisOperations = redisTemplate.boundValueOps(redisKey);
    }

    public void changeMap(ConcurrentHashMap<K, V> map) {
        Assert.notNull(this.map, "Map should already be set");
        this.map = map;
    }

    public void restoreState() {
        Map<K, V> mapFromRedis = redisOperations.get();
        if (mapFromRedis == null) {
            mapFromRedis = Collections.emptyMap();
        }
        map.putAll(mapFromRedis);
    }

    public void dumpState() {
        redisOperations.set(map);
    }

}