package ru.advantum.csptm.jep.service.diagnostics.manager.service.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.jep.proto.UnitPacket;
import ru.advantum.csptm.jep.service.diagnostics.manager.dao.ConfigDao;
import ru.advantum.csptm.jep.service.diagnostics.manager.dao.DiagnosticScheduleDao;
import ru.advantum.csptm.jep.service.diagnostics.manager.dao.RedisDumpScheduler;
import ru.advantum.csptm.jep.service.diagnostics.manager.dao.RedisMapDumper;
import ru.advantum.csptm.jep.service.diagnostics.manager.model.EquipmentType;
import ru.advantum.csptm.jep.service.diagnostics.manager.model.UnitDisablingReason;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class PackagesTimeRateValidator implements TelematicsValidator, RedisDumpScheduler.RedisDumpSubscriber<Long, Instant> {

    /**
     * unitId -> unit's last packet time
     */
    private final ConcurrentHashMap<Long, Instant> lastUnitsPacketsTimes = new ConcurrentHashMap<>();

    private final ConfigDao configDao;

    private final RedisMapDumper<Long, Instant> mapDumper;

    private final DiagnosticScheduleDao diagnosticScheduleDao;

    @Autowired
    public PackagesTimeRateValidator(ConfigDao configDao, RedisMapDumper<Long, Instant> mapDumper, DiagnosticScheduleDao diagnosticScheduleDao) {
        this.configDao = configDao;
        this.mapDumper = mapDumper;
        this.diagnosticScheduleDao = diagnosticScheduleDao;

        mapDumper.bindMap(lastUnitsPacketsTimes, "packages-time-rate-validator-state", Long.class, Instant.class);
    }

    @Override
    public void validate(UnitPacket unitPacket) {
        lastUnitsPacketsTimes.put(unitPacket.getUnitId(), unitPacket.getEventTime());
    }

    @Override
    public boolean isUnitValid(long unitId) {
        Instant lastUnitTime = lastUnitsPacketsTimes.get(unitId);
        Instant diagnosticStartTime = diagnosticScheduleDao.getDiagnosticStartTime(unitId);

        if (lastUnitTime == null && diagnosticStartTime == null) {
            return false;
        }

        if (lastUnitTime == null || ((diagnosticStartTime != null && lastUnitTime.isBefore(diagnosticStartTime)))) {
            lastUnitTime = diagnosticStartTime;
        }

        Duration packageWaitingDuration = configDao.findPackagesTimeRateValidationConfig(unitId).getPackageWaitingDuration();

        return lastUnitTime.plus(packageWaitingDuration).isAfter(Instant.now());
    }

    @Override
    public UnitDisablingReason getUnitDisablingReason() {
        return UnitDisablingReason.PACKAGES_TIME_RATE;
    }

    @Override
    public RedisMapDumper<Long, Instant> getDumper() {
        return mapDumper;
    }

    @Override
    public List<EquipmentType> getEquipmentTypes() {
        return Arrays.asList(
                EquipmentType.BNST,
                EquipmentType.BNSR,
                EquipmentType.KBTOB
        );
    }
}
