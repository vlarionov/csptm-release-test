package ru.advantum.csptm.jep.service.diagnostics.manager;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.advantum.csptm.spring.properties.AbstractPropertySourceFactory;

import static ru.advantum.csptm.jep.service.diagnostics.manager.Application.CONFIG_FILE_NAME;

@SpringBootApplication
@EnableScheduling
@PropertySource(value = "", name = CONFIG_FILE_NAME, factory = Application.ConfigPropertySourceFactory.class)
public class Application {
    public static final String CONFIG_FILE_NAME = "telematics-diagnostics-manager.xml";

    public static void main(String[] args) {
        new SpringApplicationBuilder(Application.class)
                .web(false)
                .bannerMode(Banner.Mode.LOG)
                .run(args);
    }

    @Bean
    public static TelematicsDiagnosticsManagerConfig applicationConfig() throws Exception {
        return ru.advantum.config.Configuration.unpackConfig(
                TelematicsDiagnosticsManagerConfig.class,
                CONFIG_FILE_NAME
        );
    }

    public static class ConfigPropertySourceFactory extends AbstractPropertySourceFactory<TelematicsDiagnosticsManagerConfig> {
        @Override
        public TelematicsDiagnosticsManagerConfig getConfig() throws Exception {
            return applicationConfig();
        }
    }
}
