package ru.advantum.csptm.jep.service.glinker.offline;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import ru.advantum.csptm.spring.rmq.ConsumerProperties;
import ru.advantum.csptm.common.RmqServiceConfig;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
@ComponentScan(basePackages = "ru.advantum.csptm.jep.service.glinker")
@MapperScan(basePackages = "ru.advantum.csptm.jep.service.glinker")
@Import({RmqServiceConfig.class})
@EnableEurekaClient
public class Application {

    @Bean
    public ConsumerProperties consumerProperties() {
        return ConsumerProperties.newBuilder()
                .setAcknowledgeMode(AcknowledgeMode.AUTO)
                .setShutdownTimeout(TimeUnit.SECONDS.toMillis(10))
                .build();
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
