package ru.advantum.csptm.jep.service.glinker.offline.db;

import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by kaganov on 23/01/2017.
 */
public interface GraphLinkerOfflineMapper {

    List<CoordinateDTO> getCoordinates(@Param("tr_id") long trId,
                                       @Param("begin_date") LocalDateTime beginDate,
                                       @Param("end_date") LocalDateTime endDate);

    String cleanLinks(@Param("order_date") LocalDate orderDate,
                      @Param("tr_id") long trId);
}
