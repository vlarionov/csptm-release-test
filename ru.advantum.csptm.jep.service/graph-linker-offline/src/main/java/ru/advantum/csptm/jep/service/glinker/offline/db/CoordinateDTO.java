package ru.advantum.csptm.jep.service.glinker.offline.db;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * Created by kaganov on 26/01/2017.
 */
@Getter
@NoArgsConstructor(force = true)
public class CoordinateDTO {

    private final long packetId;
    private final LocalDateTime eventTime;
    private final long trId;
    private final long unitId;
    private final boolean locationValid;
    private final Float lon;
    private final Float lat;
    private final Integer speed;
}
