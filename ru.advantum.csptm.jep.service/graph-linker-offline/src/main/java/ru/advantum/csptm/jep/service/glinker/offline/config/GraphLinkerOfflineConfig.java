package ru.advantum.csptm.jep.service.glinker.offline.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Validated
@Component
@ConfigurationProperties(prefix = "graph-linker-offline")
public class GraphLinkerOfflineConfig {

    /**
     * Время до принудительной остановки service.sh
     */
    private int timeoutSec = 0;

    public void setTimeoutSec(int timeoutSec) {
        this.timeoutSec = timeoutSec;
    }

    public int getTimeoutSec() {
        return timeoutSec;
    }
}
