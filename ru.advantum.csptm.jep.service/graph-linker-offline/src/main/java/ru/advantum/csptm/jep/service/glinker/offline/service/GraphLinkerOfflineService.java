package ru.advantum.csptm.jep.service.glinker.offline.service;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.jep.service.glinker.config.PersimmonConfig;
import ru.advantum.csptm.jep.service.glinker.db.TTStorageAccessor;
import ru.advantum.csptm.jep.service.glinker.linkers.GraphLinkerController;
import ru.advantum.csptm.jep.service.glinker.linkers.listener.EventCollector;
import ru.advantum.csptm.jep.service.glinker.linkers.listener.EventSender;
import ru.advantum.csptm.jep.service.glinker.model.Timetable;
import ru.advantum.csptm.jep.service.glinker.model.TtAction;
import ru.advantum.csptm.jep.service.glinker.model.event.SimpleUnitPacket;
import ru.advantum.csptm.jep.service.glinker.offline.config.GraphLinkerOfflineConfig;
import ru.advantum.csptm.jep.service.glinker.offline.db.CoordinateDTO;
import ru.advantum.csptm.jep.service.glinker.offline.db.GraphLinkerOfflineMapper;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GraphLinkerOfflineService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GraphLinkerOfflineService.class);

    private final GraphLinkerOfflineConfig config;
    private final TTStorageAccessor storageAccessor;
    private final GraphLinkerOfflineMapper mapper;
    private final EventSender eventSender;
    private final PersimmonConfig persimmonConfig;
    private GraphLinkerController linkerController;
    private EventCollector eventCollector;

    @Autowired
    public GraphLinkerOfflineService(GraphLinkerOfflineConfig config,
                                     TTStorageAccessor storageAccessor,
                                     // мапперы intellij idea не понимает
                                     @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") GraphLinkerOfflineMapper mapper,
                                     EventSender eventSender,
                                     PersimmonConfig persimmonConfig,
                                     GraphLinkerController linkerController,
                                     EventCollector eventCollector) {
        this.config = config;
        this.storageAccessor = storageAccessor;
        this.mapper = mapper;
        this.eventSender = eventSender;
        this.persimmonConfig = persimmonConfig;
        this.linkerController = linkerController;
        this.eventCollector = eventCollector;
    }

    @PostConstruct
    public void start() {
        LOGGER.info("Using PersimmonConfig {}", config);
    }

    @RabbitListener
    public void consumeRecalcRequest(RecalcRequest message) {
        LOGGER.info("Got request {}; cleaning links", message);
        String cleanResult = mapper.cleanLinks(message.getOrderDate(), message.getTrId());
        LOGGER.info("Cleaned links {}", cleanResult);
        calculateDay(message.getOrderDate(), message.getTrId());
        eventSender.sendEvents();
    }

    public void calculateDay(LocalDate orderDate, long trId) {
        List<Timetable> timetables = storageAccessor.getTimetables(orderDate, trId, true);

        if (timetables.isEmpty()) {
            LOGGER.info("No rounds for {} {}", trId, orderDate);
            return;
        } else if (timetables.size() > 1) {
            LOGGER.error("Too many timetables for {} {}", trId, orderDate);
            return;
        }

        
        linkerController.updateTimetables(timetables);
        Timetable timetable = timetables.get(0);
        List<TtAction> ttActions = timetable.getTtActions();
        Instant planBegin = ttActions.get(0).getPlanBegin().minus(1, ChronoUnit.HOURS);
        Instant planEnd = ttActions.get(ttActions.size() - 1).getPlanEnd().plus(1, ChronoUnit.HOURS);
        LOGGER.info("Getting track {} {} {} {}", trId, ttActions.size(), planBegin, planEnd);

        List<SimpleUnitPacket> track = getTrack(timetable.getTrId(),
                                                planBegin.atOffset(ZoneOffset.UTC).toLocalDateTime(),
                                                planEnd.atOffset(ZoneOffset.UTC).toLocalDateTime());
        LOGGER.info("Got track {} {}", trId, track.size());

        track.forEach(linkerController::link);

        LOGGER.info("Calculated day {} {}: links {}, sections {}, stops {}, rounds {}",
                    trId,
                    orderDate,
                    eventCollector.getLinkEvents().size(),
                    eventCollector.getGspEvents().size(),
                    eventCollector.getSipEvents().size(),
                    eventCollector.getTacEvents().size());

    }

    private List<SimpleUnitPacket> getTrack(long trId,
                                            LocalDateTime beginDate,
                                            LocalDateTime endDate) {
        List<CoordinateDTO> coordinates = mapper.getCoordinates(trId, beginDate, endDate);
        return coordinates.stream().map(this::makeSimpleUnitPacket).collect(Collectors.toList());
    }

    private SimpleUnitPacket makeSimpleUnitPacket(CoordinateDTO coordinate) {
        return new SimpleUnitPacket(coordinate.getPacketId(),
                                    coordinate.getTrId(),
                                    coordinate.getUnitId(),
                                    coordinate.getEventTime().toInstant(ZoneOffset.UTC),
                                    coordinate.isLocationValid(),
                                    coordinate.getLon() == null ? 0.0f : coordinate.getLon(),
                                    coordinate.getLat() == null ? 0.0f : coordinate.getLat(),
                                    coordinate.getSpeed() == null ? 0 : coordinate.getSpeed());
    }

    @Data
    public static class RecalcRequest {

        private final long trId;
        private final LocalDate orderDate;
    }
}
