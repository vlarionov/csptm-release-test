package ru.advantum.csptm.monitoring.overspeed;


import javax.sql.DataSource;

import com.fatboyindustrial.gsonjavatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import ru.advantum.csptm.common.DbIdGeneratorPropConfig;
import ru.advantum.csptm.common.RmqServiceConfig;
import ru.advantum.csptm.dbidgen.DbIdGenerator;
import ru.advantum.csptm.dbidgen.DbIdGeneratorConfig;

@EnableEurekaClient
@SpringBootApplication
@Import({DbIdGeneratorPropConfig.class, RmqServiceConfig.class})
public class Application {

    public static void main(String[] args){
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public DbIdGenerator dbIdGenerator(DbIdGeneratorConfig dbIdGeneratorConfig, DataSource dataSource) throws Exception{
          return new DbIdGenerator(dbIdGeneratorConfig, dataSource);
    }

    @Bean
    public Gson gson() {
        return Converters.registerAll(new GsonBuilder()).create();
    }

}
