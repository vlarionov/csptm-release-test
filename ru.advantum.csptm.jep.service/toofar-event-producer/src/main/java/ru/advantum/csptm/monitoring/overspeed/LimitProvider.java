package ru.advantum.csptm.monitoring.overspeed;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class LimitProvider {

    public static String constantName = "GL_DEVIATION_POINTS";

    private final DataSource dataSource;

    public LimitProvider(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Integer getLimit(){
        Integer limit = 3;
        try(Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("select value from asd.constants where name = ?");
            statement.setString(1, constantName);

            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()) {
                limit = resultSet.getInt(1);
            }

            resultSet.close();
            statement.close();
        }catch (Exception e){
            log.error("Got Exception while fetching limit", e);
        }
        return limit;
    }
}
