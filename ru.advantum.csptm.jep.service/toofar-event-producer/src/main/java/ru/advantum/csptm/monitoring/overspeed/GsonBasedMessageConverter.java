package ru.advantum.csptm.monitoring.overspeed;

import com.google.gson.Gson;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.converter.AbstractMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeType;
import ru.advantum.csptm.incident.model.IncidentBundle;
import ru.advantum.csptm.jep.service.glinker.model.event.LinkEvent;

@Component
public class GsonBasedMessageConverter extends AbstractMessageConverter {

    private final Gson gson;

    public GsonBasedMessageConverter(Gson gson) {
        super(new MimeType("application", "json"));
        this.gson = gson;
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return true;
    }

    @Override
    protected Object convertFromInternal(Message<?> message, Class<?> targetClass, Object conversionHint) {
        return gson.fromJson((String)message.getPayload(), targetClass);
    }

    @Override
    protected Object convertToInternal(Object payload, MessageHeaders headers, Object conversionHint) {
        headers.put("amqp_type", IncidentBundle.class.getSimpleName());
        return gson.toJson(payload);
    }
}
