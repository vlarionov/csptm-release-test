package ru.advantum.csptm.monitoring.overspeed;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Validated
@Component
@ConfigurationProperties(prefix = "toofar-event-producer")
public class TooFarEventProducerConfig {

    public int sendRetryPeriod = 5;

    public int getSendRetryPeriod() {
        return sendRetryPeriod;
    }

    public void setSendRetryPeriod(int sendRetryPeriod) {
        this.sendRetryPeriod = sendRetryPeriod;
    }
}
