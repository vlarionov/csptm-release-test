package ru.advantum.csptm.monitoring.overspeed;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.integration.annotation.Publisher;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.dbidgen.DbIdGenerator;
import ru.advantum.csptm.incident.model.Incident;
import ru.advantum.csptm.incident.model.IncidentBundle;
import ru.advantum.csptm.incident.model.IncidentPayload;
import ru.advantum.csptm.incident.model.IncidentType;
import ru.advantum.csptm.incident.model.Point;
import ru.advantum.csptm.jep.proto.UnitPacketsBundle;
import ru.advantum.csptm.jep.service.glinker.model.event.LinkEvent;
import ru.advantum.csptm.jep.service.glinker.model.event.SimpleUnitPacket;

@Service
@EnableBinding(Processor.class)
public class TooFarEventProducer{

    private static final Logger LOGGER = LoggerFactory.getLogger(TooFarEventProducer.class);
    private final int limit;
    private final DbIdGenerator dbIdGenerator;
    private final Map<Long, Integer> tooFarMap;

    @Autowired
    public TooFarEventProducer(LimitProvider limitProvider, DbIdGenerator dbIdGenerator){
        this.dbIdGenerator = dbIdGenerator;
        tooFarMap = new ConcurrentHashMap<>();
        limit = limitProvider.getLimit();
    }

    @StreamListener(Processor.INPUT)
    @SendTo(Processor.OUTPUT)
    public IncidentBundle consume(LinkEvent[] linkEvents) {
        List<Incident> incidentBundle = new ArrayList<>();
        for (LinkEvent event : linkEvents) {
            try {
                if (event.getType().equals(LinkEvent.LinkType.TOO_FAR_FROM_ROUND)) {
                    Long trId = event.getSimpleUnitPacket().getTrId();
                    final Integer eventsCnt = tooFarMap.compute(trId, (key, curValue) -> curValue == null ? 1 : curValue + 1);

                    if (eventsCnt >= this.limit) {
                        SimpleUnitPacket unitPacket = event.getSimpleUnitPacket();
                        LOGGER.info("far from round! {} limit {}", unitPacket, this.limit);

                        Incident incident = Incident.builder()
                                .incidentId(dbIdGenerator.nextId())
                                .incidentTypeId(IncidentType.TYPE.TOO_FAR.get())
                                .timeStart(unitPacket.getEventTime())
                                .trId(unitPacket.getTrId())
                                .unitId(unitPacket.getUnitId())
                                .incidentStatusId(Incident.STATUS.NEW.get())
                                .incidentInitiatorId(Incident.INITIATOR.SYSTEM.get())
                                .incidentPayload(IncidentPayload.builder()
                                                         .pointStart(Point.of(unitPacket.getLon(), unitPacket.getLat()))
                                                         .build()
                                )
                                .build();

                        incidentBundle.add(incident);
                    }
                }

                if (event.getType().isGoodLink()) {
                    tooFarMap.remove(event.getSimpleUnitPacket().getTrId());
                }
            } catch (Exception e) {
                LOGGER.error("LinkEvent processing error", e);
            }
        }
        if (!incidentBundle.isEmpty()) {
            return IncidentBundle.builder().incidentBundle(incidentBundle).build();
        }
        return null;
    }


}
