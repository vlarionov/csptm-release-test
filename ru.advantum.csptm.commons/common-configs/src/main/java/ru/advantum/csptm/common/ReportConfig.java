package ru.advantum.csptm.common;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;
import ru.advantum.jreportrq.controller.ReportControllerConfig;
import ru.advantum.rabbitmq.config.RmqConnectionConfig;
import ru.advantum.rabbitmq.config.RmqConsumerConfig;
import ru.advantum.rabbitmq.config.RmqDestinationConfig;

@Validated
@Configuration
@ConfigurationProperties(prefix = "report-client")
public class ReportConfig {

    @Valid
    @NotNull
    public String reportDir;

    @Valid
    @NotNull
    public String host;

    public Integer port;

    @Valid
    @NotNull
    public String virtualHost;

    @Valid
    @NotNull
    public String username;

    @Valid
    @NotNull
    public String password;

    public Integer heartbeatInterval;

    public Boolean automaticRecovery;

    public Integer prefetchCount;

    @Valid
    @NotNull
    public String queue;

    public Boolean persistent;

    public String routingKey;

    @Valid
    @NotNull
    public String exchange;

    public void setReportDir(String reportDir) {
        this.reportDir = reportDir;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public void setVirtualHost(String virtualHost) {
        this.virtualHost = virtualHost;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setHeartbeatInterval(Integer heartbeatInterval) {
        this.heartbeatInterval = heartbeatInterval;
    }

    public void setAutomaticRecovery(Boolean automaticRecovery) {
        this.automaticRecovery = automaticRecovery;
    }

    public void setPrefetchCount(Integer prefetchCount) {
        this.prefetchCount = prefetchCount;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public void setPersistent(Boolean persistent) {
        this.persistent = persistent;
    }

    public void setRoutingKey(String routingKey) {
        this.routingKey = routingKey;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    @Bean
    public ReportControllerConfig reportControllerConfig() {
        ReportControllerConfig rcc = new ReportControllerConfig();
        rcc.reportDir = reportDir;

        RmqConnectionConfig rmqConnectionConfig = new RmqConnectionConfig();

        if (automaticRecovery != null) {
            rmqConnectionConfig.automaticRecovery = automaticRecovery;
        }
        if (heartbeatInterval != null) {
            rmqConnectionConfig.heartbeatInterval = heartbeatInterval;
        }
        rmqConnectionConfig.host = host;
        rmqConnectionConfig.password = password;
        if (port != null) {
            rmqConnectionConfig.port = port;
        }
        rmqConnectionConfig.username = username;
        rmqConnectionConfig.virtualHost = virtualHost;

        rcc.rmqConnection = rmqConnectionConfig;

        RmqConsumerConfig rmqConsumerConfig = new RmqConsumerConfig();
        if (prefetchCount != null) {
            rmqConsumerConfig.prefetchCount = prefetchCount;
        }
        rmqConsumerConfig.queue = queue;

        rcc.rmqConsumer = rmqConsumerConfig;

        RmqDestinationConfig rmqDestinationConfig = new RmqDestinationConfig();
        rmqDestinationConfig.exchange = exchange;
        if (persistent != null) {
            rmqDestinationConfig.persistent = persistent;
        }
        if (routingKey != null) {
            rmqDestinationConfig.routingKey = routingKey;
        }
        rcc.rmqReportBuilder = rmqDestinationConfig;
        return rcc;
    }
}
