package ru.advantum.csptm.common;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;
import ru.advantum.rabbitmq.config.RmqConnectionConfig;
import ru.advantum.rabbitmq.config.RmqConsumerConfig;
import ru.advantum.rabbitmq.config.RmqDestinationConfig;

@Validated
@Configuration
@ConfigurationProperties(prefix = "rmq-service")
public class RmqServiceConfig {

    @Valid
    @NotNull
    public String host;

    @Valid
    @NotNull
    public String virtualHost;

    @Valid
    @NotNull
    public String username;

    @Valid
    @NotNull
    public String password;

    public Integer heartbeatInterval;

    public Boolean automaticRecovery;

    public String routingKey;

    public String exchange;

    public Integer prefetchCount;

    public String queue;

    public void setHost(String host) {
        this.host = host;
    }

    public void setVirtualHost(String virtualHost) {
        this.virtualHost = virtualHost;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setHeartbeatInterval(Integer heartbeatInterval) {
        this.heartbeatInterval = heartbeatInterval;
    }

    public void setAutomaticRecovery(Boolean automaticRecovery) {
        this.automaticRecovery = automaticRecovery;
    }

    public void setRoutingKey(String routingKey) {
        this.routingKey = routingKey;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public void setPrefetchCount(Integer prefetchCount) {
        this.prefetchCount = prefetchCount;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    @Bean
    public JepRmqServiceConfig reportControllerConfig() {
        JepRmqServiceConfig result = new JepRmqServiceConfig();

        RmqConnectionConfig rmqConnectionConfig = new RmqConnectionConfig();

        if (automaticRecovery != null) {
            rmqConnectionConfig.automaticRecovery = automaticRecovery;
        }
        if (heartbeatInterval != null) {
            rmqConnectionConfig.heartbeatInterval = heartbeatInterval;
        }
        rmqConnectionConfig.host = host;
        rmqConnectionConfig.password = password;

        rmqConnectionConfig.username = username;
        rmqConnectionConfig.virtualHost = virtualHost;

        result.connection = rmqConnectionConfig;


        RmqDestinationConfig rmqDestinationConfig = new RmqDestinationConfig();

        if (routingKey != null) {
            rmqDestinationConfig.routingKey = routingKey;
        }
        if(exchange != null) {
            rmqDestinationConfig.exchange = exchange;
        }

        result.destination = rmqDestinationConfig;

        if(queue!=null) {
            RmqConsumerConfig rmqConsumerConfig = new RmqConsumerConfig();
            rmqConsumerConfig.queue = queue;
            if (prefetchCount != null) rmqConsumerConfig.prefetchCount = prefetchCount;
            result.consumer = rmqConsumerConfig;
        }
        return result;
    }
}
