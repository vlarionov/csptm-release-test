package ru.advantum.csptm.common;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.simpleframework.xml.Attribute;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;
import ru.advantum.csptm.dbidgen.DbIdGeneratorConfig;

@Validated
@Configuration
@ConfigurationProperties(prefix = "db-id-generator")
public class DbIdGeneratorPropConfig {

    @Valid
    @NotNull
    private String sequenceName;

    @Valid
    @NotNull
    private int sequenceIncrement;

    public String getSequenceName() {
        return sequenceName;
    }

    public void setSequenceName(String sequenceName) {
        this.sequenceName = sequenceName;
    }

    public int getSequenceIncrement() {
        return sequenceIncrement;
    }

    public void setSequenceIncrement(int sequenceIncrement) {
        this.sequenceIncrement = sequenceIncrement;
    }

    @Bean
    public DbIdGeneratorConfig dbIdGeneratorConfig(){
        return new DbIdGeneratorConfig(sequenceName, sequenceIncrement);
    }
}
