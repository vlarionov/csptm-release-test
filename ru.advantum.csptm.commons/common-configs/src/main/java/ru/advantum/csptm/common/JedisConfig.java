package ru.advantum.csptm.common;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Protocol;

@Validated
@Configuration
@ConfigurationProperties(prefix = "redis-client")
public class JedisConfig {

    @Valid
    @NotNull
    private String host;

    private int port= 6379;

    private int timeout=0;

    private String password;

    public void setHost(String host) {
        this.host = host;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Bean
    public JedisPool jedis() throws Exception {
        return new JedisPool(new GenericObjectPoolConfig(),
                host,
                port,
                Protocol.DEFAULT_TIMEOUT,
                password,
                Protocol.DEFAULT_DATABASE);
    }

}
