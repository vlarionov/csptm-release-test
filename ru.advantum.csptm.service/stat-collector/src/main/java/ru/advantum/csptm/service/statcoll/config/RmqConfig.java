package ru.advantum.csptm.service.statcoll.config;

import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;
import ru.advantum.csptm.service.statcoll.StatCollectorConfig;
import ru.advantum.csptm.spring.rmq.ConsumerProperties;

import java.util.concurrent.TimeUnit;

@Configuration
public class RmqConfig {

    @Bean
    public ConsumerProperties consumerProperties() {
        return ConsumerProperties.newBuilder()
                .setAcknowledgeMode(AcknowledgeMode.AUTO)
                .setShutdownTimeout(TimeUnit.SECONDS.toMillis(10))
                .build();
    }

    @Bean
    public JepRmqServiceConfig config(StatCollectorConfig config) {
        return config.rmqServiceConfig;
    }
}
