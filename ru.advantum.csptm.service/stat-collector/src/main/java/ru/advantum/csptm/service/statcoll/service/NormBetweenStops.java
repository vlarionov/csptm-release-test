package ru.advantum.csptm.service.statcoll.service;

import lombok.Data;

@Data
public class NormBetweenStops {

    private final long stopItem1Id;
    private final long stopItem2Id;
    private final short trCapacityId;
    private final short trTypeId;
    private final int betweenStopDuration;
}
