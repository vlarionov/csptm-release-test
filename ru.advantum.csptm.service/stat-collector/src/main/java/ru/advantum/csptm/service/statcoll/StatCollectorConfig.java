package ru.advantum.csptm.service.statcoll;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.config.common.db.RedisClientConfig;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

@Root(name = "stat-collector")
public class StatCollectorConfig {

    @Attribute(name = "history-size", required = false)
    public final int historySize;

    @Element(name = "database")
    public final DatabaseConnection databaseConfig;

    @Element(name = "redis-client")
    public final RedisClientConfig redisConfig;

    @Element(name = "redis-state-location", required = false)
    public final String redisStateLocation;

    @Element(name = "rmq-service")
    public final JepRmqServiceConfig rmqServiceConfig;

    public StatCollectorConfig() {
        this.historySize = 4;
        this.databaseConfig = new DatabaseConnection();
        this.redisConfig = new RedisClientConfig();
        this.redisStateLocation = "stat-collector";
        this.rmqServiceConfig = new JepRmqServiceConfig();
    }
}
