package ru.advantum.csptm.service.statcoll.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.jep.service.glinker.model.event.StopItemPass;

/**
 * @author kaganov
 */
@Service
public class StatCollectorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StatCollectorService.class);

    private final TravelTimeStore travelTimeStore;

    @Autowired
    public StatCollectorService(TravelTimeStore travelTimeStore) {
        this.travelTimeStore = travelTimeStore;
    }

    @RabbitListener
    public void receiveStopPass(StopItemPass[] stopItemPasses) {
        LOGGER.info("Stops {}", stopItemPasses.length);
        for (StopItemPass stopItemPass : stopItemPasses) {
            travelTimeStore.newStopPass(stopItemPass);
        }
    }
}
