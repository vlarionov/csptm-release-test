package ru.advantum.csptm.service.statcoll.service;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.StatementType;
import org.springframework.cache.annotation.Cacheable;

import java.util.Map;

@Mapper
public interface StatCollectorMapper {

    @SuppressWarnings("SpringCacheAnnotationsOnInterfaceInspection")
    @Cacheable(value = "orderRoundTt")
    @Select("select * from oud.stat_collector_ttb_pkg$get_tr_properties()")
    @ConstructorArgs({
            @Arg(column = "tr_id", javaType = long.class),
            @Arg(column = "tr_type_id", javaType = short.class),
            @Arg(column = "tr_capacity_id", javaType = short.class)
    })
    @MapKey("trId")
    Map<Long, TrProperties> getTrProperties();

    @Options(statementType = StatementType.CALLABLE)
    @Update("{call oud.stat_collector_ttb_pkg$update_norm_from_json(#{norms}::json)}")
    void updateNormFromJson(@Param("norms") String norms);
}
