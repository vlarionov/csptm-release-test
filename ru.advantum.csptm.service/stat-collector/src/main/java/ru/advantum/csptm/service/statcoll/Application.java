package ru.advantum.csptm.service.statcoll;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * @author kaganov
 */
@SpringBootApplication
@EnableScheduling
public class Application {

    public static final String CONFIG_FILE_NAME = "stat-collector.xml";

    @Bean
    public static StatCollectorConfig applicationConfig() throws Exception {
        return ru.advantum.config.Configuration.unpackConfig(
                StatCollectorConfig.class,
                CONFIG_FILE_NAME);
    }

    @Bean
    public DataSource dataSource(StatCollectorConfig config) throws SQLException {
        return config.databaseConfig.getConnectionPool();
    }

    public static void main(String[] args) {
        new SpringApplicationBuilder(Application.class).bannerMode(Banner.Mode.OFF).web(false).run(args);
    }
}
