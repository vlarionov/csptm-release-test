package ru.advantum.csptm.service.statcoll.service;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.advantum.csptm.jep.service.glinker.model.event.StopItemPass;
import ru.advantum.csptm.service.statcoll.StatCollectorConfig;
import ru.advantum.csptm.service.statcoll.redis.RedisMapDumper;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

@Component
public class TravelTimeStore {

    private static final String STATS_KEY = "stats";
    private static final String LAST_TR_PASS_KEY = "last-tr-pass";

    private static final Logger LOGGER = LoggerFactory.getLogger(TravelTimeStore.class);

    private final StatCollectorConfig config;
    private final StatCollectorMapper mapper;
    private final Gson gson;

    private final RedisMapDumper<Long, StopItemPass> lastTrPassesDumper;
    private final ConcurrentMap<Long, StopItemPass> lastTrPasses = new ConcurrentHashMap<>();
    private final RedisMapDumper<StatCategory, AvgCalculator> statsDumper;
    private final ConcurrentMap<StatCategory, AvgCalculator> stats = new ConcurrentHashMap<>();
    private final Map<StatCategory, Double> newValues = new HashMap<>();

    @Autowired
    public TravelTimeStore(@Value("#{applicationConfig.redisStateLocation}") String redisStateLocation,
                           StatCollectorConfig config,
                           @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") StatCollectorMapper mapper,
                           Gson gson,
                           RedisMapDumper<Long, StopItemPass> lastTrPassesDumper,
                           RedisMapDumper<StatCategory, AvgCalculator> statsDumper) {
        this.config = config;
        this.mapper = mapper;
        this.gson = gson;

        this.lastTrPassesDumper = lastTrPassesDumper;
        lastTrPassesDumper.bindMap(lastTrPasses, String.format("%s:%s", redisStateLocation, LAST_TR_PASS_KEY), Long.class, StopItemPass.class);
        this.statsDumper = statsDumper;
        statsDumper.bindMap(stats, String.format("%s:%s", redisStateLocation, STATS_KEY), StatCategory.class, AvgCalculator.class);
    }

    @PostConstruct
    public void start() {
        lastTrPassesDumper.restoreState();
        statsDumper.restoreState();
        LOGGER.info("Restored state lastTrPass {} stats {}", lastTrPasses.size(), stats.size());
    }

    private boolean passesMatch(StopItemPass sip1, StopItemPass sip2) {
        return sip1.getTtActionId() == sip2.getTtActionId()
                && sip1.getStopOrderNum() == sip2.getStopOrderNum() - 1;
    }

    public void newStopPass(StopItemPass newStopItemPass) {
        final StopItemPass oldStopItemPass = lastTrPasses.put(newStopItemPass.getTrId(), newStopItemPass);
        if (oldStopItemPass != null && passesMatch(oldStopItemPass, newStopItemPass)) {
            final TrProperties trProperties = mapper.getTrProperties().get(newStopItemPass.getTrId());
            final StatCategory sc = new StatCategory(oldStopItemPass.getStopItemId(),
                                                     newStopItemPass.getStopItemId(),
                                                     trProperties.getTrTypeId(),
                                                     trProperties.getTrCapacityId());

            final double tt = ChronoUnit.MILLIS.between(oldStopItemPass.getPassTime(),
                                                        newStopItemPass.getPassTime()) / 1000.0;
            final double avgTt = stats.computeIfAbsent(sc, k -> new AvgCalculator(config.historySize)).add(tt);

            synchronized (newValues) {
                newValues.put(sc, avgTt);
            }
        }
    }

    @Scheduled(initialDelay = 60 * 1000, fixedDelay = 60 * 1000)
    @PreDestroy
    public void dumpState() {
        LOGGER.info("Dumping lastTrPasses {}", lastTrPasses.size());
        lastTrPassesDumper.dumpState();
        LOGGER.info("Dumping stats {}", stats.size());
        statsDumper.dumpState();
        synchronized (newValues) {
            LOGGER.info("Dumping new norms {}", newValues.size());

            final List<NormBetweenStops> newNorms = newValues.entrySet().stream()
                    .map(e -> new NormBetweenStops(e.getKey().getStopItemFromId(),
                                                   e.getKey().getStopItemToId(),
                                                   e.getKey().getTrCapacityId(),
                                                   e.getKey().getTrTypeId(),
                                                   (int) Math.round(e.getValue())))
                    .collect(Collectors.toList());

            // передавать одним большим JSON'ом гораздо (100 мсек против 10 сек) быстрее, чем вызывать много раз update, даже батчем
            Lists.partition(newNorms, 1000).stream()
                    .map(gson::toJson)
                    .forEach(mapper::updateNormFromJson);

            newValues.clear();
            LOGGER.info("Dumped norms");
        }
    }

    @Data
    private static class StatCategory {

        private final long stopItemFromId;
        private final long stopItemToId;
        private final short trTypeId;
        private final short trCapacityId;
    }
}
