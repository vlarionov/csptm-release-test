package ru.advantum.csptm.service.statcoll.service;

import lombok.Data;

@Data
public class TrProperties {

    private final long trId;
    private final short trTypeId;
    private final short trCapacityId;
}
