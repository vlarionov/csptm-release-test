package ru.advantum.csptm.service.statcoll.service;

public class AvgCalculator {

    private final int maxCnt;
    private final double[] values;

    private int start;
    private int curCnt;
    private double sum;

    public AvgCalculator(int maxCnt) {
        this.maxCnt = maxCnt;
        this.values = new double[maxCnt];
    }

    public double add(double val) {
        sum += val;
        if (curCnt < maxCnt) {
            values[curCnt] = val;
            curCnt++;
        } else {
            sum -= values[start];
            values[start] = val;
            start = (start + 1) % maxCnt;
        }
        return get();
    }

    public double get() {
        return sum / curCnt;
    }
}
