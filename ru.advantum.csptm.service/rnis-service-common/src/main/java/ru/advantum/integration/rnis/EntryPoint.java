package ru.advantum.integration.rnis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.UnsupportedEncodingException;

/**
 * Created by alexander on 11.08.2015.
 */

@Path("/")
public class EntryPoint {

    private static final Logger log = LoggerFactory.getLogger(EntryPoint.class);

    @GET
    @Path("file")
    public Response getDataFile(
            @QueryParam("name") final String name,
            @QueryParam("type") final String out_type
    ) throws UnsupportedEncodingException {
        if (out_type.equals(GTFSFilesType.ZIP.getName())) {
            return Response.ok((StreamingOutput) output -> GtfsProvider.getInstance().getFile(GTFSFilesType.ZIP,
                    name + GTFSFilesType.ZIP.getNameExtention(), output))
                    .type("application/zip")
                    .header("Content-Disposition", "attachment; filename=" + name + ".zip")
                    .build();
        } else if (out_type.equals(GTFSFilesType.TEXT.getName())) {
            return Response.ok((StreamingOutput) output -> GtfsProvider.getInstance().getFile(GTFSFilesType.TEXT,
                    name + GTFSFilesType.TEXT.getNameExtention(), output))
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN + "; charset=UTF-8")
                    .header("Content-Disposition", "attachment; filename=" + name + ".txt")
                    .build();
        } else if (out_type.equals(GTFSFilesType.MD5.getName())) {
            return Response.ok((StreamingOutput) output -> GtfsProvider.getInstance().getFile(GTFSFilesType.MD5,
                    name + GTFSFilesType.MD5.getNameExtention(), output))
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN)
                    .header("Content-Disposition", "attachment; filename=" + name + ".md5")
                    .build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
