package ru.advantum.integration.rnis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.integration.rnis.config.RnisServiceConfig;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;

/**
 * Created by kukushkin on 03.02.2016.
 * Modified by abc on 06.12.2016
 */
public class GtfsProvider {

    private static final Logger LOG = LoggerFactory.getLogger(GtfsProvider.class);
    private RnisServiceConfig cfg;
    private static GtfsProvider instance = null;
    private final String GTFS_PREFIX = "GTFS_";

    private GtfsProvider() {
    }

    public static synchronized GtfsProvider getInstance() {
        if (instance == null) {
            instance = new GtfsProvider();
        }
        return instance;
    }

    public void init(RnisServiceConfig cfg) {
        this.cfg = cfg;
    }

    public void getFile(GTFSFilesType type, String name, OutputStream output) {

        Path p = FileSystems.getDefault().getPath(cfg.dataFilesPath, GTFS_PREFIX + name);
        try {
            Files.copy(p, output);
        } catch (InterruptedIOException ex) {
            LOG.warn(String.format("Requested file not found %s", p.toString()));
        } catch (NoSuchFileException ex) {
            LOG.warn("run ", ex);
        } catch (IOException ex) {
            LOG.error("run ", ex);
        }
    }
}
