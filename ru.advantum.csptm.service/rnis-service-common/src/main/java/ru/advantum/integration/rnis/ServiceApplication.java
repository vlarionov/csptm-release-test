package ru.advantum.integration.rnis;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;
import ru.advantum.integration.rnis.config.RnisServiceConfig;

/**
 * @author Created by alexander on 11.08.2015.
 * @author Modified by abc <alekseenkov@advantum.pro>
 */
public class ServiceApplication {

    private static RnisServiceConfig config;
    private static final Logger LOG = LoggerFactory.getLogger(ServiceApplication.class);

    public ServiceApplication(RnisServiceConfig config) throws Exception {
        ServiceApplication.config = config;
    }

    protected void start() throws Exception {

        GtfsProvider.getInstance().init(config);

        // jersey
        ServletContextHandler jerseyResourceContext = new ServletContextHandler(ServletContextHandler.SESSIONS);
        jerseyResourceContext.setContextPath("/gtfs");

        ServletHolder jerseyServlet = jerseyResourceContext.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/*");
        jerseyServlet.setInitOrder(0);
        jerseyServlet.setInitParameter("jersey.config.server.provider.classnames", EntryPoint.class.getCanonicalName());

        // Static resource
        ResourceHandler staticResourceHandler = new ResourceHandler();
        String resLocation = ServiceApplication.class.getResource("/webapp").toString();
        staticResourceHandler.setResourceBase(resLocation);

        ContextHandler staticContextHandler = new ContextHandler("/gtfs-docs");
        staticContextHandler.setHandler(staticResourceHandler);

        // Add handlers
        Server jettyServer = new Server(ServiceApplication.config.port);
        HandlerList handlers = new HandlerList();
        handlers.setHandlers(new Handler[]{jerseyResourceContext, staticContextHandler, new DefaultHandler()});
        jettyServer.setHandler(handlers);

        try {
            SLF4JBridgeHandler.removeHandlersForRootLogger();
            SLF4JBridgeHandler.install();

            jettyServer.start();
            jettyServer.join();
        } finally {
            jettyServer.destroy();
        }
    }
}
