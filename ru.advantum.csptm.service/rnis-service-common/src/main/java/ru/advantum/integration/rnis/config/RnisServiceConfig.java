/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.advantum.integration.rnis.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name = "rnis-service")
public class RnisServiceConfig {
    @Attribute(name = "port", required = true)
    public int port;
    
    @Attribute(name = "data_files_path")
    public String dataFilesPath;
}
