package ru.advantum.csptm.service.alarmhandler.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

@EqualsAndHashCode
@Data
@FieldDefaults(level= AccessLevel.PRIVATE)
@AllArgsConstructor
@ToString
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AlarmNameCode {
    @JsonProperty("iType")
    Short incidentType;
    @JsonProperty("n")
    String incidenName;
}
