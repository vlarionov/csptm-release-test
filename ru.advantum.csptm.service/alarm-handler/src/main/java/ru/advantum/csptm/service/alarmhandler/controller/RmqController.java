package ru.advantum.csptm.service.alarmhandler.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.java.Log;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.incident.model.IncidentBundle;
import ru.advantum.csptm.service.alarmhandler.model.AlarmNameCode;
import ru.advantum.csptm.service.alarmhandler.model.TrAlarms;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

@Service
@Log
public class RmqController {
    @Autowired
    private Environment env;
    @Autowired
    private ObjectMapper mapper;
    private TrAlarms trAlarms = new TrAlarms();
    @Value("${alarm.handler.redis.key}")
    private String redisKey;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private StringRedisTemplate redisTemplate;
    private Map<Short, String> alarmsMap = new ConcurrentHashMap<>();
    @PostConstruct
    public void init() {
        try {
            trAlarms = mapper.readValue(redisTemplate.opsForValue().get(redisKey), TrAlarms.class);
        } catch (Exception e) {
            // не смогли восстановить объект из redis
            trAlarms = new TrAlarms();
            log.log(Level.SEVERE, "Error in restore tr alarms from redis.", e);
        }
        updateIncidentList();
    }

    @RabbitListener(queues = "${alarm.handler.queue.name}")
    public void receiveIncidentBundle(Message msg) throws IOException {
        String strMessage = new String(msg.getBody());
        IncidentBundle ib = mapper.readValue(strMessage, IncidentBundle.class);

        ib.getIncidentBundle().stream()
                .filter(o -> o != null)
                .filter(o -> o.getTrId() != null)
                .forEach(i -> {
                    Long trId = i.getTrId();
                    Set<AlarmNameCode> alarmSet = trAlarms.getTrAlarms().get(i.getIncidentTypeId());
                    if (alarmSet == null) {
                        alarmSet = new HashSet<>();
                        trAlarms.getTrAlarms().put(trId, alarmSet);
                    }
                    AlarmNameCode nameCode = new AlarmNameCode(i.getIncidentTypeId(), alarmsMap.get(i.getIncidentTypeId()));
                    if (i.getIsAlarmOn() == null || i.getIsAlarmOn()) {//добавляем аларм
                        alarmSet.add(nameCode);
                        System.out.println("add: " + nameCode.toString() + " ; trid: " + trId);
                    } else {// удаляем
                        alarmSet.remove(nameCode);
                        System.out.println("remove: " + nameCode.toString() + " ; trid: " + trId);
                    }
                });
    }

    @Scheduled(fixedRate = 5_000)
    // сохранение состояние тревог (алармов) в redis
    public void dump2redis() {
        try {
            redisTemplate.opsForValue().set(redisKey, mapper.writeValueAsString(trAlarms));
        } catch (JsonProcessingException e) {
            log.log(Level.SEVERE, "Error dump tr alarms to redis.", e);
            e.printStackTrace();

        }
    }

    @Scheduled(fixedRate = 60_000)
    // обновление списка инцидентов
    public void updateIncidentList() {
        List<AlarmNameCode> alarms = jdbcTemplate.query(env.getProperty("alarm.handler.incidentSql"),
                new BeanPropertyRowMapper(AlarmNameCode.class));
        alarmsMap.clear();
        alarms.forEach(o -> alarmsMap.put(o.getIncidentType(), o.getIncidenName()));
    }

    public TrAlarms getTrAlarms() {
        return trAlarms;
    }
}
