package ru.advantum.csptm.service.voip.config;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;
import ru.advantum.rabbitmq.config.RmqConsumerConfig;
import ru.advantum.rabbitmq.config.RmqDestinationConfig;

@Root(name = "rmq-service")
public class VoipRmqServiceConfig extends JepRmqServiceConfig {

    /** Очередь статусов от FreeSwitch */
    @Element(name = "rmq-consumer-usw")
    public final RmqConsumerConfig uswConsumer = null;

    /** Очередь звонков для FreeSwitch */
    @Element(name = "rmq-destination-usw")
    public final RmqDestinationConfig uswDestination = null;

    /** Очередь запросов на вызов */
    @Element(name = "rmq-consumer-request")
    public final RmqConsumerConfig requestConsumer = null;
}
