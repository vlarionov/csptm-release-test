package ru.advantum.csptm.service.voip.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
public class DBConfig {

    private VoipServiceConfig voipServiceConfig;

    @Autowired
    public DBConfig(VoipServiceConfig voipServiceConfig) {
        this.voipServiceConfig = voipServiceConfig;
    }

    @Bean(destroyMethod = "")                          // Failed to unregister the JMX name: org.apache.commons.dbcp2
    public DataSource dataSource() throws SQLException {
        return voipServiceConfig.databaseConfig.getConnectionPool();
    }
}
