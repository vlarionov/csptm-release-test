package ru.advantum.csptm.service.voip.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.jep.proto.CommonPacketFlag;
import ru.advantum.csptm.jep.proto.UnitPacket;
import ru.advantum.csptm.jep.proto.UnitPacketsBundle;
import ru.advantum.csptm.service.voip.db.VoipDbMapper;
import ru.advantum.service.usw.network.amqp.CallStatus;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Service
public class VoipService {

    private static final Logger log = LoggerFactory.getLogger(VoipService.class);
    private final VoipDbMapper voipDbMapper;

    @Autowired
    public VoipService(VoipDbMapper voipDbMapper) {
        this.voipDbMapper = voipDbMapper;
    }

    /**
     * Обработка запросов на обратный голосовой вызов
     */
    @RabbitListener(queues = "#{applicationConfig.rmqServiceConfig.requestConsumer.queue}")
    public void consumeCallRequestMessage(UnitPacketsBundle unitPacketsBundle) {
        try {
            for (UnitPacket unitPacket : unitPacketsBundle.getPackets()) {
                if (unitPacket != null &&
                        unitPacket.getTrId() != null &&
                        unitPacket.getReceiveTime() != null &&
                        unitPacket.getFlags() != null &&
                        unitPacket.getFlags().contains(CommonPacketFlag.CALL_REQUEST.name())) {

                    voipDbMapper.registerCallRequest(
                            unitPacket.getTrId(),
                            unitPacket.getUnitId(),
                            LocalDateTime.ofInstant(unitPacket.getReceiveTime(), ZoneOffset.UTC));

                    log.debug("Call request tr:{} unit:{} dt:{}", unitPacket.getTrId(), unitPacket.getUnitId(), unitPacket.getReceiveTime());
                }
            }
        } catch (Throwable e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Обработка статусов текущего соединения от FreeSwitch
     */
    @RabbitListener(queues = "#{applicationConfig.rmqServiceConfig.uswConsumer.queue}")
    public void consumeCallStatusRequestMessage(CallStatus callStatus) {
        try {
            LocalDateTime callBegin = null;
            if (callStatus.getCallBegin() != null)
                callBegin = LocalDateTime.ofInstant(callStatus.getCallBegin(), ZoneOffset.UTC);
            LocalDateTime callEnd = null;
            if (callStatus.getCallEnd() != null)
                callEnd = LocalDateTime.ofInstant(callStatus.getCallEnd(), ZoneOffset.UTC);
            String status = null;
            if (callStatus.getStatus() != null) status = callStatus.getStatus();

            voipDbMapper.updateCallRequestStatus(
                    callStatus.getIdCall(),
                    callBegin,
                    callEnd,
                    callStatus.getAudiofile(),
                    status);

            log.debug("Got status callId:{} status:{} b:{} e:{} f:{}", callStatus.getIdCall(), callStatus.getStatus(),
                    callBegin, callEnd, callStatus.getAudiofile());

        } catch (Throwable e) {
            log.error(e.getMessage());
        }
    }
}
