package ru.advantum.csptm.service.voip.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.jep.proto.UnitPacketsBundle;
import ru.advantum.csptm.spring.rmq.MessageTypeResolver;
import ru.advantum.csptm.spring.rmq.MultiTypeResolver;
import ru.advantum.service.usw.network.amqp.CallStatus;
import ru.advantum.service.usw.network.amqp.CancelCallRecord;
import ru.advantum.service.usw.network.amqp.UniversalCallRecord;

@Configuration
public class RabbitConfig {

    @Bean
    public VoipRmqServiceConfig jepRmqServiceConfig(VoipServiceConfig config) {
        return config.rmqServiceConfig;
    }

    @Bean
    public MessageTypeResolver messageTypeResolver() {
        return MultiTypeResolver.newBuilder()
                .addPair("COORDS", UnitPacketsBundle.class)
                .addPair(CallStatus.class.getSimpleName(), CallStatus.class)
                .addPair(UniversalCallRecord.class.getSimpleName(), UniversalCallRecord.class)
                .addPair(CancelCallRecord.class.getSimpleName(), CancelCallRecord.class)
                .build();
    }
}
