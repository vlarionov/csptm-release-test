package ru.advantum.csptm.service.voip.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.service.voip.db.VoipDbMapper;
import ru.advantum.csptm.spring.rmq.TypeAdder;
import ru.advantum.service.usw.network.amqp.CancelCallRecord;
import ru.advantum.service.usw.network.amqp.UniversalCallRecord;

import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Сервис "автодозвона"
 */
@Service
public class RedialService {

    private static final Logger log = LoggerFactory.getLogger(RedialService.class);

    private final RabbitTemplate rabbitTemplate;
    private final String uswRoutingKey;
    private final VoipDbMapper voipDbMapper;
    private final int maxTryCount;
    private final long statusCheckWait = 5 * 1000;
    private final long callTryTimeout;
    private final long callAllTryTimeout;

    private Set<UniversalCallRecord> calls = new ConcurrentSkipListSet<>();

    @Autowired
    public RedialService(RabbitTemplate rabbitTemplate,
                         @Value("#{applicationConfig.rmqServiceConfig.uswDestination.routingKey}") String uswRoutingKey,
                         VoipDbMapper voipDbMapper) {
        this.rabbitTemplate = rabbitTemplate;
        this.uswRoutingKey = uswRoutingKey;
        this.voipDbMapper = voipDbMapper;

        maxTryCount = voipDbMapper.getMaxTryCount();
        callTryTimeout = voipDbMapper.getCallTryTimeout() * 1000;
        callAllTryTimeout = maxTryCount * callTryTimeout;
    }

    private CallStatus processCallStatus(Long idCall) throws InterruptedException {
        CallStatus resilt = CallStatus.TIMEOUT;
        long finishTime = System.currentTimeMillis() + callTryTimeout;
        up:
        while (System.currentTimeMillis() < finishTime) {
            String status = voipDbMapper.getVoiceCallStatus(idCall);
            switch (status) {
                case "OK":
                    resilt = CallStatus.OK;
                    break up;
                case "CANCEL":
                    resilt = CallStatus.CANCEL;
                    break up;
                case "ERROR":
                    resilt = CallStatus.TRY;
                    break up;
                default:
                    resilt = CallStatus.TRY;
                    break;
            }
            Thread.sleep(statusCheckWait);
        }
        log.info("call {} status {}", idCall, resilt.name());
        return resilt;
    }

    private void setRedialStatus(Integer idRedialCall, CallStatus status, int tryNum) {
        String statusRedialCall;
        if (CallStatus.TRY == status) {
            statusRedialCall = status.name() + "_" + tryNum;
        } else {
            statusRedialCall = status.name();
        }
        voipDbMapper.setRedialStatus(idRedialCall, statusRedialCall);
    }

    private Long prepareNewTry(Long idCall) {
        Long idNewCall = voipDbMapper.getVoiceCallID();
        voipDbMapper.prepareNewCallTry(idCall, idNewCall);
        return idNewCall;
    }

    @Async
    public void processCall(UniversalCallRecord callRecord) {

        if (calls.size() > 100) {
            log.error("Too many simultaneous calls. Request canceled {}", callRecord);
            return;
        }

        try {
            if (calls.stream().anyMatch(p -> p.getIdRedialCall().equals(callRecord.getIdRedialCall()))) {
                log.info("Call already processing {}", callRecord);
                return;
            }

            calls.add(callRecord);

            long allFinishTime = System.currentTimeMillis() + callAllTryTimeout;

            for (int i = 0; i < maxTryCount; i++) {
                if (System.currentTimeMillis() > allFinishTime) {
                    setRedialStatus(callRecord.getIdRedialCall(), CallStatus.TIMEOUT, 0);
                    log.info("call {} canceled by timeout", callRecord.getIdCall());
                    break;
                }

                log.info("Try {}, call {}", i, callRecord);
                rabbitTemplate.convertAndSend(uswRoutingKey, callRecord, TypeAdder.of(UniversalCallRecord.class.getSimpleName()));

                CallStatus status = processCallStatus(callRecord.getIdCall());
                setRedialStatus(callRecord.getIdRedialCall(), status, i);
                if (status != CallStatus.TRY) break;

                Long idCall = prepareNewTry(callRecord.getIdCall());
                callRecord.setIdCall(idCall);

                if (i + 1 == maxTryCount) {
                    log.info("call {} canceled by try num", callRecord.getIdCall());
                    setRedialStatus(callRecord.getIdRedialCall(), CallStatus.TIMEOUT, 0);
                }
            }
        } catch (Exception e) {
            log.error("processCall", e);
        } finally {
            for (UniversalCallRecord call : calls) {
                if (call.getIdRedialCall().equals(callRecord.getIdRedialCall())) calls.remove(call);
            }
        }
    }

    @Async
    public void cancelCall(CancelCallRecord cancelCallRecord) {
        if (cancelCallRecord.getIdRedialCall() != null) {
            setRedialStatus(cancelCallRecord.getIdRedialCall(), CallStatus.CANCEL, 0);
        }
    }

    private enum CallStatus {
        OK,
        CANCEL,
        TIMEOUT,
        TRY
    }
}
