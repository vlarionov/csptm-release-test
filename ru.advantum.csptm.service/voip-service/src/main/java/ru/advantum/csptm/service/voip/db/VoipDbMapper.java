package ru.advantum.csptm.service.voip.db;

import org.apache.ibatis.annotations.*;

import java.time.LocalDateTime;

@Mapper
public interface VoipDbMapper {

    @Select("select voip.voice_call_pkg$register_call_request(#{trID}::bigint, #{unitID}::bigint, #{requestDate}::timestamp) as result")
    Long registerCallRequest(@Param("trID") Long trID, @Param("unitID") Long unitID, @Param("requestDate") LocalDateTime requestDate);

    @Select("select voip.voice_call_pkg$update_call_request_status(#{idCall}::bigint, #{callBegin}::timestamp, #{callEnd}::timestamp, #{audiofile}::text, #{status}::text) as result")
    Long updateCallRequestStatus(@Param("idCall") Long idCall, @Param("callBegin") LocalDateTime callBegin, @Param("callEnd") LocalDateTime callEnd, @Param("audiofile") String audiofile, @Param("status") String status);

    @Update("select voip.voice_call_pkg$set_redial_status(#{p_voice_call_redial_id}::integer, #{p_voice_call_redial_status}::text)")
    void setRedialStatus(@Param("p_voice_call_redial_id") Integer idRedialCall, @Param("p_voice_call_redial_status") String statusRedialCall);

    @Select("select nextval('voip.voice_call_voice_call_id_seq')")
    Long getVoiceCallID();

    @Insert("select voip.voice_call_pkg$prepare_new_call_try(#{p_call_id}, #{p_new_call_id})")
    void prepareNewCallTry(@Param("p_call_id") Long idCall, @Param("p_new_call_id") Long idNewCall);

    @Select("select voip.voice_call_pkg$get_call_status(#{p_call_id})")
    String getVoiceCallStatus(@Param("p_call_id") Long idCall);

    @Select("select asd.get_constant('VOIP_REDIAL_TRY_COUNT')::int")
    Integer getMaxTryCount();

    @Select("select asd.get_constant('VOIP_REDIAL_CALL_TIMEOUT_SEC')::int")
    Integer getCallTryTimeout();
}
