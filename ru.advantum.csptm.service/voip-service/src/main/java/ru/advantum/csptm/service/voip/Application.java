package ru.advantum.csptm.service.voip;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import ru.advantum.csptm.service.voip.config.VoipServiceConfig;
import ru.advantum.csptm.spring.properties.AbstractPropertySourceFactory;

import static ru.advantum.csptm.service.voip.Application.CONFIG_FILE_NAME;

@SpringBootApplication
@PropertySource(value = "", name = CONFIG_FILE_NAME, factory = Application.ConfigPropertySourceFactory.class)
@EnableAsync
public class Application {

    public static final String CONFIG_FILE_NAME = "voip-service.xml";

    @Bean
    public static VoipServiceConfig applicationConfig() throws Exception {
        return ru.advantum.config.Configuration.unpackConfig(
                VoipServiceConfig.class,
                CONFIG_FILE_NAME
        );
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public TaskExecutor threadPoolTaskExecutor() {

        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(4);
        executor.setMaxPoolSize(100);
        executor.setThreadNamePrefix("default_task_executor_thread");
        executor.initialize();

        return executor;
    }

    public static class ConfigPropertySourceFactory extends AbstractPropertySourceFactory<VoipServiceConfig> {
        @Override
        public VoipServiceConfig getConfig() throws Exception {
            return applicationConfig();
        }
    }
}
