package ru.advantum.csptm.service.voip.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.service.usw.network.amqp.CancelCallRecord;
import ru.advantum.service.usw.network.amqp.UniversalCallRecord;

/**
 * Обработка запросов на голосовой вызов с автодозвоном
 */
@Service
@RabbitListener(queues = "#{applicationConfig.rmqServiceConfig.consumer.queue}")
public class VoipCallService {

    private static final Logger log = LoggerFactory.getLogger(VoipCallService.class);
    private final RedialService redialService;

    @Autowired
    public VoipCallService(RedialService redialService) {
        this.redialService = redialService;
    }

    @RabbitHandler
    void processCall(UniversalCallRecord universalCallRecord) {
        log.info("Redial call {}", universalCallRecord);
        redialService.processCall(universalCallRecord);
    }

    @RabbitHandler
    void cancelCall(CancelCallRecord cancelCallRecord) {
        log.info("Redial call cancel {}", cancelCallRecord);
        redialService.cancelCall(cancelCallRecord);
    }
}
