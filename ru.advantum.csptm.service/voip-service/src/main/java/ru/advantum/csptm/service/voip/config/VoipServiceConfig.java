package ru.advantum.csptm.service.voip.config;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;

@Root(name = "voip-service")
public class VoipServiceConfig {

    @Element(name = "database")
    public DatabaseConnection databaseConfig;

    @Element(name = "rmq-service")
    public VoipRmqServiceConfig rmqServiceConfig;
}
