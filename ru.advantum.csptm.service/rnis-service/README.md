rnis-service - Сервис предоставления данных РНИС
============================

Сервис на порту `port` предоставлет описание сервиса в виде статического web и доступ к файлам в `data_files_path`.
Файлы в `data_files_path` подготавливаются сервисом `rnis-export`
 
Настройки сервиса 
-----------
gis-service/advconf/rnis-export.xml

``````````````    
<rnis-service port="8080"
    data_files_path="F:/Temp/export/rnis">
</rnis-service>

``````````````

Настройки логирования 
-----------
rnis-export/advconf/log4j.prop
