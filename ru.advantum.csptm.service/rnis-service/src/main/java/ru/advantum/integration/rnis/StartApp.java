package ru.advantum.integration.rnis;

import ru.advantum.config.Configuration;
import ru.advantum.integration.rnis.config.RnisServiceConfig;

/**
 * @author abc <alekseenkov@advantum.pro>
 */
public class StartApp extends ServiceApplication {

    public StartApp(RnisServiceConfig config) throws Exception {
        super(config);
    }

    public static void main(String[] args) throws Exception {
        final RnisServiceConfig config = Configuration.unpackConfig(RnisServiceConfig.class, "rnis-service.xml");
        (new StartApp(config)).start();
    }
}
