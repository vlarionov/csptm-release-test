package ru.advantum.csptm.service.forecast.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.advantum.csptm.jep.service.glinker.model.event.Forecast;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@Slf4j
public class ForecastsRepository {

    private static final String FIND_TARGET_STOP_ITEM_IDS_SQL = "SELECT * FROM ibrd.forecast_pkg$find_target_stop_item_ids()";

    private static final String SAVE_FORECAST_SQL = "INSERT INTO ibrd.forecast(" +
            "creation_time," +
            "pass_time_forecast," +
            "tt_action_id," +
            "stop_item_id," +
            "stop_order_num," +
            "from_tt_action_id," +
            "from_stop_order_num" +
            ") VALUES (?,?,?,?,?,?,?)";


    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ForecastsRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Cacheable("targetStopItemIds")
    public Set<Integer> findTargetStopItemIds() {
        return new HashSet<>(jdbcTemplate.queryForList(FIND_TARGET_STOP_ITEM_IDS_SQL, Integer.class));
    }

    @Transactional
    public void saveForecasts(List<Forecast> forecasts) {
        Function<Forecast, Object[]> mapper = forecast -> new Object[]{
                Timestamp.from(forecast.getCreationTime()),
                Timestamp.from(forecast.getPassTimeForecast()),
                (int) forecast.getTtActionId(),
                (int) forecast.getStopItemId(),
                forecast.getStopOrderNum(),
                (int) forecast.getFromTtActionId(),
                forecast.getFromStopOrderNum()
        };
        List<Object[]> args = forecasts.stream().map(mapper).collect(Collectors.toList());
        jdbcTemplate.batchUpdate(SAVE_FORECAST_SQL, args);
    }
}
