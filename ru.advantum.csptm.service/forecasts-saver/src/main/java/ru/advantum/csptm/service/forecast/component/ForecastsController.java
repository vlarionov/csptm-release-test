package ru.advantum.csptm.service.forecast.component;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.advantum.csptm.jep.service.glinker.model.event.Forecast;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class ForecastsController {

    private final ForecastsRepository forecastsRepository;

    @Autowired
    public ForecastsController(ForecastsRepository forecastsRepository) {
        this.forecastsRepository = forecastsRepository;
    }

    @RabbitListener
    public void receiveForecasts(List<Forecast> forecasts) {
        val targetStopItemIds = forecastsRepository.findTargetStopItemIds();
        val filteredForecasts = forecasts.stream()
                .filter(forecast -> targetStopItemIds.contains((int)forecast.getStopItemId()))
                .collect(Collectors.toList());
        forecastsRepository.saveForecasts(filteredForecasts);
    }
}
