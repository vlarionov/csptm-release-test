package ru.advantum.csptm.service.forecast;

import com.google.common.cache.CacheBuilder;
import lombok.val;
import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

import javax.sql.DataSource;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
public class Application {

    private static final String CONFIG_FILE_NAME = "forecasts-saver.xml";

    public static void main(String[] args) {
        new SpringApplicationBuilder(Application.class)
                .web(false)
                .bannerMode(Banner.Mode.LOG)
                .run(args);
    }

    @Bean
    public static ForecastsSaverConfig applicationConfig() throws Exception {
        return ru.advantum.config.Configuration.unpackConfig(
                ForecastsSaverConfig.class,
                CONFIG_FILE_NAME
        );
    }

    @Bean
    public JepRmqServiceConfig jepRmqServiceConfig() throws Exception {
        return applicationConfig().rmqServiceConfig;
    }

    @Bean
    public DataSource dataSource() throws Exception {
        return applicationConfig().database.getConnectionPool();
    }

    @Configuration
    @EnableCaching
    public static class CacheConfig extends CachingConfigurerSupport {
        @Override
        public CacheManager cacheManager() {
            val manager = new GuavaCacheManager();
            val builder = CacheBuilder.newBuilder().expireAfterWrite(1, TimeUnit.MINUTES);
            manager.setCacheBuilder(builder);
            return manager;
        }
    }

}
