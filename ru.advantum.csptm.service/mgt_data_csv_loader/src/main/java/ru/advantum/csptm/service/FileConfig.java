package ru.advantum.csptm.service;

import org.simpleframework.xml.Attribute;


public class FileConfig {
    @Attribute
    private String name;

    @Attribute
    private String copyParam;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCopyParam() {
        return copyParam;
    }

    public void setCopyParam(String copyParam) {
        this.copyParam = copyParam;
    }
}
