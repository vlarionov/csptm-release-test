package ru.advantum.csptm.service;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.postgresql.core.BaseConnection;
import ru.advantum.config.Configuration;
import org.postgresql.copy.CopyManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;

public class MGTcsvDataLoader {
    public static final Logger log = LoggerFactory.getLogger(MGTcsvDataLoader.class);

    public static void main(String[] args) {
        MGTcsvConfig config = null;
        try {
            config = Configuration.unpackConfig(MGTcsvConfig.class, "test.xml");
        } catch (Exception e) {
            log.error("Config file not found or incomplete! Terminate programm.");
            return;
        }
        Connection conn = null;
        CopyManager copyManager = null;
        String fullFileName = null;
        FileInputStream fis = null;
        File dataFile = null;
        try {
            conn = DriverManager.getConnection(
                    config.getDatabaseConnection().url,
                    config.getDatabaseConnection().username,
                    config.getDatabaseConnection().password);
            copyManager = new CopyManager((BaseConnection) conn);
            for (FileConfig file : config.getFilaNames()) {
                if (file == null || StringUtils.isBlank(file.getName())) {
                    continue;
                }
                try {
                    // путь к файлу из пути к папке и имени файла
                    fullFileName = FilenameUtils.concat(config.getPathConfig().name, file.getName());
                    dataFile = new File(fullFileName);
                    fis = new FileInputStream(dataFile);
                    copyManager.copyIn(file.getCopyParam(), fis);
                } catch (Exception e) {
                    log.error("Error in execute COPY command: " + file.getCopyParam(), e);
                } finally {
                    IOUtils.closeQuietly(fis);
                }
            }
        } catch (Exception e) {
            log.error("Couldn't establish connection to DB. Check config parameters.", e);
        } finally {
            DbUtils.closeQuietly(conn);
        }
    }
}
