package ru.advantum.csptm.service.forecaster.devsaver;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.advantum.csptm.spring.properties.AbstractPropertySourceFactory;

/**
 * @author kaganov
 */
@SpringBootApplication
@PropertySource(value = "", name = Application.CONFIG_FILE_NAME, factory = Application.ConfigPropertySourceFactory.class)
@EnableScheduling
public class Application {

    public static final String CONFIG_FILE_NAME = "dev-forecast-saver.xml";

    @Bean
    public static DevForecastSaverConfig applicationConfig() throws Exception {
        return ru.advantum.config.Configuration.unpackConfig(
                DevForecastSaverConfig.class,
                CONFIG_FILE_NAME);
    }

    public static void main(String[] args) {
        new SpringApplicationBuilder(Application.class).bannerMode(Banner.Mode.OFF).web(false).run(args);
    }

    public static class ConfigPropertySourceFactory extends AbstractPropertySourceFactory<DevForecastSaverConfig> {

        @Override
        public DevForecastSaverConfig getConfig() throws Exception {
            return applicationConfig();
        }
    }
}
