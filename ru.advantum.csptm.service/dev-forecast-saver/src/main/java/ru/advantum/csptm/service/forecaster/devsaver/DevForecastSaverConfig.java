package ru.advantum.csptm.service.forecaster.devsaver;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.csptm.artifact.csvsaver.config.CsvSaverConfig;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

@Root(name = "dev-forecast-saver")
public class DevForecastSaverConfig {

    @Element(name = "rmq-service")
    public final JepRmqServiceConfig rmqServiceConfig;

    @Element(name = "csv-saver-forecasts")
    public final CsvSaverConfig forecastsCsvSaverConfig;

    public DevForecastSaverConfig() {
        this.rmqServiceConfig = new JepRmqServiceConfig();
        this.forecastsCsvSaverConfig = new CsvSaverConfig();
    }
}
