package ru.advantum.csptm.service.forecaster.devsaver;

import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.artifact.csvsaver.CsvSaver;
import ru.advantum.csptm.artifact.csvsaver.apache.print.ApacheRecordPrinter;
import ru.advantum.csptm.artifact.csvsaver.limiters.Limiter;
import ru.advantum.csptm.jep.service.glinker.model.event.Forecast;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;

/**
 * @author kaganov
 */
@Service
public class DevForecastSaverService implements ApacheRecordPrinter<Forecast> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DevForecastSaverService.class);

    private final DevForecastSaverConfig config;
    private Limiter forecastsLimiter;

    @Autowired
    public DevForecastSaverService(DevForecastSaverConfig config) {
        this.config = config;
    }

    @PostConstruct
    public void start() {
        forecastsLimiter = CsvSaver.createLimiter(config.forecastsCsvSaverConfig,
                                                  new String[]{
                                                          "creation_time",
                                                          "order_round_id",
                                                          "stop_place_muid",
                                                          "stop_place_order_num",
                                                          "from_order_round_id",
                                                          "from_stop_place_order_num",
                                                          "pass_time_forecast"
                                                  },
                                                  Forecast.class,
                                                  this);
    }

    @PreDestroy
    public void stop() throws Exception {
        forecastsLimiter.close();
    }

    @RabbitListener(queues = "${dev-forecast-saver.rmq-service.rmq-consumer.queue}")
    public void receiveForecasts(Forecast[] forecasts) throws Exception {
        LOGGER.info("Forecasts {}", forecasts.length);
        for (Forecast forecast : forecasts) {
            forecastsLimiter.append(forecast);
        }
    }

    @Override
    public void printRecord(Forecast forecast, CSVPrinter csvPrinter) throws IOException {
        csvPrinter.printRecord(forecast.getCreationTime(),
                               forecast.getOrderRoundId(),
                               forecast.getStopPlaceMuid(),
                               forecast.getStopPlaceOrderNum(),
                               forecast.getFromOrderRoundId(),
                               forecast.getFromStopPlaceOrderNum(),
                               forecast.getPassTimeForecast());
    }
}
