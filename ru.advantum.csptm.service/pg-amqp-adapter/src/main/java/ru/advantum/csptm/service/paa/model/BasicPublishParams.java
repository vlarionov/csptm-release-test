package ru.advantum.csptm.service.paa.model;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

/**
 * Created by kaganov on 22/06/2017.
 */
public class BasicPublishParams {

    @SerializedName("exchange")
    private final String exchange;
    @SerializedName("routing_key")
    private final String routingKey;
    @SerializedName("type")
    private final String type;
    @SerializedName("headers")
    private final Map<String, String> headers;
    @SerializedName("payload")
    private final String payload;

    public BasicPublishParams(String exchange,
                              String routingKey,
                              String type,
                              Map<String, String> headers,
                              String payload) {
        this.exchange = exchange;
        this.routingKey = routingKey;
        this.type = type;
        this.payload = payload;
        this.headers = headers;
    }

    public String getExchange() {
        return exchange;
    }

    public String getRoutingKey() {
        return routingKey;
    }

    public String getType() {
        return type;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public String getPayload() {
        return payload;
    }

    @Override
    public String toString() {
        return "BasicPublishParams{" +
                "exchange='" + exchange + '\'' +
                ", routingKey='" + routingKey + '\'' +
                ", type='" + type + '\'' +
                ", headers=" + headers +
                ", payload='" + payload + '\'' +
                '}';
    }
}
