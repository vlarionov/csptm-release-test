package ru.advantum.csptm.service.paa;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.advantum.csptm.service.paa.model.BasicPublishParams;
import ru.advantum.csptm.service.paa.model.PaaAction;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by kaganov on 03/05/2017.
 */
@Component
public class PgAmqpAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(PgAmqpAdapter.class);

    private final PgAmqpAdapterConfig config;
    private final PaaMapper paaMapper;
    private final AmqpTemplate amqpTemplate;
    private final Gson gson;

    @Autowired
    // не видит идея, что mapper с помощью mybatis starter'а подключится
    @SuppressWarnings("SpringJavaAutowiringInspection")
    public PgAmqpAdapter(PgAmqpAdapterConfig config,
                         PaaMapper paaMapper,
                         AmqpTemplate amqpTemplate, Gson gson) {
        this.config = config;
        this.paaMapper = paaMapper;
        this.amqpTemplate = amqpTemplate;
        this.gson = gson;
    }

    @PostConstruct
    public void initListener() throws SQLException {
    }

    @Scheduled(initialDelay = 0, fixedDelay = 5000)
    private void updateFromDb() {
        LOGGER.info("Getting action list");
        List<PaaAction> notProcessedActions = paaMapper.getNotProcessedActions(config.brokerId);
        for (PaaAction notProcessedAction : notProcessedActions) {
            processAction(notProcessedAction);
            paaMapper.processAction(notProcessedAction.getActionId());
        }
        LOGGER.info("Done processing");
    }

    private void processAction(PaaAction action) {
        LOGGER.info("{}", action);

        switch (action.getActionType()) {
            case BASIC_PUBLISH:
                BasicPublishParams basicPublishParams = gson.fromJson(action.getActionParams(), BasicPublishParams.class);
                LOGGER.info("{}", basicPublishParams);
                MessageProperties messageProperties = new MessageProperties();
                if (basicPublishParams.getType() != null) {
                    messageProperties.setType(basicPublishParams.getType());
                }
                final Map<String, String> headers = basicPublishParams.getHeaders();
                if (headers != null && !headers.isEmpty()) {
                    headers.forEach(messageProperties::setHeader);
                }
                messageProperties.setDeliveryMode(MessageDeliveryMode.NON_PERSISTENT);
                amqpTemplate.send(basicPublishParams.getExchange(),
                                  basicPublishParams.getRoutingKey(),
                                  new Message(basicPublishParams.getPayload().getBytes(), messageProperties));
                break;
            default:
                throw new IllegalArgumentException(String.format("Invalid action [%s]", action));
        }
    }

    @PreDestroy
    public void stop() {
        LOGGER.info("Service stopping");
    }
}
