package ru.advantum.csptm.service.paa.model;

/**
 * Created by kaganov on 22/06/2017.
 */
public class PaaAction {

    private final long actionId;
    private final ActionType actionType;
    private final String actionParams;

    public PaaAction(long actionId, ActionType actionType, String actionParams) {
        this.actionId = actionId;
        this.actionType = actionType;
        this.actionParams = actionParams;
    }

    public long getActionId() {
        return actionId;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public String getActionParams() {
        return actionParams;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PaaAction action = (PaaAction) o;

        return actionId == action.actionId;
    }

    @Override
    public int hashCode() {
        return (int) (actionId ^ (actionId >>> 32));
    }

    @Override
    public String toString() {
        return "PaaAction{" +
                "actionId=" + actionId +
                ", actionType=" + actionType +
                ", actionParams='" + actionParams + '\'' +
                '}';
    }

    public enum ActionType {
        BASIC_PUBLISH
    }
}
