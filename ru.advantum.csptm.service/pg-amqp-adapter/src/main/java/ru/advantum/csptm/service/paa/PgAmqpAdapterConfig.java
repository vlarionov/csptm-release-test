package ru.advantum.csptm.service.paa;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

/**
 * Created by kaganov on 03/05/2017.
 */
@Root(name = "pg-amqp-adapter")
public class PgAmqpAdapterConfig {

    @Attribute(name = "broker-id")
    public short brokerId;

    @Element(name = "database")
    public DatabaseConnection databaseConfig;

    @Element(name = "rmq-service")
    public final JepRmqServiceConfig serviceConfig = new JepRmqServiceConfig();
}
