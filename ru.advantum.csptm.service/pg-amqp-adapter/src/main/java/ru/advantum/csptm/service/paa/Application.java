package ru.advantum.csptm.service.paa;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;
import ru.advantum.csptm.spring.properties.AbstractPropertySourceFactory;
import ru.advantum.csptm.spring.rmq.ConsumerProperties;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

/**
 * Created by kaganov on 01/06/2017.
 */
@SpringBootApplication
@EnableScheduling
@PropertySource(value = "", name = Application.CONFIG_FILE_NAME, factory = Application.ConfigPropertySourceFactory.class)
public class Application {

    public static final String CONFIG_FILE_NAME = "pg-amqp-adapter.xml";

    @Bean
    public JepRmqServiceConfig rmqConfig(PgAmqpAdapterConfig config) {
        return config.serviceConfig;
    }

    @Bean
    public ConsumerProperties consumerProperties() {
        return ConsumerProperties.newBuilder()
                .setShutdownTimeout(TimeUnit.SECONDS.toMillis(10))
                .build();
    }

    @Bean
    @Primary
    public DataSource dataSource(PgAmqpAdapterConfig config) throws SQLException {
        return config.databaseConfig.getConnectionPool();
    }

    @Bean
    public static PgAmqpAdapterConfig applicationConfig() throws Exception {
        return ru.advantum.config.Configuration.unpackConfig(
                PgAmqpAdapterConfig.class,
                CONFIG_FILE_NAME
        );
    }

    public static void main(String[] args) throws InterruptedException {
        new SpringApplicationBuilder(Application.class)
                .bannerMode(Banner.Mode.OFF)
                .web(false)
                .run(args);
    }

    static class ConfigPropertySourceFactory extends AbstractPropertySourceFactory<PgAmqpAdapterConfig> {

        @Override
        public PgAmqpAdapterConfig getConfig() throws Exception {
            return applicationConfig();
        }
    }
}
