package ru.advantum.csptm.service.paa;

import org.apache.ibatis.annotations.*;
import ru.advantum.csptm.service.paa.model.PaaAction;

import java.util.List;

/**
 * Created by kaganov on 22/06/2017.
 */
@Mapper
public interface PaaMapper {

    @Update("select paa.pg_amqp_adapter_pkg$process_action(#{actionId})")
    void processAction(@Param("actionId") long actionId);

    @ConstructorArgs({
            @Arg(column = "action_id", javaType = long.class),
            @Arg(column = "action_type_id", javaType = PaaAction.ActionType.class),
            @Arg(column = "action_params", javaType = String.class)
    })
    @Select("select * from paa.pg_amqp_adapter_pkg$get_not_processed_actions(#{brokerId})")
    List<PaaAction> getNotProcessedActions(@Param("brokerId") short brokerId);
}
