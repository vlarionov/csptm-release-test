package ru.advantum.csptm.service.incident.handler;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.csptm.dbidgen.DbIdGeneratorConfig;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

@Root(name = "incident-handler")
public class IncidentHandlerConfig {
    @Element(name = "rmq-service")
    public JepRmqServiceConfig rmqServiceConfig;

    @Element
    public DatabaseConnection database;

    @Element(name = "db-id-generator")
    public DbIdGeneratorConfig dbIdGeneratorConfig;

}
