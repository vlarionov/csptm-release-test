package ru.advantum.csptm.service.incident.handler.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.dbidgen.DbIdGenerator;
import ru.advantum.csptm.service.incident.handler.IncidentHandlerConfig;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
public class DBConfig {
    @Bean
    public DataSource dataSource(IncidentHandlerConfig config) throws SQLException {
        return config.database.getConnectionPool();
    }

    @Bean
    public DbIdGenerator dbIdGenerator(IncidentHandlerConfig config, DataSource dataSource) throws SQLException {
        return new DbIdGenerator(config.dbIdGeneratorConfig, dataSource);
    }


}
