package ru.advantum.csptm.service.incident.handler.db;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.StatementType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.advantum.csptm.incident.model.IncidentAction;
import java.util.List;

@Repository
public class IncidentActionDao {

    private final IncidentActioMapper mapper;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public IncidentActionDao(IncidentActioMapper mapper, JdbcTemplate jdbcTemplate) {
        this.mapper = mapper;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Cacheable("incident_action")
    public List<IncidentAction>  getByIncedentType(Short incidentTypeId)
    {
        return mapper.getByIncedentType(incidentTypeId);
    }

    @Mapper
    private interface IncidentActioMapper {
        @Select("SELECT " +
                "incident_action_id as incidentActionId, " +
                "incident_type_id as incidentTypeId, " +
                "incident_handler_id as incidentHandlerId, " +
                "order_num as orderNum, " +
                "module_name as moduleName " +
                "FROM ttb.incident_action " +
                "where incident_type_id = #{incidentTypeId}"
        )
        @Options(useCache = false, statementType = StatementType.CALLABLE)
        List<IncidentAction> getByIncedentType(Short incidentTypeId);
    }
}
