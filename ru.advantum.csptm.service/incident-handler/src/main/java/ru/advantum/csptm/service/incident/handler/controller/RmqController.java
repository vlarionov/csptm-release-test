package ru.advantum.csptm.service.incident.handler.controller;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.dbidgen.DbIdGenerator;
import ru.advantum.csptm.incident.model.*;
import ru.advantum.csptm.service.incident.handler.component.IncidentSaver;
import ru.advantum.csptm.service.incident.handler.db.IncidentActionDao;

import java.util.stream.Collectors;

@Service
@RabbitListener
@Slf4j
public class RmqController {
    private final DbIdGenerator dbIdGenerator;
    private final IncidentSaver incidentSaver;
    private final IncidentActionDao incidentActionDao;
    private static final Logger LOGGER = LoggerFactory.getLogger(RmqController.class);
    @Autowired
    public RmqController(DbIdGenerator dbIdGenerator,
                         IncidentSaver incidentSaver,
                         IncidentActionDao incidentActionDao
    ) {
        this.dbIdGenerator = dbIdGenerator;
        this.incidentSaver = incidentSaver;
        this.incidentActionDao = incidentActionDao;
    }


    @RabbitHandler
    public void receiveIncidentBundle(IncidentBundle incidentBundle) {
        incidentBundle.getIncidentBundle().forEach(this::proccessIncident);
    }

    @RabbitHandler
    private void proccessIncident(Incident incident) {
        incidentActionDao.getByIncedentType(incident.getIncidentTypeId()).forEach(incidentAction -> resolveHandler(incidentAction, incident));
    }


    private void resolveHandler(IncidentAction incidentAction, Incident incident) {
        final Incident.HANDLER handler = Incident.HANDLER.getHandler(incidentAction.getIncidentHandlerId());
        if (handler != null) {
            if (incident.getIncidentId() == null) {
                incident.setIncidentId(dbIdGenerator.nextId());
            }
            incidentSaver.send(incident, handler.name());
        } else {
            throw new IllegalArgumentException("Unsupported handler " + incident.getIncidentTypeId());
        }
    }

}
