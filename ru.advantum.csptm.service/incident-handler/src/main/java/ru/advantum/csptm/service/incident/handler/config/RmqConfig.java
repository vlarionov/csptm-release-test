package ru.advantum.csptm.service.incident.handler.config;

import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.incident.model.Incident;
import ru.advantum.csptm.incident.model.IncidentBundle;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;
import ru.advantum.csptm.service.incident.handler.IncidentHandlerConfig;
import ru.advantum.csptm.spring.rmq.ConsumerProperties;
import ru.advantum.csptm.spring.rmq.MessageTypeResolver;
import ru.advantum.csptm.spring.rmq.MultiTypeResolver;

@Configuration
public class RmqConfig {
    @Bean
    public JepRmqServiceConfig jepRmqServiceConfig(IncidentHandlerConfig config) {
        return config.rmqServiceConfig;
    }

    @Bean
    public MessageTypeResolver messageTypeResolver() {
        return MultiTypeResolver.newBuilder()
                .addPair("IncidentBundle", IncidentBundle.class)
                .addPair("Incident", Incident.class)
                .build();
    }

    @Bean
    public ConsumerProperties consumerProperties() {
        return ConsumerProperties.newBuilder()
                .setAcknowledgeMode(AcknowledgeMode.AUTO)
                .build();
    }

}
