package ru.advantum.csptm.service.incident.handler;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {
    public static final String CONFIG_FILE_NAME = "incident-handler.xml";

    public static void main(String[] args) {
        new SpringApplicationBuilder(Application.class)
                .web(false)
                .bannerMode(Banner.Mode.LOG)
                .run(args);
    }

    @Bean
    public static IncidentHandlerConfig applicationConfig() throws Exception {
        return ru.advantum.config.Configuration.unpackConfig(
                IncidentHandlerConfig.class,
                CONFIG_FILE_NAME
        );
    }

}
