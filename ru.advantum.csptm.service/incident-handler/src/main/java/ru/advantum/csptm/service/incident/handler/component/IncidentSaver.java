package ru.advantum.csptm.service.incident.handler.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.advantum.csptm.incident.model.Incident;
import ru.advantum.csptm.service.incident.handler.IncidentHandlerConfig;
import ru.advantum.csptm.spring.rmq.TypeAdder;

/**
 * Created by kukushkin on 10/31/17.
 */
@Component
@Slf4j
public class IncidentSaver {
    private final RabbitTemplate rabbitTemplate;
    private final IncidentHandlerConfig incidentHandlerConfig;

    @Autowired
    public IncidentSaver(RabbitTemplate rabbitTemplate, IncidentHandlerConfig incidentHandlerConfig) {
        this.rabbitTemplate = rabbitTemplate;
        this.incidentHandlerConfig = incidentHandlerConfig;
    }

    public void send(Incident incident, String routingKey) {
        rabbitTemplate.convertAndSend("incidents", routingKey, incident, TypeAdder.of(Incident.class.getSimpleName()));
    }


}
