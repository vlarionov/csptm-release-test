package ru.advantum.csptm.service.testkill;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by kaganov on 03/05/2017.
 */
@Root(name = "test-kill-service")
public class TestKillServiceConfig {

    @Attribute(name = "stop-after-timeout")
    public final boolean stopAfterTimeout;

    @Attribute(name = "timeout-sec")
    public final int timeout;

    @Element(name = "kill-message")
    public final String killMessage;

    public TestKillServiceConfig() {
        stopAfterTimeout = true;
        timeout = 0;
        killMessage = null;
    }
}
