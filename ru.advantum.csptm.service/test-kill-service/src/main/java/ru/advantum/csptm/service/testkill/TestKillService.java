package ru.advantum.csptm.service.testkill;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by kaganov on 03/05/2017.
 */
public class TestKillService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestKillService.class);

    private final TestKillServiceConfig config;

    public TestKillService(TestKillServiceConfig config) {
        this.config = config;
    }

    public void start() {
        LOGGER.info("Service started");
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(this::showMsg, 1, 1, TimeUnit.SECONDS);
    }

    private void showMsg() {
        LOGGER.info("OK {}", Math.round(Math.random() * 100));
    }

    public void stop() {
        LOGGER.info("Service stopping");

        try {
            TimeUnit.SECONDS.sleep(config.timeout);
        } catch (InterruptedException e) {
            LOGGER.error("stop", e);
        }

        if (config.stopAfterTimeout) {
            LOGGER.info("Service stopped [{}]", config.killMessage);
        } else {
            while (true) {
                try {
                    LOGGER.info("Service did not stop");
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {

                }
            }
        }
    }

    public static void main(String[] args) throws Exception {
        TestKillServiceConfig config = Configuration.unpackConfig(TestKillServiceConfig.class, "test-kill-service.xml");
        TestKillService testKillService = new TestKillService(config);
        testKillService.start();
        Runtime.getRuntime().addShutdownHook(new Thread(testKillService::stop));
    }
}
