package ru.advantum.service.abstracts;

import java.io.Reader;

/**
 * Класс обработки csv файлов
 */
public abstract class CsvFileHandler {

    protected final String database;
    protected final Character delimiter;

    public CsvFileHandler(String database, Character delimiter) {
        this.database = database;
        this.delimiter = delimiter;
    }

    /**
     * Если для данного вида обработчика необходимо запустить пост-обработку, необходимо вернуть "database"
     * @return
     */
    public String getDatabaseForPostProcess() {
        return null;
    }

    /**
     * Метод обработки файла
     * @param filename имя файла
     * @param reader объект для чтения содержимого файла
     * @throws Exception
     */
    public abstract void handle(String filename, Reader reader) throws Exception;
}
