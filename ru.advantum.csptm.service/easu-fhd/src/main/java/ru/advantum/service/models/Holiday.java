package ru.advantum.service.models;

import java.time.LocalDateTime;

/**
 * Класс - сущность праздничных дней для маршрута
 */
public class Holiday {

    private String route_variant_code;
    private String holiday_date;
    private String action;

    public String getRoute_variant_code() {
        return route_variant_code;
    }

    public void setRoute_variant_code(String route_variant_code) {
        this.route_variant_code = route_variant_code;
    }

    public String getHoliday_date() {
        return holiday_date;
    }

    public void setHoliday_date(String holiday_date) {
        this.holiday_date = holiday_date;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
