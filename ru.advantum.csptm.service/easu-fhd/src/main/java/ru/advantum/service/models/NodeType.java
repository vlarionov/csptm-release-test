package ru.advantum.service.models;

/**
 * Класс - сущность вида остановочных пунктов
 */
public class NodeType {
    private Long stop_type_id;
    private String stop_type_name;

    public Long getStop_type_id() {
        return stop_type_id;
    }

    public void setStop_type_id(Long stop_type_id) {
        this.stop_type_id = stop_type_id;
    }

    public String getStop_type_name() {
        return stop_type_name;
    }

    public void setStop_type_name(String stop_type_name) {
        this.stop_type_name = stop_type_name;
    }
}
