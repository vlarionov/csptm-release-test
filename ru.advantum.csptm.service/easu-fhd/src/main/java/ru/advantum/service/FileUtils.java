package ru.advantum.service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Класс работы с файлами
 */
public class FileUtils {

    private static SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");

    /**
     * Создает файл если такого не существует
     * @param filePath путь до файла
     * @return true если файл был успешно создан, false если нет
     * @throws IOException
     */
    public static Boolean createFileIfNotExist(String filePath) throws IOException {
        if (!Files.exists(Paths.get(filePath))) {
            Files.createFile(Paths.get(filePath));
            return true;
        }

        return false;
    }

    /**
     * Создает директорию по указанному пути если такой не существует
     * @param directory путь до директории
     * @return true если директория создана, false если нет
     * @throws IOException
     */
    public static Boolean createDirIfNotExist(String directory) throws IOException {
        if (!Files.exists(Paths.get(directory))) {
            Files.createDirectories(Paths.get(directory));
            return true;
        }

        return false;
    }

    /**
     * Строит путь до директории по дате, точности и корневой директории
     * @param date дата
     * @param precision точность
     * @param rootDir корневая директория
     * @return Путь до директории за указанную дату, с указанной точностью и указанной корневой директорией
     * @throws IOException
     */
    public static String getDateDirs(Date date, String precision, String rootDir) throws IOException {
        String dateString = format.format(date);
        return getDateDirs(dateString, precision, rootDir);
    }

    /**
     * Строит путь до директории по дате, точности и корневой директории
     * @param dateString дата
     * @param precision точность
     * @param rootDir корневая директория
     * @return Путь до директории за указанную дату, с указанной точностью и указанной корневой директорией
     * @throws IOException
     */
    public static String getDateDirs(String dateString, String precision, String rootDir) throws IOException {
        StringBuilder directory = new StringBuilder();
        switch (precision) {
            case "second": {
                directory.insert(0, dateString.substring(12, dateString.length()));
                directory.insert(0, "/");
            }
            case "minute": {
                directory.insert(0, dateString.substring(10, 12));
                directory.insert(0, "/");
            }
            case "hour": {
                directory.insert(0, dateString.substring(8, 10));
                directory.insert(0, "/");
            }
            case "day": {
                directory.insert(0, dateString.substring(6, 8));
                directory.insert(0, "/");
            }
            case "month": {
                directory.insert(0, dateString.substring(4, 6));
                directory.insert(0, "/");
            }
            case "year": {
                directory.insert(0, dateString.substring(0, 4));
                directory.insert(0, "/");
            }
        }

        directory.insert(0, rootDir);
        String resultDir = directory.toString();

        return resultDir;
    }

    /**
     * Сериализует карту map в файл filePath
     * @param map карта для сериализации
     * @param filePath путь до файла
     * @param <K> тип ключа карты
     * @param <V> тип значения карты
     * @throws Exception
     */
    public static <K, V> void serializeMap(Map<K, V> map, String filePath) throws Exception {
        FileUtils.createFileIfNotExist(filePath);

        FileOutputStream fos = new FileOutputStream(filePath);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(map);
        oos.close();
        fos.close();
    }

    /**
     * Десериализует карту из файла
     * @param filePath путь до файла
     * @param <K> тип ключа карты результата
     * @param <V> тип значения карты результата
     * @return карта десериализованная из файла filePath
     * @throws Exception
     */
    public static <K, V> Map<K, V> deserializeMap(String filePath) throws Exception {
        FileUtils.createFileIfNotExist(filePath);

        try (FileInputStream fis = new FileInputStream(filePath);
             ObjectInputStream ois = new ObjectInputStream(fis)) {

            return (Map<K, V>) ois.readObject();
        } catch (Exception ex) {
            return new HashMap<>();
        }
    }
}
