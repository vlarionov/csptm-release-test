package ru.advantum.service.mappers;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import ru.advantum.service.models.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Интерфейс запросов к БД
 */
public interface EASUMapper {
    /**
     * Вставка записи о водителе
     *
     * @param prknu номер парка
     * @param pernr персональный номер
     * @param cname фио водителя
     */
    @Select("select * from easu.easu_import_pkg$import_drivers(#{PRKNU}, #{PERNR}, #{CNAME})")
    @ResultType(Integer.class)
    Integer insertDriver(@Param("PRKNU") String prknu, @Param("PERNR") Long pernr, @Param("CNAME") String cname);

    /**
     * Вставка записи о наряде
     *
     * @param group таблица с данными о наряде
     */
    @Select("select * from easu.easu_import_pkg$import_orders(" +
            " #{group.PRKNU}, #{group.ADDAT}, #{group.COLNU}, #{group.ROUNR_EXT}, #{group.DEPNR_EXT}, #{group.TIME_FROM}," +
            " #{group.TIME_TO}, #{group.RKIND}, #{group.EQFNR}, #{group.EQFNR2}, #{group.SHNUM}, #{group.PERNR}, #{group.RTYPE})")
    @ResultType(Integer.class)
    Integer insertGroup(@Param("group") Map<String, Object> group);

    /**
     * Список транспортной работы для указанного маршрута, закрытую на указанную дату
     */
    @Select("select * from easu.easu_export_pkg$export_transport_work( #{p_tt_variant_id} )")
    @ResultType(TransportWork.class)
    List<TransportWork> selectTransportWork(@Param("p_tt_variant_id") Integer ttVariantID);

    /**
     * Список закрытых на указанную дату маршрутов
     *
     * @param dateEnd дата закрытия маршрута
     */
    //@Select("select * from easu.easu_export_pkg$export_closed_routes(#{dateEnd})")
    @Select("select * from easu.easu_export_pkg$transport_work_list( #{dateEnd} )")
    @ResultType(HashMap.class)
    List<Map<String, Object>> selectClosedRoutes(@Param("dateEnd") Date dateEnd);

    /**
     * Список изменений в расписании маршрута
     */
    @Select("select * from easu.easu_export_pkg$export_ras_change(#{p_export_id})")
    @ResultType(Changes.class)
    List<Changes> selectChanges(@Param("p_export_id") Long export_id);

    /**
     * Список закрытых маршрутов с указанным идентификатором
     */
    @Select("select * from easu.easu_export_pkg$export_closed_route(#{p_export_id},#{p_route_variant_id})")
    @ResultType(ClosedRoute.class)
    List<ClosedRoute> selectClosedRoutesByRouteId(@Param("p_export_id") Long export_id, @Param("p_route_variant_id") Integer routeVariantID);

    /**
     * Список записей, определяющие даты «праздничных дней» маршрутов, указанных в секции
     */
    @Select("select * from easu.easu_export_pkg$export_route_holiday(#{p_export_id},#{p_route_variant_id})")
    @ResultType(Holiday.class)
    List<Holiday> selectHolidayByRouteId(@Param("p_export_id") Long export_id, @Param("p_route_variant_id") Integer routeVariantID);

    /**
     * Записи о создании вариантов маршрутов по причине внесения изменений в паспорт маршрута или формирования новых маршрутов
     */
    @Select("select * from easu.easu_export_pkg$export_new_route(#{p_export_id},#{p_route_variant_id})")
    @ResultType(NewRoute.class)
    List<NewRoute> selectNewRoutesByRouteId(@Param("p_export_id") Long export_id, @Param("p_route_variant_id") Integer routeVariantID);

    /**
     * Изменения справочника остановочных пунктов линий маршрутов (без привязки к маршруту)
     */
    @Select("select * from easu.easu_export_pkg$export_node(#{p_route_variant_id})")
    @ResultType(Node.class)
    List<Node> selectNodes(@Param("p_route_variant_id") Integer routeVariantID);

    /**
     * @return изменения справочника видов (назначений) остановочных пунктов линий маршрутов (без привязки к маршруту)
     */
    @Select("select * from easu.easu_export_pkg$export_node_type()")
    @ResultType(NodeType.class)
    List<NodeType> selectNodeType();

    /**
     * Записи, определяющие остановочные пункты линий, указанных в секции [Path Node]
     */
    @Select("select * from easu.easu_export_pkg$export_path_node(#{p_export_id},#{p_route_variant_id})")
    @ResultType(PathNode.class)
    List<PathNode> selectPathNodeByRouteId(@Param("p_export_id") Long export_id, @Param("p_route_variant_id") Integer routeVariantID);

    /**
     * Записи, характеризующие линии маршрутов, указанных в секции [Path]
     */
    @Select("select * from easu.easu_export_pkg$export_path(#{p_export_id},#{p_route_variant_id})")
    @ResultType(Path.class)
    List<Path> selectPathByRouteId(@Param("p_export_id") Long export_id, @Param("p_route_variant_id") Integer routeVariantID);

    /**
     * Записи, определяющие сезоны действия расписания
     */
    @Select("select * from easu.easu_export_pkg$export_season()")
    @ResultType(Season.class)
    List<Season> selectSeasons();

    /**
     * Записи об изменениях расписания маршрутов
     */
    @Select("select * from easu.easu_export_pkg$export_schedule(#{p_export_id},#{p_route_variant_id})")
    @ResultType(Schedule.class)
    List<Schedule> selectScheduleByRouteId(@Param("p_export_id") Long export_id, @Param("p_route_variant_id") Integer routeVariantID);

    /**
     * Текущие маршрут и выход для ТС
     *
     * @param parkNum   номер парка
     * @param garageNum гаражный номер
     */
    @Select("select v_route_num as route_num, v_out_num as out_num from easu.easu_import_pkg$select_tr_current_route_out(#{park_num}, #{garage_num})")
    @ResultType(TrOut.class)
    TrOut selectTrCurrentRouteOut(@Param("park_num") String parkNum, @Param("garage_num") Long garageNum);

    /**
     * Отправить запрос на пересчет оперативного расписания
     */
    @Insert("select * from easu.easu_import_pkg$tt_process_request()")
    void ttProcessRequest();

    /**
     * Фиксирование момента начала выгрузки данных
     */
    @Select("select * from easu.easu_export_pkg$start_export()")
    @ResultType(Long.class)
    Long rasStartExport();

    /**
     * Фиксирование момента окончания выгрузки данных
     *
     * @param export_id
     */
    @Insert("select * from easu.easu_export_pkg$finish_export_success(#{p_export_id})")
    void rasFinishExportSuccess(@Param("p_export_id") Long export_id);

    /**
     * Фиксирование момента окончания выгрузки данных
     *
     * @param export_id
     */
    @Insert("select * from easu.easu_export_pkg$finish_export_error(#{p_export_id})")
    void rasFinishExportError(@Param("p_export_id") Long export_id);
}
