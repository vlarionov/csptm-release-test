package ru.advantum.service.models;

/**
 * Класс - сущность нового маршрута
 */
public class NewRoute {
    private String route_variant_code;
    private Short status;
    private String date_begin;
    private String time_begin;
    private String number;
    private String date_end;
    private String time_end;
    private String type_code;
    private String average_speed;
    private String season_code;
    private String day_of_week_mask;
    private Long season_id;
    private String view_code;

    public String getRoute_variant_id() {
        return route_variant_code;
    }

    public void setRoute_variant_id(String route_variant_id) {
        this.route_variant_code = route_variant_id;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public String getDate_begin() {
        return date_begin;
    }

    public void setDate_begin(String date_begin) {
        this.date_begin = date_begin;
    }

    public String getTime_begin() {
        return time_begin;
    }

    public void setTime_begin(String time_begin) {
        this.time_begin = time_begin;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDate_end() {
        return date_end;
    }

    public void setDate_end(String date_end) {
        this.date_end = date_end;
    }

    public String getTime_end() {
        return time_end;
    }

    public void setTime_end(String time_end) {
        this.time_end = time_end;
    }

    public String getType_code() {
        return type_code;
    }

    public void setType_code(String type_code) {
        this.type_code = type_code;
    }

    public String getAverage_speed() {
        return average_speed;
    }

    public void setAverage_speed(String average_speed) {
        this.average_speed = average_speed;
    }

    public String getSeason_code() {
        return season_code;
    }

    public void setSeason_code(String season_code) {
        this.season_code = season_code;
    }

    public String getDay_of_week_mask() {
        return day_of_week_mask;
    }

    public void setDay_of_week_mask(String day_of_week_mask) {
        this.day_of_week_mask = day_of_week_mask;
    }

    public Long getSeason_id() {
        return season_id;
    }

    public void setSeason_id(Long season_id) {
        this.season_id = season_id;
    }

    public String getView_code() {
        return view_code;
    }

    public void setView_code(String view_code) {
        this.view_code = view_code;
    }
}
