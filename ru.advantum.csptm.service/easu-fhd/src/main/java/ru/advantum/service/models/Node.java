package ru.advantum.service.models;

/**
 * Класс - сущность остановочных пунктов
 */
public class Node {
    private Integer stop_id;
    private String stop_name;

    public Integer getStop_id() {
        return stop_id;
    }

    public void setStop_id(Integer stop_id) {
        this.stop_id = stop_id;
    }

    public String getStop_name() {
        return stop_name;
    }

    public void setStop_name(String stop_name) {
        this.stop_name = stop_name;
    }
}