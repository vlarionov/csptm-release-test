package ru.advantum.service.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by Leonid on 20.02.2017.
 */
@Root(name = "source")
public class EASUCsvSourceElement {
    @Attribute(name = "url")
    private String url;

    public String getUrl() {
        return url;
    }
}
