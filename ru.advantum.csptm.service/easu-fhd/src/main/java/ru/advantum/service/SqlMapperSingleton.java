package ru.advantum.service;

import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

/**
 * Класс получения доступа до БД
 */
public class SqlMapperSingleton {
    private static volatile Map<String, SqlSessionFactory> dbMap;

    public static SqlSessionFactory getInstance(final String name) {
        createMap();

        return dbMap.get(name);
    }

    /**
     * Метод создания карты соединений
     */
    private static void createMap() {
        if (null == dbMap) {
            synchronized (SqlMapperSingleton.class) {
                if (null == dbMap) {
                    dbMap = new ConcurrentHashMap<>();
                }
            }
        }
    }

    /**
     * Метод регистрации БД с именем name, средой environment, и доп. настройками задаваемыми в consumer
     * @param name имя объекта настроек БД
     * @param environment настройки соединения с БД
     * @param consumer функция дополнительных действий с соединением
     */
    public static void registerInstance(final String name, Environment environment, Consumer<SqlSessionFactory> consumer) {
        //получается, кому повезет, тот будет последним, того и слот в мапе
        //если нам надо строгую последовательность добавления, то синхронизировать эти методы
        createMap();

        Configuration configuration = new Configuration(environment);
        SqlSessionFactory instance = new SqlSessionFactoryBuilder().build(configuration);
        consumer.accept(instance);
        dbMap.put(name, instance);
    }

    /**
     * Метод регистрации БД с именем name, средой environment
     * @param name имя объекта настроек БД
     * @param environment настройки соединения с БД
     */
    public static void registerInstance(final String name, Environment environment) {
        registerInstance(name, environment, factory -> {
        });
    }
}
