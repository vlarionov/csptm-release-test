package ru.advantum.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Класс ведения журнала обработанных файлов
 */
public class ProcessedFileSaver implements Runnable {

    private static Logger log = LoggerFactory.getLogger(ProcessedFileSaver.class);
    private final File saveFile;
    private final Set<String> processedFiles;

    public ProcessedFileSaver(String filename, Set<String> processedFiles) throws IOException {
        this.processedFiles = processedFiles;
        saveFile = new File(filename);
    }

    /**
     * Выполняется в отдельном потоке, ждет уведомления об обработке очередного файла, получает имя файла из множества
     * и записывает его в файл saveFile
     */
    @Override
    public void run() {
        while (true) {
            synchronized (processedFiles) {
                try {
                    processedFiles.wait();

                    FileWriter writer = new FileWriter(saveFile, false);
                    for (String filename : processedFiles) {
                        writer.write(filename + "\n");
                    }

                    writer.flush();
                    writer.close();
                } catch (Exception e) {
                    log.error("", e);
                }
            }
        }
    }

    public static Set<String> load(String fileName) throws IOException {
        File saveFile = new File(fileName);
        if (!saveFile.exists()) {
            saveFile.createNewFile();
        }

        BufferedReader reader = new BufferedReader(new FileReader(saveFile));
        String line;
        Set<String> processedFiles = new HashSet<>();
        while ((line = reader.readLine()) != null) {
            processedFiles.add(line);
        }

        return processedFiles;
    }
}
