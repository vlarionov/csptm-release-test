package ru.advantum.service.config;

import org.simpleframework.xml.Attribute;

/**
 * Класс настройки выгрузки расписаний и маршрутов
 */

//Routes and schedule
public class EASURaS {
    /**
     * Период выгрузки в минутах
     */
    @Attribute(name = "period")
    private Integer period;

    /**
     * Директория для файлов результата
     */
    @Attribute(name = "directory")
    private String directory;

    /**
     * Файл для хранения идентификаторов выполненных выгрузок
     */
    @Attribute(name = "cacheFile")
    private String cacheFile;

    public Integer getPeriod() {
        return period;
    }

    public String getDirectory() {
        return directory;
    }

    public String getCacheFile() {
        return cacheFile;
    }
}
