package ru.advantum.service;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;
import org.apache.ibatis.session.SqlSession;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.service.mappers.EASUMapper;
import ru.advantum.service.models.*;

import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Класс выгрузки маршрутов и расписаний при изменении
 */
public class RoutesAndScheduleTask implements Job {

    private static final Logger log = LoggerFactory.getLogger(RoutesAndScheduleTask.class);

    protected JobDataMap dataMap;
    private String databaseName;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    private SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");

    /**
     * Метод срабатывает по таймеру, определяет тип изменений в расписании и делает соответствующую выгрузку
     *
     * @param context контекст запуска
     * @throws JobExecutionException
     */
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        log.info("Start routes and schedule loading ...");

        dataMap = context.getJobDetail().getJobDataMap();

        //Map<Long, String> changes = (Map<Long, String>) dataMap.get("rasMap");
        databaseName = (String) dataMap.get("databaseName");
        String targetDirectory = (String) dataMap.get("rasDirectory");

        try (SqlSession session = SqlMapperSingleton.getInstance(databaseName).openSession()) {
            Long exportID = null;
            EASUMapper mapper = null;
            try {
                mapper = session.getMapper(EASUMapper.class);
                exportID = mapper.rasStartExport();
                session.commit(true);
                List<Changes> changesList = mapper.selectChanges(exportID);

                if (changesList != null) {
                    log.debug("changes size = {}", changesList.size());
                    for (Changes change : changesList) {
                        try {
                            String routeNumber = change.getRoute_num();
                            Date date = change.getChange_date();

                            FileUtils.createDirIfNotExist(targetDirectory);

                            CSVFormat csvFileFormat = CSVFormat.DEFAULT
                                    .withDelimiter(';')
                                    .withNullString("")
                                    .withFirstRecordAsHeader()
                                    .withEscape('%')
                                    .withQuoteMode(QuoteMode.NONE);

                            String filePath = "";
                            switch (change.getChange_type()) {
                                case "full": {
                                    filePath = targetDirectory + "/R" + routeNumber + "-" + dateFormat.format(date) + "-" + timeFormat.format(date) + ".csv";
                                    makeFullLoad(mapper, change, csvFileFormat, filePath, exportID);
                                    break;
                                }
//                                case "holiday": {
//                                    filePath = targetDirectory + "/H" + routeNumber + "-" + dateFormat.format(date) + "-" + timeFormat.format(date) + ".csv";
//                                    makeHolidayLoad(mapper, change, csvFileFormat, filePath, exportID);
//                                    break;
//                                }
                                case "closed_route": {
                                    filePath = targetDirectory + "/CR" + routeNumber + "-" + dateFormat.format(date) + "-" + timeFormat.format(date) + ".csv";
                                    makeClosedRouteLoad(mapper, change, csvFileFormat, filePath, exportID);
                                    break;
                                }
                            }
                        } catch (Exception e) {
                            log.error("", e);
                        }
                    }
                }
                mapper.rasFinishExportSuccess(exportID);
                log.info("Finish routes and schedule loading");
            } catch (Exception ex) {
                mapper.rasFinishExportError(exportID);
                log.error("RoutesAndScheduleTask", ex);
            } finally {
                session.commit();
            }
        }
    }

//    private String dateToString(Date d) { return d == null ? "" : dateFormat.format(d); }
//    private String timeToString(Date d) { return d == null ? "" : timeFormat.format(d); }

    /**
     * Метод вызывается при  changes.type = full , делает полную выгрузку маршрутов и расписаний
     *
     * @param change   объект изменений из БД (таблица changes)
     * @param format   формат записи csv файла
     * @param filePath путь до файла с результатами выгрузки
     * @throws Exception
     */
    private void makeFullLoad(EASUMapper mapper, Changes change, CSVFormat format, String filePath, Long exportID) throws Exception {

        //List<ClosedRoute> closedRoutes = mapper.selectClosedRoutesByRouteId(change.getRoute_variant_id());
        List<Holiday> holidays = mapper.selectHolidayByRouteId(exportID, change.getRoute_variant_id());
        List<NewRoute> newRoutes = mapper.selectNewRoutesByRouteId(exportID, change.getRoute_variant_id());
        List<Node> nodes = mapper.selectNodes(change.getRoute_variant_id());
        //List<NodeType> nodeTypes = mapper.selectNodeType();
        List<PathNode> pathNodes = mapper.selectPathNodeByRouteId(exportID, change.getRoute_variant_id());
        List<Path> paths = mapper.selectPathByRouteId(exportID, change.getRoute_variant_id());
//      List<Season> seasons = mapper.selectSeasons();
        List<Schedule> schedules = mapper.selectScheduleByRouteId(exportID, change.getRoute_variant_id());

        if (newRoutes.size() == 0 && schedules.size() == 0) {
            return;
        }

        FileUtils.createFileIfNotExist(filePath);

        try (CSVPrinter printer = new CSVPrinter(new FileWriter(filePath), format)) {

//            if (closedRoutes.size() > 0) {
//                printer.printRecord("[Closed Route]");
//                for (ClosedRoute closedRoute : closedRoutes) {
//                    printer.printRecord(closedRoute.getRoute_variant_id(),
//                        closedRoute.getClose_date(),
//                        closedRoute.getClose_time());
//                }
//                printer.printRecord("");
//            }

            if (holidays.size() > 0) {
                printer.printRecord("[Holiday]");
                for (Holiday holiday : holidays) {
                    printer.printRecord(holiday.getRoute_variant_code(),
                            holiday.getHoliday_date(),
                            holiday.getAction());
                }
                printer.printRecord("");
            }

            if (newRoutes.size() > 0) {
                printer.printRecord("[New Route]");
                for (NewRoute newRoute : newRoutes) {
                    printer.printRecord(newRoute.getRoute_variant_id(),
                            newRoute.getStatus(),
                            newRoute.getDate_begin(),
                            newRoute.getTime_begin(),
                            newRoute.getNumber(),
                            newRoute.getDate_end(),
                            newRoute.getTime_end(),
                            newRoute.getType_code(),
                            newRoute.getAverage_speed(),
                            newRoute.getSeason_code(),
                            newRoute.getDay_of_week_mask(),
                            newRoute.getSeason_id(),
                            newRoute.getView_code());
                }
                printer.printRecord("");
            }

            if (nodes.size() > 0) {
                printer.printRecord("[Node]");
                for (Node node : nodes) {
                    printer.printRecord(node.getStop_id(), node.getStop_name());
                }
                printer.printRecord("");
            }

//            if (nodeTypes.size() > 0) {
//                printer.printRecord("[Node Type]");
//                for (NodeType nodeType : nodeTypes) {
//                    printer.printRecord(nodeType.getStop_type_id(), nodeType.getStop_type_name());
//                }
//                printer.printRecord("");
//            }

            if (pathNodes.size() > 0) {
                printer.printRecord("[Path Node]");
                for (PathNode pathNode : pathNodes) {
                    printer.printRecord(pathNode.getRoute_variant_code(),
                            pathNode.getRound_id(),
                            pathNode.getStop_id(),
                            pathNode.getTr_type_id());
                }
                printer.printRecord("");
            }

            if (paths.size() > 0) {
                printer.printRecord("[Path]");
                for (Path path : paths) {
                    printer.printRecord(path.getRoute_variant_code(),
                            path.getRound_id(),
                            path.getLine_type(),
                            path.getLine_view(),
                            path.getMovement_code(),
                            path.getEndpointa(),
                            path.getEndpointb(),
                            path.getLength());
                }
                printer.printRecord("");
            }

//            if (seasons.size() > 0) {
//                printer.printRecord("[Season]");
//                for (Season season : seasons) {
//                    printer.printRecord(season.getName(),
//                        dateToString(season.getDate_begin()),
//                        dateToString(season.getDate_end()),
//                        season.getSeason_id());
//                }
//                printer.printRecord("");
//            }

            if (schedules.size() > 0) {
                printer.printRecord("[Schedule]");
                for (Schedule schedule : schedules) {
                    printer.printRecord(schedule.getRoute_variant_code(),
                            schedule.getStatus(),
                            schedule.getDate_begin(),
                            schedule.getTime_begin(),
                            schedule.getDate_end(),
                            schedule.getOut_number(),
                            schedule.getPark_number(),
                            schedule.getShift_number(),
                            schedule.getEps_type(),
                            schedule.getOut_type(),
                            schedule.getInterval_begin(),
                            schedule.getInterval_end(),
                            schedule.getInterval_code(),
                            schedule.getLine_code(),
                            schedule.getFroute_id());
                }
            }
        }
        log.info("Processed route and schedule with route_id {} to file {}", change.getRoute_id(), filePath);
    }

//    /**
//     * Метод вызывается при  changes.type = holiday , делает выгрузку секции holiday
//     *
//     * @param change   объект изменений из БД (таблица changes)
//     * @param format   формат записи csv файла
//     * @param filePath путь до файла с результатами выгрузки
//     * @throws Exception
//     */
//    private void makeHolidayLoad(EASUMapper mapper, Changes change, CSVFormat format, String filePath, Long exportID) throws Exception {
//        List<Holiday> holidays = mapper.selectHolidayByRouteId(exportID, change.getRoute_variant_id());
//        if (holidays.size() == 0) return;
//
//        FileUtils.createFileIfNotExist(filePath);
//        try (CSVPrinter printer = new CSVPrinter(new FileWriter(filePath), format)) {
//            printer.printRecord("[Holiday]");
//            for (Holiday holiday : holidays) {
//                printer.printRecord(holiday.getRoute_variant_code(),
//                        dateFormat.format(holiday.getHoliday_date()),
//                        holiday.getAction());
//            }
//        }
//        log.info("Processed route and schedule with route_id {} to file {}", change.getRoute_id(), filePath);
//    }

    /**
     * Метод вызывается при  changes.type = closed_route, делает выгрузку секции closed_route
     *
     * @param change   объект изменений из БД (таблица changes)
     * @param format   формат записи csv файла
     * @param filePath путь до файла с результатами выгрузки
     * @throws Exception
     */
    private void makeClosedRouteLoad(EASUMapper mapper, Changes change, CSVFormat format, String filePath, Long exportID) throws Exception {
        List<ClosedRoute> closedRoutes = mapper.selectClosedRoutesByRouteId(exportID, change.getRoute_variant_id());
        if (closedRoutes.size() == 0) return;

        FileUtils.createFileIfNotExist(filePath);
        try (CSVPrinter printer = new CSVPrinter(new FileWriter(filePath), format)) {
            printer.printRecord("[Closed Route]");
            for (ClosedRoute closedRoute : closedRoutes) {
                printer.printRecord(closedRoute.getRoute_variant_id(),
                        closedRoute.getClose_date(),
                        closedRoute.getClose_time());
            }
        }
        log.info("Processed route and schedule with route_id {} to file {}", change.getRoute_id(), filePath);
    }
}