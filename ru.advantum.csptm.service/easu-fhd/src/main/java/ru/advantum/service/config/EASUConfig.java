package ru.advantum.service.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;

import java.util.List;

/**
 * Класс настроек сервиса ЕАСУ ФХД
 */
@Root(name = "easu-fhd")
public class EASUConfig {
    /**
     * Внутреннее название БД
     */
    @Attribute(name = "databaseInternalName")
    private String databaseInternalName;

    /**
     * Директория для лога по обработанным файлам
     */
    @Attribute(name = "logDirectory")
    private String logDirectory;

    /**
     * Срок хранения файлов лога
     */
    @Attribute(name = "logTimeout")
    private Integer logTimeout;

    /**
     * Имя файла в котором хранятся имена обработанных файлов
     */
    @Attribute(name = "processedFile")
    private String processedFile;

    /**
     * Настройки подключения к БД
     */
    @Element(name = "database")
    private DatabaseConnection database;

    /**
     * Список обработчиков файлов
     */
    @ElementList(name = "file-handlers")
    private List<EASUFileHandlerElement> handlers;

    /**
     * Конфиг директорий для обработки
     */
    @Element(name = "csv-sources")
    private EASUCsvSourceConfig csvSourceConfig;

    /**
     * Настройки выгрузки транспортной работы
     */
    @Element(name = "transport-work")
    private EASUTransportWork transportWork;

    /**
     * Настройки выгрузки расписаний и маршрутов
     */
    @Element(name = "routes-and-schedule")
    private EASURaS routesAndSchedule;

    public DatabaseConnection getDatabase() {
        return database;
    }

    public String getDatabaseInternalName() {
        return databaseInternalName;
    }

    public List<EASUFileHandlerElement> getHandlers() {
        return handlers;
    }

    public EASUCsvSourceConfig getCsvSourceConfig() {
        return csvSourceConfig;
    }

    public String getLogDirectory() {
        return logDirectory;
    }

    public String getProcessedFile() {
        return processedFile;
    }

    public Integer getLogTimeout() {
        return logTimeout;
    }

    public EASUTransportWork getTransportWork() {
        return transportWork;
    }

    public EASURaS getRoutesAndSchedule() {
        return routesAndSchedule;
    }
}
