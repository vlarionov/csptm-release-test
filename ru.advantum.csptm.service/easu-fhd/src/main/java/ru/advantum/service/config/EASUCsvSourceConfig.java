package ru.advantum.service.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Leonid on 20.02.2017.
 */
@Root(name = "csv-sources")
public class EASUCsvSourceConfig {
    @Attribute(name = "period")
    private Integer period;

    @Attribute(name = "timeUnit")
    private String timeUnit = "MINUTES";

    @ElementList(name = "sources")
    private List<EASUCsvSourceElement> sources;

    public Integer getPeriod() {
        return period;
    }

    public String getTimeUnit() {
        return timeUnit;
    }

    public List<EASUCsvSourceElement> getSources() {
        return sources;
    }
}
