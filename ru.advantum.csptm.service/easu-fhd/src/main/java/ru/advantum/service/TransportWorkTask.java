package ru.advantum.service;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;
import org.apache.ibatis.session.SqlSession;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.service.mappers.EASUMapper;
import ru.advantum.service.models.TransportWork;

import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static java.time.temporal.ChronoUnit.DAYS;

/**
 * Класс выгрузки транспортной работы
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class TransportWorkTask implements Job {

    private static final Logger log = LoggerFactory.getLogger(TransportWorkTask.class);

    private JobDataMap dataMap;

    private Map<Integer, String> closedRoutes;
    private LocalDateTime lastExecute = LocalDateTime.now();
    private String databaseName;

    /**
     * Срабатывает по расписанию, анализирует записи из таблицы закрытых маршрутов (closed_routes) за текущую дату,
     * при появлении новых записей, выполняет выгрузку транспортной работы где дата окончания совпадает
     * с датой закрытия маршрута (текущая дата)
     */
    @Override
    public void execute(JobExecutionContext context) {
        log.info("start transport work loading....");

        dataMap = context.getJobDetail().getJobDataMap();
        closedRoutes = (Map<Integer, String>) dataMap.get("closedRoutesMap");
        databaseName = (String) dataMap.get("databaseName");
        //String cacheFile = (String) dataMap.get("cacheFile");

        //LocalDateTime now = LocalDateTime.now();
        LocalDateTime now = LocalDateTime.now().minusDays(1);
        if (DAYS.between(lastExecute, now) > 0) closedRoutes.clear(); //очищать файл кеша
        lastExecute = now;

        List<Map<String, Object>> routes = null;
        try (SqlSession session = SqlMapperSingleton.getInstance(databaseName).openSession()) {
            EASUMapper mapper = session.getMapper(EASUMapper.class);
            routes = mapper.selectClosedRoutes(Date.from(now.atZone(ZoneId.systemDefault()).toInstant()));
        } catch (Exception ex) {
            log.error("TransportWorkTask", ex);
        }

        if (routes != null) {
            log.debug("routes size = {}", routes.size());
            routes.stream().filter((route) -> !closedRoutes.containsKey(route.get("tt_variant_id"))).forEach((route) -> {
                try {
                    closedRoutes.put((Integer) route.get("tt_variant_id"), (String) route.get("file_name_prefix"));
                    //FileUtils.serializeMap(closedRoutes, cacheFile);
                    getTransportWork((Integer) route.get("tt_variant_id"),
                            (String) route.get("file_name_prefix"),
                            (String) route.get("route_num"));
                } catch (Exception e) {
                    log.error("", e);
                }
            });
        }
        log.info("finish transport work loading....");
    }

    /**
     * Метод выгрузки транспортной работы закрытую в указанную дату, для указанного номера маршрута
     */
    private void getTransportWork(Integer ttVariantID, String filePrefix, String routeNumber) {

        try (SqlSession session = SqlMapperSingleton.getInstance(databaseName).openSession()) {

            String directory = (String) dataMap.get("transportWorkDirectory");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");

            EASUMapper mapper = session.getMapper(EASUMapper.class);
            List<TransportWork> transportWorkList = mapper.selectTransportWork(ttVariantID);

            log.debug("transport work size = {}", transportWorkList.size());
            if (!Files.exists(Paths.get(directory))) Files.createDirectories(Paths.get(directory));

            Date now = new Date();
            StringBuilder builder = new StringBuilder(directory);
            builder.append("/").append(filePrefix)
                    .append("-").append(dateFormat.format(now))
                    .append(timeFormat.format(now)).append(".csv");

            String filePath = builder.toString();
            if (!Files.exists(Paths.get(filePath))) {
                Files.createFile(Paths.get(filePath));
                log.debug("create file {}", filePath);
            }

            CSVFormat csvFileFormat = CSVFormat.DEFAULT.withDelimiter(';').withNullString("")
                    .withFirstRecordAsHeader().withEscape('%').withQuoteMode(QuoteMode.NONE);
            try (CSVPrinter csvPrinter = new CSVPrinter(new FileWriter(filePath), csvFileFormat)) {
                csvPrinter.printRecord("[New Fact]");

                for (TransportWork transportWork : transportWorkList) {
                    csvPrinter.printRecord(transportWork.getPark(),
                            dateFormat.format(transportWork.getGroup_date()),
                            transportWork.getRoute_type(),
                            transportWork.getCustomer_type(),
                            transportWork.getRoute_number(),
                            transportWork.getOut_number(),
                            transportWork.getGarage_number(),
                            transportWork.getShift_number(),
                            transportWork.getTable_number(),
                            transportWork.getTime_code(),
                            transportWork.getDate_begin(),
                            transportWork.getTime_begin(),
                            transportWork.getDate_end(),
                            transportWork.getTime_end(),
                            transportWork.getStay_type(),
                            transportWork.getMass(),
                            transportWork.getLength(),
                            transportWork.getEquipment_mask(),
                            transportWork.getEquipment_time(),
                            transportWork.getEngine_time(),
                            transportWork.getFroute_type(),
                            transportWork.getFcustomer_type(),
                            transportWork.getFroute_number(),
                            transportWork.getFout_number(),
                            transportWork.getAcc_begin(),
                            transportWork.getAcc_end(),
                            transportWork.getInterval_count(),
                            transportWork.getFpark_number());
                }
                csvPrinter.flush();
            }

            log.info("processed transport work for a route with number {} {}", routeNumber, filePrefix);
        } catch (Exception ex) {
            log.error("getTransportWork", ex);
        }
    }
}
