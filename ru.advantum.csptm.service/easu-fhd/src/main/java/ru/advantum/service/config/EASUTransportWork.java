package ru.advantum.service.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Класс настроек выгрузки транспортной работы
 */
@Root(name = "transport-work")
public class EASUTransportWork {
    /**
     * Расписание выгрузки транспортной работы в формате cron
     */
    @Attribute(name = "schedule")
    private String schedule;

    /**
     * Директория для файлов результата
     */
    @Attribute(name = "directory")
    private String directory;

    /**
     * Файл с идентификаторами выполненных выгрузок
     */
    @Attribute(name = "cacheFile")
    private String cacheFile;

    public String getSchedule() {
        return schedule;
    }

    public String getDirectory() {
        return directory;
    }

    public String getCacheFile() {
        return cacheFile;
    }
}
