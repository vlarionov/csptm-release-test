package ru.advantum.service.fileHandlers;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import ru.advantum.service.FileUtils;
import ru.advantum.service.SqlMapperSingleton;
import ru.advantum.service.abstracts.CsvFileHandler;
import ru.advantum.service.mappers.EASUMapper;
import ru.advantum.service.models.TrOut;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Leonid on 22.11.2016.
 */
public class GroupFileHandler extends CsvFileHandler {

    private Logger log = Logger.getLogger(GroupFileHandler.class);

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    private final SimpleDateFormat timeFormat = new SimpleDateFormat("hhmmss");
    private final SimpleDateFormat errDateFormat = new SimpleDateFormat("dd.MM.yyyy");

    private String parkNumber;
    private String fileDate;
    private final List<String> duplicateFields = Arrays.asList("ROUNR_EXT", "DEPNR_EXT", "SHNUM");
    private final String logDir;

    public GroupFileHandler(String database, Character delimiter) throws IOException {
        super(database, delimiter);
        this.logDir = System.getProperty("easu.fhd.log.dir");
    }

    /**
     * @param headers заголовок файла
     * @param record запись
     * @param rTypeIndex индекс в заголовке по которому находится поле RTYPE
     * @return true если запись прошла валидация, false если нет
     */
    private Boolean validate(CSVRecord headers, CSVRecord record, Writer log, Integer rTypeIndex) throws Exception {
        Boolean result = true;
        Boolean isRtype = record.get(rTypeIndex) != null && record.get(rTypeIndex).contains("R");

        for (int i = 0; i < headers.size(); i++) {
            switch (headers.get(i)) {
                case "RTYPE": {
                    if (record.get(i) == null || record.get(i).isEmpty()) {
                        result = false;
                        log.write("Строка №" + record.getRecordNumber() + ". Тип маршрута не указан" + System.lineSeparator());
                    }
                    break;
                }
                case "PRKNU": {
                    if (!record.get(i).equals(parkNumber)) {
                        result = false;
                        log.write("Строка №" + record.getRecordNumber() + ". Код парка не соответствует имени файла" + System.lineSeparator());
                    }
                    break;
                }
                case "PERNR": {
                    if (isRtype) {
                        break;
                    }
                    if (record.get(i).length() > 10) {
                        result = false;
                        log.write("Строка №" + record.getRecordNumber() + ". Длина табельного номера водителя превышает 10 симв" + System.lineSeparator());
                    }
                    if (!record.get(i).matches("-?\\d+(\\.\\d+)?")) {
                        result = false;
                        log.write("Строка №" + record.getRecordNumber() + ". Табельный номер водителя не целочисленный" + System.lineSeparator());
                    }
                    break;
                }
                case "EQFNR": {
                    if ((record.get(i) == null || record.get(i).isEmpty()) && !isRtype) {
                        result = false;
                        log.write("Строка №" + record.getRecordNumber() + ". Отсутствует гаражный номер" + System.lineSeparator());
                    }
                    break;
                }
                case "RKIND": {
                    if (record.get(i) == null || record.get(i).isEmpty()) {
                        result = false;
                        log.write("Строка №" + record.getRecordNumber() + ". Вид ТС не указан" + System.lineSeparator());
                    }
                    break;
                }
                case "ADDAT": {
                    if (!record.get(i).equals(fileDate)) {
                        result = false;
                        log.write("Строка №" + record.getRecordNumber() + ". Дата выхода в файле не соответствует дате в имени файла" + System.lineSeparator());
                    }
                    if (!record.get(i).matches("\\d{8}")) {
                        result = false;
                        log.write("Строка №" + record.getRecordNumber() + ". Дата выхода в неверном формате" + System.lineSeparator());
                    }
                    break;
                }
                case "SHNUM": {
                    if (isRtype) {
                        break;
                    }
                    if (!record.get(i).matches("-?\\d+(\\.\\d+)?")) {
                        result = false;
                        log.write("Строка №" + record.getRecordNumber() + ". Номер смены нецелочисленный" + System.lineSeparator());
                    }
                    if (record.get(i).length() > 10) {
                        result = false;
                        log.write("Строка №" + record.getRecordNumber() + ". Длина номера смены превышает 10 симв" + System.lineSeparator());
                    }
                    break;
                }
                case "TIME_FROM": {
                    if (!record.get(i).matches("\\d+") && !isRtype) {
                        result = false;
                        log.write("Строка №" + record.getRecordNumber() + ". Время начала смены в ошибочном формате" + System.lineSeparator());
                    }
                    break;
                }
                case "TIME_TO": {
                    if (!record.get(i).matches("\\d+") && !isRtype) {
                        result = false;
                        log.write("Строка №" + record.getRecordNumber() + ". Время окончания смены в ошибочном формате" + System.lineSeparator());
                    }
                    break;
                }
            }
        }

        return result;
    }

    /**
     * Обработка номера ошибки из БД
     * @param errNum
     * @param rowNum
     * @param group
     * @param log_writer
     * @param mapper
     * @throws IOException
     */
    private void validateErrorNum(Integer errNum, long rowNum, Map<String, Object> group, Writer log_writer, EASUMapper mapper) throws IOException {
        if (errNum == null) {
            log.error("ErrorNum = null");
            return;
        }
        if (errNum >= 0) return;

        String message;
        switch (errNum) {
            case -1:
                message = "Строка №" + rowNum + ". Код парка соответствует несуществующему парку";
                break;
            case -2:
                message = "Строка №" + rowNum + ". Водитель с табельным номером " + group.get("PERNR") + " в парке " + group.get("PRKNU") + " не найден";
                break;
            case -3:
                message = "Строка №" + rowNum + ". Маршрут Номер " + group.get("ROUNR_EXT") + " не найден";
                break;
            case -4:
                message = "Строка №" + rowNum + ". Несуществующий вид транспортного средства";
                break;
            case -5:
                message = "Строка №" + rowNum + ". Недопустимый тип маршрута";
                break;
            case -6:
                message = "Строка №" + rowNum + ". Не найдено ТС [вид ТС = " + group.get("RKIND") + "] с гаражным номером " + group.get("EQFNR") + " в парке " + group.get("PRKNU");
                break;
            case -7:
                message = "Строка №" + rowNum + ". Смена водителя не задана для обычного маршрута";
                break;
            case -8:
                message = "Строка №" + rowNum + ". Пересечение периодов работы водителя";
                break;
            case -9:
                message = "Строка №" + rowNum + ". Ошибка выпуска на линию. Отсутствуют активные расписания на " + errDateFormat.format(group.get("ADDAT")) + " по маршруту " + group.get("ROUNR_EXT");
                break;
            case -10:
                TrOut o = mapper.selectTrCurrentRouteOut((String) group.get("PRKNU"), (Long) group.get("EQFNR"));
                message = "Строка №" + rowNum + ". Ошибка выпуска на линию. ТС " + group.get("EQFNR") + " уже выпущено по маршруту " + o.getRoute_num() + ", выход № " + o.getOut_num();
                break;
            case -11:
                message = "Строка №" + rowNum + ". Неверный номер выхода";
                break;
            // ...
            default:
                log.error("Строка №" + rowNum + ". Неизвестный номер ошибки " + (errNum == null ? "null" : errNum.toString()));
                return;
        }
        log_writer.write(message + System.lineSeparator());
    }

    /**
     * Метод считывает файл с нарядами, валидирует, и отправляет данные в БД если все ок
     * @param filename имя файла
     * @param reader объект для чтения содержимого файла
     * @throws Exception
     */
    @Override
    public void handle(String filename, Reader reader) throws Exception {
        log.info("Start load file " + filename);

        parkNumber = filename.substring(filename.indexOf("_") + 1, filename.lastIndexOf("_"));
        fileDate = filename.substring(0, 8);
        /*PatternLayout layout = new PatternLayout("%m%n");
        FileAppender appender = new FileAppender(layout, logDir + "/" + filename.replace(".csv", "log.csv"), false);
        appender.activateOptions();
        log.addAppender(appender);*/
        String logFilePath = logDir + "/" + filename.replace(".csv", "csv.log");
        FileUtils.createFileIfNotExist(logFilePath);

        Long okRecords = 0L;
        Long initRecordsSize = 0L;
        Long distinctRecordsSize = 0L;

        try (SqlSession session = SqlMapperSingleton.getInstance(database).openSession();
             FileWriter logWriter = new FileWriter(logFilePath)) {

            EASUMapper mapper = session.getMapper(EASUMapper.class);

            CSVParser parser = CSVFormat.DEFAULT.withDelimiter(delimiter).parse(reader);
            Iterator it = parser.iterator();
            CSVRecord headers = (CSVRecord) it.next();
            Integer rTypeIndex = headers.size() - 1;

            //вычисляем индексы полей по которым фильтровать дубликаты
            List<Integer> duplicateIndexes = new ArrayList<>();
            for (int i = 0; i < headers.size(); i++) {
                if (duplicateFields.contains(headers.get(i))) {
                    duplicateIndexes.add(i);
                }

                if (headers.get(i).equals("RTYPE")) {
                    rTypeIndex = i;
                }
            }

            //фильтруем дубликаты
            Map<String, CSVRecord> distinctMap = new HashMap<>();
            List<CSVRecord> records = parser.getRecords();
            for (CSVRecord record : records) {
                String key = duplicateIndexes.stream().map(record::get).collect(Collectors.joining());

                distinctMap.put(key, record);
            }

            //вставляем записи
            Collection<CSVRecord> uniqRecords = distinctMap.values();
            List<CSVRecord> sortedUniqRecords = uniqRecords.stream().sorted((recordOne, recordTwo) ->
                Long.compare(recordOne.getRecordNumber(), recordTwo.getRecordNumber()))
                .collect(Collectors.toList());

            initRecordsSize = (long) records.size();
            distinctRecordsSize = (long) distinctMap.size();

            for (CSVRecord record : sortedUniqRecords) {
                if (validate(headers, record, logWriter, rTypeIndex) && record.size() == headers.size()) {
                    okRecords++;
                    Map<String, Object> toDb = new HashMap<>();
                    for (int i = 0; i < headers.size(); i++) {
                        switch (headers.get(i)) {
                            case "ADDAT": {
                                Date date = dateFormat.parse(record.get(i));
                                toDb.put(headers.get(i), date);
                                break;
                            }
                            case "TIME_FROM":
                            case "TIME_TO": {
                                Date date = timeFormat.parse(record.get(i));
                                toDb.put(headers.get(i), date);
                                break;
                            }
                            case "RKIND":
                            case "SHNUM": {
                                toDb.put(headers.get(i), record.get(i).isEmpty() ? null : Short.valueOf(record.get(i)));
                                break;
                            }
                            case "PERNR":
                            case "EQFNR":
                            case "EQFNR2": {
                                toDb.put(headers.get(i), record.get(i).isEmpty() ? null : Long.valueOf(record.get(i)));
                                break;
                            }
                            default: {
                                toDb.put(headers.get(i), record.get(i));
                            }
                        }
                    }

                    Integer e = mapper.insertGroup(toDb);
                    session.commit(true);
                    validateErrorNum(e, record.getRecordNumber(), toDb, logWriter, mapper);
                }

                logWriter.flush();
            }
        } finally {
            /*appender.close();
            log.removeAppender(appender);*/

            log.info(String.format("records in file %d, distinct records %d, ok %d, failed %d",
                initRecordsSize, distinctRecordsSize, okRecords, distinctRecordsSize - okRecords));
        }
    }

    /**
     * Данный вид файлов требует запуска постобработки
     * @return
     */
    @Override
    public String getDatabaseForPostProcess() {
        return this.database;
    }
}
