package ru.advantum.service.models;

import java.util.Date;

/**
 * Класс - сущность транспортной работы
 */
public class TransportWork {
    private String park;
    private Date group_date;
    private String route_type;
    private Short customer_type;
    private String route_number;
    private String out_number;
    private Integer garage_number;
    private Short shift_number;
    private Long table_number;
    private Short time_code;
    private String date_begin;
    private String time_begin;
    private String date_end;
    private String time_end;
    private String stay_type;
    private Long mass;
    private Long length;
    private String equipment_mask;
    private Long equipment_time;
    private Long engine_time;
    private String froute_type;
    private Short fcustomer_type;
    private String froute_number;
    private String fout_number;
    private Long acc_begin;
    private Long acc_end;
    private Integer interval_count;
    private String fpark_number;

    public String getPark() {
        return park;
    }

    public void setPark(String park) {
        this.park = park;
    }

    public Date getGroup_date() {
        return group_date;
    }

    public void setGroup_date(Date group_date) {
        this.group_date = group_date;
    }

    public String getRoute_type() {
        return route_type;
    }

    public void setRoute_type(String route_type) {
        this.route_type = route_type;
    }

    public Short getCustomer_type() {
        return customer_type;
    }

    public void setCustomer_type(Short customer_type) {
        this.customer_type = customer_type;
    }

    public String getRoute_number() {
        return route_number;
    }

    public void setRoute_number(String route_number) {
        this.route_number = route_number;
    }

    public String getOut_number() {
        return out_number;
    }

    public void setOut_number(String out_number) {
        this.out_number = out_number;
    }

    public Integer getGarage_number() {
        return garage_number;
    }

    public void setGarage_number(Integer garage_number) {
        this.garage_number = garage_number;
    }

    public Short getShift_number() {
        return shift_number;
    }

    public void setShift_number(Short shift_number) {
        this.shift_number = shift_number;
    }

    public Long getTable_number() {
        return table_number;
    }

    public void setTable_number(Long table_number) {
        this.table_number = table_number;
    }

    public Short getTime_code() {
        return time_code;
    }

    public void setTime_code(Short time_code) {
        this.time_code = time_code;
    }

    public String getDate_begin() {
        return date_begin;
    }

    public void setDate_begin(String date_begin) {
        this.date_begin = date_begin;
    }

    public String getTime_begin() {
        return time_begin;
    }

    public void setTime_begin(String time_begin) {
        this.time_begin = time_begin;
    }

    public String getDate_end() {
        return date_end;
    }

    public void setDate_end(String date_end) {
        this.date_end = date_end;
    }

    public String getTime_end() {
        return time_end;
    }

    public void setTime_end(String time_end) {
        this.time_end = time_end;
    }

    public String getStay_type() {
        return stay_type;
    }

    public void setStay_type(String stay_type) {
        this.stay_type = stay_type;
    }

    public Long getMass() {
        return mass;
    }

    public void setMass(Long mass) {
        this.mass = mass;
    }

    public Long getLength() {
        return length;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    public String getEquipment_mask() {
        return equipment_mask;
    }

    public void setEquipment_mask(String equipment_mask) {
        this.equipment_mask = equipment_mask;
    }

    public Long getEquipment_time() {
        return equipment_time;
    }

    public void setEquipment_time(Long equipment_time) {
        this.equipment_time = equipment_time;
    }

    public Long getEngine_time() {
        return engine_time;
    }

    public void setEngine_time(Long engine_time) {
        this.engine_time = engine_time;
    }

    public String getFroute_type() {
        return froute_type;
    }

    public void setFroute_type(String froute_type) {
        this.froute_type = froute_type;
    }

    public Short getFcustomer_type() {
        return fcustomer_type;
    }

    public void setFcustomer_type(Short fcustomer_type) {
        this.fcustomer_type = fcustomer_type;
    }

    public String getFroute_number() {
        return froute_number;
    }

    public void setFroute_number(String froute_number) {
        this.froute_number = froute_number;
    }

    public String getFout_number() {
        return fout_number;
    }

    public void setFout_number(String fout_number) {
        this.fout_number = fout_number;
    }

    public Long getAcc_begin() {
        return acc_begin;
    }

    public void setAcc_begin(Long acc_begin) {
        this.acc_begin = acc_begin;
    }

    public Long getAcc_end() {
        return acc_end;
    }

    public void setAcc_end(Long acc_end) {
        this.acc_end = acc_end;
    }

    public Integer getInterval_count() {
        return interval_count;
    }

    public void setInterval_count(Integer interval_count) {
        this.interval_count = interval_count;
    }

    public String getFpark_number() {
        return fpark_number;
    }

    public void setFpark_number(String fpark_number) {
        this.fpark_number = fpark_number;
    }

}
