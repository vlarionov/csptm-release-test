package ru.advantum.service.tasks;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Класс удаления устаревших лог файлов
 * Срабатывает по таймеру
 */
public class LogCleanerTask implements Job {

    private static Logger log = LoggerFactory.getLogger(LogCleanerTask.class);

    private String logDirectory;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    private Integer logTimeout;

    /**
     * Данный метод запускается по таймеру, в директории logDir удаляются файлы с истекшим сроком хранения logTimeout
     * @param context контекст запуска класса
     * @throws JobExecutionException
     */
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDataMap data = context.getJobDetail().getJobDataMap();
        logDirectory = (String) data.get("logDir");
        logTimeout = (Integer) data.get("logTimeout");

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -(logTimeout));
        Date expiredDate = calendar.getTime();
        //String expiredDate = dateFormat.format(date);

        File directory = new File(logDirectory);
        File[] files = directory.listFiles();
        for (File file : files) {
            try {
                Date fileDate = dateFormat.parse(file.getName().substring(0, 8));
                if (fileDate.before(expiredDate)) {
                    Files.delete(file.toPath());
                    log.info("remove file {}", file.getName());
                }
            } catch (Exception ex) {
                log.error("", ex);
            }
        }
    }
}
