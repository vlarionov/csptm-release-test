package ru.advantum.service.models;

/**
 * Класс - сущность остановочных пунктов линий
 */
public class PathNode {
    private String route_variant_code;
    private Integer round_id;
    private Integer stop_id;
    private Integer tr_type_id;

    public String getRoute_variant_code() {
        return route_variant_code;
    }

    public void setRoute_variant_code(String route_variant_code) {
        this.route_variant_code = route_variant_code;
    }

    public Integer getRound_id() {
        return round_id;
    }

    public void setRound_id(Integer round_id) {
        this.round_id = round_id;
    }

    public Integer getStop_id() {
        return stop_id;
    }

    public void setStop_id(Integer stop_id) {
        this.stop_id = stop_id;
    }

    public Integer getTr_type_id() {
        return tr_type_id;
    }

    public void setTr_type_id(Integer tr_type_id) {
        this.tr_type_id = tr_type_id;
    }
}
