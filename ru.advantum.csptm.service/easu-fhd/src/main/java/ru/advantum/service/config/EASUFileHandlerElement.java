package ru.advantum.service.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Класс настроек обработчика csv файлов
 */
@Root(name = "type")
public class EASUFileHandlerElement {

    /**
     * Паттерн имени файла, если имя файла удовлетворяет этому паттерну,
     * то он обрабатывается указанным в type обработчиком
     */
    @Attribute(name = "pattern")
    private String pattern;

    /**
     * Класс обработчика
     */
    @Attribute(name = "type")
    private String type;

    /**
     * Разделитель записей в файлах
     */
    @Attribute(name = "delimiter")
    private String delimiter;

    public String getDelimiter() {
        return delimiter;
    }

    public String getPattern() {
        return pattern;
    }

    public String getType() {
        return type;
    }
}
