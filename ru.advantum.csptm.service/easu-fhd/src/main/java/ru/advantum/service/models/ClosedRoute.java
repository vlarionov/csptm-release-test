package ru.advantum.service.models;

/**
 * Класс - сущность закрытия маршрута
 */
public class ClosedRoute {
    private String route_variant_code;
    private String close_date;
    private String close_time;

    public String getRoute_variant_id() {
        return route_variant_code;
    }

    public void setRoute_variant_id(String route_variant_id) {
        this.route_variant_code = route_variant_id;
    }

    public String getClose_date() {
        return close_date;
    }

    public void setClose_date(String close_date) {
        this.close_date = close_date;
    }

    public String getClose_time() {
        return close_time;
    }

    public void setClose_time(String close_time) {
        this.close_time = close_time;
    }
}
