package ru.advantum.service;

import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.service.abstracts.CsvFileHandler;
import ru.advantum.service.config.EASUConfig;
import ru.advantum.service.config.EASUFileHandlerElement;
import ru.advantum.service.mappers.EASUMapper;
import ru.advantum.service.tasks.LogCleanerTask;
import ru.advantum.service.tasks.ScanNetworkTask;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * Класс сервиса, содержит точку входа и производит начальные настройки, чтение конфига, подключение к БД и т.д.
 * Автоматически данный сервис выгружает данные о водителях и нарядах в БД КСУПТ.
 * Выгрузка транспортной работы происходит по событию закрытия маршрута.
 * Выгрузка расписаний и маршрутов происходит по событию изменения расписаний или маршрута.
 */
public class EASUService {

    public static void main(String[] args) throws Exception {
        EASUConfig config = Configuration.unpackConfig(EASUConfig.class, "easu-fhd.xml");

        SqlMapperSingleton.registerInstance(
                config.getDatabaseInternalName(),
                new Environment(
                        config.getDatabaseInternalName(),
                        new JdbcTransactionFactory(),
                        config.getDatabase().getConnectionPool()),
                factory -> {
                    factory.getConfiguration().addMapper(EASUMapper.class);
                });

        System.setProperty("easu.fhd.log.dir", config.getLogDirectory());

        Map<Pattern, CsvFileHandler> handlerMap = new HashMap<>();
        for (EASUFileHandlerElement handlerConfig : config.getHandlers()) {
            Pattern pattern = Pattern.compile(handlerConfig.getPattern());
            CsvFileHandler handler = (CsvFileHandler) Class.forName(handlerConfig.getType())
                    .getConstructor(String.class, Character.class)
                    .newInstance(config.getDatabaseInternalName(), handlerConfig.getDelimiter().charAt(0));

            handlerMap.put(pattern, handler);
        }

        Set<String> processedFiles = ProcessedFileSaver.load(config.getProcessedFile());
        ProcessedFileSaver fileSaver = new ProcessedFileSaver(config.getProcessedFile(), processedFiles);
        Executors.newSingleThreadExecutor().execute(fileSaver);

        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

        JobDataMap scanNetworkData = new JobDataMap();
        scanNetworkData.put("config", config);
        scanNetworkData.put("handlerMap", handlerMap);
        scanNetworkData.put("processedFiles", processedFiles);

        JobDetail scanNetwork = newJob(ScanNetworkTask.class).withIdentity("scanFtp", "group1").usingJobData(scanNetworkData).build();
        SimpleTrigger scanTrigger = newTrigger()
                .withIdentity("scanTrigger", "group1")
                .startNow()
                .withSchedule(getSchedule(config.getCsvSourceConfig().getPeriod(), config.getCsvSourceConfig().getTimeUnit())
                        .repeatForever())
                .build();

        JobDataMap cleanLogData = new JobDataMap();
        cleanLogData.put("logDir", config.getLogDirectory());
        cleanLogData.put("logTimeout", config.getLogTimeout());

        JobDetail cleanLogs = newJob(LogCleanerTask.class).withIdentity("cleanLogs", "group1").usingJobData(cleanLogData).build();
        CronTrigger cleanLogsTrigger = newTrigger()
                .withIdentity("cleanLogsTrigger", "group1")
                .withSchedule(cronSchedule("0 0/2 * * * ?"))
                .build();

        JobDataMap transportWorkData = new JobDataMap();
        transportWorkData.put("transportWorkDirectory", config.getTransportWork().getDirectory());
        transportWorkData.put("databaseName", config.getDatabaseInternalName());
        transportWorkData.put("closedRoutesMap", FileUtils.deserializeMap(config.getTransportWork().getCacheFile()));
        transportWorkData.put("cacheFile", config.getTransportWork().getCacheFile());

        JobDetail transportWorkWriter = newJob(TransportWorkTask.class).withIdentity("transportWork", "group1")
                .usingJobData(transportWorkData).build();

        CronTrigger transportWorkTrigger = newTrigger()
                .withIdentity("transportWorkTrigger", "group1")
                .withSchedule(cronSchedule(config.getTransportWork().getSchedule()))
                .build();

        JobDataMap rasData = new JobDataMap();
        rasData.put("rasDirectory", config.getRoutesAndSchedule().getDirectory());
        rasData.put("databaseName", config.getDatabaseInternalName());
        rasData.put("rasMap", FileUtils.deserializeMap(config.getRoutesAndSchedule().getCacheFile()));
        rasData.put("cacheFile", config.getRoutesAndSchedule().getCacheFile());

        JobDetail rasWriter = newJob(RoutesAndScheduleTask.class)
                .withIdentity("rasWriter", "group1")
                .usingJobData(rasData)
                .build();

        SimpleTrigger rasTrigger = newTrigger()
                .withIdentity("rasTrigger", "group1")
                .startNow()
                .withSchedule(simpleSchedule()
                        .withIntervalInMinutes(config.getRoutesAndSchedule().getPeriod())
                        .repeatForever())
                .build();

        scheduler.scheduleJob(scanNetwork, scanTrigger);
        scheduler.scheduleJob(cleanLogs, cleanLogsTrigger);
        scheduler.scheduleJob(rasWriter, rasTrigger);
        scheduler.scheduleJob(transportWorkWriter, transportWorkTrigger);
        scheduler.start();
    }

    private static SimpleScheduleBuilder getSchedule(final Integer interval, final String timeUnit) {
        SimpleScheduleBuilder builder = simpleSchedule();
        switch (timeUnit.toUpperCase()) {
            case "MINUTES": {
                builder.withIntervalInMinutes(interval);
                break;
            }
            case "SECONDS": {
                builder.withIntervalInSeconds(interval);
                break;
            }
            case "HOURS": {
                builder.withIntervalInHours(interval);
                break;
            }
            case "MILLIS": {
                builder.withIntervalInMilliseconds(interval);
                break;
            }
            default: {
                throw new IllegalArgumentException("Wrong argument timeUnit " + timeUnit + " available values are HOURS, MINUTES, SECONDS, MILLIS");
            }
        }

        return builder;
    }
}
