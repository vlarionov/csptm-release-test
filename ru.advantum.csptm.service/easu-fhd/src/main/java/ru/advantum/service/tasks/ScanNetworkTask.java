package ru.advantum.service.tasks;

import org.apache.ibatis.session.SqlSession;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.service.SqlMapperSingleton;
import ru.advantum.service.abstracts.CsvFileHandler;
import ru.advantum.service.config.EASUConfig;
import ru.advantum.service.config.EASUCsvSourceElement;
import ru.advantum.service.mappers.EASUMapper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Класс сканирования сетевой папки для поиска файлов на обработку
 */
public class ScanNetworkTask implements Job {

    private static Logger log = LoggerFactory.getLogger(ScanNetworkTask.class);

    private EASUConfig config;
    private Set<String> processedFiles;
    private final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyyMMdd");
    private Map<Pattern, CsvFileHandler> handlerMap;

    private void postProcessFiles(Set<String> postProcessDB) {
        for (String database : postProcessDB) {
            log.info("Start post file import process " + database);
            try (SqlSession session = SqlMapperSingleton.getInstance(database).openSession()) {
                EASUMapper mapper = session.getMapper(EASUMapper.class);
                mapper.ttProcessRequest();
                session.commit();
            }
        }
    }

    /**
     * Метод поиска обработчика для файла file и запуск обработки если такой находится
     * @param file файл для обработки
     * @throws Exception
     */
    private void processFile(File file, Set<String> postProcessDB) throws Exception {
        for (Map.Entry<Pattern, CsvFileHandler> entry : handlerMap.entrySet()) {
            if (entry.getKey().matcher(file.getName()).matches()) {
                try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                    if (entry.getValue().getDatabaseForPostProcess() != null) {
                        postProcessDB.add(entry.getValue().getDatabaseForPostProcess());
                    }
                    entry.getValue().handle(file.getName(), reader);
                    break;
                }
            }
        }
    }

    /**
     * Поиск файлов запускается по таймеру, просматривается директория за текущую дату,
     * и в ней все файлы проверяются на соответствие паттерну обработчиков
     * @param context контекст запуска сканирования
     * @throws JobExecutionException
     */
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDataMap data = context.getJobDetail().getJobDataMap();
        config = (EASUConfig) data.get("config");
        handlerMap = (Map<Pattern, CsvFileHandler>) data.get("handlerMap");
        processedFiles = (Set<String>) data.get("processedFiles");
        Set<String> postProcessDB = new HashSet<>();

        try {
            for (EASUCsvSourceElement source : config.getCsvSourceConfig().getSources()) {
                //URI uri = new URI(config.getUrl() + dateFormat.format(startDate) + "/");
                String today = dateFormat.format(LocalDateTime.now());
                File directory = new File(source.getUrl() + today);
                log.debug("Open directory {}", directory.getAbsolutePath());
                if (!directory.exists()) {
                    log.debug("No such directory {}", directory.getPath());
                    continue;
                }

                File[] files = directory.listFiles();

                log.info("Start loading unprocessed files from {}", directory);
                log.debug("Found {} files", files.length);
                for (File file : files) {
                    if (!processedFiles.contains(file.getAbsolutePath())) {
                        processFile(file, postProcessDB);

                        log.info("Done file " + file.getAbsolutePath());
                        synchronized (processedFiles) {
                            processedFiles.add(file.getAbsolutePath());
                        }
                    }
                }

                synchronized (processedFiles) {
                    processedFiles.removeIf(filePath -> !filePath.contains(today));
                    processedFiles.notify();
                }
                log.info("Finish loading from {}", directory);
            }
            postProcessFiles(postProcessDB);
        } catch (Exception ex) {
            log.error("", ex);
        }
    }
}
