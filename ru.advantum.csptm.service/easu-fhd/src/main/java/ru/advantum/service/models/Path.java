package ru.advantum.service.models;

/**
 * Класс - сущность линий маршрутов
 */
public class Path {
    private String route_variant_code;
    private Integer round_id;
    private Integer line_type;
    private Integer line_view;
    private Integer movement_code;
    private Integer endpointa;
    private Integer endpointb;
    private String length;

    public String getRoute_variant_code() {
        return route_variant_code;
    }

    public void setRoute_variant_code(String route_variant_code) {
        this.route_variant_code = route_variant_code;
    }

    public Integer getRound_id() {
        return round_id;
    }

    public void setRound_id(Integer round_id) {
        this.round_id = round_id;
    }

    public Integer getLine_type() {
        return line_type;
    }

    public void setLine_type(Integer line_type) {
        this.line_type = line_type;
    }

    public Integer getLine_view() {
        return line_view;
    }

    public void setLine_view(Integer line_view) {
        this.line_view = line_view;
    }

    public Integer getMovement_code() {
        return movement_code;
    }

    public void setMovement_code(Integer movement_code) {
        this.movement_code = movement_code;
    }

    public Integer getEndpointa() {
        return endpointa;
    }

    public void setEndpointa(Integer endpointa) {
        this.endpointa = endpointa;
    }

    public Integer getEndpointb() {
        return endpointb;
    }

    public void setEndpointb(Integer endpointb) {
        this.endpointb = endpointb;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }
}
