package ru.advantum.service.models;

/**
 * Класс - сущность изменения расписания маршрута
 */
public class Schedule {
    private String route_variant_code;
    private Integer status;
    private String date_begin;
    private String time_begin;
    private String date_end;
    private String time_end;
    private String out_number;
    private String park_number;
    private Integer shift_number;
    private String eps_type;
    private Integer out_type;
    private String interval_begin;
    private String interval_end;
    private String interval_code;
    private Integer line_code;
    private Integer froute_id;

    public String getRoute_variant_code() {
        return route_variant_code;
    }

    public void setRoute_variant_code(String route_variant_code) {
        this.route_variant_code = route_variant_code;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDate_begin() {
        return date_begin;
    }

    public void setDate_begin(String date_begin) {
        this.date_begin = date_begin;
    }

    public String getTime_begin() {
        return time_begin;
    }

    public void setTime_begin(String time_begin) {
        this.time_begin = time_begin;
    }

    public String getDate_end() {
        return date_end;
    }

    public void setDate_end(String date_end) {
        this.date_end = date_end;
    }

    public String getTime_end() {
        return time_end;
    }

    public void setTime_end(String time_end) {
        this.time_end = time_end;
    }

    public String getOut_number() {
        return out_number;
    }

    public void setOut_number(String out_number) {
        this.out_number = out_number;
    }

    public String getPark_number() {
        return park_number;
    }

    public void setPark_number(String park_number) {
        this.park_number = park_number;
    }

    public Integer getShift_number() {
        return shift_number;
    }

    public void setShift_number(Integer shift_number) {
        this.shift_number = shift_number;
    }

    public String getEps_type() {
        return eps_type;
    }

    public void setEps_type(String eps_type) {
        this.eps_type = eps_type;
    }

    public Integer getOut_type() {
        return out_type;
    }

    public void setOut_type(Integer out_type) {
        this.out_type = out_type;
    }

    public String getInterval_begin() {
        return interval_begin;
    }

    public void setInterval_begin(String interval_begin) {
        this.interval_begin = interval_begin;
    }

    public String getInterval_end() {
        return interval_end;
    }

    public void setInterval_end(String interval_end) {
        this.interval_end = interval_end;
    }

    public String getInterval_code() {
        return interval_code;
    }

    public void setInterval_code(String interval_code) {
        this.interval_code = interval_code;
    }

    public Integer getLine_code() {
        return line_code;
    }

    public void setLine_code(Integer line_code) {
        this.line_code = line_code;
    }

    public Integer getFroute_id() {
        return froute_id;
    }

    public void setFroute_id(Integer froute_id) {
        this.froute_id = froute_id;
    }
}
