package ru.advantum.service.models;

import java.util.Date;

/**
 * Класс - сущность изменений в расписании маршрута
 */
public class Changes {
    private Long change_id;
    private String change_type;
    private Date change_date;
    private String route_num;
    private Integer route_id;
    private Integer route_variant_id;

    public Long getChange_id() {
        return change_id;
    }

    public String getChange_type() {
        return change_type;
    }

    public Date getChange_date() {
        return change_date;
    }

    public String getRoute_num() {
        return route_num;
    }

    public Integer getRoute_id() {
        return route_id;
    }

    public Integer getRoute_variant_id() {
        return route_variant_id;
    }
}
