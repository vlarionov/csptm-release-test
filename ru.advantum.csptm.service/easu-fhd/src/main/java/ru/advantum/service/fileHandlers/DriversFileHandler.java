package ru.advantum.service.fileHandlers;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.ibatis.session.SqlSession;
import ru.advantum.service.SqlMapperSingleton;
import ru.advantum.service.abstracts.CsvFileHandler;
import ru.advantum.service.mappers.EASUMapper;

import java.io.Reader;
import java.util.Iterator;

/**
 * Класс обработчик выгрузки водителей
 */
public class DriversFileHandler extends CsvFileHandler {

    public DriversFileHandler(String database, Character delimiter) {
        super(database, delimiter);
    }

    /**
     * Метод считывает содержимое файла водителей и отправляет данные в БД
     * @param filename имя файла
     * @param reader объект для чтения содержимого файла
     * @throws Exception
     */
    @Override
    public void handle(String filename, Reader reader) throws Exception {
        try (SqlSession session = SqlMapperSingleton.getInstance(database).openSession()) {
            EASUMapper mapper = session.getMapper(EASUMapper.class);

            CSVParser parser = CSVFormat.DEFAULT.withDelimiter(delimiter).parse(reader);
            Iterator it = parser.iterator();
            CSVRecord headers = (CSVRecord) it.next();

            while (it.hasNext()) {
                CSVRecord record = (CSVRecord) it.next();
                mapper.insertDriver(record.get(0), Long.valueOf(record.get(1)), record.get(2));
                session.commit(true);
            }
        }
    }
}
