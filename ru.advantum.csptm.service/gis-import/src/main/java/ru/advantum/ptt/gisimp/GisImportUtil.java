package ru.advantum.ptt.gisimp;

import org.slf4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stax.StAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

/**
 * @author abc <alekseenkov@advantum.pro>
 */
public class GisImportUtil {

    public static class ErrorMessageData {
        public String messageCode = "";
        public String operation = "";
        public String objectType = "";
        public String objectCode = "";
    }

    /**
     * Получить данные для формирования сообщения об ошибке и фала источника
     */
    public static ErrorMessageData getErrorMessageData(Path file)
        throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {

        ErrorMessageData result = new ErrorMessageData();

        if (file != null && Files.exists(file)) {

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(file.toFile());

            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();

            XPathExpression expr = xpath.compile("/object-message/header/messageIdent");
            result.messageCode = (String) expr.evaluate(doc, XPathConstants.STRING);
            expr = xpath.compile("/object-message/header/operation");
            result.operation = (String) expr.evaluate(doc, XPathConstants.STRING);
            expr = xpath.compile("/object-message/header/objectType");
            result.objectType = (String) expr.evaluate(doc, XPathConstants.STRING);
            expr = xpath.compile("/object-message/body/*/muid");
            result.objectCode = (String) expr.evaluate(doc, XPathConstants.STRING);
        }

        return result;
    }

    /**
     * Создать файл с сообщением об ошибке
     */
    public static void createErrorMessageFile(String fileName, String sourceName, String operation,
                                              String messageCode, String objectType, String objectCode,
                                              String status, String messageText, Logger log) {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("status-message");
            doc.appendChild(rootElement);

            // header elements
            Element header = doc.createElement("header");
            rootElement.appendChild(header);

            // message elements
            Element element = doc.createElement("message-ident");
            element.appendChild(doc.createTextNode(UUID.randomUUID().toString()));
            header.appendChild(element);

            element = doc.createElement("status-for");
            element.appendChild(doc.createTextNode(messageCode));
            header.appendChild(element);

            element = doc.createElement("objectType");
            element.appendChild(doc.createTextNode(objectType));
            header.appendChild(element);

            element = doc.createElement("componentIdent");
            element.appendChild(doc.createTextNode(objectCode));
            header.appendChild(element);

            element = doc.createElement("timestamp");
            element.appendChild(doc.createTextNode(DateTimeFormatter.ISO_INSTANT.format(Instant.now())));
            header.appendChild(element);

            element = doc.createElement("source");
            element.appendChild(doc.createTextNode(sourceName));
            header.appendChild(element);

            element = doc.createElement("operation");
            element.appendChild(doc.createTextNode(operation));
            header.appendChild(element);

            element = doc.createElement("status");
            element.appendChild(doc.createTextNode(status));
            header.appendChild(element);

            element = doc.createElement("status-message");
            element.appendChild(doc.createTextNode(messageText));
            header.appendChild(element);

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(fileName));
            transformer.transform(source, result);

        } catch (TransformerException | ParserConfigurationException ex) {
            log.error("createErrorMessageFile", ex);
        }
    }

    /**
     * Удалить каталог с временными файлами
     */
    public static void deleteDir(Path dir) throws IOException {

        if (!Files.exists(dir)) return;

        Files.walkFileTree(dir, new SimpleFileVisitor<Path>() {

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException e) throws IOException {
                if (e == null) {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                } else {
                    // directory iteration failed
                    throw e;
                }
            }
        });
    }

    /**
     * Разбить файл источник на сообщения
     */
    public static void transformFile(Path file, Path temporaryDir)
        throws IOException, XMLStreamException, TransformerException {

        XMLInputFactory xif = XMLInputFactory.newInstance();
        FileReader fr = null;
        XMLStreamReader xsr = null;
        try {
            fr = new FileReader(file.toFile());
            xsr = xif.createXMLStreamReader(fr);
            xsr.nextTag();

            int i = 0;
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer t = tf.newTransformer();

            while (xsr.nextTag() == XMLStreamConstants.START_ELEMENT) {
                t.transform(new StAXSource(xsr), new StreamResult(temporaryDir.resolve(String.format("m%06d.xml", i++)).toFile()));
            }
        } finally {
            if (xsr != null) xsr.close();
            if (fr != null) fr.close();
        }
    }
}
