package ru.advantum.ptt.gisimp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

/**
 * Проверка на дубликаты
 *
 * @author abc <alekseenkov@advantum.pro>
 */
public class GisImportCheck {

    private static final Logger log = LoggerFactory.getLogger(GisImportCheck.class);

    private Connection connection;
    private String schemaName;

    public GisImportCheck(Connection connection, String schemaName) {
        this.connection = connection;
        this.schemaName = schemaName;
    }

    private void checkDupFunc(String funcName) throws SQLException {
        log.info("Check {}", funcName);
        String sql = String.format("{ ? = call %s()}", funcName);
        try (CallableStatement call = connection.prepareCall(sql)) {
            call.registerOutParameter(1, Types.OTHER);
            call.execute();
            try (ResultSet results = (ResultSet) call.getObject(1)) {
                while (results.next()) {
                    log.info(String.format("object %s muid=%d, %s ",
                        results.getString(3), results.getLong(1), results.getString(2)));
                }
            }
        }
    }

    public void check() throws SQLException {
        log.info("Check imported data start");
        boolean previousAutoCommit = connection.getAutoCommit();
        try {
            connection.setAutoCommit(false);
            String sql = String.format("{ ? = call %s.gis_import_pkg_v3$get_check_func_list() }", schemaName);
            try (CallableStatement call = connection.prepareCall(sql)) {
                call.registerOutParameter(1, Types.OTHER);
                call.execute();
                try (ResultSet results = (ResultSet) call.getObject(1)) {
                    while (results.next()) checkDupFunc(results.getString(1));
                }
            }
        } finally {
            connection.setAutoCommit(previousAutoCommit);
        }
        log.info("Check imported data finish");
    }
}
