package ru.advantum.ptt.gisimp;

public class RMQMail {
    private final String to;
    private final String subject;
    private final String body;

    public RMQMail(String to, String subject, String body) {
        this.to = to;
        this.subject = subject;
        this.body = body;
    }

    public String getTo() {
        return this.to;
    }

    public String getSubject() {
        return this.subject;
    }

    public String getBody() {
        return this.body;
    }
}
