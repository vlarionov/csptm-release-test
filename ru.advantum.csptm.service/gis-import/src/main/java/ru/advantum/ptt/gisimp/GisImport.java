package ru.advantum.ptt.gisimp;

import com.fatboyindustrial.gsonjavatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * @author kaganov
 * @author abc <alekseenkov@advantum.pro>
 */
@Service
public class GisImport {

    private static final Logger log = LoggerFactory.getLogger(GisImport.class);

    private final GisImportConfig config;
    private final JepRmqServiceConfig rmqConfig;
    private final Path importDir;
    private final Path errorDir;
    private final String sqlImportCall;
    private final String sqlProcessStep1Call;
    private final String sqlProcessStep2Call;
    private final ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();
    private final DataSource datasource;

    @Autowired
    public GisImport(GisImportConfig config, JepRmqServiceConfig rmqConfig, DataSource dataSource) throws SQLException {
        this.config = config;
        this.rmqConfig = rmqConfig;
        this.datasource = dataSource;

        importDir = Paths.get(this.config.getImportDir());
        if (this.config.getErrorDir() != null) {
            errorDir = Paths.get(this.config.getErrorDir());
        } else {
            errorDir = null;
        }

        switch (config.getImportVersion()) {
            case 3:
                sqlImportCall = "{ ? = call " + config.getDbSchema() + ".gis_import_pkg_v3$import_message_file(?::text, ?::bytea) }";
                break;
            default:
                sqlImportCall = "{ ? = call " + config.getDbSchema() + ".gis_import_pkg_v2$import(?::text, ?::bytea) }";
                break;
        }
        sqlProcessStep1Call = "{ ? = call " + config.getDbSchema() + ".gis_import_pkg_v3$import_step1(?::text) }";
        sqlProcessStep2Call = "{ ? = call " + config.getDbSchema() + ".gis_import_pkg_v3$import_step2() }";
    }

    private String saveMessage(Connection conn, String messageCode, Path file) throws SQLException, IOException {
        try (CallableStatement prepareCall = conn.prepareCall(sqlImportCall);
             FileInputStream fis = new FileInputStream(file.toFile())) {
            prepareCall.registerOutParameter(1, Types.VARCHAR);
            prepareCall.setString(2, messageCode);
            prepareCall.setBinaryStream(3, fis);
            prepareCall.execute();
            return prepareCall.getString(1);
        }
    }

    private String processMessageStep1(Connection conn, String messageCode) throws SQLException, IOException {
        try (CallableStatement prepareCall = conn.prepareCall(sqlProcessStep1Call)) {
            prepareCall.registerOutParameter(1, Types.VARCHAR);
            prepareCall.setString(2, messageCode);
            prepareCall.execute();
            return prepareCall.getString(1);
        }
    }

    private Long processMessageStep2(Connection conn) throws SQLException {
        try (CallableStatement prepareCall = conn.prepareCall(sqlProcessStep2Call)) {
            prepareCall.registerOutParameter(1, Types.BIGINT);
            prepareCall.execute();
            return prepareCall.getLong(1);
        }
    }

    private void sendErrorMessageToRMQ(String messageText) {
        int sendRetryPeriodSec = 1;

        if (rmqConfig == null) return;
        if (config.getErrorMessageEmailList().isEmpty()) {
            log.error("No config emails for error messages");
            return;
        }

        try (com.rabbitmq.client.Connection writeConnection = rmqConfig.connection.newConnection()) {
            Channel writeChannel = writeConnection.createChannel();
            Gson gson = Converters.registerAll(new GsonBuilder()).create();
            byte[] bytes = gson.toJson(new RMQMail(config.getErrorMessageEmailList(), config.getErrorMessageEmailList(), messageText)).getBytes();
            while (true) {
                try {
                    writeChannel.basicPublish(rmqConfig.destination.exchange,
                        rmqConfig.destination.routingKey,
                        new AMQP.BasicProperties.Builder().type(RMQMail.class.getSimpleName()).build(),
                        bytes);
                    break;
                } catch (Throwable ex) {
                    log.error("basicPublish error, retry", ex);
                    TimeUnit.SECONDS.sleep(sendRetryPeriodSec);
                }
            }
        } catch (TimeoutException | InterruptedException | IOException e) {
            log.error("Can't send to RMQ: {}", e.getMessage());
        }
    }

    private void processErrorMessage(Path fileName, GisImportUtil.ErrorMessageData data, String messageNum, String messageText) {
        if (errorDir == null) return;

        String preparedMessageText = messageText;
        int i = messageText.indexOf("Where:");
        if (i > 0) preparedMessageText = messageText.substring(0, i).trim();

        // Сохранть сообщение в файл
        String strFileName = "err-message.xml";
        if (fileName != null) strFileName = "err-" + fileName.toString();

        GisImportUtil.createErrorMessageFile(
            errorDir.resolve(strFileName).toString(),
            config.getSourceName(),
            data.operation,
            data.messageCode,
            data.objectType,
            data.objectCode,
            messageNum,
            preparedMessageText,
            log);

        // Отправить сообщение по email через mail-service
        sendErrorMessageToRMQ("file: " + fileName +
            ", message: " + data.messageCode +
            ", object: " + data.objectCode +
            "\n" + preparedMessageText);
    }

    private void CheckConnection() {
        int waitMillis = 10 * 1000;
        int maxWaitMillis = 60 * 60 * 1000;
        while (true) {
            try {
                datasource.getConnection().close();
                break;
            } catch (SQLException ex) {
                log.error("Can't connect to db server");
            }

            try {
                Thread.sleep(waitMillis);
                waitMillis = waitMillis * 2 >= maxWaitMillis ? maxWaitMillis : waitMillis * 2;
            } catch (InterruptedException noop) {
            }
        }
    }

    private void checkUpdates() {
        try {
            log.info("Check updates");

            try (DirectoryStream<Path> stream = Files.newDirectoryStream(errorDir, "*.xml")) {
                for (Path p : stream) {
                    log.info(String.format("Operation canceled. Found %s", p.toString()));
                    return;
                }
            }

            List<Path> files = new ArrayList<>();
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(importDir, "*.xml")) {
                stream.forEach(files::add);
            }
            if (files.size() <= 0) {
                log.info("Checked");
                return;
            }
            files.sort(Comparator.comparing(Path::getFileName));

            CheckConnection();

            Path fileName = null;
            Path temporaryDir = null;
            GisImportUtil.ErrorMessageData tempTileNameData = GisImportUtil.getErrorMessageData(null);

            try (Connection conn = datasource.getConnection()) {
                for (Path file : files) {
                    fileName = file.getFileName();
                    log.info("Import file {}", fileName);

                    temporaryDir = Files.createTempDirectory("gis");
                    GisImportUtil.transformFile(file, temporaryDir);

                    List<Path> tempFiles = new ArrayList<>();
                    try (DirectoryStream<Path> stream = Files.newDirectoryStream(temporaryDir, "*.xml")) {
                        stream.forEach(tempFiles::add);
                    }
                    tempFiles.sort(Comparator.comparing(Path::getFileName));

                    for (Path tempFile : tempFiles) {
                        tempTileNameData = GisImportUtil.getErrorMessageData(tempFile);
                        String importResult = saveMessage(conn, tempTileNameData.messageCode, tempFile);
                        conn.commit();
                        if (config.getImportVersion() > 2) {
                            importResult = processMessageStep1(conn, tempTileNameData.messageCode);
                            conn.commit();
                        }

                        if ("OK".equals(importResult)) {
                            Files.delete(tempFile);
                        } else {
                            processErrorMessage(fileName, tempTileNameData, "1", importResult);
                            return;
                        }
                    }

                    log.info("Deleting file {}", file);
                    Files.delete(file);
                    log.info("Import result {} {}", fileName, "OK");
                    GisImportUtil.deleteDir(temporaryDir);
                }

                if (files.size() > 0) {

                    if (config.getImportVersion() > 2) {
                        processMessageStep2(conn);
                        conn.commit();
                        log.info("Postimport process");
                    }

                    GisImportCheck ch = new GisImportCheck(conn, config.getDbSchema());
                    ch.check();
                }
            } catch (SQLException ex) {
                processErrorMessage(fileName, tempTileNameData,
                    String.valueOf(ex.getErrorCode() == 0 ? 1 : ex.getErrorCode()),
                    ex.getMessage());

                log.error("checkUpdates", ex);
                return;
            } finally {
                if (temporaryDir != null) GisImportUtil.deleteDir(temporaryDir);
            }

            log.info("Checked");

        } catch (Throwable ex) {
            log.error("checkUpdates", ex);
        }
    }

    @PostConstruct
    public void start() throws IOException {
        log.info("GisImport service start");
        ses.scheduleWithFixedDelay(this::checkUpdates, 0, config.getChangeCheckPeriod(), TimeUnit.SECONDS);
    }
}
