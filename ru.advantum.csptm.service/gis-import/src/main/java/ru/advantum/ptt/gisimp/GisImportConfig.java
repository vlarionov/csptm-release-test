package ru.advantum.ptt.gisimp;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Validated
@Component
@ConfigurationProperties(prefix = "gis-import")
public class GisImportConfig {

    @Valid
    @NotNull
    private String importDir;

    @Valid
    @NotNull

    private String errorDir;

    @Valid
    @NotNull
    private Long changeCheckPeriod;

    private String sqlImportCall = "{ ? = call ptt.gis_import_pkg$import(?::text, ?::bytea) }";

    private String sourceName = "KSUPT";

    private String dbSchema = "gis";

    private int importVersion = 2;

    private String errorMessageEmailList = "";

    private String errorMessageSubject = "Ошибка синхронизации данных ГИС \"Мосгортранс\" и ЕБнД КСУПТ";

    public void setImportDir(String importDir) {
        this.importDir = importDir;
    }

    public void setErrorDir(String errorDir) {
        this.errorDir = errorDir;
    }

    public void setChangeCheckPeriod(long changeCheckPeriod) {
        this.changeCheckPeriod = changeCheckPeriod;
    }

    public void setSqlImportCall(String sqlImportCall) {
        this.sqlImportCall = sqlImportCall;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public void setDbSchema(String dbSchema) {
        this.dbSchema = dbSchema;
    }

    public void setImportVersion(int importVersion) {
        this.importVersion = importVersion;
    }

    public void setErrorMessageEmailList(String errorMessageEmailList) {
        this.errorMessageEmailList = errorMessageEmailList;
    }

    public void setErrorMessageSubject(String errorMessageSubject) {
        this.errorMessageSubject = errorMessageSubject;
    }

    public String getImportDir() {
        return importDir;
    }

    public String getErrorDir() {
        return errorDir;
    }

    public Long getChangeCheckPeriod() {
        return changeCheckPeriod;
    }

    public void setChangeCheckPeriod(Long changeCheckPeriod) {
        this.changeCheckPeriod = changeCheckPeriod;
    }

    public String getSqlImportCall() {
        return sqlImportCall;
    }

    public String getSourceName() {
        return sourceName;
    }

    public String getDbSchema() {
        return dbSchema;
    }

    public int getImportVersion() {
        return importVersion;
    }

    public String getErrorMessageEmailList() {
        return errorMessageEmailList;
    }

    public String getErrorMessageSubject() {
        return errorMessageSubject;
    }
}
