DELETE FROM tt.impact_import2order_oper WHERE order_oper_id IN (
  SELECT order_oper_id FROM tt.order_oper WHERE dt_update > '2017-04-11'
)
;

DELETE FROM tt.order_oper2impact WHERE order_oper_id IN(
  SELECT order_oper_id FROM tt.order_oper WHERE dt_update > '2017-04-11'
);

DELETE FROM tt.order_list_tree WHERE order_list_id IN (
    SELECT FROM tt.order_list WHERE dt_update > '2017-04-11'
);

DELETE FROM tt.impact_import WHERE begin_date > '2017-04-11';

DELETE FROM tt.full_import WHERE import_date > '2017-04-11';


DELETE FROM tt.order_oper WHERE dt_update > '2017-04-11';

DELETE FROM tt.order_round WHERE dt_update > '2017-04-11';

DELETE FROM tt.round WHERE dt_update > '2017-04-11';

DELETE FROM tt.order_list WHERE dt_update > '2017-04-11';

DELETE FROM tt.timetable_entry WHERE dt_update > '2017-04-11';