package ru.advantum.csptm.service.ttloader.job.full.step;

import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;
import ru.advantum.csptm.service.ttloader.app.commons.ItemSource;
import ru.advantum.csptm.service.ttloader.app.commons.TimeRecordingExecutor;
import ru.advantum.csptm.service.ttloader.app.commons.WriteItemsTask;
import ru.advantum.csptm.service.ttloader.app.db.CSPTMConfig;
import ru.advantum.csptm.service.ttloader.job.full.NdIdentificatorProvider;
import ru.advantum.csptm.service.ttloader.job.full.model.csptm.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static ru.advantum.csptm.service.ttloader.app.commons.SendUpdatedOrderRoundStepConfig.UPDATED_ORDER_ROUND_IDS_KEY;
import static ru.advantum.csptm.service.ttloader.job.full.NdIdentificatorProvider.ORDER_DATE_KEY;

@Configuration
public class WriteItemsStepConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(WriteItemsStepConfig.class);


    public static final String STEP_NAME = "writeItems";

    private final StepBuilderFactory stepBuilderFactory;

    private final SqlSessionTemplate sqlSessionTemplate;

    private final TimeRecordingExecutor timeRecordingExecutor = new TimeRecordingExecutor(LOGGER);

    private final List<WriteItemsTask<?>> writeItemsTasks = new ArrayList<>();

    private List<Long> updatedOrderRoundIds;

    @Autowired
    public WriteItemsStepConfig(
        StepBuilderFactory stepBuilderFactory,
        @Qualifier(CSPTMConfig.SQL_SESSION_TEMPLATE) SqlSessionTemplate sqlSessionTemplate,
        @Qualifier(ReadParkStepConfig.WRITER_NAME) ItemSource<Park> parkItemSource,
        @Qualifier(ReadDriverStepConfig.WRITER_NAME) ItemSource<Driver> driverItemSource,
        @Qualifier(ReadTrStepConfig.WRITER_NAME) ItemSource<Tr> trItemSource,
        @Qualifier(ReadTimetableStepConfig.WRITER_NAME) ItemSource<Timetable> timetableItemSource,
        @Qualifier(ReadTimetableEntryStepConfig.WRITER_NAME) ItemSource<TimetableEntry> timetableEntryItemSource,
        @Qualifier(ReadOrderListStepConfig.WRITER_NAME) ItemSource<OrderList> orderListItemSource,
        @Qualifier(ReadRoundStepConfig.WRITER_NAME) ItemSource<Round> roundItemSource,
        @Qualifier(ReadTimetablePlanStepConfig.WRITER_NAME) ItemSource<TimetablePlan> timetablePlanItemSource,
        @Qualifier(ReadOrderRoundStepConfig.WRITER_NAME) ItemSource<OrderRound> orderRoundItemSource,
        @Qualifier(ReadOrderOperStepConfig.WRITER_NAME) ItemSource<OrderOper> orderOperItemSource
    ) {
        this.stepBuilderFactory = stepBuilderFactory;
        this.sqlSessionTemplate = sqlSessionTemplate;

        addTask("insertPark", parkItemSource);
        addTask(driver -> driver.getSignDelete() != null && driver.getSignDelete() == 0 ? "insertDriver" : "deleteDriver", driverItemSource);
        addTask(tr -> tr.getSignDelete() != null && tr.getSignDelete() == 0 ? "insertTr" : "deleteTr", trItemSource);
        addTask("insertTimetable", timetableItemSource);
        addTask("insertTimetableEntry", timetableEntryItemSource);
        addTask("insertOrderList", orderListItemSource);
        addTask("insertRound", roundItemSource);
        addTask("insertTimetablePlan", timetablePlanItemSource);
        addTask("insertOrderRound", orderRoundItemSource);
        addTask("insertOrderOper", orderOperItemSource);
    }

    private void addTask(WriteItemsTask<?> task) {
        writeItemsTasks.add(task);
    }

    private void addTask(String statementId, ItemSource<?> itemSource) {
        addTask(new WriteItemsTask<>(sqlSessionTemplate, statementId, itemSource));
    }

    private <T> void addTask(Function<T, String> statementClassifier, ItemSource<T> itemSource) {
        addTask(new WriteItemsTask<>(sqlSessionTemplate, statementClassifier, itemSource));
    }

    @Bean(name = STEP_NAME)
    public Step step(PlatformTransactionManager transactionManager) {
        return stepBuilderFactory
            .get(STEP_NAME)
            .allowStartIfComplete(true)
            .tasklet(writeItemsTasklet())
            .listener(putUpdatedOrderRoundIdsListener())
            .transactionManager(transactionManager)
            .build();
    }

    @Bean
    public Tasklet writeItemsTasklet() {
        return (contribution, chunkContext) -> {
            timeRecordingExecutor.execute("saveImportRecord", () -> saveImportRecord(chunkContext.getStepContext().getJobExecutionContext()));
            timeRecordingExecutor.execute("saveReadItems", this::saveReadItems);
            timeRecordingExecutor.execute("selectUpdatedOrderRoundIds", () -> selectUpdatedOrderRoundIds(chunkContext.getStepContext().getJobExecutionContext()));
            timeRecordingExecutor.execute("insertGISOrderOper", this::insertGISOrderOper);
            timeRecordingExecutor.execute("updateOperRoundVisit", this::updateOperRoundVisit);
            timeRecordingExecutor.execute("updateOrderOperStopOrderNum", this::updateOrderOperStopOrderNum);
            timeRecordingExecutor.execute("updateOrderOperTimePlan", this::updateOrderOperTimePlan);

//            if (true) {
//                throw new RuntimeException();
//            }
            return RepeatStatus.FINISHED;
        };
    }

    @Bean
    public StepExecutionListener putUpdatedOrderRoundIdsListener() {
        return new StepExecutionListenerSupport() {
            @Override
            public ExitStatus afterStep(StepExecution stepExecution) {
                stepExecution.getJobExecution().getExecutionContext().put(
                    UPDATED_ORDER_ROUND_IDS_KEY,
                    updatedOrderRoundIds
                );
                return ExitStatus.COMPLETED;
            }
        };
    }

    private void saveReadItems() throws Exception {
        for (WriteItemsTask<?> task : writeItemsTasks) {
            task.execute();
        }
    }

    private void selectUpdatedOrderRoundIds(Map<String, Object> jobExecutionContext) {
        updatedOrderRoundIds = sqlSessionTemplate.selectList(
            "selectOrderRoundIdsByOrderDate",
            Collections.singletonMap(
                "orderDate",
                jobExecutionContext.get(NdIdentificatorProvider.ORDER_DATE_KEY)
            )
        );
    }

    private void insertGISOrderOper() {
        sqlSessionTemplate.update(
            "insertGISOrderOper",
            Collections.singletonMap("orderRoundIds", updatedOrderRoundIds)
        );
        sqlSessionTemplate.flushStatements();
    }

    private void updateOperRoundVisit() {
        sqlSessionTemplate.update(
            "updateOperRoundVisit",
            Collections.singletonMap("orderRoundIds", updatedOrderRoundIds)
        );
        sqlSessionTemplate.flushStatements();

    }

    private void updateOrderOperStopOrderNum() {
        sqlSessionTemplate.update(
            "updateOrderOperStopOrderNum",
            Collections.singletonMap("orderRoundIds", updatedOrderRoundIds)
        );
        sqlSessionTemplate.flushStatements();
    }

    private void saveImportRecord(Map<String, Object> jobExecutionContext) {
        LocalDate orderDate = (LocalDate) jobExecutionContext.get(ORDER_DATE_KEY);
        sqlSessionTemplate.update(
            "insertFullImport",
            Collections.singletonMap(ORDER_DATE_KEY, orderDate)
        );
        sqlSessionTemplate.flushStatements();

    }

    private void updateOrderOperTimePlan() {
        sqlSessionTemplate.update(
            "updateOrderOperTimePlan",
            Collections.singletonMap("orderRoundIds", updatedOrderRoundIds)
        );
        sqlSessionTemplate.flushStatements();
    }


}
