package ru.advantum.csptm.service.ttloader.job.full.model.asdu;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class OrderListDto {
    private final Long nsidUniqueid;

    private final String garageNum;

    private final String tnInstance;

    private final Long nrdIdentificator;

    private final LocalDateTime orderDate;

    private final String nrdSmena;

    private final Integer nNum;

    private final Long nrdGrafic;

    public OrderListDto(Long nsidUniqueid, String garageNum, String tnInstance, Long nrdIdentificator, LocalDateTime orderDate, String nrdSmena, Integer nNum, Long nrdGrafic) {
        this.nsidUniqueid = nsidUniqueid;
        this.garageNum = garageNum;
        this.tnInstance = tnInstance;
        this.nrdIdentificator = nrdIdentificator;
        this.orderDate = orderDate;
        this.nrdSmena = nrdSmena;
        this.nNum = nNum;
        this.nrdGrafic = nrdGrafic;
    }

    public OrderListDto(Long nsidUniqueid, String garageNum, String tnInstance, Long nrdIdentificator, Timestamp orderDate, String nrdSmena, Integer nNum, Long nrdGrafic) {
        this(
                nsidUniqueid,
                garageNum,
                tnInstance,
                nrdIdentificator,
                orderDate != null ? orderDate.toLocalDateTime() : null,
                nrdSmena,
                nNum,
                nrdGrafic
        );
    }

    public Long getNsidUniqueid() {
        return nsidUniqueid;
    }

    public String getGarageNum() {
        return garageNum;
    }

    public String getTnInstance() {
        return tnInstance;
    }

    public Long getNrdIdentificator() {
        return nrdIdentificator;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public String getNrdSmena() {
        return nrdSmena;
    }

    public Integer getnNum() {
        return nNum;
    }

    public Long getNrdGrafic() {
        return nrdGrafic;
    }
}
