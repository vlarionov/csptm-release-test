package ru.advantum.csptm.service.ttloader.job.impact.model.asdu;

public class ImpactedRoundDto {
    private final String dstnType;

    private final Long dstnIdentificator;

    private final String tnInstance;

    private final Integer nNum;

    private final Long nrdGrafic;

    public ImpactedRoundDto(String dstnType, Long dstnIdentificator, String tnInstance, Integer nNum, Long nrdGrafic) {
        this.dstnType = dstnType;
        this.dstnIdentificator = dstnIdentificator;
        this.tnInstance = tnInstance;
        this.nNum = nNum;
        this.nrdGrafic = nrdGrafic;
    }

    public String getDstnType() {
        return dstnType;
    }

    public Long getDstnIdentificator() {
        return dstnIdentificator;
    }

    public String getTnInstance() {
        return tnInstance;
    }

    public Integer getNNum() {
        return nNum;
    }

    public Long getNrdGrafic() {
        return nrdGrafic;
    }
}
