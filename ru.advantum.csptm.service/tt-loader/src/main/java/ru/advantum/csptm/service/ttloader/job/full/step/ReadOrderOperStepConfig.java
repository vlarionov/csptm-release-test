package ru.advantum.csptm.service.ttloader.job.full.step;

import org.springframework.batch.core.Step;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.service.ttloader.app.commons.ListItemContainer;
import ru.advantum.csptm.service.ttloader.app.commons.ReadItemsStepConfigurerSupport;
import ru.advantum.csptm.service.ttloader.job.full.NdIdentificatorProvider;
import ru.advantum.csptm.service.ttloader.job.full.model.asdu.OrderOperDto;
import ru.advantum.csptm.service.ttloader.job.full.model.csptm.OrderOper;

import java.time.LocalDateTime;

@Configuration
public class ReadOrderOperStepConfig {
    public static final String STEP_NAME = "readOrderOper";
    public static final String WRITER_NAME = "orderOperWriter";


    private final ReadItemsStepConfigurerSupport configurerSupport;

    public ReadOrderOperStepConfig(ReadItemsStepConfigurerSupport configurerSupport) {
        this.configurerSupport = configurerSupport;
    }


    @Bean(name = STEP_NAME)
    public Step step() {
        return configurerSupport.buildStep(
                STEP_NAME,
                orderOperDtoItemReader(),
                orderOperDtoOrderOperItemProcessor(),
                orderOperItemWriter()
        );
    }


    @Bean
    public ItemReader<OrderOperDto> orderOperDtoItemReader() {
        return configurerSupport.buildReader("selectOrderOper", NdIdentificatorProvider.ND_IDENTIFICATOR_KEY);
    }

    @Bean
    public ItemProcessor<OrderOperDto, OrderOper> orderOperDtoOrderOperItemProcessor() {
        return orderOperDto -> new OrderOper(
                null,
                orderOperDto.getTnInstance(),
                orderOperDto.getCnrdRoutenumber(),
                orderOperDto.getDstnIdentificator(),
                orderOperDto.getDstnType(),
                orderOperDto.getNrdIdentificator(),
                getTimePlan(orderOperDto.getOrderDate(), orderOperDto.getCnrdTimeplan()),
                getTimeOper(orderOperDto.getOrderDate(), orderOperDto.getCnrdTimeplan(), orderOperDto.getCnrdDiflextion()),
                0L,
                LocalDateTime.now(),
                orderOperDto.getCnrdSysmark() == 8L ? 0L : 1L,
                orderOperDto.getErmaId(),
                (short) 1,
                orderOperDto.getCnrdOrderby()
        );
    }

    @Bean(name = WRITER_NAME)
    public ListItemContainer<OrderOper> orderOperItemWriter() {
        return new ListItemContainer<>();
    }

    private LocalDateTime getTimePlan(LocalDateTime orderDate, Long cnrdTimeplan) {
        if (orderDate == null || cnrdTimeplan == null) {
            return null;
        }
        return orderDate.plusMinutes(cnrdTimeplan);
    }

    private LocalDateTime getTimeOper(LocalDateTime orderDate, Long cnrdTimeplan, Long cnrdDiflextion) {
        LocalDateTime timePlan = getTimePlan(orderDate, cnrdTimeplan);
        if (timePlan == null || cnrdDiflextion == null) {
            return null;
        }
        return timePlan.plusMinutes(cnrdDiflextion);
    }

}
