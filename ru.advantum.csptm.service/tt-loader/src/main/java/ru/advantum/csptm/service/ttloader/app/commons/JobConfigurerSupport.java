package ru.advantum.csptm.service.ttloader.app.commons;

import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.JobParametersIncrementer;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.builder.FlowJobBuilder;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class JobConfigurerSupport {
    private final JobBuilderFactory jobBuilderFactory;
    private final JobParametersIncrementer jobParametersIncrementer;

    @Autowired
    public JobConfigurerSupport(
            JobBuilderFactory jobBuilderFactory,
            JobParametersIncrementer jobParametersIncrementer
    ) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.jobParametersIncrementer = jobParametersIncrementer;
    }


    public JobBuilder getJobBuilder(String jobName, List<JobExecutionListener> jobExecutionListeners) {
        JobBuilder jobBuilder = jobBuilderFactory.get(jobName);

        jobBuilder = jobBuilder.incrementer(jobParametersIncrementer);

        for (JobExecutionListener jobExecutionListener : jobExecutionListeners) {
            jobBuilder = jobBuilder.listener(jobExecutionListener);
        }

        return jobBuilder;
    }

    public FlowBuilder<FlowJobBuilder> addAsyncStep(FlowBuilder<FlowJobBuilder> flowBuilder, Step step) {
        return flowBuilder.split(buildTaskExecutor()).add(buildFlow(step));
    }

    public Flow buildFlow(Step step) {
        return new FlowBuilder<Flow>(step.getName() + "Flow").from(step).build();
    }

    public TaskExecutor buildTaskExecutor() {
        return new SimpleAsyncTaskExecutor("flowTaskExecutor");
    }


}
