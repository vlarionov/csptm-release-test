package ru.advantum.csptm.service.ttloader.job.impact.step;

import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;
import ru.advantum.csptm.service.ttloader.app.commons.ItemSource;
import ru.advantum.csptm.service.ttloader.app.commons.TimeRecordingExecutor;
import ru.advantum.csptm.service.ttloader.app.commons.WriteItemsTask;
import ru.advantum.csptm.service.ttloader.app.db.CSPTMConfig;
import ru.advantum.csptm.service.ttloader.job.impact.model.csptm.*;

import java.util.*;

import static ru.advantum.csptm.service.ttloader.app.commons.SendUpdatedOrderRoundStepConfig.UPDATED_ORDER_ROUND_IDS_KEY;
import static ru.advantum.csptm.service.ttloader.job.impact.ImpactPeriodProvider.BEGIN_DATE_KEY;
import static ru.advantum.csptm.service.ttloader.job.impact.ImpactPeriodProvider.END_DATE_KEY;

@Configuration
public class WriteImpactedItemsStepConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(WriteImpactedItemsStepConfig.class);

    public static final String STEP_NAME = "writeItems";

    private final StepBuilderFactory stepBuilderFactory;

    private final SqlSessionTemplate sqlSessionTemplate;

    private final ItemSource<Impact> impactItemSource;
    private final ItemSource<ImpactedOrderOper> orderOperItemSource;

    private final TimeRecordingExecutor timeRecordingExecutor = new TimeRecordingExecutor(LOGGER);

    private final List<WriteItemsTask<?>> writeItemsTasks = new ArrayList<>();


    private List<Long> updatedOrderRoundIds;

    @Autowired
    public WriteImpactedItemsStepConfig(
            StepBuilderFactory stepBuilderFactory,
            @Qualifier(CSPTMConfig.SQL_SESSION_TEMPLATE) SqlSessionTemplate sqlSessionTemplate,
            @Qualifier(ReadImpactStepConfig.WRITER_NAME) ItemSource<Impact> impactItemSource,
            @Qualifier(ReadImpactedTimetableEntryStepConfig.WRITER_NAME) ItemSource<ImpactedTimetableEntry> timetableEntryItemSource,
            @Qualifier(ReadImpactedOrderListStepConfig.WRITER_NAME) ItemSource<ImpactedOrderList> orderListItemSource,
            @Qualifier(ReadImpactedRoundStepConfig.WRITER_NAME) ItemSource<ImpactedRound> roundItemSource,
            @Qualifier(ReadImpactedOrderRoundStepConfig.WRITER_NAME) ItemSource<ImpactedOrderRound> orderRoundItemSource,
            @Qualifier(ReadImpactOrderOperStepConfig.WRITER_NAME) ItemSource<ImpactedOrderOper> orderOperItemSource
    ) {
        this.stepBuilderFactory = stepBuilderFactory;
        this.sqlSessionTemplate = sqlSessionTemplate;
        this.impactItemSource = impactItemSource;
        this.orderOperItemSource = orderOperItemSource;

        addTask("insertImpact", impactItemSource);
        addTask("insertImpactedTimetableEntry", timetableEntryItemSource);
        addTask("insertImpactedOrderList", orderListItemSource);
        addTask("insertImpactedRound", roundItemSource);
        addTask("insertImpactedOrderRound", orderRoundItemSource);
        addTask("insertImpactedOrderOper", orderOperItemSource);
    }

    private void addTask(String statementId, ItemSource<?> itemSource) {
        WriteItemsTask<?> task = new WriteItemsTask<>(sqlSessionTemplate, statementId, itemSource);
        writeItemsTasks.add(task);
    }

    @Bean(name = STEP_NAME)
    public Step step(PlatformTransactionManager transactionManager) {
        return stepBuilderFactory
                .get(STEP_NAME)
                .allowStartIfComplete(true)
                .tasklet(writeItemsTasklet())
                .listener(putUpdatedOrderRoundIdsListener())
                .transactionManager(transactionManager)
                .build();
    }

    @Bean
    public Tasklet writeItemsTasklet() {
        return (contribution, chunkContext) -> {
            Integer impactImportId = saveImportRecord(chunkContext.getStepContext().getJobExecutionContext());
            timeRecordingExecutor.execute("setImpactImportIdToItems", () -> setImpactImportIdToItems(impactImportId));
            timeRecordingExecutor.execute("saveReadItems", this::saveReadItems);
            timeRecordingExecutor.execute("selectUpdatedOrderRoundIds", () -> selectUpdatedOrderRoundIds(impactImportId));
            timeRecordingExecutor.execute("insertGISOrderOper", this::insertGISOrderOper);
            timeRecordingExecutor.execute("updateInactiveOrderRounds", this::updateInactiveOrderRounds);
            timeRecordingExecutor.execute("updateInactiveOrderLists", this::updateInactiveOrderLists);
            timeRecordingExecutor.execute("updateOrderOperStopOrderNum", this::updateOrderOperStopOrderNum);
            timeRecordingExecutor.execute("deleteDuplicateGISOrderOper", this::deleteDuplicateGISOrderOper);
            timeRecordingExecutor.execute("updateOperRoundVisit", this::updateOperRoundVisit);
            timeRecordingExecutor.execute("updateOrderOperTimePlan", this::updateOrderOperTimePlan);

//            if (true) {
//                throw new RuntimeException();
//            }

            return RepeatStatus.FINISHED;
        };
    }

    private void setImpactImportIdToItems(Integer impactImportId) {
        impactItemSource.getItems().forEach(impact -> impact.setImpactImportId(impactImportId));
        orderOperItemSource.getItems().forEach(impactedOrderOper -> impactedOrderOper.setImpactImportId(impactImportId));
    }

    @Bean
    public StepExecutionListener putUpdatedOrderRoundIdsListener() {
        return new StepExecutionListenerSupport() {
            @Override
            public ExitStatus afterStep(StepExecution stepExecution) {
                stepExecution.getJobExecution().getExecutionContext().put(
                        UPDATED_ORDER_ROUND_IDS_KEY,
                        updatedOrderRoundIds
                );
                return ExitStatus.COMPLETED;
            }
        };
    }

    private void saveReadItems() throws Exception {
        for (WriteItemsTask<?> task : writeItemsTasks) {
            task.execute();
        }
    }

    private void selectUpdatedOrderRoundIds(Integer impactImportId) {
        updatedOrderRoundIds = sqlSessionTemplate.selectList(
                "selectOrderRoundIdsByImpactImportId",
                Collections.singletonMap("impactImportId", impactImportId)
        );
    }

    private void insertGISOrderOper() {
        sqlSessionTemplate.update("insertGISOrderOper", createOrderRoundIdsParams());
        sqlSessionTemplate.flushStatements();
    }

    private void updateInactiveOrderRounds() {
        sqlSessionTemplate.update("updateInactiveOrderRounds", createOrderRoundIdsParams());
        sqlSessionTemplate.flushStatements();
    }

    private void updateInactiveOrderLists() {
        sqlSessionTemplate.update("updateInactiveOrderLists", createOrderRoundIdsParams());
        sqlSessionTemplate.flushStatements();
    }

    private void updateOperRoundVisit() {
        sqlSessionTemplate.update("updateOperRoundVisit", createOrderRoundIdsParams());
        sqlSessionTemplate.flushStatements();
    }

    private void updateOrderOperStopOrderNum() {
        sqlSessionTemplate.update("updateOrderOperStopOrderNum", createOrderRoundIdsParams());
        sqlSessionTemplate.flushStatements();
    }

    private void deleteDuplicateGISOrderOper() {
        sqlSessionTemplate.update("deleteDuplicateGISOrderOper", createOrderRoundIdsParams());
        sqlSessionTemplate.flushStatements();
    }

    private Map<String, Object> createOrderRoundIdsParams() {
        return Collections.singletonMap("orderRoundIds", updatedOrderRoundIds);
    }

    private Integer saveImportRecord(Map<String, Object> jobExecutionContext) {
        Map<String, Object> params = new HashMap<>();
        params.put(BEGIN_DATE_KEY, jobExecutionContext.get(BEGIN_DATE_KEY));
        params.put(END_DATE_KEY, jobExecutionContext.get(END_DATE_KEY));

        return sqlSessionTemplate.selectOne("insertImpactImport", params);
    }

    private void updateOrderOperTimePlan() {
        sqlSessionTemplate.update(
                "updateOrderOperTimePlan",
                Collections.singletonMap("orderRoundIds", updatedOrderRoundIds)
        );
        sqlSessionTemplate.flushStatements();
    }
}
