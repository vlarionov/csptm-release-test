package ru.advantum.csptm.service.ttloader.job.impact.model.csptm;

import java.time.LocalDateTime;

public class ImpactedOrderList {
    private final String tnInstance;

    private final Long driverTnId;

    private final String garageNum;

    private final Long orderNum;

    private final LocalDateTime orderDate;

    private final Long shift;

    private final Long signDeleted;

    private final LocalDateTime dtUpdate;

    private final Long isActive;

    private final Long rmtId;

    private final Short timetableEntryNum;

    public ImpactedOrderList(String tnInstance, Long driverTnId, String garageNum, Long orderNum, LocalDateTime orderDate, Long shift, Long signDeleted, LocalDateTime dtUpdate, Long isActive, Long rmtId, Short timetableEntryNum) {
        this.tnInstance = tnInstance;
        this.driverTnId = driverTnId;
        this.garageNum = garageNum;
        this.orderNum = orderNum;
        this.orderDate = orderDate;
        this.shift = shift;
        this.signDeleted = signDeleted;
        this.dtUpdate = dtUpdate;
        this.isActive = isActive;
        this.rmtId = rmtId;
        this.timetableEntryNum = timetableEntryNum;
    }

    public String getTnInstance() {
        return tnInstance;
    }

    public Long getDriverTnId() {
        return driverTnId;
    }

    public String getGarageNum() {
        return garageNum;
    }

    public Long getOrderNum() {
        return orderNum;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public Long getShift() {
        return shift;
    }

    public Long getSignDeleted() {
        return signDeleted;
    }

    public LocalDateTime getDtUpdate() {
        return dtUpdate;
    }

    public Long getIsActive() {
        return isActive;
    }

    public Long getRmtId() {
        return rmtId;
    }

    public Short getTimetableEntryNum() {
        return timetableEntryNum;
    }
}
