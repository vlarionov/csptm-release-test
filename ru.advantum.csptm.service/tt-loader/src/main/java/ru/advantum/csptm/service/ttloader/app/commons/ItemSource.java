package ru.advantum.csptm.service.ttloader.app.commons;

import java.util.List;

public interface ItemSource<T> {
    List<T> getItems();
}

