package ru.advantum.csptm.service.ttloader.job.impact;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.builder.FlowJobBuilder;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import ru.advantum.csptm.service.ttloader.app.commons.JobConfigurerSupport;
import ru.advantum.csptm.service.ttloader.app.commons.SendUpdatedOrderRoundStepConfig;
import ru.advantum.csptm.service.ttloader.job.impact.step.*;

import java.util.List;

@SpringBootApplication
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
public class ImpactImportJobConfig {
    public static final String JOB_NAME = "impactImport";

    private final JobConfigurerSupport jobConfigurerSupport;

    public ImpactImportJobConfig(JobConfigurerSupport jobConfigurerSupport) {
        this.jobConfigurerSupport = jobConfigurerSupport;
    }

    @Bean
    public Job importJob(
            List<JobExecutionListener> jobExecutionListeners,
            @Qualifier(ReadImpactStepConfig.STEP_NAME) Step readImpactStep,
            @Qualifier(ReadImpactedTimetableEntryStepConfig.STEP_NAME) Step readTimetableEntry,
            @Qualifier(ReadImpactedOrderListStepConfig.STEP_NAME) Step readOrderList,
            @Qualifier(ReadImpactedRoundStepConfig.STEP_NAME) Step readRoundStep,
            @Qualifier(ReadImpactedOrderRoundStepConfig.STEP_NAME) Step readOrderRoundStep,
            @Qualifier(ReadImpactOrderOperStepConfig.STEP_NAME) Step readOrderOperStep,
            @Qualifier(WriteImpactedItemsStepConfig.STEP_NAME) Step writeItemsStep,
            @Qualifier(SendUpdatedOrderRoundStepConfig.STEP_NAME) Step sendUpdatedOrderRoundStep
    ) {
        JobBuilder jobBuilder = jobConfigurerSupport.getJobBuilder(JOB_NAME, jobExecutionListeners);

        FlowBuilder<FlowJobBuilder> flowBuilder = jobBuilder.start(jobConfigurerSupport.buildFlow(readImpactStep));
        flowBuilder = jobConfigurerSupport.addAsyncStep(flowBuilder, readTimetableEntry);
        flowBuilder = jobConfigurerSupport.addAsyncStep(flowBuilder, readOrderList);
        flowBuilder = jobConfigurerSupport.addAsyncStep(flowBuilder, readRoundStep);
        flowBuilder = jobConfigurerSupport.addAsyncStep(flowBuilder, readOrderRoundStep);
        flowBuilder = jobConfigurerSupport.addAsyncStep(flowBuilder, readOrderOperStep);

        flowBuilder = flowBuilder.next(writeItemsStep);
        flowBuilder = flowBuilder.next(sendUpdatedOrderRoundStep);

        return flowBuilder.end().build();
    }

}
