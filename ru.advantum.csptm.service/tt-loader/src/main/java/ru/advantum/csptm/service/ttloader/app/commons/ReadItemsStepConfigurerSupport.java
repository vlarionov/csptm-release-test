package ru.advantum.csptm.service.ttloader.app.commons;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.batch.MyBatisPagingItemReader;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;
import ru.advantum.csptm.service.ttloader.app.db.ASDUConfig;

import java.util.HashMap;
import java.util.Map;

@Component
public class ReadItemsStepConfigurerSupport {
    private final StepBuilderFactory stepBuilderFactory;

    private final SqlSessionFactory asduSqlSessionFactory;

    @Autowired
    public ReadItemsStepConfigurerSupport(
            StepBuilderFactory stepBuilderFactory,
            @Qualifier(ASDUConfig.SQL_SESSION_FACTORY) SqlSessionFactory asduSqlSessionFactory
    ) {
        this.stepBuilderFactory = stepBuilderFactory;
        this.asduSqlSessionFactory = asduSqlSessionFactory;
    }

    public <I, O> Step buildStep(
            String stepName,
            ItemReader<I> itemReader,
            ItemProcessor<I, O> itemProcessor,
            ItemWriter<O> itemWriter
    ) {
        return stepBuilderFactory
                .get(stepName)
                .allowStartIfComplete(true)
                .<I, O>chunk(Integer.MAX_VALUE)
                .reader(itemReader)
                .processor(itemProcessor)
                .writer(itemWriter)
                .taskExecutor(buildTaskExecutor())
                .build();
    }

    public <I> ItemReader<I> buildReader(String queryId, String... requiredParameters) {
        final Map<String, Object> parameters = new HashMap<>();
        MyBatisPagingItemReader<I> reader = new MyBatisPagingItemReader<I>() {
            @BeforeStep
            public void retrieveSharedData(StepExecution stepExecution) {
                for (String requiredParameter : requiredParameters) {
                    parameters.put(
                            requiredParameter,
                            stepExecution
                                    .getJobExecution()
                                    .getExecutionContext()
                                    .get(requiredParameter)
                    );
                }
            }
        };
        reader.setParameterValues(parameters);
        reader.setPageSize(Integer.MAX_VALUE);
        reader.setSqlSessionFactory(asduSqlSessionFactory);
        reader.setQueryId(queryId);

        return reader;
    }

    private TaskExecutor buildTaskExecutor() {
        return new SimpleAsyncTaskExecutor("stepTaskExecutor");
    }

}
