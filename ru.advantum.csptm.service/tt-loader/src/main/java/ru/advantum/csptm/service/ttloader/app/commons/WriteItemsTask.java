package ru.advantum.csptm.service.ttloader.app.commons;

import com.google.common.collect.Lists;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.function.Function;

public class WriteItemsTask<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(WriteItemsTask.class);

    private static final int BATCH_SIZE = 100_000;

    private final TimeRecordingExecutor timeRecordingExecutor = new TimeRecordingExecutor(LOGGER);

    private final SqlSessionTemplate sqlSessionTemplate;

    private final Function<T, String> statementClassifier;

    private final ItemSource<T> source;


    public WriteItemsTask(SqlSessionTemplate sqlSessionTemplate, Function<T, String> statementClassifier, ItemSource<T> source) {
        this.sqlSessionTemplate = sqlSessionTemplate;
        this.statementClassifier = statementClassifier;
        this.source = source;
    }

    public WriteItemsTask(SqlSessionTemplate sqlSessionTemplate, String statementId, ItemSource<T> source) {
        this(sqlSessionTemplate, item -> statementId, source);
    }

    public void execute() throws Exception {
        List<T> sourceItems = source.getItems();

        if (!sourceItems.isEmpty()) {
            String itemClassName = sourceItems.get(0).getClass().getSimpleName();

            timeRecordingExecutor.execute(
                    itemClassName + " inserts",
                    () -> {
                        for (List<T> batch : Lists.partition(sourceItems, BATCH_SIZE)) {
                            for (T item : batch) {
                                String statementId = statementClassifier.apply(item);
                                sqlSessionTemplate.update(statementId, item);
                            }
                            sqlSessionTemplate.flushStatements();
                        }
                        LOGGER.info("{} {} inserts called", sourceItems.size(), itemClassName);
                    }
            );


        }
    }
}
