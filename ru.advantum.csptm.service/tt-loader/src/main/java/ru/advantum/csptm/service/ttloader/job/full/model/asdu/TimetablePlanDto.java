package ru.advantum.csptm.service.ttloader.job.full.model.asdu;

public class TimetablePlanDto {
    private final Long stopId;

    private final Double timeBEpoch;

    private final Double timeEEpoch;

    private final Long sm;

    private final Long mvIdentificator;

    private final String tirp;

    private final Long vector;

    private final Long nvx;


    public TimetablePlanDto(Long stopId, Double timeBEpoch, Double timeEEpoch, Long sm, Long mvIdentificator, String tirp, Long vector, Long nvx) {
        this.stopId = stopId;
        this.timeBEpoch = timeBEpoch;
        this.timeEEpoch = timeEEpoch;
        this.sm = sm;
        this.mvIdentificator = mvIdentificator;
        this.tirp = tirp;
        this.vector = vector;
        this.nvx = nvx;
    }

    public Long getStopId() {
        return stopId;
    }

    public Double getTimeBEpoch() {
        return timeBEpoch;
    }

    public Double getTimeEEpoch() {
        return timeEEpoch;
    }

    public Long getSm() {
        return sm;
    }

    public Long getMvIdentificator() {
        return mvIdentificator;
    }

    public String getTirp() {
        return tirp;
    }

    public Long getVector() {
        return vector;
    }

    public Long getNvx() {
        return nvx;
    }
}
