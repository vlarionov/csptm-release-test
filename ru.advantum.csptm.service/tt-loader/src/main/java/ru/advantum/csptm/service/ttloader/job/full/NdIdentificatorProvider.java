package ru.advantum.csptm.service.ttloader.job.full;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import ru.advantum.csptm.service.ttloader.app.commons.IllegalImportStateException;
import ru.advantum.csptm.service.ttloader.app.db.ASDUConfig;
import ru.advantum.csptm.service.ttloader.app.db.CSPTMConfig;

import java.time.LocalDate;
import java.util.List;

@Component
public class NdIdentificatorProvider extends JobExecutionListenerSupport {
    public static final String ORDER_DATE_KEY = "orderDate";
    public static final String ND_IDENTIFICATOR_KEY = "ndIdentificator";

    private final SqlSessionTemplate asduSqlSessionTemplate;
    private final SqlSessionTemplate csptmSessionTemplate;


    public NdIdentificatorProvider(
            @Qualifier(ASDUConfig.SQL_SESSION_TEMPLATE) SqlSessionTemplate asduSqlSessionTemplate,
            @Qualifier(CSPTMConfig.SQL_SESSION_TEMPLATE) SqlSessionTemplate csptmSessionTemplate
    ) {
        this.asduSqlSessionTemplate = asduSqlSessionTemplate;
        this.csptmSessionTemplate = csptmSessionTemplate;
    }

    @Override
    public void beforeJob(JobExecution jobExecution) {
        String orderDateStr = jobExecution.getJobParameters().getString(ORDER_DATE_KEY);
        Assert.notNull(orderDateStr);
        LocalDate orderDate = LocalDate.parse(orderDateStr);

        List<LocalDate> importedOrderDates = csptmSessionTemplate.selectList("selectFullImportDates");
        if (importedOrderDates.contains(orderDate)) {
            throw new IllegalImportStateException(
                    String.format("Full import for order date %s already completed", orderDate)
            );
        }

        Long ndIdentificator = asduSqlSessionTemplate.selectOne("selectNdIdentificator", orderDate);

        jobExecution.getExecutionContext().put(ORDER_DATE_KEY, orderDate);
        jobExecution.getExecutionContext().put(ND_IDENTIFICATOR_KEY, ndIdentificator);
    }
}
