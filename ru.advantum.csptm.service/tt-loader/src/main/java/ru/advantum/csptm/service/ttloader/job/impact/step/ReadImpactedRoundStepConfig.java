package ru.advantum.csptm.service.ttloader.job.impact.step;

import org.springframework.batch.core.Step;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.service.ttloader.app.commons.ListItemContainer;
import ru.advantum.csptm.service.ttloader.app.commons.ReadItemsStepConfigurerSupport;
import ru.advantum.csptm.service.ttloader.job.impact.ImpactPeriodProvider;
import ru.advantum.csptm.service.ttloader.job.impact.model.asdu.ImpactedRoundDto;
import ru.advantum.csptm.service.ttloader.job.impact.model.csptm.ImpactedRound;

import java.time.LocalDateTime;

@Configuration
public class ReadImpactedRoundStepConfig {
    public static final String STEP_NAME = "readRound";
    public static final String WRITER_NAME = "roundWriter";

    private final ReadItemsStepConfigurerSupport configurerSupport;

    public ReadImpactedRoundStepConfig(ReadItemsStepConfigurerSupport configurerSupport) {
        this.configurerSupport = configurerSupport;
    }


    @Bean(name = STEP_NAME)
    public Step step() {
        return configurerSupport.buildStep(
                STEP_NAME,
                routeDtoItemReader(),
                routeDtoRoundItemProcessor(),
                roundItemWriter()
        );
    }

    @Bean
    public ItemReader<ImpactedRoundDto> routeDtoItemReader() {
        return configurerSupport.buildReader(
                "selectImpactedRound",
                ImpactPeriodProvider.BEGIN_DATE_KEY,
                ImpactPeriodProvider.END_DATE_KEY,
                ImpactPeriodProvider.ND_IDENTIFICATORS_KEY
        );
    }

    @Bean
    public ItemProcessor<ImpactedRoundDto, ImpactedRound> routeDtoRoundItemProcessor() {
        return routeDto -> new ImpactedRound(
                getDirectionId(routeDto.getDstnType()),
                LocalDateTime.now(),
                routeDto.getDstnIdentificator(),
                routeDto.getDstnType(),
                "XX",//todo
                routeDto.getTnInstance(),
                routeDto.getNNum().longValue(),
                routeDto.getNrdGrafic().shortValue()
        );
    }

    @Bean(name = WRITER_NAME)
    public ListItemContainer<ImpactedRound> roundItemWriter() {
        return new ListItemContainer<>();
    }

    private Long getDirectionId(String dstnType) {
        Integer ascii = ascii(dstnType);
        if (ascii == null) {
            return null;
        }
        return (ascii.longValue() + 1) % 2 + 1;
    }

    private Integer ascii(String arg) {
        if (arg == null || arg.isEmpty()) {
            return null;
        }
        return (int) arg.charAt(0);
    }

}
