package ru.advantum.csptm.service.ttloader.job.impact.model.csptm;

import java.time.LocalDateTime;

public class ImpactedTimetableEntry {
    private final Long rmtId;

    private final Short timetableEntryNum;

    private final LocalDateTime dtUpdate;

    public ImpactedTimetableEntry(Long rmtId, Short timetableEntryNum, LocalDateTime dtUpdate) {
        this.rmtId = rmtId;
        this.timetableEntryNum = timetableEntryNum;
        this.dtUpdate = dtUpdate;
    }

    public Long getRmtId() {
        return rmtId;
    }

    public Short getTimetableEntryNum() {
        return timetableEntryNum;
    }

    public LocalDateTime getDtUpdate() {
        return dtUpdate;
    }

}
