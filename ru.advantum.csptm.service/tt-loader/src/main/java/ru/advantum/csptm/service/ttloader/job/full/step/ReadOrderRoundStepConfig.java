package ru.advantum.csptm.service.ttloader.job.full.step;

import org.springframework.batch.core.Step;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.service.ttloader.app.commons.ListItemContainer;
import ru.advantum.csptm.service.ttloader.app.commons.ReadItemsStepConfigurerSupport;
import ru.advantum.csptm.service.ttloader.job.full.NdIdentificatorProvider;
import ru.advantum.csptm.service.ttloader.job.full.model.asdu.OrderRoundDto;
import ru.advantum.csptm.service.ttloader.job.full.model.csptm.OrderRound;

@Configuration
public class ReadOrderRoundStepConfig {
    public static final String STEP_NAME = "readOrderRound";
    public static final String WRITER_NAME = "orderRoundWriter";

    private final ReadItemsStepConfigurerSupport configurerSupport;

    public ReadOrderRoundStepConfig(ReadItemsStepConfigurerSupport configurerSupport) {
        this.configurerSupport = configurerSupport;
    }


    @Bean(name = STEP_NAME)
    public Step step() {
        return configurerSupport.buildStep(
                STEP_NAME,
                orderRoundDtoItemReader(),
                orderRoundDtoOrderRoundItemProcessor(),
                orderRoundItemWriter()
        );
    }

    @Bean
    public ItemReader<OrderRoundDto> orderRoundDtoItemReader() {
        return configurerSupport.buildReader("selectOrderRound", NdIdentificatorProvider.ND_IDENTIFICATOR_KEY);
    }

    @Bean
    public ItemProcessor<OrderRoundDto, OrderRound> orderRoundDtoOrderRoundItemProcessor() {
        return orderRoundDto -> new OrderRound(
                orderRoundDto.getTnInstance(),
                orderRoundDto.getNrdIdentificator(),
                orderRoundDto.getDstnIdentificator(),
                orderRoundDto.getCnrdRoutenumber(),
                orderRoundDto.getAvgCnrdSysmark() != null ? orderRoundDto.getAvgCnrdSysmark().longValue() == 8L ? 0L : 1L : null,
                orderRoundDto.getCnrdGraph().shortValue()
        );
    }

    @Bean(name = WRITER_NAME)
    public ListItemContainer<OrderRound> orderRoundItemWriter() {
        return new ListItemContainer<>();
    }
}
