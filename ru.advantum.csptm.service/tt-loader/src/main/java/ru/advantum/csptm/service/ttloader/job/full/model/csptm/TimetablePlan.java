package ru.advantum.csptm.service.ttloader.job.full.model.csptm;

import java.time.LocalDateTime;

public class TimetablePlan {
    private final Long stopErmId;

    private final Double timeBeginEpoch;

    private final Double timeEndEpoch;

    private final Long shift;

    private final LocalDateTime dtUpdate;

    private final Long rmtId;

    private final String roundCode;

    private final Long directionId;

    private final Short timetableEntryNum;

    public TimetablePlan(Long stopErmId, Double timeBeginEpoch, Double timeEndEpoch, Long shift, LocalDateTime dtUpdate, Long rmtId, String roundCode, Long directionId, Short timetableEntryNum) {
        this.stopErmId = stopErmId;
        this.timeBeginEpoch = timeBeginEpoch;
        this.timeEndEpoch = timeEndEpoch;
        this.shift = shift;
        this.dtUpdate = dtUpdate;
        this.rmtId = rmtId;
        this.roundCode = roundCode;
        this.directionId = directionId;
        this.timetableEntryNum = timetableEntryNum;
    }

    public Long getStopErmId() {
        return stopErmId;
    }

    public Double getTimeBeginEpoch() {
        return timeBeginEpoch;
    }

    public Double getTimeEndEpoch() {
        return timeEndEpoch;
    }

    public Long getShift() {
        return shift;
    }

    public LocalDateTime getDtUpdate() {
        return dtUpdate;
    }

    public Long getRmtId() {
        return rmtId;
    }

    public String getRoundCode() {
        return roundCode;
    }

    public Long getDirectionId() {
        return directionId;
    }

    public Short getTimetableEntryNum() {
        return timetableEntryNum;
    }
}
