package ru.advantum.csptm.service.ttloader.app;

import org.springframework.batch.core.JobParametersIncrementer;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

@SpringBootApplication
@EnableScheduling
@EnableBatchProcessing
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
public class Application {
    public static final String CONFIG_FILE_NAME = "tt-loader.xml";

    public static void main(String[] args) {
        new SpringApplicationBuilder(Application.class).build().run(args);
    }

    @Bean
    public static TTLoaderConfig applicationConfig() throws Exception {
        return ru.advantum.config.Configuration.unpackConfig(TTLoaderConfig.class, CONFIG_FILE_NAME);
    }

    @Bean
    public static JepRmqServiceConfig jepRmqServiceConfig() throws Exception {
        return applicationConfig().rmqServiceConfig;
    }

    @Bean
    public JobParametersIncrementer jobParametersIncrementer() {
        return new RunIdIncrementer();
    }

}