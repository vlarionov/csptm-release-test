package ru.advantum.csptm.service.ttloader.job.full.model.csptm;

import java.time.LocalDateTime;

public class TimetableEntry {
    private final Long rmtId;

    private final Long drShiftId;

    private final Short timetableEntryNum;

    private final Short signDeleted;

    private final LocalDateTime dtUpdate;

    public TimetableEntry(Long rmtId, Long drShiftId, Short timetableEntryNum, Short signDeleted, LocalDateTime dtUpdate) {
        this.rmtId = rmtId;
        this.drShiftId = drShiftId;
        this.timetableEntryNum = timetableEntryNum;
        this.signDeleted = signDeleted;
        this.dtUpdate = dtUpdate;
    }

    public Long getRmtId() {
        return rmtId;
    }

    public Long getDrShiftId() {
        return drShiftId;
    }

    public Short getTimetableEntryNum() {
        return timetableEntryNum;
    }

    public Short getSignDeleted() {
        return signDeleted;
    }

    public LocalDateTime getDtUpdate() {
        return dtUpdate;
    }

}
