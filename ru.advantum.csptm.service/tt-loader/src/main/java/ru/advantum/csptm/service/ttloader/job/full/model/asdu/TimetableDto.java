package ru.advantum.csptm.service.ttloader.job.full.model.asdu;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class TimetableDto {
    private final Long mvIdentificator;

    private final Long ttIdentificator;

    private final String mvMarsh;

    private final String varr;

    private final LocalDateTime mvIncludedate;

    private final LocalDateTime mvExcludedate;

    private final Long mvDnd;

    public TimetableDto(Long mvIdentificator, Long ttIdentificator, String mvMarsh, String varr, LocalDateTime mvIncludedate, LocalDateTime mvExcludedate, Long mvDnd) {
        this.mvIdentificator = mvIdentificator;
        this.ttIdentificator = ttIdentificator;
        this.mvMarsh = mvMarsh;
        this.varr = varr;
        this.mvIncludedate = mvIncludedate;
        this.mvExcludedate = mvExcludedate;
        this.mvDnd = mvDnd;
    }

    public TimetableDto(Long mvIdentificator, Long ttIdentificator, String mvMarsh, String varr, Timestamp mvIncludedate, Timestamp mvExcludedate, Long mvDnd) {
        this(
                mvIdentificator,
                ttIdentificator,
                mvMarsh,
                varr,
                mvIncludedate != null ? mvIncludedate.toLocalDateTime() : null,
                mvExcludedate != null ? mvExcludedate.toLocalDateTime() : null,
                mvDnd
        );
    }

    public Long getMvIdentificator() {
        return mvIdentificator;
    }

    public Long getTtIdentificator() {
        return ttIdentificator;
    }

    public String getMvMarsh() {
        return mvMarsh;
    }

    public String getVarr() {
        return varr;
    }

    public LocalDateTime getMvIncludedate() {
        return mvIncludedate;
    }

    public LocalDateTime getMvExcludedate() {
        return mvExcludedate;
    }

    public Long getMvDnd() {
        return mvDnd;
    }
}
