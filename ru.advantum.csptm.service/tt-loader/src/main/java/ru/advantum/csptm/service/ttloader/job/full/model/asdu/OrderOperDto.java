package ru.advantum.csptm.service.ttloader.job.full.model.asdu;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class OrderOperDto {
    private final LocalDateTime orderDate;

    private final Long cnrdTimeplan;

    private final Long cnrdDiflextion;

    private final Long cnrdSysmark;

    private final Long ermaId;

    private final String tnInstance;

    private final Long cnrdRoutenumber;

    private final Long dstnIdentificator;

    private final String dstnType;

    private final Long nrdIdentificator;

    private final Long ndIdentificator;

    private final Long cnrdOrderby;

    public OrderOperDto(LocalDateTime orderDate, Long cnrdTimeplan, Long cnrdDiflextion, Long cnrdSysmark, Long ermaId, String tnInstance, Long cnrdRoutenumber, Long dstnIdentificator, String dstnType, Long nrdIdentificator, Long ndIdentificator, Long cnrdOrderby) {
        this.orderDate = orderDate;
        this.cnrdTimeplan = cnrdTimeplan;
        this.cnrdDiflextion = cnrdDiflextion;
        this.cnrdSysmark = cnrdSysmark;
        this.ermaId = ermaId;
        this.tnInstance = tnInstance;
        this.cnrdRoutenumber = cnrdRoutenumber;
        this.dstnIdentificator = dstnIdentificator;
        this.dstnType = dstnType;
        this.nrdIdentificator = nrdIdentificator;
        this.ndIdentificator = ndIdentificator;
        this.cnrdOrderby = cnrdOrderby;
    }

    public OrderOperDto(Timestamp orderDate, Long cnrdTimeplan, Long cnrdDiflextion, Long cnrdSysmark, Long ermaId, String tnInstance, Long cnrdRoutenumber, Long dstnIdentificator, String dstnType, Long nrdIdentificator, Long ndIdentificator, Long cnrdOrderby) {
        this(
                orderDate != null ? orderDate.toLocalDateTime() : null,
                cnrdTimeplan,
                cnrdDiflextion,
                cnrdSysmark,
                ermaId,
                tnInstance,
                cnrdRoutenumber,
                dstnIdentificator,
                dstnType,
                nrdIdentificator,
                ndIdentificator,
                cnrdOrderby);
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public Long getCnrdTimeplan() {
        return cnrdTimeplan;
    }

    public Long getCnrdDiflextion() {
        return cnrdDiflextion;
    }

    public Long getCnrdSysmark() {
        return cnrdSysmark;
    }

    public Long getErmaId() {
        return ermaId;
    }

    public String getTnInstance() {
        return tnInstance;
    }

    public Long getCnrdRoutenumber() {
        return cnrdRoutenumber;
    }

    public Long getDstnIdentificator() {
        return dstnIdentificator;
    }

    public String getDstnType() {
        return dstnType;
    }

    public Long getNrdIdentificator() {
        return nrdIdentificator;
    }

    public Long getNdIdentificator() {
        return ndIdentificator;
    }

    public Long getCnrdOrderby() {
        return cnrdOrderby;
    }
}
