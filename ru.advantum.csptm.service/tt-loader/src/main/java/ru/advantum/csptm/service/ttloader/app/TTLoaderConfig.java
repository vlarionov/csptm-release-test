package ru.advantum.csptm.service.ttloader.app;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

@Root(name = "tt-loader")
public class TTLoaderConfig {
    @Attribute(name = "fullImportCron")
    public String fullImportCron;

    @Attribute(name = "impactImportRate")
    public int impactImportRate;

    @Attribute(name = "maxImpactImportPeriod")
    public long maxImpactImportPeriod;

    @Element(name = "csptm-db")
    public DatabaseConnection csptmDBConfig;

    @Element(name = "asdu-db")
    public DatabaseConnection asduDBConfig;

    @Element(name = "rmq-service")
    public JepRmqServiceConfig rmqServiceConfig;

}
