package ru.advantum.csptm.service.ttloader.app.commons;

import com.google.common.collect.Lists;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.List;

@Configuration
public class SendUpdatedOrderRoundStepConfig {
    public static final String STEP_NAME = "sendUpdatedOrderRound";

    public static final String UPDATED_ORDER_ROUND_IDS_KEY = "updatedOrderRoundIds";

    private static final String MESSAGE_TYPE = "ORDER_ROUND_UPDATE";

    private static final int MAX_BATCH_SIZE = 100_000;

    private final StepBuilderFactory stepBuilderFactory;

    private final RabbitTemplate rabbitTemplate;

    public SendUpdatedOrderRoundStepConfig(StepBuilderFactory stepBuilderFactory, RabbitTemplate rabbitTemplate) {
        this.stepBuilderFactory = stepBuilderFactory;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Bean(name = STEP_NAME)
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public Step step() {
        return stepBuilderFactory
                .get(STEP_NAME)
                .allowStartIfComplete(true)
                .tasklet(sendUpdatedOrderRoundTasklet())
                .build();
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public Tasklet sendUpdatedOrderRoundTasklet() {
        return (contribution, chunkContext) -> {
            List<Long> orderRoundIds = (List<Long>) chunkContext.getStepContext().getJobExecutionContext().get(UPDATED_ORDER_ROUND_IDS_KEY);
            for (List<Long> batch : Lists.partition(orderRoundIds, MAX_BATCH_SIZE)) {
                rabbitTemplate.convertAndSend(batch, this::setMessageType);
            }
            return RepeatStatus.FINISHED;
        };
    }

    private Message setMessageType(Message message) {
        message.getMessageProperties().setType(MESSAGE_TYPE);
        return message;
    }
}
