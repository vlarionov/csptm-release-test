package ru.advantum.csptm.service.ttloader.job.impact.step;

import org.springframework.batch.core.Step;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.service.ttloader.app.commons.ListItemContainer;
import ru.advantum.csptm.service.ttloader.app.commons.ReadItemsStepConfigurerSupport;
import ru.advantum.csptm.service.ttloader.job.impact.ImpactPeriodProvider;
import ru.advantum.csptm.service.ttloader.job.impact.model.asdu.ImpactedOrderRoundDto;
import ru.advantum.csptm.service.ttloader.job.impact.model.csptm.ImpactedOrderRound;

@Configuration
public class ReadImpactedOrderRoundStepConfig {
    public static final String STEP_NAME = "readOrderRound";
    public static final String WRITER_NAME = "orderRoundWriter";

    private final ReadItemsStepConfigurerSupport configurerSupport;

    public ReadImpactedOrderRoundStepConfig(ReadItemsStepConfigurerSupport configurerSupport) {
        this.configurerSupport = configurerSupport;
    }

    @Bean(name = STEP_NAME)
    public Step step() {
        return configurerSupport.buildStep(
                STEP_NAME,
                orderRoundDtoItemReader(),
                orderRoundDtoOrderRoundItemProcessor(),
                orderRoundItemWriter()
        );
    }

    @Bean
    public ItemReader<ImpactedOrderRoundDto> orderRoundDtoItemReader() {
        return configurerSupport.buildReader("selectImpactedOrderRound", ImpactPeriodProvider.BEGIN_DATE_KEY, ImpactPeriodProvider.END_DATE_KEY, ImpactPeriodProvider.ND_IDENTIFICATORS_KEY);
    }

    @Bean
    public ItemProcessor<ImpactedOrderRoundDto, ImpactedOrderRound> orderRoundDtoOrderRoundItemProcessor() {
        return orderRoundDto -> new ImpactedOrderRound(
                orderRoundDto.getCnrdRoutenumber(),
                orderRoundDto.getTnInstance(),
                orderRoundDto.getAvgCnrdSysmark() != null ? orderRoundDto.getAvgCnrdSysmark().longValue() == 8L ? 0L : 1L : null,
                orderRoundDto.getNrdIdentificator(),
                orderRoundDto.getCnrdGraph().shortValue(),
                orderRoundDto.getDstnIdentificator()
        );
    }

    @Bean(name = WRITER_NAME)
    public ListItemContainer<ImpactedOrderRound> orderRoundItemWriter() {
        return new ListItemContainer<>();
    }
}
