package ru.advantum.csptm.service.ttloader.app.commons;

public class IllegalImportStateException extends RuntimeException {
    public IllegalImportStateException(String message) {
        super(message);
    }
}
