package ru.advantum.csptm.service.ttloader.app.commons;

import org.springframework.batch.item.*;

import java.util.ArrayList;
import java.util.List;

public class ListItemContainer<T> implements ItemWriter<T>, ItemReader<T>, ItemSource<T> {
    private final List<T> buffer = new ArrayList<>();

    private int readIndex = 0;

    @Override
    public synchronized void write(List<? extends T> items) {
        buffer.addAll(items);
    }

    @Override
    public synchronized List<T> getItems() {
        return buffer;
    }

    @Override
    public synchronized T read() throws Exception {
        if (readIndex >= buffer.size()) {
            return null;
        }
        return buffer.get(readIndex++);
    }
}
