package ru.advantum.csptm.service.ttloader.job.full.model.csptm;

import java.time.LocalDateTime;


public class Driver {
    private final String driverName;

    private final String driverLastName;

    private final String driverMiddleName;

    private final String tabNum;

    private final Long tnId;

    private final String tnInstance;

    private final LocalDateTime dtBegin;

    private final LocalDateTime dtEnd;

    private final Short signDelete;

    private final LocalDateTime updateDate;

    public Driver(String driverName, String driverLastName, String driverMiddleName, String tabNum, Long tnId, String tnInstance, LocalDateTime dtBegin, LocalDateTime dtEnd, Short signDelete, LocalDateTime updateDate) {
        this.driverName = driverName;
        this.driverLastName = driverLastName;
        this.driverMiddleName = driverMiddleName;
        this.tabNum = tabNum;
        this.tnId = tnId;
        this.tnInstance = tnInstance;
        this.dtBegin = dtBegin;
        this.dtEnd = dtEnd;
        this.signDelete = signDelete;
        this.updateDate = updateDate;
    }

    public String getDriverName() {
        return driverName;
    }

    public String getDriverLastName() {
        return driverLastName;
    }

    public String getDriverMiddleName() {
        return driverMiddleName;
    }

    public String getTabNum() {
        return tabNum;
    }

    public Long getTnId() {
        return tnId;
    }

    public String getTnInstance() {
        return tnInstance;
    }

    public LocalDateTime getDtBegin() {
        return dtBegin;
    }

    public LocalDateTime getDtEnd() {
        return dtEnd;
    }

    public Short getSignDelete() {
        return signDelete;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }
}
