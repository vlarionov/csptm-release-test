package ru.advantum.csptm.service.ttloader.app.db;

import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.csptm.service.ttloader.app.TTLoaderConfig;

import javax.sql.DataSource;

@Configuration
public class ASDUConfig {
    public static final String SQL_SESSION_FACTORY = "ASDUSqlSessionFactory";
    public static final String SQL_SESSION_TEMPLATE = "ASDUSqlSessionTemplate";

    private final DataSource dataSource;
    private final Resource mapper;

    public ASDUConfig(
            TTLoaderConfig config,
            @Value("classpath:ASDUMapper.xml") Resource mapper
    ) {
        DatabaseConnection databaseConnection = config.asduDBConfig;
        this.dataSource = new DriverManagerDataSource(
                databaseConnection.url,
                databaseConnection.username,
                databaseConnection.password
        );
        this.mapper = mapper;
    }

    @Bean(name = SQL_SESSION_FACTORY)
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource);
        sqlSessionFactoryBean.setMapperLocations(new Resource[]{mapper});
        return sqlSessionFactoryBean.getObject();
    }

    @Bean(name = SQL_SESSION_TEMPLATE)
    public SqlSessionTemplate sqlSessionTemplate() throws Exception {
        return new SqlSessionTemplate(
                sqlSessionFactory(),
                ExecutorType.BATCH
        );
    }


}