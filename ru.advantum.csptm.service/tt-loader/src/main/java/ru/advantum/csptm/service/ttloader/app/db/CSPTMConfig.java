package ru.advantum.csptm.service.ttloader.app.db;

import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.PlatformTransactionManager;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.csptm.service.ttloader.app.TTLoaderConfig;

import javax.sql.DataSource;

@Configuration
public class CSPTMConfig {
    public static final String SQL_SESSION_FACTORY = "CSPTMSqlSessionFactory";
    public static final String SQL_SESSION_TEMPLATE = "CSPTMSqlSessionTemplate";

    private final DataSource dataSource;
    private final Resource mapper;

    public CSPTMConfig(
            TTLoaderConfig config,
            @Value("classpath:CSPTMMapper.xml") Resource csptmMapper
    ) throws Exception {
        DatabaseConnection databaseConnection = config.csptmDBConfig;
        this.dataSource = new DriverManagerDataSource(
                databaseConnection.url,
                databaseConnection.username,
                databaseConnection.password
        );
        this.mapper = csptmMapper;
    }


    @Bean(name = SQL_SESSION_FACTORY)
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource);
        sqlSessionFactoryBean.setMapperLocations(new Resource[]{mapper});
        return sqlSessionFactoryBean.getObject();
    }

    @Bean(name = SQL_SESSION_TEMPLATE)
    public SqlSessionTemplate sqlSessionTemplate() throws Exception {
        return new SqlSessionTemplate(
                sqlSessionFactory(),
                ExecutorType.BATCH
        );
    }

    @Bean
    @Primary
    public PlatformTransactionManager csptmTransactionManager() throws Exception {
        return new DataSourceTransactionManager(dataSource);
    }






}
