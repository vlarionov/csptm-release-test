package ru.advantum.csptm.service.ttloader.job.full.step;

import org.springframework.batch.core.Step;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.service.ttloader.app.commons.ListItemContainer;
import ru.advantum.csptm.service.ttloader.app.commons.ReadItemsStepConfigurerSupport;
import ru.advantum.csptm.service.ttloader.job.full.model.asdu.ParkDto;
import ru.advantum.csptm.service.ttloader.job.full.model.csptm.Park;

@Configuration
public class ReadParkStepConfig {
    public static final String STEP_NAME = "readPark";
    public static final String WRITER_NAME = "parkWriter";

    private final ReadItemsStepConfigurerSupport configurerSupport;

    @Autowired
    public ReadParkStepConfig(ReadItemsStepConfigurerSupport configurerSupport) {
        this.configurerSupport = configurerSupport;
    }


    @Bean(name = STEP_NAME)
    public Step step() {
        return configurerSupport.buildStep(
                STEP_NAME,
                parkDtoItemReader(),
                depoDtoParkItemProcessor(),
                parkItemWriter()
        );
    }

    @Bean
    public ItemReader<ParkDto> parkDtoItemReader() {
        return configurerSupport.buildReader("selectPark");
    }

    @Bean
    public ItemProcessor<ParkDto, Park> depoDtoParkItemProcessor() {
        return parkDto -> new Park(
                parkDto.getTnId(),
                parkDto.getTnInstance(),
                parkDto.getsName(),
                (short) 0,
                parkDto.getPkRegnum(),
                parkDto.getDtBegin(),
                parkDto.getDtEnd()
        );
    }

    @Bean(name = WRITER_NAME)
    public ListItemContainer<Park> parkItemWriter() {
        return new ListItemContainer<>();
    }


}