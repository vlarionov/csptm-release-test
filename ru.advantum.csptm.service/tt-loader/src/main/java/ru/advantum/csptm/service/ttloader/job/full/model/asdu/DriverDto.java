package ru.advantum.csptm.service.ttloader.job.full.model.asdu;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class DriverDto {
    private final String firstName;

    private final String surname;

    private final String secondName;

    private final String tabNum;

    private final Long tnId;

    private final String tnInstance;

    private final LocalDateTime dtBegin;

    private final LocalDateTime dtEnd;

    private final Integer isDeleted;

    public DriverDto(String firstName, String surname, String secondName, String tabNum, Long tnId, String tnInstance, LocalDateTime dtBegin, LocalDateTime dtEnd, Integer isDeleted) {
        this.firstName = firstName;
        this.surname = surname;
        this.secondName = secondName;
        this.tabNum = tabNum;
        this.tnId = tnId;
        this.tnInstance = tnInstance;
        this.dtBegin = dtBegin;
        this.dtEnd = dtEnd;
        this.isDeleted = isDeleted;
    }

    public DriverDto(String firstName, String surname, String secondName, String tabNum, Long tnId, String tnInstance, Timestamp dtBegin, Timestamp dtEnd, Integer isDeleted) {
        this(
                firstName,
                surname,
                secondName,
                tabNum,
                tnId,
                tnInstance,
                dtBegin != null ? dtBegin.toLocalDateTime() : null,
                dtEnd != null ? dtEnd.toLocalDateTime() : null,
                isDeleted
        );
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getTabNum() {
        return tabNum;
    }

    public Long getTnId() {
        return tnId;
    }

    public String getTnInstance() {
        return tnInstance;
    }

    public LocalDateTime getDtBegin() {
        return dtBegin;
    }

    public LocalDateTime getDtEnd() {
        return dtEnd;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }
}
