package ru.advantum.csptm.service.ttloader.job.full;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.builder.FlowJobBuilder;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import ru.advantum.csptm.service.ttloader.app.commons.JobConfigurerSupport;
import ru.advantum.csptm.service.ttloader.app.commons.SendUpdatedOrderRoundStepConfig;
import ru.advantum.csptm.service.ttloader.job.full.step.*;

import java.util.List;

@SpringBootApplication
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
public class FullImportJobConfig {
    public static final String JOB_NAME = "fullImport";

    private final JobConfigurerSupport configurerSupport;

    public FullImportJobConfig(JobConfigurerSupport configurerSupport) {
        this.configurerSupport = configurerSupport;
    }

    @Bean
    public Job importJob(
            List<JobExecutionListener> jobExecutionListeners,
            @Qualifier(ReadParkStepConfig.STEP_NAME) Step readParkStep,
            @Qualifier(ReadDriverStepConfig.STEP_NAME) Step readDriverStep,
            @Qualifier(ReadTrStepConfig.STEP_NAME) Step readTrStep,
            @Qualifier(ReadTimetableStepConfig.STEP_NAME) Step readTimetableStep,
            @Qualifier(ReadTimetableEntryStepConfig.STEP_NAME) Step readTimetableEntryStep,
            @Qualifier(ReadOrderListStepConfig.STEP_NAME) Step readOrderListStep,
            @Qualifier(ReadRoundStepConfig.STEP_NAME) Step readRoundStep,
            @Qualifier(ReadTimetablePlanStepConfig.STEP_NAME) Step readTimetablePlanStep,
            @Qualifier(ReadOrderRoundStepConfig.STEP_NAME) Step readOrderRoundStep,
            @Qualifier(ReadOrderOperStepConfig.STEP_NAME) Step readASDUOrderOperStep,
            @Qualifier(WriteItemsStepConfig.STEP_NAME) Step writeItemsStep,
            @Qualifier(SendUpdatedOrderRoundStepConfig.STEP_NAME) Step sendUpdatedOrderRoundStep
    ) {
        JobBuilder jobBuilder = configurerSupport.getJobBuilder(JOB_NAME, jobExecutionListeners);

        FlowBuilder<FlowJobBuilder> flowBuilder = jobBuilder.start(configurerSupport.buildFlow(readParkStep));
        flowBuilder = configurerSupport.addAsyncStep(flowBuilder, readDriverStep);
        flowBuilder = configurerSupport.addAsyncStep(flowBuilder, readTrStep);
        flowBuilder = configurerSupport.addAsyncStep(flowBuilder, readTimetableStep);
        flowBuilder = configurerSupport.addAsyncStep(flowBuilder, readTimetableEntryStep);
        flowBuilder = configurerSupport.addAsyncStep(flowBuilder, readOrderListStep);
        flowBuilder = configurerSupport.addAsyncStep(flowBuilder, readRoundStep);
        //flowBuilder = configurerSupport.addAsyncStep(flowBuilder, readTimetablePlanStep);
        flowBuilder = configurerSupport.addAsyncStep(flowBuilder, readOrderRoundStep);
        flowBuilder = configurerSupport.addAsyncStep(flowBuilder, readASDUOrderOperStep);

        return flowBuilder
                .next(writeItemsStep)
                .next(sendUpdatedOrderRoundStep)
                .end().build();
    }
}