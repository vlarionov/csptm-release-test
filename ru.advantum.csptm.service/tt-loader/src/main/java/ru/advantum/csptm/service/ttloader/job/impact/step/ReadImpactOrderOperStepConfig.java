package ru.advantum.csptm.service.ttloader.job.impact.step;

import org.springframework.batch.core.Step;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.service.ttloader.app.commons.ListItemContainer;
import ru.advantum.csptm.service.ttloader.app.commons.ReadItemsStepConfigurerSupport;
import ru.advantum.csptm.service.ttloader.job.impact.ImpactPeriodProvider;
import ru.advantum.csptm.service.ttloader.job.impact.model.asdu.ImpactedOrderOperDto;
import ru.advantum.csptm.service.ttloader.job.impact.model.csptm.ImpactedOrderOper;

import java.time.LocalDateTime;

@Configuration
public class ReadImpactOrderOperStepConfig {
    public static final String STEP_NAME = "readOrderOper";
    public static final String WRITER_NAME = "orderOperWriter";


    private final ReadItemsStepConfigurerSupport configurerSupport;

    public ReadImpactOrderOperStepConfig(ReadItemsStepConfigurerSupport configurerSupport) {
        this.configurerSupport = configurerSupport;
    }


    @Bean(name = STEP_NAME)
    public Step step() {
        return configurerSupport.buildStep(
                STEP_NAME,
                orderOperDtoItemReader(),
                orderOperDtoOrderOperItemProcessor(),
                orderOperItemWriter()
        );
    }


    @Bean
    public ItemReader<ImpactedOrderOperDto> orderOperDtoItemReader() {
        return configurerSupport.buildReader("selectImpactedOrderOper", ImpactPeriodProvider.BEGIN_DATE_KEY, ImpactPeriodProvider.END_DATE_KEY, ImpactPeriodProvider.ND_IDENTIFICATORS_KEY);
    }

    @Bean
    public ItemProcessor<ImpactedOrderOperDto, ImpactedOrderOper> orderOperDtoOrderOperItemProcessor() {
        return orderOperDto -> new ImpactedOrderOper(
                getTimePlan(orderOperDto.getOrderDate(), orderOperDto.getCnrdTimeplan()),
                getTimeOper(orderOperDto.getOrderDate(), orderOperDto.getCnrdTimeplan(), orderOperDto.getCnrdDiflextion()),
                LocalDateTime.now(),
                orderOperDto.getCnrdSysmark() == 8L ? 0L : 1L,
                orderOperDto.getErmaId(),
                orderOperDto.getCnrdOrderby(),
                orderOperDto.getTnInstance(),
                orderOperDto.getNrdIdentificator(),
                orderOperDto.getCnrdRoutenumber(),
                orderOperDto.getDstnIdentificator(),
                orderOperDto.getDstnType()
        );
    }

    @Bean(name = WRITER_NAME)
    public ListItemContainer<ImpactedOrderOper> orderOperItemWriter() {
        return new ListItemContainer<>();
    }

    private LocalDateTime getTimePlan(LocalDateTime orderDate, Long cnrdTimeplan) {
        if (orderDate == null || cnrdTimeplan == null) {
            return null;
        }
        return orderDate.plusMinutes(cnrdTimeplan);
    }

    private LocalDateTime getTimeOper(LocalDateTime orderDate, Long cnrdTimeplan, Long cnrdDiflextion) {
        LocalDateTime timePlan = getTimePlan(orderDate, cnrdTimeplan);
        if (timePlan == null || cnrdDiflextion == null) {
            return null;
        }
        return timePlan.plusMinutes(cnrdDiflextion);
    }

}
