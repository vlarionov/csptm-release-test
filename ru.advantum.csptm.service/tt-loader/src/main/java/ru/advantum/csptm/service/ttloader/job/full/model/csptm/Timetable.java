package ru.advantum.csptm.service.ttloader.job.full.model.csptm;

import java.time.LocalDateTime;

public class Timetable {
    private final Long rmtId;

    private final Long rmtTtId;

    private final String timetableNum;

    private final String timetableVarr;

    private final LocalDateTime beginDate;

    private final LocalDateTime endDate;

    private final Short signDeleted;

    private final LocalDateTime dtUpdate;

    private final Long timetableSettingId;

    public Timetable(Long rmtId, Long rmtTtId, String timetableNum, String timetableVarr, LocalDateTime beginDate, LocalDateTime endDate, Short signDeleted, LocalDateTime dtUpdate, Long timetableSettingId) {
        this.rmtId = rmtId;
        this.rmtTtId = rmtTtId;
        this.timetableNum = timetableNum;
        this.timetableVarr = timetableVarr;
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.signDeleted = signDeleted;
        this.dtUpdate = dtUpdate;
        this.timetableSettingId = timetableSettingId;
    }

    public Long getRmtId() {
        return rmtId;
    }

    public Long getRmtTtId() {
        return rmtTtId;
    }

    public String getTimetableNum() {
        return timetableNum;
    }

    public String getTimetableVarr() {
        return timetableVarr;
    }

    public LocalDateTime getBeginDate() {
        return beginDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public Short getSignDeleted() {
        return signDeleted;
    }

    public LocalDateTime getDtUpdate() {
        return dtUpdate;
    }

    public Long getTimetableSettingId() {
        return timetableSettingId;
    }
}
