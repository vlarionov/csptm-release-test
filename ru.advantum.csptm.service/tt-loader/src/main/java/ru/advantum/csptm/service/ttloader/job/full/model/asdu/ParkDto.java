package ru.advantum.csptm.service.ttloader.job.full.model.asdu;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class ParkDto {
    private final Long tnId;

    private final String tnInstance;

    private final String sName;

    private final Long pkRegnum;

    private final LocalDateTime dtBegin;

    private final LocalDateTime dtEnd;

    public ParkDto(Long tnId, String tnInstance, String sName, Long pkRegnum, LocalDateTime dtBegin, LocalDateTime dtEnd) {
        this.tnId = tnId;
        this.tnInstance = tnInstance;
        this.sName = sName;
        this.pkRegnum = pkRegnum;
        this.dtBegin = dtBegin;
        this.dtEnd = dtEnd;
    }

    public ParkDto(Long tnId, String tnInstance, String sName, Long pkRegnum, Timestamp dtBegin, Timestamp dtEnd) {
        this(
                tnId,
                tnInstance,
                sName,
                pkRegnum,
                dtBegin != null ? dtBegin.toLocalDateTime() : null,
                dtEnd != null ? dtEnd.toLocalDateTime() : null
        );
    }

    public Long getTnId() {
        return tnId;
    }

    public String getTnInstance() {
        return tnInstance;
    }

    public String getsName() {
        return sName;
    }

    public Long getPkRegnum() {
        return pkRegnum;
    }

    public LocalDateTime getDtBegin() {
        return dtBegin;
    }

    public LocalDateTime getDtEnd() {
        return dtEnd;
    }
}
