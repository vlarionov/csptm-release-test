package ru.advantum.csptm.service.ttloader.job.impact.step;

import org.springframework.batch.core.Step;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.service.ttloader.app.commons.ListItemContainer;
import ru.advantum.csptm.service.ttloader.app.commons.ReadItemsStepConfigurerSupport;
import ru.advantum.csptm.service.ttloader.job.impact.ImpactPeriodProvider;
import ru.advantum.csptm.service.ttloader.job.impact.model.asdu.ImpactedTimetableEntryDto;
import ru.advantum.csptm.service.ttloader.job.impact.model.csptm.ImpactedTimetableEntry;

import java.time.LocalDateTime;

@Configuration
public class ReadImpactedTimetableEntryStepConfig {
    public static final String STEP_NAME = "readTimetableEntry";
    public static final String WRITER_NAME = "timetableEntryWriter";

    private final ReadItemsStepConfigurerSupport configurerSupport;

    public ReadImpactedTimetableEntryStepConfig(ReadItemsStepConfigurerSupport configurerSupport) {
        this.configurerSupport = configurerSupport;
    }

    @Bean(name = STEP_NAME)
    public Step step() {
        return configurerSupport.buildStep(
                STEP_NAME,
                timetableEntryDtoItemReader(),
                timetableEntryDtoTimetableEntryItemProcessor(),
                timetableEntryItemWriter()
        );
    }

    @Bean
    public ItemReader<ImpactedTimetableEntryDto> timetableEntryDtoItemReader() {
        return configurerSupport.buildReader(
                "selectImpactedTimetableEntry",
                ImpactPeriodProvider.BEGIN_DATE_KEY,
                ImpactPeriodProvider.END_DATE_KEY,
                ImpactPeriodProvider.ND_IDENTIFICATORS_KEY
        );
    }

    @Bean
    public ItemProcessor<ImpactedTimetableEntryDto, ImpactedTimetableEntry> timetableEntryDtoTimetableEntryItemProcessor() {
        return timetableEntryDto -> new ImpactedTimetableEntry(
                timetableEntryDto.getNNum().longValue(),
                timetableEntryDto.getNrdGrafic().shortValue(),
                LocalDateTime.now()
        );
    }


    @Bean(name = WRITER_NAME)
    public ListItemContainer<ImpactedTimetableEntry> timetableEntryItemWriter() {
        return new ListItemContainer<>();
    }
}
