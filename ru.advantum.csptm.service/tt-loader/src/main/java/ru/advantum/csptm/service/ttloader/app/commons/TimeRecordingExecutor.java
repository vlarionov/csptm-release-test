package ru.advantum.csptm.service.ttloader.app.commons;

import org.slf4j.Logger;
import org.springframework.util.StopWatch;

public class TimeRecordingExecutor {
    private final StopWatch stopWatch = new StopWatch();

    private final Logger logger;

    public TimeRecordingExecutor(Logger logger) {
        this.logger = logger;
    }

    public void execute(String actionName, Action action) throws Exception {
        stopWatch.start();
        action.execute();
        stopWatch.stop();
        logger.info("{} executed within {} seconds", actionName, (double) stopWatch.getLastTaskTimeMillis() / 1000);
    }


    public interface Action {
        void execute() throws Exception;
    }
}
