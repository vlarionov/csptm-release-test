package ru.advantum.csptm.service.ttloader.job.full.step;

import org.springframework.batch.core.Step;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.service.ttloader.app.commons.ListItemContainer;
import ru.advantum.csptm.service.ttloader.app.commons.ReadItemsStepConfigurerSupport;
import ru.advantum.csptm.service.ttloader.job.full.model.asdu.TrDto;
import ru.advantum.csptm.service.ttloader.job.full.model.csptm.Tr;

import java.time.LocalDateTime;

@Configuration
public class ReadTrStepConfig {
    public static final String STEP_NAME = "readTr";
    public static final String WRITER_NAME = "trWriter";

    private final ReadItemsStepConfigurerSupport configurerSupport;

    public ReadTrStepConfig(ReadItemsStepConfigurerSupport configurerSupport) {
        this.configurerSupport = configurerSupport;
    }


    @Bean(name = STEP_NAME)
    public Step step() {
        return configurerSupport.buildStep(
            STEP_NAME,
            trDtoItemReader(),
            trDtoTrItemProcessor(),
            trItemWriter()
        );
    }

    @Bean
    public ItemReader<TrDto> trDtoItemReader() {
        return configurerSupport.buildReader("selectTr");
    }

    @Bean
    public ItemProcessor<TrDto, Tr> trDtoTrItemProcessor() {
        return trDto -> new Tr(
            trDto.getTnId(),
            trDto.getTnInstance(),
            trDto.getGosnum(),
            trDto.getGaragenum(),
            trDto.getDtInsert(),
            null,
            trDto.getIsDeleted().shortValue(),
            LocalDateTime.now(),
            trDto.getDepoTnId()
        );
    }

    @Bean(name = WRITER_NAME)
    public ListItemContainer<Tr> trItemWriter() {
        return new ListItemContainer<>();
    }
}
