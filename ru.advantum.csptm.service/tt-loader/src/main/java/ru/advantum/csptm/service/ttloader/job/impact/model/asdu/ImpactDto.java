package ru.advantum.csptm.service.ttloader.job.impact.model.asdu;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class ImpactDto {
    private final LocalDateTime orderDate;

    private final Long nrdIdentificator;

    private final Long nrdIdentificatorOld;

    private final Long nrdGrafic;

    private final Long mngmBegintime;

    private final Long mngmEndtime;

    private final Long mngmBeginorder;

    private final Long mngmEndorder;

    private final Long mngmIdentificator;

    private final String tnInstance;

    public ImpactDto(LocalDateTime orderDate, Long nrdIdentificator, Long nrdIdentificatorOld, Long nrdGrafic, Long mngmBegintime, Long mngmEndtime, Long mngmBeginorder, Long mngmEndorder, Long mngmIdentificator, String tnInstance) {
        this.orderDate = orderDate;
        this.nrdIdentificator = nrdIdentificator;
        this.nrdIdentificatorOld = nrdIdentificatorOld;
        this.nrdGrafic = nrdGrafic;
        this.mngmBegintime = mngmBegintime;
        this.mngmEndtime = mngmEndtime;
        this.mngmBeginorder = mngmBeginorder;
        this.mngmEndorder = mngmEndorder;
        this.mngmIdentificator = mngmIdentificator;
        this.tnInstance = tnInstance;
    }

    public ImpactDto(Timestamp orderDate, Long nrdIdentificator, Long nrdIdentificatorOld, Long nrdGrafic, Long mngmBegintime, Long mngmEndtime, Long mngmBeginorder, Long mngmEndorder, Long mngmIdentificator, String tnInstance) {
        this(
                orderDate != null ? orderDate.toLocalDateTime() : null,
                nrdIdentificator,
                nrdIdentificatorOld,
                nrdGrafic,
                mngmBegintime,
                mngmEndtime,
                mngmBeginorder,
                mngmEndorder,
                mngmIdentificator,
                tnInstance
        );
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public Long getNrdIdentificator() {
        return nrdIdentificator;
    }

    public Long getNrdIdentificatorOld() {
        return nrdIdentificatorOld;
    }

    public Long getNrdGrafic() {
        return nrdGrafic;
    }

    public Long getMngmBegintime() {
        return mngmBegintime;
    }

    public Long getMngmEndtime() {
        return mngmEndtime;
    }

    public Long getMngmBeginorder() {
        return mngmBeginorder;
    }

    public Long getMngmEndorder() {
        return mngmEndorder;
    }

    public Long getMngmIdentificator() {
        return mngmIdentificator;
    }

    public String getTnInstance() {
        return tnInstance;
    }
}
