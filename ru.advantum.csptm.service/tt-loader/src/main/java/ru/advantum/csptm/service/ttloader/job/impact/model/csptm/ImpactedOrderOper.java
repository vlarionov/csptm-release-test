package ru.advantum.csptm.service.ttloader.job.impact.model.csptm;

import java.time.LocalDateTime;

public class ImpactedOrderOper {
    private final LocalDateTime timePlan;

    private final LocalDateTime timeOper;

    private final LocalDateTime dtUpdate;

    private final Long isActive;

    private final Long stopErmId;

    private final Long cnrdOrderby;

    private final String tnInstance;

    private final Long orderNum;

    private final Long orderRoundNum;

    private final Long roundNum;

    private final String roundType;

    private Integer impactImportId;

    public ImpactedOrderOper(LocalDateTime timePlan, LocalDateTime timeOper, LocalDateTime dtUpdate, Long isActive, Long stopErmId, Long cnrdOrderby, String tnInstance, Long orderNum, Long orderRoundNum, Long roundNum, String roundType) {
        this.timePlan = timePlan;
        this.timeOper = timeOper;
        this.dtUpdate = dtUpdate;
        this.isActive = isActive;
        this.stopErmId = stopErmId;
        this.cnrdOrderby = cnrdOrderby;
        this.tnInstance = tnInstance;
        this.orderNum = orderNum;
        this.orderRoundNum = orderRoundNum;
        this.roundNum = roundNum;
        this.roundType = roundType;
    }

    public LocalDateTime getTimePlan() {
        return timePlan;
    }

    public LocalDateTime getTimeOper() {
        return timeOper;
    }

    public LocalDateTime getDtUpdate() {
        return dtUpdate;
    }

    public Long getIsActive() {
        return isActive;
    }

    public Long getStopErmId() {
        return stopErmId;
    }

    public Long getCnrdOrderby() {
        return cnrdOrderby;
    }

    public String getTnInstance() {
        return tnInstance;
    }

    public Long getOrderNum() {
        return orderNum;
    }

    public Long getOrderRoundNum() {
        return orderRoundNum;
    }

    public Long getRoundNum() {
        return roundNum;
    }

    public String getRoundType() {
        return roundType;
    }

    public void setImpactImportId(Integer impactImportId) {
        this.impactImportId = impactImportId;
    }

    public Integer getImpactImportId() {
        return impactImportId;
    }
}