package ru.advantum.csptm.service.ttloader.job.full.model.csptm;

import java.time.LocalDateTime;

public class OrderOper {
    private final Long timetablePlanId;

    private final String tnInstance;

    private final Long orderRoundNum;

    private final Long roundNum;

    private final String roundType;

    private final Long orderNum;

    private final LocalDateTime timePlan;

    private final LocalDateTime timeOper;

    private final Long signDeleted;

    private final LocalDateTime dtUpdate;

    private final Long isActive;

    private final Long stopErmId;

    private final Short isKp;

    private final Long cnrdOrderby;

    public OrderOper(Long timetablePlanId, String tnInstance, Long orderRoundNum, Long roundNum, String roundType, Long orderNum, LocalDateTime timePlan, LocalDateTime timeOper, Long signDeleted, LocalDateTime dtUpdate, Long isActive, Long stopErmId, Short isKp, Long cnrdOrderby) {
        this.timetablePlanId = timetablePlanId;
        this.tnInstance = tnInstance;
        this.orderRoundNum = orderRoundNum;
        this.roundNum = roundNum;
        this.roundType = roundType;
        this.orderNum = orderNum;
        this.timePlan = timePlan;
        this.timeOper = timeOper;
        this.signDeleted = signDeleted;
        this.dtUpdate = dtUpdate;
        this.isActive = isActive;
        this.stopErmId = stopErmId;
        this.isKp = isKp;
        this.cnrdOrderby = cnrdOrderby;
    }

    public Long getTimetablePlanId() {
        return timetablePlanId;
    }

    public String getTnInstance() {
        return tnInstance;
    }

    public Long getOrderRoundNum() {
        return orderRoundNum;
    }

    public Long getRoundNum() {
        return roundNum;
    }

    public String getRoundType() {
        return roundType;
    }

    public Long getOrderNum() {
        return orderNum;
    }

    public LocalDateTime getTimePlan() {
        return timePlan;
    }

    public LocalDateTime getTimeOper() {
        return timeOper;
    }

    public Long getSignDeleted() {
        return signDeleted;
    }

    public LocalDateTime getDtUpdate() {
        return dtUpdate;
    }

    public Long getIsActive() {
        return isActive;
    }

    public Long getStopErmId() {
        return stopErmId;
    }

    public Short getIsKp() {
        return isKp;
    }

    public Long getCnrdOrderby() {
        return cnrdOrderby;
    }
}
