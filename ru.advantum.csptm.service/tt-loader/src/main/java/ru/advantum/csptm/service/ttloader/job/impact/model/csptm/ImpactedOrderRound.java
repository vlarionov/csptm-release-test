package ru.advantum.csptm.service.ttloader.job.impact.model.csptm;

public class ImpactedOrderRound {
    private final Long orderRoundNum;

    private final String tnInstance;

    private final Long isActive;

    private final Long orderNum;

    private final Short timetableEntryNum;

    private final Long roundNum;


    public ImpactedOrderRound(Long orderRoundNum, String tnInstance, Long isActive, Long orderNum, Short timetableEntryNum, Long roundNum) {
        this.orderRoundNum = orderRoundNum;
        this.tnInstance = tnInstance;
        this.isActive = isActive;
        this.orderNum = orderNum;
        this.timetableEntryNum = timetableEntryNum;
        this.roundNum = roundNum;
    }

    public Long getOrderRoundNum() {
        return orderRoundNum;
    }

    public String getTnInstance() {
        return tnInstance;
    }

    public Long getIsActive() {
        return isActive;
    }

    public Long getOrderNum() {
        return orderNum;
    }

    public Short getTimetableEntryNum() {
        return timetableEntryNum;
    }

    public Long getRoundNum() {
        return roundNum;
    }
}
