package ru.advantum.csptm.service.ttloader.job.full.model.asdu;

public class TimetableEntryDto {
    private final Long nrdGrafic;

    private final Integer nNum;

    public TimetableEntryDto(Long nrdGrafic, Integer nNum) {
        this.nrdGrafic = nrdGrafic;
        this.nNum = nNum;
    }

    public Long getNrdGrafic() {
        return nrdGrafic;
    }

    public Integer getNNum() {
        return nNum;
    }
}