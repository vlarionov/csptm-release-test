package ru.advantum.csptm.service.ttloader.job.full.step;

import org.springframework.batch.core.Step;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.service.ttloader.app.commons.ListItemContainer;
import ru.advantum.csptm.service.ttloader.app.commons.ReadItemsStepConfigurerSupport;
import ru.advantum.csptm.service.ttloader.job.full.NdIdentificatorProvider;
import ru.advantum.csptm.service.ttloader.job.full.model.asdu.RoundDto;
import ru.advantum.csptm.service.ttloader.job.full.model.csptm.Round;

import java.time.LocalDateTime;

@Configuration
public class ReadRoundStepConfig {
    public static final String STEP_NAME = "readRound";
    public static final String WRITER_NAME = "roundWriter";

    private final ReadItemsStepConfigurerSupport configurerSupport;

    public ReadRoundStepConfig(ReadItemsStepConfigurerSupport configurerSupport) {
        this.configurerSupport = configurerSupport;
    }


    @Bean(name = STEP_NAME)
    public Step step() {
        return configurerSupport.buildStep(
                STEP_NAME,
                roundDtoItemReader(),
                roundDtoRoundItemProcessor(),
                roundItemWriter()
        );
    }

    @Bean
    public ItemReader<RoundDto> roundDtoItemReader() {
        return configurerSupport.buildReader("selectRound", NdIdentificatorProvider.ND_IDENTIFICATOR_KEY);
    }

    @Bean
    public ItemProcessor<RoundDto, Round> roundDtoRoundItemProcessor() {
        return roundDto -> new Round(
                roundDto.getTnInstance(),
                roundDto.getNrdIdentificator(),
                roundDto.getOrderDate(),
                getDirectionId(roundDto.getDstnType()),
                LocalDateTime.now(),
                roundDto.getDstnIdentificator(),
                roundDto.getDstnType(),
                getRoundCode(roundDto.getRaceTitle()),
                (short) 0
        );
    }

    @Bean(name = WRITER_NAME)
    public ListItemContainer<Round> roundItemWriter() {
        return new ListItemContainer<>();
    }

    private String getRoundCode(String raceTitle) {
        if (raceTitle == null) {
            return null;
        }
        return raceTitle.replace(" ", "").toUpperCase();
    }

    private Long getDirectionId(String dstnType) {
        Integer ascii = ascii(dstnType);
        if (ascii == null) {
            return null;
        }
        return (ascii.longValue() + 1) % 2 + 1;
    }

    private Integer ascii(String arg) {
        if (arg == null || arg.isEmpty()) {
            return null;
        }
        return (int) arg.charAt(0);
    }


}
