package ru.advantum.csptm.service.ttloader.job.full.model.csptm;

public class OrderRound {

    private final String tnInstance;

    private final Long orderNum;

    private final Long roundNum;

    private final Long orderRoundNum;

    private final Long isActive;

    private final Short timetableEntryNum;

    public OrderRound(String tnInstance, Long orderNum, Long roundNum, Long orderRoundNum, Long isActive, Short timetableEntryNum) {
        this.tnInstance = tnInstance;
        this.orderNum = orderNum;
        this.roundNum = roundNum;
        this.orderRoundNum = orderRoundNum;
        this.isActive = isActive;
        this.timetableEntryNum = timetableEntryNum;
    }

    public String getTnInstance() {
        return tnInstance;
    }

    public Long getOrderNum() {
        return orderNum;
    }

    public Long getRoundNum() {
        return roundNum;
    }

    public Long getOrderRoundNum() {
        return orderRoundNum;
    }

    public Long getIsActive() {
        return isActive;
    }

    public Short getTimetableEntryNum() {
        return timetableEntryNum;
    }
}
