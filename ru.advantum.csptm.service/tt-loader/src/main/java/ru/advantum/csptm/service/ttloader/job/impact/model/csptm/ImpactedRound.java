package ru.advantum.csptm.service.ttloader.job.impact.model.csptm;

import java.time.LocalDateTime;

public class ImpactedRound {
    private final Long directionId;

    private final LocalDateTime dtUpdate;

    private final Long roundNum;

    private final String roundType;

    private final String roundCode;

    private final String tnInstance;

    private final Long rmtId;

    private final Short timetableEntryNum;


    public ImpactedRound(Long directionId, LocalDateTime dtUpdate, Long roundNum, String roundType, String roundCode, String tnInstance, Long rmtId, Short timetableEntryNum) {
        this.directionId = directionId;
        this.dtUpdate = dtUpdate;
        this.roundNum = roundNum;
        this.roundType = roundType;
        this.roundCode = roundCode;
        this.tnInstance = tnInstance;
        this.rmtId = rmtId;
        this.timetableEntryNum = timetableEntryNum;
    }

    public Long getDirectionId() {
        return directionId;
    }

    public LocalDateTime getDtUpdate() {
        return dtUpdate;
    }

    public Long getRoundNum() {
        return roundNum;
    }

    public String getRoundType() {
        return roundType;
    }

    public String getRoundCode() {
        return roundCode;
    }

    public String getTnInstance() {
        return tnInstance;
    }

    public Long getRmtId() {
        return rmtId;
    }

    public Short getTimetableEntryNum() {
        return timetableEntryNum;
    }
}
