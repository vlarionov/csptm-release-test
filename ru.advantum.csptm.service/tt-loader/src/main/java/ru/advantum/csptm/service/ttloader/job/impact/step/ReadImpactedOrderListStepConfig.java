package ru.advantum.csptm.service.ttloader.job.impact.step;

import org.springframework.batch.core.Step;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.service.ttloader.app.commons.ListItemContainer;
import ru.advantum.csptm.service.ttloader.app.commons.ReadItemsStepConfigurerSupport;
import ru.advantum.csptm.service.ttloader.job.impact.ImpactPeriodProvider;
import ru.advantum.csptm.service.ttloader.job.impact.model.asdu.ImpactedOrderListDto;
import ru.advantum.csptm.service.ttloader.job.impact.model.csptm.ImpactedOrderList;

import java.time.LocalDateTime;


@Configuration
public class ReadImpactedOrderListStepConfig {
    public static final String STEP_NAME = "readOrderList";
    public static final String WRITER_NAME = "orderListWriter";


    private final ReadItemsStepConfigurerSupport configurerSupport;

    public ReadImpactedOrderListStepConfig(ReadItemsStepConfigurerSupport configurerSupport) {
        this.configurerSupport = configurerSupport;
    }


    @Bean(name = STEP_NAME)
    public Step step() {
        return configurerSupport.buildStep(
                STEP_NAME,
                orderListDtoItemReader(),
                orderListDtoOrderListItemProcessor(),
                orderListItemWriter()
        );
    }

    @Bean
    public ItemReader<ImpactedOrderListDto> orderListDtoItemReader() {
        return configurerSupport.buildReader(
                "selectImpactedOrderList",
                ImpactPeriodProvider.BEGIN_DATE_KEY,
                ImpactPeriodProvider.END_DATE_KEY,
                ImpactPeriodProvider.ND_IDENTIFICATORS_KEY
        );
    }

    @Bean
    public ItemProcessor<ImpactedOrderListDto, ImpactedOrderList> orderListDtoOrderListItemProcessor() {
        return orderListDto -> new ImpactedOrderList(
                orderListDto.getTnInstance(),
                orderListDto.getNsidUniqueid(),
                orderListDto.getGarageNum(),
                orderListDto.getNrdIdentificator(),
                orderListDto.getOrderDate(),
                Long.valueOf(orderListDto.getNrdSmena()),
                0L,
                LocalDateTime.now(),
                1L,
                orderListDto.getnNum().longValue(),
                orderListDto.getNrdGrafic().shortValue()
        );
    }

    @Bean(name = WRITER_NAME)
    public ListItemContainer<ImpactedOrderList> orderListItemWriter() {
        return new ListItemContainer<>();
    }
}

