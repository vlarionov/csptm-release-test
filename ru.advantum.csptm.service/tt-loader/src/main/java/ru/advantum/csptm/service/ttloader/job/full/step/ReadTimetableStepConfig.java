package ru.advantum.csptm.service.ttloader.job.full.step;

import org.springframework.batch.core.Step;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.service.ttloader.app.commons.ListItemContainer;
import ru.advantum.csptm.service.ttloader.app.commons.ReadItemsStepConfigurerSupport;
import ru.advantum.csptm.service.ttloader.job.full.model.asdu.TimetableDto;
import ru.advantum.csptm.service.ttloader.job.full.model.csptm.Timetable;

import java.time.LocalDateTime;

@Configuration
public class ReadTimetableStepConfig {
    public static final String STEP_NAME = "readTimetable";
    public static final String WRITER_NAME = "timetableWriter";

    private final ReadItemsStepConfigurerSupport configurerSupport;

    public ReadTimetableStepConfig(ReadItemsStepConfigurerSupport configurerSupport) {
        this.configurerSupport = configurerSupport;
    }


    @Bean(name = STEP_NAME)
    public Step step() {
        return configurerSupport.buildStep(
                STEP_NAME,
                timetableDtoItemReader(),
                timetableDtoTimetableItemProcessor(),
                timetableItemWriter()
        );
    }

    @Bean
    public ItemReader<TimetableDto> timetableDtoItemReader() {
        return configurerSupport.buildReader("selectTimetable");
    }

    @Bean
    public ItemProcessor<TimetableDto, Timetable> timetableDtoTimetableItemProcessor() {
        return timetableDto -> new Timetable(
                timetableDto.getMvIdentificator(),
                timetableDto.getTtIdentificator(),
                timetableDto.getMvMarsh(),
                timetableDto.getVarr(),
                timetableDto.getMvIncludedate(),
                timetableDto.getMvExcludedate(),
                (short) 0,
                LocalDateTime.now(),
                timetableDto.getMvDnd()
        );
    }

    @Bean(name = WRITER_NAME)
    public ListItemContainer<Timetable> timetableItemWriter() {
        return new ListItemContainer<>();
    }
}
