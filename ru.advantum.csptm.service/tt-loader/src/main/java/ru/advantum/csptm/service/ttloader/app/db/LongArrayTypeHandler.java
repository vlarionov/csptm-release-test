package ru.advantum.csptm.service.ttloader.app.db;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class LongArrayTypeHandler extends BaseTypeHandler<List<Long>> {
    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, List<Long> parameter, JdbcType jdbcType) throws SQLException {
        ps.setArray(i, ps.getConnection().createArrayOf("int8", parameter.toArray(new Long[parameter.size()])));
    }

    @Override
    public List<Long> getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return toLongList(rs.getArray(columnName));
    }

    @Override
    public List<Long> getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return toLongList(rs.getArray(columnIndex));
    }

    @Override
    public List<Long> getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return toLongList(cs.getArray(columnIndex));
    }

    private List<Long> toLongList(Array array) throws SQLException {
        if (array == null) {
            return null;
        }
        Object[] arrayObjects = (Object[]) array.getArray();
        if (arrayObjects == null) {
            return null;
        }
        List<Long> result = new ArrayList<>();
        for (Object arrayObject : arrayObjects) {
            result.add((long) arrayObject);
        }
        return result;
    }

}