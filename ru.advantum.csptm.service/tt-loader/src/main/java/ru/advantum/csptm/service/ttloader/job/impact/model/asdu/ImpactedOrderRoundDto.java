package ru.advantum.csptm.service.ttloader.job.impact.model.asdu;

import java.math.BigDecimal;

public class ImpactedOrderRoundDto {
    private final Long cnrdRoutenumber;

    private final String tnInstance;

    private final BigDecimal avgCnrdSysmark;

    private final Long nrdIdentificator;

    private final Integer cnrdGraph;

    private final Long dstnIdentificator;

    public ImpactedOrderRoundDto(Long cnrdRoutenumber, String tnInstance, BigDecimal avgCnrdSysmark, Long nrdIdentificator, Integer cnrdGraph, Long dstnIdentificator) {
        this.cnrdRoutenumber = cnrdRoutenumber;
        this.tnInstance = tnInstance;
        this.avgCnrdSysmark = avgCnrdSysmark;
        this.nrdIdentificator = nrdIdentificator;
        this.cnrdGraph = cnrdGraph;
        this.dstnIdentificator = dstnIdentificator;
    }

    public Long getCnrdRoutenumber() {
        return cnrdRoutenumber;
    }

    public String getTnInstance() {
        return tnInstance;
    }

    public BigDecimal getAvgCnrdSysmark() {
        return avgCnrdSysmark;
    }

    public Long getNrdIdentificator() {
        return nrdIdentificator;
    }

    public Integer getCnrdGraph() {
        return cnrdGraph;
    }

    public Long getDstnIdentificator() {
        return dstnIdentificator;
    }
}
