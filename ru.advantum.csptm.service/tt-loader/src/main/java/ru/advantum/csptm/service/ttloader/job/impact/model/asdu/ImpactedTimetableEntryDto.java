package ru.advantum.csptm.service.ttloader.job.impact.model.asdu;

public class ImpactedTimetableEntryDto {
    private final Long nrdGrafic;

    private final Integer nNum;

    public ImpactedTimetableEntryDto(Long nrdGrafic, Integer nNum) {
        this.nrdGrafic = nrdGrafic;
        this.nNum = nNum;
    }

    public Long getNrdGrafic() {
        return nrdGrafic;
    }

    public Integer getNNum() {
        return nNum;
    }
}
