package ru.advantum.csptm.service.ttloader.job.full.step;

import org.springframework.batch.core.Step;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.service.ttloader.app.commons.ListItemContainer;
import ru.advantum.csptm.service.ttloader.app.commons.ReadItemsStepConfigurerSupport;
import ru.advantum.csptm.service.ttloader.job.full.NdIdentificatorProvider;
import ru.advantum.csptm.service.ttloader.job.full.model.asdu.TimetableEntryDto;
import ru.advantum.csptm.service.ttloader.job.full.model.csptm.TimetableEntry;

import java.time.LocalDateTime;

@Configuration
public class ReadTimetableEntryStepConfig {
    public static final String STEP_NAME = "readTimetableEntry";
    public static final String WRITER_NAME = "timetableEntryWriter";

    private final ReadItemsStepConfigurerSupport configurerSupport;

    public ReadTimetableEntryStepConfig(ReadItemsStepConfigurerSupport configurerSupport) {
        this.configurerSupport = configurerSupport;
    }


    @Bean(name = STEP_NAME)
    public Step step() {
        return configurerSupport.buildStep(
                STEP_NAME,
                timetableEntryDtoItemReader(),
                timetableEntryDtoTimetableEntryItemProcessor(),
                timetableEntryItemWriter()
        );
    }

    @Bean
    public ItemReader<TimetableEntryDto> timetableEntryDtoItemReader() {
        return configurerSupport.buildReader("selectTimetableEntry", NdIdentificatorProvider.ND_IDENTIFICATOR_KEY);
    }

    @Bean
    public ItemProcessor<TimetableEntryDto, TimetableEntry> timetableEntryDtoTimetableEntryItemProcessor() {
        return timetableEntryDto -> new TimetableEntry(
                timetableEntryDto.getNNum().longValue(),
                null,
                timetableEntryDto.getNrdGrafic().shortValue(),
                (short) 0,
                LocalDateTime.now()
        );
    }


    @Bean(name = WRITER_NAME)
    public ListItemContainer<TimetableEntry> timetableEntryItemWriter() {
        return new ListItemContainer<>();
    }
}
