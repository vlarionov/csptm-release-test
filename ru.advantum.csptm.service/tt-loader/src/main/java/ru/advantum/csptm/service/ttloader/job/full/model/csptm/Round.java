package ru.advantum.csptm.service.ttloader.job.full.model.csptm;

import java.time.LocalDateTime;

public class Round {
    private final String tnInstance;

    private final Long orderNum;

    private final LocalDateTime orderDate;

    private final Long directionId;

    private final LocalDateTime dtUpdate;

    private final Long roundNum;

    private final String roundType;

    private final String roundCode;

    private final Short signDeleted;

    public Round(String tnInstance, Long orderNum, LocalDateTime orderDate, Long directionId, LocalDateTime dtUpdate, Long roundNum, String roundType, String roundCode, Short signDeleted) {
        this.tnInstance = tnInstance;
        this.orderNum = orderNum;
        this.orderDate = orderDate;
        this.directionId = directionId;
        this.dtUpdate = dtUpdate;
        this.roundNum = roundNum;
        this.roundType = roundType;
        this.roundCode = roundCode;
        this.signDeleted = signDeleted;
    }

    public String getTnInstance() {
        return tnInstance;
    }

    public Long getOrderNum() {
        return orderNum;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public Long getDirectionId() {
        return directionId;
    }

    public LocalDateTime getDtUpdate() {
        return dtUpdate;
    }

    public Long getRoundNum() {
        return roundNum;
    }

    public String getRoundType() {
        return roundType;
    }

    public String getRoundCode() {
        return roundCode;
    }

    public Short getSignDeleted() {
        return signDeleted;
    }
}

