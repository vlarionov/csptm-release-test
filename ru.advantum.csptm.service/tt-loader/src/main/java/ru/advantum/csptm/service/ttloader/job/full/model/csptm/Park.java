package ru.advantum.csptm.service.ttloader.job.full.model.csptm;

import java.time.LocalDateTime;

public class Park {
    private final Long tnId;

    private final String tnInstance;

    private final String sName;

    private final Short signDeleted;

    private final Long pkRegnum;

    private final LocalDateTime dtBegin;

    private final LocalDateTime dtEnd;

    public Park(Long tnId, String tnInstance, String sName, Short signDeleted, Long pkRegnum, LocalDateTime dtBegin, LocalDateTime dtEnd) {
        this.tnId = tnId;
        this.tnInstance = tnInstance;
        this.sName = sName;
        this.signDeleted = signDeleted;
        this.pkRegnum = pkRegnum;
        this.dtBegin = dtBegin;
        this.dtEnd = dtEnd;
    }

    public Long getTnId() {
        return tnId;
    }

    public String getTnInstance() {
        return tnInstance;
    }

    public String getsName() {
        return sName;
    }

    public Short isSignDeleted() {
        return signDeleted;
    }

    public Long getPkRegnum() {
        return pkRegnum;
    }

    public LocalDateTime getDtBegin() {
        return dtBegin;
    }

    public LocalDateTime getDtEnd() {
        return dtEnd;
    }
}
