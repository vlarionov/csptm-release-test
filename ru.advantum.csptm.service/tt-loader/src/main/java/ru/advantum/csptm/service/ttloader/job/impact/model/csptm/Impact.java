package ru.advantum.csptm.service.ttloader.job.impact.model.csptm;

import java.time.LocalDateTime;

public class Impact {
    private final LocalDateTime orderDate;

    private final Long orderNum;

    private final Long orderNumOld;

    private final Short entryNum;

    private final LocalDateTime beginTimePlan;

    private final LocalDateTime endTimePlan;

    private final Long beginCnrdOrderby;

    private final Long endCnrdOrderby;

    private final Long impactId;

    private final String tnInstance;

    private Integer impactImportId;

    public Impact(LocalDateTime orderDate, Long orderNum, Long orderNumOld, Short entryNum, LocalDateTime beginTimePlan, LocalDateTime endTimePlan, Long beginCnrdOrderby, Long endCnrdOrderby, Long impactId, String tnInstance) {
        this.orderDate = orderDate;
        this.orderNum = orderNum;
        this.orderNumOld = orderNumOld;
        this.entryNum = entryNum;
        this.beginTimePlan = beginTimePlan;
        this.endTimePlan = endTimePlan;
        this.beginCnrdOrderby = beginCnrdOrderby;
        this.endCnrdOrderby = endCnrdOrderby;
        this.impactId = impactId;
        this.tnInstance = tnInstance;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public Long getOrderNum() {
        return orderNum;
    }

    public Long getOrderNumOld() {
        return orderNumOld;
    }

    public Short getEntryNum() {
        return entryNum;
    }

    public LocalDateTime getBeginTimePlan() {
        return beginTimePlan;
    }

    public LocalDateTime getEndTimePlan() {
        return endTimePlan;
    }

    public Long getBeginCnrdOrderby() {
        return beginCnrdOrderby;
    }

    public Long getEndCnrdOrderby() {
        return endCnrdOrderby;
    }

    public Long getImpactId() {
        return impactId;
    }

    public String getTnInstance() {
        return tnInstance;
    }

    public Integer getImpactImportId() {
        return impactImportId;
    }

    public void setImpactImportId(Integer impactImportId) {
        this.impactImportId = impactImportId;
    }
}


