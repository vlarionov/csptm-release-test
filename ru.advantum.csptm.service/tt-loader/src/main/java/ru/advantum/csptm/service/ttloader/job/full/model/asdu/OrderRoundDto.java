package ru.advantum.csptm.service.ttloader.job.full.model.asdu;

import java.math.BigDecimal;

public class OrderRoundDto {
    private final Long cnrdRoutenumber;

    private final String tnInstance;

    private final Long nrdIdentificator;

    private final Long dstnIdentificator;

    private final BigDecimal avgCnrdSysmark;

    private final Integer cnrdGraph;

    public OrderRoundDto(Long cnrdRoutenumber, String tnInstance, Long nrdIdentificator, Long dstnIdentificator, BigDecimal avgCnrdSysmark, Integer cnrdGraph) {
        this.cnrdRoutenumber = cnrdRoutenumber;
        this.tnInstance = tnInstance;
        this.nrdIdentificator = nrdIdentificator;
        this.dstnIdentificator = dstnIdentificator;
        this.avgCnrdSysmark = avgCnrdSysmark;
        this.cnrdGraph = cnrdGraph;
    }

    public Long getCnrdRoutenumber() {
        return cnrdRoutenumber;
    }

    public BigDecimal getAvgCnrdSysmark() {
        return avgCnrdSysmark;
    }

    public Integer getCnrdGraph() {
        return cnrdGraph;
    }

    public String getTnInstance() {
        return tnInstance;
    }

    public Long getNrdIdentificator() {
        return nrdIdentificator;
    }

    public Long getDstnIdentificator() {
        return dstnIdentificator;
    }
}
