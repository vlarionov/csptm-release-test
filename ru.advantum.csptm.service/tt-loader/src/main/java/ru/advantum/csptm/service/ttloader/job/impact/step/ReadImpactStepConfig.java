package ru.advantum.csptm.service.ttloader.job.impact.step;

import org.springframework.batch.core.Step;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.service.ttloader.app.commons.ListItemContainer;
import ru.advantum.csptm.service.ttloader.app.commons.ReadItemsStepConfigurerSupport;
import ru.advantum.csptm.service.ttloader.job.impact.ImpactPeriodProvider;
import ru.advantum.csptm.service.ttloader.job.impact.model.asdu.ImpactDto;
import ru.advantum.csptm.service.ttloader.job.impact.model.csptm.Impact;

import java.time.LocalDateTime;

@Configuration
public class ReadImpactStepConfig {
    public static final String STEP_NAME = "readImpact";
    public static final String WRITER_NAME = "impactWriter";

    private final ReadItemsStepConfigurerSupport configurerSupport;

    public ReadImpactStepConfig(ReadItemsStepConfigurerSupport configurerSupport) {
        this.configurerSupport = configurerSupport;
    }

    @Bean(name = STEP_NAME)
    public Step step() {
        return configurerSupport.buildStep(
                STEP_NAME,
                impactItemReader(),
                impactItemProcessor(),
                impactListItemWriter()
        );
    }

    @Bean
    public ItemReader<ImpactDto> impactItemReader() {
        return configurerSupport.buildReader("selectImpact", ImpactPeriodProvider.BEGIN_DATE_KEY, ImpactPeriodProvider.END_DATE_KEY, ImpactPeriodProvider.ND_IDENTIFICATORS_KEY);
    }

    @Bean
    public ItemProcessor<ImpactDto, Impact> impactItemProcessor() {
        return impactDto -> new Impact(
                impactDto.getOrderDate(),
                impactDto.getNrdIdentificator(),
                impactDto.getNrdIdentificatorOld(),
                impactDto.getNrdGrafic().shortValue(),
                getTimePlan(impactDto.getOrderDate(), impactDto.getMngmBegintime()),
                getEndTimePlan(impactDto),
                impactDto.getMngmBeginorder(),
                getEndCnrdOrderby(impactDto),
                impactDto.getMngmIdentificator(),
                impactDto.getTnInstance()
        );
    }

    @Bean(name = WRITER_NAME)
    public ListItemContainer<Impact> impactListItemWriter() {
        return new ListItemContainer<>();
    }

    private LocalDateTime getTimePlan(LocalDateTime orderDate, Long mngmTime) {
        if (orderDate == null || mngmTime == null) {
            return null;
        }
        return orderDate.plusMinutes(mngmTime);
    }

    private LocalDateTime getEndTimePlan(ImpactDto impactDto) {
        if (impactDto.getMngmBegintime() != null && impactDto.getMngmBegintime() > 0 && impactDto.getMngmEndtime() != null && impactDto.getMngmEndtime() == 0) {
            return null;
        } else {
            return getTimePlan(impactDto.getOrderDate(), impactDto.getMngmEndtime());
        }
    }

    private Long getEndCnrdOrderby(ImpactDto impactDto) {
        if (impactDto.getMngmBeginorder() != null && impactDto.getMngmBeginorder() > 0 && impactDto.getMngmEndorder() != null && impactDto.getMngmEndorder() == 0) {
            return null;
        } else {
            return impactDto.getMngmEndorder();
        }
    }

}
