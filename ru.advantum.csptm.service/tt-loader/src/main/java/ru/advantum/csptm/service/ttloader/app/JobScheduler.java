package ru.advantum.csptm.service.ttloader.app;

import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import ru.advantum.csptm.service.ttloader.app.db.CSPTMConfig;
import ru.advantum.csptm.service.ttloader.job.full.FullImportJobConfig;
import ru.advantum.csptm.service.ttloader.job.impact.ImpactImportJobConfig;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static ru.advantum.csptm.service.ttloader.job.full.NdIdentificatorProvider.ORDER_DATE_KEY;
import static ru.advantum.csptm.service.ttloader.job.impact.ImpactPeriodProvider.BEGIN_DATE_KEY;
import static ru.advantum.csptm.service.ttloader.job.impact.ImpactPeriodProvider.END_DATE_KEY;

@Service
public class JobScheduler {
    private static Logger LOG = LoggerFactory.getLogger(JobScheduler.class);

    private final ConfigurableApplicationContext applicationContext;

    private final JobLauncher jobLauncher;

    private final SqlSessionTemplate csptmSqlSessionTemplate;

    private final long maxImpactImportPeriod;

    public JobScheduler(
            ConfigurableApplicationContext applicationContext,
            JobLauncher jobLauncher,
            @Qualifier(CSPTMConfig.SQL_SESSION_TEMPLATE) SqlSessionTemplate csptmSqlSessionTemplate,
            @Value("#{applicationConfig.maxImpactImportPeriod}") long maxImpactImportPeriod
    ) {
        this.applicationContext = applicationContext;
        this.jobLauncher = jobLauncher;
        this.csptmSqlSessionTemplate = csptmSqlSessionTemplate;
        this.maxImpactImportPeriod = maxImpactImportPeriod;
    }


    @PostConstruct
    public synchronized void runRecoveryFullImportJobs() {
        LocalDate lastOrderDate = LocalDate.now();

        Set<LocalDate> fullImportedDates = new HashSet<>(csptmSqlSessionTemplate.selectList("selectFullImportDates"));

        LocalDate firstFullImportedDate = fullImportedDates
                .stream()
                .max(LocalDate::compareTo)
                .orElse(lastOrderDate);

        for (LocalDate orderDate = firstFullImportedDate; orderDate.compareTo(lastOrderDate) <= 0; orderDate = orderDate.plusDays(1)) {
            if (!fullImportedDates.contains(orderDate)) {
                runFullImportJob(orderDate);
            }
        }
    }

    @Scheduled(cron = "#{applicationConfig.fullImportCron}")
    public synchronized void runDailyFullImportJob() {
        LocalDate orderDate = LocalDate.now();
        runFullImportJob(orderDate);
    }

    private synchronized void runFullImportJob(LocalDate orderDate) {
        try {
            JobParameters jobParameters = new JobParameters(Collections.singletonMap(
                    ORDER_DATE_KEY,
                    new JobParameter(orderDate.toString())
            ));
            runJob(FullImportJobConfig.class, jobParameters);
        } catch (Exception e) {
            LOG.error("Full import job error", e);
        }
    }


    @Scheduled(fixedRateString = "#{applicationConfig.impactImportRate}")
    public synchronized void runImpactImportJob() {
        try {
            LocalDateTime now = LocalDateTime.now();
            LocalDateTime lastEndDate = csptmSqlSessionTemplate.selectOne("selectLastImpactImportEndDate");

            if (lastEndDate != null && ChronoUnit.MINUTES.between(lastEndDate, now) <= 1) {
                return;
            }

            if (lastEndDate == null) {
                lastEndDate = now;
            }

            LocalDateTime currentEndDate = now;
            Assert.isTrue(!currentEndDate.isBefore(lastEndDate), "Invalid last impact import date");

            if (!lastEndDate.toLocalDate().equals(currentEndDate.toLocalDate())) {
                currentEndDate = lastEndDate.toLocalDate().plusDays(1).atStartOfDay();
            }

            while (!lastEndDate.equals(currentEndDate)) {
                LocalDateTime finiteEndDate = currentEndDate;
                if (ChronoUnit.MILLIS.between(lastEndDate, currentEndDate) > maxImpactImportPeriod) {
                    finiteEndDate = lastEndDate.plus(maxImpactImportPeriod, ChronoUnit.MILLIS);
                }

                Map<String, JobParameter> jobParameterMap = new HashMap<>();
                jobParameterMap.put(BEGIN_DATE_KEY, new JobParameter(lastEndDate.toString()));
                jobParameterMap.put(END_DATE_KEY, new JobParameter(finiteEndDate.toString()));

                ExitStatus exitStatus = runJob(ImpactImportJobConfig.class, new JobParameters(jobParameterMap));
                if (ExitStatus.FAILED.getExitCode().equals(exitStatus.getExitCode())) {
                    return;
                }

                lastEndDate = finiteEndDate;
            }
        } catch (Exception e) {
            LOG.error("Impact import job error", e);
        }
    }

    private ExitStatus runJob(Class<?> jobConfigClass, JobParameters jobParameters) throws Exception {
        try (ConfigurableApplicationContext ctx = new SpringApplicationBuilder(jobConfigClass).parent(applicationContext).run()) {
            Job job = ctx.getBean(Job.class);
            return jobLauncher.run(job, jobParameters).getExitStatus();
        }
    }


}
