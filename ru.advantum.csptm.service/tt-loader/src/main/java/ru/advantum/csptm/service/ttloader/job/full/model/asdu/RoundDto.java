package ru.advantum.csptm.service.ttloader.job.full.model.asdu;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class RoundDto {
    private final String tnInstance;

    private final Long nrdIdentificator;

    private final LocalDateTime orderDate;

    private final String dstnType;

    private final Long dstnIdentificator;

    private final String raceTitle;

    public RoundDto(String tnInstance, Long nrdIdentificator, LocalDateTime orderDate, String dstnType, Long dstnIdentificator, String raceTitle) {
        this.tnInstance = tnInstance;
        this.nrdIdentificator = nrdIdentificator;
        this.orderDate = orderDate;
        this.dstnType = dstnType;
        this.dstnIdentificator = dstnIdentificator;
        this.raceTitle = raceTitle;
    }

    public RoundDto(String tnInstance, Long nrdIdentificator, Timestamp orderDate, String dstnType, Long dstnIdentificator, String raceTitle) {
        this(
                tnInstance,
                nrdIdentificator,
                orderDate != null ? orderDate.toLocalDateTime() : null,
                dstnType,
                dstnIdentificator,
                raceTitle
        );
    }

    public String getTnInstance() {
        return tnInstance;
    }

    public Long getNrdIdentificator() {
        return nrdIdentificator;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public String getDstnType() {
        return dstnType;
    }

    public Long getDstnIdentificator() {
        return dstnIdentificator;
    }

    public String getRaceTitle() {
        return raceTitle;
    }
}
