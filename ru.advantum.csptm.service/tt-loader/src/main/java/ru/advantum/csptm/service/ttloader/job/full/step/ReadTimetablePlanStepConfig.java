package ru.advantum.csptm.service.ttloader.job.full.step;

import org.springframework.batch.core.Step;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.service.ttloader.app.commons.ListItemContainer;
import ru.advantum.csptm.service.ttloader.app.commons.ReadItemsStepConfigurerSupport;
import ru.advantum.csptm.service.ttloader.job.full.model.asdu.TimetablePlanDto;
import ru.advantum.csptm.service.ttloader.job.full.model.csptm.TimetablePlan;

import java.time.LocalDateTime;

@Configuration
public class ReadTimetablePlanStepConfig {
    public static final String STEP_NAME = "readTimetablePlan";
    public static final String WRITER_NAME = "timetablePlanWriter";

    private final ReadItemsStepConfigurerSupport configurerSupport;

    public ReadTimetablePlanStepConfig(ReadItemsStepConfigurerSupport configurerSupport) {
        this.configurerSupport = configurerSupport;
    }

    @Bean(name = STEP_NAME)
    public Step step() {
        return configurerSupport.buildStep(
                STEP_NAME,
                timetablePlanDtoItemReader(),
                timetablePlanDtoTimetablePlanItemProcessor(),
                timetablePlanItemWriter()
        );
    }

    @Bean
    public ItemReader<TimetablePlanDto> timetablePlanDtoItemReader() {
        return configurerSupport.buildReader("selectTimetablePlan");
    }

    @Bean
    public ItemProcessor<TimetablePlanDto, TimetablePlan> timetablePlanDtoTimetablePlanItemProcessor() {
        return timetablePlanDto -> new TimetablePlan(
                timetablePlanDto.getStopId(),
                timetablePlanDto.getTimeBEpoch(),
                timetablePlanDto.getTimeEEpoch(),
                timetablePlanDto.getSm(),
                LocalDateTime.now(),
                timetablePlanDto.getMvIdentificator(),
                timetablePlanDto.getTirp(),
                timetablePlanDto.getVector() != null ? timetablePlanDto.getVector() + 1 : null,
                timetablePlanDto.getNvx().shortValue()
        );
    }

    @Bean(name = WRITER_NAME)
    public ListItemContainer<TimetablePlan> timetablePlanItemWriter() {
        return new ListItemContainer<>();
    }
}