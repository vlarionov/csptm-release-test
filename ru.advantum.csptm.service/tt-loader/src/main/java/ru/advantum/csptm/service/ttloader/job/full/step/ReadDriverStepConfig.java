package ru.advantum.csptm.service.ttloader.job.full.step;

import org.springframework.batch.core.Step;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.service.ttloader.app.commons.ListItemContainer;
import ru.advantum.csptm.service.ttloader.app.commons.ReadItemsStepConfigurerSupport;
import ru.advantum.csptm.service.ttloader.job.full.model.asdu.DriverDto;
import ru.advantum.csptm.service.ttloader.job.full.model.csptm.Driver;

import java.time.LocalDateTime;

@Configuration
public class ReadDriverStepConfig {
    public static final String STEP_NAME = "readDriver";
    public static final String WRITER_NAME = "driverWriter";

    private final ReadItemsStepConfigurerSupport configurerSupport;

    public ReadDriverStepConfig(ReadItemsStepConfigurerSupport configurerSupport) {
        this.configurerSupport = configurerSupport;
    }

    @Bean(name = STEP_NAME)
    public Step step() {
        return configurerSupport.buildStep(
                STEP_NAME,
                driverDtoItemReader(),
                driverDtoDriverItemProcessor(),
                driverItemWriter()
        );
    }

    @Bean
    public ItemReader<DriverDto> driverDtoItemReader() {
        return configurerSupport.buildReader("selectDriver");
    }

    @Bean
    public ItemProcessor<DriverDto, Driver> driverDtoDriverItemProcessor() {
        return driverDto -> new Driver(
                driverDto.getFirstName(),
                driverDto.getSurname(),
                driverDto.getSecondName(),
                driverDto.getTabNum(),
                driverDto.getTnId(),
                driverDto.getTnInstance(),
                driverDto.getDtBegin(),
                driverDto.getDtEnd(),
                driverDto.getIsDeleted().shortValue(),
                LocalDateTime.now()
        );
    }

    @Bean(name = WRITER_NAME)
    public ListItemContainer<Driver> driverItemWriter() {
        return new ListItemContainer<>();
    }
}
