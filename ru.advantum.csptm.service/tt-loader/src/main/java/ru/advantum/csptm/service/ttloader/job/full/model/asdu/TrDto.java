package ru.advantum.csptm.service.ttloader.job.full.model.asdu;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class TrDto {
    private final Long tnId;

    private final String tnInstance;

    private final String gosnum;

    private final String garagenum;

    private final LocalDateTime dtInsert;

    private final Integer isDeleted;

    private final Long depoTnId;

    public TrDto(Long tnId, String tnInstance, String gosnum, String garagenum, LocalDateTime dtInsert, Integer isDeleted, Long depoTnId) {
        this.tnId = tnId;
        this.tnInstance = tnInstance;
        this.gosnum = gosnum;
        this.garagenum = garagenum;
        this.dtInsert = dtInsert;
        this.isDeleted = isDeleted;
        this.depoTnId = depoTnId;
    }

    public TrDto(Long tnId, String tnInstance, String gosnum, String garagenum, Timestamp dtInsert, Integer isDeleted, Long depoTnId) {
        this(
            tnId,
            tnInstance,
            gosnum,
            garagenum,
            dtInsert != null ? dtInsert.toLocalDateTime() : null,
            isDeleted,
            depoTnId
        );
    }

    public Long getTnId() {
        return tnId;
    }

    public String getTnInstance() {
        return tnInstance;
    }

    public String getGosnum() {
        return gosnum;
    }

    public String getGaragenum() {
        return garagenum;
    }

    public LocalDateTime getDtInsert() {
        return dtInsert;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public Long getDepoTnId() {
        return depoTnId;
    }
}
