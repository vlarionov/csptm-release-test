package ru.advantum.csptm.service.ttloader.job.impact;

import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import ru.advantum.csptm.service.ttloader.app.commons.IllegalImportStateException;
import ru.advantum.csptm.service.ttloader.app.db.ASDUConfig;
import ru.advantum.csptm.service.ttloader.app.db.CSPTMConfig;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ImpactPeriodProvider extends JobExecutionListenerSupport {
    private static final Logger LOG = LoggerFactory.getLogger(ImpactPeriodProvider.class);

    public static final String ND_IDENTIFICATORS_KEY = "ndIdentificators";
    public static final String BEGIN_DATE_KEY = "beginDate";
    public static final String END_DATE_KEY = "endDate";

    private final SqlSessionTemplate asduSqlSessionTemplate;
    private final SqlSessionTemplate csptmSessionTemplate;

    public ImpactPeriodProvider(
            @Qualifier(ASDUConfig.SQL_SESSION_TEMPLATE) SqlSessionTemplate asduSqlSessionTemplate,
            @Qualifier(CSPTMConfig.SQL_SESSION_TEMPLATE) SqlSessionTemplate csptmSessionTemplate
    ) {
        this.asduSqlSessionTemplate = asduSqlSessionTemplate;
        this.csptmSessionTemplate = csptmSessionTemplate;
    }

    @Override
    public void beforeJob(JobExecution jobExecution) {
        String beginDateStr = jobExecution.getJobParameters().getString(BEGIN_DATE_KEY);
        String endDateStr = jobExecution.getJobParameters().getString(END_DATE_KEY);

        Assert.notNull(beginDateStr);
        Assert.notNull(endDateStr);

        LocalDateTime beginDate = LocalDateTime.parse(beginDateStr);
        LocalDateTime endDate = LocalDateTime.parse(endDateStr);

        Map<String, Object> params = new HashMap<>();
        params.put(BEGIN_DATE_KEY, beginDate);
        params.put(END_DATE_KEY, endDate);
        List<Long> ndIdentificators = asduSqlSessionTemplate.selectList("selectImpactNdIdentificator", params);

        checkFullImportState(ndIdentificators);

        jobExecution.getExecutionContext().put(BEGIN_DATE_KEY, beginDate);
        jobExecution.getExecutionContext().put(END_DATE_KEY, endDate);
        jobExecution.getExecutionContext().put(ND_IDENTIFICATORS_KEY, ndIdentificators);

        LOG.info(ndIdentificators.toString());
    }

    private void checkFullImportState(List<Long> ndIdentificators) {
        List<LocalDate> impactOrderDates = asduSqlSessionTemplate.selectList(
                "selectOrderDates",
                Collections.singletonMap("ndIdentificators", ndIdentificators)
        );

        List<LocalDate> fullImportedDates = csptmSessionTemplate.selectList("selectFullImportDates");
        if (!fullImportedDates.containsAll(impactOrderDates)) {
            throw new IllegalImportStateException(
                    String.format("Full import does not complete for order dates %s", impactOrderDates)
            );
        }
    }

}
