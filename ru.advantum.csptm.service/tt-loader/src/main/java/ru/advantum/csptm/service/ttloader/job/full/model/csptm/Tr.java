package ru.advantum.csptm.service.ttloader.job.full.model.csptm;

import java.time.LocalDateTime;

public class Tr {
    private final Long tnId;

    private final String tnInstance;

    private final String licence;

    private final String garageNum;

    private final LocalDateTime dtBegin;

    private final LocalDateTime dtEnd;

    private final Short signDelete;

    private final LocalDateTime dtUpdate;

    private final Long parkTnId;

    public Tr(Long tnId, String tnInstance, String licence, String garageNum, LocalDateTime dtBegin, LocalDateTime dtEnd, Short signDelete, LocalDateTime dtUpdate, Long parkTnId) {
        this.tnId = tnId;
        this.tnInstance = tnInstance;
        this.licence = licence;
        this.garageNum = garageNum;
        this.dtBegin = dtBegin;
        this.dtEnd = dtEnd;
        this.signDelete = signDelete;
        this.dtUpdate = dtUpdate;
        this.parkTnId = parkTnId;
    }

    public Long getTnId() {
        return tnId;
    }

    public String getTnInstance() {
        return tnInstance;
    }

    public String getLicence() {
        return licence;
    }

    public String getGarageNum() {
        return garageNum;
    }

    public LocalDateTime getDtBegin() {
        return dtBegin;
    }

    public LocalDateTime getDtEnd() {
        return dtEnd;
    }

    public Short getSignDelete() {
        return signDelete;
    }

    public LocalDateTime getDtUpdate() {
        return dtUpdate;
    }

    public Long getParkTnId() {
        return parkTnId;
    }

}
