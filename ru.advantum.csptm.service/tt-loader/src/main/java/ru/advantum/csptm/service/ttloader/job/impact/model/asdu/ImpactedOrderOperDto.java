package ru.advantum.csptm.service.ttloader.job.impact.model.asdu;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class ImpactedOrderOperDto {
    private final LocalDateTime orderDate;

    private final Long cnrdTimeplan;

    private final Long cnrdDiflextion;

    private final Long cnrdSysmark;

    private final Long ermaId;

    private final Long cnrdOrderby;

    private final String tnInstance;

    private final Long nrdIdentificator;

    private final Long cnrdRoutenumber;

    private final Long dstnIdentificator;

    private final String dstnType;

    public ImpactedOrderOperDto(LocalDateTime orderDate, Long cnrdTimeplan, Long cnrdDiflextion, Long cnrdSysmark, Long ermaId, Long cnrdOrderby, String tnInstance, Long nrdIdentificator, Long cnrdRoutenumber, Long dstnIdentificator, String dstnType) {
        this.orderDate = orderDate;
        this.cnrdTimeplan = cnrdTimeplan;
        this.cnrdDiflextion = cnrdDiflextion;
        this.cnrdSysmark = cnrdSysmark;
        this.ermaId = ermaId;
        this.cnrdOrderby = cnrdOrderby;
        this.tnInstance = tnInstance;
        this.nrdIdentificator = nrdIdentificator;
        this.cnrdRoutenumber = cnrdRoutenumber;
        this.dstnIdentificator = dstnIdentificator;
        this.dstnType = dstnType;
    }

    public ImpactedOrderOperDto(Timestamp orderDate, Long cnrdTimeplan, Long cnrdDiflextion, Long cnrdSysmark, Long ermaId, Long cnrdOrderby, String tnInstance, Long nrdIdentificator, Long cnrdRoutenumber, Long dstnIdentificator, String dstnType) {
        this(
                orderDate != null ? orderDate.toLocalDateTime() : null,
                cnrdTimeplan,
                cnrdDiflextion,
                cnrdSysmark,
                ermaId,
                cnrdOrderby,
                tnInstance,
                nrdIdentificator,
                cnrdRoutenumber,
                dstnIdentificator,
                dstnType
        );
    }


    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public Long getCnrdTimeplan() {
        return cnrdTimeplan;
    }

    public Long getCnrdDiflextion() {
        return cnrdDiflextion;
    }

    public Long getCnrdSysmark() {
        return cnrdSysmark;
    }

    public Long getErmaId() {
        return ermaId;
    }

    public Long getCnrdOrderby() {
        return cnrdOrderby;
    }

    public String getTnInstance() {
        return tnInstance;
    }

    public Long getNrdIdentificator() {
        return nrdIdentificator;
    }

    public Long getCnrdRoutenumber() {
        return cnrdRoutenumber;
    }

    public Long getDstnIdentificator() {
        return dstnIdentificator;
    }

    public String getDstnType() {
        return dstnType;
    }

}
