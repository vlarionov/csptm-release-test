package ru.advantum.csptm.service.ttloader.job.full.step;

import org.springframework.batch.core.Step;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.service.ttloader.app.commons.ListItemContainer;
import ru.advantum.csptm.service.ttloader.app.commons.ReadItemsStepConfigurerSupport;
import ru.advantum.csptm.service.ttloader.job.full.NdIdentificatorProvider;
import ru.advantum.csptm.service.ttloader.job.full.model.asdu.OrderListDto;
import ru.advantum.csptm.service.ttloader.job.full.model.csptm.OrderList;

import java.time.LocalDateTime;


@Configuration
public class ReadOrderListStepConfig {
    public static final String STEP_NAME = "readOrderList";
    public static final String WRITER_NAME = "orderListWriter";


    private final ReadItemsStepConfigurerSupport configurerSupport;

    public ReadOrderListStepConfig(ReadItemsStepConfigurerSupport configurerSupport) {
        this.configurerSupport = configurerSupport;
    }


    @Bean(name = STEP_NAME)
    public Step step() {
        return configurerSupport.buildStep(
                STEP_NAME,
                orderListDtoItemReader(),
                orderListDtoOrderListItemProcessor(),
                orderListItemWriter()
        );
    }

    @Bean
    public ItemReader<OrderListDto> orderListDtoItemReader() {
        return configurerSupport.buildReader("selectOrderList", NdIdentificatorProvider.ND_IDENTIFICATOR_KEY);
    }

    @Bean
    public ItemProcessor<OrderListDto, OrderList> orderListDtoOrderListItemProcessor() {
        return orderListDto -> new OrderList(
                orderListDto.getTnInstance(),
                orderListDto.getNsidUniqueid(),
                orderListDto.getGarageNum(),
                orderListDto.getNrdIdentificator(),
                orderListDto.getOrderDate(),
                Long.valueOf(orderListDto.getNrdSmena()),
                0L,
                LocalDateTime.now(),
                1L,
                orderListDto.getnNum().longValue(),
                orderListDto.getNrdGrafic().shortValue()
        );
    }

    @Bean(name = WRITER_NAME)
    public ListItemContainer<OrderList> orderListItemWriter() {
        return new ListItemContainer<>();
    }
}

