package ru.advantum.csptm.service.model;

import java.util.Date;
import java.util.List;

/**
 * Created by Leonid on 26.12.2016.
 */
public class EmergencySituationJson {
    private Integer emergency_situation_id;
    private Date emergency_time;
    private String emergency_type;
    private Geometry emergency_place;
    private String danger_level;
    //я хз как сохранить список ид-ов, надо переделать позже
    private List<Integer> vehicles;
    private String description;

    public EmergencySituationJson() {
    }

    public EmergencySituationJson(Integer emergency_situation_id, Date emergency_time, String emergency_type, Geometry emergency_place, String danger_level, List<Integer> vehicles, String description) {
        this.emergency_situation_id = emergency_situation_id;
        this.emergency_time = emergency_time;
        this.emergency_type = emergency_type;
        this.emergency_place = emergency_place;
        this.danger_level = danger_level;
        this.vehicles = vehicles;
        this.description = description;
    }

    public Integer getEmergency_situation_id() {
        return emergency_situation_id;
    }

    public void setEmergency_situation_id(Integer emergency_situation_id) {
        this.emergency_situation_id = emergency_situation_id;
    }

    public Date getEmergency_time() {
        return emergency_time;
    }

    public void setEmergency_time(Date emergency_time) {
        this.emergency_time = emergency_time;
    }

    public String getEmergency_type() {
        return emergency_type;
    }

    public void setEmergency_type(String emergency_type) {
        this.emergency_type = emergency_type;
    }

    public Geometry getEmergency_place() {
        return emergency_place;
    }

    public void setEmergency_place(Geometry emergency_place) {
        this.emergency_place = emergency_place;
    }

    public String getDanger_level() {
        return danger_level;
    }

    public void setDanger_level(String danger_level) {
        this.danger_level = danger_level;
    }

    public List<Integer> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Integer> vehicles) {
        this.vehicles = vehicles;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
