package ru.advantum.csptm.service.model.xml.drivers;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

import java.util.Date;

/**
 * Created by Leonid on 24.12.2016.
 */
@Root(name = "Route")
public class Route {
    @Attribute(name = "RouteID")
    public Integer route_id;

    @Attribute(name = "PRKNU")
    public String park_id;
}
