package ru.advantum.csptm.service.model;

/**
 * Created by Leonid on 24.12.2016.
 */
public class TableEvent {
    private Integer table_event_id;
    private String event;

    public TableEvent(Integer table_event_id, String event) {
        this.table_event_id = table_event_id;
        this.event = event;
    }

    public TableEvent() {
    }

    public Integer getTable_event_id() {
        return table_event_id;
    }

    public void setTable_event_id(Integer table_event_id) {
        this.table_event_id = table_event_id;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
}
