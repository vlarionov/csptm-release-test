package ru.advantum.csptm.service.mappers;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.StatementType;
import ru.advantum.csptm.service.model.*;
import ru.advantum.csptm.service.model.xml.drivers.Change;
import ru.advantum.csptm.service.model.xml.drivers.Order;
import ru.advantum.csptm.service.model.xml.schedule.Exit;
import ru.advantum.csptm.service.model.xml.schedule.Route;
import ru.advantum.csptm.service.model.xml.schedule.Trip;
import ru.advantum.csptm.service.model.xml.schedule.TripStop;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Leonid on 23.12.2016.
 */
public interface AISSCMapper {

    //вставка нештатной ситуации в БД
    /*@Insert("insert into aissc.emergency_situation (emergency_time, emergency_type, emergency_place, danger_level, vehicles, description) " +
            "values (#{emergency_time}, #{emergency_type}, point(#{emergency_place.lon}, #{emergency_place.lat})::geometry, #{danger_level}, #{vehicles}, #{description})")*/
    @Insert("{" +
            "call aissc.ais_sc_import_pkg$import_emergency_situation(#{emergency_time}, #{emergency_type}, " +
            "#{emergency_place.lon}, #{emergency_place.lat}, #{danger_level}, #{vehicles}, #{description})" +
            "}")
    @Options(keyColumn = "emergency_situation_id", useGeneratedKeys=true, keyProperty = "emergency_situation_id", statementType = StatementType.CALLABLE)
    void insertEmergencySituation(EmergencySituation situation);

    //вставка данных о регистрации НС во внешней системе
    /*@Insert("insert into aissc.emergency_situation_external (emergency_situation_external_id, emergency_type) " +
            "values (#{emergency_situation_external_id}, #{emergency_type})")*/
    @Insert("{" +
            "call aissc.ais_sc_import_pkg$import_emergency_situation_external(#{emergency_situation_external_id}, #{emergency_type})" +
            "}")
    @Options(keyColumn = "emergency_situation_external_id", useGeneratedKeys=true, keyProperty = "emergency_situation_external_id", statementType = StatementType.CALLABLE)
    void insertExternalEmergencySituation(EmergencySituationExternal situationExternal);

    //вставка данных о типе НС
    /*@Insert("insert into aissc.emergency_type (emergency_type_id, title) values (#{emergency_type_id}, #{title}) on conflict (emergency_type_id) do " +
            "update set title = EXCLUDED.title")*/
    @Insert("{" +
            "call aissc.ais_sc_import_pkg$import_emergency_type(#{emergency_type_id}, #{title})" +
            "}")
    @Options(keyColumn = "emergency_type_id", useGeneratedKeys=true, keyProperty = "emergency_type_id", statementType = StatementType.CALLABLE)
    void insertEmergencyType(EmergencyType type);

    //выборка событий из таблицы событий
    //@Select("select * from aissc.table_event")
    @Select("select * from aissc.ais_sc_export_pkg$export_table_event()")
    @ResultType(TableEvent.class)
    List<TableEvent> selectTableEvents();

    //вставка данных о переключении маршрута
    /*@Insert("insert into aissc.route_switch (route_switch_id, user_id, vehicles, target_place_id, start_time, end_time, route_id, direction)" +
            "values (#{id}, #{user_id}, #{vehicles}, #{target_place_id}, #{start_time}, #{end_time}, #{route_id}, #{direction})")*/
    @Insert("{" +
            "call aissc.ais_sc_import_pkg$import_route_switch(#{id}, #{user_id}, #{vehicles}, #{target_place_id}, " +
            "#{start_time}, #{end_time}, #{route_id}, #{direction})" +
            "}")
    @Options(keyColumn = "route_switch_id", useGeneratedKeys=true, keyProperty = "route_switch_id", statementType = StatementType.CALLABLE)
    void insertRouteSwitch(RouteSwitch routeSwitch);

    //вставка данных об окончании переключения маршрута
    //@Select("select * from aissc.route_switch_done where id = #{id}")
    @Select("select * from aissc.ais_sc_export_pkg$export_route_switch_done(#{id})")
    @ResultType(RouteSwitchDone.class)
    RouteSwitchDone selectRouteSwitchDone(@Param("id") Long id);

    //выгрузка расписания и водителей
    //@Select("select * from aissc.route")
    @Select("select * from aissc.ais_sc_export_pkg$export_route()")
    @ResultType(Route.class)
    List<Route> selectRoutes();

    //@Select("select * from aissc.exit")
    @Select("select * from aissc.ais_sc_export_pkg$export_exit()")
    @ResultType(Exit.class)
    List<Exit> selectExits();

    //@Select("select * from aissc.trip")
    @Select("select * from aissc.ais_sc_export_pkg$export_trip()")
    @ResultType(Trip.class)
    List<Trip> selectTrips();

    //@Select("select * from aissc.trip_stop")
    @Select("select * from aissc.ais_sc_export_pkg$export_trip_stop()")
    @ResultType(TripStop.class)
    List<TripStop> selectTripStops();

    //@Select("select * from aissc.order")
    @Select("select * from aissc.ais_sc_export_pkg$export_order()")
    @ResultType(Order.class)
    List<Order> selectOrders();

    //@Select("select number from aissc.change")
    @Select("select number from aissc.ais_sc_export_pkg$export_change()")
    @ResultType(Change.class)
    List<Change> selectChanges();


    //выгрузка справочников
    //@Select("select * from aissc.ts")
    @Select("select * from aissc.ais_sc_export_pkg$export_ts()")
    @ResultType(HashMap.class)
    List<Map<String, Object>> selectTransport();

    //@Select("select * from aissc.capacity")
    @Select("select * from aissc.ais_sc_export_pkg$export_capacity()")
    @ResultType(HashMap.class)
    List<Map<String, Object>> selectCapacity();

    //@Select("select * from aissc.mark")
    @Select("select * from aissc.ais_sc_export_pkg$export_mark()")
    @ResultType(HashMap.class)
    List<Map<String, Object>> selectMark();

    //@Select("select * from aissc.model")
    @Select("select * from aissc.ais_sc_export_pkg$export_model()")
    @ResultType(HashMap.class)
    List<Map<String, Object>> selectModel();

    //@Select("select * from aissc.status")
    @Select("select * from aissc.ais_sc_export_pkg$export_status()")
    @ResultType(HashMap.class)
    List<Map<String, Object>> selectStatus();

    //@Select("select * from aissc.contragent")
    @Select("select * from aissc.ais_sc_export_pkg$export_contragent()")
    @ResultType(HashMap.class)
    List<Map<String, Object>> selectContragent();

    //@Select("select * from aissc.contragent_type")
    @Select("select * from aissc.ais_sc_export_pkg$export_contragent_type()")
    @ResultType(HashMap.class)
    List<Map<String, Object>> selectContragentType();

    //@Select("select * from aissc.territory_department")
    @Select("select * from aissc.ais_sc_export_pkg$export_territory_department()")
    @ResultType(HashMap.class)
    List<Map<String, Object>> selectTerritoryDepartment();

    //@Select("select * from aissc.employee")
    @Select("select * from aissc.ais_sc_export_pkg$export_employee()")
    @ResultType(HashMap.class)
    List<Map<String, Object>> selectEmployee();

    //@Select("select * from aissc.ts_block")
    @Select("select * from aissc.ais_sc_export_pkg$export_ts_block()")
    @ResultType(HashMap.class)
    List<Map<String, Object>> selectTsBlock();

    //@Select("select * from aissc.block")
    @Select("select * from aissc.ais_sc_export_pkg$export_block()")
    @ResultType(HashMap.class)
    List<Map<String, Object>> selectBlock();

    //@Select("select * from aissc.block_type")
    @Select("select * from aissc.ais_sc_export_pkg$export_block_type()")
    @ResultType(HashMap.class)
    List<Map<String, Object>> selectBlockType();

    //@Select("select * from aissc.route_type")
    @Select("select * from aissc.ais_sc_export_pkg$export_route_type()")
    @ResultType(HashMap.class)
    List<Map<String, Object>> selectRouteType();

    //@Select("select * from aissc.race_type")
    @Select("select * from aissc.ais_sc_export_pkg$export_race_type()")
    @ResultType(HashMap.class)
    List<Map<String, Object>> selectRaceType();

    //@Select("select * from aissc.event_type")
    @Select("select * from aissc.ais_sc_export_pkg$export_event_type()")
    @ResultType(HashMap.class)
    List<Map<String, Object>> selectEventType();
}
