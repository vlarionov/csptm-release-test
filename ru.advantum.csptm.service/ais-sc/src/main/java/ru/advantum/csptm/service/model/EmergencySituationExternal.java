package ru.advantum.csptm.service.model;

/**
 * Created by Leonid on 23.12.2016.
 */
public class EmergencySituationExternal {
    private Long emergency_situation_external_id;
    private Long emergency_type;

    public EmergencySituationExternal() {
    }

    public EmergencySituationExternal(Long emergency_situation_external_id, Long emergency_type) {
        this.emergency_situation_external_id = emergency_situation_external_id;
        this.emergency_type = emergency_type;
    }

    public Long getEmergency_situation_external_id() {
        return emergency_situation_external_id;
    }

    public void setEmergency_situation_external_id(Long emergency_situation_external_id) {
        this.emergency_situation_external_id = emergency_situation_external_id;
    }

    public Long getEmergency_type() {
        return emergency_type;
    }

    public void setEmergency_type(Long emergency_type) {
        this.emergency_type = emergency_type;
    }
}
