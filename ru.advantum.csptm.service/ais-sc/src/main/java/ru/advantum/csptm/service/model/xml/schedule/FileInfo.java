package ru.advantum.csptm.service.model.xml.schedule;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

import java.util.Date;

/**
 * Created by Leonid on 24.12.2016.
 */
@Root(name = "FileInfo")
public class FileInfo {
    @Attribute(name = "Date")
    public String date;

    @Attribute(name = "OrderDate")
    public String order_date;

    @Attribute(name = "TransportationType")
    public String transportation_type;

    @Attribute(name = "ScheduleDataId")
    public String schedule_data_id;
}
