package ru.advantum.csptm.service.events;

import org.apache.ibatis.session.SqlSession;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.service.AISSCMock;
import ru.advantum.csptm.service.SqlMapperSingleton;
import ru.advantum.csptm.service.mappers.AISSCMapper;
import ru.advantum.csptm.service.model.xml.drivers.Drivers;
import ru.advantum.csptm.service.model.xml.drivers.FileInfo;

import java.util.stream.Collectors;

/**
 * Класс отправки данных о водителях
 */
public class DriversTask extends EventTask {

    private static final Logger log = LoggerFactory.getLogger(DriversTask.class);

    /**
     * Обрабатывает событие изменение данных о водителях (update_orders), выгружает из БД данные и отправляет во внешнюю систему
     */
    @Override
    public void processEvent() {
        Drivers drivers = new Drivers();

        try (SqlSession session = SqlMapperSingleton.getInstance("ais-sc").openSession()) {
            AISSCMapper mapper = session.getMapper(AISSCMapper.class);

            drivers.routes = mapper.selectRoutes().stream().map((route -> {
                ru.advantum.csptm.service.model.xml.drivers.Route driverRoute = new ru.advantum.csptm.service.model.xml.drivers.Route();
                driverRoute.park_id = route.park_id;
                driverRoute.route_id = route.route_id;

                return driverRoute;
            })).collect(Collectors.toList());

            drivers.orders = mapper.selectOrders();
            drivers.changes = mapper.selectChanges();
            drivers.trips = mapper.selectTrips().stream().map((trip) -> {
                ru.advantum.csptm.service.model.xml.drivers.Trip driverTrip = new ru.advantum.csptm.service.model.xml.drivers.Trip();
                driverTrip.trip_id = trip.trip_id.longValue();

                return driverTrip;
            }).collect(Collectors.toList());
        } catch (Exception ex) {
            log.error("", ex);
        }

        DateTime today = new DateTime();

        drivers.fileInfo = new FileInfo();
        drivers.fileInfo.date = today.toString("yyyy-MM-dd HH:mm:ss");
        drivers.fileInfo.order_date = today.plusDays(1).toString("yyyy-MM-dd");

        try {
            AISSCMock.sendDrivers(drivers);
            log.info("sent drivers to AIS SC");
        } catch (Exception ex) {
            log.error("", ex);
        }
    }
}
