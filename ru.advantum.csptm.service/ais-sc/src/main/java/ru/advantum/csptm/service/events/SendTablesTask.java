package ru.advantum.csptm.service.events;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.service.AISSCMock;
import ru.advantum.csptm.service.SqlMapperSingleton;
import ru.advantum.csptm.service.mappers.AISSCMapper;

import java.util.List;
import java.util.Map;

/**
 * Класс обработки события изменения содержимого справочников
 */
public class SendTablesTask extends EventTask{

    private static final Logger log = LoggerFactory.getLogger(SendTablesTask.class);

    /**
     * Обрабатывает событие изменение содержимого справочников (send_tables), выгружает из БД данные и отправляет во внешнюю систему
     */
    @Override
    public void processEvent() {
        try (SqlSession session = SqlMapperSingleton.getInstance("ais-sc").openSession()) {
            AISSCMapper mapper = session.getMapper(AISSCMapper.class);

            List<Map<String, Object>> table = mapper.selectTransport();
            AISSCMock.sendTable(table);
            log.info("send transport table....");

            table = mapper.selectCapacity();
            AISSCMock.sendTable(table);
            log.info("send capacity table....");

            table = mapper.selectMark();
            AISSCMock.sendTable(table);
            log.info("send mark table....");

            table = mapper.selectModel();
            AISSCMock.sendTable(table);
            log.info("send model table....");

            table = mapper.selectStatus();
            AISSCMock.sendTable(table);
            log.info("send status table....");

            table = mapper.selectContragent();
            AISSCMock.sendTable(table);
            log.info("send contragent table....");

            table = mapper.selectContragentType();
            AISSCMock.sendTable(table);
            log.info("send contragent_type table....");

            table = mapper.selectTerritoryDepartment();
            AISSCMock.sendTable(table);
            log.info("send territory_department table....");

            table = mapper.selectEmployee();
            AISSCMock.sendTable(table);
            log.info("send employee table....");

            table = mapper.selectBlockType();
            AISSCMock.sendTable(table);
            log.info("send block_type table....");

            table = mapper.selectBlock();
            AISSCMock.sendTable(table);
            log.info("send block table....");

            table = mapper.selectTsBlock();
            AISSCMock.sendTable(table);
            log.info("send ts_block table....");

            table = mapper.selectRouteType();
            AISSCMock.sendTable(table);
            log.info("send route_type table....");

            table = mapper.selectRaceType();
            AISSCMock.sendTable(table);
            log.info("send race_type table....");

            table = mapper.selectEventType();
            AISSCMock.sendTable(table);
            log.info("send event_type table....");
        } catch (Exception ex) {
            log.error("", ex);
        }
    }
}
