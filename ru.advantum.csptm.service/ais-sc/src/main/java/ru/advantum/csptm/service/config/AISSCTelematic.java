package ru.advantum.csptm.service.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by Leonid on 24.12.2016.
 */
@Root(name = "telematic")
public class AISSCTelematic {
    @Attribute(name = "period")
    private Integer period;

    public Integer getPeriod() {
        return period;
    }
}
