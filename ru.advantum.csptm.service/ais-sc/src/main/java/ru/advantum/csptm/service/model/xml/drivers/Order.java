package ru.advantum.csptm.service.model.xml.drivers;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by Leonid on 24.12.2016.
 */
@Root(name = "Order")
public class Order {
    @Attribute(name = "OrdersID")
    public Integer order_id;

    @Attribute(name = "OrdersNum")
    public Long number;

    @Attribute(name = "DriverId")
    public Long driver_id;

    @Attribute(name = "TsId")
    public Long ts_id;

    @Attribute(name = "TsId2")
    public Long ts_id2;
}
