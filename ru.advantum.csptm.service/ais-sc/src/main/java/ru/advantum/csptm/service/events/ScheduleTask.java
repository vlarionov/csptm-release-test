package ru.advantum.csptm.service.events;

import org.apache.ibatis.session.SqlSession;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.service.AISSCMock;
import ru.advantum.csptm.service.SqlMapperSingleton;
import ru.advantum.csptm.service.mappers.AISSCMapper;
import ru.advantum.csptm.service.model.xml.schedule.FileInfo;
import ru.advantum.csptm.service.model.xml.schedule.Schedule;

import java.util.UUID;

/**
 * Класс обработки события изменения расписания
 */
public class ScheduleTask extends EventTask {
    private static final Logger log = LoggerFactory.getLogger(ScheduleTask.class);

    /**
     * Обрабатывает событие изменение данных о расписании (update_schedule), выгружает из БД данные и отправляет во внешнюю систему
     */
    @Override
    public void processEvent() {
        Schedule schedule = new Schedule();

        try (SqlSession session = SqlMapperSingleton.getInstance("ais-sc").openSession()) {
            AISSCMapper mapper = session.getMapper(AISSCMapper.class);

            schedule.routes = mapper.selectRoutes();
            schedule.exits = mapper.selectExits();
            schedule.trips = mapper.selectTrips();
            schedule.stopList = mapper.selectTripStops();
        } catch (Exception ex) {
            log.error("", ex);
        }

        DateTime today = new DateTime();

        schedule.fileInfo = new FileInfo();
        schedule.fileInfo.date = today.toString("yyyy-MM-dd HH:mm:ss");
        schedule.fileInfo.order_date = today.plusDays(1).toString("yyyy-MM-dd");
        schedule.fileInfo.transportation_type = "routed";
        schedule.fileInfo.schedule_data_id = UUID.randomUUID().toString();

        try {
            AISSCMock.sendSchedule(schedule);
            log.info("sent schedule with id {}", schedule.fileInfo.schedule_data_id);
        } catch (Exception ex) {
            log.error("", ex);
        }
    }
}
