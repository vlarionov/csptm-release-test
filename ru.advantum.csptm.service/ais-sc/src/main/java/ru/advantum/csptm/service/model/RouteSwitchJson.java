package ru.advantum.csptm.service.model;

import java.util.Date;
import java.util.List;

/**
 * Created by Leonid on 26.12.2016.
 */
public class RouteSwitchJson {
    private Long id;
    private Long user_id;
    private List<Integer> vehicles;
    private Integer target_place_id;
    private Date start_time;
    private Date end_time;
    private Integer route_id;
    private String direction;

    public RouteSwitchJson(Long id, Long user_id, List<Integer> vehicles, Integer target_place_id, Date start_time, Date end_time, Integer route_id, String direction) {
        this.id = id;
        this.user_id = user_id;
        this.vehicles = vehicles;
        this.target_place_id = target_place_id;
        this.start_time = start_time;
        this.end_time = end_time;
        this.route_id = route_id;
        this.direction = direction;
    }

    public RouteSwitchJson() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public List<Integer> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Integer> vehicles) {
        this.vehicles = vehicles;
    }

    public Integer getTarget_place_id() {
        return target_place_id;
    }

    public void setTarget_place_id(Integer target_place_id) {
        this.target_place_id = target_place_id;
    }

    public Date getStart_time() {
        return start_time;
    }

    public void setStart_time(Date start_time) {
        this.start_time = start_time;
    }

    public Date getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Date end_time) {
        this.end_time = end_time;
    }

    public Integer getRoute_id() {
        return route_id;
    }

    public void setRoute_id(Integer route_id) {
        this.route_id = route_id;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }
}
