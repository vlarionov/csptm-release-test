package ru.advantum.csptm.service.model.xml.drivers;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by Leonid on 24.12.2016.
 */
@Root(name = "Change")
public class Change {
    @Attribute(name = "ChangeNum")
    public Integer number;
}
