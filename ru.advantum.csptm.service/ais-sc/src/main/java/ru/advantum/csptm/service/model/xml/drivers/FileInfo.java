package ru.advantum.csptm.service.model.xml.drivers;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by Leonid on 24.12.2016.
 */
@Root(name = "FileInfo")
public class FileInfo {
    @Attribute(name = "Date")
    public String date;

    @Attribute(name = "OrderDate")
    public String order_date;
}
