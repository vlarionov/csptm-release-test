package ru.advantum.csptm.service;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Класс отправки телематики во внешнюю систему
 */
public class TelematicTask implements Job {

    private AISSCMock aisscMock = new AISSCMock();

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        //no telematic for you
    }
}
