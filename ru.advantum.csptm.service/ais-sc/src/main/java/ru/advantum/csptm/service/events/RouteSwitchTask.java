package ru.advantum.csptm.service.events;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.service.AISSCMock;
import ru.advantum.csptm.service.SqlMapperSingleton;
import ru.advantum.csptm.service.mappers.AISSCMapper;
import ru.advantum.csptm.service.model.RouteSwitchDone;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Класс обработки события переключения маршрутов
 */
public class RouteSwitchTask extends EventTask {

    private static final Logger log = LoggerFactory.getLogger(RouteSwitchTask.class);

    public static Map<Long, Long> routeSwitches = new ConcurrentHashMap<>();

    /**
     * Обрабатывает событие окончания переключения маршрута (switch_route),
     * выгружает из БД данные о переключении маршрута и отправляет во внешнюю систему
     */
    @Override
    public void processEvent() {
        try (SqlSession sessionInternal = SqlMapperSingleton.getInstance("ais-sc").openSession()) {
            AISSCMapper mapperInternal = sessionInternal.getMapper(AISSCMapper.class);
            List<Long> toRemove = new ArrayList<>();
            for (Long routeSwitchId : routeSwitches.values()) {
                RouteSwitchDone routeSwitchDone = mapperInternal.selectRouteSwitchDone(routeSwitchId);

                if (routeSwitchDone != null) {
                    log.info("send done route switch with id {}", routeSwitchDone.getId());
                    AISSCMock.sendRouteSwitch(routeSwitchDone);

                    toRemove.add(routeSwitchId);
                }
            }

            toRemove.forEach(id -> routeSwitches.remove(id));
        } catch (Exception ex) {
            log.error("", ex);
        }
    }
}
