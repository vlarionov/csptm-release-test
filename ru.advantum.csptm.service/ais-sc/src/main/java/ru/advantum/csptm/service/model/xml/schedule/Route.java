package ru.advantum.csptm.service.model.xml.schedule;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

import java.util.Date;

/**
 * Created by Leonid on 24.12.2016.
 */
@Root(name = "Route")
public class Route {
    @Attribute(name = "RouteID")
    public Integer route_id;

    @Attribute(name = "ParkID")
    public String park_id;

    @Attribute(name = "TransportType")
    public String transport_type;

    @Attribute(name = "ScheduleId")
    public Integer schedule_id;

    @Attribute(name = "RouteBegin")
    public String route_begin;

    @Attribute(name = "RouteEnd")
    public String route_end;

    @Attribute(name = "RouteNight")
    public Boolean night;
}
