package ru.advantum.csptm.service.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;

/**
 * Класс конфига сервис АИС СЦ
 */
@Root(name = "ais-sc")
public class AISSCConfig {
    /**
     * Порт который займет сервис АИС СЦ
     */
    @Attribute(name = "port")
    private String port;

    /**
     * Адрес который займет данный сервис
     */
    @Attribute(name = "localAddress", required = false)
    private String address = "0.0.0.0";

    /**
     * Период опроса событий из БД в минутах
     */
    @Attribute(name = "eventPeriod")
    private Integer eventPeriod;

    /**
     * Настройки соединений с БД
     */
    @Element(name = "database")
    private DatabaseConnection database;

    /**
     * Настройки внешней системы АИС СЦ
     */
    @Element(name = "ais-sc-external")
    private AISSCExternal external;
    /*@Element(name = "telematic")
    private AISSCTelematic telematic;

    public AISSCTelematic getTelematic() {
        return telematic;
    }*/

    public DatabaseConnection getDatabase() {
        return database;
    }

    public AISSCExternal getExternal() {
        return external;
    }

    public void setExternal(AISSCExternal external) {
        this.external = external;
    }

    public Integer getEventPeriod() {
        return eventPeriod;
    }

    public void setEventPeriod(Integer eventPeriod) {
        this.eventPeriod = eventPeriod;
    }

    public void setDatabase(DatabaseConnection database) {
        this.database = database;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getAddress() {
        return address;
    }
}
