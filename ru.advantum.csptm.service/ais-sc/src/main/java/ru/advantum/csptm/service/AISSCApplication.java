package ru.advantum.csptm.service;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import ru.advantum.csptm.service.providers.ObjectMapperProvider;

/**
 * Класс приложения АИС СЦ
 */
public class AISSCApplication extends ResourceConfig {
    public AISSCApplication() {
        super(ObjectMapperProvider.class, JacksonFeature.class, EntryPoint.class);
    }
}
