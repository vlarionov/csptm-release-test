package ru.advantum.csptm.service.model.xml.drivers;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Leonid on 24.12.2016.
 */
@Root(name = "Drivers")
public class Drivers {
    @Element(name = "FileInfo")
    public FileInfo fileInfo;

    @ElementList(name = "Routes")
    public List<Route> routes;

    @ElementList(name = "OrdersList")
    public List<Order> orders;

    @ElementList(name = "ChangeList")
    public List<Change> changes;

    @ElementList(name = "TripList")
    public List<Trip> trips;
}
