package ru.advantum.csptm.service.events;

import org.apache.ibatis.session.SqlSession;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.service.SqlMapperSingleton;
import ru.advantum.csptm.service.mappers.AISSCMapper;
import ru.advantum.csptm.service.model.TableEvent;

import java.util.List;
import java.util.Map;

/**
 * Класс обработки события из таблицы table_event
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public abstract class EventTask implements Job {

    private static final Logger log = LoggerFactory.getLogger(EventTask.class);

    protected JobDataMap dataMap;
    private Map<Integer, String> closedEvents;
    protected String exportEvent;

    /**
     *
     * @param context контекст запуска
     * @throws JobExecutionException
     *
     * Запускается по расписанию, получает из БД события, обрабатывает те,
     * которые предназначены для наследника этого класса
     */
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        dataMap = context.getJobDetail().getJobDataMap();
        closedEvents = (Map<Integer, String>) dataMap.get("eventsMap");
        exportEvent = (String) dataMap.get("eventName");

        List<TableEvent> events = null;
        try (SqlSession session = SqlMapperSingleton.getInstance("ais-sc").openSession()) {
            AISSCMapper mapper = session.getMapper(AISSCMapper.class);
            events = mapper.selectTableEvents();

            log.debug("got {} events from db", events.size());
        } catch (Exception ex) {
            log.error("", ex);
        }

        if (events != null) {
            events.stream().filter((tableEvent) -> !closedEvents.containsKey(tableEvent.getTable_event_id())).forEach((tableEvent) -> {
                if (tableEvent.getEvent().equals(exportEvent)) {
                    closedEvents.put(tableEvent.getTable_event_id(), tableEvent.getEvent());

                    processEvent();
                }
            });
        }
    }

    /**
     * наследником реализуется процедура обработки события
     */
    public abstract void processEvent();
}
