package ru.advantum.csptm.service;


import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.service.config.AISSCConfig;
import ru.advantum.csptm.service.events.*;
import ru.advantum.csptm.service.mappers.AISSCMapper;

import java.net.URI;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * Класс сервиса АИС СЦ, точка входа, начальные настройки, поднятие сервера и т.д.
 */
public class AISSCService {
    public static void main(String[] args) throws Exception {
        AISSCConfig config = Configuration.unpackConfig(AISSCConfig.class, "ais-sc.xml");

        AISSCMock.url = config.getExternal().getIp() + ":" + config.getExternal().getPort() + config.getExternal().getRest();
        AISSCMock.timeout = config.getExternal().getTimeout();

        SqlMapperSingleton.registerInstance(
                "ais-sc",
                new Environment(
                        "ais-sc",
                        new JdbcTransactionFactory(),
                        config.getDatabase().getConnectionPool()),
                factory -> {
                    factory.getConfiguration().addMapper(AISSCMapper.class);
                    //factory.getConfiguration().getTypeHandlerRegistry().register(ArrayIntegerTypeHandler.class);
                });

        final HttpServer server = GrizzlyHttpServerFactory.createHttpServer(
                URI.create("http://" + config.getAddress() + ":" + config.getPort()),
                new AISSCApplication(),
                false);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> server.shutdownNow()));

        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

        Map<Integer, String> eventsMap = new ConcurrentHashMap<>();

        JobDataMap sendTablesData = new JobDataMap();
        sendTablesData.put("eventsMap", eventsMap);
        sendTablesData.put("eventName", "send_tables");

        JobDetail sendTablesJob = newJob(SendTablesTask.class).withIdentity("sendTables", "group1").
                usingJobData(sendTablesData).build();
        SimpleTrigger sendTablesTrigger = newTrigger()
                .withIdentity("sendTablesTrigger", "group1")
                .startNow()
                .withSchedule(simpleSchedule()
                        //.withIntervalInHours(24).repeatForever())
                        .withIntervalInMinutes(config.getEventPeriod()).repeatForever())
                .build();

        JobDataMap routeSwitchData = new JobDataMap();
        routeSwitchData.put("eventsMap", eventsMap);
        routeSwitchData.put("eventName", "switch_route");

        JobDetail routeSwitchJob = newJob(RouteSwitchTask.class).withIdentity("routeSwitch", "group1").
                usingJobData(routeSwitchData).build();
        SimpleTrigger routeSwitchTrigger = newTrigger()
                .withIdentity("routeSwitchTrigger", "group1")
                .startNow()
                .withSchedule(simpleSchedule()
                        //.withIntervalInHours(24).repeatForever())
                        .withIntervalInMinutes(config.getEventPeriod()).repeatForever())
                .build();

        JobDataMap scheduleData = new JobDataMap();
        scheduleData.put("eventsMap", eventsMap);
        scheduleData.put("eventName", "update_schedule");

        JobDetail scheduleJob = newJob(ScheduleTask.class).withIdentity("schedule", "group1").
                usingJobData(scheduleData).build();
        SimpleTrigger scheduleTrigger = newTrigger()
                .withIdentity("scheduleTrigger", "group1")
                .startNow()
                .withSchedule(simpleSchedule()
                        //.withIntervalInHours(24).repeatForever())
                        .withIntervalInMinutes(config.getEventPeriod()).repeatForever())
                .build();

        JobDataMap driversData = new JobDataMap();
        driversData.put("eventsMap", eventsMap);
        driversData.put("eventName", "update_orders");

        JobDetail driversJob = newJob(DriversTask.class).withIdentity("drivers", "group1").
                usingJobData(driversData).build();
        SimpleTrigger driversTrigger = newTrigger()
                .withIdentity("driversTrigger", "group1")
                .startNow()
                .withSchedule(simpleSchedule()
                        //.withIntervalInHours(24).repeatForever())
                        .withIntervalInMinutes(config.getEventPeriod()).repeatForever())
                .build();

        JobDetail planScheduleJob = newJob(PlanScheduleTask.class).withIdentity("planSchedule", "group1").build();
        SimpleTrigger planScheduleTrigger = newTrigger()
                .withIdentity("planScheduleTrigger", "group1")
                .startNow()
                .withSchedule(simpleSchedule()
                        //.withIntervalInHours(24).repeatForever())
                        .withIntervalInMinutes(15).repeatForever())
                .build();

        try {
            scheduler.scheduleJob(planScheduleJob, planScheduleTrigger);
            scheduler.scheduleJob(driversJob, driversTrigger);
            scheduler.scheduleJob(scheduleJob, scheduleTrigger);
            scheduler.scheduleJob(sendTablesJob, sendTablesTrigger);
            scheduler.scheduleJob(routeSwitchJob, routeSwitchTrigger);
            scheduler.start();

            SLF4JBridgeHandler.removeHandlersForRootLogger();
            SLF4JBridgeHandler.install();
            server.start();

            Thread.currentThread().join();
        } finally {
            server.shutdownNow();
        }
    }
}
