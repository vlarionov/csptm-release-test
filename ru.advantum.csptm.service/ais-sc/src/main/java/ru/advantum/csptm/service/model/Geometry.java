package ru.advantum.csptm.service.model;

/**
 * Created by Leonid on 23.12.2016.
 */
public class Geometry {
    private Double lat;
    private Double lon;

    public Geometry() {
    }

    public Geometry(Double lat, Double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }
}
