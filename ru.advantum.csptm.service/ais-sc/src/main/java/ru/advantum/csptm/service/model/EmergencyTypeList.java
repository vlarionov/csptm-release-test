package ru.advantum.csptm.service.model;

import java.util.List;

/**
 * Created by Leonid on 24.12.2016.
 */
public class EmergencyTypeList {
    private List<EmergencyType> list;

    public EmergencyTypeList(List<EmergencyType> list) {
        this.list = list;
    }

    public EmergencyTypeList() {
    }

    public List<EmergencyType> getList() {
        return list;
    }

    public void setList(List<EmergencyType> list) {
        this.list = list;
    }
}
