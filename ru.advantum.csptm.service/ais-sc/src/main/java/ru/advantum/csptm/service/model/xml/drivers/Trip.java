package ru.advantum.csptm.service.model.xml.drivers;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by Leonid on 24.12.2016.
 */
@Root(name = "Trip")
public class Trip {
    @Attribute(name = "TripId")
    public Long trip_id;
}
