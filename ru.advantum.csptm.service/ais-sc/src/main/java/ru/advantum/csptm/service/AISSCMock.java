package ru.advantum.csptm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.load.Persister;
import ru.advantum.csptm.service.model.EmergencySituation;
import ru.advantum.csptm.service.model.EmergencySituationExternal;
import ru.advantum.csptm.service.model.EmergencySituationJson;
import ru.advantum.csptm.service.model.RouteSwitchDone;
import ru.advantum.csptm.service.model.xml.drivers.Drivers;
import ru.advantum.csptm.service.model.xml.schedule.Schedule;

import javax.net.ssl.HttpsURLConnection;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Класс для работы с внешней системой АИС СЦ
 */
public class AISSCMock {

    private static ObjectMapper objectMapper = new ObjectMapper();

    public static String requestType = "POST";
    public static String url;
    public static int timeout = 3000;
    public static String userAgent = "Chrome/54.0.2840.71";

    public class AISSCMockException extends Exception {
        public AISSCMockException() {
            super();
        }

        public AISSCMockException(String message) {
            super(message);
        }
    }

    /**
     *
     * @param situation нештатная ситуация
     * @return данные о регистрации НС во внешней системе
     * @throws Exception
     */
    public static EmergencySituationExternal registerEmergencySituation(EmergencySituationJson situation) throws Exception {
        HttpURLConnection connection = openConnection();
        connection.setRequestProperty("Content-Type", "application/json");
        try (DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream())) {
            objectMapper.writeValue(outputStream, situation);

            outputStream.flush();
        }

        closeConnection(connection, "Can't send emergency situation: ");

        //пока внешней системы нет, создаем рандомные значения
        Random random = new Random();
        return new EmergencySituationExternal(random.nextLong(), random.nextLong()%10);
    }

    /**
     * no telematic for you
     */
    public static void sendTelematic() {}

    /**
     *
     * @param table справочник
     * @throws Exception
     *
     * Отправка справочников в АИС СЦ
     */
    public static void sendTable(List<Map<String, Object>> table) throws Exception {
        HttpURLConnection connection = openConnection();
        connection.setRequestProperty("Content-Type", "application/json");
        try (DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream())) {
            objectMapper.writeValue(outputStream, table);

            outputStream.flush();
        }

        closeConnection(connection, "Can't send tables: ");
    }

    /**
     *
     * @param routeSwitch данные о переключении маршрута
     * @throws Exception
     *
     * Отправка данных о переключении маршрута
     */
    public static void sendRouteSwitch(RouteSwitchDone routeSwitch) throws Exception {
        HttpURLConnection connection = openConnection();
        connection.setRequestProperty("Content-Type", "application/json");
        try (DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream())) {
            objectMapper.writeValue(outputStream, routeSwitch);

            outputStream.flush();
        }

        closeConnection(connection, "Can't send route switch: ");
    }

    /**
     *
     * @param schedule данные о расписании
     * @throws Exception
     *
     * Отправка данных о расписании в АИС СЦ
     */
    public static void sendSchedule(Schedule schedule) throws Exception {
        Serializer serializer = new Persister();
        HttpURLConnection connection = openConnection();
        connection.setRequestProperty("Content-Type", "application/xml");
        try (DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream())) {
            serializer.write(schedule, outputStream);

            outputStream.flush();
        }

        closeConnection(connection, "Cant' send schedule: ");
    }

    /**
     *
     * @param drivers данные о водителях
     * @throws Exception
     *
     * Отправка данных о водителях в АИС СЦ
     */
    public static void sendDrivers(Drivers drivers) throws Exception {
        Serializer serializer = new Persister();
        HttpURLConnection connection = openConnection();
        connection.setRequestProperty("Content-Type", "application/xml");
        try (DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream())) {
            serializer.write(drivers, outputStream);

            outputStream.flush();
        }

        closeConnection(connection, "Cant' send schedule: ");
    }

    /**
     *
     * @return Объект соединения с АИС СЦ
     * @throws Exception
     *
     * Открытие соединения с внешней системой
     */
    private static HttpURLConnection openConnection() throws Exception {
        URL url = new URL(AISSCMock.url);
        HttpURLConnection connection;
        if (url.getProtocol().equals("https")) {
            connection = (HttpsURLConnection) url.openConnection();
        } else {
            connection = (HttpURLConnection) url.openConnection();
        }

        connection.setRequestMethod(requestType);
        connection.setRequestProperty("User-Agent", userAgent);
        connection.setConnectTimeout(timeout);

        connection.setDoOutput(true);

        return connection;
    }

    /**
     *
     * @param connection объект соединения с АИС СЦ
     * @param errorMessage строка с ошибкой в случае невозможности закрытия соединения
     * @throws Exception
     *
     * Метод пытается закрыть соединение с внешней системой
     */
    private static void closeConnection(HttpURLConnection connection, String errorMessage) throws Exception {
        if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
            connection.disconnect();
            throw new Exception(errorMessage + connection.getResponseCode());
        }

        connection.disconnect();
    }
}
