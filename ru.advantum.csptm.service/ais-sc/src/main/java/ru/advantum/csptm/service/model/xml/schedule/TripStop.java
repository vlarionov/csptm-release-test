package ru.advantum.csptm.service.model.xml.schedule;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by Leonid on 24.12.2016.
 */
@Root(name = "TripStop")
public class TripStop {
    @Attribute(name = "TripStopNum")
    public Integer number;

    @Attribute(name = "TripStopTypeId")
    public Integer type_id;

    @Attribute(name = "TripStopId")
    public Integer trip_stop_id;

    @Attribute(name = "TripStopTime")
    public String time;
}
