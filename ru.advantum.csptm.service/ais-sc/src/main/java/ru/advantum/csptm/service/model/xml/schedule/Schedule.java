package ru.advantum.csptm.service.model.xml.schedule;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Leonid on 24.12.2016.
 */
@Root(name = "Schedule")
public class Schedule {
    @Element(name = "FileInfo")
    public FileInfo fileInfo;

    @ElementList(name = "Routes")
    public List<Route> routes;

    @ElementList(name = "ExitsList")
    public List<Exit> exits;

    @ElementList(name = "TripList")
    public List<Trip> trips;

    @ElementList(name = "TripStopList")
    public List<TripStop> stopList;
}
