package ru.advantum.csptm.service.model;

/**
 * Created by Leonid on 26.12.2016.
 */
public class RouteSwitchId {
    private Long id;

    public RouteSwitchId(Long id) {
        this.id = id;
    }

    public RouteSwitchId() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
