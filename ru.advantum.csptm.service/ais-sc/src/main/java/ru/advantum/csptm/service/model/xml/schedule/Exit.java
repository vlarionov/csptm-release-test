package ru.advantum.csptm.service.model.xml.schedule;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by Leonid on 24.12.2016.
 */
@Root(name = "Exit")
public class Exit {
    @Attribute(name = "ExitID")
    public Long exit_id;

    @Attribute(name = "ExitNum")
    public Long number;

    @Attribute(name = "ExitTypeTS")
    public Long type_ts;

    @Attribute(name = "GarageOut")
    public String garage_out;

    @Attribute(name = "LineBegin")
    public String line_begin;

    @Attribute(name = "LineEnd")
    public String line_end;

    @Attribute(name = "GarageIn")
    public String garage_in;

    @Attribute(name = "OrderTime")
    public String order_time;

    @Attribute(name = "LineTime")
    public String line_time;

    @Attribute(name = "NullTime")
    public String null_time;

    @Attribute(name = "LineRun")
    public Integer line_run;

    @Attribute(name = "NullRun")
    public Integer null_run;
}
