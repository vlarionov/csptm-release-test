package ru.advantum.csptm.service;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.service.events.RouteSwitchTask;
import ru.advantum.csptm.service.mappers.AISSCMapper;
import ru.advantum.csptm.service.model.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Класс для доступа до сервиса по протоколу http
 */
@Path("/mgt")
public class EntryPoint {

    private static final Logger log = LoggerFactory.getLogger(EntryPoint.class);

    /**
     *
     * @param situation данные о нештатной ситуации
     * @return Ответ о результатах регистрации НС, или 200 ОК, или 400 Bad Request в случае ошибки
     *
     * Пытается зарегистрировать НС локально и во внешней системе АИС СЦ
     */
    @POST
    @Path("es/registration")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response registerEmergencySituation(EmergencySituationJson situation) {
        try (SqlSession session = SqlMapperSingleton.getInstance("ais-sc").openSession()) {
            log.info("register new emergency situation {}", situation.toString());
            AISSCMapper mapper = session.getMapper(AISSCMapper.class);
            EmergencySituation es = EmergencySituation.create(situation);
            mapper.insertEmergencySituation(es);

            session.commit();

            log.info("trying send emergency situation to ais sc.....");
            situation.setEmergency_situation_id(es.getEmergency_situation_id());
            EmergencySituationExternal esx = AISSCMock.registerEmergencySituation(situation);
            log.info("emergency situation has sent");

            log.info("save ais sc emergency situation entity");
            mapper.insertExternalEmergencySituation(esx);

            session.commit();
        } catch (Exception ex) {
            log.error("", ex);

            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        return Response.ok().build();
    }

    /**
     *
     * @param list Классификатор нештатных ситуаций
     * @return Ответ о принятии классификатора 200 ОК или 400 Bad Request
     *
     * Метод пытается сохранить локально в БД классификатор нештатных ситуаций
     */
    @POST
    @Path("es/type")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response receiveEmergencyTypeList(EmergencyTypeList list) {
        try (SqlSession session = SqlMapperSingleton.getInstance("ais-sc").openSession()) {
            AISSCMapper mapper = session.getMapper(AISSCMapper.class);
            for (EmergencyType type : list.getList()) {
                mapper.insertEmergencyType(type);
                session.commit();
                log.info("receive emergency type list {}", list.getList().size());
            }
        } catch (Exception ex) {
            log.error("", ex);

            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        return Response.ok().build();
    }

    /**
     *
     * @param routeSwitch данные о переключении маршрутов
     * @return идентификатор записи о переключении маршрутов зарегистрированный в сервисе
     *
     * Регистрирует переключение маршрута в сервисе и возвращает идентификатор созданной записи
     */
    @POST
    @Path("switch/route")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public RouteSwitchId receiveRouteSwitch(RouteSwitchJson routeSwitch) {
        try (SqlSession session = SqlMapperSingleton.getInstance("ais-sc").openSession()) {
            AISSCMapper mapper = session.getMapper(AISSCMapper.class);

            RouteSwitch rs = RouteSwitch.create(routeSwitch);
            mapper.insertRouteSwitch(rs);
            session.commit();

            log.info("save route switch with id {}", routeSwitch.getId());

            //???
            RouteSwitchTask.routeSwitches.put(routeSwitch.getId(), routeSwitch.getId());
        } catch (Exception ex) {
            log.error("", ex);
        }

        return new RouteSwitchId(routeSwitch.getId());
    }
}
