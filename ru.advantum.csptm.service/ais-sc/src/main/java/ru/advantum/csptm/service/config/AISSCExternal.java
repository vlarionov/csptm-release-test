package ru.advantum.csptm.service.config;

import org.simpleframework.xml.Attribute;

/**
 * Класс настроек внешней системы АИС СЦ
 */
public class AISSCExternal {
    /**
     * ip адрес
     */
    @Attribute(name = "ip")
    private String ip;

    /**
     * порт
     */
    @Attribute(name = "port")
    private String port;

    /**
     * таймаут соединения
     */
    @Attribute(name = "timeout")
    private Integer timeout;

    /**
     * логин аутентификации
     */
    @Attribute(name = "login")
    private String login;

    /**
     * пароль аутентификации
     */
    @Attribute(name = "password")
    private String password;

    /**
     * rest url, после порта, может быть пустым
     */
    @Attribute(name = "rest")
    private String rest;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRest() {
        return rest;
    }

    public void setRest(String rest) {
        this.rest = rest;
    }
}
