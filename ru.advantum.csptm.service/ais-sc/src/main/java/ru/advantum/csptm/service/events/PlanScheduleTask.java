package ru.advantum.csptm.service.events;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Класс отправки планового расписания
 */
public class PlanScheduleTask implements Job {

    /**
     *
     * @param context контекст запуска
     * @throws JobExecutionException
     *
     * Процесс выгрузки планового расписания, запускается по расписанию
     */
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        new ScheduleTask().processEvent();
    }
}
