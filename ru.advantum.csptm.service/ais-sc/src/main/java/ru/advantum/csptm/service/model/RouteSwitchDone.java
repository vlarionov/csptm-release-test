package ru.advantum.csptm.service.model;

import java.util.Date;

/**
 * Created by Leonid on 24.12.2016.
 */
public class RouteSwitchDone {
    private Long id;
    private Date time;
    private String description;
    private Boolean flag;

    public RouteSwitchDone() {
    }

    public RouteSwitchDone(Long id, Date time, String description, Boolean flag) {
        this.id = id;
        this.time = time;
        this.description = description;
        this.flag = flag;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }
}
