package ru.advantum.csptm.service.model;

/**
 * Created by Leonid on 24.12.2016.
 */
public class EmergencyType {
    private Integer emergency_type_id;
    private String title;

    public EmergencyType() {
    }

    public EmergencyType(Integer emergency_type_id, String title) {
        this.emergency_type_id = emergency_type_id;
        this.title = title;
    }

    public Integer getEmergency_type_id() {
        return emergency_type_id;
    }

    public void setEmergency_type_id(Integer emergency_type_id) {
        this.emergency_type_id = emergency_type_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
