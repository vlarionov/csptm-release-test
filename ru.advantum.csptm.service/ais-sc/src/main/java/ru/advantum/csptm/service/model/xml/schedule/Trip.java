package ru.advantum.csptm.service.model.xml.schedule;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by Leonid on 24.12.2016.
 */
@Root(name = "Trip")
public class Trip {
    @Attribute(name = "TripId")
    public Integer trip_id;

    @Attribute(name = "TripNum")
    public Integer number;

    @Attribute(name = "TripScheduleNum")
    public Integer schedule_number;

    @Attribute(name = "TripKind")
    public Integer kind;

    @Attribute(name = "TripType")
    public String type;

    @Attribute(name = "TripDirection")
    public String direction;

    @Attribute(name = "Begin")
    public String trip_begin;

    @Attribute(name = "End")
    public String trip_end;

    @Attribute(name = "TripTime")
    public String time;

    @Attribute(name = "TripRun")
    public Integer run;

    @Attribute(name = "DepartureType")
    public String departure_type;

    @Attribute(name = "DepartureID")
    public Integer departure_id;

    @Attribute(name = "ArrivalType")
    public String arrival_type;

    @Attribute(name = "ArrivalID")
    public Integer arrival_id;
}
