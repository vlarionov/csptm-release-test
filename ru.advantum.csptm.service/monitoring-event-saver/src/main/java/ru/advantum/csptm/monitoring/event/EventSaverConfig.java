package ru.advantum.csptm.monitoring.event;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.csptm.artifact.csvsaver.config.CsvSaverConfig;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

@Root(name = "monitoring-event-saver")
public class EventSaverConfig {
    @Element(name = "rmq-service")
    private JepRmqServiceConfig rmqConfig;

    @Element(name = "csv-saver-event")
    private CsvSaverConfig csvEventSaverConfig;

    @Element(name = "csv-saver-alarm")
    private CsvSaverConfig csvAlarmSaverConfig;

    public JepRmqServiceConfig getRmqConfig() {
        return rmqConfig;
    }

    public CsvSaverConfig getCsvEventSaverConfig() {
        return csvEventSaverConfig;
    }

    public CsvSaverConfig getCsvAlarmSaverConfig() {
        return csvAlarmSaverConfig;
    }
}