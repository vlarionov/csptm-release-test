package ru.advantum.csptm.monitoring.event;

import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.artifact.csvsaver.CsvSaver;
import ru.advantum.csptm.artifact.csvsaver.apache.print.ApacheRecordPrinter;
import ru.advantum.csptm.artifact.csvsaver.limiters.Limiter;
import ru.advantum.csptm.monitoring.model.Alarm;
import ru.advantum.csptm.monitoring.model.Event;
import ru.advantum.csptm.monitoring.model.EventMessageType;
import ru.advantum.rabbitmq.consumer.RmqJsonTalker;

import java.io.IOException;
import java.util.concurrent.TimeoutException;


public class EventSaver {
    private static final Logger LOGGER = LoggerFactory.getLogger(EventSaver.class);

    private static final String CONFIG_FILE_NAME = "monitoring-event-saver.xml";

    private static final String[] CSV_HEADERS_EVENT = {
            "event_id"
            ,"event_time"
            ,"tr_id"
            ,"unit_id"
            ,"event_type_id"
            ,"fact_value"
            ,"deviation_value"
            ,"lon"
            ,"lat"
            ,"event_status_id"
    };

    private static final String[] CSV_HEADERS_ALARM = {
            "event_id"
            ,"event_time"
    };

    private final EventSaverConfig config;

    private volatile Limiter eventLimiter;
    private volatile Limiter alarmLimiter;

    private volatile RmqJsonTalker rmqJsonTalker;

    public EventSaver(EventSaverConfig config) {
        this.config = config;
    }

    public static void main(String[] args) throws Exception {
        try {
            EventSaverConfig config = Configuration.unpackConfig(
                    EventSaverConfig.class,
                    CONFIG_FILE_NAME
            );
            new EventSaver(config).start();
        } catch (Exception e) {
            LOGGER.error("Error on startup", e);
        }
    }

    private void start() throws Exception {
        initWriter();
        initConsumer();
        initShutdownHook();

        LOGGER.info("EventSaver started");
    }

    private void initWriter() throws Exception {
        eventLimiter = CsvSaver.createLimiter(
                config.getCsvEventSaverConfig(),
                CSV_HEADERS_EVENT,
                Event.class,
                new EventRecord()
        );

        alarmLimiter = CsvSaver.createLimiter(
                config.getCsvAlarmSaverConfig(),
                CSV_HEADERS_ALARM,
                Alarm.class,
                new AlarmRecord()
        );
    }

    private void initConsumer() throws IOException, TimeoutException {
        RmqJsonTalker.Builder rmqBuilder = RmqJsonTalker.newBuilder();

        rmqBuilder.setConsumerConfig(
                true,
                true,
                config.getRmqConfig().consumer
        );

        rmqBuilder.addConsumer(
                EventMessageType.EVENT.name(),
                Event.class,
                (tag, envelope, basicProperties, message) -> consumeEvent(message)
        );

        rmqBuilder.addConsumer(
                EventMessageType.ALARM.name(),
                Alarm.class,
                (tag, envelope, basicProperties, message) -> consumeAlarm(message)
        );

        rmqJsonTalker = rmqBuilder.connect(
                config.getRmqConfig().connection.newConnection()
        );
    }

    private void initShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                rmqJsonTalker.cancelConsumer();
            } catch (IOException e) {
                LOGGER.error("Error on cancel consumer", e);
            }
            try {
                rmqJsonTalker.close();
            } catch (IOException | TimeoutException e) {
                LOGGER.error("Error on close talker", e);
            }
            LOGGER.info("RmqJsonTalker stopped");

            try {
                eventLimiter.close();
            } catch (Exception e) {
                LOGGER.error("Error on close eventLimiter", e);
            }
            try {
                alarmLimiter.close();
            } catch (Exception e) {
                LOGGER.error("Error on close alarmLimiter", e);
            }
            LOGGER.info("Limiters stopped");
        }));
    }

    private void consumeEvent(Event message) {
            try {
                eventLimiter.append(message);
            } catch (Exception e) {
                LOGGER.error("event write error", e);
            }
    }

    private void consumeAlarm(Alarm message) {
        try {
            alarmLimiter.append(message);
        } catch (Exception e) {
            LOGGER.error("alarm write error", e);
        }
    }

    private static class EventRecord implements ApacheRecordPrinter<Event> {
        @Override
        public void printRecord(Event event, CSVPrinter csvPrinter) throws Exception {
                csvPrinter.printRecord(
                        event.getEventId()
                        ,event.getEventTime()
                        ,event.getTrId()
                        ,event.getUnitId()
                        ,event.getEventType()
                        ,event.getFactValue()
                        ,event.getDeviationValue()
                        ,event.getLon()
                        ,event.getLat()
                        ,event.getEventStatusId()
                );
            }

    }

    private static class AlarmRecord implements ApacheRecordPrinter<Alarm> {
        @Override
        public void printRecord(Alarm alarm, CSVPrinter csvPrinter) throws Exception {
            csvPrinter.printRecord(
                    alarm.getEventId()
                    ,alarm.getEventTime()
            );
        }

    }
}