package ru.advantum.csptm.service.eventmanager.config;

import com.google.common.cache.CacheBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.service.eventmanager.EventManagerConfig;

import java.util.concurrent.TimeUnit;

@EnableCaching
@Configuration
public class CacheConfig extends CachingConfigurerSupport {

    private final EventManagerConfig eventManagerConfig;

    @Autowired
    public CacheConfig(EventManagerConfig eventManagerConfig) {
        this.eventManagerConfig = eventManagerConfig;
    }

    @Override
    public CacheManager cacheManager() {
        GuavaCacheManager gcm = new GuavaCacheManager();

        gcm.setCacheBuilder(CacheBuilder.newBuilder().expireAfterWrite(eventManagerConfig.cacheDuration, TimeUnit.MILLISECONDS));

        return gcm;
    }
}
