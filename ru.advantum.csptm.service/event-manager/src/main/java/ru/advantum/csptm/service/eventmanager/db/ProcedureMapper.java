package ru.advantum.csptm.service.eventmanager.db;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.mapping.StatementType;

import java.util.Map;

@Mapper
public interface ProcedureMapper {
    @Update("{call ${name}(#{params})}")
    @Options(statementType = StatementType.CALLABLE, useCache = false)
    void callProcedure(@Param("name") String name, @Param("params") String params);

    @Update("{call ${name}()}")
    @Options(statementType = StatementType.CALLABLE, useCache = false)
    void callProcedureWithoutParams(@Param("name") String name);

}
