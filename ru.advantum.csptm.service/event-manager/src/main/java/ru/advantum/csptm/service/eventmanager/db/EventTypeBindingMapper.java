package ru.advantum.csptm.service.eventmanager.db;

import org.apache.ibatis.annotations.Arg;
import org.apache.ibatis.annotations.ConstructorArgs;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import ru.advantum.csptm.service.eventmanager.model.EventTypeBinding;

import java.util.List;

@Mapper
public interface EventTypeBindingMapper {
    @Select("select * from core.event_manager_pkg$get_event_type_binding()")
    @ConstructorArgs({
            @Arg(column = "id", javaType = Long.class),
            @Arg(column = "event_type", javaType = String.class),
            @Arg(column = "procedure_name", javaType = String.class)
    })
    List<EventTypeBinding> findAll();
}