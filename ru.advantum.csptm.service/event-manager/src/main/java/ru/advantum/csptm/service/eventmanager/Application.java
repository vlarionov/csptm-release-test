package ru.advantum.csptm.service.eventmanager;

import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;
import ru.advantum.csptm.spring.rmq.ConsumerProperties;

import java.util.concurrent.TimeUnit;

@SpringBootApplication
public class Application {

    public static final String CONFIG_FILE_NAME = "event-manager.xml";

    @Bean
    public JepRmqServiceConfig config(EventManagerConfig config) {
        return config.rmqServiceConfig;
    }

    @Bean
    public ConsumerProperties consumerProperties() {
        return ConsumerProperties.newBuilder()
                .setAcknowledgeMode(AcknowledgeMode.AUTO)
                .setShutdownTimeout(TimeUnit.SECONDS.toMillis(10))
                .build();
    }

    @Bean
    public static EventManagerConfig applicationConfig() throws Exception {
        return ru.advantum.config.Configuration.unpackConfig(
                EventManagerConfig.class,
                CONFIG_FILE_NAME
        );
    }

    public static void main(String[] args) {
        new SpringApplicationBuilder(Application.class)
                .bannerMode(Banner.Mode.OFF)
                .web(false)
                .run(args);
    }

}
