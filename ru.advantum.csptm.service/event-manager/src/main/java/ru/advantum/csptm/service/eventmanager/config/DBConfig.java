package ru.advantum.csptm.service.eventmanager.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.service.eventmanager.EventManagerConfig;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
public class DBConfig {
    @Bean
    public DataSource dataSource(EventManagerConfig config) throws SQLException {
        return config.databaseConfig.getConnectionPool();
    }

}
