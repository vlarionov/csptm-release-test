package ru.advantum.csptm.service.eventmanager.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.service.eventmanager.model.EventTypeBinding;

import java.util.Map;
import java.util.stream.Collectors;

@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class EventTypeBindingDao {
    private final EventTypeBindingMapper eventTypeBindingMapper;

    @Autowired
    public EventTypeBindingDao(EventTypeBindingMapper eventTypeBindingMapper) {
        this.eventTypeBindingMapper = eventTypeBindingMapper;
    }

    /**
     * @return eventType to procedureName map
     */
    @Cacheable("bindingMap")
    public Map<String, String> getBindingMap() {
        return eventTypeBindingMapper.findAll()
                .stream()
                .collect(Collectors.toMap(
                        EventTypeBinding::getEventType,
                        EventTypeBinding::getProcedureName
                ));
    }

}
