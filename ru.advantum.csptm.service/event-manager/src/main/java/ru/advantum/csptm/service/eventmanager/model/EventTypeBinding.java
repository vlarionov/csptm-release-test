package ru.advantum.csptm.service.eventmanager.model;

public class EventTypeBinding {
    private final Long id;

    private final String eventType;

    private final String procedureName;

    public EventTypeBinding(Long id, String eventType, String procedureName) {
        this.id = id;
        this.eventType = eventType;
        this.procedureName = procedureName;
    }

    public Long getId() {
        return id;
    }

    public String getEventType() {
        return eventType;
    }

    public String getProcedureName() {
        return procedureName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EventTypeBinding binding = (EventTypeBinding) o;

        if (id != null ? !id.equals(binding.id) : binding.id != null) return false;
        if (eventType != null ? !eventType.equals(binding.eventType) : binding.eventType != null) return false;
        return procedureName != null ? procedureName.equals(binding.procedureName) : binding.procedureName == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (eventType != null ? eventType.hashCode() : 0);
        result = 31 * result + (procedureName != null ? procedureName.hashCode() : 0);
        return result;
    }
}
