package ru.advantum.csptm.service.eventmanager.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.service.eventmanager.db.EventTypeBindingDao;
import ru.advantum.csptm.service.eventmanager.db.ProcedureMapper;

@Service
public class EventManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(EventManager.class);

    private final EventTypeBindingDao eventTypeBindingDao;
    private final ProcedureMapper procedureMapper;

    @Autowired
    public EventManager(
            EventTypeBindingDao eventTypeBindingDao,
            ProcedureMapper procedureMapper
    ) {
        this.eventTypeBindingDao = eventTypeBindingDao;
        this.procedureMapper = procedureMapper;
    }

    @RabbitListener
    public void receiveMessage(@Header("amqp_type") String type, String eventsParams) {
        try {
            String procedure = findProcedureByEventType(type);

            if (procedure == null) {
                LOGGER.error("Can not find procedure");
                return;
            }

            LOGGER.info("call {}", procedure);
            if (eventsParams == null || eventsParams.isEmpty()) {
                procedureMapper.callProcedureWithoutParams(procedure);
            } else {
                procedureMapper.callProcedure(procedure, eventsParams);
            }
        } catch (Exception e) {
            LOGGER.error("Error on event. Type: {}. Params: {}.", type, eventsParams);
            throw e;
        }
    }

    private String findProcedureByEventType(String eventType) {
        return eventTypeBindingDao.getBindingMap().get(eventType);
    }

}
