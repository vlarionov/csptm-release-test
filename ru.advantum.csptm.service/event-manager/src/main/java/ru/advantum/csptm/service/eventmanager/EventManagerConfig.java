package ru.advantum.csptm.service.eventmanager;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

@Root(name = "event-manager")
public class EventManagerConfig {
    @Element(name = "rmq-service")
    public JepRmqServiceConfig rmqServiceConfig;

    @Element(name = "database")
    public DatabaseConnection databaseConfig;

    @Attribute
    public long cacheDuration;

}
