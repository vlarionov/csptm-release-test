package ru.advantum.csptm.service.incident.ribbon.db;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.StatementType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.advantum.csptm.incident.model.IncidentType;

/**
 * Created by kukushkin on 11/9/17.
 */
@Repository
public class IncidentTypeDao {
    private final IncidentTypeDao.IncidentTypeMapper mapper;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public IncidentTypeDao(IncidentTypeDao.IncidentTypeMapper mapper, JdbcTemplate jdbcTemplate) {
        this.mapper = mapper;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Cacheable("incident_type")
    public IncidentType getById(Short id)
    {
        return mapper.getById(id);
    }

    @Mapper
    private interface IncidentTypeMapper {
        @Select("SELECT " +
                "incident_type_id as id, " +
                "incident_type_name as name, " +
                "incident_type_short_name as shortName, " +
                "is_alarm as isAlarm " +
                "FROM ttb.incident_type " +
                "where incident_type_id = #{incidentTypeId}"
        )
        @Options(useCache = false, statementType = StatementType.CALLABLE)
        IncidentType getById(Short incidentTypeId);
    }
}