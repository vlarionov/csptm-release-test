package ru.advantum.csptm.service.incident.ribbon.db;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.StatementType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.advantum.csptm.incident.model.TtVariantCurrent;

/**
 * Created by kukushkin on 2/15/18.
 */



/**
 * Created by kukushkin on 11/9/17.
 */
@Repository
public class TtVariantCurrentDao {
    private final TtVariantCurrentDao.TtVariantCurrentMapper mapper;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public TtVariantCurrentDao(TtVariantCurrentDao.TtVariantCurrentMapper mapper, JdbcTemplate jdbcTemplate) {
        this.mapper = mapper;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Cacheable("tt_variant_current")
    public TtVariantCurrent getByTrIdOrDefault(Long trId, TtVariantCurrent def)
    {
        if (trId == null) {
            return def;
        }

        final TtVariantCurrent ttVariantCurrent = mapper.getByTrId(trId);
        return (ttVariantCurrent == null ? def : ttVariantCurrent);

    }

    @Cacheable("tt_variant_current_by_ttvariantid")
    public TtVariantCurrent getByttVariantIdOrDefault(Integer ttVariantId, TtVariantCurrent def)
    {
        if (ttVariantId == null) {
            return def;
        }

        final TtVariantCurrent ttVariantCurrent = mapper.getByttVariantId(ttVariantId);
        return (ttVariantCurrent == null ? def : ttVariantCurrent);

    }

    @Mapper
    private interface TtVariantCurrentMapper {
        @Select(
                "select " +
                "tr_id as trId " +
                ",driver_id as driverId " +
                ",tt_variant_id as ttVariantId " +
                ",tt_variant_text as ttVariantName " +
                ",driver_text as driverName " +
                ",route_id as routeId " +
                ",tt_out_num as outNum " +
                ",dr_shift_num as shiftNum " +
                "from ttb.v_tt_variant_current_info " +
                "where tr_id = #{trId} "
        )
        @Options(useCache = false, statementType = StatementType.CALLABLE)
        TtVariantCurrent getByTrId(Long trId);


        @Select(
                "SELECT " +
                "tt_variant.tt_variant_id as ttVariantId " +
                ",ttb.jf_tt_variant_pkg$get_text(tt_variant.tt_variant_id) as ttVariantName " +
                ",route.route_id  as routeId " +
                "FROM rts.route " +
                "JOIN rts.route_variant ON route.route_id = route_variant.route_id " +
                "JOIN ttb.tt_variant ON tt_variant.route_variant_id = route_variant.route_variant_id " +
                "WHERE tt_variant.tt_variant_id  = #{ttVariantId}"

        )
        @Options(useCache = false, statementType = StatementType.CALLABLE)
        TtVariantCurrent getByttVariantId(Integer ttVariantId);
    }
}