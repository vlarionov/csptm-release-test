package ru.advantum.csptm.service.incident.ribbon.config;

import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;
import ru.advantum.csptm.service.incident.ribbon.IncidentsRibbonConfig;
import ru.advantum.csptm.spring.rmq.ConsumerProperties;

@Configuration
public class RmqConfig {
    @Bean
    public JepRmqServiceConfig jepRmqServiceConfig(IncidentsRibbonConfig config) {
        return config.rmqServiceConfig;
    }

    @Bean
    public ConsumerProperties consumerProperties() {
        return ConsumerProperties.newBuilder()
                .setAcknowledgeMode(AcknowledgeMode.AUTO)
                .build();
    }

}
