package ru.advantum.csptm.service.incident.ribbon;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.config.common.db.RedisClientConfig;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

@Root(name = "incidents-ribbon")
public class IncidentsRibbonConfig {
    @Element(name = "rmq-service")
    public JepRmqServiceConfig rmqServiceConfig;

    @Element
    public DatabaseConnection database;

    @Element(name = "redis-client")
    public RedisClientConfig redisConfig;

}
