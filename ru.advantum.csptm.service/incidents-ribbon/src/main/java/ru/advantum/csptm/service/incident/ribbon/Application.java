package ru.advantum.csptm.service.incident.ribbon;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {
    public static final String CONFIG_FILE_NAME = "incidents-ribbon.xml";

    public static void main(String[] args) {
        new SpringApplicationBuilder(Application.class)
                .web(false)
                .bannerMode(Banner.Mode.LOG)
                .run(args);
    }

    @Bean
    public static IncidentsRibbonConfig applicationConfig() throws Exception {
        return ru.advantum.config.Configuration.unpackConfig(
                IncidentsRibbonConfig.class,
                CONFIG_FILE_NAME
        );
    }

}
