package ru.advantum.csptm.service.incident.ribbon.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.service.incident.ribbon.IncidentsRibbonConfig;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
public class DBConfig {
    @Bean
    public DataSource dataSource(IncidentsRibbonConfig config) throws SQLException {
        return config.database.getConnectionPool();
    }

}
