package ru.advantum.csptm.service.incident.ribbon.component;

import com.google.common.collect.EvictingQueue;
import com.google.common.collect.Queues;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import ru.advantum.csptm.incident.model.*;
import ru.advantum.csptm.service.incident.ribbon.db.*;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class IncidentRibbonUpdater {

    private final RedisTemplate<String, IncidentBundle> redisTemplate;

    private BoundValueOperations redisOperationsRibbons;
    private BoundValueOperations redisOperationsIncidents;
    private static final String INCIDENS_MAP_NAME = "incidents";
    private static final Logger LOGGER = LoggerFactory.getLogger(IncidentRibbonUpdater.class);
    private final Gson GSON;
    private EvictingQueue<Incident> incidentOueue = EvictingQueue.create(1000);
    private Queue<Incident> incidentRibbon =  Queues.synchronizedQueue(incidentOueue);
    private final IncidentTypeDao incidentTypeDao;
    private final TrDao trDao;
    private final TtVariantCurrentDao ttVariantCurrentDao;
    private final TtVariantTextDao tTVariantTextDao;
    private final IncidentReasonDao incidentReasonDao;
    private final StopItemTextDao stopItemTextDao;

    @Autowired
    public IncidentRibbonUpdater(Gson gson, RedisTemplate<String, IncidentBundle> redisTemplate
            , IncidentTypeDao incidentTypeDao
            , TrDao trDao
            , TtVariantCurrentDao ttVariantCurrentDao
            , TtVariantTextDao tTVariantTextDao
            , IncidentReasonDao incidentReasonDao
            , StopItemTextDao stopItemTextDao)
            {
        this.redisTemplate = redisTemplate;
        this.trDao = trDao;
        this.GSON = gson;
        this.incidentTypeDao = incidentTypeDao;
        this.ttVariantCurrentDao = ttVariantCurrentDao;
        this.tTVariantTextDao = tTVariantTextDao;
        this.incidentReasonDao = incidentReasonDao;
        this.stopItemTextDao = stopItemTextDao;
    }

    public void add(Incident incident) {
        incidentRibbon.add(incident);
    }

    private void dumpRibbon() {
        try {
            LOGGER.info("Dumping Incident to redis");
            List<Incident> incidentRibbonList = new ArrayList<>(incidentRibbon);
            List<IncidentRibbon> incidentList = incidentRibbonList.stream()
                    .map(i -> new IncidentHelper(i).init().getIncidentRibbon())
                    .collect(Collectors.toList());
            int size = incidentList.size();
            redisOperationsRibbons.set(GSON.toJson(incidentList));
            LOGGER.info(String.format("Dumped %s Incidents to incidentRibbon", size));

            redisOperationsIncidents.set(GSON.toJson(incidentRibbonList));
            LOGGER.info(String.format("D    umped %s Incidents incidents", size));

        } catch (Throwable t) {
            LOGGER.error("dumpRibbon", t);
        }
    }

    @PostConstruct
    public void init() {
        redisOperationsRibbons = redisTemplate.boundValueOps(IncidentRibbon.INCIDENT_RIBBON_MAP_NAME);
        redisOperationsIncidents = redisTemplate.boundValueOps(INCIDENS_MAP_NAME);
        Object o = redisOperationsIncidents.get();
        if (o != null) {
            Incident[] ir = GSON.fromJson(o.toString(), Incident[].class);
            incidentOueue.addAll(Arrays.asList(ir));
        }

        Executors.newSingleThreadScheduledExecutor()
                .scheduleAtFixedRate(this::dumpRibbon,
                        5, 5, TimeUnit.SECONDS);
    }


    private final class IncidentHelper {
        private final Incident incident;
        private final Long trId;
        private final Long unitId;
        private final Integer ttVariantId;
        private final Integer driverId;
        private final Integer incidentReasonId;
        private final Integer stopItemId;
        private Tr tr;
        private TtVariantCurrent ttVariantCurrent;
        private IncidentType incidentType;
        private String ttVariantText;


        public IncidentHelper(Incident incident) {
            this.incident = incident;
            this.trId = getTrId();
            this.unitId = getUnitId();
            this.ttVariantId = getTtVariantId();
            this.driverId = getDriverId();
            this.stopItemId = getStopItemId();
            incidentReasonId = (incident.getIncidentPayload() == null ? null : incident.getIncidentPayload().getIncidentReason());
        }

        public IncidentHelper init() {
            incidentType = incidentTypeDao.getById(incident.getIncidentTypeId());
            tr = trDao.getMap().getOrDefault(this.trId, Tr.EMPTY_TR);
            ttVariantCurrent = tr.getTrId() == null
                    ? ttVariantCurrentDao.getByTrIdOrDefault(tr.getTrId(), TtVariantCurrent.EMPTY_TT_VARIANT_CURRENT)
                    : ttVariantCurrentDao.getByttVariantIdOrDefault(this.ttVariantId, TtVariantCurrent.EMPTY_TT_VARIANT_CURRENT);

            if (ttVariantId != null) {
                ttVariantText = tTVariantTextDao.getById(ttVariantId);
            }
            return this;
        }

        public IncidentRibbon getIncidentRibbon() {
            return IncidentRibbon.builder()
                    .incidentId(incident.getIncidentId())
                    .timeStart(incident.getTimeStart())
                    .incidentInitiatorId(incident.getIncidentInitiatorId())
                    .incidentStatusId(incident.getIncidentStatusId())
                    .incidentTypeId(incident.getIncidentTypeId())
                    .parentIncidentId(incident.getParentIncidentId())
                    .trId(trId)
                    .unitId(unitId)
                    .accountId(incident.getAccountId())
                    .inctdentTypeName(incidentType.getName())
                    .trGarageNum(tr.getGarageNum())
                    .trLicence(tr.getLicence())
                    .inctdentTypeShortName(incidentType.getShortName())
                    .drivertId((ttVariantCurrent.getDrivertId() == null) ? driverId : ttVariantCurrent.getDrivertId())
                    .ttVariantId((ttVariantId == null) ? ttVariantCurrent.getTtVariantId() : ttVariantId)
                    .driverName(ttVariantCurrent.getDriverName())
                    .ttVariantName(ttVariantText == null ? ttVariantCurrent.getTtVariantName() : ttVariantText)
                    .isAlarm(incidentType.getIsAlarm())
                    .incidentReasonId(incident.getIncidentPayload() == null ? null : incident.getIncidentPayload().getIncidentReason())
                    .incidentReasonName(incidentReasonId == null ? null : incidentReasonDao.getById(incidentReasonId))
                    .routeId(ttVariantCurrent.getRouteId())
                    .incommingMessage(incident.getIncidentPayload().getIncommingMessage())
                    .incommingMessageId(incident.getIncidentPayload().getIncommingMessageId())
                    .outNum(ttVariantCurrent.getOutNum())
                    .shiftNum(ttVariantCurrent.getShiftNum())
                    .trTypeName(tr.getTrTypeName())
                    .stopItemId(stopItemId)
                    .stopItemName(stopItemId == null ? null : stopItemTextDao.getById(stopItemId))
                    .build();
        }

        private Long getTrId() {
            final Long trId = incident.getTrId();
            if (trId == null) {
                final IncidentPayload ip = incident.getIncidentPayload();
                if (ip != null && ip.getTrList() != null && ip.getTrList().size() != 0) {
                    return ip.getTrList().get(0);
                }

            }
            return trId;
        }

        private Long getUnitId() {
            final Long unitId = incident.getUnitId();
            if (unitId == null) {
                final IncidentPayload ip = incident.getIncidentPayload();
                if (ip != null && ip.getUnitList() != null && ip.getUnitList().size() != 0) {
                    return ip.getUnitList().get(0).longValue();
                }

            }
            return unitId;
        }

        private Integer getTtVariantId() {

            final IncidentPayload incidentPayload = incident.getIncidentPayload();
            if (incidentPayload != null
                    && incidentPayload.getTtVariantList() != null
                    && incidentPayload.getTtVariantList().size() > 0 ) {
                return incidentPayload.getTtVariantList().get(0);

            }
            return null;
        }

        private Integer getDriverId() {

            final IncidentPayload incidentPayload = incident.getIncidentPayload();
            if (incidentPayload != null
                    && incidentPayload.getDriverList() != null
                    && incidentPayload.getDriverList().size() > 0 ) {
                return incidentPayload.getDriverList().get(0);

            }
            return null;
        }

        private Integer getStopItemId() {

            final IncidentPayload incidentPayload = incident.getIncidentPayload();
            if (incidentPayload != null
                    && incidentPayload.getStopItemList() != null
                    && incidentPayload.getStopItemList().size() > 0 ) {
                return incidentPayload.getStopItemList().get(0);

            }
            return null;
        }
    }

    public static void main(String[] args) {
        Long driverId = null;
        //System.out.print(TtVariantCurrent.EMPTY_TT_VARIANT_CURRENT.getDrivertId());
        TtVariantCurrent ttVariantCurrent = TtVariantCurrent.EMPTY_TT_VARIANT_CURRENT;
        IncidentRibbon.builder()
                .incidentId(1L)
                .timeStart(Instant.now())
                .incidentInitiatorId((short)1)
                .incidentStatusId((short)1)
                .incidentTypeId((short)1)
                .drivertId(
                        ttVariantCurrent == null ? null : ttVariantCurrent.getDrivertId()
                ).build();
    }

}