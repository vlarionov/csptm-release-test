package ru.advantum.csptm.service.incident.ribbon.controller;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.incident.model.*;
import ru.advantum.csptm.service.incident.ribbon.component.IncidentRibbonUpdater;

@Service
@Slf4j
public class RmqController {
    private final IncidentRibbonUpdater incidentRibbonUpdater;
    private static final Logger LOGGER = LoggerFactory.getLogger(IncidentRibbonUpdater.class);
    @Autowired
    public RmqController(IncidentRibbonUpdater incidentRibbonUpdater
    ) {
        this.incidentRibbonUpdater = incidentRibbonUpdater;
    }


    @RabbitListener
    public void receiveIncidentBundle(IncidentBundle incidentBundle) {
        incidentBundle.getIncidentBundle().forEach(incidentRibbonUpdater::add);
    }
}
