package ru.advantum.csptm.service.incident.ribbon.db;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.StatementType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Created by kukushkin on 11/9/17.
 */
@Repository
public class IncidentReasonDao {
    private final IncidentReasonDao.IncidentReasonMapper mapper;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public IncidentReasonDao(IncidentReasonDao.IncidentReasonMapper mapper, JdbcTemplate jdbcTemplate) {
        this.mapper = mapper;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Cacheable("incident_reason")
    public String getById(Integer id)
    {
        return mapper.getById(id);
    }

    @Mapper
    private interface IncidentReasonMapper {
        @Select("select a from ttb.jf_incident_reason_pkg$get_name4ribbon(#{id}) a")
        @Options(useCache = false, statementType = StatementType.CALLABLE)
        String getById(Integer id);
    }
}