package ru.advantum.csptm.service.incident.ribbon.db;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.StatementType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.advantum.csptm.incident.model.Tr;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by kukushkin on 11/9/17.
 */
@Repository
public class TrDao {
    private final TrDao.TrMapper mapper;
    private final JdbcTemplate jdbcTemplate;
    private static final Logger LOGGER = LoggerFactory.getLogger(TrDao.class);

    @Autowired
    public TrDao(TrDao.TrMapper mapper, JdbcTemplate jdbcTemplate) {
        this.mapper = mapper;
        this.jdbcTemplate = jdbcTemplate;
    }


    @Cacheable("trList")
    public Map<Long, Tr> getMap()
    {
        LOGGER.info("trList");
        return mapper.getAll().stream().collect(Collectors.toMap(t -> t.getTrId(), t -> t));
    }


    @Mapper
    private interface TrMapper {
        @Select("SELECT " +
                "tr_id as trId, " +
                "garage_num::text as garageNum, " +
                "licence as licence, " +
                "tr_type.short_name as trTypeName " +
                "FROM core.tr " +
                "join core.tr_type on tr.tr_type_id = tr_type.tr_type_id "
        )
        @Options(useCache = false, statementType = StatementType.CALLABLE)
        List<Tr> getAll();
    }
}
