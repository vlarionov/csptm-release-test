package ru.advantum.csptm.service.incident.ribbon.db;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.StatementType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Created by kukushkin on 11/9/17.
 */
@Repository
public class TtVariantTextDao {
    private final TtVariantTextDao.TTVariantTextMapper mapper;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public TtVariantTextDao(TtVariantTextDao.TTVariantTextMapper mapper, JdbcTemplate jdbcTemplate) {
        this.mapper = mapper;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Cacheable("tt_variant_text")
    public String getById(Integer id)
    {
        return mapper.getById(id);
    }

    @Mapper
    private interface TTVariantTextMapper {
        @Select("select a from ttb.jf_tt_variant_pkg$get_text(#{incidentTypeId}) a")
        @Options(useCache = false, statementType = StatementType.CALLABLE)
        String getById(Integer id);
    }
}