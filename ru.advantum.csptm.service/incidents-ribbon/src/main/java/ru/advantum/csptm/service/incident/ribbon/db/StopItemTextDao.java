package ru.advantum.csptm.service.incident.ribbon.db;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.StatementType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Created by kukushkin on 11/9/17.
 */
@Repository
public class StopItemTextDao {
    private final StopItemTextDao.StopItemTexMapper mapper;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public StopItemTextDao(StopItemTextDao.StopItemTexMapper mapper, JdbcTemplate jdbcTemplate) {
        this.mapper = mapper;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Cacheable("stop_item_text")
    public String getById(Integer id)
    {
        return mapper.getById(id);
    }

    @Mapper
    private interface StopItemTexMapper {
        @Select("select a from rts.jf_stop_item_pkg$get_text(#{stopItemId}) a")
        @Options(useCache = false, statementType = StatementType.CALLABLE)
        String getById(Integer id);
    }
}