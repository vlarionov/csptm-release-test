package ru.advantum.csptm.service.incident.ribbon.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import ru.advantum.config.common.db.RedisClientConfig;
import ru.advantum.csptm.incident.model.IncidentBundle;
import ru.advantum.csptm.service.incident.ribbon.IncidentsRibbonConfig;

@Configuration
public class RedisConfig {


    @Bean
    public JedisConnectionFactory jedisConnectionFactory(IncidentsRibbonConfig config) {
        RedisClientConfig redisClientConfig = config.redisConfig;
        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
        jedisConnectionFactory.setHostName(redisClientConfig.host);
        jedisConnectionFactory.setPort(redisClientConfig.port);
        jedisConnectionFactory.setTimeout(redisClientConfig.timeout);
        jedisConnectionFactory.setPassword(redisClientConfig.password);
        return jedisConnectionFactory;
    }

    @Bean
    public RedisTemplate<String, IncidentBundle> redisTeplate(JedisConnectionFactory jedisConnectionFactory) {
        RedisTemplate<String, IncidentBundle> redisTemplate = new RedisTemplate<>();
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new StringRedisSerializer());
        redisTemplate.setConnectionFactory(jedisConnectionFactory);
        return redisTemplate;
    }


}
