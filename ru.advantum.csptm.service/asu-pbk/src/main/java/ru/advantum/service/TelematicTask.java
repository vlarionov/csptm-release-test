package ru.advantum.service;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.ibatis.session.SqlSession;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Класс выгрузки телематики
 */
public class TelematicTask implements Job {
    private static final Logger log = LoggerFactory.getLogger(TelematicTask.class);

    private static final String EXPORT_TELEMATIC_SQL = "{ ? = call asupbk.asu_pbk_export_pkg$export_telematic(?)}";

    /**
     * @param context контекст выгрузки
     * @throws JobExecutionException Метод запускается по расписанию, выполняет выгрузку телематики из БД в файл, настройки указываются в конфиге
     */
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        try {
            log.info("start export of telematic data....");

            JobDataMap data = context.getJobDetail().getJobDataMap();

            String directory = (String) data.get("telematicDirectory");
            String database = (String) data.get("database");
            SimpleDateFormat format = (SimpleDateFormat) data.get("filenameFormat");

            Date yesterday = DateTime.now().withTimeAtStartOfDay().minusDays(1).toDate();
            FileUtils.createDirIfNotExist(directory);
            String filePath = directory + "/" + format.format(yesterday) + ".zip";

            try (SqlSession session = SqlMapperSingleton.getInstance(database).openSession();Connection connection = session.getConnection()) {
                connection.setAutoCommit(false);
                try (CallableStatement proc = connection.prepareCall(EXPORT_TELEMATIC_SQL)) {
                    proc.setDate(2, new java.sql.Date(yesterday.getTime()));
                    proc.registerOutParameter(1, Types.OTHER);
                    proc.execute();
                    try (ResultSet results = (ResultSet) proc.getObject(1)) {
                        results.setFetchSize(100);
                        saveToFile(filePath, results);
                    }
                }
            }
        } catch (Exception ex) {
            log.error("", ex);
        }

        log.info("finish export of telematic data....");
    }

    private void saveToFile(String filePath, ResultSet telematics) throws Exception {
        ZipOutputStream zipStream = new ZipOutputStream(new FileOutputStream(new File(filePath)));
        zipStream.putNextEntry(new ZipEntry("telematic.csv"));
        log.debug("result file is {}", filePath);

        StringBuilder buffer = new StringBuilder();

        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withDelimiter(';').withFirstRecordAsHeader();
        try (CSVPrinter csvPrinter = new CSVPrinter(buffer, csvFileFormat)) {
            csvPrinter.printRecord("GMT_UNIX_TIME", "EQUIPMENT_ID", "LON", "LAT");

            int telematicsSize = 0;
            while (telematics.next()) {
                csvPrinter.printRecord(
                        telematics.getTimestamp("datetime"),
                        telematics.getString("equipment_id"),
                        telematics.getDouble("lat"),
                        telematics.getDouble("lon")
                );
                zipStream.write(buffer.toString().getBytes());
                buffer.setLength(0);
                telematicsSize++;
            }
            log.info("size of telematics {}", telematicsSize);
            log.info("new export of telematic in the file {}", filePath);
        } catch (Exception ex) {
            log.error("", ex);
        } finally {
            zipStream.flush();
            zipStream.close();
        }
    }
}
