package ru.advantum.service.stopSequence;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.ibatis.session.SqlSession;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.service.FileUtils;
import ru.advantum.service.SqlMapperSingleton;
import ru.advantum.service.ZipFileUtils;
import ru.advantum.service.mappers.ASUPBKMapper;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Класс выгрузки прохождения остановочных пунктов
 */
public abstract class AbstractStopSequenceTask implements Job {
    private static final String[] header = { "DATE", "MR_CODE", "DEPOT_NUMBER", "SHIFT_NUM", "ROUTE_NUM", "ORDER_NUM",
            "STOP_ID", "ERM_ID", "TRIP_ID", "GRAFIC", "ARRIVAL_TIME_MSK" };

    private static final Logger log = LoggerFactory.getLogger(AbstractStopSequenceTask.class);

    protected JobDataMap dataMap;

    /**
     *
     * @param context контекст запуска
     * @throws JobExecutionException
     *
     * Запускается по расписанию, указанному в конфиге, выгружает записи последовательности ОП за прошлые транспортные сутки,
     * результат записывает в файл
     */
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        log.info("start export of stop sequence....");

        dataMap = context.getJobDetail().getJobDataMap();
        String directory = (String) dataMap.get("stopSequenceDirectory");
        String database = (String) dataMap.get("database");

        try (SqlSession session = SqlMapperSingleton.getInstance(database).openSession()) {
            DateTime today = new DateTime();
            DateTime yesterday = today.minusDays(1);
            ASUPBKMapper mapper = session.getMapper(ASUPBKMapper.class);
            List<Map<String, Object>> stopSequenceRows = mapper.selectStopSequence(yesterday.toDate());
            log.debug("size of stop sequence list {}", stopSequenceRows.size());

            FileUtils.createDirIfNotExist(directory);

            String baseFileName = getFileName();
            String csvFileName = baseFileName + ".csv";

            String filePath = directory + "/" + csvFileName;

            FileUtils.createFileIfNotExist(filePath);
            FileWriter writer = new FileWriter(filePath);

            CSVFormat csvFileFormat = CSVFormat.DEFAULT.withDelimiter(';').withFirstRecordAsHeader();
            try (CSVPrinter csvPrinter = new CSVPrinter(writer, csvFileFormat)) {
                csvPrinter.printRecord(header);
                for (Map<String, Object> stopSequence: stopSequenceRows) {
                    printRecord(csvPrinter, stopSequence);
                }

                csvPrinter.flush();
            }

            ZipFileUtils.makeZIP(filePath, directory + "/" + baseFileName + ".zip", csvFileName);

            writeMd5(filePath, directory + "/" + baseFileName + ".md5");

            log.info("stop sequence exported in {}", filePath);
            log.info("finish export of stop sequence....");
        } catch (Exception ex) {
            log.error("", ex);
        }
    }

    /**
     *
     * @param pathToSource путь до источника для взятия md5
     * @param pathToTarget путь до файла в который будет записан результат
     * @throws Exception
     *
     * Метод берет md5 от источника и записывает по пути pathToTarget
     */
    private void writeMd5(String pathToSource, String pathToTarget) throws Exception {
        String md5 = ZipFileUtils.calcMD5(Paths.get(pathToSource));

        FileUtils.createFileIfNotExist(pathToTarget);

        try (FileWriter md5Writer = new FileWriter(pathToTarget)) {
            md5Writer.write(md5);
            md5Writer.flush();
        }
    }

    /**
     *
     * @return имя файла для выгрузки
     *
     * Метод определяется наследником
     */
    protected abstract String getFileName();

    /**
     *
     * @param printer объект для записи в файл
     * @param record строка с данными
     * @throws IOException
     *
     * Метод записи в файл очередной партии данных, определяется в наследнике
     */
    protected abstract void printRecord(CSVPrinter printer, Map<String, Object> record) throws IOException;
}
