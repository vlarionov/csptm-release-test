package ru.advantum.service;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.service.mappers.ASUPBKMapper;
import ru.advantum.service.models.Equipment;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.OutputStreamWriter;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Класс доступа по http по url
 */
@Path("/mgt")
public class EntryPoint {

    private static final Logger log = LoggerFactory.getLogger(EntryPoint.class);

    private Pattern equipmentRequest = Pattern.compile("EQUIPPBK");
    private Pattern telematicRequest = Pattern.compile("TELPBK\\d{8}");
    private Pattern stopRequest = Pattern.compile("TRIPS_MGT_FULL\\d{8}");
    private Pattern stopRequestWithoutAtm = Pattern.compile("TRIPS_MGT\\d{8}");

    //TODO посмотреть как переделать проперти
    private String telematicDirectory = System.getProperty("ru.advantum.csptm.service.asupbk.telematic.dir");
    private String stopSequenceDirectory = System.getProperty("ru.advantum.csptm.service.asupbk.stopseq.dir");
    private String noAtmSuffix = System.getProperty("ru.advantum.csptm.service.asupbk.stopseq.noatm");


    /**
     *
     * @param name имя выгрузки
     * @param type тип файла
     * @return ответ с файлом, или содержимым файла выгрузки
     *
     * возвращает выгрузку с указанным именем и типом
     */
    @GET
    @Path("file")
    public Response getCsvFile(@QueryParam("name") final String name, @QueryParam("type") final String type) {
        if (equipmentRequest.matcher(name).matches()) {
            switch (type) {
                case "ZIP": {
                    return getEquipmentZip();
                }
                case "TEXT": {
                    return getEquipment();
                }
                default: {
                    log.warn("No such load type {}", type);
                    return Response.status(Response.Status.BAD_REQUEST).build();
                }
            }
        }

        if (telematicRequest.matcher(name).matches()) {
            String fileName = name.substring(6, 14) + ".zip";
            return getTelematicZip(fileName);
        }

        if (stopRequest.matcher(name).matches()) {
            String fileName = name.substring(14, 22);
            return getStopSequence(fileName, type);
        }

        if (stopRequestWithoutAtm.matcher(name).matches()) {
            String fileName = name.substring(9, 17) + noAtmSuffix;
            return getStopSequence(fileName, type);
        }

        log.warn("No such load name {}", name);
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    /**
     *
     * @param fileName имя файла с выгрузкой последовательности ОП
     * @param type тип, который необходимо вернуть
     * @return файл выгрузки ОП указанного типа
     */
    private Response getStopSequence(String fileName, String type) {
        String contentType = "application/text";
        String targetPath = null;
        String resultFile = fileName;
        try {
            targetPath = FileUtils.getDateDirs(fileName, "day", stopSequenceDirectory);

            switch (type) {
                case "TEXT": {
                    resultFile = resultFile + ".csv";
                    break;
                }
                case "MD5": {
                    resultFile = resultFile + ".md5";
                    break;
                }
                case "ZIP": {
                    contentType = "application/zip";
                    resultFile = resultFile + ".zip";
                    break;
                }
                default: {
                    log.warn("No such type {}", type);
                }
            }

        } catch (Exception e) {
            log.error("", e);
        }

        return getFileContent(targetPath, resultFile, contentType);
    }

    /**
     *
     * @param path директория с файлом
     * @param fileName имя файла
     * @param contentType тип содержимого файла
     * @return http ответ с содержимым указанного файла указанного типа
     */
    private Response getFileContent(String path, String fileName, String contentType) {
        if (Files.exists(Paths.get(path + "/" + fileName))) {
            return Response.ok((StreamingOutput) output -> {
                try {
                    java.nio.file.Path p = FileSystems.getDefault().getPath(path + "/" + fileName);
                    Files.copy(p, output);
                }
                catch(InterruptedIOException ex){
                    log.warn(String.format("Requested file not found %s", path + "/" + fileName));
                } catch(NoSuchFileException ex){
                    log.warn("run ", ex);
                } catch(IOException ex){
                    log.error("run ", ex);
                }
            }).header("Content-Disposition", "attachment; filename=\"" + fileName + "\"")
                    .header("Content-Type", contentType)
                    .build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    /**
     *
     * @param fileName путь до файла с телематикой
     * @return http ответ с файлом телематики в формате zip
     */
    private Response getTelematicZip(String fileName) {
        return getFileContent(telematicDirectory, fileName, "application/zip");
    }

    /**
     *
     * @return возвращает файл с выгрузкой бортового оборудования в формате csv
     */
    public Response getEquipment() {
        try (SqlSession session = SqlMapperSingleton.getInstance("asu-pbk").openSession()) {
            ASUPBKMapper mapper = session.getMapper(ASUPBKMapper.class);
            List<Equipment> equipmentList = mapper.selectEquipment();

            CSVFormat csvFileFormat = CSVFormat.DEFAULT.withDelimiter(';');
            return Response.ok((StreamingOutput) output -> {
                try (CSVPrinter csvPrinter = new CSVPrinter(new OutputStreamWriter(output), csvFileFormat)) {
                    csvPrinter.printRecord("EQUIPMENT_ID", "VEHICLE_ID", "TYPE", "GOSNUM");
                    for (Equipment equipment : equipmentList) {
                        csvPrinter.printRecord(equipment.getEquipmentId(),
                                equipment.getVehicleId(), equipment.getType(), equipment.getGosNumber());
                    }
                } catch (Exception ex) {
                    log.info("", ex);
                } finally {
                    output.flush();
                    output.close();
                }
            }).header("Content-Disposition", "attachment; filename=\"equipment.csv\"")
                    .header("Content-Type", "application/text").build();
        } catch (Exception ex) {
            log.info("", ex);

            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    /**
     *
     * @return возвращает файл с выгрузкой бортового оборудования в формате zip
     */
    public Response getEquipmentZip() {
        try (SqlSession session = SqlMapperSingleton.getInstance("asu-pbk").openSession()) {
            ASUPBKMapper mapper = session.getMapper(ASUPBKMapper.class);
            List<Equipment> equipmentList = mapper.selectEquipment();

            CSVFormat csvFileFormat = CSVFormat.DEFAULT.withDelimiter(';');
            return Response.ok((StreamingOutput) output -> {
                ZipOutputStream zipStream = new ZipOutputStream(output);
                zipStream.putNextEntry(new ZipEntry("equipment.csv"));
                StringBuilder buffer = new StringBuilder();

                try (CSVPrinter csvPrinter = new CSVPrinter(buffer, csvFileFormat)) {
                    csvPrinter.printRecord("EQUIPMENT_ID", "VEHICLE_ID", "TYPE", "GOSNUM");
                    for (Equipment equipment : equipmentList) {
                        csvPrinter.printRecord(equipment.getEquipmentId(),
                                equipment.getVehicleId(), equipment.getType(), equipment.getGosNumber());
                    }

                    zipStream.write(buffer.toString().getBytes());
                    buffer.setLength(0);
                } catch (Exception ex) {
                    log.info("", ex);
                } finally {
                    zipStream.flush();
                    zipStream.close();
                }
            }).header("Content-Disposition", "attachment; filename=\"equipment.zip\"")
                    .header("Content-Type", "application/zip")
                    .build();
        } catch (Exception ex) {
            log.info("", ex);

            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
