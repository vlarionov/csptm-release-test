package ru.advantum.service.mappers;

import org.apache.ibatis.annotations.Param;
import ru.advantum.service.models.Equipment;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Интерфейс запросов к БД
 */
public interface ASUPBKMapper {
    /**
     *
     * @return список бортового оборудования
     */
    List<Equipment> selectEquipment();

    /**
     *
     * @param dateBegin дата начала транспортной работы
     * @param routeNumber номер маршрута
     * @return список транспортной работы начатой в указанную дату для указанного маршрута
     */
    List<Map<String, Object>> selectTransportWork(@Param("dateBegin") Date dateBegin, @Param("routeNumber") String routeNumber);

    /**
     *
     * @param dateEnd дата закрытия маршрута
     * @return список закрытых маршрутов
     */
    List<Map<String, Object>> selectClosedRoutes(@Param("dateEnd") Date dateEnd);

    /**
     * @param closedRouteId идентификатор закрытого маршрута
     */
    void removeClosedRoute(@Param("closed_route_id") Long closedRouteId);

    /**
     *
     * @param date дата прохождения остановочного пункта
     * @return список остановочных пунктов пройденных за указанную дату
     */
    List<Map<String, Object>> selectStopSequence(@Param("date") Date date);
}
