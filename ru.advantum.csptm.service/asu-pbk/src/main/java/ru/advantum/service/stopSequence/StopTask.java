package ru.advantum.service.stopSequence;

import org.apache.commons.csv.CSVPrinter;
import org.joda.time.DateTime;
import ru.advantum.service.mappers.ASUPBKMapper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Класс выгрузки последовательности ОП с АТМ
 */
public class StopTask extends AbstractStopSequenceTask {

    /**
     *
     * @return имя файла с выгрузкой
     *
     * метод получения имени файла для выгрузки
     */
    @Override
    protected String getFileName() {
        SimpleDateFormat format = (SimpleDateFormat) dataMap.get("filenameFormat");
        DateTime yesterday = new DateTime().minusDays(1);

        String todayString = format.format(yesterday.toDate());

        return todayString;
    }

    /**
     *
     * @param printer объект для записи в файл
     * @param record строка с данными
     * @throws IOException
     *
     * Запись в файл последовательности ОП с АТМ
     */
    @Override
    protected void printRecord(CSVPrinter printer, Map<String, Object> record) throws IOException {
        printer.printRecord(record.get("transmission_date"), record.get("mr_code"),
                record.get("depot_number"), record.get("shift_number"), record.get("route_number"),
                record.get("order_number"), record.get("stop_id"), record.get("erm_id"),
                record.get("trip_id"), record.get("graphic_id"), record.get("arrival_time"));
    }
}
