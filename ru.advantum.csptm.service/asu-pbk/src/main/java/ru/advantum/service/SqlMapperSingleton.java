package ru.advantum.service;

import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

/**
 * Класс получения доступа до БД
 */
public class SqlMapperSingleton {
    private static volatile Map<String, SqlSessionFactory> dbMap;

    public static SqlSessionFactory getInstance(final String name) {
        createMap();

        return dbMap.get(name);
    }

    /**
     * Метод создания карты соединений
     */
    private static void createMap() {
        if (null == dbMap) {
            synchronized (SqlMapperSingleton.class) {
                if (null == dbMap) {
                    dbMap = new ConcurrentHashMap<>();
                }
            }
        }
    }


    /**
     *
     * @param name имя объекта настроек БД
     * @param configFile файл с настройками соединения к БД
     * @param consumer функция дополнительных действий с соединением
     *
     * Метод регистрации БД с именем name, средой environment
     */
    public static void registerInstance(final String name, final String configFile, Consumer<SqlSessionFactory> consumer) throws IOException {
        createMap();
        SqlSessionFactory instance = new SqlSessionFactoryBuilder()
                .build(new FileInputStream(new File(configFile)));

        consumer.accept(instance);
        dbMap.put(name, instance);
    }

    /**
     *
     * @param name имя объекта настроек БД
     * @param configFile файл с настройками соединения с БД
     *
     * Метод регистрации БД с именем name, средой environment
     */
    public static void registerInstance(final String name, final String configFile) throws IOException {
        registerInstance(name, configFile, factory -> {});
    }

    /**
     *
     * @param name имя объекта настроек БД
     * @param environment настройки соединения с БД
     * @param consumer функция дополнительных действий с соединением
     *
     * Метод регистрации БД с именем name, средой environment, и доп. настройками задаваемыми в consumer
     */
    public static void registerInstance(final String name, Environment environment, Consumer<SqlSessionFactory> consumer) {
        //получается, кому повезет, тот будет последним, того и слот в мапе
        //если нам надо строгую последовательность добавления, то синхронизировать эти методы
        createMap();

        Configuration configuration = new Configuration(environment);
        SqlSessionFactory instance = new SqlSessionFactoryBuilder().build(configuration);
        consumer.accept(instance);
        dbMap.put(name, instance);
    }

    /**
     *
     * @param name имя объекта настроек БД
     * @param environment настройки соединения с БД
     *
     * Метод регистрации БД с именем name, средой environment
     */
    public static void registerInstance(final String name, Environment environment) {
        registerInstance(name, environment, factory -> {});
    }
}
