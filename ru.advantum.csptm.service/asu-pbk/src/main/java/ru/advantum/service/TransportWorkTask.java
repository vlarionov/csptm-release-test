package ru.advantum.service;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;
import org.apache.ibatis.session.SqlSession;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.service.mappers.ASUPBKMapper;

import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Класс выгрузки транспортной работы
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class TransportWorkTask implements Job {

    private static final Logger log = LoggerFactory.getLogger(TransportWorkTask.class);

    protected JobDataMap dataMap;

    private Map<Integer, String> closedRoutes;
    private DateTime lastExecute = new DateTime();

    /**
     *
     * @param context контекст запуска
     * @throws JobExecutionException
     *
     * Срабатывает по расписанию, анализирует записи из таблицы закрытых маршрутов (closed_routes), при появлении новых записей,
     * выполняет выгрузку транспортной работы для них
     */
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        dataMap = context.getJobDetail().getJobDataMap();
        closedRoutes = (Map<Integer, String>) dataMap.get("closedRoutesMap");

        DateTime now = new DateTime();
        if (Days.daysBetween(lastExecute.toLocalDate(), now.toLocalDate()).getDays() > 0) {
            closedRoutes.clear();
        }
        lastExecute = now;

        List<Map<String, Object>> routes = null;
        try (SqlSession session = SqlMapperSingleton.getInstance("asu-pbk").openSession()) {
            ASUPBKMapper mapper = session.getMapper(ASUPBKMapper.class);
            routes = mapper.selectClosedRoutes(now.toDate());

            log.debug("got {} routes from db", routes.size());

            if (routes != null) {
                log.info("start export of transport work....");

                routes.stream().filter((route) -> !closedRoutes.containsKey(route.get("closed_route_id"))).forEach((route) -> {
                    closedRoutes.put((Integer) route.get("closed_route_id"), (String) route.get("route_id"));

                    try {
                        List<Map<String, Object>> transportWorkList = mapper.selectTransportWork((Date) route.get("date_begin"),
                                (String) route.get("route_number"));

                        processTransportWork(transportWorkList, (Date) route.get("date_begin"), (String) route.get("route_number"));

                        //возможно надо будет как-то помечать записи закрытых маршрутов, чтобы не загружать тр.работу для них по новой
                        //пока не понятен алгоритм определения закрытия маршрута
                    } catch (Exception ex) {
                        log.error("", ex);
                    }
                });
            }
        } catch (Exception ex) {
            log.error("", ex);
        }
    }

    /**
     * Метод выгрузки транспортной работы закрытую в указанную дату, для указанного номера маршрута
     */
    private void processTransportWork(List<Map<String, Object>> transportWorkList, Date date, String routeNumber) throws Exception {
        String directory = (String) dataMap.get("transportWorkDirectory");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");

        log.debug("transport work list size = {}", transportWorkList.size());

        if (!Files.exists(Paths.get(directory))) {
            Files.createDirectories(Paths.get(directory));
            log.debug("create directory {}", directory);
        }

        StringBuilder builder = new StringBuilder(directory);
        Date now = new Date();
        builder.append("/F-").append(dateFormat.format(date))
                .append("-").append(routeNumber).append("-").append(dateFormat.format(now))
                .append(timeFormat.format(now)).append(".csv");

        String filePath = builder.toString();
        if (!Files.exists(Paths.get(filePath))) {
            Files.createFile(Paths.get(filePath));
            log.debug("create file {}", filePath);
        }

        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withDelimiter(';').withNullString("")
                .withFirstRecordAsHeader().withEscape('%').withQuoteMode(QuoteMode.NONE);
        try (CSVPrinter csvPrinter = new CSVPrinter(new FileWriter(filePath), csvFileFormat)) {
            csvPrinter.printRecord("[New Fact]");

            for (Map<String, Object> transportWork : transportWorkList) {
                csvPrinter.printRecord(transportWork.get("park"), dateFormat.format(transportWork.get("group_date")), transportWork.get("route_type"),
                        transportWork.get("customer_type"), transportWork.get("route_number"), transportWork.get("out_number"),
                        transportWork.get("garage_number"), transportWork.get("shift_number"), transportWork.get("table_number"),
                        transportWork.get("time_code"), dateFormat.format(transportWork.get("date_begin")), timeFormat.format(transportWork.get("time_begin")),
                        dateFormat.format(transportWork.get("date_end")), timeFormat.format(transportWork.get("time_end")), transportWork.get("stay_type"),
                        transportWork.get("mass"), transportWork.get("length"), transportWork.get("equipment_mask"),
                        transportWork.get("equipment_time"), transportWork.get("engine_time"), transportWork.get("froute_type"),
                        transportWork.get("fcustomer_type"), transportWork.get("froute_number"), transportWork.get("fout_number"),
                        transportWork.get("acc_begin"), transportWork.get("acc_end"), transportWork.get("interval_count"),
                        transportWork.get("fpark_number"));
            }

            csvPrinter.flush();
        }

        log.info("finish transport work for route number {}, result in the file {}", routeNumber, filePath);
    }
}
