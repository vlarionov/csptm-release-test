package ru.advantum.service.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;

/**
 * Класс конфига сервиса АСУ ПБК
 */
@Root(name = "asu-pbk")
public class ASUPBKConfig {
    /**
     * Директория выгрузки телематики
     */
    @Attribute(name = "telematicDirectory")
    private String telematicDirectory;

    /**
     * Формат наименования файлов телематики (дата)
     */
    @Attribute(name = "telematicFileFormat")
    private String telematicFileFormat;

    /**
     * Директория выгрузки транспортной работы
     */
    @Attribute(name = "transportWorkDirectory")
    private String transportWorkDirectory;

    /**
     * Период выгрузки транспортной работы в минутах
     */
    @Attribute(name = "transportWorkPeriod")
    private Integer transportWorkPeriod;

    /**
     * Директория выгрузки остановочных пунктов
     */
    @Attribute(name = "stopSequenceDirectory")
    private String stopSequenceDirectory;

    /**
     * Формат наименования файла выгрузки остановочных пунктов
     */
    @Attribute(name = "stopSequenceFileFormat")
    private String stopSequenceFileFormat;

    /**
     * Суффикс для файлов выгрузки без АТМ
     */
    @Attribute(name = "stopSequenceNoAtmSuffix")
    private String stopSequenceNoAtmSuffix;

    /**
     * Путь до файла настроек базы данных
     */
    @Attribute(name = "databaseConfigPath")
    private String databaseConfig;

    /*@Element(name = "database")
    private DatabaseConnection database;*/

    /*public DatabaseConnection getDatabase() {
        return database;
    }*/

    public String getTelematicDirectory() {
        return telematicDirectory;
    }

    public Integer getTransportWorkPeriod() {
        return transportWorkPeriod;
    }

    public String getTelematicFileFormat() {
        return telematicFileFormat;
    }

    public String getTransportWorkDirectory() {
        return transportWorkDirectory;
    }

    public String getDatabaseConfig() {
        return databaseConfig;
    }

    public String getStopSequenceDirectory() {
        return stopSequenceDirectory;
    }

    public String getStopSequenceFileFormat() {
        return stopSequenceFileFormat;
    }

    public String getStopSequenceNoAtmSuffix() {
        return stopSequenceNoAtmSuffix;
    }
}
