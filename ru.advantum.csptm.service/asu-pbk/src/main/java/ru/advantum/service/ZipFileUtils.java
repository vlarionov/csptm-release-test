package ru.advantum.service;

import ru.advantum.tools.HEX;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Класс работы с zip
 */
public final class ZipFileUtils {

    /**
     *
     * @param pathToSource путь до источника
     * @param pathToTarget путь до результата
     * @param fileName имя файла - результата в архиве
     * @throws IOException
     *
     * создает zip файл из файла pathToSource
     */
    public static void makeZIP(String pathToSource, String pathToTarget, String fileName) throws IOException {
        final byte[] buffer = new byte[1024];
        FileOutputStream fout = new FileOutputStream(pathToTarget);
        ZipOutputStream zout = new ZipOutputStream(fout);
        ZipEntry ze = new ZipEntry(fileName);
        FileInputStream in = new FileInputStream(pathToSource);
        zout.putNextEntry(ze);
        int len;
        while ((len = in.read(buffer)) > 0) {
            zout.write(buffer, 0, len);
        }
        in.close();
        zout.closeEntry();
        zout.close();
    }

    /**
     *
     * @param pathTofile путь до файла
     * @return строку md5 от указанного файла
     * @throws NoSuchAlgorithmException
     * @throws IOException
     *
     * Метод берет md5 от содержимого указанного в pathToFile файла
     */
    public static String calcMD5(Path pathTofile) throws NoSuchAlgorithmException, IOException {
        final byte[] buffer = new byte[1024];
        try (InputStream fs = Files.newInputStream(pathTofile)) {
            MessageDigest md = MessageDigest.getInstance("MD5");
            DigestInputStream dis = new DigestInputStream(fs, md);
            int read = dis.read(buffer);
            while (read > -1) {
                read = dis.read(buffer);
            }
            return HEX.byteArrayToString(dis.getMessageDigest().digest()).replaceAll(" ", "").toUpperCase();
        }
    }
}
