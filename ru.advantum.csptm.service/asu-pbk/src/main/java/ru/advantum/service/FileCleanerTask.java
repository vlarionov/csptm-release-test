package ru.advantum.service;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Класс удаления устаревших файлов
 * Срабатывает по таймеру
 */
public class FileCleanerTask implements Job {

    private static Logger log = LoggerFactory.getLogger(FileCleanerTask.class);

    private String logDirectory;
    //private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    private Integer logTimeout;

    /**
     *
     * @param context контекст запуска класса
     * @throws JobExecutionException
     *
     * Данный метод запускается по таймеру, в директории logDir удаляются файлы с истекшим сроком хранения logTimeout
     */
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDataMap data = context.getJobDetail().getJobDataMap();
        logDirectory = (String) data.get("logDir");
        logTimeout = (Integer) data.get("logTimeout");

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -(logTimeout));
        Date expiredDate = calendar.getTime();
        //String expiredDate = dateFormat.format(date);

        File directory = new File(logDirectory);
        File[] files = directory.listFiles();
        for (File file : files) {
            try {
                Date fileDate = new Date(file.lastModified());
                //Date fileDate = dateFormat.parse(file.getName().substring(0, 8));
                if (fileDate.before(expiredDate)) {
                    Files.delete(file.toPath());
                    log.info("remove file {}", file.getName());
                }
            } catch(Exception ex) {
                log.error("", ex);
            }
        }
    }
}
