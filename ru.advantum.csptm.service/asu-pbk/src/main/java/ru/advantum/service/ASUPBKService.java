package ru.advantum.service;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;
import ru.advantum.config.Configuration;
import ru.advantum.service.config.ASUPBKConfig;
import ru.advantum.service.stopSequence.StopTask;
import ru.advantum.service.stopSequence.StopWithoutAtmTask;

import java.text.SimpleDateFormat;
import java.util.HashMap;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * Класс сервиса АСУ ПБК, точка входа, здесь выполняются начальные настройки и установка таймеров, расписаний
 */
public class ASUPBKService {
    private static final Logger log = LoggerFactory.getLogger(ASUPBKService.class);

    public static void main(String[] args) throws Exception {
        ASUPBKConfig config = Configuration.unpackConfig(ASUPBKConfig.class, "asu-pbk.xml");

        //рассмотреть возможность пробросить необходимые поля другим способом
        System.setProperty("ru.advantum.csptm.service.asupbk.telematic.dir", config.getTelematicDirectory());
        System.setProperty("ru.advantum.csptm.service.asupbk.telematic.filename", config.getTelematicFileFormat());
        System.setProperty("ru.advantum.csptm.service.asupbk.stopseq.dir", config.getStopSequenceDirectory());
        System.setProperty("ru.advantum.csptm.service.asupbk.stopseq.noatm", config.getStopSequenceNoAtmSuffix());

        SqlMapperSingleton.registerInstance(
                "asu-pbk",
                config.getDatabaseConfig());

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");

        Server jettyServer = new Server(8081);
        jettyServer.setHandler(context);

        ServletHolder jerseyServlet = context.addServlet(
                org.glassfish.jersey.servlet.ServletContainer.class, "/*");
        jerseyServlet.setInitOrder(0);

        jerseyServlet.setInitParameter(
                "jersey.config.server.provider.classnames",
                EntryPoint.class.getCanonicalName());

        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

        JobDataMap telematicTaskData = new JobDataMap();
        telematicTaskData.put("telematicDirectory", config.getTelematicDirectory());
        telematicTaskData.put("database", "asu-pbk");
        telematicTaskData.put("filenameFormat", new SimpleDateFormat(config.getTelematicFileFormat()));

        JobDetail telematicWriter = newJob(TelematicTask.class).withIdentity("telematic", "group1")
                .usingJobData(telematicTaskData).build();
        SimpleTrigger telematicTrigger = newTrigger()
                .withIdentity("telematicTrigger", "group1")
                .startNow()
                .withSchedule(simpleSchedule()
                        .withIntervalInHours(24).repeatForever())
                .build();

        JobDataMap stopSequenceTaskData = new JobDataMap();
        stopSequenceTaskData.put("stopSequenceDirectory", config.getStopSequenceDirectory());
        stopSequenceTaskData.put("database", "asu-pbk");
        stopSequenceTaskData.put("filenameFormat", new SimpleDateFormat(config.getStopSequenceFileFormat()));
        stopSequenceTaskData.put("noAtmSuffix", config.getStopSequenceNoAtmSuffix());

        JobDetail stopSequenceWriter = newJob(StopTask.class).withIdentity("stopSequence", "group1")
                .usingJobData(stopSequenceTaskData).build();
        JobDetail stopSequenceWriterWithoutAtm = newJob(StopWithoutAtmTask.class).withIdentity("stopSequenceWithoutAtm", "group1")
                .usingJobData(stopSequenceTaskData).build();

        SimpleTrigger stopSequenceTrigger = newTrigger()
                .withIdentity("stopSequenceTrigger", "group1")
                .startNow()
                .withSchedule(simpleSchedule()
                        //.withIntervalInHours(24).repeatForever())
                        .withIntervalInMinutes(5).repeatForever())
                .build();

        SimpleTrigger stopSequenceTrigger2 = newTrigger()
                .withIdentity("stopSequenceTrigger2", "group1")
                .startNow()
                .withSchedule(simpleSchedule()
                        //.withIntervalInHours(24).repeatForever())
                        .withIntervalInMinutes(5).repeatForever())
                .build();

        JobDataMap transportWorkData = new JobDataMap();
        transportWorkData.put("transportWorkDirectory", config.getTransportWorkDirectory());
        transportWorkData.put("closedRoutesMap", new HashMap());

        JobDetail transportWorkWriter = newJob(TransportWorkTask.class).withIdentity("transportWork", "group1")
                .usingJobData(transportWorkData).build();

        SimpleTrigger transportWorkTrigger = newTrigger()
                .withIdentity("transportWorkTrigger", "group1")
                .startNow()
                .withSchedule(simpleSchedule()
                        .withIntervalInMinutes(config.getTransportWorkPeriod()).repeatForever())
                .build();

        JobDataMap cleanerStopSequenceData = new JobDataMap();
        cleanerStopSequenceData.put("logDir", config.getStopSequenceDirectory());
        cleanerStopSequenceData.put("logTimeout", 30);

        JobDetail cleanerStopSequenceJob = newJob(FileCleanerTask.class).withIdentity("stopSequenceCleaner", "group1")
                .usingJobData(cleanerStopSequenceData).build();

        SimpleTrigger cleanerStopSequenceTrigger = newTrigger()
                .withIdentity("cleanerStopSequenceTrigger", "group1")
                .startNow()
                .withSchedule(simpleSchedule()
                        .withIntervalInHours(24).repeatForever())
                .build();

        JobDataMap cleanerTelematicData = new JobDataMap();
        cleanerTelematicData.put("logDir", config.getTelematicDirectory());
        cleanerTelematicData.put("logTimeout", 30);

        JobDetail cleanerTelematicJob = newJob(FileCleanerTask.class).withIdentity("telematicCleaner", "group1")
                .usingJobData(cleanerTelematicData).build();

        SimpleTrigger cleanerTelematicTrigger = newTrigger()
                .withIdentity("cleanerTelematicTrigger", "group1")
                .startNow()
                .withSchedule(simpleSchedule()
                        .withIntervalInHours(24).repeatForever())
                .build();

        try {
            scheduler.scheduleJob(telematicWriter, telematicTrigger);
            scheduler.scheduleJob(stopSequenceWriter, stopSequenceTrigger);
            scheduler.scheduleJob(stopSequenceWriterWithoutAtm, stopSequenceTrigger2);
            scheduler.scheduleJob(transportWorkWriter, transportWorkTrigger);
            scheduler.scheduleJob(cleanerStopSequenceJob, cleanerStopSequenceTrigger);
            scheduler.scheduleJob(cleanerTelematicJob, cleanerTelematicTrigger);
            scheduler.start();

            SLF4JBridgeHandler.removeHandlersForRootLogger();
            SLF4JBridgeHandler.install();
            jettyServer.start();
            jettyServer.join();
        } finally {
            jettyServer.destroy();
        }
    }
}
