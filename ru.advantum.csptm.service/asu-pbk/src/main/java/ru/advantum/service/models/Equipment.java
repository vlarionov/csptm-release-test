package ru.advantum.service.models;

/**
 * Класс - сущность бортового оборудования
 */
public class Equipment {
    private String equipment_id;
    private Long vehicle_id;
    private String gos_num;
    private String type;

    public String getEquipmentId() {
        return equipment_id;
    }

    public Long getVehicleId() {
        return vehicle_id;
    }

    public String getType() {
        return type;
    }

    public String getGosNumber() {
        return gos_num;
    }
}

