package ru.advantum.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Класс работы с файлами
 */
public class FileUtils {

    private static SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");

    /**
     *
     * @param filePath путь до файла
     * @return true если файл был успешно создан, false если нет
     * @throws IOException
     *
     * Создает файл если такого не существует
     */
    public static void createFileIfNotExist(String filePath) throws IOException {
        if (!Files.exists(Paths.get(filePath))) {
            Files.createFile(Paths.get(filePath));
        }
    }

    /**
     *
     * @param directory путь до директории
     * @return true если директория создана, false если нет
     * @throws IOException
     *
     * Создает директорию по указанному пути если такой не существует
     */
    public static void createDirIfNotExist(String directory) throws IOException {
        if (!Files.exists(Paths.get(directory))) {
            Files.createDirectories(Paths.get(directory));
        }
    }

    /**
     *
     * @param date дата
     * @param precision точность
     * @param rootDir корневая директория
     * @return Путь до директории за указанную дату, с указанной точностью и указанной корневой директорией
     * @throws IOException
     *
     * Строит путь до директории по дате, точности и корневой директории
     */
    public static String getDateDirs(Date date, String precision, String rootDir) throws IOException {
        String dateString = format.format(date);
        return getDateDirs(dateString, precision, rootDir);
    }

    /**
     *
     * @param dateString дата
     * @param precision точность
     * @param rootDir корневая директория
     * @return Путь до директории за указанную дату, с указанной точностью и указанной корневой директорией
     * @throws IOException
     *
     * Строит путь до директории по дате, точности и корневой директории
     */
    public static String getDateDirs(String dateString, String precision, String rootDir) throws IOException {
        StringBuilder directory = new StringBuilder();
        switch (precision) {
            case "second": {
                directory.insert(0, dateString.substring(12, dateString.length()));
                directory.insert(0, "/");
            }
            case "minute": {
                directory.insert(0, dateString.substring(10, 12));
                directory.insert(0, "/");
            }
            case "hour": {
                directory.insert(0, dateString.substring(8, 10));
                directory.insert(0, "/");
            }
            case "day": {
                directory.insert(0, dateString.substring(6, 8));
                directory.insert(0, "/");
            }
            case "month": {
                directory.insert(0, dateString.substring(4, 6));
                directory.insert(0, "/");
            }
            case "year": {
                directory.insert(0, dateString.substring(0, 4));
                directory.insert(0, "/");
            }
        }

        directory.insert(0, rootDir);
        String resultDir = directory.toString();

        return resultDir;
    }
}
