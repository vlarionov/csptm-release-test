package ru.advantum.csptm.service;

import org.simpleframework.xml.Attribute;


public class FileConfig {
    @Attribute(required = false)
    private String name;

    @Attribute(required = false)
    private String copyParam;

    @Attribute(name = "post", required = false)
    private String postProcessingScript;

    @Attribute(name = "before", required = false)
    private String beforeProcessingScript;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCopyParam() {
        return copyParam;
    }

    public void setCopyParam(String copyParam) {
        this.copyParam = copyParam;
    }

    public String getPostProcessingScript() {
        return postProcessingScript;
    }

    public void setPostProcessingScript(String postProcessingScript) {
        this.postProcessingScript = postProcessingScript;
    }

    public String getBeforeProcessingScript() {
        return beforeProcessingScript;
    }

    public void setBeforeProcessingScript(String beforeProcessingScript) {
        this.beforeProcessingScript = beforeProcessingScript;
    }
}
