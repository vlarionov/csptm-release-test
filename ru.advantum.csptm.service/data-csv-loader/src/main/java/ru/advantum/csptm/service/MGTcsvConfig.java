package ru.advantum.csptm.service;


import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.config.common.file.PathConfig;

import java.util.ArrayList;
import java.util.List;

@Root(name = "mgt-csv-loader")
public class MGTcsvConfig {
    @Element(name = "database")
    private DatabaseConnection databaseConnection;

    @ElementList(name = "file-names", entry = "file")
    private List<FileConfig> filaNames = new ArrayList<FileConfig>();

    @Element(name = "path")
    private PathConfig pathConfig;

    public List<FileConfig> getFilaNames() {
        return filaNames;
    }

    public void setFilaNames(List<FileConfig> filaNames) {
        this.filaNames = filaNames;
    }

    public DatabaseConnection getDatabaseConnection() {
        return databaseConnection;
    }

    public void setDatabaseConnection(DatabaseConnection databaseConnection) {
        this.databaseConnection = databaseConnection;
    }

    public PathConfig getPathConfig() {
        return pathConfig;
    }

    public void setPathConfig(PathConfig pathConfig) {
        this.pathConfig = pathConfig;
    }
}
