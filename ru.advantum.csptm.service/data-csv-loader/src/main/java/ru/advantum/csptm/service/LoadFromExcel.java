package Loader;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Stream.of;
import static org.apache.commons.dbutils.DbUtils.closeQuietly;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.math.NumberUtils.toDouble;
import static org.apache.commons.lang3.time.DateUtils.parseDate;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Старт!");

        final String url = "jdbc:postgresql://172.16.21.31:5432/csptm-dev";
        final String username = "adv";
        final String password = "adv";
        final String autoCommit = "false";
        final String poolable = "true";
        final String sheetName = "ТС-БО";

        final int licenseColumnNum = 4;

        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String cellValue;
        //List<String> licenceTr = new ArrayList<String>();
        List<Integer> columnNumForAnalize = of(4, 21).collect(toCollection(ArrayList::new));
        /*String queryTs = "select COUNT(*) from ifmp.i18_tr i18 WHERE i18.licence NOT IN ";
        String insertTs =
                "INSERT INTO ifmp.i18_equipment " +
                        "(garage_num,depo_name,tr_status_name,territory_name_short,tr_model_name,tr_type_short_name," +
                        "licence,serial_num,dt_begin,year_of_issue,garage_num_add,equipment_afixed,install_name) " +
                        "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);";*/

        final String megreTS =
                //"INSERT INTO core.tr_test(seat_qty, has_low_floor, has_conditioner, garage_num, licence, serial_num, dt_begin, tr_type_id, " +
                "INSERT INTO core.tr(seat_qty, has_low_floor, has_conditioner, garage_num, licence, serial_num, dt_begin, tr_type_id, " +
                        "                    tr_status_id, tr_model_id, has_wheelchair_space, has_bicicle_space, seat_qty_disabled_people, " +
                        "                    seat_qty_total, depo_id, year_of_issue, tr_schema_install_id, equipment_afixed, territory_id) " +
                        "VALUES(?,  ?,  ?,  ?,  ?,  ?, ?,  ?,  ?,  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) " +
                        //"ON CONFLICT ON CONSTRAINT UNQ_garage_num_depo_test DO UPDATE " +
                        "ON CONFLICT ON CONSTRAINT UNQ_garage_num_depo DO UPDATE " +
                        "SET seat_qty = EXCLUDED.seat_qty, has_low_floor = EXCLUDED.has_low_floor, has_conditioner = EXCLUDED.has_conditioner, garage_num = EXCLUDED.garage_num, " +
                        "licence = EXCLUDED.licence, serial_num = EXCLUDED.serial_num, dt_begin = EXCLUDED.dt_begin, tr_type_id = EXCLUDED.tr_type_id, " +
                        "tr_status_id = EXCLUDED.tr_status_id, tr_model_id = EXCLUDED.tr_model_id, has_wheelchair_space = EXCLUDED.has_wheelchair_space, " +
                        "has_bicicle_space = EXCLUDED.has_bicicle_space, seat_qty_disabled_people = EXCLUDED.seat_qty_disabled_people, seat_qty_total = EXCLUDED.seat_qty_total, " +
                        "depo_id = EXCLUDED.depo_id, year_of_issue = EXCLUDED.year_of_issue, tr_schema_install_id = EXCLUDED.tr_schema_install_id, " +
                        "equipment_afixed = 'updated', territory_id = EXCLUDED.territory_id;";

        final String insertEquipmentSql =
                //"INSERT INTO core.equipment_test(firmware_id, unit_service_type_id, facility_id, equipment_status_id, serial_num, " +
                "INSERT INTO core.equipment(firmware_id, unit_service_type_id, facility_id, equipment_status_id, serial_num, " +
                        "            equipment_model_id, depo_id, territory_id, unit_service_facility_id, external_id, comment, dt_begin) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, localtimestamp) " +
                        "ON CONFLICT (external_id) DO UPDATE " +
                        "SET firmware_id = EXCLUDED.firmware_id, unit_service_type_id = EXCLUDED.unit_service_type_id , facility_id = EXCLUDED.facility_id, " +
                        "  equipment_status_id = EXCLUDED.equipment_status_id, serial_num = EXCLUDED.serial_num, " +
                        "  equipment_model_id = EXCLUDED.equipment_model_id,  depo_id = EXCLUDED.depo_id, territory_id = EXCLUDED.territory_id, " +
                        "  unit_service_facility_id = EXCLUDED.unit_service_facility_id, external_id = EXCLUDED.external_id, comment = 'updated';";

        final String insertUnit2tr = //"INSERT INTO core.unit2tr_test(tr_id, unit_id, sys_period) VALUES(?, ?, tstzrange(now(), NULL::timestamp with time zone)) " +
                "INSERT INTO core.unit2tr(tr_id, unit_id, sys_period) VALUES(?, ?, tstzrange(now(), NULL::timestamp with time zone)) " +
                        "ON CONFLICT (unit_id) DO UPDATE " +
                        "SET tr_id = EXCLUDED.tr_id, sys_period = EXCLUDED.sys_period;";
        final String insertUnitSql = //"INSERT INTO core.unit_test(unit_id, unit_num, hub_id) VALUES (?,?,?) " +
                "INSERT INTO core.unit(unit_id, unit_num, hub_id) VALUES (?,?,?) " +
                        "ON CONFLICT (unit_id) DO UPDATE " +
                        "SET unit_num = EXCLUDED.unit_num, hub_id = EXCLUDED.hub_id;";

        final String insertUnitBnst = "INSERT INTO core.unit_bnst(has_manipulator, unit_bnst_id) " +
                "VALUES (?, ? ) ON CONFLICT (unit_bnst_id) " +
                "DO UPDATE SET has_manipulator = EXCLUDED.has_manipulator, unit_bnst_id = EXCLUDED.unit_bnst_id;";

        final String insertAsmppEquipment = "INSERT INTO core.equipment(equipment_id, equipment_model_id, equipment_status_id, " +
                "unit_service_type_id, unit_service_facility_id, facility_id, dt_begin, depo_id, comment, territory_id) VALUES (?,?,?,?,?,?,?,?, 'irma',?) " +
                "ON CONFLICT DO NOTHING;";

        final String insertSimCard = "insert into core.sim_card(dt_begin, iccid, phone_num, mobile_tariff_id, sim_card_group_id, " +
                "equipment_status_id, depo_id, territory_id) VALUES (?, ?, ?, ?, ?,?, ?, ?) ON CONFLICT DO NOTHING;";
        // парметры ТС - core
        /*int seat_qty = 0;
        boolean has_low_floor = false;
        boolean has_conditioner = false;
        int garage_num = 0;
        String licence = "";
        String serial_num = "";
        Date dt_begin;*/
        long tr_type_id = 0;
        long tr_status_id = 0;
        long tr_model_id = 0;
        long territory_id = 0;
        /*boolean has_wheelchair_space = false;
        boolean has_bicicle_space = false;
        boolean seat_qty_disabled_people = false;
        long seat_qty_total = 0;*/
        long depo_id = 0;
        /*long year_of_issue = 0;*/
        long tr_schema_install_id = 0;
        long irmaCounter = 0;
        // параметры ТС файл  -----------------------+++++++++++++++++++++++***************************
        String filial = "";
        String nameShort = "";
        int garageNum = -1;
        String gosNum = "";
        int yearIssue = -1;
        String serialNum = "";
        Date dateIntro = new Date();
        String model = "";
        String type = "";
        String state = "";
        String schema = "";
        int totalSeats = -1;
        int chairSeats = -1;
        int standSeats = -1;
        boolean bikeSeat = false;
        boolean disabledSeat = false;
        boolean lowFloor = false;
        boolean disabledMountKit = false;
        boolean updateResult;
        //**************************************************************************************************************
        long blokId = 0;
        long blokSerial = 0;
        String blokModel = "";
        String blokStatus = "";
        String TOstate = "";
        String serviceCompany = "";
        String firmvare = "";
        long manipulatorNum = -1;
        //++++++++++++++++
        long firmware_id = -1;
        long unit_service_type_id = -1;
        long facility_id = -1;
        long equipment_status_id = -1;
        long equipment_model_id = -1;
        long unit_service_facility_id = -1;

        long ts_id = 0;
        long equipment_id = 0;
        long unit_id = -1;
        long unit2_id = -1;
        List<String> cellValues = new ArrayList<String>();

        String asmppModel = "";
        String asmppStatus = "";
        String asmppTOvid = "";
        String asmppServiceCompany = "";

        String iccid = "";
        String phone_num = "";
        int sim_card_id = -1;
        //***************** блок 2
        String blok2ID = "";
        String blok2Serial = "";
        String blok2Model = "";
        String blok2Status = "";
        String blok2TOvid = "";
        String blok2ServiceCompany = "";
        //String blok2Firmvare = ""; // не используется
        int blok2ChannelNum = 0;
        int blok2ManipulatorNumer = 0;

        int blok2Counter = 0;
        // ---- ДУТ
        String DUT1model = "";
        String DUT1status = "";
        String DUT1TOvid = "";
        String DUT1serviceCompany = "";

        String DUT2model = "";
        String DUT2status = "";
        String DUT2TOvid = "";
        String DUT2serviceCompany = "";
        //********************************
        String kbtobSerial = "";
        String kbtobModel = "";
        String kbtobStatus = "";
        String kbtobTOvid = "";
        String kbtobTOserviceCompany = "";

        int alarmButton = -1;
        int fireDetector = -1;
        List<Long> alarmButtonBloks = new ArrayList<>();
        int hdd = -1;
        int videoCameras = -1;
        int blok1Manipulator = -1;
        try {
            conn = DriverManager.getConnection(url, username, password);

            // на время отлпдки
            //conn.setAutoCommit(false);

            File dataFile = new File("D:\\Work\\v.2.1.1.xlsx");
            Workbook wb = new XSSFWorkbook(dataFile);
            Sheet sheet = wb.getSheet(sheetName);
            Row row;
            Cell cell;
            int columnLimit;
            int rowLimit = 2;
            int lastRowNum = sheet.getLastRowNum();

            /*System.out.println("Число листов: " + wb.getNumberOfSheets());
            System.out.println("Строк в листе: " + sheet.getLastRowNum());*/

            for (int i = 1; i < lastRowNum /*rowLimit*/; i++) {
                row = sheet.getRow(i);
                if (row == null) {
                    continue;
                }
                columnLimit = 155;

                //cellValues.clear();
                lowFloor = false;
                disabledSeat = false;
                bikeSeat = false;
                disabledMountKit = false;
                standSeats = 0;
                chairSeats = 0;
                totalSeats = 0;

                nameShort = "";
                filial = "";
                garageNum = -1;
                gosNum = "";
                serialNum = "";

                asmppModel = "";
                asmppStatus = "";
                asmppTOvid = "";
                asmppServiceCompany = "";

                iccid = "";
                phone_num = "";
                sim_card_id = -1;

                blok2ID = "";
                blok2Serial = "";
                blok2Model = "";
                blok2Status = "";
                blok2TOvid = "";
                blok2ServiceCompany = "";
                blok2ChannelNum = 0;
                blok2ManipulatorNumer = 0;

                DUT1model = "";
                DUT1status = "";
                DUT1TOvid = "";
                DUT1serviceCompany = "";

                DUT2model = "";
                DUT2status = "";
                DUT2TOvid = "";
                DUT2serviceCompany = "";

                blokModel = "";
                equipment_id = -1;
                unit_id = -1;

                kbtobSerial = "";
                kbtobModel = "";
                kbtobStatus = "";
                kbtobTOvid = "";
                kbtobTOserviceCompany = "";

                alarmButton = -1;
                fireDetector = -1;
                hdd = -1;
                videoCameras = -1;
                blok1Manipulator = -1;
                for (int j = 0; j < columnLimit; j++) {
                    //for(int j : columnNumForAnalize){
                    cell = row.getCell(j);
                    if (cell == null || cell.toString().equalsIgnoreCase("-")) {
                        continue;
                    }
                    cellValue = getStringCellValue(cell);
                    cellValues.add(cellValue);
                    /*if (j == licenseColumnNum && i > 0 && isNotBlank(cellValue)) {
                        licenceTr.add(cellValue);
                    }*/
                    switch (j + 1) {
                        case 1:
                            filial = cellValue;
                            break;
                        case 3:
                            nameShort = cellValue;
                            break;
                        case 4:
                            garageNum = ((int) toDouble(cellValue));
                            break;
                        case 5:
                            gosNum = cellValue;
                            break;
                        case 6:
                            yearIssue = (int) toDouble(cellValue);
                            break;
                        case 7:
                            serialNum = cellValue;
                            break;
                        case 8:
                            try {
                                dateIntro = new Date();
                                dateIntro = parseDate(cellValue, "dd-MMM-yyyy");
                            } catch (Exception e) {
                            }
                            break;
                        case 9:
                            model = cellValue;
                            break;
                        case 10:
                            type = cellValue;
                            break;
                        case 11:
                            state = cellValue;
                            break;
                        case 12:
                            schema = cellValue;
                            break;
                        case 13:
                            totalSeats = (int) toDouble(cellValue);
                        case 14:
                            chairSeats = (int) toDouble(cellValue);
                            break;
                        case 15:
                            standSeats = (int) toDouble(cellValue);
                            break;
                        case 16:
                            bikeSeat = cellValue.equals("+") ? true : false;
                            break;
                        case 17:
                            disabledSeat = cellValue.equals("+") ? true : false;
                            break;
                        case 18:
                            lowFloor = (cellValue.equals("-") || cellValue.equals("0")) ? false : true;
                            break;
                        case 19:
                            disabledMountKit = cellValue.equals("+") ? true : false;
                            break;
                        //**********************************************************************************************
                        // БЛОК 1
                        case 21:
                            blokId = (int) toDouble(cellValue);
                            break;
                        case 22:
                            blokSerial = (int) toDouble(cellValue);
                            break;
                        case 23:
                            blokModel = cellValue;
                            break;
                        case 24:
                            blokStatus = cellValue;
                            break;
                        case 25:
                            TOstate = cellValue;
                            break;
                        case 26:
                            serviceCompany = cellValue;
                            break;
                        case 27:
                            firmvare = cellValue;
                            break;
                        case 28:
                            manipulatorNum = (int) toDouble(cellValue);
                            if (manipulatorNum < 0) {
                                manipulatorNum = 1;
                            }
                            break;
                        // ДУТ1
                        case 32:
                            DUT1model = cellValue;
                            break;
                        case 33:
                            DUT1status = cellValue;
                            break;
                        case 34:
                            DUT1TOvid = cellValue;
                            break;
                        case 35:
                            DUT1serviceCompany = cellValue;
                            break;
                        // ДУТ2
                        case 39:
                            DUT2model = cellValue;
                            break;
                        case 40:
                            DUT2status = cellValue;
                            break;
                        case 41:
                            DUT2TOvid = cellValue;
                            break;
                        case 42:
                            DUT2serviceCompany = cellValue;
                            break;
                        // АСМПП
                        case 46: // модель, идентификатор, и серийний номер не передаются
                            asmppModel = cellValue;
                            break;
                        case 47: // статус асмпп
                            asmppStatus = cellValue;
                            break;
                        case 48: // вид ТО
                            asmppTOvid = cellValue;
                            break;
                        case 49: // обслуживающая компания
                            asmppServiceCompany = cellValue;
                            break;
                        //******** сим карта блока 1
                        case 59:
                            iccid = cellValue;
                            break;
                        case 60:
                            phone_num = cellValue;
                            break;
                        case 62: // блок 1 манипулятор
                            blok1Manipulator = (int) toDouble(cellValue, -1.0);
                            break;
                        //***** блок 2   ************************     ********************       **************    *****
                        case 69:
                            blok2ID = cellValue.replace(".0", "");// убрать 0 после точки
                            break;
                        case 70:
                            if (toDouble(cellValue) > 0.0) {
                                blok2Serial = String.valueOf((long) toDouble(cellValue));
                            } else {
                                blok2Serial = cellValue;
                            }
                            break;
                        case 71:
                            blok2Model = cellValue;
                            break;
                        case 72:
                            blok2Status = cellValue;
                            break;
                        case 73:
                            blok2TOvid = cellValue;
                            break;
                        case 74:
                            blok2ServiceCompany = cellValue;
                            break;
                        /*case 74: // прошивка
                            break;*/
                        case 76:
                            blok2ChannelNum = (int) toDouble(cellValue);
                            break;
                        case 77:
                            blok2ManipulatorNumer = (int) toDouble(cellValue);
                            break;
                        // КБТОБ
                        case 90:
                            kbtobSerial = cellValue;
                            break;
                        case 91:
                            kbtobModel = cellValue;
                            if (kbtobModel.equalsIgnoreCase("#REF!")) {
                                kbtobModel = "Неопределено";
                            }
                            break;
                        case 92:
                            kbtobStatus = cellValue;
                            break;
                        case 93:
                            kbtobTOvid = cellValue;
                            break;
                        case 94:
                            kbtobTOserviceCompany = cellValue;
                            break;
                        case 117:
                            hdd = (int) toDouble(cellValue, -1.0);
                            break;
                        // пожарный датчик ИПК
                        case 134:
                            fireDetector = (int) toDouble(cellValue, -1.0);
                            break;
                        // тревожная кнопка
                        case 141:
                            alarmButton = (int) toDouble(cellValue, -1.0);
                            break;
                        //
                        case 148:
                            videoCameras = (int) toDouble(cellValue, -1.0);
                            break;
                    }
                }// iterate cells
                // тип ТС
                pstmt = conn.prepareStatement("select tr_type_id from core.tr_type WHERE UPPER(short_name) = ?;");
                pstmt.setString(1, type.toUpperCase());
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    tr_type_id = rs.getLong(1);
                }
                // статус - работают на линии, например
                pstmt = conn.prepareStatement(" select tr_status_id from core.tr_status WHERE name = ?;");
                pstmt.setString(1, state);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    tr_status_id = rs.getLong(1);
                }
                // модель ТС
                pstmt = conn.prepareStatement("select tr_model_id from core.tr_model WHERE name = UPPER(?);");
                pstmt.setString(1, model);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    tr_model_id = rs.getLong(1);
                }
                // депо id nameShort
                pstmt = conn.prepareStatement("select entity_id from core.entity WHERE name_short = ?;");
                pstmt.setString(1, filial);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    depo_id = rs.getLong(1);
                }
                // schema id
                pstmt = conn.prepareStatement
                        ("select tr_schema_install_id from core.tr_schema_install WHERE install_name = ?;");
                pstmt.setString(1, schema);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    tr_schema_install_id = rs.getLong(1);
                }
                // территория
                pstmt = conn.prepareStatement
                        /*("SELECT d2t.territory_id from core.entity e JOIN core.depo ON depo.depo_id = e.entity_id " +
                                "JOIN core.depo2territory d2t ON d2t.depo_id =  depo.depo_id WHERE e.name_short = ? ;");*/
                                ("select entity_id from core.entity WHERE name_short = ?;");
                pstmt.setString(1, nameShort);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    territory_id = rs.getLong(1);
                }
                // поиск ТС для обновления
                /*pstmt = conn.prepareStatement
                        ("select tr_id from core.tr WHERE garage_num = ? AND depo_id = ? ;");
                pstmt.setLong(1, garageNum);
                pstmt.setLong(2, depo_id);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    System.out.println("ТС id: " + rs.getLong(1) + "; i: " + i);*/
                // делаем обновление
                pstmt = conn.prepareStatement(megreTS);
                pstmt.setLong(1, chairSeats);
                pstmt.setBoolean(2, lowFloor);

                pstmt.setBoolean(3, false);// +++++++++
                pstmt.setLong(4, garageNum);
                pstmt.setString(5, gosNum);
                pstmt.setString(6, serialNum);
                pstmt.setDate(7, new java.sql.Date(dateIntro.getTime()));
                pstmt.setLong(8, tr_type_id);
                pstmt.setLong(9, tr_status_id);
                pstmt.setLong(10, tr_model_id);
                pstmt.setBoolean(11, disabledSeat);
                pstmt.setBoolean(12, bikeSeat);
                pstmt.setLong(13, 0); // +++++++++
                pstmt.setLong(14, totalSeats);
                pstmt.setLong(15, depo_id);
                pstmt.setLong(16, yearIssue);
                pstmt.setLong(17, tr_schema_install_id);
                pstmt.setString(18, "new");
                pstmt.setLong(19, territory_id);
                //updateResult = pstmt.execute();
                //System.out.println("Update res: " + updateResult);
                /*} else {
                    System.out.println("NOt found: " + garageNum + "; депо " + depo_id);
                }*/
                // id обновленного или вставленного ТС
                pstmt = conn.prepareStatement
                        ("select tr_id from core.tr WHERE garage_num = ? AND depo_id = ? ;");
                pstmt.setLong(1, garageNum);
                pstmt.setLong(2, depo_id);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    ts_id = rs.getLong(1);
                    System.out.println("ТС id: " + rs.getLong(1) + "; i: " + i);
                }

                if (isNotBlank(blokModel)) {
                    //******* firmware
                    pstmt = conn.prepareStatement("select firmware_id from core.firmware where trim(name) = ? ;");
                    pstmt.setString(1, firmvare);
                    rs = pstmt.executeQuery();
                    while (rs.next()) {
                        firmware_id = rs.getLong(1);
                    }
                    //
                    pstmt = conn.prepareStatement("select unit_service_type_id from core.unit_service_type where trim(name) = ? ;");
                    pstmt.setString(1, TOstate);
                    rs = pstmt.executeQuery();
                    while (rs.next()) {
                        unit_service_type_id = rs.getLong(1);
                    }
                    //
                    pstmt = conn.prepareStatement("select facility_id from core.equipment_model  where trim(name) = ?;");
                    pstmt.setString(1, blokModel);
                    rs = pstmt.executeQuery();
                    while (rs.next()) {
                        facility_id = rs.getLong(1);
                    }
                    //
                    pstmt = conn.prepareStatement("select equipment_status_id from core.equipment_status  where trim(name) = ?;");
                    pstmt.setString(1, blokStatus);
                    rs = pstmt.executeQuery();
                    while (rs.next()) {
                        equipment_status_id = rs.getLong(1);
                    }
                    //
                    pstmt = conn.prepareStatement("select equipment_model_id from core.equipment_model  where trim(name) = ?;");
                    pstmt.setString(1, blokModel);
                    rs = pstmt.executeQuery();
                    while (rs.next()) {
                        equipment_model_id = rs.getLong(1);
                    }
                    //
                    pstmt = conn.prepareStatement("select entity_id from core.entity where name_short = ?;");
                    pstmt.setString(1, serviceCompany);
                    rs = pstmt.executeQuery();
                    while (rs.next()) {
                        unit_service_facility_id = rs.getLong(1);
                    }
                    // insert or update equipment
                    pstmt = conn.prepareStatement(insertEquipmentSql);
                    pstmt.setLong(1, firmware_id);
                    pstmt.setLong(2, unit_service_type_id);
                    pstmt.setLong(3, facility_id);// +++++++++
                    pstmt.setLong(4, equipment_status_id);
                    pstmt.setLong(5, blokSerial);
                    pstmt.setLong(6, equipment_model_id);
                    pstmt.setLong(7, depo_id);
                    pstmt.setLong(8, territory_id);
                    pstmt.setLong(9, unit_service_facility_id);
                    pstmt.setLong(10, blokId); // ext id
                    pstmt.setString(11, "new"); // coment
                    //updateResult = pstmt.execute();
                    // id блока, который вставили или обновили
                    pstmt = conn.prepareStatement
                            ("select equipment_id from core.equipment where external_id = ?; ");
                    pstmt.setString(1, String.valueOf(blokId));
                    rs = pstmt.executeQuery();
                    if (rs.next()) {
                        equipment_id = rs.getLong(1);
                        unit_id = equipment_id;
                    }
                    //******************************************************************************************************
                    // вставка блоков
                /*pstmt = conn.prepareStatement(insertUnitSql);
                pstmt.setLong(1, equipment_id); // unit_id
                pstmt.setString(2, String.valueOf(blokId));// unit_num,
                pstmt.setLong(3, 66); // hub_id
                pstmt.execute();*/
                    // вставка связки блока и ТС
                /*pstmt = conn.prepareStatement(insertUnit2tr);
                pstmt.setLong(1, ts_id);
                pstmt.setLong(2, equipment_id);
                updateResult = pstmt.execute();*/
                    // init bnst
                /*pstmt = conn.prepareStatement(insertUnitBnst);
                pstmt.setLong(1, manipulatorNum);
                pstmt.setLong(2, equipment_id);
                updateResult = pstmt.execute();*/
                    //System.out.println(join(cellValues.iterator(), "; "));
                }
                // АСМПП
                if (isNotBlank(asmppModel) && isNotBlank(asmppStatus) && isNotBlank(asmppTOvid) &&
                        isNotBlank(asmppServiceCompany)) {
                    irmaCounter++;
                    System.out.println("IRMA counter: " + irmaCounter);
                    pstmt = conn.prepareStatement("select equipment_model_id, facility_id from core.equipment_model  where trim(name) = ?;");
                    pstmt.setString(1, asmppModel);
                    rs = pstmt.executeQuery();
                    while (rs.next()) {
                        equipment_model_id = rs.getLong(1);
                        facility_id = rs.getLong(2);
                    }
                    pstmt = conn.prepareStatement("select equipment_status_id from core.equipment_status  where trim(name) = ?;");
                    pstmt.setString(1, asmppStatus);
                    rs = pstmt.executeQuery();
                    while (rs.next()) {
                        equipment_status_id = rs.getLong(1);
                    }
                    // Вид ТО
                    pstmt = conn.prepareStatement("select unit_service_type_id from core.unit_service_type where trim(name) = ? ;");
                    pstmt.setString(1, asmppTOvid);
                    rs = pstmt.executeQuery();
                    while (rs.next()) {
                        unit_service_type_id = rs.getLong(1);
                    }
                    // обслуживающая компания
                    pstmt = conn.prepareStatement("select entity_id from core.entity where name_short = ?;");
                    pstmt.setString(1, asmppServiceCompany);
                    rs = pstmt.executeQuery();
                    while (rs.next()) {
                        unit_service_facility_id = rs.getLong(1);
                    }
                    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    /*pstmt = conn.prepareStatement("select * from nextval('core.equipment_equipment_id_seq'::regclass)");
                    rs = pstmt.executeQuery();
                    rs.next();
                    equipment_id = rs.getLong(1);*/
                    pstmt = conn.prepareStatement(insertAsmppEquipment);
                    pstmt.setLong(1, equipment_id);
                    pstmt.setLong(2, equipment_model_id); // model id
                    pstmt.setLong(3, equipment_status_id);// status id
                    pstmt.setLong(4, unit_service_type_id);// service type_id
                    pstmt.setLong(5, unit_service_facility_id);// service facility id
                    pstmt.setLong(6, facility_id);// facility id
                    pstmt.setDate(7, new java.sql.Date(new Date().getTime()));
                    pstmt.setLong(8, depo_id);
                    pstmt.setLong(9, territory_id);
                    //pstmt.execute();
                    pstmt = conn.prepareStatement("INSERT INTO core.sensor(sensor_id, sensor_num) VALUES(?, 400) ON CONFLICT DO NOTHING;");
                    pstmt.setLong(1, equipment_id);
                    //pstmt.execute();
                    //
                    pstmt = conn.prepareStatement("INSERT INTO core.sensor2tr(sensor_id, tr_id, installation_site_id) VALUES (?,?,5) ON CONFLICT DO NOTHING;");
                    pstmt.setLong(1, equipment_id);
                    pstmt.setLong(2, ts_id);
                    //pstmt.execute();
                    //
                    pstmt = conn.prepareStatement("insert INTO core.sensor2unit(sensor_id, unit_id, sensor_num) VALUES (?,?,400) ON CONFLICT DO NOTHING;");
                    pstmt.setLong(1, equipment_id);
                    pstmt.setLong(2, unit_id);
                    //pstmt.execute();
                    pstmt = conn.prepareStatement("insert into core.asmpp(sensor_id, doors_count) VALUES (?, 3) ON CONFLICT DO NOTHING;");
                    pstmt.setLong(1, equipment_id);
                    //pstmt.execute();
                }// асмпп
                //**************************************************************************************************
                // sim карта блока 1
                /*if (isNotBlank(iccid) && isNotBlank(phone_num)) {
                    pstmt = conn.prepareStatement(insertSimCard);
                    pstmt.setDate(1, new java.sql.Date(new Date().getTime()));
                    pstmt.setString(2, iccid);
                    pstmt.setString(3, String.valueOf((long) (toDouble(phone_num))));
                    pstmt.setLong(4, 14); // один тариф
                    pstmt.setLong(5, 16);// одна группа sim
                    pstmt.setLong(6, 2);// статус оборудования - исправно
                    pstmt.setLong(7, depo_id);
                    pstmt.setLong(8, territory_id);
                    //pstmt.execute();
                    pstmt = conn.prepareStatement("select sim_card_id FROM core.sim_card where iccid = ? AND phone_num = ? ;");
                    pstmt.setString(1, iccid);
                    pstmt.setString(2, String.valueOf((long) (toDouble(phone_num))));
                    rs = pstmt.executeQuery();
                    while (rs.next()) {
                        sim_card_id = rs.getInt(1);
                    }
                    pstmt = conn.prepareStatement("insert into core.sim_card2unit(sim_card_id, equipment_id) VALUES(?, ?);");
                    pstmt.setLong(1, sim_card_id);
                    pstmt.setLong(2, unit_id);
                    //pstmt.execute();
                }*/
                // блок 2
                /*if (isNotBlank(blok2Model)) {
                    pstmt = conn.prepareStatement("select equipment_model_id, facility_id from core.equipment_model  where trim(name) = ?;");
                    pstmt.setString(1, blok2Model);
                    rs = pstmt.executeQuery();
                    while (rs.next()) {
                        equipment_model_id = rs.getLong(1);
                        facility_id = rs.getLong(2);
                    }

                    //
                    pstmt = conn.prepareStatement("select equipment_status_id from core.equipment_status  where trim(name) = ?;");
                    pstmt.setString(1, blok2Status);
                    rs = pstmt.executeQuery();
                    while (rs.next()) {
                        equipment_status_id = rs.getLong(1);
                    }
                    //
                    pstmt = conn.prepareStatement("select unit_service_type_id from core.unit_service_type where trim(name) = ? ;");
                    pstmt.setString(1, blok2TOvid);
                    rs = pstmt.executeQuery();
                    while (rs.next()) {
                        unit_service_type_id = rs.getLong(1);
                    }
                    pstmt = conn.prepareStatement("select entity_id from core.entity where name_short = ?;");
                    pstmt.setString(1, blok2ServiceCompany);
                    rs = pstmt.executeQuery();
                    while (rs.next()) {
                        unit_service_facility_id = rs.getLong(1);
                    }
                    // insert or update equipment
                    pstmt = conn.prepareStatement(insertEquipmentSql);
                    pstmt.setNull(1, Types.BIGINT);
                    pstmt.setLong(2, unit_service_type_id);
                    pstmt.setLong(3, facility_id);// +++++++++
                    pstmt.setLong(4, equipment_status_id);
                    pstmt.setString(5, blok2Serial);
                    pstmt.setLong(6, equipment_model_id);
                    pstmt.setLong(7, depo_id);
                    pstmt.setLong(8, territory_id);
                    pstmt.setLong(9, unit_service_facility_id);
                    pstmt.setString(10, blok2ID); // ext id
                    pstmt.setString(11, "BNSR"); // coment
                    //pstmt.execute();
                    // id блока, который вставили или обновили
                    pstmt = conn.prepareStatement
                            ("select equipment_id from core.equipment where external_id = ?; ");
                    pstmt.setString(1, String.valueOf(blok2ID));
                    rs = pstmt.executeQuery();
                    unit2_id = -1;
                    equipment_id = -1;
                    if (rs.next()) {
                        equipment_id = rs.getLong(1);
                        unit2_id = equipment_id;
                    }
                    pstmt = conn.prepareStatement(insertUnitSql);
                    pstmt.setLong(1, unit2_id); // unit_id
                    pstmt.setString(2, blok2ID);// unit_num,
                    pstmt.setLong(3, 2); // hub_id
                    //pstmt.execute();
                    // вставка связки блока и ТС
                    pstmt = conn.prepareStatement(insertUnit2tr);
                    pstmt.setLong(1, ts_id);
                    pstmt.setLong(2, unit2_id);
                    //pstmt.execute();
                    pstmt = conn.prepareStatement
                            ("insert into core.unit_bnsr(unit_bnsr_id, channel_num, has_manipulator) VALUES (?,?,?) ON CONFLICT (unit_bnsr_id) DO UPDATE " +
                                    "SET unit_bnsr_id = EXCLUDED.unit_bnsr_id, channel_num = EXCLUDED.channel_num, has_manipulator = EXCLUDED.has_manipulator;");
                    pstmt.setLong(1, unit2_id);
                    pstmt.setLong(2, blok2ChannelNum);
                    pstmt.setLong(3, blok2ManipulatorNumer);
                    //pstmt.execute();

                    blok2Counter++;
                    System.out.println("Blok2 counter: " + blok2Counter);
                }*/
                // ДУТ (уровень топлива), подключается к БЛОк1 , БНСТ
                /*if (isNotBlank(DUT1model)) {
                    long[] ids = modelFacility(DUT1model, conn);
                    equipment_model_id = ids[0];
                    facility_id = ids[1];
                    equipment_status_id = statusByName(DUT1status, conn);
                    unit_service_type_id = toVidByName(DUT1TOvid, conn);
                    unit_service_facility_id = serviceCompanyByName(DUT1serviceCompany, conn);

                    equipment_id = insertEquipment(conn, null, unit_service_type_id, facility_id,
                            equipment_status_id, null, equipment_model_id, depo_id, territory_id,
                            unit_service_facility_id, null, "dut1");

                    createSensorFromEquipmentIdAndLink(
                            conn, equipment_id, 800, ts_id, 13, unit_id);
                }*/
                /*if (isNotBlank(DUT2model)) {
                    long[] ids = modelFacility(DUT2model, conn);
                    equipment_model_id = ids[0];
                    facility_id = ids[1];
                    equipment_status_id = statusByName(DUT2status, conn);
                    unit_service_type_id = toVidByName(DUT2TOvid, conn);
                    unit_service_facility_id = serviceCompanyByName(DUT2serviceCompany, conn);

                    equipment_id = insertEquipment(conn, null, unit_service_type_id, facility_id,
                            equipment_status_id, null, equipment_model_id, depo_id, territory_id,
                            unit_service_facility_id, null, "dut2");

                    createSensorFromEquipmentIdAndLink(
                            conn, equipment_id, 801, ts_id, 13, unit_id);
                }*/
                /*if (isNotBlank(kbtobSerial) || isNotBlank(kbtobModel)) {
                    long[] ids = modelFacility(kbtobModel, conn);
                    equipment_model_id = ids[0];
                    facility_id = ids[1];

                    equipment_status_id = statusByName(kbtobStatus, conn);
                    unit_service_type_id = toVidByName(kbtobTOvid, conn);
                    unit_service_facility_id = serviceCompanyByName(kbtobTOserviceCompany, conn);

                    String artificialId = UUID.randomUUID().toString();

                    equipment_id = insertEquipment(conn, null, unit_service_type_id, facility_id,
                            equipment_status_id, kbtobSerial, equipment_model_id, depo_id, territory_id,
                            unit_service_facility_id, artificialId, "kbtob");
                    pstmt = conn.prepareStatement(insertUnitSql);
                    pstmt.setLong(1, equipment_id); // unit_id
                    pstmt.setString(2, artificialId);// unit_num,
                    pstmt.setLong(3, 5); // hub_id
                    pstmt.execute();
                    pstmt = conn.prepareStatement(insertUnit2tr);
                    pstmt.setLong(1, ts_id);
                    pstmt.setLong(2, equipment_id);
                    pstmt.execute();
                    pstmt = conn.prepareStatement("insert into core.unit_kbtob(unit_kbtob_id) VALUES (?);");
                    pstmt.setLong(1, equipment_id);
                    pstmt.execute();
                }*/
                /*if (alarmButton > 0) {
                    pstmt = conn.prepareStatement("select unit_id from core.unit2tr where tr_id = ?;");
                    pstmt.setLong(1, ts_id);
                    rs = pstmt.executeQuery();
                    alarmButtonBloks.clear();
                    while (rs.next()) {
                        alarmButtonBloks.add(rs.getLong(1));
                    }

                    long[] ids = modelFacility("ТК", conn);
                    equipment_model_id = ids[0];
                    facility_id = ids[1];
                    equipment_status_id = statusByName("Исправно", conn);
                    unit_service_type_id = toVidByName("Отсутствует", conn);
                    unit_service_facility_id = serviceCompanyByName("Неопределено", conn);
                    equipment_id = insertEquipment(conn, null, unit_service_type_id, facility_id,
                            equipment_status_id, null, equipment_model_id, depo_id, territory_id,
                            unit_service_facility_id, null, "alarm button");
                    if (alarmButtonBloks.size() > 0) {
                        for (Long unitId : alarmButtonBloks) {
                            if(isBnsr(conn, unitId) || isBnst(conn, unitId)){
                                createSensorFromEquipmentIdAndLink(conn, equipment_id, 215, ts_id, 13, unitId);
                            }
                            if(isKbtob(conn, unitId)){
                                createSensorFromEquipmentIdAndLink(conn, equipment_id, 3002, ts_id, 13, unitId);
                            }
                        }
                    }
                    //
                    pstmt = conn.prepareStatement("UPDATE core.sensor SET sensor_num = ? WHERE sensor_id = ?;");
                    pstmt.setLong(1, 215);
                    pstmt.setLong(2, equipment_id);
                    pstmt.execute();
                }*/
                /*if (fireDetector > 0) {
                    pstmt = conn.prepareStatement("select unit_id from core.unit_kbtob t1 JOIN core.unit2tr u2t ON u2t.unit_id = t1.unit_kbtob_id AND u2t.tr_id = ?;");
                    pstmt.setLong(1, ts_id);
                    rs = pstmt.executeQuery();
                    unit_id = -1;
                    while (rs.next()) {
                        unit_id = rs.getInt(1);
                    }
                    long[] ids = modelFacility("ИПК", conn);
                    equipment_model_id = ids[0];
                    facility_id = ids[1];
                    equipment_status_id = statusByName("Исправно", conn);
                    unit_service_type_id = toVidByName("Отсутствует", conn);
                    unit_service_facility_id = serviceCompanyByName("Неопределено", conn);
                    equipment_id = insertEquipment(conn, null, unit_service_type_id, facility_id,
                            equipment_status_id, null, equipment_model_id, depo_id, territory_id,
                            unit_service_facility_id, null, "fire detector");
                    createSensorFromEquipmentIdAndLink(conn, equipment_id, 3001, ts_id, 13, unit_id);
                }*/
                /*if (hdd > 0) {
                    pstmt = conn.prepareStatement("select unit_id from core.unit_kbtob t1 JOIN core.unit2tr u2t ON u2t.unit_id = t1.unit_kbtob_id AND u2t.tr_id = ?;");
                    pstmt.setLong(1, ts_id);
                    rs = pstmt.executeQuery();
                    unit_id = -1;
                    while (rs.next()) {
                        unit_id = rs.getInt(1);
                    }
                    long[] ids = modelFacility("ЖД", conn);
                    equipment_model_id = ids[0];
                    facility_id = ids[1];
                    equipment_status_id = statusByName("Исправно", conn);
                    unit_service_type_id = toVidByName("Отсутствует", conn);
                    unit_service_facility_id = serviceCompanyByName("Неопределено", conn);
                    equipment_id = insertEquipment(conn, null, unit_service_type_id, facility_id,
                            equipment_status_id, null, equipment_model_id, depo_id, territory_id,
                            unit_service_facility_id, null, "hdd");

                    pstmt = conn.prepareStatement("insert into core.hdd(hdd_id) VALUES (?);");
                    pstmt.setLong(1, equipment_id);
                    pstmt.execute();
                    if (unit_id > 0) {
                        pstmt = conn.prepareStatement("insert INTO core.hdd2unit(unit_id, hdd_id) VALUES (?,?);");
                        pstmt.setLong(1, unit_id);
                        pstmt.setLong(2, equipment_id);
                        pstmt.execute();
                    }
                }*/
                /*if (videoCameras > 0) {
                    pstmt = conn.prepareStatement("select unit_id from core.unit_kbtob t1 JOIN core.unit2tr u2t ON u2t.unit_id = t1.unit_kbtob_id AND u2t.tr_id = ?;");
                    pstmt.setLong(1, ts_id);
                    rs = pstmt.executeQuery();
                    unit_id = -1;
                    while (rs.next()) {
                        unit_id = rs.getInt(1);
                    }
                    long[] ids = modelFacility("Камера наблюдения", conn);
                    equipment_model_id = ids[0];
                    facility_id = ids[1];
                    equipment_status_id = statusByName("Исправно", conn);
                    unit_service_type_id = toVidByName("Отсутствует", conn);
                    unit_service_facility_id = serviceCompanyByName("Неопределено", conn);

                    for (int k = 0; k < videoCameras; k++) {
                        equipment_id = insertEquipment(conn, null, unit_service_type_id, facility_id,
                                equipment_status_id, null, equipment_model_id, depo_id, territory_id,
                                unit_service_facility_id, null, "camera");

                        createSensorFromEquipmentIdAndLink(conn, equipment_id, 11001 + k, ts_id, 13, unit_id);
                    }
                }*/
                // блок 1 манипулятор - БНСТ
                /*if (blok1Manipulator > 0) {
                    pstmt = conn.prepareStatement("select unit_id from core.unit_bnst t1 JOIN core.unit2tr u2t ON u2t.unit_id = t1.unit_bnst_id AND u2t.tr_id = ?;");
                    pstmt.setLong(1, ts_id);
                    rs = pstmt.executeQuery();
                    unit_id = -1;
                    while (rs.next()) {
                        unit_id = rs.getInt(1);
                    }
                    long[] ids = modelFacility("Манипулятор БНСТ", conn);
                    equipment_model_id = ids[0];
                    facility_id = ids[1];
                    equipment_status_id = statusByName("Исправно", conn);
                    unit_service_type_id = toVidByName("Отсутствует", conn);
                    unit_service_facility_id = serviceCompanyByName("Неопределено", conn);

                    equipment_id = insertEquipment(conn, null, unit_service_type_id, facility_id,
                            equipment_status_id, null, equipment_model_id, depo_id, territory_id,
                            unit_service_facility_id, null, "bnst manipulator");
                    createSensorFromEquipmentIdAndLink(conn, equipment_id, 12000, ts_id, 13, unit_id);
                }*/
            }
            /*queryTs += "('" + join(licenceTr.iterator(), "', '") + "');";
            System.out.println(queryTs);
            // есть ли эти ТС в базе
            pstmt = conn.prepareStatement(queryTs);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                System.out.println("Не совпало: " + rs.getString(1));
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //conn.rollback();

            closeQuietly(rs);
            closeQuietly(pstmt);
            closeQuietly(conn);
        }
    }

    private static String getStringCellValue(Cell cell) {
        if (cell == null) {
            return "";
        }
        return cell.toString().trim();
        /*String result = "";
        CellType cType = cell.getCellTypeEnum();
        try {
            switch (cType) {
                case NUMERIC:
                    result = String.valueOf(cell.getNumericCellValue());
                    break;
                default:
                    result = cell.getStringCellValue();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;*/
    }

    private static long[] modelFacility(String modelName, Connection conn) throws SQLException {
        long[] modelFacility = {-1, -1};
        PreparedStatement pstmt = conn.prepareStatement(
                "select equipment_model_id, facility_id from core.equipment_model  where UPPER(trim(name)) = ?;");
        pstmt.setString(1, modelName.toUpperCase());
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            modelFacility[0] = rs.getLong(1);
            modelFacility[1] = rs.getLong(2);
        }
        closeQuietly(rs);
        closeQuietly(pstmt);
        return modelFacility;
    }

    private static long statusByName(String status, Connection conn) throws SQLException {
        long statusId = -1;
        PreparedStatement pstmt = conn.prepareStatement(
                "select equipment_status_id from core.equipment_status  where trim(name) = ?;");
        pstmt.setString(1, status);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            statusId = rs.getLong(1);
        }
        closeQuietly(rs);
        closeQuietly(pstmt);
        return statusId;
    }

    private static long toVidByName(String toVid, Connection conn) throws SQLException {
        long toId = -1;
        PreparedStatement pstmt = conn.prepareStatement(
                "select unit_service_type_id from core.unit_service_type where trim(name) = ? ;");
        pstmt.setString(1, toVid);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            toId = rs.getLong(1);
        }
        closeQuietly(rs);
        closeQuietly(pstmt);
        return toId;
    }

    private static long serviceCompanyByName(String company, Connection conn) throws SQLException {
        long companyId = -1;
        PreparedStatement pstmt = conn.prepareStatement(
                "select entity_id from core.entity where name_short = ?;");
        pstmt.setString(1, company);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            companyId = rs.getLong(1);
        }
        closeQuietly(rs);
        closeQuietly(pstmt);
        return companyId;
    }

    // возвращает id всталенного оборудования
    private static long insertEquipment(Connection conn, Long firmware_id, long unit_service_type_id, long facility_id,
                                        long equipment_status_id, String serial_num, long equipment_model_id,
                                        long depo_id, long territory_id, long unit_service_facility_id,
                                        String external_id, String comment) throws SQLException {

        final String insertSql = "INSERT INTO core.equipment(equipment_id, firmware_id, unit_service_type_id, facility_id, equipment_status_id, serial_num, " +
                "            equipment_model_id, depo_id, territory_id, unit_service_facility_id, external_id, comment, dt_begin) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, localtimestamp) " +
                "ON CONFLICT (external_id) DO UPDATE " +
                "SET firmware_id = EXCLUDED.firmware_id, unit_service_type_id = EXCLUDED.unit_service_type_id , facility_id = EXCLUDED.facility_id, " +
                "  equipment_status_id = EXCLUDED.equipment_status_id, serial_num = EXCLUDED.serial_num, " +
                "  equipment_model_id = EXCLUDED.equipment_model_id,  depo_id = EXCLUDED.depo_id, territory_id = EXCLUDED.territory_id, " +
                "  unit_service_facility_id = EXCLUDED.unit_service_facility_id, external_id = EXCLUDED.external_id, comment = 'updated';";

        PreparedStatement pstmt;
        //***********************************
        pstmt = conn.prepareStatement("select * from nextval('core.equipment_equipment_id_seq'::regclass)");
        ResultSet rs = pstmt.executeQuery();
        rs.next();
        long equipment_id = rs.getLong(1);

        pstmt = conn.prepareStatement(insertSql);
        pstmt.setLong(1, equipment_id);
        if (firmware_id == null) {
            pstmt.setNull(2, Types.BIGINT);
        } else {
            pstmt.setLong(2, firmware_id);
        }
        pstmt.setLong(3, unit_service_type_id);
        pstmt.setLong(4, facility_id);// +++++++++
        pstmt.setLong(5, equipment_status_id);
        pstmt.setString(6, serial_num);
        pstmt.setLong(7, equipment_model_id);
        pstmt.setLong(8, depo_id);
        pstmt.setLong(9, territory_id);
        pstmt.setLong(10, unit_service_facility_id);
        pstmt.setString(11, external_id); // ext id
        pstmt.setString(12, comment); // coment
        pstmt.execute();

        closeQuietly(rs);
        closeQuietly(pstmt);
        return equipment_id;
    }

    private static void createSensorFromEquipmentIdAndLink(
            Connection conn, long equipment_id, long sensor_num, long tr_id, long installation_site_id, long unit_id)
            throws SQLException {
        PreparedStatement pstmt = conn.prepareStatement(
                "INSERT INTO core.sensor(sensor_id, sensor_num) VALUES(?, ?) ON CONFLICT (sensor_id) " +
                        "DO UPDATE SET sensor_num = EXCLUDED.sensor_num;");

        pstmt.setLong(1, equipment_id);
        pstmt.setLong(2, sensor_num);
        pstmt.execute();

        pstmt = conn.prepareStatement(
                "INSERT INTO core.sensor2tr(sensor_id, tr_id, installation_site_id) VALUES (?,?,?) ON CONFLICT (sensor_id) " +
                        //"DO UPDATE SET tr_id = EXCLUDED.tr_id, installation_site_id = EXCLUDED.installation_site_id;");
                        "DO NOTHING;");

        pstmt.setLong(1, equipment_id);
        pstmt.setLong(2, tr_id);
        pstmt.setLong(3, installation_site_id);
        pstmt.execute();

        // если не можем привязать к блоку
        if (unit_id < 0) {
            return;
        }
        pstmt = conn.prepareStatement(
                "insert INTO core.sensor2unit(sensor_id, unit_id, sensor_num) VALUES (?,?,?) ON CONFLICT DO NOTHING;");

        pstmt.setLong(1, equipment_id);
        pstmt.setLong(2, unit_id);
        pstmt.setLong(3, sensor_num);
        pstmt.execute();

        closeQuietly(pstmt);
    }

    private static boolean isBnsr(Connection conn, long unit_id) throws SQLException {
        PreparedStatement pstmt;
        boolean result = false;
        pstmt = conn.prepareStatement("select COUNT(1) from core.unit_bnsr where unit_bnsr_id = ?;");
        pstmt.setLong(1, unit_id);
        ResultSet rs = pstmt.executeQuery();
        rs.next();
        int count = rs.getInt(1);
        if (count > 0) {
            result = true;
        }
        closeQuietly(pstmt);
        return result;
    }

    private static boolean isBnst(Connection conn, long unit_id) throws SQLException {
        PreparedStatement pstmt;
        boolean result = false;
        pstmt = conn.prepareStatement("select COUNT(1) from core.unit_bnst where unit_bnst_id = ?;");
        pstmt.setLong(1, unit_id);
        ResultSet rs = pstmt.executeQuery();
        rs.next();
        int count = rs.getInt(1);
        if (count > 0) {
            result = true;
        }
        closeQuietly(pstmt);
        return result;
    }

    private static boolean isKbtob(Connection conn, long unit_id) throws SQLException {
        PreparedStatement pstmt;
        boolean result = false;
        pstmt = conn.prepareStatement("select COUNT(1) from core.unit_kbtob where unit_kbtob_id = ?;");
        pstmt.setLong(1, unit_id);
        ResultSet rs = pstmt.executeQuery();
        rs.next();
        int count = rs.getInt(1);
        if (count > 0) {
            result = true;
        }
        closeQuietly(pstmt);
        return result;
    }
}
