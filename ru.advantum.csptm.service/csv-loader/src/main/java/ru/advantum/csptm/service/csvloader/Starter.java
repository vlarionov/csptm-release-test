package ru.advantum.csptm.service.csvloader;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.service.csvloader.config.CsvDirectoriesConfig;
import ru.advantum.csptm.service.csvloader.directory.DirectoryListener;
import ru.advantum.csptm.service.csvloader.directory.partition.BuildPartitionTaskScheduler;
import ru.advantum.csptm.service.csvloader.directory.partition.PartitioningType;

@Slf4j
@Service
public class Starter implements SchedulingConfigurer {
    private final CsvDirectoriesConfig csvDirectoriesConfig;
    private final DataSource dataSource;
    private final TableMetaDataProvider tableMetaDataProvider;
    private final JdbcTemplate jdbcTemplate;
    private final TaskScheduler taskScheduler;

    private Map<String, DirectoryContext> directoryContextMap= new HashMap<>();

    @Autowired
    public Starter(CsvDirectoriesConfig csvDirectoriesConfig,
            DataSource dataSource, TableMetaDataProvider tableMetaDataProvider,
            JdbcTemplate jdbcTemplate, TaskScheduler taskScheduler) {
        this.csvDirectoriesConfig = csvDirectoriesConfig;
        this.dataSource = dataSource;
        this.tableMetaDataProvider = tableMetaDataProvider;
        this.jdbcTemplate = jdbcTemplate;
        this.taskScheduler = taskScheduler;
    }

    @PostConstruct
    public void start(){
        csvDirectoriesConfig.getDirectoryConfigs().forEach(config -> {
            PartitioningType partitioningType = PartitioningType.valueOf(config.getPartitionConfig().getType().toUpperCase());
            DirectoryListener directoryListener = null;

            if(partitioningType==PartitioningType.INHERIT) {
                directoryListener = new DirectoryListener(csvDirectoriesConfig, config,
                        dataSource, tableMetaDataProvider);
                directoryListener.init();
            }

            BuildPartitionTaskScheduler buildPartitionTaskScheduler = new BuildPartitionTaskScheduler(config,
                    jdbcTemplate, tableMetaDataProvider, partitioningType);
            buildPartitionTaskScheduler.init();

            DirectoryContext ctx = new DirectoryContext(directoryListener, buildPartitionTaskScheduler);

            log.info("Created PartitionContext {}", ctx);

            directoryContextMap.put(config.getName(), ctx);
        });
    }

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.setTaskScheduler(taskScheduler);

        directoryContextMap.values().forEach(context -> context.configureTasks(taskRegistrar));

    }

    private static class DirectoryContext{
        private DirectoryListener directoryListener;
        private BuildPartitionTaskScheduler buildPartitionTaskScheduler;

        private DirectoryContext(DirectoryListener directoryListener,
                BuildPartitionTaskScheduler buildPartitionTaskScheduler) {
            this.directoryListener = directoryListener;
            this.buildPartitionTaskScheduler = buildPartitionTaskScheduler;
        }
        public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
            if(directoryListener != null) {
                directoryListener.configureTasks(taskRegistrar);
            }
            if(buildPartitionTaskScheduler != null) {
                buildPartitionTaskScheduler.configureTasks(taskRegistrar);
            }
        }
    }
}
