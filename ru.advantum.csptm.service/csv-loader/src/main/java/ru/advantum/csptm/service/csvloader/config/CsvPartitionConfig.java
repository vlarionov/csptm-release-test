package ru.advantum.csptm.service.csvloader.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import ru.advantum.csptm.service.csvloader.directory.partition.PartitionUnit;

import java.util.List;

@Root(name = "partition")
public class CsvPartitionConfig {
    /**
     * Таблица для загрузки
     */
    @Attribute(name = "table")
    private String table;

    /**
     * Тип партиционирования(inherit or declarative)
     */
    @Attribute(name = "type")
    private String type;

    /**
     * Колонка по которой производится партиционирование
     */
    @Attribute(name = "column")
    private String column;

    /**
     *  Тейблспейс
     */
    @Attribute(name = "tablespace")
    private String tablespace;

    /**
     * Директория для хранения партиций
     */
    @Attribute(name = "tablespace_directory")
    private String tablespaceDirectory;

    /**
     * Единица деления на партиции, возможные значения: YEAR, MONTH, WEEK, DAY
     */
    @Attribute(name = "partition_unit", required = false)
    private String partitionUnit = "MONTH";

    /**
     * Список настроек дополнительных индексов, кроме колонки партиционирования
     */
    @ElementList(name = "indexes", required = false)
    private List<CsvIndexConfig> indexConfigs;

    public String getTable() {
        return table;
    }

    public String getType() {
        return type;
    }

    public String getColumn() {
        return column;
    }

    public String getTablespace() {
        return tablespace;
    }

    public String getTablespaceDirectory() {
        return tablespaceDirectory;
    }

    public PartitionUnit getPartitionUnit() {
        return PartitionUnit.valueOf(partitionUnit);
    }

    public List<CsvIndexConfig> getIndexConfigs() {
        return indexConfigs;
    }
}
