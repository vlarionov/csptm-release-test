package ru.advantum.csptm.service.csvloader.directory.partition;

import com.google.common.base.MoreObjects;
import org.springframework.scheduling.Trigger;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjuster;
import java.util.Date;

import static java.time.temporal.TemporalAdjusters.*;


public enum PartitionUnit {
    YEAR("yyyy", firstDayOfYear(),  firstDayOfNextYear()),
    MONTH("yyyyMM", firstDayOfMonth(),  firstDayOfNextMonth()),
    WEEK("yyyyMMdd", DayOfWeek.MONDAY,  next(DayOfWeek.MONDAY)),
    DAY("yyyyMMdd", temporal -> temporal, temporal -> temporal.plus(Duration.ofDays(1)));

    private final DateTimeFormatter suffixFormatter;

    private final TemporalAdjuster partitionStartAdjuster;
    private final TemporalAdjuster partitionEndAdjuster;

    PartitionUnit(String suffixFormat, TemporalAdjuster partitionStartAdjuster, TemporalAdjuster partitionEndAdjuster) {
        this.suffixFormatter = DateTimeFormatter.ofPattern(suffixFormat);
        this.partitionStartAdjuster = partitionStartAdjuster;
        this.partitionEndAdjuster = partitionEndAdjuster;
    }

    public DateTimeFormatter getSuffixFormatter() {
        return suffixFormatter;
    }

    public LocalDateTime getPartitionStartDate(LocalDateTime date) {
        return date.with(partitionStartAdjuster).truncatedTo(ChronoUnit.DAYS);
    }

    public LocalDateTime getPartitionEndDate(LocalDateTime date) {
        return date.with(partitionEndAdjuster).truncatedTo(ChronoUnit.DAYS);
    }

    public Trigger getTrigger() {
        return triggerContext -> {
            Date lastExecutionDate = MoreObjects.firstNonNull(triggerContext.lastScheduledExecutionTime(), new Date());
            LocalDateTime lastExecutionTime = LocalDateTime.ofInstant(lastExecutionDate.toInstant(), ZoneId.systemDefault());
            return Date.from(getPartitionEndDate(lastExecutionTime).atZone(ZoneId.systemDefault()).toInstant());
        };
    }

}
