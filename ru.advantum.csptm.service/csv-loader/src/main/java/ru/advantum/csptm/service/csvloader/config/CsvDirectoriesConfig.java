package ru.advantum.csptm.service.csvloader.config;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "directories")
public class CsvDirectoriesConfig {
    @Attribute(name = "base_path")
    private String basePath;

    @ElementList(name = "directory", inline = true)
    private List<CsvDirectoryConfig> directoryConfigs;

    public String getBasePath() {
        return basePath;
    }

    public List<CsvDirectoryConfig> getDirectoryConfigs() {
        return directoryConfigs;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("basePath", basePath)
                .append("directoryConfigs", directoryConfigs)
                .toString();
    }
}
