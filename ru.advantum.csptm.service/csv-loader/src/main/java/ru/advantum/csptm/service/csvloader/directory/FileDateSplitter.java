package ru.advantum.csptm.service.csvloader.directory;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.apache.drill.common.AutoCloseables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.advantum.csptm.service.csvloader.config.CsvDirectoryConfig;
import ru.advantum.csptm.service.csvloader.config.CsvParserConfig;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.*;

public class FileDateSplitter {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileDateSplitter.class);

    private final CSVFormat csvFormat;
    private final DateTimeFormatter timestampFormatter;
    private final Charset charset;

    private final int partitionColumnIndex;

    public FileDateSplitter(String dateTemplate, String encoding, CSVFormat csvFormat, int partitionColumnIndex) {
        this.timestampFormatter = new DateTimeFormatterBuilder()
                .appendPattern(dateTemplate)
                .appendFraction(ChronoField.NANO_OF_SECOND, 0, 9, true)
                .appendPattern("X")
                .toFormatter();
        this.csvFormat = csvFormat;
        this.charset = Charset.forName(encoding);
        this.partitionColumnIndex = partitionColumnIndex;
    }

    public FileDateSplitter(CsvDirectoryConfig directoryConfig) {
        this(
                directoryConfig.getDataDateTemplate(),
                directoryConfig.getEncoding(),
                buildCSVFormat(directoryConfig.getParserConfig()),
                directoryConfig.getPartitionColumnIndex()
        );
    }


    private static CSVFormat buildCSVFormat(CsvParserConfig parserConfig) {
        return CSVFormat.DEFAULT
                .withDelimiter(parserConfig.getDelimiter())
                .withEscape(parserConfig.getEscape())
                .withTrim(parserConfig.getTrimEnabled())
                .withNullString(parserConfig.getNullString())
                .withRecordSeparator(parserConfig.getRecordSeparator())
                .withQuoteMode(QuoteMode.NONE);
    }

    public DateSplitFiles split(Path source,Path splitedDirectory) throws Exception {
        Set<LocalDate> dates = getDates(source);

        if (dates.isEmpty()) {
            LOGGER.info("File is empty {}", source);
            return new DateSplitFiles(Collections.emptyMap());
        }

        Map<LocalDate, Path> datePathMap = new HashMap<>();
        for (LocalDate date : dates) {
            String sourceFileName = source.toFile().getName();
            Path tempFile = Files.createTempFile(splitedDirectory, sourceFileName, date.toString());
            datePathMap.put(date, tempFile);
            LOGGER.debug("File {} split to {}", sourceFileName, tempFile.toFile().getName());
        }

        copyToResultFiles(source, datePathMap);

        return new DateSplitFiles(datePathMap);
    }


    public Set<LocalDate> getDates(Path file) throws IOException {
        final Set<LocalDate> result = new HashSet<>();
        try (BufferedReader reader = Files.newBufferedReader(file, charset);
             CSVParser parser = new CSVParser(reader, csvFormat)) {
            for (CSVRecord record : parser) {
                try {
                    LocalDate date = LocalDateTime.parse(record.get(partitionColumnIndex), timestampFormatter).toLocalDate();
                    result.add(date);
                } catch (Exception e) {
                    if (record.getRecordNumber() == 1) {
                        //this is header
                        continue;
                    }

                    LOGGER.error("Can't parse date in line {} of file {}", record.toString(), file);
                    throw e;
                }
            }
        }
        return result;
    }

    private void copyToResultFiles(Path inFile, Map<LocalDate, Path> outFiles) throws Exception {
        try (
                CSVParser parser = new CSVParser(Files.newBufferedReader(inFile, this.charset), csvFormat);
                MultiFilePrinter printer = new MultiFilePrinter(outFiles, charset, csvFormat)
        ) {
            for (CSVRecord record : parser) {
                try {
                    LocalDate date = LocalDate.parse(record.get(partitionColumnIndex), timestampFormatter);
                    printer.print(date, record);
                } catch (Exception e) {
                    if (record.getRecordNumber() == 1) {
                        //this is header
                        continue;
                    }

                    throw e;
                }
            }
            printer.flush();
        }
    }

    public static class DateSplitFiles implements AutoCloseable {
        private final Map<LocalDate, Path> dateToFileMap;

        public DateSplitFiles(Map<LocalDate, Path> dateToFileMap) {
            this.dateToFileMap = Collections.unmodifiableMap(dateToFileMap);
        }

        public Map<LocalDate, Path> getDateToFileMap() {
            return dateToFileMap;
        }

        @Override
        public void close() throws Exception {
            //если файл был разделен на временные, то их надо удалить
            if (getDateToFileMap().size() > 1) {
                for (Path path : getDateToFileMap().values()) {
                    try {
                        Files.delete(path);
                    } catch (IOException ex) {
                        LOGGER.error("Can't delete temp file {} {}", path, ex.getMessage());
                    }
                }
            }
        }
    };

    private static class MultiFilePrinter implements AutoCloseable {
        private final Map<LocalDate, CSVPrinter> printerMap = new HashMap<>();

        public MultiFilePrinter(Map<LocalDate, Path> outFiles, Charset charset, CSVFormat csvFormat) throws Exception {
            try {
                for (Map.Entry<LocalDate, Path> outFileEntry : outFiles.entrySet()) {
                    CSVPrinter printer = new CSVPrinter(Files.newBufferedWriter(outFileEntry.getValue(), charset), csvFormat);
                    //copyIn игнорит первую линию, поэтому оставляем ее пустой,  может быть хедер, но для copyIn он не нужен
                    printer.println();
                    printerMap.put(outFileEntry.getKey(), printer);
                }
            } catch (IOException e) {
                try {
                    close();
                } catch (Exception closeException) {
                    LOGGER.error("Error on close files: " + outFiles.values(), closeException);
                }

                throw e;
            }

        }

        public void print(LocalDate date, CSVRecord record) throws IOException {
            printerMap.get(date).printRecord(record);
        }

        public void flush() throws IOException {
            for (CSVPrinter printer : printerMap.values()) {
                printer.flush();
            }
        }

        @Override
        public void close() throws Exception {
            AutoCloseables.close(printerMap.values());
        }
    }


}
