package ru.advantum.csptm.service.csvloader.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name = "index")
public class CsvIndexConfig {
    /**
     * Список колонок индекса, через запятую
     */
    @Attribute(name = "column_list")
    private String columnList;

    /**
     * Тип индекса
     */
    @Attribute(name = "type", required = false)
    private String type = "btree";

    /**
     * Признак уникальности
     */
    @Attribute(name = "unique", required = false)
    private Boolean unique = false;

    /**
     * Условие для индекса
     */
    @Attribute(name = "where", required = false)
    private String where = "";

    /**
     * Суффикс имени индека
     */
    @Attribute(name = "suffix")
    private String suffix;

    public String getColumnList() {
        return columnList;
    }

    public String getType() {
        return type;
    }

    public Boolean getUnique() {
        return unique;
    }

    public String getSuffix() {
        return suffix;
    }

    public String getWhere() { return where; }
}
