package ru.advantum.csptm.service.csvloader.directory;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import ru.advantum.csptm.service.csvloader.TableMetaDataProvider;
import ru.advantum.csptm.service.csvloader.config.CsvDirectoriesConfig;
import ru.advantum.csptm.service.csvloader.config.CsvDirectoryConfig;
import ru.advantum.csptm.service.csvloader.config.CsvLoaderConfig;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.stream.Stream;

public class DirectoryListener implements SchedulingConfigurer {

    private static final Logger LOGGER = LoggerFactory.getLogger(DirectoryListener.class);

    private final CsvDirectoryConfig config;

    private final Path sourceDirectory;
    private final Path tempDirectory;
    private final Path archiveDirectory;
    private final Path splitedDirectory;

    private final DatabaseLoader loader;
    private final FileDateSplitter fileDateSplitter;

    public DirectoryListener(CsvDirectoriesConfig csvDirectoriesConfig, CsvDirectoryConfig directoryConfig,
            DataSource dataSource, TableMetaDataProvider tableMetaDataProvider)
    {
        this.config = directoryConfig;

        Path baseDirectory = Paths.get(csvDirectoriesConfig.getBasePath());
        this.sourceDirectory = baseDirectory.resolve("online").resolve(config.getName());
        this.tempDirectory = baseDirectory.resolve("temp").resolve(config.getName());
        this.archiveDirectory = baseDirectory.resolve("archive").resolve(config.getName());
        this.splitedDirectory = baseDirectory.resolve("splited").resolve(config.getName());

        this.loader = new DatabaseLoader(directoryConfig, dataSource, tableMetaDataProvider);
        this.fileDateSplitter = new FileDateSplitter(directoryConfig);
    }

    public void init() {
        sourceDirectory.toFile().mkdirs();
        tempDirectory.toFile().mkdirs();
        archiveDirectory.toFile().mkdirs();
        splitedDirectory.toFile().mkdirs();
    }

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.addFixedDelayTask(this::check, config.getTimeUnit().toMillis(config.getPeriod()));
    }

    public void check() {
        LOGGER.info("Watch directory {}", sourceDirectory);
        File[] onlineFiles = sourceDirectory.toFile().listFiles();
        Assert.notNull(onlineFiles, "Abstract pathname does not denote a directory, or if an I/O error occurs.");
        Stream.of(onlineFiles)
                .filter((file -> !file.getName().startsWith(config.getTempPrefix())))
                .sorted(Comparator.comparingLong(File::lastModified))
                .map(File::toPath)
                .forEach(this::processFile);
        File[] splitedFiles = splitedDirectory.toFile().listFiles();
        Assert.notNull(splitedFiles, "Abstract pathname does not denote a directory, or if an I/O error occurs.");
        Stream.of(splitedFiles)
                .sorted(Comparator.comparingLong(File::lastModified))
                .map(File::toPath)
                .forEach(this::processFile);
    }

    private void processFile(Path file) {
        LOGGER.info("Load file {}", file);
        try {
            //пытаемся загрузить если в splited уже находится
            if (file.getParent().equals(splitedDirectory)) {
                String formattedDate = config.getPartitionConfig().getPartitionUnit().getSuffixFormatter()
                        .format(fileDateSplitter.getDates(file).stream().findAny().get());
                loader.load(file, config.getPartitionConfig().getTable(), formattedDate);
                LOGGER.info("File {} successfully loaded", file);
                moveFile(file, archiveDirectory);
            }

            //если файл с одной датой, то перенести в splited
            else if (fileDateSplitter.getDates(file).size() == 1) {
                Files.move(file, splitedDirectory.resolve(file.getFileName()), StandardCopyOption.REPLACE_EXISTING);
                LOGGER.info("File {} moved to {}", file, splitedDirectory);
            }

            //если файл с несколькими датами, то разбиваем его на несколько в splited, а самого его удаляем
            else {
                FileDateSplitter.DateSplitFiles dateSplitFiles = fileDateSplitter.split(file, splitedDirectory);
                if (file.toFile().delete()) {
                    LOGGER.info("File splited and removed");
                }
            }

        } catch (Exception e) {
//            try {
//                moveFile(file, errorDirectory);
//            } catch (IOException moveFileException) {
//                LOGGER.error(String.format("Error on move file %s to error directory", file), moveFileException);
//            }
            LOGGER.error("Error on process file" + file, e);
        }
    }

    private void moveFile(Path srcFile, Path dstBaseDir) throws IOException {
        Path dstDirectory = getDateDirs(dstBaseDir, LocalDateTime.now(), ChronoUnit.DAYS);
        dstDirectory.toFile().mkdirs();
        Files.move(srcFile, dstDirectory.resolve(srcFile.getFileName()), StandardCopyOption.REPLACE_EXISTING);
        LOGGER.info("File {} moved to {}", srcFile, dstDirectory);
    }

    /**
     * Строит путь до директории по дате, точности и корневой директории
     *
     * @param rootDir   корневая директория
     * @param date      дата
     * @param precision точность
     * @return Путь до директории за указанную дату, с указанной точностью и указанной корневой директорией
     */
    private static Path getDateDirs(Path rootDir, LocalDateTime date, ChronoUnit precision) throws IOException {
        Path directory = rootDir;
        directory = directory.resolve(String.format("%04d", date.getYear()));
        if (precision != ChronoUnit.YEARS) {
            directory = directory.resolve(String.format("%02d", date.getMonthValue()));
            if (precision != ChronoUnit.MONTHS) {
                directory = directory.resolve(String.format("%02d", date.getDayOfMonth()));
                if (precision != ChronoUnit.DAYS) {
                    directory = directory.resolve(String.format("%02d", date.getHour()));
                    if (precision != ChronoUnit.HOURS) {
                        directory = directory.resolve(String.format("%02d", date.getMinute()));
                        if (precision != ChronoUnit.MINUTES) {
                            directory = directory.resolve(String.format("%02d", date.getSecond()));
                            if (precision != ChronoUnit.SECONDS) {
                                throw new IllegalArgumentException("Illegal ChronoUnit for precision: " + precision);
                            }
                        }
                    }
                }
            }
        }
        return directory;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("config", config)
                .append("sourceDirectory", sourceDirectory)
                .append("tempDirectory", tempDirectory)
                .append("archiveDirectory", archiveDirectory)
                .append("splitedDirectory", splitedDirectory)
                .append("loader", loader)
                .append("fileDateSplitter", fileDateSplitter)
                .toString();
    }
}
