package ru.advantum.csptm.service.csvloader.directory.partition;

import java.time.LocalDateTime;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import ru.advantum.csptm.service.csvloader.TableMetaDataProvider;
import ru.advantum.csptm.service.csvloader.config.CsvDirectoryConfig;

@Slf4j
public class BuildPartitionTaskScheduler implements SchedulingConfigurer {

    private final PartitionUnit partitionUnit;

    private final PartitionBuilder partitionBuilder;

    public BuildPartitionTaskScheduler(CsvDirectoryConfig config, JdbcTemplate jdbcTemplate
            , TableMetaDataProvider tableMetaDataProvider, PartitioningType partitioningType) {
        this.partitionUnit = config.getPartitionConfig().getPartitionUnit();
        this.partitionBuilder = new PartitionBuilder(config, jdbcTemplate, tableMetaDataProvider, partitioningType);
    }

    public void init() {
        LocalDateTime now = LocalDateTime.now();

        LocalDateTime currentPartitionStartDate = partitionUnit.getPartitionStartDate(now);
        LocalDateTime currentPartitionEndDate = partitionUnit.getPartitionEndDate(now);

        LocalDateTime nextPartitionStartDate = partitionUnit.getPartitionStartDate(currentPartitionEndDate);
        LocalDateTime nextPartitionEndDate = partitionUnit.getPartitionEndDate(currentPartitionEndDate);

        partitionBuilder.checkAndBuildPartition(currentPartitionStartDate, currentPartitionEndDate);
        partitionBuilder.checkAndBuildPartition(nextPartitionStartDate, nextPartitionEndDate);
    }

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.addTriggerTask(this::executeTask, partitionUnit.getTrigger());
    }

    private void executeTask() {
        LocalDateTime nextPartitionStartDate =  partitionUnit.getPartitionEndDate(LocalDateTime.now());
        LocalDateTime nextPartitionEndDate =    partitionUnit.getPartitionEndDate(nextPartitionStartDate);

        partitionBuilder.checkAndBuildPartition(nextPartitionStartDate, nextPartitionEndDate);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("partitionUnit", partitionUnit)
                .append("partitionBuilder", partitionBuilder)
                .toString();
    }
}
