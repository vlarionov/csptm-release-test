package ru.advantum.csptm.service.csvloader.directory.partition;

import java.sql.Timestamp;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.advantum.csptm.service.csvloader.TableMetaDataProvider;
import ru.advantum.csptm.service.csvloader.config.CsvDirectoryConfig;
import ru.advantum.csptm.service.csvloader.config.CsvIndexConfig;
import ru.advantum.csptm.service.csvloader.config.CsvPartitionConfig;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.UserPrincipal;
import java.nio.file.attribute.UserPrincipalLookupService;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

public class PartitionBuilder {

    private static final Logger LOGGER = LoggerFactory.getLogger(PartitionBuilder.class);

    private final CsvDirectoryConfig config;
    private static final String CREATE_TABLESPACE_SQL = "CREATE TABLESPACE %s LOCATION %s ;";
    private static final String CREATE_INHERITS_TABLE_SQL = "SELECT adt.csv_loader_pkg$create_inherits_table(?,?,?,?,?,?);";
    private static final String CREATE_PARTITION_TABLE_SQL = "CREATE TABLE %s PARTITION OF %s FOR VALUES FROM ('%s') TO ('%s') TABLESPACE %s;";
    private static final String CREATE_INDEX_SQL = "select adt.csv_loader_pkg$create_index(?,?,?,?,?,?,?);";
    private static final String GET_TABLESPACE_SQL = "SELECT count(1) = 1 FROM pg_tablespace WHERE spcname = ?;";

    private final JdbcTemplate jdbcTemplate;
    private final TableMetaDataProvider tableMetaDataProvider;
    private final PartitioningType partitioningType;

    public PartitionBuilder(CsvDirectoryConfig config, JdbcTemplate jdbcTemplate,
            TableMetaDataProvider tableMetaDataProvider, PartitioningType partitioningType) {
        this.config = config;
        this.jdbcTemplate = jdbcTemplate;
        this.tableMetaDataProvider = tableMetaDataProvider;
        this.partitioningType = partitioningType;
    }

    public void checkAndBuildPartition(LocalDateTime startDate, LocalDateTime endDate) {
        try {
            // Секционированная таблица уже есть?
            String inheritTableName = getChildTableName(startDate);
            if (tableMetaDataProvider
                .isTableExist(config.getPartitionConfig().getTable(), getInheritSuffix(startDate))) {
                LOGGER.info("Partition exists " + inheritTableName);
                return;
            }
            createFile(getTableTablespaceDir(startDate));

            if (!isExistsTablespace(getTableTablespaceName(startDate))) {
                createTableTablespace(startDate);
            }
            createChildTableStatement(startDate, endDate);

            createFile(getIndexTablespaceDir(startDate));
            if (!isExistsTablespace(getIndexTablespaceName(startDate))) {
                createIndexTablespace(startDate);
            }
            createIndexStatement(startDate);
        } catch (IOException e) {
            LOGGER.error("Error on file creation", e);
        }
    }

    private void createTableTablespace(LocalDateTime partitionStartDate) {
        jdbcTemplate.execute(String.format(CREATE_TABLESPACE_SQL,
            getTableTablespaceName(partitionStartDate),
            "'"+getTableTablespaceDir(partitionStartDate).toString()+"'"));
    }

    private void createIndexTablespace(LocalDateTime partitionStartDate) {
        jdbcTemplate.execute(String.format(CREATE_TABLESPACE_SQL,
            getIndexTablespaceName(partitionStartDate),
            "'"+getIndexTablespaceDir(partitionStartDate).toString()+"'"));
    }

    private String getChildTableName(LocalDateTime partitionStartDate) {
        return config.getPartitionConfig().getTable() + "_" + getInheritSuffix(partitionStartDate);
    }

    private String getInheritSuffix(LocalDateTime partitionStartDate) {
        return config.getPartitionConfig().getPartitionUnit().getSuffixFormatter().format(partitionStartDate);
    }

    private String getTableTablespaceName(LocalDateTime partitionStartDate) {
        return config.getPartitionConfig().getTablespace() + "_" + getSimpleTableName() + "_data_" + getInheritSuffix(
            partitionStartDate);
    }

    private String getIndexTablespaceName(LocalDateTime partitionStartDate) {
        return config.getPartitionConfig().getTablespace() + "_" + getSimpleTableName() + "_idx_" + getInheritSuffix(
            partitionStartDate);
    }

    private String getSimpleTableName() {
        return config.getPartitionConfig().getTable().split("\\.")[1];
    }

    private Path getTableTablespaceDir(LocalDateTime partitionStartDate) {
        return Paths.get(config.getPartitionConfig().getTablespaceDirectory())
            .resolve(getTableTablespaceName(partitionStartDate));
    }

    private Path getIndexTablespaceDir(LocalDateTime partitionStartDate) {
        return Paths.get(config.getPartitionConfig().getTablespaceDirectory())
            .resolve(getIndexTablespaceName(partitionStartDate));
    }

    private void createChildTableStatement(LocalDateTime partitionStartDate, LocalDateTime partitionEndDate) {
        if(partitioningType == PartitioningType.INHERIT) {
            jdbcTemplate.queryForList(CREATE_INHERITS_TABLE_SQL,
                    config.getPartitionConfig().getTable(),
                    getInheritSuffix(partitionStartDate),
                    getTableTablespaceName(partitionStartDate),
                    Timestamp.valueOf(partitionStartDate),
                    Timestamp.valueOf(partitionEndDate),
                    config.getPartitionConfig().getColumn());
        }else if(partitioningType == PartitioningType.DECLARATIVE){
            String partitionTableName = getChildTableName(partitionStartDate);
            jdbcTemplate.execute(
                    String.format(CREATE_PARTITION_TABLE_SQL,
                            partitionTableName,
                            config.getPartitionConfig().getTable(),
                            Timestamp.valueOf(partitionStartDate),
                            Timestamp.valueOf(partitionEndDate),
                            getTableTablespaceName(partitionStartDate)));

        }else{
            throw new IllegalArgumentException();
        }

    }

    private void createIndexStatement(LocalDateTime partitionStartDate) {
        CsvPartitionConfig partitionConfig = config.getPartitionConfig();
        String indexName = "ix_" + getSimpleTableName() + "_" +
            getInheritSuffix(partitionStartDate) + "_" + partitionConfig.getColumn();
        jdbcTemplate.queryForList(
            CREATE_INDEX_SQL,
            indexName,
            getChildTableName(partitionStartDate),
            partitionConfig.getColumn(),
            getIndexTablespaceName(partitionStartDate),
            "btree",
            " ",
            ""
        );
        List<CsvIndexConfig> indexConfigs = partitionConfig.getIndexConfigs();
        if (indexConfigs != null) {
            for (CsvIndexConfig indexConfig : indexConfigs) {
                String additionalIndexName = "ix_" + getSimpleTableName() + "_" +
                    getInheritSuffix(partitionStartDate) + "_" + indexConfig.getSuffix();
                jdbcTemplate.queryForList(
                    CREATE_INDEX_SQL,
                    additionalIndexName,
                    getChildTableName(partitionStartDate),
                    indexConfig.getColumnList(),
                    getIndexTablespaceName(partitionStartDate),
                    indexConfig.getType(),
                    !indexConfig.getWhere().trim().isEmpty() ? " WHERE " + indexConfig.getWhere() : "",
                    indexConfig.getUnique() ? " UNIQUE " : " "
                );
            }
        }
    }

    private boolean isExistsTablespace(String name) {
        return jdbcTemplate.queryForObject(
                GET_TABLESPACE_SQL,
                Collections.singletonList(name).toArray(),
                boolean.class
        );
    }


    private boolean isWindows() {
        String OS = System.getProperty("os.name").toLowerCase();
        return OS.contains("win");
    }

    private void createFile(Path path) throws IOException {
        path.toFile().mkdirs();

        if (isWindows()) {
            return;
        }

        UserPrincipalLookupService lookupService = FileSystems.getDefault().getUserPrincipalLookupService();
        UserPrincipal postgres = lookupService.lookupPrincipalByName("postgres");
        Files.setOwner(path, postgres);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("config", config)
                .append("partitioningType", partitioningType)
                .toString();
    }
}
