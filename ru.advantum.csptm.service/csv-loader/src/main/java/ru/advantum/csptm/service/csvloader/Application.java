package ru.advantum.csptm.service.csvloader;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import lombok.extern.slf4j.Slf4j;
import org.simpleframework.xml.load.Persister;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import ru.advantum.csptm.service.csvloader.config.CsvDirectoriesConfig;
import ru.advantum.csptm.service.csvloader.config.CsvLoaderConfig;

@Slf4j
@SpringBootApplication
@EnableEurekaClient
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CsvDirectoriesConfig csvDirectoriesConfig(EurekaClient discoveryClient) throws IOException, Exception {
        InstanceInfo instance = discoveryClient.getNextServerFromEureka("configserver", false);
        URI path = URI.create(instance.getHomePageUrl()).resolve("/csvloader/default/master/csvloader/directories.xml");
        try (InputStream inputStream = path.toURL().openStream()) {
            CsvDirectoriesConfig config = new Persister().read(CsvDirectoriesConfig.class, inputStream);
            log.info("Read directories config {}", config);
            return config;
        }
    }

    @Bean
    public TaskScheduler taskScheduler(CsvLoaderConfig csvLoaderConfig) {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.setPoolSize(csvLoaderConfig.getThreadPoolSize());

        Logger logger = LoggerFactory.getLogger(taskScheduler.getClass());
        taskScheduler.setErrorHandler(t -> logger.error("Error on scheduled task", t));

        return taskScheduler;
    }

}
