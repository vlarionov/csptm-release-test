package ru.advantum.csptm.service.csvloader.config;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Root(name = "directory")
public class CsvDirectoryConfig {
    /**
     * Наименование директории для файлов
     */
    @Attribute(name = "name")
    private String name;

    /**
     * Период обновления директории
     */
    @Attribute(name = "period", required = false)
    private int period = 30;

    /**
     * Единицы времени для периода
     */
    @Attribute(name = "time_unit", required = false)
    private String timeUnit = TimeUnit.SECONDS.name();

    /**
     * Префикс фильтра для временных файлов
     */
    @Attribute(name = "temp_prefix", required = false)
    private String tempPrefix = "_";

    /**
     * Кодировка целевых файлов
     */
    @Attribute(name = "encoding", required = false)
    private String encoding = "UTF-8";

    /**
     * Шаблон даты в поле партиционирования
     */
    @Attribute(name = "date_template", required = false)
    private String dataDateTemplate = "yyyy-MM-dd'T'HH:mm:ss";

    /**
     * Использовать дефолтную таблицу, если не найдется подходящая партиция
     */
    @Attribute(name = "use_default_table_if_absent", required = false)
    private boolean useDefaultTableIfAbsent = true;

    /**
     * Настройки партиционирования
     */
    @Element(name = "partition")
    private CsvPartitionConfig partitionConfig;

    /**
     * Список колонок
     */
    @Element(name = "columns")
    private String columns;

    /**
     * Настройки парсера файлов из директории
     */
    @Element(name = "csv-parser", required = false)
    private CsvParserConfig parserConfig = new CsvParserConfig();

    public String getName() {
        return name;
    }

    public int getPeriod() {
        return period;
    }

    public TimeUnit getTimeUnit() {
        return TimeUnit.valueOf(timeUnit);
    }

    public String getTempPrefix() {
        return tempPrefix;
    }

    public String getEncoding() {
        return encoding;
    }

    public String getDataDateTemplate() {
        return dataDateTemplate;
    }

    public boolean isUseDefaultTableIfAbsent() {
        return useDefaultTableIfAbsent;
    }

    public String getColumns() {
        return columns;
    }

    public CsvPartitionConfig getPartitionConfig() {
        return partitionConfig;
    }

    public CsvParserConfig getParserConfig() {
        return parserConfig;
    }

    public int getPartitionColumnIndex() {
        return Arrays.stream(columns.split(","))
                .map(String::trim)
                .collect(Collectors.toList())
                .indexOf(partitionConfig.getColumn());
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("name", name)
                .append("period", period)
                .append("timeUnit", timeUnit)
                .append("tempPrefix", tempPrefix)
                .append("encoding", encoding)
                .append("dataDateTemplate", dataDateTemplate)
                .append("useDefaultTableIfAbsent", useDefaultTableIfAbsent)
                .append("partitionConfig", partitionConfig)
                .append("columns", columns)
                .append("parserConfig", parserConfig)
                .toString();
    }
}
