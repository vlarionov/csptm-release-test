package ru.advantum.csptm.service.csvloader.config;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import ru.advantum.config.common.db.DatabaseConnection;

@Validated
@Component
@ConfigurationProperties(prefix = "csv-loader")
public class CsvLoaderConfig {

    @Valid
    @NotNull
    private int threadPoolSize;

    public int getThreadPoolSize() {
        return threadPoolSize;
    }

    public void setThreadPoolSize(int threadPoolSize) {
        this.threadPoolSize = threadPoolSize;
    }
}
