package ru.advantum.csptm.service.csvloader;

import java.sql.SQLException;
import java.util.List;

import com.google.common.base.Joiner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class TableMetaDataProvider {


    private final JdbcTemplate jdbcTemplate;
    static final String CREATE_DEFAULT_INHERITS_TABLE_AND_INDEXES =
            "select adt.csv_loader_pkg$create_default_inherits_table_and_indexes(?);";
    static final String COPY_COMMENTS_SQL = "select adt.csv_loader_pkg$copy_comments(?,?);";
    static final String IS_TABLE_EXIST_SQL = "SELECT adt.csv_loader_pkg$is_exist_table(?,?);";
    static final String GET_COLUMNS = "SELECT column_name\n"
            + "FROM information_schema.columns\n"
            + "WHERE table_schema = '%s'\n"
            + "      AND table_name   = '%s'";


    @Autowired
    public TableMetaDataProvider(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Cacheable("tableExistences")
    public boolean isTableExist(String tableName, String suffix) {
        return jdbcTemplate.queryForObject(IS_TABLE_EXIST_SQL, new Object[]{tableName, suffix}, boolean.class);
    }

    public void createInheritTable(String tableName) throws SQLException {
        jdbcTemplate.update(CREATE_DEFAULT_INHERITS_TABLE_AND_INDEXES, tableName);
        jdbcTemplate.update(COPY_COMMENTS_SQL, tableName, tableName + "_default");
    }

    public String getColumns(String tableName) {
        String schema = tableName.split("\\.")[0];
        String table = tableName.split("\\.")[1];
        List<String> columns =  jdbcTemplate.queryForList(String.format(GET_COLUMNS, schema, table), String.class);
        return Joiner.on(", ").join(columns);
    }

    public String getIndexes(String tableName) {
        String schema = tableName.split("\\.")[0];
        String table = tableName.split("\\.")[1];
        List<String> columns =  jdbcTemplate.queryForList(String.format(GET_COLUMNS, schema, table), String.class);
        return Joiner.on(", ").join(columns);
    }
}
