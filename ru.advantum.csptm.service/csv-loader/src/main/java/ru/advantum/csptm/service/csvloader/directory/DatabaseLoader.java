package ru.advantum.csptm.service.csvloader.directory;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.advantum.csptm.service.csvloader.config.CsvDirectoryConfig;

import javax.sql.DataSource;

import java.io.FileInputStream;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;

import ru.advantum.csptm.service.csvloader.TableMetaDataProvider;

public class DatabaseLoader {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseLoader.class);

    private static final String STATEMENT_FORMAT =
            "COPY %s (%s) FROM STDIN WITH DELIMITER ';' CSV HEADER ENCODING 'UTF-8'";

    private final String defaultTable;
    private final String columns;
    private final boolean useDefaultTableIfAbsent;
    private final String table;
    private static final String SUFFIX = "default";
    private final DataSource dataSource;
    private final TableMetaDataProvider tableMetaDataProvider;

    public DatabaseLoader(CsvDirectoryConfig config, DataSource dataSource,
            TableMetaDataProvider tableMetaDataProvider) {

        this.table = config.getPartitionConfig().getTable();
        this.defaultTable = table + "_default";
        this.tableMetaDataProvider = tableMetaDataProvider;
        this.columns = config.getColumns()!=null?config.getColumns():tableMetaDataProvider.getColumns(table);
        this.useDefaultTableIfAbsent = config.isUseDefaultTableIfAbsent();
        this.dataSource = dataSource;
    }

    private void copyFileToTable(Path filePath, String tableName) throws Exception {
        try (Connection connection = dataSource.getConnection();
                FileInputStream inputStream = new FileInputStream(filePath.toString())) {
            CopyManager copyManager = new CopyManager(connection.unwrap(BaseConnection.class));
            String statement = String.format(STATEMENT_FORMAT, tableName, columns);
            long rowCount = copyManager.copyIn(statement, inputStream);
            LOGGER.info("Loaded {} rows from {} into the table {}", rowCount,
                    filePath.toFile().getName(), tableName);
        }
    }

    public void load(Path filePath, String tableName, String suffixDate) throws Exception {

        try {
            if (tableMetaDataProvider.isTableExist(table, suffixDate)) {
                copyFileToTable(filePath, tableName + "_" + suffixDate);
            } else if (useDefaultTableIfAbsent) {
                if (tableMetaDataProvider.isTableExist(table, SUFFIX)) {
                    copyFileToTable(filePath, tableName + "_" + SUFFIX);
                } else {
                    tableMetaDataProvider.createInheritTable(tableName);
                    copyFileToTable(filePath, tableName + "_" + SUFFIX);
                }
            }
        } catch (SQLException e) {
            if (!defaultTable.equals(tableName + "_" + suffixDate) && handleIsUndefinedTableException(e)) {
                return;
            }
            throw e;
        }
    }


    /**
     * @return true if exception handled
     */
    private boolean handleIsUndefinedTableException(SQLException e)
            throws Exception {
        if (!"42P01".equals(e.getSQLState())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("defaultTable", defaultTable)
                .append("columns", columns)
                .append("useDefaultTableIfAbsent", useDefaultTableIfAbsent)
                .append("table", table)
                .toString();
    }
}
