/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.advantum.integration.yandex.exp;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

/**
 * @author abc
 */
@Root(name = "yandex-track-loader")
public class YandexTrackLoaderConfig {

    @Attribute(required = false)
    public int requestPeriod = 30000;

    @Attribute(required = false)
    public int requestTimeout = 5000;

    @Attribute(required = false)
    public int packetsQueueCapacity = 1_000_000;

    @Attribute(required = false)
    public int maxDelayGPStimeMills = 90 * 1000;

    @Attribute(required = true)
    public String clid;

    @Attribute(name = "url")
    public String url;

    @Element(name = "rmq-service")
    public final JepRmqServiceConfig serviceConfig;

    @Element(name = "database")
    public final DatabaseConnection database;

    public YandexTrackLoaderConfig() {
        serviceConfig = new JepRmqServiceConfig();
        database = new DatabaseConnection();
    }
}
