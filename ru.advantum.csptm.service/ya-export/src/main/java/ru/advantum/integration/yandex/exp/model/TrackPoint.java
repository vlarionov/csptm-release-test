package ru.advantum.integration.yandex.exp.model;

import java.time.Instant;

public class TrackPoint {

    private final long idTr;
    private final float lat;
    private final float lon;
    private final int speed;
    private final int heading;
    private final Instant gpsTime;

    public TrackPoint(long idTr, float lat, float lon, int speed, int heading, Instant gpsTime) {
        this.idTr = idTr;
        this.lat = lat;
        this.lon = lon;
        this.speed = speed;
        this.heading = heading;
        this.gpsTime = gpsTime;
    }

    public long getIdTr() {
        return idTr;
    }

    public float getLat() {
        return lat;
    }

    public float getLon() {
        return lon;
    }

    public int getSpeed() {
        return speed;
    }

    public int getHeading() {
        return heading;
    }

    public Instant getGpsTime() {
        return gpsTime;
    }

    @Override
    public String toString() {
        return "TrackPoint{" +
                "idTr=" + idTr +
                ", lat=" + lat +
                ", lon=" + lon +
                ", speed=" + speed +
                ", heading=" + heading +
                ", gpsTime=" + gpsTime +
                '}';
    }
}
