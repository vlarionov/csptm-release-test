package ru.advantum.integration.yandex.exp.model;

public class TrData {
    final long trId;
    final String uuid;
    final String vehicleType;
    final String route;
    final int wheelchairAccessible;
    final int bikesAllowed;
    final int airConditioning;
    final int lowFloor;
    final int toDepot;

    TrData() {
        this.trId = 0;
        this.uuid = "";
        this.vehicleType = "";
        this.route = "";
        this.wheelchairAccessible = 0;
        this.bikesAllowed = 0;
        this.airConditioning = 0;
        this.lowFloor = 0;
        this.toDepot = 0;
    }

    public long getTrId() {
        return trId;
    }

    public String getUuid() {
        return uuid;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public String getRoute() {
        return route;
    }

    public int getWheelchairAccessible() {
        return wheelchairAccessible;
    }

    public int getBikesAllowed() {
        return bikesAllowed;
    }

    public int getAirConditioning() {
        return airConditioning;
    }

    public int getLowFloor() {
        return lowFloor;
    }

    public int getToDepot() {
        return toDepot;
    }

}
