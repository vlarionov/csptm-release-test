package ru.advantum.integration.yandex.exp;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.jep.proto.CommonMessageType;
import ru.advantum.csptm.jep.proto.UnitPacket;
import ru.advantum.csptm.jep.proto.UnitPacketsBundle;
import ru.advantum.integration.yandex.exp.model.*;
import ru.advantum.rabbitmq.consumer.RmqJsonTalker;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * @author abc
 */
public class YandexTrackLoader {

    private static final Logger LOG = LoggerFactory.getLogger(YandexTrackLoader.class);
    private final YandexTrackLoaderConfig config;

    private final BlockingQueue<TrackPoint> queue;

    private final ScheduledExecutorService DM = Executors.newSingleThreadScheduledExecutor();
    private final AtomicReference<Map<Long, TrData>> dataTr = new AtomicReference<>(new HashMap<>());

    // Передача данных в Yandex
    private final String clid;
    private volatile String url;
    private final ScheduledExecutorService EX = Executors.newSingleThreadScheduledExecutor();
    private final HttpClient client;
    protected final Marshaller mTracks;

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMyyyy':'HHmmss").withZone(ZoneOffset.UTC);

    private final long maxDelayGPStimeMills;
    private AtomicReference<Instant> minLoadTimestamp;

    private YandexTrackLoader(YandexTrackLoaderConfig config) throws JAXBException {

        this.config = config;

        this.mTracks = JAXBContext.newInstance(Tracks.class).createMarshaller();
        this.mTracks.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.FALSE);

        queue = new LinkedBlockingQueue<>(config.packetsQueueCapacity);

        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(config.requestTimeout)
                .setSocketTimeout(config.requestTimeout)
                .build();
        this.client = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig).build();

        this.clid = config.clid;
        this.url = config.url;
        this.maxDelayGPStimeMills = this.config.maxDelayGPStimeMills - this.config.requestPeriod;
        this.minLoadTimestamp = new AtomicReference<>(Instant.now().minusMillis(this.maxDelayGPStimeMills));
    }

    private void consumeCoords(UnitPacketsBundle unitPacketsBundle) {
        for (UnitPacket unitPacket : unitPacketsBundle.getPackets()) {
            if (unitPacket != null && unitPacket.getTelematic() != null) {
                Instant minLoadTime = minLoadTimestamp.get();
                Instant gpsTime = unitPacket.getTelematic().getGpsTime();
                if (gpsTime != null && gpsTime.isAfter(minLoadTime)) {

                    try {
                        queue.put(new TrackPoint(unitPacket.getTrId(),
                                unitPacket.getTelematic().getLat(),
                                unitPacket.getTelematic().getLon(),
                                unitPacket.getTelematic().getSpeed() == null ? 0 : unitPacket.getTelematic().getSpeed(),
                                unitPacket.getTelematic().getHeading() == null ? 0 : unitPacket.getTelematic().getHeading(),
                                unitPacket.getTelematic().getGpsTime()
                        ));
                    } catch (InterruptedException ignored) {

                    }
                }
            }
        }
    }

    private void updateTrDataMapping() {
        try (SqlSession session = SqlMapperSingleton.getInstance().openSession()) {
            LOG.info("Updating tr data mapping");
            TrDataMapper mapper = session.getMapper(TrDataMapper.class);
            dataTr.set(mapper.TrData().stream().collect(Collectors.toMap(TrData::getTrId, d -> d)));
            LOG.info("Updated size {}", dataTr.get().size());
        } catch (Throwable ex) {
            LOG.error("Updating tr data mapping", ex);
        }
    }

    private void groupAndPostData() {
        final List<TrackPoint> drainItem = new ArrayList<>();
        try {
            final long timeStart = System.currentTimeMillis();

            LOG.debug("queue size: {}", queue.size());
            queue.drainTo(drainItem);
            LOG.debug("queue size after drain: {}", queue.size());

            Tracks tracks = new Tracks(clid);
            Map<Long, TrData> dataMap = dataTr.get();

            int trAmountWithMissingData = 0;

            // Группировка данных
            Map<Long, List<TrackPoint>> groupedData = drainItem.stream().collect(Collectors.groupingBy(TrackPoint::getIdTr));
            for (Long id : groupedData.keySet()) {
                TrData d = dataMap.get(id);
                if (d != null) {
                    Tracks.Track t = new Tracks.Track(
                            d.getUuid(),
                            d.getRoute(),
                            d.getVehicleType(),
                            new Tracks.Properties(
                                    d.getWheelchairAccessible(),
                                    d.getBikesAllowed(),
                                    d.getAirConditioning(),
                                    d.getLowFloor(),
                                    d.getToDepot()
                            ));

                    for (TrackPoint p : groupedData.get(id)) {
                        t.addPoint(new Tracks.Point(p.getLat(),
                                p.getLon(),
                                p.getSpeed(),
                                p.getHeading(),
                                formatter.format(p.getGpsTime())
                        ));
                    }

                    tracks.addTrack(t);
                } else {
                    trAmountWithMissingData++;
                }
            }

            LOG.info("trs amount with missing data: {}", trAmountWithMissingData);

            if (tracks.trackList.size() > 0) {
                // Отправка данных
                final ByteArrayOutputStream out = new ByteArrayOutputStream();
                out.write("data=".getBytes("UTF-8"));
                mTracks.marshal(tracks, out);
                out.write("&compressed=0".getBytes("UTF-8"));
                AbstractHttpEntity entity = new ByteArrayEntity(out.toByteArray(), ContentType.APPLICATION_FORM_URLENCODED);
                HttpPost post = new HttpPost(url);
                post.setEntity(entity);

                HttpResponse response = client.execute(post);

                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    LOG.info("Sent to ya {} packets in {} mills", drainItem.size(), System.currentTimeMillis() - timeStart);
                    LOG.debug("HttpResponse: {}", EntityUtils.toString(response.getEntity()));
                } else {
                    LOG.error("error on post data, response: {}, packets: {}", EntityUtils.toString(response.getEntity()), drainItem);
                }
            }

            minLoadTimestamp.set(Instant.now().minusMillis(this.maxDelayGPStimeMills));
            LOG.debug("currentTimeMillis {} ; maxDelayGPStimeMills {}; set new minLoadTimestamp: {}",
                    System.currentTimeMillis(), maxDelayGPStimeMills, minLoadTimestamp.get());

        } catch (Throwable ex) {
            LOG.error("error on post data", ex);
            LOG.error("packets: {}", drainItem);
        }
    }

    public void start() throws Exception {

        SqlMapperSingleton.setConfiguration(config.database.getConnectionPool());

        updateTrDataMapping();
        DM.scheduleAtFixedRate(this::updateTrDataMapping, 60, 60, TimeUnit.SECONDS);

        EX.scheduleAtFixedRate(this::groupAndPostData, config.requestPeriod, config.requestPeriod, TimeUnit.MILLISECONDS);

        RmqJsonTalker.newBuilder()
                .setConsumerConfig(true, true, config.serviceConfig.consumer)
                .addConsumer(CommonMessageType.COORDS.name(),
                        UnitPacketsBundle.class,
                        (consumerTag, envelope, basicProperties, message) -> consumeCoords(message))
                .connect(config.serviceConfig.connection.newConnection());
    }

    public static void main(String[] args) throws Exception {
        final YandexTrackLoaderConfig config = Configuration.unpackConfig(YandexTrackLoaderConfig.class, "ya-export.xml");
        new YandexTrackLoader(config).start();
    }
}
