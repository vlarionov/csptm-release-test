package ru.advantum.integration.yandex.exp.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author kukushkin
 * @modified abc
 */
@XmlRootElement(name = "tracks")
public class Tracks {

    public static class Point {
        @XmlAttribute(name = "longitude", required = true)
        private final float longitude;
        @XmlAttribute(name = "latitude", required = true)
        private final float latitude;
        @XmlAttribute(name = "avg_speed", required = true)
        private final long avg_speed;
        @XmlAttribute(name = "direction", required = true)
        private final long direction;
        @XmlAttribute(name = "time", required = true)
        private final String time;
        @XmlAttribute(name = "category", required = false)
        private final String category = "s";

        public Point(float latitude, float longitude, long avg_speed, long direction, String time) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.avg_speed = avg_speed;
            this.direction = direction;
            this.time = time;
        }

        public Point() {
            this.latitude = 0;
            this.longitude = 0;
            this.avg_speed = 0;
            this.direction = 0;
            this.time = null;
        }
    }

    public static class Properties {
        @XmlAttribute(name = "wheelchair_accessible", required = true)
        private final int wheelchairAccessible;
        @XmlAttribute(name = "bikes_allowed", required = true)
        private final int bikesAllowed;
        @XmlAttribute(name = "air_conditioning", required = true)
        private final int airConditioning;
        @XmlAttribute(name = "low_floor", required = true)
        private final int lowFloor;
        @XmlAttribute(name = "to_depot", required = true)
        private final int toDepot;

        public Properties(int wheelchairAccessible, int bikesAllowed, int airConditioning, int lowFloor, int toDepot) {
            this.wheelchairAccessible = wheelchairAccessible;
            this.bikesAllowed = bikesAllowed;
            this.airConditioning = airConditioning;
            this.lowFloor = lowFloor;
            this.toDepot = toDepot;
        }

        public Properties() {
            this.wheelchairAccessible = 0;
            this.bikesAllowed = 0;
            this.airConditioning = 0;
            this.lowFloor = 0;
            this.toDepot = 0;
        }
    }

    public static class Track {
        @XmlAttribute(name = "uuid", required = true)
        private final String uuid;
        @XmlAttribute(name = "route", required = true)
        private final String route;
        @XmlAttribute(name = "vehicle_type", required = true)
        private final String vehicle_type;

        @XmlElement(name = "properties")
        public final Properties properties;

        @XmlElement(name = "point")
        public final List<Point> pointList = new ArrayList<>();

        public Track(String uuid, String route, String vehicle_type, Properties properties) {
            this.uuid = uuid;
            this.vehicle_type = vehicle_type;
            this.route = route;
            this.properties = properties;
        }

        public Track() {
            this.uuid = null;
            this.vehicle_type = "bus";
            this.route = "";
            this.properties = null;
        }

        public void addPoint(Point point) {
            pointList.add(point);
        }
    }

    @XmlAttribute(name = "clid", required = true)
    private final String clid;

    @XmlElement(name = "track")
    public final List<Track> trackList = new ArrayList<>();

    public Tracks(String clid) {
        this.clid = clid;
    }

    public Tracks() {
        this.clid = null;
    }

    public void addTrack(Track track) {
        trackList.add(track);
    }
}
