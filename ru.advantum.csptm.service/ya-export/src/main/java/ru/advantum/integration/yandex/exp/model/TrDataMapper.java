package ru.advantum.integration.yandex.exp.model;

import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface TrDataMapper {

    @Select("select * from yandex.v_tr")
    List<TrData> TrData();
}
