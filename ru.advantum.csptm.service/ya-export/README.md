ya-export - Сервис экспорта данных в Яндекс
============================

Сервис получает справочные данные из БД `database` и основные из RabbitMQ `rmq-connection` и передает их в
заданном формате сервису Яндекс `url` с периодичностью `requestPeriod` 

Настройки сервиса 
-----------
ya-export/advconf/ya-export.xml

``````````````    
<ya-export clid="mosgortrans"
           url="http://tst.extjams.maps.yandex.net/.../1.x/"
           requestPeriod="10000">
        
    <rmq-service>
        <rmq-connection host="mgt-hpx01.mgt.dev"
                        virtualHost="csptm"
                        username="**"
                        password="**"
                        heartbeatInterval="10"
                        automaticRecovery="true"/>
        <rmq-consumer queue="ya-export"/>
    </rmq-service>

    <database url="jdbc:postgresql://mgt-pg01.mgt.dev:5432/csptm-dev"
              program="ya-export"
              username="**"
              password="**"
              autoCommit="false"
              poolable="true">
        <pool initialSize="1"
              maxActive="4"
              maxWait="1"
              maxIdle="1"
              maxOpenPreparedStatement="5"
              driverClassName="org.postgresql.Driver"
              validationQuery="select 1"/>
    </database>

</ya-export>

``````````````

Настройки логирования 
-----------
ya-export/advconf/log4j.prop
    
