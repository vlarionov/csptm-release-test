package ru.advantum.csptm.service.audit.notifier;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

@Root(name = "sys-event-notifier")
public class SysEventNotifierConfig {
    @Attribute
    public long checkEventsRate;

    @Element(name = "database")
    public DatabaseConnection databaseConfig;

    @Element(name = "rmq-service")
    public JepRmqServiceConfig rmqServiceConfig;
}
