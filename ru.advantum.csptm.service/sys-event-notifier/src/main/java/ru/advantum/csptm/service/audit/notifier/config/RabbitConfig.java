package ru.advantum.csptm.service.audit.notifier.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;
import ru.advantum.csptm.service.audit.notifier.SysEventNotifierConfig;

@Configuration
public class RabbitConfig  {
    @Bean
    public JepRmqServiceConfig jepRmqServiceConfig(SysEventNotifierConfig sysEventNotifierConfig) {
        return sysEventNotifierConfig.rmqServiceConfig;
    }

}
