package ru.advantum.csptm.service.audit.notifier.model;

public class SysEvent {
    private final int sysEventId;

    private final int sysAuditId;

    private final String auditName;

    private final String eventAttributes;

    private final String messageTemplate;

    public SysEvent(Integer sysEventId, Integer sysAuditId, String auditName, String eventAttributes, String messageTemplate) {
        this.sysEventId = sysEventId;
        this.sysAuditId = sysAuditId;
        this.auditName = auditName;
        this.eventAttributes = eventAttributes;
        this.messageTemplate = messageTemplate;
    }

    public Integer getSysEventId() {
        return sysEventId;
    }

    public Integer getSysAuditId() {
        return sysAuditId;
    }

    public String getAuditName() {
        return auditName;
    }

    public String getEventAttributes() {
        return eventAttributes;
    }

    public String getMessageTemplate() {
        return messageTemplate;
    }

    @Override
    public String toString() {
        return "SysEvent{" +
                "sysEventId=" + sysEventId +
                ", sysAuditId=" + sysAuditId +
                ", auditName='" + auditName + '\'' +
                ", eventAttributes='" + eventAttributes + '\'' +
                ", messageTemplate='" + messageTemplate + '\'' +
                '}';
    }
}
