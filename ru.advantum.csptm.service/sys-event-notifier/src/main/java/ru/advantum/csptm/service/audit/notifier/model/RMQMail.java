package ru.advantum.csptm.service.audit.notifier.model;

public class RMQMail {
    private final String to;
    private final String subject;
    private final String body;
    private final String filename;
    private final String filePath;

    public RMQMail(String to, String subject, String body, String filename, String filePath) {
        this.to = to;
        this.subject = subject;
        this.body = body;
        this.filename = filename;
        this.filePath = filePath;
    }

    public String getTo() {
        return this.to;
    }

    public String getSubject() {
        return this.subject;
    }

    public String getBody() {
        return this.body;
    }

    public String getFilename() {
        return this.filename;
    }

    public String getFilePath() {
        return this.filePath;
    }
}