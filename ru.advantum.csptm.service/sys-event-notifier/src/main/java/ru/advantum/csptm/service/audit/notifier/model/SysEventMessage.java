package ru.advantum.csptm.service.audit.notifier.model;

public class SysEventMessage {
    private final String destination;

    private final String message;

    public SysEventMessage(String destination, String message) {
        this.destination = destination;
        this.message = message;
    }

    public String getDestination() {
        return destination;
    }

    public String getMessage() {
        return message;
    }
}
