package ru.advantum.csptm.service.audit.notifier.model;

public class SysEventSubscriber {
    private final boolean emailSubscriber;

    private final String email;

    private final boolean phoneSubscriber;

    private final String phoneNumber;

    public SysEventSubscriber(Boolean emailSubscriber, String email, Boolean phoneSubscriber, String phoneNumber) {
        this.emailSubscriber = emailSubscriber;
        this.email = email;
        this.phoneSubscriber = phoneSubscriber;
        this.phoneNumber = phoneNumber;
    }

    public boolean isEmailSubscriber() {
        return emailSubscriber;
    }

    public String getEmail() {
        return email;
    }

    public boolean isPhoneSubscriber() {
        return phoneSubscriber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
