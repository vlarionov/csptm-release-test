package ru.advantum.csptm.service.audit.notifier.service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.service.audit.notifier.db.SysEventNotificationMapper;
import ru.advantum.csptm.service.audit.notifier.model.RMQMail;
import ru.advantum.csptm.service.audit.notifier.model.SysEvent;
import ru.advantum.csptm.service.audit.notifier.model.SysEventSubscriber;
import ru.advantum.csptm.service.sms.model.SmsMessage;
import ru.advantum.csptm.spring.rmq.TypeAdder;

import java.util.Map;

@Service
public class SysEventNotifier {
    private static final Logger LOGGER = LoggerFactory.getLogger(SysEventNotifier.class);

    private static final String EMAIL_ROUTING_KEY = "email";
    private static final String PHONE_ROUTING_KEY = "phone";

    private static final String NEXT_LINE_PLACEHOLDER = "\\$\\{nextLine\\}";

    private final Gson gson = new Gson();

    private final SysEventNotificationMapper sysEventNotificationMapper;

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public SysEventNotifier(SysEventNotificationMapper sysEventNotificationMapper, RabbitTemplate rabbitTemplate) {
        this.sysEventNotificationMapper = sysEventNotificationMapper;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Scheduled(fixedRateString = "#{applicationConfig.checkEventsRate}")
    public void checkEvents() {
        try {
            sysEventNotificationMapper.findNonNotifiedEvents().forEach(this::notify);
        } catch (Exception e) {
            LOGGER.error("Error checking event", e);
        }
    }

    private void notify(SysEvent sysEvent) {
        try {
            String message = buildMessage(sysEvent);
            if (message != null) {
                sysEventNotificationMapper
                        .findSubscribers(sysEvent.getSysAuditId())
                        .forEach(subscriber -> notify(subscriber, sysEvent.getAuditName(), message));
            }

            sysEventNotificationMapper.setSysEventNotified(sysEvent.getSysEventId());
        } catch (Exception e) {
            LOGGER.error("Error on notification {}", sysEvent, e);
        }
    }

    private void notify(SysEventSubscriber subscriber, String subject, String message) {
        if (subscriber.isEmailSubscriber() && subscriber.getEmail() != null) {
            rabbitTemplate.convertAndSend(
                    EMAIL_ROUTING_KEY,
                    new RMQMail(
                            subscriber.getEmail(),
                            subject,
                            message,
                            null,
                            null
                    ),
                    TypeAdder.of("RMQMail")
            );
        }
        if (subscriber.isPhoneSubscriber() && subscriber.getPhoneNumber() != null) {
            rabbitTemplate.convertAndSend(
                    PHONE_ROUTING_KEY,
                    new SmsMessage(
                            -1,
                            subscriber.getPhoneNumber(),
                            message,
                            null,
                            null,
                            null
                    ),
                    TypeAdder.of("SmsMessage")
            );
        }
    }

    private String buildMessage(SysEvent sysEvent) {
        String result = sysEvent.getMessageTemplate();

        JsonArray attributesArray = gson.fromJson(sysEvent.getEventAttributes(), JsonArray.class);

        if (attributesArray.size() == 0) {
            return null;
        }

        for (int i = 0; i < attributesArray.size(); i++) {
            JsonElement attributesElement = attributesArray.get(i);
            if (attributesElement.isJsonObject()) {
                for (Map.Entry<String, JsonElement> attribute : attributesElement.getAsJsonObject().entrySet()) {
                    result = result.replaceAll("\\$\\{" + attribute.getKey() + "\\}", attribute.getValue().getAsString());
                }
            }
        }

        result = result.replaceAll(NEXT_LINE_PLACEHOLDER, "\n");

        return result;
    }


}
