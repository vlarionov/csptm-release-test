package ru.advantum.csptm.service.audit.notifier;


import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Application {
    public static final String CONFIG_FILE_NAME = "sys-event-notifier.xml";

    public static void main(String[] args) {
        new SpringApplicationBuilder(Application.class).run(args);
    }

    @Bean
    public static SysEventNotifierConfig applicationConfig() throws Exception {
        return ru.advantum.config.Configuration.unpackConfig(
                SysEventNotifierConfig.class,
                CONFIG_FILE_NAME
        );
    }

}
