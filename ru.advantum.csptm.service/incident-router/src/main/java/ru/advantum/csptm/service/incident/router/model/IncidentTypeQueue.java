package ru.advantum.csptm.service.incident.router.model;

import lombok.Data;
import lombok.NonNull;

/**
 * Created by kukushkin on 11/29/17.
 */
@Data
public class IncidentTypeQueue {
    @NonNull
    private Integer incidentTypeId;
    @NonNull
    private String queueName;
}
