package ru.advantum.csptm.service.incident.router.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitManagementTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import ru.advantum.csptm.incident.model.Incident;
import java.net.MalformedURLException;

@Configuration
public class RmqConfig {
    @Autowired
    Environment environment;

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(Incident.INCIDENT_ROUTER_EXCHANGE);
    }



    @Bean
    RabbitManagementTemplate rabbitManagementTemplate() throws MalformedURLException {
        return new RabbitManagementTemplate(
                String.format("http://%s:15672/api/", environment.getProperty("spring.rabbitmq.host"))
                ,environment.getProperty("spring.rabbitmq.username")
                ,environment.getProperty("spring.rabbitmq.password")
            );
        }
}
