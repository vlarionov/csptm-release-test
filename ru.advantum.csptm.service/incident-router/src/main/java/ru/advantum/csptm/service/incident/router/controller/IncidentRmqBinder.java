package ru.advantum.csptm.service.incident.router.controller;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitManagementTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.service.incident.router.db.IncidentActionDao;
import ru.advantum.csptm.service.incident.router.model.IncidentTypeQueue;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by kukushkin on 11/29/17.
 */
@Service
@Slf4j
public class IncidentRmqBinder {
    @Autowired private AmqpAdmin admin;
    @Autowired Environment environment;
    private final TopicExchange exchange;
    private final String virtualHost;
    private final RabbitManagementTemplate rabbitManagementTemplate;
    private static final Logger LOGGER = LoggerFactory.getLogger(IncidentRmqBinder.class);
    private final IncidentActionDao incidentActionDao;
    private List<IncidentTypeQueue> incidentTypeQueue = new ArrayList<>();

    @Autowired
    public IncidentRmqBinder(IncidentActionDao incidentActionDao
                            ,RabbitManagementTemplate rabbitManagementTemplate
                            ,TopicExchange exchange
                            ,Environment environment) {
        this.incidentActionDao = incidentActionDao;
        this.rabbitManagementTemplate = rabbitManagementTemplate;
        this.exchange = exchange;
        virtualHost = environment.getProperty("spring.rabbitmq.virtual-host");
    }

    @Scheduled(initialDelay = 0, fixedDelay = 1 * 60 * 1000)
    private void bind() {

        List<Binding> bindings = rabbitManagementTemplate.getBindingsForExchange(virtualHost, exchange.getName());
        bindings.forEach(binding ->
                {
                    LOGGER.info("current binding RoutingKey: {} queue  {}", binding.getRoutingKey(), binding.getDestination());
                    incidentTypeQueue.add(new IncidentTypeQueue(Integer.valueOf(binding.getRoutingKey()), binding.getDestination()));
                }

        );


        List<IncidentTypeQueue> newSettings = incidentActionDao.getAll();

        final Collection<IncidentTypeQueue>  incidentTypeQueueToBind = new ArrayList<>(newSettings);
        incidentTypeQueueToBind.removeAll(new ArrayList<>(incidentTypeQueue));


        final Collection<IncidentTypeQueue>  incidentTypeQueueToUnbind = new ArrayList<>(incidentTypeQueue);
        incidentTypeQueueToUnbind.removeAll(new ArrayList<>(newSettings));

        incidentTypeQueueToBind.forEach( a -> {
            try {
            admin.declareBinding(BindingBuilder.bind(new Queue(a.getQueueName(), true)).to(exchange).with(a.getIncidentTypeId().toString()));LOGGER.info("add binding: Queue {}. Routing key {}.", a.getQueueName(), a.getIncidentTypeId());
            } catch (Throwable t) {
                LOGGER.error("incidentTypeQueueToUnbind", t);
            }
        });


        incidentTypeQueueToUnbind.forEach(a -> {
            try {
                LOGGER.info("remove binding: Queue {}. Routing key {}.",a.getQueueName(), a.getIncidentTypeId());
                admin.removeBinding(BindingBuilder.bind(new Queue(a.getQueueName(), true)).to(exchange).with(a.getIncidentTypeId().toString()));
            } catch (Throwable t) {
                LOGGER.error("incidentTypeQueueToUnbind", t);
            }
        });

        incidentTypeQueue = incidentActionDao.getAll();

    }
}
