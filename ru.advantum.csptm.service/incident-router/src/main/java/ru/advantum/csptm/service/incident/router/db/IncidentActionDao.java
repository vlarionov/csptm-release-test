package ru.advantum.csptm.service.incident.router.db;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.StatementType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.advantum.csptm.service.incident.router.model.IncidentTypeQueue;

import java.util.List;

@Repository
public class IncidentActionDao {

    private final IncidentActioMapper mapper;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public IncidentActionDao(IncidentActioMapper mapper, JdbcTemplate jdbcTemplate) {
        this.mapper = mapper;
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<IncidentTypeQueue>  getAll()
    {
        return mapper.getAll();
    }

    @Mapper
    private interface IncidentActioMapper {
        @Select("select incident_type_id as incidentTypeId, incident_handler_name as queueName " +
                "from ttb.incident_action " +
                "join ttb.incident_handler on incident_action.incident_handler_id = incident_handler.incident_handler_id"
        )
        @Options(useCache = false, statementType = StatementType.CALLABLE)
        List<IncidentTypeQueue> getAll();
    }
}
