package ru.advantum.integration.pbk.Model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Transport {
    @JsonProperty("tr_id")
    private String trId;
    @JsonProperty("garage_num")
    private String garageNum;
    @JsonProperty("licence")
    private String licence;
    @JsonProperty("dt_begin")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime beginDate;
    @JsonProperty("dt_end")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime endDate;
    @JsonProperty("tr_type_id")
    private String trTypeId;
    @JsonProperty("tr_status_id")
    private String trStatusId;
    @JsonProperty("tr_model_id")
    private String trModelId;
    @JsonProperty("depo_id")
    private String depoId;
    @JsonProperty("garage_num_add")
    private String garageNumAdd;
}
