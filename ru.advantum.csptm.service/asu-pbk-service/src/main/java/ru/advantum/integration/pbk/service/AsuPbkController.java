package ru.advantum.integration.pbk.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.advantum.integration.pbk.Model.*;
import ru.advantum.integration.pbk.config.PbkConfig;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
public class AsuPbkController {
    @Autowired
    private AsuPbkRequestService requestService;
    @Autowired
    private PbkConfig conf;

    @RequestMapping("/getTransport")
    public List<Transport> getTransport(@RequestParam(value = "begin_date", required = false) String strDate) {
        return requestService.getTransport();
    }

    @RequestMapping("/getTransportStatus")
    public List<TrStatus> getTrStatus() {
        return requestService.getTrStatus();
    }

    @RequestMapping("/getTransportModel")
    public List<ModelId> getModels() {
        return requestService.getModelId();
    }

    @RequestMapping("/getSchedules")
    public RouteVariant getRouteVariant(@RequestParam(value = "begin_date") String strDate,
                                        @RequestParam(value = "route_variant_id") long rvId) throws ParseException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(conf.getIncomingDatePattern());
        LocalDate date = LocalDate.parse(strDate, formatter);
        return requestService.getRouteVariant(date, rvId);
    }

    @RequestMapping("/getTransportCapacity")
    public List<TrCapacity> getRouteVariant() {
        return requestService.getTrCapacity();
    }

    @RequestMapping("/getOrderList")
    public List<Order> getOrderList(@RequestParam(value = "begin_date") String strDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(conf.getIncomingDatePattern());
        LocalDate date = LocalDate.parse(strDate, formatter);
        return requestService.getOrders(date);
    }

    @RequestMapping("/getDrivers")
    public List<Driver> getDrivers() {
        return requestService.getDrivers();
    }

    @RequestMapping("/getDriverShift")
    public List<Shift> getShifts() {
        return requestService.getShifts();
    }

    @RequestMapping("/getOuts")
    public FullOutInfo getOuts(@RequestParam(value = "schedules") Long schedule) {
        return requestService.getOuts(schedule);
    }

    @RequestMapping("/ASMPPData")
    public List<ASMPPdata> getAsmpp(@RequestParam(value = "begin_date") String strDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(conf.getIncomingDatePattern());
        LocalDate date = LocalDate.parse(strDate, formatter);
        return requestService.getAsmpp(date);
    }

    @RequestMapping("/getStopArrival")
    public Schedule getSchedule(@RequestParam(value = "schedules") Long ttVariantId) {
        return requestService.getSchedule(ttVariantId);
    }

    /*@RequestMapping("/ASKPData")
    public List<ASKPdata> getASkp(@RequestParam(value = "begin_date") String strDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(conf.getIncomingDatePattern());
        LocalDate date = LocalDate.parse(strDate, formatter);
        return requestService.getAskp(date);
    }*/

    // так как за один рабочий около 6 млн. валидаций, объем данных большой,
    // поэтому целесообразно передавать данные небольшими порциями и непрерывно формировать JSON поток
    @RequestMapping(value = "/ASKPData", method = RequestMethod.GET)
    public void getFile(@RequestParam(value = "begin_date") String strDate, HttpServletResponse response) throws IOException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(conf.getIncomingDatePattern());
        LocalDate date = LocalDate.parse(strDate, formatter);
        requestService.writeAskpToStream(response.getOutputStream(), date);
        response.flushBuffer();
    }
}

