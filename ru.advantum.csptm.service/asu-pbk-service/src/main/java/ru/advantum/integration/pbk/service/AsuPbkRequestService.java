package ru.advantum.integration.pbk.service;

import ru.advantum.integration.pbk.Model.*;

import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import java.util.List;

public interface AsuPbkRequestService {
    List<Transport> getTransport();

    List<TrStatus> getTrStatus();

    List<ModelId> getModelId();

    RouteVariant getRouteVariant(LocalDate date, Long routeV);

    List<TrCapacity> getTrCapacity();

    List<Order> getOrders(LocalDate date);

    List<Driver> getDrivers();

    List<Shift> getShifts();

    FullOutInfo getOuts(Long ttVariantId);

    List<ASMPPdata> getAsmpp(LocalDate date);

    List<ASKPdata> getAskp(LocalDate date);

    Schedule getSchedule(long ttVariantId);

    void writeAskpToStream(OutputStream outputStream, LocalDate date) throws IOException;
}
