package ru.advantum.integration.pbk.Model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * Данные АСМПП о параметрах пассажиропотоков по остановочным пунктам
 */
@Data
public class ASMPPdata {
    @JsonProperty("route_id")
    private Long routeId;
    @JsonProperty("tr_id")
    private Long trId;
    @JsonProperty("route_variant_id")
    private Long routeVariantId;
    @JsonProperty("out_num")
    private Long outNum;
    @JsonProperty("out_order_num")
    private Long outOrderNum;
    @JsonProperty("order_num")
    private Long stopOrderNum;
    @JsonProperty("stop_id")
    private Long stopId;
    @JsonProperty("time_fact")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime timeFact;
    @JsonProperty("in_cnt")
    private Integer inCounter;
    @JsonProperty("out_cnt")
    private Integer outCounter;
}
