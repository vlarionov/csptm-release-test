package ru.advantum.integration.pbk.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Shift {
    @JsonProperty("dr_shift_id")
    private Long shiftId;
    @JsonProperty("mode_id")
    private Long modeId;
    @JsonProperty("dr_shift_num")
    private Long shiftNum;
    @JsonProperty("dr_shift_name")
    private String shiftName;
}
