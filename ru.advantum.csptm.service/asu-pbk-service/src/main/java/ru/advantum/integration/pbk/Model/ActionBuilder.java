package ru.advantum.integration.pbk.Model;

/**
 *
 */
public final class ActionBuilder {
    private Long routeId;
    private String routeType;
    private String code;
    private Long trackId;
    private Long trackType;
    private Long outNum;
    private Long smenaNum;
    private Long outOrderNum;
    private Long opNum;
    private Long opEntryNumber;
    private String beginTime;
    private String endTime;

    private ActionBuilder() {
    }

    public static ActionBuilder anAction() {
        return new ActionBuilder();
    }

    public ActionBuilder withRouteId(Long routeId) {
        this.routeId = routeId;
        return this;
    }

    public ActionBuilder withRouteType(String routeType) {
        this.routeType = routeType;
        return this;
    }

    public ActionBuilder withCode(String code) {
        this.code = code;
        return this;
    }

    public ActionBuilder withTrackId(Long trackId) {
        this.trackId = trackId;
        return this;
    }

    public ActionBuilder withTrackType(Long trackType) {
        this.trackType = trackType;
        return this;
    }

    public ActionBuilder withOutNum(Long outNum) {
        this.outNum = outNum;
        return this;
    }

    public ActionBuilder withSmenaNum(Long smenaNum) {
        this.smenaNum = smenaNum;
        return this;
    }

    public ActionBuilder withOutOrderNum(Long outOrderNum) {
        this.outOrderNum = outOrderNum;
        return this;
    }

    public ActionBuilder withOpNum(Long opNum) {
        this.opNum = opNum;
        return this;
    }

    public ActionBuilder withOpEntryNumber(Long opEntryNumber) {
        this.opEntryNumber = opEntryNumber;
        return this;
    }

    public ActionBuilder withBeginTime(String beginTime) {
        this.beginTime = beginTime;
        return this;
    }

    public ActionBuilder withEndTime(String endTime) {
        this.endTime = endTime;
        return this;
    }

    public Action build() {
        Action action = new Action();
        action.setRouteId(routeId);
        action.setRouteType(routeType);
        action.setCode(code);
        action.setTrackId(trackId);
        action.setTrackType(trackType);
        action.setOutNum(outNum);
        action.setSmenaNum(smenaNum);
        action.setOutOrderNum(outOrderNum);
        action.setOpNum(opNum);
        action.setOpEntryNumber(opEntryNumber);
        action.setBeginTime(beginTime);
        action.setEndTime(endTime);
        return action;
    }
}
