package ru.advantum.integration.pbk.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class RouteVariant {
    @JsonProperty("route_id")
    private Long routeId;
    @JsonProperty("route_variant_id")
    private Long routeVariantId;
    @JsonProperty("sh_variant")
    List<TTVariant> ttVariantList = new ArrayList<>();
}
