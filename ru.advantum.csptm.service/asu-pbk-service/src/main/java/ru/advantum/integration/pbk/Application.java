package ru.advantum.integration.pbk;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.logging.Logger;

@SpringBootApplication
@EnableDiscoveryClient
public class Application {
    @Autowired
    Environment environment;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public Logger logger() {
        return Logger.getLogger(Application.class.getName());
    }

    ;

    @Bean
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate2() {
        jdbcTemplate.setFetchSize(Integer.valueOf(environment.getProperty("spring.jdbc.template.fetch-size")));
        return new NamedParameterJdbcTemplate(jdbcTemplate);
    }

}
