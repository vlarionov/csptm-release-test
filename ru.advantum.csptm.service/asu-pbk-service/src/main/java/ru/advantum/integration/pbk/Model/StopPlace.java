package ru.advantum.integration.pbk.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import ru.advantum.integration.pbk.Model.Action;

import java.util.ArrayList;
import java.util.List;

/**
 * Место посадки-высадки
 */
@Data
public class StopPlace {
    @JsonProperty("op_id")
    private Long stopPlaceId;
    @JsonProperty("op_type")
    private Long stopPlaceType;
    @JsonProperty("action_op")
    private List<Action> actions = new ArrayList<>();
}
