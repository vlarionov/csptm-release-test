package ru.advantum.integration.pbk.Model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class TTVariant {
    @JsonProperty("sh_id")
    private Long id;
    @JsonProperty("begin_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime beginDate;
    @JsonProperty("end_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime endDate;
    @JsonProperty("week_days")
    private Long weekDays;
    @JsonProperty("is_active")
    private Boolean isActive;
    @JsonProperty("is_main")
    private Boolean isMain;
    @JsonProperty("round")
    private List<Round> rounds = new ArrayList<>();
}
