package ru.advantum.integration.pbk.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Планового времени посещения ОП.
 */
@Data
public class Schedule {
    @JsonProperty("sh_id")
    private Long scheduleId;
    @JsonProperty("op")
    List<StopPlace> stopPlaceList = new ArrayList<>();
}
