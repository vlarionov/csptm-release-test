package ru.advantum.integration.pbk.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ModelId {
    @JsonProperty("tr_status_id")
    private String modelId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("tr_capacity_id")
    private String capacityId;
    @JsonProperty("seat_qty_total")
    private String seatTotal;
}
