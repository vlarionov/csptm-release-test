package ru.advantum.integration.pbk.Model;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TrCapacity {
    @JsonProperty("tr_capacity_id")
    private Long trCapacityId;
    @JsonProperty("qty")
    private Long seatNumber;
    @JsonProperty("full_name")
    private String fullName;
    @JsonProperty("short_name")
    private String shortName;
}
