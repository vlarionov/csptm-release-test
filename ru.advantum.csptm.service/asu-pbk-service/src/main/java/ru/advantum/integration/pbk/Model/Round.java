package ru.advantum.integration.pbk.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Round {
    @JsonProperty("r_id")
    private Long roundId;
    @JsonProperty("r_type")
    private String roundType;
    @JsonProperty("r_code")
    private String code;
}
