package ru.advantum.integration.pbk.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Выход
 */
@Data
@AllArgsConstructor
public class Out {
    // номер выхода
    @JsonProperty("out_num")
    private Long outNum;
    // Количество смен
    @JsonProperty("sm_count")
    private Long smCount;
}
