package ru.advantum.integration.pbk.Model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Событие прохождения места посадки-высадки
 */
@Data
@NoArgsConstructor
public class Action {
    @JsonProperty("r_id")
    private Long routeId;
    @JsonProperty("r_type")
    private String routeType;
    @JsonProperty("r_code")
    private String code;
    @JsonProperty("track_id")
    private Long trackId;
    @JsonProperty("track_type")
    private Long trackType;
    @JsonProperty("out_num")
    private Long outNum;
    @JsonProperty("sm_num")
    private Long smenaNum;
    @JsonProperty("out_order_num")
    private Long outOrderNum;
    @JsonProperty("op_num")
    private Long opNum;
    @JsonProperty("op_entry_number")
    private Long opEntryNumber;
    @JsonProperty("begin_time")
    private String beginTime;
    @JsonProperty("end_time")
    private String endTime;
}
