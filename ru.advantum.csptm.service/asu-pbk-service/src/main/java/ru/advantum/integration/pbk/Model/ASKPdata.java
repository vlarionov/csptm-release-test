package ru.advantum.integration.pbk.Model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * Данные АСКП
 */
@Data
public class ASKPdata {
    @JsonProperty("route_id")
    private Long routeId;
    @JsonProperty("tr_id")
    private Long trId;
    @JsonProperty("route_variant_id")
    private Long routeVariantId;
    @JsonProperty("out_num")
    private Long moveNum;
    @JsonProperty("out_order_num")
    private Long outOrderNum;
    @JsonProperty("order_num")
    private Long stopOrderNum;
    @JsonProperty("stop_id")
    private Long stopId;
    @JsonProperty("CHECK_TIME")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime checkTime;
    @JsonProperty("ASKP_CHECK_ID")
    private String askpCheckId;
    @JsonProperty("TICKET_TYPE_ID")
    private Long ticketTypeId;
}
