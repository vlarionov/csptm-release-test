package ru.advantum.integration.pbk.Model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;


/**
 * Наряд
 */
@Data
public class Order {
    @JsonProperty("order_list_id")
    private Long orderListId;
    @JsonProperty("depo_id")
    private Long depoId;
    @JsonProperty("route_variant_id")
    private Long routeVariantId;
    @JsonProperty("driver_id")
    private Long driverId;
    @JsonProperty("dr_shift_id")
    private Long drShiftId;
    @JsonProperty("tr_id")
    private Long trId;
    @JsonProperty("order_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate orderDate;
    @JsonProperty("out_num")
    private Long outNum;
    @JsonProperty("time_from")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime timeFrom;
    @JsonProperty("time_to")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime timeTo;
}
