package ru.advantum.integration.pbk.config;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("pbk.config")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PbkConfig {
    private String transportSql;
    private String trStatusIdSql;
    private String trModelIdSql;
    private String ttVariantSql;
    private String incomingDatePattern;
    private String trCapacitySql;
    private String orderSql;
    private String driverSql;
    private String shiftSql;
    private String outsSql;
    private String asmppSql;
    private String askpSql;
    private String scheduleSql;
}
