package ru.advantum.integration.pbk.Model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Driver {
    @JsonProperty("driver_id")
    private Long driverId;
    @JsonProperty("depo_id")
    private Long depoId;
    @JsonProperty("driver_name")
    private String driverName;
    @JsonProperty("driver_last_name")
    private String driverLastName;
    @JsonProperty("driver_middle_name")
    private String driverMiddleName;
    @JsonProperty("tab_num")
    private String tabNum;
    @JsonProperty("dt_begin")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime dateBegin;
    @JsonProperty("dt_end")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime dateEnd;
    @JsonProperty("sign_deleted")
    private Boolean deleted;
}
