package ru.advantum.integration.pbk.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Выходы варианта расписания
 */
@Data
public class FullOutInfo {
    @JsonProperty("sh_id")
    private Long scheduleId;
    @JsonProperty("outs")
    private List<Out> outs = new ArrayList<>();
}
