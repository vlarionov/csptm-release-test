package ru.advantum.integration.pbk.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TrStatus {
    @JsonProperty("tr_status_id")
    private String trStatusId;
    @JsonProperty("name")
    private String statusName;
}
