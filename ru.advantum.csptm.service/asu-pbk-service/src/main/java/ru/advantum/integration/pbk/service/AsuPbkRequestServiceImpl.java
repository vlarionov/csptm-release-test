package ru.advantum.integration.pbk.service;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Service;
import ru.advantum.integration.pbk.Model.*;
import ru.advantum.integration.pbk.config.PbkConfig;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Date;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class AsuPbkRequestServiceImpl implements AsuPbkRequestService {
    @Autowired
    private PbkConfig sqlRequests;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private Logger log;

    private SqlParameterSource namedParameters;

    @Override
    public List<Transport> getTransport() {
        return jdbcTemplate.query(sqlRequests.getTransportSql(), new BeanPropertyRowMapper(Transport.class));
    }

    @Override
    public List<TrStatus> getTrStatus() {
        return jdbcTemplate.query(sqlRequests.getTrStatusIdSql(), new BeanPropertyRowMapper(TrStatus.class));
    }

    @Override
    public List<ModelId> getModelId() {
        return jdbcTemplate.query(sqlRequests.getTrModelIdSql(), new BeanPropertyRowMapper(ModelId.class));
    }

    @Override
    public RouteVariant getRouteVariant(LocalDate date, Long routeV) {
        Map parameterMap = new HashMap();
        parameterMap.put("rv_id", routeV);
        parameterMap.put("begin_date", Date.valueOf(date));
        namedParameters = new MapSqlParameterSource(parameterMap);

        return namedParameterJdbcTemplate.query(sqlRequests.getTtVariantSql(), namedParameters,
                (ResultSet rs) -> {
                    RouteVariant rv = new RouteVariant();
                    long prevVariantId = -5;
                    long currVariantId = -1;
                    TTVariant ttVariant = new TTVariant();
                    while (rs.next()) {
                        rv.setRouteId(rs.getLong("route_id"));
                        rv.setRouteVariantId(rs.getLong("route_variant_id"));

                        currVariantId = rs.getLong("id");
                        if (currVariantId != prevVariantId) {
                            prevVariantId = currVariantId;
                            ttVariant = new TTVariant();
                            rv.getTtVariantList().add(ttVariant);
                            ttVariant.setId(currVariantId);
                            ttVariant.setBeginDate(rs.getTimestamp("begin_date").toLocalDateTime());
                            ttVariant.setEndDate(rs.getTimestamp("end_date").toLocalDateTime());
                            ttVariant.setWeekDays(rs.getLong("week_days"));
                            ttVariant.setIsActive(rs.getBoolean("is_active"));
                            ttVariant.setIsMain(rs.getBoolean("is_main"));
                        } else {// все тот же вариант расписания
                            ttVariant.getRounds().add(new Round(
                                    rs.getLong("round_id"),
                                    rs.getString("r_type"),
                                    rs.getString("code")
                            ));
                        }
                    }
                    return rv;
                }
        );
    }

    @Override
    public List<TrCapacity> getTrCapacity() {
        return jdbcTemplate.query(sqlRequests.getTrCapacitySql(), new BeanPropertyRowMapper(TrCapacity.class));
    }

    @Override
    public List<Order> getOrders(LocalDate date) {
        Map parameterMap = new HashMap();
        parameterMap.put("begin_date", Date.valueOf(date));
        namedParameters = new MapSqlParameterSource(parameterMap);

        return namedParameterJdbcTemplate.query(sqlRequests.getOrderSql(), namedParameters,
                new BeanPropertyRowMapper(Order.class));
    }

    @Override
    public List<Driver> getDrivers() {
        return jdbcTemplate.query(sqlRequests.getDriverSql(), new BeanPropertyRowMapper(Driver.class));
    }

    @Override
    public List<Shift> getShifts() {
        return jdbcTemplate.query(sqlRequests.getShiftSql(), new BeanPropertyRowMapper(Shift.class));
    }

    @Override
    public FullOutInfo getOuts(Long ttVariantId) {
        Map parameterMap = new HashMap();
        parameterMap.put("variant_id", ttVariantId);
        namedParameters = new MapSqlParameterSource(parameterMap);

        return namedParameterJdbcTemplate.query(sqlRequests.getOutsSql(), namedParameters,
                (ResultSet rs) -> {
                    FullOutInfo outInfo = new FullOutInfo();
                    while (rs.next()) {
                        outInfo.setScheduleId(rs.getLong("tt_variant_id"));
                        outInfo.getOuts().add(
                                new Out(rs.getLong("tt_out_num"), rs.getLong("count"))
                        );
                    }
                    return outInfo;
                }
        );
    }

    @Override
    public List<ASMPPdata> getAsmpp(LocalDate date) {
        Map parameterMap = new HashMap();
        parameterMap.put("begin_date", Date.valueOf(date));
        namedParameters = new MapSqlParameterSource(parameterMap);

        return namedParameterJdbcTemplate.query(sqlRequests.getAsmppSql(), namedParameters,
                new BeanPropertyRowMapper(ASMPPdata.class));
    }

    @Override
    public List<ASKPdata> getAskp(LocalDate date) {
        Map parameterMap = new HashMap();
        parameterMap.put("begin_date", Date.valueOf(date));
        namedParameters = new MapSqlParameterSource(parameterMap);

        return namedParameterJdbcTemplate.query(sqlRequests.getAskpSql(), namedParameters,
                new BeanPropertyRowMapper(ASKPdata.class));
    }

    @Override
    public Schedule getSchedule(long ttVariantId) {
        Map parameterMap = new HashMap();
        parameterMap.put("tt_variant_id", ttVariantId);
        namedParameters = new MapSqlParameterSource(parameterMap);

        return namedParameterJdbcTemplate.query(sqlRequests.getScheduleSql(), namedParameters,
                (ResultSet rs) -> {
                    Schedule schedule = new Schedule();
                    StopPlace stopPlace = null;
                    Action action;
                    ActionBuilder actionBuilder;
                    long prevOpId = -222;
                    long prevOpType = -222;
                    long opId = -222;
                    long opType = -222;
                    while (rs.next()) {
                        schedule.setScheduleId(rs.getLong("sh_id"));
                        opId = rs.getLong("op_id");
                        opType = rs.getLong("op_type");
                        // параметры для новой остановки
                        if ((opId != prevOpId) && (opType != prevOpType)) {
                            prevOpId = opId;
                            prevOpType = opType;
                            stopPlace = new StopPlace();
                            schedule.getStopPlaceList().add(stopPlace);
                            stopPlace.setStopPlaceId(opId);
                            stopPlace.setStopPlaceType(opType);
                        }
                        actionBuilder = ActionBuilder.anAction();

                        action = actionBuilder.withRouteId(rs.getLong("r_id"))
                                .withRouteType(rs.getString("r_type"))
                                .withCode(rs.getString("r_code"))
                                .withTrackId(rs.getLong("track_id"))
                                .withTrackType(rs.getLong("move_direction_id"))
                                .withOutNum(rs.getLong("out_num"))
                                .withSmenaNum(rs.getLong("dr_shift_num"))
                                .withOutOrderNum(rs.getLong("row_number"))
                                .withOpNum(rs.getLong("op_num"))
                                .withOpEntryNumber(rs.getLong("op_num"))
                                .withBeginTime(rs.getString("time_begin"))
                                .withBeginTime(rs.getString("time_end"))
                                .build();

                        stopPlace.getActions().add(action);
                    }
                    return schedule;
                });
    }

    @Override
    public void writeAskpToStream(OutputStream outputStream, LocalDate date) throws IOException {
        //response.setContentType("application/json");
        Map parameterMap = new HashMap();
        parameterMap.put("begin_date", Date.valueOf(date));
        SqlParameterSource namedParameters;
        namedParameters = new MapSqlParameterSource(parameterMap);

        JsonFactory factory = new JsonFactory();
        JsonGenerator generator = factory.createGenerator(outputStream);
        generator.setCodec(mapper);
        namedParameterJdbcTemplate.query(sqlRequests.getAskpSql(), namedParameters,
                (ResultSet rs) -> {
                    ASKPdata askPdata = new ASKPdata();
                    try {
                        generator.writeStartArray();
                        while (rs.next()) {
                            askPdata.setRouteId(rs.getLong("routeId"));
                            askPdata.setTrId(rs.getLong("trId"));
                            askPdata.setRouteVariantId(rs.getLong("routeVariantId"));
                            askPdata.setMoveNum(rs.getLong("moveNum"));
                            askPdata.setOutOrderNum(rs.getLong("outOrderNum"));
                            askPdata.setStopOrderNum(rs.getLong("stopOrderNum"));
                            askPdata.setStopId(rs.getLong("stopId"));
                            askPdata.setCheckTime(rs.getTimestamp("checkTime").toLocalDateTime());
                            askPdata.setAskpCheckId(rs.getString("askpCheckId"));
                            askPdata.setTicketTypeId(rs.getLong("ticketTypeId"));
                            generator.writeObject(askPdata);
                        }
                        generator.writeEndArray();
                        generator.flush();
                    } catch (IOException e) {
                        log.log(Level.SEVERE, "IO error in streaming JSON", e);
                        return;
                    }
                }
        );
    }
}
