package ru.advantum.service.usw.network.tcp;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @since 8/18/16
 */
public class PureTcpClient extends Thread {
    EventLoopGroup workerGroup = new NioEventLoopGroup();
    Logger log;
    ChannelHandlerAdapter handler;
    ChannelFuture ftr;
    List<ITcpClient> listeners = new ArrayList<>();
    ConnInfo info;
    boolean worked;

    public PureTcpClient(ConnInfo info, ChannelHandlerAdapter handler, ITcpClient listener, Logger log) {
        super(info.toString());
        setInfo(info);
        setHandler(handler);
        addListener(listener);
        setLog(log);
    }

    public PureTcpClient fork() {
        start();
        return this;
    }

    @Override
    public void run() {
        setWorked(true);
        try {
            Bootstrap btstrp = new Bootstrap();
            btstrp.group(workerGroup);
            btstrp.channel(NioSocketChannel.class);
            btstrp.option(ChannelOption.SO_KEEPALIVE, true);
            btstrp.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, (int) getInfo().getTimeout());
            btstrp.handler(new ChannelInitializer<SocketChannel>() {
                @Override
                public void initChannel(SocketChannel ch) throws Exception {
                    ch.pipeline().addLast(handler);
                }
            });

            // Start the client.
            try {
                log.info("Listener started for {}", getInfo());
                ftr = btstrp.connect(getInfo().getAddress(), getInfo().getPort()).sync();
                fireEvent(NetworkEvent.CONNECTED.setConn(getInfo()));
                // Wait until the connection is closed.
                ftr.channel().closeFuture().sync();
            } catch (Throwable t) {
                log.info("{}", t.getMessage());
            }
            interrupt();
        } catch (Throwable t) {
            log.debug("{}", t);
        }
    }

    @Override
    public void interrupt() {
        try {
            ftr.channel().close();
            ftr.cancel(true);
        } catch (Exception e) {}
        try{
            setWorked(false);
            workerGroup.shutdownGracefully();
            fireEvent(NetworkEvent.THREAD_STOP.setConn(getInfo()));
        } catch (Exception e) {
        }
    }

    private PureTcpClient fireEvent(NetworkEvent e) {
        for (ITcpClient l : listeners) {
            l.eventHandled(e);
        }
        return this;
    }

    public ChannelHandlerAdapter getHandler() {
        return handler;
    }

    public PureTcpClient setHandler(ChannelHandlerAdapter handler) {
        this.handler = handler;
        return this;
    }

    public ConnInfo getInfo() {
        return info;
    }

    public PureTcpClient setInfo(ConnInfo info) {
        this.info = info;
        return this;
    }

    public boolean isWorked() {
        return worked;
    }

    public PureTcpClient setWorked(boolean worked) {
        this.worked = worked;
        return this;
    }

    public PureTcpClient setLog(Logger log) {
        this.log = log;
        return this;
    }

    public PureTcpClient removeListener(ITcpClient listener) {
        try {
            this.listeners.remove(listener);
        } catch (Exception e) {
        }
        return this;
    }

    public PureTcpClient addListener(ITcpClient listener) {
        this.listeners.add(listener);
        return this;
    }
}
