package ru.advantum.service.usw.network.tcp;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @since 8/26/16
 */
public class TelnetClient extends Thread {
    public static final String CRLF = "\r\n";
    Logger log;
    SimpleChannelInboundHandler<String> handler;
    ChannelFuture lastWriteFuture = null;
    boolean worked = false;
    ConnInfo info;
    List<ITcpClient> listeners = new ArrayList<>();
    EventLoopGroup group;
    Channel ch;

    public TelnetClient(ConnInfo info, SimpleChannelInboundHandler<String> handler, ITcpClient listener, Logger log) {
        this.info = info;
        this.handler = handler;
        this.log = log;
        addListener(listener);
    }

    public TelnetClient fork() {
        start();
        return this;
    }


    @Override
    public void run() {
        setWorked(true);
        try {
            group = new NioEventLoopGroup();
            Bootstrap b = new Bootstrap();
            b.group(group)
                    .channel(NioSocketChannel.class)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) {
                            ChannelPipeline pipeline = ch.pipeline();
                            // Add the text line codec combination first,
                            pipeline.addLast(new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()));
                            pipeline.addLast(new StringDecoder());
                            pipeline.addLast(new StringEncoder());
                            // and then business logic.
                            pipeline.addLast(handler);
                        }
                    });

            ch = b.connect(info.getAddress(), info.getPort()).sync().channel();

            lastWriteFuture = ch.writeAndFlush("PING" + CRLF);
            if (lastWriteFuture != null) {
                lastWriteFuture.sync();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
        }
    }

    @Override
    public void interrupt() {
        try {
            ch.close();
        } catch (Exception e) {}
        try{
            setWorked(false);
            group.shutdownGracefully();
            fireEvent(NetworkEvent.THREAD_STOP.setConn(getInfo()));
        } catch (Exception e) {
        }
    }

    public ConnInfo getInfo() {
        return info;
    }

    private TelnetClient fireEvent(NetworkEvent e) {
        for (ITcpClient l : listeners) {
            l.eventHandled(e);
        }
        return this;
    }


    public boolean isWorked() {
        return worked;
    }

    public TelnetClient setWorked(boolean worked) {
        this.worked = worked;
        return this;
    }

    public TelnetClient removeListener(ITcpClient listener) {
        try {
            this.listeners.remove(listener);
        } catch (Exception e) {
        }
        return this;
    }

    public TelnetClient addListener(ITcpClient listener) {
        this.listeners.add(listener);
        return this;
    }

}