package ru.advantum.service.usw.data;


import ru.advantum.service.usw.utils.Funcs;

/**
 *
 * @since 8/19/16
 */
public class RadioStation {
    private long idtr;
    //номер радиостанции
    private int radioNum;
    //тип радиостанции
    private int radioType = 207;
    private String phone;

    public RadioStation(long idtr, int radionum) {
        setIdtr(idtr);
        setRadioNum(radionum);
    }

    public RadioStation(long idtr, int radionum, int radiotype) {
        this(idtr, radionum);
        setRadioType(radiotype);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        } else if (o == this) {
            return Boolean.TRUE;
        } else if (o.getClass() != getClass()) {
            return Boolean.FALSE;
        } else if (o instanceof RadioStation) {
            RadioStation other = (RadioStation) o;
            if (other.getRadioNum() == this.getRadioNum()
                    && other.getRadioType() == this.getRadioType()
                    && other.getIdtr() == this.getIdtr()
                    )
                return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Override
    public String toString() {
        return String.format("RadioStation:{%s}", Funcs.uniToString(this));
    }


    public int getRadioNum() {
        return radioNum;
    }

    public RadioStation setRadioNum(int radioNum) {
        this.radioNum = radioNum;
        return this;
    }

    public int getRadioType() {
        return radioType;
    }

    public RadioStation setRadioType(int radioType) {
        this.radioType = radioType;
        return this;
    }

    public long getIdtr() {
        return idtr;
    }

    public RadioStation setIdtr(long idtr) {
        this.idtr = idtr;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public RadioStation setPhone(String phone) {
        this.phone = phone;
        return this;
    }
}
