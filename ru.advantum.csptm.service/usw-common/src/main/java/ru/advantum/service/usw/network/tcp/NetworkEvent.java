package ru.advantum.service.usw.network.tcp;

/**
 *
 * @since 8/19/16
 */
public class NetworkEvent {
    public static final int NOTHING_CODE = 0;
    public static final int THREAD_STOP_CODE = 1;
    public static final int CONNECTED_CODE = 2;

    public static NetworkEvent NOTHING = new NetworkEvent(NOTHING_CODE);
    public static NetworkEvent THREAD_STOP = new NetworkEvent(THREAD_STOP_CODE);
    public static NetworkEvent CONNECTED = new NetworkEvent(CONNECTED_CODE);

    private int code = NOTHING_CODE;
    private ConnInfo conn;

    public NetworkEvent(int code) {
        setCode(code);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        } else if (o == this) {
            return Boolean.TRUE;
        } else if (o.getClass() != getClass()) {
            return Boolean.FALSE;
        } else if (o instanceof NetworkEvent) {
            NetworkEvent other = (NetworkEvent) o;
            if (other.getCode() == this.getCode()
                    && other.getConn().equals(getConn())
                    )
                return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public ConnInfo getConn() {
        return conn;
    }

    public NetworkEvent setConn(ConnInfo conn) {
        this.conn = conn;
        return this;
    }

    public int getCode() {
        return code;
    }

    public NetworkEvent setCode(int code) {
        this.code = code;
        return this;
    }

}
