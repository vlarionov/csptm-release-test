package ru.advantum.service.usw.annotations;

import org.reflections.Reflections;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 * @since 13.04.16 13:38.
 */
@SuppressWarnings("unused")
public class AnnotationFactory {
    public static List<Class<?>> findMappers(String pAckage) {
        List<Class<?>> result = new ArrayList<>();
        Reflections r = new Reflections(pAckage);
        result.addAll(r.getTypesAnnotatedWith(Mapper.class));
        return result;
    }

    public static Class<?> findJSONRecordDescriptor(String pAckage, String recordId) {
        Reflections r = new Reflections(pAckage);
        for (Class<?> theClass : r.getTypesAnnotatedWith(JSONRecordDescriptor.class)) {
            for (Annotation annotation : theClass.getAnnotations()) {
                Class<? extends Annotation> type = annotation.annotationType();
                for (Method method : type.getDeclaredMethods()) {
                    try {
                        Object value = method.invoke(annotation, (Object[]) null);
                        String mv = (String) value;
                        if (method.getName().equalsIgnoreCase("name")
                                && mv.equalsIgnoreCase(recordId)) {
                            return theClass;
                        }
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return null;
    }

    public static LinkedHashMap<Field, Class<?>> getJSONFieldDescriptors(String pAckage, String recordId, List<String> names) {
        LinkedHashMap<Field, Class<?>> ret = new LinkedHashMap<>();
        Class<?> theClass = findJSONRecordDescriptor(pAckage, recordId);
        if (theClass != null) {
            for (String name : names) {
                boolean found = false;
                for (Field field : theClass.getDeclaredFields()) {
                    found = resolveFieldAnnotation(field, name, ret);
                    if (found) {
                        break;
                    }
                }
                if (!found) {
                    for (Field field : theClass.getSuperclass().getDeclaredFields()) {
                        found = resolveFieldAnnotation(field, name, ret);
                        if (found) {
                            break;
                        }
                    }
                }
            }
        }
        return ret;
    }

    public static Field getAnnotatedField(Class<?> theClass, String name) {
        for (Field field : theClass.getDeclaredFields()) {
            if (name.equalsIgnoreCase(field.getAnnotation(JSONFieldDescriptor.class).name())) {
                return field;
            }
        }
        for (Field field : theClass.getSuperclass().getDeclaredFields()) {
            if (name.equalsIgnoreCase(field.getAnnotation(JSONFieldDescriptor.class).name())) {
                return field;
            }
        }
        return null;
    }

    private static boolean resolveFieldAnnotation(Field field, String name, LinkedHashMap<Field, Class<?>> ret) {
        if (name.equalsIgnoreCase(field.getAnnotation(JSONFieldDescriptor.class).name())) {
            ret.put(field, field.getType());
            return true;
        }
        return false;
    }
}
