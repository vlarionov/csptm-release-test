package ru.advantum.service.usw.utils;

import org.apache.commons.lang.SerializationUtils;
import org.jboss.netty.buffer.ChannelBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Comparator;

/**
 *
 * @since 15.01.16 17:52.
 */
public class IntegerWrapper extends AbstractWrapper implements Comparator<IntegerWrapper>, Comparable<IntegerWrapper> {
    private static final Logger LOGGER = LoggerFactory.getLogger(IntegerWrapper.class);

    private byte[] bytes = new byte[0];
    private int size = 0;
    private BigInteger bi = new BigInteger(String.valueOf((byte) 0));

    public IntegerWrapper(int size) {
        setSize(size);
        this.bytes = new byte[size];
    }

    public IntegerWrapper(int size, byte[] bytes) {
        this(size);
        setBytes(bytes);
    }

    public IntegerWrapper(int size, byte[] bytes, int offset) {
        this(size);
        setBytes(bytes, offset);
    }

    public IntegerWrapper(int size, InputStream in) throws IOException {
        this(size);
        read(in);
    }

    public IntegerWrapper(int size, ChannelBuffer buffer) {
        this(size);
        read(buffer);
    }

    public IntegerWrapper(int size, int value) {
        this(size);
        setValue(value);
    }

    public IntegerWrapper(int size, long value) {
        this(size);
        setValue(value);
    }

    /**
     * Cloner
     *
     * @param other class
     */
    public IntegerWrapper(IntegerWrapper other) {
        this(other.getSize());
        setLittleEndian(other.isLittleEndian());
        setValue(other.intValue());
    }

    /**
     * Deserializer
     *
     * @param serialized class
     */
    public IntegerWrapper(byte[] serialized) {
        this((IntegerWrapper) SerializationUtils.deserialize(serialized));
    }

    @Override
    public int read(InputStream in) throws IOException {
        int ret = in.read(this.bytes);
        bi = new BigInteger(this.bytes);
        checkLE();
        return ret;
    }

    private void checkLE() {
        if (isLittleEndian()) {
            setBytes(flipBytes());
        }
    }

    private byte[] flipBytes() {
        byte[] doubles = new byte[getSize()];
        for (int i = 0; i < getSize(); i++) {
            doubles[getSize() - i - 1] = bytes[i];
        }
        return doubles;
    }

    private byte[] correctZeroleads(byte[] tmp) {
        int length = tmp.length;
        int srcPos = 0;
        int destPos = 0;
        if (tmp[0] == 0 && tmp.length > 1 && tmp[1] != 0) {
            srcPos = 1;
            length--;
        }
        byte[] array = new byte[length];
        System.arraycopy(tmp, srcPos, array, destPos, length);
        return array;
    }


    @Override
    public int read(ChannelBuffer buffer) {
        buffer.readBytes(getBytes());
        bi = new BigInteger(this.bytes);
        checkLE();
        return getSize();
    }

    public IntegerWrapper setValue(short value) {
        bytes = ByteBuffer.allocate(Short.BYTES).putShort(value).array();
        bi = new BigInteger(bytes);
        setSize(bytes.length);
        return this;
    }

    public IntegerWrapper setValue(int value) {
        bytes = ByteBuffer.allocate(Integer.BYTES).putInt(value).array();
        bi = new BigInteger(bytes);
        setSize(bytes.length);
        return this;
    }

    public IntegerWrapper setValue(long value) {
        bytes = ByteBuffer.allocate(Long.BYTES).putLong(value).array();
        bi = new BigInteger(bytes);
        setSize(bytes.length);
        return this;
    }

    @Override
    public short shortValue() {
        return bi.shortValue();
    }

    @Override
    public int intValue() {
        return bi.intValue();
    }

    @Override
    public int uIntValue() {
        return (int) Integer.toUnsignedLong(bi.intValue());
    }

    @Override
    public byte byteValue() {
        return bi.byteValue();
    }

    @Override
    public boolean booleanValue() {
        return bi.intValue() == 1;
    }

    @Override
    public byte uByteValue() {
        return (byte) (bi.byteValue() & (byte) 0xFF);
    }

    @Override
    public double doubleValue() {
        return intValue() / 1.0;
    }

    @Override
    public long longValue() {
        return bi.longValue();
    }

    public int getSize() {
        return size;
    }

    public IntegerWrapper setSize(int size) {
        this.size = size;
        return this;
    }

    @Override
    public byte[] getBytes() {
        return bytes;
    }

    public IntegerWrapper setBytes(byte[] bytes) {
        System.arraycopy(bytes, 0, this.bytes, 0, getSize());
        bi = new BigInteger(this.bytes);
        return this;
    }

    @Override
    public byte[] getLEBytes() {
        return flipBytes();
    }

    public IntegerWrapper setBytes(byte[] bytes, int offset) {
        System.arraycopy(bytes, offset, this.bytes, 0, getSize());
        bi = new BigInteger(this.bytes);
        checkLE();
        return this;
    }

    public byte[] toByteArray() {
        return SerializationUtils.serialize(this);
    }

    @Override
    public String toString() {
        return "{value=" + intValue() + ";size=" + getSize() + "}";
    }

    @Override
    public int hashCode() {
        return (intValue() ^ 31) * (getSize() + 1);
    }

    @Override
    public int compare(IntegerWrapper o1, IntegerWrapper o2) {
        return o1.intValue() - o2.intValue();
    }

    @Override
    public boolean equals(Object other) {
        return (other instanceof IntegerWrapper
                && intValue() == ((IntegerWrapper) other).intValue()
                && getSize() == ((IntegerWrapper) other).getSize());
    }

    @Override
    public int compareTo(IntegerWrapper o) {
        return intValue() - o.intValue();
    }
}
