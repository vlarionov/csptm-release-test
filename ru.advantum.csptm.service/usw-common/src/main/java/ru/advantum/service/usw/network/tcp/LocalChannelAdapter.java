package ru.advantum.service.usw.network.tcp;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.buffer.DynamicChannelBuffer;

/**
 *
 * @since 8/19/16
 */
public abstract class LocalChannelAdapter extends ChannelHandlerAdapter {
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf m = (ByteBuf) msg;
        int length = m.writerIndex() - m.readerIndex();
        byte[] bytes = new byte[length];
        try {
            m.readBytes(bytes);
            DynamicChannelBuffer dchb = (DynamicChannelBuffer) ChannelBuffers.dynamicBuffer(length);
            dchb.writeBytes(bytes);
            messageReceived(dchb, ctx);
        } finally {
            m.release();
        }
    }

    public abstract void messageReceived(ChannelBuffer dchb, ChannelHandlerContext ctx) throws Exception;
}
