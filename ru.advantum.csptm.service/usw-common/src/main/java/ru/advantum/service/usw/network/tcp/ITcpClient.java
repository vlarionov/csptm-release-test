package ru.advantum.service.usw.network.tcp;

/**
 *
 * @since 8/19/16
 */
public interface ITcpClient {
    void eventHandled(NetworkEvent event);
}
