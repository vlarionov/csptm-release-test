package ru.advantum.service.usw.utils;

import org.jboss.netty.buffer.ChannelBuffer;

import java.io.ByteArrayInputStream;
import java.lang.reflect.Field;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.CharBuffer;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Mitsay
 */
public class Funcs {
    public static String getAddressPort(InetSocketAddress packet) {
        return packet.getHostString() + ":" + packet.getPort();
    }

    public static double getDelphiTDateTimeNow() {
        return getDelphiTDateTimeMili(System.currentTimeMillis());
    }

    public static double getDelphiTDateTime(long secs) {
        return (secs * 1000) / 86400000.0 + 25569.0;
    }

    public static byte[] encodeDouble2Bytes(double input, int size, int precision) {
        int l = size + precision + 1;
        String str = String.format("%0" + size + "." + precision + "f", input);
        return str.substring(0, str.length() < l ? str.length() : l).getBytes();
    }

    public static double getDelphiTDateTimeMili(long milisecs) {
        return milisecs / 86400000.0 + 25569.0;
    }

    public static boolean assigned(ChannelBuffer b, int min) {
        try {
            return b.readable() && b.array().length > min;
        } catch (Exception e) {
        }
        return false;
    }

    public static boolean checkIsHostAssigned(String address, int port) {
        boolean ret = Boolean.FALSE;
        try {
            Socket s = new Socket();
            s.connect(new InetSocketAddress(address, port), 1000);
            s.close();
            ret = Boolean.TRUE;
        } catch (Exception e) {
        }
        return ret;
    }

    public static void sleep(int sec) {
        Funcs.sleep((long) sec * 1000);
    }

    public static void sleep(long milisec) {
        try {
            Thread.sleep(milisec);
        } catch (InterruptedException e) {
            ;
        }
    }

    public static List<String> readToStringArray(ChannelBuffer b) {
        List<String> res = new LinkedList<>();
        CharBuffer st = b.toByteBuffer().asCharBuffer();
        int lastPtr = 0;
        CharBuffer nxt = null;
        for (int ptr = 0; ptr < st.length() - 1; ptr++) {
            if (st.charAt(ptr) == '\r' && st.charAt(ptr + 1) == '\n') { //win
                nxt = st.subSequence(lastPtr, ptr - 1);
                lastPtr = (++ptr) + 1;
            } else if (st.charAt(ptr) == '\n') { //*nix
                nxt = st.subSequence(lastPtr, ptr - 1);
                lastPtr = ptr + 1;
            } else if (st.charAt(ptr) == '\r') { //mac
                nxt = st.subSequence(lastPtr, ptr - 1);
                lastPtr = ptr + 1;
            }
            if (nxt != null) {
                res.add(new String(nxt.array()).trim());
                nxt = null;
            }
        }
        return res;
    }

    public static List<String> readToStringArray(String strings) {
        List<String> res = new LinkedList<>();
        StringTokenizer tok = new StringTokenizer(strings, "\r\n");
        while (tok.hasMoreTokens()) {
            res.add(tok.nextToken());
        }
        return res;
    }

    public static String uniToString(Object o) {
        StringBuilder sb = new StringBuilder();
        for (Field f : o.getClass().getDeclaredFields()) {
            try {
                f.setAccessible(true);
                if (f.getGenericType().getTypeName().equals("int")) {
                    sb.append(f.getName()).append(":").append((Integer) f.get(o)).append(";");
                } else if (f.getGenericType().getTypeName().equals("boolean")) {
                    sb.append(f.getName()).append(":").append((Boolean) f.get(o)).append(";");
                } else if (f.getGenericType().getTypeName().equals("long")) {
                    sb.append(f.getName()).append(":").append((Long) f.get(o)).append(";");
                } else if (f.getGenericType().getTypeName().equals("java.lang.String")) {
                    sb.append(f.getName()).append(":").append((String) f.get(o)).append(";");
                } else if (f.getGenericType().getTypeName().equals("java.util.UUID")) {
                    sb.append(f.getName()).append(":").append(((UUID) f.get(o)).toString()).append(";");
                } else {
                    sb.append(f.getName()).append(":\"").append(f.get(o).toString()).append("\";");
                }
            } catch (IllegalAccessException | NullPointerException e) {
                //System.out.println("f:" + f.getName() + " type:" + f.getGenericType().getTypeName());
            }
        }
        return sb.toString();
    }

    public static String formattedUtcDateTime(long miliseconds) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        Calendar c = Calendar.getInstance(tz);
        c.setTimeInMillis(miliseconds);
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss z");
        sdf.setTimeZone(tz);
        return sdf.format(c.getTime());
    }

    public static ByteArrayInputStream fromChannelToStream(ChannelBuffer in) {
        byte[] bytes = in.toByteBuffer(in.readerIndex(), in.writerIndex() - in.readerIndex()).array();
        return new ByteArrayInputStream(
                bytes,
                0,
                bytes.length
        );
    }

    public static byte[] stringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];

        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }

        return data;
    }

    public static byte[] asciiStringToBytes(String s) {
        byte[] data = new byte[s.length()];
        int i = 0;
        char[] var3 = s.toCharArray();
        int var4 = var3.length;

        for (int var5 = 0; var5 < var4; ++var5) {
            char c = var3[var5];
            data[i++] = (byte) c;
        }

        return data;
    }

    public static String bytesToHexString(final byte[] bytes) {
        final StringBuilder buf = new StringBuilder();
        try {
            for (final byte b : bytes) {
                buf.append(String.format("%02X", b & 0xFF));
            }
        } catch (Throwable t) {
            ;
        }
        return buf.toString();
    }

    public static String bytesToHexZtString(final byte[] bytes) {
        final StringBuilder buf = new StringBuilder();
        try {
            for (final byte b : bytes) {
                if (b == 0) {
                    break;
                }
                buf.append(String.format("%02X", b & 0xFF));
            }
        } catch (Throwable t) {
            ;
        }
        return buf.toString();
    }
}
