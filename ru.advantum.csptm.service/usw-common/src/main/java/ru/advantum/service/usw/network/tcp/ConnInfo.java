package ru.advantum.service.usw.network.tcp;


import ru.advantum.service.usw.utils.Funcs;

/**
 *
 * @since 8/18/16
 */
public class ConnInfo {
    String address;
    int port;
    long timeout = 60000;

    public ConnInfo(String address, int port) {
        setAddress(address);
        setPort(port);
    }
    public ConnInfo(String address, int port, long timeout) {
        this(address, port);
        setTimeout(timeout);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        } else if (o == this) {
            return Boolean.TRUE;
        } else if (o.getClass() != getClass()) {
            return Boolean.FALSE;
        } else if (o instanceof ConnInfo) {
            ConnInfo other = (ConnInfo) o;
            if (other.getAddress().equals(getAddress())
                    && other.getPort() == getPort()
                    && other.getTimeout() == getTimeout())
                return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }



    @Override
    public String toString() {
        return "ConnInfo:{" + Funcs.uniToString(this) + "}";
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }
}
