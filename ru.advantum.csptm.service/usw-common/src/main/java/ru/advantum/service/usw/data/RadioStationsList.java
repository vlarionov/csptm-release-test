package ru.advantum.service.usw.data;



import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @since 8/19/16
 */
public class RadioStationsList {
    private List<RadioStation> list = Collections.synchronizedList(new ArrayList<RadioStation>());
    private long lastReload = 0;

    public RadioStationsList() {
        setLastReload(System.currentTimeMillis());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("RadioStationsList:{\n");
        for (RadioStation ui : getList()) {
            sb.append(ui.toString()).append("\n");
        }
        sb.append("}");
        return sb.toString();
    }


    public RadioStationsList add(RadioStation item) {
        getList().add(item);
        return this;
    }

    public RadioStationsList remove(RadioStation item) {
        Iterator<RadioStation> it = getList().iterator();
        while (it.hasNext()) {
            RadioStation nxt = it.next();
            if (nxt.equals(item)) {
                getList().remove(nxt);
                break;
            }
        }
        return this;
    }

    public RadioStation findByNum(int item) {
        Iterator<RadioStation> it = getList().iterator();
        while (it.hasNext()) {
            RadioStation nxt = it.next();
            if (nxt.getRadioNum() == item) {
                return nxt;
            }
        }
        return null;
    }

    public RadioStation findByNumbers(int rnum, int rtype) {
        Iterator<RadioStation> it = getList().iterator();
        while (it.hasNext()) {
            RadioStation nxt = it.next();
            if (nxt.getRadioNum() == rnum && nxt.getRadioType() == rtype) {
                return nxt;
            }
        }
        return null;
    }

    public RadioStation get(int index) {
        return getList().get(index);
    }

    public List<RadioStation> getList() {
        return list;
    }

    public long getLastReload() {
        return lastReload;
    }

    public RadioStationsList setLastReload(long lastReload) {
        this.lastReload = lastReload;
        return this;
    }
}
