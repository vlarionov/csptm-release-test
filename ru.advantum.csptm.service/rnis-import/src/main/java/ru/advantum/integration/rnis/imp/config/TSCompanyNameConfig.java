package ru.advantum.integration.rnis.imp.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * @autor abc
 * @modified abc on 13.12.2016.
 */
@Root(name = "ts-company")
public class TSCompanyNameConfig {

    @Attribute
    public String name;
}
