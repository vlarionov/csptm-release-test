package ru.advantum.integration.rnis.imp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.integration.rnis.imp.config.RnisImportConfig;
import ru.advantum.integration.rnis.imp.config.TSCompanyNameConfig;
import ru.advantum.integration.rnis.imp.jsonImport.LoadRadioJamColor;
import ru.advantum.integration.rnis.imp.jsonImport.LoadTS;

import javax.sql.DataSource;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by alexander on 11.08.2015.
 * Modified by abc 02.11.2016
 */
public class StartApp {
    private static RnisImportConfig config;
    private static final Logger LOG = LoggerFactory.getLogger(StartApp.class);
    private final ScheduledExecutorService loadDataTask1 = Executors.newSingleThreadScheduledExecutor();
    private final ScheduledExecutorService loadDataTask2 = Executors.newSingleThreadScheduledExecutor();
    private final AtomicReference<LocalDateTime> nextRadioJamColor = new AtomicReference<>(LocalDateTime.now());
    private final AtomicReference<LocalDateTime> nextTS = new AtomicReference<>(LocalDateTime.now());
    private final long CHECK_PERIOD = 120;  // в секундах
    private final DataSource datasource;

    private StartApp(RnisImportConfig config) throws Exception {
        StartApp.config = config;
        nextRadioJamColor.set(LocalDateTime.now().plusSeconds(config.tsCompany.changeCheckMilliSeconds / 10000));
        this.datasource = config.database.getConnectionPool();
    }

    /**
     * Загрузка Транапортной ситуации
     */
    private void loadData_RadioJamColor() {

        try {
            LocalDateTime nextTime = nextRadioJamColor.get();
            if (LocalDateTime.now().isAfter(nextTime)) {

                nextRadioJamColor.set(LocalDateTime.now().plusSeconds(config.radioJamColor.changeCheckMilliSeconds / 1000));

                LOG.info("Start import radio jam color");
                (new LoadRadioJamColor(config, this.datasource)).load();
                LOG.info("Finish import radio jam color");
            } else {
                long minutes = ChronoUnit.SECONDS.between(LocalDateTime.now(), nextTime);
                LOG.info("Waiting for import radio jam color " + minutes + " s");
            }
        } catch (Throwable ex) {
            LOG.error("RadioJamColor", ex);
        }
    }

    /**
     * Загрузка ТС
     */
    private void loadData_TS() {
        try {
            LocalDateTime nextTime = nextTS.get();
            if (LocalDateTime.now().isAfter(nextTime)) {

                nextTS.set(LocalDateTime.now().plusSeconds(config.tsCompany.changeCheckMilliSeconds / 1000));

                LOG.info("Start import commercial TS");
                LoadTS lts = new LoadTS(config, this.datasource);
                for (TSCompanyNameConfig nc : config.tsCompany.tsCompanyNameList) {
                    LOG.info("  Start import company " + nc.name);
                    lts.load(nc.name);
                }
                LOG.info("Finish import commercial TS");
            } else {
                long minutes = ChronoUnit.SECONDS.between(LocalDateTime.now(), nextTime);
                LOG.info("Waiting for import commercial TS " + minutes + " s");
            }
        } catch (Throwable ex) {
            LOG.error("TS", ex);
        }
    }

    private void start() {
        loadDataTask1.scheduleWithFixedDelay(this::loadData_RadioJamColor, 0, CHECK_PERIOD, TimeUnit.SECONDS);
        loadDataTask2.scheduleWithFixedDelay(this::loadData_TS, CHECK_PERIOD / 2, CHECK_PERIOD, TimeUnit.SECONDS);
    }

    public static void main(String[] args) throws Exception {
        final RnisImportConfig config = Configuration.unpackConfig(RnisImportConfig.class, "rnis-import.xml");
        (new StartApp(config)).start();
    }
}
