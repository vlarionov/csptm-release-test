package ru.advantum.integration.rnis.imp.jsonImport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.integration.rnis.imp.config.RnisImportConfig;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

class RadioJamColorData implements DataItem {

    String ts;
    LocalDateTime ts_date;

    Integer ln;
    Integer fn;
    Integer tc;

    @Override
    public void clear() {
        ts = null;
        ts_date = null;
        ln = null;
        fn = null;
        tc = null;
    }

    @Override
    public boolean canLoad() {
        return ts_date != null &&
            ln != null &&
            fn != null &&
            tc != null;
    }

    @Override
    public String[][] getDataList() {
        return null;
    }

    @Override
    public List<Integer> getFieldsMap() {
        return null;
    }
}

/**
 * Загрузка Транспортной ситуации
 * @autor abc
 * @modified abc on 12.12.2016.
 */
public class LoadRadioJamColor extends BaseLoadJson<RadioJamColorData> {

    private final Logger log = LoggerFactory.getLogger(LoadRadioJamColor.class);
    private final DataSource datasource;
    private final String statement;
    private final int batchSize;
    private final String stringFormat29 = "yyyy-MM-dd HH:mm:ss.SSSSSSx";
    private final DateTimeFormatter dateFormat29 = DateTimeFormatter.ofPattern(stringFormat29);
    private final String host;
    private final String clearStatement;

    public LoadRadioJamColor(RnisImportConfig config, DataSource datasource) {
        this.datasource = datasource;
        statement = "{ call " + config.dbSchemaName + ".rnis_import_pkg$import_radio_jam_color (?::timestamp, ?::bigint, ?::bigint, ?::bigint) }";
        clearStatement = "{ call " + config.dbSchemaName + ".rnis_import_pkg$clear_radio_jam_color () }";
        batchSize = config.batchSize;
        host = config.radioJamColor.host;
    }

//    private String getTestData() {
//        String json = "";
//        try {
//            String fileName = "F:\\Project\\Advantum\\vsdev.integration.rnis.big\\data_in\\radio-jam-color.json";
//            json = new String(Files.readAllBytes(Paths.get(fileName)));
//        } catch (IOException e) {
//            System.out.println(e.getMessage());
//        }
//        return json;
//    }

    private String getData() {
        String result = "";
        if (host != null && !host.isEmpty()) {
            WebHelper w = new WebHelper();
            result = w.exportRadioJamColor(host);
        }
        //result = getTestData();
        return result;
    }

    @Override
    protected void loadItem(CallableStatement call, RadioJamColorData item, final int rowNum) throws SQLException {

        call.setTimestamp(1, Timestamp.valueOf(item.ts_date));
        call.setInt(2, item.ln);
        call.setInt(3, item.fn);
        call.setInt(4, item.tc);

        call.addBatch();
        if (rowNum % batchSize == 0)
            call.executeBatch();
    }

    public int load() {

        // Получаем данные по HTTP
        String json = getData();
        if (json == null || json.isEmpty()) {
            log.error("Can't receive data");
            return 0;
        }

        // Загрузка данных
        DataItemMap<RadioJamColorData> mapper = (d) ->
        {
            if (d.ts != null) {
                d.ts_date = ZonedDateTime.parse(d.ts, dateFormat29).withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime();
            }
        };

        try (Connection conn = datasource.getConnection()) {

            try (CallableStatement call = conn.prepareCall(clearStatement)) {
                call.execute();
                //call.getConnection().commit();
            }

            try (CallableStatement call = conn.prepareCall(statement)) {
                loadItems(call, json, mapper, RadioJamColorData[].class);
            }

        } catch (SQLException e) {
            log.error("RadioJamColorData.load", e);
        }

        return 0;
    }
}
