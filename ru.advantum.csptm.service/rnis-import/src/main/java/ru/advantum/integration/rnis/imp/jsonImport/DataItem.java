package ru.advantum.integration.rnis.imp.jsonImport;

import java.util.List;

/**
 * @autor abc
 * @modified abc on 09.12.2016.
 */
public interface DataItem {

    void clear();

    boolean canLoad();

    String[][] getDataList();

    List<Integer> getFieldsMap();
}
