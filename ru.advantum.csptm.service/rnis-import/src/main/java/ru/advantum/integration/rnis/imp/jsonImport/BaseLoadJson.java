package ru.advantum.integration.rnis.imp.jsonImport;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * @autor abc
 * @modified abc on 09.12.2016.
 */
public abstract class BaseLoadJson<T extends DataItem> {

    private final Logger log = LoggerFactory.getLogger(BaseLoadJson.class);

    protected abstract void loadItem(CallableStatement call, T item, final int rowNum) throws SQLException;

    /**
     * Загрузка json в формате массива объектов
     *
     * @param call
     * @param json
     * @param m
     * @param type
     * @return
     */
    protected int loadItems(CallableStatement call, String json, DataItemMap<T> m, Class<T[]> type) {
        int rowLoaded = 0;
        int errorCount = 0;

        Gson gson = new Gson();
        T[] d = gson.fromJson(json, type);
        for (int i = 0; i < d.length; i++) {

            T data = d[i];
            m.map(data);

            if (!data.canLoad()) {
                log.error("Required data can't be empty, see row " + rowLoaded);
                errorCount++;
            }

            try {
                loadItem(call, data, i + 1);
                rowLoaded++;
            } catch (SQLException ex) {
                log.error("Row " + rowLoaded + " - " + ex.getMessage());
                errorCount++;
            }
        }

        try {
            // insert remaining records
            call.executeBatch();
            call.getConnection().commit();
        } catch (SQLException e) {
            log.error(e.getMessage());
            errorCount++;
        }

        if (errorCount == 0) {
            log.info("Import successfully finished");
        } else {
            log.info("Import finished, found " + errorCount + " errors");
        }

        return rowLoaded;
    }

    /**
     * Загрузка json в формате массива массивов значений
     *
     * @param call
     * @param json
     * @param m
     * @param type
     * @return
     */
    protected int loadValuesItems(CallableStatement call, String json, DataItemValuesMap<T> m, Class<T[]> type) {
        int rowLoaded = 0;
        int errorCount = 0;

        Gson gson = new Gson();
        T[] dataList = gson.fromJson(json, type);
        if (dataList.length <= 0) return 0;
        T data = dataList[0];
        String[][] d = data.getDataList();
        List<Integer> f = data.getFieldsMap();
        for (int i = 0; i < d.length; i++) {

            data.clear();
            m.map(data, d[i], f);

            if (!data.canLoad()) {
                log.error("Required data can't be empty, see row " + rowLoaded);
                errorCount++;
            }

            try {
                loadItem(call, data, i + 1);
                rowLoaded++;
            } catch (SQLException ex) {
                log.error("Row " + rowLoaded + " - " + ex.getMessage());
                errorCount++;
            }
        }

        try {
            // insert remaining records
            call.executeBatch();
            call.getConnection().commit();
        } catch (SQLException e) {
            log.error(e.getMessage());
            errorCount++;
        }

        if (errorCount == 0) {
            log.info("Import successfully finished");
        } else {
            log.info("Import finished, found " + errorCount + " errors");
        }

        return rowLoaded;
    }
}
