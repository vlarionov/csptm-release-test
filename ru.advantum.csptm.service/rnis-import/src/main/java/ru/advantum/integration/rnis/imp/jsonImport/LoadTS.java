package ru.advantum.integration.rnis.imp.jsonImport;

import com.google.gson.annotations.SerializedName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.integration.rnis.imp.config.RnisImportConfig;
import ru.advantum.integration.rnis.imp.utils.DbHelper;
import ru.advantum.integration.rnis.imp.utils.TypeConvert;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

class TSData implements DataItem {

    String type;
    @SerializedName("fields")
    String fieldsList[];
    @SerializedName("values")
    String dataValues[][];

    Long packageNum;

    /**
     * Марка ТС
     */
    String marka;
    /**
     * Категория ТС
     */
    String category;
    /**
     * Модель ТС
     */
    String model;
    /**
     * Автопредприятие
     */
    String carrier;
    /**
     * Гаражный номер
     */
    String garageNomer;
    /**
     * ID в базе данных РНИС
     */
    Long objectId;
    /**
     * Имя ТС
     */
    String name;
    /**
     * Время последнего изменения
     */
    LocalDateTime changeTime;
    /**
     * ID ТС
     */
    String ID;
    /**
     * Идентификатор АТТ в РНИС
     */
    String att;
    /**
     * Метка в РНИС ТП
     */
    String label;
    /**
     * Уникальный ID в РНИС
     */
    Long tmaticID;
    /**
     * ГРЗ
     */
    String gosNomer;

    @Override
    public void clear() {
        marka = null;
        category = null;
        model = null;
        carrier = null;
        garageNomer = null;
        objectId = null;
        name = null;
        changeTime = null;
        ID = null;
        att = null;
        label = null;
        tmaticID = null;
        gosNomer = null;
    }

    @Override
    public boolean canLoad() {
        return true;
    }

    @Override
    public String[][] getDataList() {
        return dataValues;
    }

    private int findIndex(String name) {
        for (int i = 0; i < fieldsList.length; i++) {
            if (name.equals(fieldsList[i])) return i;
        }
        return -1;
    }

    public List<Integer> getFieldsMap() {
        List<Integer> result = new ArrayList<>();
        result.add(findIndex("marka"));
        result.add(findIndex("category"));
        result.add(findIndex("model"));
        result.add(findIndex("avtoPredpr"));
        int i = result.get(3);
        if (i < 0) result.set(3, findIndex("carrier"));
        result.add(findIndex("garageNomer"));
        result.add(findIndex("objectId"));
        result.add(findIndex("name"));
        result.add(findIndex("changeTime"));
        result.add(findIndex("ID"));
        result.add(findIndex("att"));
        result.add(findIndex("label"));
        result.add(findIndex("tmaticID"));
        result.add(findIndex("gosNomer"));
        return result;
    }
}

/**
 * Загрузка Справочника транспортных средств
 * @autor abc
 * @modified abc on 13.12.2016.
 */
public class LoadTS extends BaseLoadJson<TSData> {

    private final Logger log = LoggerFactory.getLogger(LoadTS.class);
    private final DataSource datasource;
    private final int batchSize;
    private final String host;
    private final String user;
    private final String password;
    private final String dbSchemaName;
    private final String statement;

    public LoadTS(RnisImportConfig config, DataSource datasource) {
        this.datasource = datasource;
        this.batchSize = config.batchSize;
        this.host = config.tsCompany.host;
        this.user = config.tsCompany.user;
        this.password = config.tsCompany.password;

        this.dbSchemaName = config.dbSchemaName;
        this.statement = "{ call " + dbSchemaName + ".rnis_import_pkg$import_com_vehicles( " +
            " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?::timestamp," +
            " ?, ?, ?, ?, ?) }";
    }

    @Override
    protected void loadItem(CallableStatement call, TSData item, int rowNum) throws SQLException {

        DbHelper.SetParamLong(call, 1, item.packageNum);         // v_package_num
        DbHelper.SetParameString(call, 2, item.type);            // v_depot_code
        DbHelper.SetParameString(call, 3, item.marka);           // v_marka
        DbHelper.SetParameString(call, 4, item.category);        // v_category
        DbHelper.SetParameString(call, 5, item.model);           // v_model
        DbHelper.SetParameString(call, 6, item.carrier);         // v_carrier
        DbHelper.SetParameString(call, 7, item.garageNomer);     // v_garage_nomer
        DbHelper.SetParamLong(call, 8, item.objectId);           // v_object_id
        DbHelper.SetParameString(call, 9, item.name);            // v_name
        DbHelper.SetParameTimestamp(call, 10, item.changeTime);  // v_change_time
        DbHelper.SetParameString(call, 11, item.ID);             // v_id
        DbHelper.SetParameString(call, 12, item.att);            // v_att
        DbHelper.SetParameString(call, 13, item.label);          // v_label
        DbHelper.SetParamLong(call, 14, item.tmaticID);          // v_tmatic_id
        DbHelper.SetParameString(call, 15, item.gosNomer);       // v_gos_nomer

        call.addBatch();
        if (rowNum % batchSize == 0)
            call.executeBatch();
    }

    private long getPackageNum(Connection conn) throws SQLException {
        final String getPackageStatementText = "{ ? = call " + dbSchemaName + ".rnis_import_pkg$get_package_num() }";
        try (CallableStatement call = conn.prepareCall(getPackageStatementText)) {
            call.registerOutParameter(1, Types.BIGINT);
            call.execute();
            return call.getLong(1);
        }
    }

    private void clearPackage(Connection conn, long packageNum) throws SQLException {
        final String getPackageStatementText = "{ ? = call " + dbSchemaName + ".rnis_import_pkg$clear_package( ? ) }";
        try (CallableStatement call = conn.prepareCall(getPackageStatementText)) {
            call.registerOutParameter(1, Types.INTEGER);
            DbHelper.SetParamLong(call, 2, packageNum);
            call.execute();
        }
    }

    private void checkDeleted(Connection conn, long packageNum) throws SQLException {
        final String getPackageStatementText = "{ ? = call " + dbSchemaName + ".rnis_import_pkg$check_deleted( ? ) }";
        try (CallableStatement call = conn.prepareCall(getPackageStatementText)) {
            call.registerOutParameter(1, Types.INTEGER);
            DbHelper.SetParamLong(call, 2, packageNum);
            call.execute();
        }
    }

//    private String getTestData() {
//        String json = "";
//        try {
//            //String fileName = "F:\\Project\\Advantum\\vsdev.integration.rnis.big\\data_in\\TSampp.json";
//            //String fileName = "F:\\Project\\Advantum\\vsdev.integration.rnis.big\\data_in\\TSATTdjkh.json";
//            String fileName = "F:\\Project\\Advantum\\vsdev.integration.rnis.big\\data_in\\TScomp2.json";
//            json = new String(Files.readAllBytes(Paths.get(fileName)));
//        } catch (IOException e) {
//            System.out.println(e.getMessage());
//        }
//        return json;
//    }

    private String getData(String tsCompanyName) {
        String result = "";
        if (host != null && !host.isEmpty()) {
            WebHelper w = new WebHelper();
            result = w.exportTS(host, user, password, tsCompanyName);
        }
        //result = getTestData();
        return result;
    }

    public int load(String tsCompanyName) {

        // Получаем данные по HTTP
        String json = getData(tsCompanyName);
        if (json == null || json.isEmpty()) {
            log.error("Can't receive data");
            return 0;
        }

        try (Connection conn = datasource.getConnection()) {
            boolean saveAutoCommit = conn.getAutoCommit();
            conn.setAutoCommit(false);

            try (CallableStatement call = conn.prepareCall(statement)) {

                final long packageNum = getPackageNum(conn);

                DataItemValuesMap<TSData> mapper = (d, r, f) ->
                {
                    d.packageNum = packageNum;

                    int i;
                    i = f.get(0);
                    if (i >= 0) d.marka = r[i];
                    i = f.get(1);
                    if (i >= 0) d.category = r[i];
                    i = f.get(2);
                    if (i >= 0) d.model = r[i];
                    i = f.get(3);
                    if (i >= 0) d.carrier = r[i];
                    i = f.get(4);
                    if (i >= 0) d.garageNomer = r[i];
                    i = f.get(5);
                    if (i >= 0) d.objectId = TypeConvert.toLong(r[i]);
                    i = f.get(6);
                    if (i >= 0) d.name = r[i];
                    i = f.get(7);
                    if (i >= 0) {
                        if (r[i].length() == 13) {
                            long li = Long.valueOf(r[i]);
                            Instant in = Instant.ofEpochMilli(li);
                            d.changeTime = in.atZone(ZoneId.of("UTC")).toLocalDateTime();
                        }
                    }
                    i = f.get(8);
                    if (i >= 0) d.ID = r[i];
                    i = f.get(9);
                    if (i >= 0) d.att = r[i];
                    i = f.get(10);
                    if (i >= 0) d.label = r[i];
                    i = f.get(11);
                    if (i >= 0) d.tmaticID = TypeConvert.toLong(r[i]);
                    i = f.get(12);
                    if (i >= 0) d.gosNomer = r[i];
                };

                // Загрузка данных
                loadValuesItems(call, json, mapper, TSData[].class);
                // Поиск удаленных
                checkDeleted(conn, packageNum);
                // Очистка проверки удаленных
                clearPackage(conn, packageNum);

                conn.commit();
            } catch (SQLException e) {
                log.error("LoadTS.load", e);
                conn.rollback();
            } finally {
                conn.setAutoCommit(saveAutoCommit);
            }
        } catch (SQLException e) {
            log.error("LoadTS.load", e);
        }
        return 0;
    }
}
