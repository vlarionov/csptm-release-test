package ru.advantum.integration.rnis.imp.jsonImport;

/**
 * @autor abc
 * @modified abc on 09.12.2016.
 */
@FunctionalInterface
public interface DataItemMap<T> {
    void map(T data);
}
