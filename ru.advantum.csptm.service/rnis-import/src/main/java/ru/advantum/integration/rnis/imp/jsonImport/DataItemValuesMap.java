package ru.advantum.integration.rnis.imp.jsonImport;

import java.util.List;

/**
 * @autor abc
 * @modified abc on 14.12.2016.
 */
@FunctionalInterface
public interface DataItemValuesMap<T> {
    void map(T data, String[] row, List<Integer> fields);
}
