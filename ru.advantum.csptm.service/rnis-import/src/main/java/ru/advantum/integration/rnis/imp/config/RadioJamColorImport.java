package ru.advantum.integration.rnis.imp.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @autor abc
 * @modified abc on 13.12.2016.
 */
@Root(name = "radio-jam-color-import")
public class RadioJamColorImport {

    @Attribute(name = "change-check-period", required = false)
    public long changeCheckMilliSeconds = 1 * 60 * 60 * 1000;

    @Element(name = "host", required = true)
    public String host;
}
