package ru.advantum.integration.rnis.imp.utils;

import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * @autor abc
 * @modified abc on 17.11.2015.
 */
public class TypeConvert {

    // === _to Date

    public static Timestamp toTimestamp(LocalDateTime date) {
        if (date == null) return null;
        return Timestamp.valueOf(date);
    }

    // === _to Number

    public static Integer toInteger(String s) {
        Integer r = null;
        if (s != null && !s.trim().isEmpty() && !s.trim().toLowerCase().equals("null")) r = Integer.valueOf(s);
        return r;
    }

    public static Integer toInteger(Object o) {
        Integer r = null;
        if (o != null) r = ((Number) o).intValue();
        return r;
    }

    public static Long toLong(String s) {
        Long r = null;
        if (s != null && !s.trim().isEmpty() && !s.trim().toLowerCase().equals("null")) r = Long.valueOf(s);
        return r;
    }

    public static Long toLong(Object o) {
        Long r = null;
        if (o != null) r = ((Number) o).longValue();
        return r;
    }
}
