package ru.advantum.integration.rnis.imp.jsonImport;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

/**
 * @autor abc
 * @modified abc on 13.12.2016.
 */
public class WebHelper {
    private final Logger log = LoggerFactory.getLogger(WebHelper.class);
    private HttpClientContext localContext = null;

    /**
     * Экспорт Транспортной ситуации
     *
     * @return
     */
    public String exportRadioJamColor(String host) {
        String result = "";

        URI uri;
        try {
            uri = new URI(host);
        } catch (URISyntaxException e) {
            log.error(e.getMessage());
            return result;
        }

        HttpGet httpGet = new HttpGet(uri);

        try (CloseableHttpClient client = HttpClients.custom().build()) {
            try (CloseableHttpResponse response = client.execute(httpGet)) {
                int status = response.getStatusLine().getStatusCode();
                if (status != 200) return "";

                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity, "UTF-8");
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return result;
    }

    /**
     * Экспорт Справочника транспортных средств
     *
     * @return
     */
    public String exportTS(String host, String user, String passwd, String tsCompanyName) {
        String result = "";

        URI uri;
        SSLContext sslContext;
        try {
            uri = new URI(host + tsCompanyName);
            sslContext = new SSLContextBuilder().loadTrustMaterial(null, (certificate, authType) -> true).build();
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException | URISyntaxException e) {
            log.error(e.getMessage());
            return result;
        }

        HttpHost target = new HttpHost(uri.getHost());
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(new AuthScope(target.getHostName(), target.getPort()), new UsernamePasswordCredentials(user, passwd));

        try (CloseableHttpClient client = HttpClients
                .custom()
                .setDefaultCredentialsProvider(credsProvider)
                .setSSLContext(sslContext)
                .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                .build()) {
            // Create AuthCache instance
            AuthCache authCache = new BasicAuthCache();
            // Generate BASIC scheme object and add it to the local auth cache
            BasicScheme basicAuth = new BasicScheme();
            authCache.put(target, basicAuth);

            // Add AuthCache to the execution context
            HttpClientContext localContext = HttpClientContext.create();
            localContext.setAuthCache(authCache);

            HttpGet httpGet = new HttpGet(uri);
            httpGet.setHeader("Accept", "application/json");

            try (CloseableHttpResponse response = client.execute(target, httpGet, localContext)) {
                int status = response.getStatusLine().getStatusCode();
                if (status != 200) return "";

                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity, "UTF-8");
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return result;
    }
}
