package ru.advantum.integration.rnis.imp.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * @autor abc
 * @modified abc on 13.12.2016.
 */
@Root(name = "ts-company-import")
public class TSCompanyImportConfig {

    @Attribute(name = "change-check-period", required = false)
    public long changeCheckMilliSeconds = 1 * 60 * 60 * 1000;

    @Element(name = "host", required = true)
    public String host;

    @Element(name = "user", required = true)
    public String user;

    @Element(name = "password", required = true)
    public String password;

    @ElementList(name = "ts-company-list", required = false)
    public List<TSCompanyNameConfig> tsCompanyNameList;
}
