rnis-import - Сервис импорта данных из РНИС
============================

Сервис загружает данные с периодичностью `change-check-period` в БД `database` из REST сервисов `radio-jam-color-import`
и `ts-company-import`

Настройки сервиса 
-----------
rnis-import/advconf/rnis-import.xml

``````````````    
<rnis-import>

    <database url="jdbc:postgresql://172.16.21.31:5432/csptm-dev"
              program="askp-import"
              username="**"
              password="**"
              autoCommit="false"
              poolable="true">
        <pool
                initialSize="1"
                maxActive="4"
                maxWait="1"
                maxIdle="1"
                maxOpenPreparedStatement="5"
                driverClassName="org.postgresql.Driver"
                validationQuery="SELECT 1"
        />
    </database>
    
    <db-schema>rnis</db-schema>
    
    <radio-jam-color-import change-check-period="3600000">
        <host>http://radiojam.apluss.ru:8080/radio-jam-color.json</host>
    </radio-jam-color-import>
    
    <ts-company-import change-check-period="3600000">
        <host>https://rnis.mos.ru/vis/expFullAttTs/get?type=</host>
        <user>**</user>
        <password>**</password>
        <ts-company-list>
            <ts-company name="TScomp"/>
            <!--<ts-company name="TSampp"/>-->
            <!--<ts-company name="TSATTdjkh"/>-->
        </ts-company-list>
    </ts-company-import>

</rnis-import>

``````````````

Настройки логирования 
-----------
rnis-import/advconf/log4j.prop
    
