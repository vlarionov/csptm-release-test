package ru.advantum.csptm.service.ttexportservice.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
public class DBConfig {

    private TtExportServiceApplicationConfig config;

    @Autowired
    public DBConfig(TtExportServiceApplicationConfig config) {
        this.config = config;
    }

    @Bean(destroyMethod = "")
    public DataSource dataSource() throws SQLException {
        return config.databaseConfig.getConnectionPool();
    }
}
