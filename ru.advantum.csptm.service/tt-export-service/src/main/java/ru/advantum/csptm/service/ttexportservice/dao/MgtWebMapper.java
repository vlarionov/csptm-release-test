package ru.advantum.csptm.service.ttexportservice.dao;

import org.apache.ibatis.annotations.Mapper;
import ru.advantum.csptm.service.ttexportservice.model.Route;
import ru.advantum.csptm.service.ttexportservice.model.RouteStation;
import ru.advantum.csptm.service.ttexportservice.model.RouteVariant;

import java.util.List;

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@Mapper
public interface MgtWebMapper {

    List<Route> getRouteData(Integer routeID);

    List<RouteVariant> getVariantData(Integer variantID, Short directionID);

    List<RouteStation> getStationData(Integer variantID, Short directionID);

    int saveLastExportDate();

    int checkExportNeed();
}
