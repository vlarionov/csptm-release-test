package ru.advantum.csptm.service.ttexportservice.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import ru.advantum.csptm.service.ttexportservice.config.TtExportServiceApplicationConfig;
import ru.advantum.csptm.service.ttexportservice.dao.MgtWebMapper;
import ru.advantum.csptm.service.ttexportservice.model.Route;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.stream.Stream;

/**
 * Сервис выгрузки плановых расписаний в формате сайта Мосгортранс
 *
 * @author abc <alekseenkov@advantum.pro>
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Service
public class TtExportService {

    private static final Logger log = LoggerFactory.getLogger(TtExportService.class);
    private final MgtWebMapper mapper;
    private final TtExportServiceApplicationConfig config;
    private final Marshaller jaxbMarshaller;

    @Autowired
    public TtExportService(MgtWebMapper mapper, TtExportServiceApplicationConfig config) throws JAXBException {
        this.mapper = mapper;
        this.config = config;

        JAXBContext jaxbContext = JAXBContext.newInstance(Route.class);
        jaxbMarshaller = jaxbContext.createMarshaller();
    }

    private String getFileName(String routeNum, String days, String direction, LocalDate dateBegin, LocalDate dateEnd) {
        return routeNum + "_" + days + "_" + direction + "_" +
            (dateBegin == null ? "0000000" : dateBegin.format(DateTimeFormatter.BASIC_ISO_DATE)) +
            "_" +
            (dateEnd == null ? "0000000" : dateEnd.format(DateTimeFormatter.BASIC_ISO_DATE)) +
            ".xml";
    }

    private void saveToXmlFile(Route route, String storeDirectory) {
        try {
            if (!Files.exists(Paths.get(storeDirectory))) Files.createDirectories(Paths.get(storeDirectory));

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(route, new File(storeDirectory, getFileName(route.routeNum, route.days, route.direction, route.beginDate, route.endDate)));
        } catch (JAXBException | IOException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Выгрузка по вмаршруту если есть расписание на текущий момент
     */
    private void exportRouteToXmlFiles(Integer routeID, String storeRootDirectory) {
        for (Route route : mapper.getRouteData(routeID)) {
            route.variantList = mapper.getVariantData(route.ttVariantID, route.directionID.shortValue());
            route.stationList = mapper.getStationData(route.ttVariantID, route.directionID.shortValue());
            saveToXmlFile(route, storeRootDirectory + File.separator + route.transport);
        }
    }

    private Path createTempDirectory(String path, String prefix) throws IOException {
        Path result;
        if (path == null) {
            result = Files.createTempDirectory(prefix);
        } else {
            if (!Files.exists(Paths.get(path))) throw new IOException("Directory " + path + " not found");

            result = Paths.get(path).resolve(prefix + Long.toString(System.nanoTime()));
            if (Files.exists(result)) {
                result = Paths.get(path).resolve(prefix + Long.toString(System.nanoTime()));
                if (Files.exists(result)) {
                    throw new IOException("Can't create temp directory " + result.toAbsolutePath());
                }
            }
            Files.createDirectories(result);
        }
        return result;
    }

    /**
     * Выгрузка по всем маршрутам по которым есть расписание на текущий момент
     */
    private void exportRoutesToXmlFiles() throws IOException {
        Path temporaryDir = createTempDirectory(config.tempDirectory, "tt-export-");
        exportRouteToXmlFiles(null, temporaryDir.toString());
        log.info("Export routes data to xml");

        try (Stream<Path> storeDir = Files.list(Paths.get(config.storeDirectory))) {
            storeDir.forEach(p -> FileSystemUtils.deleteRecursively(p.toFile()));
        }

        try (Stream<Path> tempDir = Files.list(temporaryDir)) {
            tempDir.forEach(pathFrom -> {
                try {
                    Path pathTo = Paths.get(config.storeDirectory).resolve(pathFrom.getFileName());
                    FileSystemUtils.copyRecursively(pathFrom.toFile(), pathTo.toFile());
                } catch (IOException e) {
                    log.error(e.getMessage());
                }
            });
        }

        mapper.saveLastExportDate();

        FileSystemUtils.deleteRecursively(temporaryDir.toFile());
    }

    @Scheduled(fixedRateString = "${tt-export-service.checkRequestPeriod}")
    public void checkAndExportRoutesToXmlFiles() throws IOException {
        log.info("Check for export");
        if (mapper.checkExportNeed() > 0) exportRoutesToXmlFiles();
    }
}
