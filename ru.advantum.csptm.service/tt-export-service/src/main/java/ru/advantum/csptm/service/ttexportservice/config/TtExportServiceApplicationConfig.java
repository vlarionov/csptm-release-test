package ru.advantum.csptm.service.ttexportservice.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;

@Root(name = "tt-export-service")
public class TtExportServiceApplicationConfig {

    /** Периодичность проверки изменений */
    @Attribute(required = false)
    public int checkRequestPeriod = 2 * 60 * 1000;

    @Element(name = "database")
    public DatabaseConnection databaseConfig;

    /** Путь к файлам выгрузки */
    @Element(name = "store_dir")
    public String storeDirectory;

    /** Путь к временному каталогу */
    @Element(name = "temp_dir", required = false)
    public String tempDirectory = null;
}
