package ru.advantum.csptm.service.ttexportservice.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "route")
public class Route {

    @XmlTransient
    public Integer ttVariantID;

    @XmlTransient
    public Integer directionID;

    @XmlTransient
    public Integer routeID;

    @XmlTransient
    public LocalDate beginDate;

    @XmlTransient
    public LocalDate endDate;

    @XmlElement(name = "number")
    public String routeNum;

    @XmlElement(name = "transport")
    public String transport;

    @XmlElement(name = "direction")
    public String direction;

    @XmlElement(name = "days")
    public String days;

    @XmlElement(name = "from")
    public String from;

    @XmlElement(name = "to")
    public String to;

    @XmlElement(name = "variant")
    public List<RouteVariant> variantList = new ArrayList<>();

    @XmlElement(name = "station")
    public List<RouteStation> stationList = new ArrayList<>();
}
