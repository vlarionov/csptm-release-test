//package ru.advantum.csptm.service.ttexportservice.model;
//
///**
// * @author abc <alekseenkov@advantum.pro>
// */
//public class ExportRequest {
//
//    private Long routeID;
//    private Boolean exportAll = false;
//
//    public ExportRequest() {
//    }
//
//    public Long getRouteID() {
//        return routeID;
//    }
//
//    public void setRouteID(Long routeID) {
//        this.routeID = routeID;
//    }
//
//    public Boolean getExportAll() {
//        return exportAll;
//    }
//
//    public void setExportAll(Boolean exportAll) {
//        this.exportAll = exportAll;
//    }
//
//    @Override
//    public String toString() {
//        return "ExportRequest{" + "routeID=" + routeID + ", exportAll=" + exportAll + '}';
//    }
//}
