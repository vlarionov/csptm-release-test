package ru.advantum.csptm.service.ttexportservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.advantum.csptm.service.ttexportservice.config.TtExportServiceApplicationConfig;
import ru.advantum.csptm.spring.properties.AbstractPropertySourceFactory;

import static ru.advantum.csptm.service.ttexportservice.TtExportServiceApplication.CONFIG_FILE_NAME;

@SpringBootApplication
@EnableScheduling
@PropertySource(value = "", name = CONFIG_FILE_NAME, factory = TtExportServiceApplication.ConfigPropertySourceFactory.class)
public class TtExportServiceApplication {

    public static final String CONFIG_FILE_NAME = "tt-export-service.xml";

    public static class ConfigPropertySourceFactory extends AbstractPropertySourceFactory<TtExportServiceApplicationConfig> {
        @Override
        public TtExportServiceApplicationConfig getConfig() throws Exception {
            return applicationConfig();
        }
    }

    @Bean
    public static TtExportServiceApplicationConfig applicationConfig() throws Exception {
        return ru.advantum.config.Configuration.unpackConfig(TtExportServiceApplicationConfig.class, CONFIG_FILE_NAME);
    }

    public static void main(String[] args) {
        SpringApplication.run(TtExportServiceApplication.class, args);
    }
}
