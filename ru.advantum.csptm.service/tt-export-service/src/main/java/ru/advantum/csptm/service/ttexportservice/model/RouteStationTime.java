package ru.advantum.csptm.service.ttexportservice.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "time")
public class RouteStationTime {

    @XmlAttribute(name = "hour")
    public String hour;

    @XmlValue
    public String time;

    @Override
    public String toString() {
        return "RouteStationTime{" +
            "hour='" + hour + '\'' +
            ", time='" + time + '\'' +
            '}';
    }
}
