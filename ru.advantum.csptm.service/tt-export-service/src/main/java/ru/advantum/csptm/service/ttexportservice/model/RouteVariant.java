package ru.advantum.csptm.service.ttexportservice.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "variant")
public class RouteVariant {

    @XmlTransient
    public Integer roundID;

    @XmlAttribute(name = "id")
    public String roundCode;

    @XmlElement(name = "from")
    public String from;

    @XmlElement(name = "to")
    public String to;

    @XmlElement(name = "notes")
    public String notes;

    @Override
    public String toString() {
        return "RouteVariant{" +
            "roundID=" + roundID +
            ", roundCode='" + roundCode + '\'' +
            ", from='" + from + '\'' +
            ", to='" + to + '\'' +
            ", notes='" + notes + '\'' +
            '}';
    }
}
