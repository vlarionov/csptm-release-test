package ru.advantum.csptm.service.ttexportservice.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.List;

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "station")
public class RouteStation {

    @XmlTransient
    public Integer stationID;

    @XmlElement(name = "name")
    public String stationName;

    @XmlElement(name = "time")
    public List<RouteStationTime> times = new ArrayList<>();

    @Override
    public String toString() {
        return "RouteStation{" +
            "stationID=" + stationID +
            ", stationName='" + stationName + '\'' +
            ", times=" + times +
            '}';
    }
}
