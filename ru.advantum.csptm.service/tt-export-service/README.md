tt-export-service - Сервис выгрухки расписаний для сайта Мосгортранс
====================================================================

Сервис по расписанию (checkRequestPeriod 5 мин по умолчанию )проверяет наличие изменений в расписании и при 
необходимости выгужает в каталог store_dir.   

Настройки сервиса 
-----------------
tt-export-service/advconf/tt-export-service.xml

``````````````    
<tt-export-setvice>

    <store_dir>F:/temp/export/tt</store_dir>

    <database url="jdbc:postgresql://mgt-pg01.mgt.dev:5432/csptm-dev"
              program="tt-export-service"
              username="usr"
              password="pwd"
              autoCommit="false"
              poolable="true">
        
        <pool initialSize="1"
              maxActive="4"
              maxWait="1"
              maxIdle="1"
              maxOpenPreparedStatement="5"
              driverClassName="org.postgresql.Driver"
              validationQuery="select 1"/>
    </database>

</tt-export-setvice>

``````````````

Настройки логирования 
---------------------

Используется библиотека логирования по умолчанию Spring Boot (logback)  
tt-export-service/advconf/logback.xml
