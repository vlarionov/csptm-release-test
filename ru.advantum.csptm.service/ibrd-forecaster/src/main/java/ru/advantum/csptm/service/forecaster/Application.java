package ru.advantum.csptm.service.forecaster;

import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;
import ru.advantum.csptm.spring.properties.AbstractPropertySourceFactory;
import ru.advantum.csptm.spring.rmq.ConsumerProperties;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@SpringBootApplication
@PropertySource(value = "", name = Application.CONFIG_FILE_NAME, factory = Application.ConfigPropertySourceFactory.class)
@EnableAsync
@EnableScheduling
public class Application {
    
    public static final String CONFIG_FILE_NAME = "ibrd-forecaster.xml";

    public static class ConfigPropertySourceFactory extends AbstractPropertySourceFactory<ForecasterConfig> {

        @Override
        public ForecasterConfig getConfig() throws Exception {
            return applicationConfig();
        }
    }

    @Bean
    public static ForecasterConfig applicationConfig() throws Exception {
        return ru.advantum.config.Configuration.unpackConfig(
                ForecasterConfig.class,
                CONFIG_FILE_NAME);
    }

    @Bean
    public ConsumerProperties consumerProperties() {
        return ConsumerProperties.newBuilder()
                .setAcknowledgeMode(AcknowledgeMode.AUTO)
                .setShutdownTimeout(TimeUnit.SECONDS.toMillis(10))
                .build();
    }

    @Bean
    public JepRmqServiceConfig config(ForecasterConfig config) {
        return config.rmqServiceConfig;
    }

    @Bean
    public DataSource dataSource(ForecasterConfig config) throws SQLException {
        return config.databaseConfig.getConnectionPool();
    }

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(Application.class);
        app.setWebEnvironment(false);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }
}
