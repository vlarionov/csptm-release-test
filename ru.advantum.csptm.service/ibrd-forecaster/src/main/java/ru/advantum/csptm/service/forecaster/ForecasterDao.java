package ru.advantum.csptm.service.forecaster;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.advantum.csptm.jep.service.glinker.model.event.Forecast;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@Repository
public class ForecasterDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(ForecasterDao.class);

    private final ForecasterConfig config;
    private final JdbcTemplate jdbcTemplate;
    private final String forecastSql;

    @Autowired
    public ForecasterDao(ForecasterConfig config, JdbcTemplate jdbcTemplate) {
        this.config = config;
        this.jdbcTemplate = jdbcTemplate;

        forecastSql = getForecastSql();

        LOGGER.info("Using sql for forecast [{}]", forecastSql);
    }

    private static Forecast mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Forecast(rs.getTimestamp("creation_date").toInstant(),
                            rs.getLong("order_round_id"),
                            rs.getLong("stop_place_muid"),
                            rs.getInt("stop_place_order_num"),
                            rs.getLong("from_order_round_id"),
                            rs.getInt("from_stop_place_order_num"),
                            rs.getTimestamp("pass_time_forecast").toInstant());
    }

    private String getForecastSql() {
        switch (config.algorithmId) {
            case "algorithm_1":
                return "select * from ibrd.simple_forecaster_pkg$algorithm_1( ?::jsonb )";
            case "algorithm_2":
                return "select * from ibrd.simple_forecaster_pkg$algorithm_2( ?::jsonb )";
            default:
                throw new IllegalArgumentException(String.format("Invalid algorithm id [%s]", config.algorithmId));
        }
    }

    public List<Forecast> getForecast(String eventsParams) {
        return jdbcTemplate.query(forecastSql, new Object[]{eventsParams}, ForecasterDao::mapRow);
    }

    @Transactional
    public void updateStatistics() {
        SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName("ibrd")
                .withProcedureName("forecaster_pkg$analyze_stat_prev_day");
        call.execute();
    }
}
