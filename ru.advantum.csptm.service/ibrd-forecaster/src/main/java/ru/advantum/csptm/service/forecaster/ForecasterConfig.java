package ru.advantum.csptm.service.forecaster;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@Root(name = "ibrd-forecaster")
public class ForecasterConfig {

    @Element(name = "database")
    public DatabaseConnection databaseConfig;

    @Element(name = "rmq-service")
    public JepRmqServiceConfig rmqServiceConfig;

    @Element(name = "algorithm-id")
    public String algorithmId;

    @Element(name = "statistics-update-period", required = false)
    public int statisticUpdatePeriod = 3 * 60 * 60 * 1000;

    @Element(name = "rmq-service-event-type", required = false)
    public String rmqEventType = "STOP_VISIT";

}
