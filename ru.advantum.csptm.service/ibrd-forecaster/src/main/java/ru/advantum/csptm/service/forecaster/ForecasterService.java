package ru.advantum.csptm.service.forecaster;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.jep.service.glinker.model.event.Forecast;
import ru.advantum.csptm.spring.rmq.TypeAdder;

import java.util.List;

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@Service
public class ForecasterService {

    private static final Logger log = LoggerFactory.getLogger(ForecasterService.class);

    private final ForecasterConfig config;
    private final ForecasterDao forecasterDao;
    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public ForecasterService(ForecasterConfig config, ForecasterDao data, RabbitTemplate rabbitTemplate) {
        this.config = config;
        this.forecasterDao = data;
        this.rabbitTemplate = rabbitTemplate;
    }

    @RabbitListener(queues = "${ibrd-forecaster.rmq-service.rmq-consumer.queue}")
    public void receiveMessage(@Header("amqp_type") String type, String eventsParams) {

        if (!config.rmqEventType.equals(type)) {
            return;
        }

        try {
            log.info("event {}", eventsParams);

            List<Forecast> forecasts = forecasterDao.getForecast(eventsParams);

            if (!forecasts.isEmpty()) {
                rabbitTemplate.convertAndSend(forecasts, TypeAdder.of("FORECASTS"));
            }
        } catch (Throwable e) {
            log.error("Error on event. Type: {}. Params: {}.", type, eventsParams);
            throw e;
        }
    }

    @Scheduled(fixedRateString = "${ibrd-forecaster.statistics-update-period}")
    @Async
    public void updateStatistics() {
        try {
            log.info("Statistic update");
            forecasterDao.updateStatistics();
            log.info("Statistic update finish");
        } catch (Throwable e) {
            log.error("Statistic update", e);
        }
    }
}
