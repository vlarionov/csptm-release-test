package ru.advantum.csptm.service.reportscheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by kukushkin on 09.02.2017.
 */
public class FileCleaner {
    private final Path path;
    private static final Logger LOG = LoggerFactory.getLogger(FileCleaner.class);
    private final static long STORE_MILLS = 1000*60*60*1;
    private final ScheduledExecutorService EXECUTOR =  Executors.newSingleThreadScheduledExecutor();

    public FileCleaner(Path path) {
        this.path = path;
    }

    public void start() {
        LOG.info("Start file cleaner ... ");
        EXECUTOR.scheduleWithFixedDelay(() -> {
            try {
                try {
                    Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
                        @Override
                        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                                throws IOException {
                            if (attrs.creationTime().toMillis() < System.currentTimeMillis() - STORE_MILLS) {
                                Files.delete(file);
                            }
                            return FileVisitResult.CONTINUE;
                        }

                        @Override
                        public FileVisitResult postVisitDirectory(Path dir, IOException exc)
                                throws IOException {
                            return FileVisitResult.CONTINUE;
                        }
                    });
                } catch (IOException e) {
                    LOG.error("", e);
                }
        } catch (Throwable t) {
            LOG.error("", t);
        }
    }, 0, 1, TimeUnit.HOURS);
}
}
