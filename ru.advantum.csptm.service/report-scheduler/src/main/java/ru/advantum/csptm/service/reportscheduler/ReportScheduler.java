package ru.advantum.csptm.service.reportscheduler;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Envelope;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.agat.rmq.payload.RMQMail;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.service.reportscheduler.controller.SchedulerController;
import ru.advantum.csptm.service.reportscheduler.model.AccountTask;
import ru.advantum.csptm.service.reportscheduler.model.Report;
import ru.advantum.rabbitmq.consumer.RmqJsonTalker;
import ru.advantum.reportservicerq.model.ReportCommandType;
import ru.advantum.reportservicerq.model.ReportStatus;
import ru.advantum.reportservicerq.model.ReportStatusUpdate;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


/**
 * Created by kukushkin on 01.02.2017.
 */
public class ReportScheduler {
    private final ReportSchedulerConfig config;
    private SchedulerController schedulerController;
    private static final Logger LOG = LoggerFactory.getLogger(ReportScheduler.class);
    private  final RmqJsonTalker rmqTalker;

    private ReportScheduler(ReportSchedulerConfig config) throws IOException, TimeoutException {
        this.config = config;
        rmqTalker = RmqJsonTalker.newBuilder()
                .setConsumerConfig(false, true, config.rmqConsumer)
                .addConsumer(ReportCommandType.UPDATE_STATUS.name(), ReportStatusUpdate.class, this::consumeReport)
                .connect(config.rmqConnection.newConnection(), config.rmqConnection.newConnection());
    };

    private void start() throws SchedulerException, SQLException, IOException, TimeoutException {
        schedulerController = new SchedulerController(config, rmqTalker);
        schedulerController.init().start();
        new FileCleaner(FileSystems.getDefault().getPath(config.reportDir)).start();
    }
    public static void main(String[] args) throws Exception {
        new ReportScheduler(Configuration.unpackConfig(ReportSchedulerConfig.class, "report-scheduler.xml")).start();
    }


    public void consumeReport(String s, Envelope envelope, AMQP.BasicProperties basicProperties, ReportStatusUpdate reportStatusUpdate) {
        LOG.info(String.format("report %s %s", reportStatusUpdate.getReportHash(), reportStatusUpdate.getStatus().name()));
        if (reportStatusUpdate.getStatus().isFinished()
                && (reportStatusUpdate.getStatus().equals(ReportStatus.READY)
                    || reportStatusUpdate.getStatus().equals(ReportStatus.NO_DATA)
                    )) {
            Report report = schedulerController.getIdTask(reportStatusUpdate.getReportHash());
            if (!Objects.isNull(report)) {
                List<AccountTask> accountTask = schedulerController.getAccountTasks(report.getTaskId());
                accountTask.forEach(at-> {
                    while (true) {
                        try {

                            RMQMail mail;
                            if (reportStatusUpdate.getStatus().equals(ReportStatus.READY)) {
                                Path p = FileSystems.getDefault().getPath(config.reportDir, reportStatusUpdate.getReportHash());
                                mail = new RMQMail(at.getMail(), report.getName(), report.getName(), report.getName(), p.toString());
                                LOG.info("send mail {} with report {} file {}", config.rmqMail.routingKey, report.getName());
                            } else {
                                mail = new RMQMail(at.getMail(), report.getName(), "нет данных");
                                LOG.info("send mail {} with report {} with no file", config.rmqMail.routingKey, report.getName());
                            }

                            rmqTalker.basicPublish(config.rmqMail.exchange,
                                    config.rmqMail.routingKey,
                                    RMQMail.class.getSimpleName(),
                                    mail);
                            break;
                        } catch (Throwable ex) {
                            LOG.error("send mail", ex);
                            try {
                                TimeUnit.SECONDS.sleep(5);
                            } catch (InterruptedException e) {
                                LOG.error("sleep", ex);
                            }
                        }
                    }
                });
            }
        }

        try {
            rmqTalker.basicAck(envelope.getDeliveryTag(), false);
        } catch (IOException e) {
            LOG.error("", e);
        }
    }
}
