package ru.advantum.csptm.service.reportscheduler.model;

/**
 * Created by kukushkin on 03.02.2017.
 */
public class AccountTask {
    private final Long taskId;
    private final String mail;

    public AccountTask(Long taskId, String mail) {
        this.taskId = taskId;
        this.mail = mail;
    }


    public Long getTaskId() {
        return taskId;
    }

    public String getMail() {
        return mail;
    }
}
