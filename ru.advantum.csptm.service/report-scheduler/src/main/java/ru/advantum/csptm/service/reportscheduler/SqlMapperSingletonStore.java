package ru.advantum.csptm.service.reportscheduler;

import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.mapping.VendorDatabaseIdProvider;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.service.reportscheduler.model.mapper.StoreMapper;

import javax.sql.DataSource;

/**
 * Created by kukushkin on 01.11.2016.
 */
public class SqlMapperSingletonStore {

    protected static final Logger log = LoggerFactory.getLogger(SqlMapperSingletonStore.class);
    private static volatile SqlSessionFactory instance;
    private static Environment environment;
    private static Configuration configuration;

    private SqlMapperSingletonStore() {
    }

    public static SqlSessionFactory getInstance() {
        if (null == instance) {
            synchronized (SqlMapperSingletonStore.class) {
                if (null == instance) {
                    instance = new SqlSessionFactoryBuilder().build(configuration);
                    instance.getConfiguration().addMapper(StoreMapper.class);
                }
            }
        }
        return instance;
    }

    public static void setConfiguration(DataSource dataSource){
        SqlMapperSingletonStore.environment = new Environment("development", new JdbcTransactionFactory(), dataSource);
        SqlMapperSingletonStore.configuration = new Configuration(environment);
        final String databaseId = new VendorDatabaseIdProvider().getDatabaseId(dataSource);
        log.info("Database ID {}", databaseId);
        SqlMapperSingletonStore.configuration.setDatabaseId(databaseId);

    }

}

