package ru.advantum.csptm.service.reportscheduler.model.mapper;

import org.apache.ibatis.annotations.Param;
import ru.advantum.csptm.service.reportscheduler.model.AccountTask;
import ru.advantum.csptm.service.reportscheduler.model.Report;
import ru.advantum.csptm.service.reportscheduler.model.ReportTask;

import java.time.LocalDateTime;
import java.util.List;


/**
 * Created by kukushkin on 01.11.2016.
 */
public interface StoreMapper {

    List<ReportTask> getReportTask();

    void setReportTaskNextRun(
            @Param("id") Long id,
            @Param("dt") LocalDateTime dt
    );


    List<AccountTask> getTasksAccounts();

    List<Report> getTasksReports();
}
