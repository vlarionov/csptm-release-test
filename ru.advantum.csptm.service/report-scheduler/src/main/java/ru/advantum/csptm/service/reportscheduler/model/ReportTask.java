package ru.advantum.csptm.service.reportscheduler.model;

/**
 * Created by kukushkin on 02.02.2017.
 */
public class ReportTask {
    public static final String GROUP = "DEFAULT";
    private final Long id;
    private final String cron;

    public ReportTask(Long id, String cron) {
        this.id = id;
        this.cron = cron;
    }

    public String getJobName() {
        return String.valueOf(getId()).concat(cron);
    }

    public String getCron() {
        return cron;
    }

    public Long getId() {
        return id;
    }
}
