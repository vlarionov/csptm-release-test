package ru.advantum.csptm.service.reportscheduler.model;

/**
 * Created by kukushkin on 08.02.2017.
 */
public class Report {
    private final String method;
    private final String filter;
    private final Long taskId;
    private final String name;

    public Report(String method, String filter, Long taskId, String name) {

        this.method = method;
        this.filter = filter;
        this.taskId = taskId;
        this.name = name;
    }

    public String getMethod() {
        return method;
    }

    public String getFilter() {
        return filter;
    }

    public Long getTaskId() {
        return taskId;
    }

    public String getName() {
        return name;
    }
}
