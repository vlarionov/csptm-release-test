package ru.advantum.csptm.service.reportscheduler.job;

import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.service.reportscheduler.model.Report;
import ru.advantum.csptm.service.reportscheduler.controller.SchedulerController;
import ru.advantum.csptm.service.reportscheduler.controller.StoreDBController;
import ru.advantum.reportservicerq.model.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by kukushkin on 01.02.2017.
 */
public class ReportCreateJob implements Job {
    private static final Logger LOG = LoggerFactory.getLogger(ReportCreateJob.class);
    private static final String OF_ROWS = "OF_ROWS";
    private static String generateReportHash() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        try {
                JobDataMap data = context.getJobDetail().getJobDataMap();
                Long id = data.getLongValue(SchedulerController.JOB_ID);
                LOG.info(String.format("start executing job %s", id));

                SchedulerContext schedulerContext = context.getScheduler().getContext();
                SchedulerController schedulerController = (SchedulerController) schedulerContext.get(SchedulerController.SCHEDULER_CONTROLLER);
                String exchange = data.getString(SchedulerController.EXCHANGE);
//            String exchange = "exchange";
                String routingKey = data.getString(SchedulerController.ROUTING_KEY);
//            String routingKey = "";
            Long reportAccount = data.getLong(SchedulerController.REPORT_ACCOUNT);
                Long joflReportAccount = data.getLong(SchedulerController.REPORT_JOFL_ACCOUNT);

                List <Report> reports = schedulerController.getReportsForTask(id);

                //TODO  + формировать расширение файла при передаче в почтоывй сервис".xls"

                reports.forEach(r -> {
                        String filename  = generateReportHash();
                        ReportItem reportItem = new ReportItem(filename,
                                ReportStatus.NEW,
                                0L,
                                ReportType.JOFL,
                                ReportExecType.IMMEDIATE,
                                ReportFormatType.XLS,
                                filename,
                                new Date().getTime(),
                                null,
                                null);
                        reportItem.setIdJoflAccount(joflReportAccount);
                        reportItem.getParameters().add(new ReportParameter("P_ID_ACCOUNT", ReportParameter.ParamType.ARR, Arrays.asList(reportAccount.toString())));
                        reportItem.getParameters().add(new ReportParameter("P_TIMEZONE", ReportParameter.ParamType.ARR, Arrays.asList("3")));
                        reportItem.getParameters().add(new ReportParameter("P_DB_METHOD", ReportParameter.ParamType.ARR, Arrays.asList(r.getMethod())));
                        reportItem.getParameters().add(new ReportParameter("P_PACK_METHOD", ReportParameter.ParamType.ARR, Arrays.asList(OF_ROWS)));
                        reportItem.getParameters().add(new ReportParameter("P_ATTR", ReportParameter.ParamType.ARR, Arrays.asList(r.getFilter())));

                        LOG.info(String.format("run report exchange %s routingKey %s file %s", exchange, routingKey, filename));

                        while (true) {
                            try {
                                schedulerController.getRmqTalker().basicPublish(exchange,
                                        routingKey,
                                        ReportCommandType.NEW.name(),
                                        reportItem);
                                schedulerController.setReportHash(filename, r);
                                break;
                            } catch (Throwable ex) {
                                LOG.error("send report", ex);
                                try {
                                    TimeUnit.SECONDS.sleep(5);
                                } catch (InterruptedException e) {
                                    LOG.error("sleep", ex);
                                }
                            }
                        }

                        StoreDBController.setReportTaskNextRun(id, LocalDateTime.ofInstant(context.getNextFireTime().toInstant(), ZoneId.systemDefault()));
        });
        } catch (SchedulerException e1) {
            LOG.error("", e1);
            throw new JobExecutionException();
        }

    }
}
