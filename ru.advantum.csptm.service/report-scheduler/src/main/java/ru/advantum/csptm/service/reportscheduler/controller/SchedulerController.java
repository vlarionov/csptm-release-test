package ru.advantum.csptm.service.reportscheduler.controller;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.service.reportscheduler.*;
import ru.advantum.csptm.service.reportscheduler.job.ReportCreateJob;
import ru.advantum.csptm.service.reportscheduler.model.AccountTask;
import ru.advantum.csptm.service.reportscheduler.model.Report;
import ru.advantum.csptm.service.reportscheduler.model.ReportTask;
import ru.advantum.rabbitmq.consumer.RmqJsonTalker;

import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * Created by kukushkin on 03.02.2017.
 */
public class SchedulerController {
    public static final String JOB_ID = "id";
    public static final String SCHEDULER_CONTROLLER = "SchedulerController";
    public static final String EXCHANGE = "exchange";
    public static final String ROUTING_KEY = "routingKey";
    public static final String REPORT_ACCOUNT = "reportAccount";
    public static final String REPORT_JOFL_ACCOUNT = "reportJoflAccountId";
    private final ConcurrentHashMap<String, Report> reportTasks = new ConcurrentHashMap<>();
    private final ScheduledExecutorService EXECUTOR =  Executors.newSingleThreadScheduledExecutor();
    private final Scheduler scheduler;
    private final ReportSchedulerConfig config;
    private static final Logger LOG = LoggerFactory.getLogger(SchedulerController.class);
    private final AtomicReference<Map<Long, List<AccountTask>>> currentAccountTask = new AtomicReference<>();
    private final AtomicReference<Map<Long, List<Report>>> currentReportsForTask = new AtomicReference<>();
    private RmqJsonTalker rmqTalker;
    public SchedulerController(ReportSchedulerConfig config, RmqJsonTalker rmqTalker) throws SchedulerException {
        this.config = config;
        this.rmqTalker = rmqTalker;
        scheduler = new StdSchedulerFactory().getScheduler();
    }

    public RmqJsonTalker getRmqTalker()
    {
        return rmqTalker;
    }

    public Report getIdTask(String reportHash) {
        return reportTasks.remove(reportHash);
    }

    public void setReportHash(String reportHash, Report report) {
        if (!reportTasks.containsKey(reportHash)) {
            reportTasks.put(reportHash, report);
        }
    }


    private JobDetail getJobDetail(String name, Long id) {
        return JobBuilder.newJob(ReportCreateJob.class)
                .withIdentity(name, ReportTask.GROUP)
                .setJobData(new JobDataMap())
                .usingJobData(JOB_ID, id)
                .usingJobData(л, config.rmqMail.exchange)
                .usingJobData(ROUTING_KEY, config.rmqMail.routingKey)
                .usingJobData(REPORT_ACCOUNT, config.reportAccountId)
                .usingJobData(REPORT_JOFL_ACCOUNT, config.reportJoflAccountId)
                .build();
    }

    private Trigger getTrigger(String name, String cron) {
        return TriggerBuilder.newTrigger()
                .withIdentity(name, ReportTask.GROUP)
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cron).inTimeZone(TimeZone.getTimeZone(config.timeZone)))
                .build();
    }

    public SchedulerController init() throws SchedulerException, SQLException {
        scheduler.getContext().put(SCHEDULER_CONTROLLER, this);

        SqlMapperSingletonStore.setConfiguration(config.database.getConnectionPool());
        scheduler.start();
        List<ReportTask> reportTasks = StoreDBController.getReportTask();
        LOG.info(String.format("init %s jobs ", reportTasks.size()));
        reportTasks.forEach(t ->
        {
            try {
                LOG.info(String.format("schedule job %s with cron %s", t.getId(), t.getCron()));
                scheduler.scheduleJob(getJobDetail(t.getJobName(), t.getId()),
                        getTrigger(t.getJobName(), t.getCron())
                );
            } catch (Throwable thr) {
                LOG.error(thr.getMessage());
            }
        });
        return this;
    }

    public void start() {
        EXECUTOR.scheduleWithFixedDelay(() -> {
            try {

                List<ReportTask> reportTasks = StoreDBController.getReportTask();
                for (String group : scheduler.getJobGroupNames()) {
                    Set<JobKey> current = scheduler.getJobKeys(GroupMatcher.groupEquals(group));

                    for (JobKey jobKey : current) {
                        if (!reportTasks.stream().filter(ev -> ev.getJobName().equals(jobKey.getName())).findFirst().isPresent()) {
                            scheduler.deleteJob(jobKey);
                            LOG.info(String.format("job %s deleted ", jobKey.getName()));
                        }
                    }

                    for (ReportTask task : reportTasks) {
                        if (!current.stream().filter(ev -> ev.getName().equals(task.getJobName())).findFirst().isPresent()) {
                            try {
                                scheduler.scheduleJob(getJobDetail(task.getJobName(), task.getId()),
                                        getTrigger(task.getJobName(), task.getCron())
                                );
                                LOG.info(String.format("job %s added ", task.getJobName()));
                            } catch (SchedulerException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                List<AccountTask> at = StoreDBController.getTasksAccounts();
                LOG.info(String.format("update %s accounts for tasks ...", at.size()));
                if (!Objects.isNull(at)) {
                    Map<Long, List<AccountTask>> c = at.stream().collect(Collectors.groupingBy(AccountTask::getTaskId));
                    currentAccountTask.set(c);
                }


                List<Report> reports = StoreDBController.getTasksReports();
                LOG.info(String.format("update %s reports for tasks ...", reports.size()));
                if (!Objects.isNull(reports)) {
                    Map<Long, List<Report>> c = reports.stream().collect(Collectors.groupingBy(Report::getTaskId));
                    currentReportsForTask.set(c);
                }


            } catch (SchedulerException e) {
                LOG.error("", e);
            }  catch (Throwable thr) {
                LOG.error(thr.getMessage());
            }
        }, 0, 60, TimeUnit.SECONDS);
    };

    public List<AccountTask> getAccountTasks(Long id) {
        return currentAccountTask.get().get(id);
    }

    public List<Report> getReportsForTask(Long id) {
        return currentReportsForTask.get().get(id);
    }
}
