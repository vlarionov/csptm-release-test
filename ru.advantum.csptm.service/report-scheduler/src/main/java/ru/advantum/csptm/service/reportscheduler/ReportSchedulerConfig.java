package ru.advantum.csptm.service.reportscheduler;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.rabbitmq.config.RmqConnectionConfig;
import ru.advantum.rabbitmq.config.RmqConsumerConfig;
import ru.advantum.rabbitmq.config.RmqDestinationConfig;

/**
 * Created by kukushkin on 31.01.2017.
 */
@Root(name="report-scheduler")
public class ReportSchedulerConfig {
    @Attribute(name = "report-dir")
    public String reportDir;

    @Attribute(name = "time-zome", required = false)
    public String timeZone = "Europe/Moscow";

    @Attribute(name = "report-account-id")
    public Long reportAccountId;

    @Attribute(name = "report-jofl-account-id")
    public Long reportJoflAccountId;

    @Element(name = "database")
    public DatabaseConnection database;

    @Element(name = "rmq-connection")
    public RmqConnectionConfig rmqConnection;

    @Element(name = "rmq-consumer")
    public RmqConsumerConfig rmqConsumer;

    @Element(name = "rmq-destination-mail-service")
    public RmqDestinationConfig rmqMail;


}
