package ru.advantum.csptm.service.reportscheduler.controller;

import org.apache.ibatis.session.SqlSession;
import ru.advantum.csptm.service.reportscheduler.*;
import ru.advantum.csptm.service.reportscheduler.model.AccountTask;
import ru.advantum.csptm.service.reportscheduler.model.Report;
import ru.advantum.csptm.service.reportscheduler.model.ReportTask;
import ru.advantum.csptm.service.reportscheduler.model.mapper.StoreMapper;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by kukushkin on 05.11.2016.
 */
public class StoreDBController {
    public static List<ReportTask> getReportTask() {
        List<ReportTask> list;
        try (SqlSession session = SqlMapperSingletonStore.getInstance().openSession()) {
            final StoreMapper storeMapper = session.getMapper(StoreMapper.class);
            list = storeMapper.getReportTask();
        }
        return list;
    }

    public static void setReportTaskNextRun(Long id, LocalDateTime dt) {
        try (SqlSession session = SqlMapperSingletonStore.getInstance().openSession()) {
            final StoreMapper storeMapper = session.getMapper(StoreMapper.class);
            storeMapper.setReportTaskNextRun(id, dt);
            session.commit();
        }
    }

    public static List<AccountTask> getTasksAccounts() {
        List<AccountTask> accountTask;
        try (SqlSession session = SqlMapperSingletonStore.getInstance().openSession()) {
            final StoreMapper storeMapper = session.getMapper(StoreMapper.class);
            accountTask = storeMapper.getTasksAccounts();
        }
        return accountTask;
    }

    public static List<Report> getTasksReports() {
        List<Report> reports;
        try (SqlSession session = SqlMapperSingletonStore.getInstance().openSession()) {
            final StoreMapper storeMapper = session.getMapper(StoreMapper.class);
            reports = storeMapper.getTasksReports();
        }
        return reports;
    }

}
