package ru.advantum.integration.rnis.exp.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.integration.rnis.exp.model.ExpFile;
import ru.advantum.integration.rnis.exp.provider.GTFSFilesType;

import javax.sql.DataSource;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.regex.Pattern;

/**
 * @author Created by kukushkin on 15.02.2016.
 * @author Modified by abc <alekseenkov@advantum.pro>
 */
public final class GTFSFileBuilder {

    private static final String LINE_SEPARATOR = System.getProperty("line.separator");
    public static String COLUMN_SEPARATOR = ";";
    private static final Logger LOG = LoggerFactory.getLogger(GTFSFileBuilder.class);
    private static final Pattern PATTERN_LS = Pattern.compile(LINE_SEPARATOR);
    private static final int FETCH_SIZE = 10000;

    public static void build(DataSource ds, Path path, ExpFile file) throws Exception {

        Path csv = Paths.get(path.toString(), file.getFileName(GTFSFilesType.TEXT));
        LOG.info(String.format("start exp to csv %s", csv.toString()));
        try (BufferedWriter writer = Files.newBufferedWriter(csv)) {
            try (Connection conn = ds.getConnection();
                 Statement stmt = conn.createStatement()) {

                stmt.setFetchSize(FETCH_SIZE);
                ResultSet rs = stmt.executeQuery(file.getQuery());
                ResultSetMetaData rsmd = rs.getMetaData();
                int columnCount = rsmd.getColumnCount();

                StringBuilder sb = new StringBuilder();

                for (int i = 1; i <= columnCount; i++) {
                    sb.append(rsmd.getColumnName(i));
                    if (i < columnCount) sb.append(COLUMN_SEPARATOR);
                }
                sb.append(LINE_SEPARATOR);
                writer.write(sb.toString());
                int rowcount = 1;
                while (rs.next()) {
                    sb.setLength(0);
                    for (int i = 1; i <= columnCount; i++) {
                        if (rs.getString(i) != null) {
                            sb.append(PATTERN_LS.matcher(rs.getString(i)).replaceAll(""));
                        }
                        if (i < columnCount) sb.append(COLUMN_SEPARATOR);
                    }
                    sb.append(LINE_SEPARATOR);
                    writer.write(sb.toString());
                    if (rowcount++ % FETCH_SIZE == 0) {
                        writer.flush();
                    }
                }
                conn.rollback();
            } catch (SQLException | IOException e) {
                LOG.error("run ", e);
                return;
            }

            LOG.info("finish exp to csv ");
        }

        if (file.isZIP()) {
            Path zip = Paths.get(path.toString(), file.getFileName(GTFSFilesType.ZIP));
            LOG.info(String.format("start zip %s", zip.toString()));

            try {
                ZipFileUtils.makeZIP(csv.toString(),
                    zip.toString(),
                    file.getName()
                );
            } catch (IOException ex) {
                LOG.error("run ", ex);
            }
            if (!file.isText()) {
                Files.delete(csv);
                LOG.info(String.format("finish zip csv %s then delete csv %s", zip.toString(), csv.toString()));
            }

            LOG.info(String.format("finish zip csv %s", zip.toString()));
            if (file.isMD5()) {
                Path md5 = Paths.get(path.toString(), file.getFileName(GTFSFilesType.MD5));

                LOG.info(String.format("calc MD5 for zip %s", md5.toString()));
                try {
                    String md5val = ZipFileUtils.calcMD5(zip);
                    if (md5val != null) {
                        try (BufferedWriter writer = Files.newBufferedWriter(md5)) {
                            writer.write(md5val.replaceAll(" ", "").toUpperCase());
                        }
                    }
                } catch (IOException ex) {
                    LOG.error("run ", ex);
                }

                LOG.info(String.format("finish MD5 zip %s", md5.toString()));
            }
        }
    }
}
