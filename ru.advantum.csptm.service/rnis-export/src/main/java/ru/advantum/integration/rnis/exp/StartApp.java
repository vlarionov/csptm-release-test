package ru.advantum.integration.rnis.exp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.integration.rnis.exp.config.RnisExportConfig;
import ru.advantum.integration.rnis.exp.provider.ExternalDbProvider;
import ru.advantum.integration.rnis.exp.utils.GTFSFileBuilder;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Экспорт данных РНИС
 *
 * @author abc <alekseenkov@advantum.pro>
 */
public class StartApp {

    private static RnisExportConfig config;
    private static final Logger LOG = LoggerFactory.getLogger(StartApp.class);

    private StartApp(RnisExportConfig config) throws Exception {
        StartApp.config = config;
        GTFSFileBuilder.COLUMN_SEPARATOR = config.columnSeparator;
    }

    private void start() {
        if (config.externalDbList != null) {
            config.externalDbList.forEach(externalDbConfig ->
                {
                    try {
                        new ExternalDbProvider(externalDbConfig, config.gtfsFilesPath, config.tempFilesPath).start();
                    } catch (IOException | SQLException e) {
                        LOG.error("", e);
                    } catch (Throwable ex) {
                        LOG.error("run()", ex);
                    }
                }
            );
        }
    }

    public static void main(String[] args) throws Exception {
        final RnisExportConfig config = Configuration.unpackConfig(RnisExportConfig.class, "rnis-export.xml");
        (new StartApp(config)).start();
    }
}
