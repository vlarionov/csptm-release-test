package ru.advantum.integration.rnis.exp.provider;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Created by kukushkin on 06.04.2016.
 */
public interface Realiser {
    void onFind(Path dir) throws IOException;
}
