package ru.advantum.integration.rnis.exp.utils;

import ru.advantum.tools.HEX;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Created by kukushkin on 15.02.2016.
 */
public final class ZipFileUtils {

    public static void makeZIP(String pathToSource, String pathToTarget, String fileName) throws IOException {
        final byte[] buffer = new byte[1024];
        FileOutputStream fout = new FileOutputStream(pathToTarget);
        ZipOutputStream zout = new ZipOutputStream(fout);
        ZipEntry ze = new ZipEntry(fileName);
        FileInputStream in = new FileInputStream(pathToSource);
        zout.putNextEntry(ze);
        int len;
        while ((len = in.read(buffer)) > 0) {
            zout.write(buffer, 0, len);
        }
        in.close();
        zout.closeEntry();
        zout.close();
    }

    public static String calcMD5(Path pathTofile) throws NoSuchAlgorithmException, IOException {
        final byte[] buffer = new byte[1024];
        try (InputStream fs = Files.newInputStream(pathTofile)) {
            MessageDigest md = MessageDigest.getInstance("MD5");
            DigestInputStream dis = new DigestInputStream(fs, md);
            int read = dis.read(buffer);
            while (read > -1) {
                read = dis.read(buffer);
            }
            return HEX.byteArrayToString(dis.getMessageDigest().digest()).replaceAll(" ", "").toUpperCase();
        }
    }

    public static void addFilesToExistingZip(Path zipFile,
                                             ArrayList<Path> files,
                                             int zipMethod) throws IOException {
        if (!Files.exists(zipFile)) {
            Files.createFile(zipFile);
        }
        // get a temp file
        Path tmpPathPath = Files.createTempFile(zipFile.getFileName().toString(), null);

        Files.move(zipFile, tmpPathPath, StandardCopyOption.REPLACE_EXISTING);
        byte[] buf = new byte[1024];

        ZipInputStream zin = new ZipInputStream(Files.newInputStream(tmpPathPath));
        ZipOutputStream out = new ZipOutputStream(Files.newOutputStream(zipFile));

        out.setLevel(zipMethod);

        ZipEntry entry = zin.getNextEntry();
        while (entry != null) {
            String name = entry.getName();
            boolean notInFiles = true;
            for (Path f : files) {
                if (f.getFileName().toString().equals(name)) {
                    notInFiles = false;
                    break;
                }
            }
            if (notInFiles) {
                // Add ZIP entry to output stream.
                out.putNextEntry(new ZipEntry(name));
                // Transfer bytes from the ZIP file to the output file
                int len;
                while ((len = zin.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
            entry = zin.getNextEntry();
        }
        // Close the streams
        zin.close();
        // Compress the files

        for (Path file : files) {
            InputStream in = Files.newInputStream(file);
            // Add ZIP entry to output stream.
            out.putNextEntry(new ZipEntry(file.getFileName().toString()));
            // Transfer bytes from the file to the ZIP file
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            // Complete the entry
            out.closeEntry();
            in.close();
            // Complete the ZIP file
        }
        out.close();
        Files.delete(tmpPathPath);
    }
}
