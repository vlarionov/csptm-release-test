package ru.advantum.integration.rnis.exp.model;

import ru.advantum.integration.rnis.exp.provider.GTFSFilesType;

public interface ExpFile {

    String getQuery();

    String getFileName(GTFSFilesType type);

    boolean isText();

    boolean isZIP();

    boolean isMD5();

    String getName();
}
