package ru.advantum.integration.rnis.exp.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by kukushkin on 21.07.2016.
 */
@Root(name = "query")
public class QueryConfig {

    @Attribute
    public String name;

    @Element
    public String statement;
}
