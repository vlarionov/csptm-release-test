package ru.advantum.integration.rnis.exp.model;

import ru.advantum.integration.rnis.exp.provider.GTFSFilesType;

/**
 * Created by kukushkin on 04.02.2016.
 */
public class GTFSFile implements ExpFile {
    private String name;
    private String tableName;
    private long UP_VERSION;
    private long PREV_VERSION;
    private int NEED_ADD_TO_ALL;
    private String GTFS_VIEW_NAME;
    private int REPLACE_EXISTING;
    private int IS_TEXT;
    private int IS_ZIP;
    private int IS_MD5;
    private int NEED_UPDATE;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean needUpdate() {
        return NEED_UPDATE == 1 /*UP_VERSION > PREV_VERSION*/;
    }

    public boolean needAddToAll() {
        return NEED_ADD_TO_ALL == 1;
    }

    public long getUP_VERSION() {
        return UP_VERSION;
    }

    public long getPREV_VERSION() {
        return PREV_VERSION;
    }

    public String getTableName() {
        return tableName;
    }

    public String getGTFS_VIEW_NAME() {
        return this.GTFS_VIEW_NAME;
    }

    public boolean isReplaceExisting() {
        return REPLACE_EXISTING == 1;
    }

    public void setREPLACE_EXISTING(int REPLACE_EXISTING) {
        this.REPLACE_EXISTING = REPLACE_EXISTING;
    }

    public String getFileName(GTFSFilesType type) {
        return this.name + type.getNameExtention();
    }

    public boolean isText() {
        return IS_TEXT == 1;
    }

    public boolean isZIP() {
        return IS_ZIP == 1;
    }

    public boolean isMD5() {
        return IS_MD5 == 1;
    }

    public String getQuery() {
        return "select * from " + getGTFS_VIEW_NAME();
    }
}
