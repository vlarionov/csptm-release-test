package ru.advantum.integration.rnis.exp.provider;

/**
 * Created by kukushkin on 05.02.2016.
 */
public enum GTFSFilesType {

    ZIP("ZIP"),
    MD5("MD5"),
    TEXT("TEXT"),
    BIN("BIN");

    private final String name;

    GTFSFilesType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getNameExtention() {
        return String.format(".%s", name);
    }
}
