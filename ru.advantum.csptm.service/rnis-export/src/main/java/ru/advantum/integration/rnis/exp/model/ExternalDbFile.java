package ru.advantum.integration.rnis.exp.model;

import ru.advantum.integration.rnis.exp.provider.GTFSFilesType;

/**
 * Created by kukushkin on 20.07.2016.
 */
public class ExternalDbFile implements ExpFile {

    private final String name;
    private final String query;

    public ExternalDbFile(String name, String query) {
        this.name = name;
        this.query = query;
    }

    public String getQuery() {
        return this.query;
    }

    public String getFileName(GTFSFilesType type) {
        return this.name + type.getNameExtention();
    }

    public boolean isText() {
        return true;
    }

    public boolean isZIP() {
        return true;
    }

    public boolean isMD5() {
        return true;
    }

    public String getName() {
        return this.name;
    }
}
