package ru.advantum.integration.rnis.exp.provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Created by kukushkin on 06.04.2016.
 */
public class FindFileVisitor extends SimpleFileVisitor<Path> {

    private static final Logger LOG = LoggerFactory.getLogger(FindFileVisitor.class);
    private final PathMatcher matcher;
    private int numMatches = 0;
    Realiser realiser;

    FindFileVisitor(String pattern, Realiser realiser) {
        this.realiser = realiser;
        matcher = FileSystems.getDefault().getPathMatcher("regex:" + pattern);
    }

    /**
     * Compares the glob pattern against the file or directory name.
     * @param file
     */
    void find(Path file) {
        Path name = file.getFileName();
        if (name != null && matcher.matches(name)) {
            try {
                realiser.onFind(file);
            } catch (IOException e) {
                LOG.error("", e.getMessage());
            }
            numMatches++;
        }
    }

    /**
     * Invoke the pattern matching method on each file.
     * @param file
     * @param attrs
     * @return
     */
    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
        find(file);
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) {
        LOG.warn("run ", exc);
        return FileVisitResult.CONTINUE;
    }
}
