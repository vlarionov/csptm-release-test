package ru.advantum.integration.rnis.exp.config;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * @author abc <alekseenkov@advantum.pro>
 */
public class RnisExportConfig {

    @Element(name = "gtfs_files_path")
    public String gtfsFilesPath;

    @Element(name = "temp_files_path", required = false)
    public String tempFilesPath = null;

    @Element(name = "column_separator", required = false)
    public String columnSeparator = ";";

    @ElementList(name = "external_db_list")
    public List<ExternalDbConfig> externalDbList;
}
