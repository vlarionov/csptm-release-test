package ru.advantum.integration.rnis.exp.provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.integration.rnis.exp.config.ExternalDbConfig;
import ru.advantum.integration.rnis.exp.config.QueryConfig;
import ru.advantum.integration.rnis.exp.model.ExternalDbFile;
import ru.advantum.integration.rnis.exp.utils.GTFSFileBuilder;
import ru.advantum.integration.rnis.exp.utils.ZipFileUtils;

import javax.sql.DataSource;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipOutputStream;

/**
 * Created by kukushkin on 20.07.2016.
 * Modified abc <alekseenkov@advantum.pro>
 */
public class ExternalDbProvider {

    private static final Logger LOG = LoggerFactory.getLogger(ExternalDbProvider.class);
    private DataSource ds;
    private ExternalDbConfig configDB;
    private final ScheduledExecutorService EX = Executors.newSingleThreadScheduledExecutor();
    private final Path gtfsPath;
    private final ArrayList<ExternalDbFile> files = new ArrayList<>();
    private final static String GTFS_ALL = "GTFS_ALL";
    private final String tempDirPrefix;
    private final String tempFilesPath;

    public ExternalDbProvider(ExternalDbConfig configDB, String gtfsFilesPath, String tempFilesPath) {
        this.configDB = configDB;
        this.gtfsPath = FileSystems.getDefault().getPath(gtfsFilesPath);
        this.tempFilesPath = tempFilesPath;
        tempDirPrefix = this.configDB.database.program + "_" + this.configDB.name;
    }

    public void start() throws IOException, SQLException {

        this.ds = this.configDB.database.getConnectionPool();
        List<QueryConfig> qc = this.configDB.list;
        qc.forEach((query) ->
                {
                    files.add(new ExternalDbFile(query.name, query.statement));
                    LOG.info(String.format("add %s query %s", this.configDB.name, query.name));
                }
        );

        EX.scheduleAtFixedRate(() -> {
            try {
                updateFiles(files);
            } catch (Throwable ex) {
                LOG.error("run()", ex);
            }
        }, 0, configDB.filesUpdatePeriodMs, TimeUnit.MILLISECONDS);
    }

    private void updateFiles(ArrayList<ExternalDbFile> gtfsFiles) throws Exception {

        final ArrayList<Path> filesForZipAll = new ArrayList<>();
        //Path temporaryDir = Files.createTempDirectory(tempDirPrefix);
        Path temporaryDir = createTempDirectory(tempFilesPath, tempDirPrefix);
        LOG.info(String.format("Create temp dir %s", temporaryDir.toString()));
        gtfsFiles.forEach((file) -> {
            try {
                GTFSFileBuilder.build(ds, temporaryDir, file);

                filesForZipAll.add(FileSystems.getDefault().getPath(temporaryDir.toString(), file.getFileName(GTFSFilesType.TEXT)));
            } catch (Exception e) {
                LOG.error("", e);
            }
        });

        // Create zip of all
        LOG.info(String.format("ALL_ZIP refresh with %s files", filesForZipAll.size()));
        Path zipAll = FileSystems.getDefault().getPath(temporaryDir.toString(), GTFS_ALL + GTFSFilesType.ZIP.getNameExtention());
        ZipFileUtils.addFilesToExistingZip(zipAll, filesForZipAll, ZipOutputStream.DEFLATED);
        LOG.info(String.format("Finish ALL_ZIP refresh with %s files", filesForZipAll.size()));
        String md5val = ZipFileUtils.calcMD5(zipAll);
        Path zipAllMd5 = Paths.get(temporaryDir.toString(), GTFS_ALL + GTFSFilesType.MD5.getNameExtention());
        if (md5val != null) {
            try (BufferedWriter writer = Files.newBufferedWriter(zipAllMd5)) {
                writer.write(md5val.replaceAll(" ", "").toUpperCase());
            }
        }

        // Move to result dir
        if (!Files.exists(gtfsPath)) Files.createDirectories(gtfsPath);

        moveToResultDir(temporaryDir, gtfsPath);

        // Удаляем старые временные каталоги, если есть
        Files.walk(temporaryDir.getParent(), 1)
                .filter(Files::isDirectory)
                .filter(p -> p.getFileName().toString().startsWith(tempDirPrefix))
                .forEach(p -> deleteDir(p));
    }

    private void moveToResultDir(Path fromDir, Path toDir) throws IOException {

        Files.walkFileTree(fromDir, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.move(file, toDir.resolve(fromDir.relativize(file)), StandardCopyOption.REPLACE_EXISTING);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException e) throws IOException {
                if (e == null) {
                    // Удалить временный каталог
                    Files.delete(dir);
                    LOG.info(String.format("Move files to result dir and delete temp dir %s", dir.toString()));
                    return FileVisitResult.CONTINUE;
                } else {
                    // directory iteration failed
                    throw e;
                }
            }
        });
    }

    private void deleteDir(Path dir) {
        try {
            Files.walkFileTree(dir, new SimpleFileVisitor<Path>() {

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException e) throws IOException {
                    if (e == null) {
                        // Удалить временный каталог
                        Files.delete(dir);
                        LOG.info(String.format("Delete old temp dir %s", dir.toString()));
                        return FileVisitResult.CONTINUE;
                    } else {
                        // directory iteration failed
                        throw e;
                    }
                }
            });
        } catch (IOException e) {
            LOG.error("deleteDir", e);
        }
    }


    public Path createTempDirectory(String path, String prefix) throws IOException {
        Path result = null;
        if (path == null) {
            result = Files.createTempDirectory(prefix);
        } else {
            if (!Files.exists(Paths.get(path))) throw new IOException("Directory " + path + " not found");

            result = Paths.get(path).resolve(prefix + Long.toString(System.nanoTime()));
            if (Files.exists(result)) {
                result = Paths.get(path).resolve(prefix + Long.toString(System.nanoTime()));
                if (Files.exists(result)) {
                    throw new IOException("Can't create temp directory " + result.toAbsolutePath());
                }
            }
            Files.createDirectories(result);
        }
        return result;
    }
}