/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.advantum.integration.rnis.exp.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;

import java.util.List;

@Root(name = "external_db")
public class ExternalDbConfig {

    @Attribute(name = "name")
    public String name;

    @Element(name = "database")
    public DatabaseConnection database;

    @Attribute(name = "files_update_period_ms")
    public int filesUpdatePeriodMs;

    @ElementList(name = "query_list")
    public List<QueryConfig> list;
}
