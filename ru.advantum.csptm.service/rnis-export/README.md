rnis-export - Сервис подготовки данных РНИС
============================

Сервис с периодичностью `files_update_period_ms` выполняет выгрузку из БД `database`, `query_list` в виде файлов 
.TXT, .ZIP, .MD5 в каталог `gtfs_files_path`. Доступ к эти файлам предоставляется с помощю сервися `rnis-service`

Настройки сервиса 
-----------
gis-service/advconf/rnis-export.xml

``````````````    
<rnis-export>

    <!--Каталог выгрузки файлов-->
    <gtfs_files_path>F:/Temp/export/rnis</gtfs_files_path>

    <external_db_list>

        <!--Конфигурация для формирования файлов выгрузки из базы КСУПТ-->
        <external_db name="ksupt_rnis"
                     files_update_period_ms="18000000">

            <database url="jdbc:postgresql://172.16.21.31:5432/csptm-dev"
                      program="rnis-export"
                      username="**"
                      password="**"
                      autoCommit="false"
                      poolable="true">
                <pool
                        initialSize="1"
                        maxActive="4"
                        maxWait="1"
                        maxIdle="1"
                        maxOpenPreparedStatement="5"
                        driverClassName="org.postgresql.Driver"
                        validationQuery="SELECT 1"
                />
            </database>

            <query_list>
                <query name="GTFS_AGENCY">
                    <statement>
                        select * from rnis.v_agency
                    </statement>
                </query>
                ...
            </query_list>

        </external_db>

    </external_db_list>

</rnis-export>

``````````````

Настройки логирования 
-----------
rnis-export/advconf/log4j.prop
