package ru.advantum.csptm.service.forecaster.cmplx.config;

import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;
import ru.advantum.csptm.jep.service.glinker.model.event.EventType;
import ru.advantum.csptm.jep.service.glinker.model.event.StopItemPass;
import ru.advantum.csptm.service.forecaster.cmplx.CmplxForecasterConfig;
import ru.advantum.csptm.service.forecaster.cmplx.model.ModelTrainRequest;
import ru.advantum.csptm.spring.rmq.ConsumerProperties;
import ru.advantum.csptm.spring.rmq.MessageTypeResolver;
import ru.advantum.csptm.spring.rmq.MultiTypeResolver;

import java.util.concurrent.TimeUnit;

@Configuration
public class RmqConfig {

    @Bean
    public ConsumerProperties consumerProperties() {
        return ConsumerProperties.newBuilder()
                .setAcknowledgeMode(AcknowledgeMode.AUTO)
                .setShutdownTimeout(TimeUnit.SECONDS.toMillis(10))
                .build();
    }

    @Bean
    public JepRmqServiceConfig config(CmplxForecasterConfig config) {
        return config.rmqServiceConfig;
    }

    @Bean
    public MessageTypeResolver messageTypeResolver() {
        return MultiTypeResolver.newBuilder()
                .addPair("TRAIN_REQUEST", ModelTrainRequest.class)
                .addPair(EventType.STOP_ITEM_PASS.name(), StopItemPass[].class)
                .build();
    }
}
