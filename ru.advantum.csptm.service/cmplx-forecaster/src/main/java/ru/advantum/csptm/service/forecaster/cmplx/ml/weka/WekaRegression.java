package ru.advantum.csptm.service.forecaster.cmplx.ml.weka;

import ru.advantum.csptm.service.forecaster.cmplx.ml.ModelTrainerConfig;
import ru.advantum.csptm.service.forecaster.cmplx.model.StopPassTrainData;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;
import weka.core.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class WekaRegression {

    private final Classifier classifier;

    public WekaRegression(Classifier classifier) {
        this.classifier = classifier;
    }

    public static WekaRegression train(ModelTrainerConfig config,
                                       List<StopPassTrainData> trainData) {
        try {
            Classifier c = AbstractClassifier.forName(config.getClassifier(),
                                                      Utils.splitOptions(config.getClassifierOptions()));
            c.buildClassifier(createInstances(trainData));
            return new WekaRegression(c);
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    private static Instances createInstances(List<StopPassTrainData> data) {
        final List<StopPassInstance> instanceList = data.stream().map(StopPassInstance::new).collect(Collectors.toList());
        final Instances instances = new Instances("t", new ArrayList<>(StopPassInstance.ATTRIBUTES), data.size());
        instances.setClass(StopPassInstance.ATTR_TT_NEXT);
        instances.addAll(instanceList);
        return instances;
    }

    public static WekaRegression from(String binaryData) {
        ByteArrayInputStream bais = new ByteArrayInputStream(Base64.getDecoder().decode(binaryData));
        try {
            return new WekaRegression((Classifier) SerializationHelper.read(bais));
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public double calculate(int timeWindow, double tt, double ttPrev) {
        try {
            return classifier.classifyInstance(new StopPassInstance(timeWindow, tt, ttPrev));
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public String toBinaryData() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            SerializationHelper.write(baos, classifier);
            return new String(Base64.getEncoder().encode(baos.toByteArray()));
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    private static class StopPassInstance extends DenseInstance {

        private static final List<String> TIME_WINDOW_VALUES = Arrays.asList(IntStream.range(0, 24).mapToObj(Integer::toString).toArray(String[]::new));

        public static final Attribute ATTR_TIME_WINDOW = new Attribute("time_window", TIME_WINDOW_VALUES, 0);
        public static final Attribute ATTR_TT = new Attribute("tt", 1);
        public static final Attribute ATTR_TT_PREV = new Attribute("tt_prev", 2);
        public static final Attribute ATTR_TT_NEXT = new Attribute("tt_next", 3);

        public static final List<Attribute> ATTRIBUTES = Arrays.asList(ATTR_TIME_WINDOW, ATTR_TT, ATTR_TT_PREV, ATTR_TT_NEXT);

        public static final Instances DATASET;

        static {
            DATASET = new Instances("ds", new ArrayList<>(ATTRIBUTES), 0);
            DATASET.setClass(ATTR_TT_NEXT);
        }

        public StopPassInstance(StopPassTrainData trainData) {
            super(1, new double[ATTRIBUTES.size()]);

            setValue(ATTR_TIME_WINDOW, Integer.toString(trainData.getTimeWindow()));
            setValue(ATTR_TT, trainData.getTt());
            setValue(ATTR_TT_PREV, trainData.getTtPrev());
            setValue(ATTR_TT_NEXT, trainData.getTtNext());
        }

        public StopPassInstance(int timeWindow, double tt, double ttPrev) {
            super(1, new double[ATTRIBUTES.size()]);

            setValue(ATTR_TIME_WINDOW, timeWindow);
            setValue(ATTR_TT, tt);
            setValue(ATTR_TT_PREV, ttPrev);

            setDataset(DATASET);
        }
    }
}
