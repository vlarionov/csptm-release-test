package ru.advantum.csptm.service.forecaster.cmplx.config;

import com.google.common.cache.CacheBuilder;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.util.concurrent.TimeUnit;

@Configuration
@EnableCaching
public class CacheConfig {

    public static final String LONG_CACHE_MANAGER = "longCacheManager";
    public static final String SHORT_CACHE_MANAGER = "shortCacheManager";

    @Bean(name = LONG_CACHE_MANAGER)
    public CacheManager longCacheManager() {
        final GuavaCacheManager guavaCacheManager = new GuavaCacheManager();
        guavaCacheManager.setCacheBuilder(CacheBuilder.newBuilder().expireAfterWrite(5, TimeUnit.HOURS));
        return guavaCacheManager;
    }

    @Primary
    @Bean(name = SHORT_CACHE_MANAGER)
    public CacheManager shortCacheManager() {
        final GuavaCacheManager guavaCacheManager = new GuavaCacheManager();
        guavaCacheManager.setCacheBuilder(CacheBuilder.newBuilder().expireAfterWrite(5, TimeUnit.MINUTES));
        return guavaCacheManager;
    }
}
