package ru.advantum.csptm.service.forecaster.cmplx.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class StopPassTrainData {

    private final LocalDateTime orderDate;
    private final long trId;
    private final long ttActionId;
    private final long roundId;
    private final int stopOrderNum;
    private final LocalDateTime passTime;
    private final int timeWindow;
    private final double tt;
    private final Double ttPrev;
    private final Double ttNext;
}
