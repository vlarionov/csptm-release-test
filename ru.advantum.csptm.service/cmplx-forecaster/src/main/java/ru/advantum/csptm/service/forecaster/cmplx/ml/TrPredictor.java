package ru.advantum.csptm.service.forecaster.cmplx.ml;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.advantum.csptm.jep.service.glinker.model.event.Forecast;
import ru.advantum.csptm.jep.service.glinker.model.event.StopItemPass;
import ru.advantum.csptm.service.forecaster.cmplx.model.RoundStop;
import ru.advantum.csptm.service.forecaster.cmplx.model.SimpleTtAction;
import ru.advantum.csptm.service.forecaster.cmplx.service.TimetableMapper;

import java.time.Duration;
import java.time.Instant;
import java.util.*;

@Component
public class TrPredictor {

    // TODO: в конфиг
    private static final Duration TIME_BETWEEN_ROUNDS = Duration.ZERO;

    private final RoundPredictor roundPredictor;
    private final TimetableMapper mapper;

    @Autowired
    public TrPredictor(RoundPredictor roundPredictor,
                       @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") TimetableMapper mapper) {
        this.roundPredictor = roundPredictor;
        this.mapper = mapper;
    }

    public List<Forecast> predict(StopItemPass stopItemPass, double tt) {
        final Map<Integer, RoundStop> roundStops = mapper.getRoundStops(stopItemPass.getRoundId());
        if (roundStops.isEmpty()) {
            // плохое действие
            return Collections.emptyList();
        }
        final List<Forecast> forecasts = new ArrayList<>();
        final Instant curRoundLastStopPrediction;

        // не последняя остановка
        if (stopItemPass.getStopOrderNum() < roundStops.size()) {
            forecasts.addAll(predictTtAction(stopItemPass,
                                             stopItemPass.getTtActionId(),
                                             stopItemPass.getRoundId(),
                                             stopItemPass.getStopOrderNum(),
                                             stopItemPass.getPassTime(),
                                             tt,
                                             roundStops));
            if (forecasts.isEmpty()) {
                // плохое действие
                return Collections.emptyList();
            }
            curRoundLastStopPrediction = forecasts.get(forecasts.size() - 1).getPassTimeForecast();
        } else {
            curRoundLastStopPrediction = stopItemPass.getPassTime();
        }

        final SimpleTtAction nextTtAction = mapper.getNextTtAction(stopItemPass.getTtActionId());
        // TODO: настройка, логи
        if (nextTtAction != null) {
            Map<Integer, RoundStop> nextRoundStops = mapper.getRoundStops(nextTtAction.getRoundId());
            if (nextRoundStops.isEmpty()) {
                // следующее действие плохое
                return forecasts;
            }
            final Instant firstStopPrediction = firstStopPredict(curRoundLastStopPrediction, nextTtAction.getTimePlanBegin());
            forecasts.add(new Forecast(stopItemPass.getPassTime(),
                                       nextTtAction.getTtActionId(),
                                       nextRoundStops.get(1).getStopItemId(),
                                       1,
                                       stopItemPass.getTtActionId(),
                                       stopItemPass.getStopOrderNum(),
                                       firstStopPrediction));
            forecasts.addAll(predictTtAction(stopItemPass,
                                             nextTtAction.getTtActionId(),
                                             nextTtAction.getRoundId(),
                                             1,
                                             firstStopPrediction,
                                             0,
                                             nextRoundStops));
        }

        return forecasts;
    }

    private List<Forecast> predictTtAction(StopItemPass stopItemPassFrom,
                                           long ttActionId,
                                           long roundId,
                                           int stopOrderNumPredictFrom,
                                           Instant stopItemPassTime,
                                           double tt,
                                           Map<Integer, RoundStop> roundStops) {
        SortedMap<Integer, Instant> predictions = new TreeMap<>(roundPredictor.predictTtAction(ttActionId,
                                                                                               roundId,
                                                                                               stopOrderNumPredictFrom,
                                                                                               stopItemPassTime,
                                                                                               roundStops.size(),
                                                                                               tt));

        List<Forecast> forecasts = new ArrayList<>();
        predictions.forEach((stopOrderNum, passTimeForecast) ->
                                    forecasts.add(new Forecast(stopItemPassFrom.getPassTime(),
                                                               ttActionId,
                                                               roundStops.get(stopOrderNum).getStopItemId(),
                                                               stopOrderNum,
                                                               stopItemPassFrom.getTtActionId(),
                                                               stopItemPassFrom.getStopOrderNum(),
                                                               passTimeForecast)));
        return forecasts;
    }

    private Instant firstStopPredict(Instant curRoundLastStopPrediction,
                                     Instant nextRoundTimePlanBegin) {
        curRoundLastStopPrediction = curRoundLastStopPrediction.plus(TIME_BETWEEN_ROUNDS);

        return curRoundLastStopPrediction.compareTo(nextRoundTimePlanBegin) >= 0
               ? curRoundLastStopPrediction
               : nextRoundTimePlanBegin;
    }
}
