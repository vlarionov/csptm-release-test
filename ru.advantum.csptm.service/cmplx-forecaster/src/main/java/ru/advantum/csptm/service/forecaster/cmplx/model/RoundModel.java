package ru.advantum.csptm.service.forecaster.cmplx.model;

import lombok.Data;
import ru.advantum.csptm.service.forecaster.cmplx.ml.weka.WekaRegression;

import java.util.Map;

@Data
public class RoundModel {

    private final long roundId;
    private final ModelMetaData metaData;
    private final Map<Integer, WekaRegression> stopModels;
}
