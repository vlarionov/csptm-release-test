package ru.advantum.csptm.service.forecaster.cmplx.redis;

import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Map;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RedisMapDumper<K, V> {

    private final RedisTemplate<String, Map<K, V>> redisTemplate;
    private BoundValueOperations<String, Map<K, V>> redisOperations;
    private volatile Map<K, V> map;

    @Autowired
    public RedisMapDumper(RedisTemplate<String, Map<K, V>> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public void bindMap(Map<K, V> map, String redisKey, Class<K> keyClass, Class<V> valueClass) {
        bindMap(map, redisKey, TypeToken.getParameterized(Map.class, keyClass, valueClass));
    }

    public void bindMap(Map<K, V> map, String redisKey, TypeToken typeToken) {
        this.map = map;

        redisTemplate.setKeySerializer(new StringRedisSerializer());

        redisTemplate.setValueSerializer(new GsonRedisSerializer<Map<K, V>>(typeToken.getType()));
        redisOperations = redisTemplate.boundValueOps(redisKey);
    }

    public void restoreState() {
        Map<K, V> mapFromRedis = redisOperations.get();
        if (mapFromRedis == null) {
            mapFromRedis = Collections.emptyMap();
        }
        map.putAll(mapFromRedis);
    }

    public void dumpState() {
        redisOperations.set(map);
    }
}