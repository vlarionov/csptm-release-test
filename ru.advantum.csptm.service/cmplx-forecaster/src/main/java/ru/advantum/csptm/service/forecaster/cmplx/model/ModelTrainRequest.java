package ru.advantum.csptm.service.forecaster.cmplx.model;

import lombok.Data;
import ru.advantum.csptm.service.forecaster.cmplx.ml.ModelTrainerConfig;

import java.time.LocalDate;

@Data
public class ModelTrainRequest {

    private final ModelTrainerConfig customConfig;
    private final long routeId;
    private final LocalDate beginDate;
    private final LocalDate endDate;
    private final int windowWidth;
}
