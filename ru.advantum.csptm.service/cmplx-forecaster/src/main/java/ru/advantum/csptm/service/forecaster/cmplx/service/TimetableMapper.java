package ru.advantum.csptm.service.forecaster.cmplx.service;

import org.apache.ibatis.annotations.*;
import org.springframework.cache.annotation.Cacheable;
import ru.advantum.csptm.service.forecaster.cmplx.config.CacheConfig;
import ru.advantum.csptm.service.forecaster.cmplx.model.RoundStop;
import ru.advantum.csptm.service.forecaster.cmplx.model.SimpleTtAction;
import ru.advantum.csptm.service.forecaster.cmplx.model.StopItemOperTime;

import java.time.LocalDateTime;
import java.util.Map;

@Mapper
public interface TimetableMapper {

    @SuppressWarnings("SpringCacheAnnotationsOnInterfaceInspection")
    @Cacheable(value = "ttActionTt", cacheManager = CacheConfig.SHORT_CACHE_MANAGER)
    @Select("select * from ibrd.cmplx_forecaster_ttb_pkg$get_tt_action_tt(#{tt_action_id}::int)")
    @ConstructorArgs({
            @Arg(column = "stop_order_num", javaType = int.class),
            @Arg(column = "stop_item_id", javaType = long.class),
            @Arg(column = "time_oper", javaType = LocalDateTime.class),
            @Arg(column = "tt", javaType = double.class)
    })
    @MapKey("stopOrderNum")
    Map<Integer, StopItemOperTime> getTtActionTt(@Param("tt_action_id") long ttActionId);

    @SuppressWarnings("SpringCacheAnnotationsOnInterfaceInspection")
    @Cacheable(value = "nextTtAction", cacheManager = CacheConfig.SHORT_CACHE_MANAGER)
    @Select("select * from ibrd.cmplx_forecaster_ttb_pkg$get_next_tt_action(#{tt_action_id}::int)")
    @ConstructorArgs({
            @Arg(column = "tt_action_id", javaType = long.class),
            @Arg(column = "round_id", javaType = long.class),
            @Arg(column = "time_plan_begin", javaType = LocalDateTime.class)
    })
    SimpleTtAction getNextTtAction(@Param("tt_action_id") long ttActionId);

    @SuppressWarnings("SpringCacheAnnotationsOnInterfaceInspection")
    @Cacheable(value = "roundStops", cacheManager = CacheConfig.LONG_CACHE_MANAGER)
    @Select("select * from ibrd.cmplx_forecaster_ttb_pkg$get_round_stops(#{round_id}::int)")
    @ConstructorArgs({
            @Arg(column = "round_id", javaType = long.class),
            @Arg(column = "stop_order_num", javaType = int.class),
            @Arg(column = "stop_item_id", javaType = long.class)
    })
    @MapKey("stopOrderNum")
    Map<Integer, RoundStop> getRoundStops(@Param("round_id") long roundId);
}
