package ru.advantum.csptm.service.forecaster.cmplx.model;

import lombok.Data;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Data
public class SimpleTtAction {

    private final long ttActionId;
    private final long roundId;
    private final Instant timePlanBegin;

    public SimpleTtAction(long ttActionId, long roundId, LocalDateTime timePlanBegin) {
        this.ttActionId = ttActionId;
        this.roundId = roundId;
        this.timePlanBegin = timePlanBegin.toInstant(ZoneOffset.UTC);
    }
}