package ru.advantum.csptm.service.forecaster.cmplx.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.service.forecaster.cmplx.ml.ModelTrainer;
import ru.advantum.csptm.service.forecaster.cmplx.model.ModelTrainRequest;
import ru.advantum.csptm.service.forecaster.cmplx.model.RoundModel;

import java.util.List;

@Service
public class ModelTrainerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModelTrainerService.class);

    private final ModelTrainer modelTrainer;
    private final ModelStore modelStore;

    @Autowired
    public ModelTrainerService(ModelTrainer modelTrainer,
                               ModelStore modelStore) {
        this.modelTrainer = modelTrainer;
        this.modelStore = modelStore;
    }

    @RabbitListener(queues = "#{applicationConfig.consumer.queue}")
    public void receiveTrainRequest(ModelTrainRequest request) {
        LOGGER.info("Received request {}", request);
        final List<RoundModel> routeModels = modelTrainer.trainRouteModels(request.getRouteId(),
                                                                           request.getBeginDate(),
                                                                           request.getEndDate(),
                                                                           request.getCustomConfig(),
                                                                           request.getWindowWidth());
        LOGGER.info("Got {} models", routeModels.size());
        routeModels.forEach(modelStore::storeModel);
        LOGGER.info("Models stored");
    }
}