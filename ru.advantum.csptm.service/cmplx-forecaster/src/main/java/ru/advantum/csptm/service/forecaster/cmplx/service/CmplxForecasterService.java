package ru.advantum.csptm.service.forecaster.cmplx.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.jep.service.glinker.model.event.Forecast;
import ru.advantum.csptm.jep.service.glinker.model.event.StopItemPass;
import ru.advantum.csptm.service.forecaster.cmplx.ml.TrPredictor;
import ru.advantum.csptm.spring.rmq.TypeAdder;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;

/**
 * @author kaganov
 */
@Service
public class CmplxForecasterService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CmplxForecasterService.class);

    private final TravelTimeStore travelTimeStore;
    private final TrPredictor trPredictor;
    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public CmplxForecasterService(TravelTimeStore travelTimeStore,
                                  TrPredictor trPredictor,
                                  RabbitTemplate rabbitTemplate) {
        this.travelTimeStore = travelTimeStore;
        this.trPredictor = trPredictor;
        this.rabbitTemplate = rabbitTemplate;
    }

    @RabbitListener(queues = "#{applicationConfig.rmqServiceConfig.consumer.queue}")
    public void receiveStopPass(StopItemPass[] stopItemPasses) {
        final List<Forecast> forecasts = new ArrayList<>();
        for (StopItemPass stopItemPass : stopItemPasses) {
            final OptionalDouble tt = travelTimeStore.newStopPass(stopItemPass);
            if (!tt.isPresent() && stopItemPass.getStopOrderNum() != 1) {
                LOGGER.debug("No tt {}", stopItemPass);
            }
            forecasts.addAll(trPredictor.predict(stopItemPass, tt.orElse(0)));
        }
        LOGGER.info("Stops {} forecasts {}", stopItemPasses.length, forecasts.size());

        if (!forecasts.isEmpty()) {
            rabbitTemplate.convertAndSend(forecasts, TypeAdder.of("FORECASTS"));
        }
    }
}
