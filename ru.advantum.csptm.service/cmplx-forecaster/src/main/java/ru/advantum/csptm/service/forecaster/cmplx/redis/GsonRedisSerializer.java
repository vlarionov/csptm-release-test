package ru.advantum.csptm.service.forecaster.cmplx.redis;

import com.fatboyindustrial.gsonjavatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;

public class GsonRedisSerializer<T> implements RedisSerializer<T> {

    private final Gson gson = Converters.registerAll(new GsonBuilder().enableComplexMapKeySerialization()).create();
    private final Type type;

    public GsonRedisSerializer(Class<T> klass) {
        this.type = klass;
    }

    public GsonRedisSerializer(Type type) {
        this.type = type;
    }

    @Override
    public byte[] serialize(T t) throws SerializationException {
        return gson.toJson(t).getBytes();
    }

    @Override
    public T deserialize(byte[] bytes) throws SerializationException {
        if (bytes == null) {
            return null;
        } else {
            return gson.fromJson(new String(bytes, StandardCharsets.UTF_8), type);
        }
    }
}
