package ru.advantum.csptm.service.forecaster.cmplx.ml;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.advantum.csptm.service.forecaster.cmplx.ml.weka.WekaRegression;
import ru.advantum.csptm.service.forecaster.cmplx.model.RoundModel;
import ru.advantum.csptm.service.forecaster.cmplx.model.StopItemOperTime;
import ru.advantum.csptm.service.forecaster.cmplx.service.ModelStore;
import ru.advantum.csptm.service.forecaster.cmplx.service.TimetableMapper;
import ru.advantum.csptm.service.forecaster.cmplx.service.TravelTimeStore;

import java.time.Instant;
import java.time.ZoneOffset;
import java.util.*;

@Component
public class RoundPredictor {

    private static final Logger LOGGER = LoggerFactory.getLogger(RoundPredictor.class);

    private final TravelTimeStore travelTimeStore;
    private final ModelStore modelStore;
    private final TimetableMapper mapper;

    @Autowired
    public RoundPredictor(TravelTimeStore travelTimeStore,
                          ModelStore modelStore,
                          @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") TimetableMapper mapper) {
        this.travelTimeStore = travelTimeStore;
        this.modelStore = modelStore;
        this.mapper = mapper;
    }

    public Map<Integer, Instant> predictTtAction(long ttActionId,
                                                 long roundId,
                                                 int stopOrderNum,
                                                 Instant stopItemPassTime,
                                                 int stopsCnt,
                                                 double tt) {
        final Optional<RoundModel> model = modelStore.getModel(roundId);
        if (model.isPresent()) {
            LOGGER.debug("Using {}", model.get().getMetaData());
            return predictTtActionWithModel(ttActionId,
                                            roundId,
                                            stopOrderNum,
                                            stopItemPassTime,
                                            model.get(),
                                            stopsCnt,
                                            tt);
        } else {
            LOGGER.debug("No model for round {} fallback", roundId);
            return fallbackPredict(ttActionId,
                                   stopOrderNum + 1,
                                   stopItemPassTime);
        }
    }

    private Map<Integer, Instant> predictTtActionWithModel(long ttActionId,
                                                           long roundId,
                                                           int stopOrderNum,
                                                           Instant stopItemPassTime,
                                                           RoundModel model,
                                                           int stopsCnt,
                                                           double tt) {
        Map<Integer, Instant> forecasts = new HashMap<>();

        double lastTt = tt;
        Instant lastPredictedPass = stopItemPassTime;
        for (int i = stopOrderNum + 1; i <= stopsCnt; i++) {
            // TODO: получать из travelTimeStore для целого рейса сразу?
            final OptionalDouble ttPrevOpt = travelTimeStore.getTravelTime(roundId, i);
            final double ttNext;

            if (!ttPrevOpt.isPresent()) {
                LOGGER.debug("No tt_prev {} {}, fallback", roundId, i);

                forecasts.putAll(fallbackPredict(ttActionId, i, lastPredictedPass));

                return forecasts;
            } else {
                double ttPrev = ttPrevOpt.getAsDouble();
                // модель надо брать для предыдущей остановки, потому что мы "с неё" предсказываем
                final WekaRegression regression = model.getStopModels().get(i - 1);
                if (regression == null) {
                    LOGGER.debug("No stop model {} {}, fallback", roundId, i - 1);

                    forecasts.putAll(fallbackPredict(ttActionId, i, lastPredictedPass));

                    return forecasts;
                } else {
                    ttNext = regression.calculate(calcTimeWindow(lastPredictedPass, model),
                                                  lastTt,
                                                  ttPrev);
                }
            }

            lastPredictedPass = lastPredictedPass.plusMillis((long) (ttNext * 1000));
            forecasts.put(i, lastPredictedPass);
            lastTt = ttNext;
        }

        return forecasts;
    }

    private Map<Integer, Instant> fallbackPredict(long ttActionId,
                                                  int stopOrderNum,
                                                  Instant stopItemPass) {
        final SortedMap<Integer, StopItemOperTime> ttActionTt = new TreeMap<>(mapper.getTtActionTt(ttActionId));
        final Map<Integer, Instant> result = new HashMap<>();
        Instant lastStopItemPass = stopItemPass;

        for (Map.Entry<Integer, StopItemOperTime> entry : ttActionTt.tailMap(stopOrderNum).entrySet()) {
            lastStopItemPass = lastStopItemPass.plusMillis((long) (entry.getValue().getTt() * 1000));
            result.put(entry.getKey(), lastStopItemPass);
        }

        return result;
    }

    private int calcTimeWindow(Instant passTime, RoundModel model) {
        final int timeWindowWidth = model.getMetaData().getTimeWindowWidth();
        return passTime.atZone(ZoneOffset.UTC).toLocalDateTime().getHour() / timeWindowWidth;
    }
}
