package ru.advantum.csptm.service.forecaster.cmplx.ml;

import org.apache.ibatis.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.advantum.csptm.service.forecaster.cmplx.ml.weka.WekaRegression;
import ru.advantum.csptm.service.forecaster.cmplx.model.ModelMetaData;
import ru.advantum.csptm.service.forecaster.cmplx.model.RoundModel;
import ru.advantum.csptm.service.forecaster.cmplx.model.StopPassTrainData;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ModelTrainer {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModelTrainer.class);

    private final ModelTrainerMapper mapper;

    @Autowired
    private ModelTrainer(@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") ModelTrainerMapper mapper) {
        this.mapper = mapper;
    }

    public List<RoundModel> trainRouteModels(long routeId,
                                             LocalDate trainPeriodBegin,
                                             LocalDate trainPeriodEnd,
                                             ModelTrainerConfig config,
                                             int timeWindowWidth) {
        LOGGER.info("Getting train data {} {} {} {}", routeId, trainPeriodBegin, trainPeriodEnd, timeWindowWidth);
        final List<StopPassTrainData> trainData = mapper.getTrainData(routeId,
                                                                      trainPeriodBegin,
                                                                      trainPeriodEnd,
                                                                      timeWindowWidth,
                                                                      true);
        LOGGER.info("Got data {}", trainData.size());

        LOGGER.info("Starting to train {}", config);

        final Map<Long, Map<Integer, List<StopPassTrainData>>> groups = trainData.stream()
                .collect(Collectors.groupingBy(StopPassTrainData::getRoundId,
                                               Collectors.groupingBy(StopPassTrainData::getStopOrderNum)));

        final List<RoundModel> result = new ArrayList<>();

        for (Map.Entry<Long, Map<Integer, List<StopPassTrainData>>> group : groups.entrySet()) {
            LOGGER.info("Training round {}", group.getKey());
            Map<Integer, WekaRegression> stopModels = group.getValue().entrySet().stream()
                    .collect(Collectors.toMap(Map.Entry::getKey,
                                              s -> WekaRegression.train(config, s.getValue())));

            int trainSize = group.getValue().values().stream().mapToInt(List::size).sum();

            result.add(new RoundModel(group.getKey(),
                                      new ModelMetaData(Instant.now(),
                                                        trainSize,
                                                        config,
                                                        trainPeriodBegin,
                                                        trainPeriodEnd,
                                                        timeWindowWidth),
                                      stopModels));
        }

        return result;
    }

    @Mapper
    public interface ModelTrainerMapper {

        @Select("select *" +
                "from ibrd.cmplx_forecaster_ttb_pkg$get_training_dataset(#{route_id}::int," +
                "                                                        #{train_period_begin}::date," +
                "                                                        #{train_period_end}::date," +
                "                                                        #{time_window_width}::smallint," +
                "                                                        #{skip_nulls})")
        @ConstructorArgs({
                @Arg(column = "order_date", javaType = LocalDateTime.class),
                @Arg(column = "tr_id", javaType = long.class),
                @Arg(column = "tt_action_id", javaType = long.class),
                @Arg(column = "round_id", javaType = long.class),
                @Arg(column = "stop_order_num", javaType = int.class),
                @Arg(column = "time_fact", javaType = LocalDateTime.class),
                @Arg(column = "time_window", javaType = int.class),
                @Arg(column = "tt", javaType = double.class),
                @Arg(column = "tt_prev", javaType = Double.class),
                @Arg(column = "tt_next", javaType = Double.class)
        })
        List<StopPassTrainData> getTrainData(@Param("route_id") long routeId,
                                             @Param("train_period_begin") LocalDate trainPeriodBegin,
                                             @Param("train_period_end") LocalDate trainPeriodEnd,
                                             @Param("time_window_width") int timeWindowWidth,
                                             @Param("skip_nulls") boolean skipNulls);
    }
}
