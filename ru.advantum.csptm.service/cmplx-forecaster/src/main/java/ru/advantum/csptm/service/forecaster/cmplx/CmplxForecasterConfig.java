package ru.advantum.csptm.service.forecaster.cmplx;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.config.common.db.RedisClientConfig;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;
import ru.advantum.rabbitmq.config.RmqConsumerConfig;

@Root(name = "cmplx-forecaster")
public class CmplxForecasterConfig {

    @Element(name = "database")
    public final DatabaseConnection databaseConfig;

    @Element(name = "redis-client")
    public final RedisClientConfig redisConfig;

    @Element(name = "redis-state-location", required = false)
    public final String redisStateLocation;

    @Element(name = "rmq-service")
    public final JepRmqServiceConfig rmqServiceConfig;

    @Element(name = "model-train-consumer")
    public final RmqConsumerConfig consumer;

    public CmplxForecasterConfig() {
        this.databaseConfig = new DatabaseConnection();
        this.redisConfig = new RedisClientConfig();
        this.redisStateLocation = "forecaster";
        this.rmqServiceConfig = new JepRmqServiceConfig();
        this.consumer = new RmqConsumerConfig();
    }
}
