package ru.advantum.csptm.service.forecaster.cmplx.ml;

import lombok.Data;

@Data
public class ModelTrainerConfig {

    private final String classifier;
    private final String classifierOptions;
}
