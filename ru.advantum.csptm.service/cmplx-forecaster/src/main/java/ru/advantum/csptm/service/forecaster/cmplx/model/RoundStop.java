package ru.advantum.csptm.service.forecaster.cmplx.model;

import lombok.Data;

@Data
public class RoundStop {

    private final long roundId;
    private final int stopOrderNum;
    private final long stopItemId;
}
