package ru.advantum.csptm.service.forecaster.cmplx.config;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import ru.advantum.config.common.db.RedisClientConfig;
import ru.advantum.csptm.service.forecaster.cmplx.CmplxForecasterConfig;

import java.util.Map;

@Configuration
public class RedisConfig {

    @Bean
    public JedisConnectionFactory jedisConnectionFactory(CmplxForecasterConfig config) {
        RedisClientConfig redisClientConfig = config.redisConfig;
        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
        jedisConnectionFactory.setHostName(redisClientConfig.host);
        jedisConnectionFactory.setPort(redisClientConfig.port);
        jedisConnectionFactory.setTimeout(redisClientConfig.timeout);
        jedisConnectionFactory.setPassword(redisClientConfig.password);
        return jedisConnectionFactory;
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public <K, V> RedisTemplate<String, Map<K, V>> redisTemplate(JedisConnectionFactory jedisConnectionFactory) {
        RedisTemplate<String, Map<K, V>> redisTemplate = new RedisTemplate<>();
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setConnectionFactory(jedisConnectionFactory);
        return redisTemplate;
    }
}
