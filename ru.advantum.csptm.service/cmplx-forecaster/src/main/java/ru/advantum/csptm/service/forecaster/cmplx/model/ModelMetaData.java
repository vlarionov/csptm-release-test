package ru.advantum.csptm.service.forecaster.cmplx.model;

import lombok.Data;
import ru.advantum.csptm.service.forecaster.cmplx.ml.ModelTrainerConfig;

import java.time.Instant;
import java.time.LocalDate;

@Data
public class ModelMetaData {

    private final Instant trainTime;
    private final int trainSize;
    private final ModelTrainerConfig modelTrainerConfig;
    private final LocalDate trainPeriodBegin;
    private final LocalDate trainPeriodEnd;
    private final int timeWindowWidth;
}
