package ru.advantum.csptm.service.forecaster.cmplx.service;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.advantum.csptm.jep.service.glinker.model.event.StopItemPass;
import ru.advantum.csptm.service.forecaster.cmplx.redis.RedisMapDumper;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.temporal.ChronoUnit;
import java.util.OptionalDouble;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Component
public class TravelTimeStore {

    private static final String LAST_STOP_TT_KEY = "last-stop-tt";
    private static final String LAST_TR_PASS_KEY = "last-tr-pass";

    private static final Logger LOGGER = LoggerFactory.getLogger(TravelTimeStore.class);

    private final RedisMapDumper<RoundStopItem, Double> lastStopTtDumper;
    private final RedisMapDumper<Long, StopItemPass> lastTrPassesDumper;
    // TODO: на вложенной мапе, наверное, удобнее будет все-таки
    private final ConcurrentMap<RoundStopItem, Double> lastStopTt = new ConcurrentHashMap<>();
    private final ConcurrentMap<Long, StopItemPass> lastTrPasses = new ConcurrentHashMap<>();

    @Autowired
    public TravelTimeStore(@Value("#{applicationConfig.redisStateLocation}") String redisStateLocation,
                           RedisMapDumper<RoundStopItem, Double> lastStopTtDumper,
                           RedisMapDumper<Long, StopItemPass> lastTrPassesDumper) {
        this.lastStopTtDumper = lastStopTtDumper;
        this.lastTrPassesDumper = lastTrPassesDumper;
        lastStopTtDumper.bindMap(lastStopTt, String.format("%s:%s", redisStateLocation, LAST_STOP_TT_KEY), RoundStopItem.class, Double.class);
        lastTrPassesDumper.bindMap(lastTrPasses, String.format("%s:%s", redisStateLocation, LAST_TR_PASS_KEY), Long.class, StopItemPass.class);
    }

    @PostConstruct
    public void start() {
        lastStopTtDumper.restoreState();
        lastTrPassesDumper.restoreState();
        LOGGER.info("Restored state lastStopTt {} lastTrPass {}", lastStopTt.size(), lastTrPasses.size());
    }

    private boolean passesMatch(StopItemPass spp1, StopItemPass spp2) {
        return spp1.getTtActionId() == spp2.getTtActionId()
                && spp1.getStopOrderNum() == spp2.getStopOrderNum() - 1;
    }

    public OptionalDouble newStopPass(StopItemPass newStopItemPass) {
        final StopItemPass oldStopItemPass = lastTrPasses.put(newStopItemPass.getTrId(), newStopItemPass);
        if (oldStopItemPass != null && passesMatch(oldStopItemPass, newStopItemPass)) {
            double tt = ChronoUnit.MILLIS.between(oldStopItemPass.getPassTime(),
                                                  newStopItemPass.getPassTime()) / 1000.0;
            lastStopTt.put(RoundStopItem.from(newStopItemPass), tt);

            return OptionalDouble.of(tt);
        } else {
            return OptionalDouble.empty();
        }
    }

    public OptionalDouble getTravelTime(long roundId, int stopOrderNum) {
        RoundStopItem rts = new RoundStopItem(roundId, stopOrderNum);
        final Double tt = lastStopTt.get(rts);
        if (tt == null) {
            return OptionalDouble.empty();
        } else {
            return OptionalDouble.of(tt);
        }
    }

    @Scheduled(initialDelay = 60 * 1000, fixedDelay = 60 * 1000)
    @PreDestroy
    public void dumpState() {
        LOGGER.info("Dumping lastStopTt {}", lastStopTt.size());
        lastStopTtDumper.dumpState();
        LOGGER.info("Dumping lastTrPasses {}", lastTrPasses.size());
        lastTrPassesDumper.dumpState();
    }

    @Data
    private static class RoundStopItem {

        private final long roundId;
        private final int stopOrderNum;

        public static RoundStopItem from(StopItemPass stopItemPass) {
            return new RoundStopItem(stopItemPass.getRoundId(), stopItemPass.getStopOrderNum());
        }
    }
}
