package ru.advantum.csptm.service.forecaster.cmplx.model;

import lombok.Data;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Data
public class StopItemOperTime {

    private final int stopOrderNum;
    private final long stopItemId;
    private final Instant timeOper;
    private final double tt;

    public StopItemOperTime(int stopOrderNum, long stopItemId, LocalDateTime timeOper, double tt) {
        this.stopOrderNum = stopOrderNum;
        this.stopItemId = stopItemId;
        this.timeOper = timeOper.toInstant(ZoneOffset.UTC);
        this.tt = tt;
    }
}
