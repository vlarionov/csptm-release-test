package ru.advantum.csptm.service.forecaster.cmplx.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;
import ru.advantum.csptm.service.forecaster.cmplx.config.CacheConfig;
import ru.advantum.csptm.service.forecaster.cmplx.ml.ModelTrainerConfig;
import ru.advantum.csptm.service.forecaster.cmplx.ml.weka.WekaRegression;
import ru.advantum.csptm.service.forecaster.cmplx.model.ModelMetaData;
import ru.advantum.csptm.service.forecaster.cmplx.model.RoundModel;
import ru.advantum.csptm.service.forecaster.cmplx.redis.GsonRedisSerializer;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class ModelStore {

    private static final String CACHE_NAME = "forecasterModel";
    private static final String MODEL_STORE_KEY = "models";
    private static final Logger LOGGER = LoggerFactory.getLogger(ModelStore.class);

    private final BoundHashOperations<String, Long, RoundModelDTO> hashOperations;

    @Autowired
    public ModelStore(@Value("#{applicationConfig.redisStateLocation}") String redisStateLocation,
                      JedisConnectionFactory jedisConnectionFactory) {
        RedisTemplate<String, Map<Long, RoundModelDTO>> redisTemplate = new RedisTemplate<>();
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashKeySerializer(new GsonRedisSerializer<>(Long.class));
        redisTemplate.setHashValueSerializer(new GsonRedisSerializer<>(RoundModelDTO.class));
        redisTemplate.setConnectionFactory(jedisConnectionFactory);
        redisTemplate.afterPropertiesSet();

        hashOperations = redisTemplate.boundHashOps(String.format("%s:%s", redisStateLocation, MODEL_STORE_KEY));
    }

    @Cacheable(value = CACHE_NAME, cacheManager = CacheConfig.LONG_CACHE_MANAGER)
    public Optional<RoundModel> getModel(long roundId) {
        LOGGER.debug("Loading round model from redis {}", roundId);
        return Optional.ofNullable(hashOperations.get(roundId)).map(RoundModelDTO::asModel);
    }

    @CacheEvict(value = CACHE_NAME, cacheManager = CacheConfig.LONG_CACHE_MANAGER, key = "#p0.getRoundId()")
    public void storeModel(RoundModel model) {
        hashOperations.put(model.getRoundId(), new RoundModelDTO(model));
    }

    private static class ModelMetaDataDTO {

        private final Instant trainTime;
        private final int trainSize;
        private final String classifier;
        private final String classifierOptions;
        private final LocalDate trainPeriodBegin;
        private final LocalDate trainPeriodEnd;
        private final int timeWindowWidth;

        // для десериализации
        @SuppressWarnings("unused")
        private ModelMetaDataDTO() {
            this.trainTime = null;
            this.trainSize = 0;
            this.classifier = null;
            this.classifierOptions = null;
            this.trainPeriodBegin = null;
            this.trainPeriodEnd = null;
            this.timeWindowWidth = 0;
        }

        public ModelMetaDataDTO(ModelMetaData metaData) {
            this.trainTime = metaData.getTrainTime();
            this.trainSize = metaData.getTrainSize();
            this.classifier = metaData.getModelTrainerConfig().getClassifier();
            this.classifierOptions = metaData.getModelTrainerConfig().getClassifierOptions();
            this.trainPeriodBegin = metaData.getTrainPeriodBegin();
            this.trainPeriodEnd = metaData.getTrainPeriodEnd();
            this.timeWindowWidth = metaData.getTimeWindowWidth();
        }

        public ModelMetaData asModelMetaData() {
            return new ModelMetaData(trainTime,
                                     trainSize,
                                     new ModelTrainerConfig(classifier, classifierOptions),
                                     trainPeriodBegin,
                                     trainPeriodEnd,
                                     timeWindowWidth);
        }
    }

    private static class RoundModelDTO {

        private final long roundId;
        private final ModelMetaDataDTO metaData;
        private final Map<Integer, String> stopModels;

        // для десериализации
        @SuppressWarnings("unused")
        private RoundModelDTO() {
            roundId = 0;
            metaData = null;
            stopModels = null;
        }

        public RoundModelDTO(RoundModel model) {
            this.roundId = model.getRoundId();
            this.metaData = new ModelMetaDataDTO(model.getMetaData());
            this.stopModels = model.getStopModels().entrySet().stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().toBinaryData()));
        }

        public RoundModel asModel() {
            return new RoundModel(roundId,
                                  metaData.asModelMetaData(),
                                  stopModels.entrySet().stream()
                                          .collect(Collectors.toMap(Map.Entry::getKey,
                                                                    e -> WekaRegression.from(e.getValue()))));
        }
    }
}
