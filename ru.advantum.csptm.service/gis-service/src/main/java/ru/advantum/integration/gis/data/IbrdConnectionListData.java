package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

class IbrdConnectionData {

    @XmlElement(name = "ibrd_muid")
    public long ibrdMuid;

    @XmlElement(name = "code")
    public String code;

    @XmlElement(name = "ibrd_connection")
    public int ibrdConnection;

    @XmlElement(name = "date_connection")
    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public LocalDateTime dateConnection;
}

/**
 * @author kaganov
 */
@XmlRootElement(name = "IbrdConnections")
public class IbrdConnectionListData {

    @XmlElement(name = "IbrdConnection")
    public List<IbrdConnectionData> list = new ArrayList<>();
}
