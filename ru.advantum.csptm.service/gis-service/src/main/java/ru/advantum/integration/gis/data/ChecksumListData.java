package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

class ChecksumData {

    @XmlElement(name = "cs_tablename")
    public String tableName;

    @XmlElement(name = "cs_checksum")
    public Long checksum;
}

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "ChecksumList")
public class ChecksumListData {

    @XmlElement(name = "Checksum")
    public List<ChecksumData> list = new ArrayList<>();
}
