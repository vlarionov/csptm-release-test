package ru.advantum.integration.gis.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SensorTermo {

    @JsonProperty("vehicle_id")
    public Long vehicleID;

    @JsonProperty("gos_nomer")
    public String gosNomer;

    @JsonProperty("depot_number")
    public String depotNumber;

    @JsonProperty("sensor_status")
    public Integer sensorStatus;

    @JsonProperty("sensor_termo")
    public Integer sensorTermo;

    @JsonProperty("datetime")
    public String eventdateTime;
}
