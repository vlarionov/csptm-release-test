package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "MarshPsgAvgValue")
public class MarshPsgAvgValueData {

    @XmlElement(name = "marsh_avg_value")
    public Double value;
}
