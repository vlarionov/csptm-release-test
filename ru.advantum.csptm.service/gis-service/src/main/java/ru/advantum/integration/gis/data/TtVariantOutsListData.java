package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

class TtVariantOutsData {

    /** Номер выхода */
    @XmlElement(name = "out_num")
    public Integer outNum;

    /** Количество смен */
    @XmlElement(name = "sm_count")
    public Integer smCount;

    /** Краткое наименование вместимости ТС */
    @XmlElement(name = "capacity_short_name")
    public String capacityShortName;
}

/**
 * Данные о выходах в привязке к варианту расписания
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "sh_variant_outs")
public class TtVariantOutsListData {

    /** ID варианта расписания */
    @XmlElement(name = "sh_id")
    public Long ttVariantID;

    /** Выходы */
    @XmlElement(name = "outs")
    public List<TtVariantOutsData> list = new ArrayList<>();
}
