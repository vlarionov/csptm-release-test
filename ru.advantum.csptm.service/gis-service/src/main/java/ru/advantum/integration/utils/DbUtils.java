package ru.advantum.integration.utils;

import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDateTime;

/**
 * @author abc <alekseenkov@advantum.pro>
 */
public final class DbUtils {

    // =================================================
    // Set Parameters

    public static void setInteger(PreparedStatement statement, int index, Integer value) throws SQLException {
        if (value == null)
            statement.setNull(index, Types.INTEGER);
        else
            statement.setInt(index, value);
    }

    public static void setLong(PreparedStatement statement, int index, Long value) throws SQLException {
        if (value == null)
            statement.setNull(index, Types.BIGINT);
        else
            statement.setLong(index, value);
    }

    public static void setBigDecimal(PreparedStatement statement, int index, BigDecimal value) throws SQLException {
        if (value == null)
            statement.setNull(index, Types.NUMERIC);
        else
            statement.setBigDecimal(index, value);
    }

    public static void setArray(PreparedStatement statement, int index, Array value) throws SQLException {
        if (value == null)
            statement.setNull(index, Types.ARRAY);
        else
            statement.setArray(index, value);
    }

    public static void setFloat(PreparedStatement statement, int index, Float value) throws SQLException {
        if (value == null)
            statement.setNull(index, Types.FLOAT);
        else
            statement.setFloat(index, value);
    }

    public static void setString(PreparedStatement statement, int index, String value) throws SQLException {
        if (value == null)
            statement.setNull(index, Types.VARCHAR);
        else
            statement.setString(index, value);
    }

    public static void setTimestamp(PreparedStatement statement, int index, java.sql.Timestamp value) throws SQLException {
        if (value == null)
            statement.setNull(index, Types.TIMESTAMP);
        else
            statement.setTimestamp(index, value);
    }

    public static void setLocalDateTime(PreparedStatement statement, int index, LocalDateTime value) throws SQLException {

        setTimestamp(statement, index, TypeConvert.toTimestamp(value));
    }

    // =================================================
    // Get Parameters

    public static Integer getInteger(CallableStatement rs, int parameterIndex) throws SQLException {
        Integer result = rs.getInt(parameterIndex);
        if (rs.wasNull()) result = null;
        return result;
    }

    public static Long getLong(CallableStatement rs, int parameterIndex) throws SQLException {
        Long result = rs.getLong(parameterIndex);
        if (rs.wasNull()) result = null;
        return result;
    }

    // ...

    // =================================================
    // Get ResultSet

    public static Integer getInteger(ResultSet rs, int columnIndex) throws SQLException {
        Integer result = rs.getInt(columnIndex);
        if (rs.wasNull()) result = null;
        return result;
    }

    public static Integer getInteger(ResultSet rs, String columnLabel) throws SQLException {
        int i = rs.findColumn(columnLabel);
        return getInteger(rs, i);
    }

    public static Long getLong(ResultSet rs, int columnIndex) throws SQLException {
        Long result = rs.getLong(columnIndex);
        if (rs.wasNull()) result = null;
        return result;
    }

    public static Long getLong(ResultSet rs, String columnLabel) throws SQLException {
        int i = rs.findColumn(columnLabel);
        return getLong(rs, i);
    }

    public static BigDecimal getBigDecimal(ResultSet rs, int columnIndex) throws SQLException {
        BigDecimal result = rs.getBigDecimal(columnIndex);
        if (rs.wasNull()) result = null;
        return result;
    }

    public static BigDecimal getBigDecimal(ResultSet rs, String columnLabel) throws SQLException {
        int i = rs.findColumn(columnLabel);
        return getBigDecimal(rs, i);
    }

    public static String getString(ResultSet rs, int columnIndex, boolean nullsAsEmpty) throws SQLException {
        String result = rs.getString(columnIndex);
        if (rs.wasNull()) result = null;

        if (nullsAsEmpty && result == null) result = "";
        return result;
    }

    public static String getString(ResultSet rs, String columnLabel, boolean nullsAsEmpty) throws SQLException {
        int i = rs.findColumn(columnLabel);
        return getString(rs, i, nullsAsEmpty);
    }

    public static String getString(ResultSet rs, int columnIndex) throws SQLException {
        return getString(rs, columnIndex, false);
    }

    public static String getString(ResultSet rs, String columnLabel) throws SQLException {
        return getString(rs, columnLabel, false);
    }

    public static Timestamp getTimestamp(ResultSet rs, int columnIndex) throws SQLException {
        Timestamp result = rs.getTimestamp(columnIndex);
        if (rs.wasNull()) result = null;
        return result;
    }

    public static Timestamp getTimestamp(ResultSet rs, String columnLabel) throws SQLException {
        int i = rs.findColumn(columnLabel);
        return getTimestamp(rs, i);
    }

    public static LocalDateTime getLocalDateTime(ResultSet rs, int columnIndex) throws SQLException {
        Timestamp t = getTimestamp(rs, columnIndex);
        if (t == null) return null;
        return t.toLocalDateTime();
    }

    public static LocalDateTime getLocalDateTime(ResultSet rs, String columnLabel) throws SQLException {
        Timestamp t = getTimestamp(rs, columnLabel);
        if (t == null) return null;
        return t.toLocalDateTime();
    }

    public static Boolean getBoolean(ResultSet rs, int columnIndex) throws SQLException {
        Boolean result = rs.getBoolean(columnIndex);
        if (rs.wasNull()) result = null;
        return result;
    }

    public static Boolean getBoolean(ResultSet rs, String columnLabel) throws SQLException {
        int i = rs.findColumn(columnLabel);
        return getBoolean(rs, i);
    }

    public static Double getDouble(ResultSet rs, int columnIndex) throws SQLException {
        Double result = rs.getDouble(columnIndex);
        if (rs.wasNull()) result = null;
        return result;
    }

    public static Double getDouble(ResultSet rs, String columnLabel) throws SQLException {
        int i = rs.findColumn(columnLabel);
        return getDouble(rs, i);
    }
}
