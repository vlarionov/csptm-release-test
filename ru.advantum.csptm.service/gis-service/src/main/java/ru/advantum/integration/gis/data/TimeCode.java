package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;

public class TimeCode {
    @XmlElement(name = "time_code")
    public String timeCode;

    @XmlElement(name = "time_name")
    public String timeName;

    @XmlElement(name = "avg_pas")
    public String avgPas;

    @XmlElement(name = "avg_pas_in")
    public String avgPasIn;

    @XmlElement(name = "avg_pas_out")
    public String avgPasOut;
}

