package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

class NavPointData {

    @XmlElement(name = "id")
    public Long pointID;

    @XmlElement(name = "point_date")
    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public LocalDateTime pointDate;

    @XmlElement(name = "lon")
    public Double pointLon;

    @XmlElement(name = "lat")
    public Double pointLat;

    @XmlElement(name = "heading")
    public Long heading;

    @XmlElement(name = "point_num")
    public Long pointNum;

    @XmlElement(name = "point_tag")
    public Long pointTag;

    @XmlElement(name = "route_id")
    public Long routeID;

    @XmlElement(name = "race_id")
    public Long raceID;

    @XmlElement(name = "stop_id")
    public Long stopID;

    @XmlElement(name = "route_trajectory_id")
    public Long routeTrajectoryID;

    @XmlElement(name = "gr_num")
    public String grNum;

    @XmlElement(name = "gos_num")
    public String gosNum;

    @XmlElement(name = "speed")
    public Integer speed;
}

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "NavPointList")
public class NavPointListData {

    @XmlElement(name = "NavPoint")
    public List<NavPointData> list = new ArrayList<>();
}
