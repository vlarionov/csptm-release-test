package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

class RaspVariantsData {

    @XmlElement(name = "srv_id")
    public Long serverID;

    @XmlElement(name = "rv_id")
    public Long raspVariantID;

    @XmlElement(name = "mr_id")
    public Long routeID;

    @XmlElement(name = "rv_dow")
    public Integer week_mask;

    @XmlElement(name = "rv_season")
    public String marshVariantSeason;

    @XmlElement(name = "rv_startdate")
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDateTime dateBegin;

    @XmlElement(name = "rv_enddate")
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDateTime dateEnd;

    @XmlElement(name = "rv_enddateexists")
    public Integer flagEndDateExists;

    @XmlElement(name = "rv_num")
    public Long raspVariantNum;

    @XmlElement(name = "rv_checksum")
    public Long checkSum;
}

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "RaspVariants")
public class RaspVariantsListData {

    @XmlElement(name = "RaspVariant")
    public List<RaspVariantsData> list = new ArrayList<>();
}
