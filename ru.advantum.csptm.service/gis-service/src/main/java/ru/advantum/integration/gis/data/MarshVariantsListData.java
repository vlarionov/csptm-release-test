package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

class MarshVariantsData {

    @XmlElement(name = "mv_id")
    public Long marshVariantID;

    @XmlElement(name = "mr_id")
    public Long routeID;

    @XmlElement(name = "mv_desc")
    public String marshVariantDesc;

    @XmlElement(name = "mv_startdate")
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDateTime dateBegin;

    @XmlElement(name = "mv_enddate")
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDateTime dateEnd;

    @XmlElement(name = "mv_enddateexists")
    public Integer flagEndDateExists;

    @XmlElement(name = "mv_checksum")
    public Long checkSum;
}

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "MarshVariants")
public class MarshVariantsListData {

    @XmlElement(name = "MarshVariant")
    public List<MarshVariantsData> list = new ArrayList<>();
}
