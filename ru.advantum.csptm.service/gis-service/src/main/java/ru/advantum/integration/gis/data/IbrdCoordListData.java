package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

class IbrdCoordData {

    @XmlAttribute(name = "ibrd_muid")
    public Long ibrdMuid;

    @XmlAttribute(name = "received_date")
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDateTime receivedDate;

    @XmlAttribute(name = "is_valid")
    public Boolean isValid;

    @XmlAttribute(name = "lon")
    public Double longitude;

    @XmlAttribute(name = "lat")
    public Double latitude;
}

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "ibrd_telematic_list")
public class IbrdCoordListData {

    @XmlElement(name = "ibrd_telematic")
    public List<IbrdCoordData> list = new ArrayList<>();
}
