package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

class DriverModeData {

    @XmlElement(name = "dm_id")
    public Long driverModeID;

    @XmlElement(name = "tt_id")
    public Long transportTypeID;

    @XmlElement(name = "dm_title")
    public String driverModeName;

    @XmlElement(name = "dm_digit")
    public Integer driverModeDigit;

    @XmlElement(name = "dm_sm")
    public Integer driverModeSmenaNum;
}

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "DriverModes")
public class DriverModeListData {

    @XmlElement(name = "DriverMode")
    public List<DriverModeData> list = new ArrayList<>();
}
