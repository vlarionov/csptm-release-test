package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

class TransportTypeData {

    @XmlElement(name = "tt_id")
    public Integer id;

    @XmlElement(name = "tt_title")
    public String name;

    @XmlElement(name = "tt_note")
    public String note;
}

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "TransportTypes")
public class TransportTypeListData {

    @XmlElement(name = "TransportType")
    public List<TransportTypeData> list = new ArrayList<>();
}
