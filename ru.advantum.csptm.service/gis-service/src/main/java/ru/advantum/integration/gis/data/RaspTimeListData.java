package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

class RaspTimeData {

    @XmlElement(name = "srv_id")
    public Long serverID;

    @XmlElement(name = "rv_id")
    public Long raspVariantID;

    @XmlElement(name = "gr_id")
    public Long graphShiftID;

    @XmlElement(name = "rt_orderby")
    public Long orderBy;

    @XmlElement(name = "rt_graph")
    public Integer graph;

    @XmlElement(name = "rt_smena")
    public String smena;

    @XmlElement(name = "rl_racetype")
    public String raceType;

    @XmlElement(name = "rl_racetypeext")
    public String raceTypeExt;

    @XmlElement(name = "st_id")
    public Long stopID;

    @XmlElement(name = "rt_time")
    public Integer time;

    @XmlElement(name = "rt_inv")
    public Integer flag_inv;

    @XmlElement(name = "rt_racenum")
    public Integer raceNum;

    @XmlElement(name = "rc_kkp")
    public String flagStartFinish;
}

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "RaspTimeList")
public class RaspTimeListData {

    @XmlElement(name = "RaspTime")
    public List<RaspTimeData> list = new ArrayList<>();
}
