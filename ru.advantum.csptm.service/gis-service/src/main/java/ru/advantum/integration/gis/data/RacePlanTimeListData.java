package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

class RacePlanTimeData {

    @XmlElement(name = "mv_id")
    public Long marshVariantID;

    @XmlElement(name = "rl_racetype")
    public String raceType;

    @XmlElement(name = "rl_racetypeext")
    public String raceTypeExt;

    @XmlElement(name = "rpt_avg_time")
    public Double avgTime;

    @XmlElement(name = "rpt_expl_speed")
    public Double explSpeed;
}

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "RacePalnTimeList")
public class RacePlanTimeListData {

    @XmlElement(name = "RacePalnTime")
    public List<RacePlanTimeData> list = new ArrayList<>();
}
