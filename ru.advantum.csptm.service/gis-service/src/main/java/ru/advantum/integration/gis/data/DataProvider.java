package ru.advantum.integration.gis.data;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.integration.gis.config.GisServiceConfig;
import ru.advantum.integration.utils.DbUtils;
import ru.advantum.integration.utils.TypeConvert;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class DataProvider {

    private static final int DEFAULT_FETCH_SIZE = 1000;

    private static final Logger LOG = LoggerFactory.getLogger(DataProvider.class);
    private static DataProvider instance = null;
    private final DataSource dataSource;
    private String dbSchemaName = "";
    private final ObjectMapper mapper;

    @Autowired
    public DataProvider(DataSource dataSource, GisServiceConfig config, ObjectMapper mapper) {
        this.dataSource = dataSource;
        this.dbSchemaName = config.getDbSchema();
        this.mapper = mapper;
    }

    public TransportTypeListData getTransportTypeListData(String strTransportTypesID) {
        TransportTypeListData result = new TransportTypeListData();

        Integer transportTypesID = TypeConvert.toInteger(strTransportTypesID);

        String sql = String.format("select tt_id, tt_title, tt_note from %s.v_transport_type " +
            "                    where ( ?::bigint is null or tt_id = ?::bigint )", dbSchemaName);

        try (Connection connection = dataSource.getConnection();
             PreparedStatement s = connection.prepareStatement(sql)) {

            DbUtils.setInteger(s, 1, transportTypesID);
            DbUtils.setInteger(s, 2, transportTypesID);

            try (ResultSet results = s.executeQuery()) {
                while (results.next()) {
                    TransportTypeData t = new TransportTypeData();
                    t.id = DbUtils.getInteger(results, "tt_id");
                    t.name = DbUtils.getString(results, "tt_title");
                    t.note = DbUtils.getString(results, "tt_note");
                    result.list.add(t);
                }
            }
        } catch (SQLException e) {
            LOG.error("getTransportTypeListData " + e.getMessage());
        }
        return result;
    }

    public RaspHolidaysListData getRaspHolidaysListData(String strRouteID, String strHolydayDate) {
        RaspHolidaysListData result = new RaspHolidaysListData();

        Integer routeID = TypeConvert.toInteger(strRouteID);
        LocalDateTime holydayDate = TypeConvert.toLocalDateTime(strHolydayDate);

        String sql = String.format("select mr_id, rh_date, rh_dow from %s.v_rasp_holidays " +
                "      where ( ?::bigint is null or mr_id = ?::bigint ) " +
                "        and ( ?::timestamp is null or rh_date = ?::timestamp) ",
            dbSchemaName);

        try (Connection connection = dataSource.getConnection();
             PreparedStatement s = connection.prepareStatement(sql)) {

            DbUtils.setInteger(s, 1, routeID);
            DbUtils.setInteger(s, 2, routeID);
            DbUtils.setLocalDateTime(s, 3, holydayDate);
            DbUtils.setLocalDateTime(s, 4, holydayDate);

            try (ResultSet results = s.executeQuery()) {
                while (results.next()) {
                    RaspHolidaysData t = new RaspHolidaysData();
                    t.route_id = DbUtils.getInteger(results, "mr_id");
                    t.holidaysDate = DbUtils.getLocalDateTime(results, "rh_date");
                    t.week_mask = DbUtils.getInteger(results, "rh_dow");
                    result.list.add(t);
                }
            }
        } catch (SQLException e) {
            LOG.error("getRaspHolidaysListData " + e.getMessage());
        }
        return result;
    }

    public ChecksumListData getChecksumListData(String tableName) {
        ChecksumListData result = new ChecksumListData();

        String sql = String.format("select cs_table_name, cs_check_sum from %s.v_table_checksum " +
            "                    where ( ?::text is null or cs_table_name = ?::text )", dbSchemaName);

        try (Connection connection = dataSource.getConnection();
             PreparedStatement s = connection.prepareStatement(sql)) {

            DbUtils.setString(s, 1, tableName);
            DbUtils.setString(s, 2, tableName);

            try (ResultSet results = s.executeQuery()) {
                while (results.next()) {
                    ChecksumData t = new ChecksumData();
                    t.tableName = DbUtils.getString(results, "cs_table_name");
                    t.checksum = DbUtils.getLong(results, "cs_check_sum");
                    result.list.add(t);
                }
            }
        } catch (SQLException e) {
            LOG.error("getChecksumListData " + e.getMessage());
        }
        return result;
    }

//    public EventsListData getEventsListData() {
//        EventsListData result = new EventsListData();
//
//        String sql = String.format("select ev_id, ev_title from %s.v_events", dbSchemaName);
//
//        try (Connection connection = dataSource.getConnection();
//             PreparedStatement s = connection.prepareStatement(sql)) {
//
//            try (ResultSet results = s.executeQuery()) {
//                while (results.next()) {
//                    EventsData t = new EventsData();
//                    t.event_id = DbUtils.getInteger(results, "ev_id");
//                    t.event_name = DbUtils.getString(results, "ev_title");
//                    result.list.add(t);
//                }
//            }
//        } catch (SQLException e) {
//            LOG.error("getEventsListData " + e.getMessage());
//        }
//        return result;
//    }

    public ActionTypeListData getActionTypeData() {
        ActionTypeListData result = new ActionTypeListData();
        try (Connection connection = dataSource.getConnection()) {
            boolean previousAutoCommit = connection.getAutoCommit();
            try {
                connection.setAutoCommit(false);
                String sql = String.format("{ ? = call %s.gis_export_pkg$get_action_type() }", dbSchemaName);
                try (CallableStatement call = connection.prepareCall(sql)) {
                    call.setFetchSize(DEFAULT_FETCH_SIZE);
                    call.registerOutParameter(1, Types.OTHER);

                    call.execute();
                    try (ResultSet results = (ResultSet) call.getObject(1)) {
                        while (results.next()) {
                            ActionTypeData o = new ActionTypeData();
                            result.list.add(o);
                            o.eventID = DbUtils.getInteger(results, "action_type_id");
                            o.parentEventID = DbUtils.getInteger(results, "parent_type_id");
                            o.eventName = DbUtils.getString(results, "action_type_name");
                        }
                    }
                }
            } finally {
                connection.setAutoCommit(previousAutoCommit);
            }
        } catch (SQLException e) {
            LOG.error("getActionTypeData", e);
        }
        return result;
    }

    public MarshVariantsListData getMarshVariantsListData(String strMarshVariantID) {
        MarshVariantsListData result = new MarshVariantsListData();

        Integer marshVariantID = TypeConvert.toInteger(strMarshVariantID);

        String sql = String.format("select mv_id, mr_id, mv_checksum, mv_startdate, mv_enddate, mv_enddateexists, mv_desc" +
            "                     from %s.v_marsh_variants " +
            "                    where ( ?::bigint is null or mv_id = ?::bigint )", dbSchemaName);

        try (Connection connection = dataSource.getConnection();
             PreparedStatement s = connection.prepareStatement(sql)) {

            DbUtils.setInteger(s, 1, marshVariantID);
            DbUtils.setInteger(s, 2, marshVariantID);

            try (ResultSet results = s.executeQuery()) {
                while (results.next()) {
                    MarshVariantsData t = new MarshVariantsData();
                    t.marshVariantID = DbUtils.getLong(results, "mv_id");
                    t.routeID = DbUtils.getLong(results, "mr_id");
                    t.marshVariantDesc = DbUtils.getString(results, "mv_desc");
                    t.dateBegin = DbUtils.getLocalDateTime(results, "mv_startdate");
                    t.dateEnd = DbUtils.getLocalDateTime(results, "mv_enddate");
                    t.flagEndDateExists = DbUtils.getInteger(results, "mv_enddateexists");
                    t.checkSum = DbUtils.getLong(results, "mv_checksum");
                    result.list.add(t);
                }
            }
        } catch (SQLException e) {
            LOG.error("getMarshVariantsListData " + e.getMessage());
        }
        return result;
    }

    public RaceListData getRaceListData(String strMarshVariantID) {
        RaceListData result = new RaceListData();

        Integer marshVariantID = TypeConvert.toInteger(strMarshVariantID);

        String sql = String.format("select mv_id, rl_racetype, rl_racetypeext, rl_firststation_id, rl_laststation_id" +
            "                     from %s.v_race_list " +
            "                    where ( ?::bigint is null or mv_id = ?::bigint )", dbSchemaName);

        try (Connection connection = dataSource.getConnection();
             PreparedStatement s = connection.prepareStatement(sql)) {

            DbUtils.setInteger(s, 1, marshVariantID);
            DbUtils.setInteger(s, 2, marshVariantID);

            try (ResultSet results = s.executeQuery()) {
                while (results.next()) {
                    RaceData t = new RaceData();
                    t.marshVariantID = DbUtils.getLong(results, "mv_id");
                    t.raceType = DbUtils.getString(results, "rl_racetype");
                    t.raceTypeExt = DbUtils.getString(results, "rl_racetypeext");
                    t.firstStopID = DbUtils.getLong(results, "rl_firststation_id");
                    t.lastsStopID = DbUtils.getLong(results, "rl_laststation_id");
                    result.list.add(t);
                }
            }
        } catch (SQLException e) {
            LOG.error("getRaceListData " + e.getMessage());
        }
        return result;
    }

    public RaceCardListData getRaceCardListData(String strMarshVariantID) {
        RaceCardListData result = new RaceCardListData();

        Integer marshVariantID = TypeConvert.toInteger(strMarshVariantID);

        String sql = String.format("select mv_id, rl_racetype, rl_racetypeext, rc_orderby, st_id, rc_kkp, rc_distance" +
            "                     from %s.v_race_card " +
            "                    where ( ?::bigint is null or mv_id = ?::bigint )", dbSchemaName);

        try (Connection connection = dataSource.getConnection();
             PreparedStatement s = connection.prepareStatement(sql)) {

            DbUtils.setInteger(s, 1, marshVariantID);
            DbUtils.setInteger(s, 2, marshVariantID);

            try (ResultSet results = s.executeQuery()) {
                while (results.next()) {
                    RaceCardData t = new RaceCardData();
                    t.marshVariantID = DbUtils.getLong(results, "mv_id");
                    t.raceType = DbUtils.getString(results, "rl_racetype");
                    t.raceTypeExt = DbUtils.getString(results, "rl_racetypeext");
                    t.orderBy = DbUtils.getLong(results, "rc_orderby");
                    t.stopID = DbUtils.getLong(results, "st_id");
                    t.flagStartFinish = DbUtils.getString(results, "rc_kkp");
                    t.distance = DbUtils.getDouble(results, "rc_distance");
                    result.list.add(t);
                }
            }
        } catch (SQLException e) {
            LOG.error("getRaceListData " + e.getMessage());
        }
        return result;
    }

    public RaspVariantsListData getRaspVariantsListData(String strServerID, String strRaspVsriantID) {
        RaspVariantsListData result = new RaspVariantsListData();

        Integer serverID = TypeConvert.toInteger(strServerID);
        Integer raspVsriantID = TypeConvert.toInteger(strRaspVsriantID);

        String sql = String.format("select srv_id, rv_id, mr_id, rv_dow, rv_season, rv_startdate, rv_enddate, rv_enddateexists, rv_num, rv_checksum" +
                "             from %s.v_rasp_variants " +
                "            where ( ?::bigint is null or srv_id = ?::bigint ) " +
                "              and ( ?::bigint is null or rv_id  = ?::bigint ) ",
            dbSchemaName);

        try (Connection connection = dataSource.getConnection();
             PreparedStatement s = connection.prepareStatement(sql)) {

            DbUtils.setInteger(s, 1, serverID);
            DbUtils.setInteger(s, 2, serverID);
            DbUtils.setInteger(s, 3, raspVsriantID);
            DbUtils.setInteger(s, 4, raspVsriantID);

            try (ResultSet results = s.executeQuery()) {
                while (results.next()) {
                    RaspVariantsData t = new RaspVariantsData();

                    t.serverID = DbUtils.getLong(results, "srv_id");
                    t.raspVariantID = DbUtils.getLong(results, "rv_id");
                    t.routeID = DbUtils.getLong(results, "mr_id");
                    t.week_mask = DbUtils.getInteger(results, "rv_dow");
                    t.marshVariantSeason = DbUtils.getString(results, "rv_season");
                    t.dateBegin = DbUtils.getLocalDateTime(results, "rv_startdate");
                    t.dateEnd = DbUtils.getLocalDateTime(results, "rv_enddate");
                    t.flagEndDateExists = DbUtils.getInteger(results, "rv_enddateexists");
                    t.raspVariantNum = DbUtils.getLong(results, "rv_num");
                    t.checkSum = DbUtils.getLong(results, "rv_checksum");

                    result.list.add(t);
                }
            }
        } catch (SQLException e) {
            LOG.error("getRaspVariantsListData " + e.getMessage());
        }
        return result;
    }

    public RaspTimeListData getRaspTimeListData(String strServerID, String strRaspVsriantID) {
        RaspTimeListData result = new RaspTimeListData();

        Integer serverID = TypeConvert.toInteger(strServerID);
        Integer raspVsriantID = TypeConvert.toInteger(strRaspVsriantID);

        String sql = String.format("select srv_id, rv_id, gr_id, rt_orderby, rt_graph, rt_smena, rl_racetype, rl_racetypeext, st_id, rt_time, rt_inv, rt_racenum, rc_kkp" +
                "             from %s.v_rasp_time" +
                "            where ( ?::bigint is null or srv_id = ?::bigint ) " +
                "              and ( ?::bigint is null or rv_id  = ?::bigint ) ",
            dbSchemaName);

        try (Connection connection = dataSource.getConnection();
             PreparedStatement s = connection.prepareStatement(sql)) {

            DbUtils.setInteger(s, 1, serverID);
            DbUtils.setInteger(s, 2, serverID);
            DbUtils.setInteger(s, 3, raspVsriantID);
            DbUtils.setInteger(s, 4, raspVsriantID);

            try (ResultSet results = s.executeQuery()) {
                while (results.next()) {
                    RaspTimeData t = new RaspTimeData();

                    t.serverID = DbUtils.getLong(results, "srv_id");
                    t.raspVariantID = DbUtils.getLong(results, "rv_id");
                    t.graphShiftID = DbUtils.getLong(results, "gr_id");
                    t.orderBy = DbUtils.getLong(results, "rt_orderby");
                    t.graph = DbUtils.getInteger(results, "rt_graph");
                    t.smena = DbUtils.getString(results, "rt_smena");
                    t.raceType = DbUtils.getString(results, "rl_racetype");
                    t.raceTypeExt = DbUtils.getString(results, "rl_racetypeext");
                    t.stopID = DbUtils.getLong(results, "st_id");
                    t.time = DbUtils.getInteger(results, "rt_time");
                    t.flag_inv = DbUtils.getInteger(results, "rt_inv");
                    t.raceNum = DbUtils.getInteger(results, "rt_racenum");
                    t.flagStartFinish = DbUtils.getString(results, "rc_kkp");

                    result.list.add(t);
                }
            }
        } catch (SQLException e) {
            LOG.error("getRaspTimeListData " + e.getMessage());
        }
        return result;
    }

    public DriverModeListData getDriverModeListData() {
        DriverModeListData result = new DriverModeListData();

        String sql = String.format("select dm_id, dm_title, tt_id, dm_digit, dm_sm from %s.v_driver_modes", dbSchemaName);

        try (Connection connection = dataSource.getConnection();
             PreparedStatement s = connection.prepareStatement(sql)) {

            try (ResultSet results = s.executeQuery()) {
                while (results.next()) {
                    DriverModeData t = new DriverModeData();

                    t.driverModeID = DbUtils.getLong(results, "dm_id");
                    t.transportTypeID = DbUtils.getLong(results, "tt_id");
                    t.driverModeName = DbUtils.getString(results, "dm_title");
                    t.driverModeDigit = DbUtils.getInteger(results, "dm_digit");
                    t.driverModeSmenaNum = DbUtils.getInteger(results, "dm_sm");

                    result.list.add(t);
                }
            }
        } catch (SQLException e) {
            LOG.error("getDriverModeListData " + e.getMessage());
        }
        return result;
    }

    public GraphListData getGraphListData(String strServerID, String strRaspVsriantID) {
        GraphListData result = new GraphListData();

        Integer serverID = TypeConvert.toInteger(strServerID);
        Integer raspVsriantID = TypeConvert.toInteger(strRaspVsriantID);

        String sql = String.format("select srv_id, rv_id, gl_graph, dm_id, gl_inv, gl_cap" +
                "             from %s.v_graph_list" +
                "            where ( ?::bigint is null or srv_id = ?::bigint ) " +
                "              and ( ?::bigint is null or rv_id  = ?::bigint ) ",
            dbSchemaName);

        try (Connection connection = dataSource.getConnection();
             PreparedStatement s = connection.prepareStatement(sql)) {

            DbUtils.setInteger(s, 1, serverID);
            DbUtils.setInteger(s, 2, serverID);
            DbUtils.setInteger(s, 3, raspVsriantID);
            DbUtils.setInteger(s, 4, raspVsriantID);

            try (ResultSet results = s.executeQuery()) {
                while (results.next()) {
                    GraphData t = new GraphData();

                    t.serverID = DbUtils.getLong(results, "srv_id");
                    t.raspVariantID = DbUtils.getLong(results, "rv_id");
                    t.graph = DbUtils.getInteger(results, "gl_graph");
                    t.driverModeID = DbUtils.getLong(results, "dm_id");
                    t.flag_inv = DbUtils.getInteger(results, "gl_inv");
                    t.capacity = DbUtils.getString(results, "gl_cap");

                    result.list.add(t);
                }
            }
        } catch (SQLException e) {
            LOG.error("getGraphListData " + e.getMessage());
        }
        return result;
    }

    public RaspEventsListData getRaspEventsListData(String strServerID, String strRaspVsriantID) {
        RaspEventsListData result = new RaspEventsListData();

        Integer serverID = TypeConvert.toInteger(strServerID);
        Integer raspVsriantID = TypeConvert.toInteger(strRaspVsriantID);

        String sql = String.format("select srv_id, rv_id, gr_id, re_orderby, re_graph, re_smena, re_starttime, re_endtime, ev_id, re_distance, re_nulltype" +
                "             from %s.v_rasp_events" +
                "            where ( ?::bigint is null or srv_id = ?::bigint ) " +
                "              and ( ?::bigint is null or rv_id  = ?::bigint ) ",
            dbSchemaName);

        try (Connection connection = dataSource.getConnection();
             PreparedStatement s = connection.prepareStatement(sql)) {

            DbUtils.setInteger(s, 1, serverID);
            DbUtils.setInteger(s, 2, serverID);
            DbUtils.setInteger(s, 3, raspVsriantID);
            DbUtils.setInteger(s, 4, raspVsriantID);

            try (ResultSet results = s.executeQuery()) {
                while (results.next()) {
                    RaspEventsData t = new RaspEventsData();

                    t.serverID = DbUtils.getLong(results, "srv_id");
                    t.raspVariantID = DbUtils.getLong(results, "rv_id");
                    t.graphShiftID = DbUtils.getLong(results, "gr_id");
                    t.orderBy = DbUtils.getLong(results, "re_orderby");
                    t.graph = DbUtils.getInteger(results, "re_graph");
                    t.smena = DbUtils.getString(results, "re_smena");
                    t.startTime = DbUtils.getInteger(results, "re_starttime");
                    t.endTime = DbUtils.getInteger(results, "re_endtime");
                    t.event_id = DbUtils.getInteger(results, "ev_id");
                    t.distance = DbUtils.getDouble(results, "re_distance");
                    t.nullType = DbUtils.getInteger(results, "re_nulltype");

                    result.list.add(t);
                }
            }
        } catch (SQLException e) {
            LOG.error("getGraphListData " + e.getMessage());
        }
        return result;
    }

    public MarshPsgAvgValueData getMarshPsgAvgValueData(String strRouteID, String strWeekMask,
                                                        String strStartTime, String strEndTime) {
        MarshPsgAvgValueData result = new MarshPsgAvgValueData();

        String sql = String.format("select marsh_avg_value from %s.v_marsh_psg_avg_value", dbSchemaName);

        try (Connection connection = dataSource.getConnection();
             PreparedStatement s = connection.prepareStatement(sql)) {

            try (ResultSet results = s.executeQuery()) {
                while (results.next()) {
                    result.value = DbUtils.getDouble(results, "marsh_avg_value");
                    break;
                }
            }
        } catch (SQLException e) {
            LOG.error("getMarshPsgAvgValueData " + e.getMessage());
        }
        return result;
    }

    public StopPsgAvgValueListData getStopPsgAvgValueListData(String strStopID, String strWeekMask,
                                                              String strStartTime, String strEndTime) {
        StopPsgAvgValueListData result = new StopPsgAvgValueListData();

        String sql = String.format("select mr_id, stop_avg_in, stop_avg_out from %s.v_stop_psg_avg_value", dbSchemaName);

        try (Connection connection = dataSource.getConnection();
             PreparedStatement s = connection.prepareStatement(sql)) {

            try (ResultSet results = s.executeQuery()) {
                while (results.next()) {
                    StopPsgAvgValueData t = new StopPsgAvgValueData();

                    t.route_id = DbUtils.getLong(results, "mr_id");
                    t.value_in = DbUtils.getDouble(results, "stop_avg_in");
                    t.value_out = DbUtils.getDouble(results, "stop_avg_out");

                    result.list.add(t);
                }
            }
        } catch (SQLException e) {
            LOG.error("getStopPsgAvgValueListData " + e.getMessage());
        }
        return result;
    }

    public RacePlanTimeListData getRacePlanTimeListData(String strRaveVariantNum) {
        RacePlanTimeListData result = new RacePlanTimeListData();

        String sql = String.format("select mv_id, rl_racetype, rl_racetypeext, rpt_avg_time, rpt_expl_speed" +
            "                     from %s.v_race_plan_time", dbSchemaName);

        try (Connection connection = dataSource.getConnection();
             PreparedStatement s = connection.prepareStatement(sql)) {

            try (ResultSet results = s.executeQuery()) {
                while (results.next()) {
                    RacePlanTimeData t = new RacePlanTimeData();

                    t.marshVariantID = DbUtils.getLong(results, "mv_id");
                    t.raceType = DbUtils.getString(results, "rl_racetype");
                    t.raceTypeExt = DbUtils.getString(results, "rl_racetypeext");
                    t.avgTime = DbUtils.getDouble(results, "rpt_avg_time");
                    t.explSpeed = DbUtils.getDouble(results, "rpt_expl_speed");

                    result.list.add(t);
                }
            }
        } catch (SQLException e) {
            LOG.error("getRacePlanTimeListData " + e.getMessage());
        }
        return result;
    }


    public GrNumListData getGrNumListData(Long routeID,
                                          List<LocalDateTime> beginDate,
                                          List<LocalDateTime> endDate) {
        GrNumListData result = new GrNumListData();

        try (Connection connection = dataSource.getConnection()) {
            //чтобы вернуть в пул в изначальном состоянии
            boolean previousAutoCommit = connection.getAutoCommit();
            try {
                connection.setAutoCommit(false);
                String sql = String.format("{ ? = call %s.gis_export_pkg$get_gr_num_list(?,?,?)}", dbSchemaName);
                try (CallableStatement call = connection.prepareCall(sql)) {
                    call.setFetchSize(DEFAULT_FETCH_SIZE);

                    Array a1 = connection.createArrayOf("timestamp", beginDate.toArray());
                    Array a2 = connection.createArrayOf("timestamp", endDate.toArray());

                    DbUtils.setLong(call, 2, routeID);
                    DbUtils.setArray(call, 3, a1);
                    DbUtils.setArray(call, 4, a2);

                    call.registerOutParameter(1, Types.OTHER);
                    call.execute();
                    try (ResultSet results = (ResultSet) call.getObject(1)) {
                        while (results.next()) {
                            GrNumData t = new GrNumData();
                            t.grNum = DbUtils.getString(results, "gr_num");
                            t.parkName = DbUtils.getString(results, "park_name");
                            t.parkId = DbUtils.getLong(results, "park_id");
                            t.navPointCount = DbUtils.getLong(results, "nav_point_count");
                            result.list.add(t);
                        }
                    }
                }
            } finally {
                connection.setAutoCommit(previousAutoCommit);
            }
        } catch (SQLException e) {
            LOG.error("getGrNumListData", e);
        }
        return result;
    }

    public NavPointListData getNavPointListData(List<Long> routeID,
                                                List<LocalDateTime> beginDate,
                                                List<LocalDateTime> endDate,
                                                Float lonMin,
                                                Float lonMax,
                                                Float latMin,
                                                Float latMax,
                                                Integer headingMin,
                                                Integer headingMax,
                                                Integer speedMin,
                                                Integer speedMax,
                                                List<Integer> grNum) {
        NavPointListData result = new NavPointListData();

        try (Connection connection = dataSource.getConnection()) {
            //чтобы вернуть в пул в изначальном состоянии
            boolean previousAutoCommit = connection.getAutoCommit();
            try {
                connection.setAutoCommit(false);
                String sql = String.format("{ ? = call %s.gis_export_pkg$get_nav_point_list(?,?,?,?,?,?,?,?,?,?,?,?)}", dbSchemaName);
                try (CallableStatement call = connection.prepareCall(sql)) {
                    call.setFetchSize(DEFAULT_FETCH_SIZE);

                    DbUtils.setArray(call, 2, connection.createArrayOf("int8", routeID.toArray()));
                    DbUtils.setArray(call, 3, connection.createArrayOf("timestamp", beginDate.toArray()));
                    DbUtils.setArray(call, 4, connection.createArrayOf("timestamp", endDate.toArray()));
                    DbUtils.setFloat(call, 5, lonMin);
                    DbUtils.setFloat(call, 6, lonMax);
                    DbUtils.setFloat(call, 7, latMin);
                    DbUtils.setFloat(call, 8, latMax);
                    DbUtils.setInteger(call, 9, headingMin);
                    DbUtils.setInteger(call, 10, headingMax);
                    DbUtils.setInteger(call, 11, speedMin);
                    DbUtils.setInteger(call, 12, speedMax);
                    DbUtils.setArray(call, 13, connection.createArrayOf("int4", grNum.toArray()));

                    call.registerOutParameter(1, Types.OTHER);

                    call.execute();
                    try (ResultSet results = (ResultSet) call.getObject(1)) {
                        while (results.next()) {
                            NavPointData t = new NavPointData();
                            t.pointID = DbUtils.getLong(results, "point_id");
                            t.pointDate = DbUtils.getLocalDateTime(results, "point_date");
                            t.pointLon = DbUtils.getDouble(results, "lon");
                            t.pointLat = DbUtils.getDouble(results, "lat");
                            t.heading = DbUtils.getLong(results, "heading");
                            t.pointNum = DbUtils.getLong(results, "point_num");
                            t.pointTag = DbUtils.getLong(results, "point_tag");
                            t.routeID = DbUtils.getLong(results, "route_id");
                            t.raceID = DbUtils.getLong(results, "race_id");
                            t.stopID = DbUtils.getLong(results, "stop_id");
                            t.routeTrajectoryID = DbUtils.getLong(results, "route_trajectory_id");
                            t.grNum = DbUtils.getString(results, "gr_num");
                            t.gosNum = DbUtils.getString(results, "gos_num");
                            t.speed = DbUtils.getInteger(results, "speed");
                            result.list.add(t);
                        }
                    }
                }
            } finally {
                connection.setAutoCommit(previousAutoCommit);
            }
        } catch (SQLException e) {
            LOG.error("getNavPointListData", e);
        }

        return result;
    }

    public IbrdConnectionListData getIbrdConnectionListData(Long ibrdMuid, ZoneId clientZone) {
        IbrdConnectionListData result = new IbrdConnectionListData();

        try (Connection connection = dataSource.getConnection()) {
            //чтобы вернуть в пул в изначальном состоянии
            boolean previousAutoCommit = connection.getAutoCommit();
            try {
                connection.setAutoCommit(false);
                String sql = String.format("{ ? = call %s.gis_export_pkg$get_ibrd_connection_list(?)}", dbSchemaName);
                try (CallableStatement call = connection.prepareCall(sql)) {
                    call.setFetchSize(DEFAULT_FETCH_SIZE);

                    DbUtils.setLong(call, 2, ibrdMuid);

                    call.registerOutParameter(1, Types.OTHER);

                    call.execute();
                    try (ResultSet results = (ResultSet) call.getObject(1)) {
                        while (results.next()) {
                            IbrdConnectionData t = new IbrdConnectionData();

                            t.ibrdMuid = DbUtils.getLong(results, "ibrd_muid");
                            t.code = DbUtils.getString(results, "code");
                            t.ibrdConnection = DbUtils.getInteger(results, "ibrd_connection");
                            t.dateConnection = TypeConvert.toClientLocalDateTime(DbUtils.getLocalDateTime(results, "date_connection"), clientZone);

                            result.list.add(t);
                        }
                    }
                }
            } finally {
                connection.setAutoCommit(previousAutoCommit);
            }
        } catch (SQLException e) {
            LOG.error("getIbrdConnectionListData", e);
        }

        return result;
    }

    public TrafficPass getTrafficPass(Long ibrdMuid, LocalDate startDate, LocalDate endDate, List<String> weekeday) {
        final String sql = "  with a as(" +
            // + INTERVAL '1 day' потому, что 1 - вск, 2 - пнд, 7 - субоота
            "      select (dd + interval '1 day') as date_ from generate_series ( ?, ?, '1 day'::interval) dd" +
            "  )," +
            "      b as (" +
            "        select *, count(*) over ()  from a where EXTRACT(isodow from date_ ) = any(string_to_array(?, ',')::smallint[]) " +
            "    )," +
            "      c as (select count, item.time_period_items_id, item.time_period_id," +
            "              date_ + make_interval(secs => item.start_offset) - interval '3 hour' as start_time," +
            "              date_ + make_interval(secs => item.end_offset) - interval '3 hour' as end_time" +
            "            from b, gis.time_period_items item)," +
            "      d as (select time_period_id, count, sum(in_corrected) from askp.asmpp_agg_item agg" +
            "      join c on time_start between c.start_time and c.end_time " +
            "                and agg.route_variants_muid = (select current_route_variant_muid from gis.routes where muid = ?)" +
            "    group by time_period_id, count)" +
            "  select n.time_period_id, n.time_period_name, coalesce(sum/count, 0) as avg_pass" +
            "  from d" +
            "    join gis.time_period_names n on n.time_period_id = d.time_period_id where count > 0" +
            "  order by time_period_id;";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        TrafficPass trafficPass = new TrafficPass();
        trafficPass.mr_id = ibrdMuid;
        trafficPass.startDate = startDate.format(formatter);
        trafficPass.endDate = endDate.format(formatter);
        String weekdays = String.join(",", weekeday);
        trafficPass.weekday = weekdays;
        // по всем дням недели
        if (weekdays.contains("0")) {
            weekdays = "1,2,3,4,5,6,7";
            trafficPass.weekday = "0";
        }
        TimeCode timeCode = null;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement pstmnt = connection.prepareStatement(sql)) {
            pstmnt.setDate(1, Date.valueOf(startDate));
            pstmnt.setDate(2, Date.valueOf(endDate));
            pstmnt.setString(3, weekdays);// строка, затем в массив
            pstmnt.setLong(4, ibrdMuid);
            try (ResultSet rs = pstmnt.executeQuery()) {
                while (rs.next()) {
                    timeCode = new TimeCode();

                    timeCode.timeCode = rs.getString(1);
                    timeCode.timeName = rs.getString(2);
                    timeCode.avgPas = rs.getString(3);

                    trafficPass.list.add(timeCode);
                }
            }
        } catch (Exception e) {
            LOG.error("getTrafficPass", e);
        }
        return trafficPass;
    }

    public TrafficPassOnStop getTrafficPassOnStop(String stopId, LocalDate startDate, LocalDate endDate, List<String> weekeday) {
        final String ALL_DAYS_NUMBERS = "1,2,3,4,5,6,7";

        final String sql =
            "with a as(" +
                "    select (dd + interval '1 day') as date_ from generate_series ( ?, ?, '1 day'::interval) dd" +
                " )," +
                "    b as (" +
                "      select *, count(*) over ()  from a where EXTRACT(isodow from date_) = any(string_to_array(?, ',')::smallint[])" +
                "  )," +
                "    c as (select count, item.time_period_items_id, item.time_period_id," +
                "            date_ + make_interval(secs => item.start_offset) - interval '3 hour' as start_time," +
                "            date_ + make_interval(secs => item.end_offset) - interval '3 hour' as end_time" +
                "          from b, gis.time_period_items item)," +
                "    d as (" +
                "      select distinct agg.route_variants_muid from askp.asmpp_agg_item agg where agg.time_start::date in (select date_ from b)" +
                "      and stop_place_muid = ?::bigint" +
                "  )," +
                "  dd as (" +
                "    select * from c, d" +
                "  )," +
                "  ss as(" +
                "    select count, dd.route_variants_muid, dd.time_period_id, dd.start_time, coalesce(agg.in_corrected, 0) as in_corrected," +
                "    coalesce(agg.out_corrected, 0) as out_corrected from dd" +
                "    left join askp.asmpp_agg_item agg on time_start between dd.start_time and dd.end_time" +
                "              and dd.route_variants_muid = agg.route_variants_muid" +
                "              and stop_place_muid = ?::bigint" +
                "  )," +
                "  qq as (" +
                "      select" +
                "        count," +
                "        route_variants_muid," +
                "        time_period_id," +
                "        sum(in_corrected)  as insumm," +
                "        sum(out_corrected) as outsumm" +
                "      from ss" +
                "      group by route_variants_muid, time_period_id, count" +
                "  )" +
                " select route_variants_muid, n.time_period_id, n.time_period_name, round(coalesce(insumm::numeric/count, 0),2) as inn, " +
                " round(coalesce(outsumm::numeric/count, 0),2) as outt " +
                " from qq  " +
                "  join gis.time_period_names n on n.time_period_id = qq.time_period_id " +
                " order by route_variants_muid, time_period_id;";

        TrafficPassOnStop trafficPassOnStop = new TrafficPassOnStop();

        trafficPassOnStop.stopId = stopId;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        String weekdays = String.join(",", weekeday);
        // по всем дням недели
        if (weekdays.contains("0")) {
            weekdays = ALL_DAYS_NUMBERS;
        }
        TimeCode timeCode = null;
        String prevRv = "";
        String rv = "";
        TrafficPass trafficPass_ = null;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement pstmnt = connection.prepareStatement(sql)) {
            pstmnt.setDate(1, Date.valueOf(startDate));
            pstmnt.setDate(2, Date.valueOf(endDate));
            pstmnt.setString(3, weekdays);// строка, затем в массив
            pstmnt.setString(4, stopId);// id остановки
            pstmnt.setString(5, stopId);// id остановки
            try (ResultSet rs = pstmnt.executeQuery()) {
                while (rs.next()) {
                    rv = rs.getString(1);
                    if (!rv.equalsIgnoreCase(prevRv)) {// новый вариант маршрута
                        prevRv = rv;
                        trafficPass_ = new TrafficPass();
                        trafficPassOnStop.trafficPassList.add(trafficPass_);
                        trafficPass_.startDate = startDate.format(formatter);
                        trafficPass_.endDate = endDate.format(formatter);
                        trafficPass_.mr_id = Long.parseLong(rv);
                        if (weekdays.equals(ALL_DAYS_NUMBERS)) {
                            trafficPass_.weekday = "0";
                        }
                    }
                    timeCode = new TimeCode();

                    timeCode.timeCode = rs.getString(2);
                    timeCode.timeName = rs.getString(3);
                    timeCode.avgPasIn = rs.getString(4);
                    timeCode.avgPasOut = rs.getString(5);

                    trafficPass_.list.add(timeCode);
                }
            }
        } catch (Exception e) {
            LOG.error("getTrafficPass", e);
        }
        return trafficPassOnStop;
    }


    public IbrdCoordListData getIbrdCoordListData(LocalDateTime beginDate, LocalDateTime endDate, ZoneId clientZone) {
        IbrdCoordListData result = new IbrdCoordListData();

        String sql = String.format("select info_board_muid, received_date, is_valid, lon, lat from %s.v_ibrd_coord " +
                "            where received_date between ?::timestamp and ?::timestamp",
            dbSchemaName);

        try (Connection connection = dataSource.getConnection();
             PreparedStatement s = connection.prepareStatement(sql)) {

            DbUtils.setLocalDateTime(s, 1, TypeConvert.toServerLocalDateTime(beginDate, clientZone));
            DbUtils.setLocalDateTime(s, 2, TypeConvert.toServerLocalDateTime(endDate, clientZone));

            try (ResultSet results = s.executeQuery()) {
                while (results.next()) {
                    IbrdCoordData t = new IbrdCoordData();

                    t.ibrdMuid = DbUtils.getLong(results, "info_board_muid");
                    t.receivedDate = TypeConvert.toClientLocalDateTime(DbUtils.getLocalDateTime(results, "received_date"), clientZone);
                    t.isValid = DbUtils.getBoolean(results, "is_valid");
                    t.longitude = DbUtils.getDouble(results, "lon");
                    t.latitude = DbUtils.getDouble(results, "lat");

                    result.list.add(t);
                }
            }
        } catch (SQLException e) {
            LOG.error("getIbrdCoordListData " + e.getMessage());
        }
        return result;
    }


    public TtVariantListData getTtVariants(Long routeMuid, String ttVariantIDList, ZoneId clientZone) {
        TtVariantListData result = new TtVariantListData();
        result.routeMuid = routeMuid;
        try (Connection connection = dataSource.getConnection()) {
            boolean previousAutoCommit = connection.getAutoCommit();
            try {
                connection.setAutoCommit(false);
                String sql = String.format("{ ? = call %s.gis_export_pkg$get_schedules(?::bigint, ?::text) }", dbSchemaName);
                try (CallableStatement call = connection.prepareCall(sql)) {
                    call.setFetchSize(DEFAULT_FETCH_SIZE);
                    call.registerOutParameter(1, Types.OTHER);
                    DbUtils.setLong(call, 2, routeMuid);
                    DbUtils.setString(call, 3, ttVariantIDList);

                    call.execute();
                    try (ResultSet results = (ResultSet) call.getObject(1)) {
                        TtVariantData item = null;
                        Long prevTtVariantID = -1L;
                        while (results.next()) {
                            if (result.routeVariantMuid == null)
                                result.routeVariantMuid = DbUtils.getLong(results, "route_variant_muid");

                            if (!prevTtVariantID.equals(DbUtils.getLong(results, "tt_variant_id"))) {
                                prevTtVariantID = DbUtils.getLong(results, "tt_variant_id");
                                item = new TtVariantData();
                                result.list.add(item);
                                item.ttVariantID = DbUtils.getLong(results, "tt_variant_id");
                                item.dateBegin = TypeConvert.toClientLocalDateTime(DbUtils.getLocalDateTime(results, "action_begin"), clientZone);
                                item.dateEnd = TypeConvert.toClientLocalDateTime(DbUtils.getLocalDateTime(results, "action_end"), clientZone);
                                item.week_days = DbUtils.getInteger(results, "week_days");
                                item.isActive = DbUtils.getBoolean(results, "is_active");
                                item.isMain = DbUtils.getBoolean(results, "is_main");
                                item.checksum = DbUtils.getString(results, "checksum");
                            }

                            if (item != null) {
                                TtVariantRouteData r = new TtVariantRouteData();
                                r.roundID = DbUtils.getLong(results, "round_id");
                                ;
                                r.roundType = DbUtils.getString(results, "round_type");
                                r.roundCode = DbUtils.getString(results, "round_code");
                                item.rounds.add(r);
                            }
                        }
                    }
                }
            } finally {
                connection.setAutoCommit(previousAutoCommit);
            }
        } catch (SQLException e) {
            LOG.error("getTtVariants", e);
        }
        return result;
    }


    public TtVariantOutsListData getTtVariantOuts(Long ttVariantID) {
        TtVariantOutsListData result = new TtVariantOutsListData();
        result.ttVariantID = ttVariantID;
        try (Connection connection = dataSource.getConnection()) {
            boolean previousAutoCommit = connection.getAutoCommit();
            try {
                connection.setAutoCommit(false);
                String sql = String.format("{ ? = call %s.gis_export_pkg$get_outs(?::bigint) }", dbSchemaName);
                try (CallableStatement call = connection.prepareCall(sql)) {
                    call.setFetchSize(DEFAULT_FETCH_SIZE);
                    call.registerOutParameter(1, Types.OTHER);
                    DbUtils.setLong(call, 2, ttVariantID);

                    call.execute();
                    try (ResultSet results = (ResultSet) call.getObject(1)) {
                        while (results.next()) {
                            TtVariantOutsData o = new TtVariantOutsData();
                            result.list.add(o);
                            o.outNum = DbUtils.getInteger(results, "tt_out_num");
                            o.smCount = DbUtils.getInteger(results, "sm_count");
                            o.capacityShortName = DbUtils.getString(results, "capacity_short_name");
                        }
                    }
                }
            } finally {
                connection.setAutoCommit(previousAutoCommit);
            }
        } catch (SQLException e) {
            LOG.error("getTtVariantOuts", e);
        }
        return result;
    }


    public TtVariantOutsReleaseListData getTtVariantOutsRelease(Long ttVariantID) {
        TtVariantOutsReleaseListData result = new TtVariantOutsReleaseListData();
        result.ttVariantID = ttVariantID;
        try (Connection connection = dataSource.getConnection()) {
            boolean previousAutoCommit = connection.getAutoCommit();
            try {
                connection.setAutoCommit(false);
                String sql = String.format("{ ? = call %s.gis_export_pkg$get_release(?::bigint) }", dbSchemaName);
                try (CallableStatement call = connection.prepareCall(sql)) {
                    call.setFetchSize(DEFAULT_FETCH_SIZE);
                    call.registerOutParameter(1, Types.OTHER);
                    DbUtils.setLong(call, 2, ttVariantID);

                    call.execute();
                    try (ResultSet results = (ResultSet) call.getObject(1)) {
                        while (results.next()) {
                            TtVariantOutsReleaseData o = new TtVariantOutsReleaseData();
                            result.list.add(o);
                            if (result.release == null)
                                result.release = DbUtils.getInteger(results, "all_tr_capacity_count");
                            o.capacityShortName = DbUtils.getString(results, "capacity_short_name");
                            o.quantity = DbUtils.getInteger(results, "tr_capacity_count");
                        }
                    }
                }
            } finally {
                connection.setAutoCommit(previousAutoCommit);
            }
        } catch (SQLException e) {
            LOG.error("getTtVariantOuts", e);
        }
        return result;
    }

    public TtVariantOutsEventsListData getTtVariantOutsEventsListData(Long ttVariantID, ZoneId clientZone) {
        TtVariantOutsEventsListData result = new TtVariantOutsEventsListData();
        result.ttVariantID = ttVariantID;
        try (Connection connection = dataSource.getConnection()) {
            boolean previousAutoCommit = connection.getAutoCommit();
            try {
                connection.setAutoCommit(false);
                String sql = String.format("{ ? = call %s.gis_export_pkg$get_event_type(?::bigint) }", dbSchemaName);
                try (CallableStatement call = connection.prepareCall(sql)) {
                    call.setFetchSize(DEFAULT_FETCH_SIZE);
                    call.registerOutParameter(1, Types.OTHER);
                    DbUtils.setLong(call, 2, ttVariantID);

                    call.execute();
                    try (ResultSet results = (ResultSet) call.getObject(1)) {
                        TtVariantOutsEventsData item = null;
                        Long prevTtOutID = -1L;
                        while (results.next()) {
                            if (!prevTtOutID.equals(DbUtils.getLong(results, "tt_out_id"))) {
                                prevTtOutID = DbUtils.getLong(results, "tt_out_id");
                                item = new TtVariantOutsEventsData();
                                result.list.add(item);
                                item.outNum = DbUtils.getString(results, "tt_out_num");
                            }

                            if (item != null) {
                                TtVariantOutsRoundEventData d = new TtVariantOutsRoundEventData();
                                item.roundEvent.add(d);
                                d.eventNum = DbUtils.getInteger(results, "event_num");
                                d.routeMuid = DbUtils.getLong(results, "round_muid");
                                d.routeType = DbUtils.getString(results, "round_type");
                                d.routeCode = DbUtils.getString(results, "round_code");
                                d.trackID = DbUtils.getLong(results, "route_trajectory_muid");
                                d.trackType = DbUtils.getInteger(results, "trajectory_type_muid");
                                d.eventTypeID = DbUtils.getInteger(results, "action_type_id");
                                d.timeBegin = DbUtils.getLocalDateTime(results, "time_begin");
                                d.timeEnd = DbUtils.getLocalDateTime(results, "time_end");
                            }
                        }
                    }
                }
            } finally {
                connection.setAutoCommit(previousAutoCommit);
            }
        } catch (SQLException e) {
            LOG.error("getTtVariantOutsEventsListData", e);
        }
        return result;
    }


    public StopTimesListData getStopTimesListData(Long ttVariantID, List<Long> stopPlaceList) {
        StopTimesListData result = new StopTimesListData();
        result.ttVariantID = ttVariantID;
        try (Connection connection = dataSource.getConnection()) {
            boolean previousAutoCommit = connection.getAutoCommit();
            try {
                connection.setAutoCommit(false);
                String sql = String.format("{ ? = call %s.gis_export_pkg$get_stop_times(?::bigint, ?::bigint[]) }", dbSchemaName);
                try (CallableStatement call = connection.prepareCall(sql)) {
                    call.setFetchSize(DEFAULT_FETCH_SIZE);
                    call.registerOutParameter(1, Types.OTHER);
                    DbUtils.setLong(call, 2, ttVariantID);
                    DbUtils.setArray(call, 3, connection.createArrayOf("int8", stopPlaceList.toArray()));

                    call.execute();

                    try (ResultSet results = (ResultSet) call.getObject(1)) {
                        StopTimesData item = null;
                        Long prevStopPlaceID = -1L;
                        while (results.next()) {
                            if (!prevStopPlaceID.equals(DbUtils.getLong(results, "stop_place_muid"))) {
                                prevStopPlaceID = DbUtils.getLong(results, "stop_place_muid");
                                item = new StopTimesData();
                                result.list.add(item);
                                item.stopPlaceID = prevStopPlaceID;
                                item.stopPlaceTypeID = DbUtils.getString(results, "stop_type");
                            }
                            RaspTimeAction action = new RaspTimeAction();
                            item.actions.add(action);
                            action.raceMuid = DbUtils.getLong(results, "route_muid");
                            action.raceType = DbUtils.getString(results, "round_type");
                            action.routeCode = DbUtils.getString(results, "round_code");
                            action.trackID = DbUtils.getLong(results, "route_trajectory_muid");
                            action.trackType = DbUtils.getInteger(results, "trajectory_type_muid");
                            action.outNum = DbUtils.getString(results, "tt_out_num");
                            action.shiftNum = DbUtils.getInteger(results, "dr_shift_num");
                            action.outOrderNum = DbUtils.getInteger(results, "out_order_num");
                            action.opNum = DbUtils.getInteger(results, "order_num");
                            action.opEntryNumber = DbUtils.getInteger(results, "op_entry_number");
                            action.timeBegin = DbUtils.getLocalDateTime(results, "time_begin");
                            action.timeBeginMinute = DbUtils.getInteger(results, "time_begin_min");
                            action.timeEnd = DbUtils.getLocalDateTime(results, "time_end");
                            action.timeEndMinute = DbUtils.getInteger(results, "time_end_min");
                        }
                    }
                }
            } finally {
                connection.setAutoCommit(previousAutoCommit);
            }
        } catch (SQLException e) {
            LOG.error("getStopTimesListData", e);
        }
        return result;
    }


    public ExploitationPeriodsListData getExploitationPeriodsData(Integer ttVariantID) {
        ExploitationPeriodsListData result = new ExploitationPeriodsListData();
        result.ttVariantID = ttVariantID;
        try (Connection connection = dataSource.getConnection()) {
            boolean prevAutoCommit = connection.getAutoCommit();
            try {
                connection.setAutoCommit(false);
                String sql = String.format("{ ? = call %s.gis_export_pkg$get_exploitation_by_periods(?::int) }", dbSchemaName);
                try (CallableStatement call = connection.prepareCall(sql)) {
                    call.setFetchSize(DEFAULT_FETCH_SIZE);
                    call.registerOutParameter(1, Types.OTHER);
                    DbUtils.setInteger(call, 2, ttVariantID);

                    call.execute();
                    try (ResultSet results = (ResultSet) call.getObject(1)) {
                        while (results.next()) {
                            ExploitationPeriodsData o = new ExploitationPeriodsData();
                            result.list.add(o);

                            o.timeCode = DbUtils.getInteger(results, "time_code");
                            o.timeName = DbUtils.getString(results, "time_name");
                            o.frequency = DbUtils.getBigDecimal(results, "frequency");
                            o.interval = DbUtils.getInteger(results, "interval");
                            o.roundTime = DbUtils.getInteger(results, "round_time");
                            o.speed = DbUtils.getBigDecimal(results, "speed");
                        }
                    }
                }
            } finally {
                connection.setAutoCommit(prevAutoCommit);
            }
        } catch (SQLException e) {
            LOG.error("getExploitationPeriodsData", e);
        }

        return result;
    }


    public ExploitationListData getExploitationListData(Integer ttVariantID, LocalTime beginTime, LocalTime endTime, ZoneId clientZone) {
        ExploitationListData result = new ExploitationListData();
        result.ttVariantID = ttVariantID;
        try (Connection connection = dataSource.getConnection()) {
            boolean prevAutoCommit = connection.getAutoCommit();
            try {
                connection.setAutoCommit(false);
                String sql = String.format("{ ? = call %s.gis_export_pkg$get_exploitation(?::int, ?::timestamp, ?::timestamp) }", dbSchemaName);
                try (CallableStatement call = connection.prepareCall(sql)) {
                    call.setFetchSize(DEFAULT_FETCH_SIZE);
                    call.registerOutParameter(1, Types.OTHER);
                    DbUtils.setInteger(call, 2, ttVariantID);

                    LocalDate date = LocalDate.of(1900, 1, 1);
                    DbUtils.setLocalDateTime(call, 3, LocalDateTime.of(date, beginTime));
                    DbUtils.setLocalDateTime(call, 4, LocalDateTime.of(date, endTime));

                    call.execute();
                    try (ResultSet results = (ResultSet) call.getObject(1)) {
                        while (results.next()) {
                            result.timeBegin = DbUtils.getLocalDateTime(results, "begin_time");
                            result.timeEnd = DbUtils.getLocalDateTime(results, "end_time");
                            result.frequency = DbUtils.getBigDecimal(results, "frequency");
                            result.interval = DbUtils.getInteger(results, "interval");
                            result.roundTime = DbUtils.getInteger(results, "round_time");
                            result.speed = DbUtils.getBigDecimal(results, "speed");
                            break;
                        }
                    }
                }
            } finally {
                connection.setAutoCommit(prevAutoCommit);
            }
        } catch (SQLException e) {
            LOG.error("getExploitationListData", e);
        }
        return result;
    }


    public List<SensorTermo> getSensorTermo(LocalDate beginDate, ZoneId clientZone) {
        List<SensorTermo> result = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            boolean previousAutoCommit = connection.getAutoCommit();
            try {
                connection.setAutoCommit(false);
                String sql = String.format("{ ? = call %s.gis_export_pkg$get_sensor_termo(?::timestamp) }", dbSchemaName);
                try (CallableStatement call = connection.prepareCall(sql)) {
                    call.setFetchSize(DEFAULT_FETCH_SIZE);
                    call.registerOutParameter(1, Types.OTHER);
                    DbUtils.setLocalDateTime(call, 2, beginDate.atStartOfDay());

                    call.execute();
                    try (ResultSet results = (ResultSet) call.getObject(1)) {
                        while (results.next()) {
                            SensorTermo s = new SensorTermo();
                            s.vehicleID = DbUtils.getLong(results, "vehicle_id");
                            s.gosNomer = DbUtils.getString(results, "gos_nomer");
                            s.depotNumber = DbUtils.getString(results, "depot_number");
                            s.sensorStatus = DbUtils.getInteger(results, "sensor_status");
                            s.sensorTermo = DbUtils.getInteger(results, "sensor_termo");
                            s.eventdateTime = DbUtils.getString(results, "datetime");
                            result.add(s);
                        }
                    }
                }
            } finally {
                connection.setAutoCommit(previousAutoCommit);
            }
        } catch (SQLException e) {
            LOG.error("getSensorTermo", e);
        }
        return result;
    }

    public void getSensorTermo(LocalDate beginDate, OutputStream out) {
        JsonFactory factory = new JsonFactory();
        try (JsonGenerator generator = factory.createGenerator(out)) {
            generator.setCodec(mapper);
            try (Connection connection = dataSource.getConnection()) {
                boolean previousAutoCommit = connection.getAutoCommit();
                try {
                    connection.setAutoCommit(false);
                    String sql = String.format("{ ? = call %s.gis_export_pkg$get_sensor_termo(?::timestamp) }", dbSchemaName);
                    try (CallableStatement call = connection.prepareCall(sql)) {
                        call.setFetchSize(DEFAULT_FETCH_SIZE);
                        call.registerOutParameter(1, Types.OTHER);
                        DbUtils.setLocalDateTime(call, 2, beginDate.atStartOfDay());

                        call.execute();
                        try (ResultSet results = (ResultSet) call.getObject(1)) {
                            int i = 1;
                            SensorTermo s = new SensorTermo();
                            generator.writeStartArray();
                            while (results.next()) {
                                s.vehicleID = DbUtils.getLong(results, "vehicle_id");
                                s.gosNomer = DbUtils.getString(results, "gos_nomer");
                                s.depotNumber = DbUtils.getString(results, "depot_number");
                                s.sensorStatus = DbUtils.getInteger(results, "sensor_status");
                                s.sensorTermo = DbUtils.getInteger(results, "sensor_termo");
                                s.eventdateTime = DbUtils.getString(results, "datetime");

                                generator.writeObject(s);
                                if (i++ % DEFAULT_FETCH_SIZE == 0) {
                                    generator.flush();
                                }
                            }
                        }
                        generator.writeEndArray();
                        generator.flush();
                    }
                } finally {
                    connection.setAutoCommit(previousAutoCommit);
                }
            }
        } catch (IOException | SQLException e) {
            LOG.error("getSensorTermo", e);
        }
    }
}
