package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

class TtVariantOutsReleaseData {

    /** Краткое наименование вместимости ТС */
    @XmlElement(name = "capacity_short_name")
    public String capacityShortName;

    /** Количество выходов */
    @XmlElement(name = "quantity")
    public Integer quantity;
}

/**
 * Данные о выпуске в привязке к варианту расписания
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "sh_variant_outs")
public class TtVariantOutsReleaseListData {

    /** ID варианта расписания */
    @XmlElement(name = "sh_id")
    public Long ttVariantID;

    /** Суммарный выпуск ТС */
    @XmlElement(name = "release")
    public Integer release;

    /** Выходы */
    @XmlElement(name = "outs")
    public List<TtVariantOutsReleaseData> list = new ArrayList<>();
}
