package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

class GraphData {

    @XmlElement(name = "srv_id")
    public Long serverID;

    @XmlElement(name = "rv_id")
    public Long raspVariantID;

    @XmlElement(name = "gl_graph")
    public Integer graph;

    @XmlElement(name = "dm_id")
    public Long driverModeID;

    @XmlElement(name = "gl_inv")
    public Integer flag_inv;

    @XmlElement(name = "gl_cap")
    public String capacity;
}

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "GraphList")
public class GraphListData {

    @XmlElement(name = "Graph")
    public List<GraphData> list = new ArrayList<>();
}
