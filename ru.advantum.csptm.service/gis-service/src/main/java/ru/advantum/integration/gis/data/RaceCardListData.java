package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

class RaceCardData {

    @XmlElement(name = "mv_id")
    public Long marshVariantID;

    @XmlElement(name = "rl_racetype")
    public String raceType;

    @XmlElement(name = "rl_racetypeext")
    public String raceTypeExt;

    @XmlElement(name = "rc_orderby")
    public Long orderBy;

    @XmlElement(name = "st_id")
    public Long stopID;

    @XmlElement(name = "rc_kkp")
    public String flagStartFinish;

    @XmlElement(name = "rc_distance")
    public Double distance;
}

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "RaceCards")
public class RaceCardListData {

    @XmlElement(name = "RaceCard")
    public List<RaceCardData> list = new ArrayList<>();
}
