package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

class TtVariantOutsRoundEventData {

    /** Порядковый номер события для выхода */
    @XmlElement(name = "event_num")
    public Integer eventNum;

    /** Идентификатор рейса ГИС */
    @XmlElement(name = "r_id")
    public Long routeMuid;

    /** Тип рейса */
    @XmlElement(name = "r_type")
    public String routeType;

    /** Код рейса */
    @XmlElement(name = "r_code")
    public String routeCode;

    /** Идентификатор траектории ГИС */
    @XmlElement(name = "track_id")
    public Long trackID;

    /** Тип траектории */
    @XmlElement(name = "track_type")
    public Integer trackType;

    /** Идентификатор типа события */
    @XmlElement(name = "et_id")
    public Integer eventTypeID;

    /** Время начала события */
    @XmlElement(name = "begin_time")
    @XmlJavaTypeAdapter(LocalTimeAdapter.class)
    public LocalDateTime timeBegin;

    /** Время окончания события */
    @XmlElement(name = "end_time")
    @XmlJavaTypeAdapter(LocalTimeAdapter.class)
    public LocalDateTime timeEnd;
}

class TtVariantOutsEventsData {

    /** Номер выхода */
    @XmlElement(name = "out_num")
    public String outNum;

    /** События рейса */
    @XmlElement(name = "round_event")
    public List<TtVariantOutsRoundEventData> roundEvent = new ArrayList<>();
}

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "round_events")
public class TtVariantOutsEventsListData {

    /** ID варианта расписания */
    @XmlElement(name = "sh_id")
    public Long ttVariantID;

    /** Выходы */
    @XmlElement(name = "outs")
    public List<TtVariantOutsEventsData> list = new ArrayList<>();
}
