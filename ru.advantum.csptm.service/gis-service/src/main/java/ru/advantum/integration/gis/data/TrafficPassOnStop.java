package ru.advantum.integration.gis.data;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="xml")
public class TrafficPassOnStop {

    @XmlElement(name = "stop_id")
    public String stopId;

    @XmlElement(name = "route")
    List<TrafficPass> trafficPassList = new ArrayList<>();
}
