/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.advantum.integration.gis.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Validated
@Configuration
@ConfigurationProperties(prefix = "gis-service")
public class GisServiceConfig {
    
    private String dbSchema = "gis";

    private long maxRequestedPeriodInHours = 168;

    private String defaultClientTimeZone = "Europe/Moscow";

    public String getDbSchema() {
        return dbSchema;
    }

    public void setDbSchema(String dbSchema) {
        this.dbSchema = dbSchema;
    }

    public long getMaxRequestedPeriodInHours() {
        return maxRequestedPeriodInHours;
    }

    public void setMaxRequestedPeriodInHours(long maxRequestedPeriodInHours) {
        this.maxRequestedPeriodInHours = maxRequestedPeriodInHours;
    }

    public String getDefaultClientTimeZone() {
        return defaultClientTimeZone;
    }

    public void setDefaultClientTimeZone(String defaultClientTimeZone) {
        this.defaultClientTimeZone = defaultClientTimeZone;
    }



}
