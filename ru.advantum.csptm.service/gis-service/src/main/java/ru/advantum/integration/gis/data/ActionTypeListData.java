package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

class ActionTypeData {
    @XmlElement(name = "et_id")
    public Integer eventID;

    @XmlElement(name = "parent_et_id")
    public Integer parentEventID;

    @XmlElement(name = "et_name")
    public String eventName;
}


/**
 * Данные о типах событий
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "event_types")
public class ActionTypeListData {

    @XmlElement(name = "event_type")
    public List<ActionTypeData> list = new ArrayList<>();
}
