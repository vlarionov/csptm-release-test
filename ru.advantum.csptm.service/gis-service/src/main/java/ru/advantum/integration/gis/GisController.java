package ru.advantum.integration.gis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import ru.advantum.integration.gis.config.GisServiceConfig;
import ru.advantum.integration.gis.data.*;
import ru.advantum.integration.utils.TypeConvert;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/")
public class GisController {

    private final Duration maxRequestedPeriod;
    private ZoneId defaultClientTimeZone;
    private DataProvider dataProvider;

    @Autowired
    public GisController(GisServiceConfig config, DataProvider dataProvider) {
        this.dataProvider = dataProvider;
        maxRequestedPeriod = Duration.ofHours(config.getMaxRequestedPeriodInHours());
        defaultClientTimeZone = ZoneId.of(config.getDefaultClientTimeZone());
    }

    /**
     * Типы транспорта
     * http://localhost:8080/getTransportTypes?tt_id
     *
     * @param transportTypesID
     */
    @RequestMapping(value = "/getTransportTypes", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public TransportTypeListData responseTransportTypes(@RequestParam("tt_id") String transportTypesID) {
        return dataProvider.getTransportTypeListData(transportTypesID);
    }

    /**
     * Праздничные дни
     * http://localhost:8080/getRaspHolidays?mr_id=1
     * http://ebnd.advantum.dev/gis/getRaspHolidays?mr_id=1
     *
     * @param strRouteID
     * @param strHolydayDate
     */
    @RequestMapping(value = "/getRaspHolidays", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public RaspHolidaysListData responseRaspHolidays(@RequestParam("mr_id") String strRouteID,
                                                     @RequestParam("rh_date") String strHolydayDate) {
        return dataProvider.getRaspHolidaysListData(strRouteID, strHolydayDate);
    }

    /**
     * Контрольные суммы
     * http://localhost:8080/getChecksum?cs_tablename=table1
     * http://ebnd.advantum.dev/gis/getChecksum?cs_tablename=table1
     *
     * @param tableName
     */
    @RequestMapping(value = "/getChecksum", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public ChecksumListData responseChecksum(@RequestParam("cs_tablename") String tableName) {
        return dataProvider.getChecksumListData(tableName);
    }

//    /**
//     * Типы технологическиих событий
//     * http://localhost:8080/getEvents
//     * http://ebnd.advantum.dev/gis/getEvents
//     */
//    @RequestMapping(value="/getEvents", method = RequestMethod.GET, produces=MediaType.APPLICATION_XML_VALUE)
//    public EventsListData responseEvents() {
//        return dataProvider.getEventsListData();
//    }

    /**
     * Данные о типах событий
     * http://localhost:8080/actionType
     * http://ebnd.advantum.dev/gis/actionType
     */
    @RequestMapping(value = "/actionType", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public ActionTypeListData responseActionType() {
        return dataProvider.getActionTypeData();
    }

    /**
     * Варианты маршрута
     * http://localhost:8080/getMarshVariants?mv_id=2
     * http://ebnd.advantum.dev/gis/getMarshVariants?mv_id=2
     *
     * @param strMarshVariantID
     */
    @RequestMapping(value = "/getMarshVariants", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public MarshVariantsListData responseMarshVariants(@RequestParam("mv_id") String strMarshVariantID) {
        return dataProvider.getMarshVariantsListData(strMarshVariantID);
    }

    /**
     * Типы рейсов
     * http://localhost:8080/getRaceList?mv_id=2
     * http://ebnd.advantum.dev/gis/getRaceList?mv_id=2
     *
     * @param strMarshVariantID
     */
    @RequestMapping(value = "/getRaceList", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public RaceListData responseRaceList(@RequestParam("mv_id") String strMarshVariantID) {
        return dataProvider.getRaceListData(strMarshVariantID);
    }

    /**
     * Последовательность остановок
     * http://localhost:8080/getRaceCard?mv_id=2
     * http://ebnd.advantum.dev/gis/getRaceCard?mv_id=2
     *
     * @param strMarshVariantID
     */
    @RequestMapping(value = "/getRaceCard", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public RaceCardListData responseRaceCard(@RequestParam("mv_id") String strMarshVariantID) {
        return dataProvider.getRaceCardListData(strMarshVariantID);
    }

    /**
     * Плановые расписания
     * http://localhost:8080/getRaspVariants?srv_id=1&rv_id=2
     * http://ebnd.advantum.dev/gis/getRaspVariants?srv_id=1&rv_id=1
     *
     * @param strServerID
     * @param strRaspVsriantID
     */
    @RequestMapping(value = "/getRaspVariants", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public RaspVariantsListData responseRaceCard(@RequestParam("srv_id") String strServerID,
                                                 @RequestParam("rv_id") String strRaspVsriantID) {
        return dataProvider.getRaspVariantsListData(strServerID, strRaspVsriantID);
    }

    /**
     * Данные о вариантах расписания
     * http://localhost:8080/schedules?mr_var_id=2994045179695341245
     * http://localhost:8080/schedules?sh_id=[4824672951846579824,2364725756375480007]
     * http://ebnd.advantum.dev/gis/schedules?mr_var_id=2994045179695341245
     * http://ebnd.advantum.dev/gis/schedules?sh_id=[4824672951846579824,2364725756375480007]
     *
     * @param strRouteMuid       Идентификатор варианта маршрута ГИС
     * @param strTtVariantIDList Идентификатор варианта расписания
     */
    @RequestMapping(value = "/schedules", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public TtVariantListData responseTtVariants(@RequestParam("mr_var_id") String strRouteMuid,
                                                @RequestParam("sh_id") String strTtVariantIDList) {

        Long routeMuid = TypeConvert.toLong(strRouteMuid);
        return dataProvider.getTtVariants(routeMuid,
            strTtVariantIDList == null ?
                strTtVariantIDList :
                strTtVariantIDList.replace("[", "").replace("]", ""),
            defaultClientTimeZone);
    }

    /**
     * Плановые времена отправлений
     * http://localhost:8080/getRaspTime?srv_id=2&rv_id=2
     * http://ebnd.advantum.dev/gis/getRaspTime?srv_id=2&rv_id=2
     *
     * @param strServerID
     * @param strRaspVariantID
     */
    @RequestMapping(value = "/getRaspTime", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public RaspTimeListData responseRaspTime(@RequestParam("srv_id") String strServerID,
                                             @RequestParam("rv_id") String strRaspVariantID) {
        return dataProvider.getRaspTimeListData(strServerID, strRaspVariantID);
    }

    /**
     * Данные о временах отправлений
     * http://localhost:8080/stopTimes?sh_id=53&op_id=[3582685314669477311,2993277476269430252]
     * http://ebnd.advantum.dev/gis/stopTimes?sh_id=53&op_id=[3582685314669477311,2993277476269430252]
     *
     * @param strTtVariantID
     * @param strStopPlaceList
     */
    @RequestMapping(value = "/stopTimes", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public StopTimesListData responseStopTimes(@RequestParam("sh_id") String strTtVariantID,
                                               @RequestParam("op_id") String strStopPlaceList) {
        Long ttVariantID = TypeConvert.toLong(strTtVariantID);
        List<Long> stopPlaceList = new ArrayList<>();
        if (strStopPlaceList != null) {
            stopPlaceList = Arrays.stream(strStopPlaceList
                .replace("[", "")
                .replace("]", "")
                .split(","))
                .map(Long::valueOf)
                .collect(Collectors.toList());
        }
        return dataProvider.getStopTimesListData(ttVariantID, stopPlaceList);
    }

    /**
     * Режтимы работы водителя
     * http://localhost:8080/getDriverModes
     * http://ebnd.advantum.dev/gis/getDriverModes
     */
    @RequestMapping(value = "/getDriverModes", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public DriverModeListData responseDriverMode() {
        return dataProvider.getDriverModeListData();
    }

    /**
     * Графики выходов
     * http://localhost:8080/getGraphList?srv_id=2&rv_id=2
     * http://ebnd.advantum.dev/gis/getGraphList?srv_id=2&rv_id=2
     *
     * @param strServerID
     * @param strRaspVariantID
     */
    @RequestMapping(value = "/getGraphList", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public GraphListData responseGraphList(@RequestParam("srv_id") String strServerID,
                                           @RequestParam("rv_id") String strRaspVariantID) {
        return dataProvider.getGraphListData(strServerID, strRaspVariantID);
    }

    /**
     * Данные о выходах в привязке к варианту расписания
     * http://localhost:8080/outs?sh_id=45
     * http://ebnd.advantum.dev/gis/outs?sh_id=45
     *
     * @param strTtVariantID Идентификатор варианта расписания
     */
    @RequestMapping(value = "/outs", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public TtVariantOutsListData responseTtVariantOuts(@RequestParam("sh_id") String strTtVariantID) {
        Long ttVariantID = TypeConvert.toLong(strTtVariantID);
        return dataProvider.getTtVariantOuts(ttVariantID);
    }

    /**
     * Данные о выпуске в привязке к варианту расписания
     * http://localhost:8080/release?sh_id=53
     * http://ebnd.advantum.dev/gis/release?sh_id=53
     *
     * @param strTtVariantID Идентификатор варианта расписания
     */
    @RequestMapping(value = "/release", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public TtVariantOutsReleaseListData responseTtVariantOutsRelease(@RequestParam("sh_id") String strTtVariantID) {
        Long ttVariantID = TypeConvert.toLong(strTtVariantID);
        return dataProvider.getTtVariantOutsRelease(ttVariantID);
    }

    /**
     * Плановые технологические события
     * http://localhost:8080/getRaspEvents?srv_id=1&rv_id=1
     * http://ebnd.advantum.dev/gis/getRaspEvents?srv_id=1&rv_id=1
     *
     * @param strServerID
     * @param strRaspVariantID
     */
    @RequestMapping(value = "/getRaspEvents", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public RaspEventsListData responseRaspEventsList(@RequestParam("srv_id") String strServerID,
                                                     @RequestParam("rv_id") String strRaspVariantID) {
        return dataProvider.getRaspEventsListData(strServerID, strRaspVariantID);
    }

    /**
     * Данные о событиях в привязке к варианту расписания
     * http://localhost:8080/eventTypes?sh_id=53
     * http://ebnd.advantum.dev/gis/eventTypes?sh_id=53
     *
     * @param strTtVariantID Идентификатор варианта расписания
     */
    @RequestMapping(value = "/eventTypes", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public TtVariantOutsEventsListData responseTtVariantOutsEvents(@RequestParam("sh_id") String strTtVariantID) {
        Long ttVariantID = TypeConvert.toLong(strTtVariantID);
        return dataProvider.getTtVariantOutsEventsListData(ttVariantID, defaultClientTimeZone);
    }

    /**
     * Данные об эксплуатационных показателях в привязке к варианту расписания с разбивкой по временным промежуткам
     * http://localhost:8080/exploitationByPeriods?sh_id=4824672951846579824
     * http://localhost:8080/exploitationByPeriods?sh_id=53
     * http://ebnd.advantum.dev/gis/exploitationByPeriods?sh_id=53
     *
     * @param strTtVariantID Идентификатор варианта расписания
     */
    @RequestMapping(value = "/exploitationByPeriods", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public ExploitationPeriodsListData responseExploitationPeriods(@RequestParam("sh_id") String strTtVariantID) {
        Integer ttVariantID = TypeConvert.toInteger(strTtVariantID);
        return dataProvider.getExploitationPeriodsData(ttVariantID);
    }

    /**
     * Данные об эксплуатационных показателях в привязке к варианту расписания
     * http://localhost:8080/exploitation?sh_id=4824672951846579824&begin_time=10:30&end_time=15:00
     * http://localhost:8080/exploitation?sh_id=53&begin_time=09:00&end_time=12:00
     * http://ebnd.advantum.dev/gis/exploitation?sh_id=53&begin_time=09:00&end_time=12:00
     *
     * @param strTtVariantID Идентификатор варианта расписания
     * @param strBeginTime   Время начала диапазона времени анализа движения ТС
     * @param strEndTime     Время окончания диапазона времени анализа движения ТС
     */
    @RequestMapping(value = "/exploitation", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public ExploitationListData responseExploitation(@RequestParam("sh_id") String strTtVariantID,
                                                     @RequestParam("begin_time") String strBeginTime,
                                                     @RequestParam("end_time") String strEndTime) {
        Integer ttVariantID = TypeConvert.toInteger(strTtVariantID);
        LocalTime beginTime = TypeConvert.toLocalTime(strBeginTime);
        LocalTime endTime = TypeConvert.toLocalTime(strEndTime);
        return dataProvider
            .getExploitationListData(ttVariantID, beginTime, endTime, defaultClientTimeZone);
    }

    /**
     * Среднее значение пассажоропотока по маршруту
     * http://localhost:8080/getMarshPsgAvgValue?mr_id=1&num_dow=1&period_starttime=22&perion_endtime=33
     * http://ebnd.advantum.dev/gis/getMarshPsgAvgValue?mr_id=1&num_dow=1&period_starttime=22&perion_endtime=33
     *
     * @param strRouteID
     * @param strWeekMask
     * @param strStartTime
     * @param strEndTime
     */
    @RequestMapping(value = "/getMarshPsgAvgValue", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public MarshPsgAvgValueData responseMarshPsgAvgValue(@RequestParam("mr_id") String strRouteID,
                                                         @RequestParam("num_dow") String strWeekMask,
                                                         @RequestParam("period_starttime") String strStartTime,
                                                         @RequestParam("perion_endtime") String strEndTime) {
        return dataProvider.getMarshPsgAvgValueData(strRouteID, strWeekMask, strStartTime, strEndTime);
    }

    /**
     * Среднее значение пассажоропотока по остановочному пункту
     * http://localhost:8080/getStopPsgAvgValue?st_id=1&num_dow=1&period_starttime=22&perion_endtime=33
     * http://ebnd.advantum.dev/gis/getStopPsgAvgValue?st_id=1&num_dow=1&period_starttime=22&perion_endtime=33
     *
     * @param strStopID
     * @param strWeekMask
     * @param strStartTime
     * @param strEndTime
     */
    @RequestMapping(value = "/getStopPsgAvgValue", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public StopPsgAvgValueListData responseStopPsgAvgValue(@RequestParam("st_id") String strStopID,
                                                           @RequestParam("num_dow") String strWeekMask,
                                                           @RequestParam("period_starttime") String strStartTime,
                                                           @RequestParam("perion_endtime") String strEndTime) {
        return dataProvider.getStopPsgAvgValueListData(strStopID, strWeekMask, strStartTime, strEndTime);
    }

    /**
     * Среднее плановое время оборотного рейса маршрута
     * http://localhost:8080/getRacePlanTime?rv_num=11
     * http://ebnd.advantum.dev/gis/getRacePlanTime?rv_num=11
     *
     * @param strRaveVariantNum
     */
    @RequestMapping(value = "/getRacePlanTime", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public RacePlanTimeListData responseRacePlanTime(@RequestParam("rv_num") String strRaveVariantNum) {
        return dataProvider.getRacePlanTimeListData(strRaveVariantNum);
    }

    /**
     * Список гаражных номеров маршрута с навигационными отметками
     * http://localhost:8080/getNavGrNum?mr_id=1&begin_date=2016-12-27 09:35:00&end_date=2016-12-27 09:36:00
     * http://localhost:8080/getNavGrNum?mr_id=1&begin_date=2016-12-27 09:35:00&end_date=2016-12-28 09:36:00
     * http://ebnd.advantum.dev/gis/getNavGrNum?mr_id=1&begin_date=2016-12-27 09:35:00&end_date=2016-12-27 09:36:00
     * http://ebnd.advantum.dev/gis/getNavGrNum?mr_id=1&begin_date=2016-12-27 09:35:00&end_date=2016-12-28 09:36:00
     *
     * @param routeID
     * @param strBeginDate
     * @param strEndDate
     */
    @RequestMapping(value = "/getNavGrNum", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public GrNumListData responseNavGrNum(@RequestParam("mr_id") Long routeID,
                                          @RequestParam("begin_date") List<String> strBeginDate,
                                          @RequestParam("end_date") List<String> strEndDate) {

        List<LocalDateTime> beginDate = new ArrayList<>();
        List<LocalDateTime> endDate = new ArrayList<>();
        for (int i = 0; i < strBeginDate.size(); i++) {
            beginDate.add(TypeConvert.toLocalDateTime(strBeginDate.get(i)));
            endDate.add(TypeConvert.toLocalDateTime(strEndDate.get(i)));
        }

        Duration duration = Duration.ZERO;
        for (int i = 0; i < beginDate.size(); i++) {
            duration = Duration.between(beginDate.get(i), endDate.get(i)).plus(duration);
            if (duration.compareTo(maxRequestedPeriod) > 0) {
                throw new IllegalArgumentException(
                    "Requested period is too long. Maximum total duration  in days: " + maxRequestedPeriod
                        .toDays());
            }
        }
        return dataProvider.getGrNumListData(routeID, beginDate, endDate);
    }

    /**
     * Навигационные отметки по маршрутам
     * http://localhost:8080/getNavPoints?mr_id=1&begin_date=2016-12-27 09:35:00&end_date=2016-12-27 09:36:00
     * http://localhost:8080/getNavPoints?mr_id=1&begin_date=2016-12-27 09:35:00&end_date=2016-12-27 09:36:00&gr_num=1
     * <p>
     * http://ebnd.advantum.dev/gis/getNavPoints?mr_id=1&begin_date=2016-12-27 09:35:00&end_date=2016-12-27 09:36:00
     * http://ebnd.advantum.dev/gis/getNavPoints?mr_id=1&begin_date=2016-12-27 09:35:00&end_date=2016-12-28 09:36:00&gr_num=gr num 1
     *
     * @param routeID      идентификаторы маршрутов
     * @param strBeginDate периоды времени дата/время с, по
     * @param strEndDate   периоды времени дата/время с, по
     * @param lonmin       долгота минимум
     * @param lonmax       долгота максимум
     * @param latmin       долгота максимум
     * @param latmax       широта максимум
     * @param headingmin   направление движения минимум
     * @param headingmax   направление движения максимум
     * @param speedmin     скорость минимум
     * @param speedmax     скорость максимум
     * @param grNum        гаражные номера ТС
     */
    @RequestMapping(value = "/getNavPoints", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public NavPointListData responseNavPoints(@RequestParam("mr_id") List<Long> routeID,
                                              @RequestParam("begin_date") List<String> strBeginDate,
                                              @RequestParam("end_date") List<String> strEndDate,
                                              @RequestParam("longmin") Float lonmin,
                                              @RequestParam("longmax") Float lonmax,
                                              @RequestParam("latmin") Float latmin,
                                              @RequestParam("latmax") Float latmax,
                                              @RequestParam("headingmin") Integer headingmin,
                                              @RequestParam("headingmax") Integer headingmax,
                                              @RequestParam("speedmin") Integer speedmin,
                                              @RequestParam("speedmax") Integer speedmax,
                                              @RequestParam("gr_num") List<Integer> grNum) {

        List<LocalDateTime> beginDate = new ArrayList<>();
        List<LocalDateTime> endDate = new ArrayList<>();
        for (int i = 0; i < strBeginDate.size(); i++) {
            beginDate.add(TypeConvert.toLocalDateTime(strBeginDate.get(i)));
            endDate.add(TypeConvert.toLocalDateTime(strEndDate.get(i)));
        }

        Duration duration = Duration.ZERO;
        for (int i = 0; i < beginDate.size(); i++) {
            duration = Duration.between(beginDate.get(i), endDate.get(i)).plus(duration);
            if (duration.compareTo(maxRequestedPeriod) > 0) {
                throw new IllegalArgumentException(
                    "Requested period is too long. Maximum total duration  in days: " + maxRequestedPeriod
                        .toDays());
            }
        }

        return dataProvider.getNavPointListData(
            routeID,
            beginDate,
            endDate,
            lonmin,
            lonmax,
            latmin,
            latmax,
            headingmin,
            headingmax,
            speedmin,
            speedmax,
            grNum
        );
    }

    /**
     * Статус подключения ИТОП
     * http://ebnd.advantum.dev/gis/getIbrdConnections
     *
     * @param strIbrdMuid идентификатор ИТОП ГИС
     */
    @RequestMapping(value = "/getIbrdConnections", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public IbrdConnectionListData ibrdConnections(@RequestParam("ibrd_muid") String strIbrdMuid) {
        Long ibrdMuid = TypeConvert.toLong(strIbrdMuid);
        return dataProvider.getIbrdConnectionListData(ibrdMuid, defaultClientTimeZone);
    }

    /**
     * Координаты местонахождения ИТОП
     * http://ebnd.advantum.dev/gis/getIbrdTelematic?begin_date=2017-07-25 05:00:00&end_date=2017-07-25 12:00:00
     * http://localhost:8080/getIbrdTelematic?begin_date=2017-07-25 05:00:00&end_date=2017-07-25 12:00:00
     * http://ebnd.advantum.dev/gis/getIbrdTelematic?begin_date=2017-12-12 05:00:00&end_date=2017-12-12 12:00:00
     *
     * @param strBeginDate
     * @param strEndDate
     */
    @RequestMapping(value = "/getIbrdTelematic", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public IbrdCoordListData ibrdCoord(@RequestParam("begin_date") String strBeginDate,
                                       @RequestParam("end_date") String strEndDate) {
        LocalDateTime beginDate = TypeConvert.toLocalDateTime(strBeginDate);
        LocalDateTime endDate = TypeConvert.toLocalDateTime(strEndDate);
        return dataProvider.getIbrdCoordListData(beginDate, endDate, defaultClientTimeZone);
    }

    /**
     * Пассажиропоток на маршруте
     *
     * @param strRouteMuid Идентификатор маршрута
     * @param strBeginDate Временной период
     * @param strEndDate   Временной период
     * @param weekday      Код(-ы) дня недели
     */
    @RequestMapping(value = "gis/routeTrafficByPeriods", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public TrafficPass traffic(@RequestParam("mr_id") String strRouteMuid,
                               @RequestParam("start_date") String strBeginDate,
                               @RequestParam("end_date") String strEndDate,
                               @RequestParam("weekday") List<String> weekday) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        Long routeMuid = TypeConvert.toLong(strRouteMuid);
        LocalDate beginDate = LocalDate.parse(strBeginDate, formatter);
        LocalDate endDate = LocalDate.parse(strEndDate, formatter);
        return dataProvider.getTrafficPass(routeMuid, beginDate, endDate, weekday);
    }

    /**
     * Пассажиропоток на остановке
     *
     * @param strStopMuid  Идентификатор остановочного пункта
     * @param strBeginDate Временной период
     * @param strEndDate   Временной период
     * @param weekday      Код(-ы) дня недели
     */
    @RequestMapping(value = "gis/stopPlaceTrafficByPeriods", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public TrafficPassOnStop trafficOnStop(@RequestParam("stop_id") String strStopMuid,
                                           @RequestParam("start_date") String strBeginDate,
                                           @RequestParam("end_date") String strEndDate,
                                           @RequestParam("weekday") List<String> weekday) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate beginDate = LocalDate.parse(strBeginDate, formatter);
        LocalDate endDate = LocalDate.parse(strEndDate, formatter);
        return dataProvider.getTrafficPassOnStop(strStopMuid, beginDate, endDate, weekday);
    }

    /**
     * Показания температурных датчиков
     * http://localhost:8124/TcODDTermo/getSensorTermo?begin_date=2018-05-03
     * http://ebnd.advantum.ru/gis/TcODDTermo/getSensorTermo?begin_date=2018-05-03
     *
     * @param strBeginDate Дата запроса
     */
    @RequestMapping(value = "TcODDTermo/getSensorTermo", method = RequestMethod.GET)
    public StreamingResponseBody getSensorTermo(@RequestParam("begin_date") String strBeginDate) {
        final LocalDate beginDate = LocalDate.parse(strBeginDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        return out -> dataProvider.getSensorTermo(beginDate, out);
    }

    @Configuration
    @EnableSwagger2
    public static class SwaggerConfig extends WebMvcConfigurerAdapter {
        @Override
        public void addResourceHandlers(ResourceHandlerRegistry registry) {
            registry.addResourceHandler("/**").addResourceLocations("classpath:/META-INF/resources/");
        }
    }
}
