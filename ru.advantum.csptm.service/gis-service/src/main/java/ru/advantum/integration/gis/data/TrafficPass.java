package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="xml")
public class TrafficPass {
    @XmlElement(name = "mr_id")
    public Long mr_id;

    @XmlElement(name = "start_date")
    public String startDate;

    @XmlElement(name = "end_date")
    public String endDate;

    @XmlElement(name = "weekday")
    public String weekday;

    @XmlElement(name = "time_codes")
    public List<TimeCode> list = new ArrayList<>();
}


