package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

class RaceData {

    @XmlElement(name = "mv_id")
    public Long marshVariantID;

    @XmlElement(name = "rl_racetype")
    public String raceType;

    @XmlElement(name = "rl_racetypeext")
    public String raceTypeExt;

    @XmlElement(name = "rl_firststation_id")
    public Long firstStopID;

    @XmlElement(name = "rl_laststation_id")
    public Long lastsStopID;
}

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "RaceList")
public class RaceListData {

    @XmlElement(name = "Race")
    public List<RaceData> list = new ArrayList<>();
}
