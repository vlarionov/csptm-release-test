package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;


class RaspEventsData {

    @XmlElement(name = "srv_id")
    public Long serverID;

    @XmlElement(name = "rv_id")
    public Long raspVariantID;

    @XmlElement(name = "gr_id")
    public Long graphShiftID;

    @XmlElement(name = "re_orderby")
    public Long orderBy;

    @XmlElement(name = "re_graph")
    public Integer graph;

    @XmlElement(name = "re_smena")
    public String smena;

    @XmlElement(name = "re_starttime")
    public Integer startTime;

    @XmlElement(name = "re_endtime")
    public Integer endTime;

    @XmlElement(name = "ev_id")
    public Integer event_id;

    @XmlElement(name = "re_distance")
    public Double distance;

    @XmlElement(name = "re_nulltype")
    public Integer nullType;
}

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "RaspEvents")
public class RaspEventsListData {

    @XmlElement(name = "RaspEvent")
    public List<RaspEventsData> list = new ArrayList<>();
}
