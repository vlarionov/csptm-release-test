package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

class RaspHolidaysData {

    @XmlElement(name = "mr_id")
    public Integer route_id;

    @XmlElement(name = "rh_date")
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDateTime holidaysDate;

    @XmlElement(name = "rh_dow")
    public Integer week_mask;
}

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "RaspHolidays")
public class RaspHolidaysListData {

    @XmlElement(name = "RaspHoliday")
    public List<RaspHolidaysData> list = new ArrayList<>();
}
