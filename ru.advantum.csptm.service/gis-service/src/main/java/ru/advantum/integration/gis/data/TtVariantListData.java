package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

class TtVariantRouteData {

    /** ID рейса */
    @XmlElement(name = "r_id")
    public Long roundID;

    /** Тип рейса */
    @XmlElement(name = "r_type")
    public String roundType;

    /** Код производственного рейса */
    @XmlElement(name = "r_code")
    public String roundCode;
}

class TtVariantData {

    /** ID варианта расписания */
    @XmlElement(name = "sh_id")
    public Long ttVariantID;

    /** Дата начала действия */
    @XmlElement(name = "begin_date")
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDateTime dateBegin;

    /** Дата окончания действия */
    @XmlElement(name = "end_date")
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDateTime dateEnd;

    /** Битовая маска дней недели */
    @XmlElement(name = "week_days")
    public Integer week_days;

    /** Признак «действующего варианта расписания» */
    @XmlElement(name = "is_active")
    public Boolean isActive;

    /** Признак «основного расписания для вычисления эксплуатационных показателей маршрута» */
    @XmlElement(name = "is_main")
    public Boolean isMain;

    /** Контрольная сумма по данным расписания */
    @XmlElement(name = "checksum")
    public String checksum;

    /** Рейсы */
    @XmlElement(name = "round")
    public List<TtVariantRouteData> rounds = new ArrayList<>();
}

/**
 * Данные о вариантах расписания
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "sh_variants")
public class TtVariantListData {

    /** ID маршрута ГИС */
    @XmlElement(name = "mr_id")
    public Long routeMuid;

    /** ID варианта маршрута ГИС */
    @XmlElement(name = "mr_var_id")
    public Long routeVariantMuid;

    /** Варианты расписания */
    @XmlElement(name = "sh_variant")
    public List<TtVariantData> list = new ArrayList<>();
}
