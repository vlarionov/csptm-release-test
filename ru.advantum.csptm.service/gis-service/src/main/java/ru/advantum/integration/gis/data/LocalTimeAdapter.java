package ru.advantum.integration.gis.data;

import ru.advantum.integration.utils.TypeConvert;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

import static java.time.temporal.ChronoField.HOUR_OF_DAY;
import static java.time.temporal.ChronoField.MINUTE_OF_HOUR;

/**
 * @author abc <alekseenkov@advantum.pro>
 */
public class LocalTimeAdapter extends XmlAdapter<String, LocalDateTime> {

    private static final DateTimeFormatter LOCAL_TIME;

    static {
        LOCAL_TIME = new DateTimeFormatterBuilder()
                .appendValue(HOUR_OF_DAY, 2)
                .appendLiteral(':')
                .appendValue(MINUTE_OF_HOUR, 2)
                .toFormatter();
    }

    @Override
    public LocalDateTime unmarshal(String v) throws Exception {
        return TypeConvert.toLocalDateTime(v);
    }

    @Override
    public String marshal(LocalDateTime v) throws Exception {
        return v.format(LOCAL_TIME);
    }
}
