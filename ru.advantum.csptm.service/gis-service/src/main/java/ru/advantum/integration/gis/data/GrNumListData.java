package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

class GrNumData {

    @XmlElement(name = "gr_num")
    public String grNum;

    @XmlElement(name = "park_name")
    public String parkName;

    @XmlElement(name = "park_id")
    public Long parkId;

    @XmlElement(name = "nav_point_count")
    public Long navPointCount;
}

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "GrNumList")
public class GrNumListData {

    @XmlElement(name = "GrNum")
    public List<GrNumData> list = new ArrayList<>();
}
