package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

class StopPsgAvgValueData {

    @XmlElement(name = "mr_id")
    public Long route_id;

    @XmlElement(name = "stop_avg_in")
    public Double value_in;

    @XmlElement(name = "stop_avg_out")
    public Double value_out;
}

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "StopPsgAvgValueList")
public class StopPsgAvgValueListData {

    @XmlElement(name = "StopPsgAvgValue")
    public List<StopPsgAvgValueData> list = new ArrayList<>();
}
