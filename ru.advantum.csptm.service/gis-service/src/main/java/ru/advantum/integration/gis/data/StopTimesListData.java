package ru.advantum.integration.gis.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

class RaspTimeAction {

    /** Идентификатор рейса ГИС */
    @XmlElement(name = "r_id")
    public Long raceMuid;

    /** Тип рейса */
    @XmlElement(name = "r_type")
    public String raceType;

    /** Код рейса */
    @XmlElement(name = "r_code")
    public String routeCode;

    /** Идентификатор траектории ГИС */
    @XmlElement(name = "track_id")
    public Long trackID;

    /** Тип траектории */
    @XmlElement(name = "track_type")
    public Integer trackType;

    /** Номер выхода */
    @XmlElement(name = "out_num")
    public String outNum;

    /** Номер текущей смены */
    @XmlElement(name = "sm_num")
    public Integer shiftNum;

    /** Порядковый номер рейса для выхода */
    @XmlElement(name = "out_order_num")
    public Integer outOrderNum;

    /** Порядковый номер места посадки-высадки в траектории */
    @XmlElement(name = "op_num")
    public Integer opNum;

    /** Порядковый номер вхождения остановки в траекторию */
    @XmlElement(name = "op_entry_number")
    public Integer opEntryNumber;

    /** Время прибытия на место посадки-высадки */
    @XmlElement(name = "begin_time")
    @XmlJavaTypeAdapter(LocalTimeAdapter.class)
    public LocalDateTime timeBegin;

    /** Время прибытия на место посадки-высадки в виде количества минут от 00:00 первых суток */
    @XmlElement(name = "begin_time_cnt")
    public Integer timeBeginMinute;

    /** Время отправления с места посадки-высадки */
    @XmlElement(name = "end_time")
    @XmlJavaTypeAdapter(LocalTimeAdapter.class)
    public LocalDateTime timeEnd;

    /** Время отправления с места посадки-высадки в виде количества минут от 00:00 первых суток */
    @XmlElement(name = "end_time_cnt")
    public Integer timeEndMinute;
}


class StopTimesData {

    /** Идентификатор места посадки-высадки ГИС */
    @XmlElement(name = "op_id")
    public Long stopPlaceID;

    /** Тип места посадки-высадки */
    @XmlElement(name = "op_type")
    public String stopPlaceTypeID;

    /** Событие прохождения места посадки-высадки */
    @XmlElement(name = "action_op")
    public List<RaspTimeAction> actions = new ArrayList<>();
}

/**
 * Данные о временах отправлений
 * @author abc <alekseenkov@advantum.pro>
 */
@XmlRootElement(name = "stop_times")
public class StopTimesListData {

    /** ID варианта расписания */
    @XmlElement(name = "sh_id")
    public Long ttVariantID;

    /** Место посадки-высадки */
    @XmlElement(name = "op")
    public List<StopTimesData> list = new ArrayList<>();
}
