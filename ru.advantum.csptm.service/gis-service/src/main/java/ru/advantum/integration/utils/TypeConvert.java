package ru.advantum.integration.utils;

import java.sql.Timestamp;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * Конвертирование типов
 * @author abc <alekseenkov@advantum.pro>
 */
public class TypeConvert {
    public static final LocalDateTime EMPTY_DATE = LocalDateTime.parse("1900-01-01T00:00:00");

    private static final DateTimeFormatter dateFormat10_1 = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private static final DateTimeFormatter dateFormat19_1 = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
    public static final DateTimeFormatter dateFormat19_2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private static final DateTimeFormatter dateFormat16_1 = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
    private static final DateTimeFormatter dateFormat16_2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    // === _to Date

    private static LocalDateTime innerToLocalDateTime(String source, DateTimeFormatter formatter) {
        LocalDateTime result = null;
        try {
            result = LocalDateTime.parse(source, formatter);
        } catch (DateTimeParseException noop) {
        }
        return result;
    }

    private static LocalDate innerToLocalDate(String source, DateTimeFormatter formatter) {
        LocalDate result = null;
        try {
            result = LocalDate.parse(source, formatter);
        } catch (DateTimeParseException noop) {
        }
        return result;
    }

    private static LocalTime innerToLocalTime(String source, DateTimeFormatter formatter) {
        LocalTime result = null;
        try {
            result = LocalTime.parse(source, formatter);
        } catch (DateTimeParseException noop) {
        }
        return result;
    }

    public static LocalDateTime toLocalDateTime(String source) {
        LocalDateTime result = null;

        if (source != null) {
            if (source.length() == 10) {
                LocalDate date = innerToLocalDate(source, dateFormat10_1);
                if (date == null) {
                    date = innerToLocalDate(source, DateTimeFormatter.ISO_LOCAL_DATE);
                }

                if (date != null) result = date.atStartOfDay();
            } else if (source.length() == 16) {
                result = innerToLocalDateTime(source, dateFormat16_1);
                if (result == null) {
                    result = innerToLocalDateTime(source, dateFormat16_2);
                    if (result == null) {
                        result = innerToLocalDateTime(source, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
                    }
                }
            } else if (source.length() == 19) {
                result = innerToLocalDateTime(source, dateFormat19_1);
                if (result == null) {
                    result = innerToLocalDateTime(source, dateFormat19_2);
                    if (result == null) {
                        result = innerToLocalDateTime(source, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
                    }
                }
            }
        }

        return result;
    }

    public static LocalTime toLocalTime(String source) {
        return innerToLocalTime(source, DateTimeFormatter.ISO_LOCAL_TIME);
    }

    public static Timestamp toTimestamp(LocalDateTime date) {
        if (date == null) return null;
        return Timestamp.valueOf(date);
    }

    public static LocalDateTime toServerLocalDateTime(LocalDateTime dateTime, ZoneId clientZone) {
        if (dateTime == null) return null;
        return dateTime.atZone(clientZone).withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime();
    }

    public static LocalDateTime toClientLocalDateTime(LocalDateTime dateTime, ZoneId clientZone) {
        if (dateTime == null) return null;
        return dateTime.atZone(ZoneOffset.UTC).withZoneSameInstant(clientZone).toLocalDateTime();
    }

    // === _to Number

    public static Integer toInteger(String s) {
        Integer r = null;
        if (s != null && !s.trim().isEmpty() && !s.trim().toLowerCase().equals("null")) r = Integer.valueOf(s);
        return r;
    }

    public static Integer toInteger(Object o) {
        Integer r = null;
        if (o != null) r = ((Number) o).intValue();
        return r;
    }

    public static Long toLong(String s) {
        Long r = null;
        if (s != null && !s.trim().isEmpty() && !s.trim().toLowerCase().equals("null")) r = Long.valueOf(s);
        return r;
    }

    public static Long toLong(Object o) {
        Long r = null;
        if (o != null) r = ((Number) o).longValue();
        return r;
    }

    public static Float toFloat(Object o) {
        Float r = null;
        if (o != null) r = ((Float) o).floatValue();
        return r;
    }
}
