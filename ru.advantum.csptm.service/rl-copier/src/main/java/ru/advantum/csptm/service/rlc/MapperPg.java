package ru.advantum.csptm.service.rlc;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface MapperPg {
    @Results({
            @Result(column = "StationNum", property = "stationNum"),
            @Result(column = "StationType", property = "stationType"),
            @Result(column = "Channel", property = "channel"),
            @Result(column = "UniqueID", property = "uniqueId"),
            @Result(column = "Param", property = "param")
    })
    @Select("select \"StationNum\", \"StationType\", \"Channel\", \"UniqueID\", \"Param\" from usw.rlc")
    List<Entity> getAllPostgres();

}
