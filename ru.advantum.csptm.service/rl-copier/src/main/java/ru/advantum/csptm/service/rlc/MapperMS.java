package ru.advantum.csptm.service.rlc;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface MapperMS {
    @Insert("delete dbo.Opros_YKV")
    void cleanTarget();

    @Insert("insert into dbo.Opros_YKV values (#{stationNum},#{stationType},#{channel},#{uniqueId},#{param})")
    void uploadMicrosoft(Entity entity);
}
