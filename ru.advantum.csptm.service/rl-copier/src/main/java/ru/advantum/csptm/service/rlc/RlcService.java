package ru.advantum.csptm.service.rlc;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class RlcService implements Runnable {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private RlcConfig config;

    private ScheduledThreadPoolExecutor ex = new ScheduledThreadPoolExecutor(1);
    private SqlMapperSingletonStore mssql;
    private SqlMapperSingletonStore pgsql;

    @Override
    public void run() {
        log.info("Start");
        List<Entity> list;
        try {
            try (SqlSession pgs = pgsql.getInstance().openSession()) {
                list = pgs.getMapper(MapperPg.class).getAllPostgres();
                pgs.commit();
            }
            log.info("Got {} records", list.size());
            try (SqlSession mss = mssql.getInstance().openSession()) {
                MapperMS m = mss.getMapper(MapperMS.class);
                m.cleanTarget();
                for (Entity e : list) {
                    m.uploadMicrosoft(e);
                }
                mss.commit();
                log.info("Uploaded {} records", list.size());
            }
        } catch (Exception e) {
            log.error("{}",e);
        }
    }

    public static void main(String... args) throws Exception {
        new RlcService();
    }


    private RlcService() throws Exception {
        config = ru.advantum.config.Configuration.unpackConfig(RlcConfig.class, RlcConfig.CFG_FILE_NAME);
        mssql = new SqlMapperSingletonStore().setConfiguration(config.mssql.getConnectionPool(), MapperMS.class);
        pgsql = new SqlMapperSingletonStore().setConfiguration(config.pgsql.getConnectionPool(), MapperPg.class);

        ex.scheduleAtFixedRate(this, 0, config.peroiodSec, TimeUnit.SECONDS);
    }

}
