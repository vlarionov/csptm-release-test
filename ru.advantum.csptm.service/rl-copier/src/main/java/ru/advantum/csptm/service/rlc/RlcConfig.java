package ru.advantum.csptm.service.rlc;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import ru.advantum.config.common.db.DatabaseConnection;

public class RlcConfig {
    public static final String CFG_FILE_NAME="rlc.xml";

    @Attribute(name="period-sec", required = false)
    public int peroiodSec = 30;

    @Element(name = "mssql")
    public DatabaseConnection mssql = null;

    @Element(name = "pgsql", required = false)
    public DatabaseConnection pgsql = null;

}
