package ru.advantum.csptm.service.rlc;

import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.mapping.VendorDatabaseIdProvider;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.SQLException;

public class SqlMapperSingletonStore {

    protected static final Logger log = LoggerFactory.getLogger(SqlMapperSingletonStore.class);
    private  Environment environment;
    private  Configuration configuration;

    public SqlMapperSingletonStore() {
    }

    public SqlSessionFactory getInstance() {
        return new SqlSessionFactoryBuilder().build(configuration);
    }

    public SqlMapperSingletonStore setConfiguration(DataSource dataSource, Class<?> mapper) throws SQLException {
        environment = new Environment("development", new JdbcTransactionFactory(), dataSource);
        configuration = new Configuration(environment);
        configuration.addMapper(mapper);

        final String databaseId = new VendorDatabaseIdProvider().getDatabaseId(dataSource);
        log.info("Database ID {}", databaseId);
        configuration.setDatabaseId(databaseId);

        return this;
    }

}

