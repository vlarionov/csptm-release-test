package test;

import org.junit.Test;
import ru.advantum.config.Configuration;
import ru.advantum.csptm.service.rlc.RlcConfig;

public class CommonTests {

    public static RlcConfig getConfig() {
        if (System.getProperty("advantum.config.dir") == null) {
            System.setProperty("advantum.config.dir", "src/main/resources/extras/examples");
        }
        try {
            return Configuration.unpackConfig(RlcConfig.class, RlcConfig.CFG_FILE_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Test
    public void configTest() throws Exception {
        RlcConfig config = getConfig();
        if (config != null) {
            System.out.println(config.toString());
//            SessionFactoryManager.init(config);
//            log.info("pgsql conn OK: " + SessionFactoryManager.PostgreSqlSession.getRadioItemList(1).size());
        } else {
            System.err.println("Config not found");
        }
    }

}
