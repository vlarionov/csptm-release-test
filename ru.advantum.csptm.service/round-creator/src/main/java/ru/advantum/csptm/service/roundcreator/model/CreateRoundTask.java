package ru.advantum.csptm.service.roundcreator.model;

public class CreateRoundTask {

    private int startStopItemId;
    private int endStopItemId;

    public CreateRoundTask(int startStopItemId, int endStopItemId) {
        this.startStopItemId = startStopItemId;
        this.endStopItemId = endStopItemId;
    }

    public int getStartStopItemId() {
        return startStopItemId;
    }

    public void setStartStopItemId(int startStopItemId) {
        this.startStopItemId = startStopItemId;
    }

    public int getEndStopItemId() {
        return endStopItemId;
    }

    public void setEndStopItemId(int endStopItemId) {
        this.endStopItemId = endStopItemId;
    }

}
