package ru.advantum.csptm.service.roundcreator.service;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.service.roundcreator.model.CreateRoundTask;

import java.util.List;

@Service
public class TaskReceiver {

    private final RabbitTemplate rabbitTemplate;
    private final RoundCreator roundCreator;

    @Autowired
    public TaskReceiver(RabbitTemplate rabbitTemplate, RoundCreator roundCreator) {
        this.rabbitTemplate = rabbitTemplate;
        this.roundCreator = roundCreator;
    }

    @RabbitListener
    public void receiveMessage(CreateRoundTask createRoundTask) {
        List<Integer> graphSections = roundCreator.findPathBetween(createRoundTask.getStartStopItemId(), createRoundTask.getEndStopItemId());
        rabbitTemplate.convertAndSend(graphSections);
    }
}
