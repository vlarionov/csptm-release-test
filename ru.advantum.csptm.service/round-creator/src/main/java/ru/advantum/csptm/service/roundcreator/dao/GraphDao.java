package ru.advantum.csptm.service.roundcreator.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.StatementType;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import ru.advantum.csptm.service.roundcreator.model.GraphSection;

import java.sql.SQLException;
import java.util.List;

@Repository
public class GraphDao {

    private final GraphDaoMapper graphDaoMapper;

    @Autowired
    public GraphDao(GraphDaoMapper graphDaoMapper) throws SQLException {
        this.graphDaoMapper = graphDaoMapper;
    }

    public int findNodeByStopItemId(int stopItemId) {
        return graphDaoMapper.findNodeByStopItemId(stopItemId);
    }

    public List<Integer> getRoundNodeIds(List<Integer> nodeBeginIdList, List<Integer> nodeEndIdList) {
        return graphDaoMapper.getRoundNodesId(nodeBeginIdList, nodeEndIdList);
    }

    /**
     * Строит граф по всем маршрутам
     */
    @Cacheable("graph")
    public SimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge> getGraph() {
        SimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge> graph = new SimpleDirectedWeightedGraph<>(DefaultWeightedEdge.class);
        graphDaoMapper.findAllSectionForGraph().forEach(graphSection -> {
            graph.addVertex(graphSection.getNodeBeginId());
            graph.addVertex(graphSection.getNodeEndId());
            DefaultWeightedEdge e = graph.addEdge(graphSection.getNodeBeginId(), graphSection.getNodeEndId());
            if (e != null) {
                graph.setEdgeWeight(e, graphSection.getLength());
            }
        });
        return graph;
    }

    @Mapper
    public interface GraphDaoMapper {

        @Select("select mnt.find_node_id_by_stop_item_id(#{stopItemId})")
        int findNodeByStopItemId(@Param("stopItemId") int stopItemId);

        @Select("SELECT mnt.find_shortest_round(" +
                "#{nodeBeginIdList,typeHandler=ru.advantum.csptm.service.roundcreator.dao.IntegerListHandler}," +
                "#{nodeEndIdList,typeHandler=ru.advantum.csptm.service.roundcreator.dao.IntegerListHandler}" +
                ")")
        @Options(useCache = false, statementType = StatementType.CALLABLE)
        List<Integer> getRoundNodesId(@Param("nodeBeginIdList") List<Integer> nodeBeginIdList,
                                      @Param("nodeEndIdList") List<Integer> nodeEndIdList);

        @Select("SELECT rts.graph_section.node_begin_id, rts.graph_section.node_end_id ,rts.graph_section.length from rts.graph_section")
        List<GraphSection> findAllSectionForGraph();
    }

}
