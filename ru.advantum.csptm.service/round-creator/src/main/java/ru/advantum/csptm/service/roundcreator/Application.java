package ru.advantum.csptm.service.roundcreator;

import com.google.common.cache.CacheBuilder;
import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.context.annotation.Bean;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
@EnableCaching
public class Application {
    public static final String CONFIG_FILE_NAME = "create-manager.xml";

    public static void main(String[] args) {
        new SpringApplicationBuilder(Application.class)
                .web(false)
                .bannerMode(Banner.Mode.LOG)
                .run(args);
    }

    @Bean
    public static CreateManagerConfig applicationConfig() throws Exception {
        return ru.advantum.config.Configuration.unpackConfig(
                CreateManagerConfig.class,
                CONFIG_FILE_NAME
        );
    }

    @Bean
    public JepRmqServiceConfig config(CreateManagerConfig config) {
        return config.rmqServiceConfig;
    }

    @Bean
    public DataSource dataSource(CreateManagerConfig config) throws SQLException {
        return config.databaseConfig.getConnectionPool();
    }

    @Bean
    public CacheManager cacheManager() {
        GuavaCacheManager guavaCacheManager = new GuavaCacheManager();
        guavaCacheManager.setCacheBuilder(CacheBuilder.newBuilder().expireAfterWrite(1, TimeUnit.DAYS));
        return guavaCacheManager;
    }

}