package ru.advantum.csptm.service.roundcreator.service;

import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.advantum.csptm.service.roundcreator.dao.GraphDao;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoundCreator {

    private final GraphDao graphDao;

    @Autowired
    public RoundCreator(GraphDao graphDao) {
        this.graphDao = graphDao;
    }

    /**
     * @return список id секций rts.graph_section.graph_section_id в кратчайшем маршруте
     */
    public List<Integer> findPathBetween(int beginStopItemId, int endStopItemId) {
        List<DefaultWeightedEdge> weightedEdges = DijkstraShortestPath.findPathBetween(graphDao.getGraph(), graphDao.findNodeByStopItemId(beginStopItemId), endStopItemId).getEdgeList();
        List<Integer> nodeBeginIdList = new ArrayList<>();
        List<Integer> nodeEndIdList = new ArrayList<>();

        for (DefaultWeightedEdge weightedEdge : weightedEdges) {
            String str = weightedEdge.toString().substring(1, weightedEdge.toString().length() - 1);
            nodeBeginIdList.add(Integer.parseInt(str.split(":")[0].trim()));
            nodeEndIdList.add(Integer.parseInt(str.split(":")[1].trim()));
        }
        return graphDao.getRoundNodeIds(nodeBeginIdList, nodeEndIdList);
    }
}
