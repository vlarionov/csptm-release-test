package ru.advantum.csptm.service.roundcreator;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;


@Root(name = "create-manager")
public class CreateManagerConfig {
    @Element(name = "rmq-service")
    public JepRmqServiceConfig rmqServiceConfig;

    @Element(name = "database")
    public DatabaseConnection databaseConfig;

}