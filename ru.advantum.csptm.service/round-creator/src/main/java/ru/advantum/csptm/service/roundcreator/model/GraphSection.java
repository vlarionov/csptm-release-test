package ru.advantum.csptm.service.roundcreator.model;

public class GraphSection {

    private int nodeBeginId;
    private int nodeEndId;
    private double length;

    public GraphSection(Integer nodeBeginId, Integer nodeEndId, Double length) {
        this.nodeBeginId = nodeBeginId;
        this.nodeEndId = nodeEndId;
        this.length = length;
    }

    public int getNodeBeginId() {
        return nodeBeginId;
    }

    public void setNodeBeginId(int nodeBeginId) {
        this.nodeBeginId = nodeBeginId;
    }

    public int getNodeEndId() {
        return nodeEndId;
    }

    public void setNodeEndId(int nodeEndId) {
        this.nodeEndId = nodeEndId;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }
}
