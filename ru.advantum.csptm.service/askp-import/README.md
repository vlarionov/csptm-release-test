askp-import - Сервис импорта данных АСКП
============================

Сервис с периодичностью `download-check-period` загружет файлы с `ftp` в каталог `localDirectory`. После успешной 
загрузки файл перемещается в каталог `import-dir` для загрузки уже в БД. Имена файлов регистрируются в БД `database` 
при помощи `statement-get_file_loaded` и `statement-set_file_loaded` и повторно не загружаются. Файла после успешной 
загрузки из источника не удаляются `delete-source-files`.
 
Файлы из каталога `import-dir` загружаются в БД. 

Файлы c данными о типах билетов `mask="t_*.txt` загружаются JDBC командами insert. 

Файлы c данными о проходах `mask="v_*.txt` загружаются командой Postgres COPY. При загрузке используется партиционирование
по месяцу `partition_type="M"`. Партиции создаются в каталоге `pg-data-dir`

Настройки сервиса 
-----------
askp-import/advconf/askp-import.xml

``````````````    
<askp-import change-check-period="3600" download-check-period="3600">
     
    <ftp server="10.68.1.66"
         username="**"
         password="**"
         remoteDirectory="/"
         localDirectory="F:/Temp/ftp/download">
    </ftp>
    
    <database url="jdbc:postgresql://localhost:5433/advdev"
              program="askp-import"
              username="postgres"
              password="**"
              autoCommit="false"
              poolable="true">

        <pool initialSize="1"
              maxActive="4"
              maxWait="1"
              maxIdle="1"
              maxOpenPreparedStatement="5"
              driverClassName="org.postgresql.Driver"
              validationQuery="select 1"/>
    </database>
    
    <pg-data-dir>F:/postgres/pgdata.95.askp/</pg-data-dir>
    <pg-tablespace>askp</pg-tablespace>
    
    <statement-get_file_loaded>{ ? = call askp.askp_import_pkg$get_flag_file_loaded( ? ) }</statement-get_file_loaded>
    <statement-set_file_loaded>{ ? = call askp.askp_import_pkg$set_flag_file_loaded( ?, ? ) }</statement-set_file_loaded>
    
    <import-task table_name="askp.ticket_type" partition_type="" mask="t_*.txt" order="1">
        <statement>{ call askp.askp_import_pkg$import_ticket_type(?, ?, ?) }</statement>
    </import-task>
    
    <import-task table_name="askp.ticket_oper" partition_type="M" mask="v_*.txt" order="2" fileDateTemplate="yyyyMMdd" partitionColumn="check_time">
        <statement>copy %s (route_id, move_num, ticket_type_id, check_time, ticket_id) FROM STDIN WITH DELIMITER ';' CSV HEADER ENCODING 'WIN1251'</statement>
    </import-task>
    
    <import-dir>F:/Temp/ftp/work</import-dir>
    <delete-source-files>false</delete-source-files>

</askp-import>

``````````````

Настройки логирования 
-----------
askp-import/advconf/log4j.prop
    
