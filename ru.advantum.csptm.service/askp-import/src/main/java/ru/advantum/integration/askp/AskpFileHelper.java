package ru.advantum.integration.askp;

import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 * @author abc <alekseenkov@advantum.pro>
 */
public class AskpFileHelper {

    private final Logger log = LoggerFactory.getLogger(AskpFileHelper.class);
    private final String delimiter;
    private final DateTimeFormatter dateFormat19;
    private final DateTimeFormatter dateFormat8;
    private final Charset charset;
    private final int dateFieldNum;
    private final int BUFFER_SIZE = 2048;

    public AskpFileHelper() {
        this.dateFormat19 = DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm:ss");
        this.dateFormat8 = DateTimeFormatter.ofPattern("yyyyMMdd");
        this.delimiter = ";";
        this.charset = Charset.forName("CP1251");
        this.dateFieldNum = 5;
    }

    public AskpFileHelper(String dateTemplate, String fileDateTemplate, String codePage, String delimiter, int dateFieldNum) {
        this.dateFormat19 = DateTimeFormatter.ofPattern(dateTemplate);
        this.dateFormat8 = DateTimeFormatter.ofPattern(fileDateTemplate);
        this.delimiter = delimiter;
        this.charset = Charset.forName(codePage);
        this.dateFieldNum = dateFieldNum;
    }

    /**
     * Получить список дат файла данных
     *
     * @param file - файл
     * @return
     */
    private Set<LocalDate> getDataPeriods(Path file) {

        final Set<LocalDate> result = new HashSet<>();

        try (BufferedReader br = Files.newBufferedReader(file, this.charset);
             Stream<String> lines = br.lines()) {

            lines.forEachOrdered(line ->
            {
                String[] items = line.split(this.delimiter);
                if (items.length > this.dateFieldNum) {
                    LocalDateTime d = LocalDateTime.parse(items[this.dateFieldNum], this.dateFormat19);
                    if (d != null && !result.contains(d.toLocalDate())) result.add(d.toLocalDate());
                }
            });
        } catch (IOException e) {
            log.error("getDataPeriods", e);
        }

        return result;
    }

    private Path getSafeFileName(String fileDir, String fileTemplate, String fileExt) {

        Path result = Paths.get(fileDir + File.separator + fileTemplate + fileExt);
        if (Files.exists(result)) {
            for (int i = 1; i < 100; i++) {
                result = Paths.get(fileDir + File.separator + fileTemplate + "_" + i + fileExt);
                if (!Files.exists(result)) break;
            }
        }
        return result;
    }

    private void copyToResultFiles(Path inFile, Path outFile, LocalDate checkDate) {

        try (final BufferedWriter writer = Files.newBufferedWriter(outFile, this.charset);
             final BufferedReader br = Files.newBufferedReader(inFile, this.charset);
             final Stream<String> lines = br.lines()) {

            lines.forEachOrdered(line ->
            {
                String[] items = line.split(this.delimiter);
                if (items.length > this.dateFieldNum) {
                    LocalDateTime d = LocalDateTime.parse(items[this.dateFieldNum], this.dateFormat19);
                    if (d != null && d.toLocalDate().equals(checkDate)) {
                        try {
                            writer.write(line);
                            writer.newLine();
                        } catch (IOException e) {
                            log.error("copyToResultFiles", e);
                        }
                    }
                }
            });
        } catch (IOException e) {
            log.error("copyToResultFiles", e);
        }
    }

    public String getOrgFileName(String fileName) {
        if (fileName.length() > 19) {
            return fileName.substring(0, 10) + "." + fileName.split("\\.")[1];
        } else {
            return fileName;
        }
    }

    public LocalDate getFileDateFromFileName(String s) {
        return LocalDate.parse(s.substring(2, 10), dateFormat8);
    }

    public LocalDate getDataDateFromFileName(String s) {
        if (s.length() > 19) {
            return LocalDate.parse(s.substring(11, 19), dateFormat8);
        } else {
            return getFileDateFromFileName(s);
        }
    }

    public void moveToTempDir(Path file, String tempDir) {

        String fileNamePart = file.getFileName().toString().split("\\.")[0];
        String fileExt = "." + file.getFileName().toString().split("\\.")[1];

        Set<LocalDate> dates = getDataPeriods(file);

        try {
            if (dates.size() == 0) {
                Path o = Paths.get(tempDir + File.separator + file.getFileName().toString());
                Files.move(file, o, REPLACE_EXISTING);
            } else if (dates.size() == 1) {

                LocalDate fd = getFileDateFromFileName(file.getFileName().toString());
                LocalDate dd = dates.iterator().next();
                String newFilename = fileNamePart;

                if (!fd.equals(dd)) {
                    newFilename = fileNamePart + "_" + dd.format(this.dateFormat8);
                    log.info("Bad date in file name. Renamed to " + newFilename + fileExt);
                }
                Path o = getSafeFileName(tempDir, newFilename, fileExt);
                Files.move(file, o, REPLACE_EXISTING);

            } else if (dates.size() > 1) {

                for (LocalDate dd : dates) {
                    Path o = getSafeFileName(tempDir, fileNamePart + "_" + dd.format(this.dateFormat8), fileExt);
                    copyToResultFiles(file, o, dd);
                    log.info("File " + file.getFileName().toString() + " split to " + o.getFileName().toString());
                }

                Files.delete(file);
            }
        } catch (IOException e) {
            log.error("moveToWorkDir", e);
        }
    }

    private void unarchiveFile(InputStream gzIn, Path archiveFile, Path resultFile) throws IOException {
        try (FileOutputStream out = new FileOutputStream(resultFile.toFile())) {
            log.info("Unarchive file {}", archiveFile);
            int n = 0;
            final byte[] buffer = new byte[BUFFER_SIZE];
            while (-1 != (n = gzIn.read(buffer))) out.write(buffer, 0, n);
        }
    }

    public void uncompressFileIfNeeded(Path archiveFile) {
        if (!Files.exists(archiveFile)) return;
        Path resultFile = null;
        try {
            if (archiveFile.toString().endsWith(".bz2")) {
                try (InputStream gzIn = new BZip2CompressorInputStream(new FileInputStream(archiveFile.toFile()), true)) {
                    resultFile = Paths.get(archiveFile.toString().substring(0, archiveFile.toString().length() - 4));
                    unarchiveFile(gzIn, archiveFile, resultFile);
                }
            } else if (archiveFile.toString().endsWith(".gz")) {
                try (InputStream gzIn = new GzipCompressorInputStream(new FileInputStream(archiveFile.toFile()), true)) {
                    resultFile = Paths.get(archiveFile.toString().substring(0, archiveFile.toString().length() - 3));
                    unarchiveFile(gzIn, archiveFile, resultFile);
                }
            } else if (archiveFile.toString().endsWith(".zip")) {
                try (InputStream gzIn = new ZipArchiveInputStream(new FileInputStream(archiveFile.toFile()))) {
                    resultFile = Paths.get(archiveFile.toString().substring(0, archiveFile.toString().length() - 4));
                    unarchiveFile(gzIn, archiveFile, resultFile);
                }
            } else {
                // Не архивный файл
                return;
            }
        } catch (IOException e) {
            log.error("File unarchive failed", e);
            try {
                // Удаление частично разархивированного файла
                if (resultFile != null && Files.exists(resultFile)) Files.delete(resultFile);
            } catch (IOException sx) {
                log.error("File deleted failed {}  {}", resultFile, e.getMessage());
            }
        }

        try {
            Files.delete(archiveFile);
            log.info("Deleted file {}", archiveFile);
        } catch (IOException e) {
            log.error("File deleted failed {}  {}", archiveFile, e.getMessage());
        }
    }
}
