package ru.advantum.integration.askp;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author abc <alekseenkov@advantum.pro>
 */
public class AskpFtpClient implements AutoCloseable {

    private static final Logger log = LoggerFactory.getLogger(AskpFtpClient.class);

    private final FTPClient ftp = new FTPClient();

    public AskpFtpClient() {
    }

    public void connect(String host, String user, String pwd, int timeout) throws IOException {

        if (this.ftp.isConnected()) return;

        log.info("Connect to FTP server");

        ftp.connect(host);
        int reply = ftp.getReplyCode();
        ftp.setDataTimeout(timeout);
        if (!FTPReply.isPositiveCompletion(reply)) {
            ftp.disconnect();
            throw new IOException("Exception in connecting to FTP Server");
        }
        ftp.login(user, pwd);
        ftp.setFileType(FTP.BINARY_FILE_TYPE);
        ftp.enterLocalPassiveMode();
    }
    
    public void changeWorkingDirectory(String dir) throws IOException {
        ftp.changeWorkingDirectory(dir);
    }

    public String[] listNames(String pathname) throws IOException {
        return ftp.listNames(pathname);
    }

    public FTPFile[] listFiles() throws IOException {
        return ftp.listFiles();
    }

    public boolean downloadFile(String remoteFilePath, String localFilePath) {
        log.info("Start download " + remoteFilePath);
        try (FileOutputStream fos = new FileOutputStream(localFilePath)) {
            this.ftp.retrieveFile(remoteFilePath, fos);
            log.info("File " + remoteFilePath + " downloaded");
            return true;
        } catch (IOException ex) {
            log.error("File download failed", ex);
            try {
                // Удаление частично скаченного файла
                Path localFile = Paths.get(localFilePath);
                if (Files.exists(localFile)) Files.delete(localFile);
            } catch (IOException sx) {
                log.error("Downloaded bad file delete failed", ex.getMessage());
            }
        }
        return false;
    }

    @Override
    public void close() throws IOException {
        if (this.ftp.isConnected()) {
            this.ftp.logout();
            this.ftp.disconnect();
            log.info("Disconnect from FTP server");
        }
    }
}
