package ru.advantum.integration.askp;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@Root(name = "import-task")
public class AskpImportConfigStatement {

    @Attribute(name = "table_name")
    public String tableName;

    /**
     * Тип секционировнаия талицы данных
     * D - ежедневно
     * M - ежемесячно
     * Y - ежегодно
     * пустая строка- секционирование не используется
     */
    @Attribute(name = "partition_type", required = false)
    public String partitionType;

    /**
     * Маска файлов (локальных) с данными
     */
    @Attribute(name = "mask")
    public String fileMask;

    /**
     * Маска файлов (удаленных) с данными
     */
    @Attribute(name = "remote_mask", required = false)
    public String remoteFileMask;

    /**
     * Удаленный каталог с данными
     */
    @Attribute(name = "remote_dir", required = false)
    public String remoteDirectory;

    /**
     * Формат даты в имени файла
     */
    @Attribute(name = "fileDateTemplate", required = false)
    public String fileDateTemplate;

    /**
     * Имя поля секционирования
     */
    @Attribute(name = "partitionColumn", required = false)
    public String partitionColumn;

    /**
     * Имена полей дополнительнях индексов секционированной таблицы
     */
    @Attribute(name = "addIndexColumn1", required = false)
    public String addIndexColumn1;
    @Attribute(name = "addIndexColumn2", required = false)
    public String addIndexColumn2;
    @Attribute(name = "addIndexColumn3", required = false)
    public String addIndexColumn3;
    @Attribute(name = "addIndexColumn4", required = false)
    public String addIndexColumn4;
    @Attribute(name = "addIndexColumn5", required = false)
    public String addIndexColumn5;
    @Attribute(name = "addIndexColumn6", required = false)
    public String addIndexColumn6;
    @Attribute(name = "addIndexColumn7", required = false)
    public String addIndexColumn7;

    @Attribute(name = "batch-size", required = false)
    public int batchSize = 10000;

    /**
     * Команда загрузки. функция или COPY
     */
    @Element(name = "statement")
    public String statementText;

    /**
     * Очередь загрузки
     */
    @Attribute(name = "order")
    private Integer order;

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
}
