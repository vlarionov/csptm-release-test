package ru.advantum.integration.askp;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.pg.loader.PGCopy;
import ru.advantum.pg.partition.PGPartition;
import ru.advantum.pg.partition.PGPartitionBuilder;
import ru.advantum.pg.partition.PartitionBuilder;

import javax.sql.DataSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.SocketException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 * Загрузка данных АСКП
 *
 * @author abc <alekseenkov@advantum.pro>
 */
public class AskpImport {

    private static final Logger log = LoggerFactory.getLogger(AskpImport.class);

    private final AskpImportConfig config;
    private final ScheduledExecutorService fileImportTask = Executors.newSingleThreadScheduledExecutor();
    private final ScheduledExecutorService fileDownloadTask = Executors.newSingleThreadScheduledExecutor();

    private final DataSource datasource;
    private final AskpFileHelper fileHelper;
    private final String tempSubDir = "temp";

    public AskpImport(AskpImportConfig config) throws SQLException {
        this.config = config;
        this.datasource = config.database.getConnectionPool();
        this.fileHelper = new AskpFileHelper();
    }

    private boolean getFlagFileLoaded(String getFileLoadedStatementText, String fileName) throws SQLException {
        try (Connection conn = datasource.getConnection();
             CallableStatement prepareCall = conn.prepareCall(getFileLoadedStatementText)) {
            prepareCall.registerOutParameter(1, Types.INTEGER);
            prepareCall.setString(2, fileName);
            prepareCall.execute();
            return (prepareCall.getInt(1) > 0);
        }
    }

    private void setFlagFileLoaded(String setFileLoadedStatementText, String fileName) throws SQLException {

        String orgFileName = fileHelper.getOrgFileName(fileName);
        LocalDate dataDate = fileHelper.getDataDateFromFileName(fileName);
        LocalDateTime dataTimeDate = LocalDateTime.of(dataDate, LocalTime.MIDNIGHT);

        try (Connection conn = datasource.getConnection();
             CallableStatement prepareCall = conn.prepareCall(setFileLoadedStatementText)) {
            prepareCall.registerOutParameter(1, Types.INTEGER);
            prepareCall.setString(2, orgFileName);
            prepareCall.setTimestamp(3, Timestamp.valueOf(dataTimeDate));
            prepareCall.execute();
            prepareCall.getConnection().commit();
        }
    }

    private void processLine(CallableStatement call, String line, int paramCount) {

        if (line == null || "".equals(line)) return;

        String delimiter = ";";
        String[] items = line.split(delimiter);

        if (items.length < paramCount - 1) {
            log.error("Bad line - " + line);
            return;
        }

        try {
            for (int i = 0; i < items.length; i++) call.setString(i + 1, items[i]);
            call.addBatch();
        } catch (SQLException e) {
            log.error("processLine", e);
        }
    }

    private void loadTxtFile(CallableStatement call, Path fileName, String sourceFileCharset,
                             final int paramCount, final int batchSize) {
        Charset charset = Charset.forName(sourceFileCharset);
        AtomicInteger index = new AtomicInteger();
        try (Stream<String> lines = Files.lines(fileName, charset)) {
            lines.forEachOrdered(line ->
            {
                processLine(call, line, paramCount);
                if (index.incrementAndGet() % batchSize == 0) try {
                    call.executeBatch();
                } catch (SQLException e) {
                    log.error("loadTxtFile", e);
                }
            });
            // insert remaining records
            call.executeBatch();
        } catch (IOException e) {
            log.error("loadTxtFile", e);
        } catch (SQLException e) {
            log.error("loadTxtFile", e);
        }
    }

    private void loadFilesByFunc(String statement, final int batchSize, boolean deleteSourceFiles,
                                 List<Path> files, String sourceFileCharset) {

        final int paramCount = statement.length() - statement.replace("?", "").length();

        try (Connection conn = datasource.getConnection();
             CallableStatement call = conn.prepareCall(statement)) {

            call.registerOutParameter(1, Types.INTEGER);

            for (Path file : files) {
                loadTxtFile(call, file, sourceFileCharset, paramCount, batchSize);
                call.getConnection().commit();
                log.info("\"Succesfully loaded file {}", file.getFileName().toString());

                if (deleteSourceFiles) Files.delete(file);
            }
        } catch (SQLException e) {
            log.error("loadFilesByFunc", e);
        } catch (IOException e) {
            log.error("loadFilesByFunc", e);
        }
    }

    /**
     * Получить имена таблиц для загрузки
     *
     * @param files                Файлы IN
     * @param partitions           Список секций OUT
     * @param partitionForDataFile Таблицы для загрузки файла
     * @param partitionDir         Каталог для PG секций
     * @param tablespaceName       Наименование tablespace
     * @param tableName            Наименование таблицы
     * @param partitionType        Тип секции
     * @param partitionColumn      Поле секционирования
     * @param addIndexColumns      Поля дополнительных индексов
     * @return Список файлов для загрузки
     */
    private List<Path> getPartitions(List<Path> files,
                                     List<PartitionBuilder> partitions,
                                     Map<String, String> partitionForDataFile,
                                     String partitionDir,
                                     String tablespaceName,
                                     String tableName,
                                     String partitionType,
                                     String partitionColumn,
                                     List<String> addIndexColumns) {
        List<Path> filesForLoad = new ArrayList<>();

        for (Path file : files) {
            if ("".equals(partitionType)) {
                // Без секций
                filesForLoad.add(file);
            } else {
                final LocalDate d = fileHelper.getDataDateFromFileName(file.getFileName().toString());
                if (null == d) {
                    log.error("Date parsing error from file name: " + file.getFileName().toString());
                } else {
                    // Парисинг и проверка секций
                    final LocalDateTime dt = LocalDateTime.of(d, LocalTime.MIDNIGHT);
                    PartitionBuilder partition;
                    switch (partitionType) {
                        case "D":
                            partition = new PartitionBuilder(partitionDir,
                                    tableName,
                                    String.format("%s%02d%02d", d.getYear() - 2000, d.getMonth().getValue(), d.getDayOfMonth()),
                                    tablespaceName,
                                    partitionColumn,
                                    addIndexColumns,
                                    dt,
                                    dt.plusDays(1));
                            break;
                        case "M":
                            partition = new PartitionBuilder(partitionDir,
                                    tableName,
                                    String.format("%s%02d", d.getYear() - 2000, d.getMonth().getValue()),
                                    tablespaceName,
                                    partitionColumn,
                                    addIndexColumns,
                                    dt.withDayOfMonth(1),
                                    dt.withDayOfMonth(1).plusMonths(1));
                            break;
                        case "Y":
                            partition = new PartitionBuilder(partitionDir,
                                    tableName,
                                    String.format("%s", d.getYear() - 2000),
                                    tablespaceName,
                                    partitionColumn,
                                    addIndexColumns,
                                    dt.withDayOfYear(1),
                                    dt.withDayOfYear(1).minusYears(1));
                            break;
                        default:
                            log.error("Bad table partition type: " + partitionType);
                            continue;
                    }

                    // Список секций
                    final String sf = partition.getSuffix();
                    if (!partitions.stream()
                            .filter(o -> o.getSuffix().equals(sf))
                            .findFirst()
                            .isPresent()) {
                        partitions.add(partition);
                    }

                    // Файл и имя таблицы для импорта
                    partitionForDataFile.put(file.getFileName().toString(), tableName + "_" + partition.getSuffix());
                    filesForLoad.add(file);
                }
            }
        }
        return filesForLoad;
    }

    /**
     * Проверка наличия секций у таблицы
     *
     * @param partitions
     */
    private void checkPartition(List<PartitionBuilder> partitions) {
        try {
            PGPartitionBuilder.getInstance().of_init((BasicDataSource) config.database.getConnectionPool());
            for (PGPartition partition : partitions) {
                PGPartitionBuilder.getInstance().checkPartition(partition);
            }
        } catch (SQLException e) {
            log.error("checkPartition", e);
        }
    }

    private void loadFileByLoadCommand(String statement, Path file) throws IOException, SQLException {
        try (FileInputStream inputStream = new FileInputStream(file.toFile())) {
            long timeMillis = System.currentTimeMillis();
            long rowCount = PGCopy.getInstance().load(statement, inputStream);
            log.info(String.format("Succesfully loaded file %s %s rows for %s s", file.getFileName(), rowCount,
                    (System.currentTimeMillis() - timeMillis) / 1000));
        }
    }

    private void loadFilesByLoad(String statementText, String setFileLoadedStatementText, boolean deleteSourceFiles,
                                 List<Path> filesForLoad, Map<String, String> tableForFile) throws IOException {
        for (Path file : filesForLoad) {
            final String fileName = file.getFileName().toString();
            final String tableName = tableForFile.get(fileName);
            if (tableName == null || tableName.isEmpty()) {
                log.error("Not found table name for file: " + fileName);
            } else {
                try {
                    loadFileByLoadCommand(String.format(statementText, tableName), file);

                    if (deleteSourceFiles) Files.delete(file);
                } catch (SQLException e) {
                    log.error("loadFilesByLoadCommand", e);
                }
            }
        }
    }

    private List<String> getAddIndexColumnsList(AskpImportConfigStatement task) {
        List<String> addIndexColumns = new ArrayList<>();
        if (task.addIndexColumn1 != null) addIndexColumns.add(task.addIndexColumn1);
        if (task.addIndexColumn2 != null) addIndexColumns.add(task.addIndexColumn2);
        if (task.addIndexColumn3 != null) addIndexColumns.add(task.addIndexColumn3);
        if (task.addIndexColumn4 != null) addIndexColumns.add(task.addIndexColumn4);
        if (task.addIndexColumn5 != null) addIndexColumns.add(task.addIndexColumn5);
        if (task.addIndexColumn6 != null) addIndexColumns.add(task.addIndexColumn6);
        if (task.addIndexColumn7 != null) addIndexColumns.add(task.addIndexColumn7);
        return addIndexColumns;
    }

    private void loadData() {

        PGCopy.getInstance().init((BasicDataSource) this.datasource);

        List<Path> files = new ArrayList<>();
        List<Path> filesForLoad;
        Map<String, String> tableForFile = new HashMap<>();
        List<PartitionBuilder> partitions = new ArrayList<>();

        config.tasks.sort(Comparator.comparing(AskpImportConfigStatement::getOrder));
        for (AskpImportConfigStatement task : config.tasks) {
            try {
                files.clear();
                tableForFile.clear();
                partitions.clear();

                try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(this.config.importDir), task.fileMask)) {
                    stream.forEach(files::add);
                }
                if (!files.isEmpty()) {
                    files.sort(Comparator.comparing(Path::getFileName));

                    // Разбор имен файлов и формирование списка имена таблиц для загрузки и списка секций
                    filesForLoad = getPartitions(files,
                            partitions,
                            tableForFile,
                            config.pgDataDir,
                            config.pgTablespaceName,
                            task.tableName,
                            task.partitionType,
                            task.partitionColumn,
                            getAddIndexColumnsList(task));

                    // Проверка наличия секций
                    checkPartition(partitions);

                    // Загрузка данных
                    if (task.statementText.trim().toUpperCase().startsWith("COPY ")) {
                        // Импорт командой COPY c секциями
                        loadFilesByLoad(task.statementText, config.setFileLoadedStatementText, config.deleteSourceFiles,
                                filesForLoad, tableForFile);
                    } else {
                        // Импорт функцией
                        loadFilesByFunc(task.statementText, task.batchSize, config.deleteSourceFiles, filesForLoad, config.sourceFileCharset);
                    }
                }
            } catch (Throwable e) {
                log.error("loadData", e);
            }
        }
    }

    /**
     * Проверка наличия места на диске для скачивания файла
     *
     * @param fileProp
     * @param fileName
     * @param store
     * @return
     * @throws IOException
     */
    private boolean isCanDownload(FTPFile[] fileProp, String fileName, FileStore store) throws IOException {
        long free = store.getUsableSpace();
        for (FTPFile p : fileProp) {
            if (p.isFile() && p.getName().equals(fileName)) {
                log.info("Free space " + (free - p.getSize()));
                return (free - (p.getSize() + 100 * 1024 * 1024) > 0);
            }
        }
        return false;
    }

    private void moveFilesToWorkDir() throws IOException {
        Path tempPath = Paths.get(config.ftpSource.localDirectory + File.separator + tempSubDir);
        // Разархивируем, если требуется
        try (DirectoryStream<Path> ds = Files.newDirectoryStream(Paths.get(config.ftpSource.localDirectory), "*.*")) {
            for (Path s : ds) this.fileHelper.uncompressFileIfNeeded(s);
        }
        // Разбиваем на части, если требуется
        try (DirectoryStream<Path> ds = Files.newDirectoryStream(Paths.get(config.ftpSource.localDirectory), "*.txt")) {
            for (Path s : ds) fileHelper.moveToTempDir(s, tempPath.toString());
        }
        // Перекладываем в рабочий каталог что осталось
        try (DirectoryStream<Path> ds = Files.newDirectoryStream(tempPath, "*.txt")) {
            for (Path s : ds)
                Files.move(s, Paths.get(config.importDir + File.separator + s.getFileName().toString()), REPLACE_EXISTING);
        }
    }

    private void downloadFiles() {
        log.info("Download start");
        try {
            try (AskpFtpClient ftpDownloader = new AskpFtpClient()) {
                ftpDownloader.connect(config.ftpSource.server, config.ftpSource.username,
                        config.ftpSource.password, config.ftpSource.dataTimeout);
                // Поиск фалов для скачивания и загрузки
                Set<String> fileNamesForLoad = new HashSet<>();
                for (AskpImportConfigStatement task : config.tasks) {

                    if (task.remoteDirectory != null) {
                        ftpDownloader.changeWorkingDirectory(task.remoteDirectory);
                    } else {
                        ftpDownloader.changeWorkingDirectory(config.ftpSource.remoteDirectory);
                    }

                    for (String fileName : ftpDownloader.listNames(task.remoteFileMask != null ? task.remoteFileMask : task.fileMask)) {
                        // Проверка, файл скачен или загружен
                        if (!Files.exists(Paths.get(config.ftpSource.localDirectory + File.separator + fileName), NOFOLLOW_LINKS) &&
                                !getFlagFileLoaded(config.getFileLoadedStatementText, fileName)) {
                            if (!fileNamesForLoad.contains(fileName)) fileNamesForLoad.add(fileName);
                        }
                    }

                    // Выгрузка по FTP
                    FileStore store = Files.getFileStore(Paths.get(config.ftpSource.localDirectory));
                    FTPFile[] fileProp = ftpDownloader.listFiles();
                    int i = 0;
                    for (String fileName : fileNamesForLoad.toArray(new String[0])) {
                        // Проверка места на диске
                        if (!isCanDownload(fileProp, fileName, store)) {
                            log.error("Not enough local disk space");
                            break;
                        }
                        if (ftpDownloader.downloadFile(fileName, config.ftpSource.localDirectory + File.separator + fileName)) {
                            setFlagFileLoaded(config.setFileLoadedStatementText, fileName);
                        }

                        // Не все сразу. Загрузка по частям. Компромис, т.к. FTP рвет коннект по бездействию.
                        if (i++ > config.ftpSource.fileBatchCount) break;
                    }
                    fileNamesForLoad.clear();
                }
            } catch (SocketException ex) {
                log.error("downloadFiles", ex);
            }

            // Обработка и перемещение в каталог для загрузки в БД
            moveFilesToWorkDir();

            log.info("Download finished");

        } catch (Throwable ex) {
            log.error("downloadFiles", ex);
        }
    }

    public void start() throws IOException {

        Path tempPath = Paths.get(config.ftpSource.localDirectory + File.separator + tempSubDir);
        if (!Files.exists(tempPath)) {
            try {
                Files.createDirectories(tempPath);
            } catch (IOException e) {
                log.error(e.getMessage());
                return;
            }
        }

        fileDownloadTask.scheduleWithFixedDelay(this::downloadFiles, 0, config.downloadCheckSeconds, TimeUnit.SECONDS);
        fileImportTask.scheduleWithFixedDelay(this::loadData, config.changeCheckSeconds, config.changeCheckSeconds, TimeUnit.SECONDS);

        log.info("Start AskpImport Service");
    }

    public static void main(String[] args) throws Exception {
        AskpImportConfig config = Configuration.unpackConfig(AskpImportConfig.class, "askp-import.xml");
        new AskpImport(config).start();
    }
}
