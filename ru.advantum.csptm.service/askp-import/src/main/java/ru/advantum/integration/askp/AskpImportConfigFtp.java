package ru.advantum.integration.askp;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@Root(name = "ftp")
public class AskpImportConfigFtp {
    @Attribute
    public String server;
    @Attribute
    public String username;
    @Attribute
    public String password;
    @Attribute
    public String remoteDirectory;
    @Attribute
    public String localDirectory;

    @Attribute(name = "file-batch-count", required = false)
    int fileBatchCount = 30;

    @Attribute(name = "data-timeout", required = false)
    int dataTimeout = 30 * 60 * 1000;
}
