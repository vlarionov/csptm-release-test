package ru.advantum.integration.askp;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;

import java.util.List;

/**
 * @author abc <alekseenkov@advantum.pro>
 */
@Root(name = "askp-import")
public class AskpImportConfig {

    @Attribute(name = "change-check-period", required = false)
    public long changeCheckSeconds = 1 * 60 * 60;
    @Attribute(name = "download-check-period", required = false)
    public long downloadCheckSeconds = 3 * 60 * 60;

    @Element(name = "database")
    public DatabaseConnection database;

    /**
     * Каталог с данными для импорта
     */
    @Element(name = "import-dir")
    public String importDir;

    /**
     * Удалять файлы после импорта
     */
    @Element(name = "delete-source-files", required = false)
    public Boolean deleteSourceFiles = true;

    @Element(name = "source-files-charset", required = false)
    public String sourceFileCharset = "CP1251";

    /**
     * Каталог postgres для tablespace
     */
    @Element(name = "pg-data-dir", required = false)
    public String pgDataDir;

    /**
     * Имя tablespace в которм будут создаваться секционрованные таблицы
     */
    @Element(name = "pg-tablespace", required = false)
    public String pgTablespaceName;

    /**
     * Описание процедур импорта
     */
    @ElementList(inline = true)
    public List<AskpImportConfigStatement> tasks;

    /**
     * Функция проверки что файл газружен
     */
    @Element(name = "statement-get_file_loaded", required = false)
    public String getFileLoadedStatementText;

    /**
     * Функция отметки что файл газружен
     */
    @Element(name = "statement-set_file_loaded", required = false)
    public String setFileLoadedStatementText;

    @Element(name = "ftp")
    public AskpImportConfigFtp ftpSource;
}
