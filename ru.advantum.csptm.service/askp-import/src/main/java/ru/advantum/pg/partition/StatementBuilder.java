package ru.advantum.pg.partition;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by kukushkin on 26.07.2016.
 */
public class StatementBuilder {
    public static final DateTimeFormatter PG_DATA_LOAD_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static String getCreateTablespaceStatement(String tablespaceName, String location) {
        return new StringBuilder()
                .append("CREATE TABLESPACE ")
                .append(tablespaceName)
                .append(" LOCATION '")
                .append(location)
                .append("';")
                .toString();
    }

    public static String getCreateInheritsTableStatement(String tableName, String suffix, String tablespace, LocalDateTime startDate, LocalDateTime finishDate, String partitionColumn) {
        return new StringBuilder()
                .append("CREATE TABLE IF NOT EXISTS ")
                .append(tableName)
                .append("_")
                .append(suffix)
                .append(" (CONSTRAINT ")
                .append(tableName.replaceAll("\\.", "_"))
                .append("_")
                .append(suffix)
                .append("_")
                .append(partitionColumn)
                .append("_check CHECK(")
                .append(partitionColumn)
                .append(">= '")
                .append(startDate.format(PG_DATA_LOAD_FORMAT))
                .append("' AND ")
                .append(partitionColumn)
                .append("< '")
                .append(finishDate.format(PG_DATA_LOAD_FORMAT))
                .append("' )) INHERITS (")
                .append(tableName)
                .append(") WITH (OIDS=TRUE) TABLESPACE ")
                .append(tablespace)
                .append(";").toString();
    }

    public static String getCreateIndexStatement(String indexName, String tableName, String columnList, String tablespace) {
        return new StringBuilder()
                .append("CREATE INDEX IF NOT EXISTS ")
                .append(indexName)
                .append("  ON ")
                .append(tableName)
                .append("  USING btree  (")
                .append(columnList)
                .append(") TABLESPACE ")
                .append(tablespace)
                .append(";")
                .toString();

    }

    public static String getCreateInheritsTableStatement(String schema, String tableName, String suffix, String tablespace, LocalDateTime startDate, LocalDateTime finishDate, String partitionColumn) {
        return new StringBuilder()
                .append("CREATE TABLE IF NOT EXISTS ")
                .append(schema)
                .append(".")
                .append(tableName)
                .append("_")
                .append(suffix)
                .append(" (CONSTRAINT ")
                .append(tableName.replaceAll("\\.", "_"))
                .append("_")
                .append(suffix)
                .append("_")
                .append(partitionColumn)
                .append("_check CHECK(")
                .append(partitionColumn)
                .append(">= '")
                .append(startDate.format(PG_DATA_LOAD_FORMAT))
                .append("' AND ")
                .append(partitionColumn)
                .append("< '")
                .append(finishDate.format(PG_DATA_LOAD_FORMAT))
                .append("' )) INHERITS (")
                .append(schema)
                .append(".")
                .append(tableName)
                .append(") WITH (OIDS=TRUE) TABLESPACE ")
                .append(tablespace)
                .append(";").toString();
    }

    public static String getCreateIndexStatement(String schema, String indexName, String tableName, String columnList, String tablespace) {
        return new StringBuilder()
                .append("CREATE INDEX IF NOT EXISTS ")
                .append(indexName)
                .append("  ON ")
                .append(schema)
                .append(".")
                .append(tableName)
                .append("  USING btree  (")
                .append(columnList)
                .append(") TABLESPACE ")
                .append(tablespace)
                .append(";")
                .toString();
    }
}
