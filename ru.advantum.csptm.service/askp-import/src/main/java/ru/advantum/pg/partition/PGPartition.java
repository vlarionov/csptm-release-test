package ru.advantum.pg.partition;

import java.time.temporal.ChronoUnit;

/**
 * Created by kukushkin on 26.07.2016.
 */
public interface PGPartition {
    ChronoUnit getChronoUnit();

    //void of_init(String suffix, LocalDateTime begin, LocalDateTime end);
    
    String getTableName();

    String getCreateTableTablespaceDir();

    String getCreateTableTablespaceStatement();

    String getTableTablespaceName();

    String getCreateTableStatement();

    String getCreateIndexTablespaceDir();

    String getCreateIndexTablespaceStatement();

    String getIndexTablespaceName();

    String getCreateIndexStatement();
}
