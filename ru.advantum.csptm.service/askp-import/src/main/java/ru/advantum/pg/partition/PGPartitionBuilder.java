package ru.advantum.pg.partition;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.UserPrincipal;
import java.nio.file.attribute.UserPrincipalLookupService;
import java.sql.*;

/**
 * Created by kukushkin on 26.07.2016.
 */
//    public class PGPartitionBuilder<T extends PartitionChecker> {
public class PGPartitionBuilder {
    private static PGPartitionBuilder ourInstance = new PGPartitionBuilder();
    //    private final ScheduledExecutorService EX_PARTITION_BUILDER = Executors.newSingleThreadScheduledExecutor();
    private static final Logger log = Logger.getLogger("PGLoader");
    private static BasicDataSource ds = null;
//    private static ArrayList<PGPartition> partitionList = new ArrayList<>();
    private static final Logger LOG = Logger.getLogger("PGPartitionBuilder");
    //    private T partitionChecker;
    private final static String IS_TBS_EXISTS_SQL = "SELECT count(*) FROM pg_tablespace where spcname = ?";
    private final String IS_TAB_EXISTS_SQL = "select count(*) from information_schema.tables where table_schema = ? and table_name = ?";

    public static PGPartitionBuilder getInstance() {
        return ourInstance;
    }

    private PGPartitionBuilder() {
    }

//    public PGPartitionBuilder of_init(BasicDataSource ds, ArrayList<PGPartition> partitionList, T partitionChecker) {
//        this.ds = ds;
//        this.partitionList = partitionList;
//        this.partitionChecker = partitionChecker;
//        return ourInstance;
//    }

    public PGPartitionBuilder of_init(BasicDataSource ds) {
        this.ds = ds;
        return ourInstance;
    }

    private void execute(String statement) {
        try (Connection conn = ds.getConnection();) {
            log.info(statement);
            conn.setAutoCommit(true);
            Statement st = conn.createStatement();
            st.execute(statement);
            st.close();
        } catch (SQLException e) {
            LOG.info("", e);
        }
    }

    private boolean ifExistsTablespace(String name) {
        int cnt = 0;
        try (Connection conn = ds.getConnection();) {

            PreparedStatement st = conn.prepareStatement(IS_TBS_EXISTS_SQL);
            st.setString(1, name);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                cnt = rs.getInt(1);
            }
            rs.close();
            st.close();
        } catch (SQLException e) {
            LOG.info("", e);
        }

        return cnt == 1;
    }

    private boolean ifExistsTable(String name) {
        int cnt = 0;
        try (Connection conn = ds.getConnection();) {
            String schemaName;
            String tableName;
            if (name.contains(".")) {
                schemaName = name.split("\\.")[0];
                tableName = name.split("\\.")[1];
            } else {
                schemaName = "public";
                tableName = name;
            }
            PreparedStatement st = conn.prepareStatement(IS_TAB_EXISTS_SQL);
            st.setString(1, schemaName);
            st.setString(2, tableName);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                cnt = rs.getInt(1);
            }
            rs.close();
            st.close();
        } catch (SQLException e) {
            LOG.info("", e);
        }
        return cnt == 1;
    }

//    public void start() {
//        //Первичная проверка на текущую дату
//        partitionList.forEach(p ->
//        {
//            PartitionCheckParam param = partitionChecker.getInitParam();
//            if (param != null) {
//                LOG.info(String.format("start initial partition check from suffix %s begin %s end %s", param.getSuffix(), param.getBegin().toString(), param.getEnd().toString()));
//                p.of_init(param.getSuffix(), param.getBegin(), param.getEnd());
//                checkPartition(p);
//            }
//        });
//
//        EX_PARTITION_BUILDER.scheduleWithFixedDelay(() -> {
//            try {
//                partitionList.forEach(p ->
//                {
//                    if (partitionChecker.checkNeedPartition()) {
//                        PartitionCheckParam param = partitionChecker.getScheduledParam();
//                        if (param != null) {
//                            p.of_init(param.getSuffix(), param.getBegin(), param.getEnd());
//                            checkPartition(p);
//                        }
//                    }
//
//                });
//            } catch (Throwable e) {
//                log.error("load()", e);
//            }
//        }, 0, 1, TimeUnit.MINUTES);
//    }

    public void checkPartition(PGPartition p) {
        // Секционированная таблица уже есть?
        if (ifExistsTable(p.getTableName())) return;

        try {
            createFile(p.getCreateTableTablespaceDir());

            if (!ifExistsTablespace(p.getTableTablespaceName())) {
                execute(p.getCreateTableTablespaceStatement());
            }
            execute(p.getCreateTableStatement());

            createFile(p.getCreateIndexTablespaceDir());
            if (!ifExistsTablespace(p.getIndexTablespaceName())) {
                execute(p.getCreateIndexTablespaceStatement());
            }
            execute(p.getCreateIndexStatement());

        } catch (IOException e) {
            LOG.info("", e);
        }
    }

    private boolean isWindows() {
        String OS = System.getProperty("os.name").toLowerCase();
        return OS.indexOf("win") >= 0;
    }

    private void createFile(String dir) throws IOException {
        Path path = FileSystems.getDefault().getPath(dir);
        if (!Files.exists(path)) {
            try {
                if (isWindows()) {
                    Files.createDirectories(path);
                } else {
                    UserPrincipalLookupService lookupService = FileSystems.getDefault().getUserPrincipalLookupService();
                    UserPrincipal postgres = lookupService.lookupPrincipalByName("postgres");
                    path = Files.createDirectories(path);
                    Files.setOwner(path, postgres);
                }
            } catch (FileAlreadyExistsException ignored) {
                LOG.info(path.toString() + " directory already exists ...");
            }
        }
    }
}
