package ru.advantum.pg.partition;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

/**
 * @author abc <alekseenkov@advantum.pro>
 */
public class PartitionBuilder implements PGPartition {
    private final String dir;
    private final String baseTableName;                             // Наимнование базовой таблицы
    private final String orgTableName;                              // Наимнование таблицы без схемы
    private final String tableName;                                 // Наимнование секционированной таблицы
    private final String tableSpaceNameData;
    private final String tableSpaceNameIndex;
    private final LocalDateTime begin;
    private final LocalDateTime end;
    private final String partitionColumn;
    private final List<String> addIndexColumns;
    private final String suffix;

    public String getTableName() {
        return tableName;
    }

    public String getSuffix() {
        return suffix;
    }

    public PartitionBuilder(String dir, String baseTableName, String suffix, String tablespaceName, String partitionColumn,
                            List<String> addIndexColumns, LocalDateTime begin, LocalDateTime end) {
        this.dir = dir;
        this.suffix = suffix;
        this.begin = begin;
        this.end = end;
        this.baseTableName = baseTableName;
        if (!baseTableName.contains("."))
            this.orgTableName = baseTableName;
        else
            this.orgTableName = baseTableName.split("\\.")[1];

        this.tableName = baseTableName + "_" + suffix;
        this.tableSpaceNameData = tablespaceName + "_" + this.orgTableName + "_data_" + suffix;
        this.tableSpaceNameIndex = tablespaceName + "_" + this.orgTableName + "_idx_" + suffix;
        this.partitionColumn = partitionColumn;
        this.addIndexColumns = addIndexColumns;
    }

    @Override
    public ChronoUnit getChronoUnit() {
        return null;
    }

    @Override
    public String getCreateTableTablespaceDir() {
        return new StringBuilder().append(dir).append(tableSpaceNameData).toString();
    }

    @Override
    public String getCreateIndexTablespaceDir() {
        return new StringBuilder().append(dir).append(tableSpaceNameIndex).toString();
    }

    @Override
    public String getCreateTableTablespaceStatement() {
        return StatementBuilder.getCreateTablespaceStatement(tableSpaceNameData, getCreateTableTablespaceDir());
    }

    @Override
    public String getTableTablespaceName() {
        return this.tableSpaceNameData;
    }

    @Override
    public String getCreateIndexTablespaceStatement() {
        return StatementBuilder.getCreateTablespaceStatement(tableSpaceNameIndex, getCreateIndexTablespaceDir());
    }

    @Override
    public String getIndexTablespaceName() {
        return this.tableSpaceNameIndex;
    }

    @Override
    public String getCreateTableStatement() {
        return StatementBuilder.getCreateInheritsTableStatement(this.baseTableName, suffix, tableSpaceNameData, begin, end, partitionColumn);
    }

    @Override
    public String getCreateIndexStatement() {
        StringBuilder sb = new StringBuilder();
        sb.append(StatementBuilder.getCreateIndexStatement("ix_" + orgTableName + "_" + suffix + "_" + partitionColumn, tableName, partitionColumn, tableSpaceNameIndex)).append(";");

        for (String addIndexColumn : addIndexColumns) {
            if (addIndexColumn != null && !addIndexColumn.isEmpty()) {
                sb.append(StatementBuilder.getCreateIndexStatement("ix_" + orgTableName + "_" + suffix + "_" + addIndexColumn, tableName, addIndexColumn, tableSpaceNameIndex)).append(";");
            }
        }
        return sb.toString();
    }
}
