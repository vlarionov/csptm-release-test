package ru.advantum.pg.loader;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by kukushkin on 12.04.2016.
 */
public class PGCopy {
    private static BasicDataSource ds = null;
    private static PGCopy instance;
    private static final Logger log = Logger.getLogger("PGCopy");

    public static PGCopy getInstance() {
        if (instance == null) {
            instance = new PGCopy();
        }
        return instance;
    }

    public void init(BasicDataSource ds) {
        PGCopy.ds = ds;
    }

    public long load(String statement, InputStream inputStream) throws SQLException, IOException {
        try (Connection con = ds.getConnection()) {
            CopyManager cm = new CopyManager(con.unwrap(BaseConnection.class));
            long rowCount = cm.copyIn(statement, inputStream);
            con.commit();
            return rowCount;
        }
    }
}
