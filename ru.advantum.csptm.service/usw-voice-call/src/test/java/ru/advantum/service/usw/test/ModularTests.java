package ru.advantum.service.usw.test;

import com.google.gson.Gson;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Envelope;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.freeswitch.esl.client.transport.event.EslEvent;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.rabbitmq.consumer.TypedConsumer;
import ru.advantum.service.usw.UswServiceConfig;
import ru.advantum.service.usw.network.amqp.RmqHandler;
import ru.advantum.service.usw.network.amqp.UniversalCallRecord;

import static org.apache.log4j.Logger.getRootLogger;

public class ModularTests {
    private static Logger log = LoggerFactory.getLogger(ModularTests.class.getSimpleName());
    public static final String PATTERN = "%d{dd-MM-yyyy HH:mm:ss.SSS}: %-5p %c{1}.%M(%L): %m%n";

    static {
        /*
         * This configuring console logger without log4j.properties file
         */
        if (System.getProperty("log4j.configuration") == null) { // not .isEmpty() - will NPE
            ConsoleAppender console = new ConsoleAppender(); //createParentDirs appender
            console.setLayout(new PatternLayout(PATTERN));
            console.setThreshold(Level.DEBUG);
            console.activateOptions();
            getRootLogger().addAppender(console);
        }
    }


    public static UswServiceConfig getConfig() {
        if (System.getProperty("advantum.config.dir") == null) {
            System.setProperty("advantum.config.dir", "src/main/resources/extras/examples");
        }
        try {
            return Configuration.unpackConfig(UswServiceConfig.class, UswServiceConfig.CONFIG_NAME).prepare();
        } catch (Exception e) {
            log.error("{}", e);
        }
        return null;
    }

    @Test
    public void configTest() throws Exception {
        String d = String.format("%04d", 4);
        UswServiceConfig config = getConfig().prepare();
        if (config != null) {
            log.info("{}", config.toString());
//            SessionFactoryManager.init(config);
//            log.info("pgsql conn OK: " + SessionFactoryManager.PostgreSqlSession.getRadioItemList(1).size());
            for (UswServiceConfig.Fxo2Number f : config.usw.fxo2Numbers) {
                log.info("{}", f.toString());
            }
            log.info("lookup test: for {} ip is {}", "6402", config.usw.lookupIpByPhone("6402"));
            log.info("lookup test: for {} ip is {}", "6410", config.usw.lookupIpByPhone("6410"));
            log.info("lookup test: for {} ip is {}", "6425", config.usw.lookupIpByPhone("6425"));
            log.info("lookup test: for {} ip is {}", "6430", config.usw.lookupIpByPhone("6430"));
            log.info("lookup test: for {} ip is {}", "6445", config.usw.lookupIpByPhone("6445"));

            //log.info("sound file name: {}", VoipWorker.getWavName(config.usw.recordFormat, "1234", "rs6543"));
        } else {
            log.error("Config not found");
        }
    }

    @Test
    public void fswTest() throws Exception {
        org.freeswitch.esl.client.inbound.Client cl = new org.freeswitch.esl.client.inbound.Client();
        cl.connect("10.0.1.27", 8021, "ClueCon", 600);
        cl.addEventListener(new org.freeswitch.esl.client.IEslEventListener() {

            @Override
            public void eventReceived(EslEvent eslEvent) {
                System.out.println("eventReceived:" + eslEvent.getEventName());
            }

            @Override
            public void backgroundJobResultReceived(EslEvent eslEvent) {
                System.out.println("backgroundJobResultReceived:" + eslEvent.getEventName());

            }
        });
        cl.setEventSubscriptions("plain", "all");
        cl.sendAsyncApiCommand("originate", "sofia/internal/0004%10.0.1.27 &bridge(sofia/internal/0003%10.0.1.27)");
        //cl.sendSyncApiCommand("originate", "sofia/internal/0004%10.0.1.27 &bridge(sofia/internal/0003%10.0.1.27)");
        cl.close();
    }

    //{"idCall":12345,"owner":3001,"gsmNumbers":[85212354,8654321654],"uswNumbers":[6484],"stationNumbers":[3214,5456,6623]}

    @Test
    public void cdrTest() throws Throwable {
        System.setProperty("advantum.config.dir", "src/test/resources/");

        UswServiceConfig config = Configuration.unpackConfig(UswServiceConfig.class, UswServiceConfig.CONFIG_NAME).prepare();

        RmqHandler rh = new RmqHandler().connect(config);
        rh.addRabbitConnection(UniversalCallRecord.class, new TypedConsumer<UniversalCallRecord>() {

            @Override
            public void consume(String s, Envelope envelope, AMQP.BasicProperties basicProperties, UniversalCallRecord universalUswConsumer) {
                System.out.println(universalUswConsumer.toString());
            }
        }).startConsumer();

        Thread.sleep(500000);
    }

}