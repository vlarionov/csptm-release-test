package ru.advantum.service.usw;

import org.freeswitch.esl.client.transport.event.EslEvent;
import org.freeswitch.esl.client.transport.message.EslMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HungDownThread extends Thread {
    private Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName() + ":" + Thread.currentThread().getId());
    private final String callUUID;

    public HungDownThread(String callUUID) {
        this.callUUID = callUUID;
    }

    public HungDownThread fork() {
        start();
        return this;
    }

    @Override
    public void run() {
        org.freeswitch.esl.client.inbound.Client freeSwitchClient = new org.freeswitch.esl.client.inbound.Client();
        freeSwitchClient.addEventListener(new org.freeswitch.esl.client.IEslEventListener() {

            @Override
            public void eventReceived(EslEvent eslEvent) {
                log.info("---- eventReceived: {}", eslEvent.getEventName());

            }

            @Override
            public void backgroundJobResultReceived(EslEvent eslEvent) {
                log.info("----- backgroundJobResultReceived: {}", eslEvent.getEventName());

            }
        });

        try {
            freeSwitchClient.connect(
                    UswService.config.usw.fsaddress
                    , 8021
                    , UswService.config.usw.fspassword
                    , 10000);

            EslMessage emsg = freeSwitchClient.sendSyncApiCommand(
                    "hupall"
                    , callUUID);
            log.info("### USW Sent command: {}", emsg);

        } catch (Exception e) {
            log.error("", e);
        }

        freeSwitchClient.close();


    }

}
