package ru.advantum.service.usw.network.amqp;

import com.fatboyindustrial.gsonjavatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import ru.advantum.rabbitmq.consumer.JsonConsumer;
import ru.advantum.rabbitmq.consumer.TypedConsumer;
import ru.advantum.service.usw.UswService;
import ru.advantum.service.usw.UswServiceConfig;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RmqHandler {
    //    private static final Logger log = LoggerFactory.getLogger(RmqHandler.class);

    private static final Gson GSON = Converters.registerAll(new GsonBuilder()).create();

    private Connection uswRabbitconnection;
    private Channel uswReadChannel;
    private JsonConsumer jsonConsumer;
    private UswServiceConfig config;

    public RmqHandler connect() throws Throwable {
        this.config = UswService.config;
        this.uswRabbitconnection = connectInternal();
        return this;
    }

    public RmqHandler connect(UswServiceConfig config) throws Throwable {
        this.config = config;
        this.uswRabbitconnection = connectInternal();
        return this;
    }

    private Connection connectInternal() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(config.rmqConnection.host);
        factory.setPort(config.rmqConnection.port);
        factory.setVirtualHost(config.rmqConnection.virtualHost);
        factory.setUsername(config.rmqConnection.username);
        factory.setPassword(config.rmqConnection.password);
        return factory.newConnection();
    }

    public void close() {
        try {
            uswReadChannel.close();
            uswRabbitconnection.close();
        } catch (IOException | TimeoutException ignore) {
        }
    }

    public RmqHandler addRabbitConnection(Class messageClass, TypedConsumer<?> consumer) throws Exception {
        if (this.uswReadChannel == null && this.jsonConsumer == null) {
            this.uswReadChannel = this.uswRabbitconnection.createChannel();
            this.jsonConsumer = new JsonConsumer(uswReadChannel);
        }

        this.jsonConsumer.addConsumer(messageClass.getSimpleName(), messageClass, consumer);
        return this;
    }

    public RmqHandler startConsumer() throws IOException {
        this.uswReadChannel.basicConsume(UswService.config.rmqUi.queue, true, "", false, true, null, this.jsonConsumer);
        return this;
    }

    public void publish(CallStatus msg) throws Exception {
        if (uswRabbitconnection.isOpen()) {
            Channel channel = this.uswRabbitconnection.createChannel();
            channel.basicPublish(config.rmqDest.exchange, config.rmqDest.routingKey,
                    (new AMQP.BasicProperties.Builder()).type(msg.getClass().getSimpleName()).build(),
                    GSON.toJson(msg).getBytes());
            channel.close();
        }
    }

}
