package ru.advantum.service.usw;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Envelope;
import edu.umd.cs.findbugs.annotations.SuppressWarnings;
import org.freeswitch.esl.client.inbound.InboundConnectionFailure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.rabbitmq.consumer.TypedConsumer;
import ru.advantum.service.usw.data.RadioStation;
import ru.advantum.service.usw.fsw.CustomEventListener;
import ru.advantum.service.usw.fsw.FreeSwitchClient;
import ru.advantum.service.usw.network.amqp.CallStatus;
import ru.advantum.service.usw.network.amqp.RmqHandler;
import ru.advantum.service.usw.network.amqp.UniversalCallRecord;
import ru.advantum.service.usw.uswclient.ICallBack;
import ru.advantum.service.usw.uswclient.Status;
import ru.advantum.service.usw.uswclient.UswClient;
import ru.advantum.service.usw.uswclient.UswManager;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

// {"idCall":101202, "idTr":113072, "radioNumber":8414, "dispatcherNumber": 1002 }
// {"idCall":101202, "idTr":113072, "firstNumber":89857985736, "secondNumber": 1002 }

// 113072 - 89854548514
// 114966 - 89857985736

//{"idCall":12345,"owner":1003,"gsmNumbers":[89854548514,89857985736],"uswNumbers":[8414],"stationNumbers":[1002]}

@SuppressWarnings("unused")
public class VoipWorker implements TypedConsumer<UniversalCallRecord>, ICallBack, Runnable {
    private Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());
    private RmqHandler rmq;
    private final FreeSwitchClient freeSwitchClient = new FreeSwitchClient();

    public VoipWorker(RmqHandler rmq) {
        try {
            this.freeSwitchClient.connect(
                    UswService.config.usw.fsaddress
                    , UswService.config.usw.fsport
                    , UswService.config.usw.fspassword
                    , UswService.config.usw.fsTimeout);

            this.freeSwitchClient.setEventSubscriptions("plain", "all");
        } catch (InboundConnectionFailure ex) {
            log.error("", ex);
        }
        setRmq(rmq);
    }

    RmqHandler getRmqHandler() {
        return rmq;
    }

    public void setRmq(RmqHandler rmq) {
        this.rmq = rmq;
    }

    @Override
    public void run() {
        try {
            if (UswService.config.rmqUi != null) {
                getRmqHandler()
                        .addRabbitConnection(UniversalCallRecord.class, this)
                        .startConsumer();
                log.info("Rmq queue listener attached");
            }
        } catch (Exception e) {
            log.error("{}", e);
        }
    }

    @Override
    public void consume(String s, Envelope envelope, AMQP.BasicProperties basicProperties, UniversalCallRecord ucr) {

        CallStatus callstatus = new CallStatus().setIdCall(ucr.getIdCall()).setCallBegin(Instant.now());
        if (ucr.getUswNumbers() != null && ucr.getUswNumbers().size() > 0) {
            for (Long radioNumber : ucr.getUswNumbers()) {
                UswManager.add(callstatus,
                        new RadioStation(ucr.getIdCall(), radioNumber.intValue()), this);
            }
        }

/*
        if (UswManager.getUswClients().size() > 0) {
            boolean someOneRsReady = false;
            while(!someOneRsReady) {
                for (UswClient uswClient : UswManager.getUswClients()) {
                    if (uswClient.getStatus() == Status.fstWait && uswClient.getActualPhoneNumber() != null) {
                        someOneRsReady = true;
                        break;
                    }
                }
            }
        }
*/
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ignored) {
        }
        // Could touch 'conferenceUUID' here and use further
        String conferenceUUID = callOriginator(ucr.getIdCall(), String.format("%04d", ucr.getOwner()), ucr.getGsmNumbers(), ucr.getStationNumbers(), callstatus);
    }


    public String callOriginator(Long idCall, String ownerPhone, List<Long> gsmNumbers, List<Long> stationNumbers, CallStatus callStatus) {
        HashMap<String, FswThread> callStack = new HashMap<>();
        String conferenceUUID = UUID.randomUUID().toString();

        log.info("{} conference call started", conferenceUUID);
        try {

            String wavName = getWavName(idCall, UswService.config.usw.recordFormat, "" + ownerPhone);
            callStatus.setAudiofile(getCdrPath() + "/" + wavName);

            // Owner thread construction
            //
            String ownerUUID = UUID.nameUUIDFromBytes(("" + ownerPhone).getBytes()).toString();
            String ownerOriginate;
            if (UswService.config.usw.fsLocalNumbersList.contains(ownerPhone)) {
                ownerOriginate = String.format(UswService.config.usw.callOwnerLocalTemplate,
                        UswService.config.usw.soundFilePath,
                        getCdrPath(),
                        wavName,
                        ownerUUID,
                        ownerPhone,
                        conferenceUUID
                );
            } else {
                ownerOriginate = String.format(UswService.config.usw.callOwnerRemoteTemplate,
                        UswService.config.usw.soundFilePath,
                        getCdrPath(),
                        wavName,
                        ownerUUID,
                        ownerPhone,
                        UswService.config.usw.cucmAddress,
                        conferenceUUID
                );
            }
            callStack.put(ownerUUID, new FswThread(freeSwitchClient, ownerOriginate, ownerUUID, conferenceUUID).fork());

            if (gsmNumbers != null && gsmNumbers.size() > 0) {
                for (Long gsmNumber : gsmNumbers) {

                    //GSM thread construction
                    //
                    String gsmPhone = String.format("%04d", gsmNumber);
                    String gsmUUID = UUID.nameUUIDFromBytes(gsmPhone.getBytes()).toString();
                    String gsmOriginate = String.format(UswService.config.usw.callCommonRemoteTemplate,
                            gsmUUID,
                            gsmPhone,
                            UswService.config.usw.cucmAddress,
                            conferenceUUID
                    );
                    callStack.put(gsmUUID, new FswThread(freeSwitchClient, gsmOriginate, gsmUUID, conferenceUUID).fork());

                }
            }

            if (stationNumbers != null && stationNumbers.size() > 0) {
                for (Long stNumber : stationNumbers) {

                    //Stationary phone thread construction
                    //
                    String stPhone = String.format("%04d", stNumber);
                    String stUUID = UUID.nameUUIDFromBytes(stPhone.getBytes()).toString();
                    String stOriginate;

                    if (UswService.config.usw.fsLocalNumbersList.contains(stPhone)) {

                        stOriginate = String.format(UswService.config.usw.callStationaryLocalTemplate,
                                stUUID,
                                stPhone,
                                conferenceUUID
                        );

                    } else {

                        stOriginate = String.format(UswService.config.usw.callStationaryRemoteTemplate,
                                stUUID,
                                stPhone,
                                UswService.config.usw.cucmAddress,
                                conferenceUUID
                        );
                    }
                    callStack.put(stUUID, new FswThread(freeSwitchClient, stOriginate, stUUID, conferenceUUID).fork());

                }
            }


            if (UswManager.getUswClients().size() > 0) {
                for (UswClient uswClient : UswManager.getUswClients()) {
                    if (uswClient.getStatus() == Status.fstWait && uswClient.getActualPhoneNumber() != null) {

                        //Radio station thread construction
                        //
                        String uswPhone = uswClient.getActualPhoneNumber();
                        String uswUUID = UUID.nameUUIDFromBytes(uswPhone.getBytes()).toString();
                        String uswOriginate = String.format(UswService.config.usw.callCommonRemoteTemplate,
                                uswUUID,
                                uswPhone,
                                UswService.config.usw.lookupIpByPhone(uswPhone),
                                conferenceUUID
                        );

                        callStack.put(uswUUID, new FswThread(freeSwitchClient, uswOriginate, uswUUID, conferenceUUID).fork());

                    } else {
                        log.warn("uswClient.getStatus() = {} && uswClient.getActualPhoneNumber() = {}",
                                uswClient.getStatus(),
                                uswClient.getActualPhoneNumber()
                        );
                    }
                }
            } else {
                log.warn("RadioStation client list is empty");
            }


            //--------------------------------------------------------------------------------------------------------

            try {
                freeSwitchClient.sendSyncApiCommand("myevents", conferenceUUID); //ownerUUID);
                CustomEventListener listener = new CustomEventListener(freeSwitchClient, this, callStatus, conferenceUUID);
                freeSwitchClient.addEventListener(listener);
                while (freeSwitchClient.isAlive(listener)) {
                    Thread.sleep(10000);
                }
                callStack.clear();

                log.info("{} conference call over", conferenceUUID);

            } catch (Exception e) {
                log.error("conference start:", e);
            }
        } catch (Exception e) {
            log.error("conference tun:", e);
        }

        return conferenceUUID;
    }


    @Override
    public void fireCallStatus(CallStatus status) {
        try {
            getRmqHandler().publish(status);
        } catch (Exception e) {
            log.error("", e);
        }
    }

    public static String getWavName(Long idCall, String pattern, String ownerPhone) {
        return String.format(pattern, "" + idCall + "-" + "", ownerPhone, UswService.config.usw.soundFileExt);
    }

    public static String getCdrPath() {
        return String.format("%s", new SimpleDateFormat("yyyy/MM/dd")
                .format(Instant.now().toEpochMilli()));
    }

    public enum FswEvent {
        CHANNEL_ANSWER,
        CHANNEL_STATE,
        CHANNEL_CALLSTATE,
        CHANNEL_OUTGOING,
        CHANNEL_EXECUTE,
        CHANNEL_HANGUP,
        CHANNEL_HANGUP_COMPLETE,
        CHANNEL_EXECUTE_COMPLETE,
        CHANNEL_DESTROY,
        API,
        PRESENCE_IN,
        CODEC,
        CUSTOM,
        CALL_UPDATE,
        HEARTBEAT,
        RE_SCHEDULE,
        RECORD_STOP,
        MEDIA_BUG_STOP;

        CallStatus.Status callstatus;

        public CallStatus.Status getCallstatus() {
            return callstatus;
        }

        private FswEvent() {
            this.callstatus = CallStatus.Status.UNDEFINED;
        }

        private FswEvent(CallStatus.Status callstatus) {
            this.callstatus = callstatus;
        }

        public static CallStatus.Status lookup(String fswEvent) {
            for (FswEvent e : FswEvent.values()) {
                if (e.name().equalsIgnoreCase(fswEvent)) {
                    return e.getCallstatus();
                }
            }
            return CallStatus.Status.UNDEFINED;
        }

    }
}
