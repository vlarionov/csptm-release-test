package ru.advantum.service.usw.fsw;

import org.freeswitch.esl.client.IEslEventListener;
import org.freeswitch.esl.client.transport.event.EslEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.service.usw.VoipWorker;
import ru.advantum.service.usw.network.amqp.CallStatus;

import java.time.Instant;

public class CustomEventListener implements IEslEventListener {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final String conferenceUUID;

    private final FreeSwitchClient freeSwitchClient;
    private final VoipWorker voipWorker;
    private CallStatus callStatus;

    public CustomEventListener(final FreeSwitchClient freeSwitchClient,
                               final VoipWorker voipWorker,
                               CallStatus callStatus,
                               final String conferenceUUID
    ) {
        if (conferenceUUID == null)
            throw new IllegalArgumentException("ConferenceUUID MUST NOT be null");

        this.freeSwitchClient = freeSwitchClient;
        this.voipWorker = voipWorker;
        this.callStatus = callStatus;
        this.conferenceUUID = conferenceUUID;
    }

    @Override
    public int hashCode() {
        return conferenceUUID.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)
            return false;
        if (o == this)
            return true;
        if (o instanceof CustomEventListener) {
            CustomEventListener other = (CustomEventListener) o;
            return other.getConferenceUUID().equals(this.getConferenceUUID());
        }
        return false;
    }

    public String getConferenceUUID() {
        return conferenceUUID;
    }

    @Override
    public void eventReceived(EslEvent event) {
        log.debug("======= EL event received  (conference UUID: {}) {}", conferenceUUID, event.toString());

        voipWorker.fireCallStatus(callStatus.setStatus(event.getEventName()).setCallEnd(Instant.now()));

        if (event.getEventName().contains("CHANNEL_DESTROY")) {
            log.debug("======= EL event {} over ", conferenceUUID);
            freeSwitchClient.removeEventListener(this);

        }

    }

    @Override
    public void backgroundJobResultReceived(EslEvent event) {

    }
}
