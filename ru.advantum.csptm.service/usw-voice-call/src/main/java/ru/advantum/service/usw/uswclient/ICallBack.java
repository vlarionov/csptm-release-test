package ru.advantum.service.usw.uswclient;

import ru.advantum.service.usw.data.RadioStation;
import ru.advantum.service.usw.network.amqp.CallStatus;

public interface ICallBack {
    void fireCallStatus(CallStatus status); //send back msg 2 rmq
}
