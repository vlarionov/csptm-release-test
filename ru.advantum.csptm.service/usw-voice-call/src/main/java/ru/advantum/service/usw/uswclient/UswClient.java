package ru.advantum.service.usw.uswclient;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.service.usw.UswService;
import ru.advantum.service.usw.data.RadioStation;
import ru.advantum.service.usw.network.amqp.CallStatus;
import ru.advantum.service.usw.network.tcp.ConnInfo;
import ru.advantum.service.usw.network.tcp.ITcpClient;
import ru.advantum.service.usw.network.tcp.NetworkEvent;
import ru.advantum.service.usw.network.tcp.TelnetClient;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.StringTokenizer;


public class UswClient extends Thread implements ITcpClient, Comparable<UswClient> {
    final Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());
    //private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(UswClient.class);
    private static final String PATTERN = "%d{dd-MM-yyyy HH:mm:ss.SSS}: %-5p %c{1}.%M(%L): %m%n";

/*
    static {
        if (System.getProperty("log4j.configuration") != null) {
            try {
                Properties logprops = new Properties();
                logprops.load(new URL(System.getProperty("log4j.configuration")).openStream());

                FileAppender rjLog =
                        new FileAppender(
                                new PatternLayout(PATTERN)
                                , logprops.getProperty("logs_location") + "/usw-worker-rejects.log"
                                , true);
                rjLog.setThreshold(Level.INFO);
                LOGGER.addAppender(rjLog);

                FileAppender rjDbg =
                        new FileAppender(
                                new PatternLayout(PATTERN)
                                , logprops.getProperty("logs_location") + "/usw-worker-rejects.debug"
                                , true);
                rjDbg.setThreshold(Level.DEBUG);
                LOGGER.addAppender(rjDbg);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
*/


    private TelnetClient tcpClient;
    private String lastParams;
    private ClientHandler handler = new ClientHandler(this);

    private boolean active = true;
    private long sendTime;
    private long lastPing;
    private boolean authFlag;
    private Status status = Status.fstDisconnect;
    private RadioStation inboundRs;
    private String confID;

    private ICallBack callback;
    private CallStatus callStatus;
    private String actualPhoneNumber;

    public String getActualPhoneNumber() {
        return actualPhoneNumber;
    }

    public Status getStatus() {
        return status;
    }

    UswClient(CallStatus callstatus, RadioStation inboundRs, ICallBack callback) {
        setInboundRs(inboundRs);
        setCallback(callback);
        this.callStatus = callstatus;
        callstatus.setParticipantNumber(inboundRs.getPhone());
        start();
    }

    void doResult(ParserChain parserChain) {
        if (parserChain.getCode() < 0) {  //No result
            switch (getStatus()) {
                case fstAuth: //запрос авторизации
                    log.warn("UswClient:No response for auth req {}", UswService.config.usw.address);
                case fstCall: //вызов на связь
                    log.warn("UswClient:No response for call req {}", UswService.config.usw.address);
                case fstWait: //режим ожидания
                    log.warn("UswClient:Server {} unavailable ", UswService.config.usw.address);
                case fstDropCall: //отбой связи
                    log.warn("UswClient:No response for dropcall req  {}", UswService.config.usw.address);
                case fstQuit: //отбой связи
                    log.warn("UswClient:No response for end of session req {}", UswService.config.usw.address);
                    setStatus(Status.fstDone);
                    terminate();
                    break;
                default:
                    log.error("Undef status {} code {}", getStatus(), parserChain.getCode());
                    break;
            }
        } else if (parserChain.getCode() > 0) {  //Some errors
            switch (getStatus()) {
                case fstQuit: //отбой связи
                    log.warn("UswClient:End of session req ERROR {}", UswService.config.usw.address);

                case fstAuth: {//запрос авторизации
                    String message = "UswClient:Auth req ERROR";
                    log.warn(message + " {}", UswService.config.usw.address);
                    getCallback().fireCallStatus(this.callStatus.setStatus(message));
                    setStatus(Status.fstDone);
                    terminate();
                    break;
                }

                case fstCall: {//вызов на связь
                    String message = "UswClient: Call req ERROR";
                    log.warn(message + " {} ", UswService.config.usw.address);
                    getCallback().fireCallStatus(this.callStatus.setStatus(message));
                    log.warn(message + " where " + this.lastParams);
                }

                case fstDropCall: {//отбой связи
                    String message = "UswClient:Dropcall req ERROR";
                    log.warn(message + " {}", UswService.config.usw.address);
                    getCallback().fireCallStatus(this.callStatus.setStatus(message));
                    setStatus(Status.fstQuit);
                    terminate();
                    break;
                }
                case fstWait: //режим ожидания
                    break;
                default:
                    log.error("UswClient:Undef status {} code {}", getStatus(), parserChain.getCode());
                    break;
            }
        } else { //Ok
            switch (getStatus()) {
                case fstConnect:
                    log.info("UswClient:Connect Ok {}", UswService.config.usw.address);
                    setAuthFlag(false);
                    setStatus(Status.fstAuth);
                    break;
                case fstAuth:
                    log.info("UswClient:Auth Ok {}", UswService.config.usw.address);
                    setAuthFlag(true);
                    setStatus(Status.fstCall);
                    break;
                case fstCall: //вызов на связь
                    log.info("UswClient:Call retrieved Ok {}", UswService.config.usw.address);

                    handleCallResult(parserChain);

                    setLastPing(System.currentTimeMillis());
                    setStatus(Status.fstWait);
                    break;
                case fstWait: //режим ожидания
                    log.info("UswClient:Ping... {}", UswService.config.usw.address);
                    break;
                case fstDropCall: //отбой связи
                    log.info("UswClient:Call dropped Ok  {}", UswService.config.usw.address);
                    log.warn("UswClient: Call dropped Ok  where " + this.lastParams);


                    setStatus(Status.fstQuit);
                    break;
                case fstQuit: //отбой связи
                    log.info("UswClient:Session done Ok {}", UswService.config.usw.address);
                    setStatus(Status.fstDone);
                    terminate();
                    break;
                default:
                    log.error("Undef status {} code {}", getStatus(), parserChain.getCode());
                    break;
            }
        }
    }


    private void handleCallResult(ParserChain p) {
        log.info("UswClient:handleCallResult.recv TEXT:{}", p.getText());
        //OK 3503(4097:207)
        StringTokenizer pt = new StringTokenizer(p.getText(), "()");
        String phone = pt.nextToken().replace("OK", "").trim();

        StringTokenizer pr = new StringTokenizer(pt.nextToken(), ",");
        String[] coupleNumType = pr.nextToken().split(":");
        if (coupleNumType.length == 2) {

            int radionum = Integer.parseInt(coupleNumType[0]);
            int radiotype = Integer.parseInt(coupleNumType[1]);
            RadioStation obtain = new RadioStation(inboundRs.getIdtr(), radionum, radiotype);

            if (!StringUtils.isEmpty(phone) && obtain.equals(inboundRs)) {

                this.actualPhoneNumber = phone;
                getCallback().fireCallStatus(this.callStatus.setParticipantNumber(phone).setStatus("READY"));

            } else {
                getCallback().fireCallStatus(this.callStatus.setStatus("NOT PHONE?"));
            }
        } else {
            getCallback().fireCallStatus(this.callStatus.setStatus("INVALID USW REPLY"));
        }
    }

    ParserChain parseString(String str) {
        log.debug("\tParse recv string: {}", str);
        ParserChain res = new ParserChain();
        if (str.startsWith("ERROR")) {
            try {
                res.setText(str.substring(6))
                        .setCode(Integer.parseInt(new StringTokenizer(res.getText(), "~").nextToken()));
            } catch (Exception e) {
                log.debug("\tWhere ERROR parse: {}", e);
            }
        } else {
            res.setText(str).setCode(0);
        }
        return res;
    }

    void handleStatus() {
        switch (getStatus()) {
            case fstAuth:
                handler.send("AUTH", UswService.config.usw.login, "~", UswService.config.usw.password);
                break;
            case fstCall:
                StringBuilder params = new StringBuilder()
                        .append(inboundRs.getRadioNum())
                        .append(":")
                        .append(inboundRs.getRadioType())
                        .append("~");
                if (params.length() > 0) {
                    params.deleteCharAt(params.lastIndexOf("~"));
                    this.lastParams = "CALL " + params.toString();
                    handler.send("CALL", params.toString());
                } else {
                    log.warn("UswClient:Nothing to send! Radiostations list empty? {}", inboundRs);
                }
                break;
            case fstWait:
                if (System.currentTimeMillis() - getLastPing() >= 3000) {
                    handler.send("PING");
                }
                break;
            case fstDropCall:
                handler.send("DROPCALL");
                break;
            case fstQuit:
                handler.send("QUIT");
                setStatus(Status.fstDone);
            case fstDone:
                terminate();
                break;
            default:
                log.error("UswClient:Undef status {}", getStatus());
                break;
        }
    }

    public UswClient setConversationOver() {
        setStatus(Status.fstDropCall);
        handleStatus();
        return this;
    }

    private void terminate() {
        setActive(false);
        tcpClient.interrupt();
    }

    @Override
    public void eventHandled(NetworkEvent event) {
        if (event.equals(NetworkEvent.THREAD_STOP) && isActive()) {
            log.debug("UswClient.eventHandled:NetworkEvent.THREAD_STOP");
            this.tcpClient = new TelnetClient(
                    event.getConn(),
                    handler,
                    this,
                    log
            ).fork();
            log.debug("\tUswClient started for: " + event.getConn().toString());
            setStatus(Status.fstConnect);
        } else if (!isActive()) {
            setStatus(Status.fstQuit);
        }
    }

    @Override
    public void run() {
        eventHandled(
                NetworkEvent
                        .THREAD_STOP
                        .setConn(
                                new ConnInfo(
                                        UswService.config.usw.address,
                                        UswService.config.usw.port
                                )
                        )
        );
    }

    ICallBack getCallback() {
        return callback;
    }

    private void setCallback(ICallBack callback) {
        this.callback = callback;
    }

    long getSendTime() {
        return sendTime;
    }

    void setSendTime(long sendTime) {
        this.sendTime = sendTime;
    }

    private long getLastPing() {
        return lastPing;
    }

    private void setLastPing(long lastPing) {
        this.lastPing = lastPing;
    }

    public boolean isAuthFlag() {
        return authFlag;
    }

    private void setAuthFlag(boolean authFlag) {
        this.authFlag = authFlag;
    }


    private void setStatus(Status status) {
        this.status = status;
    }

    RadioStation getInboundRs() {
        return inboundRs;
    }

    private void setInboundRs(RadioStation inboundRs) {
        this.inboundRs = inboundRs;
    }

    public String getConfID() {
        return confID;
    }

    public void setConfID(String confID) {
        this.confID = confID;
    }

    private boolean isActive() {
        return active;
    }

    void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public int compareTo(UswClient other) {
        return Long.compare(getId(), other.getId());
    }
}
