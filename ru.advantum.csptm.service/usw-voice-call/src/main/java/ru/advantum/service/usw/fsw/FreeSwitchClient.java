package ru.advantum.service.usw.fsw;

import org.freeswitch.esl.client.IEslEventListener;
import org.freeswitch.esl.client.inbound.InboundClientHandler;
import org.freeswitch.esl.client.inbound.InboundConnectionFailure;
import org.freeswitch.esl.client.inbound.InboundPipelineFactory;
import org.freeswitch.esl.client.internal.IEslProtocolListener;
import org.freeswitch.esl.client.transport.CommandResponse;
import org.freeswitch.esl.client.transport.SendMsg;
import org.freeswitch.esl.client.transport.event.EslEvent;
import org.freeswitch.esl.client.transport.message.EslMessage;
import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class FreeSwitchClient {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final List<IEslEventListener> eventListeners = new CopyOnWriteArrayList<IEslEventListener>();
    private final Executor eventListenerExecutor = Executors.newSingleThreadExecutor(
            new ThreadFactory() {
                AtomicInteger threadNumber = new AtomicInteger(1);

                public Thread newThread(Runnable r) {
                    return new Thread(r, "EslEventNotifier-" + threadNumber.getAndIncrement());
                }
            });
    private final Executor backgroundJobListenerExecutor = Executors.newSingleThreadExecutor(
            new ThreadFactory() {
                AtomicInteger threadNumber = new AtomicInteger(1);

                public Thread newThread(Runnable r) {
                    return new Thread(r, "EslBackgroundJobNotifier-" + threadNumber.getAndIncrement());
                }
            });

    private AtomicBoolean authenticatorResponded = new AtomicBoolean(false);
    private boolean authenticated;
    private CommandResponse authenticationResponse;
    private Channel channel;

    public boolean canSend() {
        return channel != null && channel.isConnected() && authenticated;
    }

    public synchronized void addEventListener(IEslEventListener listener) {
        if (listener != null) {
            eventListeners.add(listener);
        }
    }

    public synchronized boolean isAlive(IEslEventListener listener) {
        return listener != null && eventListeners.contains(listener);
    }

    public synchronized void removeEventListener(IEslEventListener listener) {
        if (listener != null) {
            eventListeners.remove(listener);
        }
        sendSyncApiCommand("hupall normal_clearing", ((CustomEventListener) listener).getConferenceUUID());

    }

    public void connect(String host, int port, String password, int timeoutSeconds) throws InboundConnectionFailure {
        // If already connected, disconnect first
        if (canSend()) {
            close();
        }

        // Configure this client
        ClientBootstrap bootstrap = new ClientBootstrap(
                new NioClientSocketChannelFactory(
                        Executors.newCachedThreadPool(),
                        Executors.newCachedThreadPool()));

        // Add ESL handler and factory
        InboundClientHandler handler = new InboundClientHandler(password, protocolListener);
        bootstrap.setPipelineFactory(new InboundPipelineFactory(handler));

        // Attempt connection
        ChannelFuture future = bootstrap.connect(new InetSocketAddress(host, port));

        // Wait till attempt succeeds, fails or timeouts
        if (!future.awaitUninterruptibly(timeoutSeconds, TimeUnit.SECONDS)) {
            throw new InboundConnectionFailure("Timeout connecting to " + host + ":" + port);
        }
        // Did not timeout
        channel = future.getChannel();
        // But may have failed anyway
        if (!future.isSuccess()) {
            log.warn("Failed to connect to [{}:{}]", host, port);
            log.warn("  * reason: {}", future.getCause());

            channel = null;
            bootstrap.releaseExternalResources();

            throw new InboundConnectionFailure("Could not connect to " + host + ":" + port, future.getCause());
        }

        //  Wait for the authentication handshake to call back
        while (!authenticatorResponded.get()) {
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // ignore
            }
        }

        if (!authenticated) {
            throw new InboundConnectionFailure("Authentication failed: " + authenticationResponse.getReplyText());
        }
    }

    public synchronized EslMessage sendSyncApiCommand(String command, String arg) {
        checkConnected();
        InboundClientHandler handler = (InboundClientHandler) channel.getPipeline().getLast();
        StringBuilder sb = new StringBuilder();
        if (command != null && !command.isEmpty()) {
            sb.append("api ");
            sb.append(command);
        }
        if (arg != null && !arg.isEmpty()) {
            sb.append(' ');
            sb.append(arg);
        }

        return handler.sendSyncSingleLineCommand(channel, sb.toString());
    }

    public String sendAsyncApiCommand(String command, String arg) {
        checkConnected();
        InboundClientHandler handler = (InboundClientHandler) channel.getPipeline().getLast();
        StringBuilder sb = new StringBuilder();
        if (command != null && !command.isEmpty()) {
            sb.append("bgapi ");
            sb.append(command);
        }
        if (arg != null && !arg.isEmpty()) {
            sb.append(' ');
            sb.append(arg);
        }

        return handler.sendAsyncCommand(channel, sb.toString());
    }

    public synchronized CommandResponse setEventSubscriptions(String format, String events) {
        // temporary hack
        if (!format.equals("plain")) {
            throw new IllegalStateException("Only 'plain' event format is supported at present");
        }

        checkConnected();
        InboundClientHandler handler = (InboundClientHandler) channel.getPipeline().getLast();
        StringBuilder sb = new StringBuilder();
        sb.append("event ");
        sb.append(format);
        if (events != null && !events.isEmpty()) {
            sb.append(' ');
            sb.append(events);
        }
        EslMessage response = handler.sendSyncSingleLineCommand(channel, sb.toString());

        return new CommandResponse(sb.toString(), response);
    }

    public CommandResponse cancelEventSubscriptions() {
        checkConnected();
        InboundClientHandler handler = (InboundClientHandler) channel.getPipeline().getLast();
        EslMessage response = handler.sendSyncSingleLineCommand(channel, "noevents");

        return new CommandResponse("noevents", response);
    }

    public CommandResponse addEventFilter(String eventHeader, String valueToFilter) {
        checkConnected();
        InboundClientHandler handler = (InboundClientHandler) channel.getPipeline().getLast();
        StringBuilder sb = new StringBuilder();
        if (eventHeader != null && !eventHeader.isEmpty()) {
            sb.append("filter ");
            sb.append(eventHeader);
        }
        if (valueToFilter != null && !valueToFilter.isEmpty()) {
            sb.append(' ');
            sb.append(valueToFilter);
        }
        EslMessage response = handler.sendSyncSingleLineCommand(channel, sb.toString());

        return new CommandResponse(sb.toString(), response);
    }


    public CommandResponse deleteEventFilter(String eventHeader, String valueToFilter) {
        checkConnected();
        InboundClientHandler handler = (InboundClientHandler) channel.getPipeline().getLast();
        StringBuilder sb = new StringBuilder();
        if (eventHeader != null && !eventHeader.isEmpty()) {
            sb.append("filter delete ");
            sb.append(eventHeader);
        }
        if (valueToFilter != null && !valueToFilter.isEmpty()) {
            sb.append(' ');
            sb.append(valueToFilter);
        }
        EslMessage response = handler.sendSyncSingleLineCommand(channel, sb.toString());

        return new CommandResponse(sb.toString(), response);
    }

    public CommandResponse sendMessage(SendMsg sendMsg) {
        checkConnected();
        InboundClientHandler handler = (InboundClientHandler) channel.getPipeline().getLast();
        EslMessage response = handler.sendSyncMultiLineCommand(channel, sendMsg.getMsgLines());

        return new CommandResponse(sendMsg.toString(), response);
    }

    public CommandResponse setLoggingLevel(String level) {
        checkConnected();
        InboundClientHandler handler = (InboundClientHandler) channel.getPipeline().getLast();
        StringBuilder sb = new StringBuilder();
        if (level != null && !level.isEmpty()) {
            sb.append("log ");
            sb.append(level);
        }
        EslMessage response = handler.sendSyncSingleLineCommand(channel, sb.toString());

        return new CommandResponse(sb.toString(), response);
    }

    public CommandResponse cancelLogging() {
        checkConnected();
        InboundClientHandler handler = (InboundClientHandler) channel.getPipeline().getLast();
        EslMessage response = handler.sendSyncSingleLineCommand(channel, "nolog");

        return new CommandResponse("nolog", response);
    }

    public CommandResponse close() {
        checkConnected();
        InboundClientHandler handler = (InboundClientHandler) channel.getPipeline().getLast();
        EslMessage response = handler.sendSyncSingleLineCommand(channel, "exit");

        return new CommandResponse("exit", response);
    }

    /*
     *  Internal observer of the ESL protocol
     */
    private final IEslProtocolListener protocolListener = new IEslProtocolListener() {
        public void authResponseReceived(CommandResponse response) {
            authenticatorResponded.set(true);
            authenticated = response.isOk();
            authenticationResponse = response;
            log.debug("Auth response success={}, message=[{}]", authenticated, response.getReplyText());
        }

        public void eventReceived(final EslEvent event) {
            log.debug("Event received [{}]", event);
            /*
             *  Notify listeners in a different thread in order to:
             *    - not to block the IO threads with potentially long-running listeners
             *    - generally be defensive running other people's code
             *  Use a different worker thread pool for async job results than for event driven
             *  events to keep the latency as low as possible.
             */
            if (event.getEventName().equals("BACKGROUND_JOB")) {
                for (final IEslEventListener listener : eventListeners) {
                    backgroundJobListenerExecutor.execute(() -> {
                        try {
                            listener.backgroundJobResultReceived(event);
                        } catch (Throwable t) {
                            log.error("Error caught notifying listener of job result [" + event + ']', t);
                        }
                    });
                }
            } else {
                for (final IEslEventListener listener : eventListeners) {
                    eventListenerExecutor.execute(() -> {
                        try {
                            listener.eventReceived(event);
                        } catch (Throwable t) {
                            log.error("Error caught notifying listener of event [" + event + ']', t);
                        }
                    });
                }
            }
        }

        public void disconnected() {
            log.info("Disconnected ..");
        }
    };

    private void checkConnected() {
        if (!canSend()) {
            throw new IllegalStateException("Not connected to FreeSWITCH Event Socket");
        }
    }
}
