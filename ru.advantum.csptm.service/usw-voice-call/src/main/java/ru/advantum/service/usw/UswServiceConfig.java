package ru.advantum.service.usw;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import ru.advantum.hub.V3.amtsconnect.AMTSHubConfig;
import ru.advantum.rabbitmq.config.RmqConsumerConfig;
import ru.advantum.rabbitmq.config.RmqDestinationConfig;
import ru.advantum.service.usw.utils.Funcs;

import java.util.*;

@Root(name = "usw-voice-call")
public class UswServiceConfig extends AMTSHubConfig {
    public static final String CONFIG_NAME = "usw-voice-call.xml";

    @Element(name = "rmq-ui")
    public RmqConsumerConfig rmqUi;

    @Element(name = "rmq-dest")
    public RmqDestinationConfig rmqDest;

    @Element(name = "usw-server", required = false)
    public Usw usw = new Usw();


    public static class Usw {
        public String cucmAddress="10.68.39.130";
        //main switcher
        @Attribute(name = "phone-zone-locale", required = false)
        public boolean phoneZoneLocale = true;

        @Attribute(required = false)
        public String address = "10.68.1.84";
        @Attribute(required = false)
        public int port = 5560;
        @Attribute(required = false)
        public String login = "login";
        @Attribute(required = false)
        public String password = "password";

        @Attribute(required = false)
        public String fsaddress = "10.68.38.35";
        @Attribute(required = false)
        public int fsport = 8021;
        @Attribute(name = "fs-timeout-sec", required = false)
        public int fsTimeout = 10000;
        @Attribute(required = false)
        public String fspassword = "ClueCon";

        @Override
        public String toString() {
            return String.format("usw-server:{%s}", Funcs.uniToString(this));
        }

        @Attribute(name = "sound-file-path", required = false)
        public String soundFilePath = "/u00/cdr/recordings";
        @Attribute(name = "sound-file-ext", required = false)
        public String soundFileExt = "wav";

        //ddmmyy-hh24miss-rs_num-disp_num
        @Attribute(required = false)
        public String recordFormat = "%s-%s.%s";

        public String callCommonRemoteTemplate = "{origination_uuid=%s}sofia/external/%s@%s &conference(%s)";

        public String callStationaryLocalTemplate = "{origination_uuid=%s}user/%s &conference(%s)";
        public String callStationaryRemoteTemplate = callCommonRemoteTemplate; //"{origination_uuid=%s}sofia/external/%s@%s &conference(%s)";

        public String callOwnerLocalTemplate = "{ignore_early_media=false,execute_on_originate='record_session::%s/%s/%s',origination_uuid=%s}user/%s &conference(%s)";
        public String callOwnerRemoteTemplate = "{ignore_early_media=false,execute_on_originate='record_session::%s/%s/%s',origination_uuid=%s}sofia/external/%s@%s &conference(%s)";

        @ElementList(name = "fxo-2-numbers", required = false)
        public List<Fxo2Number> fxo2Numbers = new ArrayList<>();

        public Map<String, List<String>> fxoNumbersMap = new HashMap<>();

        public String lookupIpByPhone(String phone) {
            for(Map.Entry<String, List<String>> e: fxoNumbersMap.entrySet()) {
                for ( String number: e.getValue()) {
                    if ( number.equals(phone)) {
                        return e.getKey();
                    }
                }
            }
            return cucmAddress;
        }

        @ElementList(name = "fs-local-numbers", required = false)
        public List<FsLocalNumbers> fsLocalNumbers = new ArrayList<>();

        Set<String> fsLocalNumbersList = new HashSet<>();
    }

    @ElementList(name = "fxo", required = false)
    public static class Fxo2Number {
        @Attribute(required = false)
        public String address = "10.68.36.201";

        @Attribute(required = false)
        public String numbers = "6429-6436";

        @Override
        public String toString() {
            return "fxo:{address=" + address + ", numbers='" + numbers + "'}";
        }
    }

    @ElementList(name = "local-numbers", required = false)
    public static class FsLocalNumbers {
        @Attribute(required = false)
        public String numbers = "1000-1004";
    }

    public UswServiceConfig prepare() {
        usw.cucmAddress = hub.host;
        for (UswServiceConfig.Fxo2Number f : usw.fxo2Numbers) {
            List<String> list = new ArrayList<>();
            String[] numbers = f.numbers.split("-");
            if (numbers.length == 0) {
                numbers = f.numbers.split(",");
                list.addAll(Arrays.asList(numbers));
            } else {
                int from = Integer.parseInt(numbers[0]);
                int to = Integer.parseInt(numbers[1]) + 1;
                for (int i = from; i < to; i++) {
                    list.add(String.valueOf(i));
                }
            }
            usw.fxoNumbersMap.put(f.address, list);

        }

        for( FsLocalNumbers localNumbers: usw.fsLocalNumbers) {
            String[] numbers = localNumbers.numbers.split("-");
            if (numbers.length == 0) {
                numbers = localNumbers.numbers.split(",");
                usw.fsLocalNumbersList.addAll(Arrays.asList(numbers));
            } else {
                int from = Integer.parseInt(numbers[0]);
                int to = Integer.parseInt(numbers[1]) + 1;
                for (int i = from; i < to; i++) {
                    usw.fsLocalNumbersList.add(String.valueOf(i));
                }
            }
        }

        return this;
    }

}
