package ru.advantum.service.usw;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.service.usw.fsw.FreeSwitchClient;

public class FswThread {
    private final Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName() + ":" + Thread.currentThread().getId());

    private final String callLine;
    private final String thisUUID;
    private String conferenceUUID;
    private final FreeSwitchClient freeSwitchClient;

    public FswThread(final FreeSwitchClient freeSwitchClient, String calline, String thisUUID, final String conferenceUUID) {
        this.freeSwitchClient = freeSwitchClient;
        this.callLine = calline;
        this.thisUUID = thisUUID;
        this.conferenceUUID = conferenceUUID;
    }

    @Override
    public int hashCode() {
        return thisUUID.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)
            return false;
        if (o == this)
            return true;
        if (o instanceof FswThread) {
            FswThread other = (FswThread) o;
            return thisUUID.equals(other.thisUUID);
        }
        return false;
    }

    public FswThread fork() {
        run();
        return this;
    }

    private void run() {


        try {

            if (freeSwitchClient.canSend()) {

                freeSwitchClient.sendSyncApiCommand("originate", callLine);
                log.info("\t conference {} sent originate {}", conferenceUUID, callLine);

            }
        } catch (Exception e) {
            log.error("", e);
        }
    }


}
