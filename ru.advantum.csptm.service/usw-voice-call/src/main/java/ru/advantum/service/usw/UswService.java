package ru.advantum.service.usw;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.service.usw.network.amqp.RmqHandler;

import java.io.File;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class UswService implements Runnable {
    public static UswServiceConfig config = getConfig();
    private final RmqHandler rmq;
    private ScheduledThreadPoolExecutor ex = new ScheduledThreadPoolExecutor(1);

    private Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());
    private VoipWorker vw;

    @Override
    public void run() {
        ex.execute(vw);
        log.info("\t ... started");
    }

    public void stop() {
        ex.shutdownNow();
        rmq.close();
        log.info("\t ... stopped");
    }

    UswService() throws Throwable {
        this.rmq = new RmqHandler().connect();
        this.vw = new VoipWorker(rmq);
    }

    public static void main(String... args) throws Throwable {
        UswService service = new UswService();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                service.stop();
            }
        });
        service.run();
    }

    public static UswServiceConfig getConfig() {
        if (System.getProperty("advantum.config.dir") == null) {
            System.setProperty("advantum.config.dir", "src/main/resources/extras/examples");
        }
        try {
            return Configuration.unpackConfig(UswServiceConfig.class, UswServiceConfig.CONFIG_NAME).prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
