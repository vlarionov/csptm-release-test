package ru.advantum.service.usw.uswclient;

class ParserChain {
    int code;
    String text;

    int getCode() {
        return code;
    }

    ParserChain setCode(int code) {
        this.code = code;
        return this;
    }

    String getText() {
        return text;
    }

    ParserChain setText(String text) {
        this.text = text;
        return this;
    }
}
