package ru.advantum.service.usw.uswclient;

public enum Status {
    fstConnect,
    fstAuth,
    fstCall,
    fstWait,
    fstDropCall,
    fstQuit,
    fstDone,
    fstDisconnect
}
