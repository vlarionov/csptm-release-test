package ru.advantum.service.usw.uswclient;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import ru.advantum.service.usw.network.tcp.TelnetClient;
import ru.advantum.service.usw.utils.Funcs;

@ChannelHandler.Sharable
class ClientHandler extends SimpleChannelInboundHandler<String> {
    private UswClient uswClient;
    private ChannelHandlerContext ctx;

    ClientHandler(UswClient uswClient) {
        this.uswClient = uswClient;
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        uswClient.log.debug("UswClient {}", cause);
        ctx.close();
    }

    @Override
    protected void messageReceived(ChannelHandlerContext channelHandlerContext, String msg) throws Exception {
        uswClient.log.debug("UswClient recv {}", msg);
        this.ctx = channelHandlerContext;
        if (msg.length() == 0) {
            uswClient.handleStatus();
        } else {
            for (String str : Funcs.readToStringArray(msg)) {
                uswClient.doResult(uswClient.parseString(str.toUpperCase()));
                uswClient.handleStatus();
            }
        }
    }

    void send(String cmd, String... params) {
        if (params.length > 0) {
            cmd += " ";
        }
        StringBuilder cmdBuilder = new StringBuilder(cmd);
        for (String s : params) {
            cmdBuilder.append(s);
        }
        cmd = cmdBuilder.toString();
        if (ctx != null) {
            cmd += TelnetClient.CRLF;
            uswClient.setSendTime(System.currentTimeMillis());
            uswClient.log.debug("UswClient: {} sent {} to {} ", new Object[]{
                            uswClient.getSendTime(),
                            cmd,
                            ctx.channel().toString()
                    }
            );
            try {
                ctx.writeAndFlush(cmd);
            } catch (Exception e) {
                uswClient.log.debug("UswClient sent error {} ", e);
            }
        } else {
            uswClient.log.error("UswClient Telnet client ont ready (channel invalid)");
        }
    }

    void send(String cmd) {
        this.send(cmd, new String[]{});
    }

}
