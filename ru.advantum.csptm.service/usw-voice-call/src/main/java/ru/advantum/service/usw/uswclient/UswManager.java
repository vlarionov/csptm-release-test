package ru.advantum.service.usw.uswclient;

import ru.advantum.service.usw.data.RadioStation;
import ru.advantum.service.usw.network.amqp.CallStatus;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;

public class UswManager {
    private static ConcurrentSkipListSet<UswClient> uswClients = new ConcurrentSkipListSet<>();

    public static void add(CallStatus callStatus, RadioStation inboundRs, ICallBack callback) {
        uswClients.add(new UswClient(callStatus, inboundRs, callback));
    }

    public static ConcurrentSkipListSet<UswClient> getUswClients() {
        return uswClients;
    }

    public static void over() {
        uswClients.clear();
    }
}
