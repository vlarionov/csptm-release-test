Application is looking for the config file with a name "udump.xml" (the example is below)

<udump taskTargetDb="udump" taskTimeout="30000" taskSourceDb="udump" taskLimit="10" taskRequestPeriod="1">
    <database    url="jdbc:postgresql://localhost:5432/udump?socketTimeout=20&amp;connectTimeout=20"
                 program="csptm-udump"
                 username="postgres"
                 password="postgres"
                 autoCommit="false"
                 poolable="true"
                 >

        <pool
                initialSize="10"
                maxActive="10"
                maxWait="5"
                maxIdle="5"
                maxOpenPreparedStatement="10"
                driverClassName = "org.postgresql.Driver"
                validationQuery = "SELECT 1"
        />
    </database>

    <post-result-handler type="POST" user-agent="Chrome/54.0.2840.71" timeout="5000"
                        delimiter=";" null-value="null"/>
</udump>

Every ${taskRequestPeriod} minutes it gets tasks from ${taskSourceDb} database, removes excess tasks from memory and executes new tasks.
The task may be a singleshot or a scheduled.
The singleshot task is a scheduled task with null period. This task executes only once.

BE AWARE: This service is not designed to perform "update, insert, delete" sql queries.

Default sql query task timeout is set to 30 seconds.
It is the only one constant that you can't set in the config.