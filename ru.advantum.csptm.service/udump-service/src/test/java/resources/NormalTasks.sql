drop table tasks if exists;

CREATE TABLE tasks
(
  sql varchar(255) NOT NULL,
  url varchar(255) NOT NULL,
  period smallint,
  id integer NOT NULL,
  CONSTRAINT pkey PRIMARY KEY (id)
);

insert into tasks (sql, url, period, id) values ('select * from tasks','http://127.0.0.1:8081',null,4);
insert into tasks (sql, url, period, id) values ('select * from tasks','http://127.0.0.1:8081',1,5);
