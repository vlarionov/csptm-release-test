drop table tasks if exists;

CREATE TABLE tasks
(
  sql varchar(255) NOT NULL,
  url varchar(255) NOT NULL,
  period smallint,
  id integer NOT NULL,
  CONSTRAINT pkey PRIMARY KEY (id)
);
