import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import ru.advantum.service.abstracts.AbstractTask;
import ru.advantum.service.abstracts.TaskExecutor;
import ru.advantum.service.abstracts.TaskExecutorFactory;
import ru.advantum.service.abstracts.TaskTimeLimiter;
import ru.advantum.service.limiters.UdumpSingleshotLimiter;
import ru.advantum.service.model.SingleshotTask;

import java.util.concurrent.Executors;

/**
 * Created by Leonid on 15.11.2016.
 */
public class UdumpSingleshotLimiterTest {
    @Test
    public void limitOk() throws Exception {
        SingleshotTask singleshotTask = new SingleshotTask("select 1", "localhost:8081", 1L);

        TaskExecutor executor = Mockito.mock(TaskExecutor.class);
        Mockito.doAnswer((params) -> {
            AbstractTask task = params.getArgumentAt(0, AbstractTask.class);
            Assert.assertEquals(singleshotTask, task);

            return Executors.newSingleThreadExecutor().submit(() -> {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {}
            });
        }).when(executor).execute(Mockito.any(AbstractTask.class));

        TaskExecutorFactory executorFactory = Mockito.mock(TaskExecutorFactory.class);
        Mockito.when(executorFactory.build()).thenReturn(executor);

        TaskTimeLimiter limiter = new UdumpSingleshotLimiter(executorFactory, Executors.newScheduledThreadPool(10));
        limiter.limit(singleshotTask);
    }
}
