import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import ru.advantum.service.UdumpTaskManager;
import ru.advantum.service.abstracts.AbstractTask;
import ru.advantum.service.abstracts.TaskManager;
import ru.advantum.service.abstracts.TaskTimeLimiter;
import ru.advantum.service.model.ScheduledTask;
import ru.advantum.service.model.SingleshotTask;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Leonid on 14.11.2016.
 */
public class UdumpTaskManagerTest {

    private SingleshotTask singleshotTask;
    private ScheduledTask scheduledTask;
    private TaskTimeLimiter singleshotTimeLimiter;
    private TaskTimeLimiter scheduledTimeLimiter;
    private Map<String, TaskTimeLimiter> executorMap;

    @Before
    public void before() {
        singleshotTask = new SingleshotTask("select 1", "localhost:8081", 1L);
        scheduledTask = new ScheduledTask("select 1", "localhost:8081", 2L, 2);

        executorMap = new HashMap<>();
        singleshotTimeLimiter = Mockito.mock(TaskTimeLimiter.class);
        Mockito.doAnswer((params) -> {
            Assert.assertEquals(params.getArgumentAt(0, AbstractTask.class), singleshotTask);
            return null;
        }).when(singleshotTimeLimiter).limit(Mockito.any());

        scheduledTimeLimiter = Mockito.mock(TaskTimeLimiter.class);
        Mockito.doAnswer((params) -> {
            Assert.assertEquals(params.getArgumentAt(0, AbstractTask.class), scheduledTask);
            return null;
        }).when(scheduledTimeLimiter).limit(Mockito.any());

        executorMap.put(singleshotTask.getClass().getName(), singleshotTimeLimiter);
        executorMap.put(scheduledTask.getClass().getName(), scheduledTimeLimiter);
    }

    @Test
    public void performTasksTest() {
        Map<Long, AbstractTask> taskMap = new HashMap<>();
        taskMap.put(singleshotTask.getId(), singleshotTask);
        taskMap.put(scheduledTask.getId(), scheduledTask);

        TaskManager manager = new UdumpTaskManager(executorMap);
        manager.perform(taskMap);
    }

    @Test
    public void performNewTest() {
        Map<Long, AbstractTask> taskMap = new HashMap<>();
        taskMap.put(singleshotTask.getId(), singleshotTask);
        taskMap.put(scheduledTask.getId(), scheduledTask);

        TaskManager manager = new UdumpTaskManager(executorMap);
        manager.perform(taskMap);

        SingleshotTask newSingleshotTask = new SingleshotTask("select 2", "localhost:8081", 1L);
        taskMap.put(newSingleshotTask.getId(), newSingleshotTask);

        singleshotTimeLimiter = Mockito.mock(TaskTimeLimiter.class);
        Mockito.doAnswer((params) -> {
            Assert.assertEquals(params.getArgumentAt(0, AbstractTask.class), newSingleshotTask); return null;
        }).when(singleshotTimeLimiter).limit(Mockito.any());

        executorMap.put(newSingleshotTask.getClass().getName(), singleshotTimeLimiter);

        manager.perform(taskMap);
    }

    @Test(expected = NullPointerException.class)
    public void performNullTest() {
        TaskManager manager = new UdumpTaskManager(executorMap);
        manager.perform(null);
    }

    @Test
    public void removeOldTest() {
        Map<Long, AbstractTask> taskMap = new HashMap<>();
        taskMap.put(singleshotTask.getId(), singleshotTask);
        taskMap.put(scheduledTask.getId(), scheduledTask);

        TaskManager manager = new UdumpTaskManager(executorMap);
        manager.perform(taskMap);

        taskMap.clear();
        manager.perform(taskMap);
        Mockito.verify(singleshotTimeLimiter, Mockito.times(1)).off(Mockito.any(AbstractTask.class));
        Mockito.verify(scheduledTimeLimiter, Mockito.times(1)).off(Mockito.any(AbstractTask.class));
    }
}
