import org.junit.Assert;
import org.junit.Test;
import ru.advantum.service.UdumpTaskReceiver;
import ru.advantum.service.abstracts.TaskReceiver;
import ru.advantum.service.model.ScheduledTask;
import ru.advantum.service.model.SingleshotTask;

/**
 * Created by Leonid on 14.11.2016.
 */
public class UdumpTaskReceiverTest {

    private static TaskReceiver receiver;

    @Test
    public void receiveNormalDataTest() throws Exception {
        DatabaseTestHelper.createTestDatabase("udump-test", "src/test/java/resources/NormalTasks.sql");

        receiver = new UdumpTaskReceiver("udump-test", tasks -> {
            Assert.assertEquals(tasks.size(), 2);
            Assert.assertEquals(tasks.get(4L).getClass().getName(), SingleshotTask.class.getName());
            Assert.assertEquals(tasks.get(5L).getClass().getName(), ScheduledTask.class.getName());
        });
        receiver.receive();
    }

    @Test
    public void receiveEmptyDataTest() throws Exception {
        DatabaseTestHelper.createTestDatabase("udump-test", "src/test/java/resources/EmptyTasks.sql");

        receiver = new UdumpTaskReceiver("udump-test", tasks -> Assert.assertTrue(tasks.isEmpty()));
        receiver.receive();
    }

    @Test()
    public void receiveNullIdDataTest() throws Exception {
        DatabaseTestHelper.createTestDatabase("udump-test", "src/test/java/resources/NullIdTasks.sql");

        receiver = new UdumpTaskReceiver("udump-test", tasks -> {
            Assert.assertEquals(tasks.size(), 1);
            Assert.assertEquals(tasks.get(1L).getClass().getName(), ScheduledTask.class.getName());
        });
        receiver.receive();
    }
}
