import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import ru.advantum.service.abstracts.AbstractTask;
import ru.advantum.service.abstracts.TaskExecutor;
import ru.advantum.service.abstracts.TaskExecutorFactory;
import ru.advantum.service.abstracts.TaskTimeLimiter;
import ru.advantum.service.limiters.UdumpScheduledLimiter;
import ru.advantum.service.model.ScheduledTask;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Leonid on 15.11.2016.
 */
public class UdumpScheduledLimiterTest {
    @Test
    public void limitOk() throws Exception {
        ScheduledTask scheduledTask = new ScheduledTask("select 1", "localhost:8081", 1L, 1000);

        TaskExecutor executor = Mockito.mock(TaskExecutor.class);
        Mockito.doAnswer((params) -> {
            AbstractTask task = params.getArgumentAt(0, AbstractTask.class);
            Assert.assertEquals(scheduledTask, task);

            return Executors.newSingleThreadExecutor().submit(() -> {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {}
            });
        }).when(executor).execute(Mockito.any(AbstractTask.class));

        TaskExecutorFactory executorFactory = Mockito.mock(TaskExecutorFactory.class);
        Mockito.when(executorFactory.build()).thenReturn(executor);

        TaskTimeLimiter limiter = new UdumpScheduledLimiter(executorFactory, Executors.newScheduledThreadPool(10), TimeUnit.MILLISECONDS);
        limiter.limit(scheduledTask);
    }
}
