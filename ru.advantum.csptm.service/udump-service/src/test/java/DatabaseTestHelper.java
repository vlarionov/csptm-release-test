import org.apache.ibatis.datasource.unpooled.UnpooledDataSource;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import ru.advantum.service.sql.SqlMapperSingleton;
import ru.advantum.service.sql.TaskMapper;

import javax.sql.DataSource;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Connection;

/**
 * Created by Leonid on 14.11.2016.
 */
public class DatabaseTestHelper {

    public static Connection connection;

    public static void createTestDatabase(String database, String sqlFile) throws IOException {
        DataSource dataSource = new UnpooledDataSource("org.hsqldb.jdbcDriver", "jdbc:hsqldb:mem:udump", "username", "sa");
        SqlMapperSingleton.registerInstance(
                database,
                new Environment(
                        database,
                        new JdbcTransactionFactory(),
                        dataSource),
                factory -> {
                    factory.getConfiguration().addMapper(TaskMapper.class);
                });

        SqlSession session = SqlMapperSingleton.getInstance(database).openSession();
        connection = session.getConnection();

        Reader reader = new FileReader(new File(sqlFile));
        ScriptRunner runner = new ScriptRunner(connection);
        runner.setErrorLogWriter(null);
        runner.runScript(reader);

        reader.close();
        session.close();
    }
}
