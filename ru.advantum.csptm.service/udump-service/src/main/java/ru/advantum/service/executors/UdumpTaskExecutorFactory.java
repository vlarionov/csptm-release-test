package ru.advantum.service.executors;

import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.service.abstracts.TaskExecutor;
import ru.advantum.service.abstracts.TaskExecutorFactory;
import ru.advantum.service.abstracts.TaskResultHandler;

/**
 * Класс фабрики исполнителей задач на выгрузку
 */
public class UdumpTaskExecutorFactory implements TaskExecutorFactory {

    private final TaskResultHandler resultHandler;
    private final DatabaseConnection database;
    private final long timeout;

    public UdumpTaskExecutorFactory(TaskResultHandler resultHandler, DatabaseConnection database, long timeout) {
        this.resultHandler = resultHandler;
        this.timeout = timeout;
        this.database = database;
    }

    /**
     *
     * @return Объект исполнителя задачи на выгрузку
     */
    @Override
    public TaskExecutor build() {
        return new UdumpTaskExecutor(resultHandler, database, timeout);
    }
}
