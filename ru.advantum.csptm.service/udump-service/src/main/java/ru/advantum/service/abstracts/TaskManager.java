package ru.advantum.service.abstracts;

import java.util.Map;

/**
 * Интерфейс менеджера задач выгрузки
 */
public interface TaskManager {
    void perform(Map<Long, AbstractTask> tasks);
}
