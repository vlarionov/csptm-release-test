package ru.advantum.service.model;

/**
 * Класс сущности задач по событиям
 */
public class EventTask extends ScheduledTask {
    private Long event_task_id;
    private Long task_id;
    private Long event_id;

    public EventTask() {
    }

    public EventTask(Long event_task_id, Long task_id, Long event_id) {
        this.event_task_id = event_task_id;
        this.task_id = task_id;
        this.event_id = event_id;
    }

    public EventTask(String sql, String url, Long id, Integer period, Long event_task_id, Long task_id, Long event_id) {
        super(sql, url, id, period);
        this.event_task_id = event_task_id;
        this.task_id = task_id;
        this.event_id = event_id;
    }

    public Long getEvent_task_id() {
        return event_task_id;
    }

    public Long getTask_id() {
        return task_id;
    }

    public Long getEvent_id() {
        return event_id;
    }


}
