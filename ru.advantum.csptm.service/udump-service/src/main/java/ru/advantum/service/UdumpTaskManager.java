package ru.advantum.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.service.abstracts.AbstractTask;
import ru.advantum.service.abstracts.TaskManager;
import ru.advantum.service.abstracts.TaskTimeLimiter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Класс управления задачами с периодом
 */
public class UdumpTaskManager implements TaskManager {

    private static final Logger log = LoggerFactory.getLogger(UdumpTaskManager.class);

    private final Map<String, TaskTimeLimiter> limiterMap;
    private final Map<Long, AbstractTask> taskMap;

    public UdumpTaskManager(Map<String, TaskTimeLimiter> limiterMap) {
        this(limiterMap, new ConcurrentHashMap<>());
    }

    public UdumpTaskManager(Map<String, TaskTimeLimiter> limiterMap, Map<Long, AbstractTask> taskMap) {
        this.limiterMap = limiterMap;
        this.taskMap = taskMap;
    }

    /**
     *
     * @param tasks карта задач на выгрузку с периодом
     *
     * Метод выполняет задачи с периодом
     */
    @Override
    public void perform(final Map<Long, AbstractTask> tasks) {
        removeOld(tasks);

        performNew(tasks);
    }

    /**
     *
     * @param tasks карта задач из БД
     *
     * Метод определяет новые задачи, в списке полученных из БД, и выполняет их
     */
    private void performNew(final Map<Long, AbstractTask> tasks) {
        tasks.forEach((taskId, task) -> {
            if (!taskMap.containsKey(task.getId())) {
                taskMap.put(task.getId(), task);
                limiterMap.get(task.getClass().getName()).limit(task);
                log.info("Add a new task " + task);
            }
        });
    }

    /**
     *
     * @param tasks карта задач из БД
     *
     * Метод определяет удаленные из БД задачи, останавливает их и удаляет из исполнения
     */
    private void removeOld(final Map<Long, AbstractTask> tasks) {
        List<AbstractTask> toRemove = new ArrayList<>();
        taskMap.forEach((taskId, task) -> {
            if (!tasks.containsKey(taskId)) {
                toRemove.add(task);
                limiterMap.get(task.getClass().getName()).off(task);
                log.info("Remove an old task " + task);
            }
        });

        toRemove.forEach(task -> taskMap.remove(task.getId()));
    }
}
