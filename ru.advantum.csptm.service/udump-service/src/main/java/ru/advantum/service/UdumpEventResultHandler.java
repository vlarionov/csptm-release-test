package ru.advantum.service;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.service.abstracts.AbstractTask;
import ru.advantum.service.config.UdumpRequestResultHandlerConfig;
import ru.advantum.service.sql.SqlMapperSingleton;
import ru.advantum.service.sql.TaskMapper;

import java.util.List;
import java.util.Map;

/**
 * Класс обработки результатов выполнения задач по событию
 */
public class UdumpEventResultHandler extends UdumpPostResultHandler {

    private static final Logger log = LoggerFactory.getLogger(UdumpEventResultHandler.class);

    private String database;

    public UdumpEventResultHandler(String database, UdumpRequestResultHandlerConfig config) {
        super(config);

        this.database = database;
    }

    //TODO добавить удаление задачи из карты лимитеров по выполнению

    /**
     *
     * @param result Список с результатами выполнения задачи
     * @param task Выполненная задача
     * @throws Exception
     *
     * Отмечает выполнение задачи по событию
     */
    @Override
    public void handle(List<Map<String, Object>> result, AbstractTask task) throws Exception {
        super.handle(result, task);

        try(SqlSession session = SqlMapperSingleton.getInstance(database).openSession()) {
            TaskMapper mapper = session.getMapper(TaskMapper.class);
            mapper.setEventDone(task.getId());

            session.commit();
            log.info("done event with id {}", task.getId());
        } catch (Exception ex) {
            log.error("", ex);
        }
    }
}
