package ru.advantum.service.abstracts;

/**
 * Интерфейс создания объекта исполнения задачи выгрузки
 */
public interface TaskExecutorFactory {
    TaskExecutor build();
}
