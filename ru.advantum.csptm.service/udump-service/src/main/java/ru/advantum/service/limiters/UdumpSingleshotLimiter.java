package ru.advantum.service.limiters;

import ru.advantum.service.abstracts.AbstractTask;
import ru.advantum.service.abstracts.TaskExecutorFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * Класс ограничителя одноразовых задач
 */
public class UdumpSingleshotLimiter extends UdumpTaskLimiter {
    private ExecutorService executorService;

    public UdumpSingleshotLimiter(TaskExecutorFactory taskExecutor, ExecutorService executorService) {
        super(taskExecutor);
        this.executorService = executorService;
    }

    /**
     *
     * @param task задача выгрузки
     * @return Объект состояния потока выполнения задачи
     *
     * Данный метод запускает одноразовые задачи и следит за ходом исполнения
     */
    @Override
    protected Future submit(AbstractTask task) {
        return executorService.submit(getRunnableLimit(task));
    }
}
