package ru.advantum.service.abstracts;

/**
 * Интерфейс ограничения времени выполнения задачи
 */
public interface TaskTimeLimiter {
    void limit(AbstractTask task);
    void off(AbstractTask task);
}
