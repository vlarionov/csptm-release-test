package ru.advantum.service.executors;

import org.postgresql.jdbc.PgConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.service.abstracts.AbstractTask;
import ru.advantum.service.abstracts.TaskExecutor;
import ru.advantum.service.abstracts.TaskResultHandler;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Класс - реализация интерфейса исполнителя задач выгрузки
 */
public class UdumpTaskExecutor implements TaskExecutor {

    private static final Logger log = LoggerFactory.getLogger(UdumpTaskExecutor.class);

    //поле обработки результата
    private final TaskResultHandler resultHandler;
    //настройки соединения с БД
    private final DatabaseConnection database;
    //таймаут выполнения задачи
    private final long timeout;
    //соединение с БД
    private volatile Connection connection;
    //флаг остановки выполнения
    private volatile Boolean stop;

    public UdumpTaskExecutor(final TaskResultHandler resultHandler, final DatabaseConnection database, final long timeout) {
        this.resultHandler = resultHandler;
        this.database = database;
        this.timeout = timeout;
        this.stop = false;
    }

    /**
     * @param task Задача выгрузки из БД
     * @return Исполнимый объект выгрузки на основе задачи
     *
     * Метод построения функции исполнения выгрузки из задачи
     */
    private Runnable buildRunnable(AbstractTask task) {
        return () -> {
            //TODO с таким подходом будет есть много памяти, может попробовать сделать через вывод в стрим сразу?
            try (Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery(task.getSql())) {
                    ResultSetMetaData metaData = resultSet.getMetaData();
                    int columns = metaData.getColumnCount();

                    ArrayList<Map<String, Object>> data = new ArrayList<>(50);
                    while (resultSet.next()) {
                        if (stop) {
                            break;
                        }

                        Map<String, Object> row = new HashMap<>(columns);
                        for(int i = 1; i <= columns; ++i) {
                            row.put(metaData.getColumnName(i), resultSet.getObject(i));
                        }
                        data.add(row);
                    }

                    if (!stop) {
                        resultHandler.handle(data, task);
                    }
                }
            } catch (Exception ex) {
                log.error("Unable to perform task " + task, ex);
            }
        };
    }

    /**
     *
     * @param task задача выполнения выгрузки
     * @throws Exception
     *
     * Метод выполнения задачи на выгрузку
     */
    @Override
    public void execute(AbstractTask task) throws Exception {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        connection = DatabaseConnection.getConnection(database);
        Future future = executorService.submit(buildRunnable(task));

        try {
            future.get(timeout, TimeUnit.MILLISECONDS);
            log.info("Done task " + task);
        } catch (TimeoutException | InterruptedException e) {
            log.error("Task execution interrupted: " + task , e);
        } catch (ExecutionException  ex) {
            log.error("reportThread error " + task, ex);
        } catch (Exception ex) {
            log.error("error " + task, ex);
        } finally {
            if (!future.isDone()) {
                future.cancel(true);
            }

            if (!connection.isClosed() && connection.isWrapperFor(PgConnection.class)) {
                stop = true;
                connection.unwrap(PgConnection.class).cancelQuery();
                connection.close();
            }
        }
    }
}
