package ru.advantum.service.abstracts;

/**
 * Класс описания сущности задачи для выгрузки, каждая задача должна содержать численный идентификатор,
 * строку - код sql запроса, строку - url отправки результата запроса.
 */
public abstract class AbstractTask {
    private Long id;
    private String sql;
    private String url;

    public AbstractTask() {
    }

    public AbstractTask(String sql, String url, Long id) {
        this.sql = sql;
        this.url = url;
        this.id = id;
    }

    public String getSql() {
        return sql;
    }

    public String getUrl() {
        return url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractTask that = (AbstractTask) o;

        if (!id.equals(that.id)) return false;
        if (!sql.equals(that.sql)) return false;
        return url.equals(that.url);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + sql.hashCode();
        result = 31 * result + url.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return id.toString() + "-" + sql;
    }
}

