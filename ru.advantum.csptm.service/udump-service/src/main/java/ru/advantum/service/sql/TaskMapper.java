package ru.advantum.service.sql;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.StatementType;
import ru.advantum.service.model.ScheduledTask;

import java.util.List;

/**
 * Интерфейс запросов к БД
 */
public interface TaskMapper {
    /**
     *
     * @return список периодических задач
     *
     * Запрос на получение периодических задач из БД
     */
    //@Select("select * from udump.tasks where period is not null and period != 0")
    @Select("select * from udump.udump_pkg$export_tasks()")
    @ResultType(ScheduledTask.class)
    List<ScheduledTask> getTasks();

    /**
     *
     * @return список задач создаваемых по событиям
     *
     * Запрос на получение из БД списка задач по событиям
     */
    /*@Select("select ts.sql, ts.url, ts.period, es.id from udump.events as es " +
            "inner join udump.event_task as te on es.table_name = te.table_name and es.event_name = te.event_name " +
            "inner join udump.tasks as ts on te.task_id = ts.id where es.is_done = false")*/
    @Select("select * from udump.udump_pkg$export_event_tasks()")
    @ResultType(ScheduledTask.class)
    List<ScheduledTask> getEventTasks();

    /**
     *
     * @param id идентификатор задача по событию
     *
     * Маркировка задачи по событию как выполненной
     */
    //@Update("update udump.events set is_done=true where id = #{id}")
    @Update("{" +
            "call udump.udump_pkg$set_event_done(#{id})" +
            "}")
    @Options(statementType = StatementType.CALLABLE, keyColumn = "id")
    void setEventDone(@Param("id") Long id);
}
