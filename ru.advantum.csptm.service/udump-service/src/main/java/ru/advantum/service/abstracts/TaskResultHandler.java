package ru.advantum.service.abstracts;

import java.util.List;
import java.util.Map;

/**
 * Интерфейс обработки результата выгрузки
 */
public interface TaskResultHandler {
    void handle(List<Map<String, Object>> result, AbstractTask task) throws Exception;
}
