package ru.advantum.service.abstracts;

/**
 * Интерфейс исполнения задачи выгрузки
 */
public interface TaskExecutor<T> {
    void execute(AbstractTask task) throws Exception;
}
