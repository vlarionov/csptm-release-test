package ru.advantum.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.service.abstracts.AbstractTask;
import ru.advantum.service.abstracts.TaskManager;
import ru.advantum.service.abstracts.TaskTimeLimiter;

import java.util.Map;

/**
 * Класс управления задачами по событиям
 */
public class UdumpEventManager  implements TaskManager {
    private static final Logger log = LoggerFactory.getLogger(UdumpEventManager.class);

    private final Map<String, TaskTimeLimiter> limiterMap;

    public UdumpEventManager(Map<String, TaskTimeLimiter> limiterMap) {
        this.limiterMap = limiterMap;
    }

    /**
     *
     * @param tasks карта задач на выгрузку по событию
     *
     * Метод выполняет задачи по событиям
     */
    @Override
    public void perform(final Map<Long, AbstractTask> tasks) {
        for (AbstractTask task : tasks.values()) {
            limiterMap.get(task.getClass().getName()).limit(task);
            log.info("Add a new event task " + task);
        }
    }
}
