package ru.advantum.service.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Класс конфига для настройки обработки результатов выгрузки, отправка на указанный в задаче url
 */
@Root(name = "post-result-handler")
public class UdumpRequestResultHandlerConfig {
    /**
     * Содержит тип http запроса, например POST, GET
     */
    @Attribute(name = "type")
    private String type;

    /**
     * Тип ПО клиента, указывается для совместимости с принимающим сервером
     */
    @Attribute(name = "user-agent")
    private String userAgent;

    /**
     * Таймаут соединения с принимающим сервером в миллисекундах
     */
    @Attribute(name = "timeout")
    private Integer timeout;

    /**
     * Разделитель csv данных - результата выгрузки
     */
    @Attribute(name = "delimiter")
    private String delimiter;

    /**
     * null значение в csv данных - результатах выгрузки, подставляется если отсутствует значение поля
     */
    @Attribute(name = "null-value")
    private String nullValue;

    /**
     * символ экранирования
     */
    @Attribute(name = "escape")
    private Character escape;

    public String getType() {
        return type;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public String getDelimiter() {
        return delimiter;
    }

    public String getNullValue() {
        return nullValue;
    }

    public Character getEscape() {
        return escape;
    }
}
