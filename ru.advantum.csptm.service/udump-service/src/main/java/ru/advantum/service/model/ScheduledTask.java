package ru.advantum.service.model;

import ru.advantum.service.abstracts.AbstractTask;

/**
 * Класс сущности задач по расписанию
 */
public class ScheduledTask extends AbstractTask {

    private Integer period;

    public ScheduledTask() {
    }

    public ScheduledTask(String sql, String url, Long id, Integer period) {
        super(sql, url, id);
        this.period = period;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }
}
