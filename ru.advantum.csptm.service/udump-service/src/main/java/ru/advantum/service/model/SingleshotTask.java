package ru.advantum.service.model;

import ru.advantum.service.abstracts.AbstractTask;

/**
 * Класс сущности одноразовых задач
 */
public class SingleshotTask extends AbstractTask {

    public SingleshotTask() {
    }

    public SingleshotTask(String sql, String url, Long id) {
        super(sql, url, id);
    }
}
