package ru.advantum.service.limiters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.service.abstracts.AbstractTask;
import ru.advantum.service.abstracts.TaskExecutorFactory;
import ru.advantum.service.abstracts.TaskTimeLimiter;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;

/**
 * Класс ограничения времени исполнения задачи выгрузки
 */
public abstract class UdumpTaskLimiter implements TaskTimeLimiter {

    private static final Logger log = LoggerFactory.getLogger(UdumpTaskLimiter.class);

    private TaskExecutorFactory taskExecutorFactory;
    protected Map<AbstractTask, Future> executedTasks;

    public UdumpTaskLimiter(TaskExecutorFactory taskExecutorFactory) {
        this.taskExecutorFactory = taskExecutorFactory;
        this.executedTasks = new ConcurrentHashMap<>();
    }

    /**
     *
     * @param task задача выгрузки
     * @return Исполнимый объект задачи
     */
    protected Runnable getRunnableLimit(AbstractTask task) {
        return () -> {
            try {
                taskExecutorFactory.build().execute(task);
            } catch (Exception ex) {
                log.error("Unable to execute task " + task);
            }
        };
    }

    /**
     *
     * @param task задача выгрузки
     * Метод исполняет задачу и заносит ее в список задач, находящихся на исполнении
     */
    @Override
    public void limit(AbstractTask task) {
        executedTasks.put(task, submit(task));
    }

    /**
     *
     * @param task задача выгрузки
     * @return Объект состояния потока выполнения задачи
     *
     * Метод запуска задач, определяется в наследнике
     */
    protected abstract Future submit(AbstractTask task);

    /**
     *
     * @param task задача выгрузки
     * Остановка исполнения задачи, удаление из списка находящихся на исполнении
     *
     */
    @Override
    public void off(AbstractTask task) {
        executedTasks.get(task).cancel(false);
        executedTasks.remove(task);
    }
}
