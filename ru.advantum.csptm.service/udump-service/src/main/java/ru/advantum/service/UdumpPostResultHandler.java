package ru.advantum.service;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;
import ru.advantum.service.abstracts.AbstractTask;
import ru.advantum.service.abstracts.TaskResultHandler;
import ru.advantum.service.config.UdumpRequestResultHandlerConfig;
import ru.advantum.service.exceptions.UdumpResultHandlerException;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.FileWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * Класс обработки результата выполнения задачи на выгрузку
 */
public class UdumpPostResultHandler implements TaskResultHandler {

    private final UdumpRequestResultHandlerConfig config;

    public UdumpPostResultHandler(UdumpRequestResultHandlerConfig config) {
        this.config = config;
    }

    /**
     *
     * @param result Список с результатами выполнения задачи
     * @param task Выполненная задача
     * @throws Exception
     *
     * Метод отправляет http запрос с csv представлением результатов выполнения задачи на выгрузку
     */
    @Override
    public void handle(List<Map<String, Object>> result, AbstractTask task) throws Exception {
        URL url = new URL(task.getUrl());
        HttpURLConnection connection;
        if (url.getProtocol().equals("https")) {
            connection = (HttpsURLConnection) url.openConnection();
        } else {
            connection = (HttpURLConnection) url.openConnection();
        }

        connection.setRequestMethod(config.getType());
        connection.setRequestProperty("User-Agent", config.getUserAgent());
        connection.setRequestProperty("Content-type", "text/plain; charset=UTF-8");
        connection.setConnectTimeout(config.getTimeout());

        connection.setDoOutput(true);

        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withDelimiter(config.getDelimiter().charAt(0))
                .withNullString(config.getNullValue()).withFirstRecordAsHeader().withEscape(config.getEscape());

        StringBuilder builder = new StringBuilder();
        try (CSVPrinter csvPrinter = new CSVPrinter(builder, csvFileFormat);
             DataOutputStream stream = new DataOutputStream(connection.getOutputStream())) {

            csvPrinter.printRecord(result.get(0).keySet());
            for (Map<String, Object> row : result) {
                csvPrinter.printRecord(row.values());
            }

            stream.writeUTF((builder.toString()));
            stream.flush();

            /*for (Map.Entry<String, Object> row : result.get(0).entrySet()) {
                writer.writeBytes(row.getKey() == null ? config.getNullValue() : row.getKey());
                writer.writeBytes(config.getDelimiter());
            }
            writer.writeBytes("\n");

            for (Map<String, Object> row : result) {
                for (Map.Entry<String, Object> column : row.entrySet()) {
                    Object value = column.getValue();
                    writer.writeBytes(value == null ? config.getNullValue() : value.toString());
                    writer.writeBytes(config.getDelimiter());
                }

                writer.writeBytes("\n");
            }

            writer.flush();*/
        }

        if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
            connection.disconnect();
            throw new UdumpResultHandlerException("Wrong response code: " + connection.getResponseCode());
        }

        connection.disconnect();
    }
}
