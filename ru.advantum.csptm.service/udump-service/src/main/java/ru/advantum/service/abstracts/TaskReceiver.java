package ru.advantum.service.abstracts;

/**
 * Интерфейс получения задач выгрузки
 */
public interface TaskReceiver {
    void receive() throws Exception;
}
