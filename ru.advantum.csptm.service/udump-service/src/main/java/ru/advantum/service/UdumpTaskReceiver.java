package ru.advantum.service;


import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.service.abstracts.AbstractTask;
import ru.advantum.service.abstracts.TaskManager;
import ru.advantum.service.abstracts.TaskReceiver;
import ru.advantum.service.model.ScheduledTask;
import ru.advantum.service.model.SingleshotTask;
import ru.advantum.service.sql.SqlMapperSingleton;
import ru.advantum.service.sql.TaskMapper;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Класс получения задач с периодом из БД
 */
public class UdumpTaskReceiver implements TaskReceiver {

    private static final Logger log = LoggerFactory.getLogger(UdumpTaskReceiver.class);

    private final String database;
    private final TaskManager manager;

    public UdumpTaskReceiver(String database, TaskManager manager) {
        this.database = database;
        this.manager = manager;
    }

    /**
     * Метод получения задач с периодом из БД
     * @throws Exception
     */
    @Override
    public void receive() throws Exception {
        try (SqlSession session = SqlMapperSingleton.getInstance(database).openSession()) {
            List<ScheduledTask> tasks = session.getMapper(TaskMapper.class).getTasks();
            Map<Long, AbstractTask> newTasks = tasks.stream()
                    .filter(task -> task.getPeriod() != null && !task.getPeriod().equals(0))
                    .collect(Collectors.toMap(AbstractTask::getId, task -> task));

            log.info("size of tasks in data base {}", tasks.size());

            manager.perform(newTasks);
        }
    }
}
