package ru.advantum.service.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.config.common.db.DatabaseConnection;

/**
 * Класс конфига сервиса
 */
@Root(strict = false, name = "udump")
public class UdumpServiceConfig {
    /**
     * Настройки подключения к БД
     */
    @Element(name = "database")
    private DatabaseConnection database;

    /**
     * Настройки отправки результата выполнения задачи на выгрузку
     */
    @Element(name = "post-result-handler")
    private UdumpRequestResultHandlerConfig handlerConfig;

    /**
     * Имя БД - целевой для задач (БД в которой выполняются sql запросы задачи)
     */
    @Attribute(name = "taskTargetDb")
    private String taskTargetDb;

    /**
     * Таймаут выполнения задачи
     */
    @Attribute(name = "taskTimeout")
    private Integer taskTimeout;

    /**
     * Имя БД - источника задач на выгрузку
     */
    @Attribute(name = "taskSourceDb")
    private String taskSourceDb;

    /**
     * Лимит одновременно исполняемых задач
     */
    @Attribute(name = "taskLimit")
    private Integer taskLimit;

    /**
     * Период опроса (в минутах) списка задач, добавления новых, удаления старых и т.д.
     */
    @Attribute(name = "taskRequestPeriod")
    private Integer taskRequestPeriod;

    public Integer getTaskRequestPeriod() {
        return taskRequestPeriod;
    }

    public DatabaseConnection getDatabase() {
        return database;
    }

    public String getTaskTargetDb() {
        return taskTargetDb;
    }

    public Integer getTaskTimeout() {
        return taskTimeout;
    }

    public String getTaskSourceDb() {
        return taskSourceDb;
    }

    public Integer getTaskLimit() {
        return taskLimit;
    }

    public UdumpRequestResultHandlerConfig getHandlerConfig() {
        return handlerConfig;
    }
}
