package ru.advantum.service.limiters;

import ru.advantum.service.abstracts.AbstractTask;
import ru.advantum.service.abstracts.TaskExecutorFactory;
import ru.advantum.service.model.ScheduledTask;

import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Класс ограничителя времени выполнения задач по расписанию
 */
public class UdumpScheduledLimiter extends UdumpTaskLimiter {
    private final ScheduledExecutorService executorService;
    private final TimeUnit taskTimeUnit;

    public UdumpScheduledLimiter(TaskExecutorFactory taskExecutor, ScheduledExecutorService executorService, TimeUnit taskTimeUnit) {
        super(taskExecutor);
        this.executorService = executorService;
        this.taskTimeUnit = taskTimeUnit;
    }

    /**
     *
     * @param task задача выгрузки
     * @return Объект состояния потока выполнения задачи
     *
     * Данный метод запускает задачи по расписанию и следит за ходом исполнения
     */
    @Override
    protected Future submit(AbstractTask task) {
        ScheduledTask scheduledTask = (ScheduledTask) task;
        return executorService.scheduleAtFixedRate(getRunnableLimit(task), scheduledTask.getPeriod(), scheduledTask.getPeriod(), taskTimeUnit);
    }
}
