package ru.advantum.service;

import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.Configuration;
import ru.advantum.service.abstracts.*;
import ru.advantum.service.config.UdumpServiceConfig;
import ru.advantum.service.executors.UdumpTaskExecutorFactory;
import ru.advantum.service.limiters.UdumpScheduledLimiter;
import ru.advantum.service.limiters.UdumpSingleshotLimiter;
import ru.advantum.service.model.ScheduledTask;
import ru.advantum.service.model.SingleshotTask;
import ru.advantum.service.sql.SqlMapperSingleton;
import ru.advantum.service.sql.TaskMapper;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Основной класс сервиса универсальных выгрузок
 */
public class UdumpService {

    private static Logger log = LoggerFactory.getLogger(UdumpService.class);

    /**
     *
     * @param args
     * @throws Exception
     *
     * Точка входа
     */
    public static void main(String[] args) throws Exception {
        //читаем конфиг
        UdumpServiceConfig config = Configuration.unpackConfig(UdumpServiceConfig.class, "udump.xml");

        //регистрируем БД
        SqlMapperSingleton.registerInstance(
                config.getTaskSourceDb(),
                new Environment(
                        config.getTaskSourceDb(),
                        new JdbcTransactionFactory(),
                        config.getDatabase().getConnectionPool()),
                factory -> {
                    factory.getConfiguration().addMapper(TaskMapper.class);
                });

        //создаем пул потоков на котором будут выполняться задачи
        ScheduledExecutorService threadPool = new ScheduledThreadPoolExecutor(config.getTaskLimit());

        //создаем обработчики результатов исполнения выгрузки
        //для периодических задач
        TaskResultHandler dbResultHandler = new UdumpPostResultHandler(config.getHandlerConfig());
        //для задач по событиям
        TaskResultHandler eventResultHandler = new UdumpEventResultHandler(config.getTaskSourceDb(), config.getHandlerConfig());

        //создаем фабрики исполнителей задач на выгрузку
        TaskExecutorFactory udumpTaskExecutorFactory = new UdumpTaskExecutorFactory(dbResultHandler, config.getDatabase(), config.getTaskTimeout());
        TaskExecutorFactory udumpEventExecutorFactory = new UdumpTaskExecutorFactory(eventResultHandler, config.getDatabase(), config.getTaskTimeout());

        //создаем ограничители времени исполнения задачи
        TaskTimeLimiter udumpSingleshotLimiter = new UdumpSingleshotLimiter(udumpEventExecutorFactory, threadPool);
        TaskTimeLimiter udumpScheduledLimiter = new UdumpScheduledLimiter(udumpTaskExecutorFactory, threadPool, TimeUnit.MINUTES);

        //создаем карту ограничителей
        Map<String, TaskTimeLimiter> limiterMap = new ConcurrentHashMap<>();
        limiterMap.put(SingleshotTask.class.getName(), udumpSingleshotLimiter);
        limiterMap.put(ScheduledTask.class.getName(), udumpScheduledLimiter);

        //создаем объекты управления задачами и объекты получения задач из БД
        TaskManager manager = new UdumpTaskManager(limiterMap);
        TaskManager eventManager = new UdumpEventManager(limiterMap);
        TaskReceiver receiver = new UdumpTaskReceiver(config.getTaskSourceDb(), manager);
        TaskReceiver eventReceiver = new UdumpEventReceiver(config.getTaskSourceDb(), eventManager);

        //Запускаем поток опроса задач из БД
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(() -> {
            try {
                receiver.receive();
                eventReceiver.receive();
            } catch (Exception e) {
                log.error("receiver error " + e.getMessage());
            }
        }, 0, config.getTaskRequestPeriod(), TimeUnit.MINUTES);
    }
}
