package ru.advantum.service.exceptions;

/**
 * Класс исключения при ошибке обработки результата выгрузки
 */
public class UdumpResultHandlerException extends Exception {
    public UdumpResultHandlerException() {
    }

    public UdumpResultHandlerException(String message) {
        super(message);
    }

    public UdumpResultHandlerException(String message, Throwable cause) {
        super(message, cause);
    }

    public UdumpResultHandlerException(Throwable cause) {
        super(cause);
    }
}
