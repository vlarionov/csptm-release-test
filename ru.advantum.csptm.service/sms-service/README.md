sms-service - Сервис отправки SMS
============================

1. Для поставщика МТС - необходимо задать:

     `smsServiceName="MTS"` - Наименование поставщика сервиса SMS 
     `smsServiceUserName`   - Имя пользователя  
     `smsServicePassword`   - Пароль

2. Сервис отправляет SMS сообщения источником которых является очередь `sms_out`. Тип сообщения `SmsMessage`.  

  Формат сообщения:
  {"id":"1","phone":"3454533541","text":"message text"}

3. Сервис отправляет сообщения о доставке в `exchange="sms_incoming"` и routeKey 'ack' и типом сообщения AckMessage

4. Сервис отправляет входящие сообщения в `exchange="sms_incoming"` и routeKey 'sms' и типом сообщения SmsMessage

5. Сервис принимает входящие сообщения по вдресу http://host:port/ForwardMessage?msid=79161060500&message=testmsg 

Настройки сервиса 
-----------
sms-service/advconf/sms-service.xml

``````````````    
<sms-service
        smsServiceName="MTS"
        smsServiceUserName="**"
        smsServicePassword="**">
    
    <rmq-service>
        <rmq-connection host="mgt-hpx01.mgt.dev"
                        virtualHost="csptm"
                        username="**"
                        password="**"
                        heartbeatInterval="10"
                        automaticRecovery="true"/>
        <rmq-consumer queue="sms_out"/>
        <rmq-destination exchange="sms_incoming"/>
    </rmq-service>

</sms-service>

``````````````

Настройки логирования 
-----------

Используется библиотека логирования по умолчанию Spring Boot (logback)  
sms-service/advconf/logback.xml
