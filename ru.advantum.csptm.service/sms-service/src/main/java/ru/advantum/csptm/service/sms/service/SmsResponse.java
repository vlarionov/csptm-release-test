package ru.advantum.csptm.service.sms.service;

public class SmsResponse {

    public SmsResponse(String providerMessageID, String errorMessage) {
        this.providerMessageID = providerMessageID;
        this.errorMessage = errorMessage;
    }

    private final String providerMessageID;

    public String getProviderMessageID() {
        return providerMessageID;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    private final String errorMessage;
}
