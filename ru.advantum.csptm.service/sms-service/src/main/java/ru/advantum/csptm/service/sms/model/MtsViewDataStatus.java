package ru.advantum.csptm.service.sms.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "OperationResult", namespace = "http://mcommunicator.ru/M2M/")
public class MtsViewDataStatus {

    @XmlElement
    public int code;

    @XmlElement
    public String description;

    public MtsViewDataStatus() {
    }

    public MtsViewDataStatus(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public static MtsViewDataStatus okStatus() {
        return new MtsViewDataStatus(0, "OK");
    }

    public static MtsViewDataStatus systemFailureStatus() {
        return new MtsViewDataStatus(1, "SYSTEM_FAILURE");
    }

    public static MtsViewDataStatus incorrectPasswordStatus() {
        return new MtsViewDataStatus(103, "INCORRECT_PASSWORD");
    }

    public static MtsViewDataStatus msidFormatErrorStatus() {
        return new MtsViewDataStatus(506, "MSID_FORMAT_ERROR");
    }

    public static MtsViewDataStatus messageFormatErrorStatus() {
        return new MtsViewDataStatus(700, "MESSAGE_FORMAT_ERROR");
    }
}
