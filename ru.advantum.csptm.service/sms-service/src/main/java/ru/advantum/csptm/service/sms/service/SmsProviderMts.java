package ru.advantum.csptm.service.sms.service;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.load.Persister;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import ru.advantum.csptm.service.sms.model.SmsMessage;
import ru.advantum.csptm.service.sms.model.SmsStatus;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * МТС - www.mcommunicator.ru
 * @author abc <alekseenkov@advantum.pro>
 */
public class SmsProviderMts implements SmsProvider {
    private static final Logger log = LoggerFactory.getLogger(SmsProviderMts.class);

    //private final String smsUrl = "https://www.mcommunicator.ru/m2m/m2m_api.asmx/";
    private final String smsUrl = "https://api.mcommunicator.ru/m2m/m2m_api.asmx/";
    private final String userName;
    private final String passHash;
    private final String naming;
    private final String incomingPhone;
    private final int providerTimeZone;

    private final Serializer serializer;
    private final DateTimeFormatter dateTimeformatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

    SmsProviderMts(String userName, String passHash, String naming, String incomingPhone, int providerTimeZone) {
        this.userName = userName;
        this.passHash = passHash;
        this.naming = naming;
        this.serializer = new Persister();
        this.incomingPhone = incomingPhone;
        this.providerTimeZone = providerTimeZone;
    }

    private String parseSmsResponse(String phone, String txtResponse) {
        if (txtResponse.endsWith("</long>")) {
            int pos = txtResponse.indexOf("<long");
            if (pos > 0) {
                pos = txtResponse.indexOf('>', pos) + 1;
                if (pos > 0) return txtResponse.substring(pos, txtResponse.length() - 7);
            }
        } else {
            log.error("Can't send SNS to {} : {}", phone, txtResponse);
        }
        return "";
    }

    @Override
    public SmsResponse sendMessage(SmsMessage message) {

        SmsResponse result = null;

        URI uri;
        try {
            String strUrl = smsUrl + "SendMessage";
            uri = new URIBuilder(strUrl)
                .addParameter("msid", message.getPhone())
                .addParameter("naming", naming)
                .addParameter("message", message.getText())
                .addParameter("login", userName)
                .addParameter("password", passHash)
                .build();
        } catch (URISyntaxException e) {
            log.error(e.getMessage());
            return null;
        }

        HttpGet httpGet = new HttpGet(uri);

        try (CloseableHttpClient client = HttpClients.custom().build()) {
            try (CloseableHttpResponse response = client.execute(httpGet)) {
                int status = response.getStatusLine().getStatusCode();
                HttpEntity entity = response.getEntity();
                String txtResponse = EntityUtils.toString(entity, "UTF-8");
                if (200 == status) {
                    String providerMessageID = parseSmsResponse(message.getPhone(), txtResponse);
                    if (StringUtils.isEmpty(providerMessageID)) {
                        result = new SmsResponse("", txtResponse);
                        log.error("providerMessageID is null: {}", txtResponse);
                    } else {
                        result = new SmsResponse(providerMessageID, "");
                    }
                } else {
                    result = new SmsResponse("", txtResponse);
                    log.error(txtResponse);
                }
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        return result;
    }

    private SmsStatus parseAckResponse(String messageID, String txtResponse) {
        MtsAckResponse objResponse;
        try {
            objResponse = serializer.read(MtsAckResponse.class, txtResponse);
        } catch (Exception e) {
            log.error("Can't get SMS status {} : {}", messageID, txtResponse);
            log.error(e.getMessage());
            return SmsStatus.NONE;
        }
        return objResponse.getStatus();
    }

    @Override
    public SmsStatus getStatus(String messageID) {
        SmsStatus result = SmsStatus.NONE;

        URI uri;
        try {
            String strUrl = smsUrl + "GetMessageStatus";
            uri = new URIBuilder(strUrl)
                .addParameter("messageID", messageID)
                .addParameter("login", userName)
                .addParameter("password", passHash)
                .build();
        } catch (URISyntaxException e) {
            log.error(e.getMessage());
            return result;
        }

        HttpGet httpGet = new HttpGet(uri);

        try (CloseableHttpClient client = HttpClients.custom().build()) {
            try (CloseableHttpResponse response = client.execute(httpGet)) {
                int status = response.getStatusLine().getStatusCode();
                HttpEntity entity = response.getEntity();
                String txtResponse = EntityUtils.toString(entity, "UTF-8");
                if (200 == status) {
                    result = parseAckResponse(messageID, txtResponse);
                } else {
                    log.error(txtResponse);
                }
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        return result;
    }

    private MtsSmsArrayOfMessageInfo parseInSmsResponse(String txtResponse) {
        MtsSmsArrayOfMessageInfo objResponse;
        try {
            objResponse = serializer.read(MtsSmsArrayOfMessageInfo.class, txtResponse);
        } catch (Exception e) {
            log.error("Can't parse XML : {}", txtResponse);
            log.error(e.getMessage());
            return null;
        }
        return objResponse;
    }

    private Instant convertToInstant(String strDateTime) {
        LocalDateTime dateTime = LocalDateTime.parse(strDateTime);
        return dateTime.atZone(ZoneOffset.ofHours(providerTimeZone)).toInstant();
    }

    @Override
    public List<SmsMessage> getMessages(Instant dateFrom, Instant dateTo) {

        List<SmsMessage> result = new ArrayList<>();
        LocalDateTime dateTimeFrom = LocalDateTime.ofInstant(dateFrom, ZoneOffset.ofHours(providerTimeZone));
        LocalDateTime dateTimeTo = LocalDateTime.ofInstant(dateTo, ZoneOffset.ofHours(providerTimeZone));

        URI uri;
        try {
            String strUrl = smsUrl + "GetMessages";
            uri = new URIBuilder(strUrl)
                .addParameter("messageType", "MO")              // входящие
                .addParameter("subscriberMsids", this.incomingPhone)
                .addParameter("DateFrom", dateTimeFrom.format(dateTimeformatter))
                .addParameter("DateTo", dateTimeTo.format(dateTimeformatter))
                .addParameter("login", userName)
                .addParameter("password", passHash)
                .build();
        } catch (URISyntaxException e) {
            log.error(e.getMessage());
            return null;
        }

        HttpGet httpGet = new HttpGet(uri);

        try (CloseableHttpClient client = HttpClients.custom().build()) {
            try (CloseableHttpResponse response = client.execute(httpGet)) {
                int status = response.getStatusLine().getStatusCode();
                HttpEntity entity = response.getEntity();
                String txtResponse = EntityUtils.toString(entity, "UTF-8");
                if (200 == status) {
                    if (txtResponse != null && txtResponse.length() > 197) {
                        MtsSmsArrayOfMessageInfo mi = parseInSmsResponse(txtResponse);
                        if (mi != null) {
                            for (MtsSmsMessageInfo info : mi.list) {
                                result.add(new SmsMessage(-1,
                                    info.senderMsid,
                                    info.messageText,
                                    info.messageID,
                                    convertToInstant(info.creationDate),
                                    convertToInstant(info.deliveryInfo.deliveryInfoExt.deliveryDate)));
                            }
                        }
                    }
                } else {
                    log.error(txtResponse);
                }
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        return result;
    }
}

@Root(name = "DeliveryInfo")
class MtsAckResponseItem {

    @Element(name = "Msid")
    public String phone;

    @Element(name = "DeliveryStatus")
    public String deliveryStatus;

    @Element(name = "DeliveryDate")
    public String deliveryDate;

    @Element(name = "UserDeliveryDate")
    public String userDeliveryDate;

    @Element(name = "PartCount")
    public Integer partCount;
}

@Root(name = "ArrayOfDeliveryInfo")
class MtsAckResponse {

    @Element(name = "DeliveryInfo")
    private MtsAckResponseItem item;

    SmsStatus getStatus() {
        switch (item.deliveryStatus) {
            case "Pending":
            case "Sending":
            case "Sent":
                return SmsStatus.NEW;
            case "Delivered":
                return SmsStatus.SENT;
            case "TimedOut":
            case "NotSent":
            case "NotDelivered":
            case "Error":
                return SmsStatus.ERROR;
            default:
                return SmsStatus.NONE;
        }
    }
}

@Root(name = "DeliveryInfoExt")
class MtsSmsDeliveryInfoExt {

    @Element(name = "TargetMsid", required = false)
    public String targetMsid;

    @Element(name = "DeliveryStatus")
    public String deliveryStatus;

    @Element(name = "DeliveryDate")
    public String deliveryDate;

    @Element(name = "TargetName", required = false)
    public String targetName;

    @Element(name = "UserDeliveryDate")
    public String userDeliveryDate;
}

@Root(name = "DeliveryInfo")
class MtsSmsDeliveryInfo {

    @Element(name = "DeliveryInfoExt")
    public MtsSmsDeliveryInfoExt deliveryInfoExt;
}

@Root(name = "MessageInfo")
class MtsSmsMessageInfo {

    @Element(name = "MessageID")
    public String messageID;

    @Element(name = "CreationDate")
    public String creationDate;

    @Element(name = "SenderMsid")
    public String senderMsid;

    @Element(name = "SenderName")
    public String senderName;

    @Element(name = "MessageType")
    public String messageType;

    @Element(name = "MessageText")
    public String messageText;

    @Element(name = "DeliveryInfo")
    public MtsSmsDeliveryInfo deliveryInfo;
}

@Root(name = "ArrayOfMessageInfo")
class MtsSmsArrayOfMessageInfo {

    @ElementList(inline = true)
    public List<MtsSmsMessageInfo> list;
}
