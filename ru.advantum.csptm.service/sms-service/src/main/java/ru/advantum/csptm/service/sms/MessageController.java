package ru.advantum.csptm.service.sms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.advantum.csptm.service.sms.config.SmsServiceConfig;
import ru.advantum.csptm.service.sms.model.MtsViewDataStatus;
import ru.advantum.csptm.service.sms.model.SmsMessage;
import ru.advantum.csptm.spring.rmq.TypeAdder;

import java.time.Instant;
import java.util.Map;

/**
 * Контроллер SMS сервиса
 * @author abc <alekseenkov@advantum.pro>
 */
@RestController()
public class MessageController {

    private static final Logger log = LoggerFactory.getLogger(MessageController.class);

    private final RabbitTemplate rabbitTemplate;
    private final SmsServiceConfig config;

    @Autowired
    public MessageController(final SmsServiceConfig config, final RabbitTemplate rabbitTemplate) {
        this.config = config;
        this.rabbitTemplate = rabbitTemplate;
    }

//    /**
//     * Отправка SMS
//     * <p>
//     * http://localhost:8084/send/message?id=1&phone=79161060500&text=testmsg
//     * http://172.16.21.51:8084/send/message?id=1&phone=79161060500&text=testmsg
//     */
//    @RequestMapping(value = "/send/message", method = RequestMethod.GET)
//    public ViewDataStatus sendMessage(@RequestParam Map<String, String> param) {
//
//        long id = 0L;
//        if (param.containsKey("id")) {
//            String strID = param.get("id");
//            if (!StringUtils.isEmpty(strID)) {
//                try {
//                    id = Long.parseLong(strID);
//                } catch (NumberFormatException noop) {
//                }
//            }
//        }
//        String phone = "";
//        if (param.containsKey("phone")) phone = param.get("phone");
//        String text = "";
//        if (param.containsKey("text")) text = param.get("text");
//
//        if (id <= 0 || phone.isEmpty() || text.isEmpty()) return ViewDataStatus.err400();
//
//        convertAndSend(config.rmqServiceConfig.smsRouteKey, SmsMessage.class.getSimpleName(), new SmsMessage(id, phone, text));
//
//        return ViewDataStatus.ok();
//    }

    /**
     * Получение сообщений МТС
     * http://172.16.21.51:8084/ForwardMessage?msid=79161060500&message=testmsg
     * http://localhost:8084/ForwardMessage?msid=79161060500&message=testmsg
     */
    @RequestMapping(value = "/ForwardMessage", method = RequestMethod.GET)
    public MtsViewDataStatus forwardMessage(@RequestParam Map<String, String> param) {

        try {
            String msid = "";
            String key = "msid";
            if (param.containsKey(key)) msid = param.get(key);
            if (msid.isEmpty()) return MtsViewDataStatus.msidFormatErrorStatus();

            String message = "";
            key = "message";
            if (param.containsKey(key)) message = param.get(key);
            if (message.isEmpty()) return MtsViewDataStatus.messageFormatErrorStatus();

            SmsMessage smsMessage = new SmsMessage(-1L, msid, message, null, null, Instant.now());
            rabbitTemplate.convertAndSend(config.rmqServiceConfig.smsRouteKey,
                    smsMessage,
                    TypeAdder.of(SmsMessage.class.getSimpleName()));

            log.info("Received  SMS {} {}", msid, smsMessage);

        } catch (Throwable e) {
            log.error(e.getMessage());
        }

        return MtsViewDataStatus.okStatus();
    }
}
