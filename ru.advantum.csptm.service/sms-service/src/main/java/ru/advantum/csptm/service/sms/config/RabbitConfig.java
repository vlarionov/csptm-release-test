package ru.advantum.csptm.service.sms.config;

import org.springframework.context.annotation.Configuration;
import ru.advantum.csptm.spring.rmq.RabbitConfigurerSupport;

@Configuration
public class RabbitConfig extends RabbitConfigurerSupport {
    protected RabbitConfig(SmsServiceConfig config) {
        super(config.rmqServiceConfig);
    }
}
