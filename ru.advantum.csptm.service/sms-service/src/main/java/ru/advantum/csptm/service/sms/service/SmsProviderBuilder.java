package ru.advantum.csptm.service.sms.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

public class SmsProviderBuilder {

    private static final Logger log = LoggerFactory.getLogger(SmsProviderBuilder.class);

    private String smsServiceName;
    private String smsServiceUserName;
    private String smsServicePassword;
    private String smsServiceNaming;
    private int providerTimeZone = 3;

    public SmsProviderBuilder setSmsServiceName(String smsServiceName) {
        this.smsServiceName = smsServiceName;
        return this;
    }

    public SmsProviderBuilder setSmsServiceUserName(String smsServiceUserName) {
        this.smsServiceUserName = smsServiceUserName;
        return this;
    }

    public SmsProviderBuilder setSmsServicePassword(String smsServicePassword) {
        this.smsServicePassword = smsServicePassword;
        return this;
    }

    public SmsProviderBuilder setSmsServiceNaming(String smsServiceNaming) {
        this.smsServiceNaming = smsServiceNaming;
        return this;
    }

    public SmsProviderBuilder setProviderTimeZone(int providerTimeZone) {
        this.providerTimeZone = providerTimeZone;
        return this;
    }

    private String password2MD5Hex(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] digest = md.digest(password.getBytes());
        StringBuilder sb = new StringBuilder();
        for (byte b : digest) sb.append(String.format("%02x", b & 0xff));
        return sb.toString();
    }

    public SmsProvider build() {

        Objects.requireNonNull(smsServiceName, "smsServiceName can't be null");

        SmsProvider result = null;
        switch (smsServiceName) {
            case "MTS":
                try {

                    Objects.requireNonNull(smsServiceUserName, "smsServiceUserName can't be null");
                    Objects.requireNonNull(smsServicePassword, "smsServicePassword can't be null");
                    Objects.requireNonNull(smsServiceNaming, "smsServiceNaming can't be null");

                    result = new SmsProviderMts(
                            smsServiceUserName,
                            password2MD5Hex(smsServicePassword),
                            smsServiceNaming,
                            smsServiceUserName,
                            providerTimeZone);

                } catch (NoSuchAlgorithmException e) {
                    log.error(e.getMessage());
                }
                break;
        }
        return result;
    }
}
