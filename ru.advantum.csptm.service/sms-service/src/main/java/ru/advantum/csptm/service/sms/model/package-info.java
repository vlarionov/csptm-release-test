@XmlSchema(namespace = "http://mcommunicator.ru/M2M/",
        xmlns = {
                @XmlNs(namespaceURI = "http://mcommunicator.ru/M2M/", prefix = "")
        },
        elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)

package ru.advantum.csptm.service.sms.model;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;