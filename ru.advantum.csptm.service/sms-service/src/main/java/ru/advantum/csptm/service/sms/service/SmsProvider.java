package ru.advantum.csptm.service.sms.service;

import ru.advantum.csptm.service.sms.model.SmsMessage;
import ru.advantum.csptm.service.sms.model.SmsStatus;

import java.time.Instant;
import java.util.List;

public interface SmsProvider {

    SmsResponse sendMessage(SmsMessage message);

    SmsStatus getStatus(String providerMessageID);

    List<SmsMessage> getMessages(Instant dateFrom, Instant dateTo);
}
