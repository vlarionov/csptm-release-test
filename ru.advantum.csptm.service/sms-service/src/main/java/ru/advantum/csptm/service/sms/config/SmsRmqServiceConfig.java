package ru.advantum.csptm.service.sms.config;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;

@Root(name = "rmq-service")
public class SmsRmqServiceConfig extends JepRmqServiceConfig {

    @Element(name = "rmq-service-sms-route-key", required = false)
    public String smsRouteKey = "sms";

    @Element(name = "rmq-service-sms-ack-route-key", required = false)
    public String ackRouteKey = "ack";
}
