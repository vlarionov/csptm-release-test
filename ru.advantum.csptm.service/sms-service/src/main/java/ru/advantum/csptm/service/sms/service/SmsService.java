package ru.advantum.csptm.service.sms.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.advantum.csptm.service.sms.config.SmsServiceConfig;
import ru.advantum.csptm.service.sms.model.SmsAckMessage;
import ru.advantum.csptm.service.sms.model.SmsMessage;
import ru.advantum.csptm.service.sms.model.SmsStatus;
import ru.advantum.csptm.spring.rmq.TypeAdder;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.*;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * SMS сервис
 * @author abc <alekseenkov@advantum.pro>
 */
@Service
public class SmsService {

    private static final Logger log = LoggerFactory.getLogger(SmsService.class);
    private final SmsServiceConfig config;
    private final SmsProvider provider;
    private final RabbitTemplate rabbitTemplate;
    private final RabbitListenerEndpointRegistry listeners;

    private final ConcurrentLinkedQueue<SmsMessage> deliveryQueue = new ConcurrentLinkedQueue<>();
    private final AtomicInteger queueSize = new AtomicInteger(0);

    private Map<String, SmsMessage> messageCache = new HashMap<>();
    private final Object messageCacheLock = new Object();
    private volatile Instant lastRequest = Instant.now();

    @Autowired
    public SmsService(SmsServiceConfig config, SmsProvider provider, RabbitTemplate rabbitTemplate,
                      RabbitListenerEndpointRegistry listeners) {
        this.config = config;
        this.provider = provider;
        this.rabbitTemplate = rabbitTemplate;
        this.listeners = listeners;
    }

    @PostConstruct
    public void init() {

        try (FileInputStream fileIn = new FileInputStream(config.deliveryFileName);
             ObjectInputStream in = new ObjectInputStream(fileIn)) {
            Object o = in.readObject();
            if (o != null && o instanceof ConcurrentLinkedQueue) {
                @SuppressWarnings("unchecked") ConcurrentLinkedQueue<SmsMessage> savedDeliveryQueue =
                    (ConcurrentLinkedQueue<SmsMessage>) o;

                if (!savedDeliveryQueue.isEmpty()) deliveryQueue.addAll(savedDeliveryQueue);
            }
        } catch (FileNotFoundException e) {
            log.info("File " + config.deliveryFileName + " not found");
        } catch (IOException | ClassNotFoundException e) {
            log.error(e.getMessage());
        }

        queueSize.set(deliveryQueue.size());

        loadArchiveMessages();
    }

    @PreDestroy
    public void saveDeliveryQueue() {
        try (FileOutputStream fileOut = new FileOutputStream(config.deliveryFileName);
             ObjectOutputStream out = new ObjectOutputStream(fileOut)) {
            out.writeObject(deliveryQueue);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        log.info("sms-service is shutdown");
    }

    @Scheduled(fixedRateString = "#{applicationConfig.deliveryRequestPeriod}")
    public void checkDeliveryStatus() {

        final int maxQueueSize = 100; // Максимальное число SMS без подтверждения о доставке

        try {
            if (!deliveryQueue.isEmpty()) {
                for (SmsMessage message : deliveryQueue) {
                    SmsStatus status = provider.getStatus(message.getProviderID());
                    if (status == SmsStatus.ERROR || status == SmsStatus.SENT) {

                        rabbitTemplate.convertAndSend(config.rmqServiceConfig.ackRouteKey,
                            new SmsAckMessage(message.getId(), status),
                            TypeAdder.of(SmsAckMessage.class.getSimpleName()));

                        deliveryQueue.remove(message);
                        queueSize.decrementAndGet();
                        log.info("Ack for SMS {} {} {}", queueSize.get(), message.toString(), status.name());
                    }
                }
            }

            if (queueSize.get() > maxQueueSize) {
                if (listeners.isRunning()) {
                    listeners.stop();
                    log.info("SMS sending is discontinued. SMS delivery limit exceeded {}", queueSize.get());
                }
            } else {
                if (!listeners.isRunning()) {
                    listeners.start();
                    log.info("SMS sending resumed");
                }
            }
        } catch (Throwable e) {
            log.error(e.getMessage());
        }
    }

    @RabbitListener(queues = "#{applicationConfig.rmqServiceConfig.consumer.queue}")
    public void sendMessage(SmsMessage message) {
        try {
            SmsResponse r = provider.sendMessage(message);
            if (!StringUtils.isEmpty(r.getProviderMessageID())) {
                log.info("SMS sent {} {}", queueSize.get(), message.toString());

                // Если, не задан ID, не пытаемся получить уведомление о доставке
                if (message.getId() > 0) {
                    deliveryQueue.add(new SmsMessage(message, r.getProviderMessageID()));
                    queueSize.incrementAndGet();
                }
            } else {
                log.info("SMS error {}", r.getErrorMessage());

                rabbitTemplate.convertAndSend(config.rmqServiceConfig.ackRouteKey,
                    new SmsAckMessage(message.getId(), SmsStatus.ERROR),
                    TypeAdder.of(SmsAckMessage.class.getSimpleName()));
            }
        } catch (Throwable e) {
            log.error(e.getMessage());
        }
    }

    private void loadMessages(Instant beginDate, Instant endDate) {

        log.info("Get SMS from {} to {} ", beginDate, endDate);

        Instant windowDate = Instant.now().minusMillis(config.receiveMassagesRequestWindow);

        List<SmsMessage> messages = provider.getMessages(beginDate, endDate);
        for (SmsMessage message : messages) {
            if (!messageCache.containsKey(message.getProviderID())) {
                try {

                    rabbitTemplate.convertAndSend(config.rmqServiceConfig.smsRouteKey,
                        message,
                        TypeAdder.of(SmsMessage.class.getSimpleName()));

                    log.info("Got SMS {} ", message);

                    if (message.getCreationDate().isAfter(windowDate)) {
                        messageCache.put(message.getProviderID(), message);
                    }

                } catch (Throwable e) {
                    log.error(e.getMessage());
                }
            }
        }
    }

    private void cleanCache() {

        Instant windowDate = Instant.now().minusMillis(config.receiveMassagesRequestWindow);
        messageCache = messageCache.entrySet().stream()
            .filter(map -> map.getValue().getCreationDate().isAfter(windowDate))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        log.info("SMS cache size {} ", messageCache.size());
    }

    private void loadArchiveMessages() {

        synchronized (messageCacheLock) {

            if (StringUtils.isEmpty(config.initReceiveFrom)) return;

            Instant beginDate;
            try {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
                LocalDateTime dateTime = LocalDateTime.parse(config.initReceiveFrom, formatter);
                beginDate = dateTime.toInstant(ZoneOffset.ofHours(config.providerTimeZone));
            } catch (DateTimeParseException noop) {
                return;
            }
            Instant endDate = lastRequest;

            if (Duration.between(beginDate, endDate).toMinutes() < 1) return;

            log.info("Load archive SMS from {} to {} ", beginDate, endDate);

            while (beginDate.isBefore(endDate)) {

                Instant runEndDate = beginDate.minusMillis(config.receiveMassagesRequestWindow);
                if (runEndDate.isAfter(endDate)) runEndDate = endDate;

                if (Duration.between(beginDate, runEndDate).toMinutes() < 1) loadMessages(beginDate, runEndDate);

                beginDate = beginDate.plusSeconds(config.receiveMassagesRequestWindow / 1000);
            }

            cleanCache();
        }
    }

    // abc: запасной вариант - в настоящее время используем MessageController.forwardMessage
    // @Scheduled(fixedRateString = "#{applicationConfig.receiveMassagesRequestPeriod}")
    public void receiveMassages() {

        synchronized (messageCacheLock) {

            Instant endDate = Instant.now();
            Instant beginDate = endDate.minusMillis(config.receiveMassagesRequestWindow);

            if (lastRequest.isBefore(beginDate)) beginDate = lastRequest;

            if (Duration.between(beginDate, endDate).toMinutes() < 1) {
                log.info("loadMessages: beginDate and LocalDateTime diff < 1 min");
                return;
            }

            loadMessages(beginDate, endDate);
            lastRequest = endDate;

            cleanCache();
        }
    }
}
