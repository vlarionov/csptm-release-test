package ru.advantum.csptm.service.sms;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.advantum.csptm.service.sms.config.SmsServiceConfig;
import ru.advantum.csptm.service.sms.service.SmsProvider;
import ru.advantum.csptm.service.sms.service.SmsProviderBuilder;

@SpringBootApplication
@EnableScheduling
public class SmsServiceApplication {

    private final String CONFIG_FILE_NAME = "sms-service.xml";

    @Bean
    public SmsServiceConfig applicationConfig() throws Exception {
        return ru.advantum.config.Configuration.unpackConfig(SmsServiceConfig.class, CONFIG_FILE_NAME);
    }

    @Bean
    public SmsProvider smsProvider(SmsServiceConfig config) {
        return new SmsProviderBuilder()
            .setSmsServiceName(config.smsServiceName)
            .setSmsServiceUserName(config.smsServiceUserName)
            .setSmsServicePassword(config.smsServicePassword)
            .setSmsServiceNaming(config.smsServiceNaming)
            .setProviderTimeZone(config.providerTimeZone)
            .build();
    }

    public static void main(String[] args) {
        new SpringApplicationBuilder(SmsServiceApplication.class)
            .bannerMode(Banner.Mode.OFF)
            .run(args);
    }
}
