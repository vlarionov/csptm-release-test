package ru.advantum.csptm.service.sms.config;

import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;

@Controller
public class ServletConfig {

    @Bean
    public EmbeddedServletContainerCustomizer containerCustomizer(SmsServiceConfig applicationConfig) {

        return (container -> {
            container.setPort(applicationConfig.port);
        });
    }
}