package ru.advantum.csptm.service.sms.model;

/**
 * Статусы ответов
 */
public class ViewDataStatus {
    public Integer code;
    public String message;

    public static ViewDataStatus ok() {
        ViewDataStatus result = new ViewDataStatus();
        result.code = 200;
        result.message = "OK";
        return result;
    }

    public static ViewDataStatus created() {
        ViewDataStatus result = new ViewDataStatus();
        result.code = 201;
        result.message = "Created";
        return result;
    }

    public static ViewDataStatus accepted() {
        ViewDataStatus result = new ViewDataStatus();
        result.code = 202;
        result.message = "Accepted";
        return result;
    }

    public static ViewDataStatus err400(String message) {
        ViewDataStatus result = new ViewDataStatus();
        result.code = 400;
        if (message == null)
            result.message = "Bad Request";
        else
            result.message = message;

        return result;
    }

    public static ViewDataStatus err400() {
        return err400(null);
    }

    public static ViewDataStatus err401(String message) {
        ViewDataStatus result = new ViewDataStatus();
        result.code = 401;
        if (message == null)
            result.message = "Unauthorized";
        else
            result.message = message;

        return result;
    }

    public static ViewDataStatus err401() {
        return err401(null);
    }

    public static ViewDataStatus err403(String message) {
        ViewDataStatus result = new ViewDataStatus();
        result.code = 403;
        if (message == null)
            result.message = "Forbidden";
        else
            result.message = message;

        return result;
    }

    public static ViewDataStatus err403() {
        return err403(null);
    }

    public static ViewDataStatus err404(String message) {
        ViewDataStatus result = new ViewDataStatus();
        result.code = 404;
        if (message == null)
            result.message = "Not Found";
        else
            result.message = message;

        return result;
    }

    public static ViewDataStatus err404() {
        return err404(null);
    }

    public static ViewDataStatus err413(String message) {
        ViewDataStatus result = new ViewDataStatus();
        result.code = 413;
        if (message == null)
            result.message = "Bad parameters";
        else
            result.message = message;

        return result;
    }

    public static ViewDataStatus err413() {
        return err413(null);
    }

    public static ViewDataStatus err500(String message) {
        ViewDataStatus result = new ViewDataStatus();
        result.code = 500;
        if (message == null)
            result.message = "Internal Server Error";
        else
            result.message = message;

        return result;
    }

    public static ViewDataStatus err500() {
        return err500(null);
    }

    public static ViewDataStatus err501(String message) {
        ViewDataStatus result = new ViewDataStatus();
        result.code = 501;
        if (message == null)
            result.message = "Not Implemented";
        else
            result.message = message;

        return result;
    }

    public static ViewDataStatus err501() {
        return err501(null);
    }
}
