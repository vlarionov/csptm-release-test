package ru.advantum.csptm.service.sms.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "sms-service")
public class SmsServiceConfig {

    @Attribute(name = "port", required = false)
    public int port = 8084;

    @Element(name = "rmq-service")
    public final SmsRmqServiceConfig rmqServiceConfig = new SmsRmqServiceConfig();

    /** Наименование сервиса SMS */
    @Attribute()
    public String smsServiceName;

    @Attribute(required = false)
    public String smsServiceNaming = "MGT";

    @Attribute()
    public String smsServiceUserName;

    @Attribute()
    public String smsServicePassword;

    @Attribute(required = false)
    public int deliveryRequestPeriod = 2 * 60 * 1000;

    @Element(name = "deliveryFileName", required = false)
    public String deliveryFileName = "/tmp/sms-service.data";

    @Attribute(required = false)
    public int receiveMassagesRequestPeriod = 5 * 60 * 1000;

    /** Если задано, загрузить собщения с этой даты ("dd.MM.yyyy HH:mm:ss") при старте */
    @Attribute(required = false)
    public String initReceiveFrom = "";

    @Attribute(required = false)
    public int receiveMassagesRequestWindow = 15 * 60 * 1000;

    @Attribute(required = false)
    public int providerTimeZone = 3;
}
