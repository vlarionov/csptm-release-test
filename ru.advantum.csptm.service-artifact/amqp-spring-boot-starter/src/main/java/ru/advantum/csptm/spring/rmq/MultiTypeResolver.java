package ru.advantum.csptm.spring.rmq;

import org.springframework.amqp.core.Message;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kaganov on 02/06/2017.
 */
public class MultiTypeResolver implements MessageTypeResolver {

    private final Map<String, Type> amqp2Java = new HashMap<>();

    public static Builder newBuilder() {
        return new Builder();
    }

    public MultiTypeResolver(List<StringTypePair> pairs) {
        for (StringTypePair pair : pairs) {
            if (amqp2Java.put(pair.amqpType, pair.javaType) != null) {
                throw new IllegalArgumentException(String.format("Duplicated amqp type %s", pair.amqpType));
            }
        }
    }

    @Override
    public Type resolveJavaType(Message message) {
        return amqp2Java.get(message.getMessageProperties().getType());
    }

    private static class StringTypePair {

        private final String amqpType;
        private final Type javaType;

        public StringTypePair(String amqpType, Type javaType) {
            this.amqpType = amqpType;
            this.javaType = javaType;
        }
    }

    public static class Builder {

        private final List<StringTypePair> pairs = new ArrayList<>();

        public Builder addPair(String amqpType, Type javaType) {
            pairs.add(new StringTypePair(amqpType, javaType));
            return this;
        }

        public MultiTypeResolver build() {
            return new MultiTypeResolver(pairs);
        }
    }
}
