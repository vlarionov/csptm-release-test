package ru.advantum.csptm.spring.rmq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.support.converter.MessageConverter;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;
import ru.advantum.rabbitmq.config.RmqConsumerConfig;

public class JepListenerContainerFactory extends SimpleRabbitListenerContainerFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(JepListenerContainerFactory.class);

    private final RmqConsumerConfig consumerConfig;

    private final ConsumerProperties consumerProperties;

    public JepListenerContainerFactory(
            ConnectionFactory connectionFactory,
            MessageConverter messageConverter,
            JepRmqServiceConfig connectConfig,
            ConsumerProperties consumerProperties
    ) {
        this.consumerConfig = connectConfig.consumer;
        this.consumerProperties = consumerProperties;

        setMessageConverter(messageConverter);
        setConnectionFactory(connectionFactory);
        setConcurrentConsumers(consumerProperties.getConcurrency());
        setMaxConcurrentConsumers(consumerProperties.getMaxConcurrency());
        setAcknowledgeMode(consumerProperties.getAcknowledgeMode());
        if (connectConfig.consumer != null) {
            setPrefetchCount(connectConfig.consumer.prefetchCount);
        }
    }

    @Override
    protected void initializeContainer(SimpleMessageListenerContainer instance) {
        super.initializeContainer(instance);

        if (consumerConfig != null && instance.getQueueNames().length == 0) {
            LOGGER.info("Default consumer queue configured: {}", consumerConfig.queue);
            instance.addQueueNames(consumerConfig.queue);
        }

        instance.setShutdownTimeout(consumerProperties.getShutdownTimeout());
        instance.setExclusive(consumerProperties.isExclusive());
    }
}
