package ru.advantum.csptm.spring.rmq;

import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;

public class ConsumerProperties {

    private final int concurrency;
    private final int maxConcurrency;
    private final AcknowledgeMode acknowledgeMode;
    private final long shutdownTimeout;
    private final boolean exclusive;

    public static Builder newBuilder() {
        return new Builder();
    }

    public ConsumerProperties(int concurrency,
                              int maxConcurrency,
                              AcknowledgeMode acknowledgeMode,
                              long shutdownTimeout,
                              boolean exclusive) {
        this.concurrency = concurrency;
        this.maxConcurrency = maxConcurrency;
        this.acknowledgeMode = acknowledgeMode;
        this.shutdownTimeout = shutdownTimeout;
        this.exclusive = exclusive;
    }

    public int getConcurrency() {
        return concurrency;
    }

    public int getMaxConcurrency() {
        return maxConcurrency;
    }

    public AcknowledgeMode getAcknowledgeMode() {
        return acknowledgeMode;
    }

    public long getShutdownTimeout() {
        return shutdownTimeout;
    }

    public boolean isExclusive() {
        return exclusive;
    }

    public static class Builder {

        private int concurrency = 1;
        private int maxConcurrency = 1;
        private AcknowledgeMode acknowledgeMode = AcknowledgeMode.AUTO;
        private long shutdownTimeout = SimpleMessageListenerContainer.DEFAULT_SHUTDOWN_TIMEOUT;
        private boolean exclusive = true;

        public Builder setConcurrency(int concurrency) {
            this.concurrency = concurrency;
            return this;
        }

        public Builder setMaxConcurrency(int maxConcurrency) {
            this.maxConcurrency = maxConcurrency;
            return this;
        }

        public Builder setAcknowledgeMode(AcknowledgeMode acknowledgeMode) {
            this.acknowledgeMode = acknowledgeMode;
            return this;
        }

        public Builder setShutdownTimeout(long shutdownTimeout) {
            this.shutdownTimeout = shutdownTimeout;
            return this;
        }

        public Builder setExclusive(boolean exclusive) {
            this.exclusive = exclusive;
            return this;
        }

        public ConsumerProperties build() {
            return new ConsumerProperties(concurrency, maxConcurrency, acknowledgeMode, shutdownTimeout, exclusive);
        }
    }
}
