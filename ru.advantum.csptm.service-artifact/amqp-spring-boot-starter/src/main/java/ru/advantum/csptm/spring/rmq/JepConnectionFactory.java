package ru.advantum.csptm.spring.rmq;

import com.rabbitmq.client.ConnectionFactory;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;
import ru.advantum.rabbitmq.config.RmqConnectionConfig;

public class JepConnectionFactory extends ConnectionFactory {

    public JepConnectionFactory(JepRmqServiceConfig config) {
        RmqConnectionConfig rmqConnectionConfig = config.connection;

        setHost(rmqConnectionConfig.host);
        setPort(rmqConnectionConfig.port);
        setVirtualHost(rmqConnectionConfig.virtualHost);

        setUsername(rmqConnectionConfig.username);
        setPassword(rmqConnectionConfig.password);

        setRequestedHeartbeat(rmqConnectionConfig.heartbeatInterval);
        // что-то не очень понятно, при recovery exclusive consumer'ов с этой опцией не работает
        // setAutomaticRecoveryEnabled(rmqConnectionConfig.automaticRecovery);

        // зачем так? это ж плохо
        // setTopologyRecoveryEnabled(false);
    }
}
