package ru.advantum.csptm.spring.rmq;

import com.fatboyindustrial.gsonjavatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.listener.RetryListenerSupport;
import org.springframework.retry.policy.AlwaysRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import ru.advantum.csptm.jep.core.rmq.JepRmqServiceConfig;
import ru.advantum.rabbitmq.config.RmqDestinationConfig;

@Configuration
@ConditionalOnBean(JepRmqServiceConfig.class)
public class RabbitConfigurerSupport {

    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitConfigurerSupport.class);

    private final JepRmqServiceConfig connectConfig;

    @Autowired
    public RabbitConfigurerSupport(JepRmqServiceConfig connectConfig) {
        this.connectConfig = connectConfig;
    }

    @Bean
    public JepConnectionFactory jepConnectionFactory() {
        return new JepConnectionFactory(connectConfig);
    }

    @Bean
    public Gson gson() {
        return Converters.registerAll(new GsonBuilder()).create();
    }

    @Bean
    @ConditionalOnMissingBean(MessageTypeResolver.class)
    public MessageTypeResolver defaultMessageTypeResolver() {
        return new MethodArgumentTypeResolver();
    }

    @Bean
    @ConditionalOnMissingBean(ConsumerProperties.class)
    public ConsumerProperties defaultConsumerProperties() {
        return ConsumerProperties.newBuilder().build();
    }

    @Bean
    public MessageConverter messageConverter(MessageTypeResolver typeResolver) {
        return new GsonMessageConverter(gson(), typeResolver);
    }

    @Bean
    public ConnectionFactory connectionFactory() throws Exception {
        CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory(jepConnectionFactory());
        cachingConnectionFactory.setCacheMode(CachingConnectionFactory.CacheMode.CHANNEL);
        return cachingConnectionFactory;
    }

    @Bean
    public RabbitTemplate rabbitTemplate(MessageConverter messageConverter) throws Exception {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory());
        rabbitTemplate.setMessageConverter(messageConverter);

        RmqDestinationConfig destinationConfig = connectConfig.destination;
        if (destinationConfig != null) {
            if (destinationConfig.exchange != null) {
                LOGGER.info("Default destination exchange configured: {}", destinationConfig.exchange);
                rabbitTemplate.setExchange(destinationConfig.exchange);
            }
            if (destinationConfig.routingKey != null) {
                LOGGER.info("Default destination routingKey configured: {}", destinationConfig.routingKey);
                rabbitTemplate.setRoutingKey(destinationConfig.routingKey);
            }
        }

        RetryTemplate retryTemplate = new RetryTemplate();
        retryTemplate.setBackOffPolicy(new FixedBackOffPolicy());
        retryTemplate.setRetryPolicy(new AlwaysRetryPolicy());

        retryTemplate.registerListener(new RetryListenerSupport() {
            private final Logger LOGGER = LoggerFactory.getLogger(RabbitConfigurerSupport.class);

            @Override
            public <T, E extends Throwable> void onError(RetryContext context,
                                                         RetryCallback<T, E> callback,
                                                         Throwable throwable) {
                LOGGER.error("rabbitRetry", throwable);
            }
        });

        rabbitTemplate.setRetryTemplate(retryTemplate);

        return rabbitTemplate;
    }

    @Bean
    public JepListenerContainerFactory rabbitListenerContainerFactory(MessageConverter messageConverter,
                                                                      ConsumerProperties consumerProperties) throws Exception {
        return new JepListenerContainerFactory(connectionFactory(),
                                               messageConverter,
                                               connectConfig,
                                               consumerProperties);
    }
}
