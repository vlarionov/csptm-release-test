package ru.advantum.csptm.spring.rmq;

import org.springframework.amqp.core.Message;

import java.lang.reflect.Type;

/**
 * Created by kaganov on 02/06/2017.
 */
public class MethodArgumentTypeResolver implements MessageTypeResolver {

    @Override
    public Type resolveJavaType(Message message) {
        return message.getMessageProperties().getInferredArgumentType();
    }
}
