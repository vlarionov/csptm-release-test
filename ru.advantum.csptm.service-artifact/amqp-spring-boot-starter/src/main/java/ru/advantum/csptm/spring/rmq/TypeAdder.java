package ru.advantum.csptm.spring.rmq;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;

/**
 * Created by kaganov on 18/07/2017.
 */
public class TypeAdder implements MessagePostProcessor {

    private final String type;

    private TypeAdder(String type) {
        this.type = type;
    }

    public static TypeAdder of(String type) {
        return new TypeAdder(type);
    }

    @Override
    public Message postProcessMessage(Message message) throws AmqpException {
        message.getMessageProperties().setType(type);
        return message;
    }
}
