package ru.advantum.csptm.jep.service.glinker.model.event;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Created by kaganov on 18/02/2017.
 */
// TODO: хотят более точно понимать, что плохого случилось с рейсом
@Getter
@RequiredArgsConstructor
public class TtActionCompleteEvent {

    private final long ttActionId;
    private final long roundId;
    private final LinkEvent eventBegin;
    private final LinkEvent eventEnd;
    private final StopItemPass stopItemBegin;
    private final StopItemPass stopItemEnd;
    private final GraphSectionPass graphSectionBegin;
    private final GraphSectionPass graphSectionEnd;

    @Override
    public String toString() {
        return "TtActionCompleteEvent{" +
                "ttAction=" + ttActionId +
                ", type=" + (eventEnd == null ? LinkEvent.LinkType.ROUND_END_ERROR : eventEnd.getType()) +
                ", eventBegin=" + (eventBegin == null ? "" : eventBegin.getSimpleUnitPacket().getEventTime()) +
                ", eventEnd=" + (eventEnd == null ? "" : eventEnd.getSimpleUnitPacket().getEventTime()) +
                ", spBegin=" + (stopItemBegin == null ? "" : stopItemBegin.getPassTime()) +
                ", spEnd=" + (stopItemEnd == null ? "" : stopItemEnd.getPassTime()) +
                ", gsBegin=" + (graphSectionBegin == null ? "" : graphSectionBegin.getStartTime()) +
                ", gsEnd=" + (graphSectionEnd == null ? "" : graphSectionEnd.getEndTime()) +
                '}';
    }
}
