package ru.advantum.csptm.jep.service.glinker.model.event;

import lombok.Data;

import java.time.Instant;

/**
 * Created by Leonid on 15.02.2017.
 */
@Data
public class StopItemPass {

    private final long trId;
    private final long ttActionId;
    private final long roundId;
    // TODO: возможно надо использовать все-таки stopItem2RoundId
    private final long stopItemId;
    private final int stopOrderNum;
    private final Instant passTime;
}
