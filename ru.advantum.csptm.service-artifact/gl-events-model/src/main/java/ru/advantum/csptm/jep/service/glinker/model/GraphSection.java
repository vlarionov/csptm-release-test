package ru.advantum.csptm.jep.service.glinker.model;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.linearref.LengthIndexedLine;
import lombok.Getter;
import ru.advantum.csptm.artifact.geotools.GeoTools;

/**
 * Created by kaganov on 23/01/2017.
 */
@Getter
public class GraphSection implements LinkingItem {

    private final long graphSectionId;
    private final long roundId;
    private final int orderNum;
    private final double length;
    private final double roundOffset;
    transient private final Geometry geometry;
    transient private final LengthIndexedLine indexedLine;

    public GraphSection(long graphSectionId,
                        long roundId,
                        int orderNum,
                        double length,
                        double roundOffset,
                        String wktGeometry) {
        this.graphSectionId = graphSectionId;
        this.roundId = roundId;
        this.orderNum = orderNum;
        this.length = length;
        this.roundOffset = roundOffset;
        this.geometry = GeoTools.INSTANCE.convertFromWkt(wktGeometry);
        this.indexedLine = new LengthIndexedLine(geometry);
    }

    @Override
    public String toString() {
        return "GraphSection{" +
                "graphSectionId=" + graphSectionId +
                ", orderNum=" + orderNum +
                '}';
    }

    @Override
    public double getOffset() {
        return roundOffset;
    }
}
