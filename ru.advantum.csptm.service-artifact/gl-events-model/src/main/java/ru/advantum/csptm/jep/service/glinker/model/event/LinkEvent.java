package ru.advantum.csptm.jep.service.glinker.model.event;

import lombok.Data;

/**
 * Created by kaganov on 30/01/2017.
 */
@Data
public class LinkEvent {

    public enum LinkType {
        /**
         * Привязка к рейсу не началась.
         */
        WAITING_FOR_LINKING_START(false, false),
        /**
         * Точка привязана.
         */
        LINKED(true, false),
        /**
         * Не найдена подходящая секции для привязки.
         */
        TOO_FAR_FROM_GRAPH_SECTIONS(false, false),
        /**
         * Точка слишком далеко от ближайшей секции, не привязали.
         */
        TOO_FAR_FROM_ROUND(false, false),
        /**
         * Привязка к рейсу начата.
         */
        ROUND_BEGIN(true, false),
        /**
         * Привязка к рейсу закончена.
         */
        ROUND_END(true, true),
        /**
         * Закончили безуспешные попытки привязаться к рейсу
         */
        ROUND_END_ERROR(false, true),
        /**
         * Неправильный порядок точек по времени.
         */
        INVALID_ORDER_TIME(false, false),
        /**
         * Точка находится вдоль рейса раньше, чем предыдущая, не привязали.
         */
        INVALID_ORDER_OFFSET(false, false),
        /**
         * Слишком большая скорость между предыдущей и этой точками, считаем выбросом, не привязали.
         */
        TOO_HIGH_SPEED(false, false),
        /**
         * Не валидная координата или координата без телематики.
         */
        INVALID_COORDINATE(false, false),
        /**
         * Отмена рейса.
         */
        TT_ACTION_END_CANCEL(false, true);

        private final boolean goodLink;
        private final boolean finished;

        LinkType(boolean goodLink, boolean finished) {
            this.goodLink = goodLink;
            this.finished = finished;
        }

        public boolean isGoodLink() {
            return goodLink;
        }

        public boolean isFinished() {
            return finished;
        }
    }

    private final long ttActionId;
    private final long roundId;
    private final LinkType type;
    private final SectionLink sectionLink;
    private final SimpleUnitPacket simpleUnitPacket;

    @Override
    public String toString() {
        return "LinkEvent{" + "ttActionId=" + ttActionId +
                ", roundId=" + roundId +
                ", type=" + type +
                ", sectionLink=" + sectionLink +
                ", unitPacketTime=" + simpleUnitPacket.getEventTime() +
                '}';
    }
}
