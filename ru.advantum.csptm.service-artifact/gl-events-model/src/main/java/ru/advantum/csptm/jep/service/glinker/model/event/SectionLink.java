package ru.advantum.csptm.jep.service.glinker.model.event;

import lombok.Data;
import ru.advantum.csptm.jep.service.glinker.model.GraphSection;

/**
 * Created by kaganov on 23/01/2017.
 */
@Data
public class SectionLink {

    private final long graphSectionId;
    private final int graphSectionOrderNum;
    private final double sectionOffset;
    private final double roundOffset;

    public SectionLink(GraphSection graphSection,
                       double sectionOffset,
                       double roundOffset) {
        this.graphSectionId = graphSection.getGraphSectionId();
        this.graphSectionOrderNum = graphSection.getOrderNum();
        this.sectionOffset = sectionOffset;
        this.roundOffset = roundOffset;
    }

    @Override
    public String toString() {
        return String.format("Link{sectionOrderNum %d, sectionOffset %f, roundOffset %f}",
                             graphSectionOrderNum, sectionOffset, roundOffset);
    }
}
