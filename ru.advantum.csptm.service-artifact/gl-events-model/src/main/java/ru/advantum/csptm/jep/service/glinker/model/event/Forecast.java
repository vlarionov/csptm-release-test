package ru.advantum.csptm.jep.service.glinker.model.event;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.Instant;

@Getter
@RequiredArgsConstructor
public class Forecast {

    private final Instant creationTime;
    private final long ttActionId;
    private final long stopItemId;
    private final int stopOrderNum;
    private final long fromTtActionId;
    private final int fromStopOrderNum;
    private final Instant passTimeForecast;

    @Override
    public String toString() {
        return "Forecast{" +
                "creationTime=" + creationTime +
                ", ttActionId=" + ttActionId +
                ", stopItemId=" + stopItemId +
                ", stopOrderNum=" + stopOrderNum +
                ", fromTtActionId=" + fromTtActionId +
                ", fromStopOrderNum=" + fromStopOrderNum +
                ", passTimeForecast=" + passTimeForecast +
                '}';
    }
}
