package ru.advantum.csptm.jep.service.glinker.model.event;

/**
 * Created by kaganov on 22/02/2017.
 */
public enum EventType {
    PACKET_LINK,
    STOP_ITEM_PASS,
    GRAPH_SECTION_PASS,
    TT_ACTION_COMPLETE,
    TT_ACTION_UPDATE
}
