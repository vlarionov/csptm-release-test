package ru.advantum.csptm.jep.service.glinker.model.event;

import lombok.Data;
import ru.advantum.csptm.jep.service.glinker.model.LinkingItem;

import java.time.Instant;

@Data
public class LinkingItemPass<T extends LinkingItem> {

    private final T item;
    private final Instant passBeginTime;
    private final Instant passEndTime;
}
