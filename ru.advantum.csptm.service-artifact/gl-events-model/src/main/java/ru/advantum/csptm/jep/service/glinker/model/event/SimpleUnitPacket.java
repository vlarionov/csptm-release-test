package ru.advantum.csptm.jep.service.glinker.model.event;

import lombok.Data;
import ru.advantum.csptm.jep.proto.UnitPacket;

import java.time.Instant;

@Data
public class SimpleUnitPacket {

    private final long packetId;
    private final long trId;
    private final long unitId;
    private final Instant eventTime;
    private final boolean locationValid;
    private final float lon;
    private final float lat;
    private final int speed;

    public static SimpleUnitPacket from(UnitPacket unitPacket) {
        final float lon;
        final float lat;
        final int speed;

        if (unitPacket.getTelematic() != null) {
            lon = unitPacket.getTelematic().getLon();
            lat = unitPacket.getTelematic().getLat();
            speed = unitPacket.getTelematic().getSpeed();
        } else {
            lon = 0;
            lat = 0;
            speed = 0;
        }

        return new SimpleUnitPacket(unitPacket.getPacketId(),
                                    unitPacket.getTrId(),
                                    unitPacket.getUnitId(),
                                    unitPacket.getEventTime(),
                                    unitPacket.getTelematic() != null && unitPacket.getTelematic().isLocationValid(),
                                    lon,
                                    lat,
                                    speed);
    }
}
