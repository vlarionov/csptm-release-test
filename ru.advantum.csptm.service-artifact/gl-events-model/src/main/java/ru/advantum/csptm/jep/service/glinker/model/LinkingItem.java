package ru.advantum.csptm.jep.service.glinker.model;

public interface LinkingItem {

    double getOffset();

    double getLength();
}

