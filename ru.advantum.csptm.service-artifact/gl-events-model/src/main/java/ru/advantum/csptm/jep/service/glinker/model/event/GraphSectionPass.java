package ru.advantum.csptm.jep.service.glinker.model.event;

import lombok.Data;
import ru.advantum.csptm.jep.service.glinker.model.GraphSection;

import java.time.Instant;

/**
 * Created by Leonid on 17.02.2017.
 */
@Data
public class GraphSectionPass {

    private final long trId;
    private final long ttActionId;
    private final long roundId;
    private final long graphSectionId;
    private final int graphSectionOrderNum;
    private final Instant startTime;
    private final Instant endTime;

    public GraphSectionPass(long trId,
                            long ttActionId,
                            GraphSection graphSection,
                            Instant startTime,
                            Instant endTime) {
        this.trId = trId;
        this.ttActionId = ttActionId;
        this.roundId = graphSection.getRoundId();
        this.graphSectionId = graphSection.getGraphSectionId();
        this.graphSectionOrderNum = graphSection.getOrderNum();
        this.startTime = startTime;
        this.endTime = endTime;
    }
}
