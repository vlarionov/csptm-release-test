package ru.advantum.csptm.dbidgen;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * Created by kaganov on 06/04/2017.
 */
public interface DbIdGeneratorMapper {

    @Select("select nextval(#{sequenceName})")
    long nextDbId(@Param("sequenceName") String sequenceName);
}
