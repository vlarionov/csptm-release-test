package ru.advantum.csptm.dbidgen;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by kaganov on 06/04/2017.
 */
@Root(name = "db-id-generator")
public class DbIdGeneratorConfig {

    @Attribute(name = "sequence-name")
    private final String sequenceName;

    @Attribute(name = "sequence-increment")
    private final int sequenceIncrement;

    public DbIdGeneratorConfig(String sequenceName, int sequenceIncrement) {
        this.sequenceName = sequenceName;
        this.sequenceIncrement = sequenceIncrement;
    }

    public DbIdGeneratorConfig() {
        this(null, 0);
    }

    public String getSequenceName() {
        return sequenceName;
    }

    public int getSequenceIncrement() {
        return sequenceIncrement;
    }
}
