package ru.advantum.csptm.dbidgen;

/**
 * Created by kaganov on 06/04/2017.
 */
public interface IdGenerator {

    long nextId();
}
