package ru.advantum.csptm.dbidgen;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by kaganov on 06/04/2017.
 */
public class DbIdGenerator implements IdGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(DbIdGenerator.class);

    private final DbIdGeneratorConfig config;
    private final AtomicLong currentId;
    private final AtomicLong limitId;

    public DbIdGenerator(DbIdGeneratorConfig config, DataSource datasource) throws SQLException {
        this.config = config;

        SqlMapperSingleton.setConfiguration(datasource);

        currentId = new AtomicLong();
        limitId = new AtomicLong();

        loadNextId();
    }

    private void loadNextId() {
        currentId.set(fetchIdFromDb() - 1);
        limitId.set(currentId.get() + config.getSequenceIncrement());
    }

    private long fetchIdFromDb() {
        while (true) {
            try {
                try (SqlSession session = SqlMapperSingleton.getInstance().openSession()) {
                    DbIdGeneratorMapper mapper = session.getMapper(DbIdGeneratorMapper.class);
                    long dbId = mapper.nextDbId(config.getSequenceName());
                    LOGGER.info("Fetched new sequence value {}", dbId);
                    return dbId;
                }
            } catch (Throwable ex) {
                LOGGER.error("fetchIdFromDb", ex);
                try {
                    TimeUnit.SECONDS.sleep(5);
                } catch (InterruptedException e) {
                    LOGGER.error("fetchIdFromDb sleep", e);
                }
            }
        }
    }

    @Override
    public synchronized long nextId() {
        long nextId = currentId.incrementAndGet();

        // TODO: подумать о "фоновой" подгрузке id, но при этом можем случайно два раза подгрузить
        if (nextId == limitId.get()) {
            loadNextId();
        }

        return nextId;
    }
}
