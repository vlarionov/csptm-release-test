package ru.advantum.csptm.artifact.geotools;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.io.ParseException;
import org.geotools.geometry.jts.JTS;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.geometry.jts.WKTReader2;
import org.geotools.referencing.CRS;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;

public enum GeoTools {
    INSTANCE;

    private final MathTransform mathTransform;
    private final GeometryFactory geometryFactory;

    GeoTools() {
        try {
            CoordinateReferenceSystem projCRS = CRS.decode("EPSG:3338");
            mathTransform = CRS.findMathTransform(DefaultGeographicCRS.WGS84, projCRS);
            geometryFactory = JTSFactoryFinder.getGeometryFactory(null);
        } catch (FactoryException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    public Geometry transform(Geometry g) throws TransformException {
        return JTS.transform(g, mathTransform);
    }

    public Geometry inverseTransform(Geometry g) throws TransformException {
        return JTS.transform(g, mathTransform.inverse());
    }

    public GeometryFactory getGeometryFactory() {
        return geometryFactory;
    }

    public Geometry convertFromWkt(String wkt) {
        try {
            return transform(new WKTReader2(geometryFactory).read(wkt));
        } catch (TransformException | ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public Geometry createPoint(float lon, float lat) {
        try {
            return transform(geometryFactory.createPoint(new Coordinate(lon, lat)));
        } catch (TransformException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
