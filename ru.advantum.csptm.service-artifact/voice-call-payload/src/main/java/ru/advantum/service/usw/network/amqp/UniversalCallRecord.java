package ru.advantum.service.usw.network.amqp;

import java.util.ArrayList;
import java.util.List;

public class UniversalCallRecord implements Comparable<UniversalCallRecord> {
    private Long idCall = 0L;
    private Long owner = 0L;
    private Integer idRedialCall = 0;
    private List<Long> gsmNumbers = new ArrayList<>();
    private List<Long> uswNumbers = new ArrayList<>();
    private List<Long> stationNumbers = new ArrayList<>();

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder()
            .append("{id-call:").append(idCall)
            .append(", owner:").append(owner)
            .append(", idRedialCall:").append(idRedialCall);
        sb.append(", gsmNumbers:[");
        if (gsmNumbers != null) {
            for (Long gn : gsmNumbers) {
                sb.append(gn).append(",");
            }
        }
        sb.deleteCharAt(sb.lastIndexOf(",")).append("], uswNumbers:[");

        if (uswNumbers != null) {
            for (Long gn : uswNumbers) {
                sb.append(gn).append(",");
            }
        }
        sb.deleteCharAt(sb.lastIndexOf(",")).append("], stationNumbers:[");

        if (stationNumbers != null) {
            for (Long gn : stationNumbers) {
                sb.append(gn).append(",");
            }
        }
        sb.deleteCharAt(sb.lastIndexOf(",")).append("]");

        return sb.append("}").toString();
    }

    public Long getIdCall() {
        return idCall;
    }

    public void setIdCall(Long idCall) {
        this.idCall = idCall;
    }

    public Long getOwner() {
        return owner;
    }

    public void setOwner(Long owner) {
        this.owner = owner;
    }

    public List<Long> getGsmNumbers() {
        return gsmNumbers;
    }

    public void setGsmNumbers(List<Long> gsmNumbers) {
        this.gsmNumbers = gsmNumbers;
    }

    public List<Long> getUswNumbers() {
        return uswNumbers;
    }

    public void setUswNumbers(List<Long> uswNumbers) {
        this.uswNumbers = uswNumbers;
    }

    public List<Long> getStationNumbers() {
        return stationNumbers;
    }

    public void setStationNumbers(List<Long> stationNumbers) {
        this.stationNumbers = stationNumbers;
    }

    public Integer getIdRedialCall() {
        return idRedialCall;
    }

    public void setIdRedialCall(Integer idRedialCall) {
        this.idRedialCall = idRedialCall;
    }

    @Override
    public int compareTo(UniversalCallRecord o) {
        return this.getIdRedialCall().compareTo(o.getIdRedialCall());
    }
}
