package ru.advantum.service.usw.network.amqp;

public class CancelCallRecord implements Comparable<CancelCallRecord> {

    private Integer idRedialCall = 0;

    public Integer getIdRedialCall() {
        return idRedialCall;
    }

    public void setIdRedialCall(Integer idRedialCall) {
        this.idRedialCall = idRedialCall;
    }

    @Override
    public String toString() {
        return "CancelCallRecord{" + "idRedialCall=" + idRedialCall + '}';
    }

    @Override
    public int compareTo(CancelCallRecord o) {
        return this.getIdRedialCall().compareTo(o.getIdRedialCall());
    }
}
