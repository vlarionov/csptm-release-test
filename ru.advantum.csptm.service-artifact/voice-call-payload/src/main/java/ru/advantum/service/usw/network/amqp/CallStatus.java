package ru.advantum.service.usw.network.amqp;

import java.time.Instant;

public class CallStatus {

    /**
     * ID вызова
     */
    private Long idCall;
    /**
     * Время начала разговора
     */
    private Instant callBegin;
    /**
     * Время окончания разговора
     */
    private Instant callEnd;
    /**
     * Наименование аудио файла
     */
    private String audiofile;
    /**
     * Статус
     */
    private String status;
    /**
     * При возможности идентифицировать кто из участников регаты ЭТО СДЕЛАЛ, писать сюда
     */
    private String participantNumber;

    @Override
    public String toString() {
        return "CallStatus:idCall=" + idCall + ",callBegin=" + callBegin + ",callEnd=" + callEnd + ",audiofile=" + audiofile + ",status=" + status;
    }


    public Long getIdCall() {
        return idCall;
    }

    public CallStatus setIdCall(Long idCall) {
        this.idCall = idCall;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public CallStatus setStatus(String status) {
        this.status = status;
        return this;
    }

    public Instant getCallBegin() {
        return callBegin;
    }

    public CallStatus setCallBegin(Instant callBegin) {
        this.callBegin = callBegin;
        return this;
    }

    public Instant getCallEnd() {
        return callEnd;
    }

    public CallStatus setCallEnd(Instant callEnd) {
        this.callEnd = callEnd;
        return this;
    }

    public String getAudiofile() {
        return audiofile;
    }

    public CallStatus setAudiofile(String audiofile) {
        this.audiofile = audiofile;
        return this;
    }

    public String getParticipantNumber() {
        return participantNumber;
    }

    public CallStatus setParticipantNumber(String participantNumber) {
        this.participantNumber = participantNumber;
        return this;
    }

    public enum Status {
        INITIAL,
        NUMBER_NOT_AVAILABLE,
        DIALING,
        CONVERSATION,
        DONE,
        UNDEFINED;
    }
}
