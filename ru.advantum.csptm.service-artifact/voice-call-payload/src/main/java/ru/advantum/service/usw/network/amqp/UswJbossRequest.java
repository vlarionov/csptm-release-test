package ru.advantum.service.usw.network.amqp;


public class UswJbossRequest {
    // {"idTr":1123,"radioNumber":5307, "dispatcherNumber":2001, "reservedNumber":1001}
    private Long idCall;
    private Long idTr;
    private Long radioNumber;
    private Long dispatcherNumber;


    public UswJbossRequest() {
    }

    public Long getIdCall() {
        return idCall;
    }

    public void setIdCall(Long idCall) {
        this.idCall = idCall;
    }

    public Long getIdTr() {
        return idTr;
    }

    public void setIdTr(Long idTr) {
        this.idTr = idTr;
    }

    public Long getRadioNumber() {
        return radioNumber;
    }

    public void setRadioNumber(Long radioNumber) {
        this.radioNumber = radioNumber;
    }

    public Long getDispatcherNumber() {
        return dispatcherNumber;
    }

    public void setDispatcherNumber(Long dispatcherNumber) {
        this.dispatcherNumber = dispatcherNumber;
    }

}
