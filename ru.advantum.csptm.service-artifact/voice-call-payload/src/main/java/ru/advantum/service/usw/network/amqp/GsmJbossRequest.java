package ru.advantum.service.usw.network.amqp;

public class GsmJbossRequest {
    private Long idCall;
    private Long idTr;
    private String firstNumber;
    private String secondNumber;

    public GsmJbossRequest() {
    }

    public Long getIdTr() {
        return idTr;
    }

    public void setIdTr(Long idTr) {
        this.idTr = idTr;
    }

    public Long getIdCall() {
        return idCall;
    }

    public void setIdCall(Long idCall) {
        this.idCall = idCall;
    }

    public String getFirstNumber() {
        return firstNumber;
    }

    public void setFirstNumber(String firstNumber) {
        this.firstNumber = firstNumber;
    }

    public String getSecondNumber() {
        return secondNumber;
    }

    public void setSecondNumber(String secondNumber) {
        this.secondNumber = secondNumber;
    }
}
