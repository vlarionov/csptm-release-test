package ru.advantum.csptm.spring.properties;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.load.Persister;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.core.io.support.PropertySourceFactory;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public abstract class AbstractPropertySourceFactory<C> implements PropertySourceFactory {

    public abstract C getConfig() throws Exception;

    @Override
    public PropertiesPropertySource createPropertySource(String name, EncodedResource resource) throws IOException {

        try {
            Properties properties = new Properties();
            PropertiesWriterHandler propertiesWriterHandler = new PropertiesWriterHandler(properties);
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(deserializeAsStream(getConfig()), propertiesWriterHandler);
            return new PropertiesPropertySource(name, properties);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private InputStream deserializeAsStream(C config) throws Exception {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Serializer serializer = new Persister();
        serializer.write(config, outputStream);
        outputStream.flush();
        return new ByteArrayInputStream(outputStream.toByteArray());
    }
}
