package ru.advantum.csptm.spring.properties;

import org.springframework.util.StringUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.LinkedList;
import java.util.Properties;

public class PropertiesWriterHandler extends DefaultHandler {
    private final Properties properties;

    private final LinkedList<String> pathStack = new LinkedList<>();

    public PropertiesWriterHandler(Properties properties) {
        this.properties = properties;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        pathStack.addLast(qName);
        String propertyRootKey = getPropertyKeyFromPath();
        for (int i = 0; i < attributes.getLength(); i++) {
            properties.put(
                    propertyRootKey + "." + attributes.getQName(i),
                    attributes.getValue(i)
            );
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        String elementName = pathStack.removeLast();
        if (!elementName.equals(qName)) {
            throw new SAXException("Invalid xml");
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String value = new String(ch, start, length);
        if (StringUtils.hasText(value)) {
            properties.put(
                    getPropertyKeyFromPath(),
                    value
            );
        }
    }

    private String getPropertyKeyFromPath() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < pathStack.size(); i++) {
            if (i != 0) {
                builder.append(".");
            }
            builder.append(pathStack.get(i));
        }
        return builder.toString();
    }
}
