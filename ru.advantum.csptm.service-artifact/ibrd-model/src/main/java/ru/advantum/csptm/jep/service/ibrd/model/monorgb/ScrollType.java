package ru.advantum.csptm.jep.service.ibrd.model.monorgb;

public enum ScrollType {
    NONE(0), ONE_BY_ONE(1);

    private final int id;

    ScrollType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static ScrollType find(int id) {
        for (ScrollType type : ScrollType.values()) {
            if (type.getId() == id) {
                return type;
            }
        }
        throw new IllegalArgumentException();
    }

}