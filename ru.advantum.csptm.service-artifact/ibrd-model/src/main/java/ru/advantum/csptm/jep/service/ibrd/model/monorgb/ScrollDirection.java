package ru.advantum.csptm.jep.service.ibrd.model.monorgb;

public enum ScrollDirection {
    UP(0), DOWN(1);

    private final int id;

    ScrollDirection(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static ScrollDirection find(int id) {
        for (ScrollDirection direction : ScrollDirection.values()) {
            if (direction.getId() == id) {
                return direction;
            }
        }
        throw new IllegalArgumentException();
    }
}