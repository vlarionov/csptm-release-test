package ru.advantum.csptm.jep.service.ibrd.model.monorgb;

public class BrightnessLevel {
    private final short hour;

    private final short value;

    public BrightnessLevel(short hour, short value) {
        this.hour = hour;
        this.value = value;
    }

    public short getHour() {
        return hour;
    }

    public short getValue() {
        return value;
    }
}
