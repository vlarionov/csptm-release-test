package ru.advantum.csptm.jep.service.ibrd.model;

import java.util.List;

public class IbrdInfo {
    private final List<IbrdMessage> messageList;

    private final List<IbrdForecast> forecastList;

    public IbrdInfo(List<IbrdMessage> messageList, List<IbrdForecast> forecastList) {
        this.messageList = messageList;
        this.forecastList = forecastList;
    }

    public List<IbrdMessage> getMessageList() {
        return messageList;
    }

    public List<IbrdForecast> getForecastList() {
        return forecastList;
    }

}
