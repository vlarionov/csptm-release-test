package ru.advantum.csptm.jep.service.ibrd.model.serializer;

import com.google.gson.Gson;
import ru.advantum.csptm.jep.proto.CommandPacket;
import ru.advantum.csptm.jep.proto.UnitPacket;
import ru.advantum.csptm.jep.proto.UnitPacketBuilder;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdEvent;

import java.time.ZoneOffset;
import java.util.Collections;

public class IbrdEventToPacketAdapter {
    private static final String DATA_KEY = "data";

    private final Gson gson;

    public IbrdEventToPacketAdapter(Gson gson) {
        this.gson = gson;
    }

    public UnitPacket toUnitPacket(IbrdEvent ibrdEvent) {
       return new UnitPacketBuilder()
               .setUnitId(ibrdEvent.getIbrdId())
               .setEventTime(ibrdEvent.getEventDate().toInstant(ZoneOffset.UTC))
               .addAttribute(DATA_KEY, gson.toJson(ibrdEvent))
               .build();
    }

    public CommandPacket toCommandPacket(IbrdEvent ibrdEvent) {
        return new CommandPacket(
                (long) ibrdEvent.getIbrdId(),
                0,
                null,
                null,
                Collections.singletonMap(DATA_KEY, gson.toJson(ibrdEvent))
        );
    }

    public IbrdEvent toIbrdEvent(UnitPacket unitPacket) {
        return gson.fromJson(
                unitPacket.getAttributes().get(DATA_KEY).toString(),
                IbrdEvent.class
        );
    }

    public IbrdEvent toIbrdEvent(CommandPacket commandPacket) {
        return gson.fromJson(commandPacket.getParameters().get(DATA_KEY), IbrdEvent.class);
    }

}
