package ru.advantum.csptm.jep.service.ibrd.model;

public class TelematicsRequest {
    public static final String RMQ_TYPE = "TelematicsRequest";

    private final int ibrdId;

    public TelematicsRequest(int ibrdId) {
        this.ibrdId = ibrdId;
    }

    public int getIbrdId() {
        return ibrdId;
    }
}
