package ru.advantum.csptm.jep.service.ibrd.model;

public class  IbrdMessage {
    private final String text;

    private final IbrdColor color;

    public IbrdMessage(String text, IbrdColor color) {
        this.text = text;
        this.color = color;
    }

    public IbrdMessage(String text, Integer rColor, Integer gColor, Integer bColor) {
        this.text = text;
        if (rColor == null || gColor == null || bColor == null) {
            this.color = null;
        } else {
            this.color = new IbrdColor(rColor, gColor, bColor);
        }
    }

    public String getText() {
        return text;
    }

    public IbrdColor getColor() {
        return color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IbrdMessage that = (IbrdMessage) o;

        if (text != null ? !text.equals(that.text) : that.text != null) return false;
        return color != null ? color.equals(that.color) : that.color == null;
    }

    @Override
    public int hashCode() {
        int result = text != null ? text.hashCode() : 0;
        result = 31 * result + (color != null ? color.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return  text ;
    }
}
