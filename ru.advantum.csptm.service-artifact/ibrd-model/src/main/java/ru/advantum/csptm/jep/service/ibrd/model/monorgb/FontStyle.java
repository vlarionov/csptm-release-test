package ru.advantum.csptm.jep.service.ibrd.model.monorgb;


public enum FontStyle {
    SLIM_MONOSPACED(0),
    NORMAL_MONOSPACED(1),
    SLIM_NOT_MONOSPACED(2),
    NORMAL_NOT_MONOSPACED(3);

    private final int id;

    FontStyle(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static FontStyle find(int id) {
        for (FontStyle style : FontStyle.values()) {
            if (style.getId() == id) {
                return style;
            }
        }
        throw new IllegalArgumentException();
    }
}
