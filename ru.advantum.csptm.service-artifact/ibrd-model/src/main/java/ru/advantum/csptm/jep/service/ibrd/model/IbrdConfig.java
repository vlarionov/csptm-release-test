package ru.advantum.csptm.jep.service.ibrd.model;

import java.time.Duration;

public abstract class IbrdConfig {
    private final boolean displayTemperature;
    private final short runningRowSpeed;
    private final Duration dataWaitingTime;
    private final Duration maxForecastInterval;
    private final boolean isLimitForecastsByRows;

    public IbrdConfig(boolean displayTemperature, short runningRowSpeed, Duration dataWaitingTime, Duration maxForecastInterval, boolean isLimitForecastsByRows) {
        this.displayTemperature = displayTemperature;
        this.runningRowSpeed = runningRowSpeed;
        this.dataWaitingTime = dataWaitingTime;
        this.maxForecastInterval = maxForecastInterval;
        this.isLimitForecastsByRows = isLimitForecastsByRows;
    }

    public boolean isDisplayTemperature() {
        return displayTemperature;
    }

    public short getRunningRowSpeed() {
        return runningRowSpeed;
    }

    public Duration getDataWaitingTime() {
        return dataWaitingTime;
    }

    public Duration getMaxForecastInterval() {
        return maxForecastInterval;
    }

    public boolean isLimitForecastsByRows() {
        return isLimitForecastsByRows;
    }

    public abstract int getForecastRowNumber();

}
