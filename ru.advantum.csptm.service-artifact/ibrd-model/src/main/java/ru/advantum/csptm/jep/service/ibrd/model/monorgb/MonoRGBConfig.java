package ru.advantum.csptm.jep.service.ibrd.model.monorgb;

import ru.advantum.csptm.jep.service.ibrd.model.IbrdConfig;

import java.time.Duration;
import java.util.List;

public class MonoRGBConfig extends IbrdConfig {
    private final boolean displayDatetime;
    private final RunningRowPosition runningRowPosition;
    private final ScrollType scrollType;
    private final ScrollDirection scrollDirection;
    private final short rowsNumber;
    private final FontStyle routeNumberStyle;
    private final FontStyle arrivalTimeStyle;
    private final FontStyle terminalStyle;
    private final Duration forecastDelay;
    private final List<BrightnessLevel> brightnessLevels;

    public MonoRGBConfig(boolean displayTemperature, short runningRowSpeed, Duration dataWaitingTime, Duration maxForecastInterval, boolean isLimitForecastsByRows, boolean displayDatetime, RunningRowPosition runningRowPosition, ScrollType scrollType, ScrollDirection scrollDirection, short rowsNumber, FontStyle routeNumberStyle, FontStyle arrivalTimeStyle, FontStyle terminalStyle, Duration forecastDelay, List<BrightnessLevel> brightnessLevels) {
        super(displayTemperature, runningRowSpeed, dataWaitingTime, maxForecastInterval, isLimitForecastsByRows);
        this.displayDatetime = displayDatetime;
        this.runningRowPosition = runningRowPosition;
        this.scrollType = scrollType;
        this.scrollDirection = scrollDirection;
        this.rowsNumber = rowsNumber;
        this.routeNumberStyle = routeNumberStyle;
        this.arrivalTimeStyle = arrivalTimeStyle;
        this.terminalStyle = terminalStyle;
        this.forecastDelay = forecastDelay;
        this.brightnessLevels = brightnessLevels;
    }

    public MonoRGBConfig(
            boolean displayTemperature,
            short runningRowSpeed,
            Duration dataWaitingTime,
            Duration maxForecastInterval,
            boolean isLimitForecastsByRows,
            boolean displayDatetime,
            int runningRowPositionId,
            int scrollTypeId,
            int scrollDirectionId,
            short rowsNumber,
            int routeNumberStyleId,
            int arrivalTimeStyleId,
            int terminalStyleId,
            Duration forecastDelay,
            List<BrightnessLevel> brightnessLevels
    ) {
        this(
                displayTemperature,
                runningRowSpeed,
                dataWaitingTime,
                maxForecastInterval,
                isLimitForecastsByRows,
                displayDatetime,
                RunningRowPosition.find(runningRowPositionId),
                ScrollType.find(scrollTypeId),
                ScrollDirection.find(scrollDirectionId),
                rowsNumber,
                FontStyle.find(routeNumberStyleId),
                FontStyle.find(arrivalTimeStyleId),
                FontStyle.find(terminalStyleId),
                forecastDelay,
                brightnessLevels
        );
    }

    public boolean isDisplayDatetime() {
        return displayDatetime;
    }

    public RunningRowPosition getRunningRowPosition() {
        return runningRowPosition;
    }

    public ScrollType getScrollType() {
        return scrollType;
    }

    public ScrollDirection getScrollDirection() {
        return scrollDirection;
    }

    public short getRowsNumber() {
        return rowsNumber;
    }

    public FontStyle getRouteNumberStyle() {
        return routeNumberStyle;
    }

    public FontStyle getArrivalTimeStyle() {
        return arrivalTimeStyle;
    }

    public FontStyle getTerminalStyle() {
        return terminalStyle;
    }

    public Duration getForecastDelay() {
        return forecastDelay;
    }

    public List<BrightnessLevel> getBrightnessLevels() {
        return brightnessLevels;
    }

    @Override
    public int getForecastRowNumber() {
        return rowsNumber;
    }
}
