package ru.advantum.csptm.jep.service.ibrd.model;

import lombok.Data;
import lombok.NonNull;
import ru.advantum.csptm.jep.service.ibrd.model.monorgb.MonoRGBConfig;
import ru.advantum.csptm.jep.service.ibrd.model.monorgb.MonoRGBInfo;
import ru.advantum.csptm.jep.service.ibrd.model.row.RowConfig;
import ru.advantum.csptm.jep.service.ibrd.model.row.RowInfo;

import java.time.LocalDateTime;
import java.util.List;


@Data
public class IbrdEvent {

    private Long eventId;

    @NonNull
    private final LocalDateTime eventDate;
    @NonNull
    private final IbrdEventType eventType;
    @NonNull
    private final int ibrdId;
    @NonNull
    private final String channelId;

    private Long commandEventId;

    private IbrdErrorType errorType;

    private Boolean coordsValid;
    private Float lon;
    private Float lat;

    private RowConfig rowConfig;
    private MonoRGBConfig monoRGBConfig;

    private RowInfo rowInfo;
    private MonoRGBInfo monoRGBInfo;

    private Integer temperature;

    private LocalDateTime boardTime;

}
