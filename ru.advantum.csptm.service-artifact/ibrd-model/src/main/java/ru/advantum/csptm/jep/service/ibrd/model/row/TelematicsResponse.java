package ru.advantum.csptm.jep.service.ibrd.model.row;

import ru.advantum.csptm.jep.service.ibrd.model.IbrdResponse;

public class TelematicsResponse extends IbrdResponse {

    private final Boolean coordsValid;

    private final float lon;

    private final float lat;

    public TelematicsResponse(int commandNumber, Boolean coordsValid, float lon, float lat) {
        super(commandNumber);
        this.coordsValid = coordsValid;
        this.lon = lon;
        this.lat = lat;
    }

    public Boolean isCoordsValid() {
        return coordsValid;
    }

    public float getLon() {
        return lon;
    }

    public float getLat() {
        return lat;
    }


}
