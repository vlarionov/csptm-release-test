package ru.advantum.csptm.jep.service.ibrd.model.row;

import ru.advantum.csptm.jep.service.ibrd.model.IbrdForecast;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdInfo;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdMessage;

import java.time.Duration;
import java.util.List;

public class RowInfo extends IbrdInfo {
    private final short runningRowSpeed;
    private final Duration blockRate;

    public RowInfo(List<IbrdMessage> message, List<IbrdForecast> forecastList, short runningRowSpeed, Duration blockRate) {
        super(message, forecastList);
        this.runningRowSpeed = runningRowSpeed;
        this.blockRate = blockRate;
    }

    public short getRunningRowSpeed() {
        return runningRowSpeed;
    }

    public Duration getBlockRate() {
        return blockRate;
    }
}
