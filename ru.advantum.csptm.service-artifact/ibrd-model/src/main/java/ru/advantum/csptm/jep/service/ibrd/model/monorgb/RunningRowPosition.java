package ru.advantum.csptm.jep.service.ibrd.model.monorgb;

public enum RunningRowPosition {
    DOWN(0), UP(1);

    private final int id;

    RunningRowPosition(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static RunningRowPosition find(int id) {
        for (RunningRowPosition position : RunningRowPosition.values()) {
            if (position.getId() == id) {
                return position;
            }
        }
        throw new IllegalArgumentException();
    }

}
