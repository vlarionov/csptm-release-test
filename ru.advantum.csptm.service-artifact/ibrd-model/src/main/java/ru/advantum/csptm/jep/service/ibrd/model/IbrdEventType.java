package ru.advantum.csptm.jep.service.ibrd.model;

public enum IbrdEventType {
    CONNECT(0),
    DISCONNECT(1),
    COMMAND_RESPONSE(2),
    SET_CONFIG(3),
    SET_INFO(4),
    SET_DATETIME(5),
    SET_TEMPERATURE(6),
    TELEMATICS_REQUEST(7),
    TELEMATICS_RESPONSE(8);

    private final int id;

    IbrdEventType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static IbrdEventType find(int id) {
        for (IbrdEventType event : IbrdEventType.values()) {
            if (event.getId() == id) {
                return event;
            }
        }
        throw new IllegalArgumentException();
    }
}
