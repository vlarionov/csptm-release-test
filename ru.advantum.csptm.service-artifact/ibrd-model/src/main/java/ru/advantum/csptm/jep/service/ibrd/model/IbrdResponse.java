package ru.advantum.csptm.jep.service.ibrd.model;

public class IbrdResponse {
    private final int commandNumber;
    private final IbrdErrorType errorType;
    private final Integer errorByteNumber;

    public IbrdResponse(int commandNumber, IbrdErrorType errorType, Integer errorByteNumber) {
        this.commandNumber = commandNumber;
        this.errorType = errorType;
        this.errorByteNumber = errorByteNumber;
    }

    public IbrdResponse(int commandNumber, IbrdErrorType errorType) {
        this(commandNumber, errorType, null);
    }

    public IbrdResponse(int commandNumber) {
        this(commandNumber, IbrdErrorType.NULL, null);
    }

    public int getCommandNumber() {
        return commandNumber;
    }

    public IbrdErrorType getErrorType() {
        return errorType;
    }

    public Integer getErrorByteNumber() {
        return errorByteNumber;
    }


    @Override
    public String toString() {
        return "IbrdResponse{" +
                "commandNumber=" + commandNumber +
                ", errorType=" + errorType +
                ", errorByteNumber=" + errorByteNumber +
                '}';
    }
}
