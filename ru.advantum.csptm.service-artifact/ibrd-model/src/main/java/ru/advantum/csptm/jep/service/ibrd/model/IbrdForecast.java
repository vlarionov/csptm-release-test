package ru.advantum.csptm.jep.service.ibrd.model;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class IbrdForecast {
    private static final DateTimeFormatter ARRIVAL_TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");

    private final Long id;

    private final String routeNumber;

    private final String terminal;

    private final LocalDateTime arrivalTime;

    public IbrdForecast(Long id, String routeNumber, LocalDateTime arrivalTime, String terminal) {
        this.id = id;
        this.routeNumber = routeNumber;
        this.arrivalTime = arrivalTime;
        this.terminal = terminal;
    }

    public Long getId() {
        return id;
    }

    public String getRouteNumber() {
        return routeNumber;
    }

    public LocalDateTime getArrivalTime() {
        return arrivalTime;
    }

    public String getTerminal() {
        return terminal;
    }

    public String getFormattedArrivalTime() {
        if (arrivalTime == null) {
            return "";
        }

        Duration forecastDuration = Duration.between(LocalDateTime.now(), arrivalTime);
        if (forecastDuration.isNegative()) {
            forecastDuration = Duration.ZERO;
        }
        String postfix = forecastDuration.toMinutes() >= 10 ? "мин" : " мин";
        return forecastDuration.toMinutes() + postfix;

    }

}
