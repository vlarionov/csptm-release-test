package ru.advantum.csptm.jep.service.ibrd.model.row;

import ru.advantum.csptm.jep.service.ibrd.model.IbrdConfig;

import java.time.Duration;

public class RowConfig extends IbrdConfig {
    private final boolean displayDate;
    private final boolean displayTime;
    private final short scrollSpeed;
    private final Duration blockRate;
    private final Duration serverWaitingTime;
    private final Duration rebootTime;

    public RowConfig(
            boolean displayTemperature,
            short runningRowSpeed,
            Duration dataWaitingTime,
            Duration maxForecastInterval,
            boolean isLimitForecastsByRows,
            boolean displayDate,
            boolean displayTime,
            short scrollSpeed,
            Duration blockRate,
            Duration serverWaitingTime,
            Duration rebootTime
    ) {
        super(displayTemperature, runningRowSpeed, dataWaitingTime, maxForecastInterval, isLimitForecastsByRows);
        this.displayDate = displayDate;
        this.displayTime = displayTime;
        this.scrollSpeed = scrollSpeed;
        this.blockRate = blockRate;
        this.serverWaitingTime = serverWaitingTime;
        this.rebootTime = rebootTime;
    }

    public boolean isDisplayDate() {
        return displayDate;
    }

    public boolean isDisplayTime() {
        return displayTime;
    }

    public short getScrollSpeed() {
        return scrollSpeed;
    }

    public Duration getBlockRate() {
        return blockRate;
    }

    public Duration getServerWaitingTime() {
        return serverWaitingTime;
    }

    public Duration getRebootTime() {
        return rebootTime;
    }

    @Override
    public int getForecastRowNumber() {
        return 2;
    }
}
