package ru.advantum.csptm.jep.service.ibrd.model.monorgb;

import ru.advantum.csptm.jep.service.ibrd.model.IbrdForecast;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdInfo;
import ru.advantum.csptm.jep.service.ibrd.model.IbrdMessage;

import java.util.List;

public class MonoRGBInfo extends IbrdInfo {
    private final MonoRGBConfig config;

    public MonoRGBInfo(List<IbrdMessage> message, List<IbrdForecast> forecastList, MonoRGBConfig config) {
        super(message, forecastList);
        this.config = config;
    }

    public MonoRGBConfig getConfig() {
        return config;
    }
}
