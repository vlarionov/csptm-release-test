package ru.advantum.csptm.jep.service.ibrd.model;

public enum  IbrdErrorType {
    NULL(0),
    PACKET_LENGTH_EXCEEDED(1),
    CHECK_SUM_NOT_MUCH(2),
    UNEXPECTED_PACKAGE_END(3),
    VALUE_OUT_OF_RANGE(4),
    COMMAND_CAN_NOT_BE_EXECUTED(5),
    ROW_NOT_REPLY(6),
    UNKNOWN_COMMAND(7),
    INVALID_PACKAGE(8);

    private final int id;

    IbrdErrorType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static IbrdErrorType find(int id) {
        for (IbrdErrorType errorType : IbrdErrorType.values()) {
            if (errorType.getId() == id) {
                return errorType;
            }
        }
        throw new IllegalArgumentException();
    }
}
