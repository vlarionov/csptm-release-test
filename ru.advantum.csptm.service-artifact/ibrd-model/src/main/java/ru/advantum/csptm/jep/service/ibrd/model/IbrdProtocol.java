package ru.advantum.csptm.jep.service.ibrd.model;

public enum IbrdProtocol {
    ROW(8),
    MONO(9),
    RGB(10);

    private final int id;

    IbrdProtocol(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static IbrdProtocol find(int id) {
        for (IbrdProtocol type : IbrdProtocol.values()) {
            if (type.getId() == id) {
                return type;
            }
        }
        throw new IllegalArgumentException();
    }
}
