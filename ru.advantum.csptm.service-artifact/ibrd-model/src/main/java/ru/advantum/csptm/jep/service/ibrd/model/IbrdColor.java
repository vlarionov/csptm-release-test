package ru.advantum.csptm.jep.service.ibrd.model;

public class IbrdColor {
    private final int rColor;

    private final int gColor;

    private final int bColor;

    public IbrdColor(int rColor, int gColor, int bColor) {
        this.rColor = rColor;
        this.gColor = gColor;
        this.bColor = bColor;
    }

    public int getrColor() {
        return rColor;
    }

    public int getgColor() {
        return gColor;
    }

    public int getbColor() {
        return bColor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IbrdColor ibrdColor = (IbrdColor) o;

        if (rColor != ibrdColor.rColor) return false;
        if (gColor != ibrdColor.gColor) return false;
        return bColor == ibrdColor.bColor;
    }

    @Override
    public int hashCode() {
        int result = rColor;
        result = 31 * result + gColor;
        result = 31 * result + bColor;
        return result;
    }
}
