package ru.advantum.csptm.artifact.csvsaver.abstractions;

/**
 * Created by Leonid on 25.01.2017.
 */
public interface DestinationFactory {
    Appendable build() throws Exception;
}
