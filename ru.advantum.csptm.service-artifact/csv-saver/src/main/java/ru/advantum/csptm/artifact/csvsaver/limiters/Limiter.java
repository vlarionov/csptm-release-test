package ru.advantum.csptm.artifact.csvsaver.limiters;

public interface Limiter extends AutoCloseable {
    <T> void append(T object) throws Exception;

}
