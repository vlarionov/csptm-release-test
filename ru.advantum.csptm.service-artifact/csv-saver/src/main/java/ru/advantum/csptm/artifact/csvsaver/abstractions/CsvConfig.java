package ru.advantum.csptm.artifact.csvsaver.abstractions;

import java.util.Map;

/**
 * Created by Leonid on 23.01.2017.
 */
public interface CsvConfig {

    char getDelimiter();

    char getEscape();

    String[] getHeader();

    boolean getTrimEnabled();

    String getNullString();

    String getRecordSeparator();

    Map<Class, RecordPrinter> getPrintStrategyMap();
}
