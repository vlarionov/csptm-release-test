package ru.advantum.csptm.artifact.csvsaver.destination;

import java.io.File;

public class TempFileDestinationFactory extends FileDestinationFactory {
    private final String tempFilePrefix;

    public TempFileDestinationFactory(String root, String pattern, String tempFilePrefix) {
        super(root, pattern);
        this.tempFilePrefix = tempFilePrefix;
    }

    @Override
    protected String buildFileName() {
        return tempFilePrefix + super.buildFileName();
    }

    @Override
    protected void onCloseFile(File file) {
        super.onCloseFile(file);
        renameTempFile(file);
    }

    private void renameTempFile(File tempFile) {
        String persistentFileName = tempFile.getName().replaceFirst(tempFilePrefix, "");
        File persistentFile = new File(root, persistentFileName);

        boolean isSuccess = tempFile.renameTo(persistentFile);

        if (!isSuccess) {
            logger.error(
                    "Can not rename temp file {}",
                    tempFile.getAbsolutePath()
            );
        }
    }
}
