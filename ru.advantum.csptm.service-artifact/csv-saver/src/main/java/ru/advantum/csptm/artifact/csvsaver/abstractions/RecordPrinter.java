package ru.advantum.csptm.artifact.csvsaver.abstractions;

/**
 * Created by Leonid on 25.01.2017.
 */
public interface RecordPrinter<R, P> {
    void printRecord(final R record, final P printer) throws Exception;
}
