package ru.advantum.csptm.artifact.csvsaver.destination;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.artifact.csvsaver.abstractions.DestinationFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by Leonid on 25.01.2017.
 */
public class FileDestinationFactory implements DestinationFactory {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    protected final File root;

    private final String pattern;

    public FileDestinationFactory(final String root, final String pattern) {
        this.root = new File(root);
        this.pattern = pattern;
    }

    @Override
    public Appendable build() throws Exception {
        root.mkdirs();
        return buildFileWriter();
    }

    protected String buildFileName() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(pattern)) + ".csv";
    }

    protected void onCloseFile(File file) {
        logger.info("File {} closed", file.getName());
    }

    private FileWriter buildFileWriter() throws IOException {
        File file = buildFile();
        FileWriter writer = new FileWriter(file) {
            @Override
            public void close() throws IOException {
                super.close();
                onCloseFile(file);
            }
        };
        logger.info("File {} created", file.getName());
        return writer;
    }

    private File buildFile() {
        return new File(root.getPath() + File.separator + buildFileName());
    }


}
