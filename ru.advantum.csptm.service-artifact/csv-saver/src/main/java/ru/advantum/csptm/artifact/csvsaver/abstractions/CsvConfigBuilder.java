package ru.advantum.csptm.artifact.csvsaver.abstractions;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Leonid on 23.01.2017.
 */
public abstract class CsvConfigBuilder {

    protected char delimiter = ';';
    protected char escape = '"';
    protected String[] header;
    protected boolean trimEnabled = false;
    protected String nullString = "";
    protected String recordSeparator = System.lineSeparator();

    protected Map<Class, RecordPrinter> strategyMap = new HashMap<>();

    public CsvConfigBuilder() {
    }

    public CsvConfigBuilder withHeader(String... header) {
        this.header = header;

        return this;
    }

    public CsvConfigBuilder withDelimiter(char delimiter) {
        this.delimiter = delimiter;

        return this;
    }

    public CsvConfigBuilder withEscape(char escape) {
        this.escape = escape;

        return this;
    }

    public CsvConfigBuilder withTrimEnabled(boolean trimEnabled) {
        this.trimEnabled = trimEnabled;

        return this;
    }

    public CsvConfigBuilder withNullString(String nullString) {
        this.nullString = nullString;

        return this;
    }

    public CsvConfigBuilder withRecordSeparator(String recordSeparator) {
        this.recordSeparator = recordSeparator;

        return this;
    }

    public <T, K> CsvConfigBuilder withPrintStrategy(Class<T> clazz, RecordPrinter<T, K> strategy) {
        strategyMap.put(clazz, strategy);

        return this;
    }

    public abstract CsvConfig build();
}
