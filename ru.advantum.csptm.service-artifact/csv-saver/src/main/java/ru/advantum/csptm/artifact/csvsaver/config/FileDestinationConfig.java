package ru.advantum.csptm.artifact.csvsaver.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by Leonid on 27.01.2017.
 */
@Root(name = "file-destination")
public class FileDestinationConfig {
    @Attribute(name = "directory")
    private String directory;

    @Attribute(name = "pattern")
    private String pattern;

    public String getDirectory() {
        return directory;
    }

    public String getPattern() {
        return pattern;
    }
}
