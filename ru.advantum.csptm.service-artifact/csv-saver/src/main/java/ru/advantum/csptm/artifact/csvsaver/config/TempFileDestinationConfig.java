package ru.advantum.csptm.artifact.csvsaver.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name = "file-destination")
public class TempFileDestinationConfig extends FileDestinationConfig {
    @Attribute(name = "tempPrefix", required = false)
    private String tempPrefix = "";

    public String getTempPrefix() {
        return tempPrefix;
    }
}
