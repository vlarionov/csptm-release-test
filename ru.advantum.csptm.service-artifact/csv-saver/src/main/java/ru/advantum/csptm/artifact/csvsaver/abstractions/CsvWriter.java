package ru.advantum.csptm.artifact.csvsaver.abstractions;

/**
 * Created by Leonid on 23.01.2017.
 */
public abstract class CsvWriter implements AutoCloseable {

    protected final CsvConfig config;
    protected final Appendable destination;

    public CsvWriter(Appendable destination, CsvConfig config) {
        this.config = config;
        this.destination = destination;
    }

    public abstract <T> void append(final T object) throws Exception;

    public abstract void flush() throws Exception;

    public CsvConfig getConfig() {
        return config;
    }

    public Appendable getDestination() {
        return destination;
    }
}
