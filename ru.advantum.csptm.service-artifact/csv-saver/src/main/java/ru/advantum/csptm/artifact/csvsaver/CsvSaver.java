package ru.advantum.csptm.artifact.csvsaver;

import ru.advantum.csptm.artifact.csvsaver.abstractions.CsvConfig;
import ru.advantum.csptm.artifact.csvsaver.abstractions.CsvConfigBuilder;
import ru.advantum.csptm.artifact.csvsaver.abstractions.CsvWriter;
import ru.advantum.csptm.artifact.csvsaver.abstractions.RecordPrinter;
import ru.advantum.csptm.artifact.csvsaver.apache.ApacheConfig;
import ru.advantum.csptm.artifact.csvsaver.apache.ApacheConfigBuilder;
import ru.advantum.csptm.artifact.csvsaver.apache.ApacheWriter;
import ru.advantum.csptm.artifact.csvsaver.config.CsvSaverConfig;
import ru.advantum.csptm.artifact.csvsaver.config.CsvWriterConfig;
import ru.advantum.csptm.artifact.csvsaver.config.FileDestinationConfig;
import ru.advantum.csptm.artifact.csvsaver.config.TempFileDestinationConfig;
import ru.advantum.csptm.artifact.csvsaver.destination.FileDestinationFactory;
import ru.advantum.csptm.artifact.csvsaver.destination.TempFileDestinationFactory;
import ru.advantum.csptm.artifact.csvsaver.limiters.CombinedLimiter;

import java.util.Collections;
import java.util.Map;

/**
 * Created by Leonid on 24.01.2017.
 */
public class CsvSaver {

    public static FileDestinationFactory getFileDestinationFactory(final FileDestinationConfig config) {
        return new FileDestinationFactory(
                config.getDirectory(),
                config.getPattern()
        );
    }

    public static TempFileDestinationFactory getTempFileDestinationFactory(final TempFileDestinationConfig config) {
        return new TempFileDestinationFactory(
                config.getDirectory(),
                config.getPattern(),
                config.getTempPrefix()
        );
    }

    public static CsvConfigBuilder getConfigBuilder() {
        return new ApacheConfigBuilder();
    }

    public static CsvConfigBuilder getConfigBuilder(final CsvWriterConfig config) {
        return new ApacheConfigBuilder()
                .withDelimiter(config.getDelimiter())
                .withEscape(config.getEscape())
                .withNullString(config.getNullString())
                .withRecordSeparator(config.getRecordSeparator())
                .withTrimEnabled(config.getTrimEnabled());
    }

    public static CsvConfig buildConfig(final CsvWriterConfig config) {
        return getConfigBuilder(config).build();
    }

    public static CsvWriter getWriter(final Appendable destination, final CsvConfig config) throws Exception {
        return new ApacheWriter(destination, (ApacheConfig) config);
    }

    public static CsvWriter getWriter(final Appendable destination) throws Exception {
        return new ApacheWriter(destination, (ApacheConfig) CsvSaver.getConfigBuilder().build());
    }

    public static CsvWriter getWriter(final Appendable destination, final CsvWriterConfig config) throws Exception {
        return new ApacheWriter(destination, (ApacheConfig) buildConfig(config));
    }

    public static CombinedLimiter createLimiter(
            CsvSaverConfig config,
            String[] headers,
            Map<Class, RecordPrinter> printerMap
    ) {
        if (headers == null) {
            headers = new String[0];
        }

        CsvConfigBuilder configBuilder = CsvSaver.getConfigBuilder(config.getCsvWriterConfig());
        configBuilder.withHeader(headers);
        printerMap.forEach(configBuilder::withPrintStrategy);

        return new CombinedLimiter(
                configBuilder.build(),
                getTempFileDestinationFactory(config.getTempFileDestinationConfig()),
                config.getCombinedLimiterConfig()
        );
    }

    public static CombinedLimiter createLimiter(
            CsvSaverConfig config,
            String[] headers,
            Class recordClass,
            RecordPrinter recordPrinter
    ) {
        return createLimiter(
                config,
                headers,
                Collections.singletonMap(
                        recordClass,
                        recordPrinter
                )
        );
    }


    /*public static void main(String[] args) throws Exception {
        CsvConfigBuilder configBuilder = CsvSaver.getConfigBuilder();
        DestinationFactory destinationFactory = new FileDestinationFactory("csv/", "yyyyMMdd_HHmmssSS");

        CsvConfig config = configBuilder.build();
        //сложнее, с ограничителем по строкам
        config = configBuilder.withHeader("column2", "column3", "column4")
                .withPrintStrategy(Values.class, (ApacheRecordPrinter<Values>) (record, printer) ->
                        printer.printRecord(record.value2, record.value3, record.value4)).build();
        try (CsvLimiter limiter = new RowLimiter(config, destinationFactory, 20L)) {

            for (int i = 0; i < 30; i++) {
                limiter.append(new Values(-4569579972474444107L, true, new Date()));
            }

            limiter.close();

            for (int i = 0; i < 30; i++) {
                limiter.append(new Values(-4569579972474444107L, true, new Date()));
            }
        }*/

        /*config = configBuilder.withDelimiter(',').withHeader("column1", "column2", "column3", "column4")
                .withPrintStrategy(Values.class, (ApachePrintRecord<Values>) (record, printer) ->
                        printer.printRecord(record.value1, record.value2, record.value3, record.value4)).build();
        try (CsvLimiter limiter = new TimeLimiter(config, destinationFactory, 5L, TimeUnit.SECONDS)) {

            for (int i = 0; i < 20; i++) {
                limiter.append(new Values("string", 1, true, new Date()));
            }

            Thread.sleep(5500);

            for (int i = 0; i < 20; i++) {
                limiter.append(new Values("string", 1, true, new Date()));
            }
        }
    }*/

    //демонстрационный класс модели, возможно для pojo классов можно попробовать написать дефолтный обработчик на рефлекшнах
    /*private static class Values {
        Long value2;
        Boolean value3;
        Date value4;

        public Values(Long value2, Boolean value3, Date value4) {
            this.value2 = value2;
            this.value3 = value3;
            this.value4 = value4;
        }
    }*/
}
