package ru.advantum.csptm.artifact.csvsaver.apache.print;

import org.apache.commons.csv.CSVPrinter;
import ru.advantum.csptm.artifact.csvsaver.abstractions.RecordPrinter;

/**
 * Created by Leonid on 25.01.2017.
 */
public interface ApacheRecordPrinter<T> extends RecordPrinter<T, CSVPrinter> {
}
