package ru.advantum.csptm.artifact.csvsaver.limiters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.csptm.artifact.csvsaver.CsvSaver;
import ru.advantum.csptm.artifact.csvsaver.abstractions.CsvConfig;
import ru.advantum.csptm.artifact.csvsaver.abstractions.CsvWriter;
import ru.advantum.csptm.artifact.csvsaver.abstractions.DestinationFactory;
import ru.advantum.csptm.artifact.csvsaver.config.CombinedLimiterConfig;
import ru.advantum.csptm.artifact.csvsaver.config.RowLimiterConfig;
import ru.advantum.csptm.artifact.csvsaver.config.TimeLimiterConfig;

import java.util.concurrent.Executors;

public class CombinedLimiter implements Limiter {
    private static final Logger LOGGER = LoggerFactory.getLogger(CombinedLimiter.class);

    private final DestinationFactory destinationFactory;
    private final CsvConfig csvConfig;

    private long rowCount;
    private long rowLimit = Long.MAX_VALUE;

    private CsvWriter writer;

    public CombinedLimiter(
            CsvConfig csvConfig,
            DestinationFactory destinationFactory,
            CombinedLimiterConfig combinedLimiterConfig
    ) {
        this.csvConfig = csvConfig;
        this.destinationFactory = destinationFactory;

        if (combinedLimiterConfig == null) {
            return;
        }

        RowLimiterConfig rowLimiterConfig = combinedLimiterConfig.getRowLimiterConfig();
        if (rowLimiterConfig != null) {
            rowLimit = rowLimiterConfig.getLimit();
        }

        TimeLimiterConfig timeLimiterConfig = combinedLimiterConfig.getTimeLimiterConfig();
        if (timeLimiterConfig != null) {
            startTimer(timeLimiterConfig);
        }
    }

    @Override
    public synchronized <T> void append(T object) throws Exception {
        if (writer == null) {
            createWriter();
        }

        writer.append(object);
        rowCount++;

        if (rowCount > rowLimit) {
            closeWriter();
        }
    }

    @Override
    public synchronized void close() {
        closeWriter();
    }

    private void startTimer(TimeLimiterConfig timeLimiterConfig) {
        Executors
                .newSingleThreadScheduledExecutor()
                .scheduleAtFixedRate(
                        () -> {
                            synchronized (this) {
                                if (rowCount > 0) {
                                    closeWriter();
                                }
                            }
                        },
                        timeLimiterConfig.getPeriod(),
                        timeLimiterConfig.getPeriod(),
                        timeLimiterConfig.getTimeUnit()
                );
    }

    private void createWriter() throws Exception {
        rowCount = 0L;
        writer = CsvSaver.getWriter(destinationFactory.build(), csvConfig);
    }

    private void closeWriter() {
        if (writer != null) {
            try {
                writer.flush();
                writer.close();
            } catch (Exception e) {
                LOGGER.error("Writer closing exception", e);
            } finally {
                writer = null;
            }
        }
    }

}
