package ru.advantum.csptm.artifact.csvsaver.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by Leonid on 27.01.2017.
 */
@Root(name = "row-limiter")
public class RowLimiterConfig {
    @Attribute(name = "limit")
    private long limit;

    public long getLimit() {
        return limit;
    }
}
