package ru.advantum.csptm.artifact.csvsaver.apache.print;

import org.apache.commons.csv.CSVPrinter;

/**
 * Created by Leonid on 25.01.2017.
 */
public class DefaultApacheRecordPrinter implements ApacheRecordPrinter<Object> {
    @Override
    public void printRecord(Object record, CSVPrinter printer) throws Exception {
        printer.printRecord(record);
    }
}
