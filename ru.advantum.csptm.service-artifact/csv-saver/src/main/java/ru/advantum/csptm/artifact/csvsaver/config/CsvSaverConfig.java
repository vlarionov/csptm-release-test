package ru.advantum.csptm.artifact.csvsaver.config;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "csv-saver")
public class CsvSaverConfig {
    @Element(name = "file-destination")
    private TempFileDestinationConfig tempFileDestinationConfig;

    @Element(name = "csv-writer", required = false)
    private CsvWriterConfig csvWriterConfig = new CsvWriterConfig();

    @Element(name = "combined-limiter", required = false)
    private CombinedLimiterConfig combinedLimiterConfig;

    public TempFileDestinationConfig getTempFileDestinationConfig() {
        return tempFileDestinationConfig;
    }

    public CsvWriterConfig getCsvWriterConfig() {
        return csvWriterConfig;
    }

    public CombinedLimiterConfig getCombinedLimiterConfig() {
        return combinedLimiterConfig;
    }
}
