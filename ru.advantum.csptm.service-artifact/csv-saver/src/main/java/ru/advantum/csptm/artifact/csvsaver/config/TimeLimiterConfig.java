package ru.advantum.csptm.artifact.csvsaver.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

import java.util.concurrent.TimeUnit;

/**
 * Created by Leonid on 27.01.2017.
 */
@Root(name = "time-limiter")
public class TimeLimiterConfig {
    @Attribute(name = "period")
    private long period;

    @Attribute(name = "timeUnit")
    private String timeUnit;

    public long getPeriod() {
        return period;
    }

    public TimeUnit getTimeUnit() {
        return TimeUnit.valueOf(timeUnit);
    }
}
