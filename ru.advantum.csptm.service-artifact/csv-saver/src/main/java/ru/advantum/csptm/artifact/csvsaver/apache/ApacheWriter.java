package ru.advantum.csptm.artifact.csvsaver.apache;

import org.apache.commons.csv.CSVPrinter;
import ru.advantum.csptm.artifact.csvsaver.abstractions.CsvWriter;
import ru.advantum.csptm.artifact.csvsaver.abstractions.RecordPrinter;
import ru.advantum.csptm.artifact.csvsaver.apache.print.DefaultApacheRecordPrinter;

import java.util.Map;

/**
 * Created by Leonid on 24.01.2017.
 */
public class ApacheWriter extends CsvWriter {

    private final CSVPrinter printer;
    private final Map<Class, RecordPrinter> printMap;
    private final RecordPrinter defaultPrintStrategy;

    public ApacheWriter(Appendable destination, ApacheConfig config) throws Exception {
        super(destination, config);

        printer = new CSVPrinter(destination, config.getFormat());
        printMap = config.getPrintStrategyMap();


        defaultPrintStrategy = new DefaultApacheRecordPrinter();
    }

    @Override
    public <T> void append(T object) throws Exception {
        printMap.getOrDefault(object.getClass(), defaultPrintStrategy).printRecord(object, printer);
    }

    @Override
    public void flush() throws Exception {
        printer.flush();
    }

    @Override
    public void close() throws Exception {
        printer.close();
    }

    public CSVPrinter getPrinter() {
        return printer;
    }
}
