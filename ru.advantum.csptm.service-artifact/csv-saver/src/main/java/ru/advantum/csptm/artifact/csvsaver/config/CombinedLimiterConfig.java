package ru.advantum.csptm.artifact.csvsaver.config;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "combined-limiter")
public class CombinedLimiterConfig {
    @Element(name = "row-limiter", required = false)
    private RowLimiterConfig rowLimiterConfig;

    @Element(name = "time-limiter", required = false)
    private TimeLimiterConfig timeLimiterConfig;

    public RowLimiterConfig getRowLimiterConfig() {
        return rowLimiterConfig;
    }

    public TimeLimiterConfig getTimeLimiterConfig() {
        return timeLimiterConfig;
    }
}
