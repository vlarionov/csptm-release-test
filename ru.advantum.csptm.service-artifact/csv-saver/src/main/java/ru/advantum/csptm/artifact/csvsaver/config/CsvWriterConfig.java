package ru.advantum.csptm.artifact.csvsaver.config;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by Leonid on 27.01.2017.
 */
@Root(name = "csv-writer")
public class CsvWriterConfig {

    @Attribute(name = "delimiter", required = false)
    private char delimiter = ';';

    @Attribute(name = "escape", required = false)
    private char escape = '"';

    @Attribute(name = "trim", required = false)
    private boolean trimEnabled = false;

    @Attribute(name = "nullString", required = false)
    private String nullString = "";

    @Attribute(name = "recordSeparator", required = false)
    private String recordSeparator = System.lineSeparator();

    public char getDelimiter() {
        return delimiter;
    }

    public char getEscape() {
        return escape;
    }

    public boolean getTrimEnabled() {
        return trimEnabled;
    }

    public String getNullString() {
        return nullString;
    }

    public String getRecordSeparator() {
        return recordSeparator;
    }
}
