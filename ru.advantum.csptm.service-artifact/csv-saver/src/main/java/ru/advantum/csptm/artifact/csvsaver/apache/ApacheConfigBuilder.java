package ru.advantum.csptm.artifact.csvsaver.apache;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.QuoteMode;
import ru.advantum.csptm.artifact.csvsaver.abstractions.CsvConfig;
import ru.advantum.csptm.artifact.csvsaver.abstractions.CsvConfigBuilder;

/**
 * Created by Leonid on 24.01.2017.
 */
public class ApacheConfigBuilder extends CsvConfigBuilder {
    @Override
    public CsvConfig build() {
        CSVFormat format = CSVFormat.DEFAULT.withDelimiter(this.delimiter).withNullString(this.nullString)
                .withEscape(this.escape).withRecordSeparator(this.recordSeparator).withTrim(this.trimEnabled)
                .withHeader(this.header).withQuoteMode(QuoteMode.NONE);

        return new ApacheConfig(format, this.strategyMap);
    }
}
