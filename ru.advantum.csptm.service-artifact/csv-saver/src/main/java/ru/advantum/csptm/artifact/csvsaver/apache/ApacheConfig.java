package ru.advantum.csptm.artifact.csvsaver.apache;

import org.apache.commons.csv.CSVFormat;
import ru.advantum.csptm.artifact.csvsaver.abstractions.CsvConfig;
import ru.advantum.csptm.artifact.csvsaver.abstractions.RecordPrinter;

import java.util.Map;

/**
 * Created by Leonid on 24.01.2017.
 */
public class ApacheConfig implements CsvConfig {

    private final CSVFormat format;
    private final Map<Class, RecordPrinter> printMap;

    public ApacheConfig(final CSVFormat format, final Map<Class, RecordPrinter> printMap) {
        this.format = format;
        this.printMap = printMap;
    }

    @Override
    public char getDelimiter() {
        return format.getDelimiter();
    }

    @Override
    public char getEscape() {
        return format.getEscapeCharacter();
    }

    @Override
    public String[] getHeader() {
        return format.getHeader();
    }

    @Override
    public boolean getTrimEnabled() {
        return format.getTrim();
    }

    @Override
    public String getNullString() {
        return format.getNullString();
    }

    @Override
    public String getRecordSeparator() {
        return format.getRecordSeparator();
    }

    @Override
    public Map<Class, RecordPrinter> getPrintStrategyMap() {
        return printMap;
    }

    public CSVFormat getFormat() {
        return format;
    }
}
