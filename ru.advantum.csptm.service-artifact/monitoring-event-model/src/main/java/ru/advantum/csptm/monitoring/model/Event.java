package ru.advantum.csptm.monitoring.model;

import java.time.Instant;

/**
 * Created by kukushkin on 04.04.2017.
 */
public class Event {
    private final long eventId;
    private final Instant eventTime;
    private final long trId;
    private final long unitId;
    private final short eventType;
    private final Long factValue;
    private final Long deviationValue;
    private final Float lon;
    private final Float lat;
    private final short eventStatusId;

    public Event(long eventId, Instant eventTime, long trId, long unitId, short eventType, Long factValue, Long deviationValue, Float lon, Float lat, short eventStatusId) {
        this.eventId = eventId;
        this.eventTime = eventTime;
        this.trId = trId;
        this.unitId = unitId;
        this.eventType = eventType;
        this.factValue = factValue;
        this.deviationValue = deviationValue;
        this.lon = lon;
        this.lat = lat;
        this.eventStatusId = eventStatusId;
    }


    public long getEventId() {
        return eventId;
    }

    public Instant getEventTime() {
        return eventTime;
    }

    public long getTrId() {
        return trId;
    }

    public long getUnitId() {
        return unitId;
    }

    public short getEventType() {
        return eventType;
    }

    public Long getFactValue() {
        return factValue;
    }

    public Long getDeviationValue() {
        return deviationValue;
    }

    public Float getLon() {
        return lon;
    }

    public Float getLat() {
        return lat;
    }

    public String toString() {
        return this.eventId + " " + eventTime.toString();
    }

    public short getEventStatusId() {
        return eventStatusId;
    }
}
