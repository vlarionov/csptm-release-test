package ru.advantum.csptm.monitoring.model;

import java.time.Instant;

/**
 * Created by kukushkin on 05.04.2017.
 */
public class Alarm {
    private final long eventId;
    private final Instant eventTime;

    public Alarm(long eventId, Instant eventTime) {
        this.eventId = eventId;
        this.eventTime = eventTime;
    }

    public Instant getEventTime() {
        return eventTime;
    }

    public long getEventId() {
        return eventId;
    }
}
