package ru.advantum.csptm.monitoring.model;

/**
 * Created by kukushkin on 06.04.2017.
 */
public enum AlarmLevel implements Comparable<AlarmLevel> {
    CRITICAL(1),
    WARNING(2),
    INFO(3),
    UNDEFINED(0);

    private short id;

    AlarmLevel(int id) {
        this.id = (short) id;
    }

    public short getId() {
        return id;
    }

    public static AlarmLevel find(short id) {
        for (AlarmLevel level : AlarmLevel.values()) {
            if (level.id == id) return level;
        }
        throw new IllegalArgumentException("Unknown AlarmLevel, id: " + id);
    }

}
