package ru.advantum.csptm.monitoring.model;

import java.time.Instant;

/**
 * Created by kukushkin on 10.04.2017.
 */
public class AlarmConfirmation {
    private final long trId;
    private final Instant lastUnconfirmedAlarmTime;
    private final short eventTypeId;

    public AlarmConfirmation(long trId, Instant lastUnconfirmedAlarmTime, short eventTypeId) {
        this.trId = trId;
        this.lastUnconfirmedAlarmTime = lastUnconfirmedAlarmTime;
        this.eventTypeId = eventTypeId;
    }

    public long getTrId() {
        return trId;
    }

    public Instant getLastUnconfirmedAlarmTime() {
        return lastUnconfirmedAlarmTime;
    }

    public short getEventTypeId() {
        return eventTypeId;
    }
}
