package ru.advantum.csptm.monitoring.model;

/**
 * Created by kukushkin on 05.04.2017.
 */
public enum EventType {
    RED_BUTTON(1),
    OVER_SPEED(2),
    TOO_FAR(3),
    AUTONOMIC_MOVE(4),
    REVERSE_MOVE(5),
    SMOKE(6),
    VIDEO_LOSS(7),
    HDD_FULL(8),
    NO_DISK(9),
    ALL_VIDEO_LOSS(10),
    FAN_FAIL(11),
    OVER_HDD_TEMPERATURE(12),
    HDD_FAIL(13),
    UNIT_DISABLED_BY_INVALID_PACKAGES_NUMBERS(14),
    UNIT_DISABLED_BY_INVALID_PACKAGES_RATE(15),
    UNIT_DISABLED_BY_PACKAGES_TIME_RATE(16),
    OK(17);

    private final short type;

    EventType(short type) {
        this.type = type;
    }

    EventType(int type) {
        this((short) type);
    }

    public short getType() {
        return type;
    }

    public static EventType getType(short type) {
        for (EventType t : EventType.values()) {
            if (t.type == type) return t;
        }
        throw new IllegalArgumentException("unnown Event Type" + type);
    }

}
