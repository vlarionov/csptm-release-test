package ru.advantum.csptm.monitoring.model;

/**
 * Created by kukushkin on 11.04.2017.
 */
public enum EventStatus {
    NEW((short)20),
    PROSESSED((short)80);
    private short status;

    EventStatus(short status) {
        this.status = status;
    }

    public short getStatus() {
        return status;
    }

}
