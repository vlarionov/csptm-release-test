package ru.advantum.csptm.monitoring.model;

/**
 * Created by kukushkin on 05.04.2017.
 */
public enum  EventMessageType {
    EVENT, ALARM;
}
