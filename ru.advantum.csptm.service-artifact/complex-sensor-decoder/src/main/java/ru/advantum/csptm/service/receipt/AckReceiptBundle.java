package ru.advantum.csptm.service.receipt;

import java.util.Arrays;
import java.util.List;

public class AckReceiptBundle {

    private final int serviceId;
    private final List<AckReceipt> receipts;

    public static AckReceiptBundle of(int hubId, AckReceipt... receipts) {
        return new AckReceiptBundle(hubId, Arrays.asList(receipts));
    }

    public static AckReceiptBundle of(int hubId, List<AckReceipt> receipts) {
        return new AckReceiptBundle(hubId, receipts);
    }

    private AckReceiptBundle(int hubId, List<AckReceipt> receipts) {
        this.serviceId = hubId;
        this.receipts = receipts;
    }

    public int getServiceId() {
        return serviceId;
    }

    public List<AckReceipt> getReceipts() {
        return receipts;
    }

    public int getHubId() {
        return getServiceId();
    }

    public String toString() {
        return "AckReceiptBundle{serviceId=" + this.serviceId + ", receipts=" + this.receipts + '}';
    }

}
