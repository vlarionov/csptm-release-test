package ru.advantum.csptm.service.csd;

import ru.advantum.csptm.jep.proto.sensors.Type;

import java.math.BigDecimal;

public class AnalogSensor extends Sensor {

    private final BigDecimal value;

    public AnalogSensor(int number, BigDecimal value) {
        super(Type.ANALOG, number);
        this.value = value;
    }

    public BigDecimal getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "AnalogSensor:{" +
                "number=" + getNumber() +
                ", value=" + getValue() +
                "}";
    }

}
