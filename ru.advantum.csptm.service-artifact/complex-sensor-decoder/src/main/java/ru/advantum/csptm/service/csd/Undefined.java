package ru.advantum.csptm.service.csd;

import ru.advantum.csptm.jep.proto.sensors.Type;

public class Undefined extends Sensor {
    protected Undefined() {
        super(Type.UNKNOWN, -1);
    }
}
