package ru.advantum.csptm.service.csd;

import ru.advantum.csptm.jep.proto.sensors.ComplexSensorHolder;
import ru.advantum.csptm.jep.proto.sensors.Type;
import ru.advantum.csptm.jep.proto.util.BitIterable;

import java.math.BigDecimal;
import java.util.Map;
import java.util.TreeMap;

public class ComplexSensorProcessor {

    public static Map<Integer, ComplexSensorHolder> decode(Type type, int first, Map<Integer, BigDecimal> sensorData) {
        Map<Integer, ComplexSensorHolder> smap = new TreeMap<>();

        Map<Integer, Map<Short, BigDecimal>> sensors = new TreeMap<>();
        Map<Short, BigDecimal> values = new TreeMap<>();
        int highind = first;
        short lowind = 0;

        for (Map.Entry<Integer, BigDecimal> e : sensorData.entrySet()) {
            values.put(lowind++, e.getValue());

            if (lowind == type.getMaxArraySize()) {
                sensors.put(highind++, values);
                values = new TreeMap<>();
                lowind = 0;
            }
        }

        for (Map.Entry<Integer, Map<Short, BigDecimal>> e : sensors.entrySet()) {
            smap.put(e.getKey(),
                    new ComplexSensorHolder(type,
                            e.getValue().values().toArray(
                                    new BigDecimal[e.getValue().values().size()]
                            )
                    )
            );
        }
        return smap;
    }

    public static Map<Integer, ComplexSensorHolder> decode(int index, int size, BigDecimal sensorData) {
        Map<Integer, ComplexSensorHolder> smap = new TreeMap<>();
        for (Boolean b : new BitIterable(size, sensorData.longValue())) {
            smap.put(index, new ComplexSensorHolder(Type.BINARY, b ? 1 : 0));
            index++;
        }
        return smap;
    }

    public static Map<Integer, ComplexSensorHolder> decode(int index, BigDecimal... sensorData) {
        Map<Integer, ComplexSensorHolder> smap = new TreeMap<>();
        for (BigDecimal bigDecimal : sensorData) {
            smap.put(index, new ComplexSensorHolder(Type.ANALOG, bigDecimal));
            index++;
        }
        return smap;
    }

    public static Map<Integer, ComplexSensorHolder> decode(int number, boolean b) {
        Map<Integer, ComplexSensorHolder> smap = new TreeMap<>();
        smap.put(number, new ComplexSensorHolder(Type.BINARY, b ? 1 : 0));
        return smap;
    }

    public static Map<Integer, ComplexSensorHolder> decode(int first, Map<Integer, BigDecimal> sensors) {
        return decode(first, Type.EXTENDED, sensors);
    }

    public static Map<Integer, ComplexSensorHolder> decode(int first, Type type, Map<Integer, BigDecimal> sensors) {
        Map<Integer, ComplexSensorHolder> smap = new TreeMap<>();
        smap.put(first, new ComplexSensorHolder(type, sensors.values().toArray(new BigDecimal[sensors.size()])));
        return smap;
    }

    public static Sensor build(int number, ComplexSensorHolder sensor) {
        if (sensor.getType() == Type.BINARY) {
            return new BinarySensor(number, sensor.getValues()[0]);

        } else if (sensor.getType() == Type.FUEL) {
            return new FuelSensor(number, sensor.getValues());

        } else if (sensor.getType() == Type.EXTENDED) {
            return new ExtendedData(number, sensor.getValues());

        } else if (sensor.getType() == Type.IRMA) {
            return new Irma(number, sensor.getValues());

        } else if (sensor.getType() == Type.TEMPERATURE) {
            return new Temperature(number, sensor.getValues());

        } else if (sensor.getType() == Type.ANALOG) {
            return new AnalogSensor(number, sensor.getValues()[0]);

        }
        return new Undefined();
    }
}
