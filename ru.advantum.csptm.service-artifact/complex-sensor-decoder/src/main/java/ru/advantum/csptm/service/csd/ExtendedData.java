package ru.advantum.csptm.service.csd;

import ru.advantum.csptm.jep.proto.sensors.Type;

import java.math.BigDecimal;
import java.util.Map;
import java.util.TreeMap;

public class ExtendedData extends Sensor {
    public static final int ODOMETER = 1;
    public static final int ALT = ODOMETER + 1;
    public static final int VALIDITY = ODOMETER + 2;
    public static final int LON_HSPH = ODOMETER + 3;
    public static final int LAT_HSPH = ODOMETER + 4;
    public static final int IBATT = ODOMETER + 5;
    public static final int FST_SWON = ODOMETER + 6;
    public static final int SOS = ODOMETER + 7;
    public static final int ALARM = ODOMETER + 8;
    public static final int CALL_REQ = ODOMETER + 9;
    public static final int BATT_V = ODOMETER + 10;
    public static final int MAX_SPD = ODOMETER + 11;
    public static final int SATS = ODOMETER + 12;
    public static final int TRACK = ODOMETER + 13;
    public static final int GSM_LVL = ODOMETER + 14;
    public static final int GPRS_STATE = ODOMETER + 15;
    public static final int ACC_ENGY = ODOMETER + 16;

    private final long odometer;
    private final int alt;
    private final boolean validity;
    private final boolean lonHalfSphere;
    private final boolean latHalfSphere;
    private final boolean internalBattery;
    private final boolean firstSwitchOn;
    private final boolean sos;
    private final boolean alarm;
    private final boolean callRequest;
    private final int batteryVoltage;
    private final int maxSpeedPeriod;
    private final int satellitesCount;
    private final int trackLength;
    private final int gsmLevel;
    private final int gprsState;
    private final int accelEnergy;

    public ExtendedData(int number, BigDecimal[] values) {
        super(Type.EXTENDED, number);
        this.odometer = values[ODOMETER - 1].longValue();
        this.alt = values[ALT - 1].intValue();
        this.validity = values[VALIDITY - 1].intValue() == 1;
        this.lonHalfSphere = values[LON_HSPH - 1].intValue() == 1;
        this.latHalfSphere = values[LAT_HSPH - 1].intValue() == 1;
        this.internalBattery = values[IBATT - 1].intValue() == 1;
        this.firstSwitchOn = values[FST_SWON - 1].intValue() == 1;
        this.sos = values[SOS - 1].intValue() == 1;
        this.alarm = values[ALARM - 1].intValue() == 1;
        this.callRequest = values[CALL_REQ - 1].intValue() == 1;
        this.batteryVoltage = values[BATT_V - 1].intValue();
        this.maxSpeedPeriod = values[MAX_SPD - 1].intValue();
        this.satellitesCount = values[SATS - 1].intValue();

        this.trackLength = values[TRACK - 1].intValue();
        this.gsmLevel = values[GSM_LVL - 1].intValue();
        this.gprsState = values[GPRS_STATE - 1].intValue();
        this.accelEnergy = values[ACC_ENGY - 1].intValue();
    }

    public static Map<Integer, BigDecimal> getCleanExtendedArray() {
        Map<Integer, BigDecimal> extendedSensor = new TreeMap<>();
        for (int i = 1; i < ACC_ENGY + 1; i++) {
            extendedSensor.put(i, new BigDecimal(0));
        }
        return extendedSensor;
    }

    public long getOdometer() {
        return odometer;
    }

    public int getAlt() {
        return alt;
    }

    public boolean isValidity() {
        return validity;
    }

    public boolean isLonHalfSphere() {
        return lonHalfSphere;
    }

    public boolean isLatHalfSphere() {
        return latHalfSphere;
    }

    public boolean isInternalBattery() {
        return internalBattery;
    }

    public boolean isFirstSwitchOn() {
        return firstSwitchOn;
    }

    public boolean isSos() {
        return sos;
    }

    public boolean isAlarm() {
        return alarm;
    }

    public boolean isCallRequest() {
        return callRequest;
    }

    public int getBatteryVoltage() {
        return batteryVoltage;
    }

    public int getMaxSpeedPeriod() {
        return maxSpeedPeriod;
    }

    public int getSatellitesCount() {
        return satellitesCount;
    }

    public int getTrackLength() {
        return trackLength;
    }

    public int getGsmLevel() {
        return gsmLevel;
    }

    public int getGprsState() {
        return gprsState;
    }

    public int getAccelEnergy() {
        return accelEnergy;
    }

    @Override
    public String toString() {
        return "ExtendedData{" +
                "odometer=" + odometer +
                ", alt=" + alt +
                ", validity=" + validity +
                ", lonHalfSphere=" + lonHalfSphere +
                ", latHalfSphere=" + latHalfSphere +
                ", internalBattery=" + internalBattery +
                ", firstSwitchOn=" + firstSwitchOn +
                ", sos=" + sos +
                ", alarm=" + alarm +
                ", callRequest=" + callRequest +
                ", batteryVoltage=" + batteryVoltage +
                ", maxSpeedPeriod=" + maxSpeedPeriod +
                ", satellitesCount=" + satellitesCount +
                ", trackLength=" + trackLength +
                ", gsmLevel=" + gsmLevel +
                ", gprsState=" + gprsState +
                ", accelEnergy=" + accelEnergy +
                '}';
    }
}
