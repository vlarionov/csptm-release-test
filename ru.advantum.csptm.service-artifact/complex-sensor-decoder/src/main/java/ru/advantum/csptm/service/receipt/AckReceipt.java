package ru.advantum.csptm.service.receipt;

import java.io.Serializable;
import java.time.Instant;

public class AckReceipt implements Comparable<AckReceipt>, Serializable {

    private final long packetId;
    private final short serviceId;
    private final Instant dateTime;

    public AckReceipt(long packetId, short serviceId) {
        this.packetId = packetId;
        this.serviceId = serviceId;
        this.dateTime = Instant.now();
    }

    public Long getPacketId() {
        return packetId;
    }

    public Short getServiceId() {
        return serviceId;
    }

    public Instant getDateTime() {
        return dateTime;
    }

    @Override
    public int compareTo(AckReceipt ackReceipt) {
        return Long.compare(getPacketId(), ackReceipt.getPacketId());
    }
}
