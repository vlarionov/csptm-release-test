package ru.advantum.csptm.service.csd;

import ru.advantum.csptm.jep.proto.sensors.Type;

import java.math.BigDecimal;

public class BinarySensor extends Sensor {

    private final BigDecimal value;

    public BinarySensor(int number, BigDecimal value) {
        super(Type.BINARY, number);
        this.value = value;
    }

    public BigDecimal getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BinarySensor:{" +
                "number=" + getNumber() +
                ", value=" + getValue() +
                "}";
    }

}
