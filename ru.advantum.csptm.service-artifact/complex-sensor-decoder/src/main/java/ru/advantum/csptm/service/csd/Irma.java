package ru.advantum.csptm.service.csd;

import ru.advantum.csptm.jep.proto.sensors.Type;

import java.math.BigDecimal;

public class Irma extends Sensor {
    private final static int BASE_OFF = -1;
    private final static int IRMA_ODOMETER = BASE_OFF + 1;
    private final static int IRMA_ZONE = BASE_OFF + 2;
    private final static int IRMA_DOOR_IN_1 = BASE_OFF + 3;
    private final static int IRMA_DOOR_IN_2 = BASE_OFF + 4;
    private final static int IRMA_DOOR_IN_3 = BASE_OFF + 5;
    private final static int IRMA_DOOR_IN_4 = BASE_OFF + 6;
    private final static int IRMA_DOOR_OUT_1 = BASE_OFF + 7;
    private final static int IRMA_DOOR_OUT_2 = BASE_OFF + 8;
    private final static int IRMA_DOOR_OUT_3 = BASE_OFF + 9;
    private final static int IRMA_DOOR_OUT_4 = BASE_OFF + 10;
    private final static int IRMA_PRESENT_DOOR_1 = BASE_OFF + 11;
    private final static int IRMA_PRESENT_DOOR_2 = BASE_OFF + 12;
    private final static int IRMA_PRESENT_DOOR_3 = BASE_OFF + 13;
    private final static int IRMA_PRESENT_DOOR_4 = BASE_OFF + 14;
    private final static int IRMA_CLOSED_DOOR_1 = BASE_OFF + 15;
    private final static int IRMA_CLOSED_DOOR_2 = BASE_OFF + 16;
    private final static int IRMA_CLOSED_DOOR_3 = BASE_OFF + 17;
    private final static int IRMA_CLOSED_DOOR_4 = BASE_OFF + 18;

    private final long odometer;
    private final int zone;
    private final int irma_door_in1;
    private final int irma_door_in2;
    private final int irma_door_in3;
    private final int irma_door_in4;
    private final int irma_door_out1;
    private final int irma_door_out2;
    private final int irma_door_out3;
    private final int irma_door_out4;
    private final boolean irma_present_door1;
    private final boolean irma_present_door2;
    private final boolean irma_present_door3;
    private final boolean irma_present_door4;
    private final boolean irma_closed_door1;
    private final boolean irma_closed_door2;
    private final boolean irma_closed_door3;
    private final boolean irma_closed_door4;


    public Irma(int number, BigDecimal[] values) {
        super(Type.IRMA, number);
        this.odometer = values[IRMA_ODOMETER].longValue();
        this.zone = values[IRMA_ZONE].intValue();
        this.irma_door_in1 = values[IRMA_DOOR_IN_1].intValue();
        this.irma_door_in2 = values[IRMA_DOOR_IN_2].intValue();
        this.irma_door_in3 = values[IRMA_DOOR_IN_3].intValue();
        this.irma_door_in4 = values[IRMA_DOOR_IN_4].intValue();
        this.irma_door_out1 = values[IRMA_DOOR_OUT_1].intValue();
        this.irma_door_out2 = values[IRMA_DOOR_OUT_2].intValue();
        this.irma_door_out3 = values[IRMA_DOOR_OUT_3].intValue();
        this.irma_door_out4 = values[IRMA_DOOR_OUT_4].intValue();
        this.irma_present_door1 = values[IRMA_PRESENT_DOOR_1].intValue() == 1;
        this.irma_present_door2 = values[IRMA_PRESENT_DOOR_2].intValue() == 1;
        this.irma_present_door3 = values[IRMA_PRESENT_DOOR_3].intValue() == 1;
        this.irma_present_door4 = values[IRMA_PRESENT_DOOR_4].intValue() == 1;
        this.irma_closed_door1 = values[IRMA_CLOSED_DOOR_1].intValue() == 1;
        this.irma_closed_door2 = values[IRMA_CLOSED_DOOR_2].intValue() == 1;
        this.irma_closed_door3 = values[IRMA_CLOSED_DOOR_3].intValue() == 1;
        this.irma_closed_door4 = values[IRMA_CLOSED_DOOR_4].intValue() == 1;
    }

    public Long getOdometer() {
        return odometer;
    }

    public Integer getZone() {
        return zone;
    }

    public int getIrma_door_in1() {
        return irma_door_in1;
    }

    public int getIrma_door_in2() {
        return irma_door_in2;
    }

    public int getIrma_door_in3() {
        return irma_door_in3;
    }

    public int getIrma_door_in4() {
        return irma_door_in4;
    }

    public int getIrma_door_out1() {
        return irma_door_out1;
    }

    public int getIrma_door_out2() {
        return irma_door_out2;
    }

    public int getIrma_door_out3() {
        return irma_door_out3;
    }

    public int getIrma_door_out4() {
        return irma_door_out4;
    }

    public boolean isIrma_present_door1() {
        return irma_present_door1;
    }

    public boolean isIrma_present_door2() {
        return irma_present_door2;
    }

    public boolean isIrma_present_door3() {
        return irma_present_door3;
    }

    public boolean isIrma_present_door4() {
        return irma_present_door4;
    }

    public boolean isIrma_closed_door1() {
        return irma_closed_door1;
    }

    public boolean isIrma_closed_door2() {
        return irma_closed_door2;
    }

    public boolean isIrma_closed_door3() {
        return irma_closed_door3;
    }

    public boolean isIrma_closed_door4() {
        return irma_closed_door4;
    }

    @Override
    public String toString() {
        return "Irma: {"
                + "odometer=" + odometer
                + ", zone=" + zone
                + ", irma_door_in1=" + irma_door_in1
                + ", irma_door_in2=" + irma_door_in2
                + ", irma_door_in3=" + irma_door_in3
                + ", irma_door_in4=" + irma_door_in4
                + ", irma_door_out1=" + irma_door_out1
                + ", irma_door_out2=" + irma_door_out2
                + ", irma_door_out3=" + irma_door_out3
                + ", irma_door_out4=" + irma_door_out4
                + ", irma_present_door1=" + irma_present_door1
                + ", irma_present_door2=" + irma_present_door2
                + ", irma_present_door3=" + irma_present_door3
                + ", irma_present_door4=" + irma_present_door4
                + ", irma_closed_door1=" + irma_closed_door1
                + ", irma_closed_door2=" + irma_closed_door2
                + ", irma_closed_door3=" + irma_closed_door3
                + ", irma_closed_door4=" + irma_closed_door4
                + '}';
    }

}