package ru.advantum.csptm.service.csd;

import ru.advantum.csptm.jep.proto.sensors.Type;

public abstract class Sensor implements Comparable<Sensor> {

    private final Type type;
    private final int number;

    protected Sensor(Type type, int number) {
        this.type = type;
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public Type getType() {
        return type;
    }

    @Override
    public int compareTo(Sensor sensor) {
        return Integer.compare(getNumber(), sensor.getNumber());
    }

}
