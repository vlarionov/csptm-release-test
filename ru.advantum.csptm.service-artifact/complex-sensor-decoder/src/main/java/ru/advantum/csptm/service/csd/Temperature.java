package ru.advantum.csptm.service.csd;

import ru.advantum.csptm.jep.proto.sensors.Type;

import java.math.BigDecimal;

public class Temperature extends Sensor {

    private final int enabled;
    private final int value;

    public Temperature(int number, BigDecimal[] values) {
        super(Type.TEMPERATURE, number);
        this.enabled = values[0].intValue();
        this.value = values[1].intValue();
    }

    @Deprecated
    public boolean isEnabled() {
        return enabled == 0;
    }

    public int getStatus() {
        return enabled;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Temperature{" +
                "enabled=" + enabled +
                ", value=" + value +
                '}';
    }
}
