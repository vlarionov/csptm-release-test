package ru.advantum.csptm.service.csd;

import ru.advantum.csptm.jep.proto.sensors.Type;

import java.math.BigDecimal;

public class FuelSensor extends Sensor {

    private final boolean ok;
    private final int levelMM;
    private final int levelL;
    private final byte temperature;

    public FuelSensor(int number, BigDecimal[] values) {
        super(Type.FUEL, number);
        ok = values[0].intValue() == 0;
        levelMM = values[1].intValue();
        levelL = values[2].intValue();
        temperature = values[3].byteValue();
    }

    public boolean isOk() {
        return ok;
    }

    public int getLevelMM() {
        return levelMM;
    }

    public int getLevelL() {
        return levelL;
    }

    public byte getTemperature() {
        return temperature;
    }

    @Override
    public String toString() {
        return "FuelSensor{" +
                "ok=" + ok +
                ", levelMM=" + levelMM +
                ", levelL=" + levelL +
                ", temperature=" + temperature +
                '}';
    }
}
