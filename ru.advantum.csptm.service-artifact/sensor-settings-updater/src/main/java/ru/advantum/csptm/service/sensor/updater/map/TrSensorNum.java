package ru.advantum.csptm.service.sensor.updater.map;

/**
 * Created by kukushkin on 5/15/17.
 */
public class TrSensorNum {
    private final long unitId;
    private final long num;

    public TrSensorNum(long trId, long num) {
        this.unitId = trId;
        this.num = num;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TrSensorNum that = (TrSensorNum) o;

        if (unitId != that.unitId) return false;
        return num == that.num;
    }

    @Override
    public int hashCode() {
        int result = (int) (unitId ^ (unitId >>> 32));
        result = 31 * result + (int) (num ^ (num >>> 32));
        return result;
    }
}
