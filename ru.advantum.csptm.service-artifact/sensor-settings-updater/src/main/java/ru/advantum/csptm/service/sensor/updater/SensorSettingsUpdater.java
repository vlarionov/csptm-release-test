package ru.advantum.csptm.service.sensor.updater;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.config.common.db.DatabaseConnection;
import ru.advantum.csptm.service.sensor.updater.map.SensorAttrib;
import ru.advantum.csptm.service.sensor.updater.map.TrSensorNum;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * Created by kukushkin on 5/16/17.
 */
public class SensorSettingsUpdater {

    private static final Logger LOGGER = LoggerFactory.getLogger(SensorSettingsUpdater.class);
    private final ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();
    private static final AtomicReference<Map<TrSensorNum, SensorAttrib>> trSensorMapping = new AtomicReference<>(new HashMap<>());

    public SensorSettingsUpdater() {

    }

    public void start(DatabaseConnection config) throws SQLException {
        SqlMapperSingleton.setConfiguration(config.getConnectionPool());
        updateTrSensor();
        ses.scheduleAtFixedRate(this::updateTrSensor, 60, 60, TimeUnit.SECONDS);
    }


    private void updateTrSensor() {
        try (SqlSession session = SqlMapperSingleton.getInstance().openSession()) {
            LOGGER.info("Updating tr sensor mapping");
            TrSensorMapper mapper = session.getMapper(TrSensorMapper.class);
            trSensorMapping.set(mapper.getTrSensor().stream().collect(Collectors.toMap(utl -> new TrSensorNum(utl.unitId, utl.sensorNum), utl -> new SensorAttrib(utl.equipmentTypeId, utl.sensorId))));

            LOGGER.info("Updated size [{}]", trSensorMapping.get().size());
        } catch (Throwable ex) {
            LOGGER.error("update tr sensor mapping", ex);
        }
    }

    public AtomicReference<Map<TrSensorNum, SensorAttrib>> getTrSensorMapping() {
        return trSensorMapping;
    }
}
