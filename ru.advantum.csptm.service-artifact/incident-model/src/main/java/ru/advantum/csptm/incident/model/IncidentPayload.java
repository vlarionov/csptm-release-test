package ru.advantum.csptm.incident.model;

import lombok.Builder;
import lombok.Data;
import java.util.List;

/**
 * Created by kukushkin on 10/30/17.
 */
@Builder
@Data
public class IncidentPayload {
    private List<Long> trList;
    private List<Integer> driverList;
    private List<Integer> routeVarList;
    private List<Integer> stopItemList;
    private List<Integer> orderList;
    private List<Integer> ttActionList;
    private List<Integer> ttOutList;
    private List<Integer> ttVariantList;
    private List<Long> unitList;
    private Point pointStart;
    private Point pointFinish;
    private Long factVal;
    private Long deviationVal;
    private Integer incidentReason;
    private String incommingMessage;
    private String incommingMessageId;
}
