package ru.advantum.csptm.incident.model;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.util.List;

/**
 * Created by kukushkin on 11/2/17.
 */
@Builder
@Data
public class IncidentBundle {
    @NonNull
    private List<Incident> incidentBundle;
}
