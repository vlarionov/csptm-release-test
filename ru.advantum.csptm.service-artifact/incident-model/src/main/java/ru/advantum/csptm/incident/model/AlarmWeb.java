package ru.advantum.csptm.incident.model;
/**
 * Created by kukushkin on 07.04.2017.
 */
public class AlarmWeb {
    public static final String TR_ALARM_MAP_NAME = "tr-alarm";
    private final short level;
    private final String name;
    private final boolean isConfirm;
    private final short type;
    private final int priority;

    public AlarmWeb(short level, String name, boolean isConfirm, short type, int priority) {
        this.level = level;
        this.name = name;
        this.isConfirm = isConfirm;
        this.type = type;
        this.priority = priority;
    }

    public AlarmWeb(short level, String name, boolean isConfirm, IncidentType.TYPE type, int priority) {
        this(level, name, isConfirm, type.get(), priority);
    }

    public short getLevel() {
        return level;
    }

    public String getName() {
        return name;
    }

    public boolean isConfirm() {
        return isConfirm;
    }

    public short getType() {
        return type;
    }

    public int getPriority() {
        return priority;
    }
}
