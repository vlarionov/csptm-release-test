package ru.advantum.csptm.incident.model;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Created by kukushkin on 11/1/17.
 */
@RequiredArgsConstructor(staticName = "of")
public class Point {
    @NonNull
    private final Float lon;
    @NonNull
    private final Float lat;
}
