package ru.advantum.csptm.incident.model;

import lombok.Data;
import lombok.NonNull;

/**
 * Created by kukushkin on 11/9/17.
 */
@Data
public class IncidentType {
    public enum TYPE {
        RED_BUTTON(1),
        OVER_SPEED(2),
        TOO_FAR(3),
        AUTONOMIC_MOVE(4),
        REVERSE_MOVE(5),
        SMOKE(6),
        VIDEO_LOSS(7),
        HDD_FULL(8),
        NO_DISK(9),
        ALL_VIDEO_LOSS(10),
        FAN_FAIL(11),
        OVER_HDD_TEMPERATURE(12),
        HDD_FAIL(13),
        UNIT_DISABLED_BY_INVALID_PACKAGES_NUMBERS(14),
        UNIT_DISABLED_BY_INVALID_PACKAGES_RATE(15),
        UNIT_DISABLED_BY_PACKAGES_TIME_RATE(16),
        OK(17),
        TIMETABLE_VIOLATE(18),
        TIMETABLE_UPDATE(19),
        INTERVAL_VIOLATE(20),
        VOICE_CALL(21),
        INCOMMING_MESSAGE(22),
        COMING_OFF(23),
        MANUAL(24),
        CONGESTION(25),
        LATE_FROM_DEPO(26),
        MODE_VIOLATION(27),
        ORDER_SHORTAGE(28),
        ROUND_START_FINISH_DEVIATION(29),
        CABIN_TERMO_DEVIATION(30),
        SALON_TERMO_DEVIATION(31);

        private final short type;

        TYPE(short type) {
            this.type = type;
        }

        TYPE(int type) {
            this((short) type);
        }

        public short get() {
            return type;
        }

        public static TYPE getType(short type) {
            for (TYPE t : TYPE.values()) {
                if (t.type == type) return t;
            }
            throw new IllegalArgumentException("unnown Event Type" + type);
        }
    }
    @NonNull
    private Integer id;
    @NonNull
    private String name;
    @NonNull
    private String shortName;
    @NonNull
    private Boolean isAlarm;

}
