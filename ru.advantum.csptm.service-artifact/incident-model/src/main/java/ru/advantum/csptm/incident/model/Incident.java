package ru.advantum.csptm.incident.model;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.ToString;

import java.time.Instant;

/**
 * Created by kukushkin on 10/30/17.
 */
@Builder
@Data
@ToString
public class Incident {
    public final static String INCIDENT_ROUTER_EXCHANGE = "incident_router";

    public enum STATUS {
        NEW((short)20),
        PROSESSED((short)80);
        private short status;

        STATUS(short status) {
            this.status = status;
        }

        public short get() {
            return status;
        }

    }

    public enum INITIATOR {
        SYSTEM((short)0);

        private short initiator;

        INITIATOR(short initiator) {
            this.initiator = initiator;
        }

        public short get() {
            return initiator;
        }

    }


    public enum HANDLER {
        SAVE_TO_DB (1),
        USER_INFO(2),
        SERVER_FUNCTION(3);

        private int handler;

        HANDLER(int handler) {
            this.handler = handler;
        }

        public int get() {
            return handler;
        }

        public static HANDLER getHandler(int handler) {
            for (HANDLER t : HANDLER.values()) {
                if (t.handler == handler) return t;
            }
            throw new IllegalArgumentException("unnown handler" + handler);
        }
    }

    private Long incidentId;
    @NonNull
    private Short incidentInitiatorId;
    @NonNull
    private Short incidentStatusId;
    @NonNull
    private Short incidentTypeId;
    @NonNull
    private Instant timeStart;
    private Instant timeFinish;
    private Long parentIncidentId;
    private Integer accountId;
    private Long trId;
    private Long unitId;
    private IncidentPayload incidentPayload;
    private Boolean isAlarmOn = Boolean.TRUE;
}
