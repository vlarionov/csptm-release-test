package ru.advantum.csptm.incident.model;

import java.time.Instant;

/**
 * Created by kukushkin on 11.04.2017.
 */
import com.google.gson.annotations.SerializedName;
public class LastAlarm {
    private  final IncidentType incidentType;
    private final Instant lastTime;
    private final boolean isConfirm;

    public LastAlarm(Instant lastTime, IncidentType incidentType, boolean isConfirm) {
        this.lastTime = lastTime;
        this.incidentType = incidentType;
        this.isConfirm = isConfirm;
    }


    public IncidentType getEventType() {
        return incidentType;
    }

    public Instant getLastTime() {
        return lastTime;
    }

    public boolean isConfirm() {
        return isConfirm;
    }
}
