package ru.advantum.csptm.incident.model;

import lombok.Data;

/**
 * Created by kukushkin on 2/15/18.
 */
@Data
public class TtVariantCurrent {
    public static final TtVariantCurrent EMPTY_TT_VARIANT_CURRENT = new TtVariantCurrent(null, null, null);

    private final Integer ttVariantId;
    private final String ttVariantName;
    private final Integer routeId;
    private Long trId;
    private Integer drivertId;
    private String driverName;
    private String outNum;
    private String shiftNum;
}
