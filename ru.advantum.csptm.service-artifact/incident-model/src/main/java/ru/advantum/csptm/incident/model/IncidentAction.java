package ru.advantum.csptm.incident.model;

import lombok.Data;
import lombok.NonNull;

/**
 * Created by kukushkin on 11/1/17.
 */
@Data
public class IncidentAction {
    @NonNull
    private Integer incidentActionId;
    @NonNull
    private Integer incidentTypeId;
    @NonNull
    private Integer incidentHandlerId;
    @NonNull
    private Integer orderNum;
    @NonNull
    private String moduleName;

}
