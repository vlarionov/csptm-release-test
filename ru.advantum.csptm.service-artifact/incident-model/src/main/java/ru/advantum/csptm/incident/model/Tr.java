package ru.advantum.csptm.incident.model;

import lombok.Data;

/**
 * Created by kukushkin on 11/9/17.
 */
@Data
public class Tr {
    public static final Tr EMPTY_TR = new Tr(0L, "", "", null);
    private final Long trId;
    private final String licence;
    private final String garageNum;
    private final String trTypeName;
}
