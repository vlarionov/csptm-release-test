package ru.advantum.csptm.service.sms.model;

/**
 * Статус SMS сообщения
 *
 * @author abc
 */
public enum SmsStatus {
    NEW,        // Новое сообщение
    SENT,       // Сообщение передано
    ERROR,      // Ошибка
    NONE        // Не определено
}
