package ru.advantum.csptm.service.sms.model;

/**
 * Сообщение о доставке SMS
 *
 * @author abc
 */
public class SmsAckMessage {

    /** ID сообщения в csptm */
    private final long id;
    /** Статус сообщения в csptm */
    private final SmsStatus status;

    public SmsAckMessage(long id, SmsStatus status) {
        this.id = id;
        this.status = status;
    }

    public long getId() {
        return this.id;
    }

    public SmsStatus getStatus() {
        return this.status;
    }

    @Override
    public String toString() {
        return "SmsAckMessage{" +
                "id='" + id + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
