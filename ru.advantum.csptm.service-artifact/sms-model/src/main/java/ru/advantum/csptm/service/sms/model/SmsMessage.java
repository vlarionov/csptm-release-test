package ru.advantum.csptm.service.sms.model;

import java.io.Serializable;
import java.time.Instant;

/**
 * SMS сообщение
 * @author abc
 */
public class SmsMessage implements Serializable {

    /** ID сообщения в csptm (-1: отправить без получения уведомления о доставке) */
    private final long id;
    /** Номер получателя */
    private final String phone;
    /** Сообщение */
    private final String text;
    /** ID сообщения поставщика сервиса SMS (заполняется сервисом после отправки сообщения) */
    private final String providerID;
    /** Дата создания сообщения (заполняется сервисом для входящих сообщений, для исходящих не используется) */
    private final Instant creationDate;
    /** Дата доставки сообщения (заполняется сервисом для входящих сообщений, для исходящих не используется) */
    private final Instant deliveryDate;

    public SmsMessage(long id, String phone, String text, String providerID, Instant creationDate, Instant deliveryDate) {
        this.id = id;
        this.phone = phone;
        this.text = text;
        this.providerID = providerID;
        this.creationDate = creationDate;
        this.deliveryDate = deliveryDate;
    }

    public SmsMessage(SmsMessage message, String providerID) {
        this.id = message.getId();
        this.text = message.getText();
        this.phone = message.getPhone();
        this.creationDate = message.getCreationDate();
        this.deliveryDate = message.getDeliveryDate();
        this.providerID = providerID;
    }

    public long getId() {
        return this.id;
    }

    public String getText() {
        return this.text;
    }

    public String getPhone() {
        return this.phone;
    }

    public String getProviderID() {
        return providerID;
    }

    public Instant getCreationDate() {
        return creationDate;
    }

    public Instant getDeliveryDate() {
        return deliveryDate;
    }

    @Override
    public String toString() {
        return "SmsMessage{" +
                "id=" + id +
                ", phone='" + phone + '\'' +
                ", text='" + text + '\'' +
                ", providerID='" + providerID + '\'' +
                ", creationDate=" + creationDate +
                ", deliveryDate=" + deliveryDate +
                '}';
    }
}
