package ru.advantum.service.egts.cache.entity.sensor;

@SuppressWarnings("unused")
public class DigitalSensor extends AbstractSensor {
    private long value = 0L;

    public DigitalSensor(int number) {
        setNumber(number);
    }

    public DigitalSensor(int number, long value) {
        this(number);
        setValue(value);
    }

    public Long getValue() {
        return value;
    }

    @Override
    public DigitalSensor setValue(Object value)  {
        this.value = (long) value;
        return this;
    }
}
