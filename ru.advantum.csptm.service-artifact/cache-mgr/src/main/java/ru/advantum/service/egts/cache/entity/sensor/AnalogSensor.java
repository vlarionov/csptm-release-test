package ru.advantum.service.egts.cache.entity.sensor;

@SuppressWarnings("unused")
public class AnalogSensor extends AbstractSensor {
    private double value = 0D;

    public AnalogSensor(int number) {
        setNumber(number);
    }

    public AnalogSensor(int number, double value)  {
        this(number);
        setValue(value);
    }

    public Double getValue() {
        return value;
    }

    @Override
    public AnalogSensor setValue(Object value)  {
        this.value = (double) value;
        return this;
    }
}
