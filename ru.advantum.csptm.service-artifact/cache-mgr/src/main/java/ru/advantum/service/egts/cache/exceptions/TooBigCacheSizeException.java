package ru.advantum.service.egts.cache.exceptions;

@SuppressWarnings("unused")
public class TooBigCacheSizeException extends ArrayStoreException {
    private Long lastSent = 0L;

    public TooBigCacheSizeException(long lastSent) {
        super(String.valueOf(lastSent));
        setLastSent(lastSent);
    }

    public TooBigCacheSizeException(long lastSent, String message) {
        super(message);
        setLastSent(lastSent);
    }

    public Long getLastSent() {
        return lastSent;
    }

    private void setLastSent(Long lastSent) {
        this.lastSent = lastSent;
    }
}
