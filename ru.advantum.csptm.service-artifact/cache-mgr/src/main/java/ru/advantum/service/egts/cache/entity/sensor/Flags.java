package ru.advantum.service.egts.cache.entity.sensor;

@SuppressWarnings("unused")
public class Flags extends AbstractSensor {
    private byte value;

    private Flags(int number) {
        setNumber(number);
    }

    public Flags(byte value) {
        this(-1);
        setValue(value);
    }

    public Byte getValue() {
        return value;
    }

    @Override
    public Flags setValue(Object value) {
        this.value = (byte) value;
        return this;
    }
}
