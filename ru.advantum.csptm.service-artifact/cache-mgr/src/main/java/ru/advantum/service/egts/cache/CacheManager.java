package ru.advantum.service.egts.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.advantum.service.egts.cache.entity.InfoWrapper;
import ru.advantum.service.egts.cache.exceptions.NoSuchIdDataException;
import ru.advantum.service.egts.cache.exceptions.TooBigCacheSizeException;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.Instant;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * Менеджер системы "гарантированной" доставки.
 * Делался под ЕГТС, но можно прикрутить и к другим протоколам, где есть (псевдо) уникальная нумерация пакетов.
 * Базовые понятия:
 * 1. уникальный id - идентификатор, присваиваемый единице информации перед передачей и вне протокола
 * 2. сетевой номер - номер, соответствующий уникальному идентификатору, присвоенный ему при подготовке к передаче в рамках протокола
 * 3. время отправки - время в милисекундах, когда информация о переданных данных попала в кеш.
 */
@SuppressWarnings("unused")
public class CacheManager {

    public final static int RN_INDEX = 0;
    public final static int TIME_INDEX = RN_INDEX + 1;
    public final static int CNT_INDEX = RN_INDEX + 2;
    public final static int DATA_INDEX = RN_INDEX + 3;
    private final static int LAST_INDEX = RN_INDEX + 4;

    private static Logger log = LoggerFactory.getLogger(CacheManager.class);

    /**
     * Хранилище данных для каждого уникального id
     */
    private static ConcurrentSkipListMap<Long, InfoWrapper> sentInfoWrapperMap = new ConcurrentSkipListMap<>();
    /**
     * Хранилище счетчиков попыток передачи для каждого уникального id
     */
    private static ConcurrentSkipListMap<Long, Integer> sentCntMap = new ConcurrentSkipListMap<>();
    /**
     * Хранилище сетевых номеров для каждого уникального id
     */
    private static ConcurrentSkipListMap<Long, Integer> sentRnMap = new ConcurrentSkipListMap<>();
    /**
     * Хранилище времен отправки (милисекунды) для каждого уникального id
     */
    private static ConcurrentSkipListMap<Long, Long> sentTimeMap = new ConcurrentSkipListMap<>();

    /**
     * Технический метод, возвращает текущие размеры хранилищ
     *
     * @return массив, [0] - размер хранилища сетевых номеров, [1] - размер хранилища времен,
     * [2] - размер хранилища счетчиков, [3] - размер хранилища данных
     */
    public static int[] status() {
        int[] res = new int[LAST_INDEX];
        res[RN_INDEX] = sentRnMap.size();
        res[TIME_INDEX] = sentTimeMap.size();
        res[CNT_INDEX] = sentCntMap.size();
        res[DATA_INDEX] = sentInfoWrapperMap.size();
        return res;
    }

    /**
     * Генератор случайных уникальных id
     * Если им пользоваться, лучше сделать при запуске -Djava.security.egd=file:///dev/urandom иначе можно налететь на тормоза
     *
     * @return уникальный id
     */
    public static long getUniqId() throws NoSuchAlgorithmException {
        return SecureRandom.getInstance("NativePRNG").nextLong();
    }

    /**
     * Возвращает самый младший из переданных уникальных id. Если они случайные, толку от этого метода никакого.
     *
     * @return самый младший из переданных уникальных id
     */
    public static long getFirstUniqId() {
        return sentTimeMap.firstEntry().getKey();
    }

    /**
     * Возвращает самый старший из переданных уникальных id. Если они случайные, толку от этого метода никакого.
     *
     * @return самый старший из переданных уникальных id
     */
    public static long getLastUniqId() {
        return sentTimeMap.lastEntry().getKey();
    }

    /**
     * Добавляет переданный элемент в хранилище
     *
     * @param stntId уникальный id пакета данных (из БД)
     * @param rn     сетевой номер - номер переданного пакета (из протокола)
     * @return кол-во попыток передачи для этого stntId
     */
    public static Integer put(Long stntId, short rn) {
        Integer counter = 1;
        Integer recNum = sentRnMap.get(stntId);
        if (recNum != null) {
            counter = sentCntMap.get(stntId);
            sentCntMap.put(stntId, counter != null ? ++counter : 1);
        }
        sentRnMap.put(stntId, (int) rn);
        sentTimeMap.put(stntId, Instant.now().toEpochMilli());
        return counter;
    }

    /**
     * Добавляет переданный элемент в хранилище, хранит все переданные данные
     * используется если нельзя получить данные еще раз (например из кролика)
     *
     * @param infoWrapper переданные данные
     * @param rn          сетевой номер - номер переданного пакета (из протокола)
     * @return кол-во попыток передачи для этого stntId
     */
    public static Integer put(InfoWrapper infoWrapper, short rn) {
        Integer counter = put(infoWrapper.getSentId(), rn);
        sentInfoWrapperMap.put(infoWrapper.getSentId(), infoWrapper);
        return counter;
    }

    /**
     * Возвращает список неподтвержденных переданных уникальных id. Контролирует размер хранилища времен,
     * при превышении 32767 (ограничение mybatis на число элементов в списке конструкции select ... where ... in (<список>)
     * метод выбрасывает исключение с младшим непереданным уникальным id и очищает все хранилища.
     * Вызывающий код может обрабатывать дальше по своему усмотрению.
     *
     * @param timebackOffsetSeconds - сдвиг назад от которого временная метка считается просроченной
     * @return список
     */
    public static Set<Long> getUntransmitted(long timebackOffsetSeconds) throws TooBigCacheSizeException {
        int size = sentTimeMap.size();
        Set<Long> res = new TreeSet<>();

        if (size > Short.MAX_VALUE) {
            Long firstKey = sentTimeMap.firstEntry().getKey();
            log.warn("Max size of transmission set {} occured. Border is {}. Throwing exception with {}", size, Short.MAX_VALUE, firstKey);
            cleanAll();
            throw new TooBigCacheSizeException(firstKey);

        }
        long borderTime = Instant.now().toEpochMilli() - timebackOffsetSeconds * 1000;

        for (Map.Entry<Long, Long> e : sentTimeMap.entrySet()) {
            if (e.getValue() < borderTime) {
                res.add(e.getKey());
            }
        }

        return res;
    }

    /**
     * Возвращает список готовых к передаче данных. Вызывает из себя getUntransmitted(long timebackOffsetSeconds) и потом собирает нужные данные.
     * @param timebackOffsetSeconds - сдвиг назад от которого временная метка считается просроченной
     * @return - сет готовых к передаче данных (не нужно их добывать заново)
     */

    public static Set<InfoWrapper> getUntransmittedInfowrappers(long timebackOffsetSeconds) throws TooBigCacheSizeException {
        Set<InfoWrapper> res = new TreeSet<>();
        for( Long sentId: getUntransmitted(timebackOffsetSeconds)) {
            res.add(sentInfoWrapperMap.get(sentId));
        }
        return res;
    }

    /**
     * Возвращает данные для повторной отправки.
     *
     * @param sentId уникальный id
     * @return данные
     * @throws NoSuchIdDataException Если данных по какой то причине нет
     */
    public static InfoWrapper getSentInfoWrapper(Long sentId) throws NoSuchIdDataException {
        InfoWrapper infoWrapper = sentInfoWrapperMap.get(sentId);
        if (infoWrapper == null) {
            throw new NoSuchIdDataException(sentId);
        }
        return infoWrapper;
    }

    /**
     * Удалить непереданный уникальный id из всех хранилищ, кроме счетчиков
     *
     * @param sentId уникальный id
     */
    public static void removeUntransmitted(Long sentId) {
        Long time = sentTimeMap.get(sentId);
        if (time != null) {
            sentTimeMap.remove(sentId);
            sentRnMap.remove(sentId);
            sentInfoWrapperMap.remove(sentId);
        }
    }

    /**
     * Очистиить все хранилища
     */
    public static void cleanAll() {
        sentRnMap.clear();
        sentTimeMap.clear();
        sentCntMap.clear();
        sentInfoWrapperMap.clear();
    }

    /**
     * Удалить запись по сетевому номеру. Используется при получении подтверждения о приеме.
     *
     * @param rn - сетевой номер
     * @return - уникальный id удаленный из хранилищ
     */
    public static Long remove(int rn) {
        Long ret = null;
        for (Map.Entry<Long, Integer> e : sentRnMap.entrySet()) {
            if (e.getValue() == rn) {
                ret = e.getKey();
                break;
            }
        }
        if (ret != null) {
            sentRnMap.remove(ret);
            sentTimeMap.remove(ret);
            sentCntMap.remove(ret);
            sentInfoWrapperMap.remove(ret);
        }
        return ret;
    }

}
