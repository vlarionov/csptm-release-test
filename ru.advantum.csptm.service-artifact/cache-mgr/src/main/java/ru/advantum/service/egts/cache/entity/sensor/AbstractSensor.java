package ru.advantum.service.egts.cache.entity.sensor;

import ru.advantum.service.egts.cache.exceptions.MethodNotImplementedException;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.Serializable;

public abstract class AbstractSensor implements Comparable<AbstractSensor>, Serializable {
    private int number;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public abstract AbstractSensor setValue(Object value) throws MethodNotImplementedException;

    public abstract Object getValue() throws MethodNotImplementedException;

    @Override
    public int compareTo(AbstractSensor abstractSensor) {
        return Integer.compare(abstractSensor.getNumber(), getNumber());
    }

}
