package ru.advantum.service.egts.cache.entity;

import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.builder.HashCodeBuilder;
import ru.advantum.csptm.service.csd.Sensor;
import ru.advantum.protoV3.data.*;
import ru.advantum.protocore3.CorePacket;
import ru.advantum.service.egts.cache.entity.sensor.*;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@SuppressWarnings("unused")
public class InfoWrapper implements Comparable<InfoWrapper>, Serializable {

    private long serviceId = -1L;
    private long sentId = 0L;
    private long unitId = 0L;
    private long index = 0L;
    private int attempt = 0;

    private VehicleInfo vehicleInfo = new VehicleInfo();
    private VehicleTelematic vehicleTelematic = new VehicleTelematic();
    private Set<AbstractSensor> abstractSensors = new TreeSet<>();
    private Set<Sensor> jepComplexSensors = new TreeSet<>();
    private boolean alarm;

    public InfoWrapper() {
    }

    public InfoWrapper(long serviceId, long sentId) {
        this();
        setServiceId(serviceId);
        setSentId(sentId);
    }

    public InfoWrapper(long serviceId, long sentId, TrInfo trInfo, TelematicBase telematicBase, List<CorePacket> nativeSensors) {
        this(serviceId, sentId);
        setTrInfo(trInfo);
        setTelematicBase(telematicBase);
        if (nativeSensors != null) {
            for (CorePacket cp : nativeSensors) {
                AbstractSensor as = null;
                if (cp instanceof KVULong) {
                    KVULong kl = (KVULong) cp;
                    as = new DigitalSensor(kl.key.get(), kl.value.get());
                } else if (cp instanceof KVSLong) {
                    KVSLong kl = (KVSLong) cp;
                    as = new DigitalSensor(kl.key.get(), kl.value.get());
                } else if (cp instanceof KVFloat) {
                    KVFloat kl = (KVFloat) cp;
                    as = new AnalogSensor(kl.key.get(), kl.value.get());
                }
                if (as != null) {
                    getSensors().add(as);
                }
            }
        }
    }

    public InfoWrapper(long serviceId, long sentId, TrInfo trInfo, TelematicBase telematicBase) {
        this(serviceId, sentId);
        setTrInfo(trInfo);
        setTelematicBase(telematicBase);
    }

    public InfoWrapper(long serviceId, long sentId, TrInfo trInfo, TelematicBase telematicBase, Set<Sensor> jepComplexSensors) {
        this(serviceId, sentId);
        setTrInfo(trInfo);
        setTelematicBase(telematicBase);
        setJepComplexSensors(jepComplexSensors);
    }

    public InfoWrapper(long serviceId, long sentId, TrInfo trInfo, TelematicBase telematicBase, List<CorePacket> nativeSensors, TrDistance trDistance) {
        this(serviceId, sentId, trInfo, telematicBase, nativeSensors);
        setTrDistance(trDistance);
    }

    public InfoWrapper(long serviceId, long sentId, TrInfo trInfo, TelematicBase telematicBase, Set<Sensor> jepComplexSensors, TrDistance trDistance) {
        this(serviceId, sentId, trInfo, telematicBase, jepComplexSensors);
        setTrDistance(trDistance);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getSentId())
                .append(new Object[]{
                        getTrInfo(),
                        getTrDistance(),
                        getTelematicBase(),
                        getSensors(),
                        getJepComplexSensors()
                }).toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (o == null) {
            return false;
        } else if (o instanceof InfoWrapper) {
            InfoWrapper other = (InfoWrapper) o;
            if (other.getTrInfo().idTr.get() == getTrInfo().idTr.get()
                    && other.getTrInfo().utcTime.get() == getTrInfo().utcTime.get()
                    && other.getTelematicBase().lat.get() == getTelematicBase().lat.get()
                    && other.getTelematicBase().lon.get() == getTelematicBase().lon.get()
                    && other.getServiceId() == getServiceId()
                    && other.getSentId() == getSentId()
                    && other.isAlarm() == isAlarm()
                    ) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()
                + ":{sentId=" + sentId
                + ", serviceId=" + serviceId
                + ", " + getVehicleInfo().toString()
                + ", " + getVehicleTelematic().toString()
                + ", " + (
                getSensors().size() > 0 ? "sensors=" + getSensors().size()
                        : getJepComplexSensors().size() > 0 ? "jepSensors=" + getJepComplexSensors().size()
                        : "noone sensor"
        ) + " records}";
    }

    @Override
    public int compareTo(InfoWrapper infoWrapper) {
        return Long.compare(infoWrapper.getSentId(), this.getSentId());
    }

    public long getServiceId() {
        return serviceId;
    }

    public InfoWrapper setServiceId(long serviceId) {
        this.serviceId = serviceId;
        return this;
    }

    public long getSentId() {
        return sentId;
    }

    public InfoWrapper setSentId(long sentId) {
        this.sentId = sentId;
        return this;
    }

    public long getUnitId() {
        return unitId;
    }

    public InfoWrapper setUnitId(long unitId) {
        this.unitId = unitId;
        return this;
    }

    public TrInfo getTrInfo() {
        TrInfo trInfo = new TrInfo();
        trInfo.idTr.set(getVehicleInfo().getIdTr());
        trInfo.utcTime.set(getVehicleInfo().getUtcTime());
        trInfo.idEvent.set(getVehicleInfo().getIdEvent());
        trInfo.idDeviceEvent.set(getVehicleInfo().getIdDeviceEvent());
        trInfo.recordNo.set(getVehicleInfo().getRecordNo());
        return trInfo;
    }

    public InfoWrapper setTrInfo(TrInfo trInfo) {
        getVehicleInfo()
                .setIdTr(trInfo.idTr.get())
                .setUtcTime(trInfo.utcTime.get())
                .setIdEvent(trInfo.idEvent.get())
                .setIdDeviceEvent(trInfo.idDeviceEvent.get())
                .setRecordNo(trInfo.recordNo.get());
        return this;
    }

    public TelematicBase getTelematicBase() {
        TelematicBase telematicBase = new TelematicBase();
        telematicBase.lat.set(getVehicleTelematic().getLat());
        telematicBase.lon.set(getVehicleTelematic().getLon());
        telematicBase.alt.set(getVehicleTelematic().getAlt());
        telematicBase.gpsTime.set(getVehicleTelematic().getUtcTime());
        telematicBase.speed.set(getVehicleTelematic().getSpeed());
        telematicBase.heading.set(getVehicleTelematic().getHeading());
        telematicBase.opts.set((short) getVehicleTelematic().getOpts());
        return telematicBase;
    }

    public InfoWrapper setTelematicBase(TelematicBase telematicBase) {
        getVehicleTelematic()
                .setLat(telematicBase.lat.get())
                .setLon(telematicBase.lon.get())
                .setUtcTime(telematicBase.gpsTime.get())
                .setAlt(telematicBase.alt.get())
                .setSpeed(telematicBase.speed.get())
                .setHeading(telematicBase.heading.get())
                .setOpts(telematicBase.opts.get());
        return this;
    }

    public TrDistance getTrDistance() {
        TrDistance trDistance = new TrDistance();
        trDistance.odometer.set(getVehicleTelematic().getOdometer());
        trDistance.mileage.set(getVehicleTelematic().getMileage());
        return trDistance;
    }

    public InfoWrapper setTrDistance(TrDistance trDistance) {
        getVehicleTelematic()
                .setOdometer(trDistance.odometer.get())
                .setMileage(trDistance.mileage.get());
        return this;
    }

    public Set<AbstractSensor> getSensors() {
        return abstractSensors;
    }

    public AbstractSensor searchSensorNum(int number) {
        for (AbstractSensor abstractSensor : getSensors()) {
            if (abstractSensor.getNumber() == number) {
                return abstractSensor;
            }
        }
        return null;
    }

    public long getOrCreateIndex() {
        if (this.index == 0) {
            return (this.getVehicleInfo().getIdTr() << 32) | (this.getVehicleInfo().getUtcTime());
        }
        return this.index;
    }

    InfoWrapper setIndex(long index) {
        this.index = index;
        return this;
    }


    public boolean isAlarm() {
        return alarm;
    }

    public InfoWrapper setAlarm(boolean alarm) {
        this.alarm = alarm;
        return this;
    }

    public Flags getFlags() {
        for (AbstractSensor abstractSensor : getSensors()) {
            if (abstractSensor instanceof Flags) {
                return (Flags) abstractSensor;
            }
        }
        return new Flags((byte) 0);
    }

    public Set<FuelSensor> getFuels() {
        TreeSet<FuelSensor> result = new TreeSet<>();
        for (AbstractSensor abstractSensor : getSensors()) {
            if (abstractSensor instanceof FuelSensor) {
                result.add((FuelSensor) abstractSensor);
            }
        }
        return result;
    }

    public Set<AnalogSensor> getAnalogs() {
        TreeSet<AnalogSensor> result = new TreeSet<>();
        for (AbstractSensor abstractSensor : getSensors()) {
            if (abstractSensor instanceof AnalogSensor) {
                result.add((AnalogSensor) abstractSensor);
            }
        }
        return result;
    }

    public Set<DigitalSensor> getDigitals() {
        TreeSet<DigitalSensor> result = new TreeSet<>();
        for (AbstractSensor abstractSensor : getSensors()) {
            if (abstractSensor instanceof DigitalSensor) {
                result.add((DigitalSensor) abstractSensor);
            }
        }
        return result;
    }

    public Set<Sensor> getJepComplexSensors() {
        return jepComplexSensors;
    }

    public void setJepComplexSensors(Set<Sensor> jepComplexSensors) {
        this.jepComplexSensors.clear();
        this.jepComplexSensors.addAll(jepComplexSensors);
    }

    public byte[] toByteArray() {
        return SerializationUtils.serialize(this);
    }

    public InfoWrapper fromByteArray(byte[] array) {
        InfoWrapper other = (InfoWrapper) SerializationUtils.deserialize(array);
        setTrInfo(other.getTrInfo());
        setTelematicBase(other.getTelematicBase());
        setTrDistance(other.getTrDistance());
        getSensors().addAll(other.getSensors());
        return this;
    }

    public InfoWrapper addSensor(AbstractSensor abstractSensor) {
        if (abstractSensor != null) {
            getSensors().add(abstractSensor);
        }
        return this;
    }

    public VehicleInfo getVehicleInfo() {
        return vehicleInfo;
    }

    public void setVehicleInfo(VehicleInfo vehicleInfo) {
        this.vehicleInfo = vehicleInfo;
    }

    public VehicleTelematic getVehicleTelematic() {
        return vehicleTelematic;
    }

    public void setVehicleTelematic(VehicleTelematic vehicleTelematic) {
        this.vehicleTelematic = vehicleTelematic;
    }

    public int getAttempts() {
        return attempt;
    }

    public InfoWrapper increaseAttempts() {
        return setAttempt(getAttempts() + 1);
    }

    private InfoWrapper setAttempt(int attempt) {
        this.attempt = attempt;
        return this;
    }

}
