package ru.advantum.service.egts.cache.exceptions;

import java.util.NoSuchElementException;

@SuppressWarnings("unused")
public class NoSuchIdDataException extends NoSuchElementException {
    private Long sentId = 0L;

    public NoSuchIdDataException(Long sentId) {
        super("No data for sentId " + sentId);
        this.setSentId(sentId);
    }

    public NoSuchIdDataException(Long sentId, String message) {
        super(message);
        this.setSentId(sentId);
    }

    public Long getSentId() {
        return sentId;
    }

    private void setSentId(Long sentId) {
        this.sentId = sentId;
    }
}
