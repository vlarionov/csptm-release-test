package ru.advantum.service.egts.cache.exceptions;

public class MethodNotImplementedException extends Exception {
    private Long sentId = 0L;

    public MethodNotImplementedException() {
        super("Method not implemented");
    }

    public MethodNotImplementedException(String message) {
        super(message);
    }

    public MethodNotImplementedException(Long sentId) {
        super();
        this.setSentId(sentId);
    }

    public void setSentId(Long sentId) {
        this.sentId = sentId;
    }
}
