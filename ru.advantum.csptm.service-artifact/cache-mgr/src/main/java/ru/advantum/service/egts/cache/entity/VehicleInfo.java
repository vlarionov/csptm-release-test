package ru.advantum.service.egts.cache.entity;

import java.io.Serializable;

@SuppressWarnings("unused")
public class VehicleInfo implements Comparable<VehicleInfo>, Serializable {
    private long idTr;
    private long utcTime;
    private long idEvent;
    private long idDeviceEvent;
    private long recordNo;

    @Override
    public String toString() {
        return this.getClass().getSimpleName()
                + ":{idTr=" + idTr
                + ", utcTime=" + utcTime
                + ", idEvent=" + idEvent
                + ", idDeviceEvent=" + idDeviceEvent
                + ", recordNo=" + recordNo
                + "}";
    }

    public long getIdTr() {
        return idTr;
    }

    public VehicleInfo setIdTr(long idTr) {
        this.idTr = idTr;
        return this;
    }

    public long getUtcTime() {
        return utcTime;
    }

    public VehicleInfo setUtcTime(long utcTime) {
        this.utcTime = utcTime;
        return this;
    }

    public long getIdEvent() {
        return idEvent;
    }

    public VehicleInfo setIdEvent(long idEvent) {
        this.idEvent = idEvent;
        return this;
    }

    public long getIdDeviceEvent() {
        return idDeviceEvent;
    }

    public VehicleInfo setIdDeviceEvent(long idDeviceEvent) {
        this.idDeviceEvent = idDeviceEvent;
        return this;
    }

    public long getRecordNo() {
        return recordNo;
    }

    public VehicleInfo setRecordNo(long recordNo) {
        this.recordNo = recordNo;
        return this;
    }

    @Override
    public int compareTo(VehicleInfo vehicleInfo) {
        return Long.compare(vehicleInfo.getIdTr(), getIdTr());
    }
}
