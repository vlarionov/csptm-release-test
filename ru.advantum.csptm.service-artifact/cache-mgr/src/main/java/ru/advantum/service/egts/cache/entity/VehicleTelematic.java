package ru.advantum.service.egts.cache.entity;

import java.io.Serializable;

@SuppressWarnings("unused")
public class VehicleTelematic implements Serializable {
    private float lon;
    private float lat;
    private int alt;
    private int speed;
    private int heading;
    private int opts;
    private long mileage;
    private long odometer;
    private long utcTime;

    @Override
    public String toString() {
        return this.getClass().getSimpleName()
                + ":{lon=" + lon
                + ", lat=" + lat
                + ", utcTime=" + utcTime
                + ", alt=" + alt
                + ", speed=" + speed
                + ", heading=" + heading
                + ", opts=" + opts
                + ", mileage=" + mileage
                + ", odometer=" + odometer
                + "}";
    }

    public float getLon() {
        return lon;
    }

    public VehicleTelematic setLon(float lon) {
        this.lon = lon;
        return this;
    }

    public float getLat() {
        return lat;
    }

    public VehicleTelematic setLat(float lat) {
        this.lat = lat;
        return this;
    }

    public long getUtcTime() {
        return utcTime;
    }

    public VehicleTelematic setUtcTime(long utcTime) {
        this.utcTime = utcTime;
        return this;
    }

    public int getAlt() {
        return alt;
    }

    public VehicleTelematic setAlt(int alt) {
        this.alt = alt;
        return this;
    }

    public int getSpeed() {
        return speed;
    }

    public VehicleTelematic setSpeed(int speed) {
        this.speed = speed;
        return this;
    }

    public int getHeading() {
        return heading;
    }

    public VehicleTelematic setHeading(int heading) {
        this.heading = heading;
        return this;
    }

    public int getOpts() {
        return opts;
    }

    public VehicleTelematic setOpts(int opts) {
        this.opts = opts;
        return this;
    }

    public long getMileage() {
        return mileage;
    }

    public VehicleTelematic setMileage(long mileage) {
        this.mileage = mileage;
        return this;
    }

    public long getOdometer() {
        return odometer;
    }

    public VehicleTelematic setOdometer(long odometer) {
        this.odometer = odometer;
        return this;
    }

}
