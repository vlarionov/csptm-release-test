package ru.advantum.service.egts.cache.entity.sensor;

import ru.advantum.service.egts.cache.exceptions.MethodNotImplementedException;

@SuppressWarnings("unused")
public class FuelSensor extends AbstractSensor {
    private int status;
    private int levelMm;
    private int levelLit;
    private int temperature;

    public FuelSensor(int number) {
        setNumber(number);
    }

    public FuelSensor(int number, int status, int levelMm, int levelLit, int temperature) {
        this(number);
        setStatus(status);
        setLevelMm(levelMm);
        setLevelLit(levelLit);
        setTemperature(temperature);
    }

    public Object getValue() throws MethodNotImplementedException {
        throw new MethodNotImplementedException("try call gelLevel... ");
    }

    public int getStatus() {
        return status;
    }

    public FuelSensor setStatus(int status) {
        this.status = status;
        return this;
    }

    public int getLevelMm() {
        return levelMm;
    }

    public FuelSensor setLevelMm(int levelMm) {
        this.levelMm = levelMm;
        return this;
    }

    public int getLevelLit() {
        return levelLit;
    }

    public FuelSensor setLevelLit(int levelLit) {
        this.levelLit = levelLit;
        return this;
    }

    public int getTemperature() {
        return temperature;
    }

    public FuelSensor setTemperature(int temperature) {
        this.temperature = temperature;
        return this;
    }

    @Override
    public AbstractSensor setValue(Object value) throws MethodNotImplementedException{
        throw new MethodNotImplementedException("try use setLevel... and so on");
    }
}
